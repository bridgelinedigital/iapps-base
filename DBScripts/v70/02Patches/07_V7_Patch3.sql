GO
PRINT 'Update AddressLine3 column of GLAddress'
GO
IF(COL_LENGTH('GLAddress', 'AddressLine3') IS NOT NULL)
	ALTER TABLE GLAddress ALTER COLUMN AddressLine3 NVARCHAR(255) NULL
GO
PRINT 'Add NonInteractedRunId column for MKCampaignSend'
GO
IF(COL_LENGTH('MKCampaignSend', 'NonInteractedRunId') IS NULL)
	ALTER TABLE MKCampaignSend ADD NonInteractedRunId uniqueidentifier NULL
GO
PRINT 'Creating View vwEventLog'
GO
IF (OBJECT_ID('vwEventLog') IS NOT NULL)
	DROP View vwEventLog	
GO
CREATE VIEW [dbo].[vwEventLog]
AS 
SELECT L.Id,
	L.Priority,
	L.Severity,
	L.CreatedDate AS LogDate,
	L.MachineName,
	L.ActivityId,
	L.SiteId,
	L.Message,
	E.Stacktrace,
	E.InnerException,
	L.AdditionalInfo
FROM LGLog L
	LEFT JOIN LGExceptionLog E ON L.Id = E.LogId
GO
PRINT 'Creating View vwPriceSet'
GO
IF (OBJECT_ID('vwPriceSet') IS NOT NULL)
	DROP View vwPriceSet	
GO
CREATE VIEW [dbo].[vwPriceSet]
AS 
SELECT   
	A.Id,   
	A.Title,    
	A.StartDate,  
	A.EndDate,   
	A.IsSale,   
	A.IsPendingChange,   
	A.TypeId,  
	A.CreatedDate,   
	A.CreatedBy,   
	A.ModifiedDate,   
	A.ModifiedBy,  
	A.Status,  
	(SELECT COUNT(1) FROM CSCustomerGroupPriceSet WHERE PriceSetId = A.Id) AS GroupCount,
	(SELECT COUNT(SKUId) FROM vwPriceSetSku WHERE Id = A.Id) AS SkuCount,
	A.SiteId
FROM dbo.PSPriceSet A  
WHERE A.Status  = 1  
GO
PRINT 'Creating vwAutoPriceSetProductType'
GO
IF(OBJECT_ID('vwAutoPriceSetProductType') IS NOT NULL)
	DROP VIEW vwAutoPriceSetProductType
GO
CREATE VIEW [dbo].[vwAutoPriceSetProductType]
AS 
SELECT 	
	PPT.Id,
	PPT.PriceSetId,
	PT.Id AS ProductTypeId,
	PT.Title AS ProductTypeName,
	PT.SiteId,
	(SELECT COUNT(S.Id) FROM PRProductSKU S JOIN PRProduct P ON P.Id = S.ProductId WHERE P.ProductTypeID = PT.Id) As SkuCount
FROM PSPriceSetProductType PPT
	JOIN vwProductType PT ON PPT.ProductTypeId = PT.Id
GO
PRINT 'Creating vwAutoPriceSetSku'
GO
IF(OBJECT_ID('vwAutoPriceSetSku') IS NOT NULL)
	DROP VIEW vwAutoPriceSetSku
GO
CREATE VIEW [dbo].[vwAutoPriceSetSku]
AS 
SELECT 	
	PS.Id,
	PS.PriceSetId,
	PS.SKUId,
	S.Title AS SkuName,
	S.SKU,
	S.SiteId
FROM PSPriceSetSKU PS
	JOIN vwSKU S ON PS.SKUId = S.Id
GO
PRINT 'Creating vwManualPriceSetSku'
GO
IF(OBJECT_ID('vwManualPriceSetSku') IS NOT NULL)
	DROP VIEW vwManualPriceSetSku
GO
CREATE VIEW [dbo].[vwManualPriceSetSku]
AS 
SELECT 	
	M.Id, 
	M.SKUId,
	M.PriceSetId , 
	M.MinQuantity, 
	M.MaxQuantity, 
	M.Price,
	M.CreatedDate,
	M.CreatedBy,
	CFN.UserFullName AS CreatedByFullName,
	M.ModifiedDate,
	M.ModifiedBy,
	MFN.UserFullName AS ModifiedByFullName,
	S.Title AS SkuName,
	P.Title AS ProductName,
	S.SKU,
	S.ListPrice,
	PS.SiteId,
	PS.TypeId
FROM PSPriceSetManualSKU M
    INNER JOIN PSPriceSet PS ON M.PriceSetId = PS.Id
	INNER JOIN vwSku S ON M.SKUId = S.Id AND S.SiteId = PS.SiteId
	INNER JOIN vwProduct P ON P.Id = S.ProductId AND P.SiteId = PS.SiteId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = M.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = M.ModifiedBy	
GO
PRINT 'Creating vwPriceSetSku'
GO
IF(OBJECT_ID('vwPriceSetSku') IS NOT NULL)
	DROP VIEW vwPriceSetSku
GO
CREATE VIEW [dbo].[vwPriceSetSku] AS
SELECT 
	PS.Id,   
	PS.Title,    
	PS.StartDate,  
	PS.EndDate,   
	PS.IsSale,   
	PS.IsPendingChange,   
	PS.TypeId,  
	PS.CreatedDate,   
	PS.CreatedBy,   
	PS.ModifiedDate,   
	PS.ModifiedBy,  
	PS.Status, 
	PS.SiteId,
	PSS.Id AS RecordId,
	PSS.SKUId
FROM PSPriceSet PS
	INNER JOIN PSPriceSetManualSKU PSS ON PS.Id = PSS.PriceSetId
WHERE PS.TypeId = 2

UNION ALL

SELECT 
	PS.Id,   
	PS.Title,    
	PS.StartDate,  
	PS.EndDate,   
	PS.IsSale,   
	PS.IsPendingChange,   
	PS.TypeId,  
	PS.CreatedDate,   
	PS.CreatedBy,   
	PS.ModifiedDate,   
	PS.ModifiedBy,  
	PS.Status, 
	PS.SiteId,
	PSS.Id AS RecordId,
	PSS.SKUId
FROM PSPriceSet PS
	INNER JOIN PSPriceSetSKU PSS ON PS.Id = PSS.PriceSetId
WHERE PS.TypeId = 1

UNION ALL

SELECT 
	PS.Id,   
	PS.Title,    
	PS.StartDate,  
	PS.EndDate,   
	PS.IsSale,   
	PS.IsPendingChange,   
	PS.TypeId,  
	PS.CreatedDate,   
	PS.CreatedBy,   
	PS.ModifiedDate,   
	PS.ModifiedBy,  
	PS.Status, 
	PS.SiteId,
	PPT.Id AS RecordId,
	S.Id
FROM PSPriceSet PS
	INNER JOIN PSPriceSetProductType PPT ON PS.Id = PPT.PriceSetId
	INNER JOIN PRProduct P ON PPT.ProductTypeId = P.ProductTypeID
	INNER JOIN PRProductSKU S ON P.Id = S.ProductId
	LEFT JOIN PSPriceSetSKU PSS ON PSS.SKUId = S.Id AND PSS.PriceSetId = PS.Id
WHERE PS.TypeId = 1
	AND PSS.Id IS NULL
GO

IF(OBJECT_ID('vwObjectUrls') IS NOT NULL)
	DROP VIEW vwObjectUrls
GO

IF(OBJECT_ID('vwObjectUrls_Blogs') IS NOT NULL)
	DROP VIEW vwObjectUrls_Blogs
GO

IF(OBJECT_ID('vwPostUrl') IS NOT NULL)
	DROP VIEW vwPostUrl
GO

PRINT 'Modify view vwPostUrl'
GO

CREATE VIEW [dbo].[vwPostUrl] WITH SCHEMABINDING 
AS
SELECT B.Id AS Id, P.Id AS PostId,
	U.Url + '/' + CAST(YEAR(P.PostDate) AS varchar(4)) +  '/' + RIGHT('0' + CAST(DATEPART(MM, P.PostDate) AS varchar(2)), 2) +  '/' + LOWER(P.FriendlyName) AS Url,
	U.DraftUrl + '/' + CAST(YEAR(P.PostDate) AS varchar(4)) +  '/' + RIGHT('0' + CAST(DATEPART(MM, P.PostDate) AS varchar(2)), 2) +  '/' + LOWER(P.INWFFriendlyName) AS DraftUrl,
	B.ApplicationId AS SiteId
FROM dbo.BLPost P 
	JOIN dbo.BLBlog B ON B.Id = P.BlogId
	JOIN [dbo].[vwPageUrl] U ON B.BlogListPageId = U.Id
GO

PRINT 'Modify view vwObjectUrls_Blogs'
GO

CREATE VIEW [dbo].[vwObjectUrls_Blogs] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS
SELECT	SiteId, 39, Id, Id, LOWER([Url])
FROM	dbo.vwBlogUrl

UNION ALL

SELECT	SiteId, 40, PostId, PostId, LOWER([Url])
FROM	dbo.vwPostUrl
GO

PRINT 'Modify view vwObjectUrls'
GO

CREATE VIEW [dbo].[vwObjectUrls] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
AS
SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Menus

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Pages

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Blogs

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Files

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Products
GO

PRINT 'Add email template for blog comment reply notifications'
GO

IF NOT EXISTS (SELECT * FROM TAEmailTemplate WHERE [Key] = 'NewCommentReply')
BEGIN
	INSERT INTO TAEmailTemplate (Id, [Key], Title, [Description], [Subject], MimeType, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, [Status], ApplicationId, SenderDefaultEmail, MailMergeTags, ProductId, Body)
		VALUES ('59B0B815-3C02-41DF-9080-C9F17B98EAE6', 'NewCommentReply', 'New Comment Reply', 'New Comment Reply', 'New Comment Reply', 2, '00000000-0000-0000-0000-000000000000', GETUTCDATE(), '00000000-0000-0000-0000-000000000000', GETUTCDATE(), 1, '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', NULL, '_SITENAME_,_URL_,_COMMENT_', 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', '<p>The following reply has been submitted to a comment you made:</p><p>[COMMENT]</p><p>Please <a href="[URL]">click here</a> to view all comments.')
END
GO

PRINT 'Add site setting type for blog comment reply email template'
GO

IF NOT EXISTS (SELECT * FROM STSettingType WHERE [Name] = 'Comments.NewReplyEmailTemplateId')
BEGIN
	DECLARE @Sequence INT = (SELECT ISNULL(MAX(Sequence), 0) + 1 FROM STSettingType WHERE SettingGroupId = 1)

	INSERT INTO STSettingType ([Name], FriendlyName, SettingGroupId, [Sequence], ControlType, IsEnabled, IsVisible)
		VALUES ('Comments.NewReplyEmailTemplateId', 'Id of the email template used when a reply is posted to a comment', 1, @Sequence, 'Textbox', 1, 0)
END
GO

PRINT 'Add site setting value for blog comment reply email template'
GO

INSERT INTO STSiteSetting (SiteId, SettingTypeId, [Value])
	SELECT '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', T.Id, '59B0B815-3C02-41DF-9080-C9F17B98EAE6'
	FROM	STSettingType AS T
			LEFT JOIN STSiteSetting AS S ON S.SettingTypeId = T.Id
	WHERE	T.[Name] = 'Comments.NewReplyEmailTemplateId' AND
			S.Id IS NULL
GO
PRINT 'Modify stored procedure CampaignDto_Get'
GO
IF(OBJECT_ID('CampaignDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignDto_Get
GO
CREATE PROCEDURE [dbo].[CampaignDto_Get]  
(  
	@Id						uniqueidentifier = NULL,  
	@SiteId					uniqueidentifier,
	@Status					int = NULL,
	@Type					int = NULL,
	@CampaignGroupId		uniqueidentifier = NULL,
	@ParentId				uniqueidentifier = NULL,
	@CampaignRunId			uniqueidentifier = NULL,
	@PageSize				int = NULL,   
	@PageNumber				int = NULL,
	@MaxRecords				int = NULL,
	@Keyword				nvarchar(MAX) = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@IgnoreDetails			bit = NULL,
	@Filter					int = NULL
)  
AS  
BEGIN
	DECLARE @EmptyGuid uniqueidentifier, @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @Id = @EmptyGuid SET @Id = NULL
	IF @CampaignGroupId = @EmptyGuid SET @CampaignGroupId = NULL
	IF @CampaignGroupId IS NULL AND @ParentId != @EmptyGuid SET @CampaignGroupId = @ParentId

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'
	IF @Filter = 0 SET @Filter = NULL
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbQueryIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)

	IF @Id IS NULL AND @CampaignRunId IS NOT NULL
		SELECT TOP 1 @Id = CampaignId FROM MKCampaignRunHistory WHERE Id = @CampaignRunId

	IF @Id IS NULL	
	BEGIN
		INSERT INTO @tbIds
		SELECT C.Id AS Id, ROW_NUMBER() OVER (ORDER BY 
			CASE WHEN @SortClause = 'Title ASC' THEN C.Title END ASC,
			CASE WHEN @SortClause = 'Title DESC' THEN C.Title END DESC,
			CASE WHEN @SortClause = 'CreatedDate ASC' THEN C.CreatedDate END ASC,
			CASE WHEN @SortClause = 'CreatedDate DESC' THEN C.CreatedDate END DESC,
			CASE WHEN @SortClause = 'ModifiedDate ASC' THEN S.ModifiedDate END ASC,
			CASE WHEN @SortClause = 'ModifiedDate DESC' THEN S.ModifiedDate END DESC,
			CASE WHEN @SortClause IS NULL THEN C.CreatedDate END DESC		
		) AS RowNumber
		FROM MKCampaign AS C
			JOIN vwCampaignSend S ON C.Id = S.CampaignId
			LEFT JOIN MKCampaignGroup G ON G.Id = C.CampaignGroupId
		WHERE (@Id IS NULL OR C.Id = @Id) AND
			(@Type IS NULL OR C.Type = @Type) AND 
			((@CampaignGroupId IS NULL AND S.status != 4) OR G.Id = @CampaignGroupId) AND
			(@Status IS NULL OR S.Status = @Status) AND
			(@Filter IS NULL OR 
				(
					(@Filter = 1 AND (S.Status = 2 OR S.Status = 5)) OR
					(@Filter = 2 AND (S.Status = 1 OR S.Status = 6)) OR
					(@Filter = 3 AND S.Status = 3) OR
					(@Filter = 5 AND (S.Status = 1 OR S.Status = 3 OR S.Status = 6))
				)
			) AND
			(@SiteId IS NULL OR S.SiteId = @SiteId) AND
			(@Keyword IS NULL OR C.Title like @Keyword OR C.Description like @Keyword)
		OPTION (RECOMPILE)

		;WITH CTE AS(
			SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				C.Id AS Id
			FROM MKCampaign C
				JOIN @tbIds T ON T.Id = C.Id
		)
	
		INSERT INTO @tbPagedResults
		SELECT Id, RowNumber, TotalRecords FROM CTE
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
			AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	END
	ELSE
	BEGIN
		INSERT INTO @tbPagedResults
		SELECT @Id, 1, 1
	END

	SELECT C.ApplicationId, 
		G.Id AS CampaignGroupId,
		C.ConfirmationEmail,
		C.Description,
		C.Id,	       
		C.SenderEmail,
		C.SenderName,
		C.ReplyToEmail,
		ISNULL(S.Status, 1) AS Status,
		C.Title,
		C.Type,	
		S.ScheduledTimeZone, 
		A.AuthorId,
		A.AutoUpdateLists,
		A.CampaignId,
		A.CostPerEmail,	      
		A.LastPublishDate,
		S.UseLocalTimeZone AS UseLocalTimeZoneToSend,       
		S.ScheduleId,
		S.SendToNotTriggered,
		S.UniqueRecipientsOnly,
		A.WorkflowId,
		A.WorkflowStatus,
		ISNULL(A.IsEditable, 1) AS IsEditable,
		S.InteractedUsersDateRange,
		S.NonInteractedUsersPercent,
		S.NonInteractedRunId,
		S.SendCount,
		T.TotalRecords,
		A.LastPublishDate,
		C.CreatedDate,	      
		C.ModifiedDate,
		C.CreatedBy,
		C.ModifiedBy,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName
	FROM MKCampaign C
		JOIN @tbPagedResults T ON T.Id = C.Id
		LEFT JOIN MKCampaignAdditionalInfo A ON A.CampaignId = C.Id
		LEFT JOIN MKCampaignSend S ON S.CampaignId = C.Id AND S.SiteId = @SiteId
		LEFT JOIN MKCampaignGroup G ON G.Id = C.CampaignGroupId AND G.ApplicationId = @SiteId
		LEFT JOIN VW_UserFullName CU on CU.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = S.ModifiedBy
	ORDER BY T.RowNumber
	
	SELECT E.[Id],
		[CampaignId],
		[EmailSubject],
		CASE WHEN @IgnoreDetails = 1 THEN '' ELSE [EmailHtml] END AS EmailHtml,
		[CMSPageId],
		[EmailText],
		[Sequence],
		[TimeValue],
		[TimeMeasurement],
		[EmailContentType],
		[EmailContentTypePriority]
	FROM [MKCampaignEmail] E
		JOIN @tbPagedResults T ON T.Id = E.CampaignId
	WHERE @SiteId IS NULL OR E.SiteId = @SiteId

	--Getting the contact count for each campaign
	DECLARE @tbCampaignListCount TABLE 
	(
		CampaignId			uniqueidentifier,
		ContactListId		uniqueidentifier,
		ContactListTitle	nvarchar(max),
		ContactCount		int
	)

	INSERT INTO @tbCampaignListCount
	SELECT T.Id,
		CDL.DistributionListId,
		D.Title,
		ISNULL(DLS.Count, 0)
	FROM MKCampaignDistributionList CDL
		JOIN @tbPagedResults T ON T.Id = CDL.CampaignId
		JOIN MKCampaignSend S ON S.CampaignId = T.Id AND S.SiteId = @SiteId
		JOIN TADistributionLists D on D.Id = CDL.DistributionListId
		JOIN TADistributionListSite DLS ON DLS.DistributionListId = D.Id AND DLS.SiteId = @SiteId
		JOIN MKCampaign C ON C.Id = S.CampaignId
	WHERE CDL.CampaignSendId = S.Id
		
	SELECT CampaignId, SUM(ContactCount) ContactCount FROM @tbCampaignListCount
	GROUP BY CampaignId
	
	SELECT * FROM @tbCampaignListCount

	SELECT CS.CampaignId AS CampaignId,
		S.[Id],
		S.[Title],
		S.[Description],
		CAST(CONVERT(varchar(12), S.StartTime, 101) AS dateTime) AS StartDate,
		CAST(CONVERT(varchar(12), S.EndDate, 101)  AS dateTime) AS EndDate,
		S.StartTime,
		S.EndTime, 
		S.[MaxOccurrence],
		S.[Type], 
		S.[NextRunTime], 
		S.[LastRunTime],
		S.[ScheduleTypeId],
		S.[Status],
		S.[CreatedBy],
		S.CreatedDate,
		S.[ModifiedBy],
		S.ModifiedDate
	FROM TASchedule S
		JOIN MKCampaignSend CS ON S.Id = CS.ScheduleId
		JOIN @tbPagedResults T ON T.Id = CS.CampaignId AND CS.SiteId = @SiteId
END
GO
PRINT 'Modify stored procedure CampaignDto_Save'
GO
IF(OBJECT_ID('CampaignDto_Save') IS NOT NULL)
	DROP PROCEDURE CampaignDto_Save
GO
CREATE PROCEDURE [dbo].[CampaignDto_Save]
(
	@Id							uniqueidentifier = NULL OUT,
	@Title						nvarchar(MAX) = NULL,
	@Description				nvarchar(MAX) = NULL,
	@Type						int ,
	@CampaignGroupId			uniqueidentifier = NULL,
	@Status						int = NULL,
	@SenderName					nvarchar(MAX) = NULL,
	@ReplyToEmail				nvarchar(MAX) = NULL,
	@SenderEmail				nvarchar(MAX) = NULL,
	@ConfirmationEmail			nvarchar(MAX) = NULL,
	@SiteId						uniqueidentifier ,
	@Emails						xml = NULL,
	@ScheduleId					uniqueidentifier = NULL,
	@WorkflowId					uniqueidentifier = NULL,
	@WorkflowStatus				int = NULL,
	@UniqueRecipientsOnly		bit = NULL,
	@InteractedUsersDateRange	int = NULL,
	@NonInteractedUsersPercent	int = NULL,	
	@NonInteractedRunId			uniqueidentifier = NULL,
	@UseLocalTimeZoneToSend		bit = 0,
	@IsEditable					bit = 0,
	@ScheduledTimeZone			nvarchar(max) = NULL,
	@ModifiedBy					uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier, @CampaignSiteId uniqueidentifier
	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF EXISTS(SELECT * FROM MKCampaign WHERE Id != @Id AND Title = @Title AND Type = @Type AND ApplicationId = @SiteId)
	BEGIN 
		IF (@Type = 1)
			RAISERROR ('The campaign name already exists.  Please enter a unique name.', 16, 1, '')
		IF (@Type = 2)
			RAISERROR ('The response name already exists.  Please enter a unique name.', 16, 1, '')
	END

	IF @Status = 0 SET @Status = NULL
	IF @Type = 0 SET @Status = NULL

	IF @Status = 4 OR @Status = 8
	BEGIN
		IF EXISTS(SELECT 1 FROM MKCampaignGroup WHERE Name like 'Deleted Email Campaigns' AND ApplicationId = @SiteId)
			SELECT @CampaignGroupId = Id FROM MKCampaignGroup WHERE Name like 'Deleted Email Campaigns' AND ApplicationId = @SiteId
	END

	SELECT TOP 1 @CampaignSiteId = ApplicationId FROM MKCampaign WHERE Id = @Id

	IF (@Id IS NULL OR NOT EXISTS (SELECT 1 FROM MKCampaign WHERE Id = @Id))
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO MKCampaign
		(
			Id,
			Title,
			Description,
			Type,
			CampaignGroupId,
			SenderName,
			SenderEmail,
			ReplyToEmail,
			ConfirmationEmail,
			CreatedDate,
			CreatedBy,
			ApplicationId
		)
		VALUES
		(
			@Id,
			@Title,
			@Description,
			@Type,
			@CampaignGroupId,
			@SenderName,
			@SenderEmail,
			@ReplyToEmail,
			@ConfirmationEmail,
			@UtcNow,
			@ModifiedBy,
			@SiteId
		)
		
		INSERT INTO MKCampaignAdditionalInfo
		(
			Id,
			CampaignId,
			WorkflowStatus,
			WorkflowId,
			IsEditable
		)
		VALUES
		(
			NEWID(),
			@Id,
			@WorkflowStatus,
			@WorkflowId,
			@IsEditable
		)	
				
	END
	ELSE
	BEGIN
		IF @CampaignSiteId = @SiteId
		BEGIN
			UPDATE MKCampaign
			SET [Title] = ISNULL(@Title, [Title]),
				[Description] = ISNULL(@Description, [Description]),
				[CampaignGroupId] = ISNULL(@CampaignGroupId, [CampaignGroupId]),
				[SenderName] = ISNULL(@SenderName, [SenderName]),
				[SenderEmail] = ISNULL(@SenderEmail, [SenderEmail]),
				[ReplyToEmail] = ISNULL(@ReplyToEmail, [ReplyToEmail]),
				[ConfirmationEmail] = ISNULL(@ConfirmationEmail, [ConfirmationEmail]),
				[ModifiedBy] = @ModifiedBy,
				[ModifiedDate] = @UtcNow
			WHERE Id = @Id	
	  
			UPDATE MKCampaignAdditionalInfo
			SET WorkflowStatus = ISNULL(@WorkflowStatus, WorkflowStatus),
				WorkflowId = ISNULL(@WorkflowId, WorkflowId),
				IsEditable = ISNULL(@IsEditable, IsEditable)
			WHERE CampaignId = @Id
		END
	END

	DECLARE @CampaignSendId uniqueidentifier
	SELECT TOP 1 @CampaignSendId = Id FROM dbo.MKCampaignSend WHERE CampaignId = @Id AND SiteId = @SiteId
	IF @CampaignSendId IS NULL
	BEGIN
		SET @CampaignSendId = NEWID()
		INSERT INTO MKCampaignSend
		(
			Id,
			CampaignId,
			SiteId,
			Status,
			ScheduleId,
			UniqueRecipientsOnly,
			NonInteractedRunId,
			InteractedUsersDateRange,
			NonInteractedUsersPercent,
			UseLocalTimeZone,
			ScheduledTimeZone,
			CreatedDate,
			CreatedBy
		)
		VALUES
		(
			@CampaignSendId,
			@Id,
			@SiteId,
			@Status,
			@ScheduleId,
			@UniqueRecipientsOnly,
			@NonInteractedRunId,
			@InteractedUsersDateRange,
			@NonInteractedUsersPercent,
			@UseLocalTimeZoneToSend,
			@ScheduledTimeZone,
			@UtcNow,
			@ModifiedBy
		)	
	END
	ELSE
	BEGIN
		UPDATE [dbo].[MKCampaignSend]
		SET [Status] = ISNULL(@Status, [Status]),
			[ScheduleId] = ISNULL(@ScheduleId, [ScheduleId]),
			[UniqueRecipientsOnly] = ISNULL(@UniqueRecipientsOnly, [UniqueRecipientsOnly]),
			[NonInteractedRunId] = @NonInteractedRunId,
			[InteractedUsersDateRange] = ISNULL(@InteractedUsersDateRange, [InteractedUsersDateRange]),
			[NonInteractedUsersPercent] = ISNULL(@NonInteractedUsersPercent, [NonInteractedUsersPercent]),
			[UseLocalTimeZone] = ISNULL(@UseLocalTimeZoneToSend, [UseLocalTimeZone]),
			[ScheduledTimeZone] = ISNULL(@ScheduledTimeZone, [ScheduledTimeZone]),
			[ModifiedBy] = @ModifiedBy,
			[ModifiedDate] = @UtcNow
		WHERE Id = @CampaignSendId		

		IF (@Status = 4 OR @Status = 8) AND @SiteId = @CampaignSiteId
		BEGIN
			UPDATE [dbo].[MKCampaignSend]
			SET [Status] = @Status,
				[ModifiedBy] = @ModifiedBy,
				[ModifiedDate] = @UtcNow
			WHERE CampaignId = @Id
		END
	END

	IF NOT EXISTS (SELECT 1 FROM MKCampaignSendTarget WHERE CampaignSendId = @CampaignSendId AND TargetId = @SiteId)
	BEGIN
		INSERT INTO MKCampaignSendTarget	
		(
			Id,
			CampaignSendId,
			TargetId,
			TargetTypeId
		)
		VALUES
		(
			NEWID(),
			@CampaignSendId,
			@SiteId,
			2
		)
	END

	IF @Emails IS NOT NULL
	BEGIN
		DECLARE @CampaignEmailId uniqueidentifier
		SELECT TOP 1 @CampaignEmailId = Id FROM dbo.MKCampaignEmail WHERE CampaignSendId = @CampaignSendId

		IF @CampaignEmailId IS NULL
		BEGIN
			INSERT INTO MKCampaignEmail
			(
				Id,
				CampaignId,
				EmailSubject,
				EmailHtml,
				CMSPageId,
				EmailText,
				Sequence,
				TimeValue,
				TimeMeasurement,
				EmailContentType,
				EmailContentTypePriority,
				SiteId,
				CampaignSendId
			)
			SELECT
				NEWID(),
				@Id,
				Emails.email.value('@EmailSubject', 'nvarchar(max)'),
				Emails.email.value('@EmailHtml', 'nvarchar(max)'),
				Emails.email.value('@CMSPageId', 'uniqueidentifier'),
				Emails.email.value('@EmailText', 'nvarchar(max)'),
				ISNULL(Emails.email.value('@Sequence', 'int'), 0),
				Emails.email.value('@TimeValue', 'int'),
				Emails.email.value('@TimeMeasurement', 'nvarchar(max)'),
				Emails.email.value('@EmailContentType', 'int'),
				Emails.email.value('@EmailContentTypePriority', 'int'),
				@SiteId,
				@CampaignSendId
			FROM @Emails.nodes('Emails/Email') AS Emails(email)
		END
		ELSE
		BEGIN
			;WITH EmailXml AS
			(
				SELECT 
					Emails.email.value('@EmailSubject', 'nvarchar(max)') AS EmailSubject,
					Emails.email.value('@EmailHtml', 'nvarchar(max)') AS EmailHtml,
					Emails.email.value('@CMSPageId', 'uniqueidentifier') AS CMSPageId,
					Emails.email.value('@EmailText', 'nvarchar(max)') AS EmailText,
					Emails.email.value('@Sequence', 'int') AS Sequence,
					Emails.email.value('@TimeValue', 'int') AS TimeValue,
					Emails.email.value('@TimeMeasurement', 'nvarchar(max)') AS TimeMeasurement,
					Emails.email.value('@EmailContentType', 'int') AS EmailContentType,
					Emails.email.value('@EmailContentTypePriority', 'int') AS EmailContentTypePriority
				FROM @Emails.nodes('Emails/Email') AS Emails(email)
			)

			UPDATE E
			SET EmailSubject = ISNULL(X.EmailSubject, E.EmailSubject),
				EmailHtml = ISNULL(X.EmailHtml, E.EmailHtml),
				CMSPageId = ISNULL(X.CMSPageId, E.CMSPageId),
				EmailText = ISNULL(X.EmailText, E.EmailText),
				Sequence = ISNULL(X.Sequence, E.Sequence),
				TimeValue = ISNULL(X.TimeValue, E.TimeValue),
				TimeMeasurement = ISNULL(X.TimeMeasurement, E.TimeMeasurement),
				EmailContentType = ISNULL(X.EmailContentType, E.EmailContentType),
				EmailContentTypePriority = ISNULL(X.EmailContentTypePriority, E.EmailContentTypePriority)
			FROM MKCampaignEmail E, Emailxml X
			WHERE E.Id = @CampaignEmailId
		END	
	END
END
GO
PRINT 'Modify stored procedure Campaign_Get'
GO
IF(OBJECT_ID('Campaign_Get') IS NOT NULL)
	DROP PROCEDURE Campaign_Get
GO
CREATE PROCEDURE [dbo].[Campaign_Get]  
(  
	@Id				uniqueidentifier = null,  
	@ApplicationId	uniqueidentifier = null, 
	@Type			int = null,
	@PageSize		int = null,   
	@PageNumber		int = null,
	@SortColumn		nvarchar(25) = null
)  
AS  
BEGIN  
	IF (@Id IS NULL)
	BEGIN 
		SELECT ROW_NUMBER() OVER(ORDER BY a.CampaignId) as Row_Id, 
			C.[Id],
			C.[Title],
			C.[Description],
			C.[CampaignGroupId],
			S.[Status],
			C.[SenderName],
			C.[SenderEmail],
			A.[CostPerEmail],
			C.[ConfirmationEmail],
			C.[CreatedBy],
			dbo.ConvertTimeFromUtc(C.[CreatedDate] ,@ApplicationId)   CreatedDate,
			C.[ModifiedBy],  
			dbo.ConvertTimeFromUtc(C.[ModifiedDate] ,@ApplicationId) ModifiedDate,
			C.[ApplicationId],
			S.[ScheduleId],
			A.[WorkflowStatus], 
			S.[UniqueRecipientsOnly],
			A.[AutoUpdateLists], 
			dbo.ConvertTimeFromUtc(A.[LastPublishDate] ,@ApplicationId) LastPublishDate,
			A.[WorkflowId],
			A.[AuthorId],
			S.[SendToNotTriggered], 
			S.UseLocalTimeZone,
			S.ScheduledTimeZone,
			A.IsEditable,
			S.NonInteractedRunId,
			S.InteractedUsersDateRange,
			S.NonInteractedUsersPercent
		FROM [dbo].[MKCampaign] C  
			LEFT JOIN MKCampaignAdditionalInfo A ON C.Id = A.CampaignId  
			LEFT JOIN MKCampaignSend S ON C.Id = S.CampaignId AND S.SiteId = @ApplicationId
		WHERE (@Id IS NULL OR C.Id = @Id) AND 
			(@ApplicationId IS NULL OR ApplicationId = @ApplicationId) AND (@Type  IS NULL OR C.Type = @Type)
		ORDER BY c.Title
	END 
	ELSE
	BEGIN
		SELECT C.[Id],
			C.[Title],
			C.[Description],
			G.[Id] AS CampaignGroupId,
			S.[Status],
			C.[SenderName],
			C.[SenderEmail],
			A.[CostPerEmail],
			C.[ConfirmationEmail],
			C.[CreatedBy],
			dbo.ConvertTimeFromUtc(C.[CreatedDate] ,@ApplicationId)   CreatedDate,
			C.[ModifiedBy],  
			dbo.ConvertTimeFromUtc(C.[ModifiedDate] ,@ApplicationId) ModifiedDate,
			C.[ApplicationId],
			S.[ScheduleId],
			A.[WorkflowStatus], 
			S.[UniqueRecipientsOnly],
			A.[AutoUpdateLists], 
			dbo.ConvertTimeFromUtc(A.[LastPublishDate] ,@ApplicationId) LastPublishDate,
			A.[WorkflowId],
			A.[AuthorId],
			S.[SendToNotTriggered], 
			S.UseLocalTimeZone,
			S.ScheduledTimeZone,
			A.IsEditable,
			S.NonInteractedRunId,
			S.InteractedUsersDateRange,
			S.NonInteractedUsersPercent
		FROM [dbo].[MKCampaign] C  
			LEFT JOIN MKCampaignAdditionalInfo A ON C.Id = A.CampaignId  
			LEFT JOIN MKCampaignSend S ON C.Id = S.CampaignId AND S.SiteId = @ApplicationId
			LEFT JOIN MKCampaignGroup G ON G.Id = C.CampaignGroupId AND G.ApplicationId = @ApplicationId
		WHERE C.Id = @Id
	  
		SELECT E.Id, 
			E.CampaignId,
			E.[EmailSubject],
			E.[EmailHtml],  
			E.[CMSPageId],  
			E.[EmailText],  
			E.TimeValue,  
			E.TimeMeasurement,  
			E.Sequence,
			E.EmailContentType,
			E.EmailContentTypePriority,
			E.SiteId
		 FROM dbo.MKCampaignEmail E  
		 WHERE E.CampaignId = @Id  
			AND (@ApplicationId IS NULL OR E.SiteId = @ApplicationId)

		 SELECT FA.Id, 
			FA.CampaignEmailId, 
			FA.CampaignId, 
			FA.ContentId  
		 FROM dbo.MKCampaignFileAttachment FA  
		 WHERE FA.CampaignId = @Id  
			AND (@ApplicationId IS NULL OR FA.SiteId = @ApplicationId)

		 EXEC [Campaign_GetRelevantWatch] @Id, @ApplicationId
	END  
END
GO
PRINT 'Modify stored procedure Campaign_Save'
GO
IF(OBJECT_ID('Campaign_Save') IS NOT NULL)
	DROP PROCEDURE Campaign_Save
GO
CREATE PROCEDURE [dbo].[Campaign_Save]
(
	@CampaignId				uniqueidentifier,
	@CostPerEmail			decimal(10, 2) = null,
	@ScheduleId				uniqueidentifier = null,
	@Status					int = 0,
	@WorkFlowStatus			int = 0,
	@UniqueRecipientsOnly	bit = null,
	@AutoUpdateLists		bit = null,
	@LastPublishDate		dateTime = null,
	@WorkFlowId				uniqueidentifier = null,
	@AuthorId				uniqueidentifier = null,
	@ApplicationId			uniqueidentifier,
	@ModifiedBy				uniqueidentifier = null,
	@SendToNotTriggered		bit = null,
	@UseLocalTimeZoneToSend	bit = 0,
	@ScheduledTimeZone		nvarchar(max) = null,
	@InteractedUsersDateRange	int = null,
	@NonInteractedUsersPercent	int = null,
	@NonInteractedRunId			uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @EmptyGuid uniqueidentifier, @UtcNow datetime
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @UtcNow = GETUTCDATE()

	IF (@WorkFlowId = @EmptyGuid) SET @WorkFlowId = null
	IF(@ScheduleId = @EmptyGuid) SET @ScheduleId = null
	IF(@AuthorId = @EmptyGuid) SET @AuthorId = null

	IF NOT EXISTS(SELECT 1 FROM dbo.MKCampaignAdditionalInfo WHERE CampaignId = @CampaignId)
	BEGIN
		INSERT INTO [dbo].[MKCampaignAdditionalInfo]
        (
			[Id],
			[CampaignId],
			[CostPerEmail],
			[WorkflowStatus],
			[AutoUpdateLists],
			[LastPublishDate],
			[WorkflowId],
			[AuthorId]
		)
		VALUES
        ( 
			NEWID(),
			@CampaignId,
			@CostPerEmail,
			@WorkflowStatus,
			@AutoUpdateLists,
			@LastPublishDate,
			@WorkflowId,
			@AuthorId
		)
	END
	ELSE
	BEGIN
		UPDATE [dbo].[MKCampaignAdditionalInfo]
		SET [CostPerEmail] = @CostPerEmail,
			[WorkflowStatus] = @WorkflowStatus,
			[AutoUpdateLists] = @AutoUpdateLists,
			[LastPublishDate] = @LastPublishDate,
			[WorkflowId] = @WorkflowId,
			[AuthorId] = @AuthorId
		WHERE [CampaignId] = @CampaignId
	END
	
	DECLARE @CampaignSendId uniqueidentifier
	SELECT TOP 1 @CampaignSendId = Id FROM dbo.MKCampaignSend WHERE CampaignId = @CampaignId AND SiteId = @ApplicationId

	IF @CampaignSendId IS NULL
	BEGIN
		SET @CampaignSendId = NEWID()

		INSERT INTO MKCampaignSend
		(
			Id,
			CampaignId,
			SiteId,
			Status,
			ScheduleId,
			UniqueRecipientsOnly,
			SendToNotTriggered,
			UseLocalTimeZone,
			ScheduledTimeZone,
			CreatedDate,
			CreatedBy,
			InteractedUsersDateRange,
			NonInteractedUsersPercent,
			NonInteractedRunId
		)
		VALUES
		(
			@CampaignSendId,
			@CampaignId,
			@ApplicationId,
			@Status,
			@ScheduleId,
			@UniqueRecipientsOnly,
			@SendToNotTriggered,
			@UseLocalTimeZoneToSend,
			@ScheduledTimeZone,
			@UtcNow,
			@ModifiedBy,
			@InteractedUsersDateRange,
			@NonInteractedUsersPercent,
			@NonInteractedRunId
		)
	END
	ELSE
	BEGIN
		UPDATE [dbo].[MKCampaignSend]
		SET [Status] = ISNULL(@Status, [Status]),
			[ScheduleId] = ISNULL(@ScheduleId, [ScheduleId]),
			[UniqueRecipientsOnly] = ISNULL(@UniqueRecipientsOnly, [UniqueRecipientsOnly]),
			[NonInteractedRunId] = @NonInteractedRunId,
			[SendToNotTriggered] = ISNULL(@SendToNotTriggered, [SendToNotTriggered]),
			[UseLocalTimeZone] = ISNULL(@UseLocalTimeZoneToSend, [UseLocalTimeZone]),
			[ScheduledTimeZone] = ISNULL(@ScheduledTimeZone, [ScheduledTimeZone]),
			[ModifiedBy] = @ModifiedBy,
			[ModifiedDate] = @UtcNow,
			InteractedUsersDateRange = @InteractedUsersDateRange,
			NonInteractedUsersPercent = @NonInteractedUsersPercent
		WHERE Id = @CampaignSendId
	END
END
GO
PRINT 'Modify stored procedure CampaignRunHistoryDto_Fill'
GO
IF(OBJECT_ID('CampaignRunHistoryDto_Fill') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_Fill
GO
CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_Fill]
(
	@CampaignRunId			uniqueidentifier,
	@EmailPerCampaignLimit	int,
	@DebugInfo				xml = NULL OUTPUT,
	@Populated				bit = NULL OUTPUT,
	@TotalContacts			int = NULL	OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	SET @Populated = 0

	DECLARE @CampaignType int
	SELECT TOP 1 @CampaignType = C.Type FROM MKCampaignRunHistory R
		JOIN MKCampaign C ON R.CampaignId = C.Id
	WHERE R.Id = @CampaignRunId

	IF @CampaignType = 2
	BEGIN
		EXEC [CampaignRunHistoryDto_FillResponse]
			@CampaignRunId = @CampaignRunId, @DebugInfo = @DebugInfo OUTPUT

		RETURN
	END

	DECLARE @tbDebugInfo TABLE(Message nvarchar(2000))

	DECLARE @CanPopulateWorkTable int
	SET @CanPopulateWorkTable = [dbo].[CampaignRun_VerifyFillable](@CampaignRunId)
	IF (@CanPopulateWorkTable = 0)
	BEGIN
		SELECT @TotalContacts = COUNT(1) FROM MKCampaignRunWorkTable WHERE CampaignRunId = @CampaignRunId

		INSERT INTO @tbDebugInfo SELECT 'No valid campaign to fill contacts.'
	END
	ELSE
	BEGIN
		INSERT INTO @tbDebugInfo SELECT 'Start populating worktable.'

		UPDATE MKCampaignRunHistory SET ProcessingStatus = 1 WHERE Id = @CampaignRunId
		
		DECLARE @CampaignId uniqueidentifier, @CampaignSendId uniqueidentifier, @SiteId uniqueidentifier
		SELECT TOP 1 @CampaignId = CampaignId, @CampaignSendId = CampaignSendId, @SiteId = SiteId 
		FROM MKCampaignRunHistory WHERE Id = @CampaignRunId

		DECLARE @UniqueRecipientsOnly bit, 
			@InteractedUsersDateRange int,
			@NonInteractedUsersPercent int,
			@NonInteractedRunId uniqueidentifier,
			@UtcDate datetime, 
			@UseLocalTimeZone bit,
			@DoubleOptIn bit,
			@StrDoubleOptIn nvarchar(10)

		SET @UtcDate = GetUtcDate()
		SELECT TOP 1 @StrDoubleOptIn = S.Value FROM STSettingType T JOIN STSiteSetting S ON T.Id = S.SettingTypeId WHERE Name = 'Marketier.EnableDoubleOptin' AND SiteId = @SiteId
		IF (@StrDoubleOptIn IS NOT NULL AND LOWER(@StrDoubleOptIn) = 'true') SET @DoubleOptIn = 1

		SELECT TOP 1 @UseLocalTimeZone = UseLocalTimeZone,
			@UniqueRecipientsOnly = UniqueRecipientsOnly,
			@InteractedUsersDateRange = InteractedUsersDateRange,
			@NonInteractedUsersPercent = NonInteractedUsersPercent,
			@NonInteractedRunId = NonInteractedRunId
		FROM MKCampaignSend WHERE Id = @CampaignSendId
			
		INSERT INTO @tbDebugInfo SELECT 'Getting contacts for sites.'
		
		DECLARE @tbAllContactIds TABLE (Id uniqueidentifier)
		IF EXISTS (SELECT 1 FROM MKCampaignDistributionList CDL 
				JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
			WHERE CampaignSendId = @CampaignSendId AND DL.ListType = 0)
		BEGIN
			INSERT INTO @tbDebugInfo SELECT 'Getting contacts for Manual Lists'

			INSERT INTO  @tbAllContactIds 
			SELECT C.[UserId] 
			FROM MKCampaignDistributionList CDL 
				JOIN TADistributionListUser DLU ON CDL.DistributionListId = DLU.DistributionListId AND ContactListSubscriptionType != 3
				JOIN dbo.vw_contacts C ON C.UserId = DLU.UserId
				JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id
			WHERE CDL.CampaignSendId = @CampaignSendId AND DL.ListType = 0
			OPTION (RECOMPILE)

			INSERT INTO @tbDebugInfo SELECT 'Manual List Count: ' + CAST(@@ROWCOUNT AS nvarchar(20))
		END

		--Get all the contacts for AUTO LIST
		DECLARE @tbAutoListIds TABLE (Id uniqueidentifier, RowNum int)

		INSERT INTO @tbAutoListIds
		SELECT DistributionListId, ROW_NUMBER() OVER (ORDER BY DL.Id) RowNum
		FROM MKCampaignDistributionList CDL 
			JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
		WHERE CampaignSendId = @CampaignSendId AND DL.ListType = 1 AND DL.SystemListType = 1

		IF NOT EXISTS (SELECT 1 FROM @tbAutoListIds)
		BEGIN
			INSERT INTO @tbAutoListIds
			SELECT DistributionListId, ROW_NUMBER() OVER (ORDER BY DL.Id) RowNum
			FROM MKCampaignDistributionList CDL 
				JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
			WHERE CampaignSendId = @CampaignSendId AND DL.ListType = 1
		END
		
		DECLARE @Cnt int, @TempId uniqueidentifier
		SELECT @Cnt = MAX(RowNum) FROM @tbAutoListIds

		INSERT INTO @tbDebugInfo SELECT 'Getting contacts for Auto Lists Count: ' + CAST(@Cnt AS nvarchar(20))

		WHILE (@Cnt > 0)
		BEGIN
			SELECT @TempId = Id FROM @tbAutoListIds WHERE RowNum = @Cnt

			INSERT INTO @tbDebugInfo SELECT 'Start Auto List: ' + CAST(@TempId AS nvarchar(36))

			DECLARE @queryXml xml, @TotalRecords int 
			SELECT @queryXml = SearchXml FROM dbo.TADistributionListSearch a, CTSearchQuery b  
					WHERE b.Id = a.SearchQueryId AND a.DistributionListId = @TempId 
				
			INSERT INTO @tbAllContactIds
			EXEC Contact_SearchContact
				@Xml = @queryXml,
				@ApplicationId = @SiteId, 
				@MaxRecords = 0, 
				@ContactListId = @TempId, 
				@TotalRecords=@TotalRecords output,  
				@IncludeAddress = 0

			INSERT INTO @tbDebugInfo SELECT 'AutoListId:' + CAST(@TempId as nvarchar(36)) + ' | Contacts Count:' + CAST(@TotalRecords AS nvarchar(20))

			SET @Cnt = @Cnt - 1
		END
		
		-- Get all the contacts that are manually added to the campaign (Obsolete)
		IF EXISTS (SELECT 1 FROM MKCampaignUser WHERE CampaignId = @CampaignId)
		BEGIN
			INSERT @tbAllContactIds
			SELECT C.UserId
			FROM vw_contacts C
				JOIN MKCampaignUser CU ON CU.UserId = C.UserId AND CU.CampaignId = @CampaignId
				JOIN vw_contactsite CS ON CS.UserId = C.UserId

			INSERT INTO @tbDebugInfo SELECT 'Getting contacts manually added to campaign ' + CAST(@@ROWCOUNT AS nvarchar(20))
		END

		DECLARE @CurrentUser uniqueidentifier
		SELECT TOP 1 @CurrentUser = ISNULL(ModifiedBy, CreatedBy) FROM MKCampaignSend WHERE Id = @CampaignSendId AND IncludeCurrentUser = 1
		IF @CurrentUser IS NOT NULL
		BEGIN
			INSERT INTO @tbAllContactIds
			SELECT @CurrentUser
		END

		DECLARE @tbContactIds TABLE(Id uniqueidentifier primary key)

		INSERT INTO @tbContactIds
		SELECT DISTINCT C.Id 
		FROM @tbAllContactIds C 
			LEFT OUTER JOIN USUserUnsubscribe U ON U.UserId = C.Id AND (U.CampaignId IS NULL OR U.CampaignId = @CampaignId)
			LEFT OUTER JOIN USResubscribe R ON R.UserId = C.Id AND R.CampaignId = @CampaignId
		WHERE (U.Id IS NULL OR R.Id IS NOT NULL)
		AND (@DoubleOptIn IS NULL OR C.Id IN (SELECT ContactId FROM [USEmailOptIn] WHERE Type = 3))

		DECLARE @Count int
		SELECT @Count = COUNT(1) FROM @tbContactIds
		INSERT INTO @tbDebugInfo SELECT 'Total contacts :' + CAST(@Count AS nvarchar(20))

		-- If the campaign type is 3(flow)remove user which are in flow state
		IF (@CampaignType = 3)
		BEGIN
			DELETE C FROM @tbContactIds C
			INNER JOIN MAFlowUserState M ON C.Id = M.UserId
			WHERE M.FlowType= 1 OR M.FlowType = 3
		END

		-- Use unique recipients logic if there are more than one campaign run
		IF @UniqueRecipientsOnly = 1 AND EXISTS (SELECT 1 FROM MKCampaignRunHistory WHERE CampaignId = @CampaignId AND Id != @CampaignRunId)
		BEGIN
			DECLARE @tbSentIds TABLE (Id uniqueidentifier primary key)
			INSERT INTO @tbSentIds
			SELECT DISTINCT UserId FROM MKEmailSendLog WHERE CampaignId = @CampaignId AND ApplicationId = @SiteId

			DELETE C FROM @tbContactIds C
			WHERE EXISTS(SELECT 1 FROM @tbSentIds S WHERE C.Id = S.Id)

			SELECT @Count = COUNT(1) FROM @tbContactIds
			INSERT INTO @tbDebugInfo SELECT 'Total unique contacts :' + CAST(@Count AS nvarchar(20))
		END

		-- Interacted Users Only
		IF @InteractedUsersDateRange IS NOT NULL AND @InteractedUsersDateRange > 0
		BEGIN
			DECLARE @tbNonInteracted TABLE (Id uniqueidentifier)
			DELETE T OUTPUT deleted.Id INTO @tbNonInteracted
			FROM @tbContactIds T
				JOIN vw_contacts C ON T.Id = C.Id
			WHERE C.LastInteractionDate IS NULL 
				OR DATEDIFF(DD, C.LastInteractionDate, @UtcDate) > @InteractedUsersDateRange

			SELECT @Count = COUNT(1) FROM @tbContactIds
			INSERT INTO @tbDebugInfo SELECT 'After resolving interacted users :' + CAST(@Count AS nvarchar(20))

			IF @NonInteractedUsersPercent IS NOT NULL AND @NonInteractedUsersPercent > 0
			BEGIN
				DECLARE @NonInteractedCount int, @TargetPercent int
				SELECT @NonInteractedCount = COUNT(1) FROM @tbNonInteracted

				SET @TargetPercent = (@NonInteractedCount * @NonInteractedUsersPercent) / 100

				INSERT INTO @tbContactIds
				SELECT TOP (@TargetPercent) Id FROM @tbNonInteracted

				SELECT @Count = COUNT(1) FROM @tbContactIds
				INSERT INTO @tbDebugInfo SELECT 'After resolving non-interacted users :' + CAST(@Count AS nvarchar(20))
			END
		END

		-- Not opened users
		IF @NonInteractedRunId IS NOT NULL AND @NonInteractedRunId != dbo.GetEmptyGUID()
		BEGIN
			DECLARE @tbOpened TABLE (Id uniqueidentifier primary key)
			INSERT INTO @tbOpened
			SELECT DISTINCT UserId FROM MKEmailSendLog WHERE CampaignRunId = @NonInteractedRunId AND Opened = 1

			DELETE C FROM @tbOpened T 
				JOIN @tbContactIds C ON T.Id = C.Id

			SELECT @Count = COUNT(1) FROM @tbContactIds
			INSERT INTO @tbDebugInfo SELECT 'After resolving non-interacted users :' + CAST(@Count AS nvarchar(20))
		END

		IF EXISTS (SELECT 1 FROM MKCampaignRunWorkTable W WHERE CampaignRunId = @CampaignRunId)
		BEGIN
			DELETE C FROM @tbContactIds C
			WHERE EXISTS (SELECT 1 FROM MKCampaignRunWorkTable W WHERE CampaignRunId = @CampaignRunId AND W.UserId = C.Id)

			SELECT @Count = COUNT(1) FROM @tbContactIds
			INSERT INTO @tbDebugInfo SELECT 'After removing already filled contacts :' + CAST(@Count AS nvarchar(20))
		END

		DECLARE @tblContacts TYSearchContact
		INSERT @tblContacts
		SELECT DISTINCT C.UserId, 
			C.UserId, 
			C.FirstName, 
			C.MiddleName, 
			C.LastName, 
			C.CompanyName, 
			C.BirthDate,
			C.Gender, 
			C.AddressId, 
			C.Status, 
			C.HomePhone, 
			C.MobilePhone, 
			C.OtherPhone, 
			C.ImageId, 
			C.Notes, 
			C.Email, 
			C.ContactType,
			C.ContactSourceId,
			0,
			NULL,
			NULL
		FROM vw_contacts C
			JOIN @tbContactIds T ON C.Id = T.Id
		WHERE C.Status = 1
		
		DECLARE @LftValue int, @RgtValue int, @MasterSiteId uniqueidentifier
		SELECT @LftValue = LftValue, @RgtValue = RgtValue, @MasterSiteId = MasterSiteId FROM SISite WHERE Id = @SiteId

		-- Update SiteId
		IF EXISTS(SELECT TOP 1 Id FROM SISite WHERE LftValue > @LftValue AND RgtValue < @RgtValue)
		BEGIN
			IF @UseLocalTimeZone = 1
			BEGIN
				INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for local timezone - yes'

				-- If user has access to current site, then use current site
				;WITH PrimaryCTE AS
				(
					SELECT CS.UserId, CS.SiteId, ROW_NUMBER() OVER (PARTITION BY CS.UserId ORDER BY S.LftValue) AS PRank
					FROM vw_contactSite CS
						JOIN @tblContacts T ON T.UserId = CS.UserId
						JOIN SISite S ON S.Id = CS.SiteId
					WHERE CS.Status = 1 AND CS.IsPrimary = 1
						AND S.Status = 1 AND S.MasterSiteId = @MasterSiteId AND S.LftValue >= @LftValue AND S.RgtValue <= @RgtValue
				)

				UPDATE T SET T.SiteId = P.SiteId
				FROM @tblContacts T 
					JOIN PrimaryCTE P ON T.UserId = P.UserId
				WHERE P.PRank = 1

				INSERT INTO @tbDebugInfo SELECT 'Updating Primary SiteId for Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))

				IF EXISTS (SELECT 1 FROM @tblContacts WHERE SiteId IS NULL)
				BEGIN
					UPDATE T SET T.SiteId = CS.SiteId
					FROM vw_contactSite CS
						JOIN @tblContacts T ON T.UserId = CS.UserId
					WHERE T.SiteId IS NULL AND CS.SiteId = @SiteId AND CS.Status = 1

					INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
				END

				IF EXISTS (SELECT 1 FROM @tblContacts WHERE SiteId IS NULL)
				BEGIN
					UPDATE @tblContacts SET SiteId = @SiteId WHERE SiteId IS NULL

					INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for missing Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
				END
			END
			ELSE
			BEGIN
				INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for local timezone - no'

				UPDATE T SET T.SiteId = CS.SiteId
				FROM vw_contactSite CS
					JOIN @tblContacts T ON T.UserId = CS.UserId
				WHERE CS.SiteId = @SiteId AND CS.Status = 1

				INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))

				IF EXISTS (SELECT 1 FROM @tblContacts WHERE SiteId IS NULL)
				BEGIN
					;WITH PrimaryCTE AS
					(
						SELECT CS.UserId, CS.SiteId, ROW_NUMBER() OVER (PARTITION BY CS.UserId ORDER BY S.LftValue) AS PRank
						FROM vw_contactSite CS
							JOIN @tblContacts T ON T.UserId = CS.UserId
							JOIN SISite S ON S.Id = CS.SiteId
						WHERE CS.Status = 1 AND CS.IsPrimary = 1
							AND S.Status = 1 AND S.MasterSiteId = @MasterSiteId AND S.LftValue >= @LftValue AND S.RgtValue <= @RgtValue
					)

					UPDATE T SET T.SiteId = P.SiteId
					FROM @tblContacts T 
						JOIN PrimaryCTE P ON T.UserId = P.UserId
					WHERE T.SiteId IS NULL AND P.PRank = 1

					INSERT INTO @tbDebugInfo SELECT 'Updating Primary SiteId for Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
				END
			END
		END
		ELSE
			UPDATE @tblContacts SET SiteId = @SiteId

		DECLARE @RunDate datetime, @LocalRunDate datetime, @EmptyGuid uniqueidentifier
		SET @EmptyGuid = dbo.GetEmptyGUID()
		SELECT TOP 1 @Rundate = RunDate FROM MKCampaignRunHistory WHERE Id = @CampaignRunid

		-- Update SendDate
		IF @UseLocalTimeZone = 1
		BEGIN
			UPDATE @tblContacts SET SendDate = @Rundate WHERE SiteId = @SiteId

			INSERT INTO @tbDebugInfo SELECT 'Updating send date for same site contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
			
			SET @LocalRunDate = dbo.ConvertTimeFromUtc(@RunDate, @EmptyGuid)
			DECLARE @tbSiteRunDate TABLE(SiteId uniqueidentifier primary key, RunDate datetime)
			INSERT INTO @tbSiteRunDate
			SELECT Id, dbo.ConvertTimeToUtcUsingSiteTimeZone(@LocalRunDate, Id) FROM SISite

			UPDATE C SET C.SendDate = T.RunDate
			FROM @tblContacts C
				JOIN @tbSiteRunDate T ON C.SiteId = T.SiteId
			WHERE C.SendDate IS NULL

			INSERT INTO @tbDebugInfo SELECT 'Updating send date for other site contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
		END
		ELSE
		BEGIN
			UPDATE @tblContacts SET SendDate = @Rundate
		END

		;WITH CTE AS
		(
			SELECT ROW_NUMBER() OVER(PARTITION BY C.UserId ORDER BY [Email]) AS RowNumber,
				C.UserId, 
				FirstName , 
				MiddleName ,
				LastName ,
				CompanyName ,
				BirthDate, 
				Gender,
				AddressId, 
				Status,
				HomePhone,
				MobilePhone, 
				OtherPhone, 
				Notes,
				Email,
				SiteId,
				SendDate
			FROM @tblContacts C 
		)

		INSERT INTO MKCampaignRunWorkTable 
		(
			CampaignRunId, 
			UserId, 
			FirstName, 
			MiddleName, 
			LastName,
			CompanyName, 
			BirthDate, 
			Gender, 
			AddressId, 
			Status, 
			HomePhone, 
			MobilePhone, 
			OtherPhone,
			Notes, 
			Email,
			SiteId,
			SendDate
		)
		SELECT TOP(@EmailPerCampaignLimit) 
			@CampaignRunId,
			UserId, 
			FirstName, 
			MiddleName,
			LastName,
			CompanyName,
			BirthDate, 
			Gender,
			AddressId, 
			Status,
			HomePhone,
			MobilePhone, 
			OtherPhone, 
			Notes,
			Email,
			SiteId,
			SendDate
		FROM CTE
		WHERE RowNumber = 1
		ORDER BY SendDate

		INSERT INTO @tbDebugInfo SELECT 'Worktable Populated count:' + CAST(@@ROWCOUNT AS nvarchar(20))

		IF (@CampaignType = 3)
		BEGIN
			-- Add the remaining user in MAFlowUserState
			EXEC Flow_AddEmailBlastUserStateFromCampaign @CampaignRunId, @CampaignId
		END
		
		SET @Populated = 1

		DECLARE @ExternalId int
		SELECT @ExternalId = MAX(ExternalId) FROM MKCampaignRunHistory 
		SELECT @TotalContacts = COUNT(1) FROM MKCampaignRunWorkTable WHERE CampaignRunId = @CampaignRunId

		UPDATE MKCampaignRunHistory 
		SET ProcessingStatus = 2,
			TotalContacts = @TotalContacts,
			ExternalId = ISNULL(@ExternalId, 0) + 1
		WHERE Id = @CampaignRunId
	END
			
	SET @DebugInfo = (SELECT Message FROM @tbDebugInfo For XML PATH (''), ROOT('Messages'))
END
GO

GO
IF(OBJECT_ID('ShippingOption_GetShippingOptions') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetShippingOptions
GO
CREATE PROCEDURE [dbo].[ShippingOption_GetShippingOptions]
(@Id uniqueidentifier=null
,@ApplicationId uniqueidentifier=null)
AS
SELECT 
	OSO.[Id],
	OSO.[ShipperId],
	OSO.[Name],
	OSO.[Description],
	OSO.[CustomId],
	OSO.[NumberOfDaysToDeliver],
	OSO.[CutOffHour],
	OSO.[SaturdayDelivery],
	OSO.[AllowShippingCoupon],
	OSO.[IsPublic],
	OSO.[IsInternational],
	OSO.[ServiceCode],
	OSO.[ExternalShipCode],
	OSO.[BaseRate],
	OSO.[SiteId],
	OSO.[Status],
	OSO.[Sequence],
	OSO.[CreatedBy],
	OSO.CreatedDate AS CreatedDate,
	OSO.[ModifiedBy],
	 OSO.ModifiedDate AS ModifiedDate,
	1 IsActive,
	OSO.IsShared
FROM ORShippingOption OSO
Where ((@Id is null and @ApplicationId is null) or Id = @Id)
     OR
	((@Id is null and @ApplicationId is null) or (@Id is null and OSO.SiteId=@ApplicationId)) 

go

PRINT 'Creating procedure Content_GetUrls'
GO
IF(OBJECT_ID('Content_GetUrls') IS NOT NULL)
	DROP PROCEDURE Content_GetUrls
GO

CREATE PROCEDURE [dbo].[Content_GetUrls]   
(
	@ContentIds		nvarchar(max) = NULL, 
	@PageIds		nvarchar(max) = NULL
)
AS  
BEGIN  
	DECLARE   
		@id uniqueidentifier,  
		@text varchar(max),  
		@SiteId uniqueidentifier,
		@ModifiedDate DateTime, @MaxModifiedDate DateTime, @ObjectTypeId int

	DECLARE @curSQL nvarchar(max)
  
	IF (@PageIds is not null)
	BEGIN
		DELETE FROM Content_AssetLinks Where PageId IN (@PageIds)
	END

	SELECT @MaxModifiedDate = ISNULL(Min(ContentModifiedDate), '1900-01-01') FROM Content_AssetLinks
	IF (@ContentIds is not null AND @ContentIds<>'')
	BEGIN
		SET @curSQL ='SELECT Id,Text, ApplicationId, Isnull(ModifiedDate, CreatedDate) As ModifiedDate, ObjectTypeId   
			From COContent 
			where Id IN (SELECT VALUE FROM dbo.SplitComma(''' + @ContentIds + ''', '',''))'
	END
	ELSE
	BEGIN
		SET @curSQL ='SELECT Id,Text, ApplicationId, Isnull(ModifiedDate, CreatedDate) As ModifiedDate, ObjectTypeId   
			From COContent 
			where Status!= 3 and Text is not null and Text not like  ''''  
			And ( ( Isnull(ModifiedDate, CreatedDate) >=  @MaxModifiedDate  And ObjectTypeId IN (7,13)) 
				) ORDER BY ModifiedDate'
	END

	SET @curSQL='Declare textCursor cursor for '+ @curSQL
	EXEC sp_executesql  @curSQL, N'@MaxModifiedDate DateTime', @MaxModifiedDate
	
	OPEN textCursor  
	FETCH NEXT FROM textCursor INTO @id, @text, @SiteId, @ModifiedDate, @ObjectTypeId  
	WHILE @@FETCH_STATUS = 0      
	BEGIN      
		DELETE FROM Content_AssetLinks WHERE ContentId = @id
		
		IF (@text IS NOT NULL AND @text <> '')
		BEGIN 
			DECLARE @link nvarchar(max), @links nvarchar(max), @hreflinks  nvarchar(max), @srclinks  nvarchar(max), @bckgrdimglinks  nvarchar(max)

			SELECT @hreflinks = dbo.RegexSelectAll(@text,'href\s*=\s*(?:\"(?<1>[^"]*)"|(?<1>\S+))' ,'|')
			SELECT @srclinks = dbo.RegexSelectAll(@text,'src\s*=\s*(?:\"(?<1>[^"]*)"|(?<1>\S+))' ,'|')
			SELECT @bckgrdimglinks = dbo.RegexSelectAll(@text,'background-image:url\(\s*(?:\"(?<1>[^"]*)"|(?<1>\S+))\)' ,'|')
			SELECT @links = @hreflinks  			
			IF (@srclinks IS NOT NULL AND @srclinks <> '') 
				SELECT @links = @links + '|' + @srclinks

			IF (@bckgrdimglinks IS NOT NULL AND @bckgrdimglinks <> '') 
				SELECT @links = @links + '|' + @bckgrdimglinks

			IF (SELECT charindex('|', @links)) = 1
				SET @links = SUBSTRING(@links, 2, len(@links) -1)
				
			DECLARE @tblLinks Table(link nvarchar(max))
			INSERT INTO @tblLinks
				SELECT Value FROM dbo.SplitComma(@links, '|')WHERE Value not like '%data:image%'

			WHILE EXISTS(SELECT * FROM @tblLinks)  
			BEGIN  
				SET @link = (SELECT TOP 1 link FROM @tblLinks)			
				
				INSERT INTO Content_AssetLinks
					(SiteId, 
					ContentId, 
					AssetLink, 
					ContentModifiedDate, 
					PageId, 
					PageMapNodeId, 
					PageSiteId, 
					ObjectTypeId) 
				SELECT 
					@SiteId, 
					@id, 
					dbo.UrlDecode(@link), 
					@ModifiedDate, 
					pg.PageDefinitionId, 
					m.PageMapNodeId, 
					pg.SiteId As PageSiteId, 
					@ObjectTypeId
				FROM PageDefinition pg
					INNER JOIN PageMapNodePageDef m on pg.PageDefinitionId = m.PageDefinitionId
					INNER JOIN vw_PageContainerXmlDetails vg on vg.PageDefinitionId = pg.PageDefinitionId
				WHERE vg.ContentId =  @id  
						
				DELETE FROM @tblLinks WHERE link = @link
			END
		END
		FETCH NEXT FROM textCursor INTO  @id, @text, @SiteId, @ModifiedDate, @ObjectTypeId  
	END  
	CLOSE textCursor  
	DEALLOCATE textCursor  

END
GO
PRINT 'Modify stored procedure Bundle_GetBundleItems'
GO
IF(OBJECT_ID('Bundle_GetBundleItems') IS NOT NULL)
	DROP PROCEDURE Bundle_GetBundleItems
GO
CREATE PROCEDURE [dbo].[Bundle_GetBundleItems]
(  
	@Id				uniqueidentifier = null,
	@ApplicationId	uniqueidentifier = null
)  
AS  
BEGIN  
	SELECT S.[Id]  
		,S.[ProductId]  
		,S.[SiteId]  
		,S.[SKU]  
		,S.[Id] As SkuId
		,S.[Title]  
		,S.[Description]  
		,S.[Sequence]  
		,S.[IsActive]  
		,S.[IsOnline]  
		,S.[ListPrice]  
		,S.[UnitCost]  
		,S.[PreviousSoldCount]  
		,S.[CreatedBy]  
		,S.[CreatedDate]
		,S.[ModifiedBy]  
		,S.ModifiedDate
		,S.[Status]  
		,S.[Measure]  
		,S.[OrderMinimum]  
		,S.[OrderIncrement] 
		,S.[Length]
		,S.[Height]
		,S.[Width]
		,S.[Weight]
		,S.[SelfShippable]  
		,S.AvailableForBackOrder
		,S.MaximumDiscountPercentage
		,S.BackOrderLimit
		,S.IsUnlimitedQuantity
		,S.FreeShipping
		,S.UserDefinedPrice
		,BI.Id AS [BundleItemId]  
		,BI.[BundleProductId]  
		,BI.[ChildSkuId]  
		,BI.[ChildSkuQuantity]  
		,BI.[CreatedBy] as [BundleSkuCreatedBy]  
		,BI.[CreatedDate] as [BundleSkuCreatedDate]  
		,BI.[ModifiedBy] as [BundleSkuModifiedBy]  
		,BI.[ModifiedDate] as [BundleSkuModifiedDate]
		,(SELECT SUM(Quantity) FROM [dbo].[vwAllocatedQuantityPerWarehouse] A 
			INNER JOIN INWarehouseSite SI ON A.WarehouseId = SI.WarehouseId
			WHERE ProductSKUId = S.[Id] AND SI.SiteId = @ApplicationId) AS AllocatedQuantity
		,(SELECT CASE S.IsUnlimitedQuantity WHEN 1 THEN 9999999 ELSE SUM(Quantity) END		
			FROM INInventory I INNER JOIN INWarehouseSite SI ON I.WarehouseId = SI.WarehouseId 
			Where SKUId = S.[Id]  AND SI.SiteId = @ApplicationId) AS QuantityOnHand    --- sum of all warehouse -- find bundle item wise minimum in API or helper
	FROM PRBundleItem BI
		LEFT JOIN PRProductSKU S ON S.Id = BI.ChildSkuId
	WHERE BI.[BundleProductId]=@Id  
	  AND Status = dbo.GetActiveStatus() -- Select only Active Attributes  
	Order By BI.[Id]  
END
GO


Update TAEmailTemplate SET Body = replace(Body,'SKU Name :','SKU :' )
Where [Key]='LowQuantityAlertEmail'

GO


PRINT 'Creating vwSKU'
GO
IF(OBJECT_ID('vwSKU') IS NOT NULL)
	DROP VIEW vwSKU
GO
CREATE VIEW [dbo].[vwSKU]
AS
WITH CTE AS
(
	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		ISNULL(P.ModifiedDate, P.CreatedDate) AS ModifiedDate, 
		ISNULL(P.ModifiedBy, P.CreatedBy) AS  ModifiedBy
	FROM PRProductSku P

	UNION ALL

	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		P.ModifiedDate, 
		P.ModifiedBy
	FROM PRProductSKUVariantCache P
)

SELECT	C.Id, SK.ProductId, C.SiteId, SK.SiteId AS SourceSiteId, C.Title, C.[Description], SK.SKU, C.IsActive, C.IsOnline, C.UnitCost,
		C.WholesalePrice, C.PreviousSoldCount, C.Measure, C.OrderMinimum, C.OrderIncrement, C.HandlingCharges,
		SK.Height, SK.Width, SK.[Length], SK.[Weight], C.SelfShippable, C.AvailableForBackOrder, C.BackOrderLimit, C.IsUnlimitedQuantity,
		C.MaximumDiscountPercentage, C.FreeShipping, C.UserDefinedPrice, C.[Sequence], SK.[Status],
		C.ListPrice, ISNULL(ATS.Inventory, 0) AS Quantity, ISNULL(ATS.Quantity, 0) AS AvailableToSell,
		SK.CreatedDate, SK.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		C.ModifiedDate, C.ModifiedBy, MFN.UserFullName AS ModifiedByFullName,
		ISNULL(PC.Title, P.Title) AS ProductName, P.ProductTypeId, PT.IsDownloadableMedia,
		case when ISNULL(PC.IsActive,P.IsActive)=1 then C.IsActive else 0 end IsSellable
FROM	CTE AS C
		INNER JOIN PRProductSku AS SK ON SK.Id = C.Id
		INNER JOIN PRProduct AS P ON P.Id = SK.ProductId
		LEFT JOIN PRProductVariantCache AS PC ON PC.Id = SK.ProductId AND PC.SiteId = C.SiteId
		INNER JOIN PRProductType AS PT ON PT.Id = P.ProductTypeID
		LEFT JOIN vwAvailableToSellPerSite AS ATS ON ATS.SKUId = SK.Id AND ATS.SiteId = SK.SiteId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = SK.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = SK.ModifiedBy
GO

PRINT 'Modify stored procedure Product_GetProduct'
GO
IF(OBJECT_ID('Product_GetProduct') IS NOT NULL)
	DROP PROCEDURE Product_GetProduct
GO
CREATE PROCEDURE [dbo].[Product_GetProduct](
@Id uniqueidentifier = null,
@ApplicationId uniqueidentifier=null,
@skuId uniqueidentifier = null,
@ProductIDUser nvarchar(50) =null
)    
AS    

if(@skuId is not null and @Id is null)
begin 
	select @Id = ProductId from PRProductSku where Id=@skuId
End 

if(@ProductIDUser is not null and @Id is null)
begin 
	select @Id = Id from PRProduct where ProductIDUser=@ProductIDUser
End 


	SELECT     
	 [Prd].[Id],    
	 [Prd].ProductIDUser,    
	 [Prd].[Title],    
	 [Prd].[ShortDescription],    
	 [Prd].[LongDescription],    
	 [Prd].[Description],    
	 [Prd].[Keyword],    
	 [Prd].[IsActive],    
	 [Prd].[ProductTypeID],    
	 vPT.[ProductTypeName] as ProductTypeName,    
	 vPT.[ProductTypePath] +  N'id-' + Prd.ProductIDUser + '/' + Prd.UrlFriendlyTitle as DefaultUrl, Prd.UrlFriendlyTitle,    
	 vPT.[IsDownloadableMedia] ,
	 [Prd].[ProductStyle],    
	 dbo.ConvertTimeFromUtc([Prd].CreatedDate,@ApplicationId)CreatedDate,    
	 [Prd].CreatedById CreatedBy,    
	 dbo.ConvertTimeFromUtc([Prd].ModifiedDate,@ApplicationId)ModifiedDate,    
	 [Prd].ModifiedById ModifiedBy,    
	 [Prd].Status,    
	 [Prd].IsBundle,    
	 dbo.ConvertTimeFromUtc( [Prd].BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,    
	 [Prd].TaxCategoryID,    
	 [Prd].PromoteAsNewItem,
	 [Prd].PromoteAsTopSeller,
	 [Prd].ExcludeFromDiscounts,
	 [Prd].ExcludeFromShippingPromotions,
	 [Prd].Freight,
	 [Prd].Refundable,
	 [Prd].TaxExempt,
	 [Prd].SEOTitle,
	 [Prd].SEOH1,
	 [Prd].SEODescription,
	 [Prd].SEOKeywords,
	 [Prd].SEOFriendlyUrl,
	 (Select Count(*) From PRProductSKU where ProductId= [Prd].Id) as NumberOfSKUs    
	FROM vwProduct Prd INNER JOIN vwProductTypePath vPT on [Prd].ProductTypeID = vPT.ProductTypeId    
	WHERE [Prd].Id = @Id and SiteId=@ApplicationId    
	    
	    
	select     
	 a.[Id],    
	 a.[AttributeDataType],    
	 a.[Title],    
	 av.[Value],    
	 pav.[ProductId]    
	from PRProductAttributeValue pav join ATAttributeEnum av on av.[Id] = pav.[AttributeEnumId]    
	 join ATAttribute a on av.[AttributeID]=a.[Id]     
	     
	where pav.[ProductId] = @Id    
	order by [ProductId],av.[AttributeID];

GO
PRINT 'Modify stored procedure Sku_GetByXml'
GO
IF(OBJECT_ID('Sku_GetByXml') IS NOT NULL)
	DROP PROCEDURE Sku_GetByXml
GO
CREATE  PROCEDURE [dbo].[Sku_GetByXml](@SKUs xml=null,@SKUString xml=null,@ApplicationId uniqueidentifier=null)  
  
AS  
  
  IF @SKUs is not null
SELECT [Id]  
      ,[ProductId]  
      ,[SiteId]  
      ,[SKU]  
	  ,[Title]  
      ,[Description]  
      ,[Sequence]  
      ,[IsActive]  
      ,[IsOnline]  
      ,[ListPrice]  
      ,[UnitCost]  
      ,[PreviousSoldCount]  
	  ,[CreatedBy]  
	  ,CreatedDate AS [CreatedDate]  
	  ,[ModifiedBy]  
	  ,ModifiedDate AS [ModifiedDate]  
	  ,[Status]  
	  ,[Measure]  
	  ,[OrderMinimum]  
	  ,[OrderIncrement] 
	  ,[HandlingCharges] 
	  ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	  ,[IsUnlimitedQuantity]
	  ,FreeShipping
	,UserDefinedPrice
  FROM [vwSKU] S  
Inner Join @SKUs.nodes('/GenericCollectionOfGuid/guid')tab(col)   
ON  S.Id =tab.col.value('text()[1]','uniqueidentifier')  
Where S.SiteId=@ApplicationId
ELSE
SELECT [Id]  
      ,[ProductId]  
      ,[SiteId]  
      ,[SKU]  
	  ,[Title]  
      ,[Description]  
      ,[Sequence]  
      ,[IsActive]  
      ,[IsOnline]  
      ,[ListPrice]  
      ,[UnitCost]  
      ,[PreviousSoldCount]  
	  ,[CreatedBy]  
	 ,CreatedDate AS [CreatedDate]  
	  ,[ModifiedBy]  
	  ,ModifiedDate AS [ModifiedDate]  
	  ,[Status]  
	  ,[Measure]  
	  ,[OrderMinimum]  
	  ,[OrderIncrement] 
	  ,[HandlingCharges] 
	  ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	  ,[IsUnlimitedQuantity]
	  ,FreeShipping
	 ,UserDefinedPrice
  FROM [vwsku] S   
Inner Join @SKUString.nodes('/GenericCollectionOfString/string')tab(col)   
ON  S.SKU=tab.col.value('text()[1]','nvarchar(256)')
Where S.SiteId=@ApplicationId

GO
PRINT 'Modify stored procedure Product_GetProductAndSkuByXmlGuids'
GO
IF(OBJECT_ID('Product_GetProductAndSkuByXmlGuids') IS NOT NULL)
	DROP PROCEDURE Product_GetProductAndSkuByXmlGuids
GO
CREATE  PROCEDURE [dbo].[Product_GetProductAndSkuByXmlGuids]      
(      
 @Id Xml  
,@ApplicationId uniqueidentifier=null

)      
AS      

--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)      

Insert Into @tempXml(ProductId)      
Select Distinct tab.col.value('text()[1]','uniqueidentifier')      
 FROM @Id.nodes('/ProductCollection/Product')tab(col)      

 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
vPT.[IsDownloadableMedia], S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedById CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedById ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 SEOFriendlyUrl,
S.TaxCategoryId,
S.PromoteAsNewItem,
S.PromoteAsTopSeller,
S.ExcludeFromDiscounts,
S.ExcludeFromShippingPromotions,
S.Freight,
S.Refundable,
S.TaxExempt,
S.SEOTitle,
S.SEOH1,
S.SEODescription,
S.SEOKeywords
  FROM @tempXml T      
  INNER JOIN vwProduct S on S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
--Order By T.Sno      
Where S.SiteId=@ApplicationId


Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from @tempXml t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.ProductId  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  


SELECT 
	S.[Id]    
   ,S.[ProductId]    
   ,S.[SiteId]    
   ,S.[SKU]    
   ,S.[Title]    
   ,S.[Description]    
   ,S.[Sequence]    
   ,S.[IsActive]    
   ,S.[IsOnline]    
   ,ISNULL(SC.ListPrice,S.ListPrice)  ListPrice    
   ,S.[UnitCost]    
   ,S.[PreviousSoldCount]    
   ,S.[CreatedBy]    
   ,S.[CreatedDate]    
   ,S.[ModifiedBy]    
   ,S.[ModifiedDate]    
   ,S.[Status]    
   ,S.[Measure]    
   ,S.[OrderMinimum]    
   ,S.[OrderIncrement]  
   ,S.[HandlingCharges] 
   ,S.[Length]
   ,S.[Height]
   ,S.[Width]
   ,S.[Weight]
   ,S.[SelfShippable]    
   ,S.[AvailableForBackOrder]
   ,S.MaximumDiscountPercentage
   ,S.[BackOrderLimit]
   ,S.[IsUnlimitedQuantity]
   ,S.FreeShipping
   ,S.UserDefinedPrice
  FROM VWSKU S   
  Inner Join @tempXml t on S.ProductId=t.ProductId  
  LEFT JOIN PRProductSKUVariantCache SC ON S.Id = SC.Id AND SC.SiteId=@ApplicationId
  Where S.SiteId=@ApplicationId


Select   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On ( PS.Id = SA.SKUId  AND SA.ProductAttributeID = PA.ID)  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  




select  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized,
  AG.CategoryId,
  AG.AttributeGroupId

from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=dbo.GetActiveStatus()     


END

GO

PRINT 'Modify stored procedure Commerce_GetURLForSiteMap'
GO
IF(OBJECT_ID('Commerce_GetURLForSiteMap') IS NOT NULL)
	DROP PROCEDURE Commerce_GetURLForSiteMap
GO
CREATE PROCEDURE [dbo].[Commerce_GetURLForSiteMap]
(
	@SiteId		uniqueidentifier
)
AS
BEGIN
	DECLARE @tbProducts TABLE (Id uniqueidentifier, Url nvarchar(max), ModifiedDate datetime)
	DECLARE @tbProductView TABLE (Id uniqueidentifier, ViewCount int)
	DECLARE @tbProductPriority TABLE(Id uniqueidentifier, Priority int)
	DECLARE @MaxViewCount int

	INSERT INTO @tbProducts
	SELECT P.Id,
		CASE WHEN SEOFriendlyUrl IS NULL 
			THEN LOWER(dbo.GetProductTypePath(T.Id, @SiteId) + N'id-' + P.ProductIDUser + '/' + UrlFriendlyTitle)
			ELSE LOWER(SEOFriendlyUrl)
		END,
		CASE WHEN P.ModifiedDate IS NULL
			THEN P.CreatedDate
			ELSE P.ModifiedDate
		END
	FROM (SELECT DISTINCT ProductId FROM PRProductSKU WHERE IsActive = 1 AND IsOnline = 1) AS S
		INNER JOIN vwProduct AS P ON P.Id = S.ProductId
		INNER JOIN PRProductType AS T ON P.ProductTypeId = T.Id
		Where P.SiteId=@siteId

	INSERT INTO @tbProductView
	SELECT ProductId, COUNT(ProductId)
	FROM TRProductView
	GROUP BY ProductId

	SELECT @MaxViewCount = ISNULL(MAX(ViewCount), 1) FROM @tbProductView

	INSERT INTO @tbProductPriority
	SELECT Id,
		CAST((CASE WHEN ISNULL(ViewCount, 0) = 0
			THEN 0.5
			ELSE ViewCount
		END) / @MaxViewCount AS decimal(2, 1))
	FROM @tbProductView

	SELECT P.Id AS ProductId,
		P.ModifiedDate AS LastModifiedDate,
		CASE CHARINDEX('/', Url) WHEN 1 
			THEN SUBSTRING(Url, 2, LEN(Url) - 1) 
			ELSE Url 
		END AS Url,
		CASE WHEN PP.Priority IS NULL THEN 0.5 
			WHEN PP.Priority < 0.5 THEN 0.5
			ELSE PP.Priority
		END AS Priority,
		CASE WHEN P.ModifiedDate > DATEADD(DD, -1, GETDATE()) THEN 'Daily'  
			WHEN P.ModifiedDate > DATEADD(DD, -7, GETDATE()) THEN 'Weekly' 
			WHEN P.ModifiedDate > DATEADD(MM, -1, GETDATE()) THEN 'Monthly' 
			ELSE 'Weekly'
		END AS ChangeFrequency
	FROM @tbProducts P
		LEFT JOIN @tbProductPriority PP ON P.Id = PP.Id
END


GO
PRINT 'Update IsValidated column of USUserPaymentInfo'
GO
IF(COL_LENGTH('USUserPaymentInfo', 'IsValidated') IS NULL)
	ALTER TABLE USUserPaymentInfo ADD IsValidated BIT NOT NULL Default(0)
GO

PRINT 'Update IsValidated column of PTPaymentCreditCard'
GO
IF(COL_LENGTH('PTPaymentCreditCard', 'IsValidated') IS NULL)
	ALTER TABLE PTPaymentCreditCard ADD IsValidated BIT NOT NULL Default(0)
GO
IF(OBJECT_ID('CreditCardPaymentDto_Get') IS NOT NULL)
	DROP PROCEDURE CreditCardPaymentDto_Get
GO
CREATE PROCEDURE [dbo].[CreditCardPaymentDto_Get]
(		
	@PaymentInfoId		UNIQUEIDENTIFIER
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	IF NOT EXISTS (SELECT 1 FROM PTPaymentInfo WHERE Id = @PaymentInfoId)
		SELECT @PaymentInfoId = PaymentInfoId FROM USUserPaymentInfo WHERE Id = @PaymentInfoId

	SELECT P.Id,
		CCT.Title AS CardType,
		CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth,
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,
		P.NameOnCard,
		P.CreatedBy,
		P.CreatedDate,
		P.Status,
		P.ModifiedBy,
		P.ModifiedDate,
		PCC.ExternalProfileId,
		PCC.ExternalProfileInfo,
		PCC.IsValidated
	FROM PTPaymentInfo P
		LEFT JOIN PTCreditCardType AS CCT ON CCT.Id = P.CardType
		LEFT JOIN PTPaymentCreditCard PCC ON P.Id = PCC.PaymentInfoId
	WHERE P.Id = @PaymentInfoId

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO


IF(OBJECT_ID('CustomerPaymentInfoDto_Get') IS NOT NULL)
	DROP PROCEDURE CustomerPaymentInfoDto_Get
GO

CREATE PROCEDURE [dbo].[CustomerPaymentInfoDto_Get]
(
	@Id					uniqueidentifier = null,
	@CustomerId			uniqueidentifier = null,
	@SiteId				uniqueidentifier,
	@PaymentType        int = null
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	WITH CTE AS
	(
	SELECT UP.Id,  
		UP.UserId AS CustomerId,  
		1 AS PaymentType,
		UP.NickName,
		UP.IsPrimary,
		UP.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		CCT.Title AS CreditCardType,  
		P.NameOnCard AS CreditCardName,
		CONVERT(varchar(max), DecryptByKey(P.Number)) AS CreditCardNumber,  
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,  
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) AS CreditCardExpirationYear,  
		UP.ExternalProfileId AS CreditCardExternalProfileId,
		UP.ExternalProfileInfo,
		UP.IsValidated,
		NULL AS GiftCardNumber,
		NULL AS GiftCardBalance,
		NULL AS AccountNumber,
		UP.CreatedDate, 
		UP.CreatedBy AS CreatedById,
		UP.ModifiedDate, 
		UP.ModifiedBy AS ModifiedById
	FROM PTPaymentInfo P
		LEFT JOIN USUserPaymentInfo UP ON P.Id = UP.PaymentInfoId  
		LEFT JOIN PTCreditCardType AS CCT ON CCT.Id = P.CardType
		LEFT JOIN GLAddress A ON A.Id = UP.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR UP.Id = @Id)
		AND (@CustomerId IS NULL OR UP.UserId = @CustomerId) 
		AND UP.[Status] = 1
		AND (UP.SiteId IS NULL OR UP.SiteId=@SiteId)

	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		5 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		0,
		P.GiftCardNumber,
		P.BalanceAfterLastTransaction AS GiftCardBalance,
		NULL AS AccountNumber,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTGiftCardInfo P  
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 5 AND Status = 1 AND SiteId = @SiteId)
		AND (P.SiteId IS NULL OR P.SiteId=@SiteId)
	
	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		2 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		0,
		NULL,
		NULL,
		U.AccountNumber,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTLineOfCreditInfo P  
		INNER JOIN USCommerceUserProfile U ON U.Id = P.CustomerId
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 2 AND Status = 1 AND SiteId = @SiteId)
		AND (P.SiteId IS NULL OR P.SiteId=@SiteId)
	
	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		4 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		0,
		NULL,
		NULL,
		NULL,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTCODInfo P  
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 4 AND Status = 1 AND SiteId = @SiteId)
		AND (P.SiteId IS NULL OR P.SiteId=@SiteId)
	)

	SELECT Id,  
		CustomerId,  
		PaymentType,
		NickName,
		IsPrimary,
		BillingAddressId,
		BillingAddressLine1,
		BillingAddressLine2,
		BillingAddressLine3,
		BillingCity,
		BillingStateId,
		BillingZip,
		BillingCountryId,
		BillingPhone,
		BillingStateName,
		BillingStateCode,
		BillingCountryName,
		BillingCountryCode,
		PaymentTypeInfoId,
		CreditCardType,  
		CreditCardName,
		CreditCardNumber,  
		CreditCardExpirationMonth,  
		CreditCardExpirationYear,  
		CreditCardExternalProfileId,
		ExternalProfileInfo,
		IsValidated,
		GiftCardNumber,
		GiftCardBalance,
		AccountNumber,
		CreatedDate, 
		CreatedById,
		ModifiedDate, 
		ModifiedById
	FROM CTE
	WHERE PaymentType = ISNULL(@PaymentType, PaymentType)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO


IF(OBJECT_ID('PaymentDto_Get') IS NOT NULL)
	DROP PROCEDURE PaymentDto_Get
GO

CREATE PROCEDURE [dbo].[PaymentDto_Get]
(		
	@Id						UNIQUEIDENTIFIER = NULL,
	@OrderId				UNIQUEIDENTIFIER = NULL,
	@CustomerId				UNIQUEIDENTIFIER = NULL,
	@ProcessorTransactionId	NVARCHAR(256) = NULL,
	@PaymentTypeId			INT = NULL,
	@PaymentStatusId		INT = NULL,
	@IsRefund				BIT = NULL,
	@StartDate				DATETIME = NULL,
	@EndDate				DATETIME = NULL,
	@PageNumber				INT = NULL,
	@PageSize				INT = NULL,
	@SiteId					UNIQUEIDENTIFIER = NULL,
	@TotalRecords			INT = NULL OUTPUT
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	DECLARE	@StartRow	INT,
			@EndRow		INT

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END

	SELECT	P.Id, OP.Id AS OrderPaymentId, OP.OrderId, OP.[Sequence], P.PaymentTypeId AS PaymentType, P.PaymentStatusId AS PaymentStatus, P.ProcessorTransactionId,
			P.IsRefund, RE.Comments AS RefundReason, P.Amount, P.CapturedAmount, COALESCE(PR.AllocatedRefundAmount, 0) AS AllocatedRefundAmount,
			O.PurchaseOrderNumber, P.CustomerId, CASE WHEN LEN(LTRIM(C.LastName)) = 0 THEN ''  ELSE C.LastName + ', ' END + C.FirstName AS CustomerName,
			CCT.Title AS CreditCardType, CCI.NameOnCard AS CreditCardName,
			CONVERT(VARCHAR(MAX), DecryptByKey(CCI.Number)) AS CreditCardNumber,
			CAST(CONVERT(VARCHAR(2), DecryptByKey(CCI.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,
			CAST(CONVERT(VARCHAR(4), DecryptByKey(CCI.ExpirationYear)) AS INT) AS CreditCardExpirationYear,
			PCC.ExternalProfileId CreditCardExternalProfileId,
			PCC.ExternalProfileInfo,
			PCC.IsValidated,
			GCI.GiftCardNumber AS GiftCardNumber, GCI.BalanceAfterLastTransaction AS GiftCardBalance, U.AccountNumber,P.AdditionalInfo,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.BillingAddressId
				WHEN 2 THEN LOC.BillingAddressId
				WHEN 3 THEN PPI.BillingAddressId
				WHEN 4 THEN CODI.BillingAddressId
				WHEN 5 THEN GCI.BillingAddressId
			END AS BillingAddressId,
			CASE P.PaymentTypeId
				WHEN 1 THEN CCI.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentInfoId,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentTypeInfoId,
			P.CreatedDate, P.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName, P.ModifiedDate, P.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName,
			ROW_NUMBER() OVER (ORDER BY P.CreatedDate Desc) AS RowNumber
	INTO	#OrderPayments
	FROM	PTOrderPayment AS OP
			INNER JOIN PTPayment AS P ON P.Id = OP.PaymentId
			INNER JOIN OROrder AS O ON O.Id = OP.OrderId AND (@SiteId IS NULL OR O.SiteId = @SiteId)
			INNER JOIN vwCustomer AS C ON C.Id = P.CustomerId
			LEFT JOIN PTRefundPayment AS RP ON RP.RefundOrderPaymentId = OP.Id
			LEFT JOIN ORRefund AS RE ON RE.Id = RP.RefundId
			LEFT JOIN (
				SELECT		RP.OrderPaymentId, SUM(RP.RefundAmount) AS AllocatedRefundAmount
				FROM		PTRefundPayment AS RP
							INNER JOIN PTOrderPayment AS ROP ON ROP.Id = RP.RefundOrderPaymentId
							INNER JOIN PTPayment AS P ON P.Id = ROP.PaymentId
				WHERE		P.PaymentStatusId NOT IN (7, 14)
				GROUP BY	RP.OrderPaymentId
			) AS PR ON PR.OrderPaymentId = OP.Id
			LEFT JOIN PTPaymentCreditCard AS PCC ON P.PaymentTypeId = 1 AND PCC.PaymentId = P.Id
			LEFT JOIN PTPaymentInfo AS CCI ON P.PaymentTypeId = 1 AND CCI.Id = PCC.PaymentInfoId
			LEFT JOIN PTCreditCardType AS CCT ON P.PaymentTypeId = 1 AND CCT.Id = CCI.CardType
			LEFT JOIN PTPaymentLineOfCredit AS PLOC ON P.PaymentTypeId = 2 AND PLOC.PaymentId = P.Id
			LEFT JOIN PTLineOfCreditInfo AS LOC ON P.PaymentTypeId = 2 AND LOC.Id = PLOC.LOCId
			LEFT JOIN USCommerceUserProfile U ON P.PaymentTypeId = 2 AND U.Id = LOC.CustomerId
			LEFT JOIN PTPaymentPaypal AS PPP ON P.PaymentTypeId = 3 AND PPP.PaymentId = P.Id
			LEFT JOIN PTPaypalInfo AS PPI ON P.PaymentTypeId = 3 AND PPI.Id = PPP.PaypalInfoId
			LEFT JOIN PTPaymentCOD AS PCOD ON P.PaymentTypeId = 4 AND PCOD.PaymentId = P.Id
			LEFT JOIN PTCODInfo AS CODI ON P.PaymentTypeId = 4 AND CODI.Id = PCOD.CODId
			LEFT JOIN PTPaymentGiftCard AS PGC ON P.PaymentTypeId = 5 AND PGC.PaymentId = P.Id
			LEFT JOIN PTGiftCardInfo AS GCI ON P.PaymentTypeId = 5 AND GCI.Id = PGC.GiftCardId
			LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy
			LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = P.ModifiedBy
	WHERE	(@Id IS NULL OR P.Id = @Id) AND
			(@OrderId IS NULL OR OP.OrderId = @OrderId) AND
			(@CustomerId IS NULL OR P.CustomerId = @CustomerId) AND
			(@ProcessorTransactionId IS NULL OR P.ProcessorTransactionId = @ProcessorTransactionId) AND
			(@PaymentTypeId IS NULL OR P.PaymentTypeId = @PaymentTypeId) AND
			(@PaymentStatusId IS NULL OR P.PaymentStatusId = @PaymentStatusId) AND
			(@IsRefund IS NULL OR P.IsRefund = @IsRefund) AND
			(@StartDate IS NULL OR P.CreatedDate >= @StartDate) AND
			(@EndDate IS NULL OR P.CreatedDate <= @EndDate) AND
			(@SiteId IS NULL OR O.SiteId = @SiteId)
	ORDER BY P.CreatedDate DESC
	OPTION (RECOMPILE)

	SELECT	*
	FROM	#OrderPayments
	WHERE	@StartRow IS NULL OR
			@EndRow IS NULL OR 
			RowNumber BETWEEN @StartRow AND @EndRow

	SET @TotalRecords = (
		SELECT	COUNT(Id)
		FROM	#OrderPayments
	)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO

IF(OBJECT_ID('CustomerPaymentInfo_Get') IS NOT NULL)
	DROP PROCEDURE CustomerPaymentInfo_Get
GO
CREATE PROCEDURE [dbo].[CustomerPaymentInfo_Get](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	-- open the symmetric key 
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	Declare @BillingAddressId uniqueidentifier

	SELECT @BillingAddressId = BillingAddressId	
	FROM USUserPaymentInfo 
	WHERE Id=@Id

	SELECT UP.Id,
		P.Id PaymentInfoId,
		P.CardType,
		CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth  ,
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,
		P.NameOnCard,
		UP.UserId,
		UP.BillingAddressId,
		UP.Sequence,
		UP.IsPrimary,
		UP.CreatedBy,
		dbo.ConvertTimeFromUtc(UP.CreatedDate,@ApplicationId)CreatedDate,
		UP.ModifiedBy,
		dbo.ConvertTimeFromUtc(UP.ModifiedDate,@ApplicationId)ModifiedDate,
		UP.[Status],
		UP.ExternalProfileId,
		UP.ExternalProfileInfo,
		UP.IsValidated,
		UP.Nickname
	FROM PTPaymentInfo P
	Inner Join USUserPaymentInfo UP ON P.Id=UP.PaymentInfoId
	WHERE UP.Id=@Id
	AND UP. [Status] != dbo.GetDeleteStatus()

	SELECT 	 
	A.Id 
	,A.FirstName
	,A.LastName
	,A.AddressType
	,A.AddressLine1
	,A.AddressLine2
	,A.AddressLine3
	,A.City
	,A.StateId
	,A.StateName
	,A.Zip
	,A.CountryId
	,A.Phone
	,A.CreatedBy
	,dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate
	,dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate
	,A.ModifiedBy 
--	,UA.Id AddressId
--	,UA.NickName
--	,UA.IsPrimary
--	,UA.Sequence
	,A.County
	FROM GLAddress A
	--Left Outer Join USUserShippingAddress UA ON A.Id=UA.AddressId
	Where A.Id=@BillingAddressId
	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO
IF(OBJECT_ID('CustomerPaymentInfo_Save') IS NOT NULL)
	DROP PROCEDURE CustomerPaymentInfo_Save
GO

CREATE PROCEDURE [dbo].[CustomerPaymentInfo_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@Status				int=null,
	@CardType			uniqueidentifier,
    @Number				varchar(max),
	@ExpirationMonth	int,
    @ExpirationYear		int,
    @NameOnCard			varchar(255),
    @BillingAddressId	uniqueidentifier,
    @Sequence			int,
    @UserId				uniqueidentifier,
    @IsPrimary			bit,
	@ApplicationId		uniqueidentifier,
	@ExternalProfileId	nvarchar(255)=null,
	@ExternalProfileInfo nvarchar(max)=null,
	@IsValidated		bit=0,
	@Nickname			nvarchar(255)=null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@stmt	  varchar(256),
		@rowcount	int,
		@PaymentInfoId uniqueidentifier

Begin
--********************************************************************************
-- code
--********************************************************************************


	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	DECLARE @NewCustomerPayment BIT = CASE WHEN @Id IS NULL OR @Id = dbo.GetEmptyGUID() OR NOT EXISTS (SELECT Id FROM USUserPaymentInfo WHERE Id = @Id) THEN 1 ELSE 0 END

	IF @Id IS NULL OR @Id = dbo.GetEmptyGUID()
		SET @Id = NEWID()
       
    IF @IsPrimary = 1
	BEGIN
		UPDATE [dbo].[USUserPaymentInfo]
		SET IsPrimary = 0 WHERE UserId = @UserId
	END
	
	SET @ModifiedDate = GetUTCDate()
	
		-- open the symmetric key 
		OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
		OPEN SYMMETRIC KEY [key_DataShare] 
			DECRYPTION BY CERTIFICATE cert_keyProtection;

		IF @NewCustomerPayment = 1	-- new insert
		BEGIN
			SET @stmt = 'Payment Insert'
			Declare @pId uniqueidentifier
			SET @pId =NewId();

			INSERT INTO [dbo].[PTPaymentInfo]
			   ([Id]
			   ,[CardType]
			   ,[Number]
			   ,[ExpirationMonth]
			   ,[ExpirationYear]
			   ,[NameOnCard]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[Status]
			   ,[ModifiedBy]
			   ,[ModifiedDate])
		 VALUES
			   (@pId
			   ,@CardType
			   ,EncryptByKey(Key_Guid('key_DataShare'), @Number)
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS Varchar(2)))
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS varchar(4)))
			   ,@NameOnCard
			   ,@ModifiedBy
			   ,@ModifiedDate
			   ,@Status
			   ,@ModifiedBy
			   ,@ModifiedDate)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
			INSERT INTO [dbo].[USUserPaymentInfo]
			([Id]
			,[UserId]
			,[PaymentInfoId]
			,[BillingAddressId]
			,[Sequence]
			,[IsPrimary]
			,[CreatedBy]
			,[CreatedDate]
			,[Status]
			,[ModifiedDate]
			,[ModifiedBy]
			,[ExternalProfileId]
			,[ExternalProfileInfo]
			,[IsValidated]
			,[Nickname]
			,[SiteId])
			VALUES
			(@Id
			,@UserId
			,@pId
			,@BillingAddressId
			,@Sequence
			,@IsPrimary
			,@ModifiedBy
			,@ModifiedDate
			,@Status
			,@ModifiedDate
			,@ModifiedBy
			,@ExternalProfileId
			,@ExternalProfileInfo
			,@IsValidated
			,@Nickname
			,@ApplicationId)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
        END
        ELSE			-- update
        BEGIN
			SET @stmt = 'Payment Update'
			
			Select @PaymentInfoId=PaymentInfoId  FROM [dbo].[USUserPaymentInfo]
			Where Id=@Id

			UPDATE [dbo].[USUserPaymentInfo] WITH (ROWLOCK)
			SET [PaymentInfoId] = @PaymentInfoId
			  ,[BillingAddressId] = @BillingAddressId
			  ,[Sequence] = @Sequence
			  ,[IsPrimary] = @IsPrimary
			  ,[Status] = @Status
			  ,[ModifiedDate] = @ModifiedDate
			  ,[ModifiedBy] =@ModifiedBy
			  ,[ExternalProfileId]=@ExternalProfileId
			  ,[Nickname]=@Nickname
			  ,[SiteId]=@ApplicationId
			  ,[ExternalProfileInfo]=@ExternalProfileInfo
			  ,[IsValidated]=@IsValidated
			WHERE Id=@Id

			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
			  RAISERROR('DBERROR||%s',16,1,@stmt)
			  RETURN dbo.GetDataBaseErrorCode()
			END

			SET @rowcount= 0	

			UPDATE [dbo].[PTPaymentInfo]
			SET [CardType] = @CardType
				,[Number]=CASE WHEN (@Number is null or len(@Number)<=0) then Number else (EncryptByKey(Key_Guid('key_DataShare'), @Number )) end
				,[ExpirationMonth] = CASE WHEN (@ExpirationMonth is null or len(@ExpirationMonth)<=0) then ExpirationMonth else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS VARCHAR(2)) )) end
				,[ExpirationYear]= CASE WHEN (@ExpirationYear is null or len(@ExpirationMonth)<=0) then ExpirationYear else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS VARCHAR(4)) )) end
				,[NameOnCard] = @NameOnCard
				,[Status] = @Status
				,[ModifiedBy] = @ModifiedBy
				,[ModifiedDate] = @ModifiedDate
			WHERE Id=@PaymentInfoId 
					
			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
				RAISERROR('DBERROR||%s',16,1,@stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END


	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO

IF(OBJECT_ID('OrderPayment_Get') IS NOT NULL)
	DROP PROCEDURE OrderPayment_Get
GO
CREATE PROCEDURE [dbo].[OrderPayment_Get]
	(@OrderPaymentId	uniqueidentifier 
	,@ApplicationId uniqueidentifier=null)
AS
BEGIN
OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

DECLARE @PaymentId uniqueidentifier ;
SELECT 
	Id,
	OrderId,
	PaymentId,
	Amount,
	Sequence
FROM
	PTOrderPayment
WHERE
	Id = @OrderPaymentId

SELECT 
	P.Id,
	P.PaymentTypeId,
	P.PaymentStatusId,
	P.CustomerId,
	P.Amount,
	P.CapturedAmount,
	P.Description,
	dbo.ConvertTimeFromUtc(P.FailureDate,@ApplicationId)FailureDate,
	P.FailureDescription,
	dbo.ConvertTimeFromUtc(P.CreatedDate,@ApplicationId)CreatedDate,
	P.CreatedBy,
	dbo.ConvertTimeFromUtc(P.ClearDate,@ApplicationId)ClearDate,
	P.ProcessorTransactionId,
	P.AuthCode,
	P.Status,
	dbo.ConvertTimeFromUtc(P.ModifiedDate,@ApplicationId)ModifiedDate,
	P.ModifiedBy ,
	P.IsRefund,
	P.AdditionalInfo
FROM
	PTPayment P
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId


SELECT 
	PCC.Id,
	PCC.PaymentInfoId,
	PCC.ParentPaymentInfoId,
	PCC.PaymentId,
	PCC.BillingAddressId,
	PCC.Phone,
	PCC.ExternalProfileId,
	PCC.ExternalProfileInfo,
	PCC.IsValidated
FROM 
	PTPaymentCreditCard PCC
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId

SELECT 
	PINFO.Id,
	PINFO.CardType,
	CONVERT(varchar(max), DecryptByKey(PINFO.Number))  [Number],
	CAST(CONVERT(varchar(2), DecryptByKey(PINFO.ExpirationMonth)) AS INT) ExpirationMonth  ,
	CAST(CONVERT(varchar(4), DecryptByKey(PINFO.ExpirationYear)) AS INT) ExpirationYear,
	PINFO.NameOnCard,
	PINFO.CreatedBy,
	dbo.ConvertTimeFromUtc(PINFO.CreatedDate,@ApplicationId)CreatedDate,
	PINFO.Status,
	PINFO.ModifiedBy,
	dbo.ConvertTimeFromUtc(PINFO.ModifiedDate,@ApplicationId)ModifiedDate
FROM
	PTPaymentInfo PINFO
	INNER JOIN PTPaymentCreditCard PCC ON PCC.PaymentInfoId = PINFO.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId

SELECT 
	A.Id,
	A.FirstName,
	A.LastName,
	A.AddressType,
	A.AddressLine1,
	A.AddressLine2,
	A.AddressLine3,
	A.City,
	A.StateId,
	A.StateName,
	A.Zip,
	A.CountryId,
	A.Phone,
	dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
	A.CreatedBy,
	dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
	A.ModifiedBy,
	A.Status,
	A.County
FROM 
	GLAddress A	
	INNER JOIN PTPaymentCreditCard PCC ON PCC.BillingAddressId  = A.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId

	
SELECT  PC.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        dbo.ConvertTimeFromUtc(PC.CreatedDate, @ApplicationId) CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId ,
        SUM(ISNULL(PC2.CapturedAmount,0)) AS TotalRefundAmount
FROM    PTPaymentCapture PC
        INNER JOIN PTOrderPayment OP ON OP.PaymentId = PC.PaymentId
        LEFT JOIN dbo.PTPaymentCapture PC2 ON PC.Id = PC2.ParentPaymentCaptureId
WHERE   OP.Id = @OrderPaymentId
GROUP BY Pc.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        PC.CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId
ORDER BY PC.CreatedDate ASC

END

GO

IF(OBJECT_ID('OrderPayment_GetByOrderId') IS NOT NULL)
	DROP PROCEDURE OrderPayment_GetByOrderId
GO
CREATE PROCEDURE [dbo].[OrderPayment_GetByOrderId]
	(@OrderId	uniqueidentifier 
	,@ApplicationId uniqueidentifier=null)
AS
BEGIN

IF EXISTS (SELECT OrderId FROM PTOrderPayment WHERE OrderId = @OrderId)
BEGIN 

OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

DECLARE @PaymentId uniqueidentifier ;


SELECT 
	OP.Id,
	OP.OrderId,
	OP.PaymentId,
	OP.Amount,
	OP.Sequence
FROM
	PTOrderPayment OP
INNER JOIN PTPayment P ON OP.PaymentId = P.Id
WHERE
	OrderId = @OrderId Order By P.CreatedDate

SELECT 
	P.Id,
	P.PaymentTypeId,
	P.PaymentStatusId,
	P.CustomerId,
	P.Amount,
	P.CapturedAmount,
	P.Description,
	dbo.ConvertTimeFromUtc(P.FailureDate,@ApplicationId)FailureDate,
	P.FailureDescription,
	dbo.ConvertTimeFromUtc(P.CreatedDate,@ApplicationId)CreatedDate,
	P.CreatedBy,
	dbo.ConvertTimeFromUtc(P.ClearDate,@ApplicationId)ClearDate,
	P.ProcessorTransactionId,
	P.AuthCode,
	P.Status,
	dbo.ConvertTimeFromUtc(P.ModifiedDate,@ApplicationId)ModifiedDate,
	P.ModifiedBy,
	P.IsRefund,
	P.AdditionalInfo
FROM
	PTPayment P
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId 


SELECT 
	PCC.Id,
	PCC.PaymentInfoId,
	PCC.ParentPaymentInfoId,
	PCC.PaymentId,
	PCC.BillingAddressId,
	PCC.Phone,
	PCC.ExternalProfileId,
	PCC.ExternalProfileInfo,
	PCC.IsValidated
FROM 
	PTPaymentCreditCard PCC
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId



SELECT 
	PINFO.Id,
	PINFO.CardType,
	CONVERT(varchar(max), DecryptByKey(PINFO.Number))  [Number],
	CAST(CONVERT(varchar(2), DecryptByKey(PINFO.ExpirationMonth)) AS INT) ExpirationMonth  ,
	CAST(CONVERT(varchar(4), DecryptByKey(PINFO.ExpirationYear)) AS INT) ExpirationYear,
	PINFO.NameOnCard,
	PINFO.CreatedBy,
	dbo.ConvertTimeFromUtc(PINFO.CreatedDate,@ApplicationId)CreatedDate,
	PINFO.Status,
	PINFO.ModifiedBy,
	dbo.ConvertTimeFromUtc(PINFO.ModifiedDate,@ApplicationId)ModifiedDate
FROM
	PTPaymentInfo PINFO
	INNER JOIN PTPaymentCreditCard PCC ON PCC.PaymentInfoId = PINFO.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId
	



SELECT 
	A.Id,
	A.FirstName,
	A.LastName,
	A.AddressType,
	A.AddressLine1,
	A.AddressLine2,
	A.AddressLine3,
	A.City,
	A.StateId,
	A.Zip,
	A.CountryId,
	A.Phone,
	dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
	A.CreatedBy,
	dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
	A.ModifiedBy,
	A.Status,
	A.County
FROM 
	GLAddress A	
	INNER JOIN PTPaymentCreditCard PCC ON PCC.BillingAddressId  = A.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId


SELECT  PC.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        dbo.ConvertTimeFromUtc(PC.CreatedDate, @ApplicationId) CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId ,
        SUM(ISNULL(PC2.CapturedAmount,0)) AS TotalRefundAmount
FROM    PTPaymentCapture PC
        INNER JOIN PTOrderPayment OP ON OP.PaymentId = PC.PaymentId
        LEFT JOIN dbo.PTPaymentCapture PC2 ON PC.Id = PC2.ParentPaymentCaptureId
WHERE   OP.OrderId = @OrderId
GROUP BY Pc.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        PC.CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId
ORDER BY PC.CreatedDate ASC

END

END
GO
IF(OBJECT_ID('PaymentCreditCard_GetByPaymentId') IS NOT NULL)
	DROP PROCEDURE PaymentCreditCard_GetByPaymentId
GO

CREATE PROCEDURE [dbo].[PaymentCreditCard_GetByPaymentId](@PaymentId uniqueidentifier)
AS
BEGIN
	SELECT	
		Id,
		PaymentInfoId,
		ParentPaymentInfoId,
		PaymentId,
		BillingAddressId,
		Phone,
		ExternalProfileId,
		ExternalProfileInfo,
		IsValidated
	FROM PTPaymentCreditCard
	WHERE PaymentId=@PaymentId

END

GO
IF(OBJECT_ID('PaymentInfo_GetByPaymentCreditCardId') IS NOT NULL)
	DROP PROCEDURE PaymentInfo_GetByPaymentCreditCardId
GO


CREATE PROCEDURE [dbo].[PaymentInfo_GetByPaymentCreditCardId](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	-- open the symmetric key 
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	SELECT 
		P.Id,
		P.CardType,
		CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth  ,
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,
		P.NameOnCard,
		CreatedBy,
		dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,
		Status,
		ModifiedBy,
		dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,
		ExternalProfileId,
		ExternalProfileInfo,
		IsValidated
	FROM PTPaymentInfo P
	Inner Join PTPaymentCreditCard PCC ON P.Id=PCC.PaymentInfoId
	WHERE PCC.Id=@Id

	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO
IF(OBJECT_ID('Payment_GetExternalProfile') IS NOT NULL)
	DROP PROCEDURE Payment_GetExternalProfile
GO

CREATE PROCEDURE Payment_GetExternalProfile
(
	@SavedCardId uniqueidentifier	
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT ExternalProfileId,ExternalProfileInfo,IsValidated FROM USUserPaymentInfo Where Id = @SavedCardId
END
GO
IF(OBJECT_ID('Payment_Save') IS NOT NULL)
	DROP PROCEDURE Payment_Save
GO

CREATE PROCEDURE [dbo].[Payment_Save]
    (
      @Id UNIQUEIDENTIFIER OUTPUT ,
      @PaymentInfoId UNIQUEIDENTIFIER OUTPUT ,
      @OrderId UNIQUEIDENTIFIER ,
      @PaymentTypeId INT ,
      @PaymentStatusId INT = 1 ,
      @CardTypeId UNIQUEIDENTIFIER ,
      @CardNumber VARCHAR(MAX) ,
      @ExpirationMonth INT ,
      @ExpirationYear INT ,
      @NameOnCard NVARCHAR(256) ,
      @LastName NVARCHAR(256) = NULL ,
      @AuthCode NVARCHAR(256) ,
      @Description NVARCHAR(MAX) ,
      @AddressLine1 NVARCHAR(256) ,
      @AddressLine2 NVARCHAR(256) ,
	  @AddressLine3 NVARCHAR(256) ,
      @City NVARCHAR(256) ,
      @StateId UNIQUEIDENTIFIER ,
      @StateName NVARCHAR(255) ,
      @CountryId UNIQUEIDENTIFIER ,
      @Zip NVARCHAR(50) ,
      @Phone NVARCHAR(50) ,
      @Amount MONEY ,
      @CustomerId UNIQUEIDENTIFIER ,
      @UpdateCustomerPayment BIT ,
      @SavedCardId UNIQUEIDENTIFIER ,
      @ModifiedBy UNIQUEIDENTIFIER ,
      @OrderPaymentId UNIQUEIDENTIFIER OUTPUT ,
      @IsRefund TINYINT = 0 ,
      @ExternalProfileId NVARCHAR(255),
	  @ExternalProfileInfo NVARCHAR(max)=null,
	  @IsValidated	bit=0,
	  @AdditionalInfo NVARCHAR(MAX) =null
    )
AS 
    BEGIN    -- 1   
  
        DECLARE @CreatedDate DATETIME ,
            @ModifiedDate DATETIME
--Declare @PaymentInfoId uniqueidentifier
        DECLARE @AddressId UNIQUEIDENTIFIER
        DECLARE @error INT
        DECLARE @stmt VARCHAR(256)	
        DECLARE @sequence INT
        IF ( @Id IS NULL
             OR @Id = dbo.GetEmptyGUID()
           ) 
            BEGIN --2
                SET @Id = NEWID()  
                SET @PaymentInfoId = NEWID()
                SET @CreatedDate = GETUTCDATE()
                SET @AddressId = NEWID()


                INSERT  INTO PTPayment
                        ( Id ,
                          PaymentTypeId ,
                          PaymentStatusId ,
                          CustomerId ,
                          Amount ,
                          Description ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          AuthCode ,
                          Status ,
                          IsRefund,
						  AdditionalInfo
	                  )
                VALUES  ( @Id ,
                          @PaymentTypeId ,
                          @PaymentStatusId ,
                          @CustomerId ,
                          @Amount ,
                          @Description ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @AuthCode ,
                          dbo.GetActiveStatus() ,
                          @IsRefund,
						  @AdditionalInfo
                        )

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN 
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END  

-- open the symmetric key 
                OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
                OPEN SYMMETRIC KEY [key_DataShare] 
			DECRYPTION BY CERTIFICATE cert_keyProtection ;

                INSERT  INTO PTPaymentInfo
                        ( Id ,
                          CardType ,
                          Number ,
                          ExpirationMonth ,
                          ExpirationYear ,
                          NameOnCard ,
                          CreatedBy ,
                          CreatedDate ,
                          ModifiedBy ,
                          ModifiedDate ,
                          Status
	                  )
                VALUES  ( @PaymentInfoId ,
                          @CardTypeId ,
                          ENCRYPTBYKEY(KEY_GUID('key_DataShare'), @CardNumber) ,
                          ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                       CAST(@ExpirationMonth AS VARCHAR(2))) ,
                          ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                       CAST(@ExpirationYear AS VARCHAR(4))) ,
                          @NameOnCard ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          dbo.GetActiveStatus()
                        )

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
		-- close the symmetric key
                        CLOSE SYMMETRIC KEY [key_DataShare] ;
                        CLOSE MASTER KEY
		
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

                INSERT  INTO GLAddress
                        ( Id ,
                          FirstName ,
                          LastName ,
                          AddressType ,
                          AddressLine1 ,
                          AddressLine2 ,
						  AddressLine3,
                          City ,
                          StateId ,
                          StateName ,
                          CountryId ,
                          Zip ,
                          Phone ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          Status
	                  )
                VALUES  ( @AddressId ,
                          @NameOnCard ,
                          @LastName ,
                          0 ,
                          @AddressLine1 ,
                          @AddressLine2 ,
						  @AddressLine3,
                          @City ,
                          @StateId ,
                          @StateName ,
                          @CountryId ,
                          @Zip ,
                          @Phone ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          dbo.GetActiveStatus()
                        )
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END
	
	
                INSERT  INTO PTPaymentCreditCard
                        ( Id ,
                          PaymentInfoId ,
                          ParentPaymentInfoId ,
                          PaymentId ,
                          BillingAddressId ,
                          Phone ,
                          ExternalProfileId,
						  ExternalProfileInfo,
						  IsValidated
	                  )
                VALUES  ( NEWID() ,
                          @PaymentInfoId ,
                          @SavedCardId ,
                          @Id ,
                          @AddressId ,
                          @Phone ,
                          @ExternalProfileId,
						  @ExternalProfileInfo,
						  @IsValidated
	                  )
	
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

                SELECT  @sequence = MAX(ISNULL(Sequence, 0)) + 1
                FROM    PTOrderPayment
                WHERE   OrderId = @OrderId


                SET @OrderPaymentId = NEWID()
                INSERT  INTO PTOrderPayment
                        ( Id ,
                          OrderId ,
                          PaymentId ,
                          Amount ,
                          Sequence
                        )
                VALUES  ( @OrderPaymentId ,
                          @OrderId ,
                          @Id ,
                          @Amount ,
                          @sequence
                        )

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

            END  --2
        ELSE 
            BEGIN --3
                SET @CreatedDate = GETUTCDATE()
                SET @AddressId = NEWID()
                UPDATE  PTPayment
                SET     PaymentTypeId = @PaymentTypeId ,
                        PaymentStatusId = @PaymentStatusId ,
                        CustomerId = @CustomerId ,
                        Amount = @Amount ,
                        Description = @Description ,
                        ModifiedBy = @ModifiedBy ,
                        ModifiedDate = @CreatedDate ,
                        AuthCode = @AuthCode ,
                        Status = dbo.GetActiveStatus() ,
                        IsRefund = @IsRefund,
						AdditionalInfo =isnull(AdditionalInfo,@AdditionalInfo)
                WHERE   Id = @Id


                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

	-- open the symmetric key 
                OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
                OPEN SYMMETRIC KEY [key_DataShare] 
				DECRYPTION BY CERTIFICATE cert_keyProtection ;

                UPDATE  PTPaymentInfo
                SET     CardType = @CardTypeId ,
                        Number = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                              @CardNumber) ,
                        ExpirationMonth = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                       CAST(@ExpirationMonth AS VARCHAR(2))) ,
                        ExpirationYear = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                      CAST(@ExpirationYear AS VARCHAR(4))) ,
                        NameOnCard = @NameOnCard ,
                        ModifiedBy = @ModifiedBy ,
                        ModifiedDate = GETUTCDATE() ,
                        Status = dbo.GetActiveStatus()
                WHERE   Id = @PaymentInfoId
				
                
		

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
			-- close the symmetric key
                        CLOSE SYMMETRIC KEY [key_DataShare] ;
                        CLOSE MASTER KEY
			
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

				UPDATE  dbo.PTPaymentCreditCard
                SET     ExternalProfileId = @ExternalProfileId,
						ExternalProfileInfo=@ExternalProfileInfo,
						IsValidated=@IsValidated
                WHERE   PaymentInfoId = @PaymentInfoId

                SELECT  @AddressId = BillingAddressId
                FROM    PTPaymentCreditCard
                WHERE   PaymentId = @Id
		
		
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

                UPDATE  GLAddress
                SET     FirstName = @NameOnCard ,
                        LastName = @LastName ,
                        AddressType = 0 ,
                        AddressLine1 = @AddressLine1 ,
                        AddressLine2 = @AddressLine2 ,
						AddressLine3 = @AddressLine3 ,
                        City = @City ,
                        StateId = @StateId ,
                        StateName = @StateName ,
                        CountryId = @CountryId ,
                        Zip = @Zip ,
                        Phone = @Phone ,
                        ModifiedDate = GETUTCDATE() ,
                        ModifiedBy = @ModifiedBy ,
                        Status = dbo.GetActiveStatus()
                WHERE   Id = @AddressId
		
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END	


                UPDATE  PTOrderPayment
                SET     Amount = @Amount
                WHERE   Id = @OrderPaymentId
		
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
		
                    END
            END  --3

        IF @UpdateCustomerPayment = 1 
            BEGIN --4
                DECLARE @NewAddressId UNIQUEIDENTIFIER
	
                SET @NewAddressId = NEWID()
                INSERT  INTO GLAddress
                        ( Id ,
                          FirstName ,
                          LastName ,
                          AddressType ,
                          AddressLine1 ,
                          AddressLine2 ,
						  AddressLine3,
                          City ,
                          StateId ,
                          StateName ,
                          CountryId ,
                          Zip ,
                          Phone ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          Status
	                  )
                        SELECT  @NewAddressId ,
                                FirstName ,
                                LastName ,
                                AddressType ,
                                AddressLine1 ,
                                AddressLine2 ,
								AddressLine3,
                                City ,
                                StateId ,
                                StateName ,
                                CountryId ,
                                Zip ,
                                Phone ,
                                CreatedDate ,
                                CreatedBy ,
                                ModifiedDate ,
                                ModifiedBy ,
                                Status
                        FROM    GLAddress
                        WHERE   Id = @AddressId

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END
                DECLARE @ExistingPaymentInfo UNIQUEIDENTIFIER
                SELECT  @ExistingPaymentInfo = PaymentInfoId
                FROM    USUserPaymentInfo
                WHERE   Id = @SavedCardId
                DECLARE @NewPaymentInfoId UNIQUEIDENTIFIER
                SET @NewPaymentInfoId = NEWID()
                IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000' 
                    BEGIN
                        UPDATE  PTPaymentInfo
                        SET     CardType = @CardTypeId ,
                                Number = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                      @CardNumber) ,
                                ExpirationMonth = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                              CAST(@ExpirationMonth AS VARCHAR(2))) ,
                                ExpirationYear = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                              CAST(@ExpirationYear AS VARCHAR(4))) ,
                                NameOnCard = @NameOnCard ,
                                ModifiedBy = @ModifiedBy ,
                                ModifiedDate = @CreatedDate
                        WHERE   Id = @ExistingPaymentInfo
                    END
                ELSE 
                    BEGIN
                        INSERT  INTO PTPaymentInfo
                                ( Id ,
                                  CardType ,
                                  Number ,
                                  ExpirationMonth ,
                                  ExpirationYear ,
                                  NameOnCard ,
                                  CreatedBy ,
                                  CreatedDate ,
                                  Status ,
                                  ModifiedBy ,
                                  ModifiedDate
		                    )
                        VALUES  ( @NewPaymentInfoId ,
                                  @CardTypeId ,
                                  ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                               @CardNumber) ,
                                  ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                               CAST(@ExpirationMonth AS VARCHAR(2))) ,
                                  ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                               CAST(@ExpirationYear AS VARCHAR(4))) ,
                                  @NameOnCard ,
                                  @ModifiedBy ,
                                  @CreatedDate ,
                                  1 ,
                                  @ModifiedBy ,
                                  @CreatedDate
			              )
                    END
	
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END
                IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000' 
                    BEGIN
                        UPDATE  USUserPaymentInfo
                        SET     BillingAddressId = @NewAddressId ,
                                ExternalProfileId = @ExternalProfileId,
								ExternalProfileInfo=@ExistingPaymentInfo,
								IsValidated=@IsValidated
                        WHERE   PaymentInfoId = @ExistingPaymentInfo
                                AND UserId = @CustomerId
		
                    END 
                ELSE 
                    BEGIN 
                        INSERT  INTO USUserPaymentInfo
                                ( Id ,
                                  UserId ,
                                  PaymentInfoId ,
                                  BillingAddressId ,
                                  Sequence ,
                                  IsPrimary ,
                                  CreatedBy ,
                                  CreatedDate ,
                                  Status ,
                                  ModifiedDate ,
                                  ModifiedBy ,
                                  ExternalProfileId,
								  ExternalProfileInfo,
								  IsValidated
		                    )
                        VALUES  ( NEWID() ,
                                  @CustomerId ,
                                  @NewPaymentInfoId ,
                                  @AddressId ,
                                  1 ,
                                  0 ,
                                  @ModifiedBy ,
                                  @CreatedDate ,
                                  1 ,
                                  @CreatedDate ,
                                  @ModifiedBy ,
                                  @ExternalProfileId,
								  @ExternalProfileInfo,
								  @IsValidated
		                    )
                    END
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

            END --4


        UPDATE  OROrder
        SET     PaymentTotal = dbo.Order_GetPaymentTotal(@OrderId) ,
                RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
        WHERE   Id = @OrderId

  
  
    END

GO


IF NOT EXISTS (SELECT * FROM STSettingType WHERE [Name] = 'cybs.EnableOneDollarAuthValidation')
BEGIN
	DECLARE @Sequence INT = (SELECT ISNULL(MAX(Sequence), 0) + 1 FROM STSettingType WHERE SettingGroupId = (select top 1 Id FROM STSettingGroup Where Name ='Payment Settings'))

	INSERT INTO STSettingType ([Name], FriendlyName, SettingGroupId, [Sequence], ControlType, IsEnabled, IsVisible)
		VALUES ('cybs.EnableOneDollarAuthValidation', 'When enabled an auth for one dollar will be done to validate the credit card.', 7, @Sequence, 'Checkbox', 1, 1)

	INSERT INTO STSiteSetting (SiteId, SettingTypeId, [Value])
	SELECT '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', T.Id, 'False'
	FROM	STSettingType AS T
			LEFT JOIN STSiteSetting AS S ON S.SettingTypeId = T.Id
	WHERE	T.[Name] = 'cybs.EnableOneDollarAuthValidation' AND
			S.Id IS NULL
END
GO

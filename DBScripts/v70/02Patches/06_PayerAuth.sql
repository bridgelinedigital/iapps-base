
IF COL_LENGTH('dbo.PTPayment', 'AdditionalInfo') IS NULL
BEGIN
    ALTER TABLE PTPayment ADD AdditionalInfo nvarchar(max) NULL
END;


GO


IF COL_LENGTH('dbo.PTPaymentCreditCard', 'ExternalProfileInfo') IS NULL
BEGIN
    ALTER TABLE PTPaymentCreditCard ADD ExternalProfileInfo nvarchar(max) NULL
END;

GO
IF COL_LENGTH('dbo.USUserPaymentInfo', 'ExternalProfileInfo') IS NULL
BEGIN
    ALTER TABLE USUserPaymentInfo ADD ExternalProfileInfo nvarchar(max) NULL
END;
GO

IF COL_LENGTH('dbo.USUserPaymentInfo', 'SiteId') IS NULL
BEGIN
    ALTER TABLE USUserPaymentInfo ADD SiteId uniqueidentifier NULL
END;

GO

IF COL_LENGTH('dbo.PTCODInfo', 'SiteId') IS NULL
BEGIN
    ALTER TABLE PTCODInfo ADD SiteId uniqueidentifier NULL
END;

GO

IF COL_LENGTH('dbo.PTLineOfCreditInfo', 'SiteId') IS NULL
BEGIN
    ALTER TABLE PTLineOfCreditInfo ADD SiteId uniqueidentifier NULL
END;

GO

IF COL_LENGTH('dbo.PTGiftCardInfo', 'SiteId') IS NULL
BEGIN
    ALTER TABLE PTGiftCardInfo ADD SiteId uniqueidentifier NULL
END;

GO
IF NOT EXISTS (Select * from PTGateway Where Name ='PayerAuthCybersourceGateway')
BEGIN
	declare @Id Int
	SET @Id = (Select max(Id)+1 from PTGateway)
	INSERT INTO PTGateway(Id,Name,Description,Sequence,DotNetProviderName)
	Values(@Id,'PayerAuthCybersourceGateway','Payer Authentication using Cybersource',@Id,'PayerAuthCybersourceGateway')


	INSERT INTO PTGatewaySite(GatewayId,SiteId, IsActive,PaymentTypeId)
	select Distinct @Id,SiteId,0,1 from PTGAtewaysite


END
GO


PRINT 'Add Site Settings'
DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int
SET @SettingGroupId = NULL
SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Payment Settings'

IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'PayerAuth.ApiIdentifier')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('PayerAuth.ApiIdentifier', 'PayerAuth.ApiIdentifier',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '5d1bc240afa80d2714dadd42' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END

IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'PayerAuth.ApiKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('PayerAuth.ApiKey', 'PayerAuth.ApiKey',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '53de8b6b-2b90-447c-8af2-494881fd81e2' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END

IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'PayerAuth.OrgUnitId')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('PayerAuth.OrgUnitId', 'PayerAuth.OrgUnitId',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '5d1a2e35bb8768224008e629' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END


GO
GO
PRINT 'Creating Payment_Get'
GO
IF(OBJECT_ID('Payment_Get') IS NOT NULL)
	DROP PROCEDURE  Payment_Get
GO	

CREATE PROCEDURE [dbo].[Payment_Get]    
(    
 @Id uniqueidentifier=null,
 @SortColumn VARCHAR(25)=null,    
 @PagingSize INT=null,     
 @PagingIndex INT=1,    
 @CustomerId uniqueidentifier=null,    
 @PaymentTypeId int=null,
 @PaymentStatusId int=null,
 @ProcessorTxnId nvarchar(255) =null,
 @PaymentDateFrom DateTime =null,
 @PaymentDateTo DateTime =null,
 @ClearedDateFrom DateTime =null,
 @ClearedDateTo DateTime =null,
 @RefundOnly bit = null,
 @ApplicationId uniqueidentifier=null
)    
AS    
BEGIN    
    
IF @Id is not null 
BEGIN
 SELECT P.[Id] ,
        P.[PaymentTypeId] ,
        P.[PaymentStatusId] ,
        P.[CustomerId] ,
        U.[AccountNumber] ,
        P.[Amount] ,
        P.[CapturedAmount] ,
        P.[Description] ,
        dbo.ConvertTimeFromUtc(P.[FailureDate], @ApplicationId) FailureDate ,
        P.[FailureDescription] ,
        dbo.ConvertTimeFromUtc(P.[CreatedDate], @ApplicationId) CreatedDate ,
        P.[CreatedBy] ,
        dbo.ConvertTimeFromUtc(P.[ClearDate], @ApplicationId) ClearDate ,
        P.[ProcessorTransactionId] ,
        P.[AuthCode] ,
        P.[Status] ,
        dbo.ConvertTimeFromUtc(P.ModifiedDate, @ApplicationId) ModifiedDate ,
        P.[ModifiedBy] ,
        O.[PurchaseOrderNumber] ,
        O.[Id] OrderId ,
        OP.[Amount] OrderPaymentAmount ,
        OP.[Id] OrderPaymentId,
		P.IsRefund,
		P.AdditionalInfo
 FROM   [dbo].[PTPayment] P
        INNER JOIN PTOrderPayment OP ON P.Id = OP.PaymentId
        INNER JOIN OROrder O ON OP.OrderId = O.Id
        INNER JOIN USCommerceUserProfile U ON P.CustomerId = U.Id
 WHERE  P.Id = @Id

 SELECT PC.Id ,
        PC.PaymentID ,
        PC.CapturedAmount ,
        dbo.ConvertTimeFromUtc(PC.CreatedDate , @ApplicationId) as CreatedDate,
        PC.TransactionId ,
        PC.ParentPaymentCaptureID,
		SUM(ISNULL(PC2.CapturedAmount,0)) as TotalRefundAmount
 FROM   dbo.PTPaymentCapture PC
		LEFT JOIN dbo.PTPaymentCapture PC2 ON PC.Id = PC2.ParentPaymentCaptureId
 WHERE  PC.PaymentId = @Id
 GROUP BY Pc.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        PC.CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId


END
ELSE
BEGIN


SET @PaymentDateFrom = dbo.ConvertTimeToUtc(isnull(@PaymentDateFrom,'01-01-1970'),@ApplicationId)
SET @PaymentDateTo = dbo.ConvertTimeToUtc(isnull(@PaymentDateTo,'01-01-3000'),@ApplicationId)

IF @ClearedDateFrom IS NOT NULL
	SET @ClearedDateFrom = dbo.ConvertTimeToUtc(@ClearedDateFrom,@ApplicationId)
IF @ClearedDateTo IS NOT NULL
	SET @ClearedDateTo = dbo.ConvertTimeToUtc(@ClearedDateTo,@ApplicationId)


 IF @PagingSize IS NULL    
  SET @PagingSize = 2147483647    
    
 IF @SortColumn IS NULL    
  SET @SortColumn = 'cleardate Asc'    
    
 Set @SortColumn = (@SortColumn)    
    
 ;WITH PagingCTE (Row_ID    
 ,[Id]
  ,[PaymentTypeId]
  ,[PaymentStatusId]
  ,[CustomerId]
  ,[AccountNumber]
  ,[Amount]
  ,[CapturedAmount]
  ,[Description]
  , FailureDate
  ,[FailureDescription]
  ,CreatedDate
  ,[CreatedBy]
  ,ClearDate
  ,[ProcessorTransactionId]
  ,[AuthCode]
  ,[Status]
  ,ModifiedDate
  ,[ModifiedBy]
  ,[PurchaseOrderNumber]
  ,OrderId
  ,OrderPaymentAmount
  ,OrderPaymentId
  ,IsRefund
  ,AdditionalInfo)    
 AS    
 (    
 SELECT     
    ROW_NUMBER()     
 OVER(ORDER BY     
 Case When @SortColumn='paymenttypeid desc' Then PaymentTypeId END DESC,    
 Case When @SortColumn='paymenttypeid asc' or @SortColumn='paymenttypeid' Then PaymentTypeId END Asc,    
    
 Case When @SortColumn='paymentstatusid desc' Then cast(PaymentStatusId as varchar(25)) END DESC,    
 Case When @SortColumn='paymentstatusid asc' or @SortColumn='paymentstatusid' Then cast(PaymentStatusId as varchar(25)) END Asc,    
 
 Case When @SortColumn='amount desc' Then P.Amount  END DESC,    
 Case When @SortColumn='amount asc' or @SortColumn='amount' Then P.Amount END Asc,    
 
 Case When @SortColumn='description desc' Then cast(P.Description as varchar(25)) END DESC,    
 Case When @SortColumn='description asc' or @SortColumn='description' Then cast(P.Description as varchar(25)) END Asc,    
 
 Case When @SortColumn='failuredate desc' Then cast(FailureDate as varchar(25)) END DESC,    
 Case When @SortColumn='failuredate asc' or @SortColumn='failuredate' Then cast(FailureDate as varchar(25)) END Asc,    
 
 Case When @SortColumn='failuredescription desc' Then cast(FailureDescription as varchar(25)) END DESC,    
 Case When @SortColumn='failuredescription asc' or @SortColumn='failuredescription' Then cast(FailureDescription as varchar(25)) END Asc,    
 
 Case When @SortColumn='createddate desc' Then cast(P.CreatedDate as varchar(25)) END DESC,    
 Case When @SortColumn='createddate asc' or @SortColumn='createddate' Then cast(P.CreatedDate as varchar(25)) END Asc,       

 Case When @SortColumn='createdby desc' Then cast(P.CreatedBy as varchar(25)) END DESC,    
 Case When @SortColumn='createdby asc' or @SortColumn='createdby' Then cast(P.CreatedBy as varchar(25)) END Asc,       


 Case When @SortColumn='modifieddate desc' Then cast(P.ModifiedDate as varchar(25)) END DESC,    
 Case When @SortColumn='modifieddate asc' or @SortColumn='modifieddate' Then cast(P.ModifiedDate as varchar(25)) END Asc,       

 Case When @SortColumn='modifiedby desc' Then cast(P.ModifiedBy as varchar(25)) END DESC,    
 Case When @SortColumn='modifiedby asc' or @SortColumn='modifiedby' Then cast(P.ModifiedBy as varchar(25)) END Asc,       
 
 Case When @SortColumn='cleardate desc' Then cast(P.ClearDate as varchar(25)) END DESC,    
 Case When @SortColumn='cleardate asc' or @SortColumn='cleardate' Then cast(P.ClearDate as varchar(25)) END Asc,       

 Case When @SortColumn='processortransactionid desc' Then cast(ProcessorTransactionId as varchar(25)) END DESC,    
 Case When @SortColumn='processortransactionid asc' or @SortColumn='processortransactionid' Then cast(ProcessorTransactionId as varchar(25)) END Asc,       

 Case When @SortColumn='authcode desc' Then cast(P.AuthCode as varchar(25)) END DESC,    
 Case When @SortColumn='authcode asc' or @SortColumn='authcode' Then cast(P.AuthCode as varchar(25)) END Asc,       

 Case When @SortColumn='purchaseordernumber desc' Then cast(O.[PurchaseOrderNumber] as varchar(100)) END DESC,    
 Case When @SortColumn='purchaseordernumber asc' or @SortColumn='purchaseordernumber' Then cast(O.[PurchaseOrderNumber] as varchar(100)) END Asc,    

 Case When @SortColumn='status desc' Then cast(P.Status as varchar(25)) END DESC,    
 Case When @SortColumn='status asc' or @SortColumn='status' Then cast(P.Status as varchar(25))       
     
 Else P.CreatedDate End Asc    
          ) AS [Row_ID]    
,P.[Id]
      ,P.[PaymentTypeId]
      ,P.[PaymentStatusId]
      ,P.[CustomerId]
	  ,U.[AccountNumber]
      ,P.[Amount]
	  ,P.[CapturedAmount]
      ,P.[Description]
      ,dbo.ConvertTimeFromUtc(P.[FailureDate],@ApplicationId)FailureDate
      ,P.[FailureDescription]
      ,dbo.ConvertTimeFromUtc(P.[CreatedDate],@ApplicationId)CreatedDate
      ,P.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(P.[ClearDate],@ApplicationId)ClearDate
      ,P.[ProcessorTransactionId]
      ,P.[AuthCode]
      ,P.[Status]
      ,dbo.ConvertTimeFromUtc(P.[ModifiedDate],@ApplicationId)ModifiedDate
      ,P.[ModifiedBy]
	  ,O.[PurchaseOrderNumber]
	  ,O.[Id] OrderId
	  ,OP.[Amount] OrderPaymentAmount
	  ,OP.[Id] OrderPaymentId
	  ,P.IsRefund
	  ,P.AdditionalInfo
  FROM [dbo].[PTPayment] P 
Inner Join PTOrderPayment OP On P.Id =OP.PaymentId
Inner Join OROrder O on OP.OrderId =O.Id    
Inner Join USCommerceUserProfile U on P.CustomerId = U.Id
 Where     
  (@CustomerId IS NULL OR P.CustomerId = @CustomerId)
   AND (@PaymentDateFrom is null or P.CreatedDate >= @PaymentDateFrom)
   AND (@PaymentDateTo is null or P.CreatedDate <= @PaymentDateTo)
	AND ((@ClearedDateFrom is null or ClearDate >= @ClearedDateFrom)
			OR (@ClearedDateFrom is null AND ClearDate is null))   
	AND ((@ClearedDateTo is null or ClearDate <= @ClearedDateTo) 
			OR (@ClearedDateTo is null AND ClearDate is null))     
	AND (@PaymentTypeId IS NULL OR P.PaymentTypeId = @PaymentTypeId)
   AND (@PaymentStatusId IS NULL OR P.PaymentStatusId = @PaymentStatusId)
   AND (@ProcessorTxnId  is null OR ProcessorTransactionId like isnull('%'+(@ProcessorTxnId)+'%',ProcessorTransactionId))
   AND (@RefundOnly is null OR @RefundOnly = 0 OR P.Amount < 0)
   AND (@ApplicationId IS NULL OR o.SiteId in (select SiteId from dbo.GetChildrenSites(@ApplicationId,1)))
)     

SELECT     
Row_ID    
,[Id]
      ,[PaymentTypeId]
      ,[PaymentStatusId]
      ,[CustomerId]
	  ,[AccountNumber]
      ,[Amount]
	  ,[CapturedAmount]
      ,[Description]
      ,[FailureDate]
      ,[FailureDescription]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[ClearDate]
      ,[ProcessorTransactionId]
      ,[AuthCode]
      ,[Status]
      ,[ModifiedDate]
      ,[ModifiedBy]
	  ,[PurchaseOrderNumber]
	  ,[OrderId]
	  ,[OrderPaymentAmount]		  
	  ,[OrderPaymentId]
	  ,IsRefund
	  ,AdditionalInfo
FROM PagingCTE pcte    
WHERE Row_ID >= (@PagingSize * @PagingIndex) - (@PagingSize -1) AND Row_ID <= @PagingSize * @PagingIndex    
    
SELECT Count(*)[[Count]
FROM [dbo].[PTPayment] P
Inner Join PTOrderPayment OP On P.Id =OP.PaymentId
Inner Join OROrder O on OP.OrderId =O.Id    
Inner Join USCommerceUserProfile U on P.CustomerId = U.Id
 Where
	(@CustomerId IS NULL OR P.CustomerId = @CustomerId)
	AND (@PaymentDateFrom is null or P.CreatedDate >= @PaymentDateFrom)    
	AND (@PaymentDateTo is null or P.CreatedDate <= @PaymentDateTo)    
	AND ((@ClearedDateFrom is null or ClearDate >= @ClearedDateFrom)
			OR (@ClearedDateFrom is null AND ClearDate is null))   
	AND ((@ClearedDateTo is null or ClearDate <= @ClearedDateTo) 
			OR (@ClearedDateTo is null AND ClearDate is null))  
	AND (@PaymentTypeId is null OR PaymentTypeId = @PaymentTypeId)
	AND (@PaymentStatusId is null OR PaymentStatusId = @PaymentStatusId)
	AND (@ProcessorTxnId  is null OR ProcessorTransactionId like '%'+ @ProcessorTxnId +'%')
	AND (@RefundOnly is null OR @RefundOnly = 0 OR P.Amount < 0)
	AND (@ApplicationId IS NULL OR O.SiteId in (select SiteId from dbo.GetChildrenSites(@ApplicationId,1)))

 END 

END

GO

PRINT 'Creating OrderPayment_Get'
GO
IF(OBJECT_ID('OrderPayment_Get') IS NOT NULL)
	DROP PROCEDURE  OrderPayment_Get
GO	
CREATE PROCEDURE [dbo].[OrderPayment_Get]
	(@OrderPaymentId	uniqueidentifier 
	,@ApplicationId uniqueidentifier=null)
AS
BEGIN
OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

DECLARE @PaymentId uniqueidentifier ;
SELECT 
	Id,
	OrderId,
	PaymentId,
	Amount,
	Sequence
FROM
	PTOrderPayment
WHERE
	Id = @OrderPaymentId

SELECT 
	P.Id,
	P.PaymentTypeId,
	P.PaymentStatusId,
	P.CustomerId,
	P.Amount,
	P.CapturedAmount,
	P.Description,
	dbo.ConvertTimeFromUtc(P.FailureDate,@ApplicationId)FailureDate,
	P.FailureDescription,
	dbo.ConvertTimeFromUtc(P.CreatedDate,@ApplicationId)CreatedDate,
	P.CreatedBy,
	dbo.ConvertTimeFromUtc(P.ClearDate,@ApplicationId)ClearDate,
	P.ProcessorTransactionId,
	P.AuthCode,
	P.Status,
	dbo.ConvertTimeFromUtc(P.ModifiedDate,@ApplicationId)ModifiedDate,
	P.ModifiedBy ,
	P.IsRefund,
	P.AdditionalInfo
FROM
	PTPayment P
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId


SELECT 
	PCC.Id,
	PCC.PaymentInfoId,
	PCC.ParentPaymentInfoId,
	PCC.PaymentId,
	PCC.BillingAddressId,
	PCC.Phone,
	PCC.ExternalProfileId,
	PCC.ExternalProfileInfo
FROM 
	PTPaymentCreditCard PCC
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId

SELECT 
	PINFO.Id,
	PINFO.CardType,
	CONVERT(varchar(max), DecryptByKey(PINFO.Number))  [Number],
	CAST(CONVERT(varchar(2), DecryptByKey(PINFO.ExpirationMonth)) AS INT) ExpirationMonth  ,
	CAST(CONVERT(varchar(4), DecryptByKey(PINFO.ExpirationYear)) AS INT) ExpirationYear,
	PINFO.NameOnCard,
	PINFO.CreatedBy,
	dbo.ConvertTimeFromUtc(PINFO.CreatedDate,@ApplicationId)CreatedDate,
	PINFO.Status,
	PINFO.ModifiedBy,
	dbo.ConvertTimeFromUtc(PINFO.ModifiedDate,@ApplicationId)ModifiedDate
FROM
	PTPaymentInfo PINFO
	INNER JOIN PTPaymentCreditCard PCC ON PCC.PaymentInfoId = PINFO.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId

SELECT 
	A.Id,
	A.FirstName,
	A.LastName,
	A.AddressType,
	A.AddressLine1,
	A.AddressLine2,
	A.AddressLine3,
	A.City,
	A.StateId,
	A.StateName,
	A.Zip,
	A.CountryId,
	A.Phone,
	dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
	A.CreatedBy,
	dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
	A.ModifiedBy,
	A.Status,
	A.County
FROM 
	GLAddress A	
	INNER JOIN PTPaymentCreditCard PCC ON PCC.BillingAddressId  = A.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId

	
SELECT  PC.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        dbo.ConvertTimeFromUtc(PC.CreatedDate, @ApplicationId) CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId ,
        SUM(ISNULL(PC2.CapturedAmount,0)) AS TotalRefundAmount
FROM    PTPaymentCapture PC
        INNER JOIN PTOrderPayment OP ON OP.PaymentId = PC.PaymentId
        LEFT JOIN dbo.PTPaymentCapture PC2 ON PC.Id = PC2.ParentPaymentCaptureId
WHERE   OP.Id = @OrderPaymentId
GROUP BY Pc.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        PC.CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId
ORDER BY PC.CreatedDate ASC

END
GO

PRINT 'Creating OrderPayment_GetByOrderId'
GO
IF(OBJECT_ID('OrderPayment_GetByOrderId') IS NOT NULL)
	DROP PROCEDURE  OrderPayment_GetByOrderId
GO	
CREATE PROCEDURE [dbo].[OrderPayment_GetByOrderId]
	(@OrderId	uniqueidentifier 
	,@ApplicationId uniqueidentifier=null)
AS
BEGIN

IF EXISTS (SELECT OrderId FROM PTOrderPayment WHERE OrderId = @OrderId)
BEGIN 

OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

DECLARE @PaymentId uniqueidentifier ;


SELECT 
	OP.Id,
	OP.OrderId,
	OP.PaymentId,
	OP.Amount,
	OP.Sequence
FROM
	PTOrderPayment OP
INNER JOIN PTPayment P ON OP.PaymentId = P.Id
WHERE
	OrderId = @OrderId Order By P.CreatedDate

SELECT 
	P.Id,
	P.PaymentTypeId,
	P.PaymentStatusId,
	P.CustomerId,
	P.Amount,
	P.CapturedAmount,
	P.Description,
	dbo.ConvertTimeFromUtc(P.FailureDate,@ApplicationId)FailureDate,
	P.FailureDescription,
	dbo.ConvertTimeFromUtc(P.CreatedDate,@ApplicationId)CreatedDate,
	P.CreatedBy,
	dbo.ConvertTimeFromUtc(P.ClearDate,@ApplicationId)ClearDate,
	P.ProcessorTransactionId,
	P.AuthCode,
	P.Status,
	dbo.ConvertTimeFromUtc(P.ModifiedDate,@ApplicationId)ModifiedDate,
	P.ModifiedBy,
	P.IsRefund,
	P.AdditionalInfo
FROM
	PTPayment P
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId 


SELECT 
	PCC.Id,
	PCC.PaymentInfoId,
	PCC.ParentPaymentInfoId,
	PCC.PaymentId,
	PCC.BillingAddressId,
	PCC.Phone,
	PCC.ExternalProfileId,
	PCC.ExternalProfileInfo
FROM 
	PTPaymentCreditCard PCC
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId



SELECT 
	PINFO.Id,
	PINFO.CardType,
	CONVERT(varchar(max), DecryptByKey(PINFO.Number))  [Number],
	CAST(CONVERT(varchar(2), DecryptByKey(PINFO.ExpirationMonth)) AS INT) ExpirationMonth  ,
	CAST(CONVERT(varchar(4), DecryptByKey(PINFO.ExpirationYear)) AS INT) ExpirationYear,
	PINFO.NameOnCard,
	PINFO.CreatedBy,
	dbo.ConvertTimeFromUtc(PINFO.CreatedDate,@ApplicationId)CreatedDate,
	PINFO.Status,
	PINFO.ModifiedBy,
	dbo.ConvertTimeFromUtc(PINFO.ModifiedDate,@ApplicationId)ModifiedDate
FROM
	PTPaymentInfo PINFO
	INNER JOIN PTPaymentCreditCard PCC ON PCC.PaymentInfoId = PINFO.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId
	



SELECT 
	A.Id,
	A.FirstName,
	A.LastName,
	A.AddressType,
	A.AddressLine1,
	A.AddressLine2,
	A.AddressLine3,
	A.City,
	A.StateId,
	A.Zip,
	A.CountryId,
	A.Phone,
	dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
	A.CreatedBy,
	dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
	A.ModifiedBy,
	A.Status,
	A.County
FROM 
	GLAddress A	
	INNER JOIN PTPaymentCreditCard PCC ON PCC.BillingAddressId  = A.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId


SELECT  PC.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        dbo.ConvertTimeFromUtc(PC.CreatedDate, @ApplicationId) CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId ,
        SUM(ISNULL(PC2.CapturedAmount,0)) AS TotalRefundAmount
FROM    PTPaymentCapture PC
        INNER JOIN PTOrderPayment OP ON OP.PaymentId = PC.PaymentId
        LEFT JOIN dbo.PTPaymentCapture PC2 ON PC.Id = PC2.ParentPaymentCaptureId
WHERE   OP.OrderId = @OrderId
GROUP BY Pc.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        PC.CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId
ORDER BY PC.CreatedDate ASC

END

END
GO

PRINT 'Creating Payment_UpdateStatus'
GO
IF(OBJECT_ID('Payment_UpdateStatus') IS NOT NULL)
	DROP PROCEDURE  Payment_UpdateStatus
GO	

CREATE PROCEDURE [dbo].[Payment_UpdateStatus]
(
	@PaymentId				uniqueidentifier,
	@ProcessorTransactionId nvarchar(256) = null,
	@AuthCode				nvarchar(256) = null,
	@PaymentStatusId		int = null,
	@FailureDate			datetime,
	@FailureDescription		nvarchar(max) = null,
	@ClearDate				datetime = null,
	@Amount					money = null,
	@ModifiedBy				uniqueidentifier,
	@ApplicationId			uniqueidentifier,
	@IsRefund				tinyint = 0,
	@AdditionalInfo			nvarchar(max) = null
)
AS
BEGIN
	DECLARE @ModifiedDate datetime, @PaymentTypeId int, @OrderId uniqueidentifier, @PrevPaymentStatusId int
	SET @ModifiedDate = GetUTCDate()

	SELECT TOP 1 @PrevPaymentStatusId = PaymentStatusId, @PaymentTypeId = PaymentTypeId FROM PTPayment WHERE Id = @PaymentId
	IF (@PaymentStatusId = 1 AND @PrevPaymentStatusId != 1)
	BEGIN 
		DECLARE @error int, @stmt varchar(256), @msg varchar(500)
		SET @msg ='PaymentStatus ' + cast(@PrevPaymentStatusId as varchar(2)) + ' cannot be changed to Entered'
		RAISERROR ('InvalidOperation||PaymentStatusId|%s' , 16, 1, @msg)

		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE PTPayment 
	SET ProcessorTransactionId = ISNULL(@ProcessorTransactionId, ProcessorTransactionId),
		AuthCode = ISNULL(@AuthCode, AuthCode),
		PaymentStatusId = ISNULL(@PaymentStatusId, PaymentStatusId),
		FailureDate = dbo.ConvertTimeToUtc(@FailureDate, @ApplicationId),
		FailureDescription = ISNULL(@FailureDescription, FailureDescription),
		ClearDate = ISNULL(dbo.ConvertTimeToUtc(@ClearDate, @ApplicationId), ClearDate),
		Amount = ISNULL(@Amount, Amount),
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @ModifiedDate,
		IsRefund = @IsRefund,
		AdditionalInfo = ISNULL(@AdditionalInfo, AdditionalInfo)
	WHERE Id = @PaymentId	

	SELECT TOP 1 @OrderId = OrderId FROM PTOrderPayment WHERE PaymentId = @PaymentId

	UPDATE OROrder SET PaymentTotal = dbo.Order_GetPaymentTotal(@OrderId), 
		RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
	WHERE Id = @OrderId

	IF (@PaymentStatusId = 7 AND @PaymentTypeId = 5)
	BEGIN
		DECLARE @GiftCardInfoId uniqueidentifier
		SELECT @GiftCardInfoId = GiftCardId FROM PTPaymentGiftCard WHERE PaymentId = @PaymentId

		UPDATE PTGiftCardInfo SET BalanceAfterLastTransaction = BalanceAfterLastTransaction + @Amount 
		WHERE Id = @GiftCardInfoId
	END
END
GO

PRINT 'Creating PaymentDto_Get'
GO
IF(OBJECT_ID('PaymentDto_Get') IS NOT NULL)
	DROP PROCEDURE  PaymentDto_Get
GO	

CREATE PROCEDURE [dbo].[PaymentDto_Get]
(		
	@Id						UNIQUEIDENTIFIER = NULL,
	@OrderId				UNIQUEIDENTIFIER = NULL,
	@CustomerId				UNIQUEIDENTIFIER = NULL,
	@ProcessorTransactionId	NVARCHAR(256) = NULL,
	@PaymentTypeId			INT = NULL,
	@PaymentStatusId		INT = NULL,
	@IsRefund				BIT = NULL,
	@StartDate				DATETIME = NULL,
	@EndDate				DATETIME = NULL,
	@PageNumber				INT = NULL,
	@PageSize				INT = NULL,
	@SiteId					UNIQUEIDENTIFIER = NULL,
	@TotalRecords			INT = NULL OUTPUT
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	DECLARE	@StartRow	INT,
			@EndRow		INT

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END

	SELECT	P.Id, OP.Id AS OrderPaymentId, OP.OrderId, OP.[Sequence], P.PaymentTypeId AS PaymentType, P.PaymentStatusId AS PaymentStatus, P.ProcessorTransactionId,
			P.IsRefund, RE.Comments AS RefundReason, P.Amount, P.CapturedAmount, COALESCE(PR.AllocatedRefundAmount, 0) AS AllocatedRefundAmount,
			O.PurchaseOrderNumber, P.CustomerId, CASE WHEN LEN(LTRIM(C.LastName)) = 0 THEN ''  ELSE C.LastName + ', ' END + C.FirstName AS CustomerName,
			CCT.Title AS CreditCardType, CCI.NameOnCard AS CreditCardName,
			CONVERT(VARCHAR(MAX), DecryptByKey(CCI.Number)) AS CreditCardNumber,
			CAST(CONVERT(VARCHAR(2), DecryptByKey(CCI.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,
			CAST(CONVERT(VARCHAR(4), DecryptByKey(CCI.ExpirationYear)) AS INT) AS CreditCardExpirationYear,
			PCC.ExternalProfileId CreditCardExternalProfileId,
			PCC.ExternalProfileInfo,
			GCI.GiftCardNumber AS GiftCardNumber, GCI.BalanceAfterLastTransaction AS GiftCardBalance, U.AccountNumber,P.AdditionalInfo,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.BillingAddressId
				WHEN 2 THEN LOC.BillingAddressId
				WHEN 3 THEN PPI.BillingAddressId
				WHEN 4 THEN CODI.BillingAddressId
				WHEN 5 THEN GCI.BillingAddressId
			END AS BillingAddressId,
			CASE P.PaymentTypeId
				WHEN 1 THEN CCI.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentInfoId,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentTypeInfoId,
			P.CreatedDate, P.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName, P.ModifiedDate, P.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName,
			ROW_NUMBER() OVER (ORDER BY P.CreatedDate Desc) AS RowNumber
	INTO	#OrderPayments
	FROM	PTOrderPayment AS OP
			INNER JOIN PTPayment AS P ON P.Id = OP.PaymentId
			INNER JOIN OROrder AS O ON O.Id = OP.OrderId AND (@SiteId IS NULL OR O.SiteId = @SiteId)
			INNER JOIN vwCustomer AS C ON C.Id = P.CustomerId
			LEFT JOIN PTRefundPayment AS RP ON RP.RefundOrderPaymentId = OP.Id
			LEFT JOIN ORRefund AS RE ON RE.Id = RP.RefundId
			LEFT JOIN (
				SELECT		RP.OrderPaymentId, SUM(RP.RefundAmount) AS AllocatedRefundAmount
				FROM		PTRefundPayment AS RP
							INNER JOIN PTOrderPayment AS ROP ON ROP.Id = RP.RefundOrderPaymentId
							INNER JOIN PTPayment AS P ON P.Id = ROP.PaymentId
				WHERE		P.PaymentStatusId NOT IN (7, 14)
				GROUP BY	RP.OrderPaymentId
			) AS PR ON PR.OrderPaymentId = OP.Id
			LEFT JOIN PTPaymentCreditCard AS PCC ON P.PaymentTypeId = 1 AND PCC.PaymentId = P.Id
			LEFT JOIN PTPaymentInfo AS CCI ON P.PaymentTypeId = 1 AND CCI.Id = PCC.PaymentInfoId
			LEFT JOIN PTCreditCardType AS CCT ON P.PaymentTypeId = 1 AND CCT.Id = CCI.CardType
			LEFT JOIN PTPaymentLineOfCredit AS PLOC ON P.PaymentTypeId = 2 AND PLOC.PaymentId = P.Id
			LEFT JOIN PTLineOfCreditInfo AS LOC ON P.PaymentTypeId = 2 AND LOC.Id = PLOC.LOCId
			LEFT JOIN USCommerceUserProfile U ON P.PaymentTypeId = 2 AND U.Id = LOC.CustomerId
			LEFT JOIN PTPaymentPaypal AS PPP ON P.PaymentTypeId = 3 AND PPP.PaymentId = P.Id
			LEFT JOIN PTPaypalInfo AS PPI ON P.PaymentTypeId = 3 AND PPI.Id = PPP.PaypalInfoId
			LEFT JOIN PTPaymentCOD AS PCOD ON P.PaymentTypeId = 4 AND PCOD.PaymentId = P.Id
			LEFT JOIN PTCODInfo AS CODI ON P.PaymentTypeId = 4 AND CODI.Id = PCOD.CODId
			LEFT JOIN PTPaymentGiftCard AS PGC ON P.PaymentTypeId = 5 AND PGC.PaymentId = P.Id
			LEFT JOIN PTGiftCardInfo AS GCI ON P.PaymentTypeId = 5 AND GCI.Id = PGC.GiftCardId
			LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy
			LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = P.ModifiedBy
	WHERE	(@Id IS NULL OR P.Id = @Id) AND
			(@OrderId IS NULL OR OP.OrderId = @OrderId) AND
			(@CustomerId IS NULL OR P.CustomerId = @CustomerId) AND
			(@ProcessorTransactionId IS NULL OR P.ProcessorTransactionId = @ProcessorTransactionId) AND
			(@PaymentTypeId IS NULL OR P.PaymentTypeId = @PaymentTypeId) AND
			(@PaymentStatusId IS NULL OR P.PaymentStatusId = @PaymentStatusId) AND
			(@IsRefund IS NULL OR P.IsRefund = @IsRefund) AND
			(@StartDate IS NULL OR P.CreatedDate >= @StartDate) AND
			(@EndDate IS NULL OR P.CreatedDate <= @EndDate) AND
			(@SiteId IS NULL OR O.SiteId = @SiteId)
	ORDER BY P.CreatedDate DESC
	OPTION (RECOMPILE)

	SELECT	*
	FROM	#OrderPayments
	WHERE	@StartRow IS NULL OR
			@EndRow IS NULL OR 
			RowNumber BETWEEN @StartRow AND @EndRow

	SET @TotalRecords = (
		SELECT	COUNT(Id)
		FROM	#OrderPayments
	)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO
PRINT 'Creating Payment_Save'
GO
IF(OBJECT_ID('Payment_Save') IS NOT NULL)
	DROP PROCEDURE Payment_Save
GO	

CREATE PROCEDURE [dbo].[Payment_Save]
    (
      @Id UNIQUEIDENTIFIER OUTPUT ,
      @PaymentInfoId UNIQUEIDENTIFIER OUTPUT ,
      @OrderId UNIQUEIDENTIFIER ,
      @PaymentTypeId INT ,
      @PaymentStatusId INT = 1 ,
      @CardTypeId UNIQUEIDENTIFIER ,
      @CardNumber VARCHAR(MAX) ,
      @ExpirationMonth INT ,
      @ExpirationYear INT ,
      @NameOnCard NVARCHAR(256) ,
      @LastName NVARCHAR(256) = NULL ,
      @AuthCode NVARCHAR(256) ,
      @Description NVARCHAR(MAX) ,
      @AddressLine1 NVARCHAR(256) ,
      @AddressLine2 NVARCHAR(256) ,
	  @AddressLine3 NVARCHAR(256) ,
      @City NVARCHAR(256) ,
      @StateId UNIQUEIDENTIFIER ,
      @StateName NVARCHAR(255) ,
      @CountryId UNIQUEIDENTIFIER ,
      @Zip NVARCHAR(50) ,
      @Phone NVARCHAR(50) ,
      @Amount MONEY ,
      @CustomerId UNIQUEIDENTIFIER ,
      @UpdateCustomerPayment BIT ,
      @SavedCardId UNIQUEIDENTIFIER ,
      @ModifiedBy UNIQUEIDENTIFIER ,
      @OrderPaymentId UNIQUEIDENTIFIER OUTPUT ,
      @IsRefund TINYINT = 0 ,
      @ExternalProfileId NVARCHAR(255),
	  @ExternalProfileInfo NVARCHAR(max)=null,
	  @AdditionalInfo NVARCHAR(MAX) =null
    )
AS 
    BEGIN    -- 1   
  
        DECLARE @CreatedDate DATETIME ,
            @ModifiedDate DATETIME
--Declare @PaymentInfoId uniqueidentifier
        DECLARE @AddressId UNIQUEIDENTIFIER
        DECLARE @error INT
        DECLARE @stmt VARCHAR(256)	
        DECLARE @sequence INT
        IF ( @Id IS NULL
             OR @Id = dbo.GetEmptyGUID()
           ) 
            BEGIN --2
                SET @Id = NEWID()  
                SET @PaymentInfoId = NEWID()
                SET @CreatedDate = GETUTCDATE()
                SET @AddressId = NEWID()


                INSERT  INTO PTPayment
                        ( Id ,
                          PaymentTypeId ,
                          PaymentStatusId ,
                          CustomerId ,
                          Amount ,
                          Description ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          AuthCode ,
                          Status ,
                          IsRefund,
						  AdditionalInfo
	                  )
                VALUES  ( @Id ,
                          @PaymentTypeId ,
                          @PaymentStatusId ,
                          @CustomerId ,
                          @Amount ,
                          @Description ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @AuthCode ,
                          dbo.GetActiveStatus() ,
                          @IsRefund,
						  @AdditionalInfo
                        )

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN 
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END  

-- open the symmetric key 
                OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
                OPEN SYMMETRIC KEY [key_DataShare] 
			DECRYPTION BY CERTIFICATE cert_keyProtection ;

                INSERT  INTO PTPaymentInfo
                        ( Id ,
                          CardType ,
                          Number ,
                          ExpirationMonth ,
                          ExpirationYear ,
                          NameOnCard ,
                          CreatedBy ,
                          CreatedDate ,
                          ModifiedBy ,
                          ModifiedDate ,
                          Status
	                  )
                VALUES  ( @PaymentInfoId ,
                          @CardTypeId ,
                          ENCRYPTBYKEY(KEY_GUID('key_DataShare'), @CardNumber) ,
                          ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                       CAST(@ExpirationMonth AS VARCHAR(2))) ,
                          ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                       CAST(@ExpirationYear AS VARCHAR(4))) ,
                          @NameOnCard ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          dbo.GetActiveStatus()
                        )

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
		-- close the symmetric key
                        CLOSE SYMMETRIC KEY [key_DataShare] ;
                        CLOSE MASTER KEY
		
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

                INSERT  INTO GLAddress
                        ( Id ,
                          FirstName ,
                          LastName ,
                          AddressType ,
                          AddressLine1 ,
                          AddressLine2 ,
						  AddressLine3,
                          City ,
                          StateId ,
                          StateName ,
                          CountryId ,
                          Zip ,
                          Phone ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          Status
	                  )
                VALUES  ( @AddressId ,
                          @NameOnCard ,
                          @LastName ,
                          0 ,
                          @AddressLine1 ,
                          @AddressLine2 ,
						  @AddressLine3,
                          @City ,
                          @StateId ,
                          @StateName ,
                          @CountryId ,
                          @Zip ,
                          @Phone ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          dbo.GetActiveStatus()
                        )
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END
	
	
                INSERT  INTO PTPaymentCreditCard
                        ( Id ,
                          PaymentInfoId ,
                          ParentPaymentInfoId ,
                          PaymentId ,
                          BillingAddressId ,
                          Phone ,
                          ExternalProfileId,
						  ExternalProfileInfo
	                  )
                VALUES  ( NEWID() ,
                          @PaymentInfoId ,
                          @SavedCardId ,
                          @Id ,
                          @AddressId ,
                          @Phone ,
                          @ExternalProfileId,
						  @ExternalProfileInfo
	                  )
	
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

                SELECT  @sequence = MAX(ISNULL(Sequence, 0)) + 1
                FROM    PTOrderPayment
                WHERE   OrderId = @OrderId


                SET @OrderPaymentId = NEWID()
                INSERT  INTO PTOrderPayment
                        ( Id ,
                          OrderId ,
                          PaymentId ,
                          Amount ,
                          Sequence
                        )
                VALUES  ( @OrderPaymentId ,
                          @OrderId ,
                          @Id ,
                          @Amount ,
                          @sequence
                        )

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

            END  --2
        ELSE 
            BEGIN --3
                SET @CreatedDate = GETUTCDATE()
                SET @AddressId = NEWID()
                UPDATE  PTPayment
                SET     PaymentTypeId = @PaymentTypeId ,
                        PaymentStatusId = @PaymentStatusId ,
                        CustomerId = @CustomerId ,
                        Amount = @Amount ,
                        Description = @Description ,
                        ModifiedBy = @ModifiedBy ,
                        ModifiedDate = @CreatedDate ,
                        AuthCode = @AuthCode ,
                        Status = dbo.GetActiveStatus() ,
                        IsRefund = @IsRefund,
						AdditionalInfo =isnull(AdditionalInfo,@AdditionalInfo)
                WHERE   Id = @Id


                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

	-- open the symmetric key 
                OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
                OPEN SYMMETRIC KEY [key_DataShare] 
				DECRYPTION BY CERTIFICATE cert_keyProtection ;

                UPDATE  PTPaymentInfo
                SET     CardType = @CardTypeId ,
                        Number = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                              @CardNumber) ,
                        ExpirationMonth = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                       CAST(@ExpirationMonth AS VARCHAR(2))) ,
                        ExpirationYear = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                      CAST(@ExpirationYear AS VARCHAR(4))) ,
                        NameOnCard = @NameOnCard ,
                        ModifiedBy = @ModifiedBy ,
                        ModifiedDate = GETUTCDATE() ,
                        Status = dbo.GetActiveStatus()
                WHERE   Id = @PaymentInfoId
				
                
		

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
			-- close the symmetric key
                        CLOSE SYMMETRIC KEY [key_DataShare] ;
                        CLOSE MASTER KEY
			
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

				UPDATE  dbo.PTPaymentCreditCard
                SET     ExternalProfileId = @ExternalProfileId,
						ExternalProfileInfo=@ExternalProfileInfo
                WHERE   PaymentInfoId = @PaymentInfoId

                SELECT  @AddressId = BillingAddressId
                FROM    PTPaymentCreditCard
                WHERE   PaymentId = @Id
		
		
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

                UPDATE  GLAddress
                SET     FirstName = @NameOnCard ,
                        LastName = @LastName ,
                        AddressType = 0 ,
                        AddressLine1 = @AddressLine1 ,
                        AddressLine2 = @AddressLine2 ,
						AddressLine3 = @AddressLine3 ,
                        City = @City ,
                        StateId = @StateId ,
                        StateName = @StateName ,
                        CountryId = @CountryId ,
                        Zip = @Zip ,
                        Phone = @Phone ,
                        ModifiedDate = GETUTCDATE() ,
                        ModifiedBy = @ModifiedBy ,
                        Status = dbo.GetActiveStatus()
                WHERE   Id = @AddressId
		
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END	


                UPDATE  PTOrderPayment
                SET     Amount = @Amount
                WHERE   Id = @OrderPaymentId
		
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
		
                    END
            END  --3

        IF @UpdateCustomerPayment = 1 
            BEGIN --4
                DECLARE @NewAddressId UNIQUEIDENTIFIER
	
                SET @NewAddressId = NEWID()
                INSERT  INTO GLAddress
                        ( Id ,
                          FirstName ,
                          LastName ,
                          AddressType ,
                          AddressLine1 ,
                          AddressLine2 ,
						  AddressLine3,
                          City ,
                          StateId ,
                          StateName ,
                          CountryId ,
                          Zip ,
                          Phone ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          Status
	                  )
                        SELECT  @NewAddressId ,
                                FirstName ,
                                LastName ,
                                AddressType ,
                                AddressLine1 ,
                                AddressLine2 ,
								AddressLine3,
                                City ,
                                StateId ,
                                StateName ,
                                CountryId ,
                                Zip ,
                                Phone ,
                                CreatedDate ,
                                CreatedBy ,
                                ModifiedDate ,
                                ModifiedBy ,
                                Status
                        FROM    GLAddress
                        WHERE   Id = @AddressId

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END
                DECLARE @ExistingPaymentInfo UNIQUEIDENTIFIER
                SELECT  @ExistingPaymentInfo = PaymentInfoId
                FROM    USUserPaymentInfo
                WHERE   Id = @SavedCardId
                DECLARE @NewPaymentInfoId UNIQUEIDENTIFIER
                SET @NewPaymentInfoId = NEWID()
                IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000' 
                    BEGIN
                        UPDATE  PTPaymentInfo
                        SET     CardType = @CardTypeId ,
                                Number = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                      @CardNumber) ,
                                ExpirationMonth = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                              CAST(@ExpirationMonth AS VARCHAR(2))) ,
                                ExpirationYear = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                              CAST(@ExpirationYear AS VARCHAR(4))) ,
                                NameOnCard = @NameOnCard ,
                                ModifiedBy = @ModifiedBy ,
                                ModifiedDate = @CreatedDate
                        WHERE   Id = @ExistingPaymentInfo
                    END
                ELSE 
                    BEGIN
                        INSERT  INTO PTPaymentInfo
                                ( Id ,
                                  CardType ,
                                  Number ,
                                  ExpirationMonth ,
                                  ExpirationYear ,
                                  NameOnCard ,
                                  CreatedBy ,
                                  CreatedDate ,
                                  Status ,
                                  ModifiedBy ,
                                  ModifiedDate
		                    )
                        VALUES  ( @NewPaymentInfoId ,
                                  @CardTypeId ,
                                  ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                               @CardNumber) ,
                                  ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                               CAST(@ExpirationMonth AS VARCHAR(2))) ,
                                  ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                               CAST(@ExpirationYear AS VARCHAR(4))) ,
                                  @NameOnCard ,
                                  @ModifiedBy ,
                                  @CreatedDate ,
                                  1 ,
                                  @ModifiedBy ,
                                  @CreatedDate
			              )
                    END
	
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END
                IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000' 
                    BEGIN
                        UPDATE  USUserPaymentInfo
                        SET     BillingAddressId = @NewAddressId ,
                                ExternalProfileId = @ExternalProfileId,
								ExternalProfileInfo=@ExistingPaymentInfo
                        WHERE   PaymentInfoId = @ExistingPaymentInfo
                                AND UserId = @CustomerId
		
                    END 
                ELSE 
                    BEGIN 
                        INSERT  INTO USUserPaymentInfo
                                ( Id ,
                                  UserId ,
                                  PaymentInfoId ,
                                  BillingAddressId ,
                                  Sequence ,
                                  IsPrimary ,
                                  CreatedBy ,
                                  CreatedDate ,
                                  Status ,
                                  ModifiedDate ,
                                  ModifiedBy ,
                                  ExternalProfileId,
								  ExternalProfileInfo
		                    )
                        VALUES  ( NEWID() ,
                                  @CustomerId ,
                                  @NewPaymentInfoId ,
                                  @AddressId ,
                                  1 ,
                                  0 ,
                                  @ModifiedBy ,
                                  @CreatedDate ,
                                  1 ,
                                  @CreatedDate ,
                                  @ModifiedBy ,
                                  @ExternalProfileId,
								  @ExternalProfileInfo
		                    )
                    END
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

            END --4


        UPDATE  OROrder
        SET     PaymentTotal = dbo.Order_GetPaymentTotal(@OrderId) ,
                RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
        WHERE   Id = @OrderId

  
  
    END

GO
PRINT 'Creating PaymentInfo_GetByPaymentCreditCardId'
GO
IF(OBJECT_ID('PaymentInfo_GetByPaymentCreditCardId') IS NOT NULL)
	DROP PROCEDURE PaymentInfo_GetByPaymentCreditCardId
GO	

CREATE PROCEDURE [dbo].[PaymentInfo_GetByPaymentCreditCardId](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	-- open the symmetric key 
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	SELECT 
		P.Id,
		P.CardType,
		CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth  ,
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,
		P.NameOnCard,
		CreatedBy,
		dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,
		Status,
		ModifiedBy,
		dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,
		ExternalProfileId,
		ExternalProfileInfo
	FROM PTPaymentInfo P
	Inner Join PTPaymentCreditCard PCC ON P.Id=PCC.PaymentInfoId
	WHERE PCC.Id=@Id

	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO
PRINT 'Creating PaymentCreditCard_GetByPaymentId'
GO
IF(OBJECT_ID('PaymentCreditCard_GetByPaymentId') IS NOT NULL)
	DROP PROCEDURE PaymentCreditCard_GetByPaymentId
GO	

CREATE PROCEDURE [dbo].[PaymentCreditCard_GetByPaymentId](@PaymentId uniqueidentifier)
AS
BEGIN
	SELECT	
		Id,
		PaymentInfoId,
		ParentPaymentInfoId,
		PaymentId,
		BillingAddressId,
		Phone,
		ExternalProfileId,
		ExternalProfileInfo
	FROM PTPaymentCreditCard
	WHERE PaymentId=@PaymentId

END


GO

PRINT 'Creating Payment_GetExternalProfile'
GO
IF(OBJECT_ID('Payment_GetExternalProfile') IS NOT NULL)
	DROP PROCEDURE Payment_GetExternalProfile
GO	

CREATE PROCEDURE Payment_GetExternalProfile
(
	@SavedCardId uniqueidentifier	
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT ExternalProfileId FROM USUserPaymentInfo Where Id = @SavedCardId
END
GO

PRINT 'Creating CreditCardPaymentDto_Get'
GO
IF(OBJECT_ID('CreditCardPaymentDto_Get') IS NOT NULL)
	DROP PROCEDURE CreditCardPaymentDto_Get
GO	

CREATE PROCEDURE [dbo].[CreditCardPaymentDto_Get]
(		
	@PaymentInfoId		UNIQUEIDENTIFIER
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	IF NOT EXISTS (SELECT 1 FROM PTPaymentInfo WHERE Id = @PaymentInfoId)
		SELECT @PaymentInfoId = PaymentInfoId FROM USUserPaymentInfo WHERE Id = @PaymentInfoId

	SELECT P.Id,
		CCT.Title AS CardType,
		CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth,
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,
		P.NameOnCard,
		P.CreatedBy,
		P.CreatedDate,
		P.Status,
		P.ModifiedBy,
		P.ModifiedDate,
		PCC.ExternalProfileId,
		PCC.ExternalProfileInfo
	FROM PTPaymentInfo P
		LEFT JOIN PTCreditCardType AS CCT ON CCT.Id = P.CardType
		LEFT JOIN PTPaymentCreditCard PCC ON P.Id = PCC.PaymentInfoId
	WHERE P.Id = @PaymentInfoId

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO

PRINT 'Creating CustomerPaymentInfo_Save'
GO
IF(OBJECT_ID('CustomerPaymentInfo_Save') IS NOT NULL)
	DROP PROCEDURE CustomerPaymentInfo_Save
GO	
CREATE PROCEDURE [dbo].[CustomerPaymentInfo_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@Status				int=null,
	@CardType			uniqueidentifier,
    @Number				varchar(max),
	@ExpirationMonth	int,
    @ExpirationYear		int,
    @NameOnCard			varchar(255),
    @BillingAddressId	uniqueidentifier,
    @Sequence			int,
    @UserId				uniqueidentifier,
    @IsPrimary			bit,
	@ApplicationId		uniqueidentifier,
	@ExternalProfileId	nvarchar(255)=null,
	@ExternalProfileInfo nvarchar(max)=null,
	@Nickname			nvarchar(255)=null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@stmt	  varchar(256),
		@rowcount	int,
		@PaymentInfoId uniqueidentifier

Begin
--********************************************************************************
-- code
--********************************************************************************


	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	DECLARE @NewCustomerPayment BIT = CASE WHEN @Id IS NULL OR @Id = dbo.GetEmptyGUID() OR NOT EXISTS (SELECT Id FROM USUserPaymentInfo WHERE Id = @Id) THEN 1 ELSE 0 END

	IF @Id IS NULL OR @Id = dbo.GetEmptyGUID()
		SET @Id = NEWID()
       
    IF @IsPrimary = 1
	BEGIN
		UPDATE [dbo].[USUserPaymentInfo]
		SET IsPrimary = 0 WHERE UserId = @UserId
	END
	
	SET @ModifiedDate = GetUTCDate()
	
		-- open the symmetric key 
		OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
		OPEN SYMMETRIC KEY [key_DataShare] 
			DECRYPTION BY CERTIFICATE cert_keyProtection;

		IF @NewCustomerPayment = 1	-- new insert
		BEGIN
			SET @stmt = 'Payment Insert'
			Declare @pId uniqueidentifier
			SET @pId =NewId();

			INSERT INTO [dbo].[PTPaymentInfo]
			   ([Id]
			   ,[CardType]
			   ,[Number]
			   ,[ExpirationMonth]
			   ,[ExpirationYear]
			   ,[NameOnCard]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[Status]
			   ,[ModifiedBy]
			   ,[ModifiedDate])
		 VALUES
			   (@pId
			   ,@CardType
			   ,EncryptByKey(Key_Guid('key_DataShare'), @Number)
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS Varchar(2)))
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS varchar(4)))
			   ,@NameOnCard
			   ,@ModifiedBy
			   ,@ModifiedDate
			   ,@Status
			   ,@ModifiedBy
			   ,@ModifiedDate)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
			INSERT INTO [dbo].[USUserPaymentInfo]
			([Id]
			,[UserId]
			,[PaymentInfoId]
			,[BillingAddressId]
			,[Sequence]
			,[IsPrimary]
			,[CreatedBy]
			,[CreatedDate]
			,[Status]
			,[ModifiedDate]
			,[ModifiedBy]
			,[ExternalProfileId]
			,[ExternalProfileInfo]
			,[Nickname]
			,[SiteId])
			VALUES
			(@Id
			,@UserId
			,@pId
			,@BillingAddressId
			,@Sequence
			,@IsPrimary
			,@ModifiedBy
			,@ModifiedDate
			,@Status
			,@ModifiedDate
			,@ModifiedBy
			,@ExternalProfileId
			,@ExternalProfileInfo
			,@Nickname
			,@ApplicationId)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
        END
        ELSE			-- update
        BEGIN
			SET @stmt = 'Payment Update'
			
			Select @PaymentInfoId=PaymentInfoId  FROM [dbo].[USUserPaymentInfo]
			Where Id=@Id

			UPDATE [dbo].[USUserPaymentInfo] WITH (ROWLOCK)
			SET [PaymentInfoId] = @PaymentInfoId
			  ,[BillingAddressId] = @BillingAddressId
			  ,[Sequence] = @Sequence
			  ,[IsPrimary] = @IsPrimary
			  ,[Status] = @Status
			  ,[ModifiedDate] = @ModifiedDate
			  ,[ModifiedBy] =@ModifiedBy
			  ,[ExternalProfileId]=@ExternalProfileId
			  ,[Nickname]=@Nickname
			  ,[SiteId]=@ApplicationId
			  ,[ExternalProfileInfo]=@ExternalProfileInfo
			WHERE Id=@Id

			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
			  RAISERROR('DBERROR||%s',16,1,@stmt)
			  RETURN dbo.GetDataBaseErrorCode()
			END

			SET @rowcount= 0	

			UPDATE [dbo].[PTPaymentInfo]
			SET [CardType] = @CardType
				,[Number]=CASE WHEN (@Number is null or len(@Number)<=0) then Number else (EncryptByKey(Key_Guid('key_DataShare'), @Number )) end
				,[ExpirationMonth] = CASE WHEN (@ExpirationMonth is null or len(@ExpirationMonth)<=0) then ExpirationMonth else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS VARCHAR(2)) )) end
				,[ExpirationYear]= CASE WHEN (@ExpirationYear is null or len(@ExpirationMonth)<=0) then ExpirationYear else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS VARCHAR(4)) )) end
				,[NameOnCard] = @NameOnCard
				,[Status] = @Status
				,[ModifiedBy] = @ModifiedBy
				,[ModifiedDate] = @ModifiedDate
			WHERE Id=@PaymentInfoId 
					
			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
				RAISERROR('DBERROR||%s',16,1,@stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END


	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END


GO

PRINT 'Creating CustomerDto_Save'
GO
IF(OBJECT_ID('CustomerDto_Save') IS NOT NULL)
	DROP PROCEDURE CustomerDto_Save
GO	

CREATE PROCEDURE [dbo].[CustomerDto_Save]
(	
	@Id						UNIQUEIDENTIFIER,
	@SiteId					UNIQUEIDENTIFIER,
	@AccountNumber			NVARCHAR(255) = NULL,
	@CSRSecurityQuestion	NVARCHAR(MAX) = NULL,
	@CSRSecurityPassword	VARBINARY(MAX) = NULL,
	@ExternalId				NVARCHAR(255),
	@ExternalProfileId		NVARCHAR(255) = NULL,
	@IsExpressCustomer		BIT = NULL,
	@IsActive				BIT = NULL,
	@IsBadCustomer			BIT = NULL,
	@IsOnMailingList		BIT = NULL,
	@IsLockedOut			BIT = NULL
)

AS
BEGIN
	IF (NOT EXISTS(SELECT Id FROM USCommerceUserProfile WHERE Id = @Id))
	BEGIN
		INSERT INTO USCommerceUserProfile (
			Id, AccountNumber, CSRSecurityQuestion, CSRSecurityPassword, ExternalId, ExternalProfileId,
			IsExpressCustomer, IsActive, IsBadCustomer, IsOnMailingList, ModifiedDate, [Status]
		)
			VALUES (
				@Id, @AccountNumber, @CSRSecurityQuestion, @CSRSecurityPassword, @ExternalId, @ExternalProfileId,
				@IsExpressCustomer, @IsActive, @IsBadCustomer, @IsOnMailingList, GETUTCDATE(), 1
			)
	END
	ELSE
	BEGIN
		UPDATE	USCommerceUserProfile
		SET		AccountNumber = @AccountNumber,
				CSRSecurityQuestion = @CSRSecurityQuestion,
				CSRSecurityPassword = @CSRSecurityPassword,
				ExternalId = @ExternalId,
				ExternalProfileId = @ExternalProfileId,
				IsExpressCustomer = @IsExpressCustomer,
				IsActive = @IsActive,
				IsBadCustomer = @IsBadCustomer,
				IsOnMailingList = @IsOnMailingList,
				ModifiedDate = GETUTCDATE()
		WHERE	Id = @Id
	END

	UPDATE	USMembership
	SET		IsLockedOut = ISNULL(@IsLockedOut, IsLockedOut)
	WHERE	UserId = @Id

	-- Add to customer security level
	DECLARE @SecurityLevelId INT = (SELECT TOP 1 Id FROM USSecurityLevel WHERE Title = 'Commerce Customer')

	IF NOT EXISTS (SELECT * FROM USUserSecurityLevel WHERE UserId = @Id AND SecurityLevelId = @SecurityLevelId)
	BEGIN
		INSERT INTO USUserSecurityLevel (UserId, SecurityLevelId, MemberType)
			VALUES (@Id, @SecurityLevelId, 1)
	END

	-- Add to customer Everyone group
	DECLARE	@EveryOneGroupId UNIQUEIDENTIFIER
	SET		@EveryOneGroupId = (
				SELECT	TOP(1) Value 
				FROM	STSettingType AS ST
						INNER JOIN STSiteSetting AS SS ON SS.SettingTypeId = ST.Id
				WHERE	ST.[Name] = 'CommerceEveryOneGroupId' AND
						SS.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@SiteId))
			)

	IF (@EveryOneGroupId IS NOT NULL AND NOT EXISTS (
		SELECT	*
		FROM	USMemberGroup
		WHERE	MemberId = @Id AND
				GroupId = @EveryOneGroupId
	))
		INSERT INTO USMemberGroup (ApplicationId, MemberId, MemberType, GroupId)
			VALUES (@SiteId, @Id, 1, @EveryOneGroupId)
	
	-- Delete user entry from Marketier if exists	
	IF EXISTS (SELECT Id FROM USUser WHERE Id = @Id)
	BEGIN
		INSERT INTO USSiteUser (SiteId, UserId, ISSystemUser, ProductId, IsPrimarySite)
			SELECT	SiteId, @Id, 0, 'CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E', IsPrimarySite
			FROM	MKContactSite
			WHERE	ContactId = @Id AND
					SiteId NOT IN (SELECT SiteId FROM USSiteUser WHERE UserId = @Id)
	END
		
	DELETE
	FROM	MKContactSite
	WHERE	ContactId = @Id

	DELETE
	FROM	MKContact
	WHERE	Id = @Id
END


GO

PRINT 'Creating CustomerPaymentInfo_Get'
GO
IF(OBJECT_ID('CustomerPaymentInfo_Get') IS NOT NULL)
	DROP PROCEDURE CustomerPaymentInfo_Get
GO	

CREATE PROCEDURE [dbo].[CustomerPaymentInfo_Get](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	-- open the symmetric key 
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	Declare @BillingAddressId uniqueidentifier

	SELECT @BillingAddressId = BillingAddressId	
	FROM USUserPaymentInfo 
	WHERE Id=@Id

	SELECT UP.Id,
		P.Id PaymentInfoId,
		P.CardType,
		CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth  ,
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,
		P.NameOnCard,
		UP.UserId,
		UP.BillingAddressId,
		UP.Sequence,
		UP.IsPrimary,
		UP.CreatedBy,
		dbo.ConvertTimeFromUtc(UP.CreatedDate,@ApplicationId)CreatedDate,
		UP.ModifiedBy,
		dbo.ConvertTimeFromUtc(UP.ModifiedDate,@ApplicationId)ModifiedDate,
		UP.[Status],
		UP.ExternalProfileId,
		UP.ExternalProfileInfo,
		UP.Nickname
	FROM PTPaymentInfo P
	Inner Join USUserPaymentInfo UP ON P.Id=UP.PaymentInfoId
	WHERE UP.Id=@Id
	AND UP. [Status] != dbo.GetDeleteStatus()

	SELECT 	 
	A.Id 
	,A.FirstName
	,A.LastName
	,A.AddressType
	,A.AddressLine1
	,A.AddressLine2
	,A.AddressLine3
	,A.City
	,A.StateId
	,A.StateName
	,A.Zip
	,A.CountryId
	,A.Phone
	,A.CreatedBy
	,dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate
	,dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate
	,A.ModifiedBy 
--	,UA.Id AddressId
--	,UA.NickName
--	,UA.IsPrimary
--	,UA.Sequence
	,A.County
	FROM GLAddress A
	--Left Outer Join USUserShippingAddress UA ON A.Id=UA.AddressId
	Where A.Id=@BillingAddressId
	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO

PRINT 'Creating Customer_Get'
GO
IF(OBJECT_ID('Customer_Get') IS NOT NULL)
	DROP PROCEDURE Customer_Get
GO	

CREATE  PROCEDURE [dbo].[Customer_Get]
(
      @Id uniqueidentifier=null,
      @CustomerIds xml=null,
      @UserId uniqueidentifier = null,
      @UserName nvarchar(256) =null,
      @Email      nvarchar(256)=null,
	  @AccountNumber nvarchar(256)=null,
      @isExpress  bit =null,
      @ApplicationId uniqueidentifier =null
)
as
begin
      IF @UserName is null AND @isExpress is null AND @Email is null AND @UserId is null and @AccountNumber is null
            BEGIN
            IF @CustomerIds is null
                  SELECT CP.[Id]
                          ,CP.[Id] as UserId
                          ,U.[FirstName]
                          ,U.[LastName]
                          ,U.[MiddleName]
                          ,U.[CompanyName]
                          ,U.[BirthDate]
                          ,U.[Gender]
						  ,U.[LeadScore]
                          ,CP.[IsBadCustomer]
                          ,CP.[IsActive]
                          ,CP.[IsOnMailingList]
                          ,CP.[Status]
                          ,CP.[CSRSecurityQuestion]
                          ,CP.[CSRSecurityPassword]
                          ,U.[HomePhone]
                          ,U.[MobilePhone]
                          ,U.[OtherPhone]
                          ,U.ImageId
                          ,CP.[AccountNumber]
                          ,CP.[IsExpressCustomer]
                          ,CP.ExternalId
                          ,CP.ExternalProfileId
                          ,      dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                          , (Select count(*) from CSCustomerLogin where CustomerId= Isnull(@Id,CP.Id)) +1 as NumberOfLoginAccounts
                          , (Select count(*) from PTLineOfCreditInfo where CustomerId = IsnuLL(@Id, CP.Id) 
                                    and Status <> dbo.GetDeleteStatus()) as HasLOC
                   FROM [USCommerceUserProfile] CP
                   INNER JOIN USUser U on CP.Id = U.Id
            WHERE --CP.Id = Isnull(@Id,CP.Id)
				(@Id is null or CP.Id = @Id)
				 AND U.Status !=3  OPTION(RECOMPILE)
			ELSE
						  SELECT CP.[Id]
                          ,CP.[Id] as UserId
                          ,U.[FirstName]
                          ,U.[LastName]
                          ,U.[MiddleName]
                          ,U.[CompanyName]
                          ,U.[BirthDate]
                          ,U.[Gender]
						  ,U.[LeadScore]
                          ,CP.[IsBadCustomer]
                          ,CP.[IsActive]
                          ,CP.[IsOnMailingList]
                          ,CP.[Status]
                          ,CP.[CSRSecurityQuestion]
                          ,CP.[CSRSecurityPassword]
                          ,U.[HomePhone]
                          ,U.[MobilePhone]
                          ,U.[OtherPhone]
                          ,U.ImageId
                          ,CP.[AccountNumber]
                          ,CP.[IsExpressCustomer]
                          ,CP.ExternalId
                          ,CP.ExternalProfileId
                          ,      dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                          , (Select count(*) from CSCustomerLogin where CustomerId= CP.Id) +1 as NumberOfLoginAccounts
                          , (Select count(*) from PTLineOfCreditInfo where CustomerId =  CP.Id 
                                    and Status <> dbo.GetDeleteStatus()) as HasLOC
                   FROM [USCommerceUserProfile] CP
                   INNER JOIN USUser U ON CP.Id = U.Id
                   INNER JOIN @CustomerIds.nodes('/GenericCollectionOfGuid/guid')tab(col) ON CP.Id =tab.col.value('text()[1]','uniqueidentifier')
                   Where U.Status !=3
            
            END
      ELSE
            BEGIN
                  if @UserId is null
                        begin
                            DECLARE @UserIdParams nvarchar(max)
							DECLARE @UserIdSQL nvarchar(max)
							SET @UserIdParams ='@UserId uniqueidentifier OUTPUT,@UserName nvarchar(256),@Email nvarchar(256),@AccountNumber nvarchar(256),@ApplicationId uniqueidentifier';

							SET @UserIdSQL =
							'SELECT @UserId = U.Id FROM USUser U
							INNER JOIN USMembership M on M.UserId = U.Id
							INNER JOIN USSiteUser US ON US.UserId = U.Id
							LEFT JOIN USCommerceUserProfile UCP ON UCP.Id = U.Id
							WHERE U.Status <> dbo.GetDeleteStatus()';

							IF @UserName IS NOT NULL SET @UserIdSQL += ' AND U.UserName = @UserName';

							IF @Email IS NOT NULL SET @UserIdSQL += ' AND M.Email = @Email';

							IF @AccountNumber IS NOT NULL SET @UserIdSQL += ' AND UCP.AccountNumber = @AccountNumber';

							IF @ApplicationId IS NOT NULL SET @UserIdSQL +=' AND US.SiteID IN (Select SiteId from  [dbo].[GetParentSites](@ApplicationId,1,1))';
							
							EXEC sp_executesql @UserIdSQL, @UserIdParams,  @UserId OUTPUT,@UserName,@Email, @AccountNumber,@ApplicationId
                                                
                                     
                        end
						
                  
                  if(@UserId is not null)
                        begin
                              SELECT CP.[Id]
                                      ,@UserId as UserId
                                      ,U.[FirstName]
                                      ,U.[LastName]
                                      ,U.[MiddleName]
                                      ,U.[CompanyName]
                                      ,U.[BirthDate]
                                      ,U.[Gender]
									  ,U.[LeadScore]
                                      ,CP.[IsBadCustomer]
                                      ,CP.[IsActive]
                                      ,CP.[IsOnMailingList]
                                      ,CP.[Status]
                                      ,CP.[CSRSecurityQuestion]
                                      ,CP.[CSRSecurityPassword]
                                      ,U.[HomePhone]
                                      ,U.[MobilePhone]
                                      ,U.[OtherPhone]
                                      ,U.ImageId
                                      ,CP.[AccountNumber]
                                      ,CP.[IsExpressCustomer]
                                      ,CP.[ModifiedDate]
                                      ,CP.ExternalId
									  ,CP.ExternalProfileId
                                      ,      dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                                        , (Select count(*) from CSCustomerLogin where CustomerId= Isnull(@Id,CP.Id)) +1 as NumberOfLoginAccounts
                                       , (Select count(*) from PTLineOfCreditInfo where CustomerId = IsnuLL(@Id, CP.Id)
                                                                                    and Status <> dbo.GetDeleteStatus()) as HasLOC
                               FROM [USCommerceUserProfile] CP
                               INNER JOIN USUser U ON CP.Id = U.Id
                               WHERE (CP.Id = (Select CustomerId from CSCustomerLogin where UserId = @UserId) OR CP.Id = @UserId )
                                    --AND IsExpressCustomer =isnull(@isExpress,IsExpressCustomer) 
									AND (@isExpress is null or IsExpressCustomer = @isExpress)
									AND U.Status != 3
                        end
            END  
end


GO
PRINT 'Creating CustomerPaymentInfoDto_Get'
GO
IF(OBJECT_ID('CustomerPaymentInfoDto_Get') IS NOT NULL)
	DROP PROCEDURE CustomerPaymentInfoDto_Get
GO	
CREATE PROCEDURE [dbo].[CustomerPaymentInfoDto_Get]
(
	@Id					uniqueidentifier = null,
	@CustomerId			uniqueidentifier = null,
	@SiteId				uniqueidentifier,
	@PaymentType        int = null
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	WITH CTE AS
	(
	SELECT UP.Id,  
		UP.UserId AS CustomerId,  
		1 AS PaymentType,
		UP.NickName,
		UP.IsPrimary,
		UP.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		CCT.Title AS CreditCardType,  
		P.NameOnCard AS CreditCardName,
		CONVERT(varchar(max), DecryptByKey(P.Number)) AS CreditCardNumber,  
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,  
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) AS CreditCardExpirationYear,  
		UP.ExternalProfileId AS CreditCardExternalProfileId,
		UP.ExternalProfileInfo,
		NULL AS GiftCardNumber,
		NULL AS GiftCardBalance,
		NULL AS AccountNumber,
		UP.CreatedDate, 
		UP.CreatedBy AS CreatedById,
		UP.ModifiedDate, 
		UP.ModifiedBy AS ModifiedById
	FROM PTPaymentInfo P
		LEFT JOIN USUserPaymentInfo UP ON P.Id = UP.PaymentInfoId  
		LEFT JOIN PTCreditCardType AS CCT ON CCT.Id = P.CardType
		LEFT JOIN GLAddress A ON A.Id = UP.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR UP.Id = @Id)
		AND (@CustomerId IS NULL OR UP.UserId = @CustomerId) 
		AND UP.[Status] = 1
		AND (UP.SiteId IS NULL OR UP.SiteId=@SiteId)

	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		5 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		P.GiftCardNumber,
		P.BalanceAfterLastTransaction AS GiftCardBalance,
		NULL AS AccountNumber,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTGiftCardInfo P  
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 5 AND Status = 1 AND SiteId = @SiteId)
		AND (P.SiteId IS NULL OR P.SiteId=@SiteId)
	
	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		2 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		U.AccountNumber,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTLineOfCreditInfo P  
		INNER JOIN USCommerceUserProfile U ON U.Id = P.CustomerId
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 2 AND Status = 1 AND SiteId = @SiteId)
		AND (P.SiteId IS NULL OR P.SiteId=@SiteId)
	
	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		4 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTCODInfo P  
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 4 AND Status = 1 AND SiteId = @SiteId)
		AND (P.SiteId IS NULL OR P.SiteId=@SiteId)
	)

	SELECT Id,  
		CustomerId,  
		PaymentType,
		NickName,
		IsPrimary,
		BillingAddressId,
		BillingAddressLine1,
		BillingAddressLine2,
		BillingAddressLine3,
		BillingCity,
		BillingStateId,
		BillingZip,
		BillingCountryId,
		BillingPhone,
		BillingStateName,
		BillingStateCode,
		BillingCountryName,
		BillingCountryCode,
		PaymentTypeInfoId,
		CreditCardType,  
		CreditCardName,
		CreditCardNumber,  
		CreditCardExpirationMonth,  
		CreditCardExpirationYear,  
		CreditCardExternalProfileId,
		ExternalProfileInfo,
		GiftCardNumber,
		GiftCardBalance,
		AccountNumber,
		CreatedDate, 
		CreatedById,
		ModifiedDate, 
		ModifiedById
	FROM CTE
	WHERE PaymentType = ISNULL(@PaymentType, PaymentType)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO

PRINT 'Creating Payment_UpdateExternalProfile'
GO
IF(OBJECT_ID('Payment_UpdateExternalProfile') IS NOT NULL)
	DROP PROCEDURE Payment_UpdateExternalProfile
GO
CREATE PROCEDURE Payment_UpdateExternalProfile
(
	@ExternalProfileId nvarchar(255),
	@ExternalProfileInfo nvarchar(max),
	@ModifiedBy uniqueidentifier,
	@ModifiedDate DateTime
)
AS
BEGIN
	UPDATE USUserPaymentInfo 
		SET ExternalProfileInfo =@ExternalProfileInfo 
			,ModifiedBy=@ModifiedBy
			,ModifiedDate=@ModifiedDate
	Where ExternalProfileId=@ExternalProfileId
END

GO

PRINT 'Creating Payment_GetExternalProfile'
GO
IF(OBJECT_ID('Payment_GetExternalProfile') IS NOT NULL)
	DROP PROCEDURE Payment_GetExternalProfile
GO
CREATE PROCEDURE Payment_GetExternalProfile
(
	@SavedCardId uniqueidentifier	
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT ExternalProfileId,ExternalProfileInfo FROM USUserPaymentInfo Where Id = @SavedCardId
END


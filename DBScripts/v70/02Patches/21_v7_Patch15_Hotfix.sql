PRINT 'Add Site Settings'
DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int

SET @SettingGroupId = 1
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Admin.GoogleMapsApiKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Admin.GoogleMapsApiKey', 'Google Maps Api Key for admin',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, 'AIzaSyCez2iKOgXpbwa2D_mFQbcLJ5rld_In4AY' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
GO
PRINT 'Creating index IX_ATAttributeEnum_AttributeID_Id_Title'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name='IX_ATAttributeEnum_AttributeID_Id_Title' AND object_id = OBJECT_ID('ATAttributeEnum'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_ATAttributeEnum_AttributeID_Id_Title]
	ON [dbo].[ATAttributeEnum] ([AttributeID])
	INCLUDE ([Id],[Title])
END
GO
PRINT 'Alter Perf_GetPerformanceMetrics'
GO
IF (OBJECT_ID('Perf_GetPerformanceMetrics') IS NOT NULL)
	DROP PROCEDURE Perf_GetPerformanceMetrics
GO
CREATE PROCEDURE Perf_GetPerformanceMetrics
(
@QueryTypeId INT,
@DbName varchar(max)=NULL
)
AS
BEGIN
 /*
 * 1. LONG RUNNNG QUERIES
 * 2. QUERY THAT SPEND LONGEST TIME WAITING
 * 3. QUERY THAT USE MOST CPU
 * 4. QUERY THAT USE MOST IO
 * 5. QUERY THAT EXECUTED MOST OFTEN
 * 6. QuERY WITH MISSING STATISTICS
 * 7. Index Costly Missing Index
 */

 IF(@DbName='')
	SET @DbName = NULL


	--LONG RUNNGING QUERIES
	IF (@QueryTypeId = 1)
	BEGIN
		SELECT TOP 20
			CAST(qs.total_elapsed_time / 1000000.0 AS DECIMAL(28, 2))
			AS [Total Duration (s)]
			, CAST(qs.total_worker_time * 100.0 / qs.total_elapsed_time
			AS DECIMAL(28, 2)) AS [% CPU]
			, CAST((qs.total_elapsed_time - qs.total_worker_time)* 100.0 /
			qs.total_elapsed_time AS DECIMAL(28, 2)) AS [% Waiting]
			, qs.execution_count
			, CAST(qs.total_elapsed_time / 1000000.0 / qs.execution_count
			AS DECIMAL(28, 2)) AS [Average Duration (s)]
			, SUBSTRING (qt.text,(qs.statement_start_offset/2) + 1,
			((CASE WHEN qs.statement_end_offset = -1
			THEN LEN(CONVERT(NVARCHAR(MAX), qt.text)) * 2
			ELSE qs.statement_end_offset
			END - qs.statement_start_offset)/2) + 1) AS [Individual Query]
			, qt.text AS [Parent Query]
			, DB_NAME(qt.dbid) AS DatabaseName
			--, qp.query_plan
			FROM sys.dm_exec_query_stats qs
			CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle) as qt
			--CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle) qp
			WHERE qs.total_elapsed_time > 0
			AND  LOWER(DB_NAME(qt.dbid))=ISNULL(@DbName,LOWER(DB_NAME(qt.dbid)))
			ORDER BY qs.total_elapsed_time DESC
	END
	ELSE IF (@QueryTypeId = 2)
	BEGIN
				SELECT TOP 50
		CAST((qs.total_elapsed_time - qs.total_worker_time) /
		1000000.0 AS DECIMAL(28,2)) AS [Total time blocked (s)]
		, CAST(qs.total_worker_time * 100.0 / qs.total_elapsed_time
		AS DECIMAL(28,2)) AS [% CPU]
		, CAST((qs.total_elapsed_time - qs.total_worker_time)* 100.0 /
		qs.total_elapsed_time AS DECIMAL(28, 2)) AS [% Waiting]
		, qs.execution_count
		, CAST((qs.total_elapsed_time - qs.total_worker_time) / 1000000.0
		/ qs.execution_count AS DECIMAL(28, 2)) AS [Blocking average (s)]
		, SUBSTRING (qt.text,(qs.statement_start_offset/2) + 1,
		((CASE WHEN qs.statement_end_offset = -1
		THEN LEN(CONVERT(NVARCHAR(MAX), qt.text)) * 2
		ELSE qs.statement_end_offset
		END - qs.statement_start_offset)/2) + 1) AS [Individual Query]
		, qt.text AS [Parent Query]
		, DB_NAME(qt.dbid) AS DatabaseName
		--, qp.query_plan
		FROM sys.dm_exec_query_stats qs
		CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle) as qt
		--CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle) qp
		WHERE qs.total_elapsed_time > 0
		AND  LOWER(DB_NAME(qt.dbid))=ISNULL(@DbName,LOWER(DB_NAME(qt.dbid)))
		ORDER BY [Total time blocked (s)] DESC
	END
	ELSE IF(@QueryTypeId=3) 
	BEGIN
				SELECT TOP 20
		CAST((qs.total_worker_time) / 1000000.0 AS DECIMAL(28,2))
		AS [Total CPU time (s)]
		, CAST(qs.total_worker_time * 100.0 / qs.total_elapsed_time
		AS DECIMAL(28,2)) AS [% CPU]
		, CAST((qs.total_elapsed_time - qs.total_worker_time)* 100.0 /
		qs.total_elapsed_time AS DECIMAL(28, 2)) AS [% Waiting]
		, qs.execution_count
		, CAST((qs.total_worker_time) / 1000000.0
		/ qs.execution_count AS DECIMAL(28, 2)) AS [CPU time average (s)]
		, SUBSTRING (qt.text,(qs.statement_start_offset/2) + 1,
		((CASE WHEN qs.statement_end_offset = -1
		THEN LEN(CONVERT(NVARCHAR(MAX), qt.text)) * 2
		ELSE qs.statement_end_offset
		END - qs.statement_start_offset)/2) + 1) AS [Individual Query]
		, qt.text AS [Parent Query]
		, DB_NAME(qt.dbid) AS DatabaseName
		--, qp.query_plan
		FROM sys.dm_exec_query_stats qs
		CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle) as qt
		--CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle) qp
		WHERE qs.total_elapsed_time > 0
		AND  LOWER(DB_NAME(qt.dbid))=ISNULL(@DbName,LOWER(DB_NAME(qt.dbid)))
		ORDER BY [Total CPU time (s)] DESC
	END
	ELSE IF(@QueryTypeId = 4)
	BEGIN
				SELECT TOP 20
		[Total IO] = (qs.total_logical_reads + qs.total_logical_writes)
		, [Average IO] = (qs.total_logical_reads + qs.total_logical_writes) /
		qs.execution_count
		, qs.execution_count
		, SUBSTRING (qt.text,(qs.statement_start_offset/2) + 1,
		((CASE WHEN qs.statement_end_offset = -1
		THEN LEN(CONVERT(NVARCHAR(MAX), qt.text)) * 2
		ELSE qs.statement_end_offset
		END - qs.statement_start_offset)/2) + 1) AS [Individual Query]
		, qt.text AS [Parent Query]
		, DB_NAME(qt.dbid) AS DatabaseName
		--, qp.query_plan
		FROM sys.dm_exec_query_stats qs
		CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle) as qt
		--CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle) qp
		WHERE  LOWER(DB_NAME(qt.dbid))=ISNULL(@DbName,LOWER(DB_NAME(qt.dbid)))
		ORDER BY [Total IO] DESC
	END
	ELSE IF(@QueryTypeId = 5)
	BEGIN
				SELECT TOP 20
		qs.execution_count
		, SUBSTRING (qt.text,(qs.statement_start_offset/2) + 1,
		((CASE WHEN qs.statement_end_offset = -1
		THEN LEN(CONVERT(NVARCHAR(MAX), qt.text)) * 2
		ELSE qs.statement_end_offset
		END - qs.statement_start_offset)/2) + 1) AS [Individual Query]
		, qt.text AS [Parent Query]
		, DB_NAME(qt.dbid) AS DatabaseName
		--, qp.query_plan
		FROM sys.dm_exec_query_stats qs
		CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle) as qt
		--CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle) qp
		WHERE  LOWER(DB_NAME(qt.dbid))=ISNULL(@DbName,LOWER(DB_NAME(qt.dbid)))
		ORDER BY qs.execution_count DESC;
	END
	ELSE IF(@QueryTypeId = 6)
	BEGIN
				SELECT TOP 20
		st.text AS [Parent Query]
		, DB_NAME(st.dbid)AS [DatabaseName]
		, cp.usecounts AS [Usage Count]
		,@DbName DBName
		--, qp.query_plan
		FROM sys.dm_exec_cached_plans cp
		CROSS APPLY sys.dm_exec_sql_text(cp.plan_handle) st
		CROSS APPLY sys.dm_exec_query_plan(cp.plan_handle) qp
		WHERE CAST(qp.query_plan AS NVARCHAR(MAX))
		LIKE '%<ColumnsWithNoStatistics>%'
		AND LOWER(DB_NAME(st.dbid))=ISNULL(@DbName,LOWER(DB_NAME(st.dbid)))
		ORDER BY cp.usecounts DESC
	END
	ELSE IF(@QueryTypeId = 7)
	BEGIN
				SELECT TOP 20
		ROUND(s.avg_total_user_cost *
		s.avg_user_impact
		* (s.user_seeks + s.user_scans),0)
		AS [Total Cost]
		, d.[statement] AS [Table Name]
		, equality_columns
		, inequality_columns
		, included_columns
		FROM sys.dm_db_missing_index_groups g
		INNER JOIN sys.dm_db_missing_index_group_stats s
		ON s.group_handle = g.index_group_handle
		INNER JOIN sys.dm_db_missing_index_details d
		ON d.index_handle = g.index_handle
		ORDER BY [Total Cost] DESC
	END
	
END
GO
PRINT 'Creating view vwSiteAttributeValue'
GO
IF(OBJECT_ID('vwSiteAttributeValue') IS NOT NULL)
	DROP VIEW vwSiteAttributeValue
GO	
CREATE VIEW [dbo].[vwSiteAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.SiteId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V

	UNION ALL

	SELECT 1 AS vRank,
		V.Id,
		C.Id AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V
		JOIN SISite C ON V.SiteId = C.ParentSiteId
	WHERE V.IsShared = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT V.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.SiteId,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATSiteAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.SiteId
		AND C.SiteId = V.SiteId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1
GO

PRINT 'Add Content column in SEIndex'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SEIndex' AND COLUMN_NAME = 'Content')
BEGIN	
	ALTER TABLE SEIndex ADD Content nvarchar(max) NULL
END
GO
PRINT 'Add Provider column in SESearchSetting'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SESearchSetting' AND COLUMN_NAME = 'Provider')
BEGIN	
	ALTER TABLE SESearchSetting ADD [Provider] nvarchar(50) NULL
END
GO
UPDATE SESearchSetting SET Provider = 'ELASTIC' WHERE provider IS NULL
GO

IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Search.Provider' AND ControlData like '%BRIDGELINE%')
BEGIN
	UPDATE STSettingType SET 
		ControlData = '{ "ELASTIC" : "ELASTIC", "ISYS" : "ISYS", "BRIDGELINE" : "BRIDGELINE" }' 
	WHERE Name = 'Search.Provider'
END
GO

DECLARE @Sequence int, @ModifiedBy uniqueidentifier
SELECT @ModifiedBy = Id FROM USUser WHERE Username like 'iappssystemuser'
IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.ApiUrl')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.ApiUrl', 'https://50h5v7dmqb.execute-api.us-east-1.amazonaws.com', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.ApiKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.ApiKey', '4svsGY1FDW9tXhoO0lqAnl3O8HaPvRY3PEamr9s5', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.SiteKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.SiteKey', REPLACE(Title, ' ', '-'), @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END

GO
IF (OBJECT_ID('UserDto_Get') IS NOT NULL)
	DROP PROCEDURE UserDto_Get
GO
CREATE PROCEDURE [dbo].[UserDto_Get]  
(  
	@Id						uniqueidentifier = NULL,  
	@SiteId					uniqueidentifier,
	@ProductId				uniqueidentifier = NULL,
	@IgnoreSite				bit = NULL,   
	@IgnoreProduct			bit = NULL,   
	@IsSystemUser			bit = NULL,   
	@IsEditable				bit = NULL,   
	@Status					int = NULL,   
	@Role					int = NULL,   
	@PageSize				int = NULL,   
	@PageNumber				int = NULL ,
	@MaxRecords				int = NULL,
	@Keyword				nvarchar(MAX) = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL
)  
WITH RECOMPILE
AS  
BEGIN
	DECLARE @EmptyGuid uniqueidentifier, @PageLowerBound int, @PageUpperBound int, @TotalRecords int,
		@SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @Id = @EmptyGuid SET @Id = NULL
	IF @IsSystemUser IS NULL SET @IsSystemUser = 1
	IF @Status = 0 SET @Status = NULL
	IF @Role = 0 SET @Role = NULL

	IF @Role = -1 -- All Users
	BEGIN
		SET @Role = NULL
		SET @IgnoreProduct = 1
		SET @IgnoreSite = 1
	END

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)

	IF @Id IS NULL	
	BEGIN
		DECLARE @tbSiteIds TABLE(Id uniqueidentifier primary key)
		IF (@IgnoreSite = 0 AND @SiteId IS NOT NULL)
		BEGIN
			INSERT INTO @tbSiteIds
			SELECT DISTINCT SiteId FROM [dbo].[GetAncestorSitesForSystemUser](@SiteId, @IsSystemUser, null)
		END

		DECLARE @tbRoleUsers TABLE(Id uniqueidentifier primary key)
		IF (@Role IS NOT NULL)
		BEGIN
			INSERT INTO @tbRoleUsers
			SELECT Id FROM [dbo].[User_GetUsersInRole] (@Role, 1, @SiteId, @IsSystemUser, NULL, @ProductId)
		END

		INSERT INTO @tbIds
		SELECT U.Id AS Id, ROW_NUMBER() OVER (ORDER BY
			CASE WHEN @SortClause = 'FirstName ASC' THEN U.FirstName END ASC,
			CASE WHEN @SortClause = 'FirstName DESC' THEN U.FirstName END DESC,
			CASE WHEN @SortClause = 'LastName ASC' THEN U.LastName END ASC,
			CASE WHEN @SortClause = 'LastName DESC' THEN U.LastName END DESC,
			CASE WHEN @SortClause = 'Email ASC' THEN U.Email END ASC,
			CASE WHEN @SortClause = 'Email DESC' THEN U.Email END DESC,
			CASE WHEN @SortClause = 'UserName ASC' THEN U.UserName END ASC,
			CASE WHEN @SortClause = 'UserName DESC' THEN U.UserName END DESC,
			CASE WHEN @SortClause IS NULL THEN U.UserName END ASC		
		) AS RowNumber
		FROM vwUser AS U
		WHERE (U.IsSystemUser = @IsSystemUser) AND
			(@Status IS NULL OR U.[Status] = @Status) AND
			(@IsEditable IS NULL OR U.IsEditable = @IsEditable) AND
			(@Keyword IS NULL OR U.FirstName like @Keyword OR U.LastName like @Keyword OR U.UserName like @Keyword OR U.Email like @Keyword) AND
			(@IgnoreSite = 1 OR @SiteId IS NULL OR EXISTS (SELECT 1 FROM vwUserSites US JOIN @tbSiteIds T ON US.SiteId = T.Id WHERE U.Id = US.UserId)) AND
			(@IgnoreProduct = 1 OR @ProductId IS NULL OR EXISTS (SELECT 1 FROM vwUserProducts UP WHERE U.Id = UP.UserId AND UP.ProductId = @ProductId)) AND
			(@Role IS NULL OR EXISTS (SELECT 1 FROM @tbRoleUsers WHERE Id = U.Id)) 

		SELECT @TotalRecords = COUNT(1) FROM @tbIds
	
		INSERT INTO @tbPagedResults
		SELECT T.Id, T.RowNumber, @TotalRecords 
		FROM @tbIds T
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
			AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	END
	ELSE
	BEGIN
		INSERT INTO @tbPagedResults
		SELECT @Id, 1, 1
	END

	SELECT U.Id,
		U.FirstName,
		U.LastName,
		U.MiddleName,
		U.UserName,
		M.Email,
		U.LastActivityDate,
		U.EmailNotification,
		U.LeadScore,
		U.ExpirationDate,
		U.[Status],
		T.TotalRecords
	FROM vwUser U
		JOIN @tbPagedResults T ON U.Id = T.Id
		JOIN USMembership M ON U.Id = M.UserId
	ORDER BY T.RowNumber
END
GO
IF (OBJECT_ID('Site_CreateMicroSiteData') IS NOT NULL)
	DROP PROCEDURE Site_CreateMicroSiteData
GO
CREATE PROCEDURE [dbo].[Site_CreateMicroSiteData]
(
	@ProductId						uniqueidentifier,
	@MasterSiteId					uniqueidentifier,
	@MicroSiteId					uniqueidentifier,
	@ModifiedBy						uniqueidentifier = null,
	@ImportAllAdminUserAndPermission bit ,
	@ImportAllWebSiteUser			bit,
	@PageImportOptions				int = 2,
	@ImportContentStructure			bit = 0,
	@ImportFileStructure			bit = 0,
	@ImportImageStructure			bit = 0,
	@ImportFormsStructure			bit = 0
)
AS
BEGIN
	-- Import Front end and admin users
	EXEC [dbo].[VariantSite_ImportUser] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllWebSiteUser, @ImportAllAdminUserAndPermission, @ProductId
	-- Import Menu and Pages
	EXEC [dbo].[Site_CopyMenusAndPages] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, @PageImportOptions
	--content structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 7, @ImportContentStructure
	--File structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 9, @ImportFileStructure
	--Image structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 33, @ImportImageStructure
	--Forms structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 38, @ImportFormsStructure
	
	-- Import Tag, Form, etc. 
	EXEC [VariantSite_CopyMisc] @MicroSiteId, @ModifiedBy
	
	-- Changing the status of the site		
	UPDATE SISite SET Status = 1 WHERE Id = @MicroSiteId		

	DECLARE @ContainerLocking int 
	SELECT @ContainerLocking = Id FROM STSettingType WHERE Name = 'Container.EnableLocking'

	IF (@ContainerLocking IS NOT NULL)
	BEGIN
		INSERT INTO STSiteSetting (SiteId, SettingTypeId, Value)
		VALUES (@MicroSiteId, @ContainerLocking, 'false')
	END

	--CREATE MARKETIER RELATED DEFAULT DATA FOR VARIANT SITES
	EXEC Site_CreateMicrositeDataForMarketier @MicroSiteId = @MicroSiteId, @ModifiedBy = @ModifiedBy

	--CREATE COMMERCE RELATED DEFAULT DATA FOR VARIANT SITES
	EXEC Site_CreateMicrositeDataForCommerce @MasterSiteId, @MicroSiteId

	;WITH CTE AS
	(
		SELECT PageMapNodeId, ROW_NUMBER() OVER(PARTITION BY ParentId ORDER BY LftValue) AS DisplayOrder
		FROM PageMapNode WHERE SiteId = @MicroSiteId
	)

	UPDATE P
	SET P.DisplayOrder = C.DisplayOrder
	FROM PageMapNode P JOIN CTE C ON P.PageMapNodeId = C.PageMapNodeId	

	--Inserting in Country Cache
	INSERT INTO [dbo].[GLCountryVariantCache]
    (
		[Id],
        [SiteId],
        [CountryName],
        [Status],
        [ModifiedBy],
        [ModifiedDate]
	)
    SELECT
		Id,
		@MicroSiteId,
		CountryName,
		Status,
		ModifiedBy,
		ModifiedDate
	FROM vwCountry where SiteId = @MasterSiteId

	--Inserting in State Cache
	INSERT INTO [dbo].[GLStateVariantCache]
    (
		[Id],
        [SiteId],
        [State],
        [Status],
        [ModifiedBy],
        [ModifiedDate]
	)
    SELECT
		Id,
		@MicroSiteId,
		State,
		Status,
		ModifiedBy,
		ModifiedDate
	FROM vwState where SiteId = @MasterSiteId
	
	--Inserting default data into Site Robots table
	INSERT INTO [dbo].[SIRobots]
	(
	   [ApplicationId], 
	   [RobotsContent], 
	   [LastModifiedDate]
	) 
	SELECT 
	@MicroSiteId, 
	'User-agent: *
	Disallow: /admin/
	Disallow: /commonlogin/
	Disallow: /marketieradmin/
	Disallow: /commerceadmin/
    Disallow: /insightsadmin/	
	Disallow: /webservice/
	Disallow: /product-type/
	Disallow: /view_cart/
	Disallow: /checkout/
	Disallow: /my_account/
	Disallow: /payment/
	Disallow: /order/
	Disallow: /marketiercampaign/', 
	GetDate() 
	FROM SISite WHERE Id = @MicroSiteId
END
GO

IF (OBJECT_ID('PurgeCommerceData') IS NOT NULL)
	DROP PROCEDURE PurgeCommerceData
GO
CREATE PROCEDURE PurgeCommerceData
AS
BEGIN 
--NOTE: This script can run for very long time, see output message for the progresss.
-- This script rebuilds indexes, so it is better to take the site offline before executing the script.
-- If the last step - Truncating of VEVersion is taking longer, the script can be stopped and site can be bought back online and truncation of VEVersion can be re-Run (Line no:625 to end of file)
-- After the script is complte, make sure the DB is set to 'Full' Recovery mode or the one which was set before the script is executed, the first line of the script sets the recovery to simple and last line sets it to full.
Declare @count int
Declare @dbName varchar(50)
SET @dbName = db_name()

EXEC ('ALTER DATABASE [' +@dbName +'] SET RECOVERY SIMPLE');

Declare @abandonedCartTime int
SET @abandonedCartTime =30

INSERT INTO ORAbandonedCheckout
	SELECT --DISTINCT
	O.id As OrderId,
	CT.Id As CartId,
	CUP.Id As CustomerId,
	O.OrderTotal As CartTotal,
	USU.LastName + ',' + USU.FirstName AS FullName,
	UMember.Email,
	Isnull(CT.ModifiedDate,CT.CreatedDate) As ModifiedDate,
	CT.ExpirationDate,
	O.SiteId
FROM
	dbo.CSCart AS CT 
	INNER JOIN OROrder O on O.CartId = CT.Id
	INNER JOIN dbo.USUser AS USU ON (O.CustomerId = USU.Id)
	INNER JOIN dbo.USCommerceUserProfile AS CUP ON CUP.Id = USU.Id
	INNER JOIN dbo.USMembership AS UMember ON UMember.UserId = USU.Id
	--INNER JOIN dbo.USSiteUser AS SU ON SU.UserId = USU.Id
	WHERE  
	(O.OrderStatusId = 12)
	AND O.ModifiedDate < DATEADD(day, DATEDIFF(day, 0, GETDATE()), -@abandonedCartTime -1)
	AND O.Id not in (Select OrderId from ORAbandonedCheckout)
	AND O.Id NOT IN (Select OrderId from PTOrderPayment)


PRINT SYSDatetime()
print 'Clearing RPTCommerceReport_Category'
Truncate Table RPTCommerceReport_Category


PRINT SYSDatetime()
print 'Inserting to RPTCommerceReport_Category '
-- only records for 2017 are inserted, this is creating lot of records and if required, we can backfill data for previous years.
INSERT  INTO dbo.RPTCommerceReport_Category
                ( Id ,
                  OrderTotal ,
                  Quantity ,
                  NavNodeId ,
                  OrderDate
                                
                )
            SELECT newid() ,
                                            SUM(Price * Quantity) - SUM(OI.Discount) AS OrderTotal ,
                                            SUM(Quantity) AS Quantity ,
                                            NV.NavNodeId ,
                                            CONVERT(date, O.OrderDate ) OrderDate
                                     FROM   OROrder O
                                            INNER JOIN dbo.OROrderItem OI ON OI.OrderId = O.Id
                                            INNER JOIN dbo.PRProductSKU PS ON PS.Id = OI.ProductSKUId
                                            INNER JOIN NVFilterOutput NVO ON NVO.ObjectId = PS.ProductId
                                            INNER JOIN NVFilterQuery FQ ON NVO.QueryId = FQ.Id
                                            INNER JOIN NVNavNodeNavFilterMap NV ON NVO.QueryId = NV.QueryId
                                            LEFT JOIN NVFilterExclude NVEx ON NVEx.QueryId = FQ.Id
                                                              AND NVO.ObjectId = NVEx.ObjectId
                                     WHERE  NVEx.ObjectId IS NULL
                                            AND OI.OrderItemStatusId <> 3
                                            AND O.OrderStatusId IN ( 1, 2, 11,
                                                              4 )
											AND O.OrderDate>CONVERT(date, (GETDATE()-365))
                                     GROUP BY CONVERT(date, O.OrderDate ),
                                            NV.NavNodeId 
                                  




PRINT SYSDATETIME()
PRINT 'deleting LGException and LGLOG leaving last 14 days record'
Delete LG
From LGExceptionLog LG
INNER JOIN LGLog L ON L.Id = LG.LogId
WHERE CreatedDate < DATEADD(day, -14, GETDATE())

Delete From LGLog 
WHERE CreatedDate < DATEADD(day, -14, GETDATE())

 
PRINT SYSDATETIME()
PRINT 'deleting LGLogHistory'
TRUNCATE TABLE LGLogHistory

PRINT SYSDATETIME()
PRINT 'deleting LGCategoryLog'
TRUNCATE TABLE LGExceptionLogHistory

PRINT SYSDATETIME()
PRINT 'deleting LGCategoryLog'
TRUNCATE TABLE LGCategoryLog

PRINT SYSDATETIME()
PRINT 'deleting LGCategoryLog'
TRUNCATE TABLE LGCategoryLogHistory


PRINT SYSDatetime()
print 'deleting PTOrderPayment'
DELETE OP FROM PTOrderPayment OP
INNER JOIN ORAbandonedCheckout AC ON OP.OrderId = AC.OrderId


PRINT SYSDatetime()
print 'deleting CSCartCoupn'
DELETE CP FROM CSCartCoupon CP
INNER JOIN CSCart C ON CP.CartId =C.Id
INNER JOIN OROrder O on C.Id = O.CartId
INNER JOIN ORAbandonedCheckout AC ON O.Id = AC.OrderId


PRINT SYSDatetime()
print 'deleting CSCartItem'

Set @count =1
deleteCSCartItem:
DELETE TOP(10000) CI FROM CSCart C
INNER JOIN CSCartItem CI On C.Id =CI.CartId
INNER JOIN OROrder O on C.Id = O.CartId
INNER JOIN ORAbandonedCheckout AC ON O.Id=AC.OrderId

IF @@ROWCOUNT != 0
BEGIN
	PRINT cast(@count *10000 as varchar(10)) + ' deleted'
	PRINT SYSDatetime()
	SET @Count = @Count +1
	
    goto deleteCSCartItem
END



PRINT SYSDatetime()
print 'deleting OROrderItemAttriuteValue'
Set @count =1
deleteOROrderItemAttributeValue:
DELETE top (10000) OAV from OROrderItemAttributeValue OAV
INNER JOIN ORAbandonedCheckout AC ON OAV.OrderId = AC.OrderId

IF @@ROWCOUNT != 0
BEGIN
	PRINT cast(@count *10000 as varchar(10)) + ' deleted'
	PRINT SYSDatetime()
	SET @Count = @Count +1
	
    goto deleteOROrderItemAttributeValue
END



PRINT SYSDatetime()
print 'deleting OROrderattributeValue'
DELETE OAV from OROrderAttributeValue OAV
INNER JOIN ORAbandonedCheckout AC ON OAV.OrderId = AC.OrderId


PRINT SYSDatetime()
print 'deleting CPOrdercouponcode'
DELETE OP FROM CPOrderCouponCode OP
INNER JOIN ORAbandonedCheckout AC ON OP.OrderId = AC.OrderId


PRINT SYSDatetime()
print 'deleting FFOrderShipmentItem'
Set @count =1
deleteFFOrderShipmentItem:
DELETE top (10000) SI FROM FFOrderShipmentItem SI
INNER JOIN OROrderItem OI on SI.OrderItemId=OI.Id
INNER JOIN OROrder O on OI.OrderId= O.Id
INNER JOIN ORAbandonedCheckout AC ON O.Id = AC.OrderId

IF @@ROWCOUNT != 0
BEGIN
	PRINT cast(@count *10000 as varchar(10)) + ' deleted'
	PRINT SYSDatetime()
	SET @Count = @Count +1
	
    goto deleteFFOrderShipmentItem
END



 
PRINT SYSDatetime()
print 'deleting FFOrderShipmentPackage'
DELETE SP FROM FFOrderShipmentPackage SP
INNER JOIN FFOrderShipment OSP ON OSP.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS on OS.Id = OSP.OrderShippingId
INNER JOIN OROrder O on OS.OrderId= O.Id
INNER JOIN ORAbandonedCheckout AC ON O.Id = AC.OrderId



PRINT SYSDatetime()
print 'deleting FFOrderShipment'
DELETE OSP FROM FFOrderShipment OSP 
INNER JOIN OROrderShipping OS on OS.Id = OSP.OrderShippingId
INNER JOIN OROrder O on OS.OrderId= O.Id
INNER JOIN ORAbandonedCheckout AC ON O.Id = AC.OrderId

 
PRINT SYSDatetime()
print 'deleting OROrderItemCouponCode'
DELETE OIC FROM OROrderItemCouponCode OIC
INNER JOIN ORORderItem OI ON OIC.OrderItemId = OI.Id
INNER JOIN OROrder O on OI.OrderId= O.Id
INNER JOIN ORAbandonedCheckout AC ON O.Id = AC.OrderId


PRINT SYSDatetime()
print 'deleting dbo.OROrderItemCouponVersion'
DELETE OIC FROM dbo.OROrderItemCouponVersion OIC
INNER JOIN ORORderItem OI ON OIC.OrderItemId = OI.Id
INNER JOIN OROrder O on OI.OrderId= O.Id
INNER JOIN ORAbandonedCheckout AC ON O.Id = AC.OrderId


PRINT SYSDatetime()
print 'deleting dbo.ORMediaDownloadHistory'
DELETE OIC FROM dbo.ORMediaDownloadHistory OIC
INNER JOIN ORORderItem OI ON OIC.OrderItemId = OI.Id
INNER JOIN OROrder O on OI.OrderId= O.Id
INNER JOIN ORAbandonedCheckout AC ON O.Id = AC.OrderId



PRINT SYSDatetime()
print 'deleting OROrderItem'
Set @count =1
deleteOROrderItem:
DELETE top(10000) OI FROM OROrderItem OI
INNER JOIN OROrder O on OI.OrderId= O.Id
INNER JOIN ORAbandonedCheckout AC ON O.Id = AC.OrderId


IF @@ROWCOUNT != 0
BEGIN
	PRINT cast(@count *10000 as varchar(10)) + ' deleted'
	PRINT SYSDatetime()
	SET @Count = @Count +1
	
    goto deleteOROrderItem
END

 
PRINT SYSDatetime()
print 'deleting OROrderShipping'
DELETE OS FROM OROrderShipping OS
INNER JOIN OROrder O on OS.OrderId= O.Id
INNER JOIN ORAbandonedCheckout AC ON O.Id = AC.OrderId


PRINT SYSDatetime()
print 'deleting OROrder'
DELETE FROM ORORder 
Where Id in (Select OrderId FROM ORAbandonedCheckout)


PRINT SYSDatetime()
print 'deleting ORTaxLog'
DELETE TL FROM ORTaxLog TL
INNER JOIN ORAbandonedCheckout AC on TL.OrderId = AC.OrderId


PRINT SYSDatetime()
print 'deleting CSCartItem for non Orders'
Set @count =1
deleteCSCartItemExpired:

DELETE top (10000) CI FROM CSCart C
INNER JOIN CSCartItem CI On C.Id =CI.CartId
Where C.Id not in (Select CartId FROM ORORder)
AND C.ExpirationDate < DateAdd(day,-7,GETDATE())

IF @@ROWCOUNT != 0
BEGIN
	PRINT cast(@count *10000 as varchar(10)) + ' deleted'
	PRINT SYSDatetime()
	SET @Count = @Count +1
	
    goto deleteCSCartItemExpired
END


PRINT SYSDatetime()
print 'deleting CSCart for non Orders'
Set @count =1
deleteCSCartExpired:

DELETE top (10000) C FROM CSCart C
Where C.Id not in (Select CartId FROM ORORder)
AND C.ExpirationDate < DateAdd(day,-7,GETDATE())

IF @@ROWCOUNT != 0
BEGIN
	PRINT cast(@count *10000 as varchar(16)) + ' deleted'
	PRINT SYSDatetime()
	SET @Count = @Count +1
	
    goto deleteCSCartExpired
END


Set @count = 1
deleteVEVersion:
DELETE TOP(10000) V FROM VEVersion V WITH (ROWLOCK)
INNER JOIN ORAbandonedCheckout AC ON V.ObjectId=AC.OrderId
IF @@ROWCOUNT != 0
BEGIN
	PRINT cast(@count *10000 as varchar(50)) + ' Version records deleted'
	PRINT SYSDatetime()
	SET @Count = @Count +1
	if(@count <100)
	    goto deleteVEVersion
END



SET @dbName = db_name()
print 'Setting DB Recovery mode to FULL'
EXEC ('ALTER DATABASE [' +@dbName +'] SET RECOVERY FULL');

SELECT name, recovery_model_desc  
   FROM sys.databases  
      WHERE name = db_name(); 
-- Make sure Database Recovery is set to FULL, or we will not have any transactional logs.


END
GO
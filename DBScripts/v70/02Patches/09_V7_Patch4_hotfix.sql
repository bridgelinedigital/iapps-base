PRINT 'Modify stored procedure CampaignSendDto_Validate'
GO
IF(OBJECT_ID('CampaignSendDto_Validate') IS NOT NULL)
	DROP PROCEDURE CampaignSendDto_Validate
GO
CREATE PROCEDURE [dbo].[CampaignSendDto_Validate]
(
	@CampaignId		uniqueidentifier,
	@SiteId			uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @CampaignSendId uniqueidentifier, @ScheduleId uniqueidentifier, @ScheduleDate date
	SET @UtcNow = GETUTCDATE()

	SELECT TOP 1 @CampaignSendId = Id,
		@ScheduleId = ScheduleId
	FROM MKCampaignSend WHERE CampaignId = @CampaignId AND SiteId = @SiteId

	DECLARE @tbSites TABLE (Id uniqueidentifier)
	INSERT INTO @tbSites
	SELECT SiteId FROM vwCampaignSendTarget WHERE CampaignSendId = @CampaignSendId

	DECLARE @tbResult TABLE (SiteId uniqueidentifier, Reason int, SourceId uniqueidentifier)

	--ScheduledSites
	INSERT INTO @tbResult
	SELECT ST.SiteId, 1, S.SiteId FROM MKCampaignSend S
		JOIN vwCampaignSendTarget ST ON S.Id = ST.CampaignSendId
	WHERE (S.Status = 2 OR S.Status = 5)
		AND S.Id != @CampaignSendId AND S.CampaignId = @CampaignId
		AND EXISTS (SELECT 1 FROM @tbSites T WHERE T.Id = ST.SiteId)

	SELECT TOP 1 @ScheduleDate = dbo.ConvertTimeFromUtc(NextRunTime, ApplicationId) FROM TASchedule WHERE Id = @ScheduleId

	IF @ScheduleDate IS NOT NULL
	BEGIN
		--SendLimit

		;WITH RunCTE AS
		(
			SELECT R.Id, R.SiteId FROM MKCampaignRunHistory R
			WHERE EXISTS (SELECT 1 FROM @tbSites T WHERE T.Id = R.SiteId) 
				AND Status != 3 AND MONTH(RunDate) = MONTH(@ScheduleDate) AND YEAR(RunDate) = YEAR(@ScheduleDate)
		),
		RateCTE AS
		(
			SELECT SiteId, Count(Id) AS SendCount FROM RunCTE
			GROUP BY SiteId
		),
		CTE AS
		(
			SELECT SiteId, SendCount, [dbo].[CampaignSetting_Get](SiteId) AS CampaignSettingId FROM RateCTE
		)

		INSERT INTO @tbResult
		SELECT C.SiteId, 2, C.SiteId FROM CTE C
			JOIN MKCampaignSetting S ON C.CampaignSettingId = S.Id
		WHERE C.SendCount >= S.SendLimitPerMonth AND S.SendLimitPerMonth > 0

		--BlackOut dates

		INSERT INTO @tbResult
		SELECT S.Id, 3, T.Id FROM vwCampaignBlackoutTarget T
			JOIN @tbSites S ON T.SiteId = S.Id
			JOIN MKCampaignBlackout B ON B.Id = T.CampaignBlackoutId
		WHERE @ScheduleDate BETWEEN B.StartDate AND B.EndDate
	END
	
	SELECT @CampaignSendId AS CampaignSendId, T.* FROM @tbResult T

END
GO

IF(COL_LENGTH('PageMapNode', 'MasterPageMapNodeId') IS NULL)
	ALTER TABLE PageMapNode ADD MasterPageMapNodeId uniqueidentifier
GO

IF(COL_LENGTH('PageDefinition', 'MasterPageDefinitionId') IS NULL)
	ALTER TABLE PageDefinition ADD MasterPageDefinitionId uniqueidentifier
GO

UPDATE P SET P.MasterPageMapNodeId = ISNULL(S.SourcePageMapNodeId, P.PageMapNodeId) 
FROM PageMapNode P
	LEFT JOIN vwMasterSourceMenuId S ON S.PageMapNodeId = P.PageMapNodeId
WHERE P.MasterPageMapNodeId IS NULL
GO

UPDATE P SET P.MasterPageDefinitionId = ISNULL(S.SourcePageDefinitionId, P.PageDefinitionId)
FROM PageDefinition P
	LEFT JOIN vwMasterSourcePageId S ON S.PageDefinitionId = P.PageDefinitionId
WHERE P.MasterPageDefinitionId IS NULL
GO

IF(OBJECT_ID('vwObjectUrls_Menus') IS NOT NULL)
	DROP VIEW vwObjectUrls_Menus
GO	
CREATE VIEW [dbo].[vwObjectUrls_Menus] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS
SELECT	PMN.SiteId, 2, PMN.PageMapNodeId, PMN.MasterPageMapNodeId,
		LOWER(PMN.CompleteFriendlyUrl)
FROM	dbo.PageMapNode AS PMN
WHERE NULLIF(PMN.CompleteFriendlyUrl, '') IS NOT NULL
GO

IF(OBJECT_ID('vwObjectUrls_Pages') IS NOT NULL)
	DROP VIEW vwObjectUrls_Pages
GO	
CREATE VIEW [dbo].[vwObjectUrls_Pages] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS

SELECT	PD.SiteId, 8, PD.PageDefinitionId, PD.MasterPageDefinitionId,
		LOWER(PMN.CompleteFriendlyUrl + '/' + PD.FriendlyName)
FROM	dbo.PageDefinition AS PD
		INNER JOIN dbo.PageMapNodePageDef AS PMNPD ON PMNPD.PageDefinitionId = PD.PageDefinitionId
		INNER JOIN dbo.PageMapNode AS PMN ON PMN.PageMapNodeId = PMNPD.PageMapNodeId
GO
IF(OBJECT_ID('PageMapBase_UpdateLastModification') IS NOT NULL)
	DROP PROCEDURE  PageMapBase_UpdateLastModification
GO	
CREATE PROCEDURE [dbo].[PageMapBase_UpdateLastModification]
(
	@SiteId			uniqueidentifier = null
)
AS
BEGIN
	DECLARE @ModifiedDate datetime
	SET @ModifiedDate =  DATEADD(SECOND, DATEDIFF(SECOND, 39000, GETUTCDATE()), 39000)

	UPDATE PageMapNode SET CompleteFriendlyUrl = CalculatedUrl
	WHERE @SiteId IS NULL OR SiteId = @SiteId

	IF EXISTS (SELECT 1 FROM PageMapNode WHERE MasterPageMapNodeId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageMapNodeId = S.SourcePageMapNodeId 
		FROM PageMapNode P
			JOIN vwMasterSourceMenuId S ON S.PageMapNodeId = P.PageMapNodeId
		WHERE P.MasterPageMapNodeId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageMapNode SET MasterPageMapNodeId = PageMapNodeId 
		WHERE MasterPageMapNodeId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	IF EXISTS (SELECT 1 FROM PageDefinition WHERE MasterPageDefinitionId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageDefinitionId = S.SourcePageDefinitionId 
		FROM PageDefinition P
			JOIN vwMasterSourcePageId S ON S.PageDefinitionId = P.PageDefinitionId
		WHERE P.MasterPageDefinitionId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageDefinition SET MasterPageDefinitionId = PageDefinitionId 
		WHERE MasterPageDefinitionId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	UPDATE PageMapBase SET LastPageMapModificationDate = @ModifiedDate
	WHERE @SiteId IS NULL OR SiteId = @SiteId
END
GO
PRINT 'Creating vwContactAttribute'
GO
IF(OBJECT_ID('vwContactAttribute') IS NOT NULL)
	DROP VIEW vwContactAttribute
GO
CREATE VIEW [dbo].[vwContactAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	NULL AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	INNER JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 3

UNION ALL

SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	3 AS CategoryId,
	V.ContactId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	INNER JOIN ATContactAttributeValue V ON A.Id = V.AttributeId
GO
PRINT 'Modify stored procedure Membership_GetUserMembership'
GO
IF(OBJECT_ID('Membership_GetUserMembership') IS NOT NULL)
	DROP PROCEDURE Membership_GetUserMembership
GO
CREATE PROCEDURE [dbo].[Membership_GetUserMembership]
(
	@ProductId			uniqueidentifier = NULL,
	@ApplicationId		uniqueidentifier = NULL, --This is for getting applicationIds for the user and role.
	@UserId				uniqueidentifier,
	@RoleId				int = NULL,
	@ObjectTypeId		int,
	@ParentSiteId		uniqueidentifier = NULL,
	@OnlyParentSite		bit = NULL,
	@OnlySystemUser		bit = NULL,
	@OriginalMembership bit = NULL
)
AS
BEGIN
	DECLARE @tbSites AS SiteTableType 
	
	IF(@OnlyParentSite IS NULL) SET @OnlyParentSite = 0

	IF(@OriginalMembership = 1)
	BEGIN
		SELECT DISTINCT SiteId AS ObjectId
		FROM dbo.USSiteUser US
		WHERE ProductId = @ProductId
			AND UserId = @UserId
	END
	ELSE
	BEGIN
		--If Role Id is null then get all the sites in which the given user is having membership.
		IF (@RoleId IS NULL)
		BEGIN
			DECLARE @EmptyGuid uniqueidentifier
			SET @EmptyGuid = dbo.GetEmptyGUID()
	
			INSERT INTO @tbSites
			SELECT DISTINCT SiteId AS ObjectId
			FROM dbo.USSiteUser US
				JOIN SISite S ON US.SiteId = S.Id
			WHERE UserId = @UserId 
				AND (@ProductId IS NULL OR ProductId = @ProductId)
				AND (@OnlySystemUser IS NULL OR US.IsSystemUser = 1)
				AND ((@OnlyParentSite = 0 AND S.ParentSiteId = ISNULL(@ParentSiteId, S.ParentSiteId)) 
					OR (@OnlyParentSite = 1 AND S.ParentSiteId = @EmptyGuid))
			
			IF(@OnlyParentSite = 0)
				SELECT * FROM [dbo].[GetVariantSitesHavingPermission](@tbSites)
			ELSE	
				SELECT * FROM @tbSites
		END
		ELSE IF (@RoleId IS NOT NULL AND @UserId IS NOT NULL) --Gets all the sites in which the user is having the given role
		BEGIN
			INSERT INTO @tbSites
			SELECT DISTINCT SU.SiteId
			FROM USMemberRoles MR
				JOIN USMemberGroup MG ON MR.MemberId = MG.GroupId
				JOIN USSiteUser SU ON MG.MemberId = SU.UserId
			WHERE RoleId = @RoleId 
				AND SU.UserId = @UserId
				AND MG.ApplicationId = SU.SiteId  
				AND (@ObjectTypeId IS NULL OR MR.ObjectTypeId = @ObjectTypeId) 
				AND (@ProductId IS NULL OR SU.ProductId = @ProductId) 
				AND (@OnlySystemUser IS NULL OR SU.IsSystemUser = 1)
				
			SELECT * FROM [dbo].[GetVariantSitesHavingPermission](@tbSites)
		END
	END
END
GO
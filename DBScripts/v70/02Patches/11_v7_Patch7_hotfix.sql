IF (OBJECT_ID('vwPageAttributeValue') IS NOT NULL)
	DROP VIEW vwPageAttributeValue
GO
CREATE VIEW [dbo].[vwPageAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V

	UNION ALL

	SELECT ROW_NUMBER() over (PARTITION BY V.AttributeId, C.PageDefinitionId ORDER BY S.LftValue DESC) AS vRank,
		V.Id,
		C.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V
		JOIN PageDefinition P ON V.PageDefinitionId = P.PageDefinitionId
		JOIN PageDefinition C ON C.SourcePageDefinitionId = P.PageDefinitionId
		JOIN SISite S ON S.Id = P.SiteId
	WHERE V.IsShared = 1
		AND S.DistributionEnabled = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT C.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	ISNULL(V.Notes, C.Notes) AS Notes,
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATPageAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.PageDefinitionId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1
GO
IF (OBJECT_ID('MenuDto_Import') IS NOT NULL)
	DROP PROCEDURE MenuDto_Import
GO
CREATE PROCEDURE [dbo].[MenuDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@IgnoreExisting			bit = NULL,
	@InvalidateCache		bit = NULL,
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @SourceParentId uniqueidentifier, @ParentId uniqueidentifier, @LftValue int, @RgtValue int

	SET @UtcNow = GETUTCDATE()
	SET @Id = (SELECT [dbo].[Menu_GetVariantId](@SourceId, @TargetSiteId))

	IF @Id IS NOT NULL AND @IgnoreExisting = 1
		RETURN

	IF @Id IS NULL
	BEGIN
		SET @SourceParentId = (SELECT TOP 1 ParentId FROM PageMapNode WHERE PageMapNodeId = @SourceId)
		SET @ParentId = (SELECT [dbo].[Menu_GetVariantId](@SourceParentId, @TargetSiteId))
		IF (@ParentId IS NOT NULL)
		BEGIN
			SET @Id = NEWID()

			SELECT @LftValue = LftValue, @RgtValue = RgtValue FROM PageMapNode
			WHERE PageMapNodeId = @ParentId

			UPDATE PageMapNode SET  
				LftValue = CASE WHEN LftValue > @RgtValue THEN LftValue + 2 ELSE LftValue END,
				RgtValue = CASE WHEN RgtValue >= @RgtValue THEN RgtValue + 2 ELSE RgtValue END 
			WHERE SiteId = @TargetSiteId

			INSERT INTO [dbo].[PageMapNode]
			(
				[PageMapNodeId],
				[ParentId],
				[LftValue],
				[RgtValue],
				[SiteId],
				[ExcludeFromSiteMap],
				[Description],
				[DisplayTitle],
				[FriendlyUrl],
				[MenuStatus],
				[TargetId],
				[Target],
				[TargetUrl],
				[CreatedBy],
				[CreatedDate],
				[ModifiedBy],
				[ModifiedDate],
				[PropogateWorkflow],
				[InheritWorkflow],
				[PropogateSecurityLevels],
				[InheritSecurityLevels],
				[PropogateRoles],
				[InheritRoles],
				[Roles],
				[LocationIdentifier],
				[HasRolloverImages],
				[RolloverOnImage],
				[RolloverOffImage],
				[IsCommerceNav],
				[AssociatedQueryId],
				[DefaultContentId],
				[AssociatedContentFolderId],
				[CustomAttributes],
				[HiddenFromNavigation],
				[CssClass],
				[SourcePageMapNodeId],
				[DisplayOrder]
			)
			SELECT
				@Id,
				@ParentId,
				@RgtValue,
				@RgtValue + 1,
				@TargetSiteId,
				[ExcludeFromSiteMap],
				[Description],
				[DisplayTitle],
				[FriendlyUrl],
				[MenuStatus],
				[TargetId],
				[Target],
				[TargetUrl],
				ISNULL([ModifiedBy], [CreatedBy]),
				@UtcNow,
				[ModifiedBy],
				[ModifiedDate],
				[PropogateWorkflow],
				[InheritWorkflow],
				[PropogateSecurityLevels],
				[InheritSecurityLevels],
				[PropogateRoles],
				[InheritRoles],
				[Roles],
				[LocationIdentifier],
				[HasRolloverImages],
				[RolloverOnImage],
				[RolloverOffImage],
				[IsCommerceNav],
				[AssociatedQueryId],
				[DefaultContentId],
				[AssociatedContentFolderId],
				[CustomAttributes],
				[HiddenFromNavigation],
				[CssClass],
				PageMapNodeId,
				DisplayOrder
			FROM PageMapNode
			WHERE PageMapNodeId = @SourceId
		END
	END
	ELSE
	BEGIN
		UPDATE C SET
		  C.[ExcludeFromSiteMap] = P.ExcludeFromSiteMap,
		  C.[Description] = P.Description,
		  C.[DisplayTitle] = P.DisplayTitle,
		  C.[FriendlyUrl] = P.FriendlyUrl,
		  C.[MenuStatus] = P.MenuStatus,
		  C.[ModifiedBy] = P.ModifiedBy,
		  C.[ModifiedDate] = @UtcNow,
		  C.[PropogateWorkflow] = P.PropogateWorkflow,
		  C.[InheritWorkflow] = P.InheritWorkflow,
		  C.[PropogateSecurityLevels] = P.PropogateSecurityLevels,
		  C.[InheritSecurityLevels] = P.InheritSecurityLevels,
		  C.[PropogateRoles] = P.PropogateRoles,
		  C.[InheritRoles] = P.InheritRoles,
		  C.[LocationIdentifier] = P.LocationIdentifier,
		  C.[HasRolloverImages] = P.HasRolloverImages,
		  C.[RolloverOnImage] = P.RolloverOnImage,
		  C.[RolloverOffImage] = P.RolloverOffImage,
		  C.[HiddenFromNavigation] = P.HiddenFromNavigation,
		  C.[CssClass] = P.CssClass,
		  C.TargetId = P.TargetId,
		  C.Target = P.Target,
		  C.TargetUrl = P.TargetUrl,
		  C.DisplayOrder = P.DisplayOrder
		FROM PageMapNode P, PageMapNode C
		WHERE P.PageMapNodeId = @SourceId
			AND C.PageMapNodeId = @Id
	END

	IF @Id IS NOT NULL
	BEGIN
		UPDATE P
		SET P.TargetId = CP.PageDefinitionId 
		FROM PageMapNode P
			JOIN PageDefinition PP ON P.TargetId = PP.PageDefinitionId
			JOIN PageDefinition CP ON CP.MasterPageDefinitionId = PP.MasterPageDefinitionId
		WHERE P.Target = 1 AND P.PageMapNodeId = @Id AND CP.SiteId = @TargetSiteId

		UPDATE P
		SET P.TargetId = CP.PageMapNodeId 
		FROM PageMapNode P
			JOIN PageMapNode PP ON P.TargetId = PP.PageMapNodeId
			JOIN PageMapNode CP ON CP.MasterPageMapNodeId = PP.MasterPageMapNodeId
		WHERE P.Target = 2 AND P.PageMapNodeId = @Id AND CP.SiteId = @TargetSiteId

		IF EXISTS (SELECT 1 FROM PageMapBase WHERE HomePageDefinitionId = @SourceId)
		BEGIN
			UPDATE PageMapBase SET HomePageDefinitionId = @Id WHERE SiteId = @TargetSiteId
		END
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO USSecurityLevelObject
	SELECT @Id, 2, SecurityLevelId FROM USSecurityLevelObject
	WHERE ObjectId = @SourceId

	IF @InvalidateCache = 1
	BEGIN
		EXEC PageDto_Invalidate @SiteId = @TargetSiteId, @ModifiedBy = @ModifiedBy
	END
END
GO
PRINT 'Add Site Settings'
DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int

SET @SettingGroupId = 1
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Library.AllowBulkAction')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Library.AllowBulkAction', 'Allow Bulk action for Pages, Contents, Files & Images',  @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, 'false' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
GO
PRINT 'Creating Function Menu_GetVariantId'
GO
IF (OBJECT_ID('Menu_GetVariantId') IS NOT NULL)
	DROP FUNCTION Menu_GetVariantId
GO
CREATE FUNCTION [dbo].[Menu_GetVariantId]
(
	@SourceMenuId	uniqueidentifier,
	@TargetSiteId	uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @MenuId uniqueidentifier, @MasterMenuId uniqueidentifier, @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF EXISTS(SELECT 1 FROM PageMapNode WHERE PageMapNodeId = SiteId AND PageMapNodeId = @SourceMenuId)
	BEGIN
		SET @MenuId = @TargetSiteId
	END
	ELSE
	BEGIN
		SELECT TOP 1 @MenuId = PageMapNodeId FROM PageMapNode
		WHERE SourcePageMapNodeId = @SourceMenuId AND SiteId = @TargetSiteId

		IF @MenuId IS NULL
		BEGIN
			SELECT TOP 1 @MasterMenuId = PageMapNodeId FROM PageMapNode
			WHERE PageMapNodeId = @SourceMenuId

			SELECT TOP 1 @MenuId = PageMapNodeId FROM PageMapNode
			WHERE MasterPageMapNodeId = @MasterMenuId AND SiteId = @TargetSiteId
		END
	END

	RETURN @MenuId
END
GO
PRINT 'Creating Function Page_GetVariantId'
GO
IF (OBJECT_ID('Page_GetVariantId') IS NOT NULL)
	DROP FUNCTION Page_GetVariantId
GO
CREATE FUNCTION [dbo].[Page_GetVariantId]
(
	@SourcePageId	uniqueidentifier,
	@TargetSiteId	uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @PageId uniqueidentifier, @MasterPageId uniqueidentifier, @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	SELECT TOP 1 @PageId = PageDefinitionId FROM PageDefinition
	WHERE SourcePageDefinitionId = @SourcePageId AND SiteId = @TargetSiteId

	IF @PageId IS NULL
	BEGIN
		SELECT TOP 1 @MasterPageId = PageDefinitionId FROM PageDefinition
		WHERE PageDefinitionId = @SourcePageId

		SELECT TOP 1 @PageId = PageDefinitionId FROM PageDefinition
		WHERE MasterPageDefinitionId = @MasterPageId AND SiteId = @TargetSiteId
	END

	RETURN @PageId
END
GO
PRINT 'Create procedure DistributionDto_Synchronize'
GO
IF (OBJECT_ID('DistributionDto_Synchronize') IS NOT NULL)
	DROP PROCEDURE DistributionDto_Synchronize
GO
CREATE PROCEDURE [dbo].[DistributionDto_Synchronize]
(
	@SiteId			uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON

	IF NOT EXISTS (SELECT 1 FROM SISite WHERE Id = @SiteId AND AllowSynchronization = 1)
	BEGIN
        DECLARE @ErrMsg nvarchar(256)
		SET	@ErrMsg = CAST(@SiteId as nvarchar(max))
		RAISERROR ('NOTSUPPORTED|Synchronization|%s', 16, 1, @ErrMsg)
		RETURN
    END

	DECLARE @SourceSiteId uniqueidentifier, @ObjectId uniqueidentifier, @ObjectTypeId int, @ActionId int
	DECLARE @tbSource TABLE (Id uniqueidentifier, MasterId uniqueidentifier, ObjectTypeId int, DisplayOrder int)
	DECLARE @tbDistribution TABLE (SNo int identity(1,1), ObjectId uniqueidentifier, ObjectTypeId int, ActionId int)

	SELECT TOP 1 @SourceSiteId = ParentSiteId FROM SISite WHERE Id = @SiteId

	INSERT INTO @tbSource
	SELECT P.PageDefinitionId, P.MasterPageDefinitionId, 8, PD.DisplayOrder
	FROM PageDefinition P
		JOIN PageMapNodePageDef PD ON PD.PageDefinitionId = P.PageDefinitionId
		JOIN GLDistribution D ON D.ObjectId = P.PageDefinitionId
		JOIN GLDistributionTarget T ON T.DistributionId = D.Id
		JOIN SISite S ON D.SiteId = S.Id
		LEFT JOIN SISiteListSite SL ON T.TargetId = SL.SiteListId
	WHERE P.SiteId = @SourceSiteId
		AND (SL.SiteId = @SiteId OR T.TargetTypeId = 3)
		AND S.DistributionEnabled = 1


	INSERT INTO @tbSource
	SELECT P.PageMapNodeId, P.MasterPageMapNodeId, 2, P.LftValue
	FROM PageMapNode P
		JOIN GLDistribution D ON D.ObjectId = P.PageMapNodeId
		JOIN GLDistributionTarget T ON T.DistributionId = D.Id
		JOIN SISite S ON D.SiteId = S.Id
		LEFT JOIN SISiteListSite SL ON T.TargetId = SL.SiteListId
	WHERE P.SiteId = @SourceSiteId
		AND (SL.SiteId = @SiteId OR T.TargetTypeId = 3)
		AND S.DistributionEnabled = 1

	-- Delete Page
	INSERT INTO @tbDistribution
	SELECT P.PageDefinitionId, 8, 3 FROM PageDefinition P
	WHERE SiteId = @SiteId
		AND MasterPageDefinitionId NOT IN (SELECT MasterId FROM @tbSource WHERE ObjectTypeId = 8)

	-- Delete Menu
	INSERT INTO @tbDistribution
	SELECT P.PageMapNodeId, 2, 3 FROM PageMapNode P 
	WHERE SiteId = @SiteId
		AND MasterPageMapNodeId NOT IN (SELECT MasterId FROM @tbSource WHERE ObjectTypeId = 2)
		AND PageMapNodeId != SiteId
		AND (LocationIdentifier IS NULL OR LocationIdentifier = '')
	ORDER BY LftValue DESC

	-- Add Menu
	INSERT INTO @tbDistribution
	SELECT Id, 2, 1 FROM @tbSource T
	WHERE MasterId NOT IN (SELECT MasterPageMapNodeId FROM PageMapNode WHERE SiteId = @SiteId)
		AND ObjectTypeId = 2
	ORDER BY DisplayOrder

	-- Add Page
	INSERT INTO @tbDistribution
	SELECT Id, 8, 1 FROM @tbSource T
	WHERE MasterId NOT IN (SELECT MasterPageDefinitionId FROM PageDefinition WHERE SiteId = @SiteId)
		AND ObjectTypeId = 8 
	ORDER BY DisplayOrder

	-- Update Menu
	INSERT INTO @tbDistribution
	SELECT Id, 2, 2 FROM @tbSource T
	WHERE MasterId IN (SELECT MasterPageMapNodeId FROM PageMapNode WHERE SiteId = @SiteId)
		AND ObjectTypeId = 2

	-- Update Page
	INSERT INTO @tbDistribution
	SELECT Id, 8, 2 FROM @tbSource T
	WHERE MasterId IN (SELECT MasterPageDefinitionId FROM PageDefinition WHERE SiteId = @SiteId)
		AND ObjectTypeId = 8

	DECLARE @tbExcludedMenus TABLE (Id uniqueidentifier primary key)
	-- This needs to use new 5.4 column 'Allow access in child site'
	INSERT INTO @tbExcludedMenus
	SELECT DISTINCT C.PageMapNodeId FROM PageMapNode P
		JOIN PageMapNode C ON C.LftValue >= P.LftValue AND C.RgtValue <= P.RgtValue AND C.SiteId = P.SiteId
	WHERE P.SiteId = @SiteId AND P.LocationIdentifier like 'MarketierPages'

	DELETE D FROM @tbDistribution D
	WHERE D.ObjectTypeId = 2 AND EXISTS (SELECT 1 FROM @tbExcludedMenus E WHERE E.Id = D.ObjectId)

	DELETE D FROM @tbDistribution D
		JOIN PageMapNodePageDef PD ON D.ObjectId = PD.PageDefinitionId
	WHERE D.ObjectTypeId = 8 AND EXISTS (SELECT 1 FROM @tbExcludedMenus E WHERE E.Id = PD.PageMapNodeId)

	DECLARE Distribution_Cursor CURSOR FOR SELECT D.ObjectId FROM @tbDistribution D ORDER BY SNo
	OPEN Distribution_Cursor 
	FETCH NEXT FROM Distribution_Cursor INTO @ObjectId

	WHILE (@@FETCH_STATUS != -1)
	BEGIN
		SELECT @ObjectTypeId = ObjectTypeId, @ActionId = ActionId 
			FROM @tbDistribution WHERE ObjectId = @ObjectId

		PRINT 'Object Id: ' + CAST(@ObjectId AS varchar(36)) + ' Object Type Id: ' + CAST(@ObjectTypeId AS nvarchar(5)) + ' Action Id: ' + CAST(@ActionId AS nvarchar(5))

		IF @ObjectTypeId = 2
		BEGIN
			IF @ActionId = 3
				EXEC MenuDto_Delete @Id = @ObjectId, @SiteId = @SiteId,
					@ModifiedBy = @ModifiedBy
			ELSE IF @ActionId = 1
				EXEC MenuDto_Import @SourceId = @ObjectId, 
					@TargetSiteId = @SiteId, @ModifiedBy = @ModifiedBy
		END
		ELSE
		BEGIN
			IF @ActionId = 3
				EXEC PageDto_Delete @Id = @ObjectId, @SiteId = @SiteId,
					@ModifiedBy = @ModifiedBy
			ELSE IF @ActionId = 1
				EXEC PageDto_Import @SourceId = @ObjectId, 
					@TargetSiteId = @SiteId, @ModifiedBy = @ModifiedBy
		END

		FETCH NEXT FROM Distribution_Cursor INTO @ObjectId 	
	END

	CLOSE Distribution_Cursor
	DEALLOCATE Distribution_Cursor	

	EXEC PageMapBase_UpdateLastModification @SiteId = @SiteId
	
	SELECT * FROM @tbDistribution T ORDER BY SNo
END
GO
PRINT 'Create procedure MenuDto_Import'
GO
IF (OBJECT_ID('MenuDto_Import') IS NOT NULL)
	DROP PROCEDURE MenuDto_Import
GO
CREATE PROCEDURE [dbo].[MenuDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@IgnoreExisting			bit = NULL,
	@InvalidateCache		bit = NULL,
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @SourceParentId uniqueidentifier, @ParentId uniqueidentifier, @LftValue int, @RgtValue int

	SET @UtcNow = GETUTCDATE()
	SET @Id = (SELECT [dbo].[Menu_GetVariantId](@SourceId, @TargetSiteId))

	IF @Id IS NOT NULL AND @IgnoreExisting = 1
		RETURN

	IF @Id IS NULL
	BEGIN
		SET @SourceParentId = (SELECT TOP 1 ParentId FROM PageMapNode WHERE PageMapNodeId = @SourceId)
		SET @ParentId = (SELECT [dbo].[Menu_GetVariantId](@SourceParentId, @TargetSiteId))
		IF (@ParentId IS NOT NULL)
		BEGIN
			SET @Id = NEWID()

			SELECT @LftValue = LftValue, @RgtValue = RgtValue FROM PageMapNode
			WHERE PageMapNodeId = @ParentId

			UPDATE PageMapNode SET  
				LftValue = CASE WHEN LftValue > @RgtValue THEN LftValue + 2 ELSE LftValue END,
				RgtValue = CASE WHEN RgtValue >= @RgtValue THEN RgtValue + 2 ELSE RgtValue END 
			WHERE SiteId = @TargetSiteId

			INSERT INTO [dbo].[PageMapNode]
			(
				[PageMapNodeId],
				[ParentId],
				[LftValue],
				[RgtValue],
				[SiteId],
				[ExcludeFromSiteMap],
				[Description],
				[DisplayTitle],
				[FriendlyUrl],
				[MenuStatus],
				[TargetId],
				[Target],
				[TargetUrl],
				[CreatedBy],
				[CreatedDate],
				[ModifiedBy],
				[ModifiedDate],
				[PropogateWorkflow],
				[InheritWorkflow],
				[PropogateSecurityLevels],
				[InheritSecurityLevels],
				[PropogateRoles],
				[InheritRoles],
				[Roles],
				[LocationIdentifier],
				[HasRolloverImages],
				[RolloverOnImage],
				[RolloverOffImage],
				[IsCommerceNav],
				[AssociatedQueryId],
				[DefaultContentId],
				[AssociatedContentFolderId],
				[CustomAttributes],
				[HiddenFromNavigation],
				[CssClass],
				[SourcePageMapNodeId],
				[MasterPageMapNodeId],
				[DisplayOrder]
			)
			SELECT
				@Id,
				@ParentId,
				@RgtValue,
				@RgtValue + 1,
				@TargetSiteId,
				[ExcludeFromSiteMap],
				[Description],
				[DisplayTitle],
				[FriendlyUrl],
				[MenuStatus],
				[TargetId],
				[Target],
				[TargetUrl],
				ISNULL([ModifiedBy], [CreatedBy]),
				@UtcNow,
				[ModifiedBy],
				[ModifiedDate],
				[PropogateWorkflow],
				[InheritWorkflow],
				[PropogateSecurityLevels],
				[InheritSecurityLevels],
				[PropogateRoles],
				[InheritRoles],
				[Roles],
				[LocationIdentifier],
				[HasRolloverImages],
				[RolloverOnImage],
				[RolloverOffImage],
				[IsCommerceNav],
				[AssociatedQueryId],
				[DefaultContentId],
				[AssociatedContentFolderId],
				[CustomAttributes],
				[HiddenFromNavigation],
				[CssClass],
				PageMapNodeId,
				ISNULL(MasterPageMapNodeId, PageMapNodeId),
				DisplayOrder
			FROM PageMapNode
			WHERE PageMapNodeId = @SourceId
		END
	END
	ELSE
	BEGIN
		UPDATE C SET
		  C.[ExcludeFromSiteMap] = P.ExcludeFromSiteMap,
		  C.[Description] = P.Description,
		  C.[DisplayTitle] = P.DisplayTitle,
		  C.[FriendlyUrl] = P.FriendlyUrl,
		  C.[MenuStatus] = P.MenuStatus,
		  C.[ModifiedBy] = P.ModifiedBy,
		  C.[ModifiedDate] = @UtcNow,
		  C.[PropogateWorkflow] = P.PropogateWorkflow,
		  C.[InheritWorkflow] = P.InheritWorkflow,
		  C.[PropogateSecurityLevels] = P.PropogateSecurityLevels,
		  C.[InheritSecurityLevels] = P.InheritSecurityLevels,
		  C.[PropogateRoles] = P.PropogateRoles,
		  C.[InheritRoles] = P.InheritRoles,
		  C.[LocationIdentifier] = P.LocationIdentifier,
		  C.[HasRolloverImages] = P.HasRolloverImages,
		  C.[RolloverOnImage] = P.RolloverOnImage,
		  C.[RolloverOffImage] = P.RolloverOffImage,
		  C.[HiddenFromNavigation] = P.HiddenFromNavigation,
		  C.[CssClass] = P.CssClass,
		  C.TargetId = P.TargetId,
		  C.Target = P.Target,
		  C.TargetUrl = P.TargetUrl,
		  C.DisplayOrder = P.DisplayOrder
		FROM PageMapNode P, PageMapNode C
		WHERE P.PageMapNodeId = @SourceId
			AND C.PageMapNodeId = @Id
	END

	IF @Id IS NOT NULL
	BEGIN
		UPDATE P
		SET P.TargetId = CP.PageDefinitionId 
		FROM PageMapNode P
			JOIN PageDefinition PP ON P.TargetId = PP.PageDefinitionId
			JOIN PageDefinition CP ON CP.MasterPageDefinitionId = PP.MasterPageDefinitionId
		WHERE P.Target = 1 AND P.PageMapNodeId = @Id AND CP.SiteId = @TargetSiteId

		UPDATE P
		SET P.TargetId = CP.PageMapNodeId 
		FROM PageMapNode P
			JOIN PageMapNode PP ON P.TargetId = PP.PageMapNodeId
			JOIN PageMapNode CP ON CP.MasterPageMapNodeId = PP.MasterPageMapNodeId
		WHERE P.Target = 2 AND P.PageMapNodeId = @Id AND CP.SiteId = @TargetSiteId

		IF EXISTS (SELECT 1 FROM PageMapBase WHERE HomePageDefinitionId = @SourceId)
		BEGIN
			UPDATE PageMapBase SET HomePageDefinitionId = @Id WHERE SiteId = @TargetSiteId
		END
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO USSecurityLevelObject
	SELECT @Id, 2, SecurityLevelId FROM USSecurityLevelObject
	WHERE ObjectId = @SourceId

	IF @InvalidateCache = 1
	BEGIN
		EXEC PageDto_Invalidate @SiteId = @TargetSiteId, @ModifiedBy = @ModifiedBy
	END
END
GO
PRINT 'Create procedure PageDto_Import'
GO
IF (OBJECT_ID('PageDto_Import') IS NOT NULL)
	DROP PROCEDURE PageDto_Import
GO
CREATE PROCEDURE [dbo].[PageDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@PageMapNodeId			uniqueidentifier = NULL OUTPUT,
	@Publish				bit = NULL,	
	@Overwrite				bit = NULL,		
	@ModifiedBy				uniqueidentifier,
	@PreserveVariantContent bit = 1,
	@IgnoreExisting			bit = NULL,
	@InvalidateCache		bit = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier, @DisplayOrder int

	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @Id = [dbo].[Page_GetVariantId](@SourceId, @TargetSiteId)

	IF @PageMapNodeId IS NULL
	BEGIN
		DECLARE @ParentPageMapNode uniqueidentifier
		SELECT TOP 1 @ParentPageMapNode = PageMapNodeId, @DisplayOrder = DisplayOrder FROM PageMapNodePageDef 
			WHERE PageDefinitionId = @SourceId
		SET @PageMapNodeId = [dbo].[Menu_GetVariantId](@ParentPageMapNode, @TargetSiteId)
	END

	IF @PageMapNodeId IS NULL OR (@Id IS NOT NULL AND @IgnoreExisting = 1)
		RETURN

	SELECT @ModifiedBy = ModifiedBy FROM PageDefinition WHERE PageDefinitionId = @SourceId 
	IF @Publish IS NULL AND EXISTS (SELECT 1 FROM PageDefinition WHERE PageDefinitionId = @SourceId AND PageStatus = 8)
		SET @Publish = 1

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()
		INSERT INTO [dbo].[PageDefinition]
		(
			[PageDefinitionId],
			[TemplateId],
			[SiteId],
			[Title],
			[Description],
			[PageStatus],
			[WorkflowState],
			[PublishCount],
			[PublishDate],
			[FriendlyName],
			[WorkflowId],
			[CreatedBy],
			[CreatedDate],
			[ModifiedBy],
			[ModifiedDate],
			[ArchivedDate],
			[StatusChangedDate],
			[AuthorId],
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			[SourcePageDefinitionId],
			[MasterPageDefinitionId],
			[CustomAttributes]
		)
		SELECT @Id,
			[TemplateId],
			@TargetSiteId,
			[Title],
			D.[Description],
			CASE WHEN @Publish = 1 THEN 8 ELSE PageStatus END,
			1,
			0,
			'1900-01-01 00:00:00.000',
			[FriendlyName],
			@EmptyGuid,
			@ModifiedBy,
			@UtcNow,
			@ModifiedBy,
			@UtcNow,
			[ArchivedDate],
			[StatusChangedDate],
			@ModifiedBy,
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			PageDefinitionId,
			ISNULL(MasterPageDefinitionId, PageDefinitionId),
			D.[CustomAttributes]
		FROM [dbo].[PageDefinition] D
		WHERE PageDefinitionId = @SourceId				

		UPDATE PageMapNode SET TargetId = @Id
		WHERE TargetId = @SourceId AND SiteId = @TargetSiteId
		 
		INSERT INTO [dbo].[PageDefinitionSegment]
		(
			[Id],
			[PageDefinitionId],
			[DeviceId],
			[AudienceSegmentId],
			[UnmanagedXml],
			[ZonesXml]
		)
		SELECT NEWID(), 
			@Id,
			[DeviceId],
			[AudienceSegmentId],
			dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
			[ZonesXml]
		FROM [dbo].[PageDefinitionSegment] PS 
		WHERE PageDefinitionId = @SourceId

		INSERT INTO [dbo].[PageDetails]
		(
			[PageId],
			[PageDetailXML]
		)
		SELECT 
			@Id,
			[PageDetailXML]
		FROM [dbo].[PageDetails] PD
		WHERE PD.PageId = @SourceId
	END
	ELSE
	BEGIN
		UPDATE C SET
			C.Title = P.Title,
			C.TemplateId = P.TemplateId,
			C.InWFTemplateId = P.InWFTemplateId,
			C.Description = P.Description,
			C.PageStatus = CASE WHEN @Publish = 1 THEN 8 ELSE P.PageStatus END,
			C.FriendlyName = CASE WHEN @PreserveVariantContent = 0 THEN P.FriendlyName ELSE C.FriendlyName END,
			C.EnableOutputCache = P.EnableOutputCache,
			C.OutputCacheProfileName = P.OutputCacheProfileName,
			C.ExcludeFromSearch = P.ExcludeFromSearch,
			C.HasForms = P.HasForms,
			C.ScheduledPublishDate = P.ScheduledPublishDate,
			C.ScheduledArchiveDate = P.ScheduledArchiveDate,
			C.ModifiedDate = @UtcNow,
			C.ModifiedBy = P.ModifiedBy
		FROM PageDefinition P, PageDefinition C
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.UnmanagedXml = dbo.Page_GetUnmanagedXml(P.[UnmanagedXml], @TargetSiteId),
			C.ZonesXml = P.ZonesXml
		FROM PageDefinitionSegment P
			JOIN PageDefinitionSegment C ON P.DeviceId = C.DeviceId AND P.AudienceSegmentId = C.AudienceSegmentId
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.ContentId = P.ContentId,
			C.InWFContentId = P.InWFContentId,
			C.ContentTypeId = P.ContentTypeId,
			C.IsModified = P.IsModified,
			C.[IsTracked] = P.[IsTracked],
			C.[Visible] = P.[Visible],
			C.[InWFVisible] = P.[InWFVisible],
			C.[CustomAttributes] = P.[CustomAttributes],
			C.[DisplayOrder] = P.[DisplayOrder],
			C.[InWFDisplayOrder] = P.[InWFDisplayOrder]
		FROM PageDefinitionContainer P
			JOIN PageDefinitionContainer C ON P.ContainerName = C.ContainerName
			LEFT JOIN COContent CO ON C.ContentId = CO.Id
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId
			AND (@Overwrite = 1 OR CO.Id IS NULL OR CO.ApplicationId != @TargetSiteId)
		
		IF @PreserveVariantContent = 0
		BEGIN
			UPDATE C 
			SET	C.PageDetailXML = P.PageDetailXML
			FROM PageDetails P, PageDetails C
			WHERE C.PageId = @Id AND P.PageId = @SourceId	
		END

		DELETE FROM PageMapNodePageDef WHERE PageDefinitionId = @Id
	END

	INSERT INTO PageMapNodePageDef
	(
		PageDefinitionId,
		PageMapNodeId,
		DisplayOrder
	)
	SELECT
		@Id,
		@PageMapNodeId,
		@DisplayOrder

	INSERT INTO [PageDefinitionContainer]
	(
		[Id],
		PageDefinitionSegmentId ,
		[PageDefinitionId],
		[ContainerId],
		[ContentId],
		[InWFContentId],
		[ContentTypeId],
		[IsModified],
		[IsTracked],
		[Visible],
		[InWFVisible],
		DisplayOrder,
		InWFDisplayOrder,
		[CustomAttributes],
		[ContainerName]
	)
	SELECT NEWID(),
		CS.Id,
		@Id,
		ContainerId,
		ContentId,
		InWFContentId,
		ContentTypeId,
		IsModified,
		PC.IsTracked,
		Visible,
		InWFVisible,
		PC.DisplayOrder, 
		PC.InWFDisplayOrder, 
		PC.CustomAttributes,
		ContainerName
	FROM PageDefinitionContainer PC
		JOIN PageDefinitionSegment PS ON PC.PageDefinitionSegmentId = PS.Id
		JOIN PageDefinitionSegment CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
	WHERE PC.PageDefinitionId = @SourceId
		AND (@Id IS NULL OR ContainerName NOT IN (SELECT ContainerName FROM PageDefinitionContainer WHERE PageDefinitionId = @Id))

	DECLARE @SourceTargetMenuId uniqueidentifier
	SELECT TOP 1 @SourceTargetMenuId = PageMapNodeId FROM PageMapNode PN
		JOIN PageDefinition PD ON PN.TargetId = PD.PageDefinitionId AND PD.SiteId = PN.SiteId
	WHERE PD.PageDefinitionId = @SourceId

	IF @SourceTargetMenuId IS NOT NULL
	BEGIN
		UPDATE PageMapNode SET
			TargetId = @Id,
			Target = 1,
			TargetUrl = ''
		WHERE SourcePageMapNodeId = @SourceTargetMenuId
			AND SiteId = @TargetSiteId
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[USSecurityLevelObject]
	(
		[ObjectId],
		[ObjectTypeId],
		[SecurityLevelId]
	)
	SELECT @Id,
		ObjectTypeId,
		SecurityLevelId
	FROM [USSecurityLevelObject] S
	WHERE S.ObjectId = @SourceId

	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageScript]
	(
		[PageDefinitionId],
		[ScriptId],
		[CustomAttributes]
	)
	SELECT @Id,
		[ScriptId],
		[CustomAttributes]
	FROM [dbo].[SIPageScript] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageStyle]
	(
		[PageDefinitionId],
		[StyleId],
		[CustomAttributes]
	)
	SELECT @Id,
		[StyleId],
		[CustomAttributes]
	FROM [dbo].[SIPageStyle] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM COTaxonomyObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[COTaxonomyObject]
	(
		[Id],
		[TaxonomyId],
		[ObjectId],
		[ObjectTypeId],
		[SortOrder],
		[Status],
		[CreatedBy],
		[CreatedDate],
		[ModifiedBy],
		[ModifiedDate]
	)
	SELECT NEWID(),
		[TaxonomyId],
		@Id,
		[ObjectTypeId],
		[SortOrder],
		[Status],
		@ModifiedBy,
		@UtcNow,
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[COTaxonomyObject] OT
	WHERE OT.ObjectId = @SourceId

	DECLARE @VersionId uniqueidentifier, @SourceVersionId uniqueidentifier, 
		@VersionNumber int, @RevisionNumber int, @VersionStatus int
	SET @VersionId = NEWID()
	SELECT TOP 1 @SourceVersionId = Id FROM VEVersion 
	WHERE ObjectId = @SourceId ORDER BY CreatedDate DESC	

	SELECT TOP 1 @VersionNumber = VersionNumber, @RevisionNumber = RevisionNumber FROM VEVersion
	WHERE ObjectId = @Id ORDER BY CreatedDate DESC

	IF @Publish = 1
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0) + 1
		SET @VersionNumber = 0
		SET @VersionStatus = 1
	END
	ELSE
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0)
		SET @VersionNumber = ISNULL(@VersionNumber, 0) + 1
		SET @VersionStatus = 0
	END

	INSERT INTO [dbo].[VEVersion]
	(
		[Id],
		[ApplicationId],
		[ObjectTypeId],
		[ObjectId],
		[CreatedDate],
		[CreatedBy],
		[VersionNumber],
		[RevisionNumber],
		[Status],
		[ObjectStatus],
		[VersionStatus],
		[XMLString],
		[Comments],
		[TriggeredObjectId],
		[TriggeredObjectTypeId]
	)          
	SELECT TOP 1 @VersionId,
		@TargetSiteId,
		8, 
		@Id, 
		@UtcNow,
		@ModifiedBy,
		@VersionNumber,
		@RevisionNumber,
		@VersionStatus,
		ObjectStatus,
		VersionStatus, 
		dbo.UpdatePageXml(XMLString, @Id, P.WorkflowId, P.WorkflowState, P.PageStatus, 1, 
			P.CreatedBy, P.CreatedDate, P.AuthorId, P.SiteId, P.SourcePageDefinitionId, @PageMapNodeId), 
		'Imported from master',
		TriggeredObjectId,
		TriggeredObjectTypeId       
	FROM VEVersion V, 
		PageDefinition P
	WHERE V.Id = @SourceVersionId
		AND P.PageDefinitionId = @Id

	IF @Publish = 1
		UPDATE VEVersion SET Status = 1 WHERE ObjectId = @Id

	INSERT INTO [dbo].[VEPageDefSegmentVersion]
	(
		[Id],
		[VersionId],
		[PageDefinitionId],
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],  
		[ContainerContentXML],        
		[UnmanagedXml],
		[ZonesXml],
		[CreatedBy],
		[CreatedDate]
	)
	SELECT NEWID(),
		@VersionId,
		@Id,
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],
		CASE WHEN @Overwrite = 1 THEN [ContainerContentXML] 
			ELSE [dbo].[Page_GetContainerXml](@Id, AudienceSegmentId, DeviceId) END,
		dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
		[ZonesXml],
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[VEPageDefSegmentVersion]
	WHERE VersionId = @SourceVersionId

	INSERT INTO [dbo].[VEPageContentVersion]
	(
		[Id],
		[ContentVersionId],
		[IsChanged],
		[PageDefSegmentVersionId]
	)
	SELECT NEWID(),
		ContentVersionId,
		IsChanged,
		CS.Id
	FROM [dbo].[VEPageContentVersion] PC
		JOIN VEPageDefSegmentVersion PS ON PC.PageDefSegmentVersionId = PS.Id
		JOIN VEPageDefSegmentVersion CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
			AND PS.VersionId = @SourceVersionId
			AND CS.VersionId = @VersionId

	IF @Publish = 1
	BEGIN
		DECLARE @PublishCount int
		SET @PublishCount = (SELECT ISNULL(PublishCount, 0) + 1 FROM PageDefinition WHERE PageDefinitionId = @Id)

		UPDATE PageDefinition SET
			PublishCount = @PublishCount,
			PublishDate = @UtcNow,
			PageStatus = 8
		WHERE PageDefinitionId = @Id

		DECLARE @WorkflowId uniqueidentifier
		SELECT TOP 1 @WorkflowId = WorkflowId FROM WFWorkflowExecution WHERE ObjectId = @Id AND Completed = 0

		IF @WorkflowId IS NOT NULL
		BEGIN
			EXEC WorkflowExecution_Save
				@WorkflowId = @WorkflowId,
				@SequenceId = @EmptyGuid,
				@ObjectId = @Id,
				@ObjectTypeId = 8,
				@ActorId = @ModifiedBy,
				@ActorType = 1,
				@ActionId = 8,
				@Remarks = N'Page imported from master',
				@Completed = 1
		END

		DELETE
		FROM	PageObjectReferences
		WHERE	PageId = @Id

		INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
			SELECT	NEWID(), @Id, ObjectId, ObjectType, ObjectSiteId
			FROM	PageObjectReferences
			WHERE	PageId = @SourceId
	END

	IF @InvalidateCache = 1
	BEGIN
		EXEC PageDto_Invalidate @SiteId = @TargetSiteId, @ModifiedBy = @ModifiedBy
	END
END
GO
PRINT 'Create procedure VariantSite_CopyMenus'
GO
IF (OBJECT_ID('VariantSite_CopyMenus') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyMenus
GO
CREATE PROCEDURE [dbo].[VariantSite_CopyMenus]
(  
	@SiteId			uniqueidentifier,
	@MicroSiteId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	Declare @NotAllowAccessInChildrenSites Table (PageMapNodeId uniqueIdentifier, Process bit)
	Declare @ExcludeNodes Table (PageMapNodeId uniqueIdentifier, Title varchar(1000))
	Declare @LftValue int, @RgtValue int

	INSERT INTO @NotAllowAccessInChildrenSites  (PageMapNodeId, Process)
	Select PageMapnodeId, 0 from PageMapNode where AllowAccessInChildrenSites = 0 and SiteId = @SiteId
	Declare @ParentPageMapNodeIdToExclude uniqueidentifier
	While EXISTS( Select 1 from @NotAllowAccessInChildrenSites  where Process = 0)
	BEGIN
		Select top 1 @ParentPageMapNodeIdToExclude  = PageMapNodeId from @NotAllowAccessInChildrenSites  where Process = 0

		Select @LftValue = LftValue, @RgtValue = RgtValue from PageMapNode where PageMapNodeId = @ParentPageMapNodeIdToExclude and SiteId = @SiteId

		INSERT INTO @ExcludeNodes (PageMapNodeId, Title) 
		Select PageMapNodeId, DisplayTitle  from PageMapNode where LftValue Between @LftValue and @RgtValue and SiteId = @SiteId

		Update @NotAllowAccessInChildrenSites Set Process = 1 where PageMapNodeId = @ParentPageMapNodeIdToExclude
	END

	DECLARE @CampaignPageMapNodeId uniqueidentifier, @StrCampaignPageMapNodeId nvarchar(100)
	SELECT TOP 1 @StrCampaignPageMapNodeId = Value FROM STSettingType T JOIN STSiteSetting S ON T.Id = S.SettingTypeId
		WHERE T.Name = 'CampaignPageMapNodeId' AND S.SiteId = @SiteId

	IF @StrCampaignPageMapNodeId IS NOT NULL AND @StrCampaignPageMapNodeId != ''
		SET @CampaignPageMapNodeId = CAST(@StrCampaignPageMapNodeId as uniqueidentifier)
	ELSE
		SET @CampaignPageMapNodeId = dbo.GetEmptyGUID()

	INSERT INTO [dbo].[PageMapNode]
			   ([PageMapNodeId]
			   ,[ParentId]
			   ,[LftValue]
			   ,[RgtValue]
			   ,[SiteId]
			   ,[ExcludeFromSiteMap]
			   ,[Description]
			   ,[DisplayTitle]
			   ,[FriendlyUrl]
			   ,[MenuStatus]
			   ,[TargetId]
			   ,[Target]
			   ,[TargetUrl]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PropogateWorkflow]
			   ,[InheritWorkflow]
			   ,[PropogateSecurityLevels]
			   ,[InheritSecurityLevels]
			   ,[PropogateRoles]
			   ,[InheritRoles]
			   ,[Roles]
			   ,[LocationIdentifier]
			   ,[HasRolloverImages]
			   ,[RolloverOnImage]
			   ,[RolloverOffImage]
			   ,[IsCommerceNav]
			   ,[AssociatedQueryId]
			   ,[DefaultContentId]
			   ,[AssociatedContentFolderId]
			   ,[CustomAttributes]
			   ,[HiddenFromNavigation]
			   ,SourcePageMapNodeId
			   ,MasterPageMapNodeId
			   ,CssClass)
			   SELECT  case when parentId is null then @MicroSiteId else NEWID()end
				  ,[ParentId]
				  ,[LftValue]
				  ,[RgtValue]
				  ,@MicroSiteId
				  ,[ExcludeFromSiteMap]
				  ,[Description]
				  ,[DisplayTitle]
				  ,[FriendlyUrl]
				  ,[MenuStatus]
				  ,[TargetId]
				  ,[Target]
				  ,[TargetUrl]
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,[PropogateWorkflow]
				  ,[InheritWorkflow]
				  ,[PropogateSecurityLevels]
				  ,[InheritSecurityLevels]
				  ,[PropogateRoles]
				  ,[InheritRoles]
				  ,[Roles]
				  ,[LocationIdentifier]
				  ,[HasRolloverImages]
				  ,[RolloverOnImage]
				  ,[RolloverOffImage]
				  ,[IsCommerceNav]
				  ,[AssociatedQueryId]
				  ,[DefaultContentId]
				  ,[AssociatedContentFolderId]
				  ,[CustomAttributes]
				  ,[HiddenFromNavigation]
				  ,PageMapNodeId
				  ,ISNULL(MasterPageMapNodeId, PageMapNodeId)
				  ,CssClass
			  FROM [dbo].[PageMapNode]
			   Where SiteId=@SiteId
			   AND PageMapNodeId not in    
			   (
				SELECT distinct P.PageMapNodeId 
				FROM [dbo].[PageMapNode] P
				cross  join PageMapNode DP 
				Where P.SiteId=@SiteId and P.LftValue between DP.LftValue and DP.RgtValue
				AND DP.PageMapNodeId   in 
				 (
					select @CampaignPageMapNodeId
					UNION 
					select distinct PageMapNodeId from @ExcludeNodes
				 )   
			   )
	  /* END Modification for not copying the node which are not shared in variant sites*/
	  
		--remove marketierpages menu group   
		DELETE FROM PageMapNode WHERE SiteId = @MicroSiteId AND SourcePageMapNodeId IN 
		(SELECT ParentId FROM PageMapNode WHERE PageMapNodeId = @CampaignPageMapNodeId)
		
		--remove marketierpages menu group   subnodes if any exist(generally it should not)
		DELETE FROM PageMapNode WHERE SiteId = @MicroSiteId AND ParentId IN 
		(SELECT ParentId FROM PageMapNode WHERE PageMapNodeId = @CampaignPageMapNodeId)

		
	    
	   Update  M Set M.ParentId = P.PageMapNodeId
	   FROM PageMapNode M 
	   INNER JOIN PageMapNode P ON P.SourcePageMapNodeId = M.ParentId
	   Where P.SiteId =@MicroSiteId and M.SiteId=@MicroSiteId
	   
	   UPdate PN SET PN.TargetId=T.PageMapNodeId
		FROM PageMapNode PN
		INNER JOIN PageMapNode T ON PN.TargetId = T.SourcePageMapNodeId
		Where PN.Target='2' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId


		 --import shared workflow mapping of pagemapnode and workflow
	INSERT INTO [dbo].[PageMapNodeWorkflow]
           ([Id]
           ,[PageMapNodeId]
           ,[WorkflowId]
           ,[CustomAttributes])
	select NEWID(),
		P.PageMapNodeId,
		PW.WorkflowId,
		null
	FROM [dbo].[PageMapNodeWorkflow] PW
		INNER JOIN PageMapNode P ON PW.PageMapNodeId = P.SourcePageMapNodeId
		inner join WFWorkflow W on PW.WorkflowId = W.Id 
		 Where P.SiteId = @MicroSiteId  and W.IsShared=1 


	IF exists(select 1 from  [dbo].PageMapBase B INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId Where P.SiteId = @MicroSiteId)
	begin
	 INSERT INTO [dbo].[PageMapBase]
					   ([SiteId]
					   ,[HomePageDefinitionId]
					   ,[ModifiedDate]
					   ,[ModifiedBy]
					   ,[LastPageMapModificationDate])
			SELECT top 1 @MicroSiteId
				  ,P.PageMapNodeId
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,[LastPageMapModificationDate]
			FROM [dbo].PageMapBase B
			INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId
			 Where P.SiteId = @MicroSiteId
	end
	else	
		Insert into PageMapBase (SiteId, ModifiedDate,ModifiedBy)	Values(@MicroSiteId, GETUTCDATE(),@ModifiedBy) 

	EXEC PageMapBase_UpdateLastModification @MicroSiteId
END
GO
PRINT 'Create procedure VariantSite_CopyPages'
GO
IF (OBJECT_ID('VariantSite_CopyPages') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyPages
GO

CREATE PROCEDURE [dbo].[VariantSite_CopyPages]
(  
	@SiteId UNIQUEIDENTIFIER,
	@MicroSiteId UNIQUEIDENTIFIER,
	@ModifiedBy  UNIQUEIDENTIFIER,
	@MakePageStatusAsDraft BIT
	
)
AS
BEGIN

	INSERT INTO [dbo].[PageDefinition]
					   ([PageDefinitionId]
					   ,[TemplateId]
					   ,[SiteId]
					   ,[Title]
					   ,[Description]
					   ,[PageStatus]
					   ,[WorkflowState]
					   ,[PublishCount]
					   ,[PublishDate]
					   --,[DisplayOrder]
					   ,[FriendlyName]
					   ,[WorkflowId]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate]
					   ,[ArchivedDate]
					   ,[StatusChangedDate]
					   ,[AuthorId]
					   ,[EnableOutputCache]
					   ,[OutputCacheProfileName]
					   ,[ExcludeFromSearch]
					   ,[IsDefault]
					   ,[IsTracked]
					   ,[HasForms]
					   ,[TextContentCounter]
					   ,[SourcePageDefinitionId]
					   ,[MasterPageDefinitionId]
					   ,[CustomAttributes]
					   )
			SELECT NEWID()
				  ,[TemplateId]
				  ,@MicroSiteId
				  ,[Title]
				  ,D.[Description]
				  ,case when @MakePageStatusAsDraft=1 then dbo.GetActiveStatus()  else [PageStatus]  end
				  ,case when @MakePageStatusAsDraft=1 then  2  else case when WorkflowState =1 then WorkflowState else 2 end end
				  ,case when @MakePageStatusAsDraft=1 then 0 else 1 end
				  ,[PublishDate]
				  --,[DisplayOrder]
				  ,[FriendlyName]
				  ,dbo.GetEmptyGUID()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,[ArchivedDate]
				  ,[StatusChangedDate]
				  ,@ModifiedBy
				  ,[EnableOutputCache]
				  ,[OutputCacheProfileName]
				  ,[ExcludeFromSearch]
				  ,[IsDefault]
				  ,[IsTracked]
				  ,[HasForms]
				  ,[TextContentCounter]
				  ,PageDefinitionId
				  ,ISNULL(MasterPageDefinitionId, PageDefinitionId)
				  ,D.[CustomAttributes]
			      
			  FROM [dbo].[PageDefinition] D
			Where D.SiteId =@SiteId and PageDefinitionId in (select PageDefinitionId  from PageMapNodePageDef PMD inner join PageMapNode PM on PMD.PageMapNodeId = PM.SourcePageMapNodeId where PM.SiteId =@MicroSiteId)

			insert into PageMapNodePageDef
			select distinct  c.PageMapNodeId ,a.PageDefinitionId, b.DisplayOrder from [PageDefinition] a , PageMapNodePageDef b  , PageMapNode c
			where a.SiteId  = @MicroSiteId and a.SourcePageDefinitionId = b.PageDefinitionId 
			and c.SourcePageMapNodeId = b.PageMapNodeId and 
			c.SiteId = @MicroSiteId

			UPDATE PN SET PN.TargetId=T.PageDefinitionId
			FROM PageMapNode PN
			INNER JOIN PageDefinition T ON PN.TargetId = T.SourcePageDefinitionId
			Where PN.Target='1' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId

		
			insert into #tempIds SELECT PS.Id, NEWID() FROM  [dbo].[PageDefinitionSegment] PS INNER JOIN PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
			 
			 INSERT INTO [dbo].[PageDefinitionSegment]
						   ([Id]
						   ,[PageDefinitionId]
						   ,[DeviceId]
						   ,[AudienceSegmentId]
						   ,[UnmanagedXml]
						   ,[ZonesXml])
						   SELECT T.NewId 
							  ,P.PageDefinitionId 
							  ,[DeviceId]
							  ,[AudienceSegmentId]
							  ,[UnmanagedXml]
							  ,[ZonesXml]
						  FROM [dbo].[PageDefinitionSegment] PS 
						  inner join #tempIds T on PS.Id= T.OldId 
						  inner join PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
						  Where P.SiteId = @MicroSiteId
						  
			INSERT INTO [PageDefinitionContainer]
				(
					[Id]
					,PageDefinitionSegmentId 
					,[PageDefinitionId]
					,[ContainerId]
					,[ContentId]
					,[InWFContentId]
					,[ContentTypeId]
					,[IsModified]
					,[IsTracked]
					,[Visible]
					,[InWFVisible]
					,DisplayOrder 
					,InWFDisplayOrder 
					,[CustomAttributes]
					,[ContainerName])
				SELECT
						NewId() 
						,T.NewId 
					,P.PageDefinitionId
					,ContainerId
					,ContentId
					,InWFContentId
					,ContentTypeId
					,IsModified
					,PC.IsTracked
					,Visible
					,InWFVisible
					,PC.DisplayOrder 
					,PC.InWFDisplayOrder 
					,PC.CustomAttributes
					,ContainerName
				FROM PageDefinitionContainer PC
				inner join #tempIds T on PC.PageDefinitionSegmentId= T.OldId
				inner join PageDefinition P ON PC.PageDefinitionId=P.SourcePageDefinitionId 
				Where P.SiteId = @MicroSiteId
						  
			truncate table #tempIds

			INSERT INTO [dbo].[PageDetails]
					   ([PageId]
					   ,[PageDetailXML])
			  SELECT 
				 P.PageDefinitionId
					   ,[PageDetailXML]
			  FROM [dbo].[PageDetails] PD
			INNER JOIN PageDefinition P ON P.SourcePageDefinitionId = PD.PageId
			 Where P.SiteId = @MicroSiteId

			 INSERT INTO [dbo].[USSecurityLevelObject]
					   ([ObjectId]
					   ,[ObjectTypeId]
					   ,[SecurityLevelId])
			  SELECT P.PageDefinitionId
					   ,ObjectTypeId
					   ,SecurityLevelId
			FROM [USSecurityLevelObject] SO
			INNER JOIN PageDefinition P ON SO.ObjectId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageScript]
					   ([PageDefinitionId]
					   ,[ScriptId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[ScriptId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageScript] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageStyle]
					   ([PageDefinitionId]
					   ,[StyleId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[StyleId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageStyle] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[COTaxonomyObject]
					   ([Id]
					   ,[TaxonomyId]
					   ,[ObjectId]
					   ,[ObjectTypeId]
					   ,[SortOrder]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate])
			 SELECT NEWID()
				  ,[TaxonomyId]
				  ,P.PageDefinitionId
				  ,[ObjectTypeId]
				  ,[SortOrder]
				  ,[Status]
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
			  FROM [dbo].[COTaxonomyObject] OT
			INNER JOIN PageDefinition P ON OT.ObjectId= P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
	
	IF @MakePageStatusAsDraft = 0
	BEGIN
		INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
			SELECT	NEWID(), PD1.PageDefinitionId, POR.ObjectId, POR.ObjectType, POR.ObjectSiteId
			FROM	PageDefinition AS PD1
					INNER JOIN PageDefinition AS PD2 ON PD2.PageDefinitionId = PD1.SourcePageDefinitionId
					INNER JOIN PageObjectReferences AS POR ON POR.PageId = PD2.PageDefinitionId
			WHERE	PD1.SiteId = @MicroSiteId
	END
END
GO
PRINT 'Create procedure PageMapBase_UpdateLastModification'
GO
IF (OBJECT_ID('PageMapBase_UpdateLastModification') IS NOT NULL)
	DROP PROCEDURE PageMapBase_UpdateLastModification
GO
CREATE PROCEDURE [dbo].[PageMapBase_UpdateLastModification]
(
	@SiteId			uniqueidentifier = null
)
AS
BEGIN
	DECLARE @ModifiedDate datetime
	SET @ModifiedDate =  DATEADD(SECOND, DATEDIFF(SECOND, 39000, GETUTCDATE()), 39000)

	IF @SiteId IS NOT NULL
	BEGIN
		DECLARE @LatestPageMapDate datetime, @LastCacheDate datetime

		SELECT TOP 1 @LastCacheDate = ModifiedDate FROM PageMapNode
		WHERE SiteId = @SiteId
		ORDER BY ModifiedDate DESC

		SELECT @LastCacheDate = LastPageMapModificationDate FROM PageMapBase
		WHERE SiteId = @SiteId

		IF @LatestPageMapDate > @LastCacheDate
			UPDATE PageMapNode SET CompleteFriendlyUrl = CalculatedUrl
			WHERE SiteId = @SiteId
	END

	IF EXISTS (SELECT 1 FROM PageMapNode WHERE MasterPageMapNodeId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageMapNodeId = S.SourcePageMapNodeId 
		FROM PageMapNode P
			JOIN vwMasterSourceMenuId S ON S.PageMapNodeId = P.PageMapNodeId
		WHERE P.MasterPageMapNodeId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageMapNode SET MasterPageMapNodeId = PageMapNodeId 
		WHERE MasterPageMapNodeId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	IF EXISTS (SELECT 1 FROM PageDefinition WHERE MasterPageDefinitionId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageDefinitionId = S.SourcePageDefinitionId 
		FROM PageDefinition P
			JOIN vwMasterSourcePageId S ON S.PageDefinitionId = P.PageDefinitionId
		WHERE P.MasterPageDefinitionId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageDefinition SET MasterPageDefinitionId = PageDefinitionId 
		WHERE MasterPageDefinitionId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	UPDATE PageMapBase SET LastPageMapModificationDate = @ModifiedDate
	WHERE @SiteId IS NULL OR SiteId = @SiteId
END
GO
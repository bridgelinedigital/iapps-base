DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int
SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'WYSIWYG Editor'
IF NOT EXISTS (Select 1 from STSettingType where Name = 'WYSIWYG.CustomScriptPath')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('WYSIWYG.CustomScriptPath', 'This will allow to customize editor functionality', @SettingGroupId, @Sequence, 'Textbox', 1, 1)
END
IF NOT EXISTS (Select 1 from STSettingType where Name = 'WYSIWYG.CustomButtons')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('WYSIWYG.CustomButtons', 'Space delimited names of custom toolbar buttons', @SettingGroupId, @Sequence, 'Textbox', 1, 1)
END
GO
PRINT 'Creating vwSkuImage'
GO
IF(OBJECT_ID('vwSkuImage') IS NOT NULL)
	DROP VIEW vwSkuImage
GO
CREATE VIEW [dbo].[vwSkuImage]      
AS      
WITH CTE AS      
(      
 SELECT Id, SiteId, Title, AltText, Description, Size, Status, ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy        
 FROM CLImage      
      
 UNION ALL      
      
 SELECT Id, SiteId, Title, AltText, Description, NULL, Status, ModifiedDate, ModifiedBy      
 FROM CLImageVariantCache      
)      

      
SELECT I2.Id,     
 C.Title,      
 I2.FileName,      
 C.AltText,      
 C.Description,      
 C.Size,      
 ISNULL(SEQ.Sequence, OI.Sequence) Sequence,      
 P.Id AS ProductId,      
 P.Title AS ProductName,      
 I.CreatedBy,      
 I.CreatedDate,      
 C.ModifiedBy,      
 C.ModifiedDate,      
 FN.UserFullName CreatedByFullName,      
 MN.UserFullName ModifiedByFullName,      
 P.ProductTypeId,      
 '/productimages/' + CAST(P.Id AS varchar(36)) + '/images' AS RelativePath,      
 C.SiteId,      
 I.SiteId AS SourceSiteId,      
 IT.TypeNumber,      
 C.Status,      
 I.ParentImage,      
 I.IsShared,      
 PS.Id AS SkuId,
 PS.SKU AS Sku  
FROM PRProduct P  
 JOIN CLObjectImage OI ON (OI.ObjectId = P.Id  AND OI.ObjectType = 205)
 LEFT JOIN CLObjectImage OI2 ON (OI2.ImageId =OI.ImageId  AND OI2.ObjectType = 206) 
 JOIN PRProductSKU PS ON PS.ProductId = P.Id 
 JOIN CTE C ON OI.ImageId = C.Id 
 JOIN CLImage I ON I.Id = C.Id      
 JOIN CLImage I2 ON I.Id = I2.ParentImage OR I.Id = I2.Id      
 JOIN CLImageType IT ON IT.Id = I2.ImageType      
 LEFT JOIN VW_UserFullName FN on FN.UserId = I.CreatedBy      
 LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy 
 LEFT JOIN CLObjectImageSequence SEQ On SEQ.CLObjectImageId = OI.Id AND SEQ.SiteId = C.SiteId
 WHERE OI2.ObjectId = PS.Id OR OI2.ObjectId IS NULL
GO
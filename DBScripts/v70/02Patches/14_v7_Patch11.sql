----------------------------------
----UPDATED FOR CEM-1155
----------------------------------

IF (OBJECT_ID('Contact_SearchSiteAttributes') IS NOT NULL)
	DROP PROCEDURE Contact_SearchSiteAttributes
GO

CREATE PROCEDURE [dbo].[Contact_SearchSiteAttributes]
    @SiteAttributes XML ,
    @ApplicationId UNIQUEIDENTIFIER
AS 
    BEGIN
        DECLARE @ContactSearch TABLE
            (ConditionId UNIQUEIDENTIFIER,
              AttributeId UNIQUEIDENTIFIER ,
              AttributeValueID UNIQUEIDENTIFIER ,
              Value1 NVARCHAR(MAX) ,
              Value2 NVARCHAR(MAX) ,
              Operartor NVARCHAR(50) ,
              DataType NVARCHAR(50),
              ValueCount INT
            )

      
                SELECT  NEWID() as ConditionId,
						tab.col.value('@AttributeId', 'uniqueidentifier') as AttributeId ,                        
                        tab.col.value('@Value1', 'nvarchar(max)') as Value1,
                        tab.col.value('@Value2', 'nvarchar(max)') as Value2,
                        tab.col.value('@Operator', 'nvarchar(50)') as Operator,
                        'System.String' as DataType,
						col.query('AttributeValueId') AS XmlValue
				INTO    #tbltest
                FROM    @SiteAttributes.nodes('/SiteAttributes/AttributeSearchItem') tab ( col )


				      INSERT  INTO @ContactSearch
                ( ConditionId ,
                  AttributeId ,
                  AttributeValueID ,
                  Value1 ,
                  Value2 ,
                  Operartor ,
                  DataType ,
                  ValueCount
                )
                SELECT  T.ConditionId ,
                        T.AttributeId ,
                        pref.value('(text())[1]', 'uniqueidentifier') ,
                        T.Value1 ,
                        T.Value2 ,
                        T.Operator ,
                        T.DataType ,
                        COUNT(pref.value('(text())[1]', 'uniqueidentifier')) OVER ( PARTITION BY T.ConditionId,
                                                              T.AttributeId  ) AS ValueCount
                FROM    #tbltest T
                        OUTER APPLY XmlValue.nodes('AttributeValueId') AS Value ( pref )


        UPDATE  CS
        SET     CS.DataType = ADT.Type
        FROM    @ContactSearch CS
                INNER JOIN dbo.ATAttribute A ON A.Id = CS.AttributeId
                INNER JOIN dbo.ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId 


        DECLARE @CacheTable TABLE
            (
              SiteId UNIQUEIDENTIFIER ,
              AttributeId UNIQUEIDENTIFIER ,
              AttributeEnumId UNIQUEIDENTIFIER ,
              DataType NVARCHAR(50) ,
              VALUE NVARCHAR(4000)
            )
     
        INSERT  INTO @CacheTable
                ( SiteId ,
                  AttributeId ,
                  AttributeEnumId ,
                  DataType ,
                  VALUE
               
                )
                SELECT  CA.SiteId ,
                        CS.AttributeId ,
                        CA.AttributeEnumId ,
                        ADT.Type AS DataType ,
                        CA.Value
                FROM    @ContactSearch CS
                        INNER JOIN dbo.ATSiteAttributeValue CA ON CA.AttributeId = CS.AttributeId
                        INNER JOIN ATAttribute A ON A.ID = CA.AttributeId
                        INNER JOIN ATAttributeDataType ADT ON ADT.ID = A.AttributeDataTypeId


        DECLARE @SystemString varchar(20)
		DECLARE @SystemDatetime varchar(20)
		DECLARE @SystemBoolean varchar(20)
		DECLARE @SystemDouble varchar(20)
		DECLARE @SystemInt32 varchar(20)
		DECLARE @Enumerated VARCHAR(20)
		SET @SystemString = 'System.String'
		SET @SystemDatetime = 'System.DateTime'
		SET @SystemBoolean = 'System.Boolean'
		SET @SystemDouble = 'System.Double'
		SET @SystemInt32 = 'System.Int32'
		SET @Enumerated='Enumerated'

		DECLARE @Equal varchar(40)
		DECLARE @GreaterThan varchar(40)
		DECLARE @GreaterThanEqual varchar(40)
		DECLARE @LessThan varchar(40)
		DECLARE @LessThanEqual varchar(40)
		DECLARE @NotEqual varchar(40)
		DECLARE @Between varchar(40)
		DECLARE @Contains varchar(40)
		DECLARE @DoesNotContain varchar(40)
		SET @Equal = 'opOperatorEqual'
		SET @GreaterThan = 'opOperatorGreaterThan'
		SET @GreaterThanEqual = 'opOperatorGreaterThanEqual'
		SET @LessThan = 'opOperatorLessThan'
		SET @LessThanEqual = 'opOperatorLessThanEqual'
		SET @NotEqual = 'opOperatorNotEqual'
		SET @Between = 'opOperatorBetween'
		SET @Contains = 'opOperatorContains'
		SET @DoesNotContain = 'opOperatorDoesNotContain'

        DECLARE @LocalSiteId TABLE ( Id UNIQUEIDENTIFIER, AttributeValueId UNIQUEIDENTIFIER, ConditionId UNIQUEIDENTIFIER )
       
	    INSERT  INTO @LocalSiteId
                SELECT  
                        CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemString 
													  AND CS.DataType = @SystemString
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = @Equal
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemString 
													  AND CS.DataType = @SystemString
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = @GreaterThan
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemString 
													  AND CS.DataType = @SystemString
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = @GreaterThanEqual
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemString 
													  AND CS.DataType = @SystemString
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = @LessThan
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemString 
													  AND CS.DataType = @SystemString
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = @LessThanEqual
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemString 
													  AND CS.DataType = @SystemString
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = @NotEqual
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemString 
													  AND CS.DataType = @SystemString
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = @Between
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemString 
													  AND CS.DataType = @SystemString
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = @Contains
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemString 
													  AND CS.DataType = @SystemString
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = @DoesNotContain
 -------------------------------------------------------------------------------------------------------------------------------------
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemDatetime
                                                      AND CS.DataType = @SystemDatetime
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = @Equal
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemDatetime
                                                      AND CS.DataType = @SystemDatetime
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = @GreaterThan
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemDatetime
                                                      AND CS.DataType = @SystemDatetime
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = @GreaterThanEqual
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemDatetime
                                                      AND CS.DataType = @SystemDatetime
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = @LessThan
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemDatetime
                                                      AND CS.DataType = @SystemDatetime
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = @LessThanEqual
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemDatetime
                                                      AND CS.DataType = @SystemDatetime
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = @NotEqual
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemDatetime
                                                      AND CS.DataType = @SystemDatetime
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = @Between
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemDatetime
                                                      AND CS.DataType = @SystemDatetime
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = @Contains
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemDatetime
                                                      AND CS.DataType = @SystemDatetime
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = @DoesNotContain
 
 ---------------------------------------------------------------------------------------------------------------------------------------       


 -------------------------------------------------------------------------------------------------------------------------------------
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = @SystemDouble 
													          OR CAV.DataType = @SystemInt32 
															) 
															AND ( CS.DataType = @SystemDouble 
															  OR CS.DataType = @SystemInt32 
															  ) 
														   )
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = @Equal
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = @SystemDouble
                                                              OR CAV.DataType = @SystemInt32
                                                            )
                                                            AND ( CS.DataType = @SystemDouble
                                                              OR CS.DataType = @SystemInt32
                                                              )
                                                          )
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = @GreaterThan
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = @SystemDouble
                                                              OR CAV.DataType = @SystemInt32
                                                            )
                                                            AND ( CS.DataType = @SystemDouble
                                                              OR CS.DataType = @SystemInt32
                                                              )
                                                          )
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = @GreaterThanEqual
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = @SystemDouble
                                                              OR CAV.DataType = @SystemInt32
                                                            )
                                                            AND ( CS.DataType = @SystemDouble
                                                              OR CS.DataType = @SystemInt32
                                                              )
                                                          )
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = @LessThan
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = @SystemDouble
                                                              OR CAV.DataType = @SystemInt32
                                                            )
                                                            AND ( CS.DataType = @SystemDouble
                                                              OR CS.DataType = @SystemInt32
                                                              )
                                                          )
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = @LessThanEqual
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = @SystemDouble
                                                              OR CAV.DataType = @SystemInt32
                                                            )
                                                            AND ( CS.DataType = @SystemDouble
                                                              OR CS.DataType = @SystemInt32
                                                              )
                                                          )
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = @NotEqual
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = @SystemDouble
                                                              OR CAV.DataType = @SystemInt32
                                                            )
                                                            AND ( CS.DataType = @SystemDouble
                                                              OR CS.DataType = @SystemInt32
                                                              )
                                                          )
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = @Between
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = @SystemDouble
                                                              OR CAV.DataType = @SystemInt32
                                                            )
                                                            AND ( CS.DataType = @SystemDouble
                                                              OR CS.DataType = @SystemInt32
                                                              )
                                                          )
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = @Contains
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = @SystemDouble
                                                              OR CAV.DataType = @SystemInt32
                                                            )
                                                            AND ( CS.DataType = @SystemDouble
                                                              OR CS.DataType = @SystemInt32
                                                              )
                                                          )
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = @DoesNotContain
 --------------------------------------------------------------------------------------------------------------------------------------- 
				UNION
				SELECT  
                        CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemBoolean 
													  AND CS.DataType = @SystemBoolean
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = @Equal
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemBoolean
													  AND CS.DataType = @SystemBoolean
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = @GreaterThan
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemBoolean
													  AND CS.DataType = @SystemBoolean
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = @GreaterThanEqual
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemBoolean
													  AND CS.DataType = @SystemBoolean
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = @LessThan
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemBoolean
													  AND CS.DataType = @SystemBoolean
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = @LessThanEqual
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemBoolean
													  AND CS.DataType = @SystemBoolean
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = @NotEqual
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemBoolean
													  AND CS.DataType = @SystemBoolean
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = @Between
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemBoolean
													  AND CS.DataType = @SystemBoolean
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = @Contains
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @SystemBoolean
													  AND CS.DataType = @SystemBoolean
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = @DoesNotContain

 				UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @Enumerated
													  AND CS.DataType = @Enumerated
                                                      AND CAV.AttributeEnumId =CS.Value1 
                WHERE   CS.Operartor = @Equal
				UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = @Enumerated
													  AND CS.DataType = @Enumerated
                                                      AND CAV.AttributeEnumId !=CS.Value1 
                WHERE   CS.Operartor = @NotEqual
 --------------------------------------------------------------------------------------------------------------------------------------- 

        DECLARE @LocalTemp TABLE ( Id UNIQUEIDENTIFIER )
      --  INSERT  INTO @LocalTemp
                --SELECT DISTINCT
                --        CS.UserId
                --FROM    vw_ContactSite CS
                --        INNER JOIN @LocalSiteId LS ON LS.Id = CS.SiteId




						  INSERT  INTO @LocalTemp
                SELECT DISTINCT
                        CS.UserId
                FROM    vw_ContactSite CS
                        INNER JOIN (
						 SELECT DISTINCT
                        TV2.Id as SiteId
                FROM    ( SELECT    * ,
                                    COUNT(AttributeValueId) OVER ( PARTITION BY ConditionID,
                                                              Id ) AS ValueCount
                          FROM      ( SELECT  DISTINCT
                                                *
                                      FROM      @LocalSiteId
                                    ) TV
                        ) TV2
                        INNER JOIN @ContactSearch CS ON CS.ConditionId = TV2.ConditionId
                                                        AND CS.ValueCount = TV2.ValueCount
						)TV on TV.SiteId = CS.SiteId



        DECLARE @currentRecords INT
        SET @currentRecords = ( SELECT  COUNT(Id)
                                FROM    #tempContactSearchOutput
                              )
       
        IF @currentRecords > 0 
            BEGIN
                DELETE  T
                FROM    #tempContactSearchOutput t
                        LEFT JOIN @LocalTemp LT ON LT.Id = T.Id
                WHERE   LT.Id IS NULL
            END
        ELSE 
            BEGIN 
                INSERT  INTO #tempContactSearchOutput
                        SELECT  Id
                        FROM    @LocalTemp
            END
        SELECT  @@rowcount
     
    END




	
	
	go

 
-------------------------------------------------------
------UPDATED FOR CEM-1682 
----------------------------------------------------

Update BLD_SKU SET UserDefinedPrice =0
Where UserDefinedPrice IS  NULL

Update BLD_SKU SET FreeShipping =0
Where FreeShipping IS  NULL


go

----------------------------------
----UPDATED FOR CEM-711
----------------------------------

IF (OBJECT_ID('CampaignSettingDto_Save') IS NOT NULL)
	DROP PROCEDURE [CampaignSettingDto_Save]
GO

CREATE PROCEDURE [dbo].[CampaignSettingDto_Save]
(
	@Id					uniqueidentifier = null OUTPUT,
	@SiteId				uniqueidentifier,
	@SendLimitPerMonth	int,
	@Blackouts			xml,
	@IsShared			bit,
	@ModifiedBy			uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier
	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @Id = @EmptyGuid SET @Id = NULL

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()

		INSERT INTO MKCampaignSetting
		(
			Id,
			SiteId,
			SendLimitPerMonth,
			IsShared,
			CreatedBy,
			CreatedDate
		)
		VALUES
		(
			@Id,
			@SiteId,
			@SendLimitPerMonth,
			@IsShared,
			@ModifiedBy,
			@UtcNow
		)
	END
	ELSE
	BEGIN
		UPDATE MKCampaignSetting
		SET SendLimitPerMonth = @SendLimitPerMonth,
			IsShared = @IsShared,
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @UtcNow
		WHERE Id = @Id	
	END

	DELETE T FROM MKCampaignBlackoutTarget T
	WHERE EXISTS (SELECT 1 FROM MKCampaignBlackout B WHERE B.Id = T.CampaignBlackoutId AND B.CampaignSettingId = @Id)

	DELETE FROM MKCampaignBlackout WHERE CampaignSettingId = @Id

	INSERT INTO MKCampaignBlackout
	(
		Id,
		CampaignSettingId,
		Title,
		Description,
		StartDate,
		EndDate,
		CreatedBy,
		CreatedDate
	)
	SELECT
		C.B.value('@Id', 'uniqueidentifier'),
		@Id,
		C.B.value('@Title', 'nvarchar(max)'),
		C.B.value('@Description', 'nvarchar(max)'),
		C.B.value('@StartDate', 'date'),
		C.B.value('@EndDate', 'date'),
		@ModifiedBy,
		@UtcNow
	FROM @Blackouts.nodes('/Blackouts/Blackout') AS C(B)

	INSERT INTO MKCampaignBlackoutTarget
	(
		Id,
		CampaignBlackoutId,
		TargetId,
		TargetTypeId
	)
	SELECT
		C.T.value('@Id', 'uniqueidentifier'),
		C.T.value('@CampaignBlackoutId', 'uniqueidentifier'),
		C.T.value('@TargetId', 'uniqueidentifier'),
		C.T.value('@TargetTypeId', 'int')
	FROM @Blackouts.nodes('/Blackouts/Blackout/Targets/Targets') AS C(T)

END


go

----------------------------------
----UPDATED FOR CEM-795
----------------------------------
 

 
Declare @Const NVARCHAR(256) 
SET @Const = (
              SELECT TOP 1 'ALTER TABLE [RERule]   DROP CONSTRAINT '+name
              FROM sys.default_constraints A
              JOIN sysconstraints B
			  on A.parent_object_id = B.id
              WHERE id = OBJECT_ID('[RERule]')
              AND COL_NAME(id, colid)='formula'
              AND OBJECTPROPERTY(constid,'IsDefaultCnst')=1
            )
 EXEC (@Const)

 go
 
alter table RERule
alter column Formula nvarchar(max)  
go
 
ALTER TABLE [dbo].[RERule] 
ADD   DEFAULT (N'0') FOR [Formula]

go

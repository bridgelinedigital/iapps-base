IF(OBJECT_ID('vwObjectUrls') IS NOT NULL)
	DROP VIEW vwObjectUrls
GO

IF(OBJECT_ID('vwObjectUrls_Blogs') IS NOT NULL)
	DROP VIEW vwObjectUrls_Blogs
GO

IF(OBJECT_ID('vwPostUrl') IS NOT NULL)
	DROP VIEW vwPostUrl
GO

PRINT 'Modify view vwPostUrl'
GO

CREATE VIEW [dbo].[vwPostUrl] WITH SCHEMABINDING 
AS
SELECT B.Id AS Id, P.Id AS PostId,
	U.Url + '/' + CAST(YEAR(P.PostDate) AS varchar(4)) +  '/' + RIGHT('0' + CAST(DATEPART(MM, P.PostDate) AS varchar(2)), 2) +  '/' + LOWER(P.FriendlyName) AS Url,
	U.DraftUrl + '/' + CAST(YEAR(P.PostDate) AS varchar(4)) +  '/' + RIGHT('0' + CAST(DATEPART(MM, P.PostDate) AS varchar(2)), 2) +  '/' + LOWER(P.INWFFriendlyName) AS DraftUrl,
	B.ApplicationId AS SiteId
FROM dbo.BLPost P 
	JOIN dbo.BLBlog B ON B.Id = P.BlogId
	JOIN [dbo].[vwPageUrl] U ON B.BlogListPageId = U.Id
GO

PRINT 'Modify view vwObjectUrls_Blogs'
GO

CREATE VIEW [dbo].[vwObjectUrls_Blogs] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS
SELECT	SiteId, 39, Id, Id, LOWER([Url])
FROM	dbo.vwBlogUrl

UNION ALL

SELECT	SiteId, 40, PostId, PostId, LOWER([Url])
FROM	dbo.vwPostUrl
GO

PRINT 'Modify view vwObjectUrls'
GO

CREATE VIEW [dbo].[vwObjectUrls] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
AS
SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Menus

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Pages

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Blogs

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Files

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Products
GO

PRINT 'Add email template for blog comment reply notifications'
GO

IF NOT EXISTS (SELECT * FROM TAEmailTemplate WHERE [Key] = 'NewCommentReply')
BEGIN
	INSERT INTO TAEmailTemplate (Id, [Key], Title, [Description], [Subject], MimeType, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, [Status], ApplicationId, SenderDefaultEmail, MailMergeTags, ProductId, Body)
		VALUES ('59B0B815-3C02-41DF-9080-C9F17B98EAE6', 'NewCommentReply', 'New Comment Reply', 'New Comment Reply', 'New Comment Reply', 2, '00000000-0000-0000-0000-000000000000', GETUTCDATE(), '00000000-0000-0000-0000-000000000000', GETUTCDATE(), 1, '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', NULL, '_SITENAME_,_URL_,_COMMENT_', 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', '<p>The following reply has been submitted to a comment you made:</p><p>[COMMENT]</p><p>Please <a href="[URL]">click here</a> to view all comments.')
END
GO

PRINT 'Add site setting type for blog comment reply email template'
GO

IF NOT EXISTS (SELECT * FROM STSettingType WHERE [Name] = 'Comments.NewReplyEmailTemplateId')
BEGIN
	DECLARE @Sequence INT = (SELECT ISNULL(MAX(Sequence), 0) + 1 FROM STSettingType WHERE SettingGroupId = 1)

	INSERT INTO STSettingType ([Name], FriendlyName, SettingGroupId, [Sequence], ControlType, IsEnabled, IsVisible)
		VALUES ('Comments.NewReplyEmailTemplateId', 'Id of the email template used when a reply is posted to a comment', 1, @Sequence, 'Textbox', 1, 0)
END
GO

PRINT 'Add site setting value for blog comment reply email template'
GO

INSERT INTO STSiteSetting (SiteId, SettingTypeId, [Value])
	SELECT '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', T.Id, '59B0B815-3C02-41DF-9080-C9F17B98EAE6'
	FROM	STSettingType AS T
			LEFT JOIN STSiteSetting AS S ON S.SettingTypeId = T.Id
	WHERE	T.[Name] = 'Comments.NewReplyEmailTemplateId' AND
			S.Id IS NULL
GO

PRINT 'Create view vwTranslationDocumentItem'
GO

IF (OBJECT_ID('vwTranslationDocumentItem') IS NOT NULL)
	DROP VIEW vwTranslationDocumentItem	
GO

CREATE VIEW [dbo].[vwTranslationDocumentItem]
AS 
SELECT DI.Id,
	DI.TranslationDocumentId AS DocumentId,
	DI.SiteId,
	DI.Locale,
	DI.Status,
	DI.Content,
	DI.LogMessage,
	S.Title AS SiteName,
	DI.CreatedBy,
	DI.CreatedDate,
	DI.ModifiedBy,
	DI.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM TLDocumentItem DI
	JOIN SISite S ON S.Id = DI.SiteId
	LEFT JOIN VW_UserFullName FN on FN.UserId = DI.CreatedBy
	 LEFT JOIN VW_UserFullName MN on MN.UserId = DI.ModifiedBy
GO

PRINT 'Add function GetVariantObjects'
GO

IF (OBJECT_ID('GetVariantObjects') IS NOT NULL)
	DROP FUNCTION GetVariantObjects
GO

CREATE FUNCTION [dbo].[GetVariantObjects] (
	@ObjectType	INT = NULL
)
RETURNS @Objects TABLE 
(
	MasterObjectId	UNIQUEIDENTIFIER,
	ObjectId		UNIQUEIDENTIFIER,
	ObjectType		INT,
	SiteId			UNIQUEIDENTIFIER,
	Title			NVARCHAR(255),
	LeftValue		INT NULL,
	RightValue		INT NULL,
	CreatedDate		DATETIME,
	ModifiedDate	DATETIME
)
AS
BEGIN	
	IF (@ObjectType IS NULL OR @ObjectType = 2) -- Menus
	BEGIN
		WITH MenuCTE (MasterId, Id, SiteId, Title, LeftValue, RightValue, CreatedDate, ModifiedDate) AS (
			SELECT	PMN.PageMapNodeId, PMN.PageMapNodeId, PMN.SiteId, PMN.DisplayTitle, PMN.LftValue, PMN.RgtValue, PMN.CreatedDate, COALESCE(PMN.ModifiedDate, PMN.CreatedDate)
			FROM    PageMapNode AS PMN
					LEFT JOIN PageMapNode AS SPMN ON SPMN.PageMapNodeId = NULLIF(PMN.SourcePageMapNodeId, '00000000-0000-0000-0000-000000000000')
			WHERE   SPMN.PageMapNodeId IS NULL

			UNION ALL

			SELECT  CTE.MasterId, PMN.PageMapNodeId, PMN.SiteId, PMN.DisplayTitle, PMN.LftValue, PMN.RgtValue, PMN.CreatedDate, COALESCE(PMN.ModifiedDate, PMN.CreatedDate)
			FROM    PageMapNode AS PMN
					INNER JOIN MenuCTE AS CTE ON CTE.Id = PMN.SourcePageMapNodeId
		)
			INSERT INTO @Objects (MasterObjectId, ObjectId, ObjectType, SiteId, Title, LeftValue, RightValue, CreatedDate, ModifiedDate)
				SELECT	MasterId, Id, 2, SiteId, Title, LeftValue, RightValue, CreatedDate, ModifiedDate
				FROM	MenuCTE
	END

	IF (@ObjectType IS NULL OR @ObjectType = 7) -- Content Items
	BEGIN
		WITH ContentCTE (MasterId, Id, SiteId, Title, CreatedDate, ModifiedDate) AS (
			SELECT	C.Id, C.Id, C.ApplicationId, C.Title, C.CreatedDate, COALESCE(C.ModifiedDate, C.CreatedDate)
			FROM    COContent AS C
					LEFT JOIN COContent AS SC ON SC.Id = NULLIF(C.SourceContentId, '00000000-0000-0000-0000-000000000000')
			WHERE   SC.Id IS NULL

			UNION ALL

			SELECT  CTE.MasterId, C.Id, C.ApplicationId, C.Title, C.CreatedDate, COALESCE(C.ModifiedDate, C.CreatedDate)
			FROM    COContent AS C
					INNER JOIN ContentCTE AS CTE ON CTE.Id = C.SourceContentId
		)
			INSERT INTO @Objects (MasterObjectId, ObjectId, ObjectType, SiteId, Title, CreatedDate, ModifiedDate)
				SELECT	MasterId, Id, 7, SiteId, Title, CreatedDate, ModifiedDate
				FROM	ContentCTE
	END

	IF (@ObjectType IS NULL OR @ObjectType = 8) -- Pages
	BEGIN
		WITH PageCTE (MasterId, Id, SiteId, Title, CreatedDate, ModifiedDate) AS (
			SELECT	PD.PageDefinitionId, PD.PageDefinitionId, PD.SiteId, PD.Title, PD.CreatedDate, COALESCE(PD.ModifiedDate, PD.CreatedDate)
			FROM    PageDefinition AS PD
					LEFT JOIN PageDefinition AS SPD ON SPD.PageDefinitionId = NULLIF(PD.SourcePageDefinitionId, '00000000-0000-0000-0000-000000000000')
			WHERE   SPD.PageDefinitionId IS NULL

			UNION ALL

			SELECT  CTE.MasterId, PD.PageDefinitionId, PD.SiteId, PD.Title, PD.CreatedDate, COALESCE(PD.ModifiedDate, PD.CreatedDate)
			FROM    PageDefinition AS PD
					INNER JOIN PageCTE AS CTE ON CTE.Id = PD.SourcePageDefinitionId
		)
			INSERT INTO @Objects (MasterObjectId, ObjectId, ObjectType, SiteId, Title, CreatedDate, ModifiedDate)
				SELECT	MasterId, Id, 8, SiteId, Title, CreatedDate, ModifiedDate
				FROM	PageCTE
	END

	IF (@ObjectType IS NULL OR @ObjectType = 38) -- Forms
	BEGIN
		WITH FormCTE (MasterId, Id, SiteId, Title, CreatedDate, ModifiedDate) AS (
			SELECT	F.Id, F.Id, F.ApplicationId, F.Title, F.CreatedDate, COALESCE(F.ModifiedDate, F.CreatedDate)
			FROM    Forms AS F
					LEFT JOIN Forms AS SF ON SF.Id = NULLIF(F.SourceFormId, '00000000-0000-0000-0000-000000000000')
			WHERE   SF.Id IS NULL

			UNION ALL

			SELECT  CTE.MasterId, F.Id, F.ApplicationId, F.Title, F.CreatedDate, COALESCE(F.ModifiedDate, F.CreatedDate)
			FROM    Forms AS F
					INNER JOIN FormCTE AS CTE ON CTE.Id = F.SourceFormId
		)
			INSERT INTO @Objects (MasterObjectId, ObjectId, ObjectType, SiteId, Title, CreatedDate, ModifiedDate)
				SELECT	MasterId, Id, 38, SiteId, Title, CreatedDate, ModifiedDate
				FROM    FormCTE
	END

	IF (@ObjectType IS NULL OR @ObjectType = 51) -- Attributes
	BEGIN
		INSERT INTO @Objects (MasterObjectId, ObjectId, ObjectType, SiteId, Title, CreatedDate, ModifiedDate)
			SELECT	DISTINCT Id, Id, 51, SiteId, Title, CreatedDate, COALESCE(ModifiedDate, CreatedDate)
			FROM    vwAttribute
	END

	IF (@ObjectType IS NULL OR @ObjectType = 204) -- Product Types
	BEGIN
		INSERT INTO @Objects (MasterObjectId, ObjectId, ObjectType, SiteId, Title, CreatedDate, ModifiedDate)
			SELECT	DISTINCT Id, Id, 204, SiteId, Title, CreatedDate, COALESCE(ModifiedDate, CreatedDate)
			FROM    vwProductType
	END

	IF (@ObjectType IS NULL OR @ObjectType = 205) -- Products
	BEGIN
		INSERT INTO @Objects (MasterObjectId, ObjectId, ObjectType, SiteId, Title, CreatedDate, ModifiedDate)
			SELECT	DISTINCT Id, Id, 205, SiteId, Title, CreatedDate, COALESCE(ModifiedDate, CreatedDate)
			FROM    vwProduct
	END

	IF (@ObjectType IS NULL OR @ObjectType = 230) -- Email Templates
	BEGIN
		INSERT INTO @Objects (MasterObjectId, ObjectId, ObjectType, SiteId, Title, CreatedDate, ModifiedDate)
			SELECT	DISTINCT Id, Id, 230, SiteId, Title, CreatedDate, COALESCE(ModifiedDate, CreatedDate)
			FROM    vwEmailTemplate
	END
	
	RETURN 
END
GO

PRINT 'Add stored procedure Translation_GetDocuments'
GO

IF (OBJECT_ID('Translation_GetDocuments') IS NOT NULL)
	DROP PROCEDURE Translation_GetDocuments
GO

CREATE PROCEDURE [dbo].[Translation_GetDocuments]
(
	@BatchSize			int,
	@Status				int = NULL,
	@ProcessingStatus	int = NULL,
	@LastUpdatedDate	datetime = NULL
)
AS
BEGIN
	DECLARE @tbDocument TABLE (Id uniqueidentifier primary key)

	IF @ProcessingStatus = 1 --Not processed
	BEGIN
		INSERT INTO @tbDocument
		SELECT TOP (@BatchSize) Id FROM TLDocument
		WHERE (@ProcessingStatus IS NULL OR ProcessingStatus = @ProcessingStatus)
			AND (@Status IS NULL OR Status = @Status)
		ORDER BY CreatedDate

		UPDATE TLDocument
		SET ProcessingStatus = 2 --Processing
		WHERE Id IN (SELECT Id FROM @tbDocument)

		SELECT T.Id,
			T.TranslationId,
			T.Title,
			T.ObjectId,
			T.ObjectType,
			T.ObjectUrl,
			T.Status,
			T.ProcessingStatus,
			T.SiteId,
			S.DefaultTranslateToLanguage AS Locale,
			T.ExternalId,
			T.CreatedBy AS CreatedById,
			FN.UserFullName CreatedByFullName,
			GETUTCDATE() AS CreatedDate
		FROM vwTranslationDocument T
			JOIN @tbDocument D ON D.Id = T.Id
			JOIN SISite S ON S.Id = T.SiteId
			LEFT JOIN VW_UserFullName FN on FN.UserId = T.ModifiedBy

		SELECT D.TranslationDocumentId AS DocumentId,
			D.SiteId,
			S.DefaultTranslateToLanguage AS Locale
		FROM vwTranslationDocumentSite D
			JOIN @tbDocument C ON C.Id = D.TranslationDocumentId
			JOIN SISite S ON S.Id = D.SiteId
		WHERE S.SubmitToTranslation = 1
	END
	ELSE
	BEGIN
		INSERT INTO @tbDocument
		SELECT TOP (@BatchSize) Id FROM vwTranslationDocument
		WHERE (@ProcessingStatus IS NULL OR ProcessingStatus = @ProcessingStatus)
			AND (@Status IS NULL OR Status = @Status)
			AND (@LastUpdatedDate IS NULL OR ModifiedDate < @LastUpdatedDate)
		ORDER BY ModifiedDate

		SELECT T.Id,
			T.TranslationId,
			T.Title,
			T.ObjectId,
			T.ObjectType,
			T.ObjectUrl,
			T.Status,
			T.ProcessingStatus,
			T.SiteId,
			T.Locale,
			T.ExternalId,
			T.Content,
			T.CreatedBy AS CreatedById,
			T.CreatedByFullName,
			T.CreatedDate
		FROM vwTranslationDocument T
			JOIN @tbDocument C ON C.Id = T.Id

		SELECT T.Id,
			T.TranslationDocumentId AS DocumentId,
			T.SiteId,
			T.Locale,
			T.Status,
			T.Content,
			T.LogMessage
		FROM TLdocumentItem T
			JOIN @tbDocument D ON D.Id = T.TranslationDocumentId
	END
END
GO

PRINT 'Adding Unbound Tranlation to iAppsProductSuite'
GO

IF NOT EXISTS (SELECT Id FROM iAppsProductSuite WHERE Id = 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4')
	INSERT INTO iAppsProductSuite(Id, Title, [Description], Url, ProductVersion, UpdateStartDate, UpdateEndDate)
		SELECT 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4', 'Translation', 'Unbound Translation', '/TranslationAdmin', ProductVersion, GETUTCDATE(), GETUTCDATE()
		FROM	iAppsProductSuite
		WHERE	Id = 'CBB702AC-C8F1-4C35-B2DC-839C26E39848'
GO

PRINT 'Add site setting type for Translation Product ID'
GO

IF NOT EXISTS (SELECT * FROM STSettingType WHERE [Name] = 'TranslationProductId')
BEGIN
	DECLARE @Sequence INT = (SELECT ISNULL(MAX(Sequence), 0) + 1 FROM STSettingType WHERE SettingGroupId = 1)

	INSERT INTO STSettingType ([Name], FriendlyName, SettingGroupId, [Sequence], ControlType, IsEnabled, IsVisible)
		VALUES ('TranslationProductId', 'Product ID for Unbound Translation', 1, @Sequence, 'Textbox', 1, 0)

	PRINT 'Migrating site license level to new enum'

	UPDATE	SISite
	SET		LicenseLevel = (
				CASE WHEN LicenseLevel IN (1, 5, 6, 7, 11, 12, 13, 15, 17, 18, 19, 20, 21, 22, 23, 24) THEN 1 ELSE 0 END + -- Content
				CASE WHEN LicenseLevel IN (2, 5, 8, 9, 11, 12, 14, 15, 18, 21, 22, 24, 25, 26, 27, 28) THEN 2 ELSE 0 END + -- Insights
				CASE WHEN LicenseLevel IN (3, 6, 8, 10, 11, 13, 14, 15, 19, 21, 23, 24, 26, 28, 29, 30) THEN 4 ELSE 0 END + -- Commerce
				CASE WHEN LicenseLevel IN (4, 7, 9, 10, 12, 13, 14, 15, 20, 22, 23, 24, 27, 28, 30, 31) THEN 8 ELSE 0 END + -- Marketing
				CASE WHEN LicenseLevel IN (16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31) THEN 16 ELSE 0 END -- Social
			)
	WHERE	LicenseLevel > 0
END
GO

PRINT 'Add site setting value for Translation Product ID'
GO

INSERT INTO STSiteSetting (SiteId, SettingTypeId, [Value])
	SELECT '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', T.Id, 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4'
	FROM	STSettingType AS T
			LEFT JOIN STSiteSetting AS S ON S.SettingTypeId = T.Id
	WHERE	T.[Name] = 'TranslationProductId' AND
			S.Id IS NULL
GO

PRINT 'Remove old translation groups from previous implementation'
GO

DELETE	MG
FROM	USGroup AS G
		INNER JOIN USMemberGroup AS MG ON MG.GroupId = G.Id
WHERE	G.Title IN ('Translation', 'TranslationAdmin')

DELETE
FROM	USGroup
WHERE	Title IN ('Translation', 'TranslationAdmin')
GO

PRINT 'Remove old translation roles from previous implementation'
GO

DELETE	MR
FROM	USMemberRoles AS MR
		INNER JOIN USRoles AS R ON R.Id = MR.RoleId
WHERE	R.[Name] IN ('Translation', 'TranslationAdmin') AND
		R.ApplicationId = 'CBB702AC-C8F1-4C35-B2DC-839C26E39848'

DELETE	
FROM	USRoles
WHERE	[Name] IN ('Translation', 'TranslationAdmin') AND
		ApplicationId = 'CBB702AC-C8F1-4C35-B2DC-839C26E39848'
GO

PRINT 'Add TranslationAdministrator role'
GO

IF NOT EXISTS (SELECT * FROM USRoles WHERE ApplicationId = 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4' AND [Name] = 'TranslationAdministrator')
BEGIN
	DECLARE @RoleId INT = (SELECT MAX(Id) FROM USRoles) + 1

	INSERT INTO USRoles (Id, ApplicationId, [Name], [Description], IsGlobal)
		VALUES (@RoleId, 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4', 'TranslationAdministrator', 'Translation Administrator for the system', 1)
END
GO

PRINT 'Add Translation Administrator group'
GO

IF NOT EXISTS (SELECT Id FROM USGroup WHERE Id = 'D9C950EC-E1EA-4EC8-8957-544E8706F291')
	INSERT INTO USGroup (Id, ProductId, ApplicationId, Title, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, [Status], GroupType)
	VALUES ('D9C950EC-E1EA-4EC8-8957-544E8706F291', 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4', 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4', 'Translation Administrator', GETUTCDATE(), '00000000-0000-0000-0000-000000000000', GETUTCDATE(), '00000000-0000-0000-0000-000000000000', 1, 1)
GO

PRINT 'Add Installation Administrator and Site Administrator users to Translation Administrator group'
GO

IF NOT EXISTS (SELECT * FROM USMemberGroup WHERE GroupId = 'D9C950EC-E1EA-4EC8-8957-544E8706F291')
BEGIN
	INSERT INTO USMemberGroup (ApplicationId, MemberId, MemberType, GroupId)
		SELECT	MG.ApplicationId, MG.MemberId, MG.MemberType, 'D9C950EC-E1EA-4EC8-8957-544E8706F291'
		FROM	USMemberGroup AS MG
				INNER JOIN USGroup AS G ON G.Id = MG.GroupId
		WHERE	G.Title IN ('Installation Administrator', 'Site Administrator')
		AND G.ProductId = 'CBB702AC-C8F1-4C35-B2DC-839C26E39848'
END
GO

PRINT 'Add TranslationAdministrator role to Translation Administrator group'
GO

IF NOT EXISTS (
	SELECT	*
	FROM	USMemberRoles AS MR
			INNER JOIN USRoles AS R ON R.Id = MR.RoleId
			INNER JOIN USGroup AS G ON G.Id = MR.MemberId
	WHERE	R.[Name] = 'TranslationAdministrator' AND
			R.ApplicationId = 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4'
)
BEGIN
	DECLARE @GroupId UNIQUEIDENTIFIER = (
		SELECT	Id
		FROM	USGroup
		WHERE	Title = 'Translation Administrator' AND
				ApplicationId = 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4'
	)

	DECLARE @RoleId INT = (
		SELECT	Id
		FROM	USRoles
		WHERE	[Name] = 'TranslationAdministrator' AND
				ApplicationId = 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4'
	)

	INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate)
		VALUES ('DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4', '8039ce09-e7da-47e1-bcec-df96b5e411f4', @GroupId, 2, @RoleId, 'DCFB4EDF-B7F1-4064-9ED2-9653B8E90FA4', 1, 1)
END
GO

PRINT 'Modify SourcePageMapNodeId index on PageMapNode'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'IX_PageMapNode_SourcePageMapNodeId' AND object_id = OBJECT_ID('dbo.PageMapNode'))
	DROP INDEX [IX_PageMapNode_SourcePageMapNodeId] ON PageMapNode
GO
CREATE NONCLUSTERED INDEX [IX_PageMapNode_SourcePageMapNodeId] ON [dbo].[PageMapNode] ([SourcePageMapNodeId], [SiteId])
	INCLUDE ([PageMapNodeId], [LftValue], [RgtValue], [DisplayTitle], [CreatedDate], [ModifiedDate])
GO

PRINT 'Modify stored procedure iAppsForm_GetiAppsForm'
GO

IF(OBJECT_ID('iAppsForm_GetiAppsForm') IS NOT NULL)
	DROP PROCEDURE iAppsForm_GetiAppsForm
GO

CREATE PROCEDURE [dbo].[iAppsForm_GetiAppsForm] 
(
	@Id				UNIQUEIDENTIFIER,
	@ApplicationId	UNIQUEIDENTIFIER
)
AS
BEGIN
	DECLARE @VariantId UNIQUEIDENTIFIER = (
		SELECT	TOP(1) Id
		FROM	Forms
		WHERE	SourceFormId = @Id AND
				ApplicationId = @ApplicationId AND
				[Status] != dbo.GetDeleteStatus()
	)

	SELECT		Id,
				ApplicationId,
				Title,
				[Description], 
				FormType,								
				CreatedDate,
				CreatedBy,
				ModifiedBy,
				ModifiedDate, 
				[Status],
				XMLString,				
				XSLTString,
				XSLTFileName,
				SendEmailYesNo,
				Email,
				ThankYouURL,
				SubmitButtonText,
				SubmitButtonDescription,
				PollResultType,	
				PollVotingType,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				AddAsContact,
				ContactEmailField,
				WatchId,
				F.EmailSubjectText,
				F.EmailSubjectField,
				F.SourceFormId,
				ParentId,
				ThankYouContent,
				F.CssClass,
				F.Captcha
	FROM		Forms AS F
				LEFT JOIN VW_UserFullName AS FN ON FN.UserId = F.CreatedBy
				LEFT JOIN VW_UserFullName AS MN ON MN.UserId = F.ModifiedBy
	WHERE		Id = COALESCE(@VariantId, @Id) AND
				[Status] != dbo.GetDeleteStatus()
	ORDER BY	Title			  
END
GO

PRINT 'Modify stored procedure FormDto_Import'
GO

IF(OBJECT_ID('FormDto_Import') IS NOT NULL)
	DROP PROCEDURE FormDto_Import
GO

CREATE PROCEDURE [dbo].[FormDto_Import]
(
	@Id						UNIQUEIDENTIFIER = NULL OUTPUT,
	@SourceId				UNIQUEIDENTIFIER = NULL,
	@TargetSiteId			UNIQUEIDENTIFIER = NULL,
	@ModifiedBy				UNIQUEIDENTIFIER
)
AS
BEGIN
	DECLARE @UtcNow		DATETIME,
			@ParentId	UNIQUEIDENTIFIER

	SET	@UtcNow = GETUTCDATE()

	IF (@Id IS NULL)
	BEGIN
		IF (@SourceId IS NULL)
			RAISERROR ('@Id and @SourceId cannot both be NULL', 16, 1, '')

		IF (@TargetSiteId IS NULL)
			RAISERROR ('@TargetSiteId cannot be NULL when @Id IS NULL', 16, 1, '')

		SET @Id = (SELECT [dbo].[Form_GetVariantId](@SourceId, @TargetSiteId))

		IF (EXISTS (SELECT * FROM SISite WHERE Id = @TargetSiteId AND (TranslationEnabled = 1 OR SubmitToTranslation = 1)))
			SELECT	TOP 1 @ParentId = ParentId
			FROM	Forms
			WHERE	Id = @SourceId
	
		IF (@ParentId IS NULL OR @ParentId = dbo.GetEmptyGUID())
			SELECT	TOP 1 @ParentId = Id
			FROM	COFormStructure
			WHERE	Title = 'Distributed Forms' AND
					SiteId = @TargetSiteId

		IF (@ParentId IS NULL)
			RAISERROR ('Shared Form folder does not exist', 16, 1, '')
	END

	IF (@Id IS NULL)
	BEGIN
		SET @Id = NEWID()

		INSERT INTO Forms 
		(
			Id,
			ApplicationId,
			Title,
			[Description],
			FormType,
			XMLString,
			XSLTString,
			XSLTFileName,
			[Status],
			CreatedBy,
			CreatedDate,
			CssClass,
			Captcha,
			ThankYouURL,
			ThankYouContent,
			Email,
			SendEmailYesNo,
			SubmitButtonText,
			SubmitButtonDescription,
			PollResultType,
			PollVotingType,
			AddAsContact,
			ContactEmailField,
			WatchId,
			EmailSubjectText,
			EmailSubjectField,
			SourceFormId,
			ParentId
		)
			SELECT	@Id,
					@TargetSiteId,
					Title,
					[Description],
					FormType,
					XMLString,
					XSLTString,
					XSLTFileName,
					[Status],
					ISNULL(ModifiedBy, CreatedBy),
					@UtcNow,
					CssClass,
					Captcha,
					ThankYouURL,
					ThankYouContent,
					Email,
					SendEmailYesNo,
					SubmitButtonText,
					SubmitButtonDescription,
					PollResultType,
					PollVotingType,
					AddAsContact,
					ContactEmailField,
					WatchId,
					EmailSubjectText,
					EmailSubjectField,
					Id,
					@ParentId
			FROM	Forms
			WHERE	Id = @SourceId
	END
	ELSE
	BEGIN
		UPDATE	V
		SET		V.Title = S.Title,
				V.[Description] = S.[Description],
				V.FormType = S.FormType,
				V.XMLString = S.XMLString,
				V.XSLTString = S.XSLTString,
				V.XSLTFileName = S.XSLTFileName,
				V.CssClass = S.CssClass,
				V.Captcha = S.Captcha,
				V.ThankYouURL = S.ThankYouURL,
				V.ThankYouContent = S.ThankYouContent,
				V.Email = S.Email,
				V.SendEmailYesNo = S.SendEmailYesNo,
				V.[Status] = S.[Status],
				V.SubmitButtonText = S.SubmitButtonText,
				V.SubmitButtonDescription = S.SubmitButtonDescription,
				V.PollResultType = S.PollResultType,
				V.PollVotingType = S.PollVotingType,
				V.AddAsContact = S.AddAsContact,
				V.ContactEmailField = S.ContactEmailField,
				V.WatchId = S.WatchId,
				V.EmailSubjectText = S.EmailSubjectText,
				V.EmailSubjectField = S.EmailSubjectField,
				V.ModifiedBy = ISNULL(S.ModifiedBy, S.CreatedBy),
				V.ModifiedDate = @UtcNow
		FROM	Forms AS S
				INNER JOIN Forms AS V ON V.SourceFormId = S.Id
		WHERE	V.Id = @Id
	END	
END
GO

PRINT 'Create table PageObjectReferences'
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PageObjectReferences'))
BEGIN
	CREATE TABLE [dbo].[PageObjectReferences] (
		[Id]			UNIQUEIDENTIFIER NOT NULL,
		[PageId]		UNIQUEIDENTIFIER NOT NULL,
		[ObjectId]      UNIQUEIDENTIFIER NOT NULL,
		[ObjectType]	INT NOT NULL,
		[ObjectSiteId]	UNIQUEIDENTIFIER NOT NULL, 
		CONSTRAINT [PK_PageObjectReferences] PRIMARY KEY ([Id])
	);

	CREATE NONCLUSTERED INDEX [IX_PageObjectReferences] ON [dbo].[PageObjectReferences] ([ObjectId], [ObjectType]) INCLUDE ([PageId], [ObjectSiteId]);

	INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
		SELECT	NEWID(), PageId, ContentId, ObjectTypeId, SiteId
		FROM	(
					SELECT	DISTINCT PageId, ContentId, ObjectTypeId, SiteId
					FROM	Content_AssetLinks
				) AS AssetLinks
END
GO

PRINT 'Remove stored procedure PageDto_SaveObjectReferences'
GO

IF(OBJECT_ID('PageDto_SaveObjectReferences') IS NOT NULL)
	DROP PROCEDURE PageDto_SaveObjectReferences
GO

PRINT 'Remove type TYObjectReference'
GO
IF TYPE_ID('TYObjectReference') IS NOT NULL
	DROP TYPE [dbo].[TYObjectReference]
GO

PRINT 'Add type TYObjectReference'
GO

CREATE TYPE [dbo].[TYObjectReference] AS TABLE
(
	ObjectId		UNIQUEIDENTIFIER,
	ObjectType		INT,
	ObjectSiteId	UNIQUEIDENTIFIER
)
GO

PRINT 'Add stored procedure PageDto_SaveObjectReferences'
GO

CREATE PROCEDURE [dbo].[PageDto_SaveObjectReferences]
(
	@PageId				UNIQUEIDENTIFIER,
	@ObjectReferences	TYObjectReference READONLY
)
AS
BEGIN
	DELETE
	FROM	PageObjectReferences
	WHERE	PageId = @PageId

	INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
		SELECT	NEWID(), @PageId, ObjectId, ObjectType, ObjectSiteId
		FROM	@ObjectReferences
END
GO

PRINT 'Modify stored procedure PageDto_GetPagesUsingObject'
GO

IF(OBJECT_ID('PageDto_GetPagesUsingObject') IS NOT NULL)
	DROP PROCEDURE PageDto_GetPagesUsingObject
GO

CREATE PROCEDURE [dbo].[PageDto_GetPagesUsingObject]  
(
	@ObjectType	INT,
	@ObjectId	UNIQUEIDENTIFIER,
	@PageSiteId	UNIQUEIDENTIFIER = NULL
)  
AS  
BEGIN  
	DECLARE @tbPageIds TABLE(
		PageId	UNIQUEIDENTIFIER
	)
	
	IF @ObjectType IN (7, 9, 13, 33) -- Content, AssetFile, XmlForm, AssetImage
	BEGIN
		INSERT INTO @tbPageIds (PageId)
			SELECT	PageId
			FROM	PageObjectReferences
			WHERE	ObjectId = @ObjectId AND
					ObjectType = @ObjectType
	END

	IF @ObjectType = 4 -- Style
	BEGIN
		INSERT INTO @tbPageIds (PageId)
			SELECT	P.PageDefinitionId
			FROM	PageDefinition AS P
					INNER JOIN SIPageStyle AS S ON S.PageDefinitionId = P.PageDefinitionId
			WHERE	S.StyleId = @ObjectId
	END

	IF @ObjectType = 35 -- Script
	BEGIN
		INSERT INTO @tbPageIds (PageId)
			SELECT	P.PageDefinitionId
			FROM	PageDefinition AS P
					INNER JOIN SIPageScript AS S ON S.PageDefinitionId = P.PageDefinitionId
			WHERE	S.ScriptId = @ObjectId
	END

	IF @ObjectType = 3 -- Template
	BEGIN
		INSERT INTO @tbPageIds (PageId)
			SELECT	PageDefinitionId
			FROM	PageDefinition
			WHERE	TemplateId = @ObjectId
	END
		
	SELECT		DISTINCT P.PageDefinitionId, P.TemplateId, P.SiteId, P.Title, P.FriendlyName,
				CASE WHEN P.PageStatus > 0 THEN PageStatus WHEN P.WorkflowState > 1 THEN 11 ELSE 1 END AS PageStatus,
				S.LftValue
	FROM		PageDefinition AS P
				INNER JOIN @tbPageIds AS T ON T.PageId = P.PageDefinitionId
				INNER JOIN SISite S ON S.Id = P.SiteId
				LEFT JOIN MKCampaignEmail AS CE ON CE.CMSPageId = P.PageDefinitionId
	WHERE		CE.Id IS NULL AND
				(@PageSiteId IS NULL OR P.SiteId = @PageSiteId) AND
				S.[Status] = 1
	ORDER BY	S.LftValue
END
GO

PRINT 'Modify stored procedure PageDefinition_SavePageDefinitionSegment'
GO

IF(OBJECT_ID('PageDefinition_SavePageDefinitionSegment') IS NOT NULL)
	DROP PROCEDURE PageDefinition_SavePageDefinitionSegment
GO

CREATE PROCEDURE [dbo].[PageDefinition_SavePageDefinitionSegment]
(
	@PageDefinitionId   uniqueidentifier,
	@ApplicationId		uniqueidentifier
	
)
AS
BEGIN
	DECLARE @ContainerContentXml XML
	DECLARE @PageDefinitionSegmentId uniqueidentifier
	
	IF NOT EXISTS(Select 1 from PageDefinition Where PageDefinitionId = @PageDefinitionId and SiteId = @ApplicationId)
	BEGIN
		RAISERROR('NOTEXISTS||PageDefinitionId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	
	DECLARE @DeskTopDeviceId uniqueidentifier
	DECLARE @DeskTopAudienceId uniqueidentifier
	SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@ApplicationId)
	SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@ApplicationId)
	
	DECLARE @VersionId uniqueidentifier
	SELECT TOP (1) @VersionId = Id  from VEVersion A
	WHERE ObjectId=@PageDefinitionId and ObjectTypeId=8 and ApplicationId = @ApplicationId
	ORDER BY RevisionNumber DESC, VersionNumber DESC	
	
	-- Insert into PageDefinitionSegment if there is no record for the pagedefintionSegment 
	--for that Device and Audience Segment, if its present update the record
	Declare @pageDefSegmentInsertIds table(Id uniqueidentifier)	
	Declare @pageDefSegmentDeleteIds table(Id uniqueidentifier)	
	Declare @pageDefSegmentIds table(Id uniqueidentifier)	
	--Getting the latest page definition segment version for all the device
	
	;With PageDefSegmentDeleteCTE AS
	(	
		SELECT [Id],DeviceID,AudienceSegmentId
			  ,ROW_NUMBER() over (Partition BY DeviceId, AudienceSegmentId Order by DeviceId, AudienceSegmentId) as pdfRank
		  FROM PageDefinitionSegment PS
		  WHERE PageDefinitionId = @PageDefinitionId
		  AND NOT EXISTS (SELECT Id from [VEPageDefSegmentVersion] VPS where 
		  VPS.PageDefinitionId = PS.PageDefinitionId AND 
		  VPS.DeviceID = PS.DeviceId AND
		  VPS.AudienceSegmentId = PS.AudienceSegmentId)
	)
	
	DELETE FROM PageDefinitionSegment WHERE Id IN(
	SELECT Id FROM PageDefSegmentDeleteCTE P1 where pdfRank = 1
	AND Id NOT IN (SELECT Id FROM PageDefSegmentDeleteCTE P2 WHERE 
			P2.DeviceId = @DeskTopDeviceId
			AND P2.AudienceSegmentId = @DeskTopAudienceId))
	
	
	;With PageDefSegmentCTE AS
	(	
		SELECT [Id],DeviceID,AudienceSegmentId
			  ,ROW_NUMBER() over (Partition BY DeviceId, AudienceSegmentId Order by MinorVersionNumber Desc) as pdfRank
		  FROM [VEPageDefSegmentVersion] VES
		  WHERE VersionId = @VersionId
		  AND NOT EXISTS (SELECT Id from PageDefinitionSegment PDFS where 
		  VES.PageDefinitionId = PDFS.PageDefinitionId AND 
		  VES.DeviceID = PDFS.DeviceId AND
		  VES.AudienceSegmentId = PDFS.AudienceSegmentId)
	)

	INSERT INTO @pageDefSegmentInsertIds  
	SELECT Id FROM PageDefSegmentCTE P1 where pdfRank = 1
	AND Id NOT IN (SELECT Id FROM PageDefSegmentCTE P2 WHERE 
			P2.DeviceId = @DeskTopDeviceId
			AND P2.AudienceSegmentId = @DeskTopAudienceId)
	
	--select * from @pageDefSegmentInsertIds
	-- @pageDefSegmentInsertIds will not have row which are already in PageDefinitionSegment table
	
	INSERT INTO PageDefinitionSegment
	SELECT NEWID()
		  ,VES.[PageDefinitionId]
		  ,VES.[DeviceId]
		  ,VES.[AudienceSegmentId]
		  ,VES.[UnmanagedXml]
		  ,VES.[ZonesXml],
		  NULL,
		  NULL
	FROM [VEPageDefSegmentVersion] VES
	WHERE VES.Id IN (SELECT Id from @pageDefSegmentInsertIds)
	
	;With PageDefSegmentUpdateCTE AS
	(	
		SELECT [Id],DeviceID,AudienceSegmentId
			  ,ROW_NUMBER() over (Partition BY DeviceId, AudienceSegmentId Order by MinorVersionNumber Desc) as pdfRank
		  FROM [VEPageDefSegmentVersion] VES
		  WHERE VersionId = @VersionId
	)
	
	INSERT INTO @pageDefSegmentIds  
	SELECT Id FROM PageDefSegmentUpdateCTE P1 where pdfRank = 1
			AND Id NOT IN (SELECT Id FROM PageDefSegmentUpdateCTE P2 WHERE 
			P2.DeviceId = @DeskTopDeviceId
			AND P2.AudienceSegmentId = @DeskTopAudienceId)
			
	--select * from @pageDefSegmentIds		
	
	-- Update the PageDefintionSegment which is not inserted
	UPDATE PDFS Set
		PDFS.UnmanagedXml = VES.UnmanagedXml,
		PDFS.ZonesXml = VES.ZonesXml
	FROM PageDefinitionSegment PDFS
	INNER JOIN VEPageDefSegmentVersion VES ON PDFS.PageDefinitionId = VES.PageDefinitionId 
	AND PDFS.DeviceID = VES.DeviceID
	AND PDFS.AudienceSegmentId = VES.AudienceSegmentId
	AND VES.Id IN (SELECT Id from @pageDefSegmentIds)
	AND VES.Id NOT IN (SELECT Id from @pageDefSegmentInsertIds)
	
	--Insert in PageDefinitionContainer if there is no record for PageDefinitionSegmentId else update it
	--select * from @pageDefSegmentIds
				
	INSERT INTO [PageDefinitionContainer]
		   (
			[PageDefinitionSegmentId]
		   ,[PageDefinitionId]
		   ,[ContainerId]
		   ,[ContentId]
		   ,[InWFContentId]
		   ,[ContentTypeId]
		   ,[IsModified]
		   ,[IsTracked]
		   ,[Visible]
		   ,[InWFVisible]
		   ,[DisplayOrder]
		   ,[InWFDisplayOrder]
		   ,[CustomAttributes]
		   ,[ContainerName])
		Select Id,PageDefinitionId
			,P.n.value('@id','uniqueidentifier') as ContainerId
			,P.n.value('@contentId','uniqueidentifier') as ContentId
			,P.n.value('@inWFContentId','uniqueidentifier') as inWFContentId
			,P.n.value('@contentTypeId','int') as contentTypeId
			,P.n.value('@isModified','bit') as isModified
			,P.n.value('@isTracked','bit') as isTracked
			,P.n.value('@visible','bit') as Visible
			,P.n.value('@inWFVisible','bit') as InWFVisible
			,P.n.value('@displayOrder','int') as DisplayOrder
			,P.n.value('@inWFDisplayOrder','int') as InWFDisplayOrder
			,dbo.GetCustomAttributeXml(P.n.query('.'),'container') as CustomAttributes
			,P.n.value('@name','nvarchar(100)') as ContainerName
		FROM
		(
			SELECT PDFS.Id,PDFS.PageDefinitionId,VES.ContainerContentXML
			from VEPageDefSegmentVersion VES
			INNER JOIN PageDefinitionSegment PDFS ON PDFS.PageDefinitionId = VES.PageDefinitionId
			AND PDFS.DeviceId = VES.DeviceID
			AND PDFS.AudienceSegmentId = VES.AudienceSegmentId
			WHERE
			VES.Id IN (SELECT Id from @pageDefSegmentIds)
			AND PDFS.Id NOT IN (Select PageDefinitionSegmentId from PageDefinitionContainer)
		) X
		CROSS APPLY ContainerContentXML.nodes('/Containers/container') AS P(n)
		
		
		
		UPDATE PC Set  
		--Select 
		   [ContentId] = P.n.value('@contentId','uniqueidentifier')
		  ,[InWFContentId] = P.n.value('@inWFContentId','uniqueidentifier')
		  ,[ContentTypeId] = P.n.value('@contentTypeId','int') 
		  ,[IsModified] = P.n.value('@isModified','bit')
		  ,[IsTracked] = P.n.value('@isTracked','bit')
		  ,[Visible] = P.n.value('@visible','bit')
		  ,[InWFVisible] = P.n.value('@inWFVisible','bit')
		  ,[CustomAttributes] = dbo.GetCustomAttributeXml(P.n.query('.'),'container')
		  ,[DisplayOrder] = P.n.value('@displayOrder','int')
		  ,[InWFDisplayOrder] = P.n.value('@inWFDisplayOrder','int')
		  FROM 
		  (
			SELECT PDFS.Id,PDFS.PageDefinitionId,VES.ContainerContentXML from VEPageDefSegmentVersion VES
			INNER JOIN PageDefinitionSegment PDFS ON PDFS.PageDefinitionId = VES.PageDefinitionId
			AND PDFS.DeviceId = VES.DeviceID
			AND PDFS.AudienceSegmentId = VES.AudienceSegmentId
			WHERE
			VES.Id IN (SELECT Id from @pageDefSegmentIds)
			AND PDFS.Id NOT IN (Select Id from @pageDefSegmentInsertIds)
		  ) X
		CROSS APPLY ContainerContentXml.nodes('/Containers/container') P(n)
		INNER JOIN PageDefinitionContainer PC ON PC.PageDefinitionId = X.PageDefinitionId AND PC.PageDefinitionSegmentId = X.Id
		WHERE [ContainerId] = P.n.value('@id','uniqueidentifier')

		UPDATE D SET D.PageSegmentCount = T.Cnt
		FROM PageDefinition D
		INNER JOIN (SELECT PageDefinitionId, Count(PageDefinitionId) cnt FROM PageDefinitionSegment
		Where PageDefinitionId=@PageDefinitionId
		Group by PageDefinitionId
		) T
		ON T.PageDefinitionId =D.PageDefinitionId
END
GO

PRINT 'Modify stored procedure PageDto_Save'
GO

IF(OBJECT_ID('PageDto_Save') IS NOT NULL)
	DROP PROCEDURE PageDto_Save
GO

CREATE PROCEDURE [dbo].[PageDto_Save]
(
	@Id				uniqueidentifier = NULL,
	@ParentId					uniqueidentifier = NULL,
	@SiteId						uniqueidentifier = NULL,
	@Title				nvarchar(max) = NULL,
	@Description				nvarchar(max) = NULL,
	@FriendlyName				nvarchar(max) = NULL,
	@PageStatus					int = NULL,
	@WorkflowState				int =  NULL,
	@TemplateId			uniqueidentifier = NULL,
	@TemplateName nvarchar(max) = NULL,
	@Author uniqueidentifier = NULL,
	@DisplayOrder int = NULL,
	@WorkflowId		uniqueidentifier = NULL,
	@ArchiveDate datetime = NULL,
	@PublishDate datetime = NULL,
	@StatusChangedDate datetime = NULL,
	@PublishCount int = NULL,
	@IsInWorkflow BIT = NULL,
	@ScheduledPublishDate datetime = NULL,
	@NextVersionToPublish uniqueidentifier = NULL,
	@ScheduledArchiveDate datetime = NULL,
	@OutputCachingEnabled BIT = NULL,
	@OutputCacheProfileName nvarchar(max) = NULL,
	@SourcePageDefinitionId uniqueidentifier = NULL,
	@ExcludeFromSearch BIT = NULL,
	@IsDefault bit = NULL,
	@IsTracked bit = NULL,
	@HasForms bit = NULL,
	@TextContentCounter int = NULL,
	@IsInGroupPublish BIT = NULL,
	@SecurityLevels xml = NULL,
	@Styles xml = NULL,
	@Scripts xml = NULL,
	@Segments xml = NULL,
	@CustomAttributes			xml = NULL,
	@ModifiedBy					uniqueidentifier = NULL,
	@PublishPage BIT = NULL,
	@UpdatePageName bit = null,
	@ZonesXml			xml = null,
	@CurrentPageSegment xml = NULL,
	@PageXml xml = null
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	IF(@PublishPage = 1)
		SET @UpdatePageName = 1
		
	DECLARE @pageDefinitionSegmentIds table(Id uniqueidentifier)
	
	/*
	Always save to version; 
	If the page is published then update live tables also.
	*/
	
	
	
	DECLARE @PageDefinitionSegmentId uniqueidentifier 
	
	IF @PublishPage = 1
		SET @PublishDate = GETUTCDATE()
	ELSE
		SET @PublishDate = @PublishDate
	
	
	DECLARE @ActionType int
	SET @ActionType = 2
	
	
	--Parse the segment xml and construct the containers, unmanaged xml values
	DECLARE @SegmentTable as Table (Id Uniqueidentifier, DeviceId Uniqueidentifier, AudienceSegmentId uniqueidentifier, UnmanagedXml xml, MetaTagsXml xml)
	DECLARE @ContainersTable as Table (ContentId Uniqueidentifier, InWFContentId Uniqueidentifier,ContentTypeId INT, IsModified BIT, IsTracked BIT, Visible BIT,InWFVisible BIT,
	CustomAttributes xml,DisplayOrder INT,InWFDisplayOrder INT )
	
	insert into @SegmentTable
	select tab.col.value('(Id)[1]','uniqueidentifier'),
	tab.col.value('(DeviceId)[1]','uniqueidentifier') ,
	tab.col.value('(AudienceSegmentId)[1]','uniqueidentifier') ,
	tab.col.query('(UnmanagedXml/unmanagedPageContents)'),
	tab.col.query('(MetaTagsXml)')
	from @CurrentPageSegment.nodes('CurrentPageSegment') tab(col)
	
	
	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
	BEGIN
		SET @Id = NEWID()
		
		SET @ActionType = 1
		
		
		IF @PublishPage = 1
			SET @PublishCount = 1
		ELSE
			SET @PublishCount = 0
		
		-- Create page definitioin
		INSERT INTO PageDefinition
		(
			PageDefinitionId,
			TemplateId,
			SiteId,
			Title,
			Description,
			PageStatus,
			WorkflowState,
			PublishCount,
			publishDate,
			FriendlyName,
			WorkflowId,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			ArchivedDate,
			StatusChangedDate,
			AuthorId,
			EnableOutputCache,
			OutputCacheProfileName,
			ExcludeFromSearch,
			IsDefault,
			IsTracked,
			HasForms,
			IsInGroupPublish,
			ScheduledPublishDate,
			ScheduledArchiveDate,
			NextVersionToPublish,
			TextContentCounter,
			SourcePageDefinitionId,
			CustomAttributes
		)
		VALUES
		(
			@Id,
			@TemplateId,
			@SiteId,
			@Title,
			@Description,
			@PageStatus,
			@WorkflowState,
			@PublishCount,
			@publishDate,
			@FriendlyName,
			@WorkflowId,
			@Author,
			@UtcNow,
			NULL,
			NULL,
			@ArchiveDate,
			@StatusChangedDate,
			@Author,
			@OutputCachingEnabled,
			@OutputCacheProfileName,
			@ExcludeFromSearch,
			@IsDefault,
			@IsTracked,
			@HasForms,
			@IsInGroupPublish,
			@ScheduledPublishDate,
			@ScheduledArchiveDate,
			@NextVersionToPublish,
			@TextContentCounter,
			@SourcePageDefinitionId,
			@CustomAttributes
		)

		--Update the pagemapnodepage def table 
		
		DECLARE @CurrentDisplayOrder int
		SELECT @CurrentDisplayOrder = ISNULL(MAX(DisplayOrder),0) FROM PageMapNodePageDef M WHERE M.PageMapNodeId = @ParentId
		IF @CurrentDisplayOrder IS NULL
			SET @CurrentDisplayOrder = 1
			
		IF NOT EXISTS (Select 1 from PageMapNodePageDef Where PageDefinitionId =@Id AND PageMapNodeId=@ParentId)
			INSERT INTO PageMapNodePageDef(PageDefinitionId,PageMapNodeId,DisplayOrder)
			VALUES (@Id,@ParentId,@CurrentDisplayOrder + 1)
		
		--Update the page definition segments
		INSERT INTO PageDefinitionSegment(
			Id
			,PageDefinitionId
			,DeviceId
			,AudienceSegmentId
			,UnmanagedXml
			,ZonesXml
			)
			VALUES
			(
			NEWID()
			,@Id
			,dbo.GetDesktopDeviceId(@SiteId)
			,dbo.GetDesktopAudienceSegmentId(@SiteId),
			--,isnull(@UnmanagedContentXml,'<unmanagedPageContents/>')
			(SELECT TOP 1 UnmanagedXml FROM @SegmentTable)
			,@ZonesXml
			)
		
		
		--Associate the containers
		
		--Create the Contents
		

	END
	ELSE
	BEGIN
	
	
	SELECT @PublishCount = PublishCount FROM PageDefinition WHERE PageDefinitionId = @Id
	
	IF @PublishPage = 1
	BEGIN
			SELECT @PublishCount = @PublishCount + 1
	
	
		UPDATE PageDefinition
		SET TemplateId = @TemplateId,
		SiteId = ISNULL(@SiteId, SiteId),
		Title = ISNULL(@Title,Title),
		[Description] = ISNULL(@Description, [Description]),
		PageStatus = ISNULL(@PageStatus,PageStatus),
		WorkflowState = ISNULL(@WorkflowState,WorkflowState),
		PublishCount = ISNULL(@PublishCount,PublishCount),
		publishDate = ISNULL(@publishDate,publishDate),
		FriendlyName = ISNULL(@FriendlyName,FriendlyName),
		WorkflowId = ISNULL(@WorkflowId,WorkflowId),
		ModifiedBy = ISNULL(@Author,ModifiedBy),
		ModifiedDate = @UtcNow,
		ArchivedDate = ISNULL(@ArchiveDate,ArchivedDate),
		StatusChangedDate = ISNULL(@StatusChangedDate,StatusChangedDate),
		AuthorId = ISNULL(@Author,AuthorId),
		EnableOutputCache = ISNULL(@OutputCachingEnabled,EnableOutputCache),
		OutputCacheProfileName = ISNULL(@OutputCacheProfileName,OutputCacheProfileName),
		ExcludeFromSearch = ISNULL(@ExcludeFromSearch,ExcludeFromSearch),
		IsDefault = ISNULL(@IsDefault,IsDefault),
		IsTracked = ISNULL(@IsTracked,IsTracked),
		HasForms = ISNULL(@HasForms,HasForms),
		IsInGroupPublish = ISNULL(@IsInGroupPublish,IsInGroupPublish),
		ScheduledPublishDate = ISNULL(@ScheduledPublishDate,ScheduledPublishDate),
		ScheduledArchiveDate = ISNULL(@ScheduledArchiveDate,ScheduledArchiveDate),
		NextVersionToPublish =  ISNULL(@NextVersionToPublish,NextVersionToPublish),
		TextContentCounter = ISNULL(@TextContentCounter,TextContentCounter),
		SourcePageDefinitionId = ISNULL(@SourcePageDefinitionId,SourcePageDefinitionId),
		CustomAttributes = ISNULL(@CustomAttributes,CustomAttributes)
		WHERE PageDefinitionId = @Id
		
		--Update the page definition segments
		DECLARE @DeskTopDeviceId uniqueidentifier
		DECLARE @DeskTopAudienceId uniqueidentifier
		SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@SiteId)
		SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@SiteId)
		
		SELECT @PageDefinitionSegmentId = Id 
			FROM PageDefinitionSegment WHERE DeviceId = @DeskTopDeviceId AND 
				AudienceSegmentId = @DeskTopAudienceId AND
				PageDefinitionId = @Id
	
		UPDATE PageDefinitionSegment Set 
			UnmanagedXml = (SELECT TOP 1 UnmanagedXml FROM @SegmentTable ),
			ZonesXml = isnull(@ZonesXml, ZonesXml)
		WHERE PageDefinitionId = @Id AND Id = @PageDefinitionSegmentId
	  
		--Update the Containers [PageDefinitionContainer] update and insert new containers
	 
	 END

		
	END
	
	-- Only the publish action updates the live tables ; otherwise it goes to version
	IF (@PublishPage = 1)
	BEGIN
		--Update the styles SIPageStyle
		DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	
		INSERT INTO SIPageStyle
		select @Id,tab.col.value('(@Id)[1]','uniqueidentifier'), NULL from @Styles.nodes('Styles/style') tab(col)
	
		--Update the scripts  SIPageScript
		DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	
		INSERT INTO SIPageScript 
		select @Id,tab.col.value('(@Id)[1]','uniqueidentifier'),NULL from @Scripts.nodes('Scripts/script') tab(col)
	
		--Associate the security levels USSecurityLevelObject
		DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	
		INSERT INTO USSecurityLevelObject
		select @Id,tab.col.value('(@Id)[1]','int'),NULL from @SecurityLevels.nodes('SecurityLevels/securityLevel') tab(col)

	
		EXEC [PageDefinition_AdjustDisplayOrder] @ParentId
	
		EXEC PageMapBase_UpdateLastModification @SiteId
	
		EXEC PageMap_UpdateHistory @SiteId, @Id, 8, @ActionType
	END

	--Updates the version always; irrespective of the action

	EXEC PageDto_SaveVersion @Id,	@ParentId,	@SiteId,@Title,	@Description,@FriendlyName,	@PageStatus,	@WorkflowState,	@TemplateId,	@TemplateName ,	@Author ,	@DisplayOrder ,	@WorkflowId,
	@ArchiveDate ,	@PublishDate ,	@StatusChangedDate ,	@PublishCount ,	@IsInWorkflow ,	@ScheduledPublishDate ,	@NextVersionToPublish ,	@ScheduledArchiveDate ,	@OutputCachingEnabled ,	@OutputCacheProfileName ,
	@SourcePageDefinitionId ,	@ExcludeFromSearch ,	@IsDefault ,	@IsTracked ,	@HasForms ,	@TextContentCounter ,	@IsInGroupPublish ,	@SecurityLevels ,	@Styles ,	@Scripts ,	@Segments ,
	@CustomAttributes,	@ModifiedBy,	@PublishPage ,	@UpdatePageName ,	@ZonesXml,	@CurrentPageSegment ,	NULL 


END
GO

PRINT 'Modify stored procedure PageMap_SavePageDefinition'
GO

IF(OBJECT_ID('PageMap_SavePageDefinition') IS NOT NULL)
	DROP PROCEDURE PageMap_SavePageDefinition
GO

CREATE PROCEDURE [dbo].[PageMap_SavePageDefinition]
(
	@Id					uniqueidentifier output,
	@SiteId				uniqueidentifier,
	@ParentNodeId		uniqueidentifier,
	@CreatedBy			uniqueidentifier,
	@ModifiedBy			uniqueidentifier,
	@PublishPage		bit,
	@PageDefinitionXml	xml,
	@ContainerXml		xml = null,
	@UnmanagedContentXml xml = null,
	@ZonesXml			xml = null,
	@UpdatePageName bit = null
)
AS
BEGIN
	
	IF(@PublishPage = 1)
		SET @UpdatePageName = 1
		
	IF(@UpdatePageName IS NULL)
		SET @UpdatePageName = 0
	
		
	IF NOT EXISTS(Select 1 from PageMapNode Where PageMapNodeId = @ParentNodeId and SiteId = @SiteId)
	BEGIN
		RAISERROR('NOTEXISTS||ParentId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	DECLARE @pageDefinitionSegmentIds table(Id uniqueidentifier)	
	DECLARE @PublishCount int
	DECLARE @PublishDate datetime
	DECLARE @PageDefinitionSegmentId uniqueidentifier 
	
	IF @PublishPage = 1
		SET @PublishDate = GETUTCDATE()
	ELSE
		SET @PublishDate = @PageDefinitionXml.value('(pageDefinition/@publishDate)[1]','datetime')
		
	DECLARE @ActionType int
	SET @ActionType = 2
	IF @Id IS NULL OR (Select count(*) FROM PageDefinition Where PageDefinitionId=@Id)=0 -- New Page
	BEGIN
		SET @ActionType = 1
		-- get the highest value of the DisplayOrder currently in the DB for this menuId
		DECLARE @CurrentDisplayOrder int
		SELECT @CurrentDisplayOrder = ISNULL(MAX(DisplayOrder),0) FROM PageMapNodePageDef M WHERE M.PageMapNodeId = @ParentNodeId
		IF @CurrentDisplayOrder IS NULL
			SET @CurrentDisplayOrder = 1
		
		IF @PublishPage = 1
			SET @PublishCount = 1
		ELSE
			SET @PublishCount = 0
		IF @Id IS NULL 	
			SET @Id = NEWID()

		IF NOT EXISTS (Select 1 from PageDefinition Where PageDefinitionId =@Id)
			INSERT INTO PageDefinition(PageDefinitionId, SiteId, TemplateId, Title, Description, 
						PublishDate, PageStatus, WorkflowState, PublishCount, --ContainerXml,UnmanagedContentXml,
						FriendlyName, WorkflowId,
						CreatedBy, CreatedDate, ArchivedDate, StatusChangedDate, AuthorId, EnableOutputCache,
						OutputCacheProfileName, ExcludeFromSearch,ExcludeFromExternalSearch, ExcludeFromTranslation,
						IsDefault, IsTracked, HasForms, TextContentCounter, 
						CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,ScheduledPublishDate,ScheduledArchiveDate,NextVersionToPublish,PageSegmentCount)
			VALUES(@Id,  @SiteId, 
					@PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)'), 
					@PublishDate,
					@PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
					@PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'),
					@PublishCount,
					--@ContainerXml,
					--isnull(@UnmanagedContentXml,'<unmanagedPageContents/>'),
					@PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
					@CreatedBy,
					GETUTCDATE(),
					@PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(1000)'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromExternalSearch)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromTranslation)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
					dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
					@PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
					isnull(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
					@PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier'),
					1
				)
				
		IF NOT EXISTS (Select 1 from PageMapNodePageDef Where PageDefinitionId =@Id AND PageMapNodeId=@ParentNodeId)
			INSERT INTO PageMapNodePageDef(PageDefinitionId,PageMapNodeId,DisplayOrder)
			VALUES (@Id,@ParentNodeId,@CurrentDisplayOrder + 1)
			
			
		INSERT INTO PageDefinitionSegment(
			Id
			,PageDefinitionId
			,DeviceId
			,AudienceSegmentId
			,UnmanagedXml
			,ZonesXml
			)
			 output inserted.Id into @pageDefinitionSegmentIds
		VALUES (
			NEWID()
			,@Id
			,dbo.GetDesktopDeviceId(@SiteId)
			,dbo.GetDesktopAudienceSegmentId(@SiteId)
			,isnull(@UnmanagedContentXml,'<unmanagedPageContents/>')
			,@ZonesXml
			)
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			INSERT INTO [PageDefinitionContainer]
				   (
					[PageDefinitionSegmentId]
				   ,[PageDefinitionId]
				   ,[ContainerId]
				   ,[ContentId]
				   ,[InWFContentId]
				   ,[ContentTypeId]
				   ,[IsModified]
				   ,[IsTracked]
				   ,[Visible]
				   ,[InWFVisible]
				   ,[DisplayOrder]
				   ,[InWFDisplayOrder]
				   ,[CustomAttributes]
				   ,ContainerName)
			 SELECT
					S.Id -- as this would be called only for desktop version, we can come in a better way
					,@Id
					,X.value('(@id)[1]','uniqueidentifier') as ContainerId
					,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
					,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
					,X.value('(@contentTypeId)[1]','int') as contentTypeId
					,X.value('(@isModified)[1]','bit') as isModified
					,X.value('(@isTracked)[1]','bit') as isTracked
					,X.value('(@visible)[1]','bit') as Visible
					,X.value('(@inWFVisible)[1]','bit') as InWFVisible
					,X.value('(@displayOrder)[1]','int') as DisplayOrder
					,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
					,dbo.GetCustomAttributeXml(X.query('.'),'container')
					,X.value('(@name)[1]','nvarchar(100)') as ContainerName
			From @ContainerXml.nodes('/Containers/container') temp(X)
			CROSS JOIN @pageDefinitionSegmentIds S
		END
	END
	ELSE
	BEGIN
		SELECT @PublishCount = PublishCount FROM PageDefinition WHERE PageDefinitionId = @Id
		IF @PublishPage = 1
			SELECT @PublishCount = @PublishCount + 1
			
		UPDATE PageDefinition Set 
			Title = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)') ELSE title END,
			TemplateId =@PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
			description = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)') ELSE description END, 
			publishDate = @PublishDate, 
			PageStatus = @PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
			WorkflowState = @PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'), 
			PublishCount = @PublishCount,
			--ContainerXml = @containerXml,
			--UnmanagedContentXml = isnull(@unmanagedContentXml, UnmanagedContentXml),
			FriendlyName = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)') ELSE FriendlyName END,
			WorkflowId = @PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = GETUTCDATE(),
			ArchivedDate = @PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
			StatusChangedDate = @PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
			AuthorId = @PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
			EnableOutputCache = @PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
			OutputCacheProfileName = @PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(100)'),
			ExcludeFromSearch = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
		    ExcludeFromExternalSearch = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromExternalSearch)[1]','bit'),
		    ExcludeFromTranslation = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromTranslation)[1]','bit'),
			IsDefault = @PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
			IsTracked = @PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
			HasForms = @PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
			TextContentCounter = @PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
			SourcePageDefinitionId = @PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
			CustomAttributes = dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
			IsInGroupPublish  =ISNULL(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
			ScheduledPublishDate = @PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
			ScheduledArchiveDate = @PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
			NextVersionToPublish =@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier')
		WHERE PageDefinitionId = @Id AND SiteId = @SiteId
		
		DECLARE @DeskTopDeviceId uniqueidentifier
		DECLARE @DeskTopAudienceId uniqueidentifier
		SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@SiteId)
		SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@SiteId)
		
		SELECT @PageDefinitionSegmentId = Id 
			FROM PageDefinitionSegment WHERE DeviceId = @DeskTopDeviceId AND 
				AudienceSegmentId = @DeskTopAudienceId AND
				PageDefinitionId = @Id
		
		UPDATE PageDefinitionSegment Set 
			UnmanagedXml = isnull(@unmanagedContentXml, UnmanagedXml),
			ZonesXml = isnull(@ZonesXml, ZonesXml)
		WHERE PageDefinitionId = @Id AND Id = @PageDefinitionSegmentId
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			UPDATE [PageDefinitionContainer] SET 
			   [ContentId] = X.value('(@contentId)[1]','uniqueidentifier')
			  ,[InWFContentId] = X.value('(@inWFContentId)[1]','uniqueidentifier')
			  ,[ContentTypeId] = X.value('(@contentTypeId)[1]','int') 
			  ,[IsModified] = X.value('(@isModified)[1]','bit')
			  ,[IsTracked] = X.value('(@isTracked)[1]','bit')
			  ,[Visible] = X.value('(@visible)[1]','bit')
			  ,[InWFVisible] = X.value('(@inWFVisible)[1]','bit')
			  ,[CustomAttributes] = dbo.GetCustomAttributeXml(X.query('.'),'container')
			  ,[DisplayOrder] = X.value('(@displayOrder)[1]','int')
			  ,[InWFDisplayOrder] = X.value('(@inWFDisplayOrder)[1]','int')
			From @ContainerXml.nodes('/Containers/container') temp(X)
			WHERE [PageDefinitionId] = @Id 
			AND PageDefinitionSegmentId = @PageDefinitionSegmentId
			AND [ContainerId] = X.value('(@id)[1]','uniqueidentifier')
			--If any containers added to pagedefinition insert those(ex. marketier template switching)
			INSERT INTO [PageDefinitionContainer]
						   (
							[PageDefinitionSegmentId]
						   ,[PageDefinitionId]
						   ,[ContainerId]
						   ,[ContentId]
						   ,[InWFContentId]
						   ,[ContentTypeId]
						   ,[IsModified]
						   ,[IsTracked]
						   ,[Visible]
						   ,[InWFVisible]
						   ,[DisplayOrder]
						   ,[InWFDisplayOrder]
						   ,[CustomAttributes]
						   ,[ContainerName])
					 SELECT
							@PageDefinitionSegmentId
							,@Id
							,X.value('(@id)[1]','uniqueidentifier') as ContainerId
							,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
							,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
							,X.value('(@contentTypeId)[1]','int') as contentTypeId
							,X.value('(@isModified)[1]','bit') as isModified
							,X.value('(@isTracked)[1]','bit') as isTracked
							,X.value('(@visible)[1]','bit') as Visible
							,X.value('(@inWFVisible)[1]','bit') as InWFVisible
							,X.value('(@displayOrder)[1]','int') as DisplayOrder
							,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
							,dbo.GetCustomAttributeXml(X.query('.'),'container')
							,X.value('(@name)[1]','nvarchar(100)') as ContainerName
					From @ContainerXml.nodes('/Containers/container') temp(X)					
					where X.value('(@id)[1]','uniqueidentifier') not in (select ContainerId from PageDefinitionContainer where PageDefinitionSegmentId=@PageDefinitionSegmentId and PageDefinitionId=@Id)
		
		END
		
	END
	
	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO USSecurityLevelObject 
		SELECT @Id, 8, SL.Id
	FROM dbo.SplitComma(@PageDefinitionXml.value('(pageDefinition/@securityLevels)[1]','nvarchar(max)'), ',') S 
		JOIN USSecurityLevel SL ON S.Value = SL.Id
	
	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO SIPageStyle 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'style')
	FROM @PageDefinitionXml.nodes('/pageDefinition/style') T(C) JOIN SIStyle S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO SIPageScript 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'script')
	FROM @PageDefinitionXml.nodes('/pageDefinition/script') T(C) JOIN SIScript S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	UPDATE D SET D.PageSegmentCount = T.Cnt
	FROM PageDefinition D
	INNER JOIN (SELECT PageDefinitionId, Count(PageDefinitionId) cnt FROM PageDefinitionSegment
	Where PageDefinitionId=@Id
	Group by PageDefinitionId
	) T
	ON T.PageDefinitionId =D.PageDefinitionId 


	EXEC [PageDefinition_AdjustDisplayOrder] @ParentNodeId

	EXEC PageMapBase_UpdateLastModification @SiteId
	EXEC PageMap_UpdateHistory @SiteId, @Id, 8, @ActionType
END
GO

PRINT 'Modify stored procedure VariantSite_CopyPages'
GO

IF(OBJECT_ID('VariantSite_CopyPages') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyPages
GO

CREATE PROCEDURE [dbo].[VariantSite_CopyPages]
(  
	@SiteId UNIQUEIDENTIFIER,
	@MicroSiteId UNIQUEIDENTIFIER,
	@ModifiedBy  UNIQUEIDENTIFIER,
	@MakePageStatusAsDraft BIT
	
)
AS
BEGIN

	INSERT INTO [dbo].[PageDefinition]
					   ([PageDefinitionId]
					   ,[TemplateId]
					   ,[SiteId]
					   ,[Title]
					   ,[Description]
					   ,[PageStatus]
					   ,[WorkflowState]
					   ,[PublishCount]
					   ,[PublishDate]
					   --,[DisplayOrder]
					   ,[FriendlyName]
					   ,[WorkflowId]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate]
					   ,[ArchivedDate]
					   ,[StatusChangedDate]
					   ,[AuthorId]
					   ,[EnableOutputCache]
					   ,[OutputCacheProfileName]
					   ,[ExcludeFromSearch]
					   ,[IsDefault]
					   ,[IsTracked]
					   ,[HasForms]
					   ,[TextContentCounter]
					   ,[SourcePageDefinitionId]
					   ,[CustomAttributes]
					   )
			SELECT NEWID()
				  ,[TemplateId]
				  ,@MicroSiteId
				  ,[Title]
				  ,D.[Description]
				  ,case when @MakePageStatusAsDraft=1 then dbo.GetActiveStatus()  else [PageStatus]  end
				  ,case when @MakePageStatusAsDraft=1 then  2  else case when WorkflowState =1 then WorkflowState else 2 end end
				  ,case when @MakePageStatusAsDraft=1 then 0 else 1 end
				  ,[PublishDate]
				  --,[DisplayOrder]
				  ,[FriendlyName]
				  ,dbo.GetEmptyGUID()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,[ArchivedDate]
				  ,[StatusChangedDate]
				  ,@ModifiedBy
				  ,[EnableOutputCache]
				  ,[OutputCacheProfileName]
				  ,[ExcludeFromSearch]
				  ,[IsDefault]
				  ,[IsTracked]
				  ,[HasForms]
				  ,[TextContentCounter]
				  ,PageDefinitionId
				  ,D.[CustomAttributes]
			      
			  FROM [dbo].[PageDefinition] D
			Where D.SiteId =@SiteId and PageDefinitionId in (select PageDefinitionId  from PageMapNodePageDef PMD inner join PageMapNode PM on PMD.PageMapNodeId = PM.SourcePageMapNodeId where PM.SiteId =@MicroSiteId)

			insert into PageMapNodePageDef
			select distinct  c.PageMapNodeId ,a.PageDefinitionId, b.DisplayOrder from [PageDefinition] a , PageMapNodePageDef b  , PageMapNode c
			where a.SiteId  = @MicroSiteId and a.SourcePageDefinitionId = b.PageDefinitionId 
			and c.SourcePageMapNodeId = b.PageMapNodeId and 
			c.SiteId = @MicroSiteId

			UPDATE PN SET PN.TargetId=T.PageDefinitionId
			FROM PageMapNode PN
			INNER JOIN PageDefinition T ON PN.TargetId = T.SourcePageDefinitionId
			Where PN.Target='1' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId

		
			insert into #tempIds SELECT PS.Id, NEWID() FROM  [dbo].[PageDefinitionSegment] PS INNER JOIN PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
			 
			 INSERT INTO [dbo].[PageDefinitionSegment]
						   ([Id]
						   ,[PageDefinitionId]
						   ,[DeviceId]
						   ,[AudienceSegmentId]
						   ,[UnmanagedXml]
						   ,[ZonesXml])
						   SELECT T.NewId 
							  ,P.PageDefinitionId 
							  ,[DeviceId]
							  ,[AudienceSegmentId]
							  ,[UnmanagedXml]
							  ,[ZonesXml]
						  FROM [dbo].[PageDefinitionSegment] PS 
						  inner join #tempIds T on PS.Id= T.OldId 
						  inner join PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
						  Where P.SiteId = @MicroSiteId
						  
			INSERT INTO [PageDefinitionContainer]
				(
					[Id]
					,PageDefinitionSegmentId 
					,[PageDefinitionId]
					,[ContainerId]
					,[ContentId]
					,[InWFContentId]
					,[ContentTypeId]
					,[IsModified]
					,[IsTracked]
					,[Visible]
					,[InWFVisible]
					,DisplayOrder 
					,InWFDisplayOrder 
					,[CustomAttributes]
					,[ContainerName])
				SELECT
						NewId() 
						,T.NewId 
					,P.PageDefinitionId
					,ContainerId
					,ContentId
					,InWFContentId
					,ContentTypeId
					,IsModified
					,PC.IsTracked
					,Visible
					,InWFVisible
					,PC.DisplayOrder 
					,PC.InWFDisplayOrder 
					,PC.CustomAttributes
					,ContainerName
				FROM PageDefinitionContainer PC
				inner join #tempIds T on PC.PageDefinitionSegmentId= T.OldId
				inner join PageDefinition P ON PC.PageDefinitionId=P.SourcePageDefinitionId 
				Where P.SiteId = @MicroSiteId
						  
			truncate table #tempIds

			INSERT INTO [dbo].[PageDetails]
					   ([PageId]
					   ,[PageDetailXML])
			  SELECT 
				 P.PageDefinitionId
					   ,[PageDetailXML]
			  FROM [dbo].[PageDetails] PD
			INNER JOIN PageDefinition P ON P.SourcePageDefinitionId = PD.PageId
			 Where P.SiteId = @MicroSiteId

			 INSERT INTO [dbo].[USSecurityLevelObject]
					   ([ObjectId]
					   ,[ObjectTypeId]
					   ,[SecurityLevelId])
			  SELECT P.PageDefinitionId
					   ,ObjectTypeId
					   ,SecurityLevelId
			FROM [USSecurityLevelObject] SO
			INNER JOIN PageDefinition P ON SO.ObjectId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageScript]
					   ([PageDefinitionId]
					   ,[ScriptId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[ScriptId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageScript] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageStyle]
					   ([PageDefinitionId]
					   ,[StyleId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[StyleId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageStyle] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[COTaxonomyObject]
					   ([Id]
					   ,[TaxonomyId]
					   ,[ObjectId]
					   ,[ObjectTypeId]
					   ,[SortOrder]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate])
			 SELECT NEWID()
				  ,[TaxonomyId]
				  ,P.PageDefinitionId
				  ,[ObjectTypeId]
				  ,[SortOrder]
				  ,[Status]
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
			  FROM [dbo].[COTaxonomyObject] OT
			INNER JOIN PageDefinition P ON OT.ObjectId= P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
	
	IF @MakePageStatusAsDraft = 0
	BEGIN
		INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
			SELECT	NEWID(), PD1.PageDefinitionId, POR.ObjectId, POR.ObjectType, POR.ObjectSiteId
			FROM	PageDefinition AS PD1
					INNER JOIN PageDefinition AS PD2 ON PD2.PageDefinitionId = PD1.SourcePageDefinitionId
					INNER JOIN PageObjectReferences AS POR ON POR.PageId = PD2.PageDefinitionId
			WHERE	PD1.SiteId = @MicroSiteId
	END
END
GO

PRINT 'Modify stored procedure PageDto_Import'
GO

IF(OBJECT_ID('PageDto_Import') IS NOT NULL)
	DROP PROCEDURE PageDto_Import
GO

CREATE PROCEDURE [dbo].[PageDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@PageMapNodeId			uniqueidentifier = NULL OUTPUT,
	@Publish				bit = NULL,	
	@Overwrite				bit = NULL,		
	@ModifiedBy				uniqueidentifier,
	@PreserveVariantContent bit = 1,
	@IgnoreExisting			bit = NULL,
	@InvalidateCache		bit = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier, @DisplayOrder int

	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @Id = [dbo].[Page_GetVariantId](@SourceId, @TargetSiteId)

	IF @PageMapNodeId IS NULL
	BEGIN
		DECLARE @ParentPageMapNode uniqueidentifier
		SELECT TOP 1 @ParentPageMapNode = PageMapNodeId, @DisplayOrder = DisplayOrder FROM PageMapNodePageDef 
			WHERE PageDefinitionId = @SourceId
		SET @PageMapNodeId = [dbo].[Menu_GetVariantId](@ParentPageMapNode, @TargetSiteId)
	END

	IF @PageMapNodeId IS NULL OR (@Id IS NOT NULL AND @IgnoreExisting = 1)
		RETURN

	SELECT @ModifiedBy = ModifiedBy FROM PageDefinition WHERE PageDefinitionId = @SourceId 
	IF @Publish IS NULL AND EXISTS (SELECT 1 FROM PageDefinition WHERE PageDefinitionId = @SourceId AND PageStatus = 8)
		SET @Publish = 1

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()
		INSERT INTO [dbo].[PageDefinition]
		(
			[PageDefinitionId],
			[TemplateId],
			[SiteId],
			[Title],
			[Description],
			[PageStatus],
			[WorkflowState],
			[PublishCount],
			[PublishDate],
			[FriendlyName],
			[WorkflowId],
			[CreatedBy],
			[CreatedDate],
			[ModifiedBy],
			[ModifiedDate],
			[ArchivedDate],
			[StatusChangedDate],
			[AuthorId],
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			[SourcePageDefinitionId],
			[CustomAttributes]
		)
		SELECT @Id,
			[TemplateId],
			@TargetSiteId,
			[Title],
			D.[Description],
			CASE WHEN @Publish = 1 THEN 8 ELSE PageStatus END,
			1,
			0,
			'1900-01-01 00:00:00.000',
			[FriendlyName],
			@EmptyGuid,
			@ModifiedBy,
			@UtcNow,
			@ModifiedBy,
			@UtcNow,
			[ArchivedDate],
			[StatusChangedDate],
			@ModifiedBy,
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			PageDefinitionId,
			D.[CustomAttributes]
		FROM [dbo].[PageDefinition] D
		WHERE PageDefinitionId = @SourceId				

		UPDATE PageMapNode SET TargetId = @Id
		WHERE TargetId = @SourceId AND SiteId = @TargetSiteId
		 
		INSERT INTO [dbo].[PageDefinitionSegment]
		(
			[Id],
			[PageDefinitionId],
			[DeviceId],
			[AudienceSegmentId],
			[UnmanagedXml],
			[ZonesXml]
		)
		SELECT NEWID(), 
			@Id,
			[DeviceId],
			[AudienceSegmentId],
			dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
			[ZonesXml]
		FROM [dbo].[PageDefinitionSegment] PS 
		WHERE PageDefinitionId = @SourceId

		INSERT INTO [dbo].[PageDetails]
		(
			[PageId],
			[PageDetailXML]
		)
		SELECT 
			@Id,
			[PageDetailXML]
		FROM [dbo].[PageDetails] PD
		WHERE PD.PageId = @SourceId
	END
	ELSE
	BEGIN
		UPDATE C SET
			C.Title = P.Title,
			C.Description = P.Description,
			C.PageStatus = CASE WHEN @Publish = 1 THEN 8 ELSE P.PageStatus END,
			C.FriendlyName = CASE WHEN @PreserveVariantContent = 0 THEN P.FriendlyName ELSE C.FriendlyName END,
			C.EnableOutputCache = P.EnableOutputCache,
			C.OutputCacheProfileName = P.OutputCacheProfileName,
			C.ExcludeFromSearch = P.ExcludeFromSearch,
			C.HasForms = P.HasForms,
			C.ScheduledPublishDate = P.ScheduledPublishDate,
			C.ScheduledArchiveDate = P.ScheduledArchiveDate,
			C.ModifiedDate = @UtcNow,
			C.ModifiedBy = P.ModifiedBy
		FROM PageDefinition P, PageDefinition C
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.UnmanagedXml = dbo.Page_GetUnmanagedXml(P.[UnmanagedXml], @TargetSiteId),
			C.ZonesXml = P.ZonesXml
		FROM PageDefinitionSegment P
			JOIN PageDefinitionSegment C ON P.DeviceId = C.DeviceId AND P.AudienceSegmentId = C.AudienceSegmentId
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.ContentId = P.ContentId,
			C.InWFContentId = P.InWFContentId,
			C.ContentTypeId = P.ContentTypeId,
			C.IsModified = P.IsModified,
			C.[IsTracked] = P.[IsTracked],
			C.[Visible] = P.[Visible],
			C.[InWFVisible] = P.[InWFVisible],
			C.[CustomAttributes] = P.[CustomAttributes],
			C.[DisplayOrder] = P.[DisplayOrder],
			C.[InWFDisplayOrder] = P.[InWFDisplayOrder]
		FROM PageDefinitionContainer P
			JOIN PageDefinitionContainer C ON P.ContainerName = C.ContainerName
			LEFT JOIN COContent CO ON C.ContentId = CO.Id
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId
			AND (@Overwrite = 1 OR CO.Id IS NULL OR CO.ApplicationId != @TargetSiteId)
		
		IF @PreserveVariantContent = 0
		BEGIN
			UPDATE C 
			SET	C.PageDetailXML = P.PageDetailXML
			FROM PageDetails P, PageDetails C
			WHERE C.PageId = @Id AND P.PageId = @SourceId	
		END

		DELETE FROM PageMapNodePageDef WHERE PageDefinitionId = @Id
	END

	INSERT INTO PageMapNodePageDef
	(
		PageDefinitionId,
		PageMapNodeId,
		DisplayOrder
	)
	SELECT
		@Id,
		@PageMapNodeId,
		@DisplayOrder

	INSERT INTO [PageDefinitionContainer]
	(
		[Id],
		PageDefinitionSegmentId ,
		[PageDefinitionId],
		[ContainerId],
		[ContentId],
		[InWFContentId],
		[ContentTypeId],
		[IsModified],
		[IsTracked],
		[Visible],
		[InWFVisible],
		DisplayOrder,
		InWFDisplayOrder,
		[CustomAttributes],
		[ContainerName]
	)
	SELECT NEWID(),
		CS.Id,
		@Id,
		ContainerId,
		ContentId,
		InWFContentId,
		ContentTypeId,
		IsModified,
		PC.IsTracked,
		Visible,
		InWFVisible,
		PC.DisplayOrder, 
		PC.InWFDisplayOrder, 
		PC.CustomAttributes,
		ContainerName
	FROM PageDefinitionContainer PC
		JOIN PageDefinitionSegment PS ON PC.PageDefinitionSegmentId = PS.Id
		JOIN PageDefinitionSegment CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
	WHERE PC.PageDefinitionId = @SourceId
		AND (@Id IS NULL OR ContainerName NOT IN (SELECT ContainerName FROM PageDefinitionContainer WHERE PageDefinitionId = @Id))

	DECLARE @SourceTargetMenuId uniqueidentifier
	SELECT TOP 1 @SourceTargetMenuId = PageMapNodeId FROM PageMapNode PN
		JOIN PageDefinition PD ON PN.TargetId = PD.PageDefinitionId AND PD.SiteId = PN.SiteId
	WHERE PD.PageDefinitionId = @SourceId

	IF @SourceTargetMenuId IS NOT NULL
	BEGIN
		UPDATE PageMapNode SET
			TargetId = @Id,
			Target = 1,
			TargetUrl = ''
		WHERE SourcePageMapNodeId = @SourceTargetMenuId
			AND SiteId = @TargetSiteId
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[USSecurityLevelObject]
	(
		[ObjectId],
		[ObjectTypeId],
		[SecurityLevelId]
	)
	SELECT @Id,
		ObjectTypeId,
		SecurityLevelId
	FROM [USSecurityLevelObject] S
	WHERE S.ObjectId = @SourceId

	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageScript]
	(
		[PageDefinitionId],
		[ScriptId],
		[CustomAttributes]
	)
	SELECT @Id,
		[ScriptId],
		[CustomAttributes]
	FROM [dbo].[SIPageScript] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageStyle]
	(
		[PageDefinitionId],
		[StyleId],
		[CustomAttributes]
	)
	SELECT @Id,
		[StyleId],
		[CustomAttributes]
	FROM [dbo].[SIPageStyle] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM COTaxonomyObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[COTaxonomyObject]
	(
		[Id],
		[TaxonomyId],
		[ObjectId],
		[ObjectTypeId],
		[SortOrder],
		[Status],
		[CreatedBy],
		[CreatedDate],
		[ModifiedBy],
		[ModifiedDate]
	)
	SELECT NEWID(),
		[TaxonomyId],
		@Id,
		[ObjectTypeId],
		[SortOrder],
		[Status],
		@ModifiedBy,
		@UtcNow,
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[COTaxonomyObject] OT
	WHERE OT.ObjectId = @SourceId

	DECLARE @VersionId uniqueidentifier, @SourceVersionId uniqueidentifier, 
		@VersionNumber int, @RevisionNumber int, @VersionStatus int
	SET @VersionId = NEWID()
	SELECT TOP 1 @SourceVersionId = Id FROM VEVersion 
	WHERE ObjectId = @SourceId ORDER BY CreatedDate DESC	

	SELECT TOP 1 @VersionNumber = VersionNumber, @RevisionNumber = RevisionNumber FROM VEVersion
	WHERE ObjectId = @Id ORDER BY CreatedDate DESC

	IF @Publish = 1
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0) + 1
		SET @VersionNumber = 0
		SET @VersionStatus = 1
	END
	ELSE
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0)
		SET @VersionNumber = ISNULL(@VersionNumber, 0) + 1
		SET @VersionStatus = 0
	END

	INSERT INTO [dbo].[VEVersion]
	(
		[Id],
		[ApplicationId],
		[ObjectTypeId],
		[ObjectId],
		[CreatedDate],
		[CreatedBy],
		[VersionNumber],
		[RevisionNumber],
		[Status],
		[ObjectStatus],
		[VersionStatus],
		[XMLString],
		[Comments],
		[TriggeredObjectId],
		[TriggeredObjectTypeId]
	)          
	SELECT TOP 1 @VersionId,
		@TargetSiteId,
		8, 
		@Id, 
		@UtcNow,
		@ModifiedBy,
		@VersionNumber,
		@RevisionNumber,
		@VersionStatus,
		ObjectStatus,
		VersionStatus, 
		dbo.UpdatePageXml(XMLString, @Id, P.WorkflowId, P.WorkflowState, P.PageStatus, 1, 
			P.CreatedBy, P.CreatedDate, P.AuthorId, P.SiteId, P.SourcePageDefinitionId, @PageMapNodeId), 
		'Imported from master',
		TriggeredObjectId,
		TriggeredObjectTypeId       
	FROM VEVersion V, 
		PageDefinition P
	WHERE V.Id = @SourceVersionId
		AND P.PageDefinitionId = @Id

	IF @Publish = 1
		UPDATE VEVersion SET Status = 1 WHERE ObjectId = @Id

	INSERT INTO [dbo].[VEPageDefSegmentVersion]
	(
		[Id],
		[VersionId],
		[PageDefinitionId],
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],  
		[ContainerContentXML],        
		[UnmanagedXml],
		[ZonesXml],
		[CreatedBy],
		[CreatedDate]
	)
	SELECT NEWID(),
		@VersionId,
		@Id,
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],
		CASE WHEN @Overwrite = 1 THEN [ContainerContentXML] 
			ELSE [dbo].[Page_GetContainerXml](@Id, AudienceSegmentId, DeviceId) END,
		dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
		[ZonesXml],
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[VEPageDefSegmentVersion]
	WHERE VersionId = @SourceVersionId

	INSERT INTO [dbo].[VEPageContentVersion]
	(
		[Id],
		[ContentVersionId],
		[IsChanged],
		[PageDefSegmentVersionId]
	)
	SELECT NEWID(),
		ContentVersionId,
		IsChanged,
		CS.Id
	FROM [dbo].[VEPageContentVersion] PC
		JOIN VEPageDefSegmentVersion PS ON PC.PageDefSegmentVersionId = PS.Id
		JOIN VEPageDefSegmentVersion CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
			AND PS.VersionId = @SourceVersionId
			AND CS.VersionId = @VersionId

	IF @Publish = 1
	BEGIN
		DECLARE @PublishCount int
		SET @PublishCount = (SELECT ISNULL(PublishCount, 0) + 1 FROM PageDefinition WHERE PageDefinitionId = @Id)

		UPDATE PageDefinition SET
			PublishCount = @PublishCount,
			PublishDate = @UtcNow,
			PageStatus = 8
		WHERE PageDefinitionId = @Id

		DECLARE @WorkflowId uniqueidentifier
		SELECT TOP 1 @WorkflowId = WorkflowId FROM WFWorkflowExecution WHERE ObjectId = @Id AND Completed = 0

		IF @WorkflowId IS NOT NULL
		BEGIN
			EXEC WorkflowExecution_Save
				@WorkflowId = @WorkflowId,
				@SequenceId = @EmptyGuid,
				@ObjectId = @Id,
				@ObjectTypeId = 8,
				@ActorId = @ModifiedBy,
				@ActorType = 1,
				@ActionId = 8,
				@Remarks = N'Page imported from master',
				@Completed = 1
		END

		DELETE
		FROM	PageObjectReferences
		WHERE	PageId = @Id

		INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
			SELECT	NEWID(), @Id, ObjectId, ObjectType, ObjectSiteId
			FROM	PageObjectReferences
			WHERE	PageId = @SourceId
	END

	IF @InvalidateCache = 1
	BEGIN
		EXEC PageDto_Invalidate @SiteId = @TargetSiteId, @ModifiedBy = @ModifiedBy
	END
END
GO

PRINT 'Create table TLDocumentObject'
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLDocumentObject'))
BEGIN
	CREATE TABLE [dbo].[TLDocumentObject]
	(
		[Id] UNIQUEIDENTIFIER NOT NULL, 
		[DocumentId] UNIQUEIDENTIFIER NOT NULL, 
		[ObjectId] UNIQUEIDENTIFIER NOT NULL,
		[ObjectType] INT NOT NULL,
		[SiteId] UNIQUEIDENTIFIER NOT NULL, 
		CONSTRAINT [PK_TLDocumentObject] PRIMARY KEY ([Id]), 
		CONSTRAINT [FK_TLDocumentObject_TLDocument] FOREIGN KEY ([DocumentId]) REFERENCES [TLDocument]([Id]), 
		CONSTRAINT [FK_TLDocumentObject_SISite] FOREIGN KEY ([SiteId]) REFERENCES [SISite]([Id])
	)

	CREATE INDEX [IX_TLDocumenObject_DocumentId] ON [dbo].[TLDocumentItem] ([TranslationDocumentId])
END

PRINT 'Migrate data to TLDocumentObject'
GO

IF NOT EXISTS (SELECT * FROM TLDocumentObject) AND EXISTS (SELECT * FROM TLDocument)
BEGIN
	INSERT INTO TLDocumentObject (Id, DocumentId, ObjectId, ObjectType, SiteId)
		SELECT	NEWID(), D.Id, T.ObjectId, T.ObjectType, T.SiteId
		FROM	TLTranslation AS T
				INNER JOIN TLDocument AS D ON D.TranslationId = T.Id
END
GO

PRINT 'Add stored procedure TranslationReportItemDto_Get'
GO

IF (OBJECT_ID('TranslationReportItemDto_Get') IS NOT NULL)
	DROP PROCEDURE TranslationReportItemDto_Get
GO

CREATE PROCEDURE [dbo].[TranslationReportItemDto_Get]
(
	@TranslationSiteId		UNIQUEIDENTIFIER,
	@ObjectType				INT = NULL,
	@ObjectId				UNIQUEIDENTIFIER = NULL,
	@ObjectIds				NVARCHAR(MAX) = NULL,
	@VariantObjectId		UNIQUEIDENTIFIER = NULL,
	@VariantObjectIds		NVARCHAR(MAX) = NULL,
	@VariantSiteId			UNIQUEIDENTIFIER = NULL,
	@Status					INT = NULL,
	@Statuses				VARCHAR(50) = NULL,
	@Keyword				NVARCHAR(255) = NULL,
	@IncludeSummary			BIT = 0,
	@PageNumber				INT = NULL,
	@PageSize				INT = NULL,
	@TotalRecords			INT = NULL OUTPUT
)
WITH RECOMPILE
AS
BEGIN
	SELECT	Id, NULLIF(ParentSiteId, '00000000-0000-0000-0000-000000000000') AS ParentId, MasterSiteId AS MasterId, Title, DefaultTranslateToLanguage AS Locale
	INTO	#Sites
	FROM	SISite
	WHERE	[Status] = 1 AND
			SubmitToTranslation = 1 AND
			(@TranslationSiteId IS NULL OR ParentSiteId = @TranslationSiteId)
	
	SELECT	*
	INTO	#Objects
	FROM	dbo.GetVariantObjects(@ObjectType)

	CREATE CLUSTERED INDEX IX_Objects ON #Objects (ObjectId, SiteId, LeftValue, RightValue)

	CREATE TABLE #TranslationTargets (
		TranslationId	UNIQUEIDENTIFIER,
		SiteId			UNIQUEIDENTIFIER
	)

	INSERT INTO #TranslationTargets (TranslationId, SiteId)
		SELECT	TT.TranslationId, SLS.SiteId
		FROM	TLTranslationTarget AS TT
				INNER JOIN SISiteList AS SL ON SL.Id = TT.TargetId
				INNER JOIN SISiteListSite AS SLS ON SLS.SiteListId = SL.Id
		WHERE	TT.TargetTypeId = 1

	INSERT INTO #TranslationTargets (TranslationId, SiteId)
		SELECT	TT.TranslationId, TT.TargetId
		FROM	TLTranslationTarget AS TT
		WHERE	TT.TargetTypeId = 2

	INSERT INTO #TranslationTargets (TranslationId, SiteId)
		SELECT	TT.TranslationId, S2.Id
		FROM	TLTranslationTarget AS TT
				INNER JOIN SISite AS S1 ON S1.Id = TT.TargetId
				INNER JOIN SISite AS S2 ON S2.LftValue > S1.LftValue AND S2.RgtValue < S1.RgtValue
		WHERE	TT.TargetTypeId = 3

	SELECT  DI.Id, TT.SiteId, DI.Locale, DI.Content, DI.LogMessage,
			CASE WHEN DI.[Status] IS NULL AND D.[Status] = 8 THEN D.[Status] ELSE DI.[Status] END AS [Status],
			CASE WHEN DI.[Status] IS NULL AND D.[Status] = 8 THEN D.CreatedDate ELSE DI.CreatedDate END AS CreatedDate,
			CASE WHEN DI.[Status] IS NULL AND D.[Status] = 8 THEN D.CreatedBy ELSE DI.CreatedBy END AS CreatedBy,
			CASE WHEN DI.[Status] IS NULL AND D.[Status] = 8 THEN D.ModifiedDate ELSE DI.ModifiedDate END AS ModifiedDate,
			CASE WHEN DI.[Status] IS NULL AND D.[Status] = 8 THEN D.ModifiedBy ELSE DI.ModifiedBy END AS ModifiedBy,
			T.Id AS TranslationId, DO.ObjectId AS TranslationObjectId, DO.ObjectType AS TranslationObjectType, DO.SiteId AS TranslationSiteId,
			D.Id AS DocumentId, D.CreatedDate AS DocumentCreatedDate,
			ROW_NUMBER() OVER (PARTITION BY DO.ObjectId, TT.SiteId ORDER BY D.CreatedDate DESC) AS VersionRank
	INTO	#DocumentItemVersion
	FROM    TLTranslation AS T
			INNER JOIN TLDocument AS D ON D.TranslationId = T.Id
			INNER JOIN TLDocumentObject AS DO ON DO.DocumentId = D.Id
			INNER JOIN #TranslationTargets AS TT ON TT.TranslationId = T.Id
			INNER JOIN SISite AS S ON S.Id = TT.SiteId
			LEFT JOIN TLDocumentItem AS DI ON DI.TranslationDocumentId = D.Id AND DI.SiteId = TT.SiteId
	WHERE	(DI.[Status] IS NULL OR DI.[Status] != 3) AND
			(@VariantSiteId IS NULL OR S.Id = @VariantSiteId)

	CREATE CLUSTERED INDEX IX_DocumentItemVersion ON #DocumentItemVersion (TranslationObjectId, SiteId, DocumentCreatedDate, VersionRank)
	
	SELECT	O.MasterObjectId, O.ObjectId, O.ObjectType, O.ObjectTitle, O.CreatedDate AS ObjectCreatedDate, O.ModifiedDate AS ObjectModifiedDate,
			O.VariantSiteId, O.VariantSiteTitle, COALESCE(DIV.Locale, S.Locale) AS Locale, O.VariantObjectId, O.VariantObjectTitle, O.VariantModifiedDate AS VariantObjectModifiedDate,
			CASE WHEN DIV.[Status] = 7 AND DIV.CreatedDate < O.ModifiedDate AND O.VariantObjectId IS NOT NULL THEN 9 ELSE COALESCE(DIV.[Status], 0) END AS [Status],
			DIV.CreatedDate AS TranslationSubmittedDate, DIV.ModifiedDate AS TranslationUpdatedDate,
			ROW_NUMBER() OVER (ORDER BY O.ObjectType ASC, O.MasterObjectId ASC, O.VariantSiteTitle ASC) AS RowNumber
	INTO	#DocumentItems
	FROM	(
				SELECT	O.MasterObjectId, O.ObjectId, O.ObjectType, O.Title AS ObjectTitle, S.Id AS VariantSiteId, S.Title AS VariantSiteTitle, O2.ObjectId AS VariantObjectId,
						O2.Title AS VariantObjectTitle, O.CreatedDate, O.ModifiedDate, O2.ModifiedDate AS VariantModifiedDate, O.LeftValue, O.RightValue
				FROM	#Objects AS O
						CROSS JOIN #Sites AS S
						LEFT JOIN #Objects AS O2 ON O2.MasterObjectId = O.MasterObjectId AND O2.SiteId = S.Id	 
				WHERE	(@TranslationSiteId IS NULL OR O.SiteId = @TranslationSiteId)
			) AS O
			INNER JOIN #Sites AS S ON S.Id = O.VariantSiteId
			LEFT JOIN #DocumentItemVersion AS DIV ON DIV.SiteId = S.Id AND (DIV.TranslationObjectId = O.ObjectId OR (O.ObjectType = 2 AND EXISTS (
				SELECT	1
				FROM	#Objects AS RO
				WHERE	RO.ObjectId = DIV.TranslationObjectId AND
						RO.LeftValue < O.LeftValue AND
						RO.RightValue > O.RightValue AND
						DIV.DocumentCreatedDate > O.CreatedDate
			))) AND DIV.VersionRank = 1 -- Menus are handled this way to avoid having a huge number of records in TLDocumentObject. If we ever support submitting individual menus rather than the entire tree, this should be reconsidered.
	WHERE	(@ObjectId IS NULL OR O.ObjectId = @ObjectId) AND
			(@ObjectIds IS NULL OR O.ObjectId IN (SELECT CAST([Value] AS UNIQUEIDENTIFIER) FROM dbo.SplitComma(@ObjectIds, ','))) AND
			(@VariantObjectId IS NULL OR O.VariantObjectId = @VariantObjectId) AND
			(@VariantObjectIds IS NULL OR O.VariantObjectId IN (SELECT CAST([Value] AS UNIQUEIDENTIFIER) FROM dbo.SplitComma(@VariantObjectIds, ','))) AND
			(@VariantSiteId IS NULL OR O.VariantSiteId = @VariantSiteId) AND
			(@Status IS NULL OR CASE WHEN DIV.[Status] = 7 AND DIV.CreatedDate < O.ModifiedDate AND O.VariantObjectId IS NOT NULL THEN 9 ELSE COALESCE(DIV.[Status], 0) END = @Status) AND
			(@Statuses IS NULL OR CASE WHEN DIV.[Status] = 7 AND DIV.CreatedDate < O.ModifiedDate AND O.VariantObjectId IS NOT NULL THEN 9 ELSE COALESCE(DIV.[Status], 0) END IN (SELECT CAST([Value] AS INT) FROM dbo.SplitComma(@Statuses, ','))) AND
			(@Keyword IS NULL OR O.ObjectTitle LIKE '%' + @Keyword + '%' OR O.VariantObjectTitle LIKE '%' + @Keyword + '%')

	DECLARE	@StartRow	INT,
			@EndRow		INT

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END
	
	SELECT		*
	FROM		#DocumentItems
	WHERE		@StartRow IS NULL OR
				@EndRow IS NULL OR 
				RowNumber BETWEEN @StartRow AND @EndRow
	ORDER BY	RowNumber ASC

	SET @TotalRecords = (
		SELECT	COUNT(*)
		FROM	#DocumentItems
	)

	IF @IncludeSummary = 1
		SELECT		[Status], COUNT(*) AS [Count]
		FROM		#DocumentItems
		GROUP BY	[Status]
END
GO

PRINT 'Modify stored procedure PageMapNode_SaveWithParams'
GO

IF(OBJECT_ID('PageMapNode_SaveWithParams') IS NOT NULL)
	DROP PROCEDURE PageMapNode_SaveWithParams
GO

CREATE PROCEDURE [dbo].[PageMapNode_SaveWithParams] 
(
	@PageMapNodeId [uniqueidentifier],
	@ParentId [uniqueidentifier],
	@SiteId [uniqueidentifier] ,
	@ExcludeFromSiteMap bit = 0,
	@Description [nvarchar](500) =NULL,
	@DisplayTitle [nvarchar](256) =NULL,
	@FriendlyUrl [nvarchar](500) =NULL,
	@MenuStatus [int] =NULL,
	@TargetId [uniqueidentifier] =NULL,
	@Target [int]= NULL,
	@TargetUrl [nvarchar](500) =NULL,
	@ModifiedBy [uniqueidentifier] =NULL,
	@ModifiedDate [datetime]= NULL,
	@PropogateWorkflow [bit]= 0,
	@InheritWorkflow [bit]= 0,
	@PropogateSecurityLevels [bit]= NULL,
	@InheritSecurityLevels [bit]= 0,
	@PropogateRoles [bit]= NULL,
	@InheritRoles [bit]= 0,
	@Roles [nvarchar](max)= NULL,
	@LocationIdentifier [nvarchar](50)= NULL,
	@HasRolloverImages [bit]= 0,
	@RolloverOnImage [nvarchar](256)= NULL,
	@RolloverOffImage [nvarchar](256)= NULL,
	@IsCommerceNav [bit] =0,
	@AssociatedQueryId [uniqueidentifier]= NULL,
	@DefaultContentId [uniqueidentifier]= NULL,
	@AssociatedContentFolderId [uniqueidentifier]= NULL,
	@SecurityLevels				[varchar](max)=NULL,
	@PageMapNodeWorkFlow [varchar](max)=NULL,
	@CustomAttributes [xml] =NULL,
	@HiddenFromNavigation [bit] = 0,
	@CssClass [nvarchar](125) = NULL,
	@AllowAccessInChildrenSites [bit] = 1 ,
	@SourcePageMapNodeId uniqueidentifier = NULL
)	
AS
BEGIN
	Declare @LftValue int,@RgtValue int, @DisplayOrder int
	Declare @P_PropogateWorkflow bit,
			@P_PropogateSecurityLevels bit,
			@P_PropogateRoles bit

	Set @ModifiedDate = cast(isnull(@ModifiedDate,getutcdate()) as varchar(50))
	Set @LftValue=0
	Set @RgtValue=0
	Set @DisplayOrder = 1
	
	IF (@ParentId is not null) 
	BEGIN
		Select 
			@LftValue =LftValue,
			@RgtValue=RgtValue, 
			@P_PropogateWorkflow =PropogateWorkflow,
			@P_PropogateSecurityLevels =PropogateSecurityLevels,
			@P_PropogateRoles=PropogateRoles
		from PageMapNode 
		Where PageMapNodeId=@ParentId and SiteId=@SiteId
	END
	ELSE 
	BEGIN
		Set @LftValue=1
		Set @RgtValue=1		
	END
	
	IF @LftValue <=0
	begin
		RAISERROR('NOTEXISTS||ParentId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
	end

	if((@ParentId is not null) AND @P_PropogateSecurityLevels=1 OR @InheritSecurityLevels=1)
	BEGIN
		INSERT INTO USSecurityLevelObject(ObjectId,ObjectTypeId,SecurityLevelId)
		Select @PageMapNodeId,ObjectTypeId,SecurityLevelId from USSecurityLevelObject
		Where ObjectId=@ParentId
	END
			
	INSERT INTO USSecurityLevelObject(ObjectId,ObjectTypeId,SecurityLevelId)
	Select @PageMapNodeId,2,Value from SplitComma(@SecurityLevels,',')
	Where len(Value)>0
	except 
	Select @PageMapNodeId,ObjectTypeId,SecurityLevelId from USSecurityLevelObject
	Where ObjectId=@PageMapNodeId

	Update PageMapNode Set  LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,
							RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End 
	Where SiteId=@SiteId

	SELECT @DisplayOrder = ISNULL(MAX(DisplayOrder), 0) FROM PageMapNode WHERE ParentId = @ParentId

	INSERT INTO [dbo].[PageMapNode]
           ([PageMapNodeId]
           ,[ParentId]
           ,[LftValue]
           ,[RgtValue]
           ,[SiteId]
           ,[ExcludeFromSiteMap]
           ,[Description]
           ,[DisplayTitle]
           ,[FriendlyUrl]
           ,[MenuStatus]
           ,[TargetId]
           ,[Target]
           ,[TargetUrl]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[PropogateWorkflow]
           ,[InheritWorkflow]
           ,[PropogateSecurityLevels]
           ,[InheritSecurityLevels]
           ,[PropogateRoles]
           ,[InheritRoles]
           ,[Roles]
           ,[LocationIdentifier]
           ,[HasRolloverImages]
           ,[RolloverOnImage]
           ,[RolloverOffImage]
           ,[IsCommerceNav]
           ,[AssociatedQueryId]
           ,[DefaultContentId]
           ,[AssociatedContentFolderId]
           ,[CustomAttributes]
           ,[HiddenFromNavigation]
		   ,[CssClass]
		   ,[DisplayOrder]
		   ,[AllowAccessInChildrenSites]
		   ,[SourcePageMapNodeId])
     VALUES
           (@PageMapNodeId
           ,@ParentId
           ,@RgtValue
           ,@RgtValue+1
           ,@SiteId
           ,@ExcludeFromSiteMap
           ,@Description
           ,@DisplayTitle
           ,@FriendlyUrl
           ,@MenuStatus
           ,@TargetId
           ,@Target
           ,@TargetUrl
           ,@ModifiedBy
           ,@ModifiedDate
           ,@ModifiedBy
           ,@ModifiedDate
           ,@PropogateWorkflow
           ,@InheritWorkflow
           ,@PropogateSecurityLevels
           ,@InheritSecurityLevels
           ,@PropogateRoles
           ,@InheritRoles
           ,@Roles
           ,@LocationIdentifier
           ,@HasRolloverImages
           ,@RolloverOnImage
           ,@RolloverOffImage
           ,@IsCommerceNav
           ,@AssociatedQueryId
           ,@DefaultContentId
           ,@AssociatedContentFolderId
           ,@CustomAttributes
           ,@HiddenFromNavigation
		   ,@CssClass
		   ,@DisplayOrder + 1
		   ,@AllowAccessInChildrenSites
		   ,@SourcePageMapNodeId)

	-- Moved down after the Insert page map node as there is a relationship with workflowId
	if((@ParentId is not null) AND @P_PropogateWorkflow=1 OR @InheritWorkflow=1)
	BEGIN
		--PRINT 'In propogate workflow'
		Insert into PageMapNodeWorkflow(Id,PageMapNodeId,WorkflowId,CustomAttributes)
		Select NEWID(),@PageMapNodeId,WorkflowId,CustomAttributes from PageMapNodeWorkflow
		Where PageMapNodeId=@ParentId
	END
	
	Insert into PageMapNodeWorkflow(Id,PageMapNodeId,WorkflowId,CustomAttributes)
	Select NEWID(),@PageMapNodeId,WorkflowId,NULL
	from (
	select Value WorkflowId
	from SplitComma(@PageMapNodeWorkFlow,',')
	Where LEN(Value)>0
	except
	Select WorkflowId 
	from PageMapNodeWorkflow
	Where PageMapNodeId=@PageMapNodeId
	)F

	exec PageMapBase_UpdateLastModification @SiteId
	exec PageMap_UpdateHistory @SiteId, @PageMapNodeId, 2, 1

END
GO

PRINT 'Migration lead form Email Opt-in default value to separate property excluded from translation'
GO

UPDATE	Forms
SET		XMLString = dbo.Regex_ReplaceMatches(CAST(XMLString AS NVARCHAR(MAX)), '(?<=\<GeneralField[^>]+)defaultValue="true"(?=[^>]+controltype="EmailOptIn")', 0, 'checked="True"')
FROM	Forms
WHERE	FormType = 2
GO
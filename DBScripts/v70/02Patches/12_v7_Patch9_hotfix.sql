IF(OBJECT_ID('ProductImage_GetImageForProducts') IS NOT NULL)
	DROP PROCEDURE ProductImage_GetImageForProducts
GO
CREATE PROCEDURE [dbo].[ProductImage_GetImageForProducts]
    (
      @ProductIds XML = NULL ,
      @IncludeSKUs BIT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    ) 
AS --********************************************************************************
-- Variable declaration
--********************************************************************************
    BEGIN
--********************************************************************************
-- code
--********************************************************************************
                DECLARE @ProductIdTable TABLE ( Id UNIQUEIDENTIFIER )
                INSERT  INTO @ProductIdTable
                        ( Id
                        )
                        SELECT  tab.col.value('text()[1]', 'uniqueidentifier')
                        FROM    @ProductIds.nodes('/ObjectIdCollection/ObjectId') tab ( col )
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
                        INNER JOIN CLImage C ON OI.ImageId = C.ParentImage AND OI.SiteId=C.SiteId
				WHERE (@ApplicationId is null or C.SiteId = @ApplicationId)
						AND C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                

				union all

				SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
                        INNER JOIN CLImage C ON C.ParentImage is null AND OI.ImageId = C.Id AND OI.SiteId=C.SiteId
				WHERE (@ApplicationId is null or C.SiteId = @ApplicationId)
						AND C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                
    END

    GO

IF(OBJECT_ID('ProductImage_GetImageForProduct') IS NOT NULL)
 DROP PROCEDURE ProductImage_GetImageForProduct
GO
CREATE PROCEDURE [dbo].[ProductImage_GetImageForProduct]
    (
      @ProductId UNIQUEIDENTIFIER = NULL ,
      @ProductIds XML = NULL ,
      @IncludeSKUs BIT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    ) WITH RECOMPILE
AS --********************************************************************************
-- Variable declaration
--********************************************************************************
    BEGIN
--********************************************************************************
-- code
--********************************************************************************

        IF @ProductIds IS NULL 
            BEGIN
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        @ProductId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(@ProductId,
                                                              C.Id)
                FROM    CLObjectImage OI 
                        INNER JOIN CLImage C ON OI.ImageId = C.ParentImage --ISNULL(C.ParentImage, C.Id)
                WHERE   C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                        AND OI.ObjectId = @ProductId
                
				union all

				SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        @ProductId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(@ProductId,
                                                              C.Id)
                FROM    CLObjectImage OI 
                        INNER JOIN CLImage C ON C.ParentImage is null AND OI.ImageId = C.Id --ISNULL(C.ParentImage, C.Id)
                WHERE   C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                        AND OI.ObjectId = @ProductId
            END
        ELSE 
            BEGIN
                DECLARE @ProductIdTable TABLE ( Id UNIQUEIDENTIFIER )
                INSERT  INTO @ProductIdTable
                        ( Id
                        )
                        SELECT  tab.col.value('text()[1]', 'uniqueidentifier')
                        FROM    @ProductIds.nodes('/ObjectIdCollection/ObjectId') tab ( col )
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
	--INNER JOIN CLImage I on I.Id=OI.ImageId
                        INNER JOIN CLImage C ON OI.ImageId = ISNULL(C.ParentImage,
                                                              C.Id)
	--Inner join @ProductIds.nodes('/ObjectIdCollection/ObjectId')tab(col) ON  OI.ObjectId =tab.col.value('text()[1]','uniqueidentifier')
                WHERE   --I.SiteId=Isnull(@ApplicationId,I.SiteId) 
	--(@ApplicationId is null or C.SiteId = @ApplicationId)
	--AND 
                        C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                ORDER BY OI.Sequence ,
                        C.Title
            END
    END
GO
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_CLImage_ParentImage_Status_SiteId' AND object_id = OBJECT_ID('CLImage'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_CLImage_ParentImage_Status_SiteId] ON [dbo].[CLImage]
	(
		[ParentImage] ASC,
		[Status] ASC,
		[SiteId] ASC
	)
	INCLUDE([Title],[Description],[FileName],[Size],[Attributes],[RelativePath],[AltText],[Height],[Width],[ImageType],[CreatedDate],[CreatedBy],[ModifiedDate],[ModifiedBy]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_CLImage_Id_ParentImage_Status_SiteId' AND object_id = OBJECT_ID('CLImage'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_CLImage_Id_ParentImage_Status_SiteId] ON [dbo].[CLImage]
(
	[Id] ASC,
	[ParentImage] ASC,
	[Status] ASC,
	[SiteId] ASC
)
INCLUDE([Title],[Description],[FileName],[Size],[Attributes],[RelativePath],[AltText],[Height],[Width],[ImageType],[CreatedDate],[CreatedBy],[ModifiedDate],[ModifiedBy]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_CLObjectImage_ObjectId_ImageId_SiteId' AND object_id = OBJECT_ID('CLObjectImage'))
BEGIN
CREATE NONCLUSTERED INDEX [IX_CLObjectImage_ObjectId_ImageId_SiteId] ON [dbo].[CLObjectImage]
(
	[ObjectId] ASC,
	[ImageId] ASC,
	[SiteId] ASC
)
INCLUDE([Id],[ObjectType],[IsPrimary],[Sequence]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END

GO
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_ObjectType' AND object_id = OBJECT_ID('CLObjectImage'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_ObjectType] ON [dbo].[CLObjectImage]
	(
		[ObjectType] ASC
	)
	INCLUDE([ObjectId],[ImageId],[IsPrimary],[Sequence]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END

IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Search.Provider' AND ControlData like '%BRIDGELINE%')
BEGIN
	UPDATE STSettingType SET 
		ControlData = '{ "ELASTIC" : "ELASTIC", "ISYS" : "ISYS", "BRIDGELINE" : "BRIDGELINE" }' 
	WHERE Name = 'Search.Provider'
END
GO

DECLARE @Sequence int, @ModifiedBy uniqueidentifier
SELECT @ModifiedBy = Id FROM USUser WHERE Username like 'iappssystemuser'
IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.ApiUrl')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.ApiUrl', 'https://50h5v7dmqb.execute-api.us-east-1.amazonaws.com', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
ELSE
BEGIN 
 Update SESearchSetting SET Title='Search.ApiUrl', Provider='BRIDGELINE'
 Where  Title ='Search.Celebros.ApiUrl'
 END

IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.ApiKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.ApiKey', '4svsGY1FDW9tXhoO0lqAnl3O8HaPvRY3PEamr9s5', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
ELSE
BEGIN 
 Update SESearchSetting SET Title='Search.ApiKey', Provider='BRIDGELINE'
 Where  Title ='Search.Celebros.ApiKey'
END

IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.SiteKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.SiteKey', REPLACE(Title, ' ', '-'), @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
ELSE
BEGIN 
 Update SESearchSetting SET Title='Search.SiteKey', Provider='BRIDGELINE'
 Where  Title ='Search.Celebros.SiteKey'
END
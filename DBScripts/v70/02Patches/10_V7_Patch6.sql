IF(COL_LENGTH('MKCampaignRunWorkTable', 'ProcessingBatchId') IS NULL)
	ALTER TABLE MKCampaignRunWorkTable ADD ProcessingBatchId uniqueidentifier NULL
GO
IF (OBJECT_ID('CampaignContact_Get') IS NOT NULL)
	DROP PROCEDURE CampaignContact_Get
GO
CREATE PROCEDURE [dbo].[CampaignContact_Get]
(
	@SiteId					uniqueidentifier,
	@CampaignRunId			uniqueidentifier,
	@BatchSize				int,
	@ProcessingStatus		int = NULL,
	@MarkAsProcessing		bit = NULL
)
AS
BEGIN
	IF NOT EXISTS(SELECT 1 FROM MKCampaignRunWorkTable WHERE @CampaignRunId = CampaignRunId)
		RETURN

	DECLARE @UtcNow datetime, @BatchId uniqueidentifier, @TotalRecords int
	SET @UtcNow = GETUTCDATE()

	IF @ProcessingStatus IS NULL SET @ProcessingStatus = 0

	SET @BatchId = NEWID()
		
	;WITH CTE AS
	(
		SELECT TOP (@BatchSize) UserId,
			ProcessingBatchId,
			ProcessingCount,
			ProcessingStatus,
			ProcessingTime
		FROM MKCampaignRunWorkTable C
		WHERE @CampaignRunId = CampaignRunId 
			AND ProcessingStatus = @ProcessingStatus
			AND (SendDate IS NULL OR SendDate <= @UtcNow)
		ORDER BY SendDate
	)

	UPDATE CTE
	SET ProcessingBatchId = @BatchId,
		ProcessingCount = CASE WHEN @MarkAsProcessing = 1 THEN ProcessingCount + 1 ELSE ProcessingCount END, 
		ProcessingStatus = CASE WHEN @MarkAsProcessing = 1 THEN 1 ELSE ProcessingStatus END,
		ProcessingTime = CASE WHEN @MarkAsProcessing = 1 THEN @UtcNow ELSE ProcessingTime END

	SELECT @TotalRecords = COUNT(1) FROM MKCampaignRunWorkTable C
	WHERE @CampaignRunId = CampaignRunId 
		AND (ProcessingStatus = @ProcessingStatus OR ProcessingBatchId = @BatchId)

	-- Contacts
	SELECT W.CampaignRunId,
		W.UserId Id,
		W.FirstName,
		W.MiddleName,
		W.LastName,
		W.CompanyName,
		W.BirthDate,
		W.Gender,
		W.AddressId,
		W.Status,
		W.HomePhone,
		W.MobilePhone,
		W.OtherPhone,
		W.Email,
		W.Notes,
		W.ContactType,
		ISNULL(W.SiteId, @SiteId) AS SiteId,
		ISNULL(W.SiteId, @SiteId) AS PrimarySiteId,
		@TotalRecords
	FROM MKCampaignRunWorkTable W
	WHERE ProcessingBatchId = @BatchId

	DECLARE @tbContacts TABLE 
	(
		ContactId		uniqueidentifier primary key,
		AddressId		uniqueidentifier
	)

	INSERT INTO @tbContacts
	SELECT UserId, AddressId FROM MKCampaignRunWorkTable W
	WHERE ProcessingBatchId = @BatchId

	-- Address
	SELECT A.Id,
		A.FirstName,
		A.LastName,
		A.AddressType,
		A.AddressLine1,
		A.AddressLine2,
		A.AddressLine3,
		A.City,
		A.StateId,
		A.Zip,		
		A.CountryId,
		A.Phone,
		A.CreatedDate,
		A.CreatedBy,
		A.ModifiedDate,
		A.ModifiedBy,
		A.Status,
		A.County,
		A.CountryName,
		A.CountryCode,
		A.StateCode,
		A.StateName
	FROM vw_Address A
		JOIN @tbContacts C ON C.AddressId = A.Id

	SELECT V.Id,
		V.ContactId,
		V.AttributeId,
		V.AttributeEnumId,
		V.Value,
		V.Notes,
		V.CreatedBy,
		V.CreatedDate,
		A.Title AS AttributeTitle
	FROM ATContactAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbContacts T ON V.ContactId = T.ContactId

	UNION ALL

	SELECT V.Id,
		V.CustomerId AS ContactId,
		V.AttributeId,
		V.AttributeEnumId,
		V.Value,
		NULL AS Notes,
		V.CreatedBy,
		V.CreatedDate,
		A.Title AS AttributeTitle
	FROM CSCustomerAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbContacts T ON V.CustomerId = T.ContactId
END	
GO
PRINT 'Modify stored procedure CampaignRunHistoryDto_CheckWorkTable'
GO

IF (OBJECT_ID('CampaignRunHistoryDto_CheckWorkTable') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_CheckWorkTable
GO
CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_CheckWorkTable]
(
	@CampaignRunId	uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @CampaignId uniqueidentifier
	SET @UtcNow = GETUTCDATE()

	SELECT TOP 1 @CampaignId = CampaignId FROM MKCampaignRunHistory WHERE Id = @CampaignRunId

	--Check for failed records (To avoid campaign to be running forever)
	UPDATE MKCampaignRunWorkTable SET
		ProcessingStatus = CASE WHEN ProcessingCount < 2 THEN 0 ELSE 4 END, --To avoid any infinite loop
		ProcessingCount = ProcessingCount + 1
	WHERE CampaignRunId = @CampaignRunId 
		AND ProcessingStatus = 1 
		AND DATEDIFF(MINUTE, ProcessingTime, @UtcNow) > 30

	INSERT INTO MKCampaignRunErrorLog 
	(
		Id,
		CampaignId,
		CampaignRunId,
		ContactId,
		ErrorMessage
	)
	SELECT NEWID(),
		@CampaignId,
		@CampaignRunId,
		UserId,
		LogMessage 
	FROM MKCampaignRunWorkTable 
	WHERE CampaignRunId = @CampaignRunId
		AND ProcessingStatus = 4

	DELETE FROM  MKCampaignRunWorkTable 
	WHERE CampaignRunId = @CampaignRunId AND ProcessingStatus = 4
END
GO
IF (OBJECT_ID('CampaignRunHistoryDto_Reconcile') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_Reconcile
GO
CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_Reconcile]
(
	@CampaignRunId	uniqueidentifier
)
AS
BEGIN
	EXEC [dbo].[CampaignRunHistoryDto_UpdateResponse] @Id = @CampaignRunId
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_MKContact_Status_Id' AND object_id = OBJECT_ID('MKContact'))
	CREATE NONCLUSTERED INDEX [IX_MKContact_Status_Id]
		ON [dbo].[MKContact] ([Status]) INCLUDE ([Id])
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_SISite_Status_Lft_Rgt' AND object_id = OBJECT_ID('SISite'))
	CREATE NONCLUSTERED INDEX [IX_SISite_Status_Lft_Rgt] 
		ON [dbo].[SISite]([Status] ASC, [LftValue] ASC, [RgtValue] ASC)
GO

IF (OBJECT_ID('GetDynamicOperator') IS NOT NULL)
	DROP FUNCTION GetDynamicOperator
GO
CREATE FUNCTION [dbo].[GetDynamicOperator] (@Operator varchar(100), @DataType varchar(50), @Value1 varchar(1000), @Value2 varchar(1000), @AttributeId varchar(100), @ApplicationId uniqueidentifier)
RETURNS Varchar(max)
AS
BEGIN
	DECLARE @Dynamic varchar(max), @LocalApplicationId varchar(100), @LocalDate varchar(100)
	SET @LocalApplicationId  = QUOTENAME(CAST(@Applicationid as varchar(100)),'''') 

	SET @LocalDate = CONVERT(VARCHAR(100), dbo.GetLocalDate('6B142336-161E-436B-BE24-E3ACB462BBF0'), 121)

	IF (@DataType = 'System.Double' OR @DataType = 'System.Int32')
	BEGIN
		SELECT @Dynamic =
			CASE @Operator
				WHEN 'opOperatorContains' THEN ' LIKE ''%' + @Value1 + '%'''
				WHEN 'opOperatorEqual' THEN ' = '  + @Value1  
				WHEN 'opOperatorBetween' THEN ' BETWEEN '  + @Value1 + ' AND ' + @Value2
				WHEN 'opOperatorLessThanEqual' THEN ' <= '  + @Value1 
				WHEN 'opOperatorLessThan' THEN ' < '  + @Value1 
				WHEN 'opOperatorGreaterThanEqual' THEN ' >= '  + @Value1 
				WHEN 'opOperatorGreaterThan' THEN ' > '  + @Value1 
				WHEN 'opOperatorNotEqual' THEN ' != '  + @Value1 
				WHEN 'opOperatorDoesNotContain' THEN ' NOT LIKE ''%' + @Value1 + '%'''
			END
	END
	ELSE IF (@DataType = 'System.DateTime') --Date
	BEGIN	
		SELECT @Dynamic =
			CASE @Operator
				WHEN 'opOperatorContains' THEN ' LIKE ''%' + @Value1 + '%'''
				WHEN 'opOperatorEqual' THEN ' = '''  + @Value1 + ''''
				WHEN 'opOperatorBetween' THEN ' BETWEEN '''  + @Value1 + ''' AND ''' + @Value2 + ''''
				WHEN 'opOperatorLessThanEqual' THEN ' <= '''  + @Value1 + ''''
				WHEN 'opOperatorLessThan' THEN ' < '''  + @Value1 + ''''
				WHEN 'opOperatorGreaterThanEqual' THEN ' >= '''  + @Value1 + ''''
				WHEN 'opOperatorGreaterThan' THEN ' > '''  + @Value1 + ''''
				WHEN 'opOperatorNotEqual' THEN ' != '''  + @Value1 + ''''
				WHEN 'opOperatorDoesNotContain' THEN ' NOT LIKE ''%' + @Value1 + '%'''
				WHEN 'opOnDay' THEN ' DATEADD(dd, 0, DATEDIFF(dd, 0, ' + @AttributeId + ')) = DATEADD(dd, 0, DATEDIFF(dd, 0, ''' + @LocalDate + '''))'
				WHEN 'opOnNthDayAfter' THEN ' DATEDIFF(day,''' + @LocalDate + ''', ' + @AttributeId + ' )  =  CAST( ''' + @Value1  + ''' AS INT) '
				WHEN 'opOnNthDayBefore' THEN ' DATEDIFF(day, ' + @AttributeId  + ' ,''' + @LocalDate + ''')  =  CAST( ''' + @Value1  + ''' AS INT) '
				WHEN 'opFortheFollowingXDays' THEN ' DATEDIFF(day,''' + @LocalDate + ''', ' + @AttributeId + ')   between 1 and  CAST( ''' + @Value1 + ''' AS INT) '
				WHEN 'opForthePreviousXDays' THEN ' DATEDIFF(day, ' +  @AttributeId + ' ,''' + @LocalDate + ''')    between 1 and  CAST( ''' + @Value1 + ''' AS INT) '
				WHEN 'opFortheDaysBeforeNthDay' THEN ' DATEDIFF(day, ' +  @AttributeId + ' ,''' + @LocalDate + ''')  >  CAST( ''' + @Value1 + ''' AS INT) '
				WHEN 'opFortheDaysAfterNthDay' THEN ' DATEDIFF(day,''' + @LocalDate + ''', ' +  @AttributeId + ' )  >  CAST( ''' + @Value1 + ''' AS INT) '
			END
	END
	ELSE -- String,  Boolean and for any other data type 
	BEGIN	
		SELECT @Dynamic =
			CASE @Operator
				WHEN 'opOperatorContains' THEN ' LIKE ''%' + @Value1 + '%'''
				WHEN 'opOperatorEqual' THEN ' = '''  + @Value1 + ''''
				WHEN 'opOperatorBetween' THEN ' BETWEEN '''  + @Value1 + ''' AND ''' + @Value2 + ''''
				WHEN 'opOperatorLessThanEqual' THEN ' <= '''  + @Value1 + ''''
				WHEN 'opOperatorLessThan' THEN ' < '''  + @Value1 + ''''
				WHEN 'opOperatorGreaterThanEqual' THEN ' >= '''  + @Value1 + ''''
				WHEN 'opOperatorGreaterThan' THEN ' > '''  + @Value1 + ''''
				WHEN 'opOperatorNotEqual' THEN ' != '''  + @Value1 + ''''
				WHEN 'opOperatorDoesNotContain' THEN ' NOT LIKE ''%' + @Value1 + '%'''
			END
	END

	RETURN @Dynamic
END
GO
IF (OBJECT_ID('Contact_ContactAttributeSearch') IS NOT NULL)
	DROP PROCEDURE Contact_ContactAttributeSearch
GO
CREATE PROCEDURE [dbo].[Contact_ContactAttributeSearch]
(
	@ContactAttributeSearchXml	xml,
	@ContactSource				varchar(100),
    @ApplicationId				uniqueidentifier
)
AS
BEGIN
	DECLARE	@Condition varchar(3)
	SET @Condition = @ContactAttributeSearchXml.value('(/ContactAttributeSearch/@Condition)[1]', 'varchar(3)')
	IF @Condition IS NULL SET @Condition ='OR'

	DECLARE @ContactSearch TYContactAttributeSearch
    INSERT INTO @ContactSearch
    ( 
		ConditionId,
        AttributeId,
        AttributeValueID,
        Value1,
        Value2,
        Operator,
        DataType,
        ValueCount
    )
    SELECT NEWID() AS ConditionId ,
		col.value('@AttributeId', 'uniqueidentifier') AS AttributeId ,
		NULL,
		col.value('@Value1', 'nvarchar(max)') AS Value1 ,
		col.value('@Value2', 'nvarchar(max)') AS Value2 ,
		col.value('@Operator', 'nvarchar(50)') AS Operator ,
		'System.String' AS DataType,
		1
    FROM @ContactAttributeSearchXml.nodes('/ContactAttributeSearch/AttributeSearchItem') tab (col)

	DECLARE @ContactSearchLocal TABLE
	(
		RowId		uniqueidentifier primary key,
		AttributeId uniqueidentifier,
		Value1		nvarchar(1000),
		Value2		nvarchar(1000),
		Operator	nvarchar(50),
		DataType	varchar(50),
		Processed	bit
	)
	
	DECLARE @AttributeLocal TABLE (Id UniqueIdentifier)
			
	INSERT INTO @AttributeLocal
	SELECT A.Id FROM ATAttribute A 
		INNER JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND (C.CategoryId = 3 OR C.CategoryId = 4)
	WHERE A.Status = 1

	INSERT INTO @AttributeLocal VALUES ('11111111-0000-0000-0000-000000000000')
	INSERT INTO @AttributeLocal VALUES ('22222222-0000-0000-0000-000000000000')
	INSERT INTO @AttributeLocal VALUES ('33333333-0000-0000-0000-000000000000')
	INSERT INTO @AttributeLocal VALUES ('44444444-0000-0000-0000-000000000000')
	INSERT INTO @AttributeLocal VALUES ('55555555-0000-0000-0000-000000000000')
	INSERT INTO @AttributeLocal VALUES ('66666666-0000-0000-0000-000000000000')
	INSERT INTO @AttributeLocal VALUES ('77777777-0000-0000-0000-000000000000')

	INSERT INTO @ContactSearchLocal
	Select ConditionId, AttributeId, Value1, Value2, Operator, DataType, 0 
	FROM @ContactSearch C
	INNER JOIN @AttributeLocal A ON A.Id = C.AttributeId 

	UPDATE CS
	SET CS.DataType = ADT.TYPE
	FROM @ContactSearchLocal CS 
    INNER JOIN dbo.ATAttribute A ON A.Id = CS.AttributeId
    INNER JOIN dbo.ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId 

	UPDATE @ContactSearchLocal SET DataType = 'System.DateTime' WHERE AttributeId = '66666666-0000-0000-0000-000000000000'
	UPDATE @ContactSearchLocal SET DataType = 'System.Int32' WHERE AttributeId = '77777777-0000-0000-0000-000000000000'

	CREATE TABLE #ContactIds (ContactId uniqueidentifier)

	DECLARE @Sql varchar(max), @Datetimesql varchar(256), @MinDateTime varchar(256), @QAttributeId varchar(100), @QDAttributeId varchar(100)
	SET @Sql = ''
	SET @MinDateTime = '1900-01-01 00:00:00.000'

	DECLARE @AttributeId uniqueidentifier, @Value1 nvarchar(1000), @Value2 nvarchar(1000), @RowId  uniqueidentifier, @Operator varchar(100), @DataType varchar(50)
	IF EXISTS(Select top 1 RowId from @ContactSearchLocal)
	BEGIN
		SET @Sql = 'INSERT INTO #ContactIds Select ContactId from DNContactAttribute' 

		WHILE EXISTS(SELECT 1 FROM @ContactSearchLocal WHERE Processed = 0)
		BEGIN
			Select top 1 @AttributeId = AttributeId, @RowId = RowId, @Value1= Value1, @Value2 = Value2, @Operator = Operator, @DataType = DataType from @ContactSearchLocal where Processed = 0
					
			Set @Datetimesql = ''
			Set @QAttributeId  = QUOTENAME(CAST(@AttributeId AS Varchar(100)))
			Set @QDAttributeId  = @QAttributeId

			IF(@DataType = 'System.DateTime')
				Set @Datetimesql = @QAttributeId + ' != '  + QUOTENAME(@MinDateTime, '''') + ' AND '

			IF(@Operator = 'opOnDay' OR @Operator = 'opOnNthDayAfter' OR @Operator = 'opOnNthDayBefore' 
				OR @Operator = 'opFortheFollowingXDays' OR @Operator = 'opForthePreviousXDays'
				OR @Operator = 'opFortheDaysBeforeNthDay' OR @Operator = 'opFortheDaysAfterNthDay')
				Set @QAttributeId = ''

			IF (CHARINDEX(' WHERE ', @Sql)) > 0
				SET @Sql = @Sql + ' '+ @Condition +' (' + @Datetimesql + @QAttributeId +  dbo.GetDynamicOperator (@Operator, @DataType, @Value1, @Value2, @QDAttributeId, @ApplicationId) + ' ) '
			else
				SET @Sql = @Sql + ' WHERE (' +  @Datetimesql + @QAttributeId + dbo.GetDynamicOperator (@Operator, @DataType, @Value1, @Value2, @QDAttributeId, @ApplicationId) + ' ) '
	
			Update @ContactSearchLocal Set Processed = 1 where RowId = @RowId
		END
	END

	PRINT (@Sql) 

	IF(@Sql != '') EXEC (@Sql)

	DECLARE @CurrentRecords int
	SELECT @CurrentRecords = COUNT(ID) FROM #tempContactSearchOutput

	IF @CurrentRecords > 0 
	BEGIN
		DELETE T FROM #tempContactSearchOutput T
			LEFT JOIN #ContactIds LT ON LT.ContactId = T.Id
		WHERE LT.ContactId IS NULL
	END
	ELSE 
	BEGIN 
		INSERT INTO #tempContactSearchOutput
		SELECT ContactId FROM #ContactIds
	END

	SELECT @@ROWCOUNT
END
GO
IF (OBJECT_ID('Contact_FilterBySiteContext') IS NOT NULL)
	DROP PROCEDURE Contact_FilterBySiteContext
GO
CREATE PROCEDURE [dbo].[Contact_FilterBySiteContext]
(
	@ApplicationId	uniqueidentifier,
	@ReturnScalar	bit = 1
)
AS 
BEGIN
IF EXISTS (SELECT TOP 1 Id FROM #tempContactSearchOutput) 
BEGIN

	DECLARE @Lft bigint, @Rgt bigint, @MasterSiteId uniqueidentifier
    DECLARE @SiteIds TABLE(SiteId uniqueidentifier primary key)
	
	SELECT @Lft = LftValue, @Rgt = RgtValue
    FROM SISite WHERE Id = @ApplicationId
                
	INSERT INTO @SiteIds
    SELECT DISTINCT Id FROM dbo.SISite
    WHERE LftValue >= @Lft AND RgtValue <= @Rgt AND Status = 1

	CREATE CLUSTERED INDEX IX_tempContactSearchOutput ON #tempContactSearchOutput(Id)

	DELETE T
	FROM #tempContactSearchOutput T
		JOIN (SELECT Id FROM dbo.MKContact WHERE Status != 1
					UNION ALL
				SELECT Id FROM dbo.USUser WHERE Status != 1
				)  C ON T.Id = C.Id

	DELETE T
    FROM #tempContactSearchOutput T
		LEFT JOIN (	SELECT CS.ContactId AS UserId FROM dbo.MKContactSite CS
						INNER JOIN @SiteIds S ON CS.SiteId = S.SiteId
                    UNION ALL
                    SELECT UserId FROM dbo.USSiteUser CS
                        INNER JOIN @SiteIds S ON CS.SiteId = S.SiteId
                ) TV ON TV.UserId = T.Id
    WHERE TV.UserId IS NULL

    IF(@ReturnScalar = 1)
		SELECT COUNT(Id) FROM #tempContactSearchOutput
END
ELSE
	SELECT  0
END
GO

IF (OBJECT_ID('vwPageAttributeValue') IS NOT NULL)
	DROP VIEW vwPageAttributeValue
GO
CREATE VIEW [dbo].[vwPageAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V

	UNION

	SELECT ROW_NUMBER() over (PARTITION BY V.AttributeId, C.PageDefinitionId ORDER BY S.LftValue DESC) AS vRank,
		V.Id,
		C.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V
		JOIN PageDefinition P ON V.PageDefinitionId = P.PageDefinitionId
		JOIN PageDefinition C ON C.SourcePageDefinitionId = P.PageDefinitionId
		JOIN SISite S ON S.Id = P.SiteId
	WHERE V.IsShared = 1
		AND S.DistributionEnabled = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT C.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	ISNULL(V.Notes, C.Notes) AS Notes,
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATPageAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.PageDefinitionId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1
GO

IF (OBJECT_ID('ObjectUrlDto_GetById') IS NOT NULL)
	DROP PROCEDURE ObjectUrlDto_GetById
GO
CREATE PROCEDURE [dbo].[ObjectUrlDto_GetById]
(
	@Urls			TYObjectUrlById READONLY,
	@SiteId			UNIQUEIDENTIFIER,
	@PreferSource	BIT
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ObjectUrls TABLE (
		TokenSiteId		UNIQUEIDENTIFIER,
		TokenObjectId	UNIQUEIDENTIFIER,
		TokenObjectType	INT,
		SiteId			UNIQUEIDENTIFIER,
		ObjectId		UNIQUEIDENTIFIER,
		ObjectType		INT,
		[Url]			NVARCHAR(2048)
	)

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType = 2)
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Menus AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Menus AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType = 2
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType = 8)
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Pages AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Pages AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType = 8
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType IN (9, 33))
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Files AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Files AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType IN (9, 33)
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType IN (39, 40))
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Blogs AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Blogs AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType IN (39, 40)
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType = 205)
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Products AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Products AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType = 205
	END

	SELECT	TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url]
	FROM	@ObjectUrls
END
GO
GO

IF (OBJECT_ID('PageDefinitionContainer_GetContainerXml') IS NOT NULL)
	DROP FUNCTION PageDefinitionContainer_GetContainerXml
GO
CREATE FUNCTION [dbo].[PageDefinitionContainer_GetContainerXml]
(
	@PageDefinitionId uniqueidentifier,
	@PageDefinitionSegmentId uniqueidentifier,
	@ContainerId uniqueidentifier
)
RETURNS xml
AS
BEGIN
	DECLARE @customAttributesXml XML;
	
	SELECT	@customAttributesXml = CustomAttributes 
	FROM	PageDefinitionContainer 
	WHERE	ContainerId = @ContainerId 
			AND PageDefinitionSegmentId = @PageDefinitionSegmentId
			AND PageDefinitionId = @PageDefinitionId

	DECLARE @attributesAndValues varchar(max)

	SELECT @attributesAndValues = COALESCE(@attributesAndValues + ' ', '') + 
		CAST(Attribute.Name.query('local-name(.)') AS VARCHAR(512)) + '="' +
		Attribute.Name.value('.','VARCHAR(1024)') + '"'
	FROM @customAttributesXml.nodes('/customAttributes/@*') Attribute(Name)


	DECLARE @ContainerXml nvarchar(max)
	SET @ContainerXml = ''

	SELECT @ContainerXml = 
		'<container id="' + CAST(ContainerId as nvarchar(36)) +'" 
			name="' + ContainerName +'" 
			contentId="' + CAST(ContentId as nvarchar(36)) +'" 
			inWFContentId="' + CAST(InWFContentId as nvarchar(36)) +'" 
			contentTypeId="' + CAST(ContentTypeId as nvarchar(10)) +'" 
			isModified="' + CASE IsModified WHEN 1 THEN 'True' ELSE 'False' END +'" 
			isTracked="' + CASE IsTracked WHEN 1 THEN 'True' ELSE 'False' END +'" 
			visible="' + CASE Visible WHEN 1 THEN 'True' ELSE 'False' END +'" 
			inWFVisible="' + CASE InWFVisible WHEN 1 THEN 'True' ELSE 'False' END +'" 
			' + COALESCE(@attributesAndValues, '') + ' />'
	FROM PageDefinitionContainer C
	WHERE C.PageDefinitionSegmentId = @PageDefinitionSegmentId
		AND C.PageDefinitionId = @PageDefinitionId
		AND C.ContainerId = @ContainerId

	RETURN CAST(@ContainerXml as xml)
END
GO

IF (OBJECT_ID('Page_GetContainerXml') IS NOT NULL)
	DROP FUNCTION Page_GetContainerXml
GO
CREATE FUNCTION [dbo].[Page_GetContainerXml]
(
	@PageDefinitionId uniqueidentifier,
	@AudienceSegmentId uniqueidentifier,
	@DeviceId uniqueidentifier
)
RETURNS xml
AS
BEGIN
	DECLARE @ContainerXml nvarchar(max)
	SET @ContainerXml = ''

	SELECT @ContainerXml = @ContainerXml +
		CAST([dbo].[PageDefinitionContainer_GetContainerXml](@PageDefinitionId,  C.PageDefinitionSegmentId, C.ContainerId) AS nvarchar(max))
	FROM PageDefinitionContainer C
		JOIN PageDefinitionSegment S ON S.Id = C.PageDefinitionSegmentId
	WHERE C.PageDefinitionId = @PageDefinitionId
		AND S.AudienceSegmentId = @AudienceSegmentId 
		AND S.DeviceId = @DeviceId
	ORDER BY ContainerName

	RETURN CAST('<Containers>' + @ContainerXml + '</Containers>' as xml)
END
GO
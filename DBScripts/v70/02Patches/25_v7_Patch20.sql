DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int
SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Email Settings'
IF NOT EXISTS (Select 1 from STSettingType where Name = 'Marketier.AllowToOptOutUnsubscribeHeader')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Marketier.AllowToOptOutUnsubscribeHeader', 'This will enable the option to toggle one click unsubscribe email header', @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
END
IF NOT EXISTS (Select 1 from STSettingType where Name = 'Marketier.AllowToIncludeUnsubscribedUsers')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Marketier.AllowToIncludeUnsubscribedUsers', 'This will allow to enable sending email to unsubscribed users', @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
END
SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Payment Settings'
IF @SettingGroupId IS NULL SET @SettingGroupId = 6
IF NOT EXISTS(SELECT 1 from STSettingType WHERE Name ='AuthorizeNetAPILogin')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, IsEnabled, IsVisible, ControlType)
	VALUES('AuthorizeNetAPILogin','API Login Id',@settingGroupId,@sequence,1,1,'Textbox')
	
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @settingTypeId, '9Zt7x2Ah8M3r' 
	FROM SISite
	WHERE ParentSiteId='00000000-0000-0000-0000-000000000000'
END
ELSE 
BEGIN 
	UPDATE STSettingType SET SettingGroupId=@settingGroupId, IsVisible=1  Where Name='AuthorizeNetAPILogin'
END

IF NOT EXISTS(SELECT 1 from STSettingType WHERE Name ='AuthorizeNetClientKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,Sequence,IsEnabled,IsVisible,ControlType)
	VALUES('AuthorizeNetClientKey','public key for frontend token creation',@settingGroupId,@sequence,1,1,'Textbox')
		
	SET @settingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	SELECT Id,@settingTypeId,'6E5HWV8AxtW7u8k9T9RYz5ar5psrPzX7PgKSMp5zZF7Xq2VFa3bMH9RrXzJmp37n' 
	FROM SISite
	WHERE ParentSiteId='00000000-0000-0000-0000-000000000000'
	SET @sequence =@sequence +1	
END
ELSE
BEGIN 
	UPDATE STSettingType SET SettingGroupId=@settingGroupId,IsVisible=1  Where Name='AuthorizeNetClientKey'
END

IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'UPSoAuthToken')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('UPSoAuthToken', 'UPS oAuth Token',  @SettingGroupId, @Sequence, 'TextBox', 1, 0)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '{"refresh_token_expires_in":"5183999","refresh_token_status":"approved","token_type":"Bearer","issued_at":"1717181986484","client_id":"lqb5ZcevcA3PoUJm7CEy1DEO6xsFUn6HCgdVFaFMCgtJNAtG","access_token":"eyJraWQiOiI2NGM0YjYyMC0yZmFhLTQzNTYtYjA0MS1mM2EwZjM2Y2MxZmEiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzM4NCJ9.eyJzdWIiOiJ2cHJha2FzaEBibGluZWRpZ2l0YWwuY29tIiwiY2xpZW50aWQiOiJscWI1WmNldmNBM1BvVUptN0NFeTFERU82eHNGVW42SENnZFZGYUZNQ2d0Sk5BdEciLCJpc3MiOiJodHRwczovL2FwaXMudXBzLmNvbSIsInV1aWQiOiI2RjIwQzg0OC01RDExLTFGMjktOUNERi02RTM2NzUxNkRDNkQiLCJzaWQiOiI2NGM0YjYyMC0yZmFhLTQzNTYtYjA0MS1mM2EwZjM2Y2MxZmEiLCJhdWQiOiJVbmJvdW5kIiwiYXQiOiJpME5ENmxYMjh3R3dYaUZJZHpmZWU0WmV6VHNZIiwibmJmIjoxNzE3MTgxOTg2LCJEaXNwbGF5TmFtZSI6IlVuYm91bmQiLCJleHAiOjE3MTcxOTYzODYsImlhdCI6MTcxNzE4MTk4NiwianRpIjoiYzNiMTllNWYtNDZmYi00ZjRlLWE5OGMtYjQ2YzFkM2I2MjUzIn0.oZt4GrhENXCgAs4MaKlxmNAhYdQcJp2sp4hrM4RorC5v5HhgtT0TUCnhnCpl-MXzPLrArfaIgWEitnDRY-ofJ0iRIANo4_JL_oa9mWLGSs2MPzTUhSEEMMXjtnkk6GZIBYJk1Zxj81kCZqqicmCCEOMUjz5OgEiBTZUFZxMjZ4rMsFZvTPtsGqfcIBM7tCkOgaxuGzb3sgbJXAe6dM8OCNP96bw0ccFTDdCqWw7NboBWp7rqsMD1f-yGDT_O6Y2EZDkqkxP_tmk1Yemz-sXF5UyBIayVuUGqELG6n6XJ6_-YZNlbrCfZE-e_X9CTkXL6T9q1GPaZGVFUE3mcC9mgXcddOEQd_jvAcbf0txwA8OZ8N1IeSVz2AD9kHAlENLJe5_nL-vEMdL6dDRqTczRpt2OpTLVKTPjavYeUvhFZhvPT9F9HK9FTx1QNcu6bl49xRXyD2nInXvev1_YmJ7g-LjASvaZcpGsmsy83fU_xtdsoOm7FAuCK4pXUzneWYo020mmpIh-IplY6C6Udoa99JRU3BNOVoU-gZvwsTFoyPPXOZOL1dhm9oVysYhhU1iV3hrnv_xmjOgWsP1eYG3NSUVOWVevmjzso_PbKM12ejFuQLezOHFHWlnXrORgCXvGxZZs3xSoXuTCJGWPpZFlgeLFAkHzdGM1OvxKqn26pT2s","refresh_token":"sJcuXLeCdkdkt27SXFf7bQPynYbRn1FC","refresh_token_issued_at":"1717181986484","expires_in":"14399","status":"approved"}' 
	FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
GO
PRINT 'Add column EnableUnsubscribeHeader to MKCampaign'
GO
IF(COL_LENGTH('MKCampaign', 'EnableUnsubscribeHeader') IS NULL)
BEGIN
	ALTER TABLE MKCampaign ADD [EnableUnsubscribeHeader] bit DEFAULT(1) NULL	
END
GO
UPDATE MKCampaign SET EnableUnsubscribeHeader = 1 WHERE EnableUnsubscribeHeader IS NULL
GO
PRINT 'Modify stored procedure CampaignDto_Get'
GO
IF(OBJECT_ID('CampaignDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignDto_Get
GO
CREATE PROCEDURE [dbo].[CampaignDto_Get]  
(  
	@Id						uniqueidentifier = NULL,  
	@SiteId					uniqueidentifier,
	@Status					int = NULL,
	@Type					int = NULL,
	@CampaignGroupId		uniqueidentifier = NULL,
	@ParentId				uniqueidentifier = NULL,
	@CampaignRunId			uniqueidentifier = NULL,
	@PageSize				int = NULL,   
	@PageNumber				int = NULL,
	@MaxRecords				int = NULL,
	@Keyword				nvarchar(MAX) = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@IgnoreDetails			bit = NULL,
	@Filter					int = NULL
)  
AS  
BEGIN
	DECLARE @EmptyGuid uniqueidentifier, @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @Id = @EmptyGuid SET @Id = NULL
	IF @CampaignGroupId = @EmptyGuid SET @CampaignGroupId = NULL
	IF @CampaignGroupId IS NULL AND @ParentId != @EmptyGuid SET @CampaignGroupId = @ParentId

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'
	IF @Filter = 0 SET @Filter = NULL
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbQueryIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)

	IF @Id IS NULL AND @CampaignRunId IS NOT NULL
		SELECT TOP 1 @Id = CampaignId FROM MKCampaignRunHistory WHERE Id = @CampaignRunId

	IF @Id IS NULL	
	BEGIN
		INSERT INTO @tbIds
		SELECT C.Id AS Id, ROW_NUMBER() OVER (ORDER BY 
			CASE WHEN @SortClause = 'Title ASC' THEN C.Title END ASC,
			CASE WHEN @SortClause = 'Title DESC' THEN C.Title END DESC,
			CASE WHEN @SortClause = 'CreatedDate ASC' THEN C.CreatedDate END ASC,
			CASE WHEN @SortClause = 'CreatedDate DESC' THEN C.CreatedDate END DESC,
			CASE WHEN @SortClause = 'ModifiedDate ASC' THEN S.ModifiedDate END ASC,
			CASE WHEN @SortClause = 'ModifiedDate DESC' THEN S.ModifiedDate END DESC,
			CASE WHEN @SortClause IS NULL THEN C.CreatedDate END DESC		
		) AS RowNumber
		FROM MKCampaign AS C
			JOIN vwCampaignSend S ON C.Id = S.CampaignId
			LEFT JOIN MKCampaignGroup G ON G.Id = C.CampaignGroupId
		WHERE (@Id IS NULL OR C.Id = @Id) AND
			(@Type IS NULL OR C.Type = @Type) AND 
			((@CampaignGroupId IS NULL AND S.status != 4) OR G.Id = @CampaignGroupId) AND
			(@Status IS NULL OR S.Status = @Status) AND
			(@Filter IS NULL OR 
				(
					(@Filter = 1 AND (S.Status = 2 OR S.Status = 5)) OR
					(@Filter = 2 AND (S.Status = 1 OR S.Status = 6)) OR
					(@Filter = 3 AND S.Status = 3) OR
					(@Filter = 5 AND (S.Status = 1 OR S.Status = 3 OR S.Status = 6))
				)
			) AND
			(@SiteId IS NULL OR S.SiteId = @SiteId) AND
			(@Keyword IS NULL OR C.Title like @Keyword OR C.Description like @Keyword)
		OPTION (RECOMPILE)

		;WITH CTE AS(
			SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				C.Id AS Id
			FROM MKCampaign C
				JOIN @tbIds T ON T.Id = C.Id
		)
	
		INSERT INTO @tbPagedResults
		SELECT Id, RowNumber, TotalRecords FROM CTE
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
			AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	END
	ELSE
	BEGIN
		INSERT INTO @tbPagedResults
		SELECT @Id, 1, 1
	END

	SELECT C.ApplicationId, 
		G.Id AS CampaignGroupId,
		C.ConfirmationEmail,
		C.Description,
		C.Id,	       
		C.SenderEmail,
		C.SenderName,
		C.ReplyToEmail,
		ISNULL(S.Status, 1) AS Status,
		C.Title,
		C.Type,	
		C.EnableUnsubscribeHeader,
		S.ScheduledTimeZone, 
		A.AuthorId,
		A.AutoUpdateLists,
		A.CampaignId,
		A.CostPerEmail,	      
		A.LastPublishDate,
		S.UseLocalTimeZone AS UseLocalTimeZoneToSend,       
		S.ScheduleId,
		S.SendToNotTriggered,
		S.UniqueRecipientsOnly,
		A.WorkflowId,
		A.WorkflowStatus,
		ISNULL(A.IsEditable, 1) AS IsEditable,
		S.InteractedUsersDateRange,
		S.NonInteractedUsersPercent,
		S.NonInteractedRunId,
		S.SendCount,
		T.TotalRecords,
		A.LastPublishDate,
		C.CreatedDate,	      
		C.ModifiedDate,
		C.CreatedBy,
		C.ModifiedBy,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName
	FROM MKCampaign C
		JOIN @tbPagedResults T ON T.Id = C.Id
		LEFT JOIN MKCampaignAdditionalInfo A ON A.CampaignId = C.Id
		LEFT JOIN MKCampaignSend S ON S.CampaignId = C.Id AND S.SiteId = @SiteId
		LEFT JOIN MKCampaignGroup G ON G.Id = C.CampaignGroupId AND G.ApplicationId = @SiteId
		LEFT JOIN VW_UserFullName CU on CU.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = S.ModifiedBy
	ORDER BY T.RowNumber
	
	SELECT E.[Id],
		[CampaignId],
		[EmailSubject],
		CASE WHEN @IgnoreDetails = 1 THEN '' ELSE [EmailHtml] END AS EmailHtml,
		[CMSPageId],
		[EmailText],
		[Sequence],
		[TimeValue],
		[TimeMeasurement],
		[EmailContentType],
		[EmailContentTypePriority]
	FROM [MKCampaignEmail] E
		JOIN @tbPagedResults T ON T.Id = E.CampaignId
	WHERE @SiteId IS NULL OR E.SiteId = @SiteId

	--Getting the contact count for each campaign
	DECLARE @tbCampaignListCount TABLE 
	(
		CampaignId			uniqueidentifier,
		ContactListId		uniqueidentifier,
		ContactListTitle	nvarchar(max),
		ContactCount		int
	)

	INSERT INTO @tbCampaignListCount
	SELECT T.Id,
		CDL.DistributionListId,
		D.Title,
		ISNULL(DLS.Count, 0)
	FROM MKCampaignDistributionList CDL
		JOIN @tbPagedResults T ON T.Id = CDL.CampaignId
		JOIN MKCampaignSend S ON S.CampaignId = T.Id AND S.SiteId = @SiteId
		JOIN TADistributionLists D on D.Id = CDL.DistributionListId
		JOIN TADistributionListSite DLS ON DLS.DistributionListId = D.Id AND DLS.SiteId = @SiteId
		JOIN MKCampaign C ON C.Id = S.CampaignId
	WHERE CDL.CampaignSendId = S.Id
		
	SELECT CampaignId, SUM(ContactCount) ContactCount FROM @tbCampaignListCount
	GROUP BY CampaignId
	
	SELECT * FROM @tbCampaignListCount

	SELECT CS.CampaignId AS CampaignId,
		S.[Id],
		S.[Title],
		S.[Description],
		CAST(CONVERT(varchar(12), S.StartTime, 101) AS dateTime) AS StartDate,
		CAST(CONVERT(varchar(12), S.EndDate, 101)  AS dateTime) AS EndDate,
		S.StartTime,
		S.EndTime, 
		S.[MaxOccurrence],
		S.[Type], 
		S.[NextRunTime], 
		S.[LastRunTime],
		S.[ScheduleTypeId],
		S.[Status],
		S.[CreatedBy],
		S.CreatedDate,
		S.[ModifiedBy],
		S.ModifiedDate
	FROM TASchedule S
		JOIN MKCampaignSend CS ON S.Id = CS.ScheduleId
		JOIN @tbPagedResults T ON T.Id = CS.CampaignId AND CS.SiteId = @SiteId
END
GO
PRINT 'Modify stored procedure CampaignDto_Save'
GO
IF(OBJECT_ID('CampaignDto_Save') IS NOT NULL)
	DROP PROCEDURE CampaignDto_Save
GO
CREATE PROCEDURE [dbo].[CampaignDto_Save]
(
	@Id							uniqueidentifier = NULL OUT,
	@Title						nvarchar(MAX) = NULL,
	@Description				nvarchar(MAX) = NULL,
	@Type						int ,
	@CampaignGroupId			uniqueidentifier = NULL,
	@Status						int = NULL,
	@SenderName					nvarchar(MAX) = NULL,
	@ReplyToEmail				nvarchar(MAX) = NULL,
	@SenderEmail				nvarchar(MAX) = NULL,
	@ConfirmationEmail			nvarchar(MAX) = NULL,
	@EnableUnsubscribeHeader	bit = NULL,
	@SiteId						uniqueidentifier ,
	@Emails						xml = NULL,
	@ScheduleId					uniqueidentifier = NULL,
	@WorkflowId					uniqueidentifier = NULL,
	@WorkflowStatus				int = NULL,
	@UniqueRecipientsOnly		bit = NULL,
	@InteractedUsersDateRange	int = NULL,
	@NonInteractedUsersPercent	int = NULL,	
	@NonInteractedRunId			uniqueidentifier = NULL,
	@UseLocalTimeZoneToSend		bit = 0,
	@IsEditable					bit = 0,
	@ScheduledTimeZone			nvarchar(max) = NULL,
	@ModifiedBy					uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier, @CampaignSiteId uniqueidentifier
	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF EXISTS(SELECT * FROM MKCampaign WHERE Id != @Id AND Title = @Title AND Type = @Type AND ApplicationId = @SiteId)
	BEGIN 
		IF (@Type = 1)
			RAISERROR ('The campaign name already exists.  Please enter a unique name.', 16, 1, '')
		IF (@Type = 2)
			RAISERROR ('The response name already exists.  Please enter a unique name.', 16, 1, '')
	END

	IF @Status = 0 SET @Status = NULL
	IF @Type = 0 SET @Status = NULL

	IF @Status = 4 OR @Status = 8
	BEGIN
		IF EXISTS(SELECT 1 FROM MKCampaignGroup WHERE Name like 'Deleted Email Campaigns' AND ApplicationId = @SiteId)
			SELECT @CampaignGroupId = Id FROM MKCampaignGroup WHERE Name like 'Deleted Email Campaigns' AND ApplicationId = @SiteId
	END

	SELECT TOP 1 @CampaignSiteId = ApplicationId FROM MKCampaign WHERE Id = @Id

	IF (@Id IS NULL OR NOT EXISTS (SELECT 1 FROM MKCampaign WHERE Id = @Id))
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO MKCampaign
		(
			Id,
			Title,
			Description,
			Type,
			CampaignGroupId,
			SenderName,
			SenderEmail,
			ReplyToEmail,
			ConfirmationEmail,
			EnableUnsubscribeHeader,
			CreatedDate,
			CreatedBy,
			ApplicationId
		)
		VALUES
		(
			@Id,
			@Title,
			@Description,
			@Type,
			@CampaignGroupId,
			@SenderName,
			@SenderEmail,
			@ReplyToEmail,
			@ConfirmationEmail,
			@EnableUnsubscribeHeader,
			@UtcNow,
			@ModifiedBy,
			@SiteId
		)
		
		INSERT INTO MKCampaignAdditionalInfo
		(
			Id,
			CampaignId,
			WorkflowStatus,
			WorkflowId,
			IsEditable
		)
		VALUES
		(
			NEWID(),
			@Id,
			@WorkflowStatus,
			@WorkflowId,
			@IsEditable
		)	
				
	END
	ELSE
	BEGIN
		IF @CampaignSiteId = @SiteId
		BEGIN
			UPDATE MKCampaign
			SET [Title] = ISNULL(@Title, [Title]),
				[Description] = ISNULL(@Description, [Description]),
				[CampaignGroupId] = ISNULL(@CampaignGroupId, [CampaignGroupId]),
				[SenderName] = ISNULL(@SenderName, [SenderName]),
				[SenderEmail] = ISNULL(@SenderEmail, [SenderEmail]),
				[ReplyToEmail] = ISNULL(@ReplyToEmail, [ReplyToEmail]),
				[ConfirmationEmail] = ISNULL(@ConfirmationEmail, [ConfirmationEmail]),
				[EnableUnsubscribeHeader] = ISNULL(@EnableUnsubscribeHeader, [EnableUnsubscribeHeader]),
				[ModifiedBy] = @ModifiedBy,
				[ModifiedDate] = @UtcNow
			WHERE Id = @Id	
	  
			UPDATE MKCampaignAdditionalInfo
			SET WorkflowStatus = ISNULL(@WorkflowStatus, WorkflowStatus),
				WorkflowId = ISNULL(@WorkflowId, WorkflowId),
				IsEditable = ISNULL(@IsEditable, IsEditable)
			WHERE CampaignId = @Id
		END
	END

	DECLARE @CampaignSendId uniqueidentifier
	SELECT TOP 1 @CampaignSendId = Id FROM dbo.MKCampaignSend WHERE CampaignId = @Id AND SiteId = @SiteId
	IF @CampaignSendId IS NULL
	BEGIN
		SET @CampaignSendId = NEWID()
		INSERT INTO MKCampaignSend
		(
			Id,
			CampaignId,
			SiteId,
			Status,
			ScheduleId,
			UniqueRecipientsOnly,
			NonInteractedRunId,
			InteractedUsersDateRange,
			NonInteractedUsersPercent,
			UseLocalTimeZone,
			ScheduledTimeZone,
			CreatedDate,
			CreatedBy
		)
		VALUES
		(
			@CampaignSendId,
			@Id,
			@SiteId,
			@Status,
			@ScheduleId,
			@UniqueRecipientsOnly,
			@NonInteractedRunId,
			@InteractedUsersDateRange,
			@NonInteractedUsersPercent,
			@UseLocalTimeZoneToSend,
			@ScheduledTimeZone,
			@UtcNow,
			@ModifiedBy
		)	
	END
	ELSE
	BEGIN
		UPDATE [dbo].[MKCampaignSend]
		SET [Status] = ISNULL(@Status, [Status]),
			[ScheduleId] = ISNULL(@ScheduleId, [ScheduleId]),
			[UniqueRecipientsOnly] = ISNULL(@UniqueRecipientsOnly, [UniqueRecipientsOnly]),
			[NonInteractedRunId] = @NonInteractedRunId,
			[InteractedUsersDateRange] = ISNULL(@InteractedUsersDateRange, [InteractedUsersDateRange]),
			[NonInteractedUsersPercent] = ISNULL(@NonInteractedUsersPercent, [NonInteractedUsersPercent]),
			[UseLocalTimeZone] = ISNULL(@UseLocalTimeZoneToSend, [UseLocalTimeZone]),
			[ScheduledTimeZone] = ISNULL(@ScheduledTimeZone, [ScheduledTimeZone]),
			[ModifiedBy] = @ModifiedBy,
			[ModifiedDate] = @UtcNow
		WHERE Id = @CampaignSendId		

		IF (@Status = 4 OR @Status = 8) AND @SiteId = @CampaignSiteId
		BEGIN
			UPDATE [dbo].[MKCampaignSend]
			SET [Status] = @Status,
				[ModifiedBy] = @ModifiedBy,
				[ModifiedDate] = @UtcNow
			WHERE CampaignId = @Id
		END
	END

	IF NOT EXISTS (SELECT 1 FROM MKCampaignSendTarget WHERE CampaignSendId = @CampaignSendId AND TargetId = @SiteId)
	BEGIN
		INSERT INTO MKCampaignSendTarget	
		(
			Id,
			CampaignSendId,
			TargetId,
			TargetTypeId
		)
		VALUES
		(
			NEWID(),
			@CampaignSendId,
			@SiteId,
			2
		)
	END

	IF @Emails IS NOT NULL
	BEGIN
		DECLARE @CampaignEmailId uniqueidentifier
		SELECT TOP 1 @CampaignEmailId = Id FROM dbo.MKCampaignEmail WHERE CampaignSendId = @CampaignSendId

		IF @CampaignEmailId IS NULL
		BEGIN
			INSERT INTO MKCampaignEmail
			(
				Id,
				CampaignId,
				EmailSubject,
				EmailHtml,
				CMSPageId,
				EmailText,
				Sequence,
				TimeValue,
				TimeMeasurement,
				EmailContentType,
				EmailContentTypePriority,
				SiteId,
				CampaignSendId
			)
			SELECT
				NEWID(),
				@Id,
				Emails.email.value('@EmailSubject', 'nvarchar(max)'),
				Emails.email.value('@EmailHtml', 'nvarchar(max)'),
				Emails.email.value('@CMSPageId', 'uniqueidentifier'),
				Emails.email.value('@EmailText', 'nvarchar(max)'),
				ISNULL(Emails.email.value('@Sequence', 'int'), 0),
				Emails.email.value('@TimeValue', 'int'),
				Emails.email.value('@TimeMeasurement', 'nvarchar(max)'),
				Emails.email.value('@EmailContentType', 'int'),
				Emails.email.value('@EmailContentTypePriority', 'int'),
				@SiteId,
				@CampaignSendId
			FROM @Emails.nodes('Emails/Email') AS Emails(email)
		END
		ELSE
		BEGIN
			;WITH EmailXml AS
			(
				SELECT 
					Emails.email.value('@EmailSubject', 'nvarchar(max)') AS EmailSubject,
					Emails.email.value('@EmailHtml', 'nvarchar(max)') AS EmailHtml,
					Emails.email.value('@CMSPageId', 'uniqueidentifier') AS CMSPageId,
					Emails.email.value('@EmailText', 'nvarchar(max)') AS EmailText,
					Emails.email.value('@Sequence', 'int') AS Sequence,
					Emails.email.value('@TimeValue', 'int') AS TimeValue,
					Emails.email.value('@TimeMeasurement', 'nvarchar(max)') AS TimeMeasurement,
					Emails.email.value('@EmailContentType', 'int') AS EmailContentType,
					Emails.email.value('@EmailContentTypePriority', 'int') AS EmailContentTypePriority
				FROM @Emails.nodes('Emails/Email') AS Emails(email)
			)

			UPDATE E
			SET EmailSubject = ISNULL(X.EmailSubject, E.EmailSubject),
				EmailHtml = ISNULL(X.EmailHtml, E.EmailHtml),
				CMSPageId = ISNULL(X.CMSPageId, E.CMSPageId),
				EmailText = ISNULL(X.EmailText, E.EmailText),
				Sequence = ISNULL(X.Sequence, E.Sequence),
				TimeValue = ISNULL(X.TimeValue, E.TimeValue),
				TimeMeasurement = ISNULL(X.TimeMeasurement, E.TimeMeasurement),
				EmailContentType = ISNULL(X.EmailContentType, E.EmailContentType),
				EmailContentTypePriority = ISNULL(X.EmailContentTypePriority, E.EmailContentTypePriority)
			FROM MKCampaignEmail E, Emailxml X
			WHERE E.Id = @CampaignEmailId
		END	
	END
END
GO
IF(OBJECT_ID('ProductImage_GetImageForProducts') IS NOT NULL)
	DROP PROCEDURE ProductImage_GetImageForProducts
GO
CREATE PROCEDURE [dbo].[ProductImage_GetImageForProducts]
    (
      @ProductIds XML = NULL ,
      @IncludeSKUs BIT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    ) 
AS --********************************************************************************
-- Variable declaration
--********************************************************************************
    BEGIN
--********************************************************************************
-- code
--********************************************************************************
                DECLARE @ProductIdTable TABLE ( Id UNIQUEIDENTIFIER )
                INSERT  INTO @ProductIdTable
                        ( Id
                        )
                        SELECT  tab.col.value('text()[1]', 'uniqueidentifier')
                        FROM    @ProductIds.nodes('/ObjectIdCollection/ObjectId') tab ( col )
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        CASE WHEN OIS.[Sequence] is NULL THEN OI.[Sequence] ELSE OIS.[Sequence] END as Sequence,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
                        INNER JOIN CLImage C ON OI.ImageId = C.ParentImage AND OI.SiteId=C.SiteId
                        LEFT JOIN CLObjectImageSequence OIS ON OI.Id = OIS.CLObjectImageId AND OIS.SiteId=OI.SiteId
				WHERE (@ApplicationId is null or C.SiteId = @ApplicationId)
						AND C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                
				union all
				SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        CASE WHEN OIS.[Sequence] is NULL THEN OI.[Sequence] ELSE OIS.[Sequence] END as Sequence,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
                        INNER JOIN CLImage C ON C.ParentImage is null AND OI.ImageId = C.Id AND OI.SiteId=C.SiteId
                        LEFT JOIN CLObjectImageSequence OIS ON OI.Id = OIS.CLObjectImageId AND OIS.SiteId=OI.SiteId
				WHERE (@ApplicationId is null or C.SiteId = @ApplicationId)
						AND C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                
    END
    GO
IF(OBJECT_ID('ProductImage_GetImageForProduct') IS NOT NULL)
 DROP PROCEDURE ProductImage_GetImageForProduct
GO
CREATE PROCEDURE [dbo].[ProductImage_GetImageForProduct]
    (
      @ProductId UNIQUEIDENTIFIER = NULL ,
      @ProductIds XML = NULL ,
      @IncludeSKUs BIT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    ) WITH RECOMPILE
AS --********************************************************************************
-- Variable declaration
--********************************************************************************
    BEGIN
--********************************************************************************
-- code
--********************************************************************************
        IF @ProductIds IS NULL 
            BEGIN
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        CASE WHEN OIS.[Sequence] is NULL THEN OI.[Sequence] ELSE OIS.[Sequence] END as Sequence,
                        C.[ParentImage] ,
                        C.ImageType ,
                        @ProductId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(@ProductId,
                                                              C.Id)
                FROM    CLObjectImage OI 
                        INNER JOIN CLImage C ON OI.ImageId = C.ParentImage --ISNULL(C.ParentImage, C.Id)
                        LEFT JOIN CLObjectImageSequence OIS ON OI.Id = OIS.CLObjectImageId AND OIS.SiteId=OI.SiteId
                WHERE   C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                        AND OI.ObjectId = @ProductId
                
				union all
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        CASE WHEN OIS.[Sequence] is NULL THEN OI.[Sequence] ELSE OIS.[Sequence] END as Sequence,
                        C.[ParentImage] ,
                        C.ImageType ,
                        @ProductId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(@ProductId,
                                                              C.Id)
                FROM    CLObjectImage OI 
                        INNER JOIN CLImage C ON C.ParentImage is null AND OI.ImageId = C.Id --ISNULL(C.ParentImage, C.Id)
                        LEFT JOIN CLObjectImageSequence OIS ON OI.Id = OIS.CLObjectImageId AND OIS.SiteId=OI.SiteId
                WHERE   C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                        AND OI.ObjectId = @ProductId
            END
        ELSE 
            BEGIN
                DECLARE @ProductIdTable TABLE ( Id UNIQUEIDENTIFIER )
                INSERT  INTO @ProductIdTable
                        ( Id
                        )
                        SELECT  tab.col.value('text()[1]', 'uniqueidentifier')
                        FROM    @ProductIds.nodes('/ObjectIdCollection/ObjectId') tab ( col )
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        CASE WHEN OIS.[Sequence] is NULL THEN OI.[Sequence] ELSE OIS.[Sequence] END as Sequence,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
    --INNER JOIN CLImage I on I.Id=OI.ImageId
                        INNER JOIN CLImage C ON OI.ImageId = ISNULL(C.ParentImage,C.Id)
                        LEFT JOIN CLObjectImageSequence OIS ON OI.Id = OIS.CLObjectImageId AND OIS.SiteId=OI.SiteId 
	--Inner join @ProductIds.nodes('/ObjectIdCollection/ObjectId')tab(col) ON  OI.ObjectId =tab.col.value('text()[1]','uniqueidentifier')
                WHERE   --I.SiteId=Isnull(@ApplicationId,I.SiteId) 
	--(@ApplicationId is null or C.SiteId = @ApplicationId)
	--AND 
                        C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                ORDER BY OI.Sequence ,
                        C.Title
            END
    END
GO
PRINT 'Add column IncludeUnsubscribedUsers to MKCampaign'
GO
IF(COL_LENGTH('MKCampaign', 'IncludeUnsubscribedUsers') IS NULL)
BEGIN
	ALTER TABLE MKCampaign ADD [IncludeUnsubscribedUsers] bit NULL	
END
GO
PRINT 'Modify stored procedure CampaignDto_Get'
GO
IF(OBJECT_ID('CampaignDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignDto_Get
GO
CREATE PROCEDURE [dbo].[CampaignDto_Get]  
(  
	@Id						uniqueidentifier = NULL,  
	@SiteId					uniqueidentifier,
	@Status					int = NULL,
	@Type					int = NULL,
	@CampaignGroupId		uniqueidentifier = NULL,
	@ParentId				uniqueidentifier = NULL,
	@CampaignRunId			uniqueidentifier = NULL,
	@PageSize				int = NULL,   
	@PageNumber				int = NULL,
	@MaxRecords				int = NULL,
	@Keyword				nvarchar(MAX) = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@IgnoreDetails			bit = NULL,
	@Filter					int = NULL
)  
AS  
BEGIN
	DECLARE @EmptyGuid uniqueidentifier, @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @Id = @EmptyGuid SET @Id = NULL
	IF @CampaignGroupId = @EmptyGuid SET @CampaignGroupId = NULL
	IF @CampaignGroupId IS NULL AND @ParentId != @EmptyGuid SET @CampaignGroupId = @ParentId

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'
	IF @Filter = 0 SET @Filter = NULL
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbQueryIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)

	IF @Id IS NULL AND @CampaignRunId IS NOT NULL
		SELECT TOP 1 @Id = CampaignId FROM MKCampaignRunHistory WHERE Id = @CampaignRunId

	IF @Id IS NULL	
	BEGIN
		INSERT INTO @tbIds
		SELECT C.Id AS Id, ROW_NUMBER() OVER (ORDER BY 
			CASE WHEN @SortClause = 'Title ASC' THEN C.Title END ASC,
			CASE WHEN @SortClause = 'Title DESC' THEN C.Title END DESC,
			CASE WHEN @SortClause = 'CreatedDate ASC' THEN C.CreatedDate END ASC,
			CASE WHEN @SortClause = 'CreatedDate DESC' THEN C.CreatedDate END DESC,
			CASE WHEN @SortClause = 'ModifiedDate ASC' THEN S.ModifiedDate END ASC,
			CASE WHEN @SortClause = 'ModifiedDate DESC' THEN S.ModifiedDate END DESC,
			CASE WHEN @SortClause IS NULL THEN C.CreatedDate END DESC		
		) AS RowNumber
		FROM MKCampaign AS C
			JOIN vwCampaignSend S ON C.Id = S.CampaignId
			LEFT JOIN MKCampaignGroup G ON G.Id = C.CampaignGroupId
		WHERE (@Id IS NULL OR C.Id = @Id) AND
			(@Type IS NULL OR C.Type = @Type) AND 
			((@CampaignGroupId IS NULL AND S.status != 4) OR G.Id = @CampaignGroupId) AND
			(@Status IS NULL OR S.Status = @Status) AND
			(@Filter IS NULL OR 
				(
					(@Filter = 1 AND (S.Status = 2 OR S.Status = 5)) OR
					(@Filter = 2 AND (S.Status = 1 OR S.Status = 6)) OR
					(@Filter = 3 AND S.Status = 3) OR
					(@Filter = 5 AND (S.Status = 1 OR S.Status = 3 OR S.Status = 6))
				)
			) AND
			(@SiteId IS NULL OR S.SiteId = @SiteId) AND
			(@Keyword IS NULL OR C.Title like @Keyword OR C.Description like @Keyword)
		OPTION (RECOMPILE)

		;WITH CTE AS(
			SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				C.Id AS Id
			FROM MKCampaign C
				JOIN @tbIds T ON T.Id = C.Id
		)
	
		INSERT INTO @tbPagedResults
		SELECT Id, RowNumber, TotalRecords FROM CTE
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
			AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	END
	ELSE
	BEGIN
		INSERT INTO @tbPagedResults
		SELECT @Id, 1, 1
	END

	SELECT C.ApplicationId, 
		G.Id AS CampaignGroupId,
		C.ConfirmationEmail,
		C.Description,
		C.Id,	       
		C.SenderEmail,
		C.SenderName,
		C.ReplyToEmail,
		ISNULL(S.Status, 1) AS Status,
		C.Title,
		C.Type,	
		C.EnableUnsubscribeHeader,
		C.IncludeUnsubscribedusers,
		S.ScheduledTimeZone, 
		A.AuthorId,
		A.AutoUpdateLists,
		A.CampaignId,
		A.CostPerEmail,	      
		A.LastPublishDate,
		S.UseLocalTimeZone AS UseLocalTimeZoneToSend,       
		S.ScheduleId,
		S.SendToNotTriggered,
		S.UniqueRecipientsOnly,
		A.WorkflowId,
		A.WorkflowStatus,
		ISNULL(A.IsEditable, 1) AS IsEditable,
		S.InteractedUsersDateRange,
		S.NonInteractedUsersPercent,
		S.NonInteractedRunId,
		S.SendCount,
		T.TotalRecords,
		A.LastPublishDate,
		C.CreatedDate,	      
		C.ModifiedDate,
		C.CreatedBy,
		C.ModifiedBy,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName
	FROM MKCampaign C
		JOIN @tbPagedResults T ON T.Id = C.Id
		LEFT JOIN MKCampaignAdditionalInfo A ON A.CampaignId = C.Id
		LEFT JOIN MKCampaignSend S ON S.CampaignId = C.Id AND S.SiteId = @SiteId
		LEFT JOIN MKCampaignGroup G ON G.Id = C.CampaignGroupId AND G.ApplicationId = @SiteId
		LEFT JOIN VW_UserFullName CU on CU.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = S.ModifiedBy
	ORDER BY T.RowNumber
	
	SELECT E.[Id],
		[CampaignId],
		[EmailSubject],
		CASE WHEN @IgnoreDetails = 1 THEN '' ELSE [EmailHtml] END AS EmailHtml,
		[CMSPageId],
		[EmailText],
		[Sequence],
		[TimeValue],
		[TimeMeasurement],
		[EmailContentType],
		[EmailContentTypePriority]
	FROM [MKCampaignEmail] E
		JOIN @tbPagedResults T ON T.Id = E.CampaignId
	WHERE @SiteId IS NULL OR E.SiteId = @SiteId

	--Getting the contact count for each campaign
	DECLARE @tbCampaignListCount TABLE 
	(
		CampaignId			uniqueidentifier,
		ContactListId		uniqueidentifier,
		ContactListTitle	nvarchar(max),
		ContactCount		int
	)

	INSERT INTO @tbCampaignListCount
	SELECT T.Id,
		CDL.DistributionListId,
		D.Title,
		ISNULL(DLS.Count, 0)
	FROM MKCampaignDistributionList CDL
		JOIN @tbPagedResults T ON T.Id = CDL.CampaignId
		JOIN MKCampaignSend S ON S.CampaignId = T.Id AND S.SiteId = @SiteId
		JOIN TADistributionLists D on D.Id = CDL.DistributionListId
		JOIN TADistributionListSite DLS ON DLS.DistributionListId = D.Id AND DLS.SiteId = @SiteId
		JOIN MKCampaign C ON C.Id = S.CampaignId
	WHERE CDL.CampaignSendId = S.Id
		
	SELECT CampaignId, SUM(ContactCount) ContactCount FROM @tbCampaignListCount
	GROUP BY CampaignId
	
	SELECT * FROM @tbCampaignListCount

	SELECT CS.CampaignId AS CampaignId,
		S.[Id],
		S.[Title],
		S.[Description],
		CAST(CONVERT(varchar(12), S.StartTime, 101) AS dateTime) AS StartDate,
		CAST(CONVERT(varchar(12), S.EndDate, 101)  AS dateTime) AS EndDate,
		S.StartTime,
		S.EndTime, 
		S.[MaxOccurrence],
		S.[Type], 
		S.[NextRunTime], 
		S.[LastRunTime],
		S.[ScheduleTypeId],
		S.[Status],
		S.[CreatedBy],
		S.CreatedDate,
		S.[ModifiedBy],
		S.ModifiedDate
	FROM TASchedule S
		JOIN MKCampaignSend CS ON S.Id = CS.ScheduleId
		JOIN @tbPagedResults T ON T.Id = CS.CampaignId AND CS.SiteId = @SiteId
END
GO
PRINT 'Modify stored procedure CampaignDto_Save'
GO
IF(OBJECT_ID('CampaignDto_Save') IS NOT NULL)
	DROP PROCEDURE CampaignDto_Save
GO
CREATE PROCEDURE [dbo].[CampaignDto_Save]
(
	@Id							uniqueidentifier = NULL OUT,
	@Title						nvarchar(MAX) = NULL,
	@Description				nvarchar(MAX) = NULL,
	@Type						int ,
	@CampaignGroupId			uniqueidentifier = NULL,
	@Status						int = NULL,
	@SenderName					nvarchar(MAX) = NULL,
	@ReplyToEmail				nvarchar(MAX) = NULL,
	@SenderEmail				nvarchar(MAX) = NULL,
	@ConfirmationEmail			nvarchar(MAX) = NULL,
	@EnableUnsubscribeHeader	bit = NULL,
	@IncludeUnsubscribedUsers	bit = NULL,
	@SiteId						uniqueidentifier ,
	@Emails						xml = NULL,
	@ScheduleId					uniqueidentifier = NULL,
	@WorkflowId					uniqueidentifier = NULL,
	@WorkflowStatus				int = NULL,
	@UniqueRecipientsOnly		bit = NULL,
	@InteractedUsersDateRange	int = NULL,
	@NonInteractedUsersPercent	int = NULL,	
	@NonInteractedRunId			uniqueidentifier = NULL,
	@UseLocalTimeZoneToSend		bit = 0,
	@IsEditable					bit = 0,
	@ScheduledTimeZone			nvarchar(max) = NULL,
	@ModifiedBy					uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier, @CampaignSiteId uniqueidentifier
	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF EXISTS(SELECT * FROM MKCampaign WHERE Id != @Id AND Title = @Title AND Type = @Type AND ApplicationId = @SiteId)
	BEGIN 
		IF (@Type = 1)
			RAISERROR ('The campaign name already exists.  Please enter a unique name.', 16, 1, '')
		IF (@Type = 2)
			RAISERROR ('The response name already exists.  Please enter a unique name.', 16, 1, '')
	END

	IF @Status = 0 SET @Status = NULL
	IF @Type = 0 SET @Status = NULL

	IF @Status = 4 OR @Status = 8
	BEGIN
		IF EXISTS(SELECT 1 FROM MKCampaignGroup WHERE Name like 'Deleted Email Campaigns' AND ApplicationId = @SiteId)
			SELECT @CampaignGroupId = Id FROM MKCampaignGroup WHERE Name like 'Deleted Email Campaigns' AND ApplicationId = @SiteId
	END

	SELECT TOP 1 @CampaignSiteId = ApplicationId FROM MKCampaign WHERE Id = @Id

	IF (@Id IS NULL OR NOT EXISTS (SELECT 1 FROM MKCampaign WHERE Id = @Id))
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO MKCampaign
		(
			Id,
			Title,
			Description,
			Type,
			CampaignGroupId,
			SenderName,
			SenderEmail,
			ReplyToEmail,
			ConfirmationEmail,
			EnableUnsubscribeHeader,
			IncludeUnsubscribedUsers,
			CreatedDate,
			CreatedBy,
			ApplicationId
		)
		VALUES
		(
			@Id,
			@Title,
			@Description,
			@Type,
			@CampaignGroupId,
			@SenderName,
			@SenderEmail,
			@ReplyToEmail,
			@ConfirmationEmail,
			@EnableUnsubscribeHeader,
			@IncludeUnsubscribedUsers,
			@UtcNow,
			@ModifiedBy,
			@SiteId
		)
		
		INSERT INTO MKCampaignAdditionalInfo
		(
			Id,
			CampaignId,
			WorkflowStatus,
			WorkflowId,
			IsEditable
		)
		VALUES
		(
			NEWID(),
			@Id,
			@WorkflowStatus,
			@WorkflowId,
			@IsEditable
		)	
				
	END
	ELSE
	BEGIN
		IF @CampaignSiteId = @SiteId
		BEGIN
			UPDATE MKCampaign
			SET [Title] = ISNULL(@Title, [Title]),
				[Description] = ISNULL(@Description, [Description]),
				[CampaignGroupId] = ISNULL(@CampaignGroupId, [CampaignGroupId]),
				[SenderName] = ISNULL(@SenderName, [SenderName]),
				[SenderEmail] = ISNULL(@SenderEmail, [SenderEmail]),
				[ReplyToEmail] = ISNULL(@ReplyToEmail, [ReplyToEmail]),
				[ConfirmationEmail] = ISNULL(@ConfirmationEmail, [ConfirmationEmail]),
				[EnableUnsubscribeHeader] = ISNULL(@EnableUnsubscribeHeader, [EnableUnsubscribeHeader]),
				[IncludeUnsubscribedUsers] = ISNULL(@IncludeUnsubscribedUsers, [IncludeUnsubscribedUsers]),
				[ModifiedBy] = @ModifiedBy,
				[ModifiedDate] = @UtcNow
			WHERE Id = @Id	
	  
			UPDATE MKCampaignAdditionalInfo
			SET WorkflowStatus = ISNULL(@WorkflowStatus, WorkflowStatus),
				WorkflowId = ISNULL(@WorkflowId, WorkflowId),
				IsEditable = ISNULL(@IsEditable, IsEditable)
			WHERE CampaignId = @Id
		END
	END

	DECLARE @CampaignSendId uniqueidentifier
	SELECT TOP 1 @CampaignSendId = Id FROM dbo.MKCampaignSend WHERE CampaignId = @Id AND SiteId = @SiteId
	IF @CampaignSendId IS NULL
	BEGIN
		SET @CampaignSendId = NEWID()
		INSERT INTO MKCampaignSend
		(
			Id,
			CampaignId,
			SiteId,
			Status,
			ScheduleId,
			UniqueRecipientsOnly,
			NonInteractedRunId,
			InteractedUsersDateRange,
			NonInteractedUsersPercent,
			UseLocalTimeZone,
			ScheduledTimeZone,
			CreatedDate,
			CreatedBy
		)
		VALUES
		(
			@CampaignSendId,
			@Id,
			@SiteId,
			@Status,
			@ScheduleId,
			@UniqueRecipientsOnly,
			@NonInteractedRunId,
			@InteractedUsersDateRange,
			@NonInteractedUsersPercent,
			@UseLocalTimeZoneToSend,
			@ScheduledTimeZone,
			@UtcNow,
			@ModifiedBy
		)	
	END
	ELSE
	BEGIN
		UPDATE [dbo].[MKCampaignSend]
		SET [Status] = ISNULL(@Status, [Status]),
			[ScheduleId] = ISNULL(@ScheduleId, [ScheduleId]),
			[UniqueRecipientsOnly] = ISNULL(@UniqueRecipientsOnly, [UniqueRecipientsOnly]),
			[NonInteractedRunId] = @NonInteractedRunId,
			[InteractedUsersDateRange] = ISNULL(@InteractedUsersDateRange, [InteractedUsersDateRange]),
			[NonInteractedUsersPercent] = ISNULL(@NonInteractedUsersPercent, [NonInteractedUsersPercent]),
			[UseLocalTimeZone] = ISNULL(@UseLocalTimeZoneToSend, [UseLocalTimeZone]),
			[ScheduledTimeZone] = ISNULL(@ScheduledTimeZone, [ScheduledTimeZone]),
			[ModifiedBy] = @ModifiedBy,
			[ModifiedDate] = @UtcNow
		WHERE Id = @CampaignSendId		

		IF (@Status = 4 OR @Status = 8) AND @SiteId = @CampaignSiteId
		BEGIN
			UPDATE [dbo].[MKCampaignSend]
			SET [Status] = @Status,
				[ModifiedBy] = @ModifiedBy,
				[ModifiedDate] = @UtcNow
			WHERE CampaignId = @Id
		END
	END

	IF NOT EXISTS (SELECT 1 FROM MKCampaignSendTarget WHERE CampaignSendId = @CampaignSendId AND TargetId = @SiteId)
	BEGIN
		INSERT INTO MKCampaignSendTarget	
		(
			Id,
			CampaignSendId,
			TargetId,
			TargetTypeId
		)
		VALUES
		(
			NEWID(),
			@CampaignSendId,
			@SiteId,
			2
		)
	END

	IF @Emails IS NOT NULL
	BEGIN
		DECLARE @CampaignEmailId uniqueidentifier
		SELECT TOP 1 @CampaignEmailId = Id FROM dbo.MKCampaignEmail WHERE CampaignSendId = @CampaignSendId

		IF @CampaignEmailId IS NULL
		BEGIN
			INSERT INTO MKCampaignEmail
			(
				Id,
				CampaignId,
				EmailSubject,
				EmailHtml,
				CMSPageId,
				EmailText,
				Sequence,
				TimeValue,
				TimeMeasurement,
				EmailContentType,
				EmailContentTypePriority,
				SiteId,
				CampaignSendId
			)
			SELECT
				NEWID(),
				@Id,
				Emails.email.value('@EmailSubject', 'nvarchar(max)'),
				Emails.email.value('@EmailHtml', 'nvarchar(max)'),
				Emails.email.value('@CMSPageId', 'uniqueidentifier'),
				Emails.email.value('@EmailText', 'nvarchar(max)'),
				ISNULL(Emails.email.value('@Sequence', 'int'), 0),
				Emails.email.value('@TimeValue', 'int'),
				Emails.email.value('@TimeMeasurement', 'nvarchar(max)'),
				Emails.email.value('@EmailContentType', 'int'),
				Emails.email.value('@EmailContentTypePriority', 'int'),
				@SiteId,
				@CampaignSendId
			FROM @Emails.nodes('Emails/Email') AS Emails(email)
		END
		ELSE
		BEGIN
			;WITH EmailXml AS
			(
				SELECT 
					Emails.email.value('@EmailSubject', 'nvarchar(max)') AS EmailSubject,
					Emails.email.value('@EmailHtml', 'nvarchar(max)') AS EmailHtml,
					Emails.email.value('@CMSPageId', 'uniqueidentifier') AS CMSPageId,
					Emails.email.value('@EmailText', 'nvarchar(max)') AS EmailText,
					Emails.email.value('@Sequence', 'int') AS Sequence,
					Emails.email.value('@TimeValue', 'int') AS TimeValue,
					Emails.email.value('@TimeMeasurement', 'nvarchar(max)') AS TimeMeasurement,
					Emails.email.value('@EmailContentType', 'int') AS EmailContentType,
					Emails.email.value('@EmailContentTypePriority', 'int') AS EmailContentTypePriority
				FROM @Emails.nodes('Emails/Email') AS Emails(email)
			)

			UPDATE E
			SET EmailSubject = ISNULL(X.EmailSubject, E.EmailSubject),
				EmailHtml = ISNULL(X.EmailHtml, E.EmailHtml),
				CMSPageId = ISNULL(X.CMSPageId, E.CMSPageId),
				EmailText = ISNULL(X.EmailText, E.EmailText),
				Sequence = ISNULL(X.Sequence, E.Sequence),
				TimeValue = ISNULL(X.TimeValue, E.TimeValue),
				TimeMeasurement = ISNULL(X.TimeMeasurement, E.TimeMeasurement),
				EmailContentType = ISNULL(X.EmailContentType, E.EmailContentType),
				EmailContentTypePriority = ISNULL(X.EmailContentTypePriority, E.EmailContentTypePriority)
			FROM MKCampaignEmail E, Emailxml X
			WHERE E.Id = @CampaignEmailId
		END	
	END
END
GO
PRINT 'Modify stored procedure CampaignRunHistoryDto_Fill'
GO
IF(OBJECT_ID('CampaignRunHistoryDto_Fill') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_Fill
GO
CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_Fill]
(
	@CampaignRunId			uniqueidentifier,
	@EmailPerCampaignLimit	int,
	@DebugInfo				xml = NULL OUTPUT,
	@Populated				bit = NULL OUTPUT,
	@TotalContacts			int = NULL	OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	SET @Populated = 0

	DECLARE @CampaignType int
	SELECT TOP 1 @CampaignType = C.Type FROM MKCampaignRunHistory R
		JOIN MKCampaign C ON R.CampaignId = C.Id
	WHERE R.Id = @CampaignRunId

	IF @CampaignType = 2
	BEGIN
		EXEC [CampaignRunHistoryDto_FillResponse]
			@CampaignRunId = @CampaignRunId, @DebugInfo = @DebugInfo OUTPUT

		RETURN
	END

	DECLARE @tbDebugInfo TABLE(Message nvarchar(2000))

	DECLARE @CanPopulateWorkTable int
	SET @CanPopulateWorkTable = [dbo].[CampaignRun_VerifyFillable](@CampaignRunId)
	IF (@CanPopulateWorkTable = 0)
	BEGIN
		SELECT @TotalContacts = COUNT(1) FROM MKCampaignRunWorkTable WHERE CampaignRunId = @CampaignRunId

		INSERT INTO @tbDebugInfo SELECT 'No valid campaign to fill contacts.'
	END
	ELSE
	BEGIN
		INSERT INTO @tbDebugInfo SELECT 'Start populating worktable.'

		UPDATE MKCampaignRunHistory SET ProcessingStatus = 1 WHERE Id = @CampaignRunId
		
		DECLARE @CampaignId uniqueidentifier, @CampaignSendId uniqueidentifier, @SiteId uniqueidentifier
		SELECT TOP 1 @CampaignId = CampaignId, @CampaignSendId = CampaignSendId, @SiteId = SiteId 
		FROM MKCampaignRunHistory WHERE Id = @CampaignRunId

		DECLARE @UniqueRecipientsOnly bit, 
			@InteractedUsersDateRange int,
			@NonInteractedUsersPercent int,
			@NonInteractedRunId uniqueidentifier,
			@UtcDate datetime, 
			@UseLocalTimeZone bit,
			@DoubleOptIn bit,
			@StrDoubleOptIn nvarchar(10)

		SET @UtcDate = GetUtcDate()
		SELECT TOP 1 @StrDoubleOptIn = S.Value FROM STSettingType T JOIN STSiteSetting S ON T.Id = S.SettingTypeId WHERE Name = 'Marketier.EnableDoubleOptin' AND SiteId = @SiteId
		IF (@StrDoubleOptIn IS NOT NULL AND LOWER(@StrDoubleOptIn) = 'true') SET @DoubleOptIn = 1

		SELECT TOP 1 @UseLocalTimeZone = UseLocalTimeZone,
			@UniqueRecipientsOnly = UniqueRecipientsOnly,
			@InteractedUsersDateRange = InteractedUsersDateRange,
			@NonInteractedUsersPercent = NonInteractedUsersPercent,
			@NonInteractedRunId = NonInteractedRunId
		FROM MKCampaignSend WHERE Id = @CampaignSendId
			
		INSERT INTO @tbDebugInfo SELECT 'Getting contacts for sites.'
		
		DECLARE @tbAllContactIds TABLE (Id uniqueidentifier)
		IF EXISTS (SELECT 1 FROM MKCampaignDistributionList CDL 
				JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
			WHERE CampaignSendId = @CampaignSendId AND DL.ListType = 0)
		BEGIN
			INSERT INTO @tbDebugInfo SELECT 'Getting contacts for Manual Lists'

			INSERT INTO  @tbAllContactIds 
			SELECT C.[UserId] 
			FROM MKCampaignDistributionList CDL 
				JOIN TADistributionListUser DLU ON CDL.DistributionListId = DLU.DistributionListId AND ContactListSubscriptionType != 3
				JOIN dbo.vw_contacts C ON C.UserId = DLU.UserId
				JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id
			WHERE CDL.CampaignSendId = @CampaignSendId AND DL.ListType = 0
			OPTION (RECOMPILE)

			INSERT INTO @tbDebugInfo SELECT 'Manual List Count: ' + CAST(@@ROWCOUNT AS nvarchar(20))
		END

		--Get all the contacts for AUTO LIST
		DECLARE @tbAutoListIds TABLE (Id uniqueidentifier, RowNum int)

		INSERT INTO @tbAutoListIds
		SELECT DistributionListId, ROW_NUMBER() OVER (ORDER BY DL.Id) RowNum
		FROM MKCampaignDistributionList CDL 
			JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
		WHERE CampaignSendId = @CampaignSendId AND DL.ListType = 1 AND DL.SystemListType = 1

		IF NOT EXISTS (SELECT 1 FROM @tbAutoListIds)
		BEGIN
			INSERT INTO @tbAutoListIds
			SELECT DistributionListId, ROW_NUMBER() OVER (ORDER BY DL.Id) RowNum
			FROM MKCampaignDistributionList CDL 
				JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
			WHERE CampaignSendId = @CampaignSendId AND DL.ListType = 1
		END
		
		DECLARE @Cnt int, @TempId uniqueidentifier
		SELECT @Cnt = MAX(RowNum) FROM @tbAutoListIds

		INSERT INTO @tbDebugInfo SELECT 'Getting contacts for Auto Lists Count: ' + CAST(@Cnt AS nvarchar(20))

		WHILE (@Cnt > 0)
		BEGIN
			SELECT @TempId = Id FROM @tbAutoListIds WHERE RowNum = @Cnt

			INSERT INTO @tbDebugInfo SELECT 'Start Auto List: ' + CAST(@TempId AS nvarchar(36))

			DECLARE @queryXml xml, @TotalRecords int 
			SELECT @queryXml = SearchXml FROM dbo.TADistributionListSearch a, CTSearchQuery b  
					WHERE b.Id = a.SearchQueryId AND a.DistributionListId = @TempId 
				
			INSERT INTO @tbAllContactIds
			EXEC Contact_SearchContact
				@Xml = @queryXml,
				@ApplicationId = @SiteId, 
				@MaxRecords = 0, 
				@ContactListId = @TempId, 
				@TotalRecords=@TotalRecords output,  
				@IncludeAddress = 0

			INSERT INTO @tbDebugInfo SELECT 'AutoListId:' + CAST(@TempId as nvarchar(36)) + ' | Contacts Count:' + CAST(@TotalRecords AS nvarchar(20))

			SET @Cnt = @Cnt - 1
		END
		
		-- Get all the contacts that are manually added to the campaign (Obsolete)
		IF EXISTS (SELECT 1 FROM MKCampaignUser WHERE CampaignId = @CampaignId)
		BEGIN
			INSERT @tbAllContactIds
			SELECT C.UserId
			FROM vw_contacts C
				JOIN MKCampaignUser CU ON CU.UserId = C.UserId AND CU.CampaignId = @CampaignId
				JOIN vw_contactsite CS ON CS.UserId = C.UserId

			INSERT INTO @tbDebugInfo SELECT 'Getting contacts manually added to campaign ' + CAST(@@ROWCOUNT AS nvarchar(20))
		END

		DECLARE @CurrentUser uniqueidentifier
		SELECT TOP 1 @CurrentUser = ISNULL(ModifiedBy, CreatedBy) FROM MKCampaignSend WHERE Id = @CampaignSendId AND IncludeCurrentUser = 1
		IF @CurrentUser IS NOT NULL
		BEGIN
			INSERT INTO @tbAllContactIds
			SELECT @CurrentUser
		END

		DECLARE @tbContactIds TABLE(Id uniqueidentifier primary key)

		IF EXISTS (SELECT 1 FROM MKCampaign WHERE Id = @CampaignId AND IncludeUnsubscribedUsers = 1)
		BEGIN
			INSERT INTO @tbContactIds
			SELECT DISTINCT C.Id FROM @tbAllContactIds C
		END
		ELSE
		BEGIN
		INSERT INTO @tbContactIds
			SELECT DISTINCT C.Id 
			FROM @tbAllContactIds C 
				LEFT OUTER JOIN USUserUnsubscribe U ON U.UserId = C.Id AND (U.CampaignId IS NULL OR U.CampaignId = @CampaignId)
				LEFT OUTER JOIN USResubscribe R ON R.UserId = C.Id AND R.CampaignId = @CampaignId
			WHERE (U.Id IS NULL OR R.Id IS NOT NULL)
			AND (@DoubleOptIn IS NULL OR C.Id IN (SELECT ContactId FROM [USEmailOptIn] WHERE Type = 3))
		END

		DECLARE @Count int
		SELECT @Count = COUNT(1) FROM @tbContactIds
		INSERT INTO @tbDebugInfo SELECT 'Total contacts :' + CAST(@Count AS nvarchar(20))

		-- If the campaign type is 3(flow)remove user which are in flow state
		IF (@CampaignType = 3)
		BEGIN
			DELETE C FROM @tbContactIds C
			INNER JOIN MAFlowUserState M ON C.Id = M.UserId
			WHERE M.FlowType= 1 OR M.FlowType = 3
		END

		-- Use unique recipients logic if there are more than one campaign run
		IF @UniqueRecipientsOnly = 1 AND EXISTS (SELECT 1 FROM MKCampaignRunHistory WHERE CampaignId = @CampaignId AND Id != @CampaignRunId)
		BEGIN
			DECLARE @tbSentIds TABLE (Id uniqueidentifier primary key)
			INSERT INTO @tbSentIds
			SELECT DISTINCT UserId FROM MKEmailSendLog WHERE CampaignId = @CampaignId AND ApplicationId = @SiteId

			DELETE C FROM @tbContactIds C
			WHERE EXISTS(SELECT 1 FROM @tbSentIds S WHERE C.Id = S.Id)

			SELECT @Count = COUNT(1) FROM @tbContactIds
			INSERT INTO @tbDebugInfo SELECT 'Total unique contacts :' + CAST(@Count AS nvarchar(20))
		END

		-- Interacted Users Only
		IF @InteractedUsersDateRange IS NOT NULL AND @InteractedUsersDateRange > 0
		BEGIN
			DECLARE @tbNonInteracted TABLE (Id uniqueidentifier)
			DELETE T OUTPUT deleted.Id INTO @tbNonInteracted
			FROM @tbContactIds T
				JOIN vw_contacts C ON T.Id = C.Id
			WHERE C.LastInteractionDate IS NULL 
				OR DATEDIFF(DD, C.LastInteractionDate, @UtcDate) > @InteractedUsersDateRange

			SELECT @Count = COUNT(1) FROM @tbContactIds
			INSERT INTO @tbDebugInfo SELECT 'After resolving interacted users :' + CAST(@Count AS nvarchar(20))

			IF @NonInteractedUsersPercent IS NOT NULL AND @NonInteractedUsersPercent > 0
			BEGIN
				DECLARE @NonInteractedCount int, @TargetPercent int
				SELECT @NonInteractedCount = COUNT(1) FROM @tbNonInteracted

				SET @TargetPercent = (@NonInteractedCount * @NonInteractedUsersPercent) / 100

				INSERT INTO @tbContactIds
				SELECT TOP (@TargetPercent) Id FROM @tbNonInteracted

				SELECT @Count = COUNT(1) FROM @tbContactIds
				INSERT INTO @tbDebugInfo SELECT 'After resolving non-interacted users :' + CAST(@Count AS nvarchar(20))
			END
		END

		-- Not opened users
		IF @NonInteractedRunId IS NOT NULL AND @NonInteractedRunId != dbo.GetEmptyGUID()
		BEGIN
			DECLARE @tbOpened TABLE (Id uniqueidentifier primary key)
			INSERT INTO @tbOpened
			SELECT DISTINCT UserId FROM MKEmailSendLog WHERE CampaignRunId = @NonInteractedRunId AND Opened = 1

			DELETE C FROM @tbOpened T 
				JOIN @tbContactIds C ON T.Id = C.Id

			SELECT @Count = COUNT(1) FROM @tbContactIds
			INSERT INTO @tbDebugInfo SELECT 'After resolving non-interacted users :' + CAST(@Count AS nvarchar(20))
		END

		IF EXISTS (SELECT 1 FROM MKCampaignRunWorkTable W WHERE CampaignRunId = @CampaignRunId)
		BEGIN
			DELETE C FROM @tbContactIds C
			WHERE EXISTS (SELECT 1 FROM MKCampaignRunWorkTable W WHERE CampaignRunId = @CampaignRunId AND W.UserId = C.Id)

			SELECT @Count = COUNT(1) FROM @tbContactIds
			INSERT INTO @tbDebugInfo SELECT 'After removing already filled contacts :' + CAST(@Count AS nvarchar(20))
		END

		DECLARE @tblContacts TYSearchContact
		INSERT @tblContacts
		SELECT DISTINCT C.UserId, 
			C.UserId, 
			C.FirstName, 
			C.MiddleName, 
			C.LastName, 
			C.CompanyName, 
			C.BirthDate,
			C.Gender, 
			C.AddressId, 
			C.Status, 
			C.HomePhone, 
			C.MobilePhone, 
			C.OtherPhone, 
			C.ImageId, 
			C.Notes, 
			C.Email, 
			C.ContactType,
			C.ContactSourceId,
			0,
			NULL,
			NULL
		FROM vw_contacts C
			JOIN @tbContactIds T ON C.Id = T.Id
		WHERE C.Status = 1
		
		DECLARE @LftValue int, @RgtValue int, @MasterSiteId uniqueidentifier
		SELECT @LftValue = LftValue, @RgtValue = RgtValue, @MasterSiteId = MasterSiteId FROM SISite WHERE Id = @SiteId

		-- Update SiteId
		IF EXISTS(SELECT TOP 1 Id FROM SISite WHERE LftValue > @LftValue AND RgtValue < @RgtValue)
		BEGIN
			IF @UseLocalTimeZone = 1
			BEGIN
				INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for local timezone - yes'

				-- If user has access to current site, then use current site
				;WITH PrimaryCTE AS
				(
					SELECT CS.UserId, CS.SiteId, ROW_NUMBER() OVER (PARTITION BY CS.UserId ORDER BY S.LftValue) AS PRank
					FROM vw_contactSite CS
						JOIN @tblContacts T ON T.UserId = CS.UserId
						JOIN SISite S ON S.Id = CS.SiteId
					WHERE CS.Status = 1 AND CS.IsPrimary = 1
						AND S.Status = 1 AND S.MasterSiteId = @MasterSiteId AND S.LftValue >= @LftValue AND S.RgtValue <= @RgtValue
				)

				UPDATE T SET T.SiteId = P.SiteId
				FROM @tblContacts T 
					JOIN PrimaryCTE P ON T.UserId = P.UserId
				WHERE P.PRank = 1

				INSERT INTO @tbDebugInfo SELECT 'Updating Primary SiteId for Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))

				IF EXISTS (SELECT 1 FROM @tblContacts WHERE SiteId IS NULL)
				BEGIN
					UPDATE T SET T.SiteId = CS.SiteId
					FROM vw_contactSite CS
						JOIN @tblContacts T ON T.UserId = CS.UserId
					WHERE T.SiteId IS NULL AND CS.SiteId = @SiteId AND CS.Status = 1

					INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
				END

				IF EXISTS (SELECT 1 FROM @tblContacts WHERE SiteId IS NULL)
				BEGIN
					UPDATE @tblContacts SET SiteId = @SiteId WHERE SiteId IS NULL

					INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for missing Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
				END
			END
			ELSE
			BEGIN
				INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for local timezone - no'

				UPDATE T SET T.SiteId = CS.SiteId
				FROM vw_contactSite CS
					JOIN @tblContacts T ON T.UserId = CS.UserId
				WHERE CS.SiteId = @SiteId AND CS.Status = 1

				INSERT INTO @tbDebugInfo SELECT 'Updating SiteId for Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))

				IF EXISTS (SELECT 1 FROM @tblContacts WHERE SiteId IS NULL)
				BEGIN
					;WITH PrimaryCTE AS
					(
						SELECT CS.UserId, CS.SiteId, ROW_NUMBER() OVER (PARTITION BY CS.UserId ORDER BY S.LftValue) AS PRank
						FROM vw_contactSite CS
							JOIN @tblContacts T ON T.UserId = CS.UserId
							JOIN SISite S ON S.Id = CS.SiteId
						WHERE CS.Status = 1 AND CS.IsPrimary = 1
							AND S.Status = 1 AND S.MasterSiteId = @MasterSiteId AND S.LftValue >= @LftValue AND S.RgtValue <= @RgtValue
					)

					UPDATE T SET T.SiteId = P.SiteId
					FROM @tblContacts T 
						JOIN PrimaryCTE P ON T.UserId = P.UserId
					WHERE T.SiteId IS NULL AND P.PRank = 1

					INSERT INTO @tbDebugInfo SELECT 'Updating Primary SiteId for Contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
				END
			END
		END
		ELSE
			UPDATE @tblContacts SET SiteId = @SiteId

		DECLARE @RunDate datetime, @LocalRunDate datetime, @EmptyGuid uniqueidentifier
		SET @EmptyGuid = dbo.GetEmptyGUID()
		SELECT TOP 1 @Rundate = RunDate FROM MKCampaignRunHistory WHERE Id = @CampaignRunid

		-- Update SendDate
		IF @UseLocalTimeZone = 1
		BEGIN
			UPDATE @tblContacts SET SendDate = @Rundate WHERE SiteId = @SiteId

			INSERT INTO @tbDebugInfo SELECT 'Updating send date for same site contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
			
			SET @LocalRunDate = dbo.ConvertTimeFromUtc(@RunDate, @EmptyGuid)
			DECLARE @tbSiteRunDate TABLE(SiteId uniqueidentifier primary key, RunDate datetime)
			INSERT INTO @tbSiteRunDate
			SELECT Id, dbo.ConvertTimeToUtcUsingSiteTimeZone(@LocalRunDate, Id) FROM SISite

			UPDATE C SET C.SendDate = T.RunDate
			FROM @tblContacts C
				JOIN @tbSiteRunDate T ON C.SiteId = T.SiteId
			WHERE C.SendDate IS NULL

			INSERT INTO @tbDebugInfo SELECT 'Updating send date for other site contacts: ' + CAST(@@ROWCOUNT AS nvarchar(20))
		END
		ELSE
		BEGIN
			UPDATE @tblContacts SET SendDate = @Rundate
		END

		;WITH CTE AS
		(
			SELECT ROW_NUMBER() OVER(PARTITION BY C.UserId ORDER BY [Email]) AS RowNumber,
				C.UserId, 
				FirstName , 
				MiddleName ,
				LastName ,
				CompanyName ,
				BirthDate, 
				Gender,
				AddressId, 
				Status,
				HomePhone,
				MobilePhone, 
				OtherPhone, 
				Notes,
				Email,
				SiteId,
				SendDate
			FROM @tblContacts C 
		)

		INSERT INTO MKCampaignRunWorkTable 
		(
			CampaignRunId, 
			UserId, 
			FirstName, 
			MiddleName, 
			LastName,
			CompanyName, 
			BirthDate, 
			Gender, 
			AddressId, 
			Status, 
			HomePhone, 
			MobilePhone, 
			OtherPhone,
			Notes, 
			Email,
			SiteId,
			SendDate
		)
		SELECT TOP(@EmailPerCampaignLimit) 
			@CampaignRunId,
			UserId, 
			FirstName, 
			MiddleName,
			LastName,
			CompanyName,
			BirthDate, 
			Gender,
			AddressId, 
			Status,
			HomePhone,
			MobilePhone, 
			OtherPhone, 
			Notes,
			Email,
			SiteId,
			SendDate
		FROM CTE
		WHERE RowNumber = 1
		ORDER BY SendDate

		INSERT INTO @tbDebugInfo SELECT 'Worktable Populated count:' + CAST(@@ROWCOUNT AS nvarchar(20))

		IF (@CampaignType = 3)
		BEGIN
			-- Add the remaining user in MAFlowUserState
			EXEC Flow_AddEmailBlastUserStateFromCampaign @CampaignRunId, @CampaignId
		END
		
		SET @Populated = 1

		DECLARE @ExternalId int
		SELECT @ExternalId = MAX(ExternalId) FROM MKCampaignRunHistory 
		SELECT @TotalContacts = COUNT(1) FROM MKCampaignRunWorkTable WHERE CampaignRunId = @CampaignRunId

		UPDATE MKCampaignRunHistory 
		SET ProcessingStatus = 2,
			TotalContacts = @TotalContacts,
			ExternalId = ISNULL(@ExternalId, 0) + 1
		WHERE Id = @CampaignRunId
	END
			
	SET @DebugInfo = (SELECT Message FROM @tbDebugInfo For XML PATH (''), ROOT('Messages'))
END
GO
IF(OBJECT_ID('Facet_GetFacetsByNavigation') IS NOT NULL)
	DROP PROCEDURE Facet_GetFacetsByNavigation
GO
CREATE PROCEDURE [dbo].[Facet_GetFacetsByNavigation]
    (
      @Id UNIQUEIDENTIFIER ,
      @ExistingFacetValueIds XML = NULL ,
      @ExistingFacets XML = NULL ,
      @UseLimit BIT = 1,
	  @ApplicationId uniqueidentifier=NULL
    )
AS 
    BEGIN

        DECLARE @filterId UNIQUEIDENTIFIER
        SELECT  @filterId = QueryId
        FROM    NVNavNodeNavFilterMap
        WHERE   NavNodeId = @Id

        DECLARE @totProd INT
        SELECT  @totProd = COUNT(C.Id)
        FROM    ATFacetRangeProduct_Cache C
		INNER JOIN VWProduct P ON C.ProductId=P.Id AND P.SiteId=@ApplicationId  
		Where C.SiteId=@ApplicationId
		Group By P.Id

        SET @UseLimit = ISNULL(@UseLimit, 0)
        IF ( @ExistingFacetValueIds IS NULL
             AND @ExistingFacets IS NULL
           ) 
            BEGIN

                SELECT 
			C.FacetID,
			C.FacetValueID,
			MIN(C.DisplayText) DisplayText,
			Count(*) ProductCount,
			MIN(NF.Sequence) FacetSequence,
			Case FR.Sequence 
				WHEN NULL  THEN @totProd - COUNT(*)
				ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
			END AS SortOrder,
			MIN(AF.AllowMultiple) AllowMultiple
		FROM 
			ATFacetRangeProduct_Cache C 
			INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
			INNER JOIN vwFacet AF ON AF.Id = NF.FacetId	AND AF.SiteId = C.SiteId
			INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
			INNER JOIN (Select Distinct ProductId FROM VWSKU Where SiteId=@ApplicationId AND IsActive=1 AND IsOnline=1 AND IsSellable=1  ) P ON C.ProductId=P.ProductId 
			LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
			LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
						AND C.SiteId=@ApplicationId
                GROUP BY C.FacetValueID ,
                        C.FacetID
                UNION ALL
                SELECT  C.FacetID ,
                        NULL FacetValueID ,
                        MIN(AF.Title) DisplayText ,
                        COUNT(*) ,
                        MIN(Sequence) FacetSequence ,
                        0 SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN vwFacet AF ON AF.Id = NF.FacetId AND AF.SiteId = C.SiteId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
						INNER JOIN (Select Distinct ProductId FROM VWSKU Where SiteId=@ApplicationId AND IsActive=1 AND IsOnline=1 AND IsSellable=1  ) P ON C.ProductId=P.ProductId 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
						AND C.SiteId=@ApplicationId
                GROUP BY C.FacetID 
            END
        ELSE 
            IF ( @ExistingFacetValueIds IS NOT NULL ) 
                BEGIN
 

                    CREATE TABLE #FilterProductIds
                        (
                          Id UNIQUEIDENTIFIER
                            CONSTRAINT [PK_#cteValueToIdValue]
                            PRIMARY KEY CLUSTERED ( [Id] ASC )
                            WITH ( PAD_INDEX = OFF,
                                   STATISTICS_NORECOMPUTE = OFF,
                                   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                                   ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
                        )
                    ON  [PRIMARY]

                    INSERT  INTO #FilterProductIds
                            SELECT  C.ProductID
                            FROM    ATFacetRangeProduct_Cache C
							INNER JOIN ATFacet F ON C.FacetID =F.Id 
							INNER JOIN (Select Distinct ProductId FROM VWSKU Where SiteId=@ApplicationId AND IsActive=1 AND IsOnline=1 AND IsSellable=1  ) P ON C.ProductId=P.ProductId 
                                    INNER JOIN @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON tab.col.value('text()[1]',
                                                              'uniqueidentifier') = C.FacetValueID
                            Where C.SiteId=@ApplicationId
							AND F.SiteId=@ApplicationId
							GROUP BY C.ProductID
                            HAVING  COUNT(C.FacetValueID) >= ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab1 ( col1 )
                                                             )

				SELECT 
					C.FacetID,
					C.FacetValueID,
					MIN(C.DisplayText) DisplayText,
					Count(*) ProductCount,
					MIN(NF.Sequence) FacetSequence,
					Case FR.Sequence 
								WHEN NULL  THEN @totProd - COUNT(*)
								ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
							END AS SortOrder,
					MIN(AF.AllowMultiple) AllowMultiple
				FROM 
					ATFacetRangeProduct_Cache C 
					INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
					INNER JOIN vwFacet AF ON AF.Id = NF.FacetId	AND AF.SiteId = C.SiteId
					INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
					INNER JOIN #FilterProductIds P ON P.ID=C.ProductID
					LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
					LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
							AND C.SiteId=@ApplicationId
                    GROUP BY C.FacetValueID ,
                            C.FacetID
                    UNION ALL
                    SELECT  C.FacetID ,
                            NULL FacetValueID ,
                            MIN(AF.Title) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            0 SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProductTop_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN vwFacet AF ON AF.Id = NF.FacetId AND AF.SiteId = C.SiteId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
							AND C.SiteId=@ApplicationId
                    GROUP BY C.FacetID 


                    DROP TABLE #FilterProductIds

                END
            ELSE 
                BEGIN
                   ------------------NEW---------------------------------
                    DECLARE @FacetValues TABLE
                        (
                          FacetValueID UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER
                        )
                    INSERT  INTO @FacetValues
                            SELECT  tab.col.value('(value/guid/text())[1]',
                                                  'uniqueidentifier') ,
                                    tab.col.value('(key/guid/text())[1]',
                                                  'uniqueidentifier')
                            FROM    @ExistingFacets.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item') tab ( col ) ;
                 

                 
                 

                    DECLARE @FilteredProducts TABLE
                        (
                          ProductId UNIQUEIDENTIFIER
                        )           
                    
                    DECLARE @RequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
       
                    INSERT  INTO @RequiredProduct
                            SELECT DISTINCT
                                    C.ProductId ,
                                    Row_Number() OVER ( PARTITION BY ProductId ORDER BY ProductID ) AS RowNumber
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.vwFacet F ON F.Id = FV.FacetId AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                            WHERE   F.AllowMultiple = 0 

                    DELETE  FROM @RequiredProduct
                    WHERE   RowNumber NOT IN ( SELECT   MAX(RowNumber)
                                               FROM     @RequiredProduct )


                    DECLARE @MultiRequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
                     INSERT  INTO @MultiRequiredProduct
                            SELECT  DISTINCT
                                    ProductID ,
                                    FacetID ,
                                    Row_Number() OVER ( PARTITION BY ProductID ORDER BY ProductID ) AS RowNumber
                                    
                                    FROM (
                                    SELECT DISTINCt C.ProductID,
                                    C.FacetID
                                    
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.vwFacet F ON F.Id = FV.FacetId AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                            WHERE   F.AllowMultiple = 1)TV
                           



                    IF NOT EXISTS ( SELECT  *
                                    FROM    @RequiredProduct ) 
                        BEGIN
                            INSERT  INTO @FilteredProducts
                                    SELECT  ProductId
                                    FROM    @MultiRequiredProduct
                                    WHERE   RowNumber IN (
                                            SELECT  MAX(RowNumber)
                                            FROM    @MultiRequiredProduct )
                       
                        END
                    ELSE 
                        IF NOT EXISTS ( SELECT  *
                                        FROM    @MultiRequiredProduct ) 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  ProductId
                                        FROM    @RequiredProduct
                            END
                        ELSE 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  RP.ProductId
                                        FROM    @RequiredProduct RP
                                                INNER JOIN ( SELECT
                                                              ProductId
                                                             FROM
                                                              @MultiRequiredProduct
                                                             WHERE
                                                              RowNumber IN (
                                                              SELECT
                                                              MAX(RowNumber)
                                                              FROM
                                                              @MultiRequiredProduct )
                                                           ) TV ON TV.ProductId = RP.ProductId


                            END

                    DECLARE @FacetResults TABLE
                        (
                          FacetId UNIQUEIDENTIFIER ,
                          FacetValueId UNIQUEIDENTIFIER ,
                          DisplayText NVARCHAR(250) ,
                          FacetName NVARCHAR(250) ,
                          ProductCount INT ,
                          FacetSequence INT ,
                          SortOrder INT ,
                          AllowMultiple INT ,
                          ENABLED INT
                        )
                    INSERT  INTO @FacetResults
                            ( FacetId ,
                              FacetValueId ,
                              DisplayText ,
                              FacetName ,
                              ProductCount ,
                              FacetSequence ,
                              SortOrder ,
                              AllowMultiple ,
                              ENABLED
                            )
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.vwFacet F ON F.Id = C.FacetID AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @FilteredProducts RP ON RP.ProductId = C.ProductID
									LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 0
							        AND NFE.QueryId IS NULL
                                    AND ( RP.ProductId IS NOT NULL
                                          OR NOT EXISTS ( SELECT
                                                              ProductId
                                                          FROM
                                                              @FilteredProducts )
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
--Close now we need to get the other multi values that are not currently in the required products but would be filtered by any required products. 
--This query should be the multi facets that have no products currently showing in the grid. 
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.vwFacet F ON F.Id = C.FacetID AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( 
																		--MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
                                                              ( SELECT
                                                              COUNT(DISTINCT FacetId)
                                                              FROM
                                                              @MultiRequiredProduct
                                                              ) = 1 )
																	---End of expensive
                                                              )
							        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 1
							        AND NFE.QueryId IS NULL  
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.vwFacet F ON F.Id = C.FacetID AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
																		--( SELECT
																		--COUNT(DISTINCT FacetId)
																		--FROM
																		--@MultiRequiredProduct
																		--) = 1
                                                              )
																	---End of expensive
                                                              )
									LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 1
							        AND NFE.QueryId IS NULL
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                             
                                        )
                                        
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
        
                        
        
        
        
                    SELECT  FacetId ,
                            FacetValueId ,
                            DisplayText ,
                            ProductCount ,
                            FacetSequence ,
                            SortOrder ,
                            AllowMultiple ,
                            ENABLED
                    FROM    @FacetResults
                    UNION
                    SELECT  FacetId ,
                            NULL ,
                            FacetName AS DisplayText ,
                            SUM(ProductCount) ,
                            MAX(FacetSequence) ,
                            MAX(SortOrder) ,
                            MAX(AllowMultiple) ,
                            MAX(ENABLED)
                    FROM    @FacetResults
                    GROUP BY FacetId ,
                            FacetName
                    ORDER BY FacetValueId DESC
                   
                   
                   
                   
                   ----------------END OF NEW------------------------------
                        
                        
                        
                        
                        

                END
    END
GO
IF NOT EXISTS (SELECT *  FROM sys.indexes  WHERE name='IX_ATFacetRangeProduct_Cache_ProductId_SiteId' 
    AND object_id = OBJECT_ID('[dbo].[ATFacetRangeProduct_Cache]'))
BEGIN
   CREATE NONCLUSTERED INDEX [IX_ATFacetRangeProduct_Cache_ProductId_SiteId]
		ON [dbo].[ATFacetRangeProduct_Cache] ([ProductID],[SiteId])

END


  
GO
IF EXISTS (SELECT *  FROM sys.indexes  WHERE name='IX_ATFacetRangeProduct_Cache_SiteId' 
    AND object_id = OBJECT_ID('[dbo].[ATFacetRangeProduct_Cache]'))
BEGIN
 	DROP INDEX [IX_ATFacetRangeProduct_Cache_SiteId] ON [dbo].[ATFacetRangeProduct_Cache]
END
GO


CREATE NONCLUSTERED INDEX [IX_ATFacetRangeProduct_Cache_SiteId]
ON [dbo].[ATFacetRangeProduct_Cache] ([SiteId])
INCLUDE ([FacetID],[FacetValueID],[DisplayText],[ProductID])

GO

IF NOT EXISTS (SELECT *  FROM sys.indexes  WHERE name='IX_NVNavNodeFacet_NavNodeId' 
    AND object_id = OBJECT_ID('[dbo].[NVNavNodeFacet]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_NVNavNodeFacet_NavNodeId]
	ON [dbo].[NVNavNodeFacet] ([NavNodeId])
END
GO
IF(OBJECT_ID('Taxonomy_GetTaxonomyHierarchy') IS NOT NULL)
	DROP PROCEDURE Taxonomy_GetTaxonomyHierarchy
GO
CREATE PROCEDURE [dbo].[Taxonomy_GetTaxonomyHierarchy](
@SiteId uniqueidentifier,
@TaxonomyId uniqueidentifier = null,
@ParentLevel int,
@ChildrenLevel int 
)
as
Begin
Declare
@Lft bigint,
@Rgt bigint,
@Id uniqueidentifier


if(@TaxonomyId is null)
	begin
		select @Id = Id from COTaxonomy s where s.ParentId is null And ApplicationId =@SiteId and status<> dbo.GetDeleteStatus()
		SELECT @Lft=LftValue,@Rgt=RgtValue FROM COTaxonomy WHERE Id=@Id And ApplicationId=@SiteId
		 
	End
else
	begin
		SELECT @Lft=LftValue,@Rgt=RgtValue FROM COTaxonomy WHERE Id=@TaxonomyId AND ApplicationId=@SiteId
	End

IF @ParentLevel <> -1
		Select @ParentLevel = count(*)+1 - @ParentLevel from COTaxonomy S
		Where S.LftValue < @Lft and S.RgtValue > @Rgt AND ApplicationId=@SiteId
	ELSE
		Select @ParentLevel = 1

If (@ChildrenLevel is null)
	set @ChildrenLevel =0

	IF @ChildrenLevel = 1 
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
							   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
							    FN.UserFullName CreatedByFullName,
								MN.UserFullName ModifiedByFullName,
								xmlTable.Status
			FROM
				(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM  COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId  AND T.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId  AND T.ApplicationId=@SiteId)
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T
					WHERE T.ParentId = @TaxonomyId or T.Id = @TaxonomyId AND T.ApplicationId=@SiteId
					UNION ALL
				SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T
					WHERE T.ParentId = @TaxonomyId or T.Id = @TaxonomyId  AND T.ApplicationId=@SiteId)as xmlTable
					LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
					LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
					 ORDER BY xmlTable.LftValue, xmlTable.[Sign]
	END
	ELSE IF ( @ChildrenLevel = 0 and @TaxonomyId is not null)
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
						   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
						   FN.UserFullName CreatedByFullName,
						   MN.UserFullName ModifiedByFullName,
						   xmlTable.Status
		FROM
			(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue )
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
							   T.ModifiedBy,T.ModifiedDate,
							   T.Status
				FROM  COTaxonomy T
				WHERE T.Id = @TaxonomyId AND T.ApplicationId=@SiteId
				UNION ALL
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
				FROM COTaxonomy T 
				WHERE T.Id = @TaxonomyId AND T.ApplicationId=@SiteId)as xmlTable 
				LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
				ORDER BY xmlTable.LftValue, xmlTable.[Sign]
	END
	ELSE IF @ChildrenLevel = -1 
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
							   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
							   FN.UserFullName CreatedByFullName,
						       MN.UserFullName ModifiedByFullName,
							   xmlTable.Status
			FROM
				(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM  COTaxonomy T 
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId
					UNION ALL
				SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId) as xmlTable 
					LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
					LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
					ORDER BY xmlTable.LftValue, xmlTable.[Sign]
	END
	ELSE IF (@ChildrenLevel = 0 and @TaxonomyId is null) 
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
							   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
							   FN.UserFullName CreatedByFullName,
						       MN.UserFullName ModifiedByFullName,
							   xmlTable.Status
			FROM
				(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND ApplicationId=@SiteId AND P.ApplicationId=@SiteId)
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM  COTaxonomy T 
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId
					UNION ALL
				SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T 
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId) as xmlTable 
					LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
					LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
					ORDER BY xmlTable.LftValue, xmlTable.[Sign]
	End
	Else
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
							   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
						       FN.UserFullName CreatedByFullName,
						       MN.UserFullName ModifiedByFullName,
						       xmlTable.Status
			FROM
				(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T
					WHERE T.LftValue >= @Lft and T.RgtValue <= @Rgt AND T.ApplicationId=@SiteId
						  AND @ChildrenLevel>=(Select count(*) + 1 
												From COTaxonomy M 
												WHERE M.LftValue Between @Lft and @Rgt
												And T.LftValue Between M.LftValue and M.RgtValue
												And M.LftValue Not in (T.LftValue ,@Lft) AND M.ApplicationId=@SiteId)
					UNION ALL
				SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM  COTaxonomy T 
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId
							AND @ChildrenLevel>=(Select count(*) + 1 
												From COTaxonomy M 
												WHERE M.LftValue Between @Lft and @Rgt
												And T.LftValue Between M.LftValue and M.RgtValue
												And M.LftValue Not in (T.LftValue ,@Lft) AND M.ApplicationId=@SiteId)) as xmlTable 
												LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
												LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
												ORDER BY xmlTable.LftValue, xmlTable.[Sign]

	END
END
GO
PRINT 'Altering FUNCTION Page_GetUnmanagedXml'
GO
IF (OBJECT_ID('Page_GetUnmanagedXml') IS NOT NULL)
	DROP FUNCTION Page_GetUnmanagedXml
GO
CREATE FUNCTION [dbo].[Page_GetUnmanagedXml] 
(	
	@SourceUnmanagedXml		xml,
	@TargetUnmanagedXml		xml,
	@TargetSiteId			uniqueidentifier
)
RETURNS xml
AS
BEGIN
	IF @SourceUnmanagedXml IS NOT NULL
	BEGIN
		DECLARE @tbControls TABLE (Name nvarchar(1000), Type nvarchar(500))

		IF (@TargetUnmanagedXml IS NOT NULL)
			SET @SourceUnmanagedXml = dbo.MergeUnManagedXml(@SourceUnmanagedXml, @TargetUnmanagedXml, @TargetSiteId)

		INSERT INTO @tbControls
		SELECT C.value('fn:local-name(.)', 'nvarchar(1000)'), 
			C.value('@type', 'nvarchar(500)')
		FROM @SourceUnmanagedXml.nodes('//CLControls/*') as Controls(C)	
		WHERE C.value('fn:local-name(.)', 'nvarchar(1000)') != 'CLLeadContainer'

		INSERT INTO @tbControls
		SELECT C.value('@id', 'nvarchar(1000)'), 
			'CLLeadContainer'
		FROM @SourceUnmanagedXml.nodes('//CLControls/CLLeadContainer') as Controls(C)	
		
		WHILE EXISTS (SELECT 1 FROM @tbControls)
		BEGIN
			DECLARE @ControlName nvarchar(1000), @ControlType nvarchar(500), 
				@SourceObjectId uniqueidentifier, @VariantObjectId uniqueidentifier, @TargetObjectId uniqueidentifier
			SELECT TOP 1 @ControlName = Name, @ControlType = Type FROM @tbControls

			IF LOWER(@ControlType) = 'clleadcontainer'
			BEGIN
				SELECT @SourceObjectId = C.value('@formId', 'uniqueidentifier')
				FROM @SourceUnmanagedXml.nodes('//CLControls/CLLeadContainer[@id=sql:variable("@ControlName")]') as Controls(C)

				IF @TargetUnmanagedXml IS NOT NULL
				BEGIN
					SELECT @TargetObjectId = C.value('@formId', 'uniqueidentifier')
					FROM @TargetUnmanagedXml.nodes('//CLControls/CLLeadContainer[@id=sql:variable("@ControlName")]') as Controls(C)
				END

				IF @TargetObjectId IS NOT NULL AND @TargetObjectId != dbo.GetEmptyGUID()
				BEGIN
					SET @SourceUnmanagedXml.modify('replace value of (//CLControls/CLLeadContainer[@id=sql:variable("@ControlName")]/@formId)[1] with sql:variable("@TargetObjectId")')	
				END
				ELSE
				BEGIN
					SELECT @VariantObjectId = [dbo].[Form_GetVariantId](@SourceObjectId, @TargetSiteId)
					IF @SourceObjectId IS NOT NULL AND @VariantObjectId IS NOT NULL
						SET @SourceUnmanagedXml.modify('replace value of (//CLControls/CLLeadContainer[@id=sql:variable("@ControlName")]/@formId)[1] with sql:variable("@VariantObjectId")')	
				END
			END
			ELSE IF LOWER(@ControlType) = 'clformcontainer'
			BEGIN
				SELECT @SourceObjectId = C.value('@formId', 'uniqueidentifier')
				FROM @SourceUnmanagedXml.nodes('//CLControls/*[local-name()=sql:variable("@ControlName")]') as Controls(C)

				IF @TargetUnmanagedXml IS NOT NULL
				BEGIN
					SELECT @TargetObjectId = C.value('@formId', 'uniqueidentifier')
					FROM @TargetUnmanagedXml.nodes('//CLControls/*[local-name()=sql:variable("@ControlName")]') as Controls(C)
				END

				IF @TargetObjectId IS NOT NULL AND @TargetObjectId != dbo.GetEmptyGUID()
				BEGIN
					SET @SourceUnmanagedXml.modify('replace value of (//CLControls/*[local-name()=sql:variable("@ControlName")]/@formId)[1] with sql:variable("@TargetObjectId")')	
				END
				ELSE
				BEGIN
					SELECT @VariantObjectId = [dbo].[Form_GetVariantId](@SourceObjectId, @TargetSiteId)
					IF @SourceObjectId IS NOT NULL AND @VariantObjectId IS NOT NULL
						SET @SourceUnmanagedXml.modify('replace value of (//CLControls/*[local-name()=sql:variable("@ControlName")]/@formId)[1] with sql:variable("@VariantObjectId")')	
				END
			END
			ELSE IF LOWER(@ControlType) = 'clcontentdataview'
			BEGIN
				SELECT @SourceObjectId = C.value('@nodeId', 'uniqueidentifier')
				FROM @SourceUnmanagedXml.nodes('//CLControls/*[local-name()=sql:variable("@ControlName")]') as Controls(C)
				
				IF @TargetUnmanagedXml IS NOT NULL
				BEGIN
					SELECT @TargetObjectId = C.value('@nodeId', 'uniqueidentifier')
					FROM @TargetUnmanagedXml.nodes('//CLControls/*[local-name()=sql:variable("@ControlName")]') as Controls(C)
				END

				IF @TargetObjectId IS NOT NULL AND @TargetObjectId != dbo.GetEmptyGUID()
				BEGIN
					SET @SourceUnmanagedXml.modify('replace value of (//CLControls/*[local-name()=sql:variable("@ControlName")]/@nodeId)[1] with sql:variable("@TargetObjectId")')	
				END
				ELSE
				BEGIN
					SELECT @VariantObjectId = [dbo].[Menu_GetVariantId](@SourceObjectId, @TargetSiteId)
					IF @SourceObjectId IS NOT NULL AND @VariantObjectId IS NOT NULL
						SET @SourceUnmanagedXml.modify('replace value of (//CLControls/*[local-name()=sql:variable("@ControlName")]/@nodeId)[1] with sql:variable("@VariantObjectId")')	
				END
			END

			DELETE FROM @tbControls WHERE Name like @ControlName
		END

	END
	
	RETURN @SourceUnmanagedXml
END
GO
IF(OBJECT_ID('PageDto_Import') IS NOT NULL)
	DROP PROCEDURE PageDto_Import
GO
CREATE PROCEDURE [dbo].[PageDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@PageMapNodeId			uniqueidentifier = NULL OUTPUT,
	@Publish				bit = NULL,	
	@Overwrite				bit = NULL,		
	@ModifiedBy				uniqueidentifier,
	@PreserveVariantContent bit = 1,
	@IgnoreExisting			bit = NULL,
	@InvalidateCache		bit = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier, @DisplayOrder int

	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @Id = [dbo].[Page_GetVariantId](@SourceId, @TargetSiteId)

	IF @PageMapNodeId IS NULL
	BEGIN
		DECLARE @ParentPageMapNode uniqueidentifier
		SELECT TOP 1 @ParentPageMapNode = PageMapNodeId, @DisplayOrder = DisplayOrder FROM PageMapNodePageDef 
			WHERE PageDefinitionId = @SourceId
		SET @PageMapNodeId = [dbo].[Menu_GetVariantId](@ParentPageMapNode, @TargetSiteId)
	END

	IF @PageMapNodeId IS NULL OR (@Id IS NOT NULL AND @IgnoreExisting = 1)
		RETURN

	SELECT @ModifiedBy = ModifiedBy FROM PageDefinition WHERE PageDefinitionId = @SourceId 
	IF @Publish IS NULL AND EXISTS (SELECT 1 FROM PageDefinition WHERE PageDefinitionId = @SourceId AND PageStatus = 8)
		SET @Publish = 1

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()
		INSERT INTO [dbo].[PageDefinition]
		(
			[PageDefinitionId],
			[TemplateId],
			[SiteId],
			[Title],
			[Description],
			[PageStatus],
			[WorkflowState],
			[PublishCount],
			[PublishDate],
			[FriendlyName],
			[WorkflowId],
			[CreatedBy],
			[CreatedDate],
			[ModifiedBy],
			[ModifiedDate],
			[ArchivedDate],
			[StatusChangedDate],
			[AuthorId],
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[ExcludeFromExternalSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			[SourcePageDefinitionId],
			[CustomAttributes]
		)
		SELECT @Id,
			[TemplateId],
			@TargetSiteId,
			[Title],
			D.[Description],
			CASE WHEN @Publish = 1 THEN 8 ELSE PageStatus END,
			1,
			0,
			'1900-01-01 00:00:00.000',
			[FriendlyName],
			@EmptyGuid,
			@ModifiedBy,
			@UtcNow,
			@ModifiedBy,
			@UtcNow,
			[ArchivedDate],
			[StatusChangedDate],
			@ModifiedBy,
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[ExcludeFromExternalSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			PageDefinitionId,
			D.[CustomAttributes]
		FROM [dbo].[PageDefinition] D
		WHERE PageDefinitionId = @SourceId				

		UPDATE PageMapNode SET TargetId = @Id
		WHERE TargetId = @SourceId AND SiteId = @TargetSiteId
		 
		INSERT INTO [dbo].[PageDefinitionSegment]
		(
			[Id],
			[PageDefinitionId],
			[DeviceId],
			[AudienceSegmentId],
			[UnmanagedXml],
			[ZonesXml]
		)
		SELECT NEWID(), 
			@Id,
			[DeviceId],
			[AudienceSegmentId],
			dbo.Page_GetUnmanagedXml([UnmanagedXml], NULL, @TargetSiteId),
			[ZonesXml]
		FROM [dbo].[PageDefinitionSegment] PS 
		WHERE PageDefinitionId = @SourceId

		INSERT INTO [dbo].[PageDetails]
		(
			[PageId],
			[PageDetailXML]
		)
		SELECT 
			@Id,
			[PageDetailXML]
		FROM [dbo].[PageDetails] PD
		WHERE PD.PageId = @SourceId
	END
	ELSE
	BEGIN
		UPDATE C SET
			C.Title = P.Title,
			C.TemplateId = P.TemplateId,
			C.InWFTemplateId = P.InWFTemplateId,
			C.Description = P.Description,
			C.PageStatus = CASE WHEN @Publish = 1 THEN 8 ELSE P.PageStatus END,
			C.FriendlyName = CASE WHEN @PreserveVariantContent = 0 THEN P.FriendlyName ELSE C.FriendlyName END,
			C.EnableOutputCache = P.EnableOutputCache,
			C.OutputCacheProfileName = P.OutputCacheProfileName,
			C.ExcludeFromSearch = P.ExcludeFromSearch,
			C.ExcludeFromExternalSearch = P.ExcludeFromExternalSearch,
			C.HasForms = P.HasForms,
			C.ScheduledPublishDate = P.ScheduledPublishDate,
			C.ScheduledArchiveDate = P.ScheduledArchiveDate,
			C.ModifiedDate = @UtcNow,
			C.ModifiedBy = P.ModifiedBy
		FROM PageDefinition P, PageDefinition C
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.UnmanagedXml = dbo.Page_GetUnmanagedXml(P.[UnmanagedXml], C.[UnmanagedXml], @TargetSiteId),
			C.ZonesXml = dbo.MergePageZone(P.ZonesXml, C.ZonesXml, @TargetSiteId)
		FROM PageDefinitionSegment P
			JOIN PageDefinitionSegment C ON P.DeviceId = C.DeviceId AND P.AudienceSegmentId = C.AudienceSegmentId
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.ContentId = P.ContentId,
			C.InWFContentId = P.InWFContentId,
			C.ContentTypeId = P.ContentTypeId,
			C.IsModified = P.IsModified,
			C.[IsTracked] = P.[IsTracked],
			C.[Visible] = P.[Visible],
			C.[InWFVisible] = P.[InWFVisible],
			C.[CustomAttributes] = P.[CustomAttributes],
			C.[DisplayOrder] = P.[DisplayOrder],
			C.[InWFDisplayOrder] = P.[InWFDisplayOrder]
		FROM PageDefinitionContainer P
			JOIN PageDefinitionContainer C ON P.ContainerName = C.ContainerName
			LEFT JOIN COContent CO ON C.ContentId = CO.Id
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId
			AND (@Overwrite = 1 OR CO.Id IS NULL OR CO.ApplicationId != @TargetSiteId)
		
		IF @PreserveVariantContent = 0
		BEGIN
			UPDATE C 
			SET	C.PageDetailXML = P.PageDetailXML
			FROM PageDetails P, PageDetails C
			WHERE C.PageId = @Id AND P.PageId = @SourceId	
		END

		DELETE FROM PageMapNodePageDef WHERE PageDefinitionId = @Id
	END

	INSERT INTO PageMapNodePageDef
	(
		PageDefinitionId,
		PageMapNodeId,
		DisplayOrder
	)
	SELECT
		@Id,
		@PageMapNodeId,
		@DisplayOrder

	INSERT INTO [PageDefinitionContainer]
	(
		[Id],
		PageDefinitionSegmentId ,
		[PageDefinitionId],
		[ContainerId],
		[ContentId],
		[InWFContentId],
		[ContentTypeId],
		[IsModified],
		[IsTracked],
		[Visible],
		[InWFVisible],
		DisplayOrder,
		InWFDisplayOrder,
		[CustomAttributes],
		[ContainerName]
	)
	SELECT NEWID(),
		CS.Id,
		@Id,
		ContainerId,
		ContentId,
		InWFContentId,
		ContentTypeId,
		IsModified,
		PC.IsTracked,
		Visible,
		InWFVisible,
		PC.DisplayOrder, 
		PC.InWFDisplayOrder, 
		PC.CustomAttributes,
		ContainerName
	FROM PageDefinitionContainer PC
		JOIN PageDefinitionSegment PS ON PC.PageDefinitionSegmentId = PS.Id
		JOIN PageDefinitionSegment CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
	WHERE PC.PageDefinitionId = @SourceId
		AND (@Id IS NULL OR ContainerName NOT IN (SELECT ContainerName FROM PageDefinitionContainer WHERE PageDefinitionId = @Id))

	DECLARE @SourceTargetMenuId uniqueidentifier
	SELECT TOP 1 @SourceTargetMenuId = PageMapNodeId FROM PageMapNode PN
		JOIN PageDefinition PD ON PN.TargetId = PD.PageDefinitionId AND PD.SiteId = PN.SiteId
	WHERE PD.PageDefinitionId = @SourceId

	IF @SourceTargetMenuId IS NOT NULL
	BEGIN
		UPDATE PageMapNode SET
			TargetId = @Id,
			Target = 1,
			TargetUrl = ''
		WHERE SourcePageMapNodeId = @SourceTargetMenuId
			AND SiteId = @TargetSiteId
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[USSecurityLevelObject]
	(
		[ObjectId],
		[ObjectTypeId],
		[SecurityLevelId]
	)
	SELECT @Id,
		ObjectTypeId,
		SecurityLevelId
	FROM [USSecurityLevelObject] S
	WHERE S.ObjectId = @SourceId

	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageScript]
	(
		[PageDefinitionId],
		[ScriptId],
		[CustomAttributes]
	)
	SELECT @Id,
		[ScriptId],
		[CustomAttributes]
	FROM [dbo].[SIPageScript] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageStyle]
	(
		[PageDefinitionId],
		[StyleId],
		[CustomAttributes]
	)
	SELECT @Id,
		[StyleId],
		[CustomAttributes]
	FROM [dbo].[SIPageStyle] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM COTaxonomyObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[COTaxonomyObject]
	(
		[Id],
		[TaxonomyId],
		[ObjectId],
		[ObjectTypeId],
		[SortOrder],
		[Status],
		[CreatedBy],
		[CreatedDate],
		[ModifiedBy],
		[ModifiedDate]
	)
	SELECT NEWID(),
		[TaxonomyId],
		@Id,
		[ObjectTypeId],
		[SortOrder],
		[Status],
		@ModifiedBy,
		@UtcNow,
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[COTaxonomyObject] OT
	WHERE OT.ObjectId = @SourceId

	DECLARE @VersionId uniqueidentifier, @SourceVersionId uniqueidentifier, 
		@VersionNumber int, @RevisionNumber int, @VersionStatus int
	SET @VersionId = NEWID()
	SELECT TOP 1 @SourceVersionId = Id FROM VEVersion 
	WHERE ObjectId = @SourceId ORDER BY CreatedDate DESC	

	SELECT TOP 1 @VersionNumber = VersionNumber, @RevisionNumber = RevisionNumber FROM VEVersion
	WHERE ObjectId = @Id ORDER BY CreatedDate DESC

	IF @Publish = 1
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0) + 1
		SET @VersionNumber = 0
		SET @VersionStatus = 1
	END
	ELSE
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0)
		SET @VersionNumber = ISNULL(@VersionNumber, 0) + 1
		SET @VersionStatus = 0
	END

	INSERT INTO [dbo].[VEVersion]
	(
		[Id],
		[ApplicationId],
		[ObjectTypeId],
		[ObjectId],
		[CreatedDate],
		[CreatedBy],
		[VersionNumber],
		[RevisionNumber],
		[Status],
		[ObjectStatus],
		[VersionStatus],
		[XMLString],
		[Comments],
		[TriggeredObjectId],
		[TriggeredObjectTypeId]
	)          
	SELECT TOP 1 @VersionId,
		@TargetSiteId,
		8, 
		@Id, 
		@UtcNow,
		@ModifiedBy,
		@VersionNumber,
		@RevisionNumber,
		@VersionStatus,
		ObjectStatus,
		VersionStatus, 
		dbo.UpdatePageXml(XMLString, @Id, P.WorkflowId, P.WorkflowState, P.PageStatus, 1, 
			P.CreatedBy, P.CreatedDate, P.AuthorId, P.SiteId, P.SourcePageDefinitionId, @PageMapNodeId), 
		'Imported from master',
		TriggeredObjectId,
		TriggeredObjectTypeId       
	FROM VEVersion V, 
		PageDefinition P
	WHERE V.Id = @SourceVersionId
		AND P.PageDefinitionId = @Id

	IF @Publish = 1
		UPDATE VEVersion SET Status = 1 WHERE ObjectId = @Id

	DECLARE @ZonesXml xml, @UnmanagedXml xml
	SELECT @ZonesXml = ZonesXml, @UnmanagedXml = UnmanagedXml FROM PageDefinitionSegment WHERE PageDefinitionId = @Id

	INSERT INTO [dbo].[VEPageDefSegmentVersion]
	(
		[Id],
		[VersionId],
		[PageDefinitionId],
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],  
		[ContainerContentXML],        
		[UnmanagedXml],
		[ZonesXml],
		[CreatedBy],
		[CreatedDate]
	)
	SELECT NEWID(),
		@VersionId,
		@Id,
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],
		CASE WHEN @Overwrite = 1 THEN [ContainerContentXML] 
			ELSE [dbo].[Page_GetContainerXml](@Id, AudienceSegmentId, DeviceId) END,
		dbo.Page_GetUnmanagedXml([UnmanagedXml], @UnmanagedXml, @TargetSiteId),
		dbo.MergePageZone([ZonesXml], @ZonesXml, @TargetSiteId),
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[VEPageDefSegmentVersion]
	WHERE VersionId = @SourceVersionId

	INSERT INTO [dbo].[VEPageContentVersion]
	(
		[Id],
		[ContentVersionId],
		[IsChanged],
		[PageDefSegmentVersionId]
	)
	SELECT NEWID(),
		ContentVersionId,
		IsChanged,
		CS.Id
	FROM [dbo].[VEPageContentVersion] PC
		JOIN VEPageDefSegmentVersion PS ON PC.PageDefSegmentVersionId = PS.Id
		JOIN VEPageDefSegmentVersion CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
			AND PS.VersionId = @SourceVersionId
			AND CS.VersionId = @VersionId

	IF @Publish = 1
	BEGIN
		DECLARE @PublishCount int
		SET @PublishCount = (SELECT ISNULL(PublishCount, 0) + 1 FROM PageDefinition WHERE PageDefinitionId = @Id)

		UPDATE PageDefinition SET
			PublishCount = @PublishCount,
			PublishDate = @UtcNow,
			PageStatus = 8
		WHERE PageDefinitionId = @Id

		DECLARE @WorkflowId uniqueidentifier
		SELECT TOP 1 @WorkflowId = WorkflowId FROM WFWorkflowExecution WHERE ObjectId = @Id AND Completed = 0

		IF @WorkflowId IS NOT NULL
		BEGIN
			EXEC WorkflowExecution_Save
				@WorkflowId = @WorkflowId,
				@SequenceId = @EmptyGuid,
				@ObjectId = @Id,
				@ObjectTypeId = 8,
				@ActorId = @ModifiedBy,
				@ActorType = 1,
				@ActionId = 8,
				@Remarks = N'Page imported from master',
				@Completed = 1
		END

		DELETE
		FROM	PageObjectReferences
		WHERE	PageId = @Id

		INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
			SELECT	NEWID(), @Id, ObjectId, ObjectType, ObjectSiteId
			FROM	PageObjectReferences
			WHERE	PageId = @SourceId
	END

	IF @InvalidateCache = 1
	BEGIN
		EXEC PageDto_Invalidate @SiteId = @TargetSiteId, @ModifiedBy = @ModifiedBy
	END
END
GO
PRINT 'Creating vwSiteMap'
GO
IF(OBJECT_ID('vwSiteMap') IS NOT NULL)
	DROP VIEW vwSiteMap
GO
CREATE VIEW [dbo].[vwSiteMap] --WITH SCHEMABINDING     
AS    
     
SELECT TargetId AS PageId,     
	PageMapNodeId AS MenuId,     
	PageMapNodeId AS ObjectId,     
	ParentId AS ParentId,     
	2 AS ObjectType,    
	LOWER(CompleteFriendlyUrl) AS Url,    
	LOWER(CompleteFriendlyUrl) AS DraftUrl,    
	SiteId,    
	CreatedDate,    
	ModifiedDate AS PublishDate,    
	1 AS PublishCount,    
	CASE WHEN MenuStatus = 0 THEN 1 ELSE 0 END AS IsLive,    
	ExcludeFromSiteMap AS ExcludeFromSearch,
	1 AS IsConnected       
FROM dbo.PageMapNode     
WHERE LocationIdentifier IS NULL OR LocationIdentifier = ''    
     
UNION    
     
SELECT P.PageDefinitionId AS PageId,     
	PD.PageMapNodeId AS MenuId,    
	P.PageDefinitionId AS ObjectId,     
	PD.PageMapNodeId AS ParentId,     
	8 AS ObjectType,    
	LOWER(ISNULL(N.CompleteFriendlyUrl, N.CalculatedUrl) + '/' + P.FriendlyName) AS Url,    
	LOWER(ISNULL(N.CompleteFriendlyUrl, N.CalculatedUrl) + '/' + V.FriendlyName) AS DraftUrl,    
	P.SiteId,    
	P.CreatedDate,    
	P.PublishDate,    
	P.PublishCount,    
	CASE WHEN P.PageStatus = 8 AND N.MenuStatus = 0 THEN 1 ELSE 0 END AS IsLive,    
	CASE WHEN P.ExcludeFromExternalSearch = 1 OR N.ExcludeFromSiteMap = 1 THEN 1 ELSE 0 END AS ExcludeFromSearch,
	CASE WHEN N.TargetId = P.PageDefinitionId THEN 1 ELSE 0 END AS IsConnected      
FROM dbo.PageMapNodePageDef PD    
	JOIN dbo.PageDefinition P ON PD.PageDefinitionId = P.PageDefinitionId    
	JOIN dbo.PageMapNode N ON N.PageMapNodeId = PD.PageMapNodeId    
	LEFT JOIN dbo.VEPageVersion V ON PD.PageDefinitionId = V.PageDefinitionId    
WHERE LocationIdentifier IS NULL OR LocationIdentifier = '' 

UNION

SELECT BlogListPageId AS PageId,       
	N.PageMapNodeId AS MenuId,      
	BlogListPageId AS ObjectId,       
	N.PageMapNodeId AS ParentId,       
	40 AS ObjectType,      
	LOWER( N.CompleteFriendlyUrl + '/' + PageDef.FriendlyName+'/'+ CAST(datepart(year,P.PostDate) AS VARCHAR(10)) +'/'+  LEFT(CONVERT(CHAR(20), P.PostDate, 101), 2)+'/'+P.FriendlyName) AS Url,      
	LOWER( N.CompleteFriendlyUrl + '/' + PageDef.FriendlyName+'/'+ CAST(datepart(year,P.PostDate) AS VARCHAR(10)) +'/'+  LEFT(CONVERT(CHAR(20), P.PostDate, 101), 2) +'/'+P.FriendlyName) AS DraftUrl,      
	PageDef.SiteId,      
	P.CreatedDate,      
	P.PublishDate,      
	P.PublishCount,      
	CASE WHEN PageDef.PageStatus = 8 AND N.MenuStatus = 0 THEN 1 ELSE 0 END AS IsLive,      
	CASE WHEN PageDef.ExcludeFromExternalSearch = 1 OR N.ExcludeFromSiteMap = 1 THEN 1 ELSE 0 END AS ExcludeFromSearch,
	1 AS IsConnected      
FROM BLBlog B 
	JOIN BLPost P ON B.Id = P.BlogId 
	--JOIN vwCompletefriendlyurl U ON B.BlogDetailPageId = U.PageId
	JOIN PageMapNodePageDef PD  ON PD.PageDefinitionId = BlogListPageId
	JOIN dbo.PageDefinition PageDef ON PD.PageDefinitionId = PageDef.PageDefinitionId  
	JOIN dbo.PageMapNode N ON N.PageMapNodeId = PD.PageMapNodeId
WHERE ContentDefinitionId IS NOT NULL AND P.Status = 8
GO
PRINT 'Creating vwPageAttributeValue'
GO
IF(OBJECT_ID('vwPageAttributeValue') IS NOT NULL)
	DROP VIEW vwPageAttributeValue
GO
CREATE VIEW [dbo].[vwPageAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V

	UNION ALL

	SELECT 1 AS vRank,
		V.Id,
		P.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V
		JOIN PageDefinition P ON V.PageDefinitionId = P.SourcePageDefinitionId
	WHERE V.IsShared = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT C.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	ISNULL(V.Notes, C.Notes) AS Notes,
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATPageAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.PageDefinitionId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1
GO
PRINT 'Creating vwSiteAttributeValue'
GO
IF(OBJECT_ID('vwSiteAttributeValue') IS NOT NULL)
	DROP VIEW vwSiteAttributeValue
GO
CREATE VIEW [dbo].[vwSiteAttributeValue]
AS
WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.SiteId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V

	UNION ALL

	SELECT 1 AS vRank,
		V.Id,
		C.Id AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V
		JOIN SISite C ON V.SiteId = C.ParentSiteId
	WHERE V.IsShared = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT C.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.SiteId,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATSiteAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.SiteId
		AND C.SiteId = V.SiteId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1
GO
IF(OBJECT_ID('SiteDto_Get') IS NOT NULL)
	DROP PROCEDURE  SiteDto_Get
GO	
CREATE PROCEDURE [dbo].[SiteDto_Get]
(
	@Id						uniqueidentifier = NULL,
	@Ids					nvarchar(max)=NULL,
	@MasterSiteId			uniqueidentifier = NULL,
	@IncludeVariantSites	bit = NULL,
	@CurrentMasterSiteOnly	bit = NULL,
	@AllowTranslation		bit = NULL,
	@CampaignId				uniqueidentifier = NULL,
	@SiteListId				uniqueidentifier = NULL,
	@ParentSiteId			uniqueidentifier = NULL,
	@UserId					uniqueidentifier = NULL,
	@SiteId					uniqueidentifier = NULL,
	@Status					int = NULL,
	@PageNumber				int = NULL,
	@PageSize				int = NULL,
	@MaxRecords				int = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@Keyword				nvarchar(MAX) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50), 
		@IdsExist bit, @LftValue int, @RgtValue int
	SET @PageLowerBound = @PageSize * @PageNumber

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @IncludeVariantSites IS NULL SET @IncludeVariantSites = 1
	IF @Status IS NULL SET @Status = 1
	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'	

	IF @MasterSiteId IS NULL AND @CurrentMasterSiteOnly = 1
		SELECT TOP 1 @MasterSiteId = MasterSiteId FROM SISite WHERE Id = @SiteId
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbSiteIds TABLE (Id uniqueidentifier)
	
	IF @Ids IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT Items FROM dbo.SplitGUID(@Ids, ',') 
	END
	IF @SiteListId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM SISiteListSite WHERE SiteListId = @SiteListId
	END
	
	IF @CampaignId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		DECLARE @CampaignSiteId uniqueidentifier
		SELECT TOP 1 @CampaignSiteId = ApplicationId FROM MKCampaign WHERE Id = @CampaignId
		IF @CampaignSiteId = @SiteId SET @CampaignSiteId = NULL

		INSERT INTO @tbSiteIds
		SELECT SiteId FROM MKCampaignRunHistory H
		WHERE EXISTS (SELECT 1 FROM MKCampaignSend S WHERE H.CampaignSendId = S.Id 
				AND CampaignId = @CampaignId AND (@CampaignSiteId IS NULL OR SiteId = @SiteId))
	END

	IF @UserId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM [dbo].[GetAccessibleSitesForUser](@UserId)
	END
	ELSE IF @ParentSiteId IS NOT NULL AND @ParentSiteId != dbo.GetEmptyGUID()
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM [dbo].[GetChildrenSites](@ParentSiteId, 1)
	END
	
	INSERT INTO @tbIds
	SELECT S.Id AS Id, ROW_NUMBER() OVER (ORDER BY
							CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
							CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
							CASE WHEN @SortClause = 'CREATEDDATE ASC' THEN CreatedDate END ASC,
							CASE WHEN @SortClause = 'CREATEDDATE DESC' THEN CreatedDate END DESC,
							CASE WHEN @SortClause = 'MODIFIEDDATE ASC' THEN ISNULL(ModifiedDate, CreatedDate) END ASC,
							CASE WHEN @SortClause = 'MODIFIEDDATE DESC' THEN ISNULL(ModifiedDate, CreatedDate) END DESC,
							CASE WHEN @SortClause IS NULL THEN CreatedDate END ASC	
						) AS RowNumber
	FROM SISite AS S
	WHERE (@Id IS NULL OR S.Id = @Id) AND
		(@AllowTranslation IS NULL OR S.SubmitToTranslation = @AllowTranslation) AND
		(@Status IS NULL OR S.Status = @Status) AND
		(@MasterSiteId IS NULL OR S.Id = @MasterSiteId OR S.MasterSiteId = @MasterSiteId) AND
		(@IncludeVariantSites = 1 OR S.MasterSiteId = S.Id OR S.MasterSiteId IS NULL) AND
		(@IdsExist IS NULL OR EXISTS (Select 1 FROM @tbSiteIds T Where T.Id = S.Id)) AND
		(@Keyword IS NULL OR (S.Title like @Keyword OR S.Description like @Keyword OR S.PrimarySiteUrl like @Keyword OR S.ExternalCode like @Keyword))
		
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				S.Id AS Id
		FROM SISite S
			JOIN @tbIds T ON T.Id = S.Id
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT		S.*,
				S.SubmitToTranslation AS AllowTranslation,
				S.DefaultTranslateToLanguage AS TranslationLocale,
				SiteHierarchyPath AS SitePath,
				T.RowNumber,
				T.TotalRecords
	FROM SISite AS S
		JOIN @tbPagedResults AS T ON T.Id = S.Id
	ORDER BY RowNumber

	SELECT V.*, A.Title AS AttributeTitle
	FROM ATSiteAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbPagedResults T ON V.SiteId = T.Id
END
GO
PRINT 'Creating UrlDto_Get'
GO
IF(OBJECT_ID('UrlDto_Get') IS NOT NULL)
	DROP PROCEDURE  UrlDto_Get
GO	
CREATE PROCEDURE [dbo].[UrlDto_Get]
(
	@Url			nvarchar(500)	= NULL,
	@SiteId			uniqueidentifier = NULL,
	@ObjectType		int = NULL,
	@IsLive			int = NULL
)
AS
BEGIN
	SELECT *
	FROM vwSiteMap
	WHERE (@Url IS NULL OR Url like @Url)
			AND (@SiteId IS NULL OR SiteId = @SiteId)
			AND (@ObjectType IS NULL OR ObjectType = @ObjectType)
			AND (@IsLive IS NULL OR IsLive = @IsLive)
			AND Url IS NOT NULL
END
GO
PRINT 'Creating ObjectUrlDto_GetById'
GO
IF(OBJECT_ID('ObjectUrlDto_GetById') IS NOT NULL)
	DROP PROCEDURE  ObjectUrlDto_GetById
GO	
CREATE PROCEDURE [dbo].[ObjectUrlDto_GetById]
(
	@Urls			TYObjectUrlById READONLY,
	@SiteId			UNIQUEIDENTIFIER,
	@PreferSource	BIT
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ObjectUrls TABLE (
		TokenSiteId		UNIQUEIDENTIFIER,
		TokenObjectId	UNIQUEIDENTIFIER,
		TokenObjectType	INT,
		SiteId			UNIQUEIDENTIFIER,
		ObjectId		UNIQUEIDENTIFIER,
		ObjectType		INT,
		[Url]			NVARCHAR(2048)
	)

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType = 2)
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Menus AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Menus AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType = 2
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType = 8)
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU.SiteId, OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU.ObjectId, OU2.ObjectId, OU1.ObjectId,U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU.[Url], OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Pages AS OU ON (
						OU.SiteId = @SiteId AND
						OU.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Pages AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Pages AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType = 8
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType IN (9, 33))
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Files AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Files AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType IN (9, 33)
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType IN (39, 40))
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Blogs AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Blogs AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType IN (39, 40)
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType = 205)
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.SiteId, U.SiteId) ELSE COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) END AS SiteId,
					CASE @PreferSource WHEN 1 THEN COALESCE(OU1.ObjectId, U.ObjectId) ELSE COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) END AS ObjectId,
					U.ObjectType,
					CASE @PreferSource WHEN 1 THEN OU1.[Url] ELSE COALESCE(OU2.[Url], OU1.[Url]) END AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Products AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Products AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType = 205
	END

	SELECT	TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url]
	FROM	@ObjectUrls
END
GO
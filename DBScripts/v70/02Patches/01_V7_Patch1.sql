GO
PRINT 'Updating the version in the productsuite table'
GO
UPDATE iAppsProductSuite SET UpdateStartDate = GETUTCDATE(), ProductVersion='7.0.0204.0101'
GO

PRINT 'Add SEOTitle column to PRProduct'
GO

IF(COL_LENGTH('PRProduct', 'SEOTitle') IS NULL)
	ALTER TABLE PRProduct ADD SEOTitle NVARCHAR(255) NULL
GO

PRINT 'Add SEOTitle column to PRProductVariant'
GO

IF(COL_LENGTH('PRProductVariant', 'SEOTitle') IS NULL)
	ALTER TABLE PRProductVariant ADD SEOTitle NVARCHAR(255) NULL
GO

PRINT 'Add SEOTitle column to PRProductVariantCache'
GO

IF(COL_LENGTH('PRProductVariantCache', 'SEOTitle') IS NULL)
	ALTER TABLE PRProductVariantCache ADD SEOTitle NVARCHAR(255) NULL
GO


PRINT 'Creating BLD_ProductImport_ProductAttributeVariant'
GO
IF(OBJECT_ID('BLD_ProductImport_ProductAttributeVariant') IS NOT NULL)
	DROP PROCEDURE  BLD_ProductImport_ProductAttributeVariant
GO	
CREATE PROCEDURE [dbo].[BLD_ProductImport_ProductAttributeVariant]
(
	@SiteId					uniqueidentifier,
	@ProductIdUser			nvarchar(50),
	@AttributeTitle			nvarchar(4000),
	@AttributeCategoryName	nvarchar(2000),
	@Sku					nvarchar(255),
	@IsSkuLevel				bit,
	@Value					nvarchar(max),
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	begin try
		
		declare @MasterProductId			uniqueidentifier,
				@MasterProductAttributeId	uniqueidentifier,
				@MasterAttributeId			uniqueidentifier,
				@MasterAttributeEnumId		uniqueidentifier,
				@MasterSkuId				uniqueidentifier,
				@MasterValue				nvarchar(max),
				@NumbericValue				int = iif(@Value like 'true', 1, iif(@Value like 'false', 0, null)),
				@IsSkipped					bit = 1,
				@Output						varchar(512),
				@IsEnum bit

		if @SiteId is null or @ProductIdUser is null or @AttributeTitle is null or @AttributeCategoryName is null or (@IsSkuLevel=1 and @Sku is null)
			raiserror(15600, -1, -1, 'SiteId, ProductIdUser, AttributeTitle, AttributeCategoryName, or Sku is null')

		begin tran
		select
			@MasterProductId=p.Id,
			@MasterProductAttributeId=pa.Id,
			@MasterAttributeId=pa.AttributeId,
			@MasterAttributeEnumId = e.Id,
			@MasterSkuId=s.Id,
			@IsEnum = a.IsEnum
		from PRProductAttribute pa
			join PRProduct p on pa.ProductId=p.Id
			left join PRProductSKU s on p.Id=s.ProductId and s.SKU=@Sku
			join ATAttribute a on pa.AttributeId=a.Id
			left join ATAttributeVariant av on a.Id=av.Id and av.SiteId=@SiteId
			join ATAttributeCategoryItem i on a.Id=i.AttributeId
			join ATAttributeCategory c on i.CategoryId=c.Id and c.Name=@AttributeCategoryName
			left join ATAttributeEnum e on a.Id=e.AttributeID
			left join ATAttributeEnumVariant ev on e.Id=ev.Id and ev.SiteId=@SiteId
		where
			p.ProductIDUser like @ProductIdUser and
			pa.IsSKULevel = isnull(@IsSkuLevel, 0) and
			((a.IsEnum=1 and isnull(ev.Title, e.Title) like @Value and e.Status = 1) or
			(a.IsEnum=0 and isnull(av.Title, a.Title) like @AttributeTitle))

		if @MasterProductId is null
		begin
			set @Output = concat(
				'Could not find product attribute for ProductId=', @ProductIdUser, 
				' | sku=', @Sku,
				' | category=', @AttributeCategoryName, 
				' | attribute title=', @AttributeTitle,
				' | value=', @Value)
			raiserror(15600, -1, -1, @Output)
		end

		if @IsSkuLevel=1
		begin
			declare @MasterProductSkuAttributeValueId	uniqueidentifier,
					@ProductSkuAttributeValueVariantId	uniqueidentifier

			select 
				@MasterProductSkuAttributeValueId=m.Id, 
				@MasterValue=iif(v.Value is null, m.Value, null),
				@ProductSkuAttributeValueVariantId=v.Id
			from PRProductSkuAttributeValue m
				left join PRProductSkuAttributeValueVariant v on m.Id=v.Id and v.SiteId=@SiteId
				left join ATAttributeEnum ae on m.attributeId = ae.attributeId
				left join ATAttributeEnumVariant aev on m.attributeId = aev.attributeId
			where m.SKUId=@MasterSkuId and m.AttributeId=@MasterAttributeId and ProductAttributeId=@MasterProductAttributeId
			      AND ((@IsEnum = 1 and (ae.Title = @Value or aev.Title = @Value) and m.AttributeEnumId = ae.id) or @IsEnum=0)

			if @MasterProductSkuAttributeValueId is null
			begin
				set @Output = concat('Unable to find PRProductSkuAttributeValue with sku/attribute/productattribute/isenum/value/siteid: ', @MasterSkuId, '|', @MasterAttributeId, '|', @MasterProductAttributeId, '|', @IsEnum, '|', @Value, '|', @SiteId)
				raiserror(15600, -1, -1, @Output)
			end
			
			select @Value = case when @MasterValue is null or @Value <> @MasterValue and @Value is not null then @Value else null end

			if @ProductSkuAttributeValueVariantId is not null
			begin
				update PRProductSkuAttributeValueVariant set
					Value=@Value,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getdate()
				where Id=@ProductSkuAttributeValueVariantId and SiteId=@SiteId

				set @Output = concat('updated PRProductSkuAttributeValueVariant id=', @MasterProductSkuAttributeValueId, ' value=', @Value)
			end
			else
			begin
				if @Value is not null
				begin
					insert into PRProductSkuAttributeValueVariant (Id, SiteId, SkuId, AttributeId, AttributeEnumId, Value, ModifiedBy, ModifiedDate)
						values (@MasterProductSkuAttributeValueId, @SiteId, @MasterSkuId, @MasterAttributeId, @MasterAttributeEnumId, @Value, @ModifiedBy, getdate())
					set @IsSkipped = 0
				end
				set @Output = concat(
					'Sku level attribute value variant ', 
					iif(@IsSkipped=1, 'skipped', 'inserted '), 
					' id=', @ProductIdUser, 
					' | sku', @Sku,
					' | category', @AttributeCategoryName, 
					' | attribute', @AttributeTitle, 
					' | value', @Value)
			end
		end
		else
		begin
			--Product Level Attribute
			declare @MasterProductAttributeValueId	uniqueidentifier,
					@ProductAttributeValueVariantId	uniqueidentifier

			select 
				@MasterProductAttributeValueId=m.Id, 
				@MasterValue=iif(v.Value is null, m.Value, null), 
				@ProductAttributeValueVariantId=v.Id
			from PRProductAttributeValue m
				left join PRProductAttributeValueVariant v on m.Id=v.Id and v.SiteId=@SiteId
				left join ATAttributeEnum ae on m.attributeId = ae.attributeId
				left join ATAttributeEnumVariant aev on m.attributeId = aev.attributeId
			where m.ProductId=@MasterProductId and m.AttributeId=@MasterAttributeId and ProductAttributeId=@MasterProductAttributeId
			      AND ((@IsEnum = 1 and (ae.Title = @Value or aev.Title = @Value) and m.AttributeEnumId = ae.id) or @IsEnum=0)

			if @MasterProductAttributeValueId is null
			begin
				set @Output = concat('Unable to find PRProductAttributeValue with product/attribute/productattribute/isenum/value/siteid: ', @MasterProductId, '|', @MasterAttributeId, '|', @MasterProductAttributeId, '|', @IsEnum, '|', @Value, '|', @SiteId)
				raiserror(15600, -1, -1, @Output)
			end

			select @Value = case when (@MasterValue is null or @Value <> @MasterValue) and @Value is not null then @Value else null end
			if @ProductAttributeValueVariantId is not null
			begin
				update PRProductAttributeValueVariant set 
					Value = @Value, 
					ModifiedBy = @ModifiedBy, 
					ModifiedDate = getdate()
				where Id=@ProductAttributeValueVariantId and SiteId=@SiteId
				set @Output = concat('updated PRProductAttributeValueVariant id=', @ProductAttributeValueVariantId, ' value=', @Value, ' mastervalue=', @MasterValue)
			end
			else
			begin
				if @Value is not null
				begin
					insert into PRProductAttributeValueVariant (Id, SiteId, ProductId, AttributeId, AttributeEnumId, Value, ModifiedBy, ModifiedDate) 
						values (@MasterProductAttributeValueId, @SiteId, @MasterProductId, @MasterAttributeId, @MasterAttributeEnumId, @Value, @ModifiedBy, getdate())
					set @IsSkipped = 0
				end
				
				set @Output = concat(
					'Product level attribute value variant ', 
					iif(@IsSkipped=1, 'skipped', 'inserted '), 
					'id=', @ProductIdUser, 
					' | category=', @AttributeCategoryName, 
					' | attribute', @AttributeTitle, 
					' | value', @Value)
			end
		end

		commit tran
		select @MasterProductAttributeValueId, @MasterProductSkuAttributeValueId, @Output
	end try
	begin catch
		rollback tran
		DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE(),
				@ErrorState int				= ERROR_STATE(),
				@ErrorSeverity int			= ERROR_SEVERITY()
        RAISERROR(@ErrorMessage,@ErrorSeverity, @ErrorState)

	end catch
END
GO
PRINT 'Creating ProductImage_Save'
GO
IF(OBJECT_ID('ProductImage_Save') IS NOT NULL)
	DROP PROCEDURE  ProductImage_Save
GO	
CREATE PROCEDURE [dbo].[ProductImage_Save]
(
	@Id [uniqueidentifier] out,
	@Description [nvarchar](4000),
	@Title [nvarchar](256),
	@FileName [nvarchar](256),
	@Size [int],
	@Attributes [nvarchar](256),
	@RelativePath [nvarchar](4000),
	@AltText [nvarchar](1024),
	@Height [int],
	@Width [int],
	@Status [int],
	@ModifiedBy [uniqueidentifier],
	@ObjectId [uniqueidentifier],
	@ObjectType [int] ,
	@IsPrimary [bit] ,
	@Sequence [int]=null,
	@ParentImage [uniqueidentifier],
	@ImageNumber [int],
	@ApplicationId [uniqueidentifier],
	@ImageType [uniqueidentifier]=null ,
	@ProductSKUs xml = null
	)
	
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @error	  int,
		@Now dateTime


BEGIN
--********************************************************************************
-- code
--********************************************************************************

	/* IF @Id specified, ensure exists */
   IF (@Id is not null)
       IF((SELECT count(*) FROM  CLImage  WITH (NOLOCK) 
					WHERE Id = @Id AND Status != 3 And SiteId=@ApplicationId) = 0)
       BEGIN
			RAISERROR('NOTEXISTS||Id', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
       END

	SET @Status= dbo.GetActiveStatus()
	SET @Now = GetUTCDate()
	IF (@Id is null)			-- New INSERT
	BEGIN
		SET @Id = newid();

		
		Insert into  CLImage([Id],
							[Description],
							[Title],
							[FileName],
							[Size],
							[Attributes],
							[RelativePath],
							[AltText],
							[Height],
							[Width],
							[ParentImage],
							[ImageType],
							[Status],
							[CreatedDate],
							[CreatedBy],
							[SiteId])
		Values				(@Id,
							@Description,
							@Title,
							@FileName,
							@Size,
							@Attributes,
							@RelativePath,
							@AltText,
							@Height,
							@Width,
							@ParentImage,
							@ImageType,
							@Status,
							@now,
							@ModifiedBy,
							@ApplicationId)

	END
	ELSE
	BEGIN
			UPDATE CLImage  WITH (ROWLOCK)
			SET [Description]=@Description,
							[FileName]=@FileName,
							[Title] = @Title,
							[Size]=@Size,
							[Attributes]=@Attributes,
							[RelativePath]=@RelativePath,
							[AltText]=@AltText,
							[Height]=@Height,
							[Width]=@Width,
							[ParentImage]= @ParentImage,
							[ImageType]=@ImageType,
							[Status]=@Status,
							[ModifiedDate]=@Now,
							[ModifiedBy]=@ModifiedBy,
							[SiteId]=@ApplicationId
			Where Id=@Id	
	END

IF (@ObjectId Is not null)			
BEGIN 
		IF Exists (Select 1 from CLObjectImage Where ObjectId=@ObjectId AND ObjectType =@ObjectType AND ImageId=@Id)
		BEGIN
			Update CLObjectImage  WITH (ROWLOCK) SET IsPrimary =@IsPrimary,Sequence =@Sequence 
			Where ObjectId=@ObjectId AND ObjectType =@ObjectType AND ImageId=@Id
		END			
		ELSE
		BEGIN 
			Select @Sequence = Isnull(max(Sequence),0) + 1 
					from CLObjectImage  WITH (NOLOCK)  where ObjectId= @ObjectId
			INSERT INTO CLObjectImage(Id,ObjectId,ObjectType,ImageId,IsPrimary,Sequence, SiteId)
			VALUES(newId(),@ObjectId,@ObjectType,@Id,@IsPrimary,@Sequence, @ApplicationId)

		END

		Print Cast(@ProductSKUs as varchar(max))
		IF(@ProductSKUs is not null and cast(@ProductSKUs as nvarchar(max))<> '<SkuCollection/>')
		BEGIN
			Delete from CLObjectImage where 
						ImageId = @Id and ObjectId in (Select Id from PRProductSKU where ProductId = @ObjectId)

				INSERT INTO CLObjectImage(Id,ObjectId,ObjectType,ImageId,IsPrimary,Sequence, SiteId)
				SELECT newId(),tab.col.value('(text())[1]','uniqueidentifier'),
						206,@Id,@IsPrimary,@Sequence, @ApplicationId 
				FROM @ProductSKUs.nodes('SkuCollection/Sku') tab(col)
		END 
		ELSE
		BEGIN
			--Sankar (02/17/2014)(TFSID:485):This can be achieved in the above condition itself. Just to make sure not to introduce any other issues
			DELETE FROM CLObjectImage WHERE ImageId = @Id AND ObjectId in (Select Id from PRProductSKU where ProductId = @ObjectId) 
		END
END

Select @Id

END
GO

PRINT 'Updating UseRealTimeShippingRate setting'
GO
Update STSettingType Set IsVisible = 1 where Name = 'UseRealTimeShippingRate'
GO

print 'altering BLD_ExportProducts'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportProducts')
    drop procedure BLD_ExportProducts
go	

CREATE PROCEDURE [dbo].[BLD_ExportProducts]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		BEGIN
			select 
				 p.ProductIDUser as ProductId
				,p.Title as ProductName
				,cast(p.ProductTypeID as varchar(max)) as ProductTypeId
				,pt.Title as ProductType
				,p.ShortDescription
				,p.Description
				,p.LongDescription
				,p.IsActive
				,0 as UpdateFlag
				,coalesce(SEOTitle, '') as 'SEO Title'
				,coalesce(SEODescription, '') as 'SEO Description'
				,coalesce(SEOKeywords, '') as 'SEO Keywords'
				,coalesce(SEOFriendlyUrl, '') as 'SEO Friendly URL'
				,p.IsShared
			from dbo.PRProduct p
			join dbo.PRProductType pt on (p.ProductTypeID = pt.Id)
			order by p.Title
		END
	ELSE
		BEGIN
			select
				 p.ProductIDUser as ProductId
				,ISNULL(prvc.Title, p.Title) as ProductName
				,cast(p.ProductTypeID as varchar(max)) as ProductTypeId
				,pt.Title as ProductType
				,ISNULL(prvc.ShortDescription ,p.ShortDescription) as ShortDescription
				,ISNULL(prvc.Description ,p.Description) as Description
				,ISNULL(prvc.LongDescription, p.LongDescription) as LongDescription
				,ISNULL(prvc.IsActive, p.IsActive) as IsActive
				,0 as UpdateFlag
				,coalesce(prvc.SEOTitle, p.SEOTitle) as [SEO Title]
				,coalesce(prvc.SEODescription, p.SEODescription) as 'SEO Description'
				,coalesce(prvc.SEOKeywords, p.SEOKeywords) as 'SEO Keywords'
				,coalesce(prvc.SEOFriendlyUrl, p.SEOFriendlyUrl) as 'SEO Friendly URL'
				,p.IsShared
			from dbo.PRProductVariantCache prvc
			left join dbo.PRProduct p on (prvc.Id = p.Id)
			left join dbo.PRProductType pt on (p.ProductTypeID = pt.Id)
			where prvc.SiteId = @siteId
			order by p.Title
		END
END
GO

print 'altering BLD_ProductImport_Product'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_Product')
    drop procedure BLD_ProductImport_Product
go	
CREATE PROCEDURE [dbo].[BLD_ProductImport_Product]
	@ProductIDUser nvarchar(max),
	@ProductType nvarchar(max) = null,
	@UrlFriendlyTitle nvarchar(max) = null,
	@Title nvarchar(max) = null,
	@ShortDescription nvarchar(max) = null,
	@Description nvarchar(max) = null,
	@LongDescription nvarchar(max) = null,
	@SEODescription nvarchar(max) = null,
	@SEOKeywords nvarchar(max) = null,
	@SEOTitle nvarchar(max) = null,
	@SEOH1 nvarchar(max) = null,
	@SEOFriendlyURL nvarchar(max) = null,
	@IsActive bit = null,
	@IsOnline bit = null,
	@IsBundle bit = null,
	@TaxExempt bit = null,
	@OperatorId uniqueidentifier = null,
	@ProductIDUserOld nvarchar(max) = null,
	@ProductIdGuid uniqueidentifier = null,
	@IsShared bit
AS
BEGIN
	SET NOCOUNT ON;

	declare @actionType int = 0

	BEGIN TRY
        BEGIN TRAN

		if @OperatorId is null
		begin
			set @OperatorId = (select top 1 CreatedBy from PRProductType where CreatedBy is not null);
		end
		

		declare @ProductId uniqueidentifier;
		declare @IsNewProduct bit
		declare @ProductTypeId uniqueidentifier;
		declare @TaxCategoryId uniqueidentifier;
		Set @IsNewProduct = 1

		If (@ProductIdGuid IS NOT NULL)
			SET @ProductId = @ProductIdGuid

		-- TODO: fix this since title may not be unique
		select @ProductTypeId = Id from PRProductType where Title like @ProductType

		IF @ProductId IS NULL
		BEGIN 
			if @ProductIDUserOld is null
			begin
		
				select @ProductId = Id FROM PRProduct where ProductIDUser like @ProductIDUser;
			
			end else
			begin
				select @ProductId = Id FROM PRProduct where ProductIDUser like @ProductIDUserOld;
			end
		END

		IF EXISTS(Select 1 From PRProduct where Id = @ProductId)
			SET @IsNewProduct = 0

		--INSERTING THE PRODUCT
		if @IsNewProduct = 1
		begin
		
			if(@TaxExempt=1)
			begin
				select @TaxCategoryId = Id from PRTaxCategory where Title like '%non%';
			end else
			begin
				select @TaxCategoryId = Id from PRTaxCategory where Title not like '%non%';
			end


			if(@ProductTypeId is null)RAISERROR ('Unknown product type!', 16, 1);
			if(@TaxCategoryId is null)RAISERROR ('Unknown tax category!', 16, 1);
		
			IF(@ProductId IS NULL)
				SET @ProductId = newid();

			insert into PRProduct (
				Id, 
				ProductIdUser, 
				Title, 
				UrlFriendlyTitle, 
				ShortDescription, 
				LongDescription, 
				Description, 
				KeyWord, 
				IsActive, 
				ProductTypeId, 
				ProductStyle, 
				SiteId, 
				CreatedDate, 
				CreatedBy, 
				ModifiedDate, 
				ModifiedBy, 
				Status, 
				IsBundle, 
				BundleCompositionLastModified, 
				TaxCategoryId,
				TaxExempt,
				SEOTitle,
				SEOH1, 
				SEODescription, 
				SEOKeywords, 
				SEOFriendlyUrl,
				IsShared)
			values (
				@ProductId, 
				@ProductIDUser, 
				@Title, 
				@UrlFriendlyTitle, 
				@ShortDescription, 
				@LongDescription, 
				@Description, 
				'', 
				@IsActive, 
				@ProductTypeId, 
				null, 
				'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', 
				GETDATE(), 
				@OperatorId, 
				null, 
				null, 
				1, 
				@IsBundle, 
				null, 
				@TaxCategoryId, 
				@TaxExempt, 
				@SEOTitle,
				@SEOH1,
				@SEODescription, 
				@SEOKeywords, 
				@SEOFriendlyURL,
				@IsShared);

			set @actionType = 1

		end else
		begin
		
			update PRProduct set
				ProductIDUser = @ProductIDUser,
				Title = isnull(@Title, Title),
				UrlFriendlyTitle = isnull(@UrlFriendlyTitle, UrlFriendlyTitle),
				ShortDescription = isnull(@ShortDescription, ShortDescription),
				LongDescription = isnull(@LongDescription, LongDescription),
				Description = isnull(@Description, Description),
				-- Keyword = isnull(@Keyword, Keyword),
				IsActive = isnull(@IsActive, IsActive),
				ProductTypeID = isnull(@ProductTypeId, ProductTypeID),
				ModifiedDate = GETDATE(),
				ModifiedBy = isnull(@OperatorId, ModifiedBy),
				IsBundle = isnull(@IsBundle, IsBundle),
				TaxCategoryID = isnull(@TaxCategoryID, TaxCategoryID),
				TaxExempt = isnull(@TaxExempt, TaxExempt),
				SEOTitle = @SEOTitle,
				SEOH1 = @SEOH1,
				SEODescription = @SEODescription,
				SEOKeywords = @SEOKeywords,
				SEOFriendlyURL = @SEOFriendlyURL,
				IsShared = @IsShared
			where Id = @ProductId;

			set @actionType = 2
			
		end

		COMMIT TRAN

		select @actionType, @ProductId

    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX)
        SET @ErrorMessage = ERROR_MESSAGE()
        RAISERROR(@ErrorMessage,16, 1)
    END CATCH

END
GO

print 'altering BLD_ProductImport_ProductVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_ProductVariant')
    drop procedure BLD_ProductImport_ProductVariant
go

CREATE PROCEDURE [dbo].[BLD_ProductImport_ProductVariant]
	@SiteId				uniqueidentifier,
	@ProductIDUser		nvarchar(100),
	@ProductTypeName	nvarchar(50),
	@ProductTypeId		uniqueidentifier,
	@Title				nvarchar(555),
	@ShortDescription	nvarchar(4000),
	@Description	nvarchar(4000),
	@LongDescription	nvarchar(4000),
	@IsActive			bit,
	@SEOTitle			nvarchar(255),
	@SEODescription		nvarchar(4000),
	@SEOKeywords		nvarchar(4000),
	@SEOFriendlyURL		nvarchar(4000),
	@URLFriendlyTitle	nvarchar(255),
	@ModifiedBy			uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	declare @ActionType				int					= 0
	declare @MasterSiteId			uniqueidentifier
	declare @MasterProductId		uniqueidentifier
	declare @MasterProductTypeId	uniqueidentifier
	declare @MasterTitle			nvarchar(555)
	declare @MasterShortDescription nvarchar(4000)
	declare @MasterDescription nvarchar(4000)
	declare @MasterLongDescription	nvarchar(4000)
	declare @MasterIsActive			bit
	declare @MasterSEOTitle			nvarchar(255)
	declare @MasterSEODescription	nvarchar(4000)
	declare @MasterSEOKeywords		nvarchar(4000)
	declare @MasterSEOFriendlyURL	nvarchar(255)
	declare @MasterURLFriendlyTitle	nvarchar(255)
	declare @IsSkipped				bit = 1
	declare @Output					nvarchar(512)

	if coalesce(cast(@SiteId as sql_variant), @ProductIdUser, @ProductTypeId) is null
		raiserror(15600, -1, -1, 'SiteId, ProductIdUser, or ProductTypeId is null')

	BEGIN TRY
        BEGIN TRAN
		
		select @MasterSiteId=Id from SISite where Id=MasterSiteId

		select 
			@MasterProductId=p.Id, 
			@MasterProductTypeId=p.ProductTypeID,
			@MasterTitle=iif(v.Title is null, p.Title, null),
			@MasterShortDescription=iif(v.ShortDescription is null, p.ShortDescription, null),
			@MasterDescription=iif(v.Description is null, p.Description, null),
			@MasterLongDescription=iif(v.LongDescription is null, p.LongDescription, null),
			@MasterIsActive=iif(v.IsActive is null, p.IsActive, null),
			@MasterSEOTitle=iif(v.SEOH1 is null, p.SEOH1, null),
			@MasterSEODescription=iif(v.SEODescription is null, p.SEODescription, null),
			@MasterSEOKeywords=iif(v.SEOKeywords is null, p.SEOKeywords, null),
			@MasterSEOFriendlyURL=iif(v.SEOFriendlyUrl is null, p.SEOFriendlyUrl, null),
			@MasterURLFriendlyTitle=iif(v.UrlFriendlyTitle is null, p.UrlFriendlyTitle, null)
		from PRProduct p
			left join PRProductVariant v on p.Id=v.Id and v.SiteId=@SiteId
		where p.ProductIDUser like @ProductIDUser and p.ProductTypeID like @ProductTypeId and p.SiteId=@MasterSiteId

		if @MasterProductId is null or @MasterProductTypeId is null
		begin
			set @Output = concat('Product Id: [', @ProductIDUser, ']/[', @ProductTypeId, '] does not exist.')
			raiserror(15600, -1, -1, @Output)
		end
		else
		begin
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@ShortDescription = case when @MasterShortDescription is null or @ShortDescription <> @MasterShortDescription and @ShortDescription is not null then @ShortDescription else null end,
				@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end,
				@LongDescription = case when @MasterLongDescription is null or @LongDescription <> @MasterLongDescription and @LongDescription is not null then @LongDescription else null end,
				@IsActive = case when @MasterIsActive is null or @IsActive <> @MasterIsActive and @IsActive is not null then @IsActive else null end,
				@SEOTitle = case when @MasterSEOTitle is null or @SEOTitle <> @MasterSEOTitle and @SEOTitle is not null then @SEOTitle else null end,
				@SEODescription = case when @MasterSEODescription is null or @SEODescription <> @MasterSEODescription and @SEODescription is not null then @SEODescription else null end,
				@SEOKeywords = case when @MasterSEOKeywords is null or @SEOKeywords <> @MasterSEOKeywords and @SEOKeywords is not null then @SEOKeywords else null end,
				@SEOFriendlyURL = case when @MasterSEOFriendlyURL is null or @SEOFriendlyURL <> @MasterSEOFriendlyURL and @SEOFriendlyURL is not null then @SEOFriendlyURL else null end,
				@URLFriendlyTitle = case when @MasterURLFriendlyTitle is null or @URLFriendlyTitle <> @MasterURLFriendlyTitle and @URLFriendlyTitle is not null then @URLFriendlyTitle else null end

			if exists (select 1 from PRProductVariant where Id=@MasterProductId and SiteId=@SiteId)
			begin
				update PRProductVariant set
					Title=@Title,
					ShortDescription=@ShortDescription,
					LongDescription=@LongDescription,
					Description=@Description,
					IsActive=@IsActive,
					SEOH1=@SEOTitle,
					SEODescription=@SEODescription,
					SEOKeywords=@SEOKeywords,
					SEOFriendlyURL=@SEOFriendlyURL,
					UrlFriendlyTitle=@URLFriendlyTitle,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getdate()
				where Id=@MasterProductId and SiteId=@SiteId

				set @Output = concat('updated PRProductVariant id=', @MasterProductId)
				set @actionType = 2
			end
			else
			begin
				if coalesce(cast(@Title as sql_variant), @ShortDescription, @LongDescription, @IsActive, @SEOTitle, @SEODescription, @SEOKeywords, @SEOFriendlyURL) is not null
				begin
					insert into PRProductVariant (SiteId, Id, ProductTypeId, Title, ShortDescription, LongDescription, Description, IsActive, SEOH1, SEODescription, SEOKeywords, SEOFriendlyURL, URLFriendlyTitle,  ModifiedBy, ModifiedDate)
						values (@SiteId, @MasterProductId, @MasterProductTypeId, @Title, @ShortDescription, @LongDescription, @Description, @IsActive, @SEOTitle, @SEODescription, @SEOKeywords, @SEOFriendlyURL, @URLFriendlyTitle, @Modifiedby, getdate())
					set @IsSkipped = 0
				end

				set @Output = concat(
					'PRProductVariant ', 
					iif(
						@IsSkipped=1, 
						'skipped', 
						concat('inserted Name=', @Title, ' | product type=', @ProductTypeName)))
				set @actionType = 2
			end

		end
		COMMIT TRAN
		select @MasterProductid, @ActionType, @Output
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		declare @ErrorState int = ERROR_STATE()
		declare @ErrorSeverity int = ERROR_SEVERITY()
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
    END CATCH

END
go

print 'adding Description column BLD_Product'
GO
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME='BLD_Product') and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME='BLD_Product' AND COLUMN_NAME in ('Description')))
begin
	alter table BLD_Product add Description nvarchar(max) not null default('N/A')
end
GO


PRINT 'Creating vwProductAndSKUAttributeValue'
GO
IF(OBJECT_ID('vwProductAndSKUAttributeValue') IS NOT NULL)
	DROP VIEW vwProductAndSKUAttributeValue
GO	
CREATE VIEW [dbo].[vwProductAndSKUAttributeValue]
AS
SELECT     
	P.Id AS Id,  P.Title AS ProductName, VP.AttributeId, 
	A.Title AS AttributeName, Isnull(AE.Value,VP.Value ) AS Value  
	FROM vwProductAttributeValue VP
	INNER JOIN PRProduct P ON P.Id = VP.ObjectId
	INNER JOIN ATAttribute A ON A.Id = VP.AttributeId
	LEFT JOIN ATAttributeEnum AS AE ON VP.AttributeEnumId = AE.Id

UNION 

SELECT
	P.Id AS Id,  P.Title AS ProductName, VP.AttributeId, 
	A.Title AS AttributeName, Isnull(AE.Value,VP.Value ) AS Value  
	FROM vwSkuAttributeValue VP
	INNER JOIN PRProductSKU S ON S.Id=VP.ObjectId
	INNER JOIN PRProduct P ON P.Id = S.ProductId
	INNER JOIN ATAttribute A ON A.Id = VP.AttributeId
	LEFT JOIN ATAttributeEnum AS AE ON VP.AttributeEnumId = AE.Id

GO

PRINT 'Creating vwProductImage'
GO
IF(OBJECT_ID('vwProductImage') IS NOT NULL)
	DROP VIEW vwProductImage
GO
CREATE VIEW [dbo].[vwProductImage]
AS
WITH CTE AS
(
	SELECT Id, SiteId, Title, AltText, Description, Size, Status, ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy  
	FROM CLImage

	UNION ALL

	SELECT Id, SiteId, Title, AltText, Description, NULL, Status, ModifiedDate, ModifiedBy
	FROM CLImageVariantCache
)

SELECT I2.Id,
	C.Title,
	I2.FileName,
	C.AltText,
	C.Description,
	C.Size,
	OI.Sequence,
	P.Id AS ProductId,
	P.Title AS ProductName,
	I.CreatedBy,
	I.CreatedDate,
	C.ModifiedBy,
	C.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName,
	P.ProductTypeId,
	'/productimages/' + CAST(P.Id AS varchar(36)) + '/images' AS RelativePath,
	C.SiteId,
	I.SiteId AS SourceSiteId,
	IT.TypeNumber,
	C.Status,
	I2.ParentImage,
	I.IsShared
FROM PRProduct P
	JOIN CLObjectImage OI ON OI.ObjectId = P.Id
	JOIN CTE C ON OI.ImageId = C.Id --AND OI.SiteId = C.SiteId
	JOIN CLImage I ON I.Id = C.Id
	JOIN CLImage I2 ON I.Id = I2.ParentImage OR I.Id = I2.Id
	JOIN CLImageType IT ON IT.Id = I2.ImageType
	LEFT JOIN VW_UserFullName FN on FN.UserId = I.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
 WHERE C.SiteId = I.SiteId or (C.SiteId != I.SiteId and I.IsShared = 1)

 GO
 PRINT 'Creating vwSkuImage'
GO
IF(OBJECT_ID('vwSkuImage') IS NOT NULL)
	DROP VIEW vwSkuImage
GO

CREATE VIEW [dbo].[vwSkuImage]      
AS      
WITH CTE AS      
(      
 SELECT Id, SiteId, Title, AltText, Description, Size, Status, ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy        
 FROM CLImage      
      
 UNION ALL      
      
 SELECT Id, SiteId, Title, AltText, Description, NULL, Status, ModifiedDate, ModifiedBy      
 FROM CLImageVariantCache      
)      

      
SELECT I2.Id,     
 C.Title,      
 I2.FileName,      
 C.AltText,      
 C.Description,      
 C.Size,      
 OI.Sequence,      
 P.Id AS ProductId,      
 P.Title AS ProductName,      
 I.CreatedBy,      
 I.CreatedDate,      
 C.ModifiedBy,      
 C.ModifiedDate,      
 FN.UserFullName CreatedByFullName,      
 MN.UserFullName ModifiedByFullName,      
 P.ProductTypeId,      
 '/productimages/' + CAST(P.Id AS varchar(36)) + '/images' AS RelativePath,      
 C.SiteId,      
 I.SiteId AS SourceSiteId,      
 IT.TypeNumber,      
 C.Status,      
 I.ParentImage,      
 I.IsShared,      
 PS.Id AS SkuId,
 PS.SKU AS Sku  
FROM PRProduct P  
 JOIN CLObjectImage OI ON (OI.ObjectId = P.Id  AND OI.ObjectType = 205)
 LEFT JOIN CLObjectImage OI2 ON (OI2.ImageId =OI.ImageId  AND OI2.ObjectType = 206) 
 JOIN PRProductSKU PS ON PS.ProductId = P.Id 
 JOIN CTE C ON OI.ImageId = C.Id 
 JOIN CLImage I ON I.Id = C.Id      
 JOIN CLImage I2 ON I.Id = I2.ParentImage OR I.Id = I2.Id      
 JOIN CLImageType IT ON IT.Id = I2.ImageType      
 LEFT JOIN VW_UserFullName FN on FN.UserId = I.CreatedBy      
 LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy 
 WHERE OI2.ObjectId = PS.Id OR OI2.ObjectId IS NULL
 GO


PRINT 'Creating Search_ReIndex'
GO
IF(OBJECT_ID('Search_ReIndex') IS NOT NULL)
	DROP PROCEDURE  Search_ReIndex
GO	
CREATE PROC [dbo].[Search_ReIndex]
(
	@ObjectType int,
	@SiteId uniqueidentifier
)
AS
BEGIN

	If(@ObjectType = 8)
	BEGIN
	-- Page

	Declare @ExcludeNodes Table (PageMapNodeId uniqueIdentifier)
	INSERT INTO @ExcludeNodes 
	SELECT  Cast(Value as Uniqueidentifier) from STSettingType T
		INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
		where Name in ('ProductTypePageMapNodeId','CampaignPageMapNodeId')
		AND SiteId=@SiteId And Value is not null AND rtrim(ltrim(Value)) !=''
	
	Delete from SEIndex where ObjectType = 8 and SiteId = @SiteId
	INSERT INTO [dbo].[SEIndex]
           ([Id]
		   ,[Title]
		   ,[DynamicObjectId]	
           ,[Url]
           ,[ObjectType]
           ,[SiteId]
           ,[Processed]
           ,[ProcessedDateTime]
           ,[ErrorMessage])
		   Select distinct P.PageDefinitionId, P.Title, null, null, 8, P.SiteId, 1, null, null  
		   from PageDefinition P
		   INNER JOIN PageMapNodePageDef PDM ON P.PageDefinitionId = PDM.PageDefinitionId
		   INNER JOIN PageMapNode PM ON PDM.PageMapNodeId = PM.PageMapNodeId
		   where PublishCount > 0 and (PageStatus = 1 OR PageStatus = 8) 
		   and MenuStatus = 0 --Visible
		   and P.SiteId = @SiteId 
		   --and PM.PageMapNodeId Not in (Select PageMapNodeId from @ExcludeNodes) TODO: Revisite the logic of exlcuding page in commerce
	END
	
	If(@ObjectType = 9)
	BEGIN
	-- File
	Delete from SEIndex where ObjectType = 9 and SiteId = @SiteId
	INSERT INTO [dbo].[SEIndex]
           ([Id]
		   ,[Title]
		   ,[DynamicObjectId]
           ,[Url]
           ,[ObjectType]
           ,[SiteId]
           ,[Processed]
           ,[ProcessedDateTime]
           ,[ErrorMessage])
		   Select C.Id, C.Title, null, null, 9, C.ApplicationId, 1, null, null from COFile F
			INNER JOIN COContent C ON F.ContentId = C.Id
			where ObjectTypeId = 9 and Status = 1 and C.ApplicationId = @SiteId 
		   
	END

	If(@ObjectType = 40)
	BEGIN
	-- POst
	Delete from SEIndex where ObjectType = 40 and SiteId = @SiteId
	INSERT INTO [dbo].[SEIndex]
           ([Id]
		   ,[Title]
		   ,[DynamicObjectId]
           ,[Url]
           ,[ObjectType]
           ,[SiteId]
           ,[Processed]
           ,[ProcessedDateTime]
           ,[ErrorMessage])
		   Select P.Id, P.Title, null, null, 40, P.ApplicationId, 1, null, null 
		   from BLPost P
			where Status = 8 and P.ApplicationId = @SiteId 
		
	END

	If(@ObjectType = 36)
	BEGIN	
	--Product 
	Delete from SEIndex where ObjectType = 36 and SiteId = @SiteId
	INSERT INTO [dbo].[SEIndex]
           ([Id]
		   ,[Title]
		   ,[DynamicObjectId]
           ,[Url]
           ,[ObjectType]
           ,[SiteId]
           ,[Processed]
           ,[ProcessedDateTime]
           ,[ErrorMessage])
		   Select Distinct P.Id, P.Title, null, null,  36, P.SiteId, 1, NULL, NULL 
		   from vwProduct P
			INNER JOIN vwSKU S ON P.Id = S.ProductId and S.SiteId = @SiteId
			where P.SiteId = @SiteId 
			AND P.IsActive = 1 AND (S.IsActive = 1 and S.IsOnline = 1)

	END
END
GO

Print 'Updating Bulk index count' 
Update SESearchSetting Set Value = 10 where Title = 'Search.BulkIndexCount'
GO

GO
DECLARE @Sequence int
SELECT @Sequence = MAX(sequence) + 1 FROM dbo.STSettingType
IF NOT EXISTS(SELECT 1 from STSettingType WHERE Name = 'DateFormat.UseSiteTimeZone')
BEGIN
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, IsEnabled, ControlType, IsVisible)
	VALUES('DateFormat.UseSiteTimeZone', 'Display all dates in site timezone', 1, @sequence, 1, 'Checkbox', 1)

	Update STSettingType SET SettingGroupId = (SELECT TOP 1 Id FROM STSettingGroup WHERE Name = 'General')
	WHERE Name = 'DateFormat.UseSiteTimeZone'
END
GO

IF(COL_LENGTH('USUserPaymentInfo', 'SiteId') IS NULL)
 ALTER TABLE USUserPaymentInfo ADD SiteId uniqueidentifier
GO
IF(COL_LENGTH('PTGiftCardInfo', 'SiteId') IS NULL)
 ALTER TABLE PTGiftCardInfo ADD SiteId uniqueidentifier
GO
IF(COL_LENGTH('PTLineOfCreditInfo', 'SiteId') IS NULL)
 ALTER TABLE PTLineOfCreditInfo ADD SiteId uniqueidentifier
GO
IF(COL_LENGTH('PTCODInfo', 'SiteId') IS NULL)
 ALTER TABLE PTCODInfo ADD SiteId uniqueidentifier
GO

IF(OBJECT_ID('CustomerPaymentInfo_Save') IS NOT NULL)
	DROP PROCEDURE  CustomerPaymentInfo_Save
GO
CREATE PROCEDURE [dbo].[CustomerPaymentInfo_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@Status				int=null,
	@CardType			uniqueidentifier,
    @Number				varchar(max),
	@ExpirationMonth	int,
    @ExpirationYear		int,
    @NameOnCard			varchar(255),
    @BillingAddressId	uniqueidentifier,
    @Sequence			int,
    @UserId				uniqueidentifier,
    @IsPrimary			bit,
	@ApplicationId		uniqueidentifier,
	@ExternalProfileId	nvarchar(255)=null,
	@Nickname			nvarchar(255)=null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@stmt	  varchar(256),
		@rowcount	int,
		@PaymentInfoId uniqueidentifier

Begin
--********************************************************************************
-- code
--********************************************************************************


	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	DECLARE @NewCustomerPayment BIT = CASE WHEN @Id IS NULL OR @Id = dbo.GetEmptyGUID() OR NOT EXISTS (SELECT Id FROM USUserPaymentInfo WHERE Id = @Id) THEN 1 ELSE 0 END

	IF @Id IS NULL OR @Id = dbo.GetEmptyGUID()
		SET @Id = NEWID()
       
    IF @IsPrimary = 1
	BEGIN
		UPDATE [dbo].[USUserPaymentInfo]
		SET IsPrimary = 0 WHERE UserId = @UserId
	END
	
	SET @ModifiedDate = GetUTCDate()
	
		-- open the symmetric key 
		OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
		OPEN SYMMETRIC KEY [key_DataShare] 
			DECRYPTION BY CERTIFICATE cert_keyProtection;

		IF @NewCustomerPayment = 1	-- new insert
		BEGIN
			SET @stmt = 'Payment Insert'
			Declare @pId uniqueidentifier
			SET @pId =NewId();

			INSERT INTO [dbo].[PTPaymentInfo]
			   ([Id]
			   ,[CardType]
			   ,[Number]
			   ,[ExpirationMonth]
			   ,[ExpirationYear]
			   ,[NameOnCard]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[Status]
			   ,[ModifiedBy]
			   ,[ModifiedDate])
		 VALUES
			   (@pId
			   ,@CardType
			   ,EncryptByKey(Key_Guid('key_DataShare'), @Number)
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS Varchar(2)))
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS varchar(4)))
			   ,@NameOnCard
			   ,@ModifiedBy
			   ,@ModifiedDate
			   ,@Status
			   ,@ModifiedBy
			   ,@ModifiedDate)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
			INSERT INTO [dbo].[USUserPaymentInfo]
			([Id]
			,[UserId]
			,[PaymentInfoId]
			,[BillingAddressId]
			,[Sequence]
			,[IsPrimary]
			,[CreatedBy]
			,[CreatedDate]
			,[Status]
			,[ModifiedDate]
			,[ModifiedBy]
			,[ExternalProfileId]
			,[Nickname]
			,[SiteId])
			VALUES
			(@Id
			,@UserId
			,@pId
			,@BillingAddressId
			,@Sequence
			,@IsPrimary
			,@ModifiedBy
			,@ModifiedDate
			,@Status
			,@ModifiedDate
			,@ModifiedBy
			,@ExternalProfileId
			,@Nickname
			,@ApplicationId)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
        END
        ELSE			-- update
        BEGIN
			SET @stmt = 'Payment Update'
			
			Select @PaymentInfoId=PaymentInfoId  FROM [dbo].[USUserPaymentInfo]
			Where Id=@Id

			UPDATE [dbo].[USUserPaymentInfo] WITH (ROWLOCK)
			SET [PaymentInfoId] = @PaymentInfoId
			  ,[BillingAddressId] = @BillingAddressId
			  ,[Sequence] = @Sequence
			  ,[IsPrimary] = @IsPrimary
			  ,[Status] = @Status
			  ,[ModifiedDate] = @ModifiedDate
			  ,[ModifiedBy] =@ModifiedBy
			  ,[ExternalProfileId]=@ExternalProfileId
			  ,[Nickname]=@Nickname
			  ,[SiteId]=@ApplicationId
			WHERE Id=@Id

			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
			  RAISERROR('DBERROR||%s',16,1,@stmt)
			  RETURN dbo.GetDataBaseErrorCode()
			END

			SET @rowcount= 0	

			UPDATE [dbo].[PTPaymentInfo]
			SET [CardType] = @CardType
				,[Number]=CASE WHEN (@Number is null or len(@Number)<=0) then Number else (EncryptByKey(Key_Guid('key_DataShare'), @Number )) end
				,[ExpirationMonth] = CASE WHEN (@ExpirationMonth is null or len(@ExpirationMonth)<=0) then ExpirationMonth else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS VARCHAR(2)) )) end
				,[ExpirationYear]= CASE WHEN (@ExpirationYear is null or len(@ExpirationMonth)<=0) then ExpirationYear else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS VARCHAR(4)) )) end
				,[NameOnCard] = @NameOnCard
				,[Status] = @Status
				,[ModifiedBy] = @ModifiedBy
				,[ModifiedDate] = @ModifiedDate
			WHERE Id=@PaymentInfoId 
					
			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
				RAISERROR('DBERROR||%s',16,1,@stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END


	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END


GO

IF(OBJECT_ID('CustomerPaymentInfoDto_Get') IS NOT NULL)
	DROP PROCEDURE  CustomerPaymentInfoDto_Get
GO
CREATE PROCEDURE [dbo].[CustomerPaymentInfoDto_Get]
(
	@Id					uniqueidentifier = null,
	@CustomerId			uniqueidentifier = null,
	@SiteId				uniqueidentifier,
	@PaymentType        int = null
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	WITH CTE AS
	(
	SELECT UP.Id,  
		UP.UserId AS CustomerId,  
		1 AS PaymentType,
		UP.NickName,
		UP.IsPrimary,
		UP.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		CCT.Title AS CreditCardType,  
		P.NameOnCard AS CreditCardName,
		CONVERT(varchar(max), DecryptByKey(P.Number)) AS CreditCardNumber,  
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,  
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) AS CreditCardExpirationYear,  
		UP.ExternalProfileId AS CreditCardExternalProfileId,
		NULL AS GiftCardNumber,
		NULL AS GiftCardBalance,
		NULL AS AccountNumber,
		UP.CreatedDate, 
		UP.CreatedBy AS CreatedById,
		UP.ModifiedDate, 
		UP.ModifiedBy AS ModifiedById
	FROM PTPaymentInfo P
		LEFT JOIN USUserPaymentInfo UP ON P.Id = UP.PaymentInfoId  
		LEFT JOIN PTCreditCardType AS CCT ON CCT.Id = P.CardType
		LEFT JOIN GLAddress A ON A.Id = UP.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR UP.Id = @Id)
		AND (@CustomerId IS NULL OR UP.UserId = @CustomerId) 
		AND UP.[Status] = 1
		AND (UP.SiteId IS NULL OR UP.SiteId=@SiteId)

	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		5 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		P.GiftCardNumber,
		P.BalanceAfterLastTransaction AS GiftCardBalance,
		NULL AS AccountNumber,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTGiftCardInfo P  
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 5 AND Status = 1 AND SiteId = @SiteId)
		AND (P.SiteId IS NULL OR P.SiteId=@SiteId)
	
	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		2 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		U.AccountNumber,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTLineOfCreditInfo P  
		INNER JOIN USCommerceUserProfile U ON U.Id = P.CustomerId
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 2 AND Status = 1 AND SiteId = @SiteId)
		AND (P.SiteId IS NULL OR P.SiteId=@SiteId)
	
	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		4 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTCODInfo P  
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 4 AND Status = 1 AND SiteId = @SiteId)
		AND (P.SiteId IS NULL OR P.SiteId=@SiteId)
	)

	SELECT Id,  
		CustomerId,  
		PaymentType,
		NickName,
		IsPrimary,
		BillingAddressId,
		BillingAddressLine1,
		BillingAddressLine2,
		BillingAddressLine3,
		BillingCity,
		BillingStateId,
		BillingZip,
		BillingCountryId,
		BillingPhone,
		BillingStateName,
		BillingStateCode,
		BillingCountryName,
		BillingCountryCode,
		PaymentTypeInfoId,
		CreditCardType,  
		CreditCardName,
		CreditCardNumber,  
		CreditCardExpirationMonth,  
		CreditCardExpirationYear,  
		CreditCardExternalProfileId,
		GiftCardNumber,
		GiftCardBalance,
		AccountNumber,
		CreatedDate, 
		CreatedById,
		ModifiedDate, 
		ModifiedById
	FROM CTE
	WHERE PaymentType = ISNULL(@PaymentType, PaymentType)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO

PRINT 'Creating vwInventoryList'
GO
IF(OBJECT_ID('vwInventoryList') IS NOT NULL)
	DROP VIEW vwInventoryList
GO
CREATE VIEW vwInventoryList
AS
SELECT I.[Id],
    I.[WarehouseId],
    I.[SKUId],
    I.[Quantity],
    I.[LowQuantityAlert],
    I.[FirstTimeOrderMinimum],
    I.[OrderMinimum],
    I.[OrderIncrement],
    I.[ReasonId],
    I.[ReasonDetail],
    I.[Status],
    I.[CreatedBy],
    I.[CreatedDate],
    I.[ModifiedBy],
    I.[ModifiedDate],
	S.ProductId,
	W.Title AS WarehouseName,
	AQ.Quantity AS AllocatedQuantity,
	BAQ.BackOrderQuantity AS BackOrderAllocatedQuantity,
	ATS.BackOrderLimit,
	ATS.Quantity AS AvailableToSell,
	ATS.SiteId
FROM INInventory I
	INNER JOIN INWarehouse W ON W.Id = I.WarehouseId
	INNER JOIN PRProductSKU S ON S.Id = I.SKUId
	LEFT JOIN dbo.vwAllocatedQuantityperWarehouse AQ ON I.SKUId = AQ.ProductSKUId AND AQ.WarehouseId = W.Id
	LEFT JOIN dbo.vwBackOrderAllocatedQuantityPerWarehouse BAQ ON I.SKUId = BAQ.ProductSKUId AND BAQ.WarehouseId = W.Id
	LEFT JOIN dbo.vwAvailableToSellPerWarehouse ATS ON I.SKUId = ATS.SKUId AND ATS.WarehouseId = W.Id
WHERE W.Status = 1
GO

PRINT 'Creating Procedure CouponCode_Get'
GO
IF(OBJECT_ID('CouponCode_Get') IS NOT NULL)
	DROP PROCEDURE  CouponCode_Get
GO

CREATE PROCEDURE [dbo].[CouponCode_Get]
    (
      @Code XML = NULL ,
      @CouponId XML = NULL ,
      @Id XML = NULL ,
      @OrderId XML = NULL ,
      @Keyword NVARCHAR(255) = NULL ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    )
AS 
    DECLARE @CouponCodeId TABLE
        (
          [Id] UNIQUEIDENTIFIER,
		  [CouponId]  UNIQUEIDENTIFIER,
          [OrderId] UNIQUEIDENTIFIER NULL
        )
	
    BEGIN
        IF @Id IS NULL 
            BEGIN      
            
                IF @Code IS NOT NULL 
                    BEGIN          
                        INSERT  INTO @CouponCodeId(Id,CouponId,OrderId)
                                SELECT  CC.Id,CC.CouponId,NULL
                                FROM    dbo.CPCouponCode CC
                                        INNER JOIN @Code.nodes('/GenericCollectionOfString/string') tab ( col ) ON CC.Code = tab.col.value('text()[1]',
                                                              'nvarchar(256)')
                                                              
                                                             
                    END
                ELSE 
                    IF @CouponId IS NOT NULL 
                        BEGIN
                        
						
                            INSERT  INTO @CouponCodeId(Id,CouponId,OrderId)
                                    SELECT  CC.Id,CC.CouponId,NULL
                                    FROM    dbo.CPCouponCode CC
                                            INNER JOIN @CouponId.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON CC.CouponId = tab.col.value('text()[1]',
                                                              'uniqueidentifier')
                                    WHERE   @Keyword IS NULL
                                            OR CC.Code LIKE '%' + @Keyword
                                            + '%'

                     
                        
                        END
                        
                    ELSE 
                        IF @OrderId IS NOT NULL 
                            BEGIN
                        
                                INSERT  INTO @CouponCodeId(Id,CouponId,OrderId)
                                        SELECT  CC.CouponCodeId,C.CouponId,CC.OrderId
                                        FROM    dbo.CPOrderCouponCode CC
                                                INNER JOIN @OrderId.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON CC.OrderId = tab.col.value('text()[1]',
                                                              'uniqueidentifier')
												INNER JOIN CPCouponCode C ON CC.CouponCodeId =C.Id
                       
                        
                            END
                        ELSE 
                            IF @Keyword IS NOT NULL 
                                BEGIN
                        
                                    INSERT  INTO @CouponCodeId(Id,CouponId)
                                            SELECT  CC.Id,CC.CouponId
                                            FROM    dbo.CPCoupon C
                                                    INNER JOIN CPCouponCode CC ON CC.CouponId = C.ID
                                            WHERE   CC.Code LIKE '%'
                                                    + @Keyword + '%'
                                                    OR C.Title LIKE '%'
                                                    + @Keyword + '%'
                       
                        
                                END
                
            END
                        
        ELSE 
            BEGIN
                INSERT  INTO @CouponCodeId(Id,CouponId,OrderId)
                        SELECT  tab.col.value('text()[1]', 'uniqueidentifier'),CouponId,null
                        FROM    @Id.nodes('GenericCollectionOfGuid/guid') tab ( col ) 
						INNER JOIN CPCouponCode CC ON CC.Id = tab.col.value('text()[1]', 'uniqueidentifier')
            END
            
            
            
            

        SELECT CC.Id ,
                CC.Code ,
                CC.CouponId ,
                CC.CreatedBy ,
                CC.CreatedDate ,
                CC.Status ,
                dbo.GetUsedCouponCodeCount(CC.ID) AS Uses
        FROM    @CouponCodeId CCI
                INNER JOIN CPCouponCode CC ON CCI.Id = CC.Id
                INNER JOIN CPCoupon C ON C.Id = CC.CouponId
        WHERE  (@ApplicationId is null or C.SiteId = @ApplicationId)
		



        SELECT  
                C.[ID] ,
                C.[Title] ,
                C.[Description] ,
                C.CreatedDate AS CreatedDate ,
                C.[CreatedBy] ,
                C.ModifiedDate AS ModifiedDate ,
                C.[ModifiedBy] ,
                C.[Status] ,
                C.[IsGenerated] ,
                C.[AutoApply] ,
                C.[MaxUses] ,
                C.[MaxUsesPerCustomer] ,
                C.[CouponTypeId] ,
                dbo.ConvertTimeFromUtc(C.[StartDate], @ApplicationId) StartDate ,
                dbo.ConvertTimeFromUtc(C.[EndDate], @ApplicationId) EndDate ,
                C.[CouponValue] ,
                C.[MaxValue] ,
                C.[MinPurchase] ,
                C.[ShippingOptionId] ,
                dbo.GetUsedCouponCount(C.ID) AS Used ,
                C.Stackable ,
                C.RestrictionTypeId ,
                dbo.GetCouponAttachedId(C.Id, C.RestrictionTypeId) AS AttachedIds ,
                C.MaxUsesPerCode ,
                C.AppliedSequence,
				C.RefundRestricted,
				C.DiscountAmountIsTaxable,
				TV.AppliedDiscount
        FROM    CPCoupon C
				--INNER JOIN CPCouponCode CD ON C.Id= CD.CouponId
				LEFT JOIN (SELECT CCI.CouponId  CouponId, isnull(sum(OIC.Amount),0) AppliedDiscount
							FROM @CouponCodeId CCI
							INNER JOIN  OROrderItemCouponCode OIC  ON OIC.CouponCodeId =CCI.Id
							INNER JOIN OROrderItem OI on OIC.OrderItemId = OI.Id AND CCI.OrderId =OI.OrderId 
							AND OI.Status!=3
							Group By CCI.CouponId				
                           ) TV ON TV.CouponID = C.Id
		WHERE C.Id IN (Select CouponId FROM @CouponCodeId) and (@ApplicationId is null or C.SiteId = @ApplicationId)



    END
	GO
	
PRINT 'Creating Procedure PaymentDto_Get'
GO
IF(OBJECT_ID('PaymentDto_Get') IS NOT NULL)
	DROP PROCEDURE  PaymentDto_Get
GO
CREATE PROCEDURE [dbo].[PaymentDto_Get]
(		
	@Id						UNIQUEIDENTIFIER = NULL,
	@OrderId				UNIQUEIDENTIFIER = NULL,
	@CustomerId				UNIQUEIDENTIFIER = NULL,
	@ProcessorTransactionId	NVARCHAR(256) = NULL,
	@PaymentTypeId			INT = NULL,
	@PaymentStatusId		INT = NULL,
	@IsRefund				BIT = NULL,
	@StartDate				DATETIME = NULL,
	@EndDate				DATETIME = NULL,
	@PageNumber				INT = NULL,
	@PageSize				INT = NULL,
	@SiteId					UNIQUEIDENTIFIER = NULL,
	@TotalRecords			INT = NULL OUTPUT
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	DECLARE	@StartRow	INT,
			@EndRow		INT

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END

	SELECT	P.Id, OP.Id AS OrderPaymentId, OP.OrderId, OP.[Sequence], P.PaymentTypeId AS PaymentType, P.PaymentStatusId AS PaymentStatus, P.ProcessorTransactionId,
			P.IsRefund, RE.Comments AS RefundReason, P.Amount, P.CapturedAmount, COALESCE(PR.AllocatedRefundAmount, 0) AS AllocatedRefundAmount,
			O.PurchaseOrderNumber, P.CustomerId, CASE WHEN LEN(LTRIM(C.LastName)) = 0 THEN ''  ELSE C.LastName + ', ' END + C.FirstName AS CustomerName,
			CCT.Title AS CreditCardType, CCI.NameOnCard AS CreditCardName,
			CONVERT(VARCHAR(MAX), DecryptByKey(CCI.Number)) AS CreditCardNumber,
			CAST(CONVERT(VARCHAR(2), DecryptByKey(CCI.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,
			CAST(CONVERT(VARCHAR(4), DecryptByKey(CCI.ExpirationYear)) AS INT) AS CreditCardExpirationYear,
			PCC.ExternalProfileId CreditCardExternalProfileId,
			GCI.GiftCardNumber AS GiftCardNumber, GCI.BalanceAfterLastTransaction AS GiftCardBalance, U.AccountNumber,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.BillingAddressId
				WHEN 2 THEN LOC.BillingAddressId
				WHEN 3 THEN PPI.BillingAddressId
				WHEN 4 THEN CODI.BillingAddressId
				WHEN 5 THEN GCI.BillingAddressId
			END AS BillingAddressId,
			CASE P.PaymentTypeId
				WHEN 1 THEN CCI.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentInfoId,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentTypeInfoId,
			P.CreatedDate, P.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName, P.ModifiedDate, P.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName,
			ROW_NUMBER() OVER (ORDER BY P.CreatedDate Desc) AS RowNumber
	INTO	#OrderPayments
	FROM	PTOrderPayment AS OP
			INNER JOIN PTPayment AS P ON P.Id = OP.PaymentId
			INNER JOIN OROrder AS O ON O.Id = OP.OrderId AND (@SiteId IS NULL OR O.SiteId = @SiteId)
			INNER JOIN vwCustomer AS C ON C.Id = P.CustomerId
			LEFT JOIN PTRefundPayment AS RP ON RP.RefundOrderPaymentId = OP.Id
			LEFT JOIN ORRefund AS RE ON RE.Id = RP.RefundId
			LEFT JOIN (
				SELECT		RP.OrderPaymentId, SUM(RP.RefundAmount) AS AllocatedRefundAmount
				FROM		PTRefundPayment AS RP
							INNER JOIN PTOrderPayment AS ROP ON ROP.Id = RP.RefundOrderPaymentId
							INNER JOIN PTPayment AS P ON P.Id = ROP.PaymentId
				WHERE		P.PaymentStatusId NOT IN (7, 14)
				GROUP BY	RP.OrderPaymentId
			) AS PR ON PR.OrderPaymentId = OP.Id
			LEFT JOIN PTPaymentCreditCard AS PCC ON P.PaymentTypeId = 1 AND PCC.PaymentId = P.Id
			LEFT JOIN PTPaymentInfo AS CCI ON P.PaymentTypeId = 1 AND CCI.Id = PCC.PaymentInfoId
			LEFT JOIN PTCreditCardType AS CCT ON P.PaymentTypeId = 1 AND CCT.Id = CCI.CardType
			LEFT JOIN PTPaymentLineOfCredit AS PLOC ON P.PaymentTypeId = 2 AND PLOC.PaymentId = P.Id
			LEFT JOIN PTLineOfCreditInfo AS LOC ON P.PaymentTypeId = 2 AND LOC.Id = PLOC.LOCId
			LEFT JOIN USCommerceUserProfile U ON P.PaymentTypeId = 2 AND U.Id = LOC.CustomerId
			LEFT JOIN PTPaymentPaypal AS PPP ON P.PaymentTypeId = 3 AND PPP.PaymentId = P.Id
			LEFT JOIN PTPaypalInfo AS PPI ON P.PaymentTypeId = 3 AND PPI.Id = PPP.PaypalInfoId
			LEFT JOIN PTPaymentCOD AS PCOD ON P.PaymentTypeId = 4 AND PCOD.PaymentId = P.Id
			LEFT JOIN PTCODInfo AS CODI ON P.PaymentTypeId = 4 AND CODI.Id = PCOD.CODId
			LEFT JOIN PTPaymentGiftCard AS PGC ON P.PaymentTypeId = 5 AND PGC.PaymentId = P.Id
			LEFT JOIN PTGiftCardInfo AS GCI ON P.PaymentTypeId = 5 AND GCI.Id = PGC.GiftCardId
			LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy
			LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = P.ModifiedBy
	WHERE	(@Id IS NULL OR P.Id = @Id) AND
			(@OrderId IS NULL OR OP.OrderId = @OrderId) AND
			(@CustomerId IS NULL OR P.CustomerId = @CustomerId) AND
			(@ProcessorTransactionId IS NULL OR P.ProcessorTransactionId = @ProcessorTransactionId) AND
			(@PaymentTypeId IS NULL OR P.PaymentTypeId = @PaymentTypeId) AND
			(@PaymentStatusId IS NULL OR P.PaymentStatusId = @PaymentStatusId) AND
			(@IsRefund IS NULL OR P.IsRefund = @IsRefund) AND
			(@StartDate IS NULL OR P.CreatedDate >= @StartDate) AND
			(@EndDate IS NULL OR P.CreatedDate <= @EndDate) AND
			(@SiteId IS NULL OR O.SiteId = @SiteId)
	ORDER BY P.CreatedDate DESC
	OPTION (RECOMPILE)

	SELECT	*
	FROM	#OrderPayments
	WHERE	@StartRow IS NULL OR
			@EndRow IS NULL OR 
			RowNumber BETWEEN @StartRow AND @EndRow

	SET @TotalRecords = (
		SELECT	COUNT(Id)
		FROM	#OrderPayments
	)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO


PRINT 'Creating Procedure cache_RefreshProductMinEffectivePrice'
GO
IF(OBJECT_ID('cache_RefreshProductMinEffectivePrice') IS NOT NULL)
	DROP PROCEDURE  cache_RefreshProductMinEffectivePrice
GO
CREATE PROCEDURE [dbo].[cache_RefreshProductMinEffectivePrice](@ApplicationId uniqueidentifier = null)
AS
BEGIN

	Declare @DefaultGroupId uniqueidentifier
	SET @DefaultGroupId=  (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
	where STT.Name='CommerceEveryOneGroupId')

	declare @tempPriceTab table (CustomerGroupId uniqueidentifier, ProductId uniqueidentifier,
						  MinPricedSKU uniqueidentifier, MinimumEffectivePrice money)

	declare @defaultPriceTab table (CustomerGroupId uniqueidentifier, ProductId uniqueidentifier,
				MinPricedSKU uniqueidentifier, MinimumEffectivePrice money)
	print 'starting insert to @tempPriceTab'
		Insert into @tempPriceTab
		Select 	EFPS.CustomerGroupId,PSKU.ProductId, PSKU.Id as MinPricedSKUId,
			MinimumEffectivePrice=
			Case WHEN PSKU.ListPrice < IsNull(EFPS.EffectivePrice, PSKU.ListPrice) THEN PSKU.ListPrice
			ELSE IsNull(EFPS.EffectivePrice, PSKU.ListPrice)
			end
		FROM
		(
			Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
				PSC.IsSale, PSC.EffectivePrice,
				PSC.CustomerGroupId,
				(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId ORDER BY PSC.EffectivePrice ASC))  as Sno1,
				(ROW_NUMBER() OVER(PARTITION BY PSC.ProductId ORDER BY PSC.EffectivePrice ASC))  as Sno2
			from dbo.cache_FlatPriceSets PSC 
			Where 
			
				(MinQuantity <= 1 and MaxQuantity >= 1)	
		) EFPS
			Inner join PRProductSKU PSKU on EFPS.SkuId = PSKU.Id
		WHERE EFPS.Sno1=1	and EFPS.Sno2=1
		--Select Distinct ProductId from @tempPriceTab
		print 'starting insert to @defaultPriceTab'
		Insert into @defaultPriceTab
		Select @DefaultGroupId, MPSKU.ProductId, MPSKU.Id as MinPricedSKUId,
		   MinimumEffectivePrice = MPSKU.ListPrice
		FROM 
			(
				Select ProductId, Id, ListPrice,
						(ROW_NUMBER() OVER(PARTITION BY ProductId ORDER BY ListPrice ASC))  as Sno1
				from PRProductSKU 
			 ) MPSKU
		WHERE MPSKU.Sno1 =1 
	
		
	print 'starting truncate cache_ProductMinEffectivePrice '
	truncate table cache_ProductMinEffectivePrice 
	
	print 'starting insert from @defaultPriceTab'
	insert into cache_ProductMinEffectivePrice
	SELECT CustomerGroupId, ProductId, MinPricedSKU, MinimumEffectivePrice
		FROM 
			(
				Select CustomerGroupId, ProductId, MinPricedSKU, MinimumEffectivePrice,
						(ROW_NUMBER() OVER(PARTITION BY ProductId ORDER BY MinimumEffectivePrice ASC))  as Sno1
				FROM
				(select CustomerGroupId, ProductId, MinPricedSKU, MinimumEffectivePrice from @defaultPriceTab
				UNION ALL
				select CustomerGroupId, ProductId, MinPricedSKU, MinimumEffectivePrice from @tempPriceTab)T
			 ) MPSKU 
		WHERE MPSKU.Sno1 =1 

		
END
GO

PRINT 'Creating Procedure Feature_PopulateFeatureOutput'
GO
IF(OBJECT_ID('Feature_PopulateFeatureOutput') IS NOT NULL)
	DROP PROCEDURE  Feature_PopulateFeatureOutput
GO
CREATE PROCEDURE [dbo].[Feature_PopulateFeatureOutput]
(
	@ApplicationId uniqueidentifier=null
)
as
begin
	declare @NewProductsFeatureId uniqueidentifier
	declare @TopSellersFeatureId uniqueidentifier
	declare @NewProductsFeatureTypeId nvarchar(256)
	declare @TopSellersFeatureTypeId nvarchar(256)
	
	declare @PrevDays int
	declare @TopSellersCount int
	
	Set @NewProductsFeatureTypeId= (select top 1 Value from STSiteSetting SV, STSettingType ST where SV.SettingTypeId=ST.Id and ST.Name='FeatureTypeForAutoNewProducts')
	set @TopSellersFeatureTypeId	=(select top 1 Value from STSiteSetting SV, STSettingType ST where SV.SettingTypeId=ST.Id and ST.Name='FeatureTypeForTopSellers')
	set @PrevDays=(select top 1 Value from STSiteSetting SV, STSettingType ST where SV.SettingTypeId=ST.Id and ST.Name='AutoNewProductsDays')
	set @TopSellersCount = (select top 1 Value from STSiteSetting SV, STSettingType ST where SV.SettingTypeId=ST.Id and ST.Name='TopSellersCount')
	
	Set @NewProductsFeatureId= (select top 1 Id from MRFeature where FeatureTypeId=@NewProductsFeatureTypeId)
	Set @TopSellersFeatureId=  (select top 1 Id from MRFeature where FeatureTypeId=@TopSellersFeatureTypeId)
	
    
	--	Clear Previous Feature output data
	delete from MRFeatureOutput
	
	--insert records for New Products
	insert into MRFeatureOutput
	(
		FeatureId,
		ProductId
	)
	select @NewProductsFeatureId, Id from PRProduct where CreatedDate >=DateAdd(Day,@PrevDays,GetUTCDate())
	OR PromoteAsNewItem =1
	
	IF (select count(1) from MRFeatureOutput) =0
	BEGIN -- IF No NEW Products are foudn get the top last modified products
	insert into MRFeatureOutput
	(
		FeatureId,
		ProductId
	)
	select top (abs(@PrevDays)) @NewProductsFeatureId, Id from PRProduct 
	Order by CreatedDate desc
	
	END
	
	declare @TopSellerProductIds table(ProductId uniqueidentifier, SoldCount bigint)


	;with ProductSoldCount as
	(
		Select P.Id Id,sum(S.PreviousSoldCount) as SoldCount
		from PRProduct P 
		Inner Join PRProductSKU S on P.Id = S.ProductId
		Group By P.Id
	)
	insert into @TopSellerProductIds
	select Top (@TopSellersCount) Id,SoldCount 
	from ProductSoldCount
	order by SoldCount DESC
	
	--insert records for Top sellers
	insert into MRFeatureOutput
	(
		FeatureId,
		ProductId
	)		
	select  @TopSellersFeatureId,ProductId from @TopSellerProductIds where IsNull(SoldCount,0) >0
	Union
	Select   @TopSellersFeatureId,Id From PRProduct
	Where PromoteAsTopSeller =1
	--update Feature Output cache updated date 	
	update GLCacheUpdate  set FeatureOutputCacheDate=getutcdate() --where  ApplicationId= @ApplicationId
	
end
GO

PRINT 'Creating vwCustomer'
GO
IF(OBJECT_ID('vwCustomer') IS NOT NULL)
	DROP VIEW vwCustomer
GO
CREATE VIEW [dbo].[vwCustomer] 
AS         
SELECT	CUP.Id, U.FirstName, U.LastName, U.MiddleName, U.UserName, NULL AS [Password], M.Email, U.EmailNotification, U.CompanyName, U.BirthDate, U.Gender, U.LeadScore,
		CUP.IsBadCustomer, CUP.IsActive, M.IsApproved, M.IsLockedOut, CUP.IsOnMailingList, CUP.[Status], CUP.CSRSecurityQuestion, CUP.CSRSecurityPassword, U.HomePhone,
		U.MobilePhone, U.OtherPhone, U.ImageId, CUP.AccountNumber, CUP.IsExpressCustomer, CUP.ExternalId, CUP.ExternalProfileId,
		(
			SELECT	COUNT(*)
			FROM	CSCustomerLogin
			WHERE	CustomerId = CUP.Id
		) + 1 AS NumberOfLoginAccounts,
		CASE WHEN EXISTS
		(
			SELECT	Id
			FROM	PTLineOfCreditInfo
			WHERE	CustomerId =  CUP.Id AND
					[Status] <> dbo.GetDeleteStatus()
		) THEN 1 ELSE 0 END AS HasLOC,
		A.Id AS AddressId, A.AddressLine1, A.AddressLine2, A.AddressLine3, A.City, A.StateId, ST.[State], ST.StateCode,
		A.CountryId, C.CountryName, C.CountryCode, A.Zip, A.County, A.Phone,
		U.ExpiryDate AS ExpirationDate, U.LastActivityDate,
		CASE WHEN EXISTS (
			SELECT	*
			FROM	USSiteUser
			WHERE	UserId = U.Id AND
					IsSystemUser = 1
		) THEN 1 ELSE 0 END AS IsSystemUser,
		CUP.IsTaxExempt,
		CUP.TaxId,
		U.CreatedDate,  U.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		ISNULL(U.ModifiedDate, U.CreatedDate) AS ModifiedDate, ISNULL(U.ModifiedBy, U.CreatedBy) AS ModifiedBy, MFN.UserFullName AS ModifiedByFullName
FROM	USCommerceUserProfile AS CUP
        INNER JOIN USUser AS U ON U.Id = CUP.Id
		INNER JOIN USMembership AS M ON M.UserId = U.Id
		LEFT JOIN USUserShippingAddress AS USA ON USA.UserId = U.Id AND USA.IsPrimary = 1
		LEFT JOIN GLAddress AS A ON A.Id = USA.AddressId
		LEFT JOIN GLState AS ST ON ST.Id = A.StateId
		LEFT JOIN GLCountry AS C ON C.Id = A.CountryId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = U.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = U.ModifiedBy
WHERE	U.[Status] !=3
GO

PRINT 'Creating vwCustomerList'
GO
IF(OBJECT_ID('vwCustomerList') IS NOT NULL)
	DROP VIEW vwCustomerList
GO
CREATE VIEW [dbo].[vwCustomerList] 
AS    
WITH CTE
AS
(     
SELECT	CUP.Id, U.FirstName, U.LastName, U.MiddleName, U.UserName, NULL AS [Password], M.Email, U.EmailNotification, U.CompanyName, U.BirthDate, U.Gender, U.LeadScore,
		CUP.IsBadCustomer, CUP.IsActive, M.IsApproved, M.IsLockedOut, CUP.IsOnMailingList, CUP.[Status], CUP.CSRSecurityQuestion, CUP.CSRSecurityPassword, U.HomePhone,
		U.MobilePhone, U.OtherPhone, U.ImageId, CUP.AccountNumber, CUP.IsExpressCustomer, CUP.ExternalId, CUP.ExternalProfileId,
		(
			SELECT	COUNT(*)
			FROM	CSCustomerLogin
			WHERE	CustomerId = CUP.Id
		) + 1 AS NumberOfLoginAccounts,
		CASE WHEN EXISTS
		(
			SELECT	Id
			FROM	PTLineOfCreditInfo
			WHERE	CustomerId =  CUP.Id AND
					[Status] <> dbo.GetDeleteStatus()
		) THEN 1 ELSE 0 END AS HasLOC,
		A.Id AS AddressId, A.AddressLine1, A.AddressLine2, A.AddressLine3, A.City, A.StateId, ST.[State], ST.StateCode,
		A.CountryId, C.CountryName, C.CountryCode, A.Zip, A.County, A.Phone,
		U.ExpiryDate AS ExpirationDate, U.LastActivityDate,
		CASE WHEN EXISTS (
			SELECT	*
			FROM	USSiteUser
			WHERE	UserId = U.Id AND
					IsSystemUser = 1
		) THEN 1 ELSE 0 END AS IsSystemUser,
		CUP.IsTaxExempt,
		CUP.TaxId,
		U.CreatedDate,  U.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		ISNULL(U.ModifiedDate, U.CreatedDate) AS ModifiedDate, ISNULL(U.ModifiedBy, U.CreatedBy) AS ModifiedBy, MFN.UserFullName AS ModifiedByFullName
FROM	USCommerceUserProfile AS CUP
        INNER JOIN USUser AS U ON U.Id = CUP.Id
		INNER JOIN USMembership AS M ON M.UserId = U.Id
		LEFT JOIN USUserShippingAddress AS USA ON USA.UserId = U.Id AND USA.IsPrimary = 1
		LEFT JOIN GLAddress AS A ON A.Id = USA.AddressId
		LEFT JOIN GLState AS ST ON ST.Id = A.StateId
		LEFT JOIN GLCountry AS C ON C.Id = A.CountryId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = U.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = U.ModifiedBy
WHERE	U.[Status] !=3
)

SELECT C.Id, C.FirstName, C.LastName, C.MiddleName, C.UserName, NULL AS [Password], C.Email, C.EmailNotification, C.CompanyName, C.BirthDate, C.Gender, C.LeadScore,
		C.IsBadCustomer, C.IsActive, C.IsApproved, C.IsLockedOut, C.IsOnMailingList, C.[Status], C.CSRSecurityQuestion, C.CSRSecurityPassword, C.HomePhone,
		C.MobilePhone, C.OtherPhone, C.ImageId, C.AccountNumber, C.IsExpressCustomer, C.ExternalId, C.ExternalProfileId,
		C.NumberOfLoginAccounts, C.HasLOC, C.AddressId, C.AddressLine1, C.AddressLine2, C.AddressLine3, C.City, C.StateId, C.[State],C.StateCode,
		C.CountryId, C.CountryName, C.CountryCode, C.Zip, C.County, C.Phone, C.ExpirationDate, C.LastActivityDate, C.IsSystemUser,
		C.IsTaxExempt, C.TaxId,	C.CreatedDate,  C.CreatedBy, C.CreatedByFullName, C.ModifiedDate, C.ModifiedBy, C.ModifiedByFullName,
        S.Id AS SiteId, CASE WHEN CO.OrderSiteId=S.Id THEN CO.TotalOrders ELSE 0 END AS TotalOrders, CASE WHEN CO.OrderSiteId=S.Id THEN CO.TotalSpend ELSE 0 END AS TotalSpend, CO.LastPurchase
FROM CTE  C cross join SISIte S
     Left join  (
	        SELECT		CustomerId, SiteId as OrderSiteId, COUNT(*) AS TotalOrders, SUM(GrandTotal) AS TotalSpend, MAX(OrderDate) AS LastPurchase
		    FROM		OROrder O JOIN SISITE S on O.SiteId = S.Id
		    WHERE S.Status = 1
		    GROUP BY	CustomerId, SiteId
		  ) AS CO ON CO.CustomerId = C.Id and CO.OrderSiteId = S.Id
WHERE S.Status = 1 
GO

PRINT 'Creating OrderItem_Save'
GO
IF(OBJECT_ID('OrderItem_Save') IS NOT NULL)
	DROP PROCEDURE  OrderItem_Save
GO	
CREATE PROCEDURE [dbo].[OrderItem_Save]  
(   
 @Id uniqueidentifier output,  
 @OrderId uniqueidentifier,  
 @OrderShippingId uniqueidentifier,  
 @OrderItemStatusId int,  
 @ProductSKUId uniqueidentifier,  
 @Quantity decimal(18,2),  
 @Price money,  
 @UnitCost money,    
 @IsFreeShipping bit,  
 @Sequence int,  
 @Status int,  
 --@CreatedDate datetime,  
 @CreatedBy uniqueidentifier,  
 --@ModifiedDate datetime,  
 @ModifiedBy uniqueidentifier,
@HandlingCharge money,
@ParentOrderItemId uniqueidentifier,
@BundleItemId uniqueidentifier,
@CartItemId uniqueidentifier,
@Tax money,
@Discount money,
@HandlingTax Money
)  
  
AS  
BEGIN       

  DECLARE @CreatedDate datetime, @ModifiedDate DateTime
IF(@OrderShippingId is null OR @OrderShippingId = dbo.GetEmptyGUID())
BEGIN
	SET @OrderShippingId = null
END
 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin  
  set @Id = newid()  
  set @CreatedDate = getUTCDate()
  INSERT INTO OROrderItem  WITH (ROWLOCK)
           ([Id]  
   ,[OrderId]  
   ,[OrderShippingId]  
   ,[OrderItemStatusId]  
   ,[ProductSKUId]  
   ,[Quantity]  
   ,[Price]  
   ,[UnitCost]  
   ,[IsFreeShipping]  
   ,[Sequence]  
   ,[Status]  
   ,[CreatedDate]  
   ,[CreatedBy]  
   ,[ModifiedDate]  
   ,[ModifiedBy]
   ,[HandlingCharge]
   ,[ParentOrderItemId]
   ,[BundleItemId]
	,[CartItemId]
	,[Tax]
	,[TaxableDiscount]
	,[Discount]
	,[HandlingTax])  
     VALUES  
           (@Id,   
   @OrderId ,  
   @OrderShippingId ,  
   @OrderItemStatusId ,  
   @ProductSKUId ,  
   @Quantity ,  
   @Price ,  
   @UnitCost ,  
   @IsFreeShipping ,  
   @Sequence ,  
   @Status ,  
   @CreatedDate ,  
   @CreatedBy ,  
   @CreatedDate ,  
   @ModifiedBy, 
   @HandlingCharge,
   @ParentOrderItemId,
   @BundleItemId,
	@CartItemId,
	@Tax,
	@Discount,
	@Discount,
	@HandlingTax)  

 end  
 else  
 begin  
  --set @ModifiedDate= getUTCDate()
  update OROrderItem  WITH (ROWLOCK)
  set   
   [OrderId]=@OrderId ,  
   [OrderShippingId]=@OrderShippingId ,  
   [OrderItemStatusId]=@OrderItemStatusId ,  
   [ProductSKUId]=@ProductSKUId ,  
   [Quantity]=@Quantity ,  
   [Price]=@Price ,  
   [UnitCost]=@UnitCost ,  
   [IsFreeShipping]=@IsFreeShipping ,  
   [Sequence]=@Sequence ,  
   [Status]=@Status ,  
   --[CreatedDate]=@CreatedDate ,  
   [CreatedBy]=@CreatedBy ,  
   [ModifiedDate]=GetUTCDate() ,  
   [ModifiedBy]=@ModifiedBy ,
   [HandlingCharge] = @HandlingCharge,
   [ParentOrderItemId]=@ParentOrderItemId,
   [BundleItemId]=@BundleItemId,
   [CartItemId]=@CartItemId,
	[Tax] = @Tax,
	[TaxableDiscount] = @Discount,
	[Discount]=@Discount,
	[HandlingTax] = @HandlingTax
  where Id=@Id  
     


 end  

	CREATE Table #UPAttributeValue(OrderId uniqueidentifier,OrderItemId Uniqueidentifier,OrderItemAttributeValueId uniqueidentifier)
	
	INSERT INTO #UPAttributeValue(OrderId,OrderItemId,OrderItemAttributeValueId)
	SELECT b.OrderId,b.Id,a.OrderItemAttributeValueId
	from OROrderItemAttributeValue a
	INNER JOIN  OROrderItem b ON  	a.CartItemId = b.CartItemId
	where a.OrderItemId is null and b.CartItemId= @CartItemId

	Update AV WITH (ROWLOCK) SET AV.OrderId =T.OrderId,AV.OrderItemId=T.OrderItemId
	FROM OROrderItemAttributeValue AV
	INNER JOIN #UPAttributeValue T on AV.OrderItemAttributeValueId = T.OrderItemAttributeValueId


	DECLARE @OrderItemQuantity money
	DECLARE @OrderShipmentItemQuantity money
	DECLARE @OrderStatusId int
	DECLARE @count1 int
	DECLARE @count2 int

	Select @OrderItemQuantity = Quantity
	FROM  OROrderItem  
	Where Id = @Id

	Select @OrderShipmentItemQuantity = sum(Quantity)
	FROM  FFOrderShipmentItem  SI
	INNER JOIN FFOrderShipmentPackage  S ON SI.OrderShipmentContainerId=S.Id
	Where OrderItemId =@Id AND ShipmentStatus=2
	Group By OrderItemId

	If(@OrderItemQuantity = @OrderShipmentItemQuantity OR @OrderShipmentItemQuantity>0.0)
	BEGIN
	
		Update OROrderItem WITH (ROWLOCK)
		Set OrderItemStatusId=Case When @OrderItemQuantity = @OrderShipmentItemQuantity Then 2 Else 7 End
		Where  Id =@Id

		if @@error <> 0
		begin
			raiserror('DBERROR||%s',16,1,'Update Status Shipped OrderShipmentItem')
			return dbo.GetDataBaseErrorCode()	
		end
	END


END  
GO

PRINT 'Creating BLD_ExportAttributeEnums'
GO
IF(OBJECT_ID('BLD_ExportAttributeEnums') IS NOT NULL)
	DROP PROCEDURE  BLD_ExportAttributeEnums
GO
CREATE PROCEDURE [dbo].[BLD_ExportAttributeEnums]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		Begin
			select 
				e.Id as 'Key'
				,a.Title as AttributeTitle
				,coalesce(cast(e.Value as varchar(max)), cast(e.NumericValue as varchar(max)), '')
				,ac.Name as AttributeCategoryName
				,0 as UpdateFlag
			from dbo.ATAttributeCategoryItem aci
			join dbo.ATAttributeCategory ac on (ac.id = aci.CategoryId)
			join dbo.ATAttribute a on (aci.AttributeId = a.Id)
			join dbo.ATAttributeEnum e on (a.Id = e.AttributeId)
			where 
				--ac.IsSystem = 0 and 
				(ac.IsGlobal != 1 or ac.IsGlobal is null) and e.Status = 1
		END
	ELSE
		Begin
			select 
			 e.Id  as 'Key'
            ,a.Title as AttributeTitle
            ,coalesce(cast(e.Value as varchar(max)), cast(e.NumericValue as varchar(max)), '')
            ,ac.Name as AttributeCategoryName
            ,0 as UpdateFlag
            from dbo.ATAttributeEnumVariantCache aevc
            join dbo.ATAttributeEnum e on aevc.id=e.id
            join dbo.ATAttribute a on (e.AttributeID = a.Id)
            join dbo.ATAttributeCategoryItem aci on (a.id = aci.AttributeId)
            join dbo.ATAttributeCategory ac on (aci.CategoryId = ac.Id)
			where 
				--ac.IsSystem = 0 and
				(ac.IsGlobal != 1 or ac.IsGlobal is null) and 
				aevc.SiteId = @siteId and e.Status = 1
		END
END
GO

PRINT 'Creating CommerceCacheDto_Invalidate'
GO
IF(OBJECT_ID('CommerceCacheDto_Invalidate') IS NOT NULL)
	DROP PROCEDURE  CommerceCacheDto_Invalidate
GO
CREATE PROCEDURE [dbo].[CommerceCacheDto_Invalidate]
(
	@Key		nvarchar(500),
	@SiteId		uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	IF @Key = 'InvalidateNavFilter' OR @Key = 'All'
	BEGIN
		UPDATE NVFilterQuery SET IsLatest = 0 

		UPDATE GLCacheUpdate SET NavFilterCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END
	
	IF @Key = 'InvalidateFacet' OR @Key = 'All'
	BEGIN
		EXEC Facet_ReloadProductFacetCache @ApplicationId = @SiteId

		UPDATE GLCacheUpdate SET FacetCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END
	
	IF @Key = 'InvalidateFeatureOutput' OR @Key = 'All'
	BEGIN
		EXEC Feature_PopulateFeatureOutput @ApplicationId = @SiteId

		UPDATE GLCacheUpdate SET FeatureOutputCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END
	
	IF @Key = 'InvalidatePriceSet' OR @Key = 'All'
	BEGIN
		EXEC Cache_RefreshFlatPriceSets
		EXEC Cache_RefreshProductMinEffectivePrice @ApplicationId = @SiteId

		UPDATE GLCacheUpdate SET PriceSetCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END

	IF @Key = 'Application' OR @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET ApplicationCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END

	IF @Key = 'ApplicationBaseData' OR @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET ApplicationBaseDataCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END

	IF @Key = 'SiteSettings' OR @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET SiteSettingsCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END

	IF @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET AllCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END
END
GO	

Print 'Migration for Boolean Attributes' -- Few Attributes which is set Boolean having wrong AttributeDataTypeId
Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateBooleanAttributesAllOne'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN

    Declare @EnumValues Table
    (
        Value varchar(20)
    )
    INSERT INTO @EnumValues Values ('true')
    INSERT INTO @EnumValues Values ('false')

    Declare @BooleanDataTypeId uniqueidentifier
    Select @BooleanDataTypeId = Id from ATAttributeDataType where Type= 'System.Boolean' or Type ='Boolean'

    DECLARE @Attribute TABLE
    (
      ID uniqueidentifier,
      Processed bit
    )

    INSERT INTO @Attribute
    Select Id, 0 from ATAttribute where AttributeDataType = 'System.Boolean' or AttributeDataType ='Boolean'
    AND AttributeDataTypeId != @BooleanDataTypeId

    Declare @AttributeId uniqueidentifier

    WHILE EXISTS(Select 1 from @Attribute where Processed = 0)
     BEGIN
        Select top 1 @AttributeId = Id from @Attribute where Processed=0

         IF NOT EXISTS (Select 1 from ATAttributeEnum AE LEFT JOIN @EnumValues A ON A.Value = AE.Value
                    where AE.AttributeID =@AttributeId
                    AND A.Value IS NULL)
             BEGIN
                Print 'Attribute Updating to Boolean'
                Print @AttributeId
                Update A Set AttributeDataTypeId = @BooleanDataTypeId, IsEnum = 0
                From ATAttribute A Where Id = @AttributeId

             END

        Update @Attribute Set Processed = 1 where Id = @AttributeId

     END

    INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(),@ScriptName,@Version,GetDate())

END
GO

PRINT 'Creating Product_GetProductSKUListP'
GO
IF(OBJECT_ID('Product_GetProductSKUListP') IS NOT NULL)
	DROP PROCEDURE  Product_GetProductSKUListP
GO
CREATE PROCEDURE [dbo].[Product_GetProductSKUListP](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@pageNumber		int = null,
			@pageSize		int = null,
			@SortColumn VARCHAR(50)=null, 
			@Searchkeyword nvarchar(4000)=null,
			@WhereClause nvarchar(max)= null	
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 


--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

		
set @pageNumber = @pageNumber  -1
SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
SET @SortColumn = lower(rtrim(ltrim((@SortColumn))))
SET @PageUpperBound = @PageLowerBound + Isnull(@pageSize,0) -1;

DECLARE @PageUpperBoundVar varchar(250)
DECLARE @PageLowerBoundVar varchar(250)
DECLARE @ApplicationIdVar nvarchar(64)
SET @ApplicationIdVar = cast(@ApplicationId as nvarchar(64))
SET @ApplicationIdVar = ltrim(rtrim(@ApplicationIdVar))
SET @PageLowerBoundVar = cast(@PageLowerBound as varchar(256)) +1
SET @PageUpperBoundVar = cast(@PageUpperBound as varchar(256)) + 1



--SET @SearchKeyword = ''

DECLARE  @DynamicSQL  AS NVARCHAR(MAX) 


SET @SearchKeyword = (@SearchKeyword)

IF @SortColumn = 'producttitle desc'	 
	SET @SortColumn = 'Product.Title DESC' 	
ELSE IF @SortColumn = 'producttitle asc'	
	SET @SortColumn = 'Product.Title asc' 	
  
ELSE IF @SortColumn = 'sku desc'
	SET @SortColumn = 'SKU.SKU DESC'  
ELSE IF @SortColumn = 'sku asc'
	SET @SortColumn = 'SKU.SKU ASC'  

ELSE IF @SortColumn = 'typetitle desc'
	SET @SortColumn = 'Type.Title DESC'  
ELSE IF @SortColumn = 'typetitle asc'
	SET @SortColumn = 'Type.Title ASC'  

ELSE IF @SortColumn = 'listprice desc'
	SET @SortColumn = 'SKU.ListPrice DESC'  
ELSE IF @SortColumn = 'listprice asc'
	SET @SortColumn = 'SKU.ListPrice ASC'  

ELSE IF @SortColumn = 'quantity desc'
	SET @SortColumn = 'inv.Quantity DESC'  
ELSE IF @SortColumn = 'quantity asc'
	SET @SortColumn = 'inv.Quantity ASC'

ELSE IF @SortColumn = 'availabletosell desc'
	SET @SortColumn = 'avail.Quantity DESC'  
ELSE IF @SortColumn = 'availabletosell asc'
	SET @SortColumn = 'avail.Quantity ASC'
  
ELSE IF @SortColumn = 'isactivedescription desc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) DESC'  
ELSE IF @SortColumn = 'isactivedescription asc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) ASC'  

DECLARE @DynamicWhereClause nvarchar(max)


IF @SearchKeyword = ''
	SET @DynamicWhereClause = N''
ELSE
	SET @DynamicWhereClause = 
					N'AND ((Product.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Product.ProductIDUser) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Type.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (SKU.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (SKU.SKU) Like ''%''+'''+@SearchKeyword+'''+''%''
					) '
					
IF @WhereClause != '' 
	SET @DynamicWhereClause = @DynamicWhereClause +
			N'AND ('+@WhereClause+')';


SET @DynamicSQL = N'
SELECT * FROM (
   SELECT   
     ROW_NUMBER() OVER (ORDER BY 	
	 '+@SortColumn+'
) AS RowNumber, 				
					SKU.Id, 
					SKU.ProductId,
					SKU.SiteId, 
					SKU.Title, 
					SKU.SKU, 
					SKU.Description, 
					SKU.Sequence, 
					SKU.IsActive, 
					case SKU.IsActive WHEN 1 THEN ''Active'' ELSE ''Inactive'' END  As IsActiveDescription ,  
					SKU.IsOnline, 
					SKU.ListPrice, 
					SKU.UnitCost, 
					SKU.ListPrice As SalePrice, 
					SKU.WholesalePrice As WholesalePrice, 
					IsNull(inv.Quantity,0) as Quantity,
					IsNull(avail.Quantity,IsNull(inv.Quantity,0)) as AvailableToSell,
					SKU.HandlingCharges,
					Product.Title As ProductTitle, 
					Product.ProductIDUser As ProductIDUser,
					Type.Title	As TypeTitle,  
					Type.Id as TypeId,
					dbo.GetProductCategoryTitleCollection(Product.Id)  
					As CategoryTitle, 
					dbo.GetProductCategoryCollection(Product.Id)  
					 As CategoryIdsAndTitles,
					IsUnlimitedQuantity AS IsUnlimited,
					SKU.Length,SKU.Height,SKU.Width,SKU.Weight,SKU.SelfShippable,SKU.AvailableForBackOrder, SKU.MaximumDiscountPercentage,SKU.BackOrderLimit,
					ISNULL(Type.IsDownloadableMedia,0) as IsDownloadableMedia,FreeShipping,UserDefinedPrice
			FROM PRProductSKU As SKU
			INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
			LEFT JOIN [vwInventoryPerSite] inv on SKU.Id = inv.SKUId AND inv.SiteId=''' + @ApplicationIdVar + '''
			LEFT JOIN [dbo].vwAvailableToSellPerSite avail ON SKU.Id = avail.SKUId and avail.SiteId=inv.SiteId
            WHERE	
				Product.IsBundle = 0	
				'+@DynamicWhereClause+'
		) TV
		WHERE 
		(RowNumber BETWEEN '+@PageLowerBoundVar+' AND '+@PageUpperBoundVar+')'


EXEC sp_executesql   @DynamicSQL 

SET @DynamicSQL = N'
SELECT  
	Count(*)
FROM 
	PRProductSKU As SKU
	INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
	INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
WHERE	
		Product.IsBundle = 0
		'+@DynamicWhereClause

EXEC sp_executesql   @DynamicSQL 

END
GO
PRINT 'Update view vwProduct'
GO

IF(OBJECT_ID('vwProduct') IS NOT NULL)
	DROP VIEW vwProduct
GO

CREATE VIEW [dbo].[vwProduct]  
AS  
WITH CTE AS  
(  
 SELECT P.[Id],   
	P.SiteId,  
	P.Title,   
	P.ShortDescription,   
	P.LongDescription,   
	P.[Description],   
	P.ProductStyle,  
	P.UrlFriendlyTitle,   
	P.TaxCategoryID,   
	P.IsActive,   
	P.PromoteAsNewItem,   
	P.PromoteAsTopSeller,  
	P.ExcludeFromDiscounts,   
	P.ExcludeFromShippingPromotions,   
	P.Freight,   
	P.Refundable,   
	P.TaxExempt,  
	P.SEOTitle,
	P.SEOH1,   
	P.SEODescription,   
	P.SEOKeywords,   
	P.SEOFriendlyUrl,  
	P.IsShared,  
	ISNULL(P.ModifiedDate, P.CreatedDate) AS ModifiedDate,   
	ISNULL(P.ModifiedBy, P.CreatedBy) AS  ModifiedBy  
FROM PRProduct P  
  
UNION ALL  
  
SELECT P.[Id],   
	P.SiteId,  
	P.Title,   
	P.ShortDescription,   
	P.LongDescription,   
	P.[Description],   
	P.ProductStyle,  
	P.UrlFriendlyTitle,   
	P.TaxCategoryID,   
	P.IsActive,   
	P.PromoteAsNewItem,   
	P.PromoteAsTopSeller,  
	P.ExcludeFromDiscounts,   
	P.ExcludeFromShippingPromotions,   
	P.Freight,   
	P.Refundable,   
	P.TaxExempt,  
	P.SEOTitle,
	P.SEOH1,   
	P.SEODescription,   
	P.SEOKeywords,   
	P.SEOFriendlyUrl,  
	0,  
	P.ModifiedDate,   
	P.ModifiedBy  
 FROM PRProductVariantCache P  
)  
  
SELECT P.[Id],   
	C.SiteId,  
	P.SiteId AS SourceSiteId,  
	C.Title,   
	C.ShortDescription,   
	C.LongDescription,   
	C.[Description],   
	P.ProductIDUser,   
	P.Keyword,   
	C.ProductStyle,  
	P.BundleCompositionLastModified,   
	C.UrlFriendlyTitle,   
	C.TaxCategoryID,   
	CAST(C.IsActive AS BIT) AS IsActive,   
	C.PromoteAsNewItem,   
	C.PromoteAsTopSeller,  
	C.ExcludeFromDiscounts,   
	C.ExcludeFromShippingPromotions,   
	C.Freight,   
	C.Refundable,   
	C.TaxExempt,  
	P.ProductTypeID,   
	P.[Status],   
	CAST(P.IsBundle AS BIT) AS IsBundle,   
	PTP.ProductTypeName,  
	CASE WHEN C.SEOFriendlyUrl IS NULL OR C.SEOFriendlyUrl = '' THEN  '/' + PTP.ProductTypePath +  N'id-' + P.ProductIDUser + '/' + C.UrlFriendlyTitle ELSE C.SEOFriendlyUrl END AS ProductUrl,
	C.SEOTitle,
	C.SEOH1,   
	C.SEODescription,   
	C.SEOKeywords,   
	C.SEOFriendlyUrl,  
	P.IsShared,  
	P.CreatedDate,   
	P.CreatedBy AS CreatedById,   
	CFN.UserFullName AS CreatedByFullName,  
	C.ModifiedDate,   
	C.ModifiedBy AS ModifiedById,   
	MFN.UserFullName AS ModifiedByFullName
FROM CTE C  
	INNER JOIN PRProduct AS P ON P.Id = C.Id  
	INNER JOIN vwProductTypePathVariant AS PTP ON PTP.ProductTypeId = P.ProductTypeID AND PTP.SiteId = C.SiteId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy  
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
GO

PRINT 'Update stored procedure ProductSEO_Get'
GO
IF(OBJECT_ID('ProductSEO_Get') IS NOT NULL)
	DROP PROCEDURE  ProductSEO_Get
GO
CREATE PROCEDURE [dbo].[ProductSEO_Get](
--********************************************************************************
-- Parameters Passing in ProductSEO_ 
--********************************************************************************
			@Id uniqueidentifier = null OUTPUT,
			@ProductId uniqueidentifier = null ,
			@ApplicationId uniqueidentifier=null
)
AS
--********************************************************************************
-- code
--********************************************************************************
BEGIN
		IF (@Id Is Null) AND (@ProductId Is Null)
		BEGIN
				RAISERROR('Either SEO Term Id or Product Id is required', 16, 1)
				RETURN dbo.GetBusinessRuleErrorCode()
		END

				Select Id, 
						SEOTitle Title, 
						SEOH1 ProductTitleH1,
						SEODescription Description, 
						SEOKeywords Keywords, 
						SEOFriendlyUrl FriendlyUrl, 
						dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId) CreatedDate, 
						dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,
						CreatedBy,
						ModifiedBy
				FROM PRProduct
				WHERE 
						[Status] = dbo.GetActiveStatus()
						AND Id = @Id or Id = @ProductId
END
GO

PRINT 'Update stored procedure ProductSEO_Save'
GO

IF(OBJECT_ID('ProductSEO_Save') IS NOT NULL)
	DROP PROCEDURE  ProductSEO_Save
GO
CREATE PROCEDURE [dbo].[ProductSEO_Save](
--********************************************************************************
-- Parameters Passing in ProductSEO_ 
--********************************************************************************
			@Id uniqueidentifier = null OUTPUT,
			@ProductId uniqueidentifier, 
			@Title nvarchar(512) = null,
			@ProductTitleH1 nvarchar(512) = null,
			@Description nvarchar(2048) = null,
			@Keywords  nvarchar(1024)=null,
			@FriendlyUrl nvarchar(1024) = null,
			@ModifiedDate datetime = null,
			@ModifiedBy uniqueidentifier,
			@ApplicationId uniqueidentifier
)
AS

Declare @slash char(1)
select @slash= SUBSTRING(@FriendlyUrl, 1, 1) 
if(@slash<> '/')
	set @FriendlyUrl = '/' + @FriendlyUrl
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Now dateTime
DECLARE @NewGuid uniqueidentifier
DECLARE @RowCount int

--********************************************************************************
-- code
--********************************************************************************
BEGIN

	IF((SELECT Count(Title) FROM  PRProduct
				WHERE (isnull(SEOFriendlyUrl,'1')) = (isnull(@FriendlyUrl,'2')) 
						AND SiteId= @ApplicationId AND Id!=@ProductId)> 0 )
		   BEGIN
				RAISERROR('SEO Friendly Url already exists', 16, 1)
				RETURN dbo.GetBusinessRuleErrorCode()
		   END
		   
	If (@Id is null)   -- Check if the product has a SEO already
	BEGIN
		Set @Id = @ProductId
	END

				Set @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId)

						UPDATE	PRProduct  WITH (ROWLOCK)
					SET			SEOTitle			=	@Title
							   ,SEOH1 = @ProductTitleH1
							   ,SEODescription	= @Description
							   ,[SEOKeywords]		= @Keywords
							   ,[SEOFriendlyUrl]	= @FriendlyUrl
							   ,[ModifiedBy]	= @ModifiedBy
							   ,[ModifiedDate]	= GetUTCDate()
					WHERE   
						[Id]           		=	@Id 
				

Select @Id
END
GO

PRINT 'Update stored procedure Product_GetProduct'
GO
IF(OBJECT_ID('Product_GetProduct') IS NOT NULL)
	DROP PROCEDURE  Product_GetProduct
GO

CREATE PROCEDURE [dbo].[Product_GetProduct](
@Id uniqueidentifier = null,
@ApplicationId uniqueidentifier=null,
@skuId uniqueidentifier = null,
@ProductIDUser nvarchar(50) =null
)    
AS    

if(@skuId is not null and @Id is null)
begin 
	select @Id = ProductId from PRProductSku where Id=@skuId
End 

if(@ProductIDUser is not null and @Id is null)
begin 
	select @Id = Id from PRProduct where ProductIDUser=@ProductIDUser
End 


	SELECT     
	 [Prd].[Id],    
	 [Prd].ProductIDUser,    
	 [Prd].[Title],    
	 [Prd].[ShortDescription],    
	 [Prd].[LongDescription],    
	 [Prd].[Description],    
	 [Prd].[Keyword],    
	 [Prd].[IsActive],    
	 [Prd].[ProductTypeID],    
	 vPT.[ProductTypeName] as ProductTypeName,    
	 vPT.[ProductTypePath] +  N'id-' + Prd.ProductIDUser + '/' + Prd.UrlFriendlyTitle as DefaultUrl, Prd.UrlFriendlyTitle,    
	 vPT.[IsDownloadableMedia] ,
	 [Prd].[ProductStyle],    
	 dbo.ConvertTimeFromUtc([Prd].CreatedDate,@ApplicationId)CreatedDate,    
	 [Prd].CreatedBy,    
	 dbo.ConvertTimeFromUtc([Prd].ModifiedDate,@ApplicationId)ModifiedDate,    
	 [Prd].ModifiedBy,    
	 [Prd].Status,    
	 [Prd].IsBundle,    
	 dbo.ConvertTimeFromUtc( [Prd].BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,    
	 [Prd].TaxCategoryID,    
	 [Prd].PromoteAsNewItem,
	 [Prd].PromoteAsTopSeller,
	 [Prd].ExcludeFromDiscounts,
	 [Prd].ExcludeFromShippingPromotions,
	 [Prd].Freight,
	 [Prd].Refundable,
	 [Prd].TaxExempt,
	 [Prd].SEOTitle,
	 [Prd].SEOH1,
	 [Prd].SEODescription,
	 [Prd].SEOKeywords,
	 [Prd].SEOFriendlyUrl,
	 (Select Count(*) From PRProductSKU where ProductId= [Prd].Id) as NumberOfSKUs    
	FROM PRProduct Prd INNER JOIN vwProductTypePath vPT on [Prd].ProductTypeID = vPT.ProductTypeId    
	WHERE [Prd].Id = @Id;    
	    
	    
	select     
	 a.[Id],    
	 a.[AttributeDataType],    
	 a.[Title],    
	 av.[Value],    
	 pav.[ProductId]    
	from PRProductAttributeValue pav join ATAttributeEnum av on av.[Id] = pav.[AttributeEnumId]    
	 join ATAttribute a on av.[AttributeID]=a.[Id]     
	     
	where pav.[ProductId] = @Id    
	order by [ProductId],av.[AttributeID];
GO

PRINT 'Update stored procedure Product_GetProductAndSkuByType'
GO
IF(OBJECT_ID('Product_GetProductAndSkuByType') IS NOT NULL)
	DROP PROCEDURE  Product_GetProductAndSkuByType
GO
CREATE PROCEDURE [dbo].[Product_GetProductAndSkuByType]      
(      
 @ProductTypeID uniqueidentifier
,@ApplicationId uniqueidentifier=null

)      
AS      

--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
   

 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
vPT.[IsDownloadableMedia], S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 S.TaxCategoryId,
 S.PromoteAsNewItem,
 S.PromoteAsTopSeller,
 S.ExcludeFromDiscounts,
 S.ExcludeFromShippingPromotions,
 S.Freight,
 S.Refundable,
 S.TaxExempt,
 S.SEOTitle,
 S.SEOH1,
 S.SEODescription,
 S.SEOKeywords,
 S.SEOFriendlyUrl
 FROM PRProduct S       
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
  Where S.ProductTypeID =@ProductTypeID
--Order By T.Sno      


Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from PRProduct t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.Id  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  
AND t.ProductTypeID =@ProductTypeID


SELECT S.[Id]    
   ,S.[ProductId]    
   ,S.[SiteId]    
   ,[SKU]    
   ,S.[Title]    
   ,S.[Description]    
   ,S.[Sequence]    
   ,S.[IsActive]    
   ,[IsOnline]    
   ,[ListPrice]    
   ,[UnitCost]    
   ,[PreviousSoldCount]    
   ,S.[CreatedBy]    
   ,S.[CreatedDate]    
   ,S.[ModifiedBy]    
   ,S.[ModifiedDate]    
   ,S.[Status]    
   ,[Measure]    
   ,[OrderMinimum]    
   ,[OrderIncrement]  
   ,[HandlingCharges] 
   ,[Length]
   ,[Height]
   ,[Width]
   ,[Weight]
   ,[SelfShippable]    
   ,[AvailableForBackOrder]
   ,MaximumDiscountPercentage
   ,[BackOrderLimit]
   ,[IsUnlimitedQuantity]
   ,FreeShipping
   ,UserDefinedPrice
  FROM [PRProductSKU] S   
  Inner Join PRProduct t on S.ProductId=t.Id  
  Where t.ProductTypeID =@ProductTypeID



Select   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from PRProduct t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.Id  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On ( PS.Id = SA.SKUId  AND SA.ProductAttributeID = PA.ID)  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
and t.ProductTypeID =@ProductTypeID
--Order By SA.AttributeId  




select  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
from PRProduct t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.Id   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId  
where  A.Status=dbo.GetActiveStatus()     
and t.ProductTypeID =@ProductTypeID

END
GO

PRINT 'Update stored procedure Product_GetProductAndSkuByXmlGuids'
GO
IF(OBJECT_ID('Product_GetProductAndSkuByXmlGuids') IS NOT NULL)
	DROP PROCEDURE  Product_GetProductAndSkuByXmlGuids
GO
CREATE PROCEDURE [dbo].[Product_GetProductAndSkuByXmlGuids]      
(      
 @Id Xml  
,@ApplicationId uniqueidentifier=null

)      
AS      

--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)      

Insert Into @tempXml(ProductId)      
Select Distinct tab.col.value('text()[1]','uniqueidentifier')      
 FROM @Id.nodes('/ProductCollection/Product')tab(col)      

 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
vPT.[IsDownloadableMedia], S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 SEOFriendlyUrl,
S.TaxCategoryId,
S.PromoteAsNewItem,
S.PromoteAsTopSeller,
S.ExcludeFromDiscounts,
S.ExcludeFromShippingPromotions,
S.Freight,
S.Refundable,
S.TaxExempt,
S.SEOTitle,
S.SEOH1,
S.SEODescription,
S.SEOKeywords
  FROM @tempXml T      
  INNER JOIN PRProduct S on S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
--Order By T.Sno      


Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from @tempXml t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.ProductId  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  


SELECT 
	S.[Id]    
   ,S.[ProductId]    
   ,S.[SiteId]    
   ,S.[SKU]    
   ,S.[Title]    
   ,S.[Description]    
   ,S.[Sequence]    
   ,S.[IsActive]    
   ,S.[IsOnline]    
   ,ISNULL(SC.ListPrice,S.ListPrice)  ListPrice    
   ,S.[UnitCost]    
   ,S.[PreviousSoldCount]    
   ,S.[CreatedBy]    
   ,S.[CreatedDate]    
   ,S.[ModifiedBy]    
   ,S.[ModifiedDate]    
   ,S.[Status]    
   ,S.[Measure]    
   ,S.[OrderMinimum]    
   ,S.[OrderIncrement]  
   ,S.[HandlingCharges] 
   ,S.[Length]
   ,S.[Height]
   ,S.[Width]
   ,S.[Weight]
   ,S.[SelfShippable]    
   ,S.[AvailableForBackOrder]
   ,S.MaximumDiscountPercentage
   ,S.[BackOrderLimit]
   ,S.[IsUnlimitedQuantity]
   ,S.FreeShipping
   ,S.UserDefinedPrice
  FROM [PRProductSKU] S   
  Inner Join @tempXml t on S.ProductId=t.ProductId  
  LEFT JOIN PRProductSKUVariantCache SC ON S.Id = SC.Id AND SC.SiteId=@ApplicationId



Select   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On ( PS.Id = SA.SKUId  AND SA.ProductAttributeID = PA.ID)  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  




select  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized,
  AG.CategoryId,
  AG.AttributeGroupId

from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=dbo.GetActiveStatus()     


END
GO

PRINT 'Update stored procedure Product_GetProductAndSkuByXmlGuidsForProductIndexer'
GO
IF(OBJECT_ID('Product_GetProductAndSkuByXmlGuidsForProductIndexer') IS NOT NULL)
	DROP PROCEDURE  Product_GetProductAndSkuByXmlGuidsForProductIndexer
GO
CREATE PROCEDURE [dbo].[Product_GetProductAndSkuByXmlGuidsForProductIndexer]      
(      
		@ApplicationId uniqueidentifier=null
)      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)      
      
Insert Into @tempXml(ProductId)      
Select distinct ProductId as Id from PRProductSKU where IsActive =1 and IsOnline=1  
      
 SELECT   
 DISTINCT     
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
 vPT.[IsDownloadableMedia],S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 SEOFriendlyUrl,
 PromoteAsNewItem,
PromoteAsTopSeller,
ExcludeFromDiscounts,
ExcludeFromShippingPromotions,
Freight,
Refundable,
TaxExempt,
SEOTitle,
SEOH1,
SEODescription,
SEOKeywords,
 [S].TaxCategoryID        
  FROM @tempXml T      
  INNER JOIN PRProduct S on S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
--Order By T.Sno      
  
  
Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from @tempXml t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.ProductId  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  
  
  
SELECT [Id]    
      ,S.[ProductId]    
      ,[SiteId]    
      ,[SKU]    
   ,[Title]    
      ,[Description]    
      ,[Sequence]    
      ,[IsActive]    
      ,[IsOnline]    
      ,[ListPrice]    
      ,[UnitCost]    
      ,[PreviousSoldCount]    
   ,[CreatedBy]    
   ,[CreatedDate]    
   ,[ModifiedBy]    
   ,[ModifiedDate]    
   ,[Status]    
   ,[Measure]    
   ,[OrderMinimum]    
   ,[OrderIncrement]  
   ,[HandlingCharges]  
    ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]    
    ,[AvailableForBackOrder]
	,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
	,FreeShipping
,UserDefinedPrice
  FROM [PRProductSKU] S   
  Inner Join @tempXml t on S.ProductId=t.ProductId  
  
  
Select   
 SA.Id,  
 SA.SKUId,  
 PA.ProductId,
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On (SA.AttributeId=PA.AttributeId AND PS.Id = SA.SKUId )  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  
  
  
  
  
select  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
	A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=dbo.GetActiveStatus()    and  A.IsSystem=0 and A.IsFaceted=1
  

SELECT P.Id AS ProductId,	CASE WHEN Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice) WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
 ELSE Min(ListPrice) END  as Price ,  cast( SUM(PS.PreviousSoldCount) as int) AS soldcount, P.CreatedDate		
			FROM			
				dbo.PRProduct P
				INNER JOIN dbo.PRProductSKU PS ON P.Id = PS.ProductId 		
					LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
									) PP ON P.Id= PP.ProductId 	
					INNER JOIN @tempXml TP  ON P.Id = TP.ProductId 	
			WHERE     
				(PS.IsActive = 1) AND (PS.IsOnline = 1) AND (P.IsActive = 1)     
			GROUP BY     
				P.Id, 
					PP.MinimumEffectivePrice,
				P.CreatedDate,   
				P.ProductIDUser 

  
END
GO

PRINT 'Update stored procedure Product_GetProductByType'
GO
IF(OBJECT_ID('Product_GetProductByType') IS NOT NULL)
	DROP PROCEDURE  Product_GetProductByType
GO
CREATE PROCEDURE [dbo].[Product_GetProductByType]    
(    
 @ProductTypeID uniqueidentifier,
 @ApplicationId uniqueidentifier=null
)    
AS    
    
--********************************************************************************    
-- Variable declaration    
--********************************************************************************    
BEGIN    
--********************************************************************************    
-- code    
--********************************************************************************    
 
 SELECT      
 S.[Id],    
 S.[ProductIDUser],    
 S.[Title],    
 S.[ShortDescription],    
 S.[LongDescription],    
 S.[Description],    
 S.[Keyword],    
 S.[IsActive],    
 S.[ProductTypeID],    
 vPT.[ProductTypeName] as ProductTypeName,    
  vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,     
  vPT.[IsDownloadableMedia],
 S.[ProductStyle],    
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,    
 dbo.ConvertTimeFromUtc(S.CreatedDate,@ApplicationId)CreatedDate,    
 S.CreatedBy,    
 dbo.ConvertTimeFromUtc(S.ModifiedDate,@ApplicationId)ModifiedDate,    
 S.ModifiedBy,    
 S.Status,    
 S.IsBundle,    
 dbo.ConvertTimeFromUtc(S.BundleCompositionLastModified,@ApplicationId) BundleCompositionLastModified ,  
 SEOFriendlyUrl,
S.TaxcategoryId,
PromoteAsNewItem,
PromoteAsTopSeller,
ExcludeFromDiscounts,
ExcludeFromShippingPromotions,
Freight,
Refundable,
TaxExempt,
SEOTitle,
SEOH1,
SEODescription,
SEOKeywords
  FROM  PRProduct S 
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId    
  Where S.ProductTypeID =@ProductTypeID	
END    
    
select     
 a.[Id],    
 a.[AttributeDataType],    
 a.[Title],    
 pav.[Value],    
 pav.[ProductId]    
from PRProductAttributeValue pav     
 join ATAttribute a on pav.[AttributeId]=a.[Id]    
 Inner Join PRProduct P on pav.ProductId = P.Id
where P.ProductTypeID =@ProductTypeID
order by [ProductId],[AttributeId];
GO

PRINT 'Update stored procedure Product_GetProductByXmlGuids'
GO
IF(OBJECT_ID('Product_GetProductByXmlGuids') IS NOT NULL)
	DROP PROCEDURE  Product_GetProductByXmlGuids
GO
CREATE PROCEDURE [dbo].[Product_GetProductByXmlGuids]    
(    
 @Id Xml,
 @ApplicationId uniqueidentifier=null
)    
AS    
    
--********************************************************************************    
-- Variable declaration    
--********************************************************************************    
BEGIN    
--********************************************************************************    
-- code    
--********************************************************************************    
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)    
    
Insert Into @tempXml(ProductId)    
Select tab.col.value('text()[1]','uniqueidentifier')    
 FROM @Id.nodes('/ProductCollection/Product')tab(col)    
    
 SELECT      
 S.[Id],    
 S.[ProductIDUser],    
 S.[Title],    
 S.[ShortDescription],    
 S.[LongDescription],    
 S.[Description],    
 S.[Keyword],    
 S.[IsActive],    
 S.[ProductTypeID],    
 vPT.[ProductTypeName] as ProductTypeName,    
  vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,     
  vPT.[IsDownloadableMedia],
 S.[ProductStyle],    
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,    
 dbo.ConvertTimeFromUtc(S.CreatedDate,@ApplicationId)CreatedDate,    
 S.CreatedBy,    
 dbo.ConvertTimeFromUtc(S.ModifiedDate,@ApplicationId)ModifiedDate,    
 S.ModifiedBy,    
 S.Status,    
 S.IsBundle,    
 dbo.ConvertTimeFromUtc(S.BundleCompositionLastModified,@ApplicationId) BundleCompositionLastModified ,  
 SEOFriendlyUrl,
S.TaxcategoryId,
PromoteAsNewItem,
PromoteAsTopSeller,
ExcludeFromDiscounts,
ExcludeFromShippingPromotions,
Freight,
Refundable,
TaxExempt,
SEOTitle,
SEOH1,
SEODescription,
SEOKeywords
  FROM @tempXml T    
  INNER JOIN PRProduct S on S.Id=T.ProductId    
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId    
Order By T.Sno    
END    
    
select     
 a.[Id],    
 a.[AttributeDataType],    
 a.[Title],    
 pav.[Value],    
 pav.[ProductId],
  AG.AttributeGroupId
from PRProductAttributeValue pav     
 join ATAttribute a on pav.[AttributeId]=a.[Id]    
 LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where pav.[ProductId] in (select ProductId FROM @Id.nodes('/ProductCollection/Product')tab(col)    
  INNER JOIN PRProduct S on S.Id=tab.col.value('text()[1]','uniqueidentifier'))    
order by [ProductId],pav.[AttributeId];
GO

PRINT 'Update stored procedure Product_GetProductListP'
GO
IF(OBJECT_ID('Product_GetProductListP') IS NOT NULL)
	DROP PROCEDURE  Product_GetProductListP
GO
CREATE PROCEDURE [dbo].[Product_GetProductListP](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@pageNumber		int = null,
			@pageSize		int = null,
			@SortColumn VARCHAR(100)=null, 
			@Searchkeyword nvarchar(4000)=null,
			@WhereClause nvarchar(max)= null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN	

set @pageNumber = @pageNumber  -1
SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0) +1
SET @SortColumn = rtrim(ltrim((@SortColumn)))
SET @PageUpperBound = @PageLowerBound + Isnull(@pageSize,0) -1;

DECLARE @PageUpperBoundVar varchar(250)
DECLARE @PageLowerBoundVar varchar(250)
DECLARE @ApplicationIdVar nvarchar(64)
SET @ApplicationIdVar = cast(@ApplicationId as nvarchar(64))
SET @ApplicationIdVar = ltrim(rtrim(@ApplicationIdVar))
SET @PageLowerBoundVar = cast(@PageLowerBound as varchar(256))
SET @PageUpperBoundVar = cast(@PageUpperBound as varchar(256))
	


DECLARE  @DynamicSQL  AS NVARCHAR(MAX) 


SET @SearchKeyword = (@SearchKeyword)

     IF @SortColumn = 'producttitle desc'	 
	SET @SortColumn = 'Product.Title DESC' 	
ELSE IF @SortColumn = 'producttitle asc'	
	SET @SortColumn = 'Product.Title asc' 	  


ELSE IF @SortColumn = 'typetitle desc'
	SET @SortColumn = 'Type.Title DESC'  
ELSE IF @SortColumn = 'typetitle asc'
	SET @SortColumn = 'Type.Title ASC'  

ELSE IF @SortColumn = 'reallistprice desc'
	SET @SortColumn = 'PPR.ListPrice DESC'  
ELSE IF @SortColumn = 'reallistprice asc'
	SET @SortColumn = 'PPR.ListPrice ASC'  

ELSE IF @SortColumn = 'quantity desc'
	SET @SortColumn = 'PPR.Quantity DESC'  
ELSE IF @SortColumn = 'quantity asc'
	SET @SortColumn = 'PPR.Quantity ASC'
  
ELSE IF @SortColumn = 'isactivedescription desc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) DESC'  
ELSE IF @SortColumn = 'isactivedescription asc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) ASC'  

DECLARE @DynamicWhereClause nvarchar(max)


IF @SearchKeyword = ''
	SET @DynamicWhereClause = N''
ELSE
	SET @DynamicWhereClause = 	
					N'AND ((Product.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Product.ProductIDUser) Like ''%''+'''+@SearchKeyword+'''+''%''			
					OR (Type.Title) Like ''%''+'''+@SearchKeyword+'''+''%''			
					) '

IF @WhereClause != '' 
	SET @DynamicWhereClause = @DynamicWhereClause +
			N'AND ('+@WhereClause+')';

SET @DynamicSQL = N'
SELECT * FROM (
   SELECT   
     ROW_NUMBER() OVER (ORDER BY 	
	 '+@SortColumn+'
) AS RowNumber, 				
					Product.Id As ProductId,
					Product.SiteId,
					Product.ShortDescription As Description, 
					Product.ProductIDUser ProductIDUser,
					Product.ProductIDUser As Sequence, 
					Product.IsActive, 
					case Product.IsActive WHEN 1 THEN ''Active'' ELSE ''Inactive'' END  As IsActiveDescription ,
					PPR.PriceRange as ListPrice,
					0 As UnitCost,
					0 As SalePrice,
					0 As WholesalePrice, 
					isnull(PPR.Quantity,0) As Quantity,
					isnull(PPR.AvailableToSell,0) As AvailableToSell,
					isnull(PPR.ListPrice,0) as RealListPrice,
					Product.IsBundle as IsBundle,
					dbo.ConvertTimeFromUtc(Product.BundleCompositionLastModified, '''+@ApplicationIdVar+''') as BundleCompositionLastModified,
					Product.Title As ProductTtitle,
					Product.Title As ProductTitle,
					Type.Title As TypeTitle,  
					Type.Id as TypeId,
					dbo.GetProductCategoryTitleCollection(Product.Id)  As CategoryTitle, 
					dbo.GetProductCategoryCollection(Product.Id)  As CategoryIdsAndTitles,
					dbo.IsProductUnlimited(Product.Id) as IsUnlimited,
					ISNULL(Type.IsDownloadableMedia,0) as IsDownloadableMedia,
					Product.TaxCategoryID,
					PromoteAsNewItem,
					PromoteAsTopSeller,
					ExcludeFromDiscounts,
					ExcludeFromShippingPromotions,
					Freight,
					Refundable,
					TaxExempt,
					SEOTitle,
					SEOH1,
					SEODescription,
					SEOKeywords
			FROM PRProduct As Product
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
			LEFT JOIN vwPRProductPriceRange PPR  ON PPR.Id =  Product.Id AND PPR.SiteId=''' + cast(@ApplicationId as Char(36)) + '''
            WHERE		Product.IsBundle = 0 			
				'+@DynamicWhereClause+'
		) TV
		WHERE 
		(RowNumber BETWEEN '+@PageLowerBoundVar+' AND '+@PageUpperBoundVar+')'

print @DynamicSQL
EXEC sp_executesql   @DynamicSQL 


SET @DynamicSQL = N'
SELECT 
	Count(*) 
FROM PRProduct As Product
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  

            WHERE	
					Product.IsBundle = 0 
		'+@DynamicWhereClause
EXEC sp_executesql   @DynamicSQL 

END
GO

PRINT 'Update stored procedure Product_GetProductVariant'
GO
IF(OBJECT_ID('Product_GetProductVariant') IS NOT NULL)
	DROP PROCEDURE  Product_GetProductVariant
GO
CREATE PROCEDURE [dbo].[Product_GetProductVariant]
	(
		@ProductIds xml = null,
		@IncludeSku bit = 0,
		@ApplicationId uniqueidentifier
	)
AS
BEGIN
	IF(@ProductIds IS NOT NULL)
	BEGIN

		declare @TabProdIds as table (Id uniqueidentifier)

		insert into @TabProdIds 
		SELECT tab.col.value('text()[1]','uniqueidentifier')    
		FROM @ProductIds.nodes('/GenericCollectionOfGuid/guid') tab(col) 

		 SELECT     
		 [Prd].[Id],    
		 [PrdSeo].ProductIDUser,    
		 [Prd].[Title],    
		 [Prd].[ShortDescription],    
		 [Prd].[LongDescription],    
		 [Prd].[Description],    
		 [Prd].[Keyword],    
		 [Prd].[IsActive],    
		 [Prd].[ProductTypeID],    
		 vPT.[ProductTypeName] AS ProductTypeName,    
		 vPT.[ProductTypePath] +  N'id-' + PrdSeo.ProductIDUser + '/' + Prd.UrlFriendlyTitle AS DefaultUrl, Prd.UrlFriendlyTitle,    
		 vPT.[IsDownloadableMedia] ,
		 [Prd].[ProductStyle],    
		 dbo.ConvertTimeFromUtc([PrdSeo].CreatedDate,@ApplicationId)CreatedDate,    
		 [PrdSeo].CreatedBy,    
		 dbo.ConvertTimeFromUtc([Prd].ModifiedDate,@ApplicationId)ModifiedDate,    
		 [Prd].ModifiedBy,    
		 [PrdSeo].Status,    
		 [PrdSeo].IsBundle,    
		dbo.ConvertTimeFromUtc( [PrdSeo].BundleCompositionLastModified,@ApplicationId) AS BundleCompositionLastModified,    
		 [PrdSeo].SEOFriendlyUrl AS SEOFriendlyUrl,
		 [Prd].TaxCategoryID,    
		 [PrdSeo].PromoteAsNewItem,
		[PrdSeo].PromoteAsTopSeller,
		[PrdSeo].ExcludeFromDiscounts,
		[PrdSeo].ExcludeFromShippingPromotions,
		[PrdSeo].Freight,
		[PrdSeo].Refundable,
		[PrdSeo].TaxExempt,
		[PrdSeo].SEOTitle,
		[PrdSeo].SEOH1,
		[PrdSeo].SEODescription,
		[PrdSeo].SEOKeywords,
		 (Select Count(*) From PRProductSKU where ProductId= [Prd].Id) as NumberOfSKUs    
		FROM PRProductVariant Prd INNER JOIN vwProductTypePath vPT on [Prd].ProductTypeID = vPT.ProductTypeId    
		INNER JOIN PRProduct PrdSeo ON Prd.Id = PrdSeo.Id    
		WHERE [Prd].Id in (SELECT Id from @TabProdIds)
		and @ApplicationId=Prd.SiteId;


		if(@IncludeSku = 1)
		BEGIN
			
			SELECT S.[Id]    
			  ,PRS.[Id] as ProductId
			  ,S.[SiteId]    
			  ,[SKU]    
				,S.[Title]    
			  ,S.[Description]    
			  ,S.[Sequence]    
			  ,S.[IsActive]    
			  ,S.[IsOnline]    
			  ,S.[ListPrice]    
			  ,S.[UnitCost]    
			  ,S.[PreviousSoldCount]    
			   ,[CreatedBy]    
			   ,[CreatedDate]    
			   ,S.[ModifiedBy]    
			   ,S.[ModifiedDate]    
			   ,[Status]    
			   ,S.[Measure]    
			   ,S.[OrderMinimum]    
			   ,S.[OrderIncrement]  
			   ,S.[HandlingCharges] 
				,[Length]
			  ,[Height]
			  ,[Width]
			  ,[Weight]
			  ,S.[SelfShippable]    
			  ,S.[AvailableForBackOrder]
			  ,S.MaximumDiscountPercentage
			  ,S.[BackOrderLimit]
			,S.[IsUnlimitedQuantity]
			FROM [PRProductSKUVariant] S   
			INNER JOIN PRProductSKU PRS ON PRS.Id = S.Id
			WHERE PRS.Id in (SELECT Id FROM @TabProdIds )
			AND @ApplicationId = S.SiteId
		END	
	END
END
GO

PRINT 'Update stored procedure Product_SaveProduct'
GO
IF(OBJECT_ID('Product_SaveProduct') IS NOT NULL)
	DROP PROCEDURE  Product_SaveProduct
GO
CREATE PROCEDURE [dbo].[Product_SaveProduct]
(
	@Id uniqueidentifier output ,
	@ProductCode nvarchar(50),
	@Title	nvarchar(max),
	@ShortDescription nvarchar(max)= '',
	@LongDescription nvarchar(max)= '',
	@Description nvarchar(max),
    @Keywords nvarchar(max)= '',
    @Status int,
	@IsBundle tinyint,
	@IsActive tinyint,
    @ProductTypeId uniqueidentifier,
    @ApplicationId uniqueidentifier,
    @ModifiedBy uniqueidentifier,
	@UrlFriendlyTitle nvarchar(500)=null,
	@TaxCategoryId uniqueidentifier = null,
	@ProductURLReplacementChar char(1) ='-',
	@PromoteAsNewItem bit =0,
	@PromoteAsTopSeller bit =0,
	@ExcludeFromDiscounts bit =0,
	@ExcludeFromShippingPromotions bit=0,
	@Freight bit =0,
	@Refundable bit =0,
	@TaxExempt bit=0,
	@SEOTitle nvarchar(255) = null,
	@SEOH1 nvarchar(255) =null,
	@SEODescription nvarchar(max) =null,
	@SEOKeywords nvarchar(max) =null,
	@SEOFriendlyUrl nvarchar(255) =null
)
as
begin
	Declare @CurrentDate datetime
	set @CurrentDate = GetUTCDate() 

	if (@UrlFriendlyTitle is null OR @UrlFriendlyTitle ='')
	Begin
		set @UrlFriendlyTitle = dbo.MakeFriendlyProductTitle(@Title,@ProductURLReplacementChar)
	End

	if(@Id is null OR @Id = dbo.GetEmptyGUID())
	Begin		
			IF Exists(Select Id from PRProduct Where (ProductIDUser) =(@ProductCode))
		BEGIN
			RAISERROR('Product with this code already exists. Please choose a different product code.', 16, 1)
		END		
		Set @Id = newid()
        insert into PRProduct 
			([Id]
			,[ProductIDUser]
			,[Title], UrlFriendlyTitle 
			,[ShortDescription]
			,[LongDescription]
			,[Description]
			,[Keyword]
			,[IsBundle]
			,[IsActive]
			,[Status]
			,[ProductTypeID]
			,[SiteId]
			,[CreatedDate]
			,[CreatedBy]
			,[TaxCategoryID]
			,PromoteAsNewItem
			,PromoteAsTopSeller
			,ExcludeFromDiscounts
			,ExcludeFromShippingPromotions
			,Freight
			,Refundable
			,TaxExempt
			,SEOTitle
			,SEOH1
			,SEODescription
			,SEOKeywords
			)
		 values
			(@Id,
			@ProductCode,
			@Title, @UrlFriendlyTitle, 
			@ShortDescription,
			@LongDescription,
			@Description,
			@Keywords,
			@IsBundle,
			@IsActive,
			@Status,
			@ProductTypeId,
			@ApplicationId,
			@CurrentDate,
			@ModifiedBy,
			@TaxCategoryId
			,@PromoteAsNewItem
			,@PromoteAsTopSeller
			,@ExcludeFromDiscounts
			,@ExcludeFromShippingPromotions
			,@Freight
			,@Refundable
			,@TaxExempt
			,@SEOTitle
			,@SEOH1
			,@SEODescription
			,@SEOKeywords)

		
		declare @defaultCategoryId uniqueidentifier
		declare @prCategorySequence int
		select @defaultCategoryId = Id from PRCategory where Title='Unassigned'
		select @prCategorySequence = max(isnull(Sequence,0)) from PRProductCategory  
		
		insert into PRProductCategory (Id,ProductId, CategoryId,Sequence) 
					values(newid(), @Id,@defaultCategoryId,@prCategorySequence+1)
		
	end
	else
	begin

		IF Exists(Select Id from PRProduct Where (ProductIDUser) =(@ProductCode) AND Id!=@Id)
		BEGIN
			RAISERROR('Product with this code already exists. Please choose a different product code.', 16, 1)
		END
		
		update PRProduct 
		set	 [ProductIDUser]=@ProductCode
			,[Title] = @Title
			,[UrlFriendlyTitle] = @UrlFriendlyTitle
			,[ShortDescription] =@ShortDescription
			,[LongDescription] =@LongDescription
			,[Description] =@Description
			,[Keyword] =@Keywords
			,[IsActive]=@IsActive
			,[IsBundle] =@IsBundle
			,[Status] =@Status
			,[ProductTypeID] =@ProductTypeId
			,[SiteId]=@ApplicationId
			,[ModifiedDate]=@CurrentDate
			,[ModifiedBy]=@ModifiedBy
			,[TaxCategoryID]=@TaxCategoryId
			,PromoteAsNewItem=@PromoteAsNewItem
			,PromoteAsTopSeller=@PromoteAsTopSeller
			,ExcludeFromDiscounts=@ExcludeFromDiscounts
			,ExcludeFromShippingPromotions=@ExcludeFromShippingPromotions
			,Freight=@Freight
			,Refundable=@Refundable
			,TaxExempt=@TaxExempt
			,SEOTitle=isnull(@SEOTitle,SEOTitle)
			,SEOH1=isnull(@SEOH1,SEOH1)
			,SEODescription=isnull(@SEODescription,SEODescription)
			,SEOKeywords=isnull(@SEOKeywords,SEOKeywords)
		where Id =@Id
		
	
	End
		
	
end
GO

PRINT 'Update stored procedure BLD_ProductImport_ProductVariant'
GO
IF(OBJECT_ID('BLD_ProductImport_ProductVariant') IS NOT NULL)
	DROP PROCEDURE  BLD_ProductImport_ProductVariant
GO
CREATE PROCEDURE [dbo].[BLD_ProductImport_ProductVariant]
	@SiteId				uniqueidentifier,
	@ProductIDUser		nvarchar(100),
	@ProductTypeName	nvarchar(50),
	@ProductTypeId		uniqueidentifier,
	@Title				nvarchar(555),
	@ShortDescription	nvarchar(4000),
	@Description	nvarchar(4000),
	@LongDescription	nvarchar(4000),
	@IsActive			bit,
	@SEOTitle			nvarchar(255),
	@SEODescription		nvarchar(4000),
	@SEOKeywords		nvarchar(4000),
	@SEOFriendlyURL		nvarchar(4000),
	@ModifiedBy			uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	declare @ActionType				int					= 0
	declare @MasterSiteId			uniqueidentifier
	declare @MasterProductId		uniqueidentifier
	declare @MasterProductTypeId	uniqueidentifier
	declare @MasterTitle			nvarchar(555)
	declare @MasterShortDescription nvarchar(4000)
	declare @MasterDescription nvarchar(4000)
	declare @MasterLongDescription	nvarchar(4000)
	declare @MasterIsActive			bit
	declare @MasterSEOTitle			nvarchar(255)
	declare @MasterSEODescription	nvarchar(4000)
	declare @MasterSEOKeywords		nvarchar(4000)
	declare @MasterSEOFriendlyURL	nvarchar(255)
	declare @IsSkipped				bit = 1
	declare @Output					nvarchar(512)

	if coalesce(cast(@SiteId as sql_variant), @ProductIdUser, @ProductTypeId) is null
		raiserror(15600, -1, -1, 'SiteId, ProductIdUser, or ProductTypeId is null')

	BEGIN TRY
        BEGIN TRAN
		
		select @MasterSiteId=Id from SISite where Id=MasterSiteId

		select 
			@MasterProductId=p.Id, 
			@MasterProductTypeId=p.ProductTypeID,
			@MasterTitle=iif(v.Title is null, p.Title, null),
			@MasterShortDescription=iif(v.ShortDescription is null, p.ShortDescription, null),
			@MasterDescription=iif(v.Description is null, p.Description, null),
			@MasterLongDescription=iif(v.LongDescription is null, p.LongDescription, null),
			@MasterIsActive=iif(v.IsActive is null, p.IsActive, null),
			@MasterSEOTitle=iif(v.SEOH1 is null, p.SEOH1, null),
			@MasterSEODescription=iif(v.SEODescription is null, p.SEODescription, null),
			@MasterSEOKeywords=iif(v.SEOKeywords is null, p.SEOKeywords, null),
			@MasterSEOFriendlyURL=iif(v.SEOFriendlyUrl is null, p.SEOFriendlyUrl, null)
		from PRProduct p
			left join PRProductVariant v on p.Id=v.Id and v.SiteId=@SiteId
		where p.ProductIDUser like @ProductIDUser and p.ProductTypeID like @ProductTypeId and p.SiteId=@MasterSiteId

		if @MasterProductId is null or @MasterProductTypeId is null
		begin
			set @Output = concat('Product Id: [', @ProductIDUser, ']/[', @ProductTypeId, '] does not exist.')
			raiserror(15600, -1, -1, @Output)
		end
		else
		begin
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@ShortDescription = case when @MasterShortDescription is null or @ShortDescription <> @MasterShortDescription and @ShortDescription is not null then @ShortDescription else null end,
				@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end,
				@LongDescription = case when @MasterLongDescription is null or @LongDescription <> @MasterLongDescription and @LongDescription is not null then @LongDescription else null end,
				@IsActive = case when @MasterIsActive is null or @IsActive <> @MasterIsActive and @IsActive is not null then @IsActive else null end,
				@SEOTitle = case when @MasterSEOTitle is null or @SEOTitle <> @MasterSEOTitle and @SEOTitle is not null then @SEOTitle else null end,
				@SEODescription = case when @MasterSEODescription is null or @SEODescription <> @MasterSEODescription and @SEODescription is not null then @SEODescription else null end,
				@SEOKeywords = case when @MasterSEOKeywords is null or @SEOKeywords <> @MasterSEOKeywords and @SEOKeywords is not null then @SEOKeywords else null end,
				@SEOFriendlyURL = case when @MasterSEOFriendlyURL is null or @SEOFriendlyURL <> @MasterSEOFriendlyURL and @SEOFriendlyURL is not null then @SEOFriendlyURL else null end

			if exists (select 1 from PRProductVariant where Id=@MasterProductId and SiteId=@SiteId)
			begin
				update PRProductVariant set
					Title=@Title,
					ShortDescription=@ShortDescription,
					LongDescription=@LongDescription,
					Description=@Description,
					IsActive=@IsActive,
					SEOH1=@SEOTitle,
					SEODescription=@SEODescription,
					SEOKeywords=@SEOKeywords,
					SEOFriendlyURL=@SEOFriendlyURL,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getdate()
				where Id=@MasterProductId and SiteId=@SiteId

				set @Output = concat('updated PRProductVariant id=', @MasterProductId)
				set @actionType = 2
			end
			else
			begin
				if coalesce(cast(@Title as sql_variant), @ShortDescription, @LongDescription, @IsActive, @SEOTitle, @SEODescription, @SEOKeywords, @SEOFriendlyURL) is not null
				begin
					insert into PRProductVariant (SiteId, Id, ProductTypeId, Title, ShortDescription, LongDescription, Description, IsActive, SEOH1, SEODescription, SEOKeywords, SEOFriendlyURL, ModifiedBy, ModifiedDate)
						values (@SiteId, @MasterProductId, @MasterProductTypeId, @Title, @ShortDescription, @LongDescription, @Description, @IsActive, @SEOTitle, @SEODescription, @SEOKeywords, @SEOFriendlyURL, @Modifiedby, getdate())
					set @IsSkipped = 0
				end

				set @Output = concat(
					'PRProductVariant ', 
					iif(
						@IsSkipped=1, 
						'skipped', 
						concat('inserted Name=', @Title, ' | product type=', @ProductTypeName)))
				set @actionType = 2
			end

		end
		COMMIT TRAN
		select @MasterProductid, @ActionType, @Output
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		declare @ErrorState int = ERROR_STATE()
		declare @ErrorSeverity int = ERROR_SEVERITY()
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
    END CATCH

END
GO
PRINT 'Update stored procedure Search_GetProducts'
GO
IF(OBJECT_ID('Search_GetProducts') IS NOT NULL)
	DROP PROCEDURE  Search_GetProducts
GO
CREATE PROCEDURE [dbo].[Search_GetProducts]      
(      
		@ApplicationId UNIQUEIDENTIFIER = NULL
)      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
DECLARE @tempXml TABLE(Sno int IDENTITY(1,1),ProductId UNIQUEIDENTIFIER)      
      
INSERT INTO @tempXml(ProductId)      
SELECT DISTINCT ProductId AS Id FROM PRProductSKU WHERE IsActive=1 AND IsOnline=1  
      
 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] AS ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle AS DefaultUrl, S.UrlFriendlyTitle,       
 vPT.[IsDownloadableMedia],S.[ProductStyle],      
 (SELECT Count(*) FROM PRProductSKU WHERE ProductId= S.[Id])AS NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) AS BundleCompositionLastModified,      
 SEOFriendlyUrl,
 [S].TaxCategoryID,
 PromoteAsNewItem,
 PromoteAsTopSeller,
 ExcludeFromDiscounts,
 ExcludeFromShippingPromotions,
 Freight,
 Refundable,
 TaxExempt,
 SEOTitle,
 SEOH1,
 SEODescription,
 SEOKeywords
  FROM @tempXml T      
  INNER JOIN PRProduct S ON S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT ON [S].ProductTypeID = vPT.ProductTypeId      
  
--Order By T.Sno      
  
  
SELECT   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
FROM @tempXml t   
INNER JOIN PRProductAttributeValue SA  ON SA.ProductId=t.ProductId  
LEFT JOIN ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
WHERE SA.Status = dbo.GetActiveStatus()  
  
  
SELECT [Id]    
      ,S.[ProductId]    
      ,S.[SiteId]    
      ,S.[SKU]    
      ,S.[Title]    
      ,S.[Description]    
      ,S.[Sequence]    
      ,S.[IsActive]    
      ,S.[IsOnline]    
      ,S.[ListPrice]    
      ,S.[UnitCost]    
      ,S.[PreviousSoldCount]    
      ,S.[CreatedBy]    
      ,S.[CreatedDate]    
      ,S.[ModifiedBy]    
      ,S.[ModifiedDate]    
      ,S.[Status]    
      ,S.[Measure]    
      ,S.[OrderMinimum]    
      ,S.[OrderIncrement]  
      ,S.[HandlingCharges]  
      ,S.[Length]
	  ,S.[Height]
	  ,S.[Width]
	  ,S.[Weight]
	  ,S.[SelfShippable]    
      ,S.[AvailableForBackOrder]
	  ,S.[BackOrderLimit]
	  ,S.[IsUnlimitedQuantity]
	  ,FreeShipping
	  ,UserDefinedPrice
  FROM [PRProductSKU] S   
  INNER JOIN @tempXml t ON S.ProductId=t.ProductId  
  
  
SELECT   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 ISNULL(AE.[IsDefault], 0) AS [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
FROM @tempXml t   
INNER JOIN PRProductAttribute PA  ON PA.ProductId=t.ProductId  
INNER JOIN PRProductSKU PS ON PA.ProductId = PS.ProductId  
INNER JOIN PRProductSKUAttributeValue SA  ON (SA.AttributeId=PA.AttributeId AND PS.Id = SA.SKUId )  
LEFT JOIN ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
WHERE IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  
 
SELECT  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
FROM @tempXml t   
INNER JOIN PRProductAttribute PA  ON PA.ProductId=t.ProductId   
INNER JOIN ATAttribute A ON PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
WHERE  A.Status=dbo.GetActiveStatus() AND  A.IsSystem=0 AND A.IsFaceted=1
  

SELECT P.Id AS ProductId,	
CASE WHEN Min(MinimumEffectivePrice) IS NULL          THEN MIN(ListPrice) 
     WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
ELSE Min(ListPrice) END  as Price , cast( SUM(PS.PreviousSoldCount) AS INT) AS soldcount, P.CreatedDate		
			FROM			
				dbo.PRProduct P
				INNER JOIN dbo.PRProductSKU PS ON P.Id = PS.ProductId 		
				LEFT JOIN (SELECT * FROM cache_ProductMinEffectivePrice MP ) PP ON P.Id= PP.ProductId 	
				INNER JOIN @tempXml TP  ON P.Id = TP.ProductId 	
			WHERE     
				(PS.IsActive = 1) AND (PS.IsOnline = 1) AND (P.IsActive = 1)     
			GROUP BY     
				P.Id, 
				PP.MinimumEffectivePrice,
				P.CreatedDate,   
				P.ProductIDUser   
END
GO

PRINT 'Migrate data from PRProductSEO_BeforeV70'
GO
Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateSEOTitle'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN

IF(OBJECT_ID('PRProductSEO_BeforeV70') IS NOT NULL)
	UPDATE	P
	SET		SEOTitle = COALESCE(B.Title, P.SEOTitle)
	FROM	PRProduct AS P
			INNER JOIN PRProductSEO_BeforeV70 AS B ON B.ProductId = P.Id AND B.SiteId = P.SiteId
    
INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(),@ScriptName,@Version,GetDate())

END
GO

PRINT 'Update stored procedure Users_ImportBatch'
GO
IF(OBJECT_ID('Users_ImportBatch') IS NOT NULL)
	DROP PROCEDURE  Users_ImportBatch
GO

CREATE PROCEDURE [dbo].[Users_ImportBatch]
(
      @OverwriteExistingContacts BIT ,
      @ModifiedBy UNIQUEIDENTIFIER ,
      @BatchId UNIQUEIDENTIFIER ,
      @BatchUserTable VARCHAR(250) ,
      @BatchMembershipTable VARCHAR(250) ,
      @BatchProfileTable VARCHAR(250) ,
      @BatchSiteUserTable VARCHAR(250) ,
      @BatchUserDistributionListTable VARCHAR(250) ,
      @BatchUSMarketierUserTable VARCHAR(250) ,
      @BatchIndexTermTable VARCHAR(250) ,
      @BatchAddressTable VARCHAR(250) ,
      @OverrideWSUSersAndCustomers BIT = 0
)
AS 
    BEGIN
        DECLARE @BatchTablePrefix VARCHAR(250) ,
            @BatchIdMapTableName VARCHAR(250) ,
            @StrModifiedBy VARCHAR(36) ,
            @ImportedContacts INT ,
            @AddedToDistributionList INT ,
            @ExistingContacts INT ,
            @UpdatedContacts INT
		
        SET @AddedToDistributionList = 0
        SET @ImportedContacts = 0
        SET @UpdatedContacts = 0

		-- Clean up any records that may have have been marked active from a bad import from launch
		UPDATE  U
		SET     U.STATUS = 3
		FROM    dbo.USUser U
        INNER JOIN dbo.MKContact C ON C.Id = U.Id
		WHERE   U.Status = 1

	-- set up temp table name for batch import user id mapping
        SET @BatchTablePrefix = 'ZTMP' + REPLACE(CONVERT(VARCHAR(36), @BatchId),
                                                 '-', '')
        SET @BatchIdMapTableName = @BatchTablePrefix + '_BatchImportIdMap'
        SET @StrModifiedBy = CONVERT(VARCHAR(36), @ModifiedBy)
	
	--start delete duplicate records within the temporary tables if any
        EXEC('delete from ' + @BatchUserTable + '  where ID in (select ID from
        (Select Id, row_number()over(partition by Email order by CreatedDate) as rnum--,Email,FirstName,LastActivityDate
        from ' + @BatchUserTable + '
        Where Email in (
        select Email
        from ' + @BatchUserTable + ' U
        Group by Email
        Having count(*)>1
        )) Duplicates where rnum >1)')
	
        EXEC ('delete from ' + @BatchMembershipTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchProfileTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchSiteUserTable + ' where ContactId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchUserDistributionListTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchUSMarketierUserTable + ' where UserId not in (select Id from '+@BatchUserTable+')')	
        EXEC ('delete from ' + @BatchAddressTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
	--end delete duplicate records within the temporary tables if any


        IF EXISTS ( SELECT  *
                    FROM    sysobjects
                    WHERE   name = @BatchIdMapTableName ) 
            EXEC('Drop Table ' + @BatchIdMapTableName)

	-- Step 1 locate existing records and create mapping
        EXEC(
        'Create Table ' + @BatchIdMapTableName + '(' +
        'importId UniqueIdentifier, ' +
        'actualId UniqueIdentifier, ' +
        'importAddressId UniqueIdentifier, ' +
        'actualAddressId uniqueidentifier ' +
        ')'
        )

        PRINT @BatchIdMapTableName

        EXEC(
        'Create Index ' + @BatchTablePrefix + '_MappingImportId On ' + @BatchIdMapTableName + '(importId)'
        )

        PRINT @BatchTablePrefix 

        PRINT '1a: locate existing users and populate id mapping'
            + @BatchUSMarketierUserTable
	-- 1a: locate existing users and populate id mapping
        EXEC(
        'Insert Into ' + @BatchIdMapTableName + ' (importId, actualId, importAddressId, actualAddressId) ' +
        'Select z.Id, m.UserId, z.AddressId, m.AddressId ' +
        'From ' + @BatchUserTable + ' z ' +			
        'Join vw_contacts m on m.Email = z.Email order by m.contactType ' 
			
        )

        PRINT @BatchMembershipTable 
        PRINT '1b: update batch table user id fields to the existing user''s id'

	-- 1b: update batch table user id fields to the existing user's id
        EXEC(
        'Update ' + @BatchUserTable + ' Set ' +
        'ID = m.actualId ' +
        'From ' + @BatchUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.Id'
        )
		
        PRINT @BatchAddressTable
        EXEC(
        'Update ' + @BatchAddressTable + ' Set ' +
        'ID = isnull(m.actualAddressId, m.importAddressId), UserId = m.actualId ' +
        'From ' + @BatchAddressTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchIdMapTableName 

        EXEC(
        'Update ' + @BatchMembershipTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchMembershipTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchIdMapTableName 


        EXEC(
        'Update ' + @BatchProfileTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchProfileTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchProfileTable

        EXEC(
        'Update ' + @BatchSiteUserTable + ' Set ' +
        'ContactId = m.actualId ' +
        'From ' + @BatchSiteUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.ContactId'
        )

        PRINT @BatchSiteUserTable

        EXEC(
        'Update ' + @BatchUSMarketierUserTable + ' Set ' +
        'UserId = m.actualId, ' +
        'AddressId = m.actualAddressId ' +
        'From ' + @BatchUSMarketierUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchUSMarketierUserTable


        EXEC(
        'Update ' + @BatchUserDistributionListTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchUserDistributionListTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchUserDistributionListTable

	-- Insert Index term
        DECLARE @idxTerms INT
        CREATE TABLE #tmpIdxTerms ( recCount INT )

        EXEC('insert into #tmpIdxTerms (recCount) select count(*) from ' + @BatchIndexTermTable)

        SELECT  @idxTerms = recCount
        FROM    #tmpIdxTerms
        IF @idxTerms > 0 
            BEGIN
                EXEC Users_ImportIndexTerms @BatchIndexTermTable,
                    @BatchSiteUserTable
            END
        DROP TABLE #tmpIdxTerms

	-- To get existing records count
        DECLARE @ParmDefinition NVARCHAR(100) ;
        DECLARE @ExistingContactsStr NVARCHAR(30)
        DECLARE @query NVARCHAR(500)
        
        SET @query = 'select @result =count(U.Id) from MKContact U inner join '
            + @BatchUserTable + ' z on z.id = u.id'
        SET @ParmDefinition = N'@result varchar(30) OUTPUT' ;	 
        EXEC sp_executesql @query, @ParmDefinition,
            @result = @ExistingContactsStr OUTPUT
        SET @ExistingContacts = CAST(@ExistingContactsStr AS INT)
        
        PRINT 'remove system users'

-- remove commerce users
        PRINT 'remove commerce users'
        IF ( @OverrideWSUSersAndCustomers <> 1 ) 
            BEGIN
                PRINT 'DELETE @BatchUserTable'
                EXEC ('Delete ' + @BatchUserTable + ' Where Id in(select userid from vw_contacts where contactType in(1,2,3))')
                PRINT 'DELETE @BatchMembershipTable'
                EXEC ('Delete ' + @BatchMembershipTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2,3))')
                PRINT 'DELETE @BatchProfileTable'
                EXEC ('Delete ' + @BatchProfileTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2,3))')
                PRINT 'DELETE @BatchSiteUserTable'
                EXEC ('Delete ' + @BatchSiteUserTable + ' Where ContactId in(select userid from vw_contacts where contactType in(1,2,3))')
--exec ('Delete ' + @BatchUserDistributionListTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchUSMarketierUserTable'
                EXEC ('Delete ' + @BatchUSMarketierUserTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2,3))')
                PRINT 'DELETE @BatchAddressTable'
                EXEC ('Delete ' + @BatchAddressTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2,3))')
            END
	
        PRINT 'Step 2: Update records for existing users'
	-- Step 2: Update records for existing users
        IF ( @OverwriteExistingContacts = 1
             OR @OverrideWSUSersAndCustomers = 1
           ) 
            BEGIN
		-- Step 2a: User Record
                EXEC(
                'Update MKContact Set ' +
                'FirstName = ISNULL(z.FirstName, u.FirstName), MiddleName = ISNULL(z.Middlename, u.MiddleName), LastName = ISNULL(Z.LastName, u.LastName), '+
                'CompanyName = ISNULL(z.CompanyName, u.CompanyName), BirthDate = ISNULL(z.BirthDate, u.BirthDate), Gender = ISNULL(z.Gender, u.Gender), ' +
                'HomePhone = ISNULL(z.HomePhone, u.HomePhone), MobilePhone = ISNULL(z.MobilePhone, u.MobilePhone), OtherPhone = ISNULL(z.OtherPhone, u.OtherPhone), ' +
				'LeadScore = ISNULL(z.LeadScore, u.LeadScore), AddressId = ISNULL(z.AddressId, u.AddressId), '+
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'ModifiedDate = GetDate(), Status=dbo.GetActiveStatus() ' + 
                'From MKContact u ' +
                'join ' + @BatchUserTable + ' z on z.id = u.id'
                )
			
                SET @UpdatedContacts = (SELECT @@ROWCOUNT)
            
                PRINT 'Step 2a Must update User records outside of MKContact'
		--Step 2a Must update User records outside of MKContact
                EXEC(
                'Update USUser Set ' +
                'FirstName = ISNULL(z.FirstName, u.FirstName), MiddleName = ISNULL(z.Middlename, u.MiddleName), LastName = ISNULL(Z.LastName, u.LastName), '+
                'CompanyName = ISNULL(z.CompanyName, u.CompanyName), BirthDate = ISNULL(z.BirthDate, u.BirthDate), Gender = ISNULL(z.Gender, u.Gender), ' +
                'HomePhone = ISNULL(z.HomePhone, u.HomePhone), MobilePhone = ISNULL(z.MobilePhone, u.MobilePhone), OtherPhone = ISNULL(z.OtherPhone, u.OtherPhone), LeadScore = ISNULL(z.LeadScore, u.LeadScore), '+
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'ModifiedDate = GetDate(), Status=dbo.GetActiveStatus() ' + 
                'From USUser u ' +
                'join ' + @BatchUserTable + ' z on z.id = u.id ' +
				'LEFT JOIN MKContact MK ON MK.Id = U.Id WHERE MK.Id IS NULL'
                )
			
                SET @UpdatedContacts = ( SELECT @@ROWCOUNT) + @UpdatedContacts
                SET @ImportedContacts = ( @ImportedContacts + @UpdatedContacts )	
		
		
                PRINT 'UPDATE GLADDRESS'
                EXEC(
                'Update GLAddress Set ' +
                'AddressType = z.AddressType, ' + 
                'AddressLine1 = z.AddressLine1, ' +
                'AddressLine2 = z.AddressLine2, ' +
                'City = z.City, ' +
                'StateId = s.Id, ' +
                'Zip = z.Zip, ' +
                'CountryId = c.Id, ' +
                'ModifiedDate = z.ModifiedDate, ' +
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'Status = z.Status ' +
                'From GLAddress u ' +
                'join ' + @BatchAddressTable + ' z on z.Id = u.Id ' +
                'join GLCountry c on c.CountryName = z.Country ' +
				'join GLState s on (s.StateCode = z.State or s.State = z.State) and s.CountryId = c.Id'
                )
				
            END
        PRINT 'Insert Records for users that do not exist'
	-- Step 3: Insert Records for users that do not exist
	-- Step 3a: User record
        EXEC(
        'INSERT INTO dbo.MKContact
        ( Id ,
        Email ,
        FirstName ,
        MiddleName ,
        LastName ,
        CompanyName ,
        Gender ,
        BirthDate ,
        HomePhone ,
        MobilePhone ,
        OtherPhone ,
        ImageId ,
        AddressId ,
        Notes ,
		LeadScore,
        ContactSourceId ,
        Status ,
        CreatedBy ,
        CreatedDate ,
        ModifiedBy ,
        ModifiedDate ,
        LastSynced
        ) ' +
        'select 
        z.Id ,
        z.Email ,
        z.FirstName ,
        z.MiddleName ,
        z.LastName ,
        z.CompanyName ,
        z.Gender ,
        z.BirthDate ,
        z.HomePhone ,
        z.MobilePhone ,
        z.OtherPhone ,
        z.ImageId ,
        z.AddressId ,
        z.Notes ,
		z.LeadScore,
        z.ContactSourceId ,
        z.Status ,
        z.CreatedBy ,
        z.CreatedDate ,
        z.ModifiedBy ,
        ISNULL(z.ModifiedDate, Z.CreatedDate) ,
        z.LastSynced ' +
        'from ' + @BatchUserTable + ' z ' +
        'Left Outer Join vw_contacts u on u.UserId = z.Id ' +
        'Where u.UserId is null'
        )
        SET @ImportedContacts = ( @ImportedContacts + ( SELECT
                                                              @@ROWCOUNT
                                                      ) )


		--Insert into TRKContact for denorm process, deleting before inserting 
		EXEC ('DELETE FROM TRKContact WHERE ContactId IN (SELECT Z.Id FROM ' + @BatchUserTable + ' Z)') 
		EXEC ('INSERT INTO TRKContact (ContactId, CreatedDate, Source, Status, SiteDenormStatus, AttributeDenormStatus)
		SELECT Z.Id, GetDate(), ''MKContact'',1,0,0 FROM ' + @BatchUserTable + ' Z')


		--Inserting for all new contact as the lead score
		EXEC ('insert into dbo.TREventQueue(Id,VisitorId,EventType,ObjectId,DataEnvelope,CreatedDate,SiteId,ContactId) 
		select newid(), Z.Id, 304, ''11111111-0000-0000-0000-000000000001'', ''<int>'' + CONVERT(varchar(24),Z.LeadScore) + ''</int>'', GetDate(), b.SiteId, Z.Id FROM ' + @BatchUserTable + ' Z Join ' +
		@BatchSiteUserTable  +  ' b ON b.ContactId = Z.Id where Z.LeadScore IS NOT NULL' 
		)

        PRINT 'Insert into GLAddress'
        EXEC('insert into GLAddress(Id, AddressType, AddressLine1, AddressLine2, City, StateId, Zip, CountryId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status) ' +
        'select ISNULL(z.Id, NEWID()), z.AddressType, z.AddressLine1, z.AddressLine2, z.City, s.Id, z.Zip, c.Id, z.CreatedDate, z.CreatedBy, null, null, z.Status ' +
        'from ' + @BatchAddressTable + ' z ' +
        'left outer join GLCountry c on c.CountryName = z.Country ' +
        'left outer join GLState s on (s.StateCode = z.State or s.State = z.State) and s.CountryId = c.Id ' +
        'left outer join GLAddress a on a.Id = z.Id ' +
        'Where a.Id is null '
        )

        EXEC ('UPDATE MKContact SET AddressId = ISNULL(z.Id, NEWID()) ' + 
        'from  ' + @BatchAddressTable + '  z  '+
        'INNER Join MKContact u on u.Id = z.userId ')


	---------------------ATTRIBUTE INSERT-----------------------
        EXEC(
        'SELECT * FROM '+@BatchProfileTable+'')
		
             
                    
        CREATE TABLE #tmpContactAttributes
            (
              AttributeId UNIQUEIDENTIFIER ,
              AttributeEnumId UNIQUEIDENTIFIER ,
              VALUE NVARCHAR(4000) ,
              UserId UNIQUEIDENTIFIER
            )
                    
                    
        PRINT 'insert into #tmpContactAttributes'
        INSERT  INTO #tmpContactAttributes
                EXEC
                    ( '	 SELECT  
                A.Id AS AttributeId ,
                AE.Id AS AttributeEnumId ,
                P.PropertyValueString AS Value ,
                P.UserId                 
                FROM   ' + @BatchProfileTable
                      + ' P
                INNER JOIN dbo.ATAttribute A ON A.Title = P.PropertyName
                INNER JOIN dbo.ATAttributeCategoryItem ACI ON ACI.AttributeId = A.Id
                INNER JOIN dbo.ATAttributeCategory AC ON AC.ID = ACI.CategoryId
                AND AC.Name = ''Contact Attributes''
                LEFT JOIN dbo.ATAttributeEnum AE ON AE.AttributeID = A.Id
                AND AE.Title = P.PropertyValueString
                WHERE  ( A.IsEnum = 1
                AND AE.ID IS NOT NULL
                )
                OR A.IsEnum = 0'
                    )
 
 
        DELETE  CAV
        FROM    dbo.ATContactAttributeValue CAV
                INNER JOIN #tmpContactAttributes TCA ON TCA.AttributeId = CAV.AttributeId
                                                        AND TCA.UserId = CAV.ContactId
 
        INSERT  INTO dbo.ATContactAttributeValue
                ( Id ,
                  ContactId ,
                  AttributeId ,
                  AttributeEnumId ,
                  Value ,
                  Notes ,
                  CreatedDate ,
                  CreatedBy
         
                        
                )
                SELECT  NEWID() ,
                        UserId ,
                        AttributeId ,
                        AttributeEnumId ,
                        Value ,
                        '' ,
                        GETUTCDATE() ,
                        '6CB2B06E-C6D6-455B-BBEA-E4162A7692D3'
                FROM    #tmpContactAttributes
						
		
		-------------------------------------------------------------


        PRINT ' Step 3d: Site User Record'
	-- Step 3d: Site User Record
        EXEC(
        'insert into MKContactSite (SiteId, ContactId) ' +
        'select z.SiteId, z.ContactId ' +
        'from ' + @BatchSiteUserTable + ' z ' +
        ' inner join MKContact MC ON MC.Id = z.ContactId'+
        ' left outer join MKContactSite u on u.ContactId = z.ContactId ' +
        'where u.ContactId is null '
        )

		 --This is to add it the current site if contact already present in different site 
        EXEC(
        'insert into MKContactSite (SiteId, ContactId, IsPrimarySite )'+  
		' select distinct z.SiteId SiteId, z.ContactId ContactId, 0  from ' + @BatchSiteUserTable + ' z ' + 
		' inner join MKContact MC ON MC.Id = z.ContactId' + 
		' where z.SiteId not in (Select SiteId from MKContactSite where ContactId = z.ContactId)')

		 EXEC(
        'insert into USSiteUser (SiteId, UserId,  IsSystemUser, ProductId, IsPrimarySite )'+  
		' select distinct z.SiteId SiteId, z.ContactId ContactId, 0, ''CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E'', 0  from ' + @BatchSiteUserTable + ' z ' + 
		' inner join USUser MC ON MC.Id = z.ContactId' + 
		' where z.SiteId not in (Select SiteId from USSiteUser where UserId = z.ContactId)')


	-- Step 3e: Distribution List Record

        PRINT 'Update ' + @BatchUserDistributionListTable + ' Set '
            + '  ListId = null ' + 'From ' + @BatchUserDistributionListTable
            + ' z '
            + 'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'


        EXEC(
        'Update ' + @BatchUserDistributionListTable + ' Set ' +
        ' ListId = null ' +
        ' from  ' + @BatchUserDistributionListTable + ' z ' +
        'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'
        )
        SET @AddedToDistributionList = ( SELECT @@ROWCOUNT
                                       )

        PRINT 'insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) '
            + ' select newid(),z.ListId,z.UserId ' + 'from '
            + @BatchUserDistributionListTable + ' z '
            + 'Left Outer Join TADistributionListUser u on u.UserId = z.UserId '
            + 'Where u.UserId is null and z.ListId is not null '


        EXEC (';With ListCTE AS ('+
        ' select distinct z.ListId,z.UserId ' + 
        'from ' + @BatchUserDistributionListTable + ' z ' +
        'Join vw_contacts u on u.userid = z.UserId ' +
        'Where z.ListId is not null) ' +
		'insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) select newid(), Listid, UserId from ListCTE ')
        SET @AddedToDistributionList = ( @AddedToDistributionList + ( SELECT
                                                              @@ROWCOUNT
                                                              ) )
         EXEC (';With ListEventCTE AS ('+
        ' select distinct z.ListId,z.UserId , t.ApplicationId ' + 
        'from ' + @BatchUserDistributionListTable + ' z ' +
		'Join TADistributionLists t on t.Id = z.ListId ' +
        'Where z.ListId is not null) ' +
		'insert into dbo.TREventQueue(Id,VisitorId,EventType,ObjectId,DataEnvelope,CreatedDate,SiteId,ContactId) 
		select newid(), UserId, 501, ListId, null, GetDate(), ApplicationId, UserId from ListEventCTE ')


        INSERT  INTO TADistributionListSite
                ( Id ,
                  DistributionListId ,
                  SiteId
                )
                SELECT  NEWID() ,
                        *
                FROM    ( SELECT    Id ,
                                    ApplicationId
                          FROM      TADistributionLists
                          EXCEPT
                          SELECT    DistributionListId ,
                                    SiteId
                          FROM      TADistributionListSite
                        ) T


        EXEC (' UPDATE TLS
        SET    TLS.COUNT = TV.ContactCount
        FROM   ( SELECT    DLU.DistributionListId ,
        COUNT(DLU.Id) AS ContactCount
        FROM      dbo.TADistributionListUser DLU
        INNER JOIN dbo.TADistributionListSite TLS ON DLU.DistributionListId = TLS.DistributionListId
        GROUP BY  DLU.DistributionListId
        ) TV
        INNER JOIN dbo.TADistributionListSite TLS ON TLS.DistributionListId = TV.DistributionListId
        INNER JOIN ' + @BatchUserDistributionListTable + ' z ON z.ListId = TV.DistributionListId' )

		SET @query = 'select @result =count(U.Id) from USUser U inner join '
            + @BatchUserTable + ' z on z.id = u.id'
        SET @ParmDefinition = N'@result varchar(30) OUTPUT' ;	 
        EXEC sp_executesql @query, @ParmDefinition,
            @result = @ExistingContactsStr OUTPUT
        SET @ExistingContacts = CAST(@ExistingContactsStr AS INT) +@ExistingContacts	

        UPDATE  UploadContactData
        SET     ImportedContacts = @ImportedContacts ,
                AddedToDistributionList = @AddedToDistributionList ,
                ExistingRecords = @ExistingContacts ,
                UpdatedContacts = @UpdatedContacts
        WHERE   UploadHistoryId = @BatchId
    END
GO

IF(OBJECT_ID('vwProductSearch') IS NOT NULL)
	DROP VIEW vwProductSearch
GO
CREATE VIEW [dbo].[vwProductSearch]  
AS  
SELECT P.*,  
	SKU.SKU,   
	SKU.Title AS SkuTitle
FROM vwProduct AS P
	LEFT JOIN vwSKU AS SKU ON SKU.ProductId = P.Id AND SKU.SiteId = P.SiteId  


GO

IF(OBJECT_ID('vwNavProductSearch') IS NOT NULL)
	DROP VIEW vwNavProductSearch
GO
CREATE VIEW [dbo].[vwNavProductSearch]  
AS  
SELECT P.*,  
	SKU.SKU,   
	SKU.Title AS SkuTitle
FROM vwNavProduct AS P
	LEFT JOIN vwSKU AS SKU ON SKU.ProductId = P.Id AND SKU.SiteId = P.SiteId 

GO


IF(OBJECT_ID('vwNavProduct') IS NOT NULL)
	DROP VIEW vwNavProduct
GO
CREATE VIEW vwNavProduct
AS
SELECT N.NavNodeId NavId,
	N.NavNodeUrl,
	FO.SerialNo AS DisplayOrder,
	CASE WHEN E.Id IS NULL THEN 0 ELSE 1 END AS ExcludedFromNav,
	P.*
FROM NVFilterOutput FO
	INNER JOIN NVNavNodeNavFilterMap N ON FO.QueryId = N.QueryId
	INNER JOIN vwProduct P ON FO.ObjectId = P.Id
	LEFT JOIN NVFilterExclude E ON N.QueryId = E.QueryId AND P.Id = E.ObjectId

GO
IF(OBJECT_ID('vwFeatureProduct') IS NOT NULL)
	DROP VIEW vwFeatureProduct
GO
CREATE VIEW vwFeatureProduct
AS
SELECT FO.FeatureId,
	CASE WHEN E.FeatureId IS NULL THEN 0 ELSE 1 END AS ExcludedFromFeature,
	0 AS Sequence,
	P.*
FROM MRFeatureOutput FO
	INNER JOIN MRFeature F ON F.Id = FO.FeatureId
	INNER JOIN vwProduct P ON FO.ProductId = P.Id
	LEFT JOIN MRFeatureAutoExcludedItems E ON FO.FeatureId = E.FeatureId AND P.Id = E.ProductId
WHERE F.FeatureTypeId != 3

UNION

SELECT FO.FeatureId,
	NULL,
	FO.Sequence,
	P.*
FROM MRFeatureManualItems FO
	INNER JOIN MRFeature F ON F.Id = FO.FeatureId
	INNER JOIN vwProduct P ON FO.ProductId = P.Id
WHERE F.FeatureTypeId = 3
GO

IF(OBJECT_ID('vwFeatureProductSearch') IS NOT NULL)
	DROP VIEW vwFeatureProductSearch
GO
CREATE VIEW [dbo].[vwFeatureProductSearch]  
AS  
SELECT P.*,  
	SKU.SKU,   
	SKU.Title AS SkuTitle
FROM vwFeatureProduct AS P
	LEFT JOIN vwSKU AS SKU ON SKU.ProductId = P.Id AND SKU.SiteId = P.SiteId  
GO
IF(OBJECT_ID('vwObjectLanguageVariants') IS NOT NULL)
	DROP VIEW vwObjectLanguageVariants
GO
CREATE VIEW [dbo].[vwObjectLanguageVariants] (MasterObjectId, VariantObjectId, ObjectId, ObjectType, SiteId, Culture, IsAutomatic, [Url])
AS
WITH VariantPages (MasterPageId, SitePageId, SiteId)
AS (
	SELECT	PD.PageDefinitionId, PD.PageDefinitionId, PD.SiteId
	FROM	PageDefinition AS PD
			LEFT JOIN PageDefinition AS PD2 ON PD2.PageDefinitionId = PD.SourcePageDefinitionId
	WHERE	NULLIF(PD.SourcePageDefinitionId, '00000000-0000-0000-0000-000000000000') IS NULL OR
			PD2.PageDefinitionId IS NULL

	UNION ALL

	SELECT	VP.MasterPageId, PD.PageDefinitionId, PD.SiteId
	FROM	PageDefinition AS PD
			INNER JOIN VariantPages AS VP ON VP.SitePageId = PD.SourcePageDefinitionId
)
SELECT		VP1.MasterPageId, PD.PageDefinitionId, VP1.SitePageId, 8, PD.SiteId, NULLIF(S.UICulture, ''), 1,
			CASE WHEN RIGHT(S.PrimarySiteUrl, 1) = '/' THEN LEFT(S.PrimarySiteUrl, LEN(S.PrimarySiteUrl) - 1) ELSE S.PrimarySiteUrl END +
			CASE
				WHEN PMN.TargetId = PD.PageDefinitionId THEN PMN.CompleteFriendlyUrl
				ELSE PMN.CompleteFriendlyUrl + '/' + PD.FriendlyName
			END
FROM		VariantPages AS VP1
			INNER JOIN VariantPages AS VP2 ON VP2.MasterPageId = VP1.MasterPageId
			INNER JOIN PageDefinition AS PD ON PD.PageDefinitionId = VP2.SitePageId
			INNER JOIN PageMapNodePageDef AS PMNPD ON PMNPD.PageDefinitionId = PD.PageDefinitionId
			INNER JOIN PageMapNode AS PMN ON PMN.PageMapNodeId = PMNPD.PageMapNodeId
			INNER JOIN SISite AS S ON S.Id = PD.SiteId
WHERE		S.[Status] = 1 AND
			PD.PageStatus = 8 AND
			PMN.MenuStatus = 0 AND
			NULLIF(PMN.CompleteFriendlyUrl, '') IS NOT NULL AND
			NULLIF(S.UICulture, '') IS NOT NULL

UNION ALL

SELECT		DISTINCT VP1.MasterPageId, OLV.Id, VP1.SitePageId, 8, NULL, OLV.Culture, 0, OLV.VariantUrl
FROM		VariantPages AS VP1
			INNER JOIN VariantPages AS VP2 ON VP2.MasterPageId = VP1.MasterPageId
			INNER JOIN ObjectLanguageVariants AS OLV ON OLV.ObjectType = 8 AND OLV.ObjectId = VP2.SitePageId
WHERE		OLV.VariantUrl IS NOT NULL AND
			OLV.Culture IS NOT NULL

UNION ALL

SELECT		DISTINCT VP1.MasterPageId, OLV.Id, VP1.SitePageId, 8, PD.SiteId, NULLIF(S.UICulture, ''), 0,
			CASE WHEN RIGHT(S.PrimarySiteUrl, 1) = '/' THEN LEFT(S.PrimarySiteUrl, LEN(S.PrimarySiteUrl) - 1) ELSE S.PrimarySiteUrl END +
			CASE
				WHEN PMN.TargetId = PD.PageDefinitionId THEN PMN.CompleteFriendlyUrl
				ELSE PMN.CompleteFriendlyUrl + '/' + PD.FriendlyName
			END
FROM		VariantPages AS VP1
			INNER JOIN VariantPages AS VP2 ON VP2.MasterPageId = VP1.MasterPageId
			INNER JOIN ObjectLanguageVariants AS OLV ON OLV.ObjectType = 8 AND OLV.ObjectId = VP2.SitePageId
			INNER JOIN PageDefinition AS PD ON PD.PageDefinitionId = OLV.VariantId
			INNER JOIN PageMapNodePageDef AS PMNPD ON PMNPD.PageDefinitionId = PD.PageDefinitionId
			INNER JOIN PageMapNode AS PMN ON PMN.PageMapNodeId = PMNPD.PageMapNodeId
			INNER JOIN SISite AS S ON S.Id = PD.SiteId
WHERE		OLV.VariantId IS NOT NULL AND
			NULLIF(S.UICulture, '') IS NOT NULL

UNION ALL

SELECT		DISTINCT P.Id, P.Id, P.Id, 205, S.Id, NULLIF(S.UICulture, '') AS Culture, 1, 
			CASE WHEN RIGHT(S.PrimarySiteUrl, 1) = '/' THEN LEFT(S.PrimarySiteUrl, LEN(S.PrimarySiteUrl) - 1) ELSE S.PrimarySiteUrl END +
			CASE WHEN LEFT(P.ProductUrl, 1) = '/' THEN P.ProductUrl ELSE '/' + P.ProductUrl END
FROM		vwProduct AS P
			INNER JOIN SISite AS S ON S.Id = P.SiteId
WHERE		S.[Status] = 1 AND
			P.IsActive = 1 AND
			NULLIF(P.ProductUrl, '') IS NOT NULL

UNION ALL

SELECT		DISTINCT P.Id, OLV.Id, P.Id, 205, NULL, OLV.Culture, 0, OLV.VariantUrl
FROM		vwProduct AS P
			INNER JOIN SISite AS S ON S.Id = P.SiteId
			INNER JOIN ObjectLanguageVariants AS OLV ON OLV.ObjectType = 205 AND OLV.ObjectId = P.Id
WHERE		S.[Status] = 1 AND
			P.IsActive = 1 AND
			NULLIF(P.ProductUrl, '') IS NOT NULL AND
			OLV.VariantUrl IS NOT NULL AND
			OLV.Culture IS NOT NULL


GO

IF(OBJECT_ID('vwVariantObjects') IS NOT NULL)
	DROP VIEW vwVariantObjects
GO
CREATE VIEW [dbo].[vwVariantObjects] (MasterObjectId, VariantObjectId, ObjectId, ObjectType, SiteId, Culture, [Url])
AS
WITH VariantPages (MasterPageId, SitePageId, SiteId)
AS (
	SELECT	PD.PageDefinitionId, PD.PageDefinitionId, PD.SiteId
	FROM	PageDefinition AS PD
			LEFT JOIN PageDefinition AS PD2 ON PD2.PageDefinitionId = PD.SourcePageDefinitionId
	WHERE	NULLIF(PD.SourcePageDefinitionId, '00000000-0000-0000-0000-000000000000') IS NULL OR
			PD2.PageDefinitionId IS NULL

	UNION ALL

	SELECT	VP.MasterPageId, PD.PageDefinitionId, PD.SiteId
	FROM	PageDefinition AS PD
			INNER JOIN VariantPages AS VP ON VP.SitePageId = PD.SourcePageDefinitionId
)
SELECT		VP1.MasterPageId, PD.PageDefinitionId, VP1.SitePageId, 8, PD.SiteId, NULLIF(S.UICulture, ''),
			S.PrimarySiteUrl +
			CASE
				WHEN PMN.TargetId = PD.PageDefinitionId THEN PMN.CompleteFriendlyUrl
				ELSE PMN.CompleteFriendlyUrl + '/' + PD.FriendlyName
			END
FROM		VariantPages AS VP1
			INNER JOIN VariantPages AS VP2 ON VP2.MasterPageId = VP1.MasterPageId
			INNER JOIN PageDefinition AS PD ON PD.PageDefinitionId = VP2.SitePageId
			INNER JOIN PageMapNodePageDef AS PMNPD ON PMNPD.PageDefinitionId = PD.PageDefinitionId
			INNER JOIN PageMapNode AS PMN ON PMN.PageMapNodeId = PMNPD.PageMapNodeId
			INNER JOIN SISite AS S ON S.Id = PD.SiteId
WHERE		S.[Status] = 1 AND
			PD.PageStatus = 8 AND
			PMN.MenuStatus = 0 AND
			NULLIF(PMN.CompleteFriendlyUrl, '') IS NOT NULL

UNION ALL

SELECT		DISTINCT P.Id, P.Id, P.Id, 205, S.Id, NULLIF(S.UICulture, '') AS Culture, S.PrimarySiteUrl + P.ProductUrl
FROM		vwProduct AS P
			INNER JOIN SISite AS S ON S.Id = P.SiteId
WHERE		S.[Status] = 1 AND
			P.IsActive = 1 AND
			NULLIF(P.ProductUrl, '') IS NOT NULL

GO
PRINT 'Creating SiteSettings_GetSettings'
GO
IF(OBJECT_ID('SiteSettings_GetSettings') IS NOT NULL)
	DROP PROCEDURE  SiteSettings_GetSettings
GO
CREATE PROCEDURE [dbo].[SiteSettings_GetSettings](@siteId uniqueidentifier)    
AS    
BEGIN  
SELECT     
 SG.[Name] as [GroupName],    
 SG.[FriendlyName] as [GroupFriendlyName],    
 SG.[Sequence] as [GroupSequence],    
 ST.[Name] as [TypeName],    
 ST.[FriendlyName] as [TypeFriendlyName],    
 ST.[Sequence] as [TypeSequence],    
 case when ST.Name='LocalTimeZone' then SI.TimeZone else SS.[Value] end Value,    
 SS.SettingTypeId,    
 SS.SiteId    
FROM STSiteSetting SS     
 inner join STSettingType ST on SS.SettingTypeId = ST.Id     
 inner join STSettingGroup SG on ST.SettingGroupId=SG.Id    
 inner join SISite SI on SS.SiteId = SI.Id    
--WHERE SS.SiteId=@siteId or SI.ParentSiteId = @siteId    
ORDER BY SS.SiteId, SG.Sequence, ST.[Name]--, ST.Sequence    
  
END	
GO


PRINT 'Creating Order_Save'
GO
IF(OBJECT_ID('Order_Save') IS NOT NULL)
	DROP PROCEDURE  Order_Save
GO
CREATE PROCEDURE [dbo].[Order_Save]  
(   
 @Id uniqueidentifier output,  
 @CartId uniqueidentifier,  
 @OrderTypeId int,  
 @OrderStatusId int,  
 @PurchaseOrderNumber nvarchar(100),  
 @ExternalOrderNumber nvarchar(100),
 @SiteId uniqueidentifier,  
 @CustomerId uniqueidentifier,  
 @TotalTax money,  
 --@ShippingTotal money,
 @OrderTotal money,  
 @CODCharges money,
 @TaxableOrderTotal money,  
 @GrandTotal money,
 @TotalShippingCharge money,
 @TotalDiscount money,
 @PaymentTotal money,
 @PaymentRemaining money out,  
 @OrderDate datetime,  
 --@CreatedDate datetime,  
 --@CreatedBy uniqueidentifier,  
 --@ModifiedDate datetime,  
 @ModifiedBy uniqueidentifier,
 @TotalShippingDiscount money,
 @ApplicationId uniqueidentifier	,
 @AddedAttributes	xml=null,
 @CouponIds xml=null

)  
  
AS  
BEGIN       
  
DECLARE @CreatedDate datetime, @ModifiedDate DateTime

SET @SiteId = isnull(@SiteId,@ApplicationId)


if(@CartId IS NULL OR @CartId = dbo.GetEmptyGUID())
	SET @CartId = NULL;
ELSE IF @Id is null  OR @Id = dbo.GetEmptyGUID()  
		SELECT @Id =Id from OROrder Where CartId=@CartId

if @CODCharges is null 
	set @CODCharges = 0

 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin  
  set @Id = newid()  
  set @CreatedDate = GetUTCDate()



  INSERT INTO OROrder  
           ([Id]  
   ,[CartId]  
   ,[OrderTypeId]  
   ,[OrderStatusId]  
   ,[PurchaseOrderNumber]  
	,[ExternalOrderNumber]
   ,[SiteId]  
   ,[CustomerId]  
   ,[TotalTax]  
   --,[ShippingTotal]  
   ,[OrderTotal]  
   ,[CODCharges]
   ,[TaxableOrderTotal]  
   ,[GrandTotal] 
   ,[TotalShippingCharge] 
   ,[TotalDiscount] 
   ,[PaymentTotal] 
   ,[RefundTotal]
   -- ,[PaymentRemaining]   -- This is changed to calculated columns
   ,[OrderDate]  
   ,[CreatedDate]  
   ,[CreatedBy]  
   ,[ModifiedDate]  
   ,[ModifiedBy]
   ,[TotalShippingDiscount])  
     VALUES  
           (@Id,  
   @CartId,  
   @OrderTypeId,  
   @OrderStatusId,  
   @PurchaseOrderNumber, 
	@ExternalOrderNumber, 
   @SiteId,  
   @CustomerId,  
   @TotalTax,  
   --@ShippingTotal,
   @OrderTotal,  
	@CODCharges,
   @TaxableOrderTotal,  
   @GrandTotal, 
   @TotalShippingCharge, 
   @TotalDiscount, 
   dbo.Order_GetPaymentTotal(@Id), 
   dbo.Order_GetRefundTotal(@Id),
  -- @PaymentRemaining,  -- This is changed to calculated columns
   dbo.ConvertTimeToUtc(@OrderDate,@SiteId),  
   @CreatedDate,  
   @ModifiedBy,  
   @CreatedDate,  
   @ModifiedBy,
   @TotalShippingDiscount)  
   		
		if @AddedAttributes is not null
			begin
				Insert into OROrderAttributeValue(Id, OrderId, 
													AttributeId, AttributeEnumId, 
													Value, CreatedDate,CreatedBy)
				Select newId(), @Id, tab.col.value('(AttributeId)[1]','uniqueidentifier'),
													tab.col.value('(AttributeEnumId)[1]','uniqueidentifier'),
													tab.col.value('(Value)[1]','nvarchar(max)'), @CreatedDate,@ModifiedBy
				From @AddedAttributes.nodes('GenericCollectionOfAssignableAttributeValueTuple/AssignableAttributeValueTuple') tab(col)

			end

 end  
 else  
 begin  
  update OROrder  
  set CartId=@CartId,  
   OrderTypeId=@OrderTypeId,  
   OrderStatusId=@OrderStatusId,  
   PurchaseOrderNumber=@PurchaseOrderNumber,  
   ExternalOrderNumber =@ExternalOrderNumber,
   SiteId=isnull(SiteId,@SiteId),  
   CustomerId=@CustomerId,  
   TotalTax=@TotalTax,  
   --ShippingTotal=@ShippingTotal,  
   OrderTotal=@OrderTotal,  
	CODCharges=@CODCharges,
   TaxableOrderTotal=@TaxableOrderTotal,  
   GrandTotal=@GrandTotal,
   TotalShippingCharge=@TotalShippingCharge,
   TotalDiscount=@TotalDiscount,
   PaymentTotal=dbo.Order_GetPaymentTotal(@Id),
   RefundTotal=dbo.Order_GetRefundTotal(@Id),
 --  PaymentRemaining=@PaymentRemaining,   -- This is changed to calculated columns
   OrderDate=dbo.ConvertTimeToUtc(@OrderDate,@SiteId),  
   --CreatedDate=@CreatedDate,  
   --CreatedBy=@CreatedBy,  
   ModifiedDate=GetUTCDate(),  
   ModifiedBy=@ModifiedBy,
   TotalShippingDiscount=@TotalShippingDiscount  
   where Id=@Id  
  
		if @AddedAttributes is not null
			begin
					Delete From dbo.OROrderAttributeValue where OrderId= @Id 
					
				Insert into OROrderAttributeValue(Id, OrderId, 
													AttributeId, AttributeEnumId, 
													Value, CreatedDate,CreatedBy)
				Select newId(), @Id, tab.col.value('(AttributeId)[1]','uniqueidentifier'),
													tab.col.value('(AttributeEnumId)[1]','uniqueidentifier'),
													tab.col.value('(Value)[1]','nvarchar(max)'), GetUTCDate(),@ModifiedBy
				From @AddedAttributes.nodes('GenericCollectionOfAssignableAttributeValueTuple/AssignableAttributeValueTuple') tab(col)
			end
     
 end  
 --if @CouponIds is not null
 if @CouponIds is not NULL ---AND CAST(@CouponIds AS NVARCHAR(MAX))!= '<GenericCollectionOfGuid/>'
	Exec Coupon_ApplyToOrder @CouponCodeIds=@CouponIds,
	 @OrderId=@Id,
	 @CustomerId=@CustomerId,
	 @ModifiedBy =@ModifiedBy

Select @PaymentRemaining = PaymentRemaining from OROrder Where Id =@Id
  
END
GO
PRINT 'Creating Taxonomy_ReplaceRelationReference'
GO
IF(OBJECT_ID('Taxonomy_ReplaceRelationReference') IS NOT NULL)
	DROP PROCEDURE  Taxonomy_ReplaceRelationReference
GO
CREATE PROCEDURE [dbo].[Taxonomy_ReplaceRelationReference] 
(
	@TaxonomyIds	xml,
	@ObjectId		uniqueidentifier,
	@ObjectTypeId	int,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier = null
)
AS
BEGIN
	DECLARE @UtcNow DateTime		
	SET @UtcNow = getutcdate()

	DECLARE @tbIds AS TABLE(Id uniqueidentifier)
	INSERT INTO @tbIds
	SELECT DISTINCT tab.col.value('text()[1]','uniqueidentifier')
		FROM  @TaxonomyIds.nodes('/GenericCollectionOfGuid/guid')tab(col)

	DELETE FROM COTaxonomyObject WHERE ObjectId = @ObjectId 

	INSERT INTO COTaxonomyObject
	(
		Id,
		TaxonomyId,
		ObjectId,
		ObjectTypeId,
		SortOrder,
		Status,
		CreatedBy,
		CreatedDate,
		ModifiedBy,
		ModifiedDate,
		SiteId
	)
	SELECT DISTINCT
		NEWID(),
		Id,
		@ObjectId, 
		@ObjectTypeId,
		1, 
		1,
		@ModifiedBy, 
		@UtcNow,
		@ModifiedBy, 
		@UtcNow,
		@ApplicationId
	FROM
		@tbIds

	IF @@Error <> 0
	BEGIN
		RAISERROR('DBERROR||%s',16,1,'')
		RETURN dbo.GetDataBaseErrorCode()
	END

END
GO
PRINT 'Update view vwAbandonedCheckout'
GO
IF(OBJECT_ID('vwAbandonedCheckout') IS NOT NULL)
	DROP VIEW vwAbandonedCheckout
GO
CREATE VIEW [dbo].[vwAbandonedCheckout]
AS
SELECT	AC.CustomerId, AC.CartId, AC.CartTotal, AC.FullName, AC.Email, AC.ModifiedDate, AC.ExpirationDate, AC.SiteId
FROM	ORAbandonedCheckout AS AC
		INNER JOIN USCommerceUserProfile AS CUP ON CUP.Id = AC.CustomerId AND CUP.IsExpressCustomer = 0

UNION

SELECT	C.Id AS CustomerId, CT.Id AS CartId, O.OrderTotal AS CartTotal, C.LastName + ',' + C.FirstName AS FullName,
		C.Email, ISNULL(O.ModifiedDate,O.CreatedDate) AS ModifiedDate, CT.ExpirationDate, CT.SiteId
FROM	CSCart AS CT
		INNER JOIN OROrder AS O ON O.CartId = CT.Id
		INNER JOIN vwCustomer AS C ON C.Id = O.CustomerId
WHERE   O.OrderStatusId = 12 AND C.IsExpressCustomer = 0
GO

PRINT 'Update stored procedure USGetUserIdForSite'
GO
IF(OBJECT_ID('USGetUserIdForSite') IS NOT NULL)
	DROP Function [dbo].[USGetUserIdForSite] 
GO

CREATE Function [dbo].[USGetUserIdForSite] 
(
	@UserName	nvarchar(256),
	@ApplicationId		uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @UserId		uniqueidentifier
	SET @UserId	=	null
	Declare @loweredUserName nvarchar(256)
	SET @loweredUserName =lower(@UserName)

	SELECT  @UserId = u.Id
    FROM    USUser u, USSiteUser s, USMembership m
    WHERE   LoweredUserName = @loweredUserName AND
            --s.SiteId = @ApplicationId AND
            --(s.SiteId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) AND
            s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND
			u.Id = m.UserId AND
			u.Id = s.UserId AND
			u.Status <> dbo.GetDeleteStatus()
	IF @UserId is null
	SELECT  @UserId = u.Id
    FROM    USUser u 
			INNER JOIN USMembership m on u.Id=m.UserId
    WHERE   LoweredUserName = @loweredUserName AND
			u.Status <> dbo.GetDeleteStatus()

	RETURN @UserId
END
GO
PRINT 'Update stored procedure CommerceReport_ProductWithoutCategory'
GO
IF(OBJECT_ID('CommerceReport_ProductWithoutCategory') IS NOT NULL)
	DROP PROCEDURE  CommerceReport_ProductWithoutCategory
GO
CREATE PROCEDURE [dbo].[CommerceReport_ProductWithoutCategory]
(	
	@maxReturnCount int,
	@pageSize int = 10,@pageNumber int, 
	@sortBy nvarchar(50) = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier = null,
	@IncludeChildrenSites Bit = 0
) 
AS

BEGIN
if @maxReturnCount = 0
	set @maxReturnCount = null

IF @pageSize IS NULL
	SET @PageSize = 2147483647

declare @tmpProductWithoutCategory table(
	ProductId uniqueidentifier,
	ProductTitle nvarchar(255),
	IsActive bit
	)

Insert Into @tmpProductWithoutCategory
select P.Id, P.Title, P.IsActive
FROM (
SELECT Id, Title, IsActive,SiteId FROM PRProduct
UNION ALL 
SELECT Id, Title, IsActive,SiteId FROM PRProductVariantCache
) P 
WHERE P.Id not in(
		Select A.ObjectId FROM
				(
			Select NVO.ObjectId, NVO.QueryId FROM NVFilterOutput NVO
				EXCEPT 
			Select NVE.ObjectId, NVE.QueryId FROM NVFilterExclude NVE 
				) A
	)
	AND (
			(@IncludeChildrenSites = 0 AND P.SiteId = @ApplicationId )
			OR
			(@IncludeChildrenSites = 1 AND P.SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites(@ApplicationId,1)))
		)
		

Set @virtualCount = @@ROWCOUNT

if @sortBy is null or (@sortBy) = 'producttitle asc'
Begin
	Select A.*
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by ProductTitle asc) as RowNumber
		 from @tmpProductWithoutCategory
		) A
		where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	 Order by A.ProductTitle ASC
end
else if (@sortBy) = 'producttitle desc'
Begin
	Select A.*
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by ProductTitle desc) as RowNumber
		 from @tmpProductWithoutCategory
		) A
		where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	 Order by A.ProductTitle DESC
end
END

GO
PRINT 'Update stored procedure Product_GetMinPrice'
GO
IF(OBJECT_ID('Product_GetMinPrice') IS NOT NULL)
	DROP PROCEDURE  Product_GetMinPrice
GO
CREATE PROCEDURE [dbo].[Product_GetMinPrice](
			 @CustomerId uniqueidentifier = null,
			 @IdQuantity  TYIdNumber READONLY,
			 @ApplicationId uniqueidentifier=null
			)
AS

BEGIN
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId')

			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice,ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY P.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @IdQuantity IQ
				INNER JOIN  PRProduct P ON IQ.Id =P.Id
				INNER JOIN PRProductSKU S on P.Id = S.ProductId
				LEFT JOIN PRProductSKUVariant SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					Where CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= S.Id AND IQ.Number between minQuantity and maxQuantity
					Where P.Id=IQ.Id
				
			) EFPS
			WHERE EFPS.Sno=1

		End
		else
		Begin
			Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice,ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @IdQuantity IQ
				INNER JOIN  PRProduct P ON IQ.Id =P.Id
				INNER JOIN PRProductSKU S on P.Id = S.ProductId
				LEFT JOIN PRProductSKUVariant SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					Where USMG.MemberId = @CustomerId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= S.Id AND IQ.Number between minQuantity and maxQuantity
					Where P.Id =IQ.Id					
		End
END

GO
PRINT 'Update stored procedure Sku_GetPrice'
GO
IF(OBJECT_ID('Sku_GetPrice') IS NOT NULL)
	DROP PROCEDURE  Sku_GetPrice
GO
CREATE PROCEDURE [dbo].[Sku_GetPrice](
			 @CustomerId uniqueidentifier = null,
			 @SKUQuantity  TYSKUQuantity READONLY,
			 @ApplicationId uniqueidentifier=null
										)
AS

BEGIN
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId')

			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale,  ISNULL(PST.EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY S.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @SKUQuantity inSKU 
				INNER JOIN PRProductSKU S on inSKU.SKUId = S.Id
				LEFT JOIN PRProductSKUVariant SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					Where CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= inSKU.SKUId AND inSKU.Quantity between minQuantity and maxQuantity
			) EFPS
			WHERE EFPS.Sno=1				

		End
		else
		Begin
			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY S.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @SKUQuantity inSKU 
				INNER JOIN PRProductSKU S on inSKU.SKUId = S.Id
				LEFT JOIN PRProductSkuVariant SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					Where USMG.MemberId = @CustomerId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= inSKU.SKUId AND inSKU.Quantity between minQuantity and maxQuantity
			) EFPS
			WHERE EFPS.Sno=1				
		End
END
GO
PRINT 'Update stored procedure Product_GetProductIdByUrl'
GO
IF(OBJECT_ID('Product_GetProductIdByUrl') IS NOT NULL)
	DROP PROCEDURE  Product_GetProductIdByUrl
GO

CREATE PROCEDURE [dbo].[Product_GetProductIdByUrl]
(
	@url nvarchar(max), 
	@productIDUser nvarchar(max) = null, 
	@SiteId UniqueIdentifier
)    
AS 
BEGIN
	SELECT	DISTINCT Id, ProductUrl, SEOFriendlyUrl
	FROM	vwProduct
	WHERE	(ProductUrl = @url OR '/' + SEOFriendlyUrl = @url OR SEOFriendlyUrl =  '/' + @url) AND
			SiteId = @SiteId	
END 
GO
PRINT 'Update stored procedure ShippingOption_Save'
GO
IF(OBJECT_ID('ShippingOption_Save') IS NOT NULL)
	DROP PROCEDURE  ShippingOption_Save
GO

CREATE PROCEDURE [dbo].[ShippingOption_Save]
(
	@Id uniqueidentifier output,
	@ApplicationId uniqueidentifier =null,
	@ShipperId uniqueidentifier,
	@Name nvarchar(255),
	@Description nvarchar(1024)= null,
	@CustomId nvarchar(50) =null,
	@NumberOfDaysToDeliver int =null,
	@CutOffHour int =null,
	@SaturdayDelivery tinyint =null,
	@AllowShippingCoupon tinyint =null,
    @IsPublic tinyint =null,
    @IsInternational tinyint =null,
    @ServiceCode nvarchar(100) =null,
	@ExternalShipCode nvarchar(100) = null,
    @BaseRate money =null,
    @Status int =null,
	@Sequence int =null,
    @CreatedBy uniqueidentifier,
    @ModifiedBy uniqueidentifier =null 
)
as
begin
	DECLARE	@NewId	uniqueidentifier
	DECLARE @Now dateTime
	DECLARE @Stmt VARCHAR(36)	
	DECLARE @Error int		
	
	SET @Now = getutcdate()
	
	if(@Id is null)
	begin
		set @NewId = newid()
		set @Id  =@NewId	
		Declare @currentSequence int     
		Select @currentSequence = Isnull(max(Sequence),0) from ORShippingOption where ShipperId=@ShipperId 

		SET @Stmt = 'Insert ShippingOption'
		INSERT INTO [ORShippingOption]
           ([Id]
           ,[ShipperId]
           ,[Name]
           ,[Description]
           ,[CustomId]
           ,[NumberOfDaysToDeliver]
           ,[CutOffHour]
           ,[SaturdayDelivery]
           ,[AllowShippingCoupon]
           ,[IsPublic]
           ,[IsInternational]
           ,[ServiceCode]
		   ,[ExternalShipCode]
           ,[BaseRate]
           ,[SiteId]
           ,[Status]
		   ,[Sequence]
           ,[CreatedBy]
           ,[CreatedDate])
           
     VALUES
           (@NewId
           ,@ShipperId
           ,@Name
           ,@Description      
           ,@CustomId
           ,@NumberOfDaysToDeliver
           ,@CutOffHour
           ,@SaturdayDelivery
           ,@AllowShippingCoupon
           ,@IsPublic
           ,@IsInternational
           ,@ServiceCode
		   ,@ExternalShipCode
           ,@BaseRate
           ,@ApplicationId
           ,@Status
		   ,@currentSequence+1
           ,@CreatedBy
           ,@Now
           )	

			SELECT @Error = @@ERROR
 			IF @Error <> 0
			BEGIN
				RAISERROR('DBERROR||%s',16,1,@Stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END
	end
	else
	begin
		IF (NOT EXISTS(SELECT 1 FROM  ORShippingOption WHERE  Id = @Id))
		BEGIN			
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END	

		SET @Stmt = 'Update Shipper'
		UPDATE [ORShippingOption]
		   SET [ShipperId] = @ShipperId
			  ,[Name] = @Name
			  ,[Description] = @Description
			  ,[CustomId] = @CustomId
			  ,[NumberOfDaysToDeliver] = @NumberOfDaysToDeliver
			  ,[CutOffHour] = @CutOffHour
			  ,[SaturdayDelivery] = @SaturdayDelivery
			  ,[AllowShippingCoupon] = @AllowShippingCoupon
			  ,[IsPublic] = @IsPublic
			  ,[IsInternational] = @IsInternational
			  ,[ServiceCode] = @ServiceCode
			  ,[ExternalShipCode] = @ExternalShipCode
			  ,[BaseRate] = @BaseRate
			  ,[SiteId] = @ApplicationId
			  ,[Sequence] = @Sequence
			  ,[Status] = @Status			 
			  ,[ModifiedBy] = @ModifiedBy
			  ,[ModifiedDate] = @Now
		 WHERE Id=@Id
		
		SELECT @Error = @@ERROR
 		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END
		 
	  
	end


end
GO
PRINT 'Update stored procedure PageDto_Import'
GO
IF(OBJECT_ID('PageDto_Import') IS NOT NULL)
	DROP PROCEDURE  PageDto_Import
GO
CREATE PROCEDURE [dbo].[PageDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@PageMapNodeId			uniqueidentifier = NULL,
	@Publish				bit = NULL,	
	@Overwrite				bit = NULL,		
	@ModifiedBy				uniqueidentifier,
	@PreserveVariantContent bit = 1
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier

	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @Id = [dbo].[Page_GetVariantId](@SourceId, @TargetSiteId)

	DECLARE @DisplayOrder int
	IF @PageMapNodeId IS NULL
	BEGIN
		DECLARE @ParentPageMapNode uniqueidentifier
		SELECT TOP 1 @ParentPageMapNode = PageMapNodeId, @DisplayOrder = DisplayOrder FROM PageMapNodePageDef 
			WHERE PageDefinitionId = @SourceId
		SET @PageMapNodeId = [dbo].[Menu_GetVariantId](@ParentPageMapNode, @TargetSiteId)
	END

	IF @PageMapNodeId IS NULL
		RETURN

	SELECT @ModifiedBy = ModifiedBy FROM PageDefinition WHERE PageDefinitionId = @SourceId 
	IF @Publish IS NULL AND EXISTS (SELECT 1 FROM PageDefinition WHERE PageDefinitionId = @SourceId AND PageStatus = 8)
		SET @Publish = 1

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()
		INSERT INTO [dbo].[PageDefinition]
		(
			[PageDefinitionId],
			[TemplateId],
			[SiteId],
			[Title],
			[Description],
			[PageStatus],
			[WorkflowState],
			[PublishCount],
			[PublishDate],
			[FriendlyName],
			[WorkflowId],
			[CreatedBy],
			[CreatedDate],
			[ModifiedBy],
			[ModifiedDate],
			[ArchivedDate],
			[StatusChangedDate],
			[AuthorId],
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			[SourcePageDefinitionId],
			[CustomAttributes]
		)
		SELECT @Id,
			[TemplateId],
			@TargetSiteId,
			[Title],
			D.[Description],
			CASE WHEN @Publish = 1 THEN 8 ELSE PageStatus END,
			1,
			0,
			'1900-01-01 00:00:00.000',
			[FriendlyName],
			@EmptyGuid,
			@ModifiedBy,
			@UtcNow,
			@ModifiedBy,
			@UtcNow,
			[ArchivedDate],
			[StatusChangedDate],
			@ModifiedBy,
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			PageDefinitionId,
			D.[CustomAttributes]
		FROM [dbo].[PageDefinition] D
		WHERE PageDefinitionId = @SourceId				

		UPDATE PageMapNode SET TargetId = @Id
		WHERE TargetId = @SourceId AND SiteId = @TargetSiteId
		 
		INSERT INTO [dbo].[PageDefinitionSegment]
		(
			[Id],
			[PageDefinitionId],
			[DeviceId],
			[AudienceSegmentId],
			[UnmanagedXml],
			[ZonesXml]
		)
		SELECT NEWID(), 
			@Id,
			[DeviceId],
			[AudienceSegmentId],
			dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
			[ZonesXml]
		FROM [dbo].[PageDefinitionSegment] PS 
		WHERE PageDefinitionId = @SourceId

		INSERT INTO [dbo].[PageDetails]
		(
			[PageId],
			[PageDetailXML]
		)
		SELECT 
			@Id,
			[PageDetailXML]
		FROM [dbo].[PageDetails] PD
		WHERE PD.PageId = @SourceId
	END
	ELSE
	BEGIN
		UPDATE C SET
			C.Title = P.Title,
			C.Description = P.Description,
			C.PageStatus = CASE WHEN @Publish = 1 THEN 8 ELSE P.PageStatus END,
			C.FriendlyName = CASE WHEN @PreserveVariantContent = 0 THEN P.FriendlyName ELSE C.FriendlyName END,
			C.EnableOutputCache = P.EnableOutputCache,
			C.OutputCacheProfileName = P.OutputCacheProfileName,
			C.ExcludeFromSearch = P.ExcludeFromSearch,
			C.HasForms = P.HasForms,
			C.ScheduledPublishDate = P.ScheduledPublishDate,
			C.ScheduledArchiveDate = P.ScheduledArchiveDate,
			C.ModifiedDate = @UtcNow,
			C.ModifiedBy = P.ModifiedBy
		FROM PageDefinition P, PageDefinition C
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.UnmanagedXml = dbo.Page_GetUnmanagedXml(P.[UnmanagedXml], @TargetSiteId),
			C.ZonesXml = P.ZonesXml
		FROM PageDefinitionSegment P
			JOIN PageDefinitionSegment C ON P.DeviceId = C.DeviceId AND P.AudienceSegmentId = C.AudienceSegmentId
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.ContentId = P.ContentId,
			C.InWFContentId = P.InWFContentId,
			C.ContentTypeId = P.ContentTypeId,
			C.IsModified = P.IsModified,
			C.[IsTracked] = P.[IsTracked],
			C.[Visible] = P.[Visible],
			C.[InWFVisible] = P.[InWFVisible],
			C.[CustomAttributes] = P.[CustomAttributes],
			C.[DisplayOrder] = P.[DisplayOrder],
			C.[InWFDisplayOrder] = P.[InWFDisplayOrder]
		FROM PageDefinitionContainer P
			JOIN PageDefinitionContainer C ON P.ContainerName = C.ContainerName
			LEFT JOIN COContent CO ON C.ContentId = CO.Id
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId
			AND (@Overwrite = 1 OR CO.Id IS NULL OR CO.ApplicationId != @TargetSiteId)
		
		IF @PreserveVariantContent = 0
		BEGIN
			UPDATE C 
			SET	C.PageDetailXML = P.PageDetailXML
			FROM PageDetails P, PageDetails C
			WHERE C.PageId = @Id AND P.PageId = @SourceId	
		END

		DELETE FROM PageMapNodePageDef WHERE PageDefinitionId = @Id
	END

	INSERT INTO PageMapNodePageDef
	(
		PageDefinitionId,
		PageMapNodeId,
		DisplayOrder
	)
	SELECT
		@Id,
		@PageMapNodeId,
		@DisplayOrder

	INSERT INTO [PageDefinitionContainer]
	(
		[Id],
		PageDefinitionSegmentId ,
		[PageDefinitionId],
		[ContainerId],
		[ContentId],
		[InWFContentId],
		[ContentTypeId],
		[IsModified],
		[IsTracked],
		[Visible],
		[InWFVisible],
		DisplayOrder,
		InWFDisplayOrder,
		[CustomAttributes],
		[ContainerName]
	)
	SELECT NEWID(),
		CS.Id,
		@Id,
		ContainerId,
		ContentId,
		InWFContentId,
		ContentTypeId,
		IsModified,
		PC.IsTracked,
		Visible,
		InWFVisible,
		PC.DisplayOrder, 
		PC.InWFDisplayOrder, 
		PC.CustomAttributes,
		ContainerName
	FROM PageDefinitionContainer PC
		JOIN PageDefinitionSegment PS ON PC.PageDefinitionSegmentId = PS.Id
		JOIN PageDefinitionSegment CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
	WHERE PC.PageDefinitionId = @SourceId
		AND (@Id IS NULL OR ContainerName NOT IN (SELECT ContainerName FROM PageDefinitionContainer WHERE PageDefinitionId = @Id))

	DECLARE @SourceTargetMenuId uniqueidentifier
	SELECT TOP 1 @SourceTargetMenuId = PageMapNodeId FROM PageMapNode PN
		JOIN PageDefinition PD ON PN.TargetId = PD.PageDefinitionId AND PD.SiteId = PN.SiteId
	WHERE PD.PageDefinitionId = @SourceId

	IF @SourceTargetMenuId IS NOT NULL
	BEGIN
		UPDATE PageMapNode SET
			TargetId = @Id,
			Target = 1,
			TargetUrl = ''
		WHERE SourcePageMapNodeId = @SourceTargetMenuId
			AND SiteId = @TargetSiteId
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[USSecurityLevelObject]
	(
		[ObjectId],
		[ObjectTypeId],
		[SecurityLevelId]
	)
	SELECT @Id,
		ObjectTypeId,
		SecurityLevelId
	FROM [USSecurityLevelObject] S
	WHERE S.ObjectId = @SourceId

	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageScript]
	(
		[PageDefinitionId],
		[ScriptId],
		[CustomAttributes]
	)
	SELECT @Id,
		[ScriptId],
		[CustomAttributes]
	FROM [dbo].[SIPageScript] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageStyle]
	(
		[PageDefinitionId],
		[StyleId],
		[CustomAttributes]
	)
	SELECT @Id,
		[StyleId],
		[CustomAttributes]
	FROM [dbo].[SIPageStyle] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM COTaxonomyObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[COTaxonomyObject]
	(
		[Id],
		[TaxonomyId],
		[ObjectId],
		[ObjectTypeId],
		[SortOrder],
		[Status],
		[CreatedBy],
		[CreatedDate],
		[ModifiedBy],
		[ModifiedDate]
	)
	SELECT NEWID(),
		[TaxonomyId],
		@Id,
		[ObjectTypeId],
		[SortOrder],
		[Status],
		@ModifiedBy,
		@UtcNow,
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[COTaxonomyObject] OT
	WHERE OT.ObjectId = @SourceId

	DECLARE @VersionId uniqueidentifier, @SourceVersionId uniqueidentifier, 
		@VersionNumber int, @RevisionNumber int, @VersionStatus int
	SET @VersionId = NEWID()
	SELECT TOP 1 @SourceVersionId = Id FROM VEVersion 
	WHERE ObjectId = @SourceId ORDER BY CreatedDate DESC	

	SELECT TOP 1 @VersionNumber = VersionNumber, @RevisionNumber = RevisionNumber FROM VEVersion
	WHERE ObjectId = @Id ORDER BY CreatedDate DESC

	IF @Publish = 1
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0) + 1
		SET @VersionNumber = 0
		SET @VersionStatus = 1
	END
	ELSE
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0)
		SET @VersionNumber = ISNULL(@VersionNumber, 0) + 1
		SET @VersionStatus = 0
	END

	INSERT INTO [dbo].[VEVersion]
	(
		[Id],
		[ApplicationId],
		[ObjectTypeId],
		[ObjectId],
		[CreatedDate],
		[CreatedBy],
		[VersionNumber],
		[RevisionNumber],
		[Status],
		[ObjectStatus],
		[VersionStatus],
		[XMLString],
		[Comments],
		[TriggeredObjectId],
		[TriggeredObjectTypeId]
	)          
	SELECT TOP 1 @VersionId,
		@TargetSiteId,
		8, 
		@Id, 
		@UtcNow,
		@ModifiedBy,
		@VersionNumber,
		@RevisionNumber,
		@VersionStatus,
		ObjectStatus,
		VersionStatus, 
		dbo.UpdatePageXml(XMLString, @Id, P.WorkflowId, P.WorkflowState, P.PageStatus, 1, 
			P.CreatedBy, P.CreatedDate, P.AuthorId, P.SiteId, P.SourcePageDefinitionId, @PageMapNodeId), 
		'Imported from master',
		TriggeredObjectId,
		TriggeredObjectTypeId       
	FROM VEVersion V, 
		PageDefinition P
	WHERE V.Id = @SourceVersionId
		AND P.PageDefinitionId = @Id

	IF @Publish = 1
		UPDATE VEVersion SET Status = 1 WHERE ObjectId = @Id

	INSERT INTO [dbo].[VEPageDefSegmentVersion]
	(
		[Id],
		[VersionId],
		[PageDefinitionId],
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],  
		[ContainerContentXML],        
		[UnmanagedXml],
		[ZonesXml],
		[CreatedBy],
		[CreatedDate]
	)
	SELECT NEWID(),
		@VersionId,
		@Id,
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],
		CASE WHEN @Overwrite = 1 THEN [ContainerContentXML] 
			ELSE [dbo].[Page_GetContainerXml](@Id, AudienceSegmentId, DeviceId) END,
		dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
		[ZonesXml],
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[VEPageDefSegmentVersion]
	WHERE VersionId = @SourceVersionId

	INSERT INTO [dbo].[VEPageContentVersion]
	(
		[Id],
		[ContentVersionId],
		[IsChanged],
		[PageDefSegmentVersionId]
	)
	SELECT NEWID(),
		ContentVersionId,
		IsChanged,
		CS.Id
	FROM [dbo].[VEPageContentVersion] PC
		JOIN VEPageDefSegmentVersion PS ON PC.PageDefSegmentVersionId = PS.Id
		JOIN VEPageDefSegmentVersion CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
			AND PS.VersionId = @SourceVersionId
			AND CS.VersionId = @VersionId

	IF @Publish = 1
	BEGIN
		DECLARE @PublishCount int
		SET @PublishCount = (SELECT ISNULL(PublishCount, 0) + 1 FROM PageDefinition WHERE PageDefinitionId = @Id)

		UPDATE PageDefinition SET
			PublishCount = @PublishCount,
			PublishDate = @UtcNow,
			PageStatus = 8
		WHERE PageDefinitionId = @Id

		DECLARE @WorkflowId uniqueidentifier
		SELECT TOP 1 @WorkflowId = WorkflowId FROM WFWorkflowExecution WHERE ObjectId = @Id AND Completed = 0

		IF @WorkflowId IS NOT NULL
		BEGIN
			EXEC WorkflowExecution_Save
				@WorkflowId = @WorkflowId,
				@SequenceId = @EmptyGuid,
				@ObjectId = @Id,
				@ObjectTypeId = 8,
				@ActorId = @ModifiedBy,
				@ActorType = 1,
				@ActionId = 8,
				@Remarks = N'Page imported from master',
				@Completed = 1
		END
	END
END
GO

PRINT 'Update view vwPageAttributeValue'
GO

IF(OBJECT_ID('vwPageAttributeValue') IS NOT NULL)
	DROP VIEW vwPageAttributeValue
GO

CREATE VIEW [dbo].[vwPageAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V

	UNION

	SELECT ROW_NUMBER() over (PARTITION BY V.AttributeId, C.PageDefinitionId ORDER BY S.LftValue DESC) AS vRank,
		V.Id,
		C.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V
		JOIN PageDefinition P ON V.PageDefinitionId = P.PageDefinitionId
		JOIN PageDefinition C ON C.SourcePageDefinitionId = P.PageDefinitionId
		JOIN SISite S ON S.Id = P.SiteId
	WHERE V.IsShared = 1
		AND S.DistributionEnabled = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT V.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	ISNULL(V.Notes, C.Notes) AS Notes,
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATPageAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.PageDefinitionId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1

GO

IF(OBJECT_ID('Membership_GetUsersInGroup') IS NOT NULL)
	DROP PROCEDURE  Membership_GetUsersInGroup
GO
CREATE PROCEDURE [dbo].[Membership_GetUsersInGroup]
(
	@ProductId				uniqueidentifier = NULL,
	@ApplicationId			uniqueidentifier,
	@Id				        uniqueidentifier,
	@MemberType				smallint,
	@IsSystemUser			bit = 0,
	@MemberId				uniqueidentifier = NULL, --either userid or groupid.
	@IgnoreProduct			bit = NULL,
	@WithProfile			bit = 0,
	@PageNumber				int,
	@PageSize				int
)
AS
BEGIN

	IF(@Id IS NULL)
	BEGIN
		RAISERROR ('ISNULL||GroupId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	IF @PageNumber IS NULL SET @PageNumber = 0
	IF @IgnoreProduct = 1 SET @ProductId = NULL
		
	IF (@MemberType = 1)
	BEGIN
		DECLARE @tbUser TYUser
		INSERT INTO @tbUser
		SELECT * FROM [dbo].[User_GetUsersInGroup] 
			(
				@ProductId, 
				@ApplicationId,
				@Id,
				@MemberType,
				@MemberId,
				@IsSystemUser
			)

		EXEC [dbo].[Membership_BuildUser] 
			@tbUser = @tbUser, 
			@SortBy = NULL, 
			@SortOrder = NULL,
			@PageNumber = @PageNumber,
			@PageSize = @PageSize
	END
	ELSE IF (@MemberType = 2) -- Group
	BEGIN
		DECLARE @PageLowerBound int
		DECLARE @PageUpperBound int
		SET @PageLowerBound = @PageSize * @PageNumber
		IF (@PageNumber > 0)
			SET @PageUpperBound = @PageLowerBound - @PageSize + 1
		ELSE
			SET @PageUpperBound = 0

		;WITH ResultEntries AS
		(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY g.Title) AS RowNumber,
				g.ProductId,
				ug.ApplicationId,
				Id,
				Title,
				Description,
				CreatedBy,
				CreatedDate,
				ModifiedBy,
				ModifiedDate,
				Status,
				ExpiryDate,
				g.GroupType
			FROM dbo.USMemberGroup ug 
				INNER JOIN dbo.USGroup g ON g.Id = ug.MemberId	
			WHERE Status <> dbo.GetDeleteStatus() AND
				ug.GroupId = @Id AND 
				ug.MemberId = ISNULL(@MemberId,ug.MemberId)	AND
				(g.ApplicationId = g.ProductId OR g.ApplicationId IN (Select SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser,0))) AND
				(g.ExpiryDate is null OR g.ExpiryDate >= GETUTCDATE()) AND
				g.ProductId = ISNULL(@ProductId,g.ProductId)
		)

		SELECT DISTINCT ProductId,
			ApplicationId,
			Id,
			Title,
			Description,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			Status,
			ExpiryDate,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			GroupType
		FROM ResultEntries g
			LEFT JOIN VW_UserFullName FN on FN.UserId = g.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = g.ModifiedBy
		WHERE (@PageNumber = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)
	END 
END
GO
PRINT 'Update view User_GetUsersInGroup'
GO
IF(OBJECT_ID('User_GetUsersInGroup') IS NOT NULL)
	DROP FUNCTION User_GetUsersInGroup
GO

CREATE FUNCTION [dbo].[User_GetUsersInGroup]  
(  
 @ProductId    uniqueidentifier = NULL,   
 @ApplicationId   uniqueidentifier,  
 @Id            uniqueidentifier,   -- GroupId  
 @MemberType    smallint,  
 @MemberId    uniqueidentifier = NULL, -- Either userId or groupid.  
 @IsSystemUser   bit = 0  
)  
RETURNS @ResultTable TABLE       
(      
 Id      uniqueidentifier,      
 UserName    nvarchar(256),        
 Email     nvarchar(512),      
 PasswordQuestion  nvarchar(512),        
 IsApproved    bit,      
 CreatedDate    datetime,      
 LastLoginDate   datetime,      
 LastActivityDate  datetime,      
 LastPasswordChangedDate datetime,      
 IsLockedOut    bit,      
 LastLockoutDate   datetime,    
 FirstName    nvarchar(256),    
 LastName    nvarchar(256),    
 MiddleName    nvarchar(256) ,    
 ExpiryDate    datetime ,    
 EmailNotification  bit ,    
 TimeZone    nvarchar(100),    
 ReportRangeSelection varchar(50),    
 ReportStartDate   datetime ,    
 ReportEndDate   datetime,     
 BirthDate    datetime,    
 CompanyName    nvarchar(1024),    
 Gender     nvarchar(50),    
 HomePhone    varchar(50),    
 MobilePhone    varchar(50),    
 OtherPhone    varchar(50),    
 ImageId     uniqueidentifier,
 LeadScore	int,  
 Status     int
)   
AS  
BEGIN  
  
 DECLARE @tbUser TYUser  
 INSERT INTO @tbUser  
 SELECT DISTINCT   
  U.Id,  
  U.UserName,  
  M.Email,   
  M.PasswordQuestion,    
  M.IsApproved,  
  U.CreatedDate,  
  M.LastLoginDate,   
  U.LastActivityDate,  
  M.LastPasswordChangedDate,   
  M.IsLockedOut,  
  M.LastLockoutDate,  
  U.FirstName,  
  U.LastName,  
  U.MiddleName,  
  U.ExpiryDate,  
  U.EmailNotification,  
  U.TimeZone,  
  U.ReportRangeSelection,  
  U.ReportStartDate,  
  U.ReportEndDate,  
  U.BirthDate,  
  U.CompanyName,  
  U.Gender,  
  U.HomePhone,  
  U.MobilePhone,  
  U.OtherPhone,  
  U.ImageId,
  U.LeadScore,
  U.Status
 FROM dbo.USUser U  
  INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
  INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
   (@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser)   
  INNER JOIN dbo.USMemberGroup G ON G.MemberId = U.Id AND  
   (@ApplicationId IS NULL OR  
    G.ApplicationId IN (Select SiteId From dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser, 0))   
   )   
 WHERE   
  G.GroupId = @Id AND  
  U.Status != 3 AND  
  (@ProductId IS NULL OR S.ProductId = @ProductId) AND  
  (@MemberId IS NULL OR G.MemberId = @MemberId)  
   
 INSERT INTO @ResultTable  
 SELECT * FROM @tbUser  
  
 RETURN  
END

GO
PRINT 'Update view Membership_GetUserWithProfile'
GO
IF(OBJECT_ID('Membership_GetUserWithProfile') IS NOT NULL)
	DROP PROCEDURE  Membership_GetUserWithProfile
GO
CREATE PROCEDURE [dbo].[Membership_GetUserWithProfile]  
(
	@ProductId     uniqueidentifier = NULL,  
	@ApplicationId    uniqueidentifier,  
	@UserId      uniqueidentifier,  
	@UserName     nvarchar(256) = NULL  
)
AS   
BEGIN  
	DECLARE @EmptyGuid uniqueidentifier  
	SELECT @EmptyGuid = dbo.GetEmptyGUID()  

	DECLARE @DeleteStatus int
	SELECT @DeleteStatus = dbo.GetDeleteStatus()
  
	IF(@UserId IS NULL)  
	BEGIN  
		SELECT @UserId = Id FROM dbo.USUser U, dbo.USSiteUser S 
		WHERE LOWER(U.UserName) = LOWER(@UserName) 
			AND (@ApplicationId IS NULL OR S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
			AND U.Id = S.UserId   
			AND (@ProductId IS NULL OR @ProductId = @EmptyGuid OR @ProductId = S.ProductId)  
			And Status != @DeleteStatus
	END    
  
	SELECT  U.UserName,  
		U.CreatedDate,  
		M.Email,   
		M.PasswordQuestion,    
		M.IsApproved,  
		M.LastLoginDate,   
		U.LastActivityDate,  
		M.LastPasswordChangedDate,   
		U.Id,   
		M.IsLockedOut,  
		M.LastLockoutDate,
		U.FirstName,
		U.LastName,
		U.MiddleName,
		U.ExpiryDate,
		U.EmailNotification,
		U.TimeZone,
		U.ReportRangeSelection,
		U.ReportStartDate,
		U.ReportEndDate,
		U.BirthDate,
		U.CompanyName,
		U.Gender,
		U.HomePhone,
		U.MobilePhone,
		U.OtherPhone,
		U.ImageId,
		U.LeadScore,
		U.DashboardData,
		S.IsSystemUser
	FROM dbo.USUser U, dbo.USMembership M, dbo.USSiteUser S
	WHERE   U.Id = @UserId  
		AND U.Id = M.UserId     
		AND (@ApplicationId IS NULL OR S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
		AND M.UserId = S.UserId  
		AND U.Status != @DeleteStatus
		AND (@ProductId IS NULL OR @ProductId = @EmptyGuid or @ProductId = S.ProductId)  
  
	SELECT UserId,  
		PropertyName,  
		PropertyValueString,  
		PropertyValuesBinary,  
		LastUpdatedDate  
	FROM dbo.USMemberProfile  
	WHERE UserId = @UserId  
END


GO
PRINT 'Update view Membership_GetUserNameByEmail'
GO
IF(OBJECT_ID('Membership_GetUserNameByEmail') IS NOT NULL)
	DROP PROCEDURE  Membership_GetUserNameByEmail
GO

CREATE PROCEDURE [dbo].[Membership_GetUserNameByEmail]
--********************************************************************************
-- PARAMETERS

    @ApplicationId			  uniqueidentifier,
    @Email            nvarchar(256)

--********************************************************************************

AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.USSiteUser s, dbo.USUser u, dbo.USMembership m
        WHERE   --s.SiteId = @ApplicationId	AND
				s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND
                u.Id = m.UserId AND
				u.Id = s.UserId AND
				u.Status <> dbo.GetDeleteStatus() AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  UserName
        FROM USUser
        Where Id in (
        SELECT Distinct U.Id
        FROM    dbo.USSiteUser s, dbo.USUser u, dbo.USMembership m
        WHERE   --s.SiteId = @ApplicationId  AND
				--s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND
                u.Id = m.UserId AND
				u.Id = s.UserId AND
				u.Status <> dbo.GetDeleteStatus() AND
                LOWER(@Email) = m.LoweredEmail)
    RETURN(0)
END

GO
PRINT 'Update view vwManualPriceSetSku'
GO

IF(OBJECT_ID('vwManualPriceSetSku') IS NOT NULL)
	DROP VIEW vwManualPriceSetSku
GO
CREATE VIEW [dbo].[vwManualPriceSetSku]
AS 
WITH CTE AS
(
SELECT 	
	M.Id, 
	M.SKUId,
	M.PriceSetId , 
	M.MinQuantity, 
	M.MaxQuantity, 
	M.Price,
	M.CreatedDate,
	M.CreatedBy,
	CFN.UserFullName AS CreatedByFullName,
	M.ModifiedDate,
	M.ModifiedBy,
	MFN.UserFullName AS ModifiedByFullName,
	P.Title AS ProductName,
	S.SKU,
	S.ListPrice,
	S.SiteId AS SKUSiteId,
	PS.SiteId AS PriceSetSiteId
FROM PSPriceSetManualSKU M
    INNER JOIN PSPriceSet PS ON M.PriceSetId = PS.Id
	INNER JOIN PRProductSKU S ON M.SKUId = S.Id
	INNER JOIN PRPRoduct P ON P.Id = S.ProductId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = M.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = M.ModifiedBy

UNION

SELECT 	
	M.Id, 
	M.SKUId,
	M.PriceSetId , 
	M.MinQuantity, 
	M.MaxQuantity, 
	M.Price,
	M.CreatedDate,
	M.CreatedBy,
	CFN.UserFullName AS CreatedByFullName,
	M.ModifiedDate,
	M.ModifiedBy,
	MFN.UserFullName AS ModifiedByFullName,
	P.Title AS ProductName,
	S.SKU,
	SV.ListPrice,
	SV.SiteId AS SKUSiteId,
	PS.SiteId AS PriceSetSiteId
FROM PSPriceSetManualSKU M
    INNER JOIN PSPriceSet PS ON M.PriceSetId = PS.Id
    INNER JOIN PRProductSKU S ON M.SKUId = S.Id
	INNER JOIN PRProductSKUVariant SV ON M.SKUId = SV.Id
	INNER JOIN PRPRoduct P ON P.Id = S.ProductId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = M.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = M.ModifiedBy
)

SELECT Id, 
	SKUId,
	PriceSetId , 
	MinQuantity, 
	MaxQuantity, 
	Price,
	CreatedDate,
	CreatedBy,
	CreatedByFullName,
	ModifiedDate,
	ModifiedBy,
	ModifiedByFullName,
	ProductName,
	SKU,
	ListPrice,
	SKUSiteId AS SiteId
FROM CTE
WHERE SKUSiteId = PriceSetSiteId
		
GO


PRINT 'Update view vwSiteAttributeValue'
GO

IF(OBJECT_ID('vwSiteAttributeValue') IS NOT NULL)
	DROP VIEW vwSiteAttributeValue
GO

CREATE VIEW [dbo].[vwSiteAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.SiteId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V

	UNION

	SELECT ROW_NUMBER() over (PARTITION BY V.AttributeId, C.Id ORDER BY P.LftValue DESC) AS vRank,
		V.Id,
		C.Id AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V
		JOIN SISite P ON V.SiteId = P.Id
		JOIN SISite C ON C.LftValue > P.LftValue AND C.RgtValue < P.RgtValue
	WHERE V.IsShared = 1
		AND P.DistributionEnabled = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT V.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.SiteId,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATSiteAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.SiteId
		AND C.SiteId = V.SiteId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1

GO

PRINT 'Update view vwSkuAttributeValue'
GO

IF(OBJECT_ID('vwSkuAttributeValue') IS NOT NULL)
	DROP VIEW vwSkuAttributeValue
GO
CREATE VIEW [dbo].[vwSkuAttributeValue]
AS
SELECT V.Id,
	P.SiteId,
	P.Id AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	V.Value,
	V.CreatedBy,
	V.CreatedDate,
	A.Title as AttributeTitle
FROM PRProductSKUAttributeValue V
	JOIN PRProductSKU P ON P.Id = V.SKUId
	JOIN ATAttribute A ON A.Id = V.AttributeId

UNION ALL

SELECT V.Id,
	V.SiteId,
	V.SKUId,
	V.AttributeId, 
	V.AttributeEnumId, 
	V.Value,
	V.ModifiedBy,
	V.ModifiedDate,
	A.Title as AttributeTitle
FROM PRProductSKUAttributeValueVariantCache V
    JOIN ATAttribute A ON A.Id = V.AttributeId
GO

PRINT 'Update view vwProductAttributeValue'
GO

IF(OBJECT_ID('vwProductAttributeValue') IS NOT NULL)
	DROP VIEW vwProductAttributeValue
GO
CREATE VIEW [dbo].[vwProductAttributeValue]
AS
SELECT V.Id, 
	P.SiteId,
	P.Id AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	V.Value,
	V.CreatedBy,
	V.CreatedDate,
	A.Title as AttributeTitle
FROM PRProductAttributeValue V
	JOIN PRProduct P ON P.Id = V.ProductId
	JOIN ATAttribute A ON A.Id = V.AttributeId

UNION ALL

SELECT V.Id,
	V.SiteId,
	V.ProductId AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	V.Value,
	V.ModifiedBy,
	V.ModifiedDate,
	A.Title as AttributeTitle
FROM PRProductAttributeValueVariantCache V
	JOIN ATAttribute A ON A.Id = V.AttributeId
GO

PRINT 'Update view vwAttributeValue'
GO

IF(OBJECT_ID('vwAttributeValue') IS NOT NULL)
	DROP VIEW vwAttributeValue
GO
CREATE VIEW [dbo].[vwAttributeValue]
AS 
SELECT V.Id,
	V.ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	A.Title AS AttributeTitle,
	V.Value,
	V.CreatedBy,
	V.CreatedDate
FROM ATAttributeValue V
	JOIN ATAttribute A ON A.Id = V.AttributeId
GO

PRINT 'Update view vwContactAttributeValue'
GO

IF(OBJECT_ID('vwContactAttributeValue') IS NOT NULL)
	DROP VIEW vwContactAttributeValue
GO
CREATE VIEW [dbo].[vwContactAttributeValue]
AS 
SELECT V.Id,
	V.ContactId AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	A.Title AS AttributeTitle,
	V.Value,
	V.CreatedBy,
	V.CreatedDate
FROM ATContactAttributeValue V
	JOIN ATAttribute A ON A.Id = V.AttributeId
GO

PRINT 'Update view vwCustomerAttributeValue'
GO

IF(OBJECT_ID('vwCustomerAttributeValue') IS NOT NULL)
	DROP VIEW vwCustomerAttributeValue
GO
CREATE VIEW [dbo].[vwCustomerAttributeValue]
AS 
SELECT V.Id,
	V.CustomerId AS ObjectId,
	V.AttributeId, 
	A.Title AS AttributeTitle,
	V.AttributeEnumId, 
	V.Value,
	V.CreatedBy,
	V.CreatedDate
FROM CSCustomerAttributeValue V
	JOIN ATAttribute A ON A.Id = V.AttributeId
GO

PRINT 'Update view vwOrderAttributeValue'
GO

IF(OBJECT_ID('vwOrderAttributeValue') IS NOT NULL)
	DROP VIEW vwOrderAttributeValue
GO
CREATE VIEW [dbo].[vwOrderAttributeValue]
AS
SELECT V.Id, 
	O.SiteId,
	O.Id AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	A.Title AttributeTitle,
	V.Value,
	V.CreatedBy,
	V.CreatedDate
FROM OROrderAttributeValue V
	INNER JOIN OROrder O ON O.Id = V.OrderId
	INNER JOIN ATAttribute A ON A.Id = V.AttributeId
GO

PRINT 'Update view vwOrderItemAttributeValue'
GO

IF(OBJECT_ID('vwOrderItemAttributeValue') IS NOT NULL)
	DROP VIEW vwOrderItemAttributeValue
GO
CREATE VIEW [dbo].[vwOrderItemAttributeValue]
AS
SELECT V.OrderItemAttributeValueId AS Id, 
	O.SiteId,
	O.Id AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	A.Title AttributeTitle,
	V.Value,
	V.CreatedBy,
	V.CreatedDate
FROM OROrderItemAttributeValue V
	JOIN OROrder O ON O.Id = V.OrderId
	JOIN ATAttribute A ON A.Id = V.AttributeId
GO

PRINT 'Update view Coupon_ApplyToOrder'
GO
IF(OBJECT_ID('Coupon_ApplyToOrder') IS NOT NULL)
	DROP PROCEDURE  Coupon_ApplyToOrder
GO

CREATE PROCEDURE [dbo].[Coupon_ApplyToOrder]    
(    
 @CouponCodeId uniqueidentifier=null,     
 @CouponCodeIds xml=null,  
 @OrderId uniqueidentifier,  
 @CustomerId uniqueidentifier,  
 @ModifiedBy uniqueidentifier  
)    
as    
begin    
   
 IF @CouponCodeId is not null  
 BEGIN  
 insert into CPOrderCouponCode  
 (  
  OrderId,  
  CouponCodeId,  
  CustomerId,  
  CreatedDate,  
  CreatedBy  
 )  
 values  
 (  
  @OrderId,  
  @CouponCodeId,  
  @CustomerId,  
  GetUTCDate(),  
  @ModifiedBy  
 )  
  
 DECLARE @CouponId uniqueidentifier  
 SELECT @CouponId = CouponId from CPCouponCode where Id = @CouponCodeId  
   
 DECLARE @CartId uniqueidentifier   
 SELECT @CartId = CartId from OROrder where Id = @OrderId  
 IF @CartId is not null  
 BEGIN  
 -- UPDATE CSCart SET CouponId = @CouponId  where Id = @CartId  
  IF Not Exists (Select * from CSCartCoupon Where CouponId=@CouponId And CartId=@CartId)  
  Insert CSCartCoupon(Id,CartId,CouponId,CreatedBy,CreatedDate)  
  Values(newid(),@CartId,@CouponId,@ModifiedBy,GetUTCDAte())  
 END  
   
 END  -- End of @CouponCodeId
 ELSE IF @CouponCodeIds is not null  
 BEGIN   
	 
	  Declare @CouponCode Table
	  (
		CouponCodeId uniqueidentifier
	  )
		
	  Declare @CouponCodeCount int
	  Declare @LocalCouponCodeId uniqueidentifier
	  Declare @OrderCouponCount int
	  
	  SELECT @OrderCouponCount = COUNT(1) FROM CPOrderCouponCode where OrderId = @OrderId		
	 
	 INSERT INTO @CouponCode  (CouponCodeId)
	 SELECT Distinct CC.Id  
	 FROM @CouponCodeIds.nodes('/GenericCollectionOfGuid/guid')tab(col)  
	 inner join CPCouponCode CC ON tab.col.value('text()[1]','uniqueidentifier')=CC.Id
 
	Select @CouponCodeCount = count(1) from @CouponCode 
 
	If(@CouponCodeCount = 1)
	BEGIN
		Select top 1 @LocalCouponCodeId  = CouponCodeId from @CouponCode
 
		IF (NOT EXISTS (Select * from CPOrderCouponCode Where CouponCodeId=@LocalCouponCodeId AND OrderId=@OrderId)  AND @OrderCouponCount = 0)
			Insert into  CPOrderCouponCode  
			  (  
			  OrderId,  
			  CouponCodeId,  
			  CustomerId,  
			  CreatedDate,  
			  CreatedBy  
			  )  
			  Select @OrderId  
				  ,@LocalCouponCodeId 
				,@CustomerId  
				,GetUTCDATE(),  
				@ModifiedBy 
		
	ELSE IF(@OrderCouponCount = 1)
		BEGIN
		--Update the CouponCode
		UPDATE CPOrderCouponCode Set CouponCodeId = @LocalCouponCodeId Where @OrderId = OrderId and CustomerId = @CustomerId
		
		END
	--END	
	ELSE --When multiple Coupon for the same order, but only one coupon is passed
	BEGIN
			Delete from CPOrderCouponCode  
			Where OrderId =@OrderId  
	
			Insert into  CPOrderCouponCode  
			  (  
			  OrderId,  
			  CouponCodeId,  
			  CustomerId,  
			  CreatedDate,  
			  CreatedBy  
			  )  
			  Select @OrderId  
				  ,@LocalCouponCodeId 
				,@CustomerId  
				,GetUTCDATE(),  
				@ModifiedBy 
	END				
	END
	
  ELSE 
	BEGIN
  
		  Delete from CPOrderCouponCode  
		  Where OrderId =@OrderId  
		  Insert into  CPOrderCouponCode  
		  (  
		  OrderId,  
		  CouponCodeId,  
		  CustomerId,  
		  CreatedDate,  
		  CreatedBy  
		  )  
		  Select @OrderId  
			  ,C.Id  
			,@CustomerId  
			,GetUTCDATE(),  
			@ModifiedBy  
		  FROM  
		  (Select Distinct CC.Id  
		  FROM @CouponCodeIds.nodes('/GenericCollectionOfGuid/guid')tab(col)  
		  inner join CPCouponCode CC ON tab.col.value('text()[1]','uniqueidentifier')=CC.Id) C  
    END
 END  -- end for else of @CouponCodeIds not null
  
end  -- sp end
GO



PRINT N'Dropping [dbo].[ClearCachedTimeZone]...';
GO
DROP FUNCTION [dbo].[ClearCachedTimeZone];


GO
PRINT N'Dropping [dbo].[ConvertTimeFromUtc]...';


GO
DROP FUNCTION [dbo].[ConvertTimeFromUtc];


GO
PRINT N'Dropping [dbo].[ConvertTimeToUtc]...';


GO
DROP FUNCTION [dbo].[ConvertTimeToUtc];


GO
PRINT N'Dropping [dbo].[GetLocalDate]...';


GO
DROP FUNCTION [dbo].[GetLocalDate];


GO
PRINT N'Dropping [dbo].[ConvertTimeToUtcUsingSiteTimeZone]...';


GO
DROP FUNCTION [dbo].[ConvertTimeToUtcUsingSiteTimeZone];


GO
PRINT N'Dropping [Bridgeline.TimeZone]...';


GO
DROP ASSEMBLY [Bridgeline.TimeZone];


GO
PRINT N'Creating [Bridgeline.TimeZone]...';


GO
CREATE ASSEMBLY [Bridgeline.TimeZone]
    AUTHORIZATION [dbo]
    FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500004C01030084E2A45C0000000000000000E00022200B013000001A000000060000000000006A380000002000000040000000000010002000000002000004000000000000000400000000000000008000000002000000000000030040850000100000100000000010000010000000000000100000000000000000000000183800004F00000000400000EC03000000000000000000000000000000000000006000000C000000E03600001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000080000000000000000000000082000004800000000000000000000002E746578740000007018000000200000001A000000020000000000000000000000000000200000602E72737263000000EC0300000040000000040000001C0000000000000000000000000000400000402E72656C6F6300000C00000000600000000200000020000000000000000000000000000040000042000000000000000000000000000000004C3800000000000048000000020005002C250000B41100000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000133002004900000001000011000F00281100000A16FE010A062C300F00281200000A0F01281300000A2D090F01281400000A2B057E1500000A2808000006281600000A281700000A0B2B087E1800000A0B2B00072A000000133002004900000001000011000F00281100000A16FE010A062C300F00281200000A0F01281300000A2D090F01281400000A2B057E1500000A2808000006281900000A281700000A0B2B087E1800000A0B2B00072A000000133002004900000001000011000F00281100000A16FE010A062C300F00281200000A0F01281300000A2D090F01281400000A2B057E1500000A2808000006281600000A281700000A0B2B087E1800000A0B2B00072A00000013300200390000000200001100281A00000A0A1200281B00000A0F00281300000A2D090F00281400000A2B057E1500000A2808000006281900000A281700000A0B2B00072A000000133001002200000003000011000F00281400000A2809000006007201000070281C00000A281D00000A0A2B00062A2202281E00000A002A001B3003008C00000004000011007E030000040A06281F00000A0000140B7E01000004026F2000000A0C082C0C7E01000004026F2100000A0B072C24076F0E00000613041204282200000A281A00000A13041204282200000A282300000A2B01170D092C1F0002280A00000613051105730D0000060B7E0100000402076F2400000A0000076F100000061306DE0806282500000A00DC11062A0110000002000E0073810008000000001B3003008C00000004000011007E030000040A06281F00000A0000140B7E02000004026F2000000A0C082C0C7E02000004026F2100000A0B072C24076F0E00000613041204282200000A281A00000A13041204282200000A282300000A2B01170D092C1F0002280B00000613051105730D0000060B7E0200000402076F2400000A0000076F100000061306DE0806282500000A00DC11062A0110000002000E007381000800000000133002003800000005000011007E01000004026F2000000A0A062C0C7E01000004026F2600000A267E02000004026F2000000A0B072C0C7E02000004026F2600000A262A1B3003008600000006000011007E2700000A0A140C720B0000700B07732800000A0D723B0000700F00FE16180000016F2900000A723A010070282A00000A130400110409732B00000A1305096F2C00000A0011056F2D00000A6F2900000A0A096F2E00000A0006282F00000A0C00DE05260000DE00DE13000814FE01130611062C06283000000A0C00DC0813072B0011072A0000011C000000003400306400050F00000102003400376B0013000000001B3003008700000006000011007E2700000A0A140C720B0000700B07732800000A0D723E0100700F00FE16180000016F2900000A723A010070282A00000A130400110409732B00000A1305096F2C00000A0011056F2D00000A6F2900000A0A096F2E00000A0006282F00000A0C00DE05260000DE00DE14000814FE01130611062C0702280A0000060C00DC0813072B0011072A00011C000000003400306400050F00000102003400376B0014000000007E733100000A8001000004733100000A8002000004731E00000A80030000042A7602281E00000A0000020328110000060002281A00000A280F000006002A1E027B040000042A2202037D040000042A1E027B050000042A2202037D050000042A42534A4201000100000000000C00000076322E302E35303732370000000005006C0000002C050000237E0000980500003C06000023537472696E677300000000D40B00009001000023555300640D0000100000002347554944000000740D00004004000023426C6F6200000000000000020000015715A2090902000000FA013300160000010000002300000004000000050000001100000010000000310000001A00000006000000010000000200000004000000010000000100000003000000020000000000530401000000000006006C036C050600D9036C05060063023A050F008C0500000600F502A9040600D802A9040600C003A9040600A503A90406008C03A9040600BD02A9040600A602A90406004F03A90406001B03A904060092024D050600DB0588040A003A030A050A00EC000A050A00E6000A050A0033019B050A0093009B050600360188040A008F049B050600010036000600960088040E00E90488040A00C804F3050A00DB00F305060048026C050600F4013A05060077023A05060009068804060032050E040600210488040A00BB04D6040A00D100D604000000000E00000000000100010001001000BF0500003D000100010083011000B60100003D000100070003001000230000003D0004000D003100B005E20031002002E20031004304EC0001009B00EF000100B700F30050200000000096009101F7000100A8200000000096005100F700030000210000000096006400F70005005821000000009600E70100010700A0210000000096007D0107010800CE21000000008618250506000900D821000000009600B3010E01090080220000000096000B020E010A0028230000000096007D0115010B006C2300000000910028040E010C001C2400000000910034020E010D00CC240000000091182B051B010E00EC2400000000861825051F010E000A25000000008608FB002A000F0012250000000086080A0125010F001B2500000000860863012B011000232500000000860870011F01100000000100250100000200750000000100190100000200750000000100250100000200750000000100750000000100750000000100750000000100750000000100750000000100750000000100750000000100C301000001000104000001000104090025050100110025050600190025050A00290025051000310025051000390025051000410025051000490025051000510025051000590025051000610025051000690025051000710025051500810025050600E10025050600F10025051A0099006B0426009900F7032A00A1006B042600A100F7032F00C10033063400C900640038009900E2054100990071044800C90051003800A90011065300A9003F012A00F9009A045D00B100E20562007900250506000101040576000C00190684000C0076048A00A900DE012A00A900250691000C007F0499000101EE0576000C000704840009013306B500D1002505100079001F04B8000901D405BC00D9002505C3001101A40406001901F604CA001101D8010600C9007C00CE00C9004904D4000C002505060020007300E0012E000B003A012E00130043012E001B0062012E0023006B012E002B006B012E00330078012E003B0094012E004300B6012E004B00C1012E005300C7012E005B00C1012E006300C1012E006B00C10140007300E00160007300E00180007300E00181007B003004810083003504A00073000803A1007B003004A10083003504C0017B003004E0017B00300400027B00300420027B00300420004C0058006800A100A6000400010000000E0130010000BA01350102000E00030001000F0003000200100005000100110005007B000480000007000000000000000000000000004F010000020000000000000000000000D9002D0000000000020000000000000000000000D900170000000000030005000000000000000000D900CC0100000000030002000400030000000044696374696F6E6172796032003C4D6F64756C653E0053797374656D2E44617461004C6F63616C44617461006D73636F726C69620053797374656D2E436F6C6C656374696F6E732E47656E6572696300436F6E7665727454696D6546726F6D55746300436F6E7665727454696D65546F557463007369746549640046696E6453797374656D54696D655A6F6E65427949640053716C47756964003C4C6F6164656454696D653E6B5F5F4261636B696E674669656C64003C54696D655A6F6E653E6B5F5F4261636B696E674669656C64004462436F6D6D616E640053716C436F6D6D616E640053797374656D446174614163636573734B696E64006765745F4C6F6164656454696D65007365745F4C6F6164656454696D65007574634461746554696D65006C6F63616C4461746554696D650053716C4461746554696D6500546F556E6976657273616C54696D65004272696467656C696E652E54696D655A6F6E65006765745F54696D655A6F6E65007365745F54696D655A6F6E6500436C65617243616368656454696D655A6F6E6500436F6E7665727454696D65546F5574635573696E675369746554696D655A6F6E65004765745369746554696D655A6F6E650074696D655A6F6E650053797374656D2E436F726500436C6F7365006765745F44617465004765744C6F63616C4461746500446562756767657242726F777361626C655374617465004765745369746554696D655A6F6E655F536974650063616368656453657474696E67735F536974650047657454696D655A6F6E6546726F6D5369746500436F6D70696C657247656E6572617465644174747269627574650044656275676761626C6541747472696275746500446562756767657242726F777361626C6541747472696275746500436F6D56697369626C6541747472696275746500417373656D626C795469746C6541747472696275746500417373656D626C7954726164656D61726B41747472696275746500417373656D626C7946696C6556657273696F6E41747472696275746500417373656D626C79496E666F726D6174696F6E616C56657273696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E4174747269627574650053716C46756E6374696F6E41747472696275746500417373656D626C794465736372697074696F6E41747472696275746500436F6D70696C6174696F6E52656C61786174696F6E7341747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C79436F6D70616E794174747269627574650052756E74696D65436F6D7061746962696C697479417474726962757465006765745F56616C75650076616C75650052656D6F76650053797374656D2E546872656164696E6700546F537472696E670047657454696D655A6F6E6546726F6D5369746553657474696E67005F6C6F636B006765745F4C6F63616C004272696467656C696E652E54696D655A6F6E652E646C6C006765745F49734E756C6C006765745F4974656D007365745F4974656D0053797374656D0053716C426F6F6C65616E00546F426F6F6C65616E004F70656E0053797374656D2E5265666C656374696F6E004462436F6E6E656374696F6E0053716C436F6E6E656374696F6E0053797374656D2E446174612E436F6D6D6F6E0054696D655A6F6E65496E666F00457865637574655363616C617200456E746572004D6963726F736F66742E53716C5365727665722E536572766572002E63746F72002E6363746F72004D6F6E69746F720053797374656D2E446961676E6F73746963730053797374656D2E52756E74696D652E496E7465726F7053657276696365730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300446562756767696E674D6F6465730053797374656D2E446174612E53716C54797065730063616368656453657474696E67730055736572446566696E656446756E6374696F6E7300436F6E636174004F626A656374006F705F496D706C6963697400457869740053797374656D2E446174612E53716C436C69656E7400436F6E76657274006765745F4E6F7700436F6E7461696E734B6579006F705F496E657175616C69747900456D7074790000000000097400720075006500002F63006F006E007400650078007400200063006F006E006E0065006300740069006F006E003D0074007200750065000080FD530065006C0065006300740020005B00560061006C00750065005D002000660072006F006D002000530054005300690074006500530065007400740069006E00670020005300200049006E006E006500720020004A006F0069006E00200053005400530065007400740069006E00670054007900700065002000540020004F006E00200053002E00530065007400740069006E0067005400790070006500490064003D0054002E004900640020005700680065007200650020004E0061006D0065003D0027004C006F00630061006C00540069006D0065005A006F006E0065002700200041006E00640020005300690074006500490064003D002700010327000151530065006C006500630074002000540069006D0065005A006F006E0065002000660072006F006D002000530049005300690074006500200053002000570068006500720065002000490064003D002700017ECA6CECF751AD488C5E7ADBE37B0A9000042001010803200001052001011111042001010E042001010205200101117505070202114D032000020420001155042000116103061161080002115511551265060001114D11550306114D0607021155114D04000011550407011159040001020E0500011159020D07071C12100202115512651265040001011C0815125D0211611210052001021300062001130113000700020211551155072002011300130104070202020E07080E0E126512690E126D02126502060E0320000E0600030E0E0E0E062002010E12690320001C05000112650E040000126508B77A5C561934E089090615125D021161121002061C0306115503061265080002114D114D1151060001114D11510600011159115106000112651161050001011161030000010520010112650520010111550420001265042800115504280012650801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F7773010801000701000000000C010007372E302E302E3000001B0100164272696467656C696E65204469676974616C20496E6300002101001C436F7079726967687420C2A9204272696467656C696E65203230313500000A01000569415050530000050100000000180100134272696467656C696E652E54696D655A6F6E6500008126010002005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A446174614163636573730100000054557F4D6963726F736F66742E53716C5365727665722E5365727665722E53797374656D446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038391053797374656D44617461416363657373010000008126010002005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A446174614163636573730000000054557F4D6963726F736F66742E53716C5365727665722E5365727665722E53797374656D446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038391053797374656D4461746141636365737300000000040100000008010000000000000000000000000084E2A45C00000000020000001C010000FC360000FC180000525344531B25A4623D0B1D47BB7C8C57694A37D101000000433A5C50726F64756374735C69415050535C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E54696D655A6F6E655C6F626A5C44656275675C4272696467656C696E652E54696D655A6F6E652E70646200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004038000000000000000000005A3800000020000000000000000000000000000000000000000000004C380000000000000000000000005F436F72446C6C4D61696E006D73636F7265652E646C6C0000000000FF25002000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058400000900300000000000000000000900334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100000007000000000000000700000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B004F0020000010053007400720069006E006700460069006C00650049006E0066006F000000CC02000001003000300030003000300034006200300000001A000100010043006F006D006D0065006E00740073000000000000004E001700010043006F006D00700061006E0079004E0061006D006500000000004200720069006400670065006C0069006E00650020004400690067006900740061006C00200049006E00630000000000500014000100460069006C0065004400650073006300720069007000740069006F006E00000000004200720069006400670065006C0069006E0065002E00540069006D0065005A006F006E0065000000300008000100460069006C006500560065007200730069006F006E000000000037002E0030002E0030002E003000000050001800010049006E007400650072006E0061006C004E0061006D00650000004200720069006400670065006C0069006E0065002E00540069006D0065005A006F006E0065002E0064006C006C0000005C001C0001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020004200720069006400670065006C0069006E0065002000320030003100350000002A00010001004C006500670061006C00540072006100640065006D00610072006B00730000000000000000005800180001004F0072006900670069006E0061006C00460069006C0065006E0061006D00650000004200720069006400670065006C0069006E0065002E00540069006D0065005A006F006E0065002E0064006C006C0000002C0006000100500072006F0064007500630074004E0061006D00650000000000690041005000500053000000340008000100500072006F006400750063007400560065007200730069006F006E00000037002E0030002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000037002E0030002E0030002E0030000000000000000000000000000000000000000000000000000000003000000C0000006C3800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
    WITH PERMISSION_SET = UNSAFE;


GO
ALTER ASSEMBLY [Bridgeline.TimeZone]
    DROP FILE ALL
    ADD FILE FROM 0x4D6963726F736F667420432F432B2B204D534620372E30300D0A1A4453000000000200000200000023000000B00000000000000021000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF38000000FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0BCA3101380000000010000000100000000000001000FFFF04000000FFFF03000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000BCA3101380000000010000000100000000000001100FFFF04000000FFFF0300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000942E310184E2A45C010000001B25A4623D0B1D47BB7C8C57694A37D100000000000000000100000001000000000000000000000000000000DC51330100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000BCA310138000000001000000010000000000000FFFFFFFF04000000FFFF030000000000FFFFFFFF00000000FFFFFFFF00000000FFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000BCA310138000000001000000010000000000000FFFFFFFF04000000FFFF030000000000FFFFFFFF00000000FFFFFFFF00000000FFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BDEC1618FF5EAA104D87F76F49638334601400000000000000699CB8A918B05932874B7A2E38BA3C687859098F0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ED03000000000000ED030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FEEFFEEF01000000C200000000433A5C50726F64756374735C69415050535C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E54696D655A6F6E655C436F6E7665727454696D6546726F6D546F5574632E63730000633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E74696D657A6F6E655C636F6E7665727474696D6566726F6D746F7574632E6373000400000061000000010000006200000000000000030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001BE2300180000000A230E3423CEAD401010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000200000001000000010000000000000062000000280000001BE23001B20B38585C000000010000006100000062000000650000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000004A002A11000000002C0100000000000049000000000000000000000001000006000000000100000000436F6E7665727454696D65546F5574635573696E675369746554696D655A6F6E6500001600031104000000EC0000004900000000000000010000000A0024115553797374656D001A0024115553797374656D2E446174612E53716C54797065730000001E002411554D6963726F736F66742E53716C5365727665722E536572766572001A0024115553797374656D2E446174612E53716C436C69656E7400001E0024115553797374656D2E436F6C6C656374696F6E732E47656E6572696300020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C00000001000500040600000C000000020C1601020006003A002A1100000000A80100000000000049000000000000000000000002000006490000000100000000436F6E7665727454696D6546726F6D557463003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600000C000000020C1601020006003A002A1100000000240200000000000049000000000000000000000003000006920000000100000000436F6E7665727454696D65546F5574630000003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600000C000000020C16010200060036002A11000000009C0200000000000039000000000000000000000004000006DB00000001000000004765744C6F63616C446174650000003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600010C00000000160100020006003E002A11000000001C0300000000000022000000000000000000000005000006140100000100000000436C65617243616368656454696D655A6F6E65000000003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600020C0000001601000002000600F200000060000000000000000100010049000000000000000600000054000000000000001000008001000000110000800C000000EEEFFE800F000000120000803F0000001300008047000000140000800500060009002300000000000D0095000900210005000600F200000060000000490000000100010049000000000000000600000054000000000000002000008001000000210000800C000000EEEFFE800F000000220000803F0000002300008047000000240000800500060009002100000000000D0095000900210005000600F200000060000000920000000100010049000000000000000600000054000000000000002E000080010000002F0000800C000000EEEFFE800F000000300000803F0000003100008047000000320000800500060009002300000000000D0095000900210005000600F20000003C000000DB0000000100010039000000000000000300000030000000000000003B000080010000003C000080370000003D0000800500060008009D0005000600F20000004800000014010000010001002200000000000000040000003C000000000000004700008001000000480000800E00000049000080200000004B000080050006000900380009002A0005000600F4000000080000000100000000000000280000000000000030000000480000006C00000084000000A4000000BC000000D8000000F000000014010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000002E002A110000000064000000000000001D00000000000000000000000D000006B203000001000000002E63746F7200002E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004010000040100000C000000010000060200060036002A1100000000A0000000000000000700000000000000000000000E000006CF03000001000000006765745F4C6F6164656454696D65000200060036002A1100000000DC000000000000000800000000000000000000000F000006D603000001000000007365745F4C6F6164656454696D65000200060036002A1100000000180100000000000007000000000000000000000010000006DE03000001000000006765745F54696D655A6F6E650000000200060036002A1100000000540100000000000008000000000000000000000011000006E503000001000000007365745F54696D655A6F6E6500000002000600F200000054000000B2030000010001001D00000000000000050000004800000000000000AD00008007000000AE00008008000000AF00008010000000B00000801C000000B10000800D0034000D000E001100250011002B000D000E00F200000024000000CF030000010001000700000000000000010000001800000000000000B20000802A002E00F200000024000000D6030000010001000800000000000000010000001800000000000000B20000802F003300F200000024000000DE030000010001000700000000000000010000001800000000000000B30000802C003000F200000024000000E5030000010001000800000000000000010000001800000000000000B300008031003500F4000000080000000100000000000000280000002C01000040010000580100007801000090010000B0010000C8010000E4010000FC0100001802000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000003A002A11000000001C010000000000008C0000000000000000000000070000063601000001000000004765745369746554696D655A6F6E65000000001600031104000000D00000008C00000036010000010000001600031140000000CC0000007300000044010000010000001E002011010000000400001100000000000000006C6F63616C446174610000001600031158000000C80000001F0000008E010000010000001E0020110500000004000011000000000000000074696D655A6F6E650000000002000600020006000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600011800000004100147026A0280DC0001815A160100020006003E002A11000000003C020000000000008C000000000000000000000008000006C201000001000000004765745369746554696D655A6F6E655F536974650000001600031120010000F00100008C000000C2010000010000001600031160010000EC01000073000000D0010000010000001E002011010000000400001100000000000000006C6F63616C446174610000001600031178010000E80100001F0000001A020000010000001E0020110500000004000011000000000000000074696D655A6F6E650000000002000600020006000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600011800000004100147026A0280E600018164160100020006003E002A1100000000BC02000000000000380000000000000000000000090000064E0200000100000000436C65617243616368656454696D655A6F6E65000000003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600000C000000021002640200060042002A110000000038040000000000008600000000000000000000000A00000686020000010000000047657454696D655A6F6E6546726F6D5369746553657474696E670016000311C0020000E80300008600000086020000010000001A002011000000000600001100000000000000006F757470757400002600201101000000060000110000000000000000636F6E6E636574696F6E537472696E67000000001E0020110200000006000011000000000000000074696D655A6F6E65000000001A00201103000000060000110000000000000000636F6E6E00000000160020110400000006000011000000000000000073716C001600031104030000E40300002E000000BA020000010000001600201105000000060000110000000000000000636D640002000600020006004A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600001C00000001170142016E0180D50181150181F302832F1601020006003E002A1100000000B0050000000000008700000000000000000000000B0000060C030000010000000047657454696D655A6F6E6546726F6D5369746500000000160003113C04000060050000870000000C030000010000001A002011000000000600001100000000000000006F757470757400002600201101000000060000110000000000000000636F6E6E636574696F6E537472696E67000000001E0020110200000006000011000000000000000074696D655A6F6E65000000001A00201103000000060000110000000000000000636F6E6E00000000160020110400000006000011000000000000000073716C00160003117C0400005C0500002E00000040030000010000001600201105000000060000110000000000000000636D640002000600020006004A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600001C00000001170142016E0180D501811501819D0282D91601020006002E002A110000000014060000000000001F00000000000000000000000C0000069303000001000000002E6363746F72002E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004010000040100000C0000000100000602000600F2000000E400000036010000010001008C0000000000000011000000D8000000000000005200008001000000530000800E000000540000800F0000005500008011000000560000801D000000EEEFFE8020000000570000802C0000005800008055000000EEEFFE805800000059000080590000005A000080610000005B000080690000005C000080760000005D000080770000005E00008081000000EEEFFE80890000006000008009000A000D0019000D000E0011002C001100380000000000150038001100590000000000110012001500500015003900150038001100120011002B000000000009000A00F2000000E4000000C2010000010001008C0000000000000011000000D8000000000000006300008001000000640000800E000000650000800F0000006600008011000000670000801D000000EEEFFE8020000000680000802C0000006900008055000000EEEFFE80580000006A000080590000006B000080610000006C000080690000006D000080760000006E000080770000006F00008081000000EEEFFE80890000007100008009000A000D0019000D000E0011002C0011003D000000000015003D00110059000000000011001200150049001500390015003D001100120011002B000000000009000A00F2000000780000004E020000010001003800000000000000080000006C000000000000007400008001000000750000800D000000EEEFFE8010000000750000801C0000007600008028000000EEEFFE802B00000076000080370000007700008009000A000D00340000000000350053000D003900000000003A005D0009000A00F20000003801000086020000010001008600000000000000180000002C010000000000007A000080010000007B000080070000007D000080090000007E0000800F0000007F0000801600000080000080340000008200008035000000830000803F00000084000080460000008500008053000000860000805A00000087000080610000008800008064000000890000806500000089000080660000008900008069000000EEEFFE806B0000008B0000806C0000008C00008072000000EEEFFE80760000008D0000807C0000008E0000807E0000008F000080830000009000008009000A000D002A000D002A000D003A000D0046000D00B5000D000E0011003C0011001D001100390011001E00110048000D000E000D0012001300140015001600000000000D000E001100260000000000150033000D000E000D001D0009000A00F2000000380100000C030000010001008700000000000000180000002C01000000000000930000800100000094000080070000009600008009000000970000800F000000980000801600000099000080340000009B000080350000009C0000803F0000009D000080460000009E000080530000009F0000805A000000A000008061000000A100008064000000A200008065000000A200008066000000A200008069000000EEEFFE806B000000A40000806C000000A500008072000000EEEFFE8076000000A60000807D000000A70000807F000000A800008084000000A900008009000A000D002A000D002A000D003A000D0046000D005F000D000E0011003C0011001D001100390011001E00110048000D000E000D0012001300140015001600000000000D000E001100260000000000150043000D000E000D001D0009000A00F20000003C00000093030000010001001F000000000000000300000030000000000000004E0000800A0000004F0000801400000050000080090070000900750009003D00F4000000080000000100000000000000300000003002000050020000680200008C020000A4020000C8020000E00200000C0300002403000048030000600300007803000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFF1A092FF1000100007C0200006D0000000100000049030000010000006902000001000000A500000001000000790300000100000025030000010000003102000001000000A502000001000000F100000001000000E5010000010000002D01000001000000FD010000010000000100000001000000C90100000100000031000000010000000D030000010000001902000001000000B1010000010000005102000001000000590100000100000085000000010000009101000001000000D90000000100000041010000010000001501000001000000790100000100000061030000010000008D02000001000000E102000001000000C9020000010000004900000001000000BD00000001000000010400000000000000000000000000000000000000000020000000000000000001040000004000000000000000000000000001000000800000000000000000000000000002000000000000000000000000020000000000000000000000020024010400000200000000000000000000000000000000000000000000000000000000040000000000000000000000000000000000000000000000000000000000000100000000000000000000000002002000000000000000000000000000000000010400000000000000000000000000000000000000000000000000000000000001040000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000400000000000000000000000000000000000002000000000000000000010000000000000000000000000000000000000000000200040000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000C0000001800000024000000300000003C00000048000000540000006C0000007800000084000000900000009C000000A8000000B4000000C0000000CC000000D8000000E4000000FC0000000801000014010000200100002C0100003801000044010000500100005C010000680100007401000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002E00251100000000040000000100436F6E7665727454696D65546F5574635573696E675369746554696D655A6F6E65001600291100000000040000000100303630303030303100002200251100000000300100000100436F6E7665727454696D6546726F6D557463000000001600291100000000300100000100303630303030303200001E00251100000000AC0100000100436F6E7665727454696D65546F55746300001600291100000000AC0100000100303630303030303300001A002511000000002802000001004765744C6F63616C4461746500001600291100000000280200000100303630303030303400002200251100000000A00200000100436C65617243616368656454696D655A6F6E650000001600291100000000A002000001003036303030303035000012002511000000000400000003002E63746F72001600291100000000040000000300303630303030306400001E002511000000006800000003006765745F4C6F6164656454696D65000000001600291100000000680000000300303630303030306500001E00251100000000A400000003007365745F4C6F6164656454696D65000000001600291100000000A40000000300303630303030306600001A00251100000000E000000003006765745F54696D655A6F6E6500001600291100000000E00000000300303630303030313000001A00251110000000000000000000000000000000000000000000000000000000FFFFFFFF1A092FF10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001C01000003007365745F54696D655A6F6E65000016002911000000001C0100000300303630303030313100001E002511000000000400000002004765745369746554696D655A6F6E6500000016002911000000000400000002003036303030303037000022002511000000002001000002004765745369746554696D655A6F6E655F5369746500001600291100000000200100000200303630303030303800002200251100000000400200000200436C65617243616368656454696D655A6F6E650000001600291100000000400200000200303630303030303900002A00251100000000C0020000020047657454696D655A6F6E6546726F6D5369746553657474696E67000000001600291100000000C002000002003036303030303061000022002511000000003C040000020047657454696D655A6F6E6546726F6D5369746500000016002911000000003C0400000200303630303030306200001600251100000000B405000002002E6363746F72000000001600291100000000B405000002003036303030303063000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFF77093101010000000D00108E0E0084690F00060044010000C40100002C0000007C000000000000000000000016000000190000000000EEC00000000000000000FFFF000000000000FFFFFFFF00000000FFFF0000000000000000000000000A002003000000000000DC0100000100000000000000000000000000000055736572446566696E656446756E6374696F6E7300373141414145453800000000000000FFFF000000000000FFFFFFFF00000000FFFF0000000000000000000000000C0018060000000000002C0500000100000000000000000000000000000055736572446566696E656446756E6374696F6E732E5369746554696D655A6F6E65004638383841444333000000000000FFFF000000000000FFFFFFFF00000000FFFF0000000000000000000000000B0058010000000000001C0100000100000000000000000000000000000055736572446566696E656446756E6374696F6E732E5369746554696D655A6F6E652E4C6F63616C44617461003937383337413136000000002DBA2EF101000000000000004900000000000000000000000000000000000000010000004900000049000000000000000000000000000000000000000100000092000000490000000000000000000000000000000000000001000000DB00000039000000000000000000000000000000000000000100000014010000220000000000000000000000000000000000000001000000360100008C0000000000000001000000000000000000000001000000C20100008C00000000000000010000000000000000000000010000004E020000380000000000000001000000000000000000000001000000860200008600000000000000010000000000000000000000010000000C030000870000000000000001000000000000000000000001000000930300001F0000000000000001000000000000000000000001000000B20300001D0000000000000002000000000000000000000001000000CF030000070000000000000002000000000000000000000001000000D6030000080000000000000002000000000000000000000001000000DE030000070000000000000002000000000000000000000001000000E50300000800000000000000020000000000000000000000020002000D01000000000100FFFFFFFF00000000ED0300000802000000000000FFFFFFFF00000000FFFFFFFF03000300000001000200010001000100000000000000000000000000433A5C50726F64756374735C69415050535C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E54696D655A6F6E655C436F6E7665727454696D6546726F6D546F5574632E637300FEEFFEEF010000000100000000010000000000000000000000FFFFFFFFFFFFFFFFFFFF0900FFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000942E310184E2A45C010000001B25A4623D0B1D47BB7C8C57694A37D18D0000002F4C696E6B496E666F002F6E616D6573002F7372632F686561646572626C6F636B002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E74696D657A6F6E655C636F6E7665727474696D6566726F6D746F7574632E6373000400000006000000010000003A0000000000000011000000070000000A000000060000000000000005000000220000000800000000000000DC5133010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000020000000E9000000380000001F0400003800000000000000E6000000800000005C0000002800000028050000A0020000780B00008C0300002C00000090030000030000001F000000060000001C0000001D0000001E000000070000000A0000000B00000008000000090000000C0000000D0000000E0000000F0000001000000011000000120000001300000014000000150000001600000017000000180000001A000000190000001B00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 AS N'Bridgeline.TimeZone.pdb';


GO
PRINT N'Creating [dbo].[ClearCachedTimeZone]...';


GO
CREATE FUNCTION [dbo].[ClearCachedTimeZone]
(@siteId UNIQUEIDENTIFIER)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.TimeZone].[UserDefinedFunctions].[ClearCachedTimeZone]


GO
PRINT N'Creating [dbo].[ConvertTimeFromUtc]...';


GO
CREATE FUNCTION [dbo].[ConvertTimeFromUtc]
(@utcDateTime DATETIME, @siteId UNIQUEIDENTIFIER)
RETURNS DATETIME
AS
 EXTERNAL NAME [Bridgeline.TimeZone].[UserDefinedFunctions].[ConvertTimeFromUtc]


GO
PRINT N'Creating [dbo].[ConvertTimeToUtc]...';


GO
CREATE FUNCTION [dbo].[ConvertTimeToUtc]
(@localDateTime DATETIME, @siteId UNIQUEIDENTIFIER)
RETURNS DATETIME
AS
 EXTERNAL NAME [Bridgeline.TimeZone].[UserDefinedFunctions].[ConvertTimeToUtc]


GO
PRINT N'Creating [dbo].[GetLocalDate]...';


GO
CREATE FUNCTION [dbo].[GetLocalDate]
(@siteId UNIQUEIDENTIFIER)
RETURNS DATETIME
AS
 EXTERNAL NAME [Bridgeline.TimeZone].[UserDefinedFunctions].[GetLocalDate]


GO
PRINT N'Creating [dbo].[ConvertTimeToUtcUsingSiteTimeZone]...';


GO
CREATE FUNCTION [dbo].[ConvertTimeToUtcUsingSiteTimeZone]
(@localDateTime DATETIME, @siteId UNIQUEIDENTIFIER)
RETURNS DATETIME
AS
 EXTERNAL NAME [Bridgeline.TimeZone].[UserDefinedFunctions].[ConvertTimeToUtcUsingSiteTimeZone]


GO
PRINT N'Update complete.';
GO

PRINT 'Add SiteId column to ATFacetRange'
GO

IF(COL_LENGTH('ATFacetRange', 'SiteId') IS NULL)
	ALTER TABLE ATFacetRange ADD SiteId UNIQUEIDENTIFIER NULL
GO

PRINT 'Migrate SiteId values to ATFacetRange'
GO

UPDATE	FR
SET		SiteId = F.SiteId
FROM	ATFacetRange AS FR
		INNER JOIN ATFacet AS F ON F.Id = FR.FacetId
WHERE	FR.SiteId IS NULL

PRINT 'Set SiteId column on ATFacetRange to non-nullable'
GO

ALTER TABLE ATFacetRange ALTER COLUMN SiteId UNIQUEIDENTIFIER NOT NULL
GO

PRINT 'Creating table ATFacetVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ATFacetVariant'))
BEGIN
	CREATE TABLE [dbo].[ATFacetVariant] (
		[Id]            UNIQUEIDENTIFIER NOT NULL,
		[SiteId]        UNIQUEIDENTIFIER NOT NULL,   
		[Title]         NVARCHAR (50)    NULL,
		[Description]   NVARCHAR (200)   NULL,
		[ModifiedDate]  DATETIME         NULL,
		[ModifiedBy]    UNIQUEIDENTIFIER NULL,    
		CONSTRAINT [PK_ATFacetVariant] PRIMARY KEY CLUSTERED (
			[Id] ASC,
			[SiteId] ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

PRINT 'Creating table AtFacetVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'AtFacetVariantCache'))
BEGIN
	CREATE TABLE [dbo].[ATFacetVariantCache] (
		[Id]            UNIQUEIDENTIFIER NOT NULL,
		[SiteId]        UNIQUEIDENTIFIER NOT NULL,   
		[Title]         NVARCHAR (50)    NOT NULL,
		[Description]   NVARCHAR (200)   NULL,
		[ModifiedDate]  DATETIME         NULL,
		[ModifiedBy]    UNIQUEIDENTIFIER NULL,    
		CONSTRAINT [PK_ATFacetVariantCache] PRIMARY KEY ([Id], [SiteId])
	)
END
GO

PRINT 'Modify view vwFacet'
GO
IF(OBJECT_ID('vwFacet') IS NOT NULL)
	DROP VIEW vwFacet
GO
CREATE VIEW [dbo].[vwFacet]
AS  
WITH CTE AS  
(  
	SELECT	Id, SiteId, Title, [Description],
			COALESCE(ModifiedDate, CreatedDate) AS ModifiedDate, COALESCE(ModifiedBy, CreatedBy) AS ModifiedBy
	FROM	ATFacet
  
	UNION ALL  
  
	SELECT	Id, SiteId, Title, [Description], ModifiedDate, ModifiedBy
	FROM	ATFacetVariantCache
)  
	SELECT	F.Id, CTE.SiteId, F.SiteId AS SourceSiteId, F.AttributeID, F.Limit, F.IsRange, CTE.Title, CTE.[Description],
			F.DisplayOrder, F.IsManualOrder, F.AllowMultiple, A.Title AS AttributeName, ADT.[Type] AS DataType, F.[Status],
			F.CreatedDate, F.CreatedBy, CFN.UserFullName AS CreatedByFullName,
			CTE.ModifiedDate, CTE.ModifiedBy, MFN.UserFullName AS ModifiedByFullName
	FROM	CTE
			INNER JOIN ATFacet AS F ON F.Id = CTE.Id
			INNER JOIN (
				SELECT	DISTINCT Id, Title, SiteId, AttributeDataTypeId
				FROM	vwAttribute
			) AS A ON A.Id = F.AttributeID AND A.SiteId = CTE.SiteId
			LEFT JOIN ATAttributeDataType AS ADT ON ADT.Id = A.AttributeDataTypeId
			LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = F.CreatedBy  
			LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = CTE.ModifiedBy
GO

PRINT 'Creating table ATFacetRangeVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ATFacetRangeVariant'))
BEGIN
	CREATE TABLE [dbo].[ATFacetRangeVariant] (
		[Id]            UNIQUEIDENTIFIER NOT NULL,
		[SiteId]        UNIQUEIDENTIFIER NOT NULL,      
		[DisplayText]   NVARCHAR (50)    NULL,
		[AlternateText] NVARCHAR (50)    NULL,
		[ModifiedBy]    UNIQUEIDENTIFIER NULL,
		[ModifiedDate]  DATETIME         NULL,
		CONSTRAINT [PK_ATFacetRangeVariant] PRIMARY KEY CLUSTERED (
			[Id] ASC,
			[SiteId] ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

PRINT 'Creating table AtFacetRangeVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'AtFacetRangeVariantCache'))
BEGIN
	CREATE TABLE [dbo].[ATFacetRangeVariantCache] (
		[Id]            UNIQUEIDENTIFIER NOT NULL,
		[SiteId]        UNIQUEIDENTIFIER NOT NULL,      
		[DisplayText]   NVARCHAR (50)    NOT NULL,
		[AlternateText] NVARCHAR (50)    NULL,
		[ModifiedBy]    UNIQUEIDENTIFIER NULL,
		[ModifiedDate]  DATETIME         NULL,
		CONSTRAINT [PK_ATFacetRangeVariantCache] PRIMARY KEY ([Id], [SiteId])
	)
END
GO

PRINT 'Modify view vwFacetRange'
GO
IF(OBJECT_ID('vwFacetRange') IS NOT NULL)
	DROP VIEW vwFacetRange
GO
CREATE VIEW [dbo].[vwFacetRange]
AS  
WITH CTE AS  
(  
	SELECT	FR.Id, FR.SiteId, FR.DisplayText, FR.AlternateText,
			COALESCE(FR.ModifiedDate, FR.CreatedDate) AS ModifiedDate, COALESCE(FR.ModifiedBy, FR.CreatedBy) AS ModifiedBy
	FROM	ATFacetRange AS FR
  
	UNION ALL  
  
	SELECT	Id, SiteId, DisplayText, AlternateText, ModifiedDate, ModifiedBy
	FROM	ATFacetRangeVariantCache
)  
	SELECT	FR.Id, CTE.SiteId, FR.SiteId AS SourceSiteId, FR.FacetID, CTE.DisplayText, CTE.AlternateText, FR.[Sequence], FR.[Status], FRV.[From], FRV.[To],
			FR.CreatedDate, FR.CreatedBy, CFN.UserFullName AS CreatedByFullName,
			CTE.ModifiedDate, CTE.ModifiedBy, MFN.UserFullName AS ModifiedByFullName
	FROM	CTE
			INNER JOIN ATFacetRange AS FR ON FR.Id = CTE.Id
			INNER JOIN (
				SELECT FacetRangeId, CAST([From] AS NVARCHAR(50)) AS [From], CAST([To] AS NVARCHAR(50)) AS [To] FROM ATFacetRangeDate

				UNION ALL

				SELECT FacetRangeId, CAST([From] AS NVARCHAR(50)) AS [From], CAST([To] AS NVARCHAR(50)) AS [To] FROM ATFacetRangeDecimal

				UNION ALL

				SELECT FacetRangeId, CAST([From] AS NVARCHAR(50)) AS [From], CAST([To] AS NVARCHAR(50)) AS [To] FROM ATFacetRangeInt

				UNION ALL

				SELECT FacetRangeId, [From], [To] FROM ATFacetRangeString
			) AS FRV ON FRV.FacetRangeID = FR.Id
			LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = FR.CreatedBy  
			LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = CTE.ModifiedBy
GO

PRINT 'Migrating variant facet data'
GO

IF (NOT EXISTS(SELECT * FROM ATFacetVariantCache))
BEGIN
	INSERT INTO ATFacetVariantCache (Id, SiteId, Title, [Description], ModifiedBy, ModifiedDate)
		SELECT	F.Id, S.Id, F.Title, F.[Description], F.ModifiedBy, F.ModifiedDate
		FROM    SISite AS S
				INNER JOIN ATFacet AS F ON F.SiteId = S.MasterSiteId
		WHERE	S.Id != S.MasterSiteId
END
GO

PRINT 'Migrating variant facet range data'
GO

IF (NOT EXISTS(SELECT * FROM ATFacetRangeVariantCache))
BEGIN
	INSERT INTO ATFacetRangeVariantCache (Id, SiteId, DisplayText, AlternateText, ModifiedBy, ModifiedDate)
		SELECT	FR.Id, S.Id, FR.DisplayText, FR.AlternateText, FR.ModifiedBy, FR.ModifiedDate
		FROM    SISite AS S
				INNER JOIN ATFacet AS F ON F.SiteId = S.MasterSiteId
				INNER JOIN ATFacetRange AS FR ON FR.FacetId = F.Id
		WHERE	S.Id != S.MasterSiteId
END
GO

PRINT 'Modify stored procedure Facet_SaveWithRange'
GO

IF(OBJECT_ID('Facet_SaveWithRange') IS NOT NULL)
	DROP PROCEDURE Facet_SaveWithRange
GO

CREATE PROCEDURE [dbo].[Facet_SaveWithRange](
--********************************************************************************
-- Parameters Passing in ItemAttribute 
--********************************************************************************
			@Id uniqueidentifier = null OUTPUT,
			@IsRange bit, 
			@Title nvarchar(50),
			@Limit int,
			@ParentAttributeId uniqueidentifier,
			@DisplayOrder int,
			@ModifiedDate datetime,
			@ModifiedBy uniqueidentifier,
			@Status int,
			@IsManualOrder bit,
			@AllowMultiple bit,
			@AddedRange xml,
			@EditedRange xml,
			@DeleteRange xml,
			@ApplicationId uniqueidentifier
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Now dateTime
DECLARE @NewGuid uniqueidentifier
DECLARE @RowCount int
DECLARE @FacetDataType nvarchar(50)

--********************************************************************************
-- code
--********************************************************************************
BEGIN
	EXEC Facet_Save @Id OUTPUT,
			@IsRange, 
			@Title,
			@Limit,
			@ParentAttributeId,
			@DisplayOrder,
			@ModifiedDate,
			@ModifiedBy,
			@Status,
			@IsManualOrder,
			@AllowMultiple,
			@ApplicationId

IF (Select count(F.Id) from ATFacet  F
INNER JOIN ATAttribute A
ON F.AttributeID=A.Id
Where (F.Title)=(@Title) AND F.AttributeID=@ParentAttributeId AND F.SiteId = @ApplicationId  AND  F.Id !=@Id AND F.Status!=dbo.GetDeleteStatus())>0
	   BEGIN
				RAISERROR('EXISTS||Name', 16, 1)
				RETURN dbo.GetBusinessRuleErrorCode()
		   END

	Select @FacetDataType = (AttributeDataType)
	FROM 
		ATFacet F INNER JOIN  ATAttribute A
		ON F.AttributeID = A.Id
	WHERE
		F.Id = @Id   
SET @Now = getutcdate()

	IF (@FacetDataType ='SYSTEM.INT32' OR @FacetDataType ='INT32' OR @FacetDataType='SYSTEM.INT16'OR 
			@FacetDataType='INT16' OR @FacetDataType='SYSTEM.BOOLEAN'OR @FacetDataType='BOOLEAN')
	BEGIN
		DECLARE @TempFacetRangeInt TABLE (
		Id uniqueidentifier,
		FacetID uniqueidentifier,
		[From] int,
		[To] int,
		DisplayText nvarchar(50),
		AlternateText nvarchar(50),
		Sequence int,
		CreatedBy uniqueidentifier,
		CreatedDate datetime,
		Status int )

		INSERT INTO @TempFacetRangeInt(Id,FacetID,[From],[To],DisplayText,AlternateText,Sequence,CreatedBy,CreatedDate,Status)
		SELECT	newId(), @Id, 
				tab.col.value('FromValue[1]/text()[1]','int') [From],
				tab.col.value('ToValue[1]/text()[1]','int') [To],
				tab.col.value('DisplayText[1]/text()[1]','nvarchar(50)') DisplayText,
				tab.col.value('AlternateText[1]/text()[1]','nvarchar(50)') AlternateText,
				tab.col.value('Sequence[1]/text()[1]','int') Sequence,
				@ModifiedBy,
				@Now,
				dbo.GetActiveStatus()
  		FROM @AddedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col)

		INSERT INTO ATFacetRange(Id,FacetID,DisplayText,AlternateText,Sequence,CreatedBy,CreatedDate,Status,SiteId)
		SELECT  TFR.Id,
				TFR.FacetID,
				TFR.DisplayText,
				TFR.AlternateText,
				TFR.Sequence,
				TFR.CreatedBy,
				TFR.CreatedDate,
				TFR.Status,
				@ApplicationId
		FROM @TempFacetRangeInt TFR

		INSERT INTO ATFacetRangeInt(Id,FacetRangeID,[From],[To])
		SELECT	newId(), TFR.Id, 
				TFR.[From],
				TFR.[To]
  		FROM @TempFacetRangeInt TFR
	
		
		UPDATE R Set R.[DisplayText]=tab.col.value('DisplayText[1]/text()[1]','nvarchar(50)'),
					R.[AlternateText]=tab.col.value('AlternateText[1]/text()[1]','nvarchar(50)'),
					R.Sequence=tab.col.value('Sequence[1]/text()[1]','int') 
		FROM ATFacetRange R 
		INNER JOIN @EditedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col) 
			ON R.Id =tab.col.value('Id[1]/text()[1]','uniqueidentifier')

		UPDATE R Set R.[From] =tab.col.value('FromValue[1]/text()[1]','int'),
					R.[To]=tab.col.value('ToValue[1]/text()[1]','int')
		FROM ATFacetRangeInt R 
		INNER JOIN @EditedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col) 
			ON R.FacetRangeID =tab.col.value('Id[1]/text()[1]','uniqueidentifier')

	END
	ELSE IF @FacetDataType ='SYSTEM.DATETIME' OR @FacetDataType ='DATETIME'
	BEGIN
		DECLARE @TempFacetRangeDate TABLE (
		Id uniqueidentifier,
		FacetID uniqueidentifier,
		[From] datetime,
		[To] datetime,
		DisplayText nvarchar(50),
		AlternateText nvarchar(50),
		Sequence int,
		CreatedBy uniqueidentifier,
		CreatedDate datetime,
		Status int )

		INSERT INTO @TempFacetRangeDate(Id,FacetID,[From],[To],DisplayText,AlternateText,Sequence,CreatedBy,CreatedDate,Status)
		SELECT	newId(), @Id, 
				tab.col.value('FromValue[1]/text()[1]','datetime') [From],
				tab.col.value('ToValue[1]/text()[1]','datetime') [To],
				tab.col.value('DisplayText[1]/text()[1]','nvarchar(50)') DisplayText,
				tab.col.value('AlternateText[1]/text()[1]','nvarchar(50)') AlternateText,
				tab.col.value('Sequence[1]/text()[1]','int') Sequence,
				@ModifiedBy,
				@Now,
				dbo.GetActiveStatus()
  		FROM @AddedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col)

		INSERT INTO ATFacetRange(Id,FacetID,DisplayText,AlternateText,Sequence,CreatedBy,CreatedDate,Status,SiteId)
		SELECT  TFR.Id,
				TFR.FacetID,
				TFR.DisplayText,
				TFR.AlternateText,
				TFR.Sequence,
				TFR.CreatedBy,
				TFR.CreatedDate,
				TFR.Status,
				@ApplicationId
		FROM @TempFacetRangeDate TFR

		INSERT INTO ATFacetRangeDate(Id,FacetRangeID,[From],[To])
		SELECT	newId(), TFR.Id, 
				TFR.[From][From],
				TFR.[To][To]
  		FROM @TempFacetRangeDate TFR


		UPDATE R Set R.[DisplayText]=tab.col.value('DisplayText[1]/text()[1]','nvarchar(50)'),
					R.[AlternateText]=tab.col.value('AlternateText[1]/text()[1]','nvarchar(50)'),
					R.Sequence=tab.col.value('Sequence[1]/text()[1]','int') 
		FROM ATFacetRange R 
		INNER JOIN @EditedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col) 
			ON R.Id =tab.col.value('Id[1]/text()[1]','uniqueidentifier')

		UPDATE R Set R.[From] =tab.col.value('FromValue[1]/text()[1]','datetime'),
					R.[To]=tab.col.value('ToValue[1]/text()[1]','datetime')
		FROM ATFacetRangeDate R 
		INNER JOIN @EditedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col) 
			ON R.FacetRangeID =tab.col.value('Id[1]/text()[1]','uniqueidentifier')

	END
	ELSE IF @FacetDataType ='SYSTEM.DOUBLE' OR @FacetDataType ='DOUBLE'
	BEGIN
		
		DECLARE @TempFacetRangeDecimal TABLE (
		Id uniqueidentifier,
		FacetID uniqueidentifier,
		[From] decimal(14, 4),
		[To] decimal(14, 4),
		DisplayText nvarchar(50),
		AlternateText nvarchar(50),
		Sequence int,
		CreatedBy uniqueidentifier,
		CreatedDate datetime,
		Status int )

		INSERT INTO @TempFacetRangeDecimal(Id,FacetID,[From],[To],DisplayText,AlternateText,Sequence,CreatedBy,CreatedDate,Status)
		SELECT	newId(), @Id, 
				tab.col.value('FromValue[1]/text()[1]','decimal(14,4)') [From],
				tab.col.value('ToValue[1]/text()[1]','decimal(14,4)') [To],
				tab.col.value('DisplayText[1]/text()[1]','nvarchar(50)') DisplayText,
				tab.col.value('AlternateText[1]/text()[1]','nvarchar(50)') AlternateText,
				tab.col.value('Sequence[1]/text()[1]','int') Sequence,
				@ModifiedBy,
				@Now,
				dbo.GetActiveStatus()
  		FROM @AddedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col)

		INSERT INTO ATFacetRange(Id,FacetID,DisplayText,AlternateText,Sequence,CreatedBy,CreatedDate,Status,SiteId)
		SELECT  TFR.Id,
				TFR.FacetID,
				TFR.DisplayText,
				TFR.AlternateText,
				TFR.Sequence,
				TFR.CreatedBy,
				TFR.CreatedDate,
				TFR.Status,
				@ApplicationId
		FROM @TempFacetRangeDecimal TFR

		INSERT INTO ATFacetRangeDecimal(Id,FacetRangeID,[From],[To])
		SELECT	newId(), TFR.Id, 
				TFR.[From],
				TFR.[To]
  		FROM @TempFacetRangeDecimal TFR



		UPDATE R Set R.[DisplayText]=tab.col.value('DisplayText[1]/text()[1]','nvarchar(50)'),
					R.[AlternateText]=tab.col.value('AlternateText[1]/text()[1]','nvarchar(50)'),
					R.Sequence=tab.col.value('Sequence[1]/text()[1]','int') 
		FROM ATFacetRange R 
		INNER JOIN @EditedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col) 
			ON R.Id =tab.col.value('Id[1]/text()[1]','uniqueidentifier')

		UPDATE R Set R.[From] =tab.col.value('FromValue[1]/text()[1]','decimal(14,4)'),
					R.[To]=tab.col.value('ToValue[1]/text()[1]','decimal(14,4)')
		FROM ATFacetRangeDecimal R 
		INNER JOIN @EditedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col) 
			ON R.FacetRangeID =tab.col.value('Id[1]/text()[1]','uniqueidentifier')

	END
	ELSE 
	BEGIN

		DECLARE @TempFacetRangeString TABLE (
		Id uniqueidentifier,
		FacetID uniqueidentifier,
		[From] nvarchar(max),
		[To] nvarchar(max),
		DisplayText nvarchar(50),
		AlternateText nvarchar(50),
		Sequence int,
		CreatedBy uniqueidentifier,
		CreatedDate datetime,
		Status int )

		INSERT INTO @TempFacetRangeString(Id,FacetID,[From],[To],DisplayText,AlternateText,Sequence,CreatedBy,CreatedDate,Status)
		SELECT	newId(), @Id, 
				tab.col.value('FromValue[1]/text()[1]','nvarchar(max)') [From],
				tab.col.value('ToValue[1]/text()[1]','nvarchar(max)') [To],
				tab.col.value('DisplayText[1]/text()[1]','nvarchar(50)') DisplayText,
				tab.col.value('AlternateText[1]/text()[1]','nvarchar(50)') AlternateText,
				tab.col.value('Sequence[1]/text()[1]','int') Sequence,
				@ModifiedBy,
				@Now,
				dbo.GetActiveStatus()
  		FROM @AddedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col)

		INSERT INTO ATFacetRange(Id,FacetID,DisplayText,AlternateText,Sequence,CreatedBy,CreatedDate,Status,SiteId)
		SELECT  TFR.Id,
				TFR.FacetID,
				TFR.DisplayText,
				TFR.AlternateText,
				TFR.Sequence,
				TFR.CreatedBy,
				TFR.CreatedDate,
				TFR.Status,
				@ApplicationId
		FROM @TempFacetRangeString TFR

		INSERT INTO ATFacetRangeString(Id,FacetRangeID,[From],[To])
		SELECT	newId(), TFR.Id, 
				TFR.[From],
				TFR.[To]
  		FROM @TempFacetRangeString TFR

		UPDATE R Set R.[DisplayText]=tab.col.value('DisplayText[1]/text()[1]','nvarchar(50)'),
					R.[AlternateText]=tab.col.value('AlternateText[1]/text()[1]','nvarchar(50)'),
					R.Sequence=tab.col.value('Sequence[1]/text()[1]','int') 
		FROM ATFacetRange R 
		INNER JOIN @EditedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col) 
			ON R.Id =tab.col.value('Id[1]/text()[1]','uniqueidentifier')

		UPDATE R Set R.[From] =tab.col.value('FromValue[1]/text()[1]','nvarchar(max)'),
					R.[To]=tab.col.value('ToValue[1]/text()[1]','nvarchar(max)')
		FROM ATFacetRangeString R 
		INNER JOIN @EditedRange.nodes('/GenericCollectionOfFacetRange/FacetRange')tab(col) 
			ON R.FacetRangeID =tab.col.value('Id[1]/text()[1]','uniqueidentifier')

	END
	
		Delete R 
		From ATFacetRangeInt R
		Inner Join @DeleteRange.nodes('/GenericCollectionOfGuid/guid')tab(col) 
			ON R.FacetRangeID =tab.col.value('text()[1]','uniqueidentifier')
		Delete R 
		From ATFacetRangeDate R
		Inner Join @DeleteRange.nodes('/GenericCollectionOfGuid/guid')tab(col) 
			ON R.FacetRangeID =tab.col.value('text()[1]','uniqueidentifier')
		Delete R 
		From ATFacetRangeDecimal R
		Inner Join @DeleteRange.nodes('/GenericCollectionOfGuid/guid')tab(col) 
			ON R.FacetRangeID =tab.col.value('text()[1]','uniqueidentifier')
		Delete R 
		From ATFacetRangeString R
		Inner Join @DeleteRange.nodes('/GenericCollectionOfGuid/guid')tab(col) 
			ON R.FacetRangeID =tab.col.value('text()[1]','uniqueidentifier')
		Delete R 
		From ATFacetRange R
		Inner Join @DeleteRange.nodes('/GenericCollectionOfGuid/guid')tab(col) 
			ON R.Id =tab.col.value('text()[1]','uniqueidentifier')
END
GO

PRINT 'Add columns to ATFacetRangeProduct_Cache'
GO
IF(COL_LENGTH('ATFacetRangeProduct_Cache', 'SiteId') IS NULL)
	ALTER TABLE ATFacetRangeProduct_Cache ADD SiteId uniqueidentifier
GO
PRINT 'Add columns to ATFacetRangeProductTop_Cache'
GO
IF(COL_LENGTH('ATFacetRangeProductTop_Cache', 'SiteId') IS NULL)
	ALTER TABLE ATFacetRangeProductTop_Cache ADD SiteId uniqueidentifier

GO

Declare @SiteId uniqueidentifier
SET @SiteId = (Select top 1 Id from SISite Where ParentSiteId is null OR ParentSiteId='00000000-0000-0000-0000-000000000000' AND Status=1)
IF EXISTS (select 1 from ATFacetRangeProduct_Cache Where SiteId IS NULL)
	Update ATFacetRangeProduct_Cache SET SiteId=@SiteId
IF EXISTS (select 1 from ATFacetRangeProductTop_Cache Where SiteId IS NULL)
	Update ATFacetRangeProductTop_Cache SET SiteId=@SiteId
GO
IF(COL_LENGTH('ATFacetRangeProduct_Cache', 'SiteId') IS NOT NULL)
	ALTER TABLE ATFacetRangeProduct_Cache ALTER COLUMN SiteId uniqueidentifier NOT NULL

GO
IF(COL_LENGTH('ATFacetRangeProductTop_Cache', 'SiteId') IS NOT NULL)
	ALTER TABLE ATFacetRangeProductTop_Cache ALTER COLUMN SiteId uniqueidentifier NOT NULL

GO
ALTER TABLE ATFacetRangeProductTop_Cache DROP CONSTRAINT [PK_ATFacetRangeProductTop_Cache_1] 
GO
ALTER TABLE ATFacetRangeProductTop_Cache ADD CONSTRAINT [PK_ATFacetRangeProductTop_Cache_1] PRIMARY KEY CLUSTERED ([FacetID] ASC, [ProductID] ASC,[SiteId] ASC)
GO

GO
IF(OBJECT_ID('Facet_GetFacetsByNavigation') IS NOT NULL)
	DROP PROCEDURE Facet_GetFacetsByNavigation
GO

CREATE PROCEDURE [dbo].[Facet_GetFacetsByNavigation]
    (
      @Id UNIQUEIDENTIFIER ,
      @ExistingFacetValueIds XML = NULL ,
      @ExistingFacets XML = NULL ,
      @UseLimit BIT = 1,
	  @ApplicationId uniqueidentifier=NULL
    )
AS 
    BEGIN

        DECLARE @filterId UNIQUEIDENTIFIER
        SELECT  @filterId = QueryId
        FROM    NVNavNodeNavFilterMap
        WHERE   NavNodeId = @Id

        DECLARE @totProd INT
        SELECT  @totProd = COUNT(C.Id)
        FROM    ATFacetRangeProduct_Cache C
		INNER JOIN VWProduct P ON C.ProductId=P.Id AND P.SiteId=@ApplicationId  
		Where C.SiteId=@ApplicationId
		Group By P.Id

        SET @UseLimit = ISNULL(@UseLimit, 0)
        IF ( @ExistingFacetValueIds IS NULL
             AND @ExistingFacets IS NULL
           ) 
            BEGIN

                SELECT 
			C.FacetID,
			C.FacetValueID,
			MIN(C.DisplayText) DisplayText,
			Count(*) ProductCount,
			MIN(NF.Sequence) FacetSequence,
			Case FR.Sequence 
				WHEN NULL  THEN @totProd - COUNT(*)
				ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
			END AS SortOrder,
			MIN(AF.AllowMultiple) AllowMultiple
		FROM 
			ATFacetRangeProduct_Cache C 
			INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
			INNER JOIN ATFacet AF ON AF.Id = NF.FacetId	
			INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
			INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
			LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
			LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
						AND C.SiteId=@ApplicationId
                GROUP BY C.FacetValueID ,
                        C.FacetID
                UNION ALL
                SELECT  C.FacetID ,
                        NULL FacetValueID ,
                        MIN(AF.Title) DisplayText ,
                        COUNT(*) ,
                        MIN(Sequence) FacetSequence ,
                        0 SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
						INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
						AND C.SiteId=@ApplicationId
                GROUP BY C.FacetID 
            END
        ELSE 
            IF ( @ExistingFacetValueIds IS NOT NULL ) 
                BEGIN
 

                    CREATE TABLE #FilterProductIds
                        (
                          Id UNIQUEIDENTIFIER
                            CONSTRAINT [PK_#cteValueToIdValue]
                            PRIMARY KEY CLUSTERED ( [Id] ASC )
                            WITH ( PAD_INDEX = OFF,
                                   STATISTICS_NORECOMPUTE = OFF,
                                   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                                   ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
                        )
                    ON  [PRIMARY]

                    INSERT  INTO #FilterProductIds
                            SELECT  C.ProductID
                            FROM    ATFacetRangeProduct_Cache C
							INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    INNER JOIN @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON tab.col.value('text()[1]',
                                                              'uniqueidentifier') = C.FacetValueID
                            Where C.SiteId=@ApplicationId
							GROUP BY C.ProductID
                            HAVING  COUNT(C.FacetValueID) >= ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab1 ( col1 )
                                                             )

				SELECT 
					C.FacetID,
					C.FacetValueID,
					MIN(C.DisplayText) DisplayText,
					Count(*) ProductCount,
					MIN(NF.Sequence) FacetSequence,
					Case FR.Sequence 
								WHEN NULL  THEN @totProd - COUNT(*)
								ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
							END AS SortOrder,
					MIN(AF.AllowMultiple) AllowMultiple
				FROM 
					ATFacetRangeProduct_Cache C 
					INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
					INNER JOIN ATFacet AF ON AF.Id = NF.FacetId	
					INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
					INNER JOIN #FilterProductIds P ON P.ID=C.ProductID
					LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
					LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
							AND C.SiteId=@ApplicationId
                    GROUP BY C.FacetValueID ,
                            C.FacetID
                    UNION ALL
                    SELECT  C.FacetID ,
                            NULL FacetValueID ,
                            MIN(AF.Title) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            0 SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProductTop_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
							AND C.SiteId=@ApplicationId
                    GROUP BY C.FacetID 


                    DROP TABLE #FilterProductIds

                END
            ELSE 
                BEGIN
                   ------------------NEW---------------------------------
                    DECLARE @FacetValues TABLE
                        (
                          FacetValueID UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER
                        )
                    INSERT  INTO @FacetValues
                            SELECT  tab.col.value('(value/guid/text())[1]',
                                                  'uniqueidentifier') ,
                                    tab.col.value('(key/guid/text())[1]',
                                                  'uniqueidentifier')
                            FROM    @ExistingFacets.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item') tab ( col ) ;
                 

                 
                 

                    DECLARE @FilteredProducts TABLE
                        (
                          ProductId UNIQUEIDENTIFIER
                        )           
                    
                    DECLARE @RequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
       
                    INSERT  INTO @RequiredProduct
                            SELECT DISTINCT
                                    C.ProductId ,
                                    Row_Number() OVER ( PARTITION BY ProductId ORDER BY ProductID ) AS RowNumber
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                            WHERE   F.AllowMultiple = 0 

                    DELETE  FROM @RequiredProduct
                    WHERE   RowNumber NOT IN ( SELECT   MAX(RowNumber)
                                               FROM     @RequiredProduct )


                    DECLARE @MultiRequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
                     INSERT  INTO @MultiRequiredProduct
                            SELECT  DISTINCT
                                    ProductID ,
                                    FacetID ,
                                    Row_Number() OVER ( PARTITION BY ProductID ORDER BY ProductID ) AS RowNumber
                                    
                                    FROM (
                                    SELECT DISTINCt C.ProductID,
                                    C.FacetID
                                    
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                            WHERE   F.AllowMultiple = 1)TV
                           



                    IF NOT EXISTS ( SELECT  *
                                    FROM    @RequiredProduct ) 
                        BEGIN
                            INSERT  INTO @FilteredProducts
                                    SELECT  ProductId
                                    FROM    @MultiRequiredProduct
                                    WHERE   RowNumber IN (
                                            SELECT  MAX(RowNumber)
                                            FROM    @MultiRequiredProduct )
                       
                        END
                    ELSE 
                        IF NOT EXISTS ( SELECT  *
                                        FROM    @MultiRequiredProduct ) 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  ProductId
                                        FROM    @RequiredProduct
                            END
                        ELSE 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  RP.ProductId
                                        FROM    @RequiredProduct RP
                                                INNER JOIN ( SELECT
                                                              ProductId
                                                             FROM
                                                              @MultiRequiredProduct
                                                             WHERE
                                                              RowNumber IN (
                                                              SELECT
                                                              MAX(RowNumber)
                                                              FROM
                                                              @MultiRequiredProduct )
                                                           ) TV ON TV.ProductId = RP.ProductId


                            END

                    DECLARE @FacetResults TABLE
                        (
                          FacetId UNIQUEIDENTIFIER ,
                          FacetValueId UNIQUEIDENTIFIER ,
                          DisplayText NVARCHAR(250) ,
                          FacetName NVARCHAR(250) ,
                          ProductCount INT ,
                          FacetSequence INT ,
                          SortOrder INT ,
                          AllowMultiple INT ,
                          ENABLED INT
                        )
                    INSERT  INTO @FacetResults
                            ( FacetId ,
                              FacetValueId ,
                              DisplayText ,
                              FacetName ,
                              ProductCount ,
                              FacetSequence ,
                              SortOrder ,
                              AllowMultiple ,
                              ENABLED
                            )
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @FilteredProducts RP ON RP.ProductId = C.ProductID
									LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 0
							        AND NFE.QueryId IS NULL
                                    AND ( RP.ProductId IS NOT NULL
                                          OR NOT EXISTS ( SELECT
                                                              ProductId
                                                          FROM
                                                              @FilteredProducts )
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
--Close now we need to get the other multi values that are not currently in the required products but would be filtered by any required products. 
--This query should be the multi facets that have no products currently showing in the grid. 
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( 
																		--MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
                                                              ( SELECT
                                                              COUNT(DISTINCT FacetId)
                                                              FROM
                                                              @MultiRequiredProduct
                                                              ) = 1 )
																	---End of expensive
                                                              )
							        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 1
							        AND NFE.QueryId IS NULL  
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
																		--( SELECT
																		--COUNT(DISTINCT FacetId)
																		--FROM
																		--@MultiRequiredProduct
																		--) = 1
                                                              )
																	---End of expensive
                                                              )
									LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 1
							        AND NFE.QueryId IS NULL
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                             
                                        )
                                        
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
        
                        
        
        
        
                    SELECT  FacetId ,
                            FacetValueId ,
                            DisplayText ,
                            ProductCount ,
                            FacetSequence ,
                            SortOrder ,
                            AllowMultiple ,
                            ENABLED
                    FROM    @FacetResults
                    UNION
                    SELECT  FacetId ,
                            NULL ,
                            FacetName AS DisplayText ,
                            SUM(ProductCount) ,
                            MAX(FacetSequence) ,
                            MAX(SortOrder) ,
                            MAX(AllowMultiple) ,
                            MAX(ENABLED)
                    FROM    @FacetResults
                    GROUP BY FacetId ,
                            FacetName
                    ORDER BY FacetValueId DESC
                   
                   
                   
                   
                   ----------------END OF NEW------------------------------
                        
                        
                        
                        
                        

                END
    END

GO

IF(OBJECT_ID('Facet_GetFacetsByProductGuids') IS NOT NULL)
	DROP PROCEDURE Facet_GetFacetsByProductGuids
GO

CREATE PROCEDURE [dbo].[Facet_GetFacetsByProductGuids]
(
	@Id Xml,
	@ApplicationId uniqueidentifier
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
BEGIN
--********************************************************************************
-- code
--********************************************************************************

Declare @inputProducts  table(Sno int Identity(1,1),ProductId uniqueidentifier)

Insert Into @inputProducts(ProductId)
	Select tab.col.value('text()[1]','uniqueidentifier')
	FROM @Id.nodes('/GenericCollectionOfGuid/guid')tab(col)
END

SELECT FRPC.DisplayText,FRPC.FacetID, FRPC.FacetValueID, TV.NumFacetRanges, TV.NumProductsInFacet, COUNT(FRPC.ProductID) AS NumProductsInRange
FROM 
	ATFacetRangeProduct_Cache FRPC 
	INNER JOIN @inputProducts PRS ON FRPC.ProductID = PRS.ProductId
	--INNER JOIN ATFacetRange FR ON FRPC.FacetValueID = FR.Id
	INNER JOIN ( -- precalc facet level totals to determine most useful facets
		SELECT FacetID, COUNT(DISTINCT FacetValueID) AS NumFacetRanges, COUNT(FRPC.ProductID) AS NumProductsInFacet
		FROM 
			ATFacetRangeProduct_Cache FRPC 
			INNER JOIN @inputProducts PRS ON FRPC.ProductID = PRS.ProductId
			Where FRPC.SiteId=@ApplicationId
		GROUP BY FacetID
		) TV ON FRPC.FacetID = TV.FacetID
GROUP BY TV.NumProductsInFacet, TV.NumFacetRanges, FRPC.FacetID, FRPC.FacetValueID, FRPC.DisplayText
ORDER BY TV.NumProductsInFacet DESC, TV.NumFacetRanges DESC, FRPC.FacetID

GO


IF(OBJECT_ID('Product_GetOnlineProductByFilter') IS NOT NULL)
	DROP PROCEDURE Product_GetOnlineProductByFilter
GO

CREATE PROCEDURE [dbo].[Product_GetOnlineProductByFilter]
(
	@FilterId uniqueidentifier,
    @SortCol VARCHAR(25)=null,
    @PageSize INT=null, 
	@PageNumber INT=1,
	@ApplicationId uniqueidentifier = null,
	@FacetValueIds Xml=null,
	@CustomerId uniqueidentifier= null,
	@IsActiveAndIsOnline TINYINT=1,
	@IsOnline TINYINT=0
)

AS
BEGIN
	Declare @Rowcount int
	Declare @FacetValues table(FacetValueID uniqueidentifier)

SET @SortCol = lower(@SortCol)



DECLARE @GroupIds TABLE (
GroupId uniqueidentifier
 )

IF @CustomerId IS NOT NULL
	INSERT INTO @GroupIds
	select GroupId from USMemberGroup WHERE MemberId = @CustomerId
ELSE
BEGIN
	
	DECLARE @GId uniqueidentifier
		SET @GId =(SELECT top 1 Value from STSiteSetting SS  
		INNER  JOIN STSettingType ST ON SS.SettingTypeId = ST.Id where ST.Name = 'CommerceEveryOneGroupId')
		
			INSERT INTO @GroupIds(GroupId) Values (@GId)
END

IF @PageSize IS NULL
	SET @PageSize = 2147483647

DECLARE @SQL nvarchar(max),@params nvarchar(100)
declare @isLatest bit

	Select @IsLatest =IsLatest from NVFilterQuery 
	Where Id=@FilterId And 
	(@ApplicationId IS NULL OR SiteId = @ApplicationId)
	--SiteId=ISNULL(@ApplicationId, SiteId)
	if (@IsLatest=0)
	Begin
		Exec NavFilter_ExecuteQuery @FilterId 
	End

	DECLARE @ProductIDs Table
	(
		Row_ID int IDENTITY(1,1) PRIMARY KEY,
		ProductID uniqueidentifier
	)

IF(@FacetValueIds is not null)
BEGIN
			Insert into @FacetValues
			Select tab.col.value('text()[1]','uniqueidentifier')	
			FROM @FacetValueIds.nodes('/GenericCollectionOfGuid/guid')tab(col) 
			Set @Rowcount =@@RowCount		 
END


IF @SortCol = 'price asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice ASC			
												) AS [Row_ID],
										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice 
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId  and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID

									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId 

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Asc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											 MinPrice ASC			
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,   
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId )
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId
									GROUP BY   cte.ProductId
						) iq	
						
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice asc
						

					END 
END
IF @SortCol = 'price desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID
								
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											MinPrice DESC
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId

									GROUP BY   cte.ProductId
						) iq	
					
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END 
END

IF @SortCol = 'soldcount desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount DESC		
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc


					END 
END


IF @SortCol = 'soldcount asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
							
							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate 
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
								--	INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount ASC				
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC


					END 
END

IF @SortCol = 'createddate asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate ASC			
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

					END 
END

IF @SortCol = 'createddate desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
						
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate DESC

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate desc	
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate Desc
						
					END 
END

IF @SortCol = 'title desc' 
    BEGIN
        IF ( @FacetValueIds IS NOT NULL ) 
            BEGIN							
                
					;WITH    PagingCTE (Row_ID,ProductId,CreatedDate, Title)
                              AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY Title DESC ) AS [Row_ID] ,
                                            PRS.Id ProductId ,
                                            PRS.CreatedDate,
											P.Title	
                                   FROM     ( SELECT    P.Id ,
                                                        SUM(PS.PreviousSoldCount) TotalSoldCount ,
                                                        MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
                                              FROM      ( SELECT
                                                              ProductID
                                                          FROM
                                                              ( SELECT DISTINCT
                                                              ProductID ,
                                                              FV.FacetValueID
                                                              FROM
                                                              @FacetValues FV
                                                              INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID
                                                              INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              WHERE
                                                              QueryId = @FilterId and C.SiteId=@ApplicationId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                              ) PF
                                                          GROUP BY ProductID
                                                          HAVING
                                                              COUNT(FacetValueID) = @RowCount
                                                        ) PID
                                                        INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
                                                        INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId
                                              WHERE     P.IsActive = 1
                                              GROUP BY  P.Id
                                            ) PRS
											INNER JOIN dbo.PRProduct P ON PRS.Id = P.Id
                                 )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title DESC

            END
        ELSE 
            BEGIN 

						;
                WITH    PagingCTE ( Row_ID, ProductId, CreatedDate, Title )
                          AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY p.Title DESC ) AS [Row_ID] ,
                                        iq.ProductId ,
                                        iq.CreatedDate,
										P.Title
                               FROM     ( SELECT    ProductId ,
                                                    MIN(ListPrice) AS MinPrice ,
                                                    SUM(PreviousSoldCount) AS TotalSoldCount ,
                                                    MIN(CreatedDate) CreatedDate
                                          FROM      ( SELECT  P.Id AS ProductId ,
                                                              PS.ListPrice ,
                                                              PS.PreviousSoldCount ,
                                                              P.CreatedDate
                                                      FROM    dbo.PRProduct P
                                                              INNER JOIN NVFilterOutput FnFilter ON P.Id = FnFilter.ObjectId
                                                              INNER JOIN dbo.PRProductSKU PS ON ( P.Id = PS.ProductId )
                                                      WHERE   P.IsActive = 1
                                                              AND FnFilter.QueryId = @FilterId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                    ) cte
                                          GROUP BY  ProductId
                                        ) iq	
										INNER JOIN Prproduct P ON iq.ProductId = p.ID
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
                                        
                             )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title DESC
						
            END 
    END
  
  IF @SortCol = 'title asc' 
    BEGIN
        IF ( @FacetValueIds IS NOT NULL ) 
            BEGIN							
                
					;WITH    PagingCTE (Row_ID,ProductId,CreatedDate, Title)
                              AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY Title asc ) AS [Row_ID] ,
                                            PRS.Id ProductId ,
                                            PRS.CreatedDate,
											P.Title	
                                   FROM     ( SELECT    P.Id ,
                                                        SUM(PS.PreviousSoldCount) TotalSoldCount ,
                                                        MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
                                              FROM      ( SELECT
                                                              ProductID
                                                          FROM
                                                              ( SELECT DISTINCT
                                                              ProductID ,
                                                              FV.FacetValueID
                                                              FROM
                                                              @FacetValues FV
                                                              INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID
                                                              INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              WHERE
                                                              QueryId = @FilterId and C.SiteId=@ApplicationId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                              ) PF
                                                          GROUP BY ProductID
                                                          HAVING
                                                              COUNT(FacetValueID) = @RowCount
                                                        ) PID
                                                        INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
                                                        INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId
                                              WHERE     P.IsActive = 1
                                              GROUP BY  P.Id
                                            ) PRS
											INNER JOIN dbo.PRProduct P ON PRS.Id = P.Id
                                 )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title asc

            END
        ELSE 
            BEGIN 

						;
                WITH    PagingCTE ( Row_ID, ProductId, CreatedDate, Title )
                          AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY p.Title asc ) AS [Row_ID] ,
                                        iq.ProductId ,
                                        iq.CreatedDate,
										P.Title
                               FROM     ( SELECT    ProductId ,
                                                    MIN(ListPrice) AS MinPrice ,
                                                    SUM(PreviousSoldCount) AS TotalSoldCount ,
                                                    MIN(CreatedDate) CreatedDate
                                          FROM      ( SELECT  P.Id AS ProductId ,
                                                              PS.ListPrice ,
                                                              PS.PreviousSoldCount ,
                                                              P.CreatedDate
                                                      FROM    dbo.PRProduct P
                                                              INNER JOIN NVFilterOutput FnFilter ON P.Id = FnFilter.ObjectId
                                                              INNER JOIN dbo.PRProductSKU PS ON ( P.Id = PS.ProductId )
                                                      WHERE   P.IsActive = 1
                                                              AND FnFilter.QueryId = @FilterId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                    ) cte
                                          GROUP BY  ProductId
                                        ) iq	
										INNER JOIN Prproduct P ON iq.ProductId = p.ID
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
                                        
                             )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title asc
						
            END 
    END  




--START
	IF (@IsActiveAndIsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1 AND PS.IsActive = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber
			
		END
	ELSE IF (@IsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber

		END
--END


END

GO

GO
IF(OBJECT_ID('Product_GetOnlineProductByFilterMulti') IS NOT NULL)
	DROP PROCEDURE Product_GetOnlineProductByFilterMulti
GO

CREATE PROCEDURE [dbo].[Product_GetOnlineProductByFilterMulti]
(
	@FilterId uniqueidentifier,
    @SortCol VARCHAR(25)=null,
    @PageSize INT=null, 
	@PageNumber INT=1,
	@ApplicationId uniqueidentifier = null,
	@FacetValueIds Xml=null,
	@CustomerId uniqueidentifier= null,
	@IsActiveAndIsOnline TINYINT=1,
	@IsOnline TINYINT=0	
	--,@VirtualCount int output
)

AS
BEGIN
	Declare @Rowcount int
	Declare @FacetValues table(FacetValueID uniqueidentifier,FacetId Uniqueidentifier)


SET @SortCol = lower(@SortCol)



DECLARE @GroupIds TABLE (
GroupId uniqueidentifier
 )

IF @CustomerId IS NOT NULL
	INSERT INTO @GroupIds
	select GroupId from USMemberGroup WHERE MemberId = @CustomerId
ELSE
BEGIN
	
	DECLARE @GId uniqueidentifier
		SET @GId =(SELECT top 1 Value from STSiteSetting SS  
		INNER  JOIN STSettingType ST ON SS.SettingTypeId = ST.Id where ST.Name = 'CommerceEveryOneGroupId')
		
			INSERT INTO @GroupIds(GroupId) Values (@GId)
END

IF @PageSize IS NULL
	SET @PageSize = 2147483647

DECLARE @SQL nvarchar(max),@params nvarchar(100)
declare @isLatest bit

	Select @IsLatest =IsLatest from NVFilterQuery 
	Where Id=@FilterId And 
	(@ApplicationId is null or SiteId = @ApplicationId)
	--SiteId=ISNULL(@ApplicationId, SiteId)
	if (@IsLatest=0)
	Begin
		Exec NavFilter_ExecuteQuery @FilterId 
	End

	DECLARE @ProductIDs Table
	(
		Row_ID int IDENTITY(1,1) PRIMARY KEY,
		ProductID uniqueidentifier
	)
	
IF(@FacetValueIds is not null)
BEGIN
		Insert into @FacetValues
		Select tab.col.value('(value/guid/text())[1]','uniqueidentifier') ,tab.col.value('(key/guid/text())[1]','uniqueidentifier')
		FROM @FacetValueIds.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item')tab(col) 
END

IF @SortCol = 'price asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	
							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice ASC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice 
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID

									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId 

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice ASC
							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											 MinPrice ASC			
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,   
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND (@IsActiveAndIsOnline = 0 OR (PS.IsActive = 1 AND PS.IsOnline = 1))
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId
									GROUP BY   cte.ProductId
						) iq	
						
					)

					
						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice ASC

					END 
END
IF @SortCol = 'price desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID
								
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											MinPrice DESC
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND (@IsActiveAndIsOnline = 0 OR (PS.IsActive = 1 AND PS.IsOnline = 1))
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId

									GROUP BY   cte.ProductId
						) iq	
					
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc


					END 
END

IF @SortCol = 'soldcount desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
									
				Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc
							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount DESC		
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc

					END 
END


IF @SortCol = 'soldcount asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
										
			Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate 
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
								--	INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount ASC				
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC


					END 
END

IF @SortCol = 'createddate asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate ASC			
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC


					END 
END

IF @SortCol = 'createddate desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate DESC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate desc	
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate Desc


					END 
END

IF @SortCol = 'title asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													Title ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 Title
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.Title) Title
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,Title)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											Title ASC			
											) AS [Row_ID],
									iq.ProductId,
									Title
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(Title) Title
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.Title
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title ASC


					END 
END

IF @SortCol = 'title desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													Title desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 Title
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.Title) Title
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title DESC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,Title)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											Title desc	
											) AS [Row_ID],
									iq.ProductId,
									Title
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(Title) Title
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.Title
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title Desc


					END 
END

--START
	IF (@IsActiveAndIsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1 AND PS.IsActive = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber
			
		END
	ELSE IF (@IsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber

		END
--END

END

GO

GO
IF(OBJECT_ID('Product_GetOnlineProductByFilterMultiVariant') IS NOT NULL)
	DROP PROCEDURE Product_GetOnlineProductByFilterMultiVariant
GO

CREATE PROCEDURE [dbo].[Product_GetOnlineProductByFilterMultiVariant]
(
	@FilterId uniqueidentifier,
    @SortCol VARCHAR(25)=null,
    @PageSize INT=null, 
	@PageNumber INT=1,
	@ApplicationId uniqueidentifier = null,
	@FacetValueIds Xml=null,
	@CustomerId uniqueidentifier= null,
	@IsActiveAndIsOnline TINYINT=1,
	@IsOnline TINYINT=0	
	--,@VirtualCount int output
)

AS
BEGIN
	Declare @Rowcount int
	Declare @FacetValues table(FacetValueID uniqueidentifier,FacetId Uniqueidentifier)


SET @SortCol = lower(@SortCol)



DECLARE @GroupIds TABLE (
GroupId uniqueidentifier
 )

IF @CustomerId IS NOT NULL
	INSERT INTO @GroupIds
	select GroupId from USMemberGroup WHERE MemberId = @CustomerId
ELSE
BEGIN
	
	DECLARE @GId uniqueidentifier
		SET @GId =(SELECT top 1 Value from STSiteSetting SS  
		INNER  JOIN STSettingType ST ON SS.SettingTypeId = ST.Id where ST.Name = 'CommerceEveryOneGroupId')
		
			INSERT INTO @GroupIds(GroupId) Values (@GId)
END

IF @PageSize IS NULL
	SET @PageSize = 2147483647

DECLARE @SQL nvarchar(max),@params nvarchar(100)
declare @isLatest bit

	Select @IsLatest =IsLatest from NVFilterQuery 
	Where Id=@FilterId And 
	(@ApplicationId is null or SiteId = @ApplicationId)
	--SiteId=ISNULL(@ApplicationId, SiteId)
	if (@IsLatest=0)
	Begin
		Exec NavFilter_ExecuteQuery @FilterId 
	End

	DECLARE @ProductIDs Table
	(
		Row_ID int IDENTITY(1,1) PRIMARY KEY,
		ProductID uniqueidentifier
	)
	
IF(@FacetValueIds is not null)
BEGIN
		Insert into @FacetValues
		Select tab.col.value('(value/guid/text())[1]','uniqueidentifier') ,tab.col.value('(key/guid/text())[1]','uniqueidentifier')
		FROM @FacetValueIds.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item')tab(col) 
END

IF @SortCol = 'price asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	
							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice ASC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice 
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND C.SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID

									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId 

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId and PV.SiteId=@ApplicationId
									WHERE isnull(PV.IsActive,P.IsActive) = 1    
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice ASC
							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											 MinPrice ASC			
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,   
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
														WHERE isnull(PV.IsActive,P.IsActive) = 1    
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId
									GROUP BY   cte.ProductId
						) iq	
						
					)

					
						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice ASC

					END 
END
IF @SortCol = 'price desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND C.SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID
								
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
									WHERE isnull(PV.IsActive,P.IsActive) = 1        
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											MinPrice DESC
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
													WHERE isnull(PV.IsActive,P.IsActive) = 1    
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId

									GROUP BY   cte.ProductId
						) iq	
					
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc


					END 
END

IF @SortCol = 'soldcount desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
									
				Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND C.SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
									WHERE isnull(PV.IsActive,P.IsActive) = 1        
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc
							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount DESC		
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
													WHERE isnull(PV.IsActive,P.IsActive) = 1    
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc

					END 
END


IF @SortCol = 'soldcount asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
										
			Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate 
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND C.SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
								--	INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
									WHERE isnull(PV.IsActive,P.IsActive) = 1       
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount ASC				
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
													WHERE isnull(PV.IsActive,P.IsActive) = 1    
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC


					END 
END

IF @SortCol = 'createddate asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND C.SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
									WHERE isnull(PV.IsActive,P.IsActive) = 1       
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate ASC			
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
													WHERE isnull(PV.IsActive,P.IsActive) = 1    
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC


					END 
END

IF @SortCol = 'createddate desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND C.SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
									WHERE isnull(PV.IsActive,P.IsActive) = 1       
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate DESC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate desc	
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													LEFT JOIN PRProductVariant PV on P.Id =PV.Id and PV.SiteId=@ApplicationId
													WHERE isnull(PV.IsActive,P.IsActive) = 1				
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate Desc


					END 
END
--START
	IF (@IsActiveAndIsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			LEFT JOIN PRProductSKUVariant PSV ON PSV.Id=PS.Id and PSV.SiteId=@ApplicationId
			WHERE PS.IsOnline = 1 AND isnull(PSV.IsActive,PS.IsActive) = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber
			
		END
	ELSE IF (@IsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber

		END
--END

END

GO

GO
IF(OBJECT_ID('Product_GetProductsByFacetValueGuids') IS NOT NULL)
	DROP PROCEDURE Product_GetProductsByFacetValueGuids
GO

CREATE PROCEDURE [dbo].[Product_GetProductsByFacetValueGuids]
(
	@Id Xml,
	@ApplicationId uniqueidentifier
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
BEGIN
--********************************************************************************
-- code
--********************************************************************************


Declare @inputFacets  table(Sno int Identity(1,1),FacetValueId uniqueidentifier)

Insert Into @inputFacets(FacetValueId)
	Select tab.col.value('text()[1]','uniqueidentifier')
	FROM @Id.nodes('/GenericCollectionOfGuid/guid')tab(col);

WITH CTEProducts AS (
SELECT FRPC.ProductID
FROM ATFacetRangeProduct_Cache FRPC
WHERE FRPC.FacetValueID IN (SELECT FacetValueId from @inputFacets) AND FRPC.SiteId=@ApplicationId
GROUP BY FRPC.ProductID HAVING COUNT(FRPC.FacetValueID)>=(SELECT COUNT(1) FROM @inputFacets) )

SELECT C.ProductID,P.Id FROM CTEProducts C left outer join PRProduct P on P.Id=C.ProductID

END


IF (OBJECT_ID('vwProductSKUFacetAttributeValue_NonEnumerated') IS NOT NULL)
DROP VIEW [dbo].[vwProductSKUFacetAttributeValue_NonEnumerated]
GO
IF (OBJECT_ID('vwProductFacetAttributeValue_Enumerated') IS NOT NULL)
DROP VIEW [dbo].[vwProductFacetAttributeValue_Enumerated] 
GO
IF (OBJECT_ID('vwSKUFacetAttributeValue_Enumerated') IS NOT NULL)
DROP VIEW [dbo].[vwSKUFacetAttributeValue_Enumerated]
GO
GO

IF (OBJECT_ID('vwProductSKUFacetAttributeValue_Enumerated') IS NOT NULL)
	DROP View vwProductSKUFacetAttributeValue_Enumerated
GO
CREATE VIEW [dbo].[vwProductSKUFacetAttributeValue_Enumerated]     
AS  
  
SELECT  F.id AS FacetId, F.IsRange, PAV.ProductId AS ProductId, AE.AttributeId AttributeId, AE.Value Value, PAV.AttributeEnumId AttributeEnumId, F.SiteId--, AE.NumericValue  
FROM  
 [dbo].ATFacet F   
 INNER JOIN [dbo].ATAttribute A ON A.Id = F.AttributeID --AND A.IsFaceted = 1 AND A.IsEnum = 1  
 INNER JOIN [dbo].ATAttributeEnum AE ON A.Id = AE.AttributeId  
 INNER JOIN [dbo].PRProductAttributeValue PAV ON PAV.AttributeEnumId = AE.Id  
 INNER JOIN [dbo].PRProduct P on P.Id = PAV.ProductId and P.ISActive=1  
 INNER JOIN [dbo].PRProductSKU PS on PS.Productid = P.Id and PS.IsActive=1 and PS.ISOnline=1  
WHERE   
 AE.Status =1 AND A.Status=1    
UNION  
SELECT F.id AS FacetId, F.IsRange IsRange, PS.ProductId ProductId, AE.AttributeId AttributeId, AE.Value Value, PSAV.AttributeEnumId AttributeEnumId, F.SiteId--, AE.NumericValue  
FROM  
 [dbo].[ATFacet] F   
 INNER JOIN [dbo].ATAttribute A ON A.Id = F.AttributeID --AND A.IsFaceted = 1 AND A.IsEnum = 1  
 INNER JOIN [dbo].ATAttributeEnum AE ON A.Id = AE.AttributeId  
 INNER JOIN [dbo].PRProductSKUAttributeValue PSAV ON PSAV.AttributeEnumId = AE.Id --PSAV.AttributeId = A.Id AND   
 INNER JOIN [dbo].PRProductSKU PS ON PS.Id = PSAV.SKUId and PS.IsActive = 1 and PS.IsOnline=1  
 INNER JOIN [dbo].PRPRoduct P on P.Id = PS.ProductId and P.IsActive =1  
WHERE   
 AE.Status = 1 AND A.Status = 1  


 GO
 IF (OBJECT_ID('vwProductSKUFacetAttributeValue_Range') IS NOT NULL)
	DROP View vwProductSKUFacetAttributeValue_Range
GO
CREATE VIEW [dbo].[vwProductSKUFacetAttributeValue_Range]  
AS  
  
--Non Enumerated Products   
SELECT distinct F.id AS FacetId, PAV.ProductId, PAV.AttributeId, PAV.Value, NULL AS AttributeEnumId, F.SiteId  
FROM  
 PRProductAttributeValue PAV   
 INNER JOIN ATAttribute A ON PAV.AttributeId = A.Id AND A.IsEnum = 0 -- AND A.IsFaceted = 1   
 INNER JOIN ATFacet F ON A.Id = F.AttributeID  
 INNER JOIN PRProduct P on P.Id = PAV.ProductId and P.ISActive=1  
 INNER JOIN PRProductSKU PS on PS.Productid = P.Id and PS.IsActive=1 and PS.ISOnline=1  
WHERE   
 A.Status = dbo.GetActiveStatus()   AND IsRange=1  
   
--29 seconds --> 0 seconds  
  
UNION ALL  
  
--Non Enumerated SKUs  
SELECT F.id AS FacetId,  PS.ProductId AS ProductId, PSAV.AttributeId, PSAV.Value, NULL AS AttributeEnumId, F.SiteId  
FROM  
 PRProductSKU PS   
 INNER JOIN PRProductSKUAttributeValue PSAV ON PS.Id = PSAV.SKUId   
 INNER JOIN ATAttribute A ON PSAV.AttributeId = A.Id AND A.IsEnum = 0 --AND A.IsFaceted = 1   
 INNER JOIN ATFacet F ON A.Id = F.AttributeID  
 INNER JOIN PRProduct P on P.Id = PS.ProductID and PS.IsActive=1 and PS.IsOnline=1 and P.IsActive=1  
WHERE   
 A.Status=dbo.GetActiveStatus()  AND IsRange=1  
  
UNION ALL   
  
SELECT FacetId, ProductId, AttributeId, Value, NULL AS AttributeEnumId, SiteId  
FROM [vwProductSKUFacetAttributeValue_Enumerated] WHERE IsRange=1  

GO

IF(OBJECT_ID('Facet_ReloadProductFacetCache') IS NOT NULL)
	DROP PROCEDURE Facet_ReloadProductFacetCache
GO

CREATE PROCEDURE [dbo].[Facet_ReloadProductFacetCache]  
(  
 @ApplicationId uniqueidentifier=null  
)  
--now only 39 seconds, now 50 seconds with all Enumed Faceted in Shaw  
AS  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
  
--********************************************************************************  
-- Procedure Body  
--********************************************************************************  
BEGIN  
  
--create temp table to load new cache information into   
--(not sure if this is the best way to declare a large temp table - DM)  
   
Print 'Starting'
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))
Create Table #tempCache
(
   Id INT IDENTITY(1,1) Primary Key,   
    FacetId uniqueidentifier,  
 AttributeId uniqueidentifier,  
 FacetValueID uniqueidentifier,  
 DisplayText nvarchar(50),  
 ProductId uniqueidentifier,  
 SortOrder int Default(0),
 SiteId uniqueidentifier
)  
  
  
--Enumerated Attributes Defined Ranges  
Print 'Insert INTO STARt #tempCache '  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))

INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder,SiteId)  
SELECT DISTINCT FacetID, PSFAV.AttributeID, AttributeEnumId AS FacetValueID, PSFAV.Value AS DisplayText, ProductId,AE.Sequence,PSFAV.SiteId
--, AE.Value AS [From], AE.Value AS [To]  
FROM   
 vwProductSKUFacetAttributeValue_Enumerated PSFAV   
 LEFT JOIN ATAttributeEnum AE ON AE.Id=PSFAV.AttributeEnumId  
--WHERE IsRange = 0 AND (@ApplicationId IS NULL OR PSFAV.SiteId = @ApplicationId)   

Print 'Insert INTO AFTER #tempCache Enumerated'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121)) 

SELECT AttributeId, ProductId, convert(Decimal(14, 4), value) as Value,SiteId
INTO #PSFAV
FROM vwProductSKUFacetAttributeValue_RANGE where ISNUMERIC(Value)=1
--Ranges Per Type:  
INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder,SiteId)  
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId,FR.Sequence,PSFAV.SiteId  
--, FRX.[From], FRX.[To]  
-- select *  
FROM   
 ATFacet F   
 INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID -- F.IsRange = 1  
 INNER JOIN ATFacetRangeInt FRX ON FR.Id = FRX.FacetRangeID   
 INNER JOIN #PSFAV PSFAV ON F.AttributeID = PSFAV.AttributeId AND  
   PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value)    
   AND ISNUMERIC(PSFAV.Value)=1  
   --Order by DisplayText
--WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)  

 
PRINT  'Insert AFTER INTO #tempCache Int'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))   

Print 'Start Insert Decimal'
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))

INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder,SiteId)  
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId,FR.Sequence,PSFAV.SiteId
--, FRX.[From], FRX.[To]  
FROM   
 ATFacet F   
 INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --and  F.IsRange = 1   
 INNER JOIN ATFacetRangeDecimal FRX ON FR.Id = FRX.FacetRangeID   
 INNER JOIN #PSFAV PSFAV ON F.AttributeID = PSFAV.AttributeId AND PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) 
 AND IsNull(FRX.[To],PSFAV.Value) AND ISNUMERIC(PSFAV.Value)=1  
  --  WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)  


Print 'Insert AFTER INTO #tempCache Decimal'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))  

Drop table #PSFAV

SELECT F.AttributeId, ProductId, Value,V1.SiteId
INTO #PSFAVString
FROM vwProductSKUFacetAttributeValue_RANGE V1 
INNER JOIN ATFacet F ON V1.AttributeId = F.AttributeID
INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --AND F.IsRange = 1   
INNER JOIN ATFacetRangeString FRX ON FR.Id = FRX.FacetRangeID   

INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder,SiteId)  
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId,FR.Sequence,PSFAV.SiteId
--, FRX.[From], FRX.[To]  
FROM   
 ATFacet F   
 INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --AND F.IsRange = 1   
 INNER JOIN ATFacetRangeString FRX ON FR.Id = FRX.FacetRangeID   
 INNER JOIN #PSFAVString PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value)  
    AND F.AttributeID = PSFAV.AttributeId  
  --  WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)  
--delete data from existing cache  

PRINT 'Insert AFTER INTO #tempCache String'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))  

Drop table  #PSFAVString


SELECT AttributeId, ProductId, convert(DateTime, value) as Value,SiteId
INTO #PSFAVDate
FROM vwProductSKUFacetAttributeValue_RANGE where ISDATE(Value)=1

INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder,SiteId)  
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId,FR.Sequence,PSFAV.SiteId
--, FRX.[From], FRX.[To]  
FROM   
 ATFacet F   
 INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --and  F.IsRange = 1   
 INNER JOIN ATFacetRangeDate FRX ON FR.Id = FRX.FacetRangeID  
 INNER JOIN #PSFAVDate PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value) AND ISDATE(PSFAV.Value)=1  
    AND F.AttributeID = PSFAV.AttributeId  
   -- WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)  
      
PRINT 'Insert AFTER INTO #tempCache Date'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))  

Drop table  #PSFAVDate

truncate table dbo.ATFacetRangeProduct_Cache  
  
 --repopulate the real cache  
 --(again, best method for this? this will be a LOT of data - DM)  
 INSERT INTO dbo.ATFacetRangeProduct_Cache   
 (Id,FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder,SiteId)  
 SELECT newid() Id,T.FacetId,T.AttributeId,T.FacetValueID,T.DisplayText,T.ProductId,T.SortOrder,T.SiteId   
 FROM #tempCache T  
  
TRUNCATE TABLE ATFacetRangeProductTop_Cache  
  
-- Create Distinct Product Count by Facet.  
INSERT INTO ATFacetRangeProductTop_Cache(FacetID,ProductID,SiteId) --112,008  
SELECT   
 [FacetID],      
    [ProductID],
	SiteId
FROM   
 ATFacetRangeProduct_Cache --242,572  
GROUP BY  
 [FacetID]    
  ,[ProductID]  
  ,[SiteId]
  
END
GO

PRINT 'Updating end date in the iappsproductsuite table'
GO
UPDATE iAppsProductSuite SET UpdateEndDate = GETUTCDATE()
GO

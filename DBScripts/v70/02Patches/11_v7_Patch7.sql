IF NOT EXISTS (SELECT * FROM STSettingType WHERE [Name] = 'Container.EnableLocking')
BEGIN
	DECLARE @SettingGroupId INT = (SELECT TOP 1 Id FROM STSettingGroup Where Name ='General')
	DECLARE @Sequence INT = (SELECT ISNULL(MAX(Sequence), 0) + 1 FROM STSettingType WHERE SettingGroupId = @SettingGroupId)

	INSERT INTO STSettingType ([Name], FriendlyName, SettingGroupId, [Sequence], ControlType, IsEnabled, IsVisible)
		VALUES ('Container.EnableLocking', 'Allow the option to lock containers for variant site.', @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
END
GO
IF (OBJECT_ID('Site_CreateMicroSiteData') IS NOT NULL)
	DROP PROCEDURE Site_CreateMicroSiteData
GO
CREATE PROCEDURE [dbo].[Site_CreateMicroSiteData]
(
	@ProductId						uniqueidentifier,
	@MasterSiteId					uniqueidentifier,
	@MicroSiteId					uniqueidentifier,
	@ModifiedBy						uniqueidentifier = null,
	@ImportAllAdminUserAndPermission bit ,
	@ImportAllWebSiteUser			bit,
	@PageImportOptions				int = 2,
	@ImportContentStructure			bit = 0,
	@ImportFileStructure			bit = 0,
	@ImportImageStructure			bit = 0,
	@ImportFormsStructure			bit = 0
)
AS
BEGIN
	-- Import Front end and admin users
	EXEC [dbo].[VariantSite_ImportUser] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllWebSiteUser, @ImportAllAdminUserAndPermission, @ProductId
	-- Import Menu and Pages
	EXEC [dbo].[Site_CopyMenusAndPages] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, @PageImportOptions
	--content structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 7, @ImportContentStructure
	--File structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 9, @ImportFileStructure
	--Image structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 33, @ImportImageStructure
	--Forms structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 38, @ImportFormsStructure
	
	-- Import Tag, Form, etc. 
	EXEC [VariantSite_CopyMisc] @MicroSiteId, @ModifiedBy
	
	-- Changing the status of the site		
	UPDATE SISite SET Status = 1 WHERE Id = @MicroSiteId		

	DECLARE @ContainerLocking int 
	SELECT @ContainerLocking = Id FROM STSettingType WHERE Name = 'Container.EnableLocking'

	IF (@ContainerLocking IS NOT NULL)
	BEGIN
		INSERT INTO STSiteSetting (SiteId, SettingTypeId, Value)
		VALUES (@MicroSiteId, @ContainerLocking, 'false')
	END

	--CREATE MARKETIER RELATED DEFAULT DATA FOR VARIANT SITES
	EXEC Site_CreateMicrositeDataForMarketier @MicroSiteId = @MicroSiteId, @ModifiedBy = @ModifiedBy

	--CREATE COMMERCE RELATED DEFAULT DATA FOR VARIANT SITES
	EXEC Site_CreateMicrositeDataForCommerce @MasterSiteId, @MicroSiteId

	;WITH CTE AS
	(
		SELECT PageMapNodeId, ROW_NUMBER() OVER(PARTITION BY ParentId ORDER BY LftValue) AS DisplayOrder
		FROM PageMapNode WHERE SiteId = @MicroSiteId
	)

	UPDATE P
	SET P.DisplayOrder = C.DisplayOrder
	FROM PageMapNode P JOIN CTE C ON P.PageMapNodeId = C.PageMapNodeId	

	--Inserting in Country Cache
	INSERT INTO [dbo].[GLCountryVariantCache]
    (
		[Id],
        [SiteId],
        [CountryName],
        [Status],
        [ModifiedBy],
        [ModifiedDate]
	)
    SELECT
		Id,
		@MicroSiteId,
		CountryName,
		Status,
		ModifiedBy,
		ModifiedDate
	FROM vwCountry where SiteId = @MasterSiteId

	--Inserting in State Cache
	INSERT INTO [dbo].[GLStateVariantCache]
    (
		[Id],
        [SiteId],
        [State],
        [Status],
        [ModifiedBy],
        [ModifiedDate]
	)
    SELECT
		Id,
		@MicroSiteId,
		State,
		Status,
		ModifiedBy,
		ModifiedDate
	FROM vwState where SiteId = @MasterSiteId
END
GO
IF NOT EXISTS (SELECT 1 FROM MAFlowControl WHERE Id = 201)
BEGIN

INSERT [dbo].[MAFlowControl] ([Id], [Title], [ObjectTypeId], [ActionType], [Properties], [SiteId], [Sequence], [status]) VALUES (
201
, N'Abandoned Cart'
, 212
,1
, N'<Attribute xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Item>
    <Key>controlType</Key>
    <Value>abandoned-cart</Value>
  </Item>
   <Item>
    <Key>icon</Key>
    <Value>circle</Value>
  </Item>
  <Item>
    <Key>requiredMessage</Key>
    <Value>Send After is required</Value>
  </Item>
</Attribute>'
, N'8039ce09-e7da-47e1-bcec-df96b5e411f4'
, 1
, 1)
END

GO
IF (OBJECT_ID('Cart_GetAbandonedCart') IS NOT NULL)
	DROP PROCEDURE Cart_GetAbandonedCart
GO
GO
/****** Object:  StoredProcedure [dbo].[Cart_GetAbandonedCart]    Script Date: 12/18/2020 9:26:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Cart_GetAbandonedCart]
    (
      @MinDate DateTime,
      @MaxDate DateTime,
	  @PageNumber int,
	  @PageSize int,
      @SiteId UNIQUEIDENTIFIER = NULL,
	  @IgnoreProcessed bit
								
    )
AS 
    BEGIN
      
	  Declare @cart table(
						[RowNumber] int NOT NULL,
						[Id] [uniqueidentifier] NOT NULL,
						[CustomerId] [uniqueidentifier] NULL,
						[Title] [nvarchar](50) NULL,
						[Description] [nvarchar](256) NULL,
						[ExpirationDate] [datetime] NOT NULL,
						[Status] [int] NOT NULL,
						[SiteId] [uniqueidentifier] NULL,
						[CreatedBy] [uniqueidentifier] NOT NULL,
						[CreatedDate] [datetime] NOT NULL,
						[ModifiedDate] [datetime] NULL,
						[ModifiedBy] [uniqueidentifier] NULL,
						[Email] [nvarchar](256) NULL,
						[FirstName] [nvarchar](256) NULL,
						[LastName] [nvarchar](256) NULL
						)

		Declare @ActionCount int

		select @ActionCount =count(1) from MAFlowAction
		Where ControlId=201

		INSERT INTO @cart
		select 
		ROW_NUMBER() over (order by C.ModifiedDate) RowNumber
		,C.Id
		,U.Id CustomerId
		,C.Title
		,C.Description
		,C.ExpirationDate
		,C.Status
		,C.SiteId
		,C.CreatedBy
		,C.CreatedDate
		,CASE WHEN O.ModifiedDate > C.ModifiedDate THEN O.ModifiedDate ELSE C.ModifiedDate END ModifiedDate
		,C.ModifiedBy
		,M.Email
		,U.FirstName
		,U.LastName
		from CSCart C
		LEFT JOIN OROrder O ON C.Id = O.CartId
		INNER JOIN USUser U ON U.Id =C.CustomerId OR U.Id = O.CustomerId
		INNER JOIN USMembership M ON U.Id = M.UserId
		Where (@IgnoreProcessed =1 OR C.Id NOT IN
		(select ObjectId from MAFlowUserAction UA
		INNER JOIN MAFlowAction FA ON UA.ActionId =FA.Id
		Where FA.ControlId=201
		Group by UA.ObjectId
		having count(UA.ObjectId) >= @ActionCount))
		AND ( EXISTS (SELECT Id FROM CSCartItem CI Where CI.CartId =C.Id ))
		AND
		((O.Id IS NOT NULL AND O.OrderStatusId =12 AND O.ModifiedDate between @minDate and @maxDate )
		OR (O.Id IS NULL AND C.CustomerId IS NOT NULL AND C.ModifiedDate  between @minDate and @maxDate) )
		Order By C.ModifiedDate 
		Offset (@PageNumber -1) * @PageSize rows
		Fetch next @PageSize row only;
		
		select * from @cart

		select CI.Id
			,CI.ParentCartItemId
			,CI.CartId
			,CI.SKUId
			,CI.Quantity
			,CI.Price
			,CI.CreatedDate
			,CI.CreatedBy
			,CI.ModifiedDate
			,CI.ModifiedBy
			,CI.Sequence
			,CI.Comment
			,CI.BundleItemId
		from CSCartItem CI
		INNER JOIN PRProductSKU S  ON S.Id = CI.SKUId
		INNER JOIN vwProduct P ON P.Id =S.ProductId
		INNER JOIN @cart C on CI.CartId = C.Id AND P.SiteId = C.SiteId
    
	END


GO

GO
IF (OBJECT_ID('CommerceReport_AbandonedCheckout') IS NOT NULL)
	DROP PROCEDURE CommerceReport_AbandonedCheckout
GO


CREATE PROCEDURE [dbo].[CommerceReport_AbandonedCheckout]  
(   
 @startDate datetime,@endDate datetime, @maxReturnCount int,  
 @pageSize int = 10,@pageNumber int,   
 @sortBy nvarchar(50) = null,  
 @virtualCount int output,  
 @ApplicationId uniqueidentifier=null ,
 @IncludeChildrenSites Bit = 0   
)   
AS  
  
BEGIN  
 declare @abandonedTime decimal(18,2)  
--  
--   declare @startDate datetime  
--   declare @endDate datetime  
--   declare @maxReturnCount int  
--   declare @virtualCount int  
--   declare @pageSize int  
--   declare @pageNumber int  
  
   if @maxReturnCount = 0  
	set @maxReturnCount = null  
--  
--   set @pageSize = 50  
--   set @pageNumber =1  
--  
--  set @startDate='07-25-2009'  
--  set @endDate='08-25-2009'  
  
  IF @pageSize IS NULL  
	SET @PageSize = 2147483647  
  
SET @startDate =dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
IF @endDate is not null
SET @endDate = dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
ELSE
	SET @endDate = GetUTCDate()
 
   
 SET @abandonedTime =(SELECT top 1 case when St.Value is null then '0.000000' else St.Value end 
	--isnull(St.Value,0)  
  FROM STSiteSetting St  
  INNER JOIN STSettingType Ty ON SettingTypeId = Ty.Id  
  WHERE Ty.Name ='AbandonedCartTime')
  
  SET @abandonedTime = @abandonedTime  *60

  declare @tmpAbandonedCheckout table(  
	   CustomerId uniqueidentifier, CartId uniqueidentifier,  
	   CartTotal money,  
	   FullName nvarchar(1000), Email nvarchar(200),  
	   AbandonedDate DateTime,    
	   SiteId uniqueidentifier)  
  
  INSERT INTO @tmpAbandonedCheckout  
  SELECT CustomerId, CartId, CartTotal, FullName, Email, 
				dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId) As AbandonedDate, SiteId  
  FROM vwAbandonedCheckout   
  WHERE  
	ModifiedDate >= @startDate
	AND DateAdd(MINUTE,@abandonedTime,ModifiedDate) < @endDate
	and (
			(@IncludeChildrenSites = 0 AND SiteId = @ApplicationId )
			OR
			(@IncludeChildrenSites = 1 AND SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites( @ApplicationId,1)))
		) 
  
 Set @virtualCount = @@ROWCOUNT  
  
if @sortBy is null or (@sortBy) = 'carttotal desc'  
begin  
  Select A.*   
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by CartTotal desc) as RowNumber  
	from @tmpAbandonedCheckout  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.CartTotal desc  
end  
else if (@sortBy) = 'carttotal asc'  
begin  
  Select A.*   
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by CartTotal asc) as RowNumber  
	from @tmpAbandonedCheckout  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.CartTotal asc  
end  
else if (@sortBy) = 'email asc'  
 Begin  
  Select A.*   
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by Email asc) as RowNumber  
	from @tmpAbandonedCheckout  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.Email asc  
 end  
else if (@sortBy) = 'email desc'  
 Begin  
  Select A.*   
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by Email desc) as RowNumber  
	from @tmpAbandonedCheckout  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.Email desc  
 end  
else if (@sortBy) = 'fullname asc'  
 Begin  
  Select A.*   
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by FullName asc) as RowNumber  
	from @tmpAbandonedCheckout  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.FullName asc  
 end  
else if (@sortBy) = 'fullname desc'  
 Begin  
  Select A.*   
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by FullName desc) as RowNumber  
	from @tmpAbandonedCheckout  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.FullName desc  
 end  
else if (@sortBy) = 'abandoneddate asc'  
 Begin  
  Select A.*   
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by AbandonedDate asc) as RowNumber  
	from @tmpAbandonedCheckout  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.AbandonedDate asc  
 end  
else if (@sortBy) = 'abandoneddate desc'  
 Begin  
  Select A.*   
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by AbandonedDate desc) as RowNumber  
	from @tmpAbandonedCheckout  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.AbandonedDate desc  
 end  
END
GO
PRINT 'Add Site Settings'
DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int

SET @SettingGroupId = 1
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'SiteAttribute.DisablePropagate')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('SiteAttribute.DisablePropagate', 'Disable Proagate option for site attributes',  @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Images.EnableCompression')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Images.EnableCompression', 'Enable compression on file upload',  @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, 'true' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
GO
PRINT 'Creating view vwSiteAttributeSimple'
GO
IF(OBJECT_ID('vwSiteAttributeSimple') IS NOT NULL)
	DROP VIEW vwSiteAttributeSimple
GO	
CREATE VIEW [dbo].[vwSiteAttributeSimple]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle,
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.SiteId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 1
	LEFT JOIN ATSiteAttributeValue V ON A.Id = V.AttributeId
GO
PRINT 'Creating view vwSiteAttributeValueSimple'
GO
IF(OBJECT_ID('vwSiteAttributeValueSimple') IS NOT NULL)
	DROP VIEW vwSiteAttributeValueSimple
GO	
CREATE VIEW [dbo].[vwSiteAttributeValueSimple]
AS
SELECT V.Id,
	V.SiteId AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	V.Value, 
	V.IsShared,
	V.IsEditable,
	0 AS IsReadOnly,
	V.SiteId,
	V.CreatedDate, 
	V.CreatedBy,
	A.Title as AttributeTitle
FROM ATSiteAttributeValue V 
	LEFT JOIN ATAttribute A ON V.AttributeId = A.Id
GO
IF NOT EXISTS (SELECT * FROM USRoles WHERE Name ='OutputCacheAdministrator')
BEGIN
	DECLARE @CreatedById	uniqueidentifier
	SET @CreatedById = (SELECT TOP 1 Id FROM USUser WHERE LoweredUserName = 'insadmin')

	DECLARE @CMSProductId uniqueidentifier
	SET @CMSProductId ='CBB702AC-C8F1-4C35-B2DC-839C26E39848'

	DECLARE @SiteId uniqueidentifier
	SET @SiteId = '8039ce09-e7da-47e1-bcec-df96b5e411f4'

	DECLARE @GroupObjectTypeId int
	SET @GroupObjectTypeId = 15 

	DECLARE @OutputCacheAdminRoleId int
	SELECT @OutputCacheAdminRoleId = MAX(Id) + 1 FROM USRoles
	
	DECLARE @OutputCacheAdminGroupId uniqueidentifier
	DECLARE @WebApiAdminId uniqueidentifier

	INSERT INTO USRoles (ApplicationId,Id,[Name],Description,IsGlobal) 
	VALUES (@CMSProductId, @OutputCacheAdminRoleId, 'OutputCacheAdministrator','Output cache Admin for the system', 1)
	
	EXEC Group_Save  
		@CMSProductId,
		@ApplicationId=@CMSProductId, 
		@Id = @OutputCacheAdminGroupId output,
		@Title = 'OutputCache Administrator',
		@Description = 'Outputcache Admin Group',
		@CreatedBy=@CreatedById,
		@ModifiedBy=NULL,
		@ExpiryDate=NULL,
		@Status=NULL,
		@GroupType=1
	
	EXEC HierarchyNode_Save 
		@Id=@OutputCacheAdminGroupId output, 
		@Title=N'OutputCache Administrator',
		@ParentId=@SiteId,
		@Keywords=NULL,
		@ObjectTypeId=@GroupObjectTypeId, 
		@ApplicationId=@SiteId
	
	INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
	VALUES(@CMSProductId, @SiteId,@OutputCacheAdminGroupId,2,@OutputCacheAdminRoleId, @CMSProductId,1,1 )

END
GO
IF (OBJECT_ID('PageDto_GetPagesForSharedContent') IS NOT NULL)
	DROP PROCEDURE PageDto_GetPagesForSharedContent
GO
CREATE PROCEDURE [dbo].[PageDto_GetPagesForSharedContent]
(
	@ContentIds		nvarchar(max),
	@SiteId			uniqueidentifier = NULL
)
AS
BEGIN
	SELECT * FROM [dbo].[Page_GetPagesForSharedContent](@ContentIds, @SiteId)
END
GO
IF (OBJECT_ID('Page_GetPagesForSharedContent') IS NOT NULL)
	DROP FUNCTION Page_GetPagesForSharedContent
GO
CREATE FUNCTION [dbo].[Page_GetPagesForSharedContent]  
(  
	@ContentIds	nvarchar(max),
	@SiteId		uniqueidentifier = NULL
)  
RETURNS @tbPages TABLE   
(  
	PageDefinitionId	uniqueidentifier,
	SiteId				uniqueidentifier,
	Title				nvarchar(max),
	FriendlyName		nvarchar(max),
	PageStatus			int,
	WorkflowState		int
)  
AS  
BEGIN  
	DECLARE @tbContentIds TABLE (Id uniqueidentifier)
	
	INSERT INTO @tbContentIds
	SELECT [Value] FROM dbo.SplitComma(@ContentIds, ',')
	
	INSERT INTO @tbPages
	SELECT DISTINCT P.PageDefinitionId,
		P.SiteId,
		P.title,
		P.FriendlyName,
		P.PageStatus,
		P.WorkflowState
	FROM PageDefinition P
		JOIN PageDefinitionContainer PC ON PC.PageDefinitionId = P.PageDefinitionId 
		JOIN @tbContentIds C ON C.Id = PC.ContentId
		LEFT JOIN MKCampaignEmail E ON E.CMSPageId = P.PageDefinitionId
	WHERE (P.WorkflowState = 1 OR P.WorkflowState = 2) 
		AND P.PageStatus = 8
		AND (@SiteId IS NULL OR P.SiteId = @SiteId)
		AND E.Id IS NULL

	RETURN   
END  
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name='IX_PageDefinition_Status_EnableOutputCache' AND object_id = OBJECT_ID('PageDefinition'))
BEGIN
	PRINT 'Creating index IX_PageDefinition_Status_EnableOutputCache'
	
	CREATE NONCLUSTERED INDEX [IX_PageDefinition_Status_EnableOutputCache]
	ON [dbo].[PageDefinition]([PageStatus] ASC, [EnableOutputCache] ASC)
END
GO
IF (OBJECT_ID('PageDto_GetOutputCacheUrls') IS NOT NULL)
	DROP PROCEDURE PageDto_GetOutputCacheUrls
GO
CREATE PROCEDURE [dbo].[PageDto_GetOutputCacheUrls]
(
	@SiteId	uniqueidentifier
)
AS
BEGIN
	SELECT P.PageDefinitionId AS Id, 
		CASE WHEN B.HomePageDefinitionId = N.PageMapNodeId AND N.TargetId = P.PageDefinitionId THEN
				LOWER(S.PrimarySiteUrl)
			WHEN N.TargetId = P.PageDefinitionId THEN
				LOWER(S.PrimarySiteUrl + N.CompleteFriendlyUrl)
			ELSE
				LOWER(S.PrimarySiteUrl + N.CompleteFriendlyUrl + '/' + P.FriendlyName)
		END AS Url  
	FROM dbo.PageDefinition P    
		JOIN dbo.PageMapNodePageDef PD ON PD.PageDefinitionId = P.PageDefinitionId    
		JOIN dbo.PageMapNode N ON N.PageMapNodeId = PD.PageMapNodeId 
		JOIN dbo.SISite S ON P.SiteId = S.Id
		JOIN dbo.PageMapBase B ON B.SiteId = S.Id
	WHERE P.PageStatus = 8 AND S.Id = @SiteId
		AND (P.EnableOutputCache IS NULL OR P.EnableOutputCache = 1)

	--UNION ALL

	--SELECT P.Id AS Id, 
	--	LOWER(S.PrimarySiteUrl + '/' + N.CompleteFriendlyUrl + '/' + PageDef.FriendlyName+'/'+ CAST(datepart(year,P.PostDate) AS VARCHAR(10)) +'/'+  LEFT(CONVERT(CHAR(20), P.PostDate, 101), 2)+'/'+P.FriendlyName) AS Url
	--FROM dbo.BLBlog B 
	--	JOIN dbo.BLPost P ON B.Id = P.BlogId 
	--	JOIN PageMapNodePageDef PD  ON PD.PageDefinitionId = BlogListPageId
	--	JOIN dbo.PageDefinition PageDef ON PD.PageDefinitionId = PageDef.PageDefinitionId  
	--	JOIN dbo.PageMapNode N ON N.PageMapNodeId = PD.PageMapNodeId
	--	JOIN dbo.SISite S ON S.Id = B.ApplicationId
	--WHERE ContentDefinitionId IS NOT NULL AND P.Status = 8 AND S.Id = @SiteId
END
GO
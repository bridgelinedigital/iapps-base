GO
PRINT 'Updating the version in the productsuite table'
GO
UPDATE iAppsProductSuite SET UpdateStartDate = GETUTCDATE(), ProductVersion='7.0.2.0'
GO

PRINT 'Modify trigger tgrATAttribute'
GO
Update COTaxonomy SET SiteId = ApplicationId WHERE SiteId IS NULL
GO

PRINT 'Modify trigger tgrATAttribute'
IF(OBJECT_ID('tgrATAttribute') IS NOT NULL)
	DROP TRIGGER tgrATAttribute
GO
GO
CREATE Trigger tgrATAttribute ON ATAttribute
AFTER UPDATE
AS 

DECLARE @AttributeId uniqueidentifier
IF EXISTS (Select 1 FROM inserted i where i.Status = 3)
BEGIN
	SELECT @AttributeId = i.Id FROM inserted i;
	EXEC Contact_DropColumnFromFlatTable @AttributeId
END

IF EXISTS (
    SELECT 1 FROM INSERTED I
		JOIN DELETED D ON D.Id = I.Id AND D.AttributeDataTypeId != I.AttributeDataTypeId 
		WHERE I.Status != 3
    )
BEGIN
	SELECT @AttributeId = i.Id FROM inserted i;
	EXEC Contact_DropColumnFromFlatTable @AttributeId

	IF EXISTS (SELECT 1 FROM ATAttributeCategoryItem WHERE AttributeId = @AttributeId AND 
		(CategoryId = 3 OR CategoryId = 4))
	BEGIN
		DELETE FROM TRKAttribute WHERE AttributeId = @AttributeId

		INSERT INTO TRKAttribute(AttributeId, CreatedDate, [Status]) 
		SELECT	@AttributeId, GETUTCDATE(), 1

		EXEC Contact_AddColumnsToFlatTable
	END
END
GO
PRINT 'Modify trigger tgrATAttributeCategoryItem'
GO
IF(OBJECT_ID('tgrATAttributeCategoryItem') IS NOT NULL)
	DROP TRIGGER tgrATAttributeCategoryItem
GO
CREATE TRIGGER [dbo].[tgrATAttributeCategoryItem] ON [dbo].[ATAttributeCategoryItem]
AFTER INSERT
AS 
IF EXISTS(SELECT 1 FROM inserted WHERE CategoryId = 3 OR CategoryId = 4)
BEGIN	
	DELETE
	FROM	TRKAttribute
	WHERE	AttributeId IN (
		SELECT	Id
		FROM	inserted
	) 

	INSERT INTO TRKAttribute(AttributeId, CreatedDate, [Status]) 
		SELECT	DISTINCT AttributeId, GETUTCDATE(), 1
		FROM	inserted

	EXEC Contact_AddColumnsToFlatTable
END
GO
PRINT 'Modify trigger tgrATAttributeCategoryItemDelete'
GO
IF(OBJECT_ID('tgrATAttributeCategoryItemDelete') IS NOT NULL)
	DROP TRIGGER tgrATAttributeCategoryItemDelete
GO
CREATE Trigger [dbo].[tgrATAttributeCategoryItemDelete] ON [dbo].[ATAttributeCategoryItem]
AFTER DELETE
AS 

DECLARE @AttributeId uniqueidentifier

IF EXISTS(Select 1 FROM deleted i where CategoryId = 3 OR CategoryId = 4)
BEGIN
	
	SELECT @AttributeId = i.AttributeId FROM deleted i;
	--PRINT @AttributeId 
	EXEC Contact_DropColumnFromFlatTable @AttributeId
END
GO
PRINT 'Creating View vwEventLog'
GO
IF (OBJECT_ID('vwEventLog') IS NOT NULL)
	DROP View vwEventLog	
GO
CREATE VIEW [dbo].[vwEventLog]
AS 
SELECT L.Id,
	L.Priority,
	L.Severity,
	L.CreatedDate AS LogDate,
	L.MachineName,
	L.ActivityId,
	L.SiteId,
	L.Message,
	E.Stacktrace,
	E.InnerException,
	L.AdditionalInfo
FROM LGLog L
	LEFT JOIN LGExceptionLog E ON L.Id = E.LogId
GO
PRINT 'Creating View vwInventoryNotes'
GO
IF (OBJECT_ID('vwInventoryNotes') IS NOT NULL)
	DROP View vwInventoryNotes	
GO
CREATE VIEW [dbo].[vwInventoryNotes]
AS 
SELECT N.Id,
	N.InventoryId AS ObjectId,
	N.OldQuantity,
	N.NewQuantity,
	CASE WHEN N.ReasonDetail IS NULL OR N.ReasonDetail = '' THEN R.Title ELSE R.Title + ' - ' + N.ReasonDetail END AS Message,
	N.ModifiedDate AS CreatedDate,
	N.ModifiedBy AS CreatedBy,
	CFN.UserFullName AS CreatedByFullName
FROM INInventoryLog N
	LEFT JOIN GLReason R ON N.ReasonId = R.Id
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = N.ModifiedBy
GO
PRINT 'Creating View vwContactPropertiesAndAttributes'
GO
IF (OBJECT_ID('vwContactPropertiesAndAttributes') IS NOT NULL)
	DROP View vwContactPropertiesAndAttributes	
GO
CREATE VIEW vwContactPropertiesAndAttributes
AS
SELECT ContactId, 
	CAV.AttributeId, 
	AttributeEnumId, 
	CASE WHEN A.IsEnum = 1 THEN CAST(AttributeEnumId AS varchar(36)) ELSE Value END AS Value, 
	Notes, 
	CAV.CreatedDate, 
	CAV.CreatedBy, 
	CAV.CreatedDate AS ModifiedDate, 
	CAV.CreatedBy AS ModifiedBy, 
	ADT.Type as DataType 
FROM ATContactAttributeValue CAV  WITH (INDEX(IXATContactAttributeValue_ContactId))
	INNER JOIN ATAttribute A ON A.ID = CAV.AttributeId 
	INNER JOIN ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId 
WHERE (Value IS NOT NULL OR AttributeEnumId IS NOT NULL) AND ContactId IS NOT NULL

UNION 

SELECT CustomerId, 
	CAV.AttributeId, 
	AttributeEnumId, 
	CASE WHEN A.IsEnum = 1 THEN CAST(AttributeEnumId AS varchar(36)) ELSE Value END AS Value, 
	NULL AS Notes, 
	CAV.CreatedDate, 
	CAV.CreatedBy, 
	CAV.CreatedDate AS ModifiedDate, 
	CAV.CreatedBy AS ModifiedBy, 
	ADT.Type as DataType 
FROM CSCustomerAttributeValue CAV 
	INNER JOIN ATAttribute A ON A.ID = CAV.AttributeId 
	INNER JOIN ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId 
WHERE (Value IS NOT NULL OR AttributeEnumId IS NOT NULL) AND CustomerId IS NOT NULL

UNION 

SELECT Id AS ContactId,
	'11111111-0000-0000-0000-000000000000', 
	NULL, 
	FirstName AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.String'  
FROM dbo.MKContact 
WHERE FirstName IS NOT NULL

UNION

SELECT Id AS ContactId,
	'22222222-0000-0000-0000-000000000000', 
	NULL, 
	MiddleName AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.String'  
FROM dbo.MKContact 
WHERE MiddleName IS NOT NULL

UNION

SELECT Id AS ContactId,
	'33333333-0000-0000-0000-000000000000', 
	NULL, 
	LastName AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.String'  
FROM dbo.MKContact 
WHERE LastName IS NOT NULL

UNION

SELECT Id AS ContactId,
	'44444444-0000-0000-0000-000000000000',
	NULL, 
	Email AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.String'  
FROM dbo.MKContact 
WHERE Email IS NOT NULL

UNION

SELECT Id AS ContactId,
	'55555555-0000-0000-0000-000000000000', 
	NULL, 
	CompanyName AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.String'  
FROM dbo.MKContact 
WHERE CompanyName IS NOT NULL

UNION 

SELECT Id AS ContactId,
	'66666666-0000-0000-0000-000000000000', 
	NULL, 
	CAST(LastInteractionDate AS nvarchar(max)) AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.DateTime'  
FROM dbo.MKContact 
WHERE LastInteractionDate IS NOT NULL

UNION 

SELECT Id AS ContactId,
	'77777777-0000-0000-0000-000000000000', 
	NULL, 
	CAST(LeadScore AS nvarchar(max)) AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.Int32'  
FROM dbo.MKContact 
WHERE LeadScore IS NOT NULL

UNION 

SELECT Id AS ContactId,
	'11111111-0000-0000-0000-000000000000', 
	NULL, 
	FirstName AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.String'  
FROM dbo.USUser 
WHERE FirstName IS NOT NULL

UNION

SELECT Id AS ContactId,
	'22222222-0000-0000-0000-000000000000', 
	NULL, 
	MiddleName AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.String'  
FROM dbo.USUser 
WHERE MiddleName IS NOT NULL

UNION

SELECT Id AS ContactId,
	'33333333-0000-0000-0000-000000000000', 
	NULL, 
	LastName AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.String'  
FROM dbo.USUser 
WHERE LastName IS NOT NULL

UNION

SELECT UserId AS ContactId,
	'44444444-0000-0000-0000-000000000000', 
	NULL, 
	Email AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.String'  
FROM dbo.USMembership M 
	INNER JOIN dbo.USUser U ON U.Id = M.UserId AND U.Status = 1 
WHERE Email IS NOT NULL

UNION

SELECT Id AS ContactId,'66666666-0000-0000-0000-000000000000', 
	NULL, 
	CAST(LastInteractionDate AS nvarchar(max)) AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.DateTime'  
FROM dbo.USUser 
WHERE LastInteractionDate IS NOT NULL

UNION

SELECT Id AS ContactId,
	'77777777-0000-0000-0000-000000000000', 
	NULL, 
	CAST(LeadScore AS nvarchar(max)) AS VALUE, 
	NULL AS NOTES, 
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy, 
	'System.Int32'  
FROM dbo.USUser 
WHERE LeadScore IS NOT NULL
GO

PRINT 'Modify stored procedure Contact_AddColumnsToFlatTable'
GO
IF(OBJECT_ID('Contact_AddColumnsToFlatTable') IS NOT NULL)
	DROP PROCEDURE Contact_AddColumnsToFlatTable
GO
CREATE PROCEDURE Contact_AddColumnsToFlatTable
(
	@InsertAll bit = NULL
)
AS 
BEGIN
	Declare @Attributes Table (AttributeId uniqueidentifier, DataType varchar(100), Processed bit)
	
	IF(@InsertAll IS NULL OR  @InsertAll = 0)
		INSERT INTO @Attributes 
		Select A.Id, AT.Type, 0 from ATAttribute A
		INNER JOIN ATAttributeDataType AT ON A.AttributeDataTypeId = AT.Id
		INNER JOIN ATAttributeCategoryItem AG ON AG.AttributeId = A.Id AND (AG.CategoryId = 3 OR AG.CategoryId = 4) 
		INNER JOIN TRKAttribute TA ON TA.AttributeId = A.Id 
		where TA.Status = 1 and A.Status = 1
	ELSE
		INSERT INTO @Attributes 
		Select A.Id, AT.Type, 0 from ATAttribute A
		INNER JOIN ATAttributeDataType AT ON A.AttributeDataTypeId = AT.Id
		INNER JOIN ATAttributeCategoryItem AG ON AG.AttributeId = A.Id AND (AG.CategoryId = 3 OR AG.CategoryId = 4)  
		where A.Status = 1


	Declare @AttributeId uniqueidentifier, @Type nvarchar(100), @SQL nvarchar(2000), @SqlType nvarchar(100)
	
	WHILE EXISTS(Select 1 from @Attributes where Processed = 0)
	BEGIN
		Select top 1 @AttributeId = AttributeId, @Type = DataType from @Attributes where Processed = 0
		IF NOT EXISTS(SELECT * FROM sys.columns 
					WHERE Name = CAST(@AttributeId AS Varchar(40)) AND Object_ID = Object_ID(N'DNContactAttribute'))
		BEGIN
			Print 'Attribute not exists'
			SET @SqlType = dbo.GetSQLDataType(@Type)

			SET @SQL = 'ALTER Table DNContactAttribute ADD ' + QUOTENAME(CAST(@AttributeId AS Varchar(40)))  + @SqlType
			EXEC (@SQL)

			IF @SqlType = 'bit'
			BEGIN
				SET @SQL = 'UPDATE DNContactAttribute SET ' + QUOTENAME(CAST(@AttributeId AS Varchar(40)))  + ' = 0'
				EXEC (@SQL)
			END
		END
		ELSE
			Print 'Attribute exists'

			Update @Attributes Set Processed = 1 where AttributeId = @AttributeId
	END

	DELETE TA
	FROM TRKAttribute TA
	INNER JOIN @Attributes A ON A.AttributeId = TA.AttributeId
	where A.Processed = 1

END
GO
PRINT 'Modify stored procedure Contact_ContactAttributeSearch'
GO
IF(OBJECT_ID('Contact_ContactAttributeSearch') IS NOT NULL)
	DROP PROCEDURE Contact_ContactAttributeSearch
GO
CREATE PROCEDURE [dbo].[Contact_ContactAttributeSearch]
(
	@ContactAttributeSearchXml XML,
	@ContactSource varchar(100),
    @ApplicationId UNIQUEIDENTIFIER
)
AS
BEGIN

	--truncate table LogContactSearch
	--IF EXISTS(Select * from tempdb.dbo.sysobjects o where o.xtype in ('U')	and	o.id = object_id( N'tempdb..#tempContactSearchOutput'))
	--	 Truncate table #tempContactSearchOutput
	--ELSE
	--	CREATE TABLE #tempContactSearchOutput (Id uniqueidentifier NOT NULL PRIMARY KEY)

DECLARE	@Condition varchar(3)

SET @Condition = @ContactAttributeSearchXml.value('(/ContactAttributeSearch/@Condition)[1]','varchar(3)')
IF @Condition IS NULL
SET @Condition ='OR'

DECLARE @ContactSearch TYContactAttributeSearch
exec WriteToLogContact 'START In Contact_SearchDynamicContactAttribute,  '
    INSERT  INTO @ContactSearch
            ( ConditionId ,
                AttributeId ,
                AttributeValueID ,
                Value1 ,
                Value2 ,
                Operator ,
                DataType ,
                ValueCount
            )
            SELECT  NEWID() AS ConditionId ,
                    col.value('@AttributeId', 'uniqueidentifier') AS AttributeId ,
                    NULL,
                    col.value('@Value1', 'nvarchar(max)') AS Value1 ,
                    col.value('@Value2', 'nvarchar(max)') AS Value2 ,
                    col.value('@Operator', 'nvarchar(50)') AS Operator ,
                    'System.String' AS DataType,
					1
            FROM    @ContactAttributeSearchXml.nodes('/ContactAttributeSearch/AttributeSearchItem') tab ( col )

			Declare @ContactSearchLocal Table 
					(RowId uniqueidentifier primary key,
					AttributeId uniqueidentifier,
					Value1 nvarchar(1000),
					Value2 nvarchar(1000),
					Operator nvarchar(50),
					DataType varchar(50),
					Processed bit
					)


			Declare @AttributeLocal Table (Id UniqueIdentifier)
			
			INSERT INTO @AttributeLocal
			SELECT A.Id FROM ATAttribute A 
			INNER JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND (C.CategoryId = 3 OR C.CategoryId = 4)
			Where A.Status = 1

			INSERT INTO @AttributeLocal VALUES ('11111111-0000-0000-0000-000000000000')
			INSERT INTO @AttributeLocal VALUES ('22222222-0000-0000-0000-000000000000')
			INSERT INTO @AttributeLocal VALUES ('33333333-0000-0000-0000-000000000000')
			INSERT INTO @AttributeLocal VALUES ('44444444-0000-0000-0000-000000000000')
			INSERT INTO @AttributeLocal VALUES ('55555555-0000-0000-0000-000000000000')
			INSERT INTO @AttributeLocal VALUES ('66666666-0000-0000-0000-000000000000')
			INSERT INTO @AttributeLocal VALUES ('77777777-0000-0000-0000-000000000000')

			INSERT INTO @ContactSearchLocal
			Select ConditionId, AttributeId, Value1, Value2, Operator, DataType, 0 
			from @ContactSearch C
			INNER JOIN @AttributeLocal A ON A.Id = C.AttributeId 

			UPDATE  CS
			SET     CS.DataType = ADT.TYPE
			FROM    @ContactSearchLocal CS 
            INNER JOIN dbo.ATAttribute A ON A.Id = CS.AttributeId
            INNER JOIN dbo.ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId 

			UPDATE @ContactSearchLocal SET DataType = 'System.DateTime' WHERE AttributeId = '66666666-0000-0000-0000-000000000000'
			UPDATE @ContactSearchLocal SET DataType = 'System.Int32' WHERE AttributeId = '77777777-0000-0000-0000-000000000000'

				CREATE TABLE #ContactIds
				(
					ContactId UNIQUEIDENTIFIER Primary Key
				)

			Declare @Sql varchar(max), @datetimesql varchar(256), @minDateTime varchar(256), @QAttributeId varchar(100), @QDAttributeId varchar(100)
			SET @Sql = ''
			Set @minDateTime = '1900-01-01 00:00:00.000'

			Declare @AttributeId uniqueidentifier, @Value1 nvarchar(1000), @Value2 nvarchar(1000), @RowId  Uniqueidentifier, @Operator varchar(100), @DataType varchar(50)
			IF EXISTS(Select top 1 RowId from @ContactSearchLocal)
			BEGIN
				Set @Sql = 'INSERT INTO #ContactIds Select ContactId from DNContactAttribute' 

				WHILE EXISTS(Select 1 from @ContactSearchLocal where Processed = 0)
				BEGIN
					Select top 1 @AttributeId = AttributeId, @RowId = RowId, @Value1= Value1, @Value2 = Value2, @Operator = Operator, @DataType = DataType from @ContactSearchLocal where Processed = 0
					
					Set @datetimesql = ''
					Set @QAttributeId  = QUOTENAME(CAST(@AttributeId AS Varchar(100)))
					Set @QDAttributeId  = @QAttributeId

					IF(@DataType = 'System.DateTime')
						Set @datetimesql = @QAttributeId + ' != '  + QUOTENAME(@minDateTime, '''') + ' AND '

					IF(@Operator = 'opOnDay' OR @Operator = 'opOnNthDayAfter' OR @Operator = 'opOnNthDayBefore' 
						OR @Operator = 'opFortheFollowingXDays' OR @Operator = 'opForthePreviousXDays'
						OR @Operator = 'opFortheDaysBeforeNthDay' OR @Operator = 'opFortheDaysAfterNthDay')
						Set @QAttributeId = ''

					IF (CHARINDEX(' WHERE ', @Sql)) > 0
						SET @Sql = @Sql + ' '+ @Condition +' (' + @datetimesql + @QAttributeId +  dbo.GetDynamicOperator (@Operator, @DataType, @Value1, @Value2, @QDAttributeId, @ApplicationId) + ' ) '
					else
						SET @Sql = @Sql + ' WHERE (' +  @datetimesql + @QAttributeId + dbo.GetDynamicOperator (@Operator, @DataType, @Value1, @Value2, @QDAttributeId, @ApplicationId) + ' ) '
	

					 Update @ContactSearchLocal Set Processed = 1 where RowId = @RowId
				END
			END

	Print (@Sql) 

	IF(@Sql != '')
		Exec (@Sql)

	--exec WriteToLogContact @SQL
	
	exec WriteToLogContact 'After insert in ContactIds,  '
	--Select * from #ContactIds

		If EXISTS (Select * from dbo.SplitComma(@ContactSource,',') where Value = 8 or Value = 9)
		BEGIN
			exec WriteToLogContact 'In Contact Attribute search in else profile search,  '
			exec Contact_ProfileSearch @ContactSearch, @ApplicationId 
			--Insert #ContactIds exec Contact_ProfileSearch @ContactSearch, @ApplicationId 
		END

		DECLARE @currentRecords INT
		SET @currentRecords = ( SELECT  COUNT(ID)
								FROM    #tempContactSearchOutput
								)

		IF @currentRecords > 0 
			BEGIN
				

				PRINT '@currentRecords > 0 '
					+ CONVERT(NVARCHAR(23), GETUTCDATE(), 121)      
				DELETE  T
				FROM    #tempContactSearchOutput t
						LEFT JOIN #ContactIds LT ON LT.ContactId = T.Id
				WHERE   LT.ContactId IS NULL
			END
		ELSE 
			BEGIN 
				exec WriteToLogContact 'In Contact Attribute search after count,  '      
				PRINT ' INSERT  INTO #tempContactSearchOutput'
					+ CONVERT(NVARCHAR(23), GETUTCDATE(), 121)                
				INSERT  INTO #tempContactSearchOutput
						SELECT  ContactId
						FROM    #ContactIds
			END
		exec WriteToLogContact 'In end of Contact Attribute search'

--		select * from #tempContactSearchOutput

		SELECT  @@ROWCOUNT
		

END
GO

DECLARE @Sequence int
SELECT @Sequence = MAX(Sequence) + 1 FROM dbo.STSettingType
IF NOT EXISTS(SELECT 1 FROM STSettingType WHERE Name = 'Modules.ExcludedExtensions')
BEGIN
    INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, IsEnabled, IsVisible, ControlType)
    VALUES('Modules.ExcludedExtensions', 'List of file extensions to exclude from unbound http modules', 1, @Sequence, 1, 1, 'Textbox')

	IF EXISTS (SELECT 1 FROM ATAttributeCategoryItem WHERE CategoryId = 4)
		EXEC Contact_AddColumnsToFlatTable @InsertAll = 1

	IF EXISTS (SELECT 1 FROM CSCustomerAttributeValue)
		EXEC Contact_UpdateContactAttributes @InsertAll= 1
END
GO
PRINT 'Modify view vwFormResponse'
GO
IF(OBJECT_ID('vwFormResponse') IS NOT NULL)
	DROP VIEW vwFormResponse
GO
CREATE VIEW [dbo].[vwFormResponse]
AS
SELECT	R.Id,
		R.FormsId AS FormId,
		R.FormsResponseXML AS ResponseXml,
		R.ResponseDate,
		R.IPAddress,
		R.SiteId,
		R.UserId AS ResponseById,
		RN.UserFullName AS ResponseByFullName,
		LFR.Email,
		LFR.FirstName,
		LFR.MiddleName,
		LFR.LastName,
		LFR.CompanyName,
		LFR.Gender,
		LFR.BirthDate,
		LFR.HomePhone,
		LFR.MobilePhone,
		LFR.OtherPhone,
		LFR.Attributes
FROM	FormsResponse AS R
		LEFT JOIN MKLeadFormResponse AS LFR ON LFR.FormResponseId = R.Id
		LEFT JOIN VW_UserFullName AS RN ON RN.UserId = R.UserId
GO
PRINT 'Modify stored procedure Contact_UpdateContactAttributes'
GO
IF(OBJECT_ID('Contact_UpdateContactAttributes') IS NOT NULL)
	DROP PROCEDURE  Contact_UpdateContactAttributes
GO
CREATE PROCEDURE [dbo].[Contact_UpdateContactAttributes]
(
	@ContactId		uniqueidentifier = NULL,
	@BatchSize		int = null,
	@InsertAll		bit = 0,
	@DebugInfo		xml = null OUTPUT,
	@RowsAffected	bigint =null OUTPUT
)
AS
BEGIN
	DECLARE @tbDebugInfo TABLE(Message nvarchar(2000))

	IF @BatchSize IS NULL SET @BatchSize = 1000
	IF @BatchSize = 0 SET @BatchSize = 7000
	
	DECLARE @ContactsUpdate TYContact
	
	IF @ContactId IS NOT NULL
	BEGIN
		IF EXISTS(SELECT 1 FROM TRKContact WHERE ContactId = @ContactId)
		BEGIN 
			INSERT INTO @ContactsUpdate (ContactId, Status) 
			SELECT ContactId, Status FROM TRKContact 
			WHERE ContactId = @ContactId --AND AttributeDenormStatus = 0 
		END
		ELSE 
		BEGIN
			IF EXISTS(SELECT Id, Status FROM MKContact WHERE Id = @ContactId)
			BEGIN
				INSERT INTO @ContactsUpdate (ContactId, Status) 
				SELECT Id, Status FROM MKContact WHERE Id = @ContactId
			END
			ELSE IF EXISTS (SELECT Id, Status FROM USUser WHERE Id = @ContactId)
			BEGIN
				INSERT INTO @ContactsUpdate (ContactId, Status) 
				SELECT Id, Status FROM USUser WHERE Id = @ContactId
			END
		END    
	END
	ELSE
	BEGIN 
		INSERT INTO @ContactsUpdate (ContactId, Status) 
		SELECT TOP (@BatchSize) ContactId, Status FROM TRKContact 
		WHERE AttributeDenormStatus = 0
	END	

	UPDATE C
	SET C.AttributeDenormStatus = 1
	FROM TRKContact C
		JOIN @ContactsUpdate T ON C.ContactId = T.ContactId

	INSERT INTO @tbDebugInfo SELECT 'Number of contacts to update: ' + CAST(@BatchSize AS varchar(100))

	BEGIN TRY
		BEGIN TRANSACTION;

		IF (@InsertAll = 1)
			TRUNCATE TABLE DNContactAttribute
		ELSE
			DELETE FROM DNContactAttribute WHERE ContactId IN (SELECT ContactId From @ContactsUpdate)

		INSERT INTO @tbDebugInfo SELECT 'Contacts deleted from DNContactAttribute' 

		DECLARE @AttributeColumns TABLE (Position int primary key, CloumnName nvarchar(100), DataType nvarchar(50), Processed bit) 
		
		INSERT INTO @AttributeColumns 
		SELECT ORDINAL_POSITION, COLUMN_NAME, DATA_TYPE, 0
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = N'DNContactAttribute' AND COLUMN_NAME != 'ContactId'

		DECLARE @AttributeId varchar(100),  
			@DataType nvarchar(50),
			@InsertSQL varchar(max), 
			@SelectSQL varchar(max), 
			@SQL nvarchar(max)
	
		DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10)
		SET @InsertSQL = 'INSERT INTO DNContactAttribute ([ContactId]'
		SET @SelectSQL =  @NewLineChar + ' Select V.ContactId'
		WHILE EXISTS(SELECT 1 FROM @AttributeColumns WHERE Processed = 0)
		BEGIN
			SELECT TOP 1 @AttributeId = CloumnName, @DataType = DataType FROM @AttributeColumns 
			WHERE Processed = 0 ORDER BY Position

			SET @InsertSQL = @InsertSQL +  @NewLineChar + ',' +  QUOTENAME(@AttributeId) 
			IF @DataType = 'bit'
				SET @SelectSQL = @SelectSQL +  @NewLineChar + ', MAX (Case AttributeId When '''+ @AttributeId  + ''' Then CASE WHEN Value = ''true'' THEN ''true'' ELSE ''false'' END ELSE ''false'' End) ' + QUOTENAME(@AttributeId)
			ELSE
				SET @SelectSQL = @SelectSQL +  @NewLineChar + ', MIN (Case AttributeId When '''+ @AttributeId  + ''' Then Value ELSE NULL End) ' + QUOTENAME(@AttributeId)

			UPDATE @AttributeColumns SET Processed = 1 WHERE CloumnName = @AttributeId
		END
		
		SET @SQL = @InsertSQL + ' )' +  @SelectSQL

		IF (@InsertAll = 1)
			SET @SQL = @SQL + @NewLineChar + ' from vwContactPropertiesAndAttributes V Group By V.ContactId'
		ELSE
			SET @SQL = @SQL + @NewLineChar + ' from vwContactPropertiesAndAttributes V INNER JOIN @ContactsUpdate C ON C.ContactId = V.ContactId AND C.Status = 1 Group By V.ContactId'

		EXECUTE sp_executesql @SQL, 
			N'@ContactsUpdate TYContact READONLY',
			@ContactsUpdate

		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;

		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
		DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
		DECLARE @ErrorState INT = ERROR_STATE();

		INSERT INTO @tbDebugInfo SELECT @ErrorMessage 

		UPDATE C
		SET C.AttributeDenormStatus = 0
		FROM TRKContact C
			JOIN @ContactsUpdate T ON C.ContactId = T.ContactId
		WHERE C.AttributeDenormStatus = 1

		RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH

	INSERT INTO @tbDebugInfo SELECT 'Contacts added in DNContactAttribute' 
		
	UPDATE C
	SET C.AttributeDenormStatus = 2
	FROM TRKContact C
		JOIN @ContactsUpdate T ON C.ContactId = T.ContactId
	WHERE C.AttributeDenormStatus = 1
		
	INSERT INTO @tbDebugInfo SELECT 'Contacts removed from TRKContact' 

	EXEC [ContactTrackerDto_Delete]

	SET @DebugInfo = (SELECT Message FROM @tbDebugInfo For XML PATH (''), ROOT('Messages'))

	SELECT @RowsAffected = COUNT(1) FROM @ContactsUpdate
END
GO
PRINT 'Modify stored procedure CampaignContact_Get'
GO
IF(OBJECT_ID('CampaignContact_Get') IS NOT NULL)
	DROP PROCEDURE  CampaignContact_Get
GO
CREATE PROCEDURE [dbo].[CampaignContact_Get]
(
	@SiteId					uniqueidentifier,
	@CampaignRunId			uniqueidentifier,
	@BatchSize				int,
	@ProcessingStatus		int = NULL,
	@MarkAsProcessing		bit = NULL
)
AS
BEGIN
	IF NOT EXISTS(SELECT 1 FROM MKCampaignRunWorkTable WHERE @CampaignRunId = CampaignRunId)
		RETURN

	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	IF @ProcessingStatus IS NULL SET @ProcessingStatus = 0

	DECLARE @tbContacts TABLE 
	(
		ContactId		uniqueidentifier primary key,
		AddressId		uniqueidentifier, 
		SiteId			uniqueidentifier,
		TotalRecords	int
	)
		
	;WITH CTE AS
	(
		SELECT 
			UserId, 
			AddressId, 
			SiteId,
			COUNT(C.UserId) OVER () AS TotalRecords,
			ROW_NUMBER() OVER (ORDER BY SendDate) AS RowNumber
		FROM MKCampaignRunWorkTable C
		WHERE @CampaignRunId = CampaignRunId 
			AND ProcessingStatus = @ProcessingStatus
			AND (SendDate IS NULL OR SendDate <= @UtcNow)
	)

	INSERT INTO @tbContacts (ContactId, AddressId, SiteId, TotalRecords)
	SELECT TOP (@BatchSize) UserId, AddressId, SiteId, TotalRecords FROM CTE
	ORDER BY RowNumber

	IF @MarkAsProcessing = 1
	BEGIN
		UPDATE MKCampaignRunWorkTable 
			SET ProcessingCount = ISNULL(ProcessingCount, 0) + 1, 
				ProcessingStatus = 1,
				ProcessingTime = GETUTCDATE()
		WHERE UserId IN (SELECT ContactId FROM @tbContacts)
			AND CampaignRunId = @CampaignRunId
	END

	-- Contacts
	SELECT W.CampaignRunId,
		W.UserId Id,
		W.FirstName,
		W.MiddleName,
		W.LastName,
		W.CompanyName,
		W.BirthDate,
		W.Gender,
		W.AddressId,
		W.Status,
		W.HomePhone,
		W.MobilePhone,
		W.OtherPhone,
		W.Email,
		W.Notes,
		W.ContactType,
		ISNULL(T.SiteId, @SiteId) AS SiteId,
		ISNULL(T.SiteId, @SiteId) AS PrimarySiteId,
		T.TotalRecords
	FROM @tbContacts T 
		JOIN MKCampaignRunWorkTable W ON T.ContactId = W.UserId 
			AND W.CampaignRunId = @CampaignRunId

	-- Address
	SELECT A.Id,
		A.FirstName,
		A.LastName,
		A.AddressType,
		A.AddressLine1,
		A.AddressLine2,
		A.AddressLine3,
		A.City,
		A.StateId,
		A.Zip,		
		A.CountryId,
		A.Phone,
		A.CreatedDate,
		A.CreatedBy,
		A.ModifiedDate,
		A.ModifiedBy,
		A.Status,
		A.County,
		A.CountryName,
		A.CountryCode,
		A.StateCode,
		A.StateName
	FROM vw_Address A
		JOIN @tbContacts C ON C.AddressId = A.Id

	SELECT V.Id,
		V.ContactId,
		V.AttributeId,
		V.AttributeEnumId,
		V.Value,
		V.Notes,
		V.CreatedBy,
		V.CreatedDate,
		A.Title AS AttributeTitle
	FROM ATContactAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbContacts T ON V.ContactId = T.ContactId

	UNION ALL

	SELECT V.Id,
		V.CustomerId AS ContactId,
		V.AttributeId,
		V.AttributeEnumId,
		V.Value,
		NULL AS Notes,
		V.CreatedBy,
		V.CreatedDate,
		A.Title AS AttributeTitle
	FROM CSCustomerAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbContacts T ON V.CustomerId = T.ContactId
END	
GO
PRINT 'Modify stored procedure Comments_GetByObjectId'
GO
IF(OBJECT_ID('Comments_GetByObjectId') IS NOT NULL)
	DROP PROCEDURE  Comments_GetByObjectId
GO
CREATE PROCEDURE [dbo].[Comments_GetByObjectId]
(
	@ObjectId		uniqueidentifier,
	@Status			int = null,
	@PageNumber		int = 1,
	@PageSize		int = 10,
	@SortOrder		nvarchar(10) = null,
	@VirtualCount	int output
)
WITH RECOMPILE
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************

BEGIN
--********************************************************************************
-- code
--********************************************************************************
Declare @Local_ObjectId uniqueidentifier
Set @Local_ObjectId = @ObjectId

	IF @PageNumber is NUll
		set @PageNumber = 1
	IF @SortOrder is null
		set @SortOrder = 'desc'
	Declare @commentsRootId uniqueidentifier
	SET @commentsRootId =(Select top 1 Id from BLComments Where LftValue=1)
	
	DECLARE @tbComments table (RowNumber int identity(1,1), Id uniqueidentifier, LftValue bigint, RgtValue bigint)
	IF (lower(@SortOrder) = 'desc')
		Insert into @tbComments
			Select Id, LftValue, RgtValue from BLComments Where Level = 0 And ObjectId = @Local_ObjectId And (@Status IS NULL OR Status = @Status) AND (ParentId is NULL OR ParentId=@commentsRootId)
			Order by LftValue desc
	ELSE
		Insert into @tbComments
			Select Id, LftValue, RgtValue from BLComments Where Level = 0 And ObjectId = @Local_ObjectId And (@Status IS NULL OR Status = @Status) AND (ParentId is NULL OR ParentId=@commentsRootId)
			Order by LftValue
			

	SET @VirtualCount = @@ROWCOUNT
	
	;With ResultEntries As(
		Select 
			ROW_NUMBER() over (Order by P.RowNumber) As RowNumber,
			C.Id, 
			C.ParentId, 
			C.ObjectId, 
			C.PostedBy, 
			C.Email, 
			C.Title, 
			C.Comments, 
			C.UserId, 
			C.Status, 
			dbo.ConvertTimeFromUtc(C.CreatedDate, C.ApplicationId) AS CreatedDate, 
			C.Level,
			C.CreatedBy, 
			FN.FirstName, 
			FN.LastName
			from @tbComments P
				Inner Join BLComments C On C.LftValue Between P.LftValue AND P.RgtValue 
				LEFT JOIN USUser FN on FN.Id = COALESCE(NULLIF(C.UserId, '00000000-0000-0000-0000-000000000000'), C.CreatedBy)
		Where @PageSize IS NULL OR (RowNumber >= (@PageSize * @PageNumber) - (@PageSize - 1) AND RowNumber <= @PageSize * @PageNumber)
		AND (@Status IS NULL OR C.Status = @Status)
	)
	
	
	SELECT RE.*,R.Id as RatingId, R.RatingValue, R.ObjectId, R.IPAddress, R.CreatedDate as RatingCreatedDate
		FROM ResultEntries RE Left Join BLCommentRating CR on RE.Id= CR.CommentId 
				Left Join BLRatings R on CR.RatingId = R.Id
		
END
GO

PRINT 'Updating Store settings'
GO
DECLARE @SettingGroupId int, @Sequence int, @sql nvarchar(max)
SET @SettingGroupId = NULL
SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Store Settings'
IF @SettingGroupId IS NULL
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
	SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
	Set @sql =N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''Store Settings'', @Sequence, 1, ''ccf96e1d-84db-46e3-b79e-c7392061219b'')'
	exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
END

UPDATE STSettingType SET FriendlyName = 'Store admin email (comma delimited)',
	SettingGroupId = @SettingGroupId,
	ControlType = 'Textbox',
	IsVisible = 1
WHERE Name = 'StoreAdminEmail'

UPDATE STSettingType SET FriendlyName = 'CSR email',
	SettingGroupId = @SettingGroupId,
	ControlType = 'Textbox',
	IsVisible = 1
WHERE Name = 'StoreCustomerServiceEmail'

UPDATE STSettingType SET FriendlyName = 'Default Product Height for shipping',
	SettingGroupId = @SettingGroupId,
	ControlType = 'Textbox',
	IsVisible = 1
WHERE Name = 'DefaultProductHeight'

UPDATE STSettingType SET FriendlyName = 'Default Product Length for shipping',
	SettingGroupId = @SettingGroupId,
	ControlType = 'Textbox',
	IsVisible = 1
WHERE Name = 'DefaultProductLength'

UPDATE STSettingType SET FriendlyName = 'Default Product Weight for shipping',
	SettingGroupId = @SettingGroupId,
	ControlType = 'Textbox',
	IsVisible = 1
WHERE Name = 'DefaultProductWeight'

UPDATE STSettingType SET FriendlyName = 'Default Product Width for shipping',
	SettingGroupId = @SettingGroupId,
	ControlType = 'Textbox',
	IsVisible = 1
WHERE Name = 'DefaultProductWidth'

-- Cart & Order
SET @SettingGroupId = NULL
SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Cart & Order'
IF @SettingGroupId IS NULL
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
	SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
	Set @sql =N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''Cart & Order'', @Sequence, 1, ''ccf96e1d-84db-46e3-b79e-c7392061219b'')'
	exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
END

UPDATE STSettingType SET FriendlyName = NULL,
	SettingGroupId = @SettingGroupId,
	ControlType = 'Checkbox',
	IsVisible = 1
WHERE Name = 'BackOrderShippingSplit'
GO

PRINT 'Modify view vwWorkflow'
GO
IF(OBJECT_ID('vwWorkflow') IS NOT NULL)
	DROP VIEW vwWorkflow
GO 
CREATE VIEW [dbo].[vwWorkflow]
AS
SELECT W.Id,
	W.Title,
	W.Description,
	W.Status,
	W.ObjectTypeId,
	W.SkipAfterDays,
	W.IsGlobal,
	W.IsShared,
	W.CreatedBy,
	W.CreatedDate,
	W.ModifiedBy,
	W.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName,
	W.ApplicationId AS SiteId
FROM WFWorkflow W
	LEFT JOIN VW_UserFullName FN on FN.UserId = W.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = W.ModifiedBy
WHERE W.Status = dbo.GetActiveStatus()		
GO
PRINT 'Creating up_LoadProduct_DailyLoad'
GO
IF(OBJECT_ID('etl.up_LoadProduct_DailyLoad') IS NOT NULL)
	DROP PROCEDURE [etl].[up_LoadProduct_DailyLoad]
GO
CREATE PROCEDURE [etl].[up_LoadProduct_DailyLoad](@LogicalDate datetime)
As
SELECT     dbo.PRProduct.Id AS ProductId, 
			dbo.vwProductTypePath.[ProductTypePath] +  N'id-' + PRProduct.ProductIDUser + '/' + PRProduct.UrlFriendlyTitle 
			AS ProductDefaultVirtualPath,
			dbo.PRProduct.ProductIDUser as ProductCode,
		   (dbo.PRProduct.Title) as ProductTitle,
			(dbo.PRProduct.Description) as ProductDescription,
			(dbo.PRProduct.SEOFriendlyUrl) as ProductSEOFriendlyUrl,
			dbo.PRProduct.CreatedDate, 
			dbo.PRProduct.ModifiedDate
FROM         dbo.PRProduct INNER JOIN
            dbo.vwProductTypePath ON dbo.PRProduct.ProductTypeID = dbo.vwProductTypePath.ProductTypeId
			
WHERE    
			 ((CAST(CAST(dbo.PRProduct.ModifiedDate  AS char(11)) AS datetime) = @LogicalDate) OR
				(CAST(CAST(dbo.PRProduct.CreatedDate  AS char(11)) AS datetime) = @LogicalDate))

GO
PRINT 'Update Stored Procedure BLD_ExportAttributeEnums'
GO
IF(OBJECT_ID('BLD_ExportAttributeEnums') IS NOT NULL)
	DROP PROCEDURE  BLD_ExportAttributeEnums
GO
CREATE PROCEDURE [dbo].[BLD_ExportAttributeEnums]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		Begin
			select 
				e.Id as 'Key'
				,a.Title as AttributeTitle
				,coalesce(cast(e.Value as varchar(max)), cast(e.NumericValue as varchar(max)), '')
				,ac.Name as AttributeCategoryName
				,0 as UpdateFlag
			from dbo.ATAttributeCategoryItem aci
			join dbo.ATAttributeCategory ac on (ac.id = aci.CategoryId)
			join dbo.ATAttribute a on (aci.AttributeId = a.Id)
			join dbo.ATAttributeEnum e on (a.Id = e.AttributeId)
			where 
				--ac.IsSystem = 0 and 
				(ac.IsGlobal != 1 or ac.IsGlobal is null) and e.Status = 1
		END
	ELSE
		Begin
			select 
			 e.Id  as 'Key'
            ,a.Title as AttributeTitle
            ,coalesce(cast(e.Value as varchar(max)), cast(e.NumericValue as varchar(max)), '')
            ,ac.Name as AttributeCategoryName
            ,0 as UpdateFlag
            from dbo.ATAttributeEnumVariantCache aevc
            join dbo.ATAttributeEnum e on aevc.id=e.id
            join dbo.ATAttribute a on (e.AttributeID = a.Id)
            join dbo.ATAttributeCategoryItem aci on (a.id = aci.AttributeId)
            join dbo.ATAttributeCategory ac on (aci.CategoryId = ac.Id)
			where 
				--ac.IsSystem = 0 and
				(ac.IsGlobal != 1 or ac.IsGlobal is null) and 
				aevc.SiteId = @siteId and e.Status = 1
		END
END

GO

PRINT N'Update complete';
GO

PRINT 'Updating end date in the iAppsProductSuite table'
GO
UPDATE iAppsProductSuite SET UpdateEndDate = GETUTCDATE()
GO

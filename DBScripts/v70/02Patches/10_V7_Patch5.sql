IF EXISTS (SELECT * FROM sys.indexes WHERE name='IX_QueryId' AND object_id = OBJECT_ID('dbo.NVFilterOutput'))
BEGIN
 DROP INDEX IX_QueryId ON dbo.NVFilterOutput
END
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name='IX_ObjectId' AND object_id = OBJECT_ID('dbo.NVFilterOutput'))
BEGIN
 DROP INDEX IX_ObjectId ON dbo.NVFilterOutput
END
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name='IX_NVFilterOutput_QueryId_ObjectId' AND object_id = OBJECT_ID('dbo.NVFilterOutput'))
BEGIN
 DROP INDEX IX_NVFilterOutput_QueryId_ObjectId ON dbo.NVFilterOutput
END
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name='PK_NVFilterOutput' AND object_id = OBJECT_ID('dbo.NVFilterOutput'))
BEGIN
	BEGIN TRY
	 ALTER TABLE dbo.NVFilterOutput DROP CONSTRAINT PK_NVFilterOutput;
	END TRY
	BEGIN CATCH
	END CATCH
	BEGIN TRY
	 DROP INDEX [PK_NVFilterOutput] ON [dbo].[NVFilterOutput] WITH ( ONLINE = OFF );
	END TRY
	BEGIN CATCH
	END CATCH
END
GO

IF (OBJECT_ID('DF_NVFilterOutput_SerialNo') IS NOT NULL)
BEGIN
	ALTER TABLE [dbo].[NVFilterOutput] DROP CONSTRAINT [DF_NVFilterOutput_SerialNo]
END
GO

IF COL_LENGTH('[dbo].[NVFilterOutput]', 'SerialNo') IS NOT NULL
BEGIN
	ALTER TABLE [dbo].[NVFilterOutput] Drop Column [SerialNo]
END
GO

IF COL_LENGTH('[dbo].[NVFilterOutput]', 'SerialNo') IS NULL
BEGIN
	ALTER TABLE [dbo].[NVFilterOutput] Add [SerialNo] bigint
END

IF (OBJECT_ID('DF_NVFilterOutput_SerialNo') IS  NULL)
BEGIN
	ALTER TABLE [dbo].[NVFilterOutput] ADD CONSTRAINT [DF_NVFilterOutput_SerialNo] DEFAULT (1) FOR [SerialNo]
END
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name='IX_NVFilterOutput' AND object_id = OBJECT_ID('dbo.NVFilterOutput'))
CREATE CLUSTERED INDEX [IX_NVFilterOutput] ON [dbo].[NVFilterOutput]
(
	[QueryId] ASC,
	[ObjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
PRINT 'Creating View vwNavProduct'
GO
IF (OBJECT_ID('vwNavProduct') IS NOT NULL)
	DROP View vwNavProduct	
GO
CREATE VIEW vwNavProduct
AS
SELECT	N.NavNodeId AS NavId,
		N.NavNodeUrl,
		FO.SerialNo DisplayOrder,
		CASE WHEN E.Id IS NULL THEN 0 ELSE 1 END AS ExcludedFromNav,	
		P.[Id],   
		P.SiteId,  
		P.SourceSiteId,  
		P.Title,   
		P.ShortDescription,   
		P.LongDescription,   
		P.[Description],   
		P.ProductIDUser,   
		P.Keyword,   
		P.ProductStyle,  
		P.BundleCompositionLastModified,   
		P.UrlFriendlyTitle,   
		P.TaxCategoryID,   
		P.IsActive,
		P.PromoteAsNewItem,   
		P.PromoteAsTopSeller,  
		P.ExcludeFromDiscounts,   
		P.ExcludeFromShippingPromotions,   
		P.Freight,   
		P.Refundable,   
		P.TaxExempt,  
		P.ProductTypeID,   
		P.[Status],   
		P.IsBundle,   
		P.ProductTypeName,  
		P.ProductUrl,
		P.SEOTitle,
		P.SEOH1,   
		P.SEODescription,   
		P.SEOKeywords,   
		P.SEOFriendlyUrl,  
		P.IsShared,  
		P.CreatedDate,   
		P.CreatedById,   
		P.CreatedByFullName,  
		P.ModifiedDate,   
		P.ModifiedById,   
		P.ModifiedByFullName
FROM	NVFilterOutput FO
		INNER JOIN NVNavNodeNavFilterMap AS N ON N.QueryId = FO.QueryId
		INNER JOIN vwProduct AS P ON P.Id = FO.ObjectId
		LEFT JOIN NVFilterExclude AS E ON E.QueryId = N.QueryId AND E.ObjectId = P.Id

GO
PRINT 'Creating vwContactAttribute'
GO
IF(OBJECT_ID('vwContactAttribute') IS NOT NULL)
	DROP VIEW vwContactAttribute
GO
CREATE VIEW [dbo].[vwContactAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	NULL AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	INNER JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 3

UNION ALL

SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	3 AS CategoryId,
	V.ContactId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	INNER JOIN ATContactAttributeValue V ON A.Id = V.AttributeId
GO
IF(OBJECT_ID('SiteDto_Get') IS NOT NULL)
	DROP PROCEDURE SiteDto_Get
GO
CREATE PROCEDURE [dbo].[SiteDto_Get]
(
	@Id						uniqueidentifier = NULL,
	@Ids					nvarchar(max)=NULL,
	@MasterSiteId			uniqueidentifier = NULL,
	@IncludeVariantSites	bit = NULL,
	@CurrentMasterSiteOnly	bit = NULL,
	@AllowTranslation		bit = NULL,
	@CampaignId				uniqueidentifier = NULL,
	@SiteListId				uniqueidentifier = NULL,
	@ParentSiteId			uniqueidentifier = NULL,
	@UserId					uniqueidentifier = NULL,
	@SiteId					uniqueidentifier = NULL,
	@Status					int = NULL,
	@PageNumber				int = NULL,
	@PageSize				int = NULL,
	@MaxRecords				int = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@Keyword				nvarchar(MAX) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50), 
		@IdsExist bit, @LftValue int, @RgtValue int
	SET @PageLowerBound = @PageSize * @PageNumber

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @IncludeVariantSites IS NULL SET @IncludeVariantSites = 1
	IF @Status IS NULL SET @Status = 1
	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'	

	IF @MasterSiteId IS NULL AND @CurrentMasterSiteOnly = 1
		SELECT TOP 1 @MasterSiteId = MasterSiteId FROM SISite WHERE Id = @SiteId
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbSiteIds TABLE (Id uniqueidentifier)
	
	IF @Ids IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT Items FROM dbo.SplitGUID(@Ids, ',') 
	END
	IF @SiteListId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM SISiteListSite WHERE SiteListId = @SiteListId
	END
	
	IF @CampaignId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		DECLARE @CampaignSiteId uniqueidentifier
		SELECT TOP 1 @CampaignSiteId = ApplicationId FROM MKCampaign WHERE Id = @CampaignId
		IF @CampaignSiteId = @SiteId SET @CampaignSiteId = NULL

		INSERT INTO @tbSiteIds
		SELECT SiteId FROM MKCampaignRunHistory H
		WHERE EXISTS (SELECT 1 FROM MKCampaignSend S WHERE H.CampaignSendId = S.Id 
				AND CampaignId = @CampaignId AND (@CampaignSiteId IS NULL OR SiteId = @SiteId))
	END

	IF @UserId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM [dbo].[GetAccessibleSitesForUser](@UserId)
	END
	ELSE IF @ParentSiteId IS NOT NULL AND @ParentSiteId != dbo.GetEmptyGUID()
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM [dbo].[GetChildrenSites](@ParentSiteId, 1)
	END
	
	INSERT INTO @tbIds
	SELECT S.Id AS Id, ROW_NUMBER() OVER (ORDER BY
							CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
							CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
							CASE WHEN @SortClause IS NULL THEN CreatedDate END ASC	
						) AS RowNumber
	FROM SISite AS S
	WHERE (@Id IS NULL OR S.Id = @Id) AND
		(@AllowTranslation IS NULL OR S.SubmitToTranslation = @AllowTranslation) AND
		(@Status IS NULL OR S.Status = @Status) AND
		(@MasterSiteId IS NULL OR S.Id = @MasterSiteId OR S.MasterSiteId = @MasterSiteId) AND
		(@IncludeVariantSites = 1 OR S.MasterSiteId = S.Id OR S.MasterSiteId IS NULL) AND
		(@IdsExist IS NULL OR EXISTS (Select 1 FROM @tbSiteIds T Where T.Id = S.Id)) AND
		(@Keyword IS NULL OR (S.Title like @Keyword OR S.Description like @Keyword OR S.PrimarySiteUrl like @Keyword OR S.ExternalCode like @Keyword))
		
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				S.Id AS Id
		FROM SISite S
			JOIN @tbIds T ON T.Id = S.Id
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT		S.*,
				S.SubmitToTranslation AS AllowTranslation,
				S.DefaultTranslateToLanguage AS TranslationLocale,
				SiteHierarchyPath AS SitePath,
				T.RowNumber,
				T.TotalRecords
	FROM SISite AS S
		JOIN @tbPagedResults AS T ON T.Id = S.Id
	ORDER BY RowNumber

	SELECT V.*, A.Title AS AttributeTitle
	FROM ATSiteAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbPagedResults T ON V.SiteId = T.Id
END
GO
IF(OBJECT_ID('AttributeGroup_GetGroup') IS NOT NULL)
	DROP PROCEDURE AttributeGroup_GetGroup
GO

CREATE PROCEDURE [dbo].[AttributeGroup_GetGroup](
--********************************************************************************
-- Parameters Passing in Attribute Group ID
--********************************************************************************
			@Id uniqueidentifier = null,
			@IsSystem bit =null,
			@ApplicationId uniqueidentifier=null
)
AS
BEGIN

	SELECT 
		GroupId Id,
		Name Title,
		SiteId,
		IsSystem,
		CreatedDate,
		CreatedBy,
		ModifiedDate,
		ModifiedBy,
		[Status],
		Id CategoryId
	FROM 
		ATAttributeCategory    
	WHERE
		--[Id]=	Isnull(@Id,[Id])
		--and IsSystem =isnull(@IsSystem, IsSystem)
        (@Id is null or [GroupId] = @Id)
		and (@IsSystem is null or IsSystem = @IsSystem)
		AND
        [Status] = dbo.GetActiveStatus() -- Select only Active Attributes
		and (IsHidden is null or IsHidden=0 or @Id is not null)
	ORDER BY SiteId,Title, IsSystem

	SELECT 
		A.Id,
		C.GroupId AttributeGroupId,
		AttributeDataType,
		AttributeDataTypeId,
		Title,
		A.[Description],
		Sequence,
		Code,
		IsFaceted,
		IsSearched,
		A.IsSystem,
		IsDisplayed,
		IsEnum,
		A.CreatedDate,
		A.CreatedBy,
		A.ModifiedDate,
		A.ModifiedBy,
		A.[Status],
		IsPersonalized
	FROM ATAttribute A
	INNER JOIN ATAttributeCategoryItem CI ON A.Id=CI.AttributeId
	INNER JOIN ATAttributeCategory C on CI.CategoryId=C.Id
	WHERE --AttributeGroupId= Isnull(@Id, AttributeGroupId )
			(@Id is null or C.GroupId = @Id)
			AND
			A.[Status]	= dbo.GetActiveStatus() -- Select only Active Attributes
	Order By AttributeGroupId,Sequence, Title
END

GO

PRINT 'Updating Navigation filter queries'
declare @Id					uniqueidentifier,
	@Condition			varchar(max),
	@OrderByProperty	varchar(50),
	@SortOrder			varchar(10),
	@ApplicationId uniqueidentifier,
	@MaxNoOfRecord bigint,
	@sqlQuery nvarchar(max),
	@QueryId uniqueidentifier

DECLARE nav_cursor CURSOR
FOR select Id from NVFilterQuery
OPEN nav_cursor
FETCH NEXT FROM nav_cursor into @QueryId;
WHILE @@FETCH_STATUS = 0
BEGIN
	select @Id=Id, @Condition=LogicalCondition,@OrderByProperty=OrderByProperty,@SortOrder=SortOrder ,@ApplicationId=SiteId
from NVFilterQuery Where Id=@QueryId
SET @sqlQuery = [dbo].[NavFilter_GetQuery](@Id,@Condition,@OrderByProperty,@SortOrder,@ApplicationId)
	

	IF(@MaxNoOfRecord >-1)
		SET @sqlQuery = 'Select Top '+  cast(@MaxNoOfRecord as nvarchar(20)) + ' ' + substring(@sqlQuery,7,len(@sqlQuery)-6)

UPDATE NVFilterQuery Set ActualQuery = @sqlQuery Where Id =@Id

FETCH NEXT FROM nav_cursor into @QueryId
END 
CLOSE nav_cursor;
DEALLOCATE nav_cursor;
GO
IF NOT EXISTS (SELECT * FROM STSettingType Where Name = 'EnableGAEnhancedEcommerce')
INSERT INTO dbo.STSettingType
        ( Name ,
          FriendlyName ,
          SettingGroupId ,
          Sequence,
		  IsEnabled,
		  IsVisible,
		  ControlType
        )
SELECT top 1
		'EnableGAEnhancedEcommerce'
		,'Enable Google Analytics Enhanced Ecommerce'
		,1
		,max(Sequence)+1
		,1
		,1
		,'Checkbox'
		FROM STSettingType

GO

PRINT 'Creating View vwCampaignRun'
GO
IF (OBJECT_ID('vwCampaignRun') IS NOT NULL)
	DROP View vwCampaignRun	
GO
CREATE VIEW [dbo].[vwCampaignRun]
WITH SCHEMABINDING
AS 

SELECT R.Id,
	R.CampaignId,
	C.Title AS CampaignName,
	RunDate,
	Sends,
	UniqueOpens AS Opens,
	Clicks,
	Bounces,
	Unsubscribes,
	CASE WHEN UniqueOpens > 0 AND Sends > 0 THEN CEILING((100.0 * UniqueOpens)/Sends) ELSE 0 END AS OpenRate,
	CASE WHEN Clicks > 0 AND Sends > 0 THEN CEILING((100.0 * Clicks)/Sends) ELSE 0 END AS ClickRate,
	CASE WHEN Bounces > 0 AND Sends > 0 THEN CEILING((100.0 * Bounces)/Sends) ELSE 0 END AS BounceRate,
	CASE WHEN Unsubscribes > 0 AND Sends > 0 THEN CEILING((100.0 * Unsubscribes)/Sends) ELSE 0 END AS UnsubscribeRate,
	R.SiteId,
	R.Status
FROM dbo.MKCampaignRunHistory R
	JOIN dbo.MKCampaign C ON R.CampaignId = C.Id
WHERE C.Type = 1
GO

CREATE UNIQUE CLUSTERED INDEX IX_vwCampaignRun_Id
	ON dbo.vwCampaignRun(Id)
GO
GO
IF(OBJECT_ID('CampaignRunReportDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignRunReportDto_Get
GO
CREATE PROCEDURE [dbo].[CampaignRunReportDto_Get]  
(  
	@StartDate			datetime,
	@EndDate			datetime,
	@ReportType			int,
	@CampaignName		nvarchar(500) = NULL,
	@SiteId				uniqueidentifier = NULL,
	@IgnoreSite			bit = NULL,
	@PageNumber			int = NULL,
	@PageSize			int = NULL,	
	@SortBy				nvarchar(100) = NULL,  
	@SortOrder			nvarchar(10) = NULL
)  
AS  
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(500)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL) SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	
	IF (@CampaignName = '') SET @CampaignName = NULL
	IF (@CampaignName IS NOT NULL) SET @CampaignName = '%' + @CampaignName + '%'
	IF (@IgnoreSite = 1) SET @SiteId = NULL
	SET @EndDate = DATEADD(DD, 1, @EndDate)

	IF @ReportType = 1
	BEGIN
		;WITH CTE AS(
			SELECT ROW_NUMBER() OVER (ORDER BY 
					CASE WHEN @SortClause like 'RunDate ASC' THEN R.RunDate END ASC,
					CASE WHEN @SortClause like 'RunDate DESC' THEN R.RunDate END DESC,
					CASE WHEN @SortClause like 'Sends ASC' THEN R.Sends END ASC,
					CASE WHEN @SortClause like 'Sends DESC' THEN R.Sends END DESC,
					CASE WHEN @SortClause like 'Opens ASC' THEN R.Opens END ASC,
					CASE WHEN @SortClause like 'Opens DESC' THEN R.Opens END DESC,
					CASE WHEN @SortClause like 'OpenRate ASC' THEN R.OpenRate END ASC,
					CASE WHEN @SortClause like 'OpenRate DESC' THEN R.OpenRate END DESC,
					CASE WHEN @SortClause like 'Clicks ASC' THEN R.Clicks END ASC,
					CASE WHEN @SortClause like 'Clicks DESC' THEN R.Clicks END DESC,
					CASE WHEN @SortClause like 'ClickRate ASC' THEN R.ClickRate END ASC,
					CASE WHEN @SortClause like 'ClickRate DESC' THEN R.ClickRate END DESC,
					CASE WHEN @SortClause like 'Bounces ASC' THEN R.Bounces END ASC,
					CASE WHEN @SortClause like 'Bounces DESC' THEN R.Bounces END DESC,
					CASE WHEN @SortClause like 'BounceRate ASC' THEN R.BounceRate END ASC,
					CASE WHEN @SortClause like 'BounceRate DESC' THEN R.BounceRate END DESC,
					CASE WHEN @SortClause like 'Unsubscribes ASC' THEN R.Unsubscribes END ASC,
					CASE WHEN @SortClause like 'Unsubscribes DESC' THEN R.Unsubscribes END DESC,
					CASE WHEN @SortClause like 'UnsubscribeRate ASC' THEN R.UnsubscribeRate END ASC,
					CASE WHEN @SortClause like 'UnsubscribeRate DESC' THEN R.UnsubscribeRate END DESC,
					CASE WHEN @SortClause IS NULL THEN R.RunDate END DESC		
				) AS RowNumber,
				COUNT(R.Id) OVER () AS TotalRecords,
				R.Id AS Id,
				R.CampaignId,
				R.CampaignName,
				R.RunDate,
				R.Sends,
				R.Opens,
				R.Clicks,
				R.Bounces,
				R.Unsubscribes,
				R.OpenRate,
				R.ClickRate,
				R.BounceRate,
				R.UnsubscribeRate,
				R.SiteId
			FROM vwCampaignRun R
			WHERE RunDate BETWEEN @StartDate AND @EndDate
				AND (@CampaignName IS NULL OR CampaignName like @CampaignName)
				AND (@SiteId IS NULL OR R.SiteId = @SiteId)
				AND R.Status = 2
		)

		SELECT C.*, 
			CASE WHEN S.ExternalCode IS NOT NULL AND S.ExternalCode != '' THEN S.Title + ' (' + S.ExternalCode + ')'
				ELSE S.Title END AS SiteName
		FROM CTE C
			JOIN SISite S ON C.SiteId = S.Id
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		ORDER BY RowNumber
	END
	ELSE IF @ReportType = 2
	BEGIN
		DECLARE @tbCampaigns TABLE
		(
			RunDate date,
			Sends int,
			Opens int,
			Clicks int,
			Bounces int,
			Unsubscribes int
		)

		INSERT INTO @tbCampaigns
		SELECT CAST((CAST(YEAR(RunDate) AS VARCHAR(4)) + '-' + CAST(MONTH(RunDate) AS VARCHAR(2)) + '-10') AS DATE) AS RunDate,
			Sends,
			UniqueOpens AS Opens,
			Clicks,
			Bounces,
			Unsubscribes
		FROM dbo.MKCampaignRunHistory R
			JOIN dbo.MKCampaign C ON R.CampaignId = C.Id
		WHERE C.Type = 1 
			AND RunDate BETWEEN @StartDate AND @EndDate
			AND (@CampaignName IS NULL OR C.Title like @CampaignName)
			AND (@SiteId IS NULL OR R.SiteId = @SiteId)
			AND R.Status = 2

		;WITH CTE AS(
			SELECT RunDate,
				COUNT(1) AS NoOfCampaigns,
				SUM(Sends) AS Sends,
				SUM(Opens) AS Opens,
				SUM(Clicks) AS Clicks,
				SUM(Bounces) AS Bounces,
				SUM(Unsubscribes) AS Unsubscribes
			FROM @tbCampaigns
			GROUP BY RunDate
		), 
		RATECTE AS (
			SELECT RunDate,
				NoOfCampaigns,
				Sends,
				Opens,
				Clicks,
				Bounces,
				Unsubscribes,
				CASE WHEN Opens > 0 AND Sends > 0 THEN CEILING((100.0 * Opens)/Sends) ELSE 0 END AS OpenRate,
				CASE WHEN Clicks > 0 AND Sends > 0 THEN CEILING((100.0 * Clicks)/Sends) ELSE 0 END AS ClickRate,
				CASE WHEN Bounces > 0 AND Sends > 0 THEN CEILING((100.0 * Bounces)/Sends) ELSE 0 END AS BounceRate,
				CASE WHEN Unsubscribes > 0 THEN CEILING((100.0 * Unsubscribes)/Sends) ELSE 0 END AS UnsubscribeRate
			FROM CTE
		),
		PagedCTE AS (
			SELECT ROW_NUMBER() OVER (ORDER BY 
					CASE WHEN @SortClause like 'RunDate ASC' THEN R.RunDate END ASC,
					CASE WHEN @SortClause like 'RunDate DESC' THEN R.RunDate END DESC,
					CASE WHEN @SortClause like 'Sends ASC' THEN R.Sends END ASC,
					CASE WHEN @SortClause like 'Sends DESC' THEN R.Sends END DESC,
					CASE WHEN @SortClause like 'Opens ASC' THEN R.Opens END ASC,
					CASE WHEN @SortClause like 'Opens DESC' THEN R.Opens END DESC,
					CASE WHEN @SortClause like 'OpenRate ASC' THEN R.OpenRate END ASC,
					CASE WHEN @SortClause like 'OpenRate DESC' THEN R.OpenRate END DESC,
					CASE WHEN @SortClause like 'Clicks ASC' THEN R.Clicks END ASC,
					CASE WHEN @SortClause like 'Clicks DESC' THEN R.Clicks END DESC,
					CASE WHEN @SortClause like 'ClickRate ASC' THEN R.ClickRate END ASC,
					CASE WHEN @SortClause like 'ClickRate DESC' THEN R.ClickRate END DESC,
					CASE WHEN @SortClause like 'Bounces ASC' THEN R.Bounces END ASC,
					CASE WHEN @SortClause like 'Bounces DESC' THEN R.Bounces END DESC,
					CASE WHEN @SortClause like 'BounceRate ASC' THEN R.BounceRate END ASC,
					CASE WHEN @SortClause like 'BounceRate DESC' THEN R.BounceRate END DESC,
					CASE WHEN @SortClause like 'Unsubscribes ASC' THEN R.Unsubscribes END ASC,
					CASE WHEN @SortClause like 'Unsubscribes DESC' THEN R.Unsubscribes END DESC,
					CASE WHEN @SortClause like 'UnsubscribeRate ASC' THEN R.UnsubscribeRate END ASC,
					CASE WHEN @SortClause like 'UnsubscribeRate DESC' THEN R.UnsubscribeRate END DESC,
					CASE WHEN @SortClause IS NULL THEN R.RunDate END DESC		
				) AS RowNumber,
				COUNT(R.RunDate) OVER () AS TotalRecords,
				R.NoOfCampaigns,
				R.RunDate,
				R.Sends,
				R.Opens,
				R.Clicks,
				R.Bounces,
				R.Unsubscribes,
				R.OpenRate,
				R.ClickRate,
				R.BounceRate,
				R.UnsubscribeRate
			FROM RATECTE R
		)

		SELECT * FROM PagedCTE
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		ORDER BY RowNumber
	END
END
GO

IF(OBJECT_ID('Campaign_ResubscribeEmails') IS NOT NULL)
	DROP PROCEDURE Campaign_ResubscribeEmails
GO

CREATE PROCEDURE [dbo].[Campaign_ResubscribeEmails]
	@EmailAddresses		XML,
	@CampaignIds		XML = NULL,
	@SiteId				UNIQUEIDENTIFIER
AS
BEGIN
	BEGIN TRANSACTION
	
		DECLARE	@TempHistory TABLE
		(
			Id			UNIQUEIDENTIFIER,
			UserId		UNIQUEIDENTIFIER,
			[Action]	VARCHAR(20),
			ActionDate	DATETIME,
			CampaignId	UNIQUEIDENTIFIER
		)
		
		DECLARE	@SelectedUserIds TABLE
		(
			UserId		UNIQUEIDENTIFIER
		)
				
		-- Get UserIds from XML
		INSERT INTO @SelectedUserIds (UserId)
			SELECT	UserId
			FROM	vw_contacts
			WHERE	Email IN (
						SELECT	Email.value('.', 'NVARCHAR(255)') 
						FROM	@EmailAddresses.nodes('/ArrayOfString/string') AS Emails(Email)
					)			

		-- Resubscribing to all campaigns	
		IF @CampaignIds IS NULL OR (@CampaignIds.exist('/ArrayOfGuid/guid')= 0)
			BEGIN
				-- Delete all unsubscribe records for the given UserId and SiteId
				DELETE
				FROM	USUserUnsubscribe
				OUTPUT	NEWID(), deleted.UserId, 'Resubscribe', GETUTCDATE(), deleted.CampaignId
				INTO	@TempHistory (Id, UserId, [Action], ActionDate, CampaignId)
				WHERE	UserId IN (
							SELECT	UserId
							FROM	@SelectedUserIds
						) AND (
							CampaignId IN (
								SELECT	Id
								FROM	MKCampaign
								WHERE	ApplicationId = @SiteId
							) OR
							CampaignId IS NULL
						)
				
				-- Delete all resubscribe records for the given SiteId as they are no longer needed
				DELETE
				FROM	USResubscribe
				WHERE	UserId IN (
							SELECT	UserId
							FROM	@SelectedUserIds
						) AND (
							CampaignId IN (
								SELECT	Id
								FROM	MKCampaign
								WHERE	ApplicationId = @SiteId
							)
						)
			END
		-- Resubscribing to specific campaigns
		ELSE
			BEGIN
				DECLARE	@SelectedCampaignIds TABLE
				(
					CampaignId	UNIQUEIDENTIFIER
				)
				
				-- Get CampaignIds from XML
				INSERT INTO	@SelectedCampaignIds (CampaignId)
					SELECT	Campaign.value('.', 'UNIQUEIDENTIFIER')
					FROM	@CampaignIds.nodes('/ArrayOfGuid/guid') AS Campaigns(Campaign)
							INNER JOIN MKCampaign AS C ON C.Id = Campaign.value('.', 'UNIQUEIDENTIFIER')
				
				-- Delete all unsubscribe records for the given UserId and CampaignIds
				DELETE
				FROM	USUserUnsubscribe
				OUTPUT	NEWID(), deleted.UserId, 'Resubscribe', GETUTCDATE(), deleted.CampaignId
				INTO	@TempHistory (Id, UserId, [Action], ActionDate, CampaignId)
				WHERE	UserId IN (
							SELECT	UserId
							FROM	@SelectedUserIds
						) AND (
							CampaignId IN (
								SELECT	CampaignId
								FROM	@SelectedCampaignIds
							)
						)
				
				-- Insert resubscribe records for the given CampaignIds for each UserId that has previously unsubscribed from all campaigns
				INSERT INTO	USResubscribe (Id, UserId, CampaignId)
				OUTPUT	NEWID(), inserted.UserId, 'Resubscribe', GETUTCDATE(), inserted.CampaignId
				INTO	@TempHistory (Id, UserId, [Action], ActionDate, CampaignId)
					SELECT	NEWID(), U.UserId, C.CampaignId
					FROM	@SelectedUserIds AS U,
							@SelectedCampaignIds AS C
					WHERE	NOT EXISTS (
								SELECT	*
								FROM	USResubscribe AS R
								WHERE	R.UserId = U.UserId AND
										R.CampaignId = C.CampaignId
							) AND (
								NOT EXISTS (
									SELECT	*
									FROM	@TempHistory AS H
									WHERE	H.UserId = U.UserId AND
											H.CampaignId = C.CampaignId
								) AND
								EXISTS (
									SELECT	*
									FROM	USUserUnsubscribe AS UN
									WHERE	UN.UserId = U.UserId AND
											UN.CampaignId IS NULL
								)
							)
			END
		
		-- Insert records into history table
		INSERT INTO USUnsubscribeHistory (Id, UserId, [Action], ActionDate, CampaignId)
			SELECT	*
			FROM	@TempHistory
		
		-- Return summary table of all users that were resubscribed during the stored procedure call
		SELECT		H.*, C.Email, M.Title AS Campaign
		FROM		@TempHistory AS H
					INNER JOIN vw_contacts AS C ON C.UserId = H.UserId
					LEFT JOIN MKCampaign AS M ON M.Id = H.CampaignId
		ORDER BY	C.Email, M.Title
	
	IF @@ERROR<>0 
		BEGIN
			ROLLBACK TRANSACTION
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION
		END
END
GO

IF(OBJECT_ID('Commerce_GetURLForSiteMap') IS NOT NULL)
	DROP PROCEDURE Commerce_GetURLForSiteMap
GO

CREATE PROCEDURE [dbo].[Commerce_GetURLForSiteMap]
(
	@SiteId		uniqueidentifier
)
AS
BEGIN
	DECLARE @tbProducts TABLE (Id uniqueidentifier, Url nvarchar(max), ModifiedDate datetime)
	DECLARE @tbProductView TABLE (Id uniqueidentifier, ViewCount int)
	DECLARE @tbProductPriority TABLE(Id uniqueidentifier, Priority int)
	DECLARE @MaxViewCount int

	-- SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' means Master site else variant site--
	IF( @SiteId ='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4')	
	 BEGIN
	  INSERT INTO @tbProducts
	  SELECT P.Id,
		CASE WHEN SEOFriendlyUrl IS NULL OR SEOFriendlyUrl = ''
			THEN LOWER(dbo.GetProductTypePath(T.Id, @SiteId) + N'id-' + P.ProductIDUser + '/' + UrlFriendlyTitle)
			ELSE LOWER(SEOFriendlyUrl)
		END,
		CASE WHEN P.ModifiedDate IS NULL
			THEN P.CreatedDate
			ELSE P.ModifiedDate
		END
	    FROM (SELECT DISTINCT ProductId FROM PRProductSKU WHERE IsActive = 1 AND IsOnline = 1) AS S
		INNER JOIN vwProduct AS P ON P.Id = S.ProductId
		INNER JOIN PRProductType AS T ON P.ProductTypeId = T.Id
		WHERE P.SiteId=@siteId AND P.IsActive = 1 
     END
	ELSE
	 BEGIN
	  INSERT INTO @tbProducts
	  SELECT P.Id,
		CASE WHEN SEOFriendlyUrl IS NULL OR SEOFriendlyUrl = '' 
			THEN LOWER(dbo.GetProductTypePath(T.Id, @SiteId) + N'id-' + P.ProductIDUser + '/' + UrlFriendlyTitle)
			ELSE LOWER(SEOFriendlyUrl)
		END,
		CASE WHEN P.ModifiedDate IS NULL
			THEN P.CreatedDate
			ELSE P.ModifiedDate
		END
	    FROM  (SELECT DISTINCT ProductId FROM PRProductSKU WHERE Id IN ( SELECT DISTINCT Id FROM PRProductSKUVariantCache WHERE IsActive = 1 AND IsOnline = 1 AND SiteId = @SiteId)) AS S
        INNER JOIN vwProduct AS P ON P.Id = S.ProductId
		INNER JOIN PRProductType AS T ON P.ProductTypeId = T.Id
		Where P.SiteId=@siteId AND P.IsActive = 1 
       END
   
	INSERT INTO @tbProductView
	SELECT ProductId, COUNT(ProductId)
	FROM TRProductView
	GROUP BY ProductId

	SELECT @MaxViewCount = ISNULL(MAX(ViewCount), 1) FROM @tbProductView

	INSERT INTO @tbProductPriority
	SELECT Id,
		CAST((CASE WHEN ISNULL(ViewCount, 0) = 0
			THEN 0.5
			ELSE ViewCount
		END) / @MaxViewCount AS decimal(2, 1))
	FROM @tbProductView

	SELECT P.Id AS ProductId,
		P.ModifiedDate AS LastModifiedDate,
		CASE CHARINDEX('/', Url) WHEN 1 
			THEN SUBSTRING(Url, 2, LEN(Url) - 1) 
			ELSE Url 
		END AS Url,
		CASE WHEN PP.Priority IS NULL THEN 0.5 
			WHEN PP.Priority < 0.5 THEN 0.5
			ELSE PP.Priority
		END AS Priority,
		CASE WHEN P.ModifiedDate > DATEADD(DD, -1, GETDATE()) THEN 'Daily'  
			WHEN P.ModifiedDate > DATEADD(DD, -7, GETDATE()) THEN 'Weekly' 
			WHEN P.ModifiedDate > DATEADD(MM, -1, GETDATE()) THEN 'Monthly' 
			ELSE 'Weekly'
		END AS ChangeFrequency
	FROM @tbProducts P
		LEFT JOIN @tbProductPriority PP ON P.Id = PP.Id
END
GO
IF COL_LENGTH('[dbo].[BLD_Sku]', 'UserDefinedPrice') IS NULL
BEGIN
	ALTER TABLE [dbo].[BLD_Sku] Add [UserDefinedPrice] BIT NULL
END
GO

IF COL_LENGTH('[dbo].[BLD_Sku]', 'FreeShipping') IS NULL
BEGIN
	ALTER TABLE [dbo].[BLD_Sku] Add [FreeShipping] BIT NULL
END
GO

GO
IF(OBJECT_ID('BLD_ExportSKUs') IS NOT NULL)
	DROP PROCEDURE BLD_ExportSKUs
GO

CREATE PROCEDURE [dbo].[BLD_ExportSKUs]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		BEGIN
			select
				 p.ProductIDUser as ProductId
				,s.Title as SkuTitle
				,s.SKU 
				,s.IsOnline 
				,s.IsActive
				,s.ListPrice
				,s.UnitCost
				,coalesce(s.Measure, 'each') as UnitOfMeasure
				,coalesce(s.OrderMinimum, 0) as OrderQuantityMin
				,coalesce(s.OrderIncrement, 0) as OrderQuantityIncrement
				,s.HandlingCharges as HandlingCharge
				,coalesce(s.Length, 0) as Length
				,coalesce(s.Width, 0) as Length
				,coalesce(s.Height, 0) as Length
				,coalesce(s.Weight, 0) as Length
				,coalesce(s.SelfShippable, 0) as SelfShippable
				,s.AvailableForBackOrder
				,s.BackOrderLimit
				,s.IsUnlimitedQuantity
				,0 as UpdateFlag
				,s.UserDefinedPrice
				,s.FreeShipping
			from dbo.PRProductSKU s
			join dbo.PRProduct p on (s.ProductId = p.Id)
		END
	ELSE
		BEGIN
			select 
				 p.ProductIDUser as ProductId
				,ISNULL(prcv.Title,p.Title) as SkuTitle
				,prs.SKU as SKU
				,prcv.IsOnline as IsOnline
				,ISNULL(prcv.IsActive,p.IsActive) as IsActive
				,prcv.ListPrice as ListPrice
				,prcv.UnitCost as UnitCost
				,coalesce(prcv.Measure, 'each') as UnitOfMeasure
				,coalesce(prcv.OrderMinimum, 0) as OrderQuantityMin
				,coalesce(prcv.OrderIncrement, 0) as OrderQuantityIncrement
				,prcv.HandlingCharges as HandlingCharges
				,coalesce(prs.Length, 0) as Length
				,coalesce(prs.Width, 0) as Length
				,coalesce(prs.Height, 0) as Length
				,coalesce(prs.Weight, 0) as Length
				,coalesce(prcv.SelfShippable, 0) as SelfShippable
				,prcv.AvailableForBackOrder as AvailableForBackOrder
				,prcv.BackOrderLimit as BackOrderLimit
				,prcv.IsUnlimitedQuantity as IsUnlimitedQuantity
				,0 as UpdateFlag
				,prcv.UserDefinedPrice
				,prcv.FreeShipping
			from dbo.PRProductSKUVariantCache prcv
			left join PRProductSKU prs on (prcv.Id = prs.Id)
			left join dbo.PRProduct p on (prs.ProductId = p.Id)
			where prcv.SiteId = @siteId
		END
END


GO
IF(OBJECT_ID('BLD_ProductImport_ProductVariant') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_ProductVariant
GO

CREATE PROCEDURE [dbo].[BLD_ProductImport_ProductVariant]
	@SiteId				uniqueidentifier,
	@ProductIDUser		nvarchar(100),
	@ProductTypeName	nvarchar(50),
	@ProductTypeId		uniqueidentifier,
	@Title				nvarchar(555),
	@ShortDescription	nvarchar(4000),
	@Description	nvarchar(4000),
	@LongDescription	nvarchar(4000),
	@IsActive			bit,
	@SEOTitle			nvarchar(255),
	@SEOH1				nvarchar(255),
	@SEODescription		nvarchar(4000),
	@SEOKeywords		nvarchar(4000),
	@SEOFriendlyURL		nvarchar(4000),
	@URLFriendlyTitle	nvarchar(255),
	@ModifiedBy			uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	declare @ActionType				int					= 0
	declare @MasterSiteId			uniqueidentifier
	declare @MasterProductId		uniqueidentifier
	declare @MasterProductTypeId	uniqueidentifier
	declare @MasterTitle			nvarchar(555)
	declare @MasterShortDescription nvarchar(4000)
	declare @MasterDescription nvarchar(4000)
	declare @MasterLongDescription	nvarchar(4000)
	declare @MasterIsActive			bit
	declare @MasterSEOTitle			nvarchar(255)
	declare @MasterSEOH1			nvarchar(255)
	declare @MasterSEODescription	nvarchar(4000)
	declare @MasterSEOKeywords		nvarchar(4000)
	declare @MasterSEOFriendlyURL	nvarchar(255)
	declare @MasterURLFriendlyTitle	nvarchar(255)
	declare @IsSkipped				bit = 1
	declare @Output					nvarchar(512)

	if coalesce(cast(@SiteId as sql_variant), @ProductIdUser, @ProductTypeId) is null
		raiserror(15600, -1, -1, 'SiteId, ProductIdUser, or ProductTypeId is null')

	BEGIN TRY
        BEGIN TRAN
		
			select @MasterSiteId=Id from SISite where Id=MasterSiteId AND Status =1

		select 
			@MasterProductId=p.Id, 
			@MasterProductTypeId=p.ProductTypeID,
			@MasterTitle=iif(v.Title is null, p.Title, null),
			@MasterShortDescription=iif(v.ShortDescription is null, p.ShortDescription, null),
			@MasterDescription=iif(v.Description is null, p.Description, null),
			@MasterLongDescription=iif(v.LongDescription is null, p.LongDescription, null),
			@MasterIsActive=iif(v.IsActive is null, p.IsActive, null),
			@MasterSEOTitle=iif(v.SEOTitle is null, p.SEOTitle, null),
			@MasterSEOH1=iif(v.SEOH1 is null, p.SEOH1, null),
			@MasterSEODescription=iif(v.SEODescription is null, p.SEODescription, null),
			@MasterSEOKeywords=iif(v.SEOKeywords is null, p.SEOKeywords, null),
			@MasterSEOFriendlyURL=iif(v.SEOFriendlyUrl is null, p.SEOFriendlyUrl, null),
			@MasterURLFriendlyTitle=iif(v.UrlFriendlyTitle is null, p.UrlFriendlyTitle, null)
		from PRProduct p
			left join PRProductVariant v on p.Id=v.Id and v.SiteId=@SiteId
		where p.ProductIDUser like @ProductIDUser and p.ProductTypeID like @ProductTypeId and p.SiteId=@MasterSiteId

		if @MasterProductId is null or @MasterProductTypeId is null
		begin
			set @Output = concat('Product Id: [', @ProductIDUser, ']/[', @ProductTypeId, '] does not exist.')
			raiserror(15600, -1, -1, @Output)
		end
		else
		begin
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@ShortDescription = case when @MasterShortDescription is null or @ShortDescription <> @MasterShortDescription and @ShortDescription is not null then @ShortDescription else null end,
				@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end,
				@LongDescription = case when @MasterLongDescription is null or @LongDescription <> @MasterLongDescription and @LongDescription is not null then @LongDescription else null end,
				@IsActive = case when @MasterIsActive is null or @IsActive <> @MasterIsActive and @IsActive is not null then @IsActive else null end,
				@SEOTitle = case when @MasterSEOTitle is null or @SEOTitle <> @MasterSEOTitle and @SEOTitle is not null then @SEOTitle else null end,
				@SEOH1 = case when @MasterSEOH1 is null or @SEOH1 <> @MasterSEOH1 and @SEOH1 is not null then @SEOH1 else null end,
				@SEODescription = case when @MasterSEODescription is null or @SEODescription <> @MasterSEODescription and @SEODescription is not null then @SEODescription else null end,
				@SEOKeywords = case when @MasterSEOKeywords is null or @SEOKeywords <> @MasterSEOKeywords and @SEOKeywords is not null then @SEOKeywords else null end,
				@SEOFriendlyURL = case when @MasterSEOFriendlyURL is null or @SEOFriendlyURL <> @MasterSEOFriendlyURL and @SEOFriendlyURL is not null then @SEOFriendlyURL else null end,
				@URLFriendlyTitle = case when @MasterURLFriendlyTitle is null or @URLFriendlyTitle <> @MasterURLFriendlyTitle and @URLFriendlyTitle is not null then @URLFriendlyTitle else null end

			if exists (select 1 from PRProductVariant where Id=@MasterProductId and SiteId=@SiteId)
			begin
				update PRProductVariant set
					Title=@Title,
					ShortDescription=@ShortDescription,
					LongDescription=@LongDescription,
					Description=@Description,
					IsActive=@IsActive,
					SEOTitle=@SEOTitle,
					SEOH1=@SEOH1,
					SEODescription=@SEODescription,
					SEOKeywords=@SEOKeywords,
					SEOFriendlyURL=@SEOFriendlyURL,
					UrlFriendlyTitle=@URLFriendlyTitle,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getutcdate()
				where Id=@MasterProductId and SiteId=@SiteId

				set @Output = concat('updated PRProductVariant id=', @MasterProductId)
				set @actionType = 2
			end
			else
			begin
				if coalesce(cast(@Title as sql_variant), @ShortDescription, @LongDescription, @IsActive, @SEOTitle, @SEODescription, @SEOKeywords, @SEOFriendlyURL) is not null
				begin
					insert into PRProductVariant (SiteId, Id, ProductTypeId, Title, ShortDescription, LongDescription, Description, IsActive, SEOTitle, SEOH1, SEODescription, SEOKeywords, SEOFriendlyURL, URLFriendlyTitle, ModifiedBy, ModifiedDate)
						values (@SiteId, @MasterProductId, @MasterProductTypeId, @Title, @ShortDescription, @LongDescription, @Description, @IsActive, @SEOTitle, @SEOH1, @SEODescription, @SEOKeywords, @SEOFriendlyURL, @URLFriendlyTitle, @Modifiedby, getutcdate())
					set @IsSkipped = 0
				end

				set @Output = concat(
					'PRProductVariant ', 
					iif(
						@IsSkipped=1, 
						'skipped', 
						concat('inserted Name=', @Title, ' | product type=', @ProductTypeName)))
				set @actionType = 2
			end

		end
		COMMIT TRAN
		select @MasterProductid, @ActionType, @Output
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		declare @ErrorState int = ERROR_STATE()
		declare @ErrorSeverity int = ERROR_SEVERITY()
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
    END CATCH

END
GO


GO
IF(OBJECT_ID('BLD_ProductImport_ProductSkuVariant') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_ProductSkuVariant
GO

CREATE procedure [dbo].[BLD_ProductImport_ProductSkuVariant]
(
	@SiteId						uniqueidentifier,
	@ProductId					nvarchar(100),
	@Sku						nvarchar(255),
	@Title						nvarchar(555),
	--@Description				nvarchar(max),
	--@Sequence					int,
	@IsActive					bit,
	@IsOnline					bit,
	@ListPrice					money,
	@UnitCost					money,
	--@WholesalePrice				money,
	@Measure					nvarchar(50),
	@OrderMinimum				decimal(18,4),
	@OrderIncrement				decimal(18,4),
	@HandlingCharges			decimal(18,4),
	@SelfShippable				bit,
	@AvailableForBackOrder		bit,
	@BackOrderLimit				decimal(18,4),
	@IsUnlimitedQuantity		bit,
	--@MaximumDiscountPercentage	money,
	@FreeShipping				bit,
	@UserDefinedPrice			bit,
	@ModifiedBy					uniqueidentifier
)
as
begin
	begin try

		declare @ProductSkuVariantId				uniqueidentifier
		declare @MasterProductSkuId					uniqueidentifier
		declare @MasterTitle						nvarchar(555)
		declare @MasterSequence						int
		declare @MasterIsActive						bit
		declare @MasterIsOnline						bit
		declare @MasterListPrice					money
		declare @MasterUnitCost						money
		declare @MasterWholesalePrice				money
		declare @MasterMeasure						nvarchar(50)
		declare @MasterOrderMinimum					decimal(18,4)
		declare @MasterOrderIncrement				decimal(18,4)
		declare @MasterHandlingCharges				decimal(18,4)
		declare @MasterSelfShippable				bit
		declare @MasterAvailableForBackOrder		bit
		declare @MasterBackOrderLimit				decimal(18,4)
		declare @MasterIsUnlimitedQuantity			bit
		declare @MasterMaximumDiscountPercentage	money
		declare @MasterFreeShipping					bit
		declare @MasterUserDefinedPrice				bit
		declare @Output								nvarchar(512) = ''
		declare @IsSkipped							bit = 1

		if (@SiteId is null or @ProductId is null or @ModifiedBy is null)
			raiserror(15600, -1, -1, 'BLD_ProductImport_ProductSkuVariant SiteId, ModifiedBy, or ProductId is null')

		begin tran
		select 
			@MasterProductSkuId=ps.Id,
			@MasterTitle=iif(v.Title is null, ps.Title, null),
			@MasterSequence=ps.Sequence,
			@MasterIsActive=iif(v.IsActive is null, ps.IsActive, null),
			@MasterIsOnline=iif(v.IsOnline is null, ps.IsOnline, null),
			@MasterListPrice=iif(v.ListPrice is null, ps.ListPrice, null),
			@MasterUnitCost=iif(v.UnitCost is null, ps.UnitCost, null),
			@MasterWholesalePrice=ps.WholesalePrice,
			@MasterMeasure=iif(v.Measure is null, ps.Measure, null),
			@MasterOrderMinimum=iif(v.OrderMinimum is null, ps.OrderMinimum, null),
			@MasterOrderIncrement=iif(v.OrderIncrement is null, ps.OrderIncrement, null),
			@MasterHandlingCharges=iif(v.HandlingCharges is null, ps.HandlingCharges, null),
			@MasterSelfShippable=iif(v.SelfShippable is null, ps.SelfShippable, null),
			@MasterAvailableForBackOrder=iif(v.AvailableForBackOrder is null, ps.AvailableForBackOrder, null),
			@MasterBackOrderLimit=iif(v.BackOrderLimit is null, ps.BackOrderLimit, null),
			@MasterIsUnlimitedQuantity=iif(v.IsUnlimitedQuantity is null, ps.IsUnlimitedQuantity, null),
			@MasterMaximumDiscountPercentage=ps.MaximumDiscountPercentage,
			@MasterFreeShipping=ps.FreeShipping,
			@MasterUserDefinedPrice=ps.UserDefinedPrice
		from PRProductSku ps
			left join PRProductSKUVariant v on ps.Id=v.Id and v.SiteId=@SiteId
			join PRProduct p on ps.ProductId=p.Id
			join SISite s on ps.SiteId=s.Id
		where p.ProductIDUser=@ProductId and ps.SKU=@Sku and s.Id=s.MasterSiteId

		if @MasterProductSkuId is null
		begin
			set @Output = concat('Product Id [', @ProductId, '] with SKU [', @Sku, 'does not exist on master site')
		end 
		else
		begin
			select @ProductSkuVariantId=Id from PRProductSKUVariant where Id=@MasterProductSkuId and SiteId=@SiteId
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@IsActive = case when @MasterIsActive is null or @IsActive <> @MasterIsActive and @IsActive is not null then @IsActive else null end,
				@IsOnline = case when @MasterIsOnline is null or @IsOnline <> @MasterIsOnline and @IsOnline is not null then @IsOnline else null end,
				@ListPrice = case when @MasterListPrice is null or @ListPrice <> @MasterListPrice and @ListPrice is not null then @ListPrice else null end,
				@UnitCost = case when @MasterUnitCost is null or @UnitCost <> @MasterUnitCost and @UnitCost is not null then @UnitCost else null end,
				@Measure = case when @MasterMeasure is null or @Measure <> @MasterMeasure and @Measure is not null then @Measure else null end,
				@OrderMinimum = case when @MasterOrderMinimum is null or @OrderMinimum <> @MasterOrderMinimum and @OrderMinimum is not null then @OrderMinimum else null end,
				@OrderIncrement = case when @MasterOrderIncrement is null or @OrderIncrement <> @MasterOrderIncrement and @OrderIncrement is not null then @OrderIncrement else null end,
				@HandlingCharges = case when @MasterHandlingCharges is null or @HandlingCharges <> @MasterHandlingCharges and @HandlingCharges is not null then @HandlingCharges else null end,
				@SelfShippable = case when @MasterSelfShippable is null or @SelfShippable <> @MasterSelfShippable and @SelfShippable is not null then @SelfShippable else null end,
				@AvailableForBackOrder = case when @MasterAvailableForBackOrder is null or @AvailableForBackOrder <> @MasterAvailableForBackOrder and @AvailableForBackOrder is not null then @AvailableForBackOrder else null end,
				@BackOrderLimit = case when @MasterBackOrderLimit is null or @BackOrderLimit <> @MasterBackOrderLimit and @BackOrderLimit is not null then @BackOrderLimit else null end,
				@IsUnlimitedQuantity = case when @MasterIsUnlimitedQuantity is null or @IsUnlimitedQuantity <> @MasterIsUnlimitedQuantity and @IsUnlimitedQuantity is not null then @IsUnlimitedQuantity else null end,
				@UserDefinedPrice = case when @MasterUserDefinedPrice is null or @UserDefinedPrice <> @MasterUserDefinedPrice and @UserDefinedPrice is not null then @UserDefinedPrice else null end,
				@FreeShipping = case when @MasterFreeShipping is null or @FreeShipping <> @MasterFreeShipping and @FreeShipping is not null then @FreeShipping else null end

			if @ProductSkuVariantId is not null
			begin
				update PRProductSKUVariant set
					Title=@Title,
					IsActive=@IsActive,
					IsOnline=@IsOnline,
					ListPrice=@ListPrice,
					UnitCost=@UnitCost,
					Measure=@Measure,
					OrderMinimum=@OrderMinimum,
					OrderIncrement=@OrderIncrement,
					HandlingCharges=@HandlingCharges,
					SelfShippable=@SelfShippable,
					AvailableForBackOrder=@AvailableForBackOrder,
					BackOrderLimit=@BackOrderLimit,
					IsUnlimitedQuantity=@IsUnlimitedQuantity,
					UserDefinedPrice = @UserDefinedPrice,
					FreeShipping=@FreeShipping,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getutcdate()
				where Id=@ProductSkuVariantId and SiteId=@SiteId

				set @Output = concat('updated PRProductSkuVariant Id [', @ProductSkuVariantId, ']')
			end
			else
			begin
				if coalesce(cast(@Title as sql_variant), @IsActive, @IsOnline, @ListPrice, @UnitCost, @Measure, @OrderMinimum, @OrderIncrement, @HandlingCharges, @SelfShippable, @AvailableForBackOrder, @BackOrderLimit, @IsUnlimitedQuantity, @UserDefinedPrice, @FreeShipping) is not null
				begin
					insert into PRProductSKUVariant (SiteId, Id, Title, IsActive, IsOnline, ListPrice, UnitCost, Measure, OrderMinimum, OrderIncrement, HandlingCharges, SelfShippable, AvailableForBackOrder, BackOrderLimit, IsUnlimitedQuantity, UserDefinedPrice, FreeShipping, ModifiedBy, ModifiedDate)
						values (@SiteId, @MasterProductSkuId, @Title, @IsActive, @IsOnline, @ListPrice, @UnitCost, @Measure, @OrderMinimum, @OrderIncrement, @HandlingCharges, @SelfShippable, @AvailableForBackOrder, @BackOrderLimit, @IsUnlimitedQuantity, @UserDefinedPrice, @FreeShipping, @ModifiedBy, getutcdate())
					set @IsSkipped = 0
				end

				set @Output = concat(
					'PRProductSkuVariant ', 
					iif(@IsSkipped=1, 'skipped', concat('inserted Title=', @Title, ' | sku=', @Sku)))
			end
			
		end
		commit tran
		select @Output
	end try
	begin catch
		rollback tran
		declare @ErrorMessage nvarchar(4000) = error_message(),
				@ErrorSeverity int = error_severity(),
				@ErrorState int = error_state()
		print concat('BLD_ProductImport_ProductSkuVariant error: severity= ', @ErrorSeverity, ' state=', @ErrorState, ' message=', @ErrorMessage)
		raiserror(@ErrorMessage, @ErrorSeverity, @ErrorState)
	end catch
end
GO
IF (OBJECT_ID('vwFullyRefundedOrders') IS NOT NULL)
	DROP View vwFullyRefundedOrders	
GO

CREATE VIEW vwFullyRefundedOrders
AS
SELECT O.Id,O.PurchaseOrderNumber,TransactionDate,O.GrandTotal,O.SiteId
FROM OROrder O
INNER JOIN (select OrderId, sum(OP.Amount) *-1 Amount,max(isnull(ClearDate,ModifiedDate)) TransactionDate
from PTPayment P
INNER JOIN PTOrderPayment OP ON P.Id =OP.PaymentId
Where PaymentStatusId=12 
Group By OP.OrderId
) T ON T.OrderId=O.Id AND T.Amount=O.GrandTotal


GO

GO
IF (OBJECT_ID('vwPartiallyRefundedOrders') IS NOT NULL)
	DROP View vwPartiallyRefundedOrders	
GO
CREATE VIEW vwPartiallyRefundedOrders
AS
select O.Id, O.PurchaseOrderNumber,O.GrandTotal,S.SKU,oi.Price,RI.Quantity,RI.Amount,isNull(P.ClearDate,P.ModifiedDate) TransactionDate,O.SiteId
from ORRefundItem RI
INNER JOIN OROrderItem OI ON RI.OrderItemId = OI.Id
INNER JOIN PRProductSKU S on S.Id = OI.ProductSKUId
INNER JOIN OROrder O ON O.Id =OI.OrderId
INNER JOIN PTOrderPayment OP ON O.Id =OP.OrderId 
INNER JOIN PTPayment P ON OP.PaymentId = P.Id AND P.PaymentStatusId=12
Where O.Id NOT IN (select Id from vwFullyRefundedOrders)

GO

IF(OBJECT_ID('CommerceReport_GetFullyRefundedOrders') IS NOT NULL)
	DROP PROCEDURE CommerceReport_GetFullyRefundedOrders
GO
CREATE PROCEDURE [dbo].[CommerceReport_GetFullyRefundedOrders]  
(   
 @startDate datetime,@endDate datetime, 
 @ApplicationId uniqueidentifier=null ,
 @IncludeChildrenSites Bit = 0 
)   
AS  
select 
PurchaseOrderNumber 'ga:transactionId'
	,NULL 'ga:productSku'
	,NULL 'ga:productPrice'
	,NULL 'ga:quantityRefunded'
	,NULL 'ga:transactionRevenue'
from vwFullyRefundedOrders
	WHERE  
		TransactionDate >= @startDate
		AND TransactionDate < @endDate
		AND (
			(@IncludeChildrenSites = 0 AND SiteId = @ApplicationId )
			OR
			(@IncludeChildrenSites = 1 AND SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites( @ApplicationId,1)))
		) 
Order By TransactionDate		   

GO

IF(OBJECT_ID('CommerceReport_GetPartiallyRefundedOrders') IS NOT NULL)
	DROP PROCEDURE CommerceReport_GetPartiallyRefundedOrders
GO

CREATE PROCEDURE [dbo].[CommerceReport_GetPartiallyRefundedOrders]  
(   
 @startDate datetime,@endDate datetime, 
 @ApplicationId uniqueidentifier=null ,
 @IncludeChildrenSites Bit = 0 
)   
AS  
select PurchaseOrderNumber 'ga:transactionId'
	,SKU 'ga:productSku'
	,Price 'ga:productPrice'
	,Cast(Quantity as int) 'ga:quantityRefunded'
	,Amount 'ga:transactionRevenue'
	from vwPartiallyRefundedOrders
	WHERE  
		TransactionDate >= @startDate
		AND TransactionDate < @endDate
		AND (
			(@IncludeChildrenSites = 0 AND SiteId = @ApplicationId )
			OR
			(@IncludeChildrenSites = 1 AND SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites( @ApplicationId,1)))
		) 
Order By TransactionDate	   

GO


IF(OBJECT_ID('CommerceReport_RefundedOrders') IS NOT NULL)
	DROP PROCEDURE CommerceReport_RefundedOrders
GO

CREATE PROCEDURE [dbo].[CommerceReport_RefundedOrders]  
(   
 @startDate datetime,@endDate datetime, @maxReturnCount int,  
 @pageSize int = 10,@pageNumber int,   
 @sortBy nvarchar(50) = null,  
 @virtualCount int output,  
 @ApplicationId uniqueidentifier=null ,
 @IncludeChildrenSites Bit = 0 
)   
AS  
  
BEGIN  
	if @maxReturnCount = 0  
		set @maxReturnCount = null  
	  
	declare @tmpOrders table(  
		PurchaseOrderNumber nvarchar(100),  
		OrderTotal decimal(18,2) ,
		RefundTotal decimal(18,2),
		TransactionDate datetime,
		IsFullRefund bit,
		SiteId uniqueidentifier
	)  
	
	SET @startDate =dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
	SET @endDate = dbo.ConvertTimeToUtc(@endDate,@ApplicationId)

	INSERT INTO @tmpOrders  
	
	select PurchaseOrderNumber,GrandTotal,GrandTotal RefundTotal,TransactionDate,1 IsFullRefund,SiteId from vwFullyRefundedOrders
	union
	select * from 
	(
	select PurchaseOrderNumber,GrandTotal,Amount RefundTotal,TransactionDate,0 IsFullRefund,SiteId from vwPartiallyRefundedOrders
	--Group By PurchaseOrderNumber
	) T
	WHERE  
		T.TransactionDate >= @startDate
		AND T.TransactionDate < @endDate
		AND (
			(@IncludeChildrenSites = 0 AND T.SiteId = @ApplicationId )
			OR
			(@IncludeChildrenSites = 1 AND T.SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites( @ApplicationId,1)))
		) 
		   
  
   Set @virtualCount = @@ROWCOUNT  
	
  if @sortBy is null or (@sortBy) = 'TransactionDate desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by TransactionDate desc) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.TransactionDate DESC  
   end  
  else if (@sortBy) = 'TransactionDate asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by TransactionDate ASC) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.TransactionDate ASC  
   end  
  else if (@sortBy) = 'PurchaseOrderNumber asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by PurchaseOrderNumber asc) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.PurchaseOrderNumber ASC  
   end  
  else if (@sortBy) = 'PurchaseOrderNumber desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by PurchaseOrderNumber DESC) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.PurchaseOrderNumber DESC  
   end  
  else if (@sortBy) = 'OrderTotal asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by OrderTotal asc) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.OrderTotal ASC  
   end  
  else if (@sortBy) = 'OrderTotal desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by OrderTotal DESC) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.OrderTotal DESC  
   end  
  else if (@sortBy) = 'RefundTotal asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by RefundTotal asc) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.RefundTotal ASC  
   end  
  else if (@sortBy) = 'RefundTotal desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by RefundTotal DESC) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.RefundTotal DESC  
   end  
   else if (@sortBy) = 'IsFullRefund asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by IsFullRefund asc) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.IsFullRefund ASC  
   end  
  else if (@sortBy) = 'IsFullRefund desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by IsFullRefund DESC) as RowNumber  
	  from @tmpOrders  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.IsFullRefund DESC  
   end  
  
END

GO

DECLARE @Sequence int, @settingTypeId int
SELECT @Sequence = MAX(sequence) + 1 FROM dbo.STSettingType

IF NOT EXISTS(Select 1 from STSettingType Where Name ='SalesForceEnableLeadSync')
BEGIN
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, IsEnabled, ControlType, IsVisible)
	VALUES('SalesForceEnableLeadSync', 'SalesForceEnableLeadSync', 1, @Sequence, 1, 'Checkbox', 1)

	Update STSettingType SET SettingGroupId = (SELECT TOP 1 Id FROM STSettingGroup WHERE Name = 'General')
	WHERE Name = 'SalesForceEnableLeadSync'
END

GO


IF COL_LENGTH('[dbo].[PageMapNode]', 'CalculatedUrl') IS NULL
BEGIN
	ALTER TABLE [dbo].[PageMapNode] Add [CalculatedUrl] AS ([dbo].[GetPageMapNodeUrl]([PageMapNodeId]))
END

GO
IF(OBJECT_ID('CampaignSendDto_Validate') IS NOT NULL)
	DROP PROCEDURE CampaignSendDto_Validate
GO

 CREATE PROCEDURE [dbo].[CampaignSendDto_Validate]
(
	@CampaignId		uniqueidentifier,
	@SiteId			uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @CampaignSendId uniqueidentifier, @ScheduleId uniqueidentifier, @ScheduleDate date
	SET @UtcNow = GETUTCDATE()

	SELECT TOP 1 @CampaignSendId = Id,
		@ScheduleId = ScheduleId
	FROM MKCampaignSend WHERE CampaignId = @CampaignId AND SiteId = @SiteId

	DECLARE @tbSites TABLE (Id uniqueidentifier)
	INSERT INTO @tbSites
	SELECT SiteId FROM vwCampaignSendTarget WHERE CampaignSendId = @CampaignSendId

	DECLARE @tbResult TABLE (SiteId uniqueidentifier, Reason int, SourceId uniqueidentifier)

	--ScheduledSites
	INSERT INTO @tbResult
	SELECT ST.SiteId, 1, S.SiteId FROM MKCampaignSend S
		JOIN vwCampaignSendTarget ST ON S.Id = ST.CampaignSendId
	WHERE (S.Status = 2 OR S.Status = 5)
		AND S.Id != @CampaignSendId AND S.CampaignId = @CampaignId
		AND EXISTS (SELECT 1 FROM @tbSites T WHERE T.Id = ST.SiteId)

	SELECT TOP 1 @ScheduleDate = dbo.ConvertTimeFromUtc(NextRunTime, ApplicationId) FROM TASchedule WHERE Id = @ScheduleId

	IF @ScheduleDate IS NOT NULL
	BEGIN
		--SendLimit

		;WITH RunCTE AS
		(
			SELECT R.Id, R.SiteId FROM MKCampaignRunHistory R
			WHERE EXISTS (SELECT 1 FROM @tbSites T WHERE T.Id = R.SiteId) 
				AND Status != 3 AND MONTH(RunDate) = MONTH(@ScheduleDate) AND YEAR(RunDate) = YEAR(@ScheduleDate)
		),
		RateCTE AS
		(
			SELECT SiteId, Count(Id) AS SendCount FROM RunCTE
			GROUP BY SiteId
		),
		CTE AS
		(
			SELECT SiteId, SendCount, [dbo].[CampaignSetting_Get](SiteId) AS CampaignSettingId FROM RateCTE
		)

		INSERT INTO @tbResult
		SELECT C.SiteId, 2, C.SiteId FROM CTE C
			JOIN MKCampaignSetting S ON C.CampaignSettingId = S.Id
		WHERE C.SendCount >= S.SendLimitPerMonth AND S.SendLimitPerMonth > 0

		--BlackOut dates

		INSERT INTO @tbResult
		SELECT S.Id, 3, T.Id FROM vwCampaignBlackoutTarget T
			JOIN @tbSites S ON T.SiteId = S.Id
			JOIN MKCampaignBlackout B ON B.Id = T.CampaignBlackoutId
		WHERE @ScheduleDate BETWEEN B.StartDate AND B.EndDate
	END
	
	SELECT @CampaignSendId AS CampaignSendId, T.* FROM @tbResult T

END
GO

IF(COL_LENGTH('PageMapNode', 'MasterPageMapNodeId') IS NULL)
	ALTER TABLE PageMapNode ADD MasterPageMapNodeId uniqueidentifier
GO

IF(COL_LENGTH('PageDefinition', 'MasterPageDefinitionId') IS NULL)
	ALTER TABLE PageDefinition ADD MasterPageDefinitionId uniqueidentifier
GO

UPDATE P SET P.MasterPageMapNodeId = ISNULL(S.SourcePageMapNodeId, P.PageMapNodeId) 
FROM PageMapNode P
	LEFT JOIN vwMasterSourceMenuId S ON S.PageMapNodeId = P.PageMapNodeId
WHERE P.MasterPageMapNodeId IS NULL
GO

UPDATE P SET P.MasterPageDefinitionId = ISNULL(S.SourcePageDefinitionId, P.PageDefinitionId)
FROM PageDefinition P
	LEFT JOIN vwMasterSourcePageId S ON S.PageDefinitionId = P.PageDefinitionId
WHERE P.MasterPageDefinitionId IS NULL
GO

IF(OBJECT_ID('vwEventLog') IS NOT NULL)
	DROP VIEW vwEventLog
GO	
CREATE VIEW [dbo].[vwEventLog]
AS 
SELECT DISTINCT L.Id,
	L.Priority,
	L.Severity,
	L.CreatedDate AS LogDate,
	L.MachineName,
	L.ActivityId,
	L.SiteId,
	Cast(L.Message as NVARCHAR(MAX)) AS Message,
	Cast(E.Stacktrace as NVARCHAR(MAX)) AS StackTrace,
	Cast(E.InnerException as VARCHAR(MAX)) AS InnerException,
	Cast(L.AdditionalInfo as NVARCHAR(MAX)) AS AdditionalInfo	
FROM LGLog L
	LEFT JOIN LGExceptionLog E ON L.Id = E.LogId

GO

IF(OBJECT_ID('vwObjectUrls_Menus') IS NOT NULL)
	DROP VIEW vwObjectUrls_Menus
GO	
CREATE VIEW [dbo].[vwObjectUrls_Menus] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS
SELECT	PMN.SiteId, 2, PMN.PageMapNodeId, PMN.MasterPageMapNodeId,
		LOWER(PMN.CompleteFriendlyUrl)
FROM	dbo.PageMapNode AS PMN
WHERE NULLIF(PMN.CompleteFriendlyUrl, '') IS NOT NULL
GO

IF(OBJECT_ID('vwObjectUrls_Pages') IS NOT NULL)
	DROP VIEW vwObjectUrls_Pages
GO	
CREATE VIEW [dbo].[vwObjectUrls_Pages] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS

SELECT	PD.SiteId, 8, PD.PageDefinitionId, PD.MasterPageDefinitionId,
		LOWER(PMN.CompleteFriendlyUrl + '/' + PD.FriendlyName)
FROM	dbo.PageDefinition AS PD
		INNER JOIN dbo.PageMapNodePageDef AS PMNPD ON PMNPD.PageDefinitionId = PD.PageDefinitionId
		INNER JOIN dbo.PageMapNode AS PMN ON PMN.PageMapNodeId = PMNPD.PageMapNodeId
GO
IF(OBJECT_ID('PageMapBase_UpdateLastModification') IS NOT NULL)
	DROP PROCEDURE  PageMapBase_UpdateLastModification
GO	
CREATE PROCEDURE [dbo].[PageMapBase_UpdateLastModification]
(
	@SiteId			uniqueidentifier = null
)
AS
BEGIN
	DECLARE @ModifiedDate datetime
	SET @ModifiedDate =  DATEADD(SECOND, DATEDIFF(SECOND, 39000, GETUTCDATE()), 39000)

	UPDATE PageMapNode SET CompleteFriendlyUrl = CalculatedUrl
	WHERE @SiteId IS NULL OR SiteId = @SiteId

	IF EXISTS (SELECT 1 FROM PageMapNode WHERE MasterPageMapNodeId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageMapNodeId = S.SourcePageMapNodeId 
		FROM PageMapNode P
			JOIN vwMasterSourceMenuId S ON S.PageMapNodeId = P.PageMapNodeId
		WHERE P.MasterPageMapNodeId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageMapNode SET MasterPageMapNodeId = PageMapNodeId 
		WHERE MasterPageMapNodeId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	IF EXISTS (SELECT 1 FROM PageDefinition WHERE MasterPageDefinitionId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageDefinitionId = S.SourcePageDefinitionId 
		FROM PageDefinition P
			JOIN vwMasterSourcePageId S ON S.PageDefinitionId = P.PageDefinitionId
		WHERE P.MasterPageDefinitionId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageDefinition SET MasterPageDefinitionId = PageDefinitionId 
		WHERE MasterPageDefinitionId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	UPDATE PageMapBase SET LastPageMapModificationDate = @ModifiedDate
	WHERE @SiteId IS NULL OR SiteId = @SiteId
END
GO
PRINT 'Creating vwRss'
GO
IF(OBJECT_ID('vwRss') IS NOT NULL)
	DROP VIEW vwRss
GO	
CREATE VIEW [dbo].[vwRss]
AS
SELECT F.Id,
	F.ApplicationId AS SiteId,
	C.Title,
	C.Description,
	C.Status,
	F.FileName,
	C.ChannelType,
	C.CreatedDate,
	C.CreatedBy,
	C.ModifiedDate,
	C.ModifiedBy,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM SYFWFeed F
	INNER JOIN SYFWFeedRssChannel FC ON  FC.FWFeedId = F.[Id]
	INNER JOIN SYRssChannel C ON FC.RssChannelId = C.Id
	LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
WHERE F.Status != 3
GO
IF (OBJECT_ID('CartItem_Delete') IS NOT NULL)
	DROP PROCEDURE CartItem_Delete
GO
CREATE PROCEDURE [dbo].[CartItem_Delete]
(
	@Id		uniqueidentifier,
	@CartId uniqueidentifier
)
AS
BEGIN
	DECLARE @CurrentDate datetime
	SET @CurrentDate = GETUTCDATE()

	IF(@Id IS NULL OR  @Id = dbo.GetEmptyGUID())
	BEGIN
		DELETE FROM CSCartItem WHERE CartId = @CartId

		DELETE FROM OROrderItemAttributeValue WHERE CartId = @CartId
	END
	ELSE
	BEGIN
		DECLARE @ThisItemSequence int, @MaxSequence int

		SELECT @ThisItemSequence = Sequence FROM CSCartItem WHERE Id = @Id
		SELECT @MaxSequence = ISNULL(MAX(Sequence), 1) FROM CSCartItem WHERE CartId = @CartId
	
		DELETE FROM CSCartItem WHERE CartId=@CartId AND ParentCartItemId = @Id
		DELETE FROM CSCartItem WHERE Id = @Id

		DELETE FROM OROrderItemAttributeValue WHERE CartItemId = @Id
		
		IF @ThisItemSequence < @MaxSequence
		BEGIN	
			UPDATE CSCartItem SET Sequence = Sequence - 1 
			WHERE Sequence > @ThisItemSequence and CartId = @CartId			
		END
	END
		
	UPDATE CSCart Set ModifiedDate = @CurrentDate WHERE Id = @CartId
END
GO
IF (OBJECT_ID('CartItem_Save') IS NOT NULL)
	DROP PROCEDURE CartItem_Save
GO
CREATE PROCEDURE [dbo].[CartItem_Save]
(
	@Id uniqueidentifier output,
	@ParentCartItemId uniqueidentifier = null,
	@CartId uniqueidentifier,
	@SKUId uniqueidentifier,
	@Quantity decimal(18,2),
	@Price money,
	@CreatedBy uniqueidentifier,
	@ModifiedBy uniqueidentifier,
	@Sequence int,
	@Comment nvarchar(1024),
	@BundleItemId uniqueidentifier = null
)
AS 
BEGIN
  
	DECLARE @CurrentDate datetime
	SET @CurrentDate = GetUTCDate()

	DECLARE @MaxSequence int

	IF(@Id IS NULL OR @Id = dbo.GetEmptyGUID())  
	BEGIN
		SELECT @MaxSequence = ISNULL(MAX(Sequence), 0) FROM CSCartItem WHERE CartId = @CartId
		SET @Id = newid()  
		INSERT INTO CSCartItem
		(
			Id,
			ParentCartItemId,
			CartId,
			SKUId,
			Quantity,
			Price,
			CreatedDate,
			CreatedBy,
			Sequence,
			Comment,
			BundleItemId
		)
		Values
		(
			@Id,
			@ParentCartItemId,
			@CartId,
			@SKUId,
			@Quantity,
			@Price,
			@CurrentDate,
			@CreatedBy,
			@MaxSequence +1,
			@Comment,
			@BundleItemId
		)
	END
	ELSE
	BEGIN  
		Update CSCartItem
		SET ParentCartItemId = @ParentCartItemId,
			Quantity = @Quantity,
			Price = @Price,
			ModifiedDate = @CurrentDate,
			ModifiedBy = @ModifiedBy,
			SKUID = @SKUId 
		WHERE Id = @Id
	END

	UPDATE CSCart SET ModifiedDate = @CurrentDate, ModifiedBy = @ModifiedBy
	WHERE Id = @CartId

	SELECT @Id
END
GO
IF (OBJECT_ID('PageMapBase_UpdateLastModification') IS NOT NULL)
	DROP PROCEDURE PageMapBase_UpdateLastModification
GO
CREATE PROCEDURE [dbo].[PageMapBase_UpdateLastModification]
(
	@SiteId			uniqueidentifier = null
)
AS
BEGIN
	DECLARE @ModifiedDate datetime
	SET @ModifiedDate =  DATEADD(SECOND, DATEDIFF(SECOND, 39000, GETUTCDATE()), 39000)

	IF @SiteId IS NOT NULL
	BEGIN
		UPDATE PageMapNode SET CompleteFriendlyUrl = CalculatedUrl
		WHERE SiteId = @SiteId
	END

	IF EXISTS (SELECT 1 FROM PageMapNode WHERE MasterPageMapNodeId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageMapNodeId = S.SourcePageMapNodeId 
		FROM PageMapNode P
			JOIN vwMasterSourceMenuId S ON S.PageMapNodeId = P.PageMapNodeId
		WHERE P.MasterPageMapNodeId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageMapNode SET MasterPageMapNodeId = PageMapNodeId 
		WHERE MasterPageMapNodeId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	IF EXISTS (SELECT 1 FROM PageDefinition WHERE MasterPageDefinitionId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageDefinitionId = S.SourcePageDefinitionId 
		FROM PageDefinition P
			JOIN vwMasterSourcePageId S ON S.PageDefinitionId = P.PageDefinitionId
		WHERE P.MasterPageDefinitionId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageDefinition SET MasterPageDefinitionId = PageDefinitionId 
		WHERE MasterPageDefinitionId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	UPDATE PageMapBase SET LastPageMapModificationDate = @ModifiedDate
	WHERE @SiteId IS NULL OR SiteId = @SiteId
END
GO
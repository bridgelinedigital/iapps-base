Delete from SESearchSetting
Where Title in 
(
'Search.ApiKey',
'Search.ApiUrl',
'Search.SiteKey'
)

GO
Delete from SESearchSetting
Where Id in 
(
	select max(Id) Id from SESearchSetting 
	Group By Title
	Having count(Title)>1
)
GO
DECLARE @Sequence int, @ModifiedBy uniqueidentifier
SELECT @ModifiedBy = Id FROM USUser WHERE Username like 'iappssystemuser'

IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.HawkClientId')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.HawkClientId', '877da5a0e5e8429ea0b7e5e95cdb6639', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END


IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.HawkApiKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.HawkApiKey', '4CED1040-C20D-42A7-9076-BB09DA471D8C', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END

IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.HawkAutocompleteUrl')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.HawkAutocompleteUrl', 'https://searchapi-dev.hawksearch.net/api/autocomplete', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END


IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.HawkBaseFieldUrl')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.HawkBaseFieldUrl', 'http://dev.hawksearch.net/api/v9/', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END


IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.HawkBaseIndexingUrl')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.HawkBaseIndexingUrl', 'https://indexing-dev.hawksearch.net/api/indexing', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END

IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.HawkBaseHierarchyUrl')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.HawkBaseHierarchyUrl', 'https://indexing-dev.hawksearch.net/api/hierarchy', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END


IF NOT EXISTS (SELECT 1 FROM SESearchSetting WHERE Title = 'Search.HawkBaseSearchUrl')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.SESearchSetting

	INSERT INTO SESearchSetting(SiteId, Title, Value, ModifiedBy, ModifiedDate, Sequence, Provider)
	SELECT Id, 'Search.HawkBaseSearchUrl', 'http://searchapi-dev.hawksearch.net/api/search', @ModifiedBy, GETUTCDATE(), @Sequence, 'BRIDGELINE' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
GO
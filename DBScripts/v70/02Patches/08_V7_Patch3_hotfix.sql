PRINT N'Updating COTaxonomy - ApplicationId column';
GO
UPDATE COTaxonomy SET ApplicationId = SiteId WHERE ApplicationId is NULL
GO
DECLARE @Sequence int, @settingTypeId int
SELECT @Sequence = MAX(sequence) + 1 FROM dbo.STSettingType

IF NOT EXISTS(Select 1 from STSettingType Where Name ='Marketier.DisableUnsubscribe')
BEGIN
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, IsEnabled, ControlType, IsVisible)
	VALUES('Marketier.DisableUnsubscribe', 'User will not be unsubscribed, just redirect to page', 1, @Sequence, 1, 'Checkbox', 1)

	Update STSettingType SET SettingGroupId = (SELECT TOP 1 Id FROM STSettingGroup WHERE Name = 'Email Settings')
	WHERE Name = 'Marketier.DisableUnsubscribe'
END

IF NOT EXISTS(Select 1 from STSettingType Where Name ='Marketier.ExternalUnsubscribeUrl')
BEGIN
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, IsEnabled, ControlType, IsVisible)
	VALUES('Marketier.ExternalUnsubscribeUrl', 'Redirect to external page', 1, @Sequence, 1, 'Textbox', 1)

	Update STSettingType SET SettingGroupId = (SELECT TOP 1 Id FROM STSettingGroup WHERE Name = 'Email Settings')
	WHERE Name = 'Marketier.ExternalUnsubscribeUrl'
END
GO
PRINT 'Add FFOrderShipmentItem table indexes'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_FFOrderShipmentItem_OrderShipmentContainerId' AND object_id = OBJECT_ID('dbo.FFOrderShipmentItem'))
	CREATE NONCLUSTERED INDEX IX_FFOrderShipmentItem_OrderShipmentContainerId ON [dbo].[FFOrderShipmentItem] ([OrderShipmentContainerId])
GO
PRINT 'Add OROrder table indexes'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_OROrder_CustomerId' AND object_id = OBJECT_ID('dbo.OROrder'))
	CREATE NONCLUSTERED INDEX [IX_OROrder_CustomerId] ON [dbo].[OROrder] ([CustomerId])
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_OROrder_OrderDate' AND object_id = OBJECT_ID('dbo.OROrder'))
	CREATE NONCLUSTERED INDEX [IX_OROrder_OrderDate] ON [dbo].[OROrder] ([OrderDate])
GO
PRINT 'Add OROrderItem table indexes'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_OROrderItem_ParentOrderItemId' AND object_id = OBJECT_ID('dbo.OROrderItem'))
	CREATE NONCLUSTERED INDEX [IX_OROrderItem_ParentOrderItemId] ON [dbo].[OROrderItem]([ParentOrderItemId] ASC) WITH (FILLFACTOR = 90)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_OROrderItem_OrderShippingId' AND object_id = OBJECT_ID('dbo.OROrderItem'))
	CREATE NONCLUSTERED INDEX [IX_OROrderItem_OrderShippingId] ON [dbo].[OROrderItem] ([OrderShippingId])
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_OROrderItem_OrderItemStatusId,_ProductSKUId' AND object_id = OBJECT_ID('dbo.OROrderItem'))
	CREATE NONCLUSTERED INDEX [IX_OROrderItem_OrderItemStatusId,_ProductSKUId] ON [dbo].[OROrderItem] ([OrderItemStatusId] ASC,	[ProductSKUId] ASC)
GO
PRINT 'Add OROrderItemAttributeValue table indexes'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_OROrderItemAttributeValue_CartItemId' AND object_id = OBJECT_ID('dbo.OROrderItemAttributeValue'))
	CREATE NONCLUSTERED INDEX [IX_OROrderItemAttributeValue_CartItemId] ON [dbo].[OROrderItemAttributeValue] ([CartItemId])
GO
PRINT 'Add USMembership table indexes'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_USMembership_UserId' AND object_id = OBJECT_ID('dbo.USMembership'))
	CREATE NONCLUSTERED INDEX [IX_USMembership_UserId] ON [dbo].[USMembership]([UserId])
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_USMembership_LoweredEmail' AND object_id = OBJECT_ID('dbo.USMembership'))
	CREATE NONCLUSTERED INDEX [IX_USMembership_LoweredEmail] ON [dbo].[USMembership] ([LoweredEmail] ASC,[UserId] ASC)
GO
PRINT 'Add USCommerceUserProfile table indexes'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_USCommerceUserProfile_AccountNumber' AND object_id = OBJECT_ID('dbo.USCommerceUserProfile'))
	CREATE NONCLUSTERED INDEX [IX_USCommerceUserProfile_AccountNumber] ON [dbo].[USCommerceUserProfile] ([AccountNumber] ASC)
GO
PRINT 'Add USUser table indexes'
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'lowerusername' AND object_id = OBJECT_ID('dbo.USUser'))
	DROP INDEX USUser.lowerusername
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_USUser_LoweredUserName' AND object_id = OBJECT_ID('dbo.USUser'))
	CREATE NONCLUSTERED INDEX [IX_USUser_LoweredUserName] ON [dbo].[USUser] ([LoweredUserName])
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_USUser_ModifiedDate' AND object_id = OBJECT_ID('dbo.USUser'))
	CREATE NONCLUSTERED INDEX [IX_USUser_ModifiedDate] ON [dbo].[USUser]([ModifiedDate] ASC) INCLUDE([CreatedDate])
GO
PRINT 'Add USUserShippingAddress table indexes'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_USUserShippingAddress_IsPrimary' AND object_id = OBJECT_ID('dbo.USUserShippingAddress'))
	CREATE NONCLUSTERED INDEX [IX_USUserShippingAddress_IsPrimary] ON [dbo].[USUserShippingAddress] ([IsPrimary]) INCLUDE ([UserId])
GO
PRINT N'ALTER vwAllocatedQuantityPerWarehouseByShipment';
GO
IF (OBJECT_ID('vwAllocatedQuantityPerWarehouseByShipment') IS NOT NULL)
	DROP VIEW dbo.vwAllocatedQuantityPerWarehouseByShipment
GO
CREATE VIEW [dbo].[vwAllocatedQuantityPerWarehouseByShipment]
WITH SCHEMABINDING
AS

SELECT OS.WarehouseId,
	OI.ProductSkuId,
	SUM(SI.Quantity) Quantity, 
	COUNT_BIG(*) Total
FROM dbo.FFOrderShipment OS
	INNER JOIN dbo.FFOrderShipmentItem SI ON OS.Id = SI.OrderShipmentId 
	INNER JOIN dbo.OROrderItem OI on OI.Id = SI.OrderItemid
	INNER JOIN dbo.OROrder O on OI.OrderId = O.Id
WHERE O.OrderStatusId NOT IN (2,3,12)
	AND OS.ShipmentStatus <> 2 
	AND OS.ShipmentStatus <> 10
	-- Order shipmnet status is not shipped AND NOT Cancelled 
GROUP BY OS.WarehouseId,OI.ProductSKUId
GO

CREATE UNIQUE CLUSTERED INDEX IDX_vwAllocatedQuantityPerWarehouseByShipment
   ON dbo.vwAllocatedQuantityPerWarehouseByShipment (ProductSKUId, WarehouseId)
GO

PRINT N'ALTER vwAllocatedQuantityPerWarehouse';
GO
IF (OBJECT_ID('vwAllocatedQuantityPerWarehouse') IS NOT NULL)
	DROP VIEW dbo.vwAllocatedQuantityPerWarehouse
GO
CREATE VIEW [dbo].[vwAllocatedQuantityPerWarehouse]
AS

SELECT WarehouseId, ProductSkuId, Quantity
FROM [dbo].[vwAllocatedQuantityPerWarehouseByShipment]

UNION ALL

SELECT W.Id WarehouseId, O.ProductSKUId, O.Quantity
FROM (
		SELECT OI.ProductSKUId, SUM(OI.Quantity) Quantity
		FROM OROrderItem OI
			INNER JOIN OROrder O ON OI.OrderId = O.Id
			INNER JOIN PRProductSKU S ON OI.ProductSKUId = S.Id
			INNER JOIN PRProduct P ON P.Id = S.ProductId
			INNER JOIN PRProductType T ON T.Id = P.ProductTypeId AND IsDownloadableMedia = 1
		WHERE  OrderItemStatusId = 1 AND O.OrderStatusId NOT IN (2,3,12)
		GROUP BY OI.ProductSKUId
	) O
	CROSS JOIN INWarehouse W
WHERE W.TypeId = 2
GO
PRINT N'ALTER vwBackOrderAllocatedQuantityPerWarehouse';
GO
IF (OBJECT_ID('vwBackOrderAllocatedQuantityPerWarehouse') IS NOT NULL)
	DROP VIEW dbo.vwBackOrderAllocatedQuantityPerWarehouse
GO
CREATE VIEW [dbo].[vwBackOrderAllocatedQuantityPerWarehouse]
AS

SELECT OS.WarehouseId,OI.ProductSKUId,sum(SI.Quantity) BackOrderQuantity
FROM FFOrderShipment OS
Inner Join FFOrderShipmentItem SI on OS.Id = SI.OrderShipmentId 
Inner Join OROrderItem OI on OI.Id = SI.OrderItemid
Inner join OROrder O on OI.OrderId = O.Id
WHERE  O.OrderStatusId NOT IN (2,3,12)
AND OS.ShipmentStatus <> 2 AND OS.ShipmentStatus <> 10
AND OI.OrderItemStatusID = 4
-- Order shipmnet status is not shipped AND NOT Cancelled 
Group By OS.WarehouseId,OI.ProductSKUId
UNION ALL
Select W.Id WarehouseId,O.ProductSKUId,O.Quantity
FROM(
Select OI.ProductSKUId,sum(OI.Quantity) Quantity
from OROrderItem OI
INNER JOIN OROrder O ON OI.OrderId =O.Id
Inner Join PRProductSKU S on OI.ProductSKUId = S.Id
Inner Join PRProduct P on P.Id = S.ProductId
INNER JOIN PRProductType T on T.Id =P.ProductTypeId AND IsDownloadableMedia =1
Where  OrderItemStatusId =4 AND O.OrderStatusId NOT IN (2,3,12)
Group By OI.ProductSKUId)O
CROSS JOIN INWarehouse W
Where W.TypeId =2
GO
PRINT N'ALTER vwCustomerOrderSummary';
GO
IF (OBJECT_ID('vwCustomerOrderSummary') IS NOT NULL)
	DROP VIEW dbo.vwCustomerOrderSummary
GO
CREATE VIEW dbo.vwCustomerOrderSummary
WITH SCHEMABINDING
AS

SELECT CustomerId, 
	SiteId, 
	COUNT_BIG(*) AS TotalOrders, 
	SUM(GrandTotal) AS TotalSpend
FROM dbo.OROrder O 
GROUP BY CustomerId, SiteId
GO

CREATE UNIQUE CLUSTERED INDEX IDX_vwCustomerOrderSummary_CustomerIdSiteId
   ON dbo.vwCustomerOrderSummary (CustomerId, SiteId)
GO
PRINT N'ALTER vwCustomer';
GO
IF (OBJECT_ID('vwCustomer') IS NOT NULL)
	DROP VIEW dbo.vwCustomer
GO
CREATE VIEW [dbo].[vwCustomer] 
AS         
SELECT	CUP.Id, 
	U.FirstName, 
	U.LastName, 
	U.MiddleName, 
	U.UserName, 
	NULL AS [Password], 
	M.Email, 
	U.EmailNotification, 
	U.CompanyName, 
	U.BirthDate, 
	U.Gender, 
	U.LeadScore,
	CUP.IsBadCustomer, 
	CUP.IsActive, 
	M.IsApproved, 
	M.IsLockedOut, 
	CUP.IsOnMailingList, 
	CUP.[Status], 
	CUP.CSRSecurityQuestion, 
	CUP.CSRSecurityPassword, 
	U.HomePhone,
	U.MobilePhone, 
	U.OtherPhone, 
	U.ImageId, 
	CUP.AccountNumber, 
	CUP.IsExpressCustomer, 
	CUP.ExternalId, 
	CUP.ExternalProfileId,
	(SELECT	COUNT(*) FROM CSCustomerLogin WHERE	CustomerId = CUP.Id) + 1 AS NumberOfLoginAccounts,
	CASE WHEN EXISTS (SELECT Id FROM PTLineOfCreditInfo WHERE CustomerId = CUP.Id AND [Status] != 3) THEN 1 ELSE 0 END AS HasLOC,
	A.Id AS AddressId, 
	A.AddressLine1, 
	A.AddressLine2, 
	A.AddressLine3, 
	A.City, 
	A.StateId, 
	ST.[State], 
	ST.StateCode,
	A.CountryId, 
	C.CountryName, 
	C.CountryCode, 
	A.Zip, 
	A.County, 
	A.Phone,
	U.ExpiryDate AS ExpirationDate, 
	U.LastActivityDate,
	CASE WHEN EXISTS (SELECT 1 FROM	USSiteUser WHERE UserId = U.Id AND IsSystemUser = 1) THEN 1 ELSE 0 END AS IsSystemUser,
	CUP.IsTaxExempt,
	CUP.TaxId,
	U.CreatedDate,  
	U.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	U.ModifiedDate, 
	U.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName
FROM USCommerceUserProfile AS CUP
	INNER JOIN USUser AS U ON U.Id = CUP.Id
	INNER JOIN USMembership AS M ON M.UserId = U.Id
	LEFT JOIN USUserShippingAddress AS USA ON USA.UserId = U.Id AND USA.IsPrimary = 1
	LEFT JOIN GLAddress AS A ON A.Id = USA.AddressId
	LEFT JOIN GLState AS ST ON ST.Id = A.StateId
	LEFT JOIN GLCountry AS C ON C.Id = A.CountryId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = U.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = U.ModifiedBy
WHERE U.[Status] !=3
GO
PRINT 'Update Function User_GetUsersInRole'
GO
IF(OBJECT_ID('User_GetUsersInRole') IS NOT NULL)
	DROP Function [dbo].[User_GetUsersInRole] 
GO
CREATE FUNCTION [dbo].[User_GetUsersInRole]      
(      
 @Id   int,      
 @MemberType smallint,      
 @ApplicationId uniqueidentifier = NULL,      
 @IsSystemUser  bit = 0,      
 @ObjectTypeId  int=null,      
 @ProductId  uniqueidentifier = NULL      
      
)      
RETURNS @ResultTable TABLE       
(      
 Id      uniqueidentifier,      
 UserName    nvarchar(256),        
 Email     nvarchar(512),      
 PasswordQuestion  nvarchar(512),        
 IsApproved    bit,      
 CreatedDate    datetime,      
 LastLoginDate   datetime,      
 LastActivityDate  datetime,      
 LastPasswordChangedDate datetime,      
 IsLockedOut    bit,      
 LastLockoutDate   datetime,    
 FirstName    nvarchar(256),    
 LastName    nvarchar(256),    
 MiddleName    nvarchar(256) ,    
 ExpiryDate    datetime ,    
 EmailNotification  bit ,    
 TimeZone    nvarchar(100),    
 ReportRangeSelection varchar(50),    
 ReportStartDate   datetime ,    
 ReportEndDate   datetime,     
 BirthDate    datetime,    
 CompanyName    nvarchar(1024),    
 Gender     nvarchar(50),    
 HomePhone    varchar(50),    
 MobilePhone    varchar(50),    
 OtherPhone    varchar(50),    
 ImageId     uniqueidentifier,
 LeadScore	int,  
 Status     int    
)      
AS      
BEGIN     
	DECLARE @tbSiteIds TABLE (Id uniqueidentifier)  
	INSERT INTO @tbSiteIds
	SELECT SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser, 0)

	DECLARE @tbUserIds Table (Id uniqueidentifier)  
	INSERT INTO @tbUserIds  
	SELECT DISTINCT G.MemberId   
	FROM USMemberGroup G  
		INNER JOIN USMemberRoles R ON G.GroupId = R.MemberId  
			AND R.RoleId = @Id AND R.MemberType = 2  
	WHERE  
		(@ObjectTypeId IS NULL OR R.ObjectTypeId = @ObjectTypeId) AND  
		(@ProductId IS NULL OR R.ProductId = @ProductId) AND  
		(@ApplicationId IS NULL OR G.ApplicationId IN (SELECT Id FROM @tbSiteIds))   
  
	UNION  
  
	SELECT DISTINCT U.Id  
	FROM dbo.USUser U  
		INNER JOIN dbo.USMemberRoles R ON R.MemberId = U.Id   
			AND R.RoleId = @Id AND R.MemberType = 1  
	WHERE (@ObjectTypeId IS NULL OR R.ObjectTypeId = @ObjectTypeId)   
  
	DECLARE @tbUser TYUser  
	INSERT INTO @tbUser  
	SELECT DISTINCT   
		U.Id,  
		U.UserName,  
		M.Email,   
		M.PasswordQuestion,    
		M.IsApproved,  
		U.CreatedDate,  
		M.LastLoginDate,   
		U.LastActivityDate,  
		M.LastPasswordChangedDate,   
		M.IsLockedOut,  
		M.LastLockoutDate,  
		U.FirstName,  
		U.LastName,  
		U.MiddleName,  
		U.ExpiryDate,  
		U.EmailNotification,  
		U.TimeZone,  
		U.ReportRangeSelection,  
		U.ReportStartDate,  
		U.ReportEndDate,  
		U.BirthDate,  
		U.CompanyName,  
		U.Gender,  
		U.HomePhone,  
		U.MobilePhone,  
		U.OtherPhone,  
		U.ImageId,
		U.LeadScore,  
		U.Status
	FROM dbo.USUser U
		INNER JOIN @tbUserIds T ON U.Id = T.Id  
		INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
		INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
			(@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser) AND  
			(@ApplicationId IS NULL OR S.SiteId IN (SELECT Id FROM @tbSiteIds))  
	WHERE U.ExpiryDate IS NULL OR U.ExpiryDate >= GETUTCDATE() AND  
		M.IsApproved = 1 AND M.IsLockedOut = 0 AND U.Status != 3 AND  
		(@ProductId IS NULL OR S.ProductId = @ProductId)  
  
	INSERT INTO @ResultTable  
	SELECT * FROM @tbUser  
  
	RETURN     
END
GO

PRINT 'Add function AssetFile_GetUrl'
GO

IF(OBJECT_ID('AssetFile_GetUrl') IS NOT NULL)
	DROP FUNCTION [dbo].[AssetFile_GetUrl] 
GO

CREATE FUNCTION [dbo].[AssetFile_GetUrl]
(	
	@SiteId			UNIQUEIDENTIFIER,
	@ObjectType		INT,
	@RelativePath	NVARCHAR(4000),
	@FileName		NVARCHAR(256)
)
RETURNS NVARCHAR(2048)
AS
BEGIN
	DECLARE	@Url	NVARCHAR(2048) = (
		SELECT	LOWER(REPLACE((
					CASE 
						WHEN @ObjectType = 9 THEN '/file%20library'
						WHEN @ObjectType = 33 THEN '/image%20library' + (CASE WHEN @SiteId != '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' THEN '/' + CAST(@SiteId AS NVARCHAR(36)) ELSE '' END)
					END + REPLACE(REPLACE(@RelativePath, '~/File Library', ''), '~/Image Library', '') + '/' + @FileName
				), ' ', '%20'))
	)

	RETURN	@Url
END
GO

PRINT 'Add Url column to COFile'
GO

IF (COL_LENGTH('COFile', 'Url') IS NULL)
	ALTER TABLE COFile ADD [Url] NVARCHAR(2048) NULL
GO

PRINT 'Populate URL column on COFile'
GO

UPDATE	F
SET		[Url] = dbo.AssetFile_GetUrl(C.ApplicationId, C.ObjectTypeId, F.RelativePath, F.[FileName])
FROM	COContent AS C
		INNER JOIN COFile AS F ON F.ContentId = C.Id
GO

PRINT 'Modify stored procedure AssetFile_RenameFile'
GO

IF(OBJECT_ID('AssetFile_RenameFile') IS NOT NULL)
	DROP PROCEDURE AssetFile_RenameFile
GO

CREATE PROCEDURE [dbo].[AssetFile_RenameFile] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id  uniqueidentifier,
	@DestFN nvarchar(256)
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE	@Now datetime,
		@Stmt 	VARCHAR(36)

BEGIN
	-- if Id is specified, ensure exists 
	IF (@Id IS NOT NULL)
	BEGIN
		IF (NOT EXISTS(SELECT 1 FROM   COFile WHERE  ContentId = @Id))
		BEGIN
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END
		
		--check if the status of AssetFile row is in deleted status and if yes
		--raiserror that this action is not supported..
		IF (EXISTS(SELECT 1 FROM COContent WHERE Id=@Id AND Status = dbo.GetDeleteStatus()))
		BEGIN
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTSUPPORTED||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()				
		END
	
		SET @Now = getutcdate()
	
		--changes XMLForm status to deleted in COXMLForm table
		Update COFile WITH (ROWLOCK)
		set 		
			FileName = @DestFN
		where ContentId = @Id

		UPDATE	F
		SET		[Url] = dbo.AssetFile_GetUrl(C.ApplicationId, C.ObjectTypeId, F.RelativePath, F.[FileName])
		FROM	COContent AS C
				INNER JOIN COFile AS F ON F.ContentId = C.Id
		WHERE	C.Id = @Id
	END
End
GO

PRINT 'Modify stored procedure AssetFile_SaveFile'
GO

IF(OBJECT_ID('AssetFile_SaveFile') IS NOT NULL)
	DROP PROCEDURE AssetFile_SaveFile
GO

CREATE PROCEDURE [dbo].[AssetFile_SaveFile] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id  uniqueidentifier,
	@FileName  nvarchar(256),
	@FileSize  int = null,
	@Extension  nvarchar(10),
	@FolderName  nvarchar(256) = null,
	@Attributes  nvarchar(256) = null,
	@RelativePath  nvarchar(4000) = null,
	@PhysicalPath  nvarchar(4000)  = null,
	@MimeType nvarchar(256) = null,
    @AltText nvarchar(1024)= null, 
    @FileIcon nvarchar(1024)= null, 
    @DescriptiveMetadata nvarchar(4000)= null,
	@ExcludeFromExternalSearch bit = 0,
    @SecurityLevels nvarchar(max) = null
)
as
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE	
	@Stmt 		VARCHAR(50),	
	@Error 		int

--********************************************************************************
-- code
--********************************************************************************
BEGIN
	IF @RelativePath IS NULL OR @RelativePath = ''
	BEGIN
		DECLARE @ParentId uniqueidentifier
		DECLARE @ObjectTypeId int
		SELECT @ParentId = ParentId, @ObjectTypeId = ObjectTypeId FROM COContent WHERE Id = @Id
		
		SET @RelativePath = dbo.GetVirtualPathByObjectType(@ParentId, @ObjectTypeId)
	END

	DECLARE @SiteId		UNIQUEIDENTIFIER,
			@ObjectType INT
			
	SELECT	@SiteId = ApplicationId,
			@ObjectType = ObjectTypeId
	FROM	COContent
	WHERE	Id = @Id
	
    IF (EXISTS(SELECT 1 FROM COFile WHERE ContentId = @Id))
	BEGIN			
		SET @Stmt = 'Update AssetFile'
		UPDATE COFile WITH (ROWLOCK)
		SET		[FileName] = @FileName,
				FileSize = @FileSize,
				Extension = @Extension,
				FolderName = @FolderName,
				Attributes = @Attributes,
				RelativePath = @RelativePath,
				PhysicalPath = @PhysicalPath, 
				MimeType = @MimeType,
				AltText = @AltText,
				FileIcon = @FileIcon, 
				DescriptiveMetadata = @DescriptiveMetadata,
				ExcludeFromExternalSearch = @ExcludeFromExternalSearch,
				[Url] = dbo.AssetFile_GetUrl(@SiteId, @ObjectType, @RelativePath, @FileName)
		WHERE	ContentId = @Id
		
		IF @SecurityLevels IS NOT NULL
			BEGIN
				DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
			END
	END		
	ELSE
	BEGIN
		SET @Stmt = 'Create AssetFile'
		INSERT INTO COFile (
				ContentId,
				[FileName],
				FileSize,
				Extension,
				FolderName,
				Attributes,
				RelativePath,
				PhysicalPath, 
				MimeType,
				AltText,
				FileIcon, 
				DescriptiveMetadata,
				ExcludeFromExternalSearch,
				[Url]
		)
		values
		(
			@Id,
			@FileName,
			@FileSize,
			@Extension,
			@FolderName,
			@Attributes,
			@RelativePath,
			@PhysicalPath,
			@MimeType,
			@AltText,
			@FileIcon, 
			@DescriptiveMetadata,	
			@ExcludeFromExternalSearch,
			dbo.AssetFile_GetUrl(@SiteId, @ObjectType, @RelativePath, @FileName)
		)
	END
	
	IF @SecurityLevels IS NOT NULL
	BEGIN
			INSERT INTO USSecurityLevelObject (ObjectId,ObjectTypeId,SecurityLevelId)
			SELECT @Id,9,Value
			FROM SplitComma(@SecurityLevels,',')
			WHERE LEN(Value)>0
			EXCEPT
			SELECT @Id,ObjectTypeId,SecurityLevelId FROM USSecurityLevelObject
			WHERE ObjectId=@Id
	END

	SELECT @Error = @@ERROR
	IF @Error <> 0
	BEGIN
		RAISERROR('DBERROR||%s',16,1,@Stmt)
		RETURN dbo.GetDataBaseErrorCode()
	END
	
END
GO

PRINT 'Modify stored procedure AssetImage_SaveImage'
GO

IF(OBJECT_ID('AssetImage_SaveImage') IS NOT NULL)
	DROP PROCEDURE AssetImage_SaveImage
GO

CREATE PROCEDURE [dbo].[AssetImage_SaveImage] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id  uniqueidentifier,
	@FileName  nvarchar(256),
	@FileSize  int = null,
	@Extension  nvarchar(10),
	@FolderName  nvarchar(256) = null,
	@Attributes  nvarchar(256) = null,
	@RelativePath  nvarchar(4000) = null,
	@PhysicalPath  nvarchar(4000)  = null,
	@MimeType nvarchar(256) = null,
    @AltText nvarchar(1024) = null,
    @FileIcon nvarchar(1024)= null, 
    @DescriptiveMetadata nvarchar(4000)= null,
    @ImageHeight int = null,
    @ImageWidth int = null
)
as
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
	@Stmt 		VARCHAR(50),	
	@Error 		int,
	@ExcludeFromExternalSearch bit = 0
--********************************************************************************
-- code
--********************************************************************************
BEGIN

	IF @RelativePath IS NULL OR @RelativePath = ''
	BEGIN
		DECLARE @ParentId uniqueidentifier
		DECLARE @ObjectTypeId int
		SELECT @ParentId = ParentId, @ObjectTypeId = ObjectTypeId FROM COContent WHERE Id = @Id
		
		SET @RelativePath = dbo.GetVirtualPathByObjectType(@ParentId, @ObjectTypeId)
	END
	
	DECLARE @SiteId		UNIQUEIDENTIFIER,
			@ObjectType INT
			
	SELECT	@SiteId = ApplicationId,
			@ObjectType = ObjectTypeId
	FROM	COContent
	WHERE	Id = @Id

    IF (EXISTS(SELECT 1 FROM COFile WHERE ContentId = @Id))
	BEGIN			
		SET @Stmt = 'Update AssetImage'
		UPDATE COFile WITH (ROWLOCK)
		SET		[FileName] = @FileName,
				FileSize = @FileSize,
				Extension = @Extension,
				FolderName = @FolderName,
				Attributes = @Attributes,
				RelativePath = @RelativePath,
				PhysicalPath = @PhysicalPath, 
				MimeType = @MimeType,
				AltText = @AltText,
				FileIcon = @FileIcon, 
				DescriptiveMetadata = @DescriptiveMetadata,
				ImageHeight = @ImageHeight,
				ImageWidth = @ImageWidth,
				ExcludeFromExternalSearch = @ExcludeFromExternalSearch,
				[Url] = dbo.AssetFile_GetUrl(@SiteId, @ObjectType, @RelativePath, @FileName)
		WHERE	ContentId = @Id
	END		
	ELSE
	BEGIN		
		SET @Stmt = 'Create Asset Image'
		INSERT INTO COFile(
				ContentId,
				[FileName],
				FileSize,
				Extension,
				FolderName,
				Attributes,
				RelativePath,
				PhysicalPath, 
				MimeType,
				AltText,
				FileIcon, 
				DescriptiveMetadata,
				ImageHeight,
				ImageWidth,
				ExcludeFromExternalSearch,
				[Url]
		)
		values
		(
			@Id,
			@FileName,
			@FileSize,
			@Extension,
			@FolderName,
			@Attributes,
			@RelativePath,
			@PhysicalPath,
			@MimeType,
			@AltText,
			@FileIcon, 
			@DescriptiveMetadata,
			@ImageHeight,
			@ImageWidth,
			@ExcludeFromExternalSearch,
			dbo.AssetFile_GetUrl(@SiteId, @ObjectType, @RelativePath, @FileName)
		)
	END

	SELECT @Error = @@ERROR
	IF @Error <> 0
	BEGIN
		RAISERROR('DBERROR||%s',16,1,@Stmt)
		RETURN dbo.GetDataBaseErrorCode()
	END
	
END
GO

PRINT 'Modify stored procedure SqlDirectoryProvider_MoveFiles'
GO

IF(OBJECT_ID('SqlDirectoryProvider_MoveFiles') IS NOT NULL)
	DROP PROCEDURE SqlDirectoryProvider_MoveFiles
GO

CREATE PROCEDURE  [dbo].[SqlDirectoryProvider_MoveFiles] 
(
	@SourceNodeId 			[uniqueidentifier],  
	@DestinationNodeId 		[uniqueidentifier],
	@Recursive				[bit],
	@OverWrite				[bit],
	@ObjectType				int=null,
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	DECLARE @ApplicationId uniqueidentifier,
		@LftValue int,
		@RgtValue int,
		@OldRelativePath nvarchar(2000),
		@NewRelativePath nvarchar(2000)

	DECLARE @tbFiles TABLE(FileId uniqueidentifier, OldFileName nvarchar(max), NewFileName nvarchar(max), ObjectTypeId int)

	IF @ObjectType = 7
	BEGIN
		SELECT @LftValue = LftValue, @RgtValue = RgtValue, @ApplicationId = SiteId FROM COContentStructure WHERE Id = @SourceNodeId

		UPDATE C SET C.ParentId = @DestinationNodeId
		FROM COContent C 
			JOIN COContentStructure S ON C.ParentId = S.Id
		WHERE LftValue BETWEEN @LftValue AND @RgtValue
			AND (@Recursive = 1 OR S.Id = @SourceNodeId)
			AND C.ApplicationId = @ApplicationId
	END
	ELSE IF @ObjectType = 9 OR @ObjectType = 33
	BEGIN
		IF @ObjectType = 9
		BEGIN
			SELECT @LftValue = LftValue, @RgtValue = RgtValue, 
				@ApplicationId = SiteId, @OldRelativePath = VirtualPath FROM COFileStructure WHERE Id = @SourceNodeId

			SELECT @NewRelativePath = VirtualPath FROM COFileStructure WHERE Id = @DestinationNodeId

			INSERT INTO @tbFiles
			SELECT C.Id, F.FileName, F.FileName, @ObjectType
			FROM COContent C 
				JOIN COFile F ON C.Id = F.ContentId
				JOIN COFileStructure S ON C.ParentId = S.Id
			WHERE LftValue BETWEEN @LftValue AND @RgtValue
				AND (@Recursive = 1 OR S.Id = @SourceNodeId)
				AND C.ApplicationId = @ApplicationId AND C.Status != 3
		END
		ELSE
		BEGIN
			SELECT @LftValue = LftValue, @RgtValue = RgtValue, 
				@ApplicationId = SiteId, @OldRelativePath = VirtualPath FROM COImageStructure WHERE Id = @SourceNodeId

			SELECT @NewRelativePath = VirtualPath FROM COImageStructure WHERE Id = @DestinationNodeId

			INSERT INTO @tbFiles
			SELECT C.Id, F.FileName, F.FileName, @ObjectType
			FROM COContent C 
				JOIN COFile F ON C.Id = F.ContentId
				JOIN COImageStructure S ON C.ParentId = S.Id
			WHERE LftValue BETWEEN @LftValue AND @RgtValue
				AND (@Recursive = 1 OR S.Id = @SourceNodeId)
				AND C.ApplicationId = @ApplicationId AND C.Status != 3
		END

		IF @OverWrite = 1
		BEGIN
			UPDATE C SET C.Status = 3
			FROM COContent C
				JOIN COFile F ON C.Id = F.ContentId
				WHERE ParentId = @DestinationNodeId 
					AND LOWER(F.FileName) IN (SELECT LOWER(OldFileName) FROM @tbFiles)			
		END
		ELSE
		BEGIN
			UPDATE @tbFiles SET NewFileName = dbo.GetNewFileName(@DestinationNodeId, @SourceNodeId, 9)
			WHERE FileId IN (SELECT Id FROM COContent C
				JOIN COFile F ON C.Id = F.ContentId
				WHERE ParentId = @DestinationNodeId 
					AND LOWER(F.FileName) IN (SELECT LOWER(OldFileName) FROM @tbFiles))
		END
	
		UPDATE	F
		SET		F.FileName = T.NewFileName, F.RelativePath = @NewRelativePath,
				[Url] = dbo.AssetFile_GetUrl(@ApplicationId, @ObjectType, @NewRelativePath, T.NewFileName)
		FROM	@tbFiles T
				JOIN COFile F ON T.FileId = F.ContentId

		UPDATE COContent SET ParentId = @DestinationNodeId, ModifiedDate = GETUTCDATE(), ModifiedBy = @ModifiedBy
			WHERE Id IN (SELECT FileId FROM @tbFiles)

		UPDATE @tbFiles
			SET OldFileName = @OldRelativePath + '/' + OldFileName,
			NewFileName = @NewRelativePath + '/' + NewFileName

	END
	ELSE
	BEGIN
		SELECT @LftValue = LftValue, @RgtValue = RgtValue, @ApplicationId = SiteId FROM HSStructure WHERE Id = @SourceNodeId
		SELECT @OldRelativePath = VirtualPath, @ObjectType = ObjectTypeId FROM SISiteDirectory WHERE Id = @SourceNodeId
		SELECT @NewRelativePath = VirtualPath FROM SISiteDirectory WHERE Id = @DestinationNodeId

		IF @ObjectType = 3
		BEGIN
			INSERT INTO @tbFiles
			SELECT S.Id, S.FileName, S.FileName, @ObjectType
			FROM SITemplate S
				JOIN HSStructure H ON H.Id = S.Id
			WHERE LftValue BETWEEN @LftValue AND @RgtValue
				AND S.ApplicationId = @ApplicationId AND S.Status != 3

			IF @OverWrite = 1
			BEGIN
				UPDATE S SET S.Status = 3
				FROM SITemplate S
					JOIN HSStructure H ON H.Id = S.Id
					WHERE LftValue BETWEEN @LftValue AND @RgtValue
						AND LOWER(S.FileName) IN (SELECT LOWER(OldFileName) FROM @tbFiles)			
			END
			ELSE
			BEGIN
				SELECT @LftValue = LftValue, @RgtValue = RgtValue, @ApplicationId = SiteId FROM HSStructure WHERE Id = @DestinationNodeId
		
				UPDATE @tbFiles SET NewFileName = dbo.GetNewFileName(@DestinationNodeId, FileId, @ObjectType)
				WHERE FileId IN (SELECT S.Id FROM SITemplate S
					JOIN HSStructure H ON H.Id = S.Id
					WHERE LftValue BETWEEN @LftValue AND @RgtValue
						AND LOWER(S.FileName) IN (SELECT LOWER(OldFileName) FROM @tbFiles))
			END
		
			UPDATE S SET S.FileName = T.NewFileName, ModifiedDate = GETUTCDATE(), ModifiedBy = @ModifiedBy
			FROM @tbFiles T
				JOIN SITemplate S ON T.FileId = S.Id
		END
		ELSE IF @ObjectType = 4
		BEGIN
			INSERT INTO @tbFiles
			SELECT S.Id, S.FileName, S.FileName, @ObjectType
			FROM SIStyle S
				JOIN HSStructure H ON H.Id = S.Id
			WHERE LftValue BETWEEN @LftValue AND @RgtValue
				AND S.ApplicationId = @ApplicationId AND S.Status != 3

			IF @OverWrite = 1
			BEGIN
				UPDATE S SET S.Status = 3
				FROM SIStyle S
					JOIN HSStructure H ON H.Id = S.Id
					WHERE LftValue BETWEEN @LftValue AND @RgtValue
						AND LOWER(S.FileName) IN (SELECT LOWER(OldFileName) FROM @tbFiles)			
			END
			ELSE
			BEGIN
				SELECT @LftValue = LftValue, @RgtValue = RgtValue, @ApplicationId = SiteId FROM HSStructure WHERE Id = @DestinationNodeId
		
				UPDATE @tbFiles SET NewFileName = dbo.GetNewFileName(@DestinationNodeId, FileId, @ObjectType)
				WHERE FileId IN (SELECT S.Id FROM SIStyle S
					JOIN HSStructure H ON H.Id = S.Id
					WHERE LftValue BETWEEN @LftValue AND @RgtValue
						AND LOWER(S.FileName) IN (SELECT LOWER(OldFileName) FROM @tbFiles))
			END
		
			UPDATE S SET S.FileName = T.NewFileName, ModifiedDate = GETUTCDATE(), ModifiedBy = @ModifiedBy
			FROM @tbFiles T
				JOIN SIStyle S ON T.FileId = S.Id
		END
		ELSE IF @ObjectType = 35
		BEGIN
			INSERT INTO @tbFiles
			SELECT S.Id, S.FileName, S.FileName, @ObjectType
			FROM SIScript S
				JOIN HSStructure H ON H.Id = S.Id
			WHERE LftValue BETWEEN @LftValue AND @RgtValue
				AND S.ApplicationId = @ApplicationId AND S.Status != 3

			IF @OverWrite = 1
			BEGIN
				UPDATE S SET S.Status = 3
				FROM SIScript S
					JOIN HSStructure H ON H.Id = S.Id
					WHERE LftValue BETWEEN @LftValue AND @RgtValue
						AND LOWER(S.FileName) IN (SELECT LOWER(OldFileName) FROM @tbFiles)			
			END
			ELSE
			BEGIN
				SELECT @LftValue = LftValue, @RgtValue = RgtValue, @ApplicationId = SiteId FROM HSStructure WHERE Id = @DestinationNodeId
		
				UPDATE @tbFiles SET NewFileName = dbo.GetNewFileName(@DestinationNodeId, FileId, @ObjectType)
				WHERE FileId IN (SELECT S.Id FROM SIScript S
					JOIN HSStructure H ON H.Id = S.Id
					WHERE LftValue BETWEEN @LftValue AND @RgtValue
						AND LOWER(S.FileName) IN (SELECT LOWER(OldFileName) FROM @tbFiles))
			END
		
			UPDATE S SET S.FileName = T.NewFileName, ModifiedDate = GETUTCDATE(), ModifiedBy = @ModifiedBy
			FROM @tbFiles T
				JOIN SIScript S ON T.FileId = S.Id
		END

		DECLARE @FileId uniqueidentifier
		DECLARE Hierarchy_Cursor CURSOR FOR SELECT FileId FROM @tbFiles
		OPEN Hierarchy_Cursor 
		FETCH NEXT FROM Hierarchy_Cursor INTO @FileId
		While (@@FETCH_STATUS <> -1)
		BEGIN
			EXEC HierarchyNode_MoveTree @SourceNodeId = @FileId, @DestinationNodeId = @DestinationNodeId

			FETCH NEXT FROM Hierarchy_Cursor INTO @FileId 
		END
		CLOSE Hierarchy_Cursor
		DEALLOCATE Hierarchy_Cursor

		UPDATE @tbFiles
			SET OldFileName = @OldRelativePath + '/' + OldFileName,
			NewFileName = @NewRelativePath + '/' + NewFileName
	END

	SELECT * FROM @tbFiles

END
GO

PRINT 'Modify stored procedure SqlDirectoryProvider_UpdateFileName'
GO

IF(OBJECT_ID('SqlDirectoryProvider_UpdateFileName') IS NOT NULL)
	DROP PROCEDURE SqlDirectoryProvider_UpdateFileName
GO

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_UpdateFileName](@DestinationNodeId uniqueidentifier, @FileId UniqueIdentifier,@ObjectType int =null)  
AS  
	Declare @StIndex int,
			@EndIndex int,
			@Pos int,
			@Ver nvarchar(255),
			@ExtIndex int,
			@Ext nvarchar(4),
			@NewName nvarchar(max),
			@Name nvarchar(256),
			@intVer int,
			@Count int,
			@tempName Nvarchar(max)
BEGIN 
	IF @ObjectType is null
	Select @ObjectType=ObjectTypeId from HSStructure Where Id=@FileId

	IF @ObjectType =3
	BEGIN
		Select @Name= FileName from SITemplate Where Id=@FileId AND Status!= dbo.GetDeleteStatus()
	END
	ELSE If @ObjectType =4
	BEGIN
		Select @Name=FileName from SIStyle Where Id=@FileId AND Status!= dbo.GetDeleteStatus()
	END
	ELSE If @ObjectType =9
	BEGIN
		Select @Name=FileName from COContent C
		Inner Join COFile F on C.Id = F.ContentId
		Where ObjectTypeId = 9 AND C.Id=@FileId AND Status!= dbo.GetDeleteStatus()
	END
	ELSE If @ObjectType =33
	BEGIN
		Select @Name=FileName from COContent C
		Inner Join COFile F on C.Id = F.ContentId
		Where ObjectTypeId = 33 AND C.Id=@FileId AND Status!= dbo.GetDeleteStatus()
	END
	ELSE If @ObjectType =35
	BEGIN
		Select @Name=FileName from SIScript Where Id=@FileId AND Status!= dbo.GetDeleteStatus()
  END

--	ELSE
--	BEGIN
--		
--	END

	SET @StIndex = dbo.CharLastIndex('(',@Name)
	SET @EndIndex =dbo.CharLastIndex(')',@Name)
	SET @ExtIndex = dbo.CharLastIndex('.',@Name)
	SET @Ext =''
	IF @ExtIndex >0
		SET @Ext = substring(@Name,@ExtIndex,4)

	SET @NewName ='FileName'
	SET @Count =1
	While (@NewName!='')
	BEGIN
		IF @StIndex >0
			SET @tempName = substring(@Name,0,@StIndex+1) + CAST(CAST(@Count As int) + 1 AS Varchar(36))+')' + @Ext
		ELSE
			SET @tempName = substring(@Name,0,@ExtIndex) + '(' + CAST(CAST(@Count As int) + 1 AS Varchar(36))+')' + @Ext
			
		IF @ObjectType =3
		BEGIN
			IF NOT EXISTS (Select * from SITemplate T Inner Join HSStructure S on T.Id = S.Id Where S.ParentId=@DestinationNodeId AND Status!= dbo.GetDeleteStatus() AND FileName=@tempName)
			 BEGIN 
				SET @NewName = @tempName
				Update SITemplate SET FileName = @tempName Where Id =@FileId 
				BREAK
			 END
		END
		ELSE If @ObjectType =4
		BEGIN
			IF NOT EXISTS (Select * from SIStyle T Inner Join HSStructure S on T.Id = S.Id Where S.ParentId=@DestinationNodeId AND Status!= dbo.GetDeleteStatus() AND FileName=@tempName)
			BEGIN 
				SET @NewName = @tempName
				Update SIStyle SET FileName = @tempName Where Id =@FileId 
				BREAK
			 END					 
		END
		ELSE If @ObjectType =9
		BEGIN
			IF NOT EXISTS (Select * from COContent C
			Inner Join COFile F on C.Id = F.ContentId
			Inner Join COFileStructure S on C.ParentId = S.Id Where S.ParentId=@DestinationNodeId AND C.Status!= dbo.GetDeleteStatus() AND FileName=@tempName
			AND C.ObjectTypeId = 9)
			BEGIN 
				SET @NewName = @tempName
				
				UPDATE	F
				SET		FileName = @tempName,
						[Url] = dbo.AssetFile_GetUrl(C.ApplicationId, C.ObjectTypeId, F.RelativePath, @tempName)
				FROM	COContent AS C
						INNER JOIN COFile AS F ON F.ContentId = C.Id
				WHERE	C.Id = @FileId 
				
				BREAK
			 END
		END
		ELSE If @ObjectType =33
		BEGIN
				IF NOT EXISTS (Select * from COContent C
			Inner Join COFile F on C.Id = F.ContentId
			Inner Join COImageStructure S on C.ParentId = S.Id Where S.ParentId=@DestinationNodeId AND C.Status!= dbo.GetDeleteStatus() AND FileName=@tempName
			AND C.ObjectTypeId = 33)
			BEGIN 
				SET @NewName = @tempName
				UPDATE	F
				SET		FileName = @tempName,
						[Url] = dbo.AssetFile_GetUrl(C.ApplicationId, C.ObjectTypeId, F.RelativePath, @tempName)
				FROM	COContent AS C
						INNER JOIN COFile AS F ON F.ContentId = C.Id
				WHERE	C.Id = @FileId 
				BREAK
			 END
		END	 		  
	    ELSE If @ObjectType =35
		BEGIN
			IF NOT EXISTS (Select * from SIScript T Inner Join HSStructure S on T.Id = S.Id Where S.ParentId=@DestinationNodeId AND Status!= dbo.GetDeleteStatus() AND FileName=@tempName)
			BEGIN 
				SET @NewName = @tempName
				Update SIScript SET FileName = @tempName Where Id =@FileId 
				BREAK
			 END
		END
		SET @Count =@Count + 1
	END
	IF(@ObjectType != 9 AND @ObjectType != 33 AND @ObjectType != 7)
		EXEC HierarchyNode_MoveTree @SourceNodeId = @FileId,@DestinationNodeId =@DestinationNodeId

END
GO

PRINT N'ALTER vwObjectUrls_Files';
GO
IF (OBJECT_ID('vwObjectUrls_Files') IS NOT NULL)
	DROP VIEW dbo.vwObjectUrls_Files
GO
CREATE VIEW [dbo].[vwObjectUrls_Files] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS
SELECT	C.ApplicationId, 
		C.ObjectTypeId, 
		C.Id, 
		C.Id,
		F.[Url]
FROM	dbo.COContent AS C
		INNER JOIN dbo.COFile AS F ON F.ContentId = C.Id
GO
PRINT 'Creating ObjectUrlDto_GetByUrl'
GO
IF(OBJECT_ID('ObjectUrlDto_GetByUrl') IS NOT NULL)
	DROP PROCEDURE ObjectUrlDto_GetByUrl
GO
IF TYPE_ID('TYObjectUrlByUrl') IS NOT NULL
	DROP TYPE [dbo].[TYObjectUrlByUrl]
GO
CREATE TYPE [dbo].[TYObjectUrlByUrl] AS TABLE
(
	SiteId	UNIQUEIDENTIFIER,
	[Url]	NVARCHAR(2048)
)
GO
CREATE PROCEDURE [dbo].[ObjectUrlDto_GetByUrl]
(
	@Urls	TYObjectUrlByUrl READONLY
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT	OU.SiteId, OU.ObjectId, OU.ObjectType, OU.[Url]
	FROM	@Urls AS U
			INNER JOIN vwObjectUrls_Blogs AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = U.[Url]

	UNION ALL

	SELECT	OU.SiteId, OU.ObjectId, OU.ObjectType, OU.[Url]
	FROM	@Urls AS U
			INNER JOIN vwObjectUrls_Files AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = U.[Url]

	UNION ALL

	SELECT	OU.SiteId, OU.Id, 2, LOWER(OU.[Url])
	FROM	@Urls AS U
			INNER JOIN vwMenuUrl AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = U.[Url]

	UNION ALL

	SELECT	OU.SiteId, OU.Id, 8, OU.[Url]
	FROM	@Urls AS U
			INNER JOIN vwSimplePageUrl AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = U.[Url]

	UNION ALL

	SELECT	OU.SiteId, OU.ObjectId, OU.ObjectType, OU.[Url]
	FROM	@Urls AS U
			INNER JOIN vwObjectUrls_Products AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = U.[Url]
END
GO
PRINT 'Creating Product_GetProductAndSkuByXmlGuids'
GO
IF(OBJECT_ID('Product_GetProductAndSkuByXmlGuids') IS NOT NULL)
	DROP PROCEDURE Product_GetProductAndSkuByXmlGuids
GO
CREATE  PROCEDURE [dbo].[Product_GetProductAndSkuByXmlGuids]      
(      
    @Id             xml,  
    @ApplicationId  uniqueidentifier = null
)      
AS      
BEGIN      
    DECLARE @tbProduct TABLE(Sno int Identity(1,1), ProductId uniqueidentifier)      

    INSERT INTO @tbProduct(ProductId)      
    SELECT DISTINCT tab.col.value('text()[1]','uniqueidentifier')      
    FROM @Id.nodes('/ProductCollection/Product') tab(col)      
    
    SELECT        
        S.[Id],      
        S.[ProductIDUser],      
        S.[Title],      
        S.[ShortDescription],      
        S.[LongDescription],      
        S.[Description],      
        S.[Keyword],      
        S.[IsActive],      
        S.[ProductTypeID],      
        vPT.[ProductTypeName] AS ProductTypeName,      
        vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle AS DefaultUrl, S.UrlFriendlyTitle,       
        vPT.[IsDownloadableMedia], S.[ProductStyle],      
        (SELECT COUNT(*) FROM PRProductSKU WHERE ProductId = S.[Id]) AS NumberOfSKUs,      
        S.CreatedDate,      
        S.CreatedById CreatedBy,      
        S.ModifiedDate,      
        S.ModifiedById ModifiedBy,      
        S.Status,      
        S.IsBundle,      
        dbo.ConvertTimeFromUtc(S.BundleCompositionLastModified, @ApplicationId) AS BundleCompositionLastModified,      
        SEOFriendlyUrl,
        S.TaxCategoryId,
        S.PromoteAsNewItem,
        S.PromoteAsTopSeller,
        S.ExcludeFromDiscounts,
        S.ExcludeFromShippingPromotions,
        S.Freight,
        S.Refundable,
        S.TaxExempt,
        S.SEOTitle,
        S.SEOH1,
        S.SEODescription,
        S.SEOKeywords
    FROM @tbProduct T      
        INNER JOIN vwProduct S ON S.Id = T.ProductId      
        INNER JOIN vwProductTypePath vPT ON [S].ProductTypeID = vPT.ProductTypeId      
    WHERE S.SiteId = @ApplicationId
        AND S.Status <= 1 AND S.IsActive = 1

    SELECT
        SA.Id,  
        SA.AttributeId,  
        SA.AttributeEnumId,  
        AE.NumericValue,  
        ISNULL(AE.[IsDefault], 0) AS [IsDefault],  
        AE.Sequence,  
        SA.Value,  
        AE.Code,  
        SA.CreatedDate,  
        SA.CreatedBy,  
        SA.ModifiedDate,  
        SA.ModifiedBy,  
        SA.Status,
        SA.ProductId
    FROM @tbProduct T
        INNER JOIN PRProductAttributeValue SA ON SA.ProductId = T.ProductId  
        LEFT JOIN ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
    WHERE SA.Status = 1  

    SELECT 
	    S.[Id],   
        S.[ProductId],    
        S.[SiteId],    
        S.[SKU],    
        S.[Title],    
        S.[Description],    
        S.[Sequence],    
        S.[IsActive],   
        S.[IsOnline],   
        ISNULL(SC.ListPrice,S.ListPrice) ListPrice,    
        S.[UnitCost],
        S.[PreviousSoldCount],   
        S.[CreatedBy],
        S.[CreatedDate],    
        S.[ModifiedBy],    
        S.[ModifiedDate],    
        S.[Status],    
        S.[Measure],    
        S.[OrderMinimum],    
        S.[OrderIncrement],  
        S.[HandlingCharges], 
        S.[Length],
        S.[Height],
        S.[Width],
        S.[Weight],
        S.[SelfShippable],    
        S.[AvailableForBackOrder],
        S.MaximumDiscountPercentage,
        S.[BackOrderLimit],
        S.[IsUnlimitedQuantity],
        S.FreeShipping,
        S.UserDefinedPrice
    FROM VWSKU S   
        INNER JOIN @tbProduct T ON S.ProductId = T.ProductId  
        LEFT JOIN PRProductSKUVariantCache SC ON S.Id = SC.Id AND SC.SiteId = @ApplicationId
    WHERE S.SiteId = @ApplicationId
        AND S.Status <= 1 AND S.IsActive = 1 AND S.IsOnline = 1

    SELECT
        SA.Id,  
        SA.SKUId,  
        SA.AttributeId,  
        SA.AttributeEnumId,  
        AE.NumericValue,  
        ISNULL(AE.[IsDefault], 0) AS [IsDefault],  
        AE.Sequence,  
        SA.Value,  
        AE.Code,  
        SA.CreatedDate,  
        SA.CreatedBy,  
        SA.ModifiedDate,  
        SA.ModifiedBy,  
        SA.Status  
    FROM @tbProduct T
        INNER JOIN PRProductAttribute PA ON PA.ProductId = T.ProductId  
        INNER JOIN PRProductSKU PS ON PA.ProductId = PS.ProductId  
        INNER JOIN PRProductSKUAttributeValue SA ON (PS.Id = SA.SKUId AND SA.ProductAttributeID = PA.ID)  
        LEFT JOIN ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
    WHERE IsSKULevel = 1   
        AND SA.Status = 1  
        AND PS.Status <= 1 AND PS.IsActive = 1 AND PS.IsOnline = 1

    SELECT
        A.Id,  
        AG.CategoryId,  
        A.AttributeDataType,  
        A.Title,  
        A.Description,  
        A.Sequence,  
        A.Code,  
        A.IsFaceted,  
        A.IsSearched,  
        A.IsSystem,  
        A.IsDisplayed,  
        A.IsEnum,    
        A.IsPersonalized,
        A.CreatedDate,  
        A.CreatedBy,  
        A.ModifiedDate,  
        A.ModifiedBy,  
        A.Status,  
        PA.IsSKULevel,  
        PA.ProductId,  
        A.IsSystem   , 
        A.IsPersonalized,
        AG.CategoryId,
        AG.AttributeGroupId
    FROM @tbProduct T  
        INNER JOIN PRProductAttribute PA ON PA.ProductId = T.ProductId   
        INNER JOIN ATAttribute A ON PA.AttributeId = A.Id   
        LEFT JOIN (
            SELECT CI.AttributeId, MIN(CI.CategoryId) CategoryId ,MIN(C.Name) Title, MIN(C.GroupId) AttributeGroupId
			FROM ATAttributeCategory C
			    INNER JOIN ATAttributeCategoryItem CI ON C.Id = CI.CategoryId
			GROUP BY AttributeId) AG ON A.Id = AG.AttributeId 
    WHERE A.Status = 1
END
GO
PRINT 'Creating Customer_Get'
GO
IF(OBJECT_ID('Customer_Get') IS NOT NULL)
	DROP PROCEDURE Customer_Get
GO
CREATE PROCEDURE [dbo].[Customer_Get]
(
    @Id             uniqueidentifier = null,
    @CustomerIds    xml = null,
    @UserId         uniqueidentifier = null,
    @UserName       nvarchar(256) = null,
    @Email          nvarchar(256) = null,
    @AccountNumber  nvarchar(256) = null,
    @isExpress      bit = null,
    @ApplicationId  uniqueidentifier = null
)
AS
BEGIN
    IF @UserName is null AND @isExpress is null AND @Email is null AND @UserId is null and @AccountNumber is null
    BEGIN
        IF @CustomerIds is null
            SELECT CP.[Id]
                ,CP.[Id] as UserId
                ,U.[FirstName]
                ,U.[LastName]
                ,U.[MiddleName]
                ,U.[CompanyName]
                ,U.[BirthDate]
                ,U.[Gender]
                ,U.[LeadScore]
                ,CP.[IsBadCustomer]
                ,CP.[IsActive]
                ,CP.[IsOnMailingList]
                ,CP.[Status]
                ,CP.[CSRSecurityQuestion]
                ,CP.[CSRSecurityPassword]
                ,U.[HomePhone]
                ,U.[MobilePhone]
                ,U.[OtherPhone]
                ,U.ImageId
                ,CP.[AccountNumber]
                ,CP.[IsExpressCustomer]
                ,CP.ExternalId
                ,CP.ExternalProfileId
                ,dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId) AS ModifiedDate
                ,(SELECT COUNT(*) FROM CSCustomerLogin WHERE CustomerId = ISNULL(@Id, CP.Id)) + 1 AS NumberOfLoginAccounts
                ,(SELECT COUNT(*) FROM PTLineOfCreditInfo WHERE CustomerId = ISNULL(@Id, CP.Id) AND Status != 3) AS HasLOC
            FROM [USCommerceUserProfile] CP
                INNER JOIN USUser U on CP.Id = U.Id
            WHERE (@Id is null or CP.Id = @Id)
                AND U.Status !=3  OPTION(RECOMPILE)
		ELSE
            SELECT CP.[Id]
                ,CP.[Id] as UserId
                ,U.[FirstName]
                ,U.[LastName]
                ,U.[MiddleName]
                ,U.[CompanyName]
                ,U.[BirthDate]
                ,U.[Gender]
                ,U.[LeadScore]
                ,CP.[IsBadCustomer]
                ,CP.[IsActive]
                ,CP.[IsOnMailingList]
                ,CP.[Status]
                ,CP.[CSRSecurityQuestion]
                ,CP.[CSRSecurityPassword]
                ,U.[HomePhone]
                ,U.[MobilePhone]
                ,U.[OtherPhone]
                ,U.ImageId
                ,CP.[AccountNumber]
                ,CP.[IsExpressCustomer]
                ,CP.ExternalId
                ,CP.ExternalProfileId
                ,dbo.ConvertTimeFromUtc(CP.ModifiedDate, @ApplicationId) ModifiedDate
                ,(SELECT COUNT(*) FROM CSCustomerLogin WHERE CustomerId = CP.Id) + 1 AS NumberOfLoginAccounts
                ,(SELECT COUNT(*) FROM PTLineOfCreditInfo WHERE CustomerId = CP.Id AND Status != 3) AS HasLOC
            FROM [USCommerceUserProfile] CP
                INNER JOIN USUser U ON CP.Id = U.Id
                INNER JOIN @CustomerIds.nodes('/GenericCollectionOfGuid/guid') tab(col) ON CP.Id = tab.col.value('text()[1]','uniqueidentifier')
            WHERE U.Status != 3
        END
    ELSE
    BEGIN
        IF @UserId IS NULL
        BEGIN
            DECLARE @UserIdParams nvarchar(max), @UserIdSQL nvarchar(max)
            SET @UserIdParams ='@UserId uniqueidentifier OUTPUT,@UserName nvarchar(256),@Email nvarchar(256),@AccountNumber nvarchar(256),@ApplicationId uniqueidentifier';

            SET @UserIdSQL =
                'SELECT @UserId = U.Id FROM USUser U
                INNER JOIN USMembership M on M.UserId = U.Id
                INNER JOIN USSiteUser US ON US.UserId = U.Id
                LEFT JOIN USCommerceUserProfile UCP ON UCP.Id = U.Id
                WHERE U.Status != 3';

            IF @UserName IS NOT NULL SET @UserIdSQL += ' AND U.LoweredUserName = @UserName';

            IF @Email IS NOT NULL SET @UserIdSQL += ' AND M.Email = @Email';

            IF @AccountNumber IS NOT NULL SET @UserIdSQL += ' AND UCP.AccountNumber = @AccountNumber';

            IF @ApplicationId IS NOT NULL SET @UserIdSQL +=' AND US.SiteID IN (Select SiteId from  [dbo].[GetParentSites](@ApplicationId,1,1))';
							
            EXEC sp_executesql @UserIdSQL, @UserIdParams, @UserId OUTPUT, @UserName, @Email, @AccountNumber, @ApplicationId
        END
                  
        IF (@UserId IS NOT NULL)
        BEGIN
            SELECT CP.[Id]
                ,@UserId as UserId
                ,U.[FirstName]
                ,U.[LastName]
                ,U.[MiddleName]
                ,U.[CompanyName]
                ,U.[BirthDate]
                ,U.[Gender]
                ,U.[LeadScore]
                ,CP.[IsBadCustomer]
                ,CP.[IsActive]
                ,CP.[IsOnMailingList]
                ,CP.[Status]
                ,CP.[CSRSecurityQuestion]
                ,CP.[CSRSecurityPassword]
                ,U.[HomePhone]
                ,U.[MobilePhone]
                ,U.[OtherPhone]
                ,U.ImageId
                ,CP.[AccountNumber]
                ,CP.[IsExpressCustomer]
                ,CP.[ModifiedDate]
                ,CP.ExternalId
                ,CP.ExternalProfileId
                ,dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                ,(SELECT COUNT(*) FROM CSCustomerLogin WHERE CustomerId = ISNULL(@Id, CP.Id)) + 1 AS NumberOfLoginAccounts
                ,(SELECT COUNT(*) FROM PTLineOfCreditInfo WHERE CustomerId = ISNULL(@Id, CP.Id) AND Status != 3) AS HasLOC
            FROM [USCommerceUserProfile] CP
                INNER JOIN USUser U ON CP.Id = U.Id
            WHERE (CP.Id = @UserId OR CP.Id = (SELECT TOP 1 CustomerId FROM CSCustomerLogin WHERE UserId = @UserId))
                AND (@IsExpress is null or IsExpressCustomer = @IsExpress)
                AND U.Status != 3
        END
    END  
END
GO
PRINT 'Creating Warehouse_GetInventoryByXmlGuids'
GO
IF(OBJECT_ID('Warehouse_GetInventoryByXmlGuids') IS NOT NULL)
	DROP PROCEDURE Warehouse_GetInventoryByXmlGuids
GO
CREATE PROCEDURE [dbo].[Warehouse_GetInventoryByXmlGuids]
(
	@Ids				xml,
	@WarehouseTypeId	int,
	@WarehouseId		uniqueidentifier,
	@ApplicationId		uniqueidentifier = null	
)
AS 
BEGIN

	SELECT DISTINCT I.[Id],
		I.[WarehouseId],
		I.[SKUId],
		ISNULL(dbo.GetBundleQuantity(I.SKUId), I.[Quantity]) AS Quantity,
		ISNULL(O.Quantity, 0) AS AllocatedQuantity,
		ISNULL(BO.BackOrderQuantity ,0) AS BackOrderAllocatedQuantity,
		I.[LowQuantityAlert],
		I.[FirstTimeOrderMinimum],
		I.[OrderMinimum],
		I.[OrderIncrement],
		I.[ReasonId],
		I.[ReasonDetail],
		I.[Status],
		I.[CreatedBy],
		I.CreatedDate AS CreatedDate,
		I.[ModifiedBy],
		I.ModifiedDate AS ModifiedDate
	  FROM INInventory I
		INNER JOIN INWarehouse W ON I.WarehouseId = W.Id
		LEFT JOIN dbo.vwAllocatedQuantityperWarehouse O ON I.SKUId = O.ProductSKUId AND O.WarehouseId = W.Id
		LEFT JOIN dbo.vwBackOrderAllocatedQuantityPerWarehouse BO ON I.SKUId = BO.ProductSKUId AND BO.WarehouseId = W.Id
	  WHERE I.Status = 1 
		AND I.SKUId IN (SELECT DISTINCT T.C.value('text()[1]','uniqueidentifier') FROM @Ids.nodes('/GenericCollectionOfGuid/guid') T(C))
		AND (@WarehouseId IS NULL OR  W.Id = @WarehouseId)
		AND W.TypeId = @WarehouseTypeId
END
GO
PRINT 'Creating Cart_Delete'
GO
IF(OBJECT_ID('Cart_Delete') IS NOT NULL)
	DROP PROCEDURE Cart_Delete
GO
CREATE PROCEDURE [dbo].[Cart_Delete]
(
	@Id		uniqueidentifier
)
AS 
BEGIN
	BEGIN TRY
	    BEGIN TRANSACTION;

			Delete from CSCartItem where CartId = @Id
			Delete from CSCart where Id = @Id

			Delete from OROrderItemAttributeValue where CartId = @Id

		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		
		--transaction is uncommittable.
		IF (XACT_STATE()) = -1
		BEGIN
			ROLLBACK TRANSACTION;
		END;

		-- transaction is active and valid.
		IF (XACT_STATE()) = 1
		BEGIN
			COMMIT TRANSACTION;   
		END;
	END CATCH;

END
GO
PRINT 'Creating CartItem_Delete'
GO
IF(OBJECT_ID('CartItem_Delete') IS NOT NULL)
	DROP PROCEDURE CartItem_Delete
GO
CREATE PROCEDURE [dbo].[CartItem_Delete]
(
	@Id		uniqueidentifier,
	@CartId uniqueidentifier
)
AS
BEGIN
	DECLARE @CurrentDate datetime
	SET @CurrentDate = GETUTCDATE()

	IF(@Id IS NULL OR  @Id = dbo.GetEmptyGUID())
	BEGIN
		DELETE FROM CSCartItem WHERE CartId = @CartId

		DELETE FROM OROrderItemAttributeValue WHERE CartId = @CartId
	END
	ELSE
	BEGIN
		DECLARE @ThisItemSequence int, @MaxSequence int

		SELECT @ThisItemSequence = Sequence FROM CSCartItem WHERE Id = @Id
		SELECT @MaxSequence = ISNULL(MAX(Sequence), 1) FROM CSCartItem WHERE CartId = @CartId
	
		DELETE FROM CSCartItem WHERE ParentCartItemId = @Id
		DELETE FROM CSCartItem WHERE Id = @Id

		DELETE FROM OROrderItemAttributeValue WHERE CartItemId = @Id
		
		IF @ThisItemSequence < @MaxSequence
		BEGIN	
			UPDATE CSCartItem SET Sequence = Sequence - 1 
			WHERE Sequence > @ThisItemSequence and CartId = @CartId			
		END
	END
		
	UPDATE CSCart Set ModifiedDate = @CurrentDate WHERE Id = @CartId
END
GO
PRINT 'Creating ContactDto_GetEmail'
GO
IF(OBJECT_ID('ContactDto_GetEmail') IS NOT NULL)
	DROP PROCEDURE ContactDto_GetEmail
GO
CREATE PROCEDURE [dbo].[ContactDto_GetEmail]
(
	@Id	uniqueidentifier
)
AS
BEGIN
	SELECT Email FROM MKContact WHERE Id = @Id

	UNION ALL

	SELECT Email FROM USMembership WHERE UserId = @Id
END
GO
PRINT 'Creating ContactDto_Unsubscribe'
GO
IF(OBJECT_ID('ContactDto_Unsubscribe') IS NOT NULL)
	DROP PROCEDURE ContactDto_Unsubscribe
GO
CREATE PROCEDURE [dbo].[ContactDto_Unsubscribe]
(
	@Email		nvarchar(2000),
	@SendId		uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UserId uniqueidentifier, @CampaignId uniqueidentifier, @OptOutOfAllCampaigns nvarchar(10), @OptOutDate datetime
	IF @SendId = dbo.GetEmptyGUID()
		SET @SendId = NULL

	SET @OptOutDate = GETUTCDATE()
	IF @SendId IS NOT NULL
		SELECT @UserId = UserId, @CampaignId = CampaignId FROM MKEmailSendLog WHERE Id = @SendId

	IF @UserId IS NULL AND @Email IS NOT NULL AND LTRIM(RTRIM(@Email)) != ''
		SELECT @UserId = Id FROM vw_contacts WHERE Email like @Email

	IF @UserId IS NULL RETURN

	SELECT	TOP 1  @OptOutOfAllCampaigns = Value 
	FROM STSiteSetting AS SV
		JOIN STSettingType AS ST ON SV.SettingTypeId = ST.Id
	WHERE ST.Name='OptOutOfAllCampaigns'

	IF @OptOutOfAllCampaigns IS NOT NULL AND LOWER(@OptOutOfAllCampaigns) = 'true'
		SET @CampaignId = NULL

	DELETE FROM USUserUnsubscribe WHERE UserId = @UserId

	DELETE FROM USResubscribe WHERE UserId = @UserId

	INSERT INTO USUserUnsubscribe (Id, UserId, CampaignId, OptOutDate)
	SELECT NEWID(), @UserId, @CampaignId, @OptOutDate

	INSERT INTO USUnsubscribeHistory (Id, Userid, [Action], ActionDate, CampaignId)
	SELECT NEWID(), @UserId, 'Unsubscribe', @OptOutDate, @CampaignId

	IF @SendId IS NOT NULL
		UPDATE MKEmailSendLog SET Unsubscribed = 1 WHERE Id = @SendId
END
GO
PRINT 'Creating UserDto_Get'
GO
IF(OBJECT_ID('UserDto_Get') IS NOT NULL)
	DROP PROCEDURE UserDto_Get
GO
CREATE PROCEDURE [dbo].[UserDto_Get]  
(  
	@Id						uniqueidentifier = NULL,  
	@SiteId					uniqueidentifier,
	@ProductId				uniqueidentifier = NULL,
	@IgnoreSite				bit = NULL,   
	@IgnoreProduct			bit = NULL,   
	@IsSystemUser			bit = NULL,   
	@IsEditable				bit = NULL,   
	@Status					int = NULL,   
	@Role					int = NULL,   
	@PageSize				int = NULL,   
	@PageNumber				int = NULL,
	@MaxRecords				int = NULL,
	@Keyword				nvarchar(MAX) = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL
)  
AS  
BEGIN
	DECLARE @EmptyGuid uniqueidentifier, @PageLowerBound int, @PageUpperBound int, @TotalRecords int,
		@SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @Id = @EmptyGuid SET @Id = NULL
	IF @IsSystemUser IS NULL SET @IsSystemUser = 1
	IF @Status = 0 SET @Status = NULL
	IF @Role = 0 SET @Role = NULL

	IF @Role = -1 -- All Users
	BEGIN
		SET @Role = NULL
		SET @IgnoreProduct = 1
		SET @IgnoreSite = 1
	END

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)

	IF @Id IS NULL	
	BEGIN
		DECLARE @tbSiteIds TABLE(Id uniqueidentifier primary key)
		IF (@IgnoreSite = 0 AND @SiteId IS NOT NULL)
		BEGIN
			INSERT INTO @tbSiteIds
			SELECT DISTINCT SiteId FROM [dbo].[GetAncestorSitesForSystemUser](@SiteId, @IsSystemUser, null)
		END

		DECLARE @tbRoleUsers TABLE(Id uniqueidentifier primary key)
		IF (@Role IS NOT NULL)
		BEGIN
			INSERT INTO @tbRoleUsers
			SELECT Id FROM [dbo].[User_GetUsersInRole] (@Role, 1, @SiteId, @IsSystemUser, NULL, @ProductId)
		END

		INSERT INTO @tbIds
		SELECT U.Id AS Id, ROW_NUMBER() OVER (ORDER BY
			CASE WHEN @SortClause = 'FirstName ASC' THEN U.FirstName END ASC,
			CASE WHEN @SortClause = 'FirstName DESC' THEN U.FirstName END DESC,
			CASE WHEN @SortClause = 'LastName ASC' THEN U.LastName END ASC,
			CASE WHEN @SortClause = 'LastName DESC' THEN U.LastName END DESC,
			CASE WHEN @SortClause = 'Email ASC' THEN U.Email END ASC,
			CASE WHEN @SortClause = 'Email DESC' THEN U.Email END DESC,
			CASE WHEN @SortClause = 'UserName ASC' THEN U.UserName END ASC,
			CASE WHEN @SortClause = 'UserName DESC' THEN U.UserName END DESC,
			CASE WHEN @SortClause IS NULL THEN U.UserName END ASC		
		) AS RowNumber
		FROM vwUser AS U
		WHERE (U.IsSystemUser = @IsSystemUser) AND
			(@Status IS NULL OR U.[Status] = @Status) AND
			(@IsEditable IS NULL OR U.IsEditable = @IsEditable) AND
			(@Keyword IS NULL OR U.FirstName like @Keyword OR U.LastName like @Keyword OR U.UserName like @Keyword OR U.Email like @Keyword) AND
			(@IgnoreSite = 1 OR @SiteId IS NULL OR EXISTS (SELECT 1 FROM vwUserSites US JOIN @tbSiteIds T ON US.SiteId = T.Id WHERE U.Id = US.UserId)) AND
			(@IgnoreProduct = 1 OR @ProductId IS NULL OR EXISTS (SELECT 1 FROM vwUserProducts UP WHERE U.Id = UP.UserId AND UP.ProductId = @ProductId)) AND
			(@Role IS NULL OR EXISTS (SELECT 1 FROM @tbRoleUsers WHERE Id = U.Id)) 

		SELECT @TotalRecords = COUNT(1) FROM @tbIds
	
		INSERT INTO @tbPagedResults
		SELECT T.Id, T.RowNumber, @TotalRecords 
		FROM @tbIds T
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
			AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	END
	ELSE
	BEGIN
		INSERT INTO @tbPagedResults
		SELECT @Id, 1, 1
	END

	SELECT U.Id,
		U.FirstName,
		U.LastName,
		U.MiddleName,
		U.UserName,
		M.Email,
		U.LastActivityDate,
		U.EmailNotification,
		U.LeadScore,
		U.ExpirationDate,
		U.[Status],
		T.TotalRecords
	FROM vwUser U
		JOIN @tbPagedResults T ON U.Id = T.Id
		JOIN USMembership M ON U.Id = M.UserId
	ORDER BY T.RowNumber
END
GO
PRINT 'Creating Membership_CreateUser'
GO
IF(OBJECT_ID('Membership_CreateUser') IS NOT NULL)
	DROP PROCEDURE Membership_CreateUser
GO
CREATE PROCEDURE [dbo].[Membership_CreateUser]
	@ProductId								uniqueidentifier = NULL,
	@ApplicationId			                uniqueidentifier,
	@UserName                               nvarchar(256),
	@Password                               nvarchar(128),
	@PasswordSalt                           nvarchar(128),
	@Email                                  nvarchar(256),
	@PasswordQuestion                       nvarchar(256),
	@PasswordAnswer                         nvarchar(128),
	@IsApproved                             bit,
	@UniqueEmail                            int      = 0,
	@PasswordFormat                         int      = 0,
	@CreatedBy								uniqueidentifier,
	@IsSystemUser							bit		 = 0, --0 means website user
	@UserProfile							xml=null,
	@UserId                                 uniqueidentifier OUTPUT,
	@CreatedDate							Datetime	OUTPUT,
	@OverrideDeletedUser					bit=0,
	@EmailNotification						[bit]= NULL,
	@ExpiryDate								[datetime]= '9999-12-31',
	@TimeZone								[nvarchar](100)= NULL,
	@FirstName								[nvarchar](256)= NULL,
	@MiddleName								[nvarchar](256)= NULL,
	@LastName								[nvarchar](256)= NULL,
	@ReportRangeSelection					[varchar](50)= NULL,
	@ReportStartDate						[datetime] =NULL,
	@ReportEndDate							[datetime] =NULL,
	@BirthDate								[datetime]= NULL,
	@CompanyName							[nvarchar](1024)= NULL,
	@Gender									[nvarchar](50)= NULL,
	@HomePhone								[varchar](50) =NULL,
	@MobilePhone							[varchar](50)= NULL,
	@OtherPhone								[varchar](50) =NULL,
	@ImageId								uniqueidentifier = null,
	@LeadScore								int = NULL
AS
BEGIN  
    DECLARE @CurrentTimeUtc datetime  
	SET @CurrentTimeUtc = GetUtcDate()  
  
    DECLARE @NewUserId uniqueidentifier  
    SET  @NewUserId = NULL  
  
    DECLARE @IsLockedOut bit  
    SET  @IsLockedOut = 0  
  
    DECLARE @LastLockoutDate  datetime  
    SET  @LastLockoutDate = CONVERT( datetime, '17540101', 112 )  
  
    DECLARE @FailedPasswordAttemptCount int  
    SET  @FailedPasswordAttemptCount = 0  
  
    DECLARE @FailedPasswordAttemptWindowStart  datetime  
    SET  @FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 )  
  
    DECLARE @FailedPasswordAnswerAttemptCount int  
    SET  @FailedPasswordAnswerAttemptCount = 0  
  
    DECLARE @FailedPasswordAnswerAttemptWindowStart  datetime  
    SET  @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )  
  
    DECLARE @NewUserCreated bit  
    DECLARE @ReturnValue   int  
    SET  @ReturnValue = 0  
  
    DECLARE @ErrorCode     int  
    SET  @ErrorCode = 0  
  
    DECLARE @TranStarted   bit  
    SET  @TranStarted = 0  

	DECLARE @isContact bit
	SET @isContact =0
   
	IF @ExpiryDate IS NULL
		SET @ExpiryDate = '9999-12-31'

	IF(@ReportRangeSelection is null)
	BEGIN 
		SET @ReportRangeSelection = 'MonthToDate'
		SET @ReportStartDate= GETUTCDATE() 
		SET @ReportEndDate= GETUTCDATE()
	END
    
	DECLARE @Title nvarchar(256)  
	DECLARE @Description nvarchar(1024)  
	DECLARE @MobilePIN   nvarchar(16)  
  
	DECLARE @UserNameTemp nvarchar(256)  
	SET @UserNameTemp = NULL  
  
	IF( @@TRANCOUNT = 0 )  
	BEGIN  
		BEGIN TRANSACTION  
		SET @TranStarted = 1  
	END  
	ELSE  
		SET @TranStarted = 0  
  
	 --Check whether the username is already exist for the given application.  
	 --It is not possible to use expired,deactivated,not approved usernames.   
	 IF (@IsSystemUser = 0)   
	 BEGIN  
	  -- Website user  
		SELECT  @UserNameTemp = UserName FROM dbo.USUser u, dbo.USSiteUser s  
			WHERE LOWER(@UserName) = LoweredUserName AND   
			--@ApplicationId = SiteId AND  
			(SiteId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) AND
			Id = UserId  and u.status not in (dbo.GetDeleteStatus())  
	 END  
	 ELSE   
	 BEGIN  
		-- cms user  
		SELECT  @UserNameTemp = UserName FROM dbo.USUser u, dbo.USSiteUser s  
			WHERE LOWER(@UserName) = LoweredUserName AND   
			--@ApplicationId = SiteId AND  
			Id = UserId and u.status not in (dbo.GetDeleteStatus())  
	 END  
   
	--If the username is not exist, then return error status.  
	IF (@UserNameTemp IS NOT NULL)  
	BEGIN  
		SET @ErrorCode = 6  
		GOTO Cleanup    
	END  
  
	IF (@UserId IS NULL OR NOT EXISTS(SELECT Id FROM USUser WHERE Id = @UserId AND Status = dbo.GetActiveStatus()))  
	BEGIN 
		DECLARE @deletedUserId uniqueidentifier  
		SET @deletedUserId = NULL  
		IF (@OverrideDeletedUser = 1)  
			SELECT TOP 1 @deletedUserId = Id FROM USUser u 
				LEFT JOIN USSiteUser su ON u.Id= su.UserId 
			WHERE u.LoweredUserName = LOWER(@UserName) AND   
					(su.SiteId is NULL OR	su.SiteId=@ApplicationId) AND u.status = dbo.GetDeleteStatus() 
			ORDER BY u.LastActivityDate DESC  
      

		IF (@deletedUserId IS NULL)  
		BEGIN  

		IF EXISTS(SELECT Id FROM MKContact WHERE Email = @Email)
			SET @UserId = (SELECT top 1 Id FROM MKContact Where Email=@Email and Status=1)
		
		IF @UserId is null
			SET @UserId = NEWID()  
		ELSE 
			SET @isContact=1

		INSERT INTO dbo.USUser   
		(  
		Id  
		,Title  
		,Description  
		,CreatedBy  
		,CreatedDate  
		,ModifiedBy  
		,ModifiedDate  
		,Status  
		,UserName  
		,LoweredUserName  
		,IsAnonymous  
		,LastActivityDate 
		,[EmailNotification]
		,[ExpiryDate]
		,[TimeZone]
		,[FirstName]
		,[MiddleName]
		,[LastName]
		,[ReportRangeSelection]
		,[ReportStartDate]
		,[ReportEndDate]
		,[BirthDate]
		,[CompanyName]
		,[Gender]
		,[HomePhone]
		,[MobilePhone]
		,[OtherPhone] 
		,[ImageId]
		,[LeadScore]
		)  
		VALUES  
		(  
		@UserId  
		,@Title  
		,@Description  
		,@CreatedBy  
		,@CurrentTimeUtc  
		,@CreatedBy  
		,@CurrentTimeUtc  
		,dbo.GetActiveStatus() --Active  
		,@UserName  
		,LOWER(@UserName)  
		,0 --False, Authenticated user  
		,@CurrentTimeUtc 
		,@EmailNotification
		,@ExpiryDate
		,@TimeZone
		,@FirstName
		,@MiddleName
		,@LastName
		,@ReportRangeSelection
		,@ReportStartDate
		,@ReportEndDate
		,@BirthDate
		,@CompanyName
		,@Gender
		,@HomePhone
		,@MobilePhone
		,@OtherPhone  
		,@ImageId
		,@LeadScore
		)  
		SET @NewUserCreated = 1  
		SET @CreatedDate = @CurrentTimeUtc  
		END  
		ELSE  
		BEGIN  
			UPDATE dbo.USUser  
			SET Title = @Title  
				,Description = @Description  
				,ModifiedBy = @CreatedBy  
				,ModifiedDate = @CurrentTimeUtc  
				,Status = dbo.GetActiveStatus()  
				,LastActivityDate = @CurrentTimeUtc 
				,EmailNotification = @EmailNotification
				,ExpiryDate = @ExpiryDate
				,TimeZone = @TimeZone
				,FirstName = @FirstName
				,MiddleName = @MiddleName
				,LastName = @LastName
				,ReportRangeSelection = @ReportRangeSelection
				,ReportStartDate = @ReportStartDate
				,ReportEndDate = @ReportEndDate
				,BirthDate = @BirthDate
				,CompanyName = @CompanyName
				,Gender = @Gender
				,HomePhone = @HomePhone
				,MobilePhone = @MobilePhone
				,OtherPhone = @OtherPhone 
				,ImageId = @ImageId
				,LeadScore = ISNULL(@LeadScore, LeadScore)
			WHERE Id = @deletedUserId  

			SET @UserId = @deletedUserId  
		END  
    END  
  
    IF( @@ERROR <> 0 )  
    BEGIN  
        SET @ErrorCode = -1  
        GOTO Cleanup  
    END  
  
    IF( @ReturnValue = -1 )  
    BEGIN  
        SET @ErrorCode = 10  
        GOTO Cleanup  
    END  
  
	
	IF NOT EXISTS(Select 1 FROM USSiteUser Where SiteId=@ApplicationId AND UserId=@UserId AND ProductId=@ProductId)
	INSERT INTO dbo.USSiteUser  
		(  
			SiteId,  
			UserId,  
			IsSystemUser,  
			ProductId  
		)  
		VALUES  
		(  
			@ApplicationId,  
			@UserId,  
			@IsSystemUser,  
			@ProductId  
		)  

		IF (@isContact=1)
		BEGIN
			
			DECLARE @MarketierProductId uniqueidentifier
			SET @MarketierProductId='CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E'

			INSERT INTO dbo.USSiteUser  
		(  
			SiteId,  
			UserId,  
			IsSystemUser,  
			ProductId,
			IsPrimarySite
		)  
		select 
		SiteId 
		,@UserId
		,@IsSystemUser
		,@MarketierProductId
		,0 
		from MKContactSite
		Where SiteId!=@ApplicationId AND ContactId=@UserId

		DELETE FROM MKContactSite Where ContactId=@UserId
		DELETE FROM MKContact Where Id =@UserId

		END
    
  
		IF(@@ERROR <> 0)   
		BEGIN  
			SET @ErrorCode = -1  
			GOTO Cleanup  
		END  
  
		IF (@UniqueEmail = 1 AND @NewUserCreated=1)  
		BEGIN  
			IF (EXISTS (SELECT *  
			  FROM  dbo.USMembership m, dbo.USSiteUser z,dbo.USUser u   WITH ( UPDLOCK, HOLDLOCK )  
			  WHERE (SiteId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) AND u.Id = z.UserId  AND  m.userid=z.userid AND  
				LoweredEmail = LOWER(@Email) AND  u.status not in (dbo.GetDeleteStatus())))
			BEGIN  
				SET @ErrorCode = 7  
				GOTO Cleanup  
			END  
		END  
  
  	IF NOT EXISTS (SELECT UserId FROM dbo.USMembership WHERE  @UserId = UserId)
		INSERT INTO dbo.USMembership  
		(   
			UserId,  
			Password,  
			PasswordSalt,  
			Email,  
			LoweredEmail,  
			PasswordQuestion,  
			PasswordAnswer,  
			PasswordFormat,  
			MobilePIN,   
			IsApproved,  
			IsLockedOut,  
			LastLoginDate,  
			LastPasswordChangedDate,  
			LastLockoutDate,  
			FailedPasswordAttemptCount,  
			FailedPasswordAttemptWindowStart,  
			FailedPasswordAnswerAttemptCount,  
			FailedPasswordAnswerAttemptWindowStart   
		)  
		VALUES (   
			@UserId,  
			@Password,  
			@PasswordSalt,  
			@Email,  
			LOWER(@Email),  
			@PasswordQuestion,  
			@PasswordAnswer,  
			@PasswordFormat,  
			@MobilePIN,   
			@IsApproved,  
			@IsLockedOut,  
			NULL,  
			@CurrentTimeUtc,  
			@LastLockoutDate,  
			@FailedPasswordAttemptCount,  
			@FailedPasswordAttemptWindowStart,  
			@FailedPasswordAnswerAttemptCount,  
			@FailedPasswordAnswerAttemptWindowStart   
		)  
	  
	ELSE
	
		UPDATE dbo.USMembership  
		SET Password = @Password,  
			PasswordSalt = @PasswordSalt ,  
			Email = @Email,  
			LoweredEmail = LOWER(@Email),  
			PasswordQuestion = @PasswordQuestion,  
			PasswordAnswer = @PasswordAnswer,  
			PasswordFormat = @PasswordFormat,  
			MobilePIN = @MobilePIN,   
			IsApproved = @IsApproved,  
			IsLockedOut = @IsLockedOut,  
			LastLoginDate = null,  
			LastPasswordChangedDate = @CurrentTimeUtc,  
			LastLockoutDate = @LastLockoutDate,  
			FailedPasswordAttemptCount = @FailedPasswordAttemptCount,  
			FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,  
			FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,  
			FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart  
		WHERE UserId = @deletedUserId     
    
  
    IF( @@ERROR <> 0 )  
    BEGIN  
        SET @ErrorCode = -1  
        GOTO Cleanup  
    END  
   
	--Process the Profile data  
	IF (@UserProfile IS NOT NULL)  
	BEGIN  
		DELETE FROM dbo.USMemberProfile WHERE UserId = @UserId  
		INSERT INTO dbo.USMemberProfile  
		(  
			UserId,  
			PropertyName,  
			PropertyValueString,  
			PropertyValuesBinary,  
			LastUpdatedDate  
		)  
		SELECT   
			@UserId,  
			tab.col.value('propertyName[1]/text()[1]','nvarchar(256)') As PropertyName,  
			tab.col.value('propertyValueString[1]/text()[1]','nvarchar(1024)') AS PropertyValueString,  
			tab.col.value('propertyValuesBinary[1]/text()[1]','varbinary(max)') AS PropertyValuesBinary,  
			@CurrentTimeUtc  
		FROM @UserProfile.nodes('//userProfile/profile') tab(col)  
		WHERE tab.col.value('propertyName[1]/text()[1]','nvarchar(256)') 
		NOT IN
		(
			'EmailNotification'
			,'ExpiryDate'
			,'TimeZone'
			,'FirstName'
			,'MiddleName'
			,'LastName'
			,'ReportRangeSelection'
			,'ReportStartDate'
			,'ReportEndDate'
			,'BirthDate'
			,'CompanyName'
			,'Gender'
			,'HomePhone'
			,'MobilePhone'
			,'OtherPhone'
			,'ImageId'
			,'LeadScore'
		)
	END  
  
	IF( @TranStarted = 1 )  
	BEGIN  
		SET @TranStarted = 0  
		COMMIT TRANSACTION  
	END  
  
    RETURN 0  
  
Cleanup:  
    IF( @TranStarted = 1 )  
    BEGIN  
        SET @TranStarted = 0  
		ROLLBACK TRANSACTION  
    END  
  
    RETURN @ErrorCode  
END 
GO

PRINT 'Creating AttributeGroup_GetGroup'
GO
IF(OBJECT_ID('AttributeGroup_GetGroup') IS NOT NULL)
	DROP PROCEDURE AttributeGroup_GetGroup
GO
CREATE PROCEDURE [dbo].[AttributeGroup_GetGroup](
--********************************************************************************
-- Parameters Passing in Attribute Group ID
--********************************************************************************
			@Id uniqueidentifier = null,
			@IsSystem bit =null,
			@ApplicationId uniqueidentifier=null
)
AS
BEGIN

	SELECT 
		GroupId Id,
		Name Title,
		SiteId,
		IsSystem,
		CreatedDate,
		CreatedBy,
		ModifiedDate,
		ModifiedBy,
		[Status],
		Id CategoryId
	FROM 
		ATAttributeCategory    
	WHERE
		--[Id]=	Isnull(@Id,[Id])
		--and IsSystem =isnull(@IsSystem, IsSystem)
        (@Id is null or [GroupId] = @Id)
		and (@IsSystem is null or IsSystem = @IsSystem)
		AND
        [Status] = dbo.GetActiveStatus() -- Select only Active Attributes
		and (IsHidden is null or IsHidden=0)
	ORDER BY SiteId,Title, IsSystem

	SELECT 
		A.Id,
		C.GroupId AttributeGroupId,
		AttributeDataType,
		AttributeDataTypeId,
		Title,
		A.[Description],
		Sequence,
		Code,
		IsFaceted,
		IsSearched,
		A.IsSystem,
		IsDisplayed,
		IsEnum,
		A.CreatedDate,
		A.CreatedBy,
		A.ModifiedDate,
		A.ModifiedBy,
		A.[Status],
		IsPersonalized
	FROM ATAttribute A
	INNER JOIN ATAttributeCategoryItem CI ON A.Id=CI.AttributeId
	INNER JOIN ATAttributeCategory C on CI.CategoryId=C.Id
	WHERE --AttributeGroupId= Isnull(@Id, AttributeGroupId )
			(@Id is null or C.GroupId = @Id)
			AND
			A.[Status]	= dbo.GetActiveStatus() -- Select only Active Attributes
	Order By AttributeGroupId,Sequence, Title
END


GO

PRINT 'Creating ProductAttribute_GetAttribute'
GO
IF(OBJECT_ID('ProductAttribute_GetAttribute') IS NOT NULL)
	DROP PROCEDURE ProductAttribute_GetAttribute
GO
CREATE PROCEDURE [dbo].[ProductAttribute_GetAttribute](
--********************************************************************************
-- Parameters
--********************************************************************************
			@Id uniqueidentifier = null,
			@OnlySystem bit = 0,
			@ApplicationId uniqueidentifier = null,
			@CategoryId int= null,
			@GroupId uniqueidentifier=null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
BEGIN

	if(Isnull(@OnlySystem,0) =0)
		BEGIN
			SELECT 
				a.Id,
				ag.CategoryId,		
				ag.GroupId AttributeGroupId,
				a.AttributeDataType,
				a.AttributeDataTypeId,
				a.Title,
				a.Description,
				a.Sequence,
				a.Code,
				a.IsFaceted,
				a.IsSearched,
				a.IsSystem,
				a.IsDisplayed,
				a.IsEnum,	
				dbo.ConvertTimeFromUtc(a.CreatedDate,@ApplicationId)CreatedDate,
				a.CreatedBy,
				dbo.ConvertTimeFromUtc(a.ModifiedDate,@ApplicationId)ModifiedDate,
				a.ModifiedBy,
				a.Status,
				ag.Title as [AttributeGroupName],IsPersonalized
			FROM 
				ATAttribute a LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) GroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Where c.IsHidden is null or C.IsHidden =0
										Group By AttributeId) ag on a.Id=ag.AttributeId 
			WHERE	
				--a.[Id]=	Isnull(@Id,a.[Id])	
				(@Id is null or a.[Id] = @Id)
				AND
				a.Status = dbo.GetActiveStatus() -- Select only Active Attributes
				--AND 
				--ag.SiteId = IsNull(@ApplicationId, ag.SiteId)
				--(@ApplicationId is null or ag.SiteId = @ApplicationId)
				AND
				--ag.Id =isnull(@CategoryId,ag.Id)
				(@CategoryId is null or ag.CategoryId = @CategoryId)
				AND
				(@GroupId is null or ag.GroupId=@GroupId)
			ORDER BY a.Sequence,a.Title
		END
	ELSE
		BEGIN
			SELECT 
				a.Id,
				ag.CategoryId,			
				a.AttributeDataType,
				a.AttributeDataTypeId,
				a.Title,
				a.Description,
				a.Sequence,
				a.Code,
				a.IsFaceted,
				a.IsSearched,
				a.IsSystem,
				a.IsDisplayed,
				a.IsEnum,
				dbo.ConvertTimeFromUtc(a.CreatedDate,@ApplicationId),
				a.CreatedBy,
				dbo.ConvertTimeFromUtc(a.ModifiedDate,@ApplicationId)ModifiedDate,
				a.ModifiedBy,
				a.Status,
				ag.Title as [AttributeGroupName],IsPersonalized
			FROM 
				ATAttribute a LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) GroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
			WHERE	
				--a.[Id]=	Isnull(@Id,a.[Id])	
				(@Id is null or a.[Id] = @Id)
				AND
				a.Status = dbo.GetActiveStatus() -- Select only Active Attributes
				AND 
				a.IsSystem = 1
				--AND 
				--ag.SiteId = IsNull(@ApplicationId, ag.SiteId)
				--(@ApplicationId is null or ag.SiteId = @ApplicationId)
				AND
				--ag.Id =isnull(@CategoryId,ag.Id)
				(@CategoryId is null or ag.CategoryId = @CategoryId)
				AND
				(@GroupId is null or ag.GroupId=@GroupId)
			ORDER BY a.Sequence,a.Title

		END
END



GO


PRINT N'Dropping [dbo].[GenerateXml]...';
GO
DROP AGGREGATE [dbo].[GenerateXml];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsImagePropertiesMatch]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_IsImagePropertiesMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsLinkInContentDefinition]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_IsLinkInContentDefinition];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsPageNameMatch]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_IsPageNameMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsPagePropertiesMatch]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_IsPagePropertiesMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsTextContentMatch]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_IsTextContentMatch];


GO
PRINT N'Dropping [dbo].[NavFilter_GetQuery]...';


GO
DROP FUNCTION [dbo].[NavFilter_GetQuery];


GO
PRINT N'Dropping [dbo].[Regex_GetMatch]...';


GO
DROP FUNCTION [dbo].[Regex_GetMatch];


GO
PRINT N'Dropping [dbo].[Regex_IsMatch]...';


GO
DROP FUNCTION [dbo].[Regex_IsMatch];


GO
PRINT N'Dropping [dbo].[Regex_ReplaceMatches]...';


GO
DROP FUNCTION [dbo].[Regex_ReplaceMatches];


GO
PRINT N'Dropping [dbo].[RegexSelectAll]...';


GO
DROP FUNCTION [dbo].[RegexSelectAll];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetContentLocation]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_GetContentLocation];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedFileProperty]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedFileProperty];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedImageProperty]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedImageProperty];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedPageName]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedPageName];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedPageProperties]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedPageProperties];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedTextContentText]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedTextContentText];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsContentDefinitionMatch]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_IsContentDefinitionMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsFilePropertiesMatch]...';


GO
DROP FUNCTION [dbo].[FindAndReplace_IsFilePropertiesMatch];


GO
PRINT N'Dropping [dbo].[GetRelationTable]...';


GO
DROP FUNCTION [dbo].[GetRelationTable];


GO
PRINT N'Dropping [dbo].[Regex_GetMatches]...';


GO
DROP FUNCTION [dbo].[Regex_GetMatches];


GO
PRINT N'Dropping [dbo].[CLRPageMap_SavePageDefinition]...';


GO
DROP PROCEDURE [dbo].[CLRPageMap_SavePageDefinition];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_AddContainer]...';


GO
DROP PROCEDURE [dbo].[CLRPageMapNode_AddContainer];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_RemoveContainer]...';


GO
DROP PROCEDURE [dbo].[CLRPageMapNode_RemoveContainer];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_Save]...';


GO
DROP PROCEDURE [dbo].[CLRPageMapNode_Save];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_Update]...';


GO
DROP PROCEDURE [dbo].[CLRPageMapNode_Update];


GO
PRINT N'Dropping [dbo].[CLSPageMapNode_AttachWorkflow]...';


GO
DROP PROCEDURE [dbo].[CLSPageMapNode_AttachWorkflow];


GO
PRINT N'Dropping [dbo].[Contact_GetAutoDistributionListContactCount]...';


GO
DROP PROCEDURE [dbo].[Contact_GetAutoDistributionListContactCount];


GO
PRINT N'Dropping [dbo].[Contact_SearchCLR]...';


GO
DROP PROCEDURE [dbo].[Contact_SearchCLR];


GO
PRINT N'Dropping [dbo].[ContactList_UpdateContactCountCLR]...';


GO
DROP PROCEDURE [dbo].[ContactList_UpdateContactCountCLR];


GO
PRINT N'Dropping [dbo].[FindAndReplace_Find]...';


GO
DROP PROCEDURE [dbo].[FindAndReplace_Find];


GO
PRINT N'Dropping [dbo].[FindAndReplace_Replace]...';


GO
DROP PROCEDURE [dbo].[FindAndReplace_Replace];


GO
PRINT N'Dropping [dbo].[FindAndReplace_UpdateLinks]...';


GO
DROP PROCEDURE [dbo].[FindAndReplace_UpdateLinks];


GO
PRINT N'Dropping [Bridgeline.CLRFunctions]...';


GO
DROP ASSEMBLY [Bridgeline.CLRFunctions];


GO
PRINT N'Creating [Bridgeline.CLRFunctions]...';
GO
CREATE ASSEMBLY [Bridgeline.CLRFunctions]
    FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500004C010300EA3D545E0000000000000000E00022200B0130000070010000080000000000006A8F01000020000000A001000000001000200000000200000400000000000000040000000000000000E001000002000000000000030040850000100000100000000010000010000000000000100000000000000000000000188F01004F00000000A00100180400000000000000000000000000000000000000C001000C000000E08D01001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000080000000000000000000000082000004800000000000000000000002E74657874000000706F0100002000000070010000020000000000000000000000000000200000602E727372630000001804000000A001000006000000720100000000000000000000000000400000402E72656C6F6300000C00000000C00100000200000078010000000000000000000000000040000042000000000000000000000000000000004C8F0100000000004800000002000500FCBF0000E4CD00000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000360002731300000A7D010000042A0000133003002800000001000011000F01281400000A16FE010A062C18027B0100000472010000700F01281500000A6F1600000A262A5200027B01000004037B010000046F1700000A262A00000013300300390000000200001100027B010000041672230000706F1800000A26027B0100000472570000706F1900000A26027B010000046F1A00000A731B00000A0A2B00062A4E0002036F1C00000A731D00000A7D010000042A520003027B010000046F1A00000A6F1E00000A002A00001B300800B109000003000011007E1F00000A0A0F02FE16140000016F1A00000A282000000A16FE01130411042C70000F02FE16140000016F1A00000A0A0F02FE16140000016F1A00000A728D000070282100000A130511052C1472990000700E048C1B000001282200000A0A2B2F0F02FE16140000016F1A00000A7246010070282100000A130611062C12725A0100700E048C1B000001282200000A0A00721F020070028C1B000001060E048C1B000001282300000A0B72CC020070028C1B000001060E048C1B000001282300000A0C7E1F00000A0D72ED030070732400000A130700732500000A1308732500000A1309721D0400700F00282600000A8C30000001282200000A130A110A1107732700000A130B11076F2800000A00110B110A6F2900000A00110B732A00000A130C110C11086F2B00000A267298070070130A110B110A6F2900000A00110C11096F2B00000A2611086F2C00000A6F2D00000A11096F2C00000A6F2D00000A5817588D45000001130D110D167E1F00000AA27E1F00000A1310161311722B0C00700F01281500000A722B0C0070282E00000A13100F01281500000A6F2F00000A13142B68001110722B0C00701214283000000A283100000A722F0C00701214283000000A72350C0070282E00000A6F3200000A13101110723B0C00701214283000000A283100000A723F0C00701214283000000A72350C0070282E00000A6F3200000A131000111417591314111417FE0416FE01131511152D8A72450C00701110283100000A13101713120011086F2C00000A6F3300000A1316384E06000011166F3400000A74210000011317001117724D0C00706F3500000AA54B000001130E111772690C00706F3500000AA54B000001130F0011096F2C00000A6F3300000A131A38E0050000111A6F3400000A7421000001131B00111772850C00706F3500000A6F1A00000A6F3600000A111B729D0C00706F3500000A6F1A00000A6F3600000A282100000A2C29111772A70C00706F3500000A6F1A00000A111B72B70C00706F3500000A6F1A00000A282100000A2B0116131C111C396A05000000111B72BD0C00706F3500000A6F1A00000A6F3700000A1319111772E10C00706F3500000A6F1A00000A72F30C007072F70C00706F3200000A1318111772FD0C00706F3500000A6F1A00000A6F3700000A6F3600000A72050D0070282100000A131D111D2C13720F0D00701118720F0D0070282E00000A1318111772FD0C00706F3500000A6F1A00000A6F3700000A6F3600000A72130D0070282100000A131E111E2C13720F0D00701118720F0D0070282E00000A1318111B72250D00706F3500000A6F1A00000A723B0D0070282100000A131F111F395B020000001119723F0D0070282100000A2D2A111972470D0070282100000A2D1C111972570D0070282100000A2D0E111972650D0070282100000A2B0117132011202C71110D11121D8D45000001251672750D0070A22517111B729D0C00706F3500000A6F1A00000A72F30C007072F70C00706F3200000AA2251872810D0070A22519111772FD0C00706F3500000A6F1A00000AA2251A722B0C0070A2251B1118A2251C722B0C0070A2283800000AA2389E010000111972990D0070282100000A2D0E111972A30D0070282100000A2B0117132111212C71110D11121D8D45000001251672B50D0070A22517111B729D0C00706F3500000A6F1A00000A72F30C007072F70C00706F3200000AA2251872D90D0070A22519111772FD0C00706F3500000A6F1A00000AA2251A72E90D0070A2251B1118A2251C72310E0070A2283800000AA2380A01000000111B729D0C00706F3500000A6F1A00000A6F3700000A72430E0070282100000A132211222C7E110D11121C8D450000012516111B729D0C00706F3500000A6F1A00000A72F30C007072F70C00706F3200000AA22517722B0C0070A22518111772FD0C00706F3500000A6F1A00000A725F0E0070282100000A2D0772650E00702B05726F0E0070A22519727F0E0070A2251A1118A2251B72D30E0070A2283800000AA22B64110D11121C8D450000012516111B729D0C00706F3500000A6F1A00000A72F30C007072F70C00706F3200000AA22517722B0C0070A22518111772FD0C00706F3500000A6F1A00000AA2251972DD0E0070A2251A1118A2251B72F30C0070A2283800000AA200111217581312003833020000111B72250D00706F3500000A6F1A00000A72E30E0070282100000A13231123390F020000001713111119723F0D0070282100000A2D2A111972470D0070282100000A2D1C111972570D0070282100000A2D0E111972650D0070282100000A2B011713241124398B00000000111072E70E00706F3900000A132511252C3A110D111272EF0E0070111772A70C00706F3500000A6F1A00000A111772FD0C00706F3500000A6F1A00000A11186F3700000A282300000AA22B38110D111272FC0F0070111772A70C00706F3500000A6F1A00000A111772FD0C00706F3500000A6F1A00000A11186F3700000A282300000AA2003837010000111972990D0070282100000A2D0E111972A30D0070282100000A2B011713261126398B00000000111072E70E00706F3900000A132711272C3A110D1112726C100070111772A70C00706F3500000A6F1A00000A111772FD0C00706F3500000A6F1A00000A11186F1A00000A282300000AA22B38110D111272F7110070111772A70C00706F3500000A6F1A00000A111772FD0C00706F3500000A6F1A00000A11186F3700000A282300000AA200388600000000111072E70E00706F3900000A132811282C3A110D111272EA120070111772A70C00706F3500000A6F1A00000A111772FD0C00706F3500000A6F1A00000A11186F3700000A282300000AA22B38110D111272D5130070111772A70C00706F3500000A6F1A00000A111772FD0C00706F3500000A6F1A00000A11186F3700000A282300000AA200111217581312000000111A6F3A00000A3A14FAFFFFDE16111A7522000001132911292C0811296F3B00000A00DC0011166F3A00000A3AA6F9FFFFDE1611167522000001132911292C0811296F3B00000A00DC7E1F00000A13130F02FE16140000016F1A00000A282000000A16FE01132A112A2C2B72291400700F02FE16140000016F1A00000A722B0C00700F03FE16140000016F1A00000A283C00000A131311112C0E111072E70E00706F3900000A2B0117132B112B2C3F1C8D450000012516723D140070A2251707A225187273140070A225191110110D132C112C283D00000AA2251A7281140070A2251B1113A2283800000A0D2B3D1C8D450000012516723D140070A2251708A225187273140070A225191110110D132C112C283D00000AA2251A7281140070A2251B1113A2283800000A0D00DE0D11072C0811076F3B00000A00DC09283E00000A132D2B00112D2A000000414C00000200000084020000F3050000770800001600000000000000020000003B020000610600009C080000160000000000000002000000D6000000C1080000970900000D00000000000000133002003A00000004000011000F00281400000A16FE010A062C25000F01281500000A0F02283F00000A734000000A0B070F00281500000A6F4100000A0C2B04160C2B00082A0000133002005100000005000011000F00281400000A16FE010A062C3C000F01281500000A0F02283F00000A734000000A0B070F00281500000A6F4100000A0C082C15070F00281500000A6F4200000A6F4300000A0D2B0500140D2B00092A000000133002004C00000006000011000F00281400000A16FE010A062C37000F01281500000A0F02283F00000A734000000A0B070F00281500000A6F4100000A0C082C10070F00281500000A6F4400000A0D2B0500140D2B00092A62000302742A0000016F4300000A283E00000A81140000012A000000133003004100000007000011000F00281400000A16FE010A062C2C000F01281500000A0F02283F00000A734000000A0B070F00281500000A0F03281500000A6F4500000A0C2B04140C2B00082A00000013300400F70000000800001100731300000A0A728F140070732400000A0B076F4600000A0C0872BF1400706F2900000A00086F4700000A72261B00701F0E6F4800000A0F00282600000A8C300000016F4900000A00086F4700000A722E1B00701F0E6F4800000A0F01282600000A8C300000016F4900000A00076F2800000A00086F4A00000A0D096F4B00000A130411042C2E002B1E0006723E1B007009724C1B00706F4C00000A74450000016F1600000A2600096F4D00000A130511052DD600096F4E00000A00076F4F00000A00066F5000000A19FE02130611062C1900066F1A00000A16066F5000000A19596F5100000A13072B097E1F00000A13072B0011072A001B3004004301000009000011001A0F01281500000A0F02285200000A0F03285200000A28810000060A170F01281500000A0F02285200000A0F03285200000A28800000060B0006026F5300000A6F4400000A6F5400000A0C38C8000000086F3400000A742A0000010D00096F4300000A288600000613040711046F4100000A130511052C08171306DDBF0000000F04285200000A130711073987000000001714161628810000061308180F01281500000A0F02285200000A0F03285200000A2881000006130900110811046F4400000A6F5400000A130A2B29110A6F3400000A742A000001130B001109110B6F4300000A6F4100000A130C110C2C05171306DE4B00110A6F3A00000A2DCEDE16110A7522000001130D110D2C08110D6F3B00000A00DC0000086F3A00000A3A2DFFFFFFDE15087522000001130D110D2C08110D6F3B00000A00DC1613062B0011062A00011C00000200CB00360101160000000002004C00DA2601150000000013300500F40000000A00001173890000060A06047D0300000400026F5500000A16FE010B0739CE000000738A0000060C08067D05000004001A0F01281500000A0F03285200000A0F04285200000A28810000060D08170F01281500000A0F03285200000A0F04285200000A28800000067D040000040F05285200000A130511052C54738C00000613061106087D080000040011061714161628810000067D060000041106180F01281500000A0F03285200000A0F04285200000A28810000067D070000041106FE068D000006735600000A1304002B100008FE068B000006735600000A13040009026F5300000A11046F5700000A13072B051413072B0011072A13300300230000000B00001100026F4300000A28860000060A0306046F4500000A0A0628870000060A060B2B00072A0013300300530000000C000011738E0000060A060E047D0900000406047D0A00000400026F4300000A28860000060B0307067B0A0000046F4500000A0B06FE068F000006735600000A0C0507086F5700000A0B0728870000060B070D2B00092A0013300400890000000D000011001F200F03281500000A0F04285200000A0F05285200000A28800000060A0F00281400000A2D0F060F00281500000A6F4100000A2B01160B072C04170C2B480F01281400000A2D0F060F01281500000A6F4100000A2B01160D092C04170C2B270F02281400000A2D0F060F02281500000A6F4100000A2B0116130411042C04170C2B04160C2B00082A000000133004004A00000007000011000F00281400000A16FE010A062C35001F200F01281500000A0F03285200000A0F04285200000A28800000060B070F00281500000A0F02281500000A6F4500000A0C2B04140C2B00082A000013300400890000000D000011001F100F03281500000A0F04285200000A0F05285200000A28800000060A0F00281400000A2D0F060F00281500000A6F4100000A2B01160B072C04170C2B480F01281400000A2D0F060F01281500000A6F4100000A2B01160D092C04170C2B270F02281400000A2D0F060F02281500000A6F4100000A2B0116130411042C04170C2B04160C2B00082A000000133004004A00000007000011000F00281400000A16FE010A062C35001F100F01281500000A0F03285200000A0F04285200000A28800000060B070F00281500000A0F02281500000A6F4500000A0C2B04140C2B00082A0000133004002F0000000E000011001A0F01281500000A0F02285200000A0F03285200000A28800000060A060F00281500000A6F4100000A0B2B00072A00133004004900000007000011000F00281400000A16FE010A062C34001A0F01281500000A0F03285200000A0F04285200000A28800000060B070F00281500000A0F02281500000A6F4500000A0C2B04140C2B00082A000000133004002E0000000E000011001E0F01281500000A0F02285200000A0F03285200000A28800000060A06026F5300000A6F4100000A0B2B00072A000013300400470000000700001100026F5500000A16FE010A062C33001E0F01281500000A0F03285200000A0F04285200000A28800000060B07026F5300000A0F02281500000A6F4500000A0C2B04140C2B00082A001B300400E90000000F000011000F00281400000A16FE010A0639D100000000170F01281500000A0F02285200000A0F03285200000A28800000060B070F00281500000A6F4100000A0C082C07170D38A00000000F04285200000A13041104398B000000001714161628810000061305180F01281500000A0F02285200000A0F03285200000A288100000613060011050F00281500000A6F4400000A6F5400000A13072B2811076F3400000A742A000001130800110611086F4300000A6F4100000A130911092C04170DDE280011076F3A00000A2DCFDE1611077522000001130A110A2C08110A6F3B00000A00DC0000160D2B00092A000000011000000200960035CB00160000000013300500C90000001000001173900000060A06047D0B000004000F00281400000A16FE010B0739A2000000000F00281500000A0C170F01281500000A0F03285200000A0F04285200000A28800000060D0908067C0B000004281500000A6F4500000A0C0F05285200000A130411042C58739100000613051105067D0D0000040017141616288100000613061105180F01281500000A0F03285200000A0F04285200000A28810000067D0C0000041105FE0692000006735600000A130711060811076F5700000A0C000813082B051413082B0011082A0000001330030013000000110000110003026F4300000A046F4500000A0A2B00062A001B3004008900000012000011001A14161628810000060A0F01281500000A0F02285200000A28830000060B0006026F5300000A6F4400000A6F5400000A0C2B2E086F3400000A742A0000010D00096F4300000A288600000613040711046F4100000A130511052C05171306DE2500086F3A00000A2DCADE15087522000001130711072C0811076F3B00000A00DC1613062B0011062A00000001100000020032003A6C0015000000001B3005000E0100001300001100026F5500000A16FE010A0639F5000000001A14161628810000060B0F01281500000A0F03285200000A28830000060C731300000A0D1613040007026F5300000A6F4400000A6F5400000A13052B6A11056F3400000A742A00000113060011066F4300000A288600000613070811070F02281500000A6F4500000A2887000006130809026F5300000A110411066F5800000A1104596F5100000A1108283100000A6F1900000A2611066F5800000A11066F5900000A5813040011056F3A00000A2D8DDE1611057522000001130911092C0811096F3B00000A00DC09026F5300000A1104026F5300000A6F2F00000A1104596F5100000A6F1900000A26096F1A00000A130A2B0514130A2B00110A2A00000110000002004D0077C40016000000001B3005004B040000140000110072ED0300700A06732400000A0B732500000A0C732500000A0D72581B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A0072621B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A00726C1B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A0072761B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A0072801B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A00728A1B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A0072941B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A00729E1B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A0072A81B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A0072B21B0070D030000001285A00000A735B00000A1304096F5C00000A11046F5D00000A0072BE1B007002282200000A1306110607732700000A1307076F2800000A0011076F5E00000A6F1A00000A13051105178D5500000125161F2D9D6F5F00000A130811088E6918FE0113091109396C020000000F01286000000A16FE01130A110A2C5100728D1C00701108169A1108179A0F01FE161B0000016F1A00000A282300000A1306110711066F2900000A0011076F5E00000A6F1A00000A13051105178D5500000125161F2D9D6F5F00000A1308002B17001108161108169A286100000A176A58286200000AA20011088E6918FE01130B110B39E20100000072941D00701108169A1108179A286300000A1306110711066F2900000A001107732A00000A130C110C086F2B00000A26076F4F00000A00086F2C00000A6F2D00000A736400000A130D082C10086F2C00000A6F2D00000A16FE022B0116130F110F397A01000000086F2C00000A6F3300000A1310384301000011106F3400000A74210000011311001111721F1E00706F3500000AA557000001111172271E00706F3500000AA55700000159176AFE01131211122C7B00110D11116F6500000A00110D6F6600000A1313096F6700000A130E1713142B2D00110E11141759111311138E691114599A7421000001722F1E00706F3500000A6F6800000A0000111417581314111411138E69FE0216FE01131511152DC2096F2C00000A110E6F6900000A00110D6F6A00000A2600388B000000002B6D00110D6F6B00000A7421000001130E110E72271E00706F3500000AA557000001111172271E00706F3500000AA5570000012F26110E721F1E00706F3500000AA557000001111172271E00706F3500000AA557000001FE022B0116131611162C022B1B00110D6F6A00000A260000110D6F6C00000A16FE02131711172D83110D11116F6500000A00000011106F3A00000A3AB1FEFFFFDE1611107522000001131811182C0811186F3B00000A00DC0000096F2C00000A13192B0011192A00411C000002000000D00200005601000026040000160000000000000013300300B203000015000011000214FE030A06392E030000000274210000010B07166F6D00000A7E6E00000A2E1607166F6D00000A6F1A00000A282000000A16FE012B01160C082C190307166F6D00000AA530000001286F00000A811B0000012B0B037E7000000A811B00000107176F6D00000A7E6E00000A2E1607176F6D00000A6F1A00000A282000000A16FE012B01160D092C190407176F6D00000AA530000001286F00000A811B0000012B0B047E7000000A811B00000107186F6D00000A7E6E00000A2E1607186F6D00000A6F1A00000A282000000A16FE012B0116130411042C190507186F6D00000AA530000001286F00000A811B0000012B0B057E7000000A811B00000107196F6D00000A7E6E00000A2E1607196F6D00000A6F1A00000A282000000A16FE012B0116130511052C1A0E0407196F6D00000AA530000001286F00000A811B0000012B0C0E047E7000000A811B000001071A6F6D00000A7E6E00000A2E16071A6F6D00000A6F1A00000A282000000A16FE012B0116130611062C1A0E05071A6F6D00000AA530000001286F00000A811B0000012B0C0E057E7000000A811B000001071B6F6D00000A7E6E00000A2E16071B6F6D00000A6F1A00000A282000000A16FE012B0116130711072C1A0E06071B6F6D00000AA530000001286F00000A811B0000012B0C0E067E7000000A811B000001071C6F6D00000A7E6E00000A2E16071C6F6D00000A6F1A00000A282000000A16FE012B0116130811082C1A0E07071C6F6D00000AA530000001286F00000A811B0000012B0C0E077E7000000A811B000001071D6F6D00000A7E6E00000A2E16071D6F6D00000A6F1A00000A282000000A16FE012B0116130911092C1A0E08071D6F6D00000AA530000001286F00000A811B0000012B0C0E087E7000000A811B000001071E6F6D00000A7E6E00000A2E16071E6F6D00000A6F1A00000A282000000A16FE012B0116130A110A2C1A0E09071E6F6D00000AA530000001286F00000A811B0000012B0C0E097E7000000A811B000001071F096F6D00000A7E6E00000A2E17071F096F6D00000A6F1A00000A282000000A16FE012B0116130B110B2C1B0E0A071F096F6D00000AA530000001286F00000A811B0000012B0C0E0A7E7000000A811B000001002B7700037E7000000A811B000001047E7000000A811B000001057E7000000A811B0000010E047E7000000A811B0000010E057E7000000A811B0000010E067E7000000A811B0000010E077E7000000A811B0000010E087E7000000A811B0000010E097E7000000A811B0000010E0A7E7000000A811B000001002A2202287100000A002A001B3008008B00000016000011007E7200000A0A0F02286000000A16FE010B072C0A000F02282600000A0A00026F5500000A16FE010C082C5E00160D7E1F00000A130415130572ED030070732400000A13060011066F2800000A001106283300000600020312051203120411060616282B00000600061106282900000626110628340000060000DE0D11062C0811066F3B00000A00DC002A000110000002004500377C000D000000001B300800EA0000001700001100170A7E7200000A0B0F04287300000A16FE010C082C0A000F04285200000A0A000F05286000000A16FE010D092C0A000F05282600000A0B000516737400000A81240000010E06737500000A51026F5500000A16FE0113041104398A000000001613057E1F00000A130615130772ED030070732400000A13080011086F2800000A001108283300000600020312071205120611080717282B00000600051107737400000A81240000010E0611082835000006510F01282600000A110511060F02283F00000A1108061107283600000600110828340000060000DE0D11082C0811086F3B00000A00DC002A0000011000000200790062DB000D000000001B3008008700000018000011000516737400000A8124000001026F5500000A16FE010A062C6C00160B7E1F00000A0C150D72ED030070732400000A13040011046F2800000A001104283300000600020312031201120211047E7200000A17282B0000060007110428250000060D110428340000060000DE0D11042C0811046F3B00000A00DC0509737400000A8124000001002A0001100000020031003B6C000D0000000013300300440000001900001100178D310000010A737600000A0B0772411E00706F7700000A00071E6F7800000A0007028C4B0000016F4900000A00061607A2724F1E0070060328460000060C2B00082A133003006C0000001A00001100188D310000010A737600000A0B0772811E00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272B31E0070060428460000060D2B00092A133003006C0000001A00001100188D310000010A737600000A0B0772E31E00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272F31E0070060428460000060D2B00092A13300300450000001900001100178D310000010A737600000A0B0772971E00706F7700000A00071F0E6F7800000A0007028C300000016F4900000A00061607A2721F1F0070060328460000060C2B00082A00000013300300450000001900001100178D310000010A737600000A0B0772571F00706F7700000A00071F0E6F7800000A0007028C300000016F4900000A00061607A272731F0070060328460000060C2B00082A000000133003006C0000001A00001100188D310000010A737600000A0B0772A91F00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272C71F0070060428460000060D2B00092A1B300500790900001B00001100737900000A0A06026F5300000A6F7A00000A000516540E04720120007051041554000E0528480000066F7B00000A0C38030900001202287C00000A0D00044A2D1A0972411E0070287D00000A2C0D097203200070287D00000A2B0116130511052C0538D008000006092849000006130411042C0C11046F7E00000A16FE012B0117130611062C0538AB080000091314111413131113399C0800001113288800000613151115208371585042B300000011152082DBFB3A3555111520BEC9C006351F1115200FF25E003BF20200002B00111520BEC9C0063B760200003856080000111520B8C564313BB50100002B00111520360908393B4F0100002B0011152082DBFB3A3B5F020000382908000011152062125646351F111520AE6A4B453B690100002B00111520621256463BF501000038010800001115205973F8473B760100002B00111520AEF43C503B440200002B00111520837158503B7802000038D4070000111520AC86DB8435551115203F2F3D6C351F111520A1D2E8543B630100002B001115203F2F3D6C3BA500000038A30700001115202E9044793BDE0100002B00111520F3FFC0823BC80000002B00111520AC86DB843B120100003876070000111520A0B9BEA3352A111520B53BE6993B240100002B001115205A9F949A3B2C0100002B00111520A0B9BEA32E7138430700001115202BF384AB2E212B00111520519E94BB3B9F0100002B0011152038595EBD3B230100003819070000111372A91F0070282100000A3ABD010000380307000011137217200070282100000A3ACA01000038ED060000111372811E0070282100000A3AD701000038D7060000111372E31E0070282100000A3ADF01000038C106000011137235200070282100000A3AE701000038AB06000011137257200070282100000A3AEF01000038950600001113726D200070282100000A3AF7010000387F0600001113728B200070282100000A3A040200003869060000111372A9200070282100000A3A0C0200003853060000111372B9200070282100000A3A16020000383D060000111372DD200070282100000A3A2302000038270600001113720B210070282100000A3A5B02000038110600001113722F210070282100000A3A6802000038FB0500001113725B210070282100000A3A7502000038E505000011137277210070282100000A3A7F02000038CF0500001113728B210070282100000A3AD102000038B90500001113729D210070282100000A3A2303000038A3050000111372B1210070282100000A3A13040000388D050000111372C7210070282100000A3AEF0400003877050000111372411E0070282100000A3A3E050000386105000011137203200070282100000A3A3E050000384B050000110472A91F007028570000060B04070F01282600000A0E05282A000006543828050000110472E321007028570000060B04070F01282600000A0E052832000006543805050000110428560000060B04070F01282600000A0E0528260000065438E7040000110428560000060B04070F01282600000A0E0528270000065438C9040000110428560000060B04070F01282600000A0E0528450000065438AB040000110428560000060B04070F01282600000A0E05284400000654388D040000110472E321007028570000060B04070F01282600000A0E05284300000654386A040000110428560000060B04070F01282600000A0E05284200000654384C0400001104284C00000613070411070F01282600000A0E05283C00000654382C040000110472B920007028570000060B04070F01282600000A0E05284000000654380904000011046F7E00000A16FE02131611162C16001104166F7F00000A131711176F8000000A0B002B080072132200700B00062858000006130804070F01282600000A0E051108282D0000065438BB0300001104720B21007028570000060B04070F01282600000A0E05283E0000065438980300001104722F21007028570000060B04070F01282600000A0E05283F0000065438750300001104284D00000613090411090F01282600000A0E05283D0000065438550300001104284E000006130A1104166F8100000A6F8200000A72772200706F8300000A2C281104166F8100000A6F8200000A72772200706F8300000A6F8400000A7E1F00000A287D00000A2B0116131811182C1204110A0F01282600000A0E05283B0000065438ED0200001104284F000006130B1104166F8100000A6F8200000A72772200706F8300000A2C281104166F8100000A6F8200000A72772200706F8300000A6F8400000A7E1F00000A287D00000A2B0116131911192C1204110B0F01282600000A0E05283700000654388502000011042850000006130C120DFE15360000011104166F8100000A6F8200000A72852200706F8300000A2C281104166F8100000A6F8200000A72852200706F8300000A6F8400000A120D288500000A16FE012B0116131A111A2C0A00120DFE1536000001007E8600000A288700000A130E1104166F8100000A6F8200000A729F2200706F8300000A2C281104166F8100000A6F8200000A729F2200706F8300000A6F8400000A120E288500000A16FE012B0116131B111B2C0E007E8600000A288700000A130E00110D7E8800000A288900000A2C13110E7E8600000A288700000A288900000A2B0116131C111C2C140004110C0F01282600000A0E0528380000065400387F01000011042851000006130F1613101104166F8100000A6F8200000A72B92200706F8300000A2C281104166F8100000A6F8200000A72B92200706F8300000A6F8400000A1210288A00000A16FE012B0116131D111D2C0500161310007E8B00000A288C00000A13111104166F8100000A6F8200000A72D32200706F8300000A2C281104166F8100000A6F8200000A72D32200706F8300000A6F8400000A1211288A00000A16FE012B0116131E111E2C0E007E8B00000A288C00000A13110011102C1311117E8B00000A288C00000AFE0116FE012B0116131F111F2C140004110F0F01282600000A0E0528390000065400388D0000001104285200000613121104166F8100000A6F8200000A72772200706F8300000A2C281104166F8100000A6F8200000A72772200706F8300000A6F8400000A7E1F00000A287D00000A2B0116132011202C120411120F01282600000A0E05283A000006542B281104166F8100000A6F8D00000A05288A00000A262B120E041104166F8100000A6F8D00000A512B00001202288E00000A3AF1F6FFFFDE0F1202FE160200001B6F3B00000A00DC040E060E05282C000006540E07132111212C10040F01282600000A0E052828000006542A000000411C0000020000003000000016090000460900000F0000000000000013300300450000001900001100178D310000010A737600000A0B0772571F00706F7700000A00071F0E6F7800000A0007028C300000016F4900000A00061607A272ED220070060328460000060C2B00082A00000013300300C60000001C00001100198D310000010A7E1F00000A0B052C0B056F8F00000A16FE022B0116130511052C117221230070056F9000000A289100000A0B737600000A0C0872252300706F7700000A00081F196F7800000A0008026F4900000A00061608A2737600000A0D0972592300706F7700000A00091F166F7800000A0009076F4900000A00061709A2737600000A1304110472971E00706F7700000A0011041F0E6F7800000A001104038C300000016F4900000A0006181104A272752300700604284600000613062B0011062A0000133003006C0000001A00001100188D310000010A737600000A0B0772B32300706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272D5230070060428460000060D2B00092A13300300710000001A00001100198D310000010A737600000A0B0772811E00706F7700000A00071F196F7800000A0007027B0F0000046F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272B31E0070060428460000060D2B00092A0000001B3005006A0100001D000011001200FE150F00000200026F9200000A0B382A010000076F3400000A74370000010C001200086F8200000A72012400706F8300000A2C1C086F8200000A72012400706F8300000A6F8400000A282000000A2C0C1613041204289300000A2B15086F8200000A72012400706F8300000A6F8400000A7D0E000004739400000A0D086F8200000A72212400706F8300000A2C1F086F8200000A72212400706F8300000A6F8400000A282000000A16FE012B0116130511052C7A0000086F8200000A72212400706F8300000A6F8400000A178D5500000125161F209D6F5F00000A13061613072B34110611079A13080011086F2F00000A1F24FE01130911092C1400120A1108289500000A09110A6F9600000A000000110717581307110711068E6932C412000928550000067D0F000004002B0E0012007E1F00000A7D0F0000040000076F3A00000A3ACBFEFFFFDE15077522000001130B110B2C08110B6F3B00000A00DC06130C2B00110C2A0000411C000002000000110000003C0100004D010000150000000000000013300400590000001E000011001B8D4500000125167239240070A2251702A225187277240070A225190F01283000000AA2251A727D240070A2283800000A0A739700000A0B07046F9800000A0007066F2900000A0007176F9900000A00076F9A00000A262A000000133003006C0000001A00001100188D310000010A737600000A0B0772172000706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A27281240070060428460000060D2B00092A133002002D0000001E0000110072B92400700A739700000A0B07026F9800000A0007066F2900000A0007176F9900000A00076F9A00000A262A000000133002002D0000001E0000110072A82600700A739700000A0B07026F9800000A0007066F2900000A0007176F9900000A00076F9A00000A262A00000013300200370000001F00001100739700000A0A06026F9800000A000672F02600706F2900000A0006176F9900000A00066F9B00000A0B07739C00000A0C080D2B00092A0013300200860100002000001100737600000A0A0672971E00706F7700000A00061F0E6F7800000A0006028C300000016F4900000A00737600000A0B0772411E00706F7700000A00071E6F7800000A0007038C4B0000016F4900000A00737600000A0C0872032000706F7700000A00081F166F7800000A00081F196F9D00000A0008046F4900000A00737600000A0D0972EB2800706F7700000A00091E6F7800000A0009058C4B0000016F4900000A00737600000A1304110472012900706F7700000A001104186F7800000A0011040E058C5A0000016F4900000A00737600000A13051105721F2900706F7700000A0011051E6F7800000A0011050E068C4B0000016F4900000A00739700000A130611060E046F9800000A00110672392900706F2900000A0011061A6F9900000A0011066F4700000A066F9E00000A2611066F4700000A076F9E00000A2611066F4700000A086F9E00000A2611066F4700000A096F9E00000A2611066F4700000A11046F9E00000A2611066F4700000A11056F9E00000A26289F00000A11066FA000000A002A000013300300D700000021000011001A8D310000010A737600000A0B0772012400706F7700000A0007186F7800000A0007027B1000000428A100000A8C5A0000016F4900000A00061607A2737600000A0C0872792900706F7700000A00081A6F7800000A0008027B130000046F4900000A00061708A2737600000A0D09728D2900706F7700000A00091A6F7800000A0009027B140000046F4900000A00061809A2737600000A1304110472971E00706F7700000A0011041F0E6F7800000A001104038C300000016F4900000A0006191104A2729D2900700604284600000613052B0011052A00133003001301000022000011001B8D310000010A737600000A0B0772852200706F7700000A00071F096F7800000A0007027B150000048C420000016F4900000A00061607A2737600000A0C08729F2200706F7700000A00081F096F7800000A0008027B160000048C420000016F4900000A00061708A2737600000A0D0972792900706F7700000A00091A6F7800000A0009027B170000048C390000016F4900000A00061809A2737600000A13041104728D2900706F7700000A0011041A6F7800000A001104027B180000048C390000016F4900000A0006191104A2737600000A1305110572971E00706F7700000A0011051F0E6F7800000A001105038C300000016F4900000A00061A1105A272D32900700604284600000613062B0011062A00133003001101000022000011001B8D310000010A737600000A0B0772B92200706F7700000A00071E6F7800000A0007027B190000048C240000016F4900000A00061607A2737600000A0C0872D32200706F7700000A00081E6F7800000A0008027B1A0000048C240000016F4900000A00061708A2737600000A0D0972792900706F7700000A00091A6F7800000A0009027B1B0000048C390000016F4900000A00061809A2737600000A13041104728D2900706F7700000A0011041A6F7800000A001104027B1C0000048C390000016F4900000A0006191104A2737600000A1305110572971E00706F7700000A0011051F0E6F7800000A001105038C300000016F4900000A00061A1105A272032A00700604284600000613062B0011062A000000133003009D0000002300001100198D310000010A737600000A0B0772792900706F7700000A00071A6F7800000A0007027B130000046F4900000A00061607A2737600000A0C08728D2900706F7700000A00081A6F7800000A0008027B140000046F4900000A00061708A2737600000A0D0972971E00706F7700000A00091F0E6F7800000A0009038C300000016F4900000A00061809A272352A00700604284600000613042B0011042A000000133003009D0000002300001100198D310000010A737600000A0B0772792900706F7700000A00071A6F7800000A0007027B130000046F4900000A00061607A2737600000A0C08728D2900706F7700000A00081A6F7800000A0008027B140000046F4900000A00061708A2737600000A0D0972971E00706F7700000A00091F0E6F7800000A0009038C300000016F4900000A00061809A2726D2A00700604284600000613042B0011042A00000013300300A80000002300001100198D310000010A737600000A0B0772012400706F7700000A0007186F7800000A0007027B0E00000428A100000A8C5A0000016F4900000A00061607A2737600000A0C08729D2A00706F7700000A00081F196F7800000A0008027B0F0000046F4900000A00061708A2737600000A0D0972971E00706F7700000A00091F0E6F7800000A0009038C300000016F4900000A00061809A272B32A00700604284600000613042B0011042A13300300A80000002300001100198D310000010A737600000A0B0772012400706F7700000A0007186F7800000A0007027B0E00000428A100000A8C5A0000016F4900000A00061607A2737600000A0C0872DF2A00706F7700000A00081F196F7800000A0008027B0F0000046F4900000A00061708A2737600000A0D0972971E00706F7700000A00091F0E6F7800000A0009038C300000016F4900000A00061809A272EF2A00700604284600000613042B0011042A133003006C0000001A00001100188D310000010A737600000A0B0772292B00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A2723F2B0070060428460000060D2B00092A133003006C0000001A00001100188D310000010A737600000A0B07727F2B00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A2729D2B0070060428460000060D2B00092A133003006C0000001A00001100188D310000010A737600000A0B0772E52B00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A2720F2C0070060428460000060D2B00092A133003006C0000001A00001100188D310000010A737600000A0B07729D2A00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272B32A0070060428460000060D2B00092A133003006C0000001A00001100188D310000010A737600000A0B0772432C00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272672C0070060428460000060D2B00092A133003006C0000001A00001100188D310000010A737600000A0B0772A12C00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272C32C0070060428460000060D2B00092A133003006C0000001A00001100188D310000010A737600000A0B0772FB2C00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272172D0070060428460000060D2B00092A133003006C0000001A00001100188D310000010A737600000A0B0772492D00706F7700000A00071F196F7800000A0007026F4900000A00061607A2737600000A0C0872971E00706F7700000A00081F0E6F7800000A0008038C300000016F4900000A00061708A272712D0070060428460000060D2B00092A13300200700000002400001100732500000A0A739700000A0B07046F9800000A0007026F2900000A00071A6F9900000A0007166FA200000A0000030C160D2B2408099A130400110414FE03130511052C0E076F4700000A11046F9E00000A26000917580D09088E6932D6076F5E00000AA54B00000113062B0011062A133002008C0000002500001100732500000A0A72ED0300700B07732400000A0C739700000A0D09086F9800000A0009026F2900000A00091A6F9900000A0009166FA200000A00000313051613062B28110511069A130700110714FE03130811082C0E096F4700000A11076F9E00000A2600110617581306110611058E6932D009732A00000A13041104066F2B00000A260613092B0011092A1B300300A5000000260000110073A300000A0A732500000A0B72AF2D00700C0802732700000A0D09732A00000A13041104076F2B00000A2600076F2C00000A6F3300000A13052B2811056F3400000A742100000113060006110672442E00706F3500000A6F1A00000A6FA400000A000011056F3A00000A2DCFDE1611057522000001130711072C0811076F3B00000A00DC0672411E00706FA400000A000672032000706FA400000A000613082B0011082A0000000110000002003A00356F0016000000001330020026050000270000110003131611161315111539050500001115288800000613171117208371585042B300000011172082DBFB3A3555111720BEC9C006351F1117200FF25E003BF20200002B00111720BEC9C0063B7602000038BF040000111720B8C564313BB50100002B00111720360908393B4F0100002B0011172082DBFB3A3B5F020000389204000011172062125646351F111720AE6A4B453B690100002B00111720621256463BF5010000386A0400001117205973F8473B760100002B00111720AEF43C503B440200002B00111720837158503B78020000383D040000111720AC86DB8435551117203F2F3D6C351F111720A1D2E8543B630100002B001117203F2F3D6C3BA5000000380C0400001117202E9044793BDE0100002B00111720F3FFC0823BC80000002B00111720AC86DB843B1201000038DF030000111720A0B9BEA3352A111720B53BE6993B240100002B001117205A9F949A3B2C0100002B00111720A0B9BEA32E7138AC0300001117202BF384AB2E212B00111720519E94BB3B9F0100002B0011172038595EBD3B230100003882030000111572A91F0070282100000A3ABD010000386C03000011157217200070282100000A3ABB0100003856030000111572811E0070282100000A3AB90100003840030000111572E31E0070282100000A3AB7010000382A03000011157235200070282100000A3AB5010000381403000011157257200070282100000A3AB501000038FE0200001115726D200070282100000A3AB501000038E80200001115728B200070282100000A3AB501000038D2020000111572A9200070282100000A3AB501000038BC020000111572B9200070282100000A3AB501000038A6020000111572DD200070282100000A3AB501000038900200001115720B210070282100000A3AB5010000387A0200001115722F210070282100000A3AB501000038640200001115725B210070282100000A3AB5010000384E02000011157277210070282100000A3AB501000038380200001115728B210070282100000A3AB501000038220200001115729D210070282100000A3AB2010000380C020000111572B1210070282100000A3AAF01000038F6010000111572C7210070282100000A3AAC01000038E0010000111572411E0070282100000A3AA901000038CA01000011157203200070282100000A3AA601000038B401000002724E2E00706FA500000A0A06131838AF0100000272B02E00706FA500000A0B071318389B0100000272F22E00706FA500000A0C081318388701000002722E2F00706FA500000A0D09131838730100000272642F00706FA500000A130411041318385D0100000272AC2F00706FA500000A13051105131838470100000272E82F00706FA500000A130611061318383101000002722A3000706FA500000A130711071318381B01000002726E3000706FA500000A13081108131838050100000272BE3000706FA500000A13091109131838EF0000000272243100706FA500000A130A110A131838D900000002726E3100706FA500000A130B110B131838C30000000272C83100706FA500000A130C110C131838AD00000002722A3200706FA500000A130D110D131838970000000272863200706FA500000A130E110E131838810000000272B63200706FA500000A130F110F13182B6E0272E43200706FA500000A1310111013182B5B0272143300706FA500000A1311111113182B480272463300706FA500000A1312111213182B3502727E3300706FA500000A1313111313182B220272A83300706FA500000A1314111413182B0F0272D83300706FA500000A13182B0011182A00001B300200550000002800001100739400000A0A00026F9200000A0B2B20076F3400000A74370000010C0006086F8D00000A739500000A6F9600000A0000076F3A00000A2DD8DE120775220000010D092C07096F3B00000A00DC0613042B0011042A0000000110000002000F002C3B0012000000001B30020055000000290000110073A600000A0A00026F9200000A0B2B20076F3400000A74370000010C0006086F8D00000A28A700000A6FA800000A0000076F3A00000A2DD8DE120775220000010D092C07096F3B00000A00DC0613042B0011042A0000000110000002000F002C3B0012000000001B3005006A0100001D000011001200FE150F00000200026F9200000A0B382A010000076F3400000A74370000010C001200086F8200000A72012400706F8300000A2C1C086F8200000A72012400706F8300000A6F8400000A282000000A2C0C1613041204289300000A2B15086F8200000A72012400706F8300000A6F8400000A7D0E000004739400000A0D086F8200000A72212400706F8300000A2C1F086F8200000A72212400706F8300000A6F8400000A282000000A16FE012B0116130511052C7A0000086F8200000A72212400706F8300000A6F8400000A178D5500000125161F209D6F5F00000A13061613072B34110611079A13080011086F2F00000A1F24FE01130911092C1400120A1108289500000A09110A6F9600000A000000110717581307110711068E6932C412000928550000067D0F000004002B0E0012007E1F00000A7D0F0000040000076F3A00000A3ACBFEFFFFDE15077522000001130B110B2C08110B6F3B00000A00DC06130C2B00110C2A0000411C000002000000110000003C0100004D01000015000000000000001B3005006A0100001D000011001200FE150F00000200026F9200000A0B382A010000076F3400000A74370000010C001200086F8200000A72012400706F8300000A2C1C086F8200000A72012400706F8300000A6F8400000A282000000A2C0C1613041204289300000A2B15086F8200000A72012400706F8300000A6F8400000A7D0E000004739400000A0D086F8200000A72212400706F8300000A2C1F086F8200000A72212400706F8300000A6F8400000A282000000A16FE012B0116130511052C7A0000086F8200000A72212400706F8300000A6F8400000A178D5500000125161F209D6F5F00000A13061613072B34110611079A13080011086F2F00000A1F24FE01130911092C1400120A1108289500000A09110A6F9600000A000000110717581307110711068E6932C412000928550000067D0F000004002B0E0012007E1F00000A7D0F0000040000076F3A00000A3ACBFEFFFFDE15077522000001130B110B2C08110B6F3B00000A00DC06130C2B00110C2A0000411C000002000000110000003C0100004D01000015000000000000001B3003004D0100002A000011001200FE151000000200026F9200000A0B380D010000076F3400000A74370000010C00120072772100707D110000041200086F8200000A72F43300706F8300000A2C17086F8200000A72F43300706F8300000A6F8400000A2B0572063400707D120000041200086F8200000A72772200706F8300000A2C33086F8200000A72772200706F8300000A6F8400000A282000000A2D17086F8200000A72772200706F8300000A6F8400000A2B137EA900000A0D1203FE16390000016F1A00000A7D130000041200086F8200000A72163400706F8300000A2C33086F8200000A72163400706F8300000A6F8400000A282000000A2D17086F8200000A72163400706F8300000A6F8400000A2B137EAA00000A0D1203FE16390000016F1A00000A7D1400000400076F3A00000A3AE8FEFFFFDE15077522000001130411042C0811046F3B00000A00DC0613052B0011052A000000411C000002000000110000001F0100003001000015000000000000001B300300A40100002B000011001200FE151000000200026F9200000A0B3864010000076F3400000A74370000010C001200086F8200000A72012400706F8300000A2C1C086F8200000A72012400706F8300000A6F8400000A282000000A2C0B160D1203289300000A2B15086F8200000A72012400706F8300000A6F8400000A7D100000041200728B2100707D110000041200086F8200000A72F43300706F8300000A2C17086F8200000A72F43300706F8300000A6F8400000A2B0572063400707D120000041200086F8200000A72772200706F8300000A2C33086F8200000A72772200706F8300000A6F8400000A282000000A2D17086F8200000A72772200706F8300000A6F8400000A2B147EA900000A13041204FE16390000016F1A00000A7D130000041200086F8200000A72163400706F8300000A2C33086F8200000A72163400706F8300000A6F8400000A282000000A2D17086F8200000A72163400706F8300000A6F8400000A2B147EAA00000A13041204FE16390000016F1A00000A7D1400000400076F3A00000A3A91FEFFFFDE15077522000001130511052C0811056F3B00000A00DC0613062B0011062A411C00000200000011000000760100008701000015000000000000001B300300ED0100002C000011001200FE151100000200026F9200000A0B38AD010000076F3400000A74370000010C00086F8200000A72852200706F8300000A2C1F086F8200000A72852200706F8300000A6F8400000A282000000A16FE012B01160D092C231200086F8200000A72852200706F8300000A6F8400000A28AB00000A7D150000042B111200722434007028AB00000A7D15000004086F8200000A729F2200706F8300000A2C1F086F8200000A729F2200706F8300000A6F8400000A282000000A16FE012B0116130411042C231200086F8200000A729F2200706F8300000A6F8400000A28AB00000A7D160000042B0C12007E8600000A7D16000004086F8200000A72792900706F8300000A2C1F086F8200000A72792900706F8300000A6F8400000A282000000A16FE012B0116130511052C231200086F8200000A72792900706F8300000A6F8400000A28AC00000A7D170000042B0C12007EA900000A7D17000004086F8200000A728D2900706F8300000A2C1F086F8200000A728D2900706F8300000A6F8400000A282000000A16FE012B0116130611062C231200086F8200000A728D2900706F8300000A6F8400000A28AC00000A7D180000042B0C12007EAA00000A7D1800000400076F3A00000A3A48FEFFFFDE15077522000001130711072C0811076F3B00000A00DC0613082B0011082A000000411C00000200000011000000BF010000D001000015000000000000001B300300E90100002D000011001200FE151200000200026F9200000A0B38A9010000076F3400000A74370000010C00086F8200000A72B92200706F8300000A2C1F086F8200000A72B92200706F8300000A6F8400000A282000000A16FE012B01160D092C231200086F8200000A72B92200706F8300000A6F8400000A28AD00000A7D190000042B0D12001628AE00000A7D19000004086F8200000A72D32200706F8300000A2C1F086F8200000A72D32200706F8300000A6F8400000A282000000A16FE012B0116130411042C231200086F8200000A72D32200706F8300000A6F8400000A28AD00000A7D1A0000042B0C12007E8B00000A7D1A000004086F8200000A72792900706F8300000A2C1F086F8200000A72792900706F8300000A6F8400000A282000000A16FE012B0116130511052C231200086F8200000A72792900706F8300000A6F8400000A28AC00000A7D1B0000042B0C12007EA900000A7D1B000004086F8200000A728D2900706F8300000A2C1F086F8200000A728D2900706F8300000A6F8400000A282000000A16FE012B0116130611062C231200086F8200000A728D2900706F8300000A6F8400000A28AC00000A7D1C0000042B0C12007EAA00000A7D1C00000400076F3A00000A3A4CFEFFFFDE15077522000001130711072C0811076F3B00000A00DC0613082B0011082A000000411C00000200000011000000BB010000CC01000015000000000000001B3003004D0100002A000011001200FE151000000200026F9200000A0B380D010000076F3400000A74370000010C00120072C72100707D110000041200086F8200000A72F43300706F8300000A2C17086F8200000A72F43300706F8300000A6F8400000A2B0572063400707D120000041200086F8200000A72772200706F8300000A2C33086F8200000A72772200706F8300000A6F8400000A282000000A2D17086F8200000A72772200706F8300000A6F8400000A2B137EA900000A0D1203FE16390000016F1A00000A7D130000041200086F8200000A72163400706F8300000A2C33086F8200000A72163400706F8300000A6F8400000A282000000A2D17086F8200000A72163400706F8300000A6F8400000A2B137EAA00000A0D1203FE16390000016F1A00000A7D1400000400076F3A00000A3AE8FEFFFFDE15077522000001130411042C0811046F3B00000A00DC0613052B0011052A000000411C000002000000110000001F0100003001000015000000000000001B300300B90000002E0000110073AF00000A0A00026F9200000A0B2B7E076F3400000A74370000010C001203FE1510000002120372772100707D110000041203086F8200000A72F43300706F8300000A6F8400000A7D120000041203086F8200000A72772200706F8300000A6F8400000A7D130000041203086F8200000A72163400706F8300000A6F8400000A7D1400000406096FB000000A0000076F3A00000A3A77FFFFFFDE15077522000001130411042C0811046F3B00000A00DC0613052B0011052A0000000110000002000F008D9C0015000000001B3004007D0000002F00001100731300000A0A06722E3400706F1900000A2600026F2C00000A6F3300000A0B2B26076F3400000A74210000010C0006720100007008166F6D00000A6F1A00000A6F1600000A2600076F3A00000A2DD2DE120775220000010D092C07096F3B00000A00DC0672643400706F1900000A26066F1A00000A13042B0011042A000000011000000200200032520012000000001B3003007B0000003000001100731300000A0A06722E3400706F1900000A260214FE030B072C4700026FB100000A0C2B23120228B200000A0D000672010000701203FE16300000016F1A00000A6F1600000A2600120228B300000A2DD4DE0F1202FE160600001B6F3B00000A00DC0672643400706F1900000A26066F1A00000A13042B0011042A0001100000020023003053000F000000001330020011000000110000110002729C34007028570000060A2B00062A0000001B3003006F0000003100001100731300000A0A0672CE340070036F1600000A2600026F9200000A0B2B1B076F3400000A74370000010C0006086F8000000A6F1900000A2600076F3A00000A2DDDDE120775220000010D092C07096F3B00000A00DC0672DA340070036F1600000A26066F1A00000A13042B0011042A000110000002001C0027430012000000001B3002005F000000320000110073A300000A0A02721720007028490000060B00076F9200000A0C2B1B086F3400000A74370000010D0006096F8D00000A6FA400000A0000086F3A00000A2DDDDE15087522000001130411042C0811046F3B00000A00DC0613052B0011052A000110000002001B0027420015000000001B300300600000003300001100739400000A0A00026F2C00000A6F3300000A0B2B26076F3400000A74210000010C000608166F6D00000A6F1A00000A739500000A6F9600000A0000076F3A00000A2DD2DE120775220000010D092C07096F3B00000A00DC0613042B0011042A011000000200140032460012000000001B3002005C0000003400001100737900000A0A0672E83400706F7A00000A0000026F9200000A0B2B16076F3400000A74370000010C0006086FB400000A2600076F3A00000A2DE2DE120775220000010D092C07096F3B00000A00DC066F8000000A13042B0011042A0110000002001B00223D0012000000001330060037010000350000110072283500701F11734000000A0A140B060F01281500000A6F4100000A0D092C13060F01281500000A6F4200000A6F4300000A0B731300000A0C0872363500701813041204283000000A7E7200000A8C300000010F00282600000A13051205FE16300000016F1A00000A6FB500000A26087243360070168D170000016FB600000A260872633600700F00282600000A13051205FE16300000016F1A00000A6F1600000A2607282000000A16FE01130611062C0D0872EC360070076F1600000A260872523700701A8D1700000125160F01281500000AA225170F02285200000A28B700000A8C4B000001A225180F03285200000A28B700000A8C4B000001A225190F04285200000A28B700000A8C4B000001A26FB600000A260872FF370070168D170000016FB600000A26086F1A00000A287D000006002A0013300600A00000003600001100731300000A0A067225380070168D170000016FB600000A260672493800701B8D1700000125160F02281500000AA225170F03281500000AA225180F04285200000A28B700000A8C4B000001A225190F05285200000A28B700000A8C4B000001A2251A0F06285200000A28B700000A8C4B000001A26FB600000A2606722C390070168D170000016FB600000A26066F1A00000A036F5300000A287E000006002A13300600C90000003700001100731300000A0A0672953B00701F200B1201283000000A7E7200000A8C300000010F00282600000A0C1202FE16300000016F1A00000A6FB500000A260672C63C0070168D170000016FB600000A260672463D00701A8D1700000125160F00282600000A0C1202FE16300000016F1A00000AA225170F01281500000AA225180F02285200000A28B700000A8C4B000001A225190F03285200000A28B700000A8C4B000001A26FB600000A260672B33E0070168D170000016FB600000A26066F1A00000A287D000006002A00000013300600160100003600001100731300000A0A067225380070168D170000016FB600000A260672DF3E00701A8D1700000125160F02281500000AA225170F03281500000AA225180F04285200000A28B700000A8C4B000001A225190F05285200000A28B700000A8C4B000001A26FB600000A26067250400070168D170000016FB600000A26066F1A00000A036F5300000A287E00000600731300000A0A0672B1420070168D170000016FB600000A260672CF4200701A8D1700000125160F02281500000AA225170F03281500000AA225180F04285200000A28B700000A8C4B000001A225190F05285200000A28B700000A8C4B000001A26FB600000A26067284430070168D170000016FB600000A26066F1A00000A036F5300000A287E000006002A000013300600C90000003700001100731300000A0A0672953B00701F100B1201283000000A7E7200000A8C300000010F00282600000A0C1202FE16300000016F1A00000A6FB500000A260672C63C0070168D170000016FB600000A260672094600701A8D1700000125160F00282600000A0C1202FE16300000016F1A00000AA225170F01281500000AA225180F02285200000A28B700000A8C4B000001A225190F03285200000A28B700000A8C4B000001A26FB600000A260672B33E0070168D170000016FB600000A26066F1A00000A287D000006002A00000013300600160100003600001100731300000A0A067225380070168D170000016FB600000A2606727A4700701A8D1700000125160F02281500000AA225170F03281500000AA225180F04285200000A28B700000A8C4B000001A225190F05285200000A28B700000A8C4B000001A26FB600000A260672EF480070168D170000016FB600000A26066F1A00000A036F5300000A287E00000600731300000A0A0672B1420070168D170000016FB600000A260672524B00701A8D1700000125160F02281500000AA225170F03281500000AA225180F04285200000A28B700000A8C4B000001A225190F05285200000A28B700000A8C4B000001A26FB600000A260672094C0070168D170000016FB600000A26066F1A00000A036F5300000A287E000006002A000013300600BE0000003700001100731300000A0A0672904E00701A0B1201283000000A0F00282600000A0C1202FE16300000016F1A00000A6FB800000A260672674F0070168D170000016FB600000A260672925000701A8D1700000125160F00282600000A0C1202FE16300000016F1A00000AA225170F01281500000AA225180F02285200000A28B700000A8C4B000001A225190F03285200000A28B700000A8C4B000001A26FB600000A2606724B510070168D170000016FB600000A26066F1A00000A287D000006002A0000133006008C0000003600001100731300000A0A067277510070168D170000016FB600000A260672A55100701A8D1700000125160F02281500000AA225170F03281500000AA225180F04285200000A28B700000A8C4B000001A225190F05285200000A28B700000A8C4B000001A26FB600000A2606724A520070168D170000016FB600000A26066F1A00000A036F5300000A287E000006002A13300600D00000003700001100731300000A0A0672FF5400701E0B1201283000000A0F00282600000A0C1202FE16300000016F1A00000A6FB800000A260672D8550070168D170000016FB600000A26067297560070168D170000016FB600000A260672965700701A8D1700000125160F00282600000A0C1202FE16300000016F1A00000AA225170F01281500000AA225180F02285200000A28B700000A8C4B000001A225190F03285200000A28B700000A8C4B000001A26FB600000A26067275580070168D170000016FB600000A26066F1A00000A287D000006002A133006008C0000003600001100731300000A0A0672A3580070168D170000016FB600000A260672CB5800701A8D1700000125160F02281500000AA225170F03281500000AA225180F04285200000A28B700000A8C4B000001A225190F05285200000A28B700000A8C4B000001A26FB600000A2606729C590070168D170000016FB600000A26066F1A00000A036F5300000A287E000006002A1330060037010000350000110072195C00701F11734000000A0A140B060F01281500000A6F4100000A0D092C13060F01281500000A6F4200000A6F4300000A0B731300000A0C0872275C00701713041204283000000A7E7200000A8C300000010F00282600000A13051205FE16300000016F1A00000A6FB500000A26087243360070168D170000016FB600000A260872345D00700F00282600000A13051205FE16300000016F1A00000A6F1600000A2607282000000A16FE01130611062C0D0872BB5D0070076F1600000A260872ED5D00701A8D1700000125160F01281500000AA225170F02285200000A28B700000A8C4B000001A225180F03285200000A28B700000A8C4B000001A225190F04285200000A28B700000A8C4B000001A26FB600000A260872FF370070168D170000016FB600000A26086F1A00000A287D000006002A0013300600A00000003600001100731300000A0A067225380070168D170000016FB600000A260672885E00701B8D1700000125160F02281500000AA225170F03281500000AA225180F04285200000A28B700000A8C4B000001A225190F05285200000A28B700000A8C4B000001A2251A0F06285200000A28B700000A8C4B000001A26FB600000A260672495F0070168D170000016FB600000A26066F1A00000A036F5300000A287E000006002A13300500DB00000038000011000F01283F00000A0A287C0000060B289F00000A076FB900000A0006175F17FE010C082C1E0002040E040E050E0628650000060002050E040E050E062865000006000006185F18FE010D092C1E0002040E040E050E06285B0000060002050E040E050E06285B0000060000061A5F1AFE01130411042C0C02040E040E05286100000600061E5F1EFE01130511052C0C02040E040E05286300000600061F105F1F10FE01130611062C0C02040E040E05285F00000600061F205F1F20FE01130711072C0C02040E040E05285D00000600289F00000A6FBA00000A002A00133007001B0100003900001100737900000A0A06036F5300000A6F7A00000A00140B0672A46100706FBB00000A0B0714FE030C082C24000203040E050E060E070E082866000006000203050E050E060E070E08286600000600000672DC6100706FBB00000A0B0714FE030D092C24000203040E050E060E070E08285C000006000203050E050E060E070E08285C00000600000672226200706FBB00000A0B0714FE03130411042C0F0203040E040E060E072862000006000672566200706FBB00000A0B0714FE03130511052C0F0203040E040E060E072864000006000672946200706FBB00000A0B0714FE03130611062C0F0203040E040E060E072860000006000672D46200706FBB00000A0B0714FE03130711072C0F0203040E040E060E07285E000006002A0013300500370100003A000011000F01281500000A0F03285200000A28830000060A731300000A0B077225380070168D170000016FB600000A26077212630070066F1A00000A066FBC00000A8C4B0000010F02281500000A6FB500000A260772976300700F00282600000A0C1202FE16300000016F1A00000A6F1600000A26077226640070066F1A00000A066FBC00000A8C4B0000016FB800000A26076F1A00000A287F00000600731300000A0B077225380070168D170000016FB600000A2607727A6400700F01281500000A0F02281500000A0F03285200000A28B700000A8C4B0000016FB500000A260772436500700F00282600000A0C1202FE16300000016F1A00000A0F01281500000A6FB800000A260772386600700F01281500000A0F03285200000A28B700000A8C4B0000016FB800000A26076F1A00000A287F000006002A00133007001E0000003B000011007E7200000A0A027E7200000A037E7200000A04051200286E000006002A4A00020304050E040E050E06286E000006002A0000001B300600990400003C00001100026F2F00000A16310B036F2F00000A16FE022B01160A06397B04000000728F140070732400000A0B737900000A0C737900000A0D000372F30C00706FBD00000A2C0D0372F30C00706FBE00000A2B0116131311132C18080317036F2F00000A18596F5100000A6F7A00000A002B0808036F7A00000A000272F30C00706FBD00000A2C0D0272F30C00706FBE00000A2B0116131411142C18090217026F2F00000A18596F5100000A6F7A00000A002B0809026F7A00000A00096FBF00000A72C96600706FA500000A130411046F7E00000A73C000000A13050011046F9200000A13152B2911156F3400000A74370000011316001105720567007011166F8D00000A6FC100000A6F1600000A260011156F3A00000A2DCEDE1611157522000001131711172C0811176F3B00000A00DC11056F1A00000A130611066F2F00000A17FE02131811182C1311061611066F2F00000A17596F5100000A1306732500000A130772136700701106282200000A1308110807732700000A13091109166FA200000A00076F2800000A001109732A00000A130A110A11076F2B00000A26076F4F00000A007E1F00000A130B14130C7E1F00000A130D7E1F00000A130E7E1F00000A130F0011076F2C00000A6F3300000A1319389A01000011196F3400000A7421000001131A007E1F00000A130F111A72E26700706F3500000A6F1A00000A130D111A72066800706F3500000A6F1A00000A130E737900000A130C110C111A72E26700706F3500000A6F1A00000A6F7A00000A00086FBF00000A72C96600706FA500000A131B110C6FBF00000A72206800706FBB00000A131C111C6F8200000A72486800706F8300000A0F02FE16300000016F1A00000A6FC200000A00111C6F8200000A725E6800706F8300000A28C300000A131D121D727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A0000111B6F9200000A131E2B63111E6F3400000A7437000001131F00111F6F8D00000A6FC100000A1320110E11206F3700000A6F3900000A2D13110E11206FC100000A6F3900000A16FE012B0116132111212C1B00727C6800701120282200000A1322110F1122283100000A130F0000111E6F3A00000A2D94DE16111E7522000001131711172C0811176F3B00000A00DC111A72E2670070110C6F8000000A6FC600000A00111A7206680070110E110F283100000A6FC600000A000011196F3A00000A3A5AFEFFFFDE1611197522000001131711172C0811176F3B00000A00DC076F2800000A0072276A007013107E1F00000A13111413120011076F2C00000A6F3300000A13232B7B11236F3400000A74210000011324001110112472E26700706F3500000A6F1A00000A72F30C007072F70C00706F3200000A112472066800706F3500000A6F1A00000A112472F26A00706F3500000A6F1A00000A282300000A1311111107732700000A13121112166FA200000A00289F00000A11126FA000000A000011236F3A00000A3A79FFFFFFDE1611237522000001131711172C0811176F3B00000A00DC076F4F00000A0000DE1013250072146B0070112573C700000A7ADE1F00072C0B076FC800000A16FE032B0116132611262C07076F4F00000A0000DC002A0000004194000002000000E20000003600000018010000160000000000000002000000BF020000700000002F030000160000000000000002000000D1010000AD0100007E030000160000000000000002000000BB0300008B00000046040000160000000000000000000000350000003104000066040000100000003C000001020000003500000043040000780400001F000000000000001B300600780500003D00001100026F2F00000A16310B036F2F00000A16FE022B01160A06395A05000000728F140070732400000A0B737900000A0C737900000A0D000372F30C00706FBD00000A2C0D0372F30C00706FBE00000A2B0116131111112C18080317036F2F00000A18596F5100000A6F7A00000A002B0808036F7A00000A000272F30C00706FBD00000A2C0D0272F30C00706FBE00000A2B0116131211122C18090217026F2F00000A18596F5100000A6F7A00000A002B0809026F7A00000A00096FBF00000A72C96600706FA500000A130411046F7E00000A73C000000A13050011046F9200000A13132B2911136F3400000A74370000011314001105720567007011146F8D00000A6FC100000A6F1600000A260011136F3A00000A2DCEDE1611137522000001131511152C0811156F3B00000A00DC11056F1A00000A130611066F2F00000A17FE02131611162C1311061611066F2F00000A17596F5100000A1306732500000A1307723A6B00701106282200000A1308110807732700000A13091109166FA200000A00076F2800000A001109732A00000A130A110A11076F2B00000A26076F4F00000A007E1F00000A130B14130C14130D0011076F2C00000A6F3300000A1317388B02000011176F3400000A7421000001131800737900000A130C110C111872E26700706F3500000A6F1A00000A6F7A00000A00737900000A130D110D72056C0070111872066800706F3500000A6F1A00000A721F6C0070282E00000A6F7A00000A00086FBF00000A72C96600706FA500000A1319110C6FBF00000A72206800706FBB00000A131A111A6F8200000A72486800706F8300000A0F02FE16300000016F1A00000A6FC200000A00111A6F8200000A725E6800706F8300000A28C300000A131B121B727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A000011196F9200000A131C3821010000111C6F3400000A7437000001131D00111D6F8D00000A6FC100000A131E110D6FBF00000A723B6C0070111E282200000A6FA500000A131F110D6FBF00000A723B6C0070111E6F3700000A282200000A6FA500000A1320111F6F7E00000A2D0C11206F7E00000A16FE022B0116132111212C041120131F111F6F7E00000A16310C11206F7E00000A16FE012B0116132211222C04111F13201613232B7300112317FE01132411242C041120131F00111F6F9200000A13252B2F11256F3400000A743700000113260011266FC900000A14FE03132711272C0F11266FC900000A11266FCA00000A260011256F3A00000A2DC8DE1611257522000001131511152C0811156F3B00000A00DC00112317581323112317FE0216FE01132811283A7CFFFFFF00111C6F3A00000A3AD3FEFFFFDE16111C7522000001131511152C0811156F3B00000A00DC111872E2670070110C6F8000000A6FC600000A0011187206680070110D6FCB00000A72056C00707E1F00000A6F3200000A721F6C00707E1F00000A6F3200000A72696C00707E1F00000A6F3200000A6FC600000A000011176F3A00000A3A69FDFFFFDE1611177522000001131511152C0811156F3B00000A00DC076F2800000A0072856C0070130E7E1F00000A130F1413100011076F2C00000A6F3300000A13292B7B11296F3400000A7421000001132A00110E112A72E26700706F3500000A6F1A00000A72F30C007072F70C00706F3200000A112A72066800706F3500000A6F1A00000A112A72F26A00706F3500000A6F1A00000A282300000A130F110F07732700000A13101110166FA200000A00289F00000A11106FA000000A000011296F3A00000A3A79FFFFFFDE1611297522000001131511152C0811156F3B00000A00DC076F4F00000A0000DE10132B0072146B0070112B73C700000A7ADE1F00072C0B076FC800000A16FE032B0116132C112C2C07076F4F00000A0000DC002A41AC000002000000E200000036000000180100001600000000000000020000006A0300003C000000A6030000160000000000000002000000AF02000034010000E3030000160000000000000002000000BF0100009E0200005D0400001600000000000000020000009A0400008B00000025050000160000000000000000000000350000001005000045050000100000003C000001020000003500000022050000570500001F000000000000001B300700960300003E00001100728F140070732400000A0A140B0E067E7200000A8130000001737900000A0C737900000A0D000008046F5300000A6F7A00000A0072506D00700F00FE16300000016F1A00000A282200000A1304110406732700000A13051105166FA200000A00066F2800000A0011056F5E00000A6F1A00000A0B066F4F00000A0009076F7A00000A007E1F00000A1306141307086FBF00000A6F8200000A72B46D00706F8300000A13087E7200000A1309110814FE03130A110A2C20000011086F8400000A739500000A130900DE0C26007E7200000A130900DE00000F017E7200000A28CC00000A130B110B39E20000000012097E7200000A28CC00000A130C110C2C0C0072BA6D007073CD00000A7A72226E00701209FE16300000016F1A00000A6FC100000A282200000A1306096FBF00000A11066FBB00000A1307110714FE01130D110D2C2F0072226E00701209FE16300000016F1A00000A6F3700000A282200000A1306096FBF00000A11066FBB00000A130700110714FE03130E110E2C4B000E040E05086FBF00000A11070973CE00000A17287300000600086FBF00000A6FCF00000A130F110F2C1600086FBF00000A11070E040E050914287100000600000E0611098130000001000038000100000012097E7200000A28CC00000A1310111039E90000000072226E00700F01FE16300000016F1A00000A6FC100000A282200000A1306096FBF00000A11066FBB00000A1311111114FE01131211122C2F0072226E00700F01FE16300000016F1A00000A6F3700000A282200000A1306096FBF00000A11066FBB00000A131100111114FE01131311132C0C0072546E007073CD00000A7A72986E0070050E05086FBF00000A091728720000061307111111076FB400000A26086FBF00000A6FCF00000A131411142C1500086FBF00000A1107050E050914287100000600000E0611076F8200000A72B46D00706F8300000A6F8400000A739500000A81300000010000110714FE03131511152C670009110728700000060072B06E0070096F8000000A72F30C007072F70C00706F3200000A0F00FE16300000016F1A00000A286300000A1304110406732700000A13051105166FA200000A00066F2800000A00289F00000A11056FA000000A00066F4F00000A000000DE1013160072146B0070111673C700000A7A00DE1013170072146B0070111773C700000A7ADE2D00062C0B066FC800000A16FE032B0116131811182C07066F4F00000A00086FD000000A00096FD000000A0000DC2A00004164000000000000B800000012000000CA0000000C0000001700000100000000270000001C03000043030000100000003C00000100000000260000003003000056030000100000003C000001020000002600000042030000680300002D000000000000001B300600120600003F00001100036F2F00000A16310B046F2F00000A16FE022B01160A0639F405000000728F140070732400000A0B737900000A0C737900000A0D000472F30C00706FBD00000A2C0D0472F30C00706FBE00000A2B0116131011102C18080417046F2F00000A18596F5100000A6F7A00000A002B0808046F7A00000A000372F30C00706FBD00000A2C0D0372F30C00706FBE00000A2B0116131111112C18090317036F2F00000A18596F5100000A6F7A00000A002B0809036F7A00000A00096FBF00000A72C96600706FA500000A130411046F7E00000A73C000000A13050011046F9200000A13122B2911126F3400000A74370000011313001105720567007011136F8D00000A6FC100000A6F1600000A260011126F3A00000A2DCEDE1611127522000001131411142C0811146F3B00000A00DC11056F1A00000A130611066F2F00000A17FE02131511152C1311061611066F2F00000A17596F5100000A1306732500000A130772226F00700F00FE16300000016F1A00000A1106286300000A1308110807732700000A13091109166FA200000A00076F2800000A001109732A00000A130A110A11076F2B00000A26076F4F00000A007E1F00000A130B14130C0011076F2C00000A6F3300000A1316382F03000011166F3400000A7421000001131700737900000A130C110C111772E26700706F3500000A6F1A00000A6F7A00000A00086FBF00000A72C96600706FA500000A1318110C6FBF00000A72206800706FBB00000A131911196F8200000A72486800706F8300000A0F03FE16300000016F1A00000A6FC200000A0011196F8200000A725E6800706F8300000A28C300000A131A121A727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A000011186F9200000A131B3835020000111B6F3400000A7437000001131C00111C6F8D00000A6FC100000A131D0E041728D100000A28D200000A28D300000A131E111E394201000000110C1772F56F0070146FD400000A131F110C72B46D00706FD500000A13201120111D6FC200000A00111F6F8200000A11206FD600000A26110C72097000706FD500000A132011207E7200000A13211221FE16300000016F1A00000A6FC100000A6FC200000A00111F6F8200000A11206FD600000A26110C721D7000706FD500000A132011207E7200000A13211221FE16300000016F1A00000A6FC100000A6FC200000A00111F6F8200000A11206FD600000A26110C72397000706FD500000A1320112072E30E00706FC200000A00111F6F8200000A11206FD600000A26110C72557000706FD500000A13201120726B7000706FC200000A00111F6F8200000A11206FD600000A26110C72777000706FD500000A13201120726B7000706FC200000A00111F6F8200000A11206FD600000A261119111F6FB400000A260038BA000000001119723B6C0070111D282200000A6FA500000A13221119723B6C0070111D6F3700000A282200000A6FA500000A13231613242B7300112417FE01132511252C04112313220011226F9200000A13262B2F11266F3400000A743700000113270011276FC900000A14FE03132811282C0F11276FC900000A11276FCA00000A260011266F3A00000A2DC8DE1611267522000001131411142C0811146F3B00000A00DC00112417581324112417FE0216FE01132911293A7CFFFFFF0000111B6F3A00000A3ABFFDFFFFDE16111B7522000001131411142C0811146F3B00000A00DC111772E2670070110C6F8000000A6FC600000A000011166F3A00000A3AC5FCFFFFDE1611167522000001131411142C0811146F3B00000A00DC076F2800000A00728B700070130D7E1F00000A130E14130F0011076F2C00000A6F3300000A132A2B6A112A6F3400000A7421000001132B00110D112B72E26700706F3500000A6F1A00000A72F30C007072F70C00706F3200000A112B72F26A00706F3500000A6F1A00000A286300000A130E110E07732700000A130F110F166FA200000A00289F00000A110F6FA000000A0000112A6F3A00000A2D8DDE16112A7522000001131411142C0811146F3B00000A00DC076F4F00000A0000DE10132C0072146B0070112C73C700000A7ADE1F00072C0B076FC800000A16FE032B0116132D112D2C07076F4F00000A0000DC002A000041AC000002000000E20000003600000018010000160000000000000002000000580400003C000000940400001600000000000000020000008A02000048020000D2040000160000000000000002000000C9010000420300000B0500001600000000000000020000004805000077000000BF05000016000000000000000000000035000000AA050000DF050000100000003C0000010200000035000000BC050000F10500001F0000000000000013300500830600004000001100036F8200000A722C7100706F8300000A0A036F8200000A725C7100706F8300000A0B036F8200000A72887100706F8300000A0C036F8200000A72A67100706F8300000A0D036F8200000A72C47100706F8300000A1304036F8200000A72DE7100706F8300000A1305062C0D066F8400000A28A100000A2B01161306072C0D076F8400000A28A100000A2B01161307082C08086F8400000A2B057E1F00000A1308092C0D096F8400000A28A100000A2B0116130911042C0E11046F8400000A28A100000A2B0116130A11052C0911056F8400000A2B057E1F00000A130B036F8200000A72B46D00706F8300000A6F8400000A130C72EA710070110C282200000A130D026FBF00000A110D6FBB00000A130E110E14FE03130F110F396305000000721C720070130D110E110D6FA500000A131016131638B602000000111011166F8100000A1317110613181118395B0100000011176F8200000A72887100706F8300000A1319111914FE03131A111A3910010000001108282000000A16FE01131B111B39F90000000011196F8400000A282000000A131C111C2C1100111911086FC200000A000038D4000000001108178D5500000125161F2C9D6F5F00000A131D11196F8400000A178D5500000125161F2C9D6F5F00000A131E73CE00000A131F00111E13201613212B19112011219A132200111F11226FD700000A2600112117581321112111208E6932DF00111D13231613242B39112311249A132500111F11256FD800000A2D0C1125282000000A16FE012B0116132611262C0C00111F11256FD700000A260000112417581324112411238E6932BF11197221230070111FD045000001285A00000A6FD900000A740700001B289100000A6FC200000A000000002B28000272887100706FD500000A1319111911086FC200000A0011176F8200000A11196FD600000A26000011091327112739320100000011176F8200000A72DE7100706F8300000A1328112814FE031329112939E700000000110B282000000A16FE01132A112A39D00000000011286F8400000A282000000A132B112B2C11001128110B6FC200000A000038AB00000000110B178D5500000125161F2C9D6F5F00000A132C11286F8400000A178D5500000125161F2C9D6F5F00000A132D112D28DA00000A132E00112C132F1613302B39112F11309A133100112E11316FD800000A2D0C1131282000000A16FE012B0116133211322C0C00112E11316FD700000A2600001130175813301130112F8E6932BF11287221230070112ED045000001285A00000A6FD900000A740700001B289100000A6FC200000A000000002B28000272DE7100706FD500000A13281128110B6FC200000A0011176F8200000A11286FD600000A26000000111617581316111611106F7E00000AFE04133311333A36FDFFFF036FC900000A131111116F8200000A72887100706F8300000A131211116F8200000A72DE7100706F8300000A131311122C0911126F8400000A2B057E1F00000A131411132C0911136F8400000A2B057E1F00000A13151107133411343908010000001114282000000A16FE011335113539F3000000001108178D5500000125161F2C9D6F5F00000A13361114178D5500000125161F2C9D6F5F00000A1337113628DA00000A1338001137133916133A2B391139113A9A133B001138113B6FD800000A2D0C113B282000000A16FE012B0116133C113C2C0C001138113B6FD700000A260000113A1758133A113A11398E6932BF0814FE03133D113D2C3000036F8200000A72887100706F8300000A722123007011386FDB00000A740700001B289100000A6FC200000A00002B3B000272887100706FD500000A133E113E722123007011386FDB00000A740700001B289100000A6FC200000A00036F8200000A113E6FD600000A26000000110A133F113F3909010000001115282000000A16FE011340114039F400000000110B178D5500000125161F2C9D6F5F00000A13411115178D5500000125161F2C9D6F5F00000A1342114128DA00000A134300114213441613452B39114411459A134600114311466FD800000A2D0C1146282000000A16FE012B0116134711472C0C00114311466FD700000A260000114517581345114511448E6932BF110514FE03134811482C3000036F8200000A72DE7100706F8300000A722123007011436FDB00000A740700001B289100000A6FC200000A00002B3B000272887100706FD500000A13491149722123007011436FDB00000A740700001B289100000A6FC200000A00036F8200000A11496FD600000A26000000002A001B300700DE010000410000110000026FDC00000A6F9200000A0A38A8010000066F3400000A74370000010B00076FDD00000A6F3700000A0C076F8200000A72B46D00706F8300000A6F8400000A6FC100000A0D7E1F00000A1304141305141306161307081309110913081108391C0100001108724C720070282100000A2D2711087264720070282100000A3A8700000011087282720070282100000A3AB100000038E700000072AA72007009282200000A13040311046FBB00000A1305110514FE03130A110A2C360004050711050E0473CE00000A1728730000060011051005076FCF00000A130B110B2C0F07110504050E041105287100000600002B170017130772986E00700405070E041728720000061306002B7672E672007009282200000A13040311046FBB00000A1305110514FE01130C110C2C170017130772287300700405070E041728720000061306002B3B724673007009282200000A13040311046FBB00000A1305110514FE01130D110D2C170017130772927300700405070E041628720000061306002B0011072C07110614FE032B0116130E110E2C27000E0514FE03130F110F2C0F000311060E056FDE00000A26002B0B000311066FB400000A26000000066F3A00000A3A4DFEFFFFDE15067522000001131011102C0811106F3B00000A00DC2A0000411C0000020000000E000000BA010000C801000015000000000000001B300600D201000042000011000E04026FDF00000A0A00056F8200000A6FE000000A0B2B36076F3400000A743D0000010C000E04086FDD00000A6FD500000A0D09086F8400000A6FC200000A00066F8200000A096FD600000A2600076F3A00000A2DC2DE15077522000001130411042C0811046F3B00000A00DC0E051305110539510100000028E100000A1306066F8200000A72B46D00706F8300000A1206FE16300000016F1A00000A6FC100000A6FC200000A00066F8200000A72BA7300706F8300000A14FE03130711072C2C00066F8200000A72BA7300706F8300000A0F01FE16300000016F1A00000A6FC100000A6FC200000A00002B38000E0472BA7300706FD500000A130811080F01FE16300000016F1A00000A6FC100000A6FC200000A00066F8200000A11086FD600000A2600066F8200000A72CE7300706F8300000A14FE03130911092C3F00066F8200000A72CE7300706F8300000A0F0228E200000A130A120A727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A00002B4B000E0472CE7300706FD500000A130B110B0F0228E200000A130A120A727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A00066F8200000A110B6FD600000A26000006130C2B00110C2A0000011000000200170042590015000000001B30060069020000430000110000046F8200000A6FE000000A0A3806010000066F3400000A743D0000010B000E05076FDD00000A6FD800000A16FE010C0839E100000000076F8400000A0D7E7200000A13040009739500000A130400DE0C26007E7200000A130400DE00056F8200000A076FDD00000A6F8300000A1305110514FE03130611062C400011047E7200000A28E300000A130711072C1C11051204FE16300000016F1A00000A6FC100000A6FC200000A002B0E1105076F8400000A6FC200000A00002B5B000E04076FDD00000A6FD500000A130811047E7200000A28E300000A130911092C1C11081204FE16300000016F1A00000A6FC100000A6FC200000A002B0E1108076F8400000A6FC200000A00056F8200000A11086FD600000A26000000066F3A00000A3AEFFEFFFFDE15067522000001130A110A2C08110A6F3B00000A00DC0E06130B110B392201000000056F8200000A72486800706F8300000A14FE03130C110C2C2C00056F8200000A72486800706F8300000A0F00FE16300000016F1A00000A6FC100000A6FC200000A00002B38000E0472486800706FD500000A130D110D0F00FE16300000016F1A00000A6FC100000A6FC200000A00056F8200000A110D6FD600000A2600056F8200000A725E6800706F8300000A14FE03130E110E2C3F00056F8200000A725E6800706F8300000A0F0128E200000A130F120F727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A00002B4B000E04725E6800706FD500000A131011100F0128E200000A130F120F727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A00056F8200000A11106FD600000A2600002A0000004134000000000000460000000C000000520000000C00000040000001020000000E000000180100002601000015000000000000001B30070039040000440000110072E6730070168D17000001283D00000A0A03066FA500000A0B7208740070168D17000001283D00000A0C03086FA500000A0D7222740070168D17000001283D00000A13040311046FA500000A130500076F9200000A130938B200000011096F3400000A7437000001130A00110A6F8200000A72B46D00706F8300000A6F8400000A6F1A00000A130B723E740070110B6FC100000A282200000A130C02110C6FBB00000A130D110D14FE01130E110E2C1F00723E740070110B6F3700000A282200000A130C02110C6FBB00000A130D00110D14FE03130F110F2C28007E7200000A7EA900000A110D110A0473CE00000A1628730000060002110D6FCA00000A26002B0B0003110A6FCA00000A26000011096F3A00000A3A42FFFFFFDE1611097522000001131011102C0811106F3B00000A00DC02066FA500000A13060011066F9200000A13112B3311116F3400000A743700000113120072F56F00707E7200000A7EA900000A11120416287200000613130311136FB400000A260011116F3A00000A2DC4DE1611117522000001131011102C0811106F3B00000A00DC00096F9200000A131438B200000011146F3400000A743700000113150011156F8200000A72B46D00706F8300000A6F8400000A6F1A00000A1316727674007011166FC100000A282200000A13170211176FBB00000A1318111814FE01131911192C1F00727674007011166F3700000A282200000A13170211176FBB00000A131800111814FE03131A111A2C28007E7200000A7EA900000A111811150473CE00000A162873000006000211186FCA00000A26002B0B000311156FCA00000A26000011146F3A00000A3A42FFFFFFDE1611147522000001131011102C0811106F3B00000A00DC02086FA500000A13070011076F9200000A131B2B33111B6F3400000A7437000001131C0072A67400707E7200000A7EA900000A111C04162872000006131D03111D6FB400000A2600111B6F3A00000A2DC4DE16111B7522000001131011102C0811106F3B00000A00DC0011056F9200000A131E38B2000000111E6F3400000A7437000001131F00111F6F8200000A72B46D00706F8300000A6F8400000A6F1A00000A132072B274007011206FC100000A282200000A13210211216FBB00000A1322112214FE01132311232C1F0072B274007011206F3700000A282200000A13210211216FBB00000A132200112214FE03132411242C28007E7200000A7EA900000A1122111F0473CE00000A162873000006000211226FCA00000A26002B0B0003111F6FCA00000A260000111E6F3A00000A3A42FFFFFFDE16111E7522000001131011102C0811106F3B00000A00DC0211046FA500000A13080011086F9200000A13252B3311256F3400000A743700000113260072E47400707E7200000A7EA900000A11260416287200000613270311276FB400000A260011256F3A00000A2DC4DE1611257522000001131011102C0811106F3B00000A00DC2A000000014C000002005800C51D0116000000000200460140860116000000000200A501C56A0216000000000200930240D30216000000000200F302C5B80316000000000200E203402204160000000013300600BB0100004500001100026F8200000A72F27400706F8300000A14FE030A062C3F00026F8200000A72F27400706F8300000A6F8400000A28E400000A0B0717580C026F8200000A72F27400706F8300000A0828E500000A6FC200000A00002B27000372F27400706FD500000A0D0972E30E00706FC200000A00026F8200000A096FD600000A2600026F8200000A720C7500706F8300000A14FE03130411042C3D00026F8200000A720C7500706F8300000A28C300000A13051205727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A00002B480003720C7500706FD500000A1306110628C300000A13051205727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A00026F8200000A11066FD600000A2600026F8200000A72247500706F8300000A14FE03130711072C3D00026F8200000A720C7500706F8300000A28C300000A13051205727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A00002B48000372247500706FD500000A1308110828C300000A13051205727868007028C400000A178D5500000125161F5A9D6FC500000A6FC200000A00026F8200000A11086FD600000A26002A001B300700B803000046000011000E057E7200000A8130000001728F140070732400000A0A140B00737900000A0C737900000A0D72506D00700F00FE16300000016F1A00000A282200000A1304110406732700000A13051105166FA200000A00066F2800000A0011056F5E00000A6F1A00000A0B066F4F00000A0009076F7A00000A0072226E00700F01FE16300000016F1A00000A6FC100000A282200000A1306096FBF00000A11066FBB00000A1307110714FE01130B110B2C2F0072226E00700F01FE16300000016F1A00000A6F3700000A282200000A1306096FBF00000A11066FBB00000A130700110714FE01130C110C2C0C00724875007073CD00000A7A0000080E046F5300000A6F7A00000A0000DE042600FE1A7E7200000A1308161309086FBF00000A6F8200000A72B46D00706F8300000A130A110A14FE03130D110D2C5300110A6F8400000A282000000A130E110E2C0700171309002B380000110A6F8400000A739500000A130800DE0D2600729A75007073CD00000A7A12087E7200000A28CC00000A130F110F2C0500171309000000110916FE0113101110393B0100000072CE7500701208FE16300000016F1A00000A6FC100000A282200000A1306096FBF00000A11066FA500000A131111112C0C11116F7E00000A16FE012B0117131211122C2F0072CE7500701208FE16300000016F1A00000A6F3700000A282200000A1306096FBF00000A11066FA500000A13110011112C0C11116F7E00000A16FE022B01161313111339A100000000161314388300000000111111146F8100000A131573CE00000A1316111672F27400706FD700000A261116720C7500706FD700000A26111672247500706FD700000A260528E600000A28E700000A086FBF00000A1115091116172873000006000E0628D300000A131711172C09111509287500000600086FBF00000A11150928740000060000111417581314111411116F7E00000AFE04131811183A69FFFFFF000E0511088130000001002B7F0072287300700428E600000A28E700000A086FBF00000A091728720000061319111914FE03131A111A2C5300110711196FB400000A260E0628D300000A131B111B2C09111909287500000600086FBF00000A1119092874000006000E0511196F8200000A72B46D00706F8300000A6F8400000A739500000A8130000001000072B06E0070096F8000000A72F30C007072F70C00706F3200000A0F00FE16300000016F1A00000A286300000A1304110406732700000A13051105166FA200000A00066F2800000A00289F00000A11056FA000000A00066F4F00000A0000DE06131C00111C7A2A414C000000000000F500000012000000070100000400000017000001000000005201000012000000640100000D00000017000001000000001A00000097030000B1030000060000003C0000011B300600800500004700001100728F140070732400000A0A140B00737900000A0C737900000A0D72506D00700F00FE16300000016F1A00000A282200000A1304110406732700000A13051105166FA200000A00066F2800000A0011056F5E00000A6F1A00000A0B066F4F00000A0009076F7A00000A0072226E00700F01FE16300000016F1A00000A6FC100000A282200000A1306096FBF00000A11066FBB00000A1307110714FE01130B110B2C2F0072226E00700F01FE16300000016F1A00000A6F3700000A282200000A1306096FBF00000A11066FBB00000A130700110714FE01130C110C2C0C00720676007073CD00000A7A723C7600701306110711066FA500000A13080E0528E800000A28D300000A130D110D2C49000011086F9200000A130E2B1A110E6F3400000A7437000001130F001107110F6FCA00000A2600110E6F3A00000A2DDDDE16110E7522000001131011102C0811106F3B00000A00DC0073E900000A1309110711066FA500000A13080011086F9200000A13112B3011116F3400000A7437000001131200110911126F8200000A72B46D00706F8300000A6F8400000A11126FEA00000A000011116F3A00000A2DC7DE1611117522000001131011102C0811106F3B00000A00DC16130A0E0428D300000A1313111339F00000000011076FC900000A11066FA500000A13140011146F9200000A131538AB00000011156F3400000A743700000113160011166F8200000A72B46D00706F8300000A6F8400000A1317110911176FC100000A6FEB00000A130A110A16FE01131811182C1200110911176F3700000A6FEB00000A130A00110A16FE01131911192C4B0072927300707E7200000A7EA900000A111609162872000006131A1107111A6FB400000A261109111A6F8200000A72B46D00706F8300000A6F8400000A6FC100000A111A6FEA00000A00000011156F3A00000A3A49FFFFFFDE1611157522000001131011102C0811106F3B00000A00DC000000087272760070046F5300000A728A760070282E00000A6F7A00000A0000DE06131B00111B7A00086FBF00000A6FDC00000A6F9200000A131C38AB000000111C6F3400000A7437000001131D00111D6F8200000A72B46D00706F8300000A6F8400000A131E1109111E6FC100000A6FEB00000A130A110A16FE01131F111F2C12001109111E6F3700000A6FEB00000A130A00110A16FE01132011202C4B0072927300707E7200000A7EA900000A111D091628720000061321110711216FB400000A26110911216F8200000A72B46D00706F8300000A6F8400000A6FC100000A11216FEA00000A000000111C6F3A00000A3A49FFFFFFDE16111C7522000001131011102C0811106F3B00000A00DC0528D300000A13221122393301000000721C7200701306110711066FA500000A13230011236F9200000A132438EC00000011246F3400000A74370000011325000011096FEC00000A6FED00000A132638A400000011266F3400000A7445000001132700724673007011276F1A00000A6FC100000A282200000A1306112511066FBB00000A1328112814FE01132911292C2500724673007011276F1A00000A6F3700000A282200000A1306112511066FBB00000A132800112814FE01132A112A2C3600110911276FEE00000A7437000001132B72927300707E7200000A7EA900000A112B09162872000006132C1125112C6FB400000A26000011266F3A00000A3A50FFFFFFDE1611267522000001131011102C0811106F3B00000A00DC0011246F3A00000A3A08FFFFFFDE1611247522000001131011102C0811106F3B00000A00DC0014130972B06E0070096F8000000A72F30C007072F70C00706F3200000A0F00FE16300000016F1A00000A286300000A1304110406732700000A13051105166FA200000A00066F2800000A00289F00000A11056FA000000A00066F4F00000A0000DE06132D00112D7A2A41C400000200000017010000270000003E010000160000000000000002000000710100003D000000AE010000160000000000000002000000F2010000BE000000B0020000160000000000000000000000C802000020000000E8020000060000003C0000010200000001030000BE000000BF03000016000000000000000200000024040000B7000000DB04000016000000000000000200000001040000FF000000000500001600000000000000000000000E0000006B05000079050000060000003C00000113300200650000004800001100037E02000004734000000A0A06026F4200000A0B731300000A0C2B2A0008076F4300000A6F1900000A26076FEF00000A0B076FF000000A0D092C0A0008046F1900000A260000076FF000000A130411042DCA086F1A00000A731B00000A13052B0011052A221F2380020000042A0000133006006100000049000011001B8D65000001251672B70C00701F0E73F100000AA22517724C1B00701F0C156A73F200000AA2251872A47600701E73F100000AA2251972B67600701F0E73F100000AA2251A72C87600701F0C156A73F200000AA273F300000A0A060B2B00072A00000013300400E90000004A0000110072DA760070732400000A0A066F4600000A0B07026F2900000A00066F2800000A00076F4A00000A0C287C0000060D086F4B00000A13041104399C0000000038860000000009160872B70C00706F4C00000AA5300000016FF400000A00091708724C1B00706F4C00000A74450000016FF500000A0009180872A47600706F4C00000AA54B0000016FF600000A0009190872B67600706F4C00000AA5300000016FF400000A00091A0872C87600706F4C00000A74450000016FF500000A00289F00000A096FF700000A0000086F4D00000A130511053A6BFFFFFF00086F4E00000A00066F4F00000A002A000000133003004A0000004B0000110072DA760070732400000A0A066F4600000A0B07026F2900000A00076F4700000A720E7700701F196F4800000A036F4900000A00066F2800000A00076F9A00000A26066F4F00000A002A000013300200310000004B0000110072DA760070732400000A0A066F4600000A0B07026F2900000A00066F2800000A00076F9A00000A26066F4F00000A002A00000013300300A10000004C000011000314FE010B072C077E1F00000A10010328F800000A1001050C082C127224770070037224770070282E00000A1001140A02130411040D091A300C09172E1C2B00091A2E242B41091E2E222B00091F102E2E2B00091F202E2B2B2D722A77007003283100000A0A2B2A030A2B2672647700700372AC770070282E00000A0A2B13030A2B0F030A2B0B72F077007073CD00000A7A0604288200000613052B0011052A000000133003007E0000004D000011000314FE010B072C077E1F00000A10010328F800000A1001050C082C127224770070037224770070282E00000A1001140A02130411040D0917594504000000020000000A000000160000000E0000002B1472127800700A2B17030A2B13725C7800700A2B0B72F378007073CD00000A7A0604288200000613052B0011052A000013300200210000004E000011000316FE010A062C0B021F11734000000A0B2B0B021F10734000000A0B2B00072A000000133002002D0000004F00001100030B072C0E721779007002282200000A0A2B0C729379007002282200000A0A061F11734000000A0C2B00082A000000133003001A0000000B00001100020A0672F30C007072F70C00706F3200000A0A060B2B00072A0000133003001A0000000B00001100020A0672137A007072177A00706F3200000A0A060B2B00072A000013300300B40000005000001100020A06282000000A16FE010B07399B000000000672177A007072137A00706F3200000A0A0672237A007072177A00706F3200000A0A0672337A007072417A00706F3200000A0A0672417A0070724B7A00706F3200000A0A06724F7A007072597A00706F3200000A0A06725D7A007072777A00706F3200000A0A0672777A007072857A00706F3200000A0A0672897A007072F30C00706F3200000A0A0672977A007072A77A00706F3200000A0A00060C2B00082A133003008F0000005000001100020A06282000000A16FE010B072C79000672A77A007072977A00706F3200000A0A0672F30C007072897A00706F3200000A0A0672857A0070725D7A00706F3200000A0A06724B7A007072337A00706F3200000A0A0672177A007072237A00706F3200000A0A0672137A007072177A00706F3200000A0A0672597A0070724F7A00706F3200000A0A00060C2B00082A00133002002E00000051000011022C2920C59D1C810A160B2B1402076FF900000A066120930100015A0A0717580B07026F2F00000A2F022BE1062A00001330030022000000110000110003027B04000004027B050000047C03000004281500000A28100000060A2B00062A00001330050038000000110000110003027B080000047B04000004027B080000047B050000047C03000004281500000A027B06000004027B0700000428110000060A2B00062A1330030018000000110000110003027B09000004027B0A000004281C0000060A2B00062A1330030022000000110000110003027B0C000004027B0D0000047C0B000004281500000A281C0000060A2B00062A000042534A4201000100000000000C00000076322E302E35303732370000000005006C000000D01F0000237E00003C200000301D000023537472696E6773000000006C3D0000AC7A00002355530018B8000010000000234755494400000028B80000BC15000023426C6F620000000000000002000001571F02080902000000FA0133001600000100000065000000140000002700000092000000B701000001000000F9000000090000003E000000510000000700000001000000040000000C00000000003B0D01000000000006005D0973140600DC09731406000E088F130F00A51400000600E608BF0F0600C908BF0F0600C309BF0F0600A809BF0F06007D09BF0F0600A108BF0F06003608BF0F06004009BF0F06000C09BF0F0600220854140A00A918C2120A007308C2120600C206940E0A008F0AC2120600E011DD1A0A00080BFB150600D31113020600981213020600B518940E0A002B09C2120A005D04C2120A005704C2120A00A603FB150A005E10E7180A007B056B020A002204E7180A00B312E7180600E5120A170A00811B6B020600D605940E0A009609C2120A005F01FB150E00311CEB160600CA050A170A00A611E7180A009E0DFB150A00030FFB150E00BA0CEB160E003E13EB160A00620F6B020600000D0A170600F30773140A004D08C2120600B903940E0A007012E718120036196E0D06003C018C02CF00F41200001200731A6E0D06001A0D940E120017056E0D1200C9116E0D0A007406FB150A006F04C21206007706940E0600FF10940E1200BC086E0D0600A01A0A17120017196E0D0600F910940E0600E2050A170A00751CFB150E008517EB160600F50E940E06001E0B940E0A00511015110A000A0415110A00A51215110A0030106B020A00F9066B0206007301940E0E00F106EB160E00F40FEB160A001910E7180A009C066B020A00641215110A00991115110600E706940E0600EC05940E0A0004106B0206008611940E0600651A940E0600B101940E06005C0D940E1200DD0F6E0D0600060F940E0A00AA066B020A00631BC2120A009006C2120A00C9076B021200B6066E0D0600631C940E06006D1A0A1712004F116E0D0600D10F0A170E008011EB160A007702C21200000000D501000000000100010009211000810D000045000100010001001000351700005D000200070001001000101600005D000200220001001000AD0B00005D000200790081011000E7171D025D0003007C0081011000BB1C1D025D0003007C0000010000DE0100005D000300880003011000330000005D000300890003011000ED0000005D0004008A0003011000790100005D0006008C0003011000070000005D0009008E00030110001D0000005D000B00900003011000D70000005D000C0091000A011000D413000045000E0093000A011000680E00004500100093000A011000490B00004500150093000A0110002F0B000045001900930002010000CC06000011011D00930002010000E20600001101240093000100EF0D120C36009717160C0600B70E1B0C0600241C1F0C06004F01240C0600F41B1F0C0600031C1F0C06009A01280C0600031C1F0C0600B70E8E000600B70E1B0C0600031C1F0C06004F012C0C0600AF0F8E000600EF138E000600AF0F8E00060068068E000600FF128E00060043018E0006008E018E000600200AAD0306002D0AAD0306006607A70506005E07A70506009F19D1030600AC19D10306006607A70506005E07A70506066302300C56806C19330C5680E710330C56804D06330C56807F15330C5680A715330C5680CF15330C06066302300C56809408370C56800E0A370C5680BE03370C5020000000008600DA18060001006020000000008600AA073B0C010094200000000086006E05410C0200AC20000000008600B507470C0300F12000000000E60141034C0C0300052100000000E601ED07520C04001C210000000096008E1C580C0500282B000000009600070C670C0A00702B000000009600850C720C0D00D02B00000000960014157D0C1000282C000000009600891B8A0C1300442C000000009600FF14920C1500942C000000009600720F9F0C1A00982D000000009600DF0BA70C1C00042F000000009600D40AB90C210004300000000091000813CD0C270034300000000091000813D80C2A009430000000009600600CE90C2F002C31000000009600F81CFB0C350084310000000096003A0CE90C3A001C32000000009600D11CFB0C40007432000000009600C00B0B0D4500B0320000000096003406FB0C49000833000000009600150C190D4E0044330000000096003F15280D52009833000000009600940C390D5700A0340000000096001E1B4A0D5C0078350000000091002813CD0C62009835000000009600D0105D0D650040360000000096009F10690D68006C370000000096008505770D6C00E03B0000000091009A1B800D6E009E3F0000000086184D1306007900A83F0000000096004102A30D790050400000000096002F02AE0D7C005841000000009600ED19C70D8300FC410000000091004C18D70D87004C420000000091007511DE0D8900C442000000009100AE13DE0D8C003C43000000009100471BE80D8F009043000000009100D919E80D9100E4430000000091003016DE0D93005C44000000009100FD01F10D9600004E0000000091000E18E80D9E00544E0000000091008F0B060EA000284F000000009100610BDE0DA400A04F0000000091006311160EA70020500000000091009F17210EAA00B451000000009100C81A290EAB001C52000000009100A304DE0DAE0094520000000091009605310EB100D052000000009100B105310EB2000C530000000091008219370EB300505300000000910020183F0EB400E4540000000091001E074D0EBB00C855000000009100710A580EBE00E856000000009100B919630EC1000858000000009100271A4D0EC400B4580000000091003F0F4D0EC70060590000000091002515160ECA00145A0000000091008003160ECD00C85A0000000091006703DE0DD000405B0000000091004A03DE0DD300B85B000000009100760BDE0DD600305C0000000091002515DE0DD900A85C000000009100AD17DE0DDC00205D0000000091009A16DE0DDF00985D000000009100C416DE0DE200105E000000009100881ADE0DE500885E0000000091008B116E0EE800045F0000000091007405790EEB009C5F0000000091009E0F830EED006060000000009100F0148E0EEE0094650000000091001B14980EF00008660000000091004418A60EF1007C660000000091003415210EF20010680000000091009503210EF300A469000000009100500FB20EF4001C6B0000000091002E07B20EF500E86C000000009100820ABA0EF600006F000000009100CB19C20EF70014710000000091003C1AB20EF8008C72000000009100650ECA0EF90064730000000091000414D70EFA0000740000000091000414DD0EFB009874000000009100D514E90EFC00B874000000009100D514F00EFD0044750000000091009314F80EFF00C0750000000091001B14040F00013C76000000009100490EE90E0101B4760000000096006317110F0201F8770000000096004A17220F0701A478000000009600CC15380F0E017C79000000009600B715460F1201A07A000000009600A415380F1801787B0000000096008E15460F1C019C7C000000009600EE15380F2201687D000000009600DE15460F2601007E0000000096007C15380F2C01DC7E0000000096006715460F3001747F0000000096006919110F3601B8800000000096005719220F3B0164810000000096004304590F42014C820000000096007D046F0F490174830000000096007816890F5201B8840000000096007007960F5601E284000000009600540AA60F5A01F8840000000096000212C00F6101348A0000000096003D12C00F640164900000000091008607A60F67016C940000000091001E12C90F6E01389B0000000091005A13D80F7301C8A1000000009100130FE20F7501D0A30000000091004A05F80F7B01C0A500000000910046160C1081016CA8000000009100230F2310880100AD000000009100B01630108B01C8AE00000000960081103A108D01D8B2000000009600CC1B541094019E3F0000000086184D1306009C0128B90000000096002C0D70109C019E3F0000000086184D1306009F0199B9000000009118531378109F01A4B90000000096006C047C109F0114BA000000009600331882109F010CBB00000000960094048710A00164BB0000000096007E1C8210A201A4BB0000000096002E1C8D10A30154BC0000000096002E1C9810A701E0BC0000000091002E1CA310AB0110BD000000009600171CA310AD014CBD000000009600D90EBA0BAF0174BD0000000096005705BA0BB0019CBD000000009600040EBA0BB1015CBE000000009600C40DBA0BB201F8BE000000009300C60CAB10B3019E3F0000000086184D130600B4019E3F0000000086184D130600B40134BF0000000083004800B010B4019E3F0000000086184D130600B50164BF0000000083000201B010B5019E3F0000000086184D130600B601A8BF0000000083008200B010B6019E3F0000000086184D130600B7019E3F0000000086184D130600B701CCBF000000008300A800B010B70100000100460A00000100801100000100581300000100E81B000001003903000002007710000003001E1D00000400EE1100000500D30200000100C60400000200281100000300921700000100C60400000200281100000300921700000100C60400000200281100000300921700000100B01802000200C00C00000000000000000100C60400000200281100000300921700000400FD1800000100060300000200DA0200000100C60400000200EA0E00000300140700000400241400000500541600000100C60400000200EA0E00000300B70E00000400140700000500241400000600541600000100D50300000200241C00000300B70E00000100D50300000200241C00000300B70E00000400F41B00000500031C00000100100600000200091100000300161B00000400EA0E00000500140700000600241400000100C60400000200EA0E00000300B70E00000400140700000500241400000100100600000200091100000300161B00000400EA0E00000500140700000600241400000100C60400000200EA0E00000300B70E00000400140700000500241400000100C60400000200EA0E00000300140700000400241400000100C60400000200EA0E00000300B70E00000400140700000500241400000100C60400000200EA0E00000300140700000400241400000100C60400000200EA0E00000300B70E00000400140700000500241400000100C60400000200EA0E00000300140700000400241400000500541600000100C60400000200EA0E00000300B70E00000400140700000500241400000600541600000100C00C00000200031C00000300B70E00000100C60400000200190E000003000B0D00000100C60400000200190E00000300200E000004000B0D00000100D50600000200FD0200000100FC0C020002004A0102000300950102000400AA0102000500B70102000600BC0102000700C10102000800C60102000900CB0102000A00D00102000B00010000000100150E00000200E10200000300100300000100150E00000200E102000003004914020004002F1400000500FF17000006001003020007008C1900000100150E00000200E102000003004914020004002F14000001005D18000002006C1000000100A50D00000200EF02000003006C1000000100A50D00000200EF02000003006C1000000100EF02000002006C10000001001E03000002006C1000000100A50D00000200EF02000003006C1000000100150E00000200E102020003002F14020004005D1802000500F811000006006C10000007001E0300000800D907000001001E03000002006C1000000100A50D00000200EF02000003006C1000000400B80400000100A50D00000200EF02000003006C1000000100860E00000200EF02000003006C10000001007F1A00000100940F000002003C14000003006C1000000100A50D00000200EF02000003006C10000001006C10000001006C10000001006C1000000100EF02000002005D1800000300F811000004004914000005006C1000000600FF17000007003C14000001003D0E00000200EF02000003006C10000001006B1600000200EF02000003006C10000001006B1600000200EF02000003006C10000001003D0E00000200EF02000003006C10000001003D0E00000200EF02000003006C10000001007B0E00000200EF02000003006C10000001007B0E00000200EF02000003006C1000000100A50D00000200EF02000003006C1000000100A50D00000200EF02000003006C1000000100A50D00000200EF02000003006C1000000100A50D00000200EF02000003006C1000000100A50D00000200EF02000003006C10000001008D0D00000200EF02000003006C1000000100F60D00000200EF02000003006C1000000100A50D00000200EF02000003006C10000001001F0600000200DC17000003006D0F000001001F0600000200DC17000001006D0F000001004219000002002B06000001007F1A000001007F1A000001007F1A000001007F1A000001007F1A000001007F1A00000100F91400000100F914000001007F1A000001007F1A00000100131400000100FB1300000100F91400000100F91400000200410500000100421900000100131400000100AA1A00000100DA0200000200EA0E00000300140700000400241400000500541600000100DA0200000200CA1300000300EA0E00000400B70E00000500140700000600241400000700541600000100DA0200000200EA0E00000300140700000400241400000100DA0200000200CA1300000300EA0E00000400B70E00000500140700000600241400000100DA0200000200EA0E00000300140700000400241400000100DA0200000200CA1300000300EA0E00000400B70E00000500140700000600241400000100DA0200000200EA0E00000300140700000400241400000100DA0200000200CA1300000300EA0E00000400B70E00000500140700000600241400000100DA0200000200EA0E00000300140700000400241400000100DA0200000200CA1300000300EA0E00000400B70E00000500140700000600241400000100DA0200000200EA0E00000300140700000400241400000500541600000100DA0200000200CA1300000300EA0E00000400B70E00000500140700000600241400000700541600000100DA0200000200FA0E00000300EA0E00000400C30E00000500140700000600241400000700541600000100DA0200000200CA1300000300EA0E00000400C30E00000500B70E00000600A00E00000700140700000800241400000900541600000100DA0200000200190E00000300200E000004000B0D00000100DA0200000200260500000300421C00000400510700000100DA0200000200BB0200000300260500000400571C00000500421C00000600510702000700BB0300000100C80200000200BD1300000300421C00000100C80200000200BD1300000300421C00000100DA0200000200BB0200000300260500000400571C00000500421C00000600510702000700BB0300000100DA0200000200A21300000300BD1300000400421C00000500B80A00000100A70200000200DB04000001001F0500000200DB04000003002C0300000400510700000500A70200000600E204000001002B06000002002C03000003005107000004001F0500000500A702000006003D11000001002C03000002005107000003001F0500000400DB0400000500A702000006008013000007003D1100000100AB0A00000200CD0400000300A70200000100F00400000200A70200000100DA0200000200BB02000003004D1C00000400371C00000500B20D020006003E0300000700620500000100DA0200000200AD0200000300B81B00000400A00700000500DF1800000600791800000700421C00000800510700000100C21A00000200351100000300891200000100F91A00000100F91A00000200CA1300000100F91A00000100EC0600000200B90B00000300140700000400241400000100EC0600000200B90B00000300140700000400241400000100281100000200140700000100270E000002000B0D00000100C21A00000100C21A00000100C21A00000100C21A000001009A1800000100C00C00000100C00C00000100C00C00000100C00C0200490009004D13010011004D13060019004D130A0029004D13100031004D13100039004D13100041004D13100049004D13100051004D13100059004D13100061004D13100069004D13100071004D13150081004D131A00C1004D13060019014D13060071014D13060079014D13060099004D130600A100630D2400A100FA0928009900A3182C0099003C04330099005E1A3E0099003C044500B900120B2800A1004D131000A900C90A280099004D131000B100ED0710002902CB1C8E002902C31C91002902A11C96002902A9189C002902A918A200E1004D131000E9004D130600D900FA09AA00F1004D13B00031020E0F06003902E91A1000F9004D13B7004102570DBD00E9006418C30051027819C90029029C18CD002902DD0CC9005902120B280029029C18D40029029B04DA005102F112E00001014B19E60009012B0EEA0029029B0E28002902DD12280029029C18EF002902D616F5000101D41A240011014007060029029C18FA002902A9180201A100BC1809012101FA09C90029014D13170129010D0CF5002901BA0C28016102FA09280029011D153A0129019B04DA00E10014045901F100CD175E017102460364018102040A6E01F100B411730189026D18240089022B0EEA0089024103240089023A07060031023A0706009900DD0CC9002902250B79014901FA0924004101FA0928006902F112E0004101630D240059014D13B40129019B04BA016102EA1BC9006102DD0CC9009102FE05740261014D137D02E900DF168502A10246038B0239028B11E6002902D4189202D900630D2400B102AF019902B102120B9E022902A918A30269014D1301006901D80C6E016901611CAA02E900B11BAF020901340EB50249024603BB0269015F11E6006901060DE60069017819C90009012B0ED302C102460AD802D900BC18DD02D900690DE502B9004D1306008101CB1CF6024901630D240021014D13010041014D13060089014D130600810256061000890198061E0391014D1306009101790D10000C00F1127A0314004B198B032902AD1C9600A9017819C900A9018F0E9003B901E20D2800A901A00A9003B90121169703C902A00A9D03B901FA092800B1014807A4031102430AAD031102C818B203B1014A11BB03B101AD1CC00359024807CA032101430AD1032101C818D603B901081B28001400D41A24000C007819C9000C00611CF00329025D0FF603A901F112E000D102120B28001C004D13060081014D1310001C0046032804F1004D130600F100421034043902A6063A0439027E1CC900F100C2114F0441014D1355048102680A0100710246037304E10287067C04E9022D04B700D1024B0791003902AF1A01000C004D1306000C0046032804B901E4144D0524004D13060059024B078F05240046032804C9013A0AA705C901430AA70511024B07D405C9014B07DB0521014B07F6052101BC18FD052C004D1306002C00460328041C00F1127A0334004B198B033400D41A2400B901DB03A8069900A318C0069900A318C906B1026801D1069900A318E406E9024D1AFA06E902FB030600B901FB0410072901791722072902F10CF5002902E80CF50091012219800799004D13010029025C122800B901040A1000D9016E1B8607D901120B8C072902F30391070901340E9707E1014D139D073102BF07A507B90132050D08B901E703A806B901D50D2800810193164108E1014D131000F1014D130600B901C3142400B901220D06004901BC18AF084901A11CB60849014C0AC20891010C05C908910163089D03C9023C04D408F10146036009F101D6166509F101611C6A09F101BA127609F101611CAA02B901B4149D09B90116062800B9017D12A30991010919D3091103F112E0008101B603DA09C901FA09E0098101AD1C0C0AB10268018F05B102120B870AD901761B8607C901BC18C20A4901191A320B09024D130600090246033B0B0902691C650909029318410B3101F112E00009022B0E470B5101B60C5B0B2103F317240029034D136A0B29034D13720BD1014D137B0BD101AE03920BD1011B0B9A0BD1017001A00BE902A21BFA0629018006BA0B2902C317E60B08007800F40B08007C00F90B08008000FE0B08008400030C08008800080C08008C000D0C08009400F40B08009800F90B08009C00FE0B2E000B00B7102E001300C0102E001B00DF102E002300E8102E002B00E8102E003300F5102E003B0011112E00430033112E004B003E112E00530044112E005B0044112E0063003E112E006B003E11430073002015E0007B005A1100017B00F40B03018B00F40B20017B00F40B23018B00F40B40017B00821243018B00F40B44018300A71563018B00F40B80017B00DA1283018B00F40BA0017B00F212A3018B00F40BA4018300A715C0017B00F40BC3018B00F40BE0017B00F40B04028300A71540027B00F40B60027B00F40B80027B00F40BA0027B00F40BA4028300A715C0027B00F40BC4028300A715E0027B00F40B00037B00F40B20037B00F40B40037B00F40B60037B00F40BA0037B00F40BC0037B00F40BE0037B00831340049300F40B60049300F40B80049300F40BE40A8300A715840B8300A715E00C9300F40B000D9300F40B200D9300F40B400D9300F40B600D9300F40B800D9300F40BA00D9300F40BC00E9300F40BE00E9300F40B200F7B00F40B200039004B000F011F012F01410149017F01A201C201C701D101DB01E201FC011002140229024202C202E902FB020A03130325033303DD03FD032E0441045C0482049604AD04BE04CF04E6040505540570059405AC05C005E205040628063706520661067C069806B106D606DB06EC060107170728072E07AB0713084808DD087F09AF09E609160A730A8C0ACB0A4C0B610B830BA60BAD0BBF0BCC0BD30BDB0BE10B730384031F0488052006490673090480000007000000000000000000000000001D170000020000000000000000000000EB0B830200000000020000000000000000000000EB0B6B0200000000020000000000000000000000EB0B940E00000000020000000000000000000000EB0B6E0D00000000090003000A0003000B0003000C0003000D0003000E0003000F0004001000040011000400120004001300060014000600000000636F6C3130003C3E635F5F446973706C6179436C61737331305F30003C3E635F5F446973706C6179436C61737332305F30003C3E635F5F446973706C6179436C617373385F30003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F30003C436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F723E625F5F30003C46696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E74546578743E625F5F30003C3E635F5F446973706C6179436C61737332305F31003C3E635F5F446973706C6179436C617373385F31003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F31004C69737460310056616C75653100636F6C31004353243C3E385F5F6C6F63616C73310053716C496E74333200546F496E74333200536574496E743332003C3E635F5F446973706C6179436C617373385F320056616C75653200636F6C32004353243C3E385F5F6C6F63616C733200636F6C3300546F496E74363400636F6C3400636F6C3500636F6C3600636F6C3700636F6C3800636F6C39003C4D6F64756C653E003C50726976617465496D706C656D656E746174696F6E44657461696C733E00526574726965766556616C75657346726F6D584D4C0053797374656D2E494F0046696E64416E645265706C616365434C5200436F6E746163745F536561726368434C5200436F6E746163744C6973745F557064617465436F6E74616374436F756E74434C520076616C75655F5F0053797374656D2E446174610053716C4D65746144617461006D73636F726C69620053797374656D2E436F6C6C656374696F6E732E47656E65726963006462446F6300706167654D61704E6F6465496400706172656E744E6F646549640074656D706C61746549640053697465496400736974654964004170706C69636174696F6E4964006170706C69636174696F6E4964006F626A656374496400636F6E74656E74496400436F6E746163744C697374496400636F6E746163744C6973744964006D6F646966696564427949640071756572794964005265616400416464004578656375746550726F647563745479706573507572636861736564004578656375746550726F64756374735075726368617365640045786563757465466F726D5375626D697474656400476574466F726D5375626D69747465640053716C477569640053657447756964004E65774775696400436F6E74656E74446566696E6974696F6E4669656C64006669656C6400417070656E644368696C640052656D6F76654368696C64005472696D456E640053656E64526573756C7473456E64004462436F6D6D616E6400437265617465436F6D6D616E640053716C436F6D6D616E640045786563757465416E6453656E6400417070656E640046696E64416E645265706C6163655F46696E640053797374656D446174614163636573734B696E640047657453716C446174615265636F72640046696E64416E645265706C6163655F5265706C61636500506572666F726D5265706C6163650045786563757465436F6E74616374536F7572636500636F6E74616374536F7572636500736F75726365006F6E655061676544624E6F64650064624E6F6465006C617374466F756E644E6F6465006462506167654E6F64650053656C65637453696E676C654E6F6465004372656174654E6F646500586D6C4E6F646500696E4E6F646500706167654D61704E6F6465006765745F506172656E744E6F646500726F6F744E6F64650053657475704E65774E6F64650048746D6C456E636F6465007075626C69736850616765004D657267650045786563757465446174615461626C650047657452656C6174696F6E5461626C650043726561746554656D706F726172794F75747075745461626C650044726F7054656D706F726172794F75747075745461626C650049456E756D657261626C650049446973706F7361626C6500486173687461626C650052756E74696D655479706548616E646C65004765745479706546726F6D48616E646C65007469746C65006765745F4E616D6500636F6D6D616E644E616D65006E6F64654E616D650046696E64416E645265706C6163655F47657455706461746564506167654E616D65007365745F506172616D657465724E616D650050726F706572794E616D650053716C4461746554696D6500457363617065006765745F506970650053716C50697065007365745F53716C446254797065007365745F436F6D6D616E645479706500586D6C4E6F6465547970650056616C756554797065004974656D547970650072656C6174696F6E54797065005265676578547970650074797065004361707475726500496E7465726E616C44617461436F6C6C656374696F6E42617365006D61746368436173650045786563757465507572636861736500476574507572636861736500436C6F736500446973706F7365005472795061727365006D6F6469666965644461746500456E64446174650053746172744461746500434C52506167654D61704E6F64655F55706461746500434C52506167654D61704E6F64655F536176655570646174650070726F706F6761746500416363756D756C617465005465726D696E617465006765745F537461746500436F6E6E656374696F6E53746174650066696C746572436F6E7461637442795369746500577269746500436F6D70696C657247656E6572617465644174747269627574650044656275676761626C6541747472696275746500436F6D56697369626C6541747472696275746500417373656D626C795469746C654174747269627574650053716C50726F636564757265417474726962757465004372656174654174747269627574650053716C55736572446566696E65644167677265676174654174747269627574650054616741747472696275746500417373656D626C7954726164656D61726B41747472696275746500586D6C41747472696275746500417373656D626C7946696C6556657273696F6E41747472696275746500417373656D626C79496E666F726D6174696F6E616C56657273696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E4174747269627574650053716C46756E6374696F6E41747472696275746500417373656D626C794465736372697074696F6E41747472696275746500436F6D70696C6174696F6E52656C61786174696F6E7341747472696275746500417373656D626C7950726F647563744174747269627574650053716C466163657441747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C79436F6D70616E794174747269627574650052756E74696D65436F6D7061746962696C697479417474726962757465006765745F56616C7565007365745F56616C75650054616741747472696275746556616C7565004D696E696D756D56616C7565004D6178696D756D56616C7565004D696E56616C7565004D617856616C7565006F705F5472756500434C52506167654D61704E6F64655F53617665007365745F53697A6500457865637574654F7264657253697A65004765744F7264657253697A65004942696E61727953657269616C697A65006765745F4974656D4F6600696E7075745061676544656600696E7365727444656C657465466C61670052656164537472696E670046696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E670053716C537472696E6700546F537472696E6700536574537472696E6700537562737472696E670044617465416E64496E746567657252616E67655365617263680044617465416E644D6F6E657952616E6765536561726368004578656375746550726F66696C65536561726368004578656375746557656273697465557365725365617263680045786563757465417474726962757465436F6E74616374536561726368005265676578536561726368007365617263680046696E64416E645265706C6163655F4973506167654E616D654D617463680046696E64416E645265706C6163655F4973436F6E74656E74446566696E6974696F6E4D617463680052656765785F49734D617463680046696E64416E645265706C6163655F49735061676550726F706572746965734D617463680046696E64416E645265706C6163655F4973496D61676550726F706572746965734D617463680046696E64416E645265706C6163655F497346696C6550726F706572746965734D617463680052656765785F4765744D617463680046696E64416E645265706C6163655F497354657874436F6E74656E744D61746368004E6578744D61746368006D6174636800436F6D70757465537472696E67486173680050757368006765745F4C656E67746800456E6473576974680053746172747357697468006F626A00537461636B005065656B006D6174636857686F6C654C696E6B00446563696D616C0052656D6F7665416C6C00526567657853656C656374416C6C004272696467656C696E652E434C5246756E6374696F6E732E646C6C0046696C6C0044424E756C6C006765745F49734E756C6C0053797374656D2E586D6C004C6F6164586D6C0047656E6572617465586D6C0073656375726974794C6576656C586D6C0053716C586D6C006F7065726174696F6E586D6C0070616765446566696E6974696F6E586D6C00436F6E7665727448746D6C546F586D6C006765745F496E6E6572586D6C006765745F4F75746572586D6C00737472586D6C00696E6465785465726D73586D6C00436F6E76657274586D6C546F48746D6C00786D6C006F6C6455726C006E657755726C0075726C006765745F4974656D007365745F4974656D00705365617263684974656D00476574586D6C466F7250726F70657274795365617263684974656D0047657450726F70657274795365617263684974656D007365617263684974656D007369746547726F75704974656D0053797374656D005472696D0068746D6C456E636F6465645265706C6163655465726D007265706C6163655465726D0068746D6C456E636F6465645365617263685465726D00466F726D61745365617263685465726D007365617263685465726D00456E756D00736561726368496E0053716C426F6F6C65616E004F70656E00497465726174654368696C6472656E00536574757050616765446566696E6974696F6E4368696C6472656E00457865637574654C6173744C6F67696E004765744C6173744C6F67696E004A6F696E0044617461436F6C756D6E00636F6E6E0046696E64416E645265706C6163655F476574436F6E74656E744C6F636174696F6E004F7065726174696F6E00476574436F6E66696775726174696F6E004E656761746553656C656374696F6E0053797374656D2E5265666C656374696F6E0049436F6C6C656374696F6E00586D6C417474726962757465436F6C6C656374696F6E004D61746368436F6C6C656374696F6E0044617461436F6C756D6E436F6C6C656374696F6E0053716C506172616D65746572436F6C6C656374696F6E0044617461526F77436F6C6C656374696F6E007365745F436F6E6E656374696F6E004462436F6E6E656374696F6E0053716C436F6E6E656374696F6E00636F6E6E656374696F6E00636F6E646974696F6E00434C52506167654D61705F5361766550616765446566696E6974696F6E0046696E64416E645265706C6163655F476574557064617465644C696E6B496E436F6E74656E74446566696E6974696F6E0046696E64416E645265706C6163655F49734C696E6B496E436F6E74656E74446566696E6974696F6E00466F726D6174457863657074696F6E006465736372697074696F6E0053797374656D2E446174612E436F6D6D6F6E0072656765785061747465726E007061747465726E007365744578747261496E666F005A65726F00586D6C4E616D65644E6F64654D617000506F700045786563756974655369746547726F757000457865637574655369746547726F7570004368617200457865637574655363616C6172004462446174615265616465720053716C4461746152656164657200457865637574655265616465720045786563757465586D6C5265616465720042696E61727952656164657200537472696E674275696C64657200536F72744F7264657200736F72744F7264657200434C52506167654D61704E6F64655F416464436F6E7461696E657200434C52506167654D61704E6F64655F557064617465436F6E7461696E657200434C52506167654D61704E6F64655F52656D6F7665436F6E7461696E657200546F5570706572004462506172616D657465720053716C506172616D6574657200496E736572744166746572006D6174636844656C696D697465720042696E61727957726974657200446244617461416461707465720053716C4461746141646170746572004D6963726F736F66742E53716C5365727665722E53657276657200546F4C6F7765720049456E756D657261746F7200476574456E756D657261746F72004F70657261746F7200436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F72005461674174747269627574654576616C7561746F72004D617463684576616C7561746F72002E63746F72002E6363746F7200536574757050726F7061676174696F6E416E64496E6865726974616E636541747472696273006578636C756465417474726962730053797374656D2E446961676E6F73746963730074656D706C61746549647300457865637574655369746549647300636F6E7461696E6572496473006F626A656374496473004E6567617461626C65436F6C6C656374696F6E4F664775696473004C6973744F66477569647300636F6C477569647300476574586D6C466F72477569647300647447756964730047657447756964730077686F6C65576F72647300546F74616C5265636F72647300746F74616C5265636F726473004D61785265636F7264730053797374656D2E52756E74696D652E496E7465726F7053657276696365730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300476574436F6E74616374536F757263657300446562756767696E674D6F646573006765745F4368696C644E6F646573006765745F4861734368696C644E6F64657300476574586D6C466F724E6F6465730053656C6563744E6F646573006765744E6F646573006E6F6465730052656765785F5265706C6163654D6174636865730052656765785F4765744D61746368657300457865637574655761746368657300476574576174636865730046696E64416E645265706C6163655F476574557064617465645061676550726F70657274696573005570646174655061676550726F70657274696573004765745061676550726F7065727469657300557064617465496D61676550726F7065727469657300476574496D61676550726F706572746965730055706461746546696C6550726F706572746965730047657446696C6550726F7065727469657300557064617465506167654E616D657300476574506167654E616D65730053797374656D2E446174612E53716C54797065730053746F72656450726F63656475726573006765745F4174747269627574657300457865637574655369746541747472696275746573005365744174747269627574657300696E636C75646541747472696275746556616C7565730073656172636856616C7565730046696E64416E645265706C6163655F5570646174654C696E6B7300457175616C73004578656375746553656375697274794C6576656C730053657475705075626C69736844657461696C730045786563757465496E6465785465726D7300436F6E7461696E73006765745F436F6C756D6E730053797374656D2E546578742E526567756C617245787072657373696F6E730053797374656D2E436F6C6C656374696F6E73004272696467656C696E652E434C5246756E6374696F6E730055736572446566696E656446756E6374696F6E7300557064617465436F6E74656E74446566696E6974696F6E7300476574436F6E74656E74446566696E6974696F6E73006765745F4F7074696F6E730052656765784F7074696F6E730072656765784F7074696F6E73004765745369746547726F7570730045786563757465437573746F6D657247726F757073006765745F4368617273006765745F506172616D657465727300706172616D657465727300456E756D657261746F7273006765745F5375636365737300696E636C75646541646472657373004765744D616E75616C436F6E74616374730045786563757465476574436F6E746163747300476574536561726368526573756C747300476574496E747300476574436F756E74427953746174757300737461747573006765745F526F7773006765745F486173526F777300617070656E64546F4578697374696E67576F726B666C6F7773006765745F4B65797300436F6E63617400417070656E64466F726D6174006D617463684F626A656374006F705F496D706C69636974006F705F4578706C696369740053706C697400496E697400696E68657269740053797374656D2E446174612E53716C436C69656E74007265706C6163656D656E7400437265617465456C656D656E7400586D6C456C656D656E74006765745F446F63756D656E74456C656D656E7400586D6C446F63756D656E7400646F63756D656E74006765745F43757272656E740055706461746554657874436F6E74656E740047657454657874436F6E74656E74006765745F436F756E740045786563757465476574436F6E74616374536F75726365436F756E74004D696E696D756D436F756E74004D6178696D756D436F756E7400457865637574654F72646572436F756E74004765744F72646572436F756E740045786563757465436F6E74616374436F756E7400436F6E746163745F4765744175746F446973747269627574696F6E4C697374436F6E74616374436F756E74006F705F4C6F676963616C4E6F7400457865637574654162616E646F6E656443617274004765744162616E646F6E6564436172740053656E64526573756C7473537461727400496E7365727400436F6E7665727400494C69737400586D6C4E6F64654C697374006E6F64654C6973740045786563757465446973747269627574696F6E4C6973740041727261794C697374006C697374007365745F436F6D6D616E6454696D656F757400696E7075740057726974654F7574707574004D6F76654E6578740053797374656D2E54657874007365745F436F6D6D616E64546578740073716C436F6D6D616E6454657874006765745F496E6E65725465787400616C74546578740046696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E74546578740045786563757465427943757272656E7453697465436F6E746578740053716C436F6E74657874006765745F4E6F77006765745F5574634E6F770044617461526F770052656765785F4765744D6174636865735F46696C6C526F770053656E64526573756C7473526F77004E6577526F7700706167654D61704E6F6465576F726B466C6F7700434C53506167654D61704E6F64655F417474616368576F726B666C6F77006765745F496E6465780061747472696275746552656765780061747472696275746556616C75655265676578004765744C696E6B52656765780068746D6C5265676578004765745265676578004D6F6469666965644279006D6F6469666965644279004372656174656442790063726561746564427900546F417272617900436F6E7461696E734B65790053716C4D6F6E657900457865637574654E6F6E5175657279004E617646696C7465725F4765745175657279006F705F457175616C697479006F705F496E657175616C697479005574696C6974790049734E756C6C4F72456D7074790046696E64416E645265706C6163655F47657455706461746564496D61676550726F70657274790046696E64416E645265706C6163655F4765745570646174656446696C6550726F7065727479004F72646572427950726F706572747900000000213C0067007500690064003E007B0030007D003C002F0067007500690064003E0000333C00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064003E0000353C002F00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064003E00000B500072006900630065000080AB2800530045004C0045004300540020004D0049004E0028004C006900730074005000720069006300650029002000460052004F004D0020007600770053004B0055002000570048004500520045002000500072006F0064007500630074004900640020003D00200050002E0049006400200041004E004400200053006900740065004900640020003D00200027007B0030007D0027002900200041005300200050007200690063006500011354006F007000530065006C006C00650072000080C32800530045004C004500430054002000530055004D002800500072006500760069006F007500730053006F006C00640043006F0075006E00740029002000460052004F004D0020007600770053004B0055002000570048004500520045002000500072006F0064007500630074004900640020003D00200050002E0049006400200041004E004400200053006900740065004900640020003D00200027007B0030007D0027002900200041005300200054006F007000530065006C006C00650072000180AB530045004C004500430054002000440049005300540049004E0043005400200027007B0030007D0027002000410053002000460069006C00740065007200490064002C0020007B0031007D002C00200050002E00490064002000460052004F004D00200076007700500072006F00640075006300740020005000200057004800450052004500200050002E0053006900740065004900640020003D00200027007B0032007D002700200001811F530045004C004500430054002000440049005300540049004E0043005400200027007B0030007D0027002000410053002000460069006C00740065007200490064002C0020007B0031007D002C00200050002E00490064002000460052004F004D00200076007700500072006F00640075006300740020005000200049004E004E004500520020004A004F0049004E00200076007700500072006F00640075006300740041006E00640053004B005500410074007400720069006200750074006500560061006C00750065002000560020004F004E00200050002E004900640020003D00200056002E0049006400200057004800450052004500200050002E0053006900740065004900640020003D00200027007B0032007D0027002000012F63006F006E007400650078007400200063006F006E006E0065006300740069006F006E003D0074007200750065000083790D000A002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400200053004E006F002C0020004C00660074004F0070006E0064002C0020005200670074004F0070006E0064002C0020004C00660074004F0070006E0064004E0061006D0065002C0020005200670074004F0070006E0064004E0061006D0065002C0020004F00700072002C0020004C00660074004F0062006A0065006300740054007900700065002C0020005200670074004F0062006A0065006300740054007900700065002C00200056002E00560061006C00750065002000520067007400560061006C00750065000D000A002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020004E005600460069006C007400650072004900740065006D002000460049000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E0020004E005600460069006C007400650072004900740065006D005100750065007200790020004900510020004F004E002000460049002E004900640020003D002000490051002E004900740065006D00490064000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E0020004E005600460069006C00740065007200510075006500720079002000510020004F004E002000490051002E00510075006500720079004900640020003D00200051002E00490064000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E0020004E005600460069006C00740065007200560061006C00750065002000560020004F004E002000460049002E005200670074004F0070006E00640020003D00200056002E00490064000D000A00200020002000200020002000200020002000200020002000200020002000200057004800450052004500200051002E004900640020003D00200027007B0030007D0027000D000A0020002000200020002000200020002000200020002000200020002000200020004F0052004400450052002000420059002000460049002E0053004E006F000184910D000A002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054002000440049005300540049004E004300540020002700300030003000300030003000300030002D0030003000300030002D0030003000300030002D0030003000300030002D0030003000300030003000300030003000300030003000300027002000410053002000490064002C00200043002E004E0061006D0065002C00200054002E004E0061006D0065002000410074007400720069006200750074006500440061007400610054007900700065002C002000310020004100530020004900730050006800790073006900630061006C0020000D000A002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020007300790073002E0063006F006C0075006D006E007300200043000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E0020007300790073002E006F0062006A00650063007400730020004F0020004F004E0020004F002E006F0062006A006500630074005F006900640020003D00200063002E006F0062006A006500630074005F00690064000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E0020007300790073002E00740079007000650073002000540020004F004E00200043002E00730079007300740065006D005F0074007900700065005F006900640020003D00200054002E00730079007300740065006D005F0074007900700065005F00690064000D000A0020002000200020002000200020002000200020002000200020002000200020005700480045005200450020004F002E006E0061006D00650020003D002000270050005200500072006F0064007500630074002700200041004E00440020004F002E00740079007000650020003D002000270055002700200041004E004400200054002E006E0061006D006500200021003D0027007300790073006E0061006D00650027000D000A000D000A00200020002000200020002000200020002000200020002000200020002000200055004E0049004F004E00200041004C004C000D000A000D000A002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400200041002E00490064002C00200041002E005400690074006C0065002C00200044002E005400690074006C0065002C00200030000D000A002000200020002000200020002000200020002000200020002000200020002000460052004F004D002000410054004100740074007200690062007500740065002000410020004A004F0049004E00200041005400410074007400720069006200750074006500440061007400610054007900700065002000440020004F004E00200044002E004900640020003D00200041002E00410074007400720069006200750074006500440061007400610054007900700065004900640001032000000520007B0000057D00200000032800000528007B0000077B0030007D00001B4C00660074004F0062006A006500630074005400790070006500001B5200670074004F0062006A00650063007400540079007000650000174C00660074004F0070006E0064004E0061006D00650000096E0061006D006500000F4C00660074004F0070006E0064000005490064000023410074007400720069006200750074006500440061007400610054007900700065000011520067007400560061006C00750065000003270001052700270001074F007000720000096C0069006B0065000003250000116E006F00740020006C0069006B00650000154900730050006800790073006900630061006C0000033100000769006E007400000F69006E0074006500670065007200000D64006F00750062006C006500000F64006500630069006D0061006C00000B63006100730074002800001720006100730020006D006F006E0065007900290020000009640061007400650000116400610074006500740069006D0065000023200063006F006E007600650072007400280076006100720063006800610072002C00000F2C002000310030003100290020000047200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C0027000111270029002C0020003100300031002900011B700072006F006400750063007400740079007000650069006400000521003D000009200049004E002000000F20004E004F005400200049004E00005320002800530045004C004500430054002000490064002000460052004F004D002000640062006F002E004700650074004E0061007600500072006F00640075006300740054007900700065007300280027000109270029002900200001052000270001033000000741004E00440000810B280045005800490053005400530020002800530045004C00450043005400200031002000460052004F004D00200076007700500072006F00640075006300740041006E00640053004B005500410074007400720069006200750074006500560061006C007500650020005600200057004800450052004500200050002E004900640020003D00200056002E0049006400200061006E00640020004100740074007200690062007500740065004900640020003D00200027007B0030007D002700200061006E0064002000430041005300540028005B00560061006C00750065005D0020006100730020004D004F004E0045005900290020007B0031007D0020007B0032007D0029002900016F2800410074007400720069006200750074006500490064003D0027007B0030007D002700200041004E0044002000430041005300540028005B00560061006C00750065005D0020006100730020004D004F004E00450059002900200020007B0031007D0020007B0032007D002900018189280045005800490053005400530020002800530045004C00450043005400200031002000460052004F004D00200076007700500072006F00640075006300740041006E00640053004B005500410074007400720069006200750074006500560061006C007500650020005600200057004800450052004500200050002E00490064003D0056002E0049006400200061006E0064002000410074007400720069006200750074006500490064003D0027007B0030007D002700200061006E006400200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C005B00560061006C00750065005D0029002C003100300031002900200020007B0031007D00200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C0027007B0032007D00270029002C0020003100300031002900290029000180F12800410074007400720069006200750074006500490064003D0027007B0030007D002700200041004E004400200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C005B00560061006C00750065005D0029002C00310030003100290020007B0031007D00200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C0027007B0032007D00270029002C002000310030003100290029000180E9280045005800490053005400530020002800530045004C00450043005400200031002000460052004F004D00200076007700500072006F00640075006300740041006E00640053004B005500410074007400720069006200750074006500560061006C007500650020005600200057004800450052004500200050002E00490064003D0056002E0049006400200061006E0064002000410074007400720069006200750074006500490064003D0027007B0030007D002700200061006E00640020005B00560061006C00750065005D0020007B0031007D00200027007B0032007D0027002900290001532800410074007400720069006200750074006500490064003D0027007B0030007D002700200041004E00440020005B00560061006C00750065005D0020007B0031007D00200027007B0032007D002700290001134F00520044004500520020004200590020000035530045004C004500430054002000460069006C00740065007200490064002C002000490064002000460052004F004D0020002800000D200041004E00440020002800000D29002900200046004F002000002F43006F006E007400650078007400200043006F006E006E0065006300740069006F006E003D00740072007500650000866557004900540048000900520065006C0061007400650064004E006F0064006500730020002800490064002C00200050006100720065006E007400490064002C0020005400690074006C0065002C0020005B004C006500760065006C005D0029002000410053000D000A000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200028000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900490064002C00200050006100720065006E007400490064002C0020005400690074006C0065002C00200030000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D000900480053005300740072007500630074007500720065000D000A000900090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005700480045005200450009004900640020003D0020004000490064000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000090009000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200055004E0049004F004E00200041004C004C000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000090009000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400090070006100720065006E0074002E00490064002C00200070006100720065006E0074002E0050006100720065006E007400490064002C00200070006100720065006E0074002E005400690074006C0065002C0020005B004C006500760065006C005D0020002B00200031000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D000900520065006C0061007400650064004E006F0064006500730020004100530020006300680069006C0064002C00200048005300530074007200750063007400750072006500200041005300200070006100720065006E0074000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500090070006100720065006E0074002E004900640020003D0020006300680069006C0064002E0050006100720065006E00740049006400200041004E0044000D000A000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200070006100720065006E0074002E004900640020003C003E002000400053006900740065004900640020000D000A000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000D000A000D000A0009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400090009005400690074006C0065000D000A0009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0009000900520065006C0061007400650064004E006F006400650073000D000A00090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004F00520044004500520020004200590009005B004C006500760065006C005D00200044004500530043000007400049006400000F4000530069007400650049006400000D7B0030007D0020003E002000000B5400690074006C006500000963006F006C003100000963006F006C003200000963006F006C003300000963006F006C003400000963006F006C003500000963006F006C003600000963006F006C003700000963006F006C003800000963006F006C003900000B63006F006C00310030000080CD530065006C006500630074002000630061007300740028004C0066007400200061007300200076006100720063006800610072002800310030002900290020002B00200027002D00270020002B0020006300610073007400280052006700740020006100730020007600610072006300680061007200280031003000290029002000660072006F006D00200052004D00520065006C006100740069006F006E00730020005700680065007200650020004E006F006400650054007900700065003D0027007B0030007D002700018105530065006C006500630074002000630061007300740028004C0066007400200061007300200076006100720063006800610072002800310030002900290020002B00200027002D00270020002B0020006300610073007400280052006700740020006100730020007600610072006300680061007200280031003000290029002000660072006F006D00200052004D00520065006C006100740069006F006E00730020005700680065007200650020004C006600740020003E0020007B0030007D00200061006E006400200052006700740020003C0020007B0031007D00200061006E00640020004F0062006A00650063007400490064003D0027007B0032007D002700018089530065006C0065006300740020002A002000660072006F006D00200052004D00520065006C006100740069006F006E00730020005700680065007200650020004C006600740020004200650074007700650065006E0020007B0030007D00200061006E00640020007B0031007D0020004F00720064006500720020006200790020004C0066007400000752006700740000074C006600740000114F0062006A0065006300740049006400000D530074006100740075007300003143006F006E0074006100630074005F0047006500740043006F0075006E0074004200790053007400610074007500730000155300690074006500470072006F00750070007300001B4100700070006C00690063006100740069006F006E0049006400002F43006F006E0074006100630074005F005300650061007200630068005300690074006500470072006F0075007000000F5300690074006500490064007300002B43006F006E0074006100630074005F005300650061007200630068005300690074006500490064007300003743006F006E0074006100630074005F00460069006C0074006500720042007900530069007400650043006F006E007400650078007400001B43006F006E0074006100630074004C0069007300740049006400003543006F006E0074006100630074005F0055007000640061007400650043006F006E00740061006300740043006F0075006E007400001D53006900740065004100740074007200690062007500740065007300003943006F006E0074006100630074005F00530065006100720063006800530069007400650041007400740072006900620075007400650073000001001353006F00720074004F007200640065007200001D43006F006E00740061006300740053006F0075007200630065007300002144006900730074007200690062007500740069006F006E004C00690073007400001549006E006400650078005400650072006D007300001D530065006300750072006900740079004C006500760065006C007300001D43007500730074006F006D0065007200470072006F00750070007300000F5700610074006300680065007300002357006500620073006900740065005500730065007200530065006100720063006800002D43006F006E0074006100630074004100740074007200690062007500740065005300650061007200630068000023500072006F0064007500630074007300500075007200630068006100730065006400002B500072006F00640075006300740054007900700065007300500075007200630068006100730065006400001B46006F0072006D005300750062006D006900740074006500640000134C006100730074004C006F00670069006E0000115000750072006300680061007300650000134F007200640065007200530069007A00650000154F00720064006500720043006F0075006E007400001B4100620061006E0064006F006E00650064004300610072007400002F470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660049006E007400730000633C0043006F006E0074006100630074004100740074007200690062007500740065005300650061007200630068003E003C002F0043006F006E0074006100630074004100740074007200690062007500740065005300650061007200630068003E00000D560061006C0075006500310000194D0069006E0069006D0075006D00560061006C007500650000194D006100780069006D0075006D00560061006C007500650000194D0069006E0069006D0075006D0043006F0075006E00740000194D006100780069006D0075006D0043006F0075006E007400003343006F006E0074006100630074005F004100640064004D0061006E00750061006C0043006F006E007400610063007400730000032C00003343006F006E00740061006300740041007400740072006900620075007400650053006500610072006300680058006D006C00001B43006F006E00740061006300740053006F007500720063006500003D43006F006E0074006100630074005F0043006F006E0074006100630074004100740074007200690062007500740065005300650061007200630068000021500072006F00660069006C00650053006500610072006300680058006D006C00002B43006F006E0074006100630074005F00500072006F00660069006C006500530065006100720063006800001F4E0065006700610074006500530065006C0065006300740069006F006E0000174C006900730074004F00660047007500690064007300003D49006E007300650072007400200069006E0074006F002000740065007300740043006F0075006E0074002000760061006C0075006500730028002700010527002C0001032900003743006F006E0074006100630074005F0053006500610072006300680043006F006E00740061006300740053006F0075007200630065000081ED4900460020004500580049005300540053002800530065006C0065006300740020002A002000660072006F006D002000740065006D007000640062002E00640062006F002E007300790073006F0062006A00650063007400730020006F0020007700680065007200650020006F002E0078007400790070006500200069006E00200028002700550027002900090061006E00640009006F002E006900640020003D0020006F0062006A006500630074005F0069006400280020004E002700740065006D007000640062002E002E002300740065006D00700043006F006E0074006100630074005300650061007200630068004F007500740070007500740027002900290020005400720075006E00630061007400650020007400610062006C00650020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F0075007400700075007400200045004C0053004500200063007200650061007400650020007400610062006C00650020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F0075007400700075007400280049006400200075006E0069007100750065006900640065006E0074006900660069006500720020007000720069006D0061007200790020006B006500790029000147440072006F00700020007400610062006C00650020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F00750074007000750074000081F94400650063006C006100720065002000400078006D006C00200078006D006C003B0020007300650074002000400078006D006C0020003D0020002800730065006C0065006300740020002000690073006E0075006C006C00280043006F006E00740061006300740053006F007500720063006500490064002C0031003000290020004900640020002C0043006F0075006E007400280063006F006E00740061006300740053006F0075007200630065002E0049006400290020005B00560061006C00750065005D002000660072006F006D0020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F0075007400700075007400200063006F006E00740061006300740053006F00750072006300650020004C0045004600540020004A004F0049004E002000760077005F0063006F006E00740061006300740073002000430020006F006E00200063006F006E00740061006300740053006F0075007200630065002E004900640020003D00200043002E005500730065007200490064002000470072006F0075007000200062007900200043006F006E00740061006300740053006F00750072006300650049006400200046004F005200200058004D004C0020004100750074006F0029003B002000730065006C006500630074002000400078006D006C003B0000154D00610078005200650063006F00720064007300001D49006E0063006C007500640065004100640064007200650073007300001954006F00740061006C005200650063006F00720064007300003F43006F006E0074006100630074005F0047006500740043006F006E00740061006300740042007900490064007300460072006F006D00540065006D0070000013530074006100720074004400610074006500000F45006E0064004400610074006500003543006F006E0074006100630074005F005300650061007200630068004C0061007300740050007500720063006800610073006500002F43006F006E0074006100630074005F005300650061007200630068004F007200640065007200530069007A006500003143006F006E0074006100630074005F005300650061007200630068004F00720064006500720043006F0075006E007400003743006F006E0074006100630074005F005300650061007200630068004100620061006E0064006F006E00650064004300610072007400002F43006F006E0074006100630074005F005300650061007200630068004C006100730074004C006F00670069006E0000155700610074006300680065007300490064007300002B43006F006E0074006100630074005F005300650061007200630068005700610074006300680065007300000F46006F0072006D00490064007300003943006F006E0074006100630074005F0053006500610072006300680046006F0072006D0073005300750062006D00690074007400650064000015500072006F006400750063007400490064007300003F43006F006E0074006100630074005F00530065006100720063006800500072006F0064007500630074007300500075007200630068006100730065006400001D500072006F0064007500630074005400790070006500490064007300004743006F006E0074006100630074005F00530065006100720063006800500072006F0064007500630074005400790070006500730050007500720063006800610073006500640000295700650062007300690074006500550073006500720053006500610072006300680058006D006C00003343006F006E0074006100630074005F0053006500610072006300680057006500620073006900740065005500730065007200002343007500730074006F006D0065007200470072006F00750070007300490064007300003943006F006E0074006100630074005F0053006500610072006300680043007500730074006F006D0065007200470072006F007500700073000021530065006300750072006900740079004C006500760065006C00490064007300003743006F006E0074006100630074005F00530065006100720063006800530065006300750072006900740079004C006500760065006C00001B49006E006400650078005400650072006D007300490064007300003143006F006E0074006100630074005F0053006500610072006300680049006E006400650078005400650072006D007300002744006900730074007200690062007500740069006F006E004C00690073007400490064007300003D43006F006E0074006100630074005F0053006500610072006300680044006900730074007200690062007500740069006F006E004C00690073007400008093530065006C0065006300740020004E0061006D0065002000660072006F006D0020004300540043006F006E00740061006300740053006500610072006300680043006F006E006600690067002000570068006500720065002000490073004100630074006900760065003D00310020004F0072006400650072002000420079002000530065007100750065006E006300650000094E0061006D006500006143006F006E0074006100630074005300650061007200630068002F00530069007400650041007400740072006900620075007400650073002F004100740074007200690062007500740065005300650061007200630068004900740065006D00004143006F006E0074006100630074005300650061007200630068002F0043006F006E00740061006300740053006F00750072006300650073002F0069006E007400003B43006F006E0074006100630074005300650061007200630068002F005300690074006500470072006F007500700073002F006700750069006400003543006F006E0074006100630074005300650061007200630068002F0053006900740065004900640073002F006700750069006400004743006F006E0074006100630074005300650061007200630068002F0044006900730074007200690062007500740069006F006E004C006900730074002F006700750069006400003B43006F006E0074006100630074005300650061007200630068002F0049006E006400650078005400650072006D0073002F006700750069006400004143006F006E0074006100630074005300650061007200630068002F00530065006300750072006900740079004C006500760065006C0073002F0069006E007400004343006F006E0074006100630074005300650061007200630068002F0043007500730074006F006D0065007200470072006F007500700073002F006700750069006400004F43006F006E0074006100630074005300650061007200630068002F0057006100740063006800650073005B0040004C006900730074004F0066004700750069006400730021003D00220022005D00006543006F006E0074006100630074005300650061007200630068002F00570065006200730069007400650055007300650072005300650061007200630068002F00500072006F00700065007200740079005300650061007200630068004900740065006D00004943006F006E0074006100630074005300650061007200630068002F0043006F006E007400610063007400410074007400720069006200750074006500530065006100720063006800005943006F006E0074006100630074005300650061007200630068002F00500072006F00640075006300740073005000750072006300680061007300650064002F004B0065007900560061006C00750065005000610069007200006143006F006E0074006100630074005300650061007200630068002F00500072006F006400750063007400540079007000650073005000750072006300680061007300650064002F004B0065007900560061006C00750065005000610069007200005B43006F006E0074006100630074005300650061007200630068002F0046006F0072006D005300750062006D00690074007400650064005B0040004C006900730074004F0066004700750069006400730021003D00220022005D00002F43006F006E0074006100630074005300650061007200630068002F004C006100730074004C006F00670069006E00002D43006F006E0074006100630074005300650061007200630068002F0050007500720063006800610073006500002F43006F006E0074006100630074005300650061007200630068002F004F007200640065007200530069007A006500003143006F006E0074006100630074005300650061007200630068002F004F00720064006500720043006F0075006E007400003743006F006E0074006100630074005300650061007200630068002F004100620061006E0064006F006E00650064004300610072007400002943006F006E0074006100630074005300650061007200630068002F00530074006100740075007300002F43006F006E0074006100630074005300650061007200630068002F0053006F00720074004F007200640065007200001B43006F006E00740061006300740053006500610072006300680000114F00700065007200610074006F007200000F4200650074007700650065006E00000D560061006C00750065003200000930002E003000300000353C00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F006600470075006900640073003E0000373C002F00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F006600470075006900640073003E000031470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064007300000B3C007B0030007D003E00000D3C002F007B0030007D003E00003F3C0043006F006E0074006100630074005300650061007200630068003E003C002F0043006F006E0074006100630074005300650061007200630068003E00000D5C0077007B0032002C007D0000810B530045004C004500430054002000490064002C0020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C002000490053004E0055004C004C00280050006100720065006E007400490064002C00200027007B0031007D0027002900200041005300200046006F006C00640065007200490064002C002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740043006F006E00740065006E0074004C006F0063006100740069006F006E002800490064002C00200027007B0032007D002700290020004100530020004C006F0063006100740069006F006E000A00011F460052004F004D00200043004F0043006F006E00740065006E0074000A000080875700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D00200031003300200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D0027002000016541004E00440020004300410053005400280058006D006C0053007400720069006E00670020004100530020006E00760061007200630068006100720028004D00410058002900290020004C0049004B0045002000270025007B0030007D002500270020000180AB41004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004900730043006F006E00740065006E00740044006500660069006E006900740069006F006E004D006100740063006800280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C0020007B0031007D002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A0001254F00520044004500520020004200590020005400690074006C00650020004100530043000023550050004400410054004500200043004F0043006F006E00740065006E0074000A000080E1530045005400200058006D006C0053007400720069006E00670020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640043006F006E00740065006E00740044006500660069006E006900740069006F006E0058006D006C0053007400720069006E006700280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D002C0020007B0034007D0029000A0001826757004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0043006F006E00740065006E00740044006500660069006E006900740069006F006E0073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000290001812F530045004C00450043005400200063006F002E00490064002C00200063006F002E005400690074006C00650020004100730020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C002000490053004E0055004C004C00280063006F002E0050006100720065006E007400490064002C00200027007B0031007D0027002900200061007300200046006F006C00640065007200490064002C002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740043006F006E00740065006E0074004C006F0063006100740069006F006E002800490064002C00200027007B0032007D002700290020004100530020004C006F0063006100740069006F006E000A00017F460052004F004D00200043004F0043006F006E00740065006E007400200063006F00200049004E004E004500520020004A004F0049004E00200043004F00460069006C00650020006600690020004F004E002000660069002E0043006F006E00740065006E0074004900640020003D00200063006F002E00490064000A0000816B57004800450052004500200063006F002E005B005300740061007400750073005D0020003D0020003100200041004E004400200063006F002E004F0062006A0065006300740054007900700065004900640020003D0020003900200041004E004400200063006F002E004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0049007300460069006C006500500072006F0070006500720074006900650073004D006100740063006800280063006F002E005400690074006C0065002C00200063006F002E005B004400650073006300720069007000740069006F006E005D002C002000660069002E0041006C00740054006500780074002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A00012B4F005200440045005200200042005900200063006F002E005400690074006C006500200041005300430000816F53004500540020005400690074006C00650020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400460069006C006500500072006F007000650072007400790028005400690074006C0065002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029002C0020005B004400650073006300720069007000740069006F006E005D0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400460069006C006500500072006F007000650072007400790028005B004400650073006300720069007000740069006F006E005D002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001825F57004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F00460069006C006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A000900090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900011D550050004400410054004500200043004F00460069006C0065000A000080B3530045005400200041006C007400540065007800740020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400460069006C006500500072006F0070006500720074007900280041006C00740054006500780074002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001828357004800450052004500090043006F006E00740065006E00740049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200066002E0043006F006E00740065006E007400490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F00460069006C006500200041005300200066000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F00460069006C006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200066002E0043006F006E00740065006E007400490064000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000290001816F57004800450052004500200063006F002E005B005300740061007400750073005D0020003D0020003100200041004E004400200063006F002E004F0062006A0065006300740054007900700065004900640020003D00200033003300200041004E004400200063006F002E004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004900730049006D00610067006500500072006F0070006500720074006900650073004D006100740063006800280063006F002E005400690074006C0065002C00200063006F002E005B004400650073006300720069007000740069006F006E005D002C002000660069002E0041006C00740054006500780074002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A0001817353004500540020005400690074006C00650020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640049006D00610067006500500072006F007000650072007400790028005400690074006C0065002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029002C0020005B004400650073006300720069007000740069006F006E005D0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640049006D00610067006500500072006F007000650072007400790028005B004400650073006300720069007000740069006F006E005D002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001826157004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0049006D00610067006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000180B5530045005400200041006C007400540065007800740020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640049006D00610067006500500072006F0070006500720074007900280041006C00740054006500780074002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001828557004800450052004500090043006F006E00740065006E00740049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200066002E0043006F006E00740065006E007400490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F00460069006C006500200041005300200066000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0049006D00610067006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200066002E0043006F006E00740065006E007400490064000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000180D5530045004C004500430054002000500064002E00500061006700650044006500660069006E006900740069006F006E00490064002000410053002000490064002C002000500064002E005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C00200050006E002E0050006100670065004D00610070004E006F006400650049006400200061007300200046006F006C00640065007200490064002C0020002700270020004100530020004C006F0063006100740069006F006E000A00018129460052004F004D002000500061006700650044006500660069006E006900740069006F006E002000500064002000430052004F005300530020004100500050004C00590020002800530045004C00450043005400200054004F00500020003100200050006E0064002E0050006100670065004D00610070004E006F0064006500490064002000660072006F006D00200050006100670065004D00610070004E006F00640065005000610067006500440065006600200050006E006400200077006800650072006500200050006E0064002E00500061006700650044006500660069006E006900740069006F006E004900640020003D002000500064002E00500061006700650044006500660069006E006900740069006F006E00490064002900200050006E000A000080B757004800450052004500200053006900740065004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004900730050006100670065004E0061006D0065004D0061007400630068002800500064002E005400690074006C0065002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A00012B4F0052004400450052002000420059002000500064002E005400690074006C0065002000410053004300002D5500500044004100540045002000500061006700650044006500660069006E006900740069006F006E000A000080A353004500540020005400690074006C00650020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640050006100670065004E0061006D00650028005400690074006C0065002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A000182B3570048004500520045000900500061006700650044006500660069006E006900740069006F006E0049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200070002E00500061006700650044006500660069006E006900740069006F006E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D000900500061006700650044006500660069006E006900740069006F006E00200041005300200070000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0050006100670065004E0061006D00650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200070002E00500061006700650044006500660069006E006900740069006F006E00490064000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000180D7530045004C0045004300540020006400650074002E005000610067006500490064002000410053002000490064002C0020006400650066002E005400690074006C00650020004100530020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C00200050006E002E0050006100670065004D00610070004E006F006400650049006400200041007300200046006F006C00640065007200490064002C0020002700270020004100530020004C006F0063006100740069006F006E000A000180BD460052004F004D0020005000610067006500440065007400610069006C0073002000410053002000640065007400200049004E004E004500520020004A004F0049004E002000500061006700650044006500660069006E006900740069006F006E00200041005300200064006500660020004F004E0020006400650066002E00500061006700650044006500660069006E006900740069006F006E004900640020003D0020006400650074002E005000610067006500490064000A000080FD430052004F005300530020004100500050004C00590020002800530045004C00450043005400200054004F00500020003100200050006E0064002E0050006100670065004D00610070004E006F0064006500490064002000660072006F006D00200050006100670065004D00610070004E006F00640065005000610067006500440065006600200050006E006400200077006800650072006500200050006E0064002E00500061006700650044006500660069006E006900740069006F006E004900640020003D0020006400650066002E00500061006700650044006500660069006E006900740069006F006E00490064002900200050006E000A000080DD5700480045005200450020006400650066002E0053006900740065004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00490073005000610067006500500072006F0070006500720074006900650073004D00610074006300680028006400650074002E005000610067006500440065007400610069006C0058004D004C002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A00012D4F00520044004500520020004200590020006400650066002E005400690074006C0065002000410053004300002755005000440041005400450020005000610067006500440065007400610069006C0073000A000080CF53004500540020005000610067006500440065007400610069006C0058006D006C0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740055007000640061007400650064005000610067006500500072006F00700065007200740069006500730028005000610067006500440065007400610069006C0058006D006C002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001827B570048004500520045000900500061006700650049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200070002E005000610067006500490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0009005000610067006500440065007400610069006C007300200041005300200070000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F005000610067006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200070002E005000610067006500490064000D000A000900090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900010D5C0077007B0033002C007D0000810B530045004C004500430054002000490064002C0020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C002000490053004E0055004C004C00280050006100720065006E007400490064002C00200027007B0031007D0027002900200061007300200046006F006C00640065007200490064002C002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740043006F006E00740065006E0074004C006F0063006100740069006F006E002800490064002C00200027007B0032007D002700290020004100530020004C006F0063006100740069006F006E000A000180855700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D0020003700200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D0027002000013141004E00440020005B0054006500780074005D0020004C0049004B0045002000270025007B0030007D0025002700200001809941004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0049007300540065007800740043006F006E00740065006E0074004D00610074006300680028005B0054006500780074005D002C00200027007B0030007D0027002C0020007B0031007D002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A000180BF53004500540020005B0054006500780074005D0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400540065007800740043006F006E00740065006E007400540065007800740028005B0054006500780074005D002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D002C0020007B0034007D0029000A0001825957004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F00540065007800740043006F006E00740065006E0074002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000290001372F00460069006E00640041006E0064005200650070006C006100630065002F00540065007800740043006F006E00740065006E00740000452F00460069006E00640041006E0064005200650070006C006100630065002F0043006F006E00740065006E00740044006500660069006E006900740069006F006E00730000332F00460069006E00640041006E0064005200650070006C006100630065002F0050006100670065004E0061006D0065007300003D2F00460069006E00640041006E0064005200650070006C006100630065002F005000610067006500500072006F007000650072007400690065007300003F2F00460069006E00640041006E0064005200650070006C006100630065002F0049006D00610067006500500072006F007000650072007400690065007300003D2F00460069006E00640041006E0064005200650070006C006100630065002F00460069006C006500500072006F00700065007200740069006500730000808353004500540020005B0054006500780074005D0020003D002000640062006F002E00520065006700650078005F005200650070006C006100630065004D0061007400630068006500730028005B0054006500780074005D002C00200027007B0030007D0027002C0020007B0031007D002C00200027007B0032007D00270029000A0001808D5700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D0020003700200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E00440020000153640062006F002E00520065006700650078005F00490073004D00610074006300680028005B0054006500780074005D002C00200027007B0030007D0027002C0020007B0031007D00290020003D00200031000180C7530045005400200058006D006C0053007400720069006E00670020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740055007000640061007400650064004C0069006E006B0049006E0043006F006E00740065006E00740044006500660069006E006900740069006F006E00280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D0029000A000180F35700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D00200031003300200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E00440020004300410053005400280058006D006C0053007400720069006E00670020004100530020006E00760061007200630068006100720028004D00410058002900290020004C0049004B0045002000270025007B0031007D0025002700200041004E004400200001808F640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00490073004C0069006E006B0049006E0043006F006E00740065006E00740044006500660069006E006900740069006F006E00280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C0020007B0031007D00290020003D0020003100013B2F00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064002F006700750069006400000D27007B0030007D0027002C000180CD530065006C006500630074002000500061006700650044006500660069006E006900740069006F006E00490064002C00500061006700650044006500660069006E006900740069006F006E0058006D006C002C0043006F006E007400610069006E006500720058006D006C0020002000460052004F004D002000500061006700650044006500660069006E006900740069006F006E0020005700680065007200650020002000540065006D0070006C0061007400650049006400200069006E00200028007B0030007D0029000023500061006700650044006500660069006E006900740069006F006E0058006D006C00001943006F006E007400610069006E006500720058006D006C0000272F002F00700061006700650044006500660069006E006900740069006F006E005B0031005D0000156D006F006400690066006900650064004200790000196D006F006400690066006900650064004400610074006500000375000081A93C0063006F006E007400610069006E00650072002000690064003D0022007B0030007D002200200063006F006E00740065006E007400490064003D002200300030003000300030003000300030002D0030003000300030002D0030003000300030002D0030003000300030002D003000300030003000300030003000300030003000300030002200200069006E005700460043006F006E00740065006E007400490064003D002200300030003000300030003000300030002D0030003000300030002D0030003000300030002D0030003000300030002D003000300030003000300030003000300030003000300030002200200063006F006E00740065006E0074005400790070006500490064003D002200300022002000690073004D006F006400690066006900650064003D002200460061006C0073006500220020006900730054007200610063006B00650064003D002200460061006C007300650022002000760069007300690062006C0065003D00220054007200750065002200200069006E0057004600560069007300690062006C0065003D002200540072007500650022002F003E000180C95500700064006100740065002000500061006700650044006500660069006E006900740069006F006E0020005300650074002000500061006700650044006500660069006E006900740069006F006E0058006D006C003D004E0027007B0030007D0027002C0043006F006E007400610069006E006500720058006D006C0020003D004E0027007B0031007D0027002000570068006500720065002000500061006700650044006500660069006E006900740069006F006E00490064003D0027007B0032007D0027000121500061006700650044006500660069006E006900740069006F006E0049006400002550006100670065004D00610070004E006F00640065005F005500700064006100740065000080C9530065006C006500630074002000500061006700650044006500660069006E006900740069006F006E00490064002C00500061006700650044006500660069006E006900740069006F006E0058006D006C002C0043006F006E007400610069006E006500720058006D006C002000460052004F004D002000500061006700650044006500660069006E006900740069006F006E002000570068006500720065002000540065006D0070006C0061007400650049006400200069006E00200028007B0030007D00290000193C0043006F006E007400610069006E006500720073003E00001B3C002F0043006F006E007400610069006E006500720073003E00002D2F002F0063006F006E007400610069006E00650072005B004000690064003D0022007B0030007D0022005D00001B3C0043006F006E007400610069006E006500720073002F003E000080C95500700064006100740065002000500061006700650044006500660069006E006900740069006F006E0020005300650074002000500061006700650044006500660069006E006900740069006F006E0058006D006C003D004E0027007B0030007D0027002C0043006F006E007400610069006E006500720058006D006C003D004E0027007B0031007D00270020002000570068006500720065002000500061006700650044006500660069006E006900740069006F006E00490064003D0027007B0032007D0027000163530065006C00650063007400200070006100670065004D006100700058006D006C002000460052004F004D00200050006100670065004D006100700020005700680065007200650020007300690074006500490064003D0027007B0030007D002700010569006400006750006100670065004D00610070004E006F0064006500200064006F006500730020006E006F00740020006500780069007300740020006F007200200069006E00760061006C0069006400200050006100670065004D00610070004E006F00640065004900640000312F002F0070006100670065004D00610070004E006F00640065005B004000690064003D0022007B0030007D0022005D00004350006100720065006E007400200050006100670065004D00610070004E006F0064006500200064006F006500730020006E006F007400200065007800690073007400001770006100670065004D00610070004E006F00640065000071550070006400610074006500200050006100670065004D00610070002000530065007400200070006100670065004D006100700058006D006C0020003D00200027007B0030007D00270020005700680065007200650020007300690074006500490064003D0027007B0031007D0027000180D1530065006C006500630074002000500061006700650044006500660069006E006900740069006F006E00490064002C00500061006700650044006500660069006E006900740069006F006E0058006D006C002000460052004F004D002000500061006700650044006500660069006E006900740069006F006E0020005700680065007200650020005300690074006500490064003D0027007B0030007D002700200061006E0064002000540065006D0070006C0061007400650049006400200069006E00200028007B0031007D002900011363006F006E007400610069006E0065007200001363006F006E00740065006E00740049006400001B69006E005700460043006F006E00740065006E00740049006400001B63006F006E00740065006E0074005400790070006500490064000015690073004D006F00640069006600690065006400000B460061006C007300650000136900730054007200610063006B006500640000809F5500700064006100740065002000500061006700650044006500660069006E006900740069006F006E0020005300650074002000500061006700650044006500660069006E006900740069006F006E0058006D006C003D004E0027007B0030007D0027002000570068006500720065002000500061006700650044006500660069006E006900740069006F006E00490064003D0027007B0031007D002700012F700072006F0070006F006700610074006500530065006300750072006900740079004C006500760065006C007300002B69006E0068006500720069007400530065006300750072006900740079004C006500760065006C007300001D730065006300750072006900740079004C006500760065006C007300001D700072006F0070006F00670061007400650052006F006C0065007300001969006E006800650072006900740052006F006C0065007300000B72006F006C006500730000312F002F0070006100670065004D00610070004E006F00640065005B004000690064003D0027007B0030007D0027005D00012F640065007300630065006E00640061006E0074003A003A0070006100670065004D00610070004E006F0064006500001770006100670065006D00610070006E006F0064006500001D700061006700650064006500660069006E006900740069006F006E00002770006100670065006D00610070006E006F006400650077006F0072006B0066006C006F007700003B6300680069006C0064003A003A0070006100670065004D00610070004E006F00640065005B004000690064003D0022007B0030007D0022005D0000416300680069006C0064003A003A00700061006700650044006500660069006E006900740069006F006E005B004000690064003D0022007B0030007D0022005D00001D700061006700650044006500660069006E006900740069006F006E00004B6300680069006C0064003A003A0070006100670065004D00610070004E006F006400650057006F0072006B0046006C006F0077005B004000690064003D0022007B0030007D0022005D00002770006100670065004D00610070004E006F006400650057006F0072006B0046006C006F007700001363007200650061007400650064004200790000176300720065006100740065006400440061007400650000216300680069006C0064003A003A0063006F006E007400610069006E006500720000196300680069006C0064003A003A007300740079006C006500001B6300680069006C0064003A003A0073006300720069007000740000376300680069006C0064003A003A0063006F006E007400610069006E00650072005B004000690064003D0022007B0030007D0022005D00002F6300680069006C0064003A003A007300740079006C0065005B004000690064003D0022007B0030007D0022005D00000B7300740079006C00650000316300680069006C0064003A003A007300630072006900700074005B004000690064003D0022007B0030007D0022005D00000D73006300720069007000740000197000750062006C0069007300680043006F0075006E00740000177000750062006C00690073006800440061007400650000237300740061007400750073004300680061006E006700650064004400610074006500005150006100720065006E00740020004E006F006400650020002D00200050006100670065004D00610070004E006F0064006500200064006F006500730020006E006F007400200065007800690073007400013349006E00760061006C00690064002000500061006700650044006500660069006E006900740069006F006E0020004900440000372F002F00700061006700650044006500660069006E006900740069006F006E005B004000690064003D0022007B0030007D0022005D00003550006100670065004D00610070004E006F0064006500200064006F006500730020006E006F00740020006500780069007300740000356300680069006C0064003A003A0070006100670065004D00610070004E006F006400650057006F0072006B0046006C006F00770000173C0077006F0072006B0066006C006F00770073003E0000193C002F0077006F0072006B0066006C006F00770073003E0000114900740065006D005400790070006500001146006F006C006400650072004900640000114C006F0063006100740069006F006E00003343006F006E007400650078007400200043006F006E006E0065006300740069006F006E0020003D0020007400720075006500001540004F0062006A0065006300740049006400730000055C006200003928003F003A0028003F003C0021005B003C005D002E002A003F0029007C0028003F003C003D003E005B005E003C005D002A003F0029002900004728003F003C003D003C007000610067006500440065007400610069006C0073003E002E002A003F0028003F003A003C0028005C0077002B0029003E0029002E002A003F002900004328003F003D0028003F003A002E002A003F003C002F005C0031003E0029002E002A003F003C002F007000610067006500440065007400610069006C0073003E002900002149006E00760061006C006900640020004900740065006D005400790070006500004928003F003C003D003C005C0077002B002E002A003F005C0062005C0077002B003D0028005B00220027005D002900290028003F003A0028003F0021005C00310029002E0029002A0001809528003F003C003D003C0063006F006E00740065006E00740044006500660069006E006900740069006F006E005C0073002B00540065006D0070006C00610074006500490064003D0022005B005E0022005D007B00330036007D0022002E002A005C00620028003F003C00500072006F00700065007200740079003E005C0077002B0029003D00220029005B005E0022005D002A00002349006E00760061006C00690064002000520065006700650078005400790070006500007B28003F003C003D003C0061005B005C0073005D002B005B005E003E005D002A0028003F003A0068007200650066007C00720065007400750072006E00760061006C007500650029003D00280022007C0027002700290029007B0030007D0028003F003D0028003F003A005C0031007C005B002C005D0029002900017F28003F003C003D003C0061005B005C0073005D002B005B005E003E005D002A0028003F003A0068007200650066007C00720065007400750072006E00760061006C007500650029003D00280022007C0027002700290029007B0030007D0028003F003D0028003F003A0028003F0021005C00310029002E0029002A00290001032600000B260061006D0070003B00000F26007300700061006D0070003B00000D2600730070006C0074003B00000926006C0074003B0000033C0000092600670074003B0000033E000019260064006F00750062006C006500710075006F0074003B00000D2600710075006F0074003B0000032200000D2600610070006F0073003B00000F260073006C006100730068003B0000035C000000BD9927AE08169944B657481FA9DE921800042001010803200001052001011111042001010E042001010205200101113D03070102032000020320000E062002124D0E1C052001124D1C0407011151062002124D080E052001124D0E42072E0E0E0E0E0202021271127512750E1279127D1D0E08080E02080E08021280811280850E0E1280811280850202020202020202020202020212808902021D1C115102060E040001020E050002020E0E0500020E0E1C0700040E0E1C1C1C0520001180C1062002010E1271052001011279052001081275052000128125032000080600030E0E0E0E0500020E0E0E0520020E0E0E0520001280810320001C0420011C0E0500010E1D0E042001020E0700040E0E0E0E0E0600020E0E1D1C05000111510E0707030212809502072002010E11810D08070402128095020E0620011280A90E0A070402128095021280990620011281350E070703021280950E0F0708124D1271127912809D0202020E04200012790520001281390920021280C50E11813D042001011C05200012809D0520020E080822070E1280951280951280811280A90E0202021280951280951280811280A90212808911070812240212281280951280AD02122C0E052002011C180720020E0E1280AD0407020E0E09070412300E1280AD0E090705128095020202020607021280950219070B021280950202021280951280951280811280A9021280891307091234020E1280950212381280951280AD0E0307010E1407081280951280951280811280A90E020212808918070B02128095128095124D081280811280A90E0E1280890E31071A0E1271127512751280B10E0E12791D0E020202127D1280B512808502128081128085021D1C0802020212808912809908000112814911814D072002010E128149052000128151062001011280B10620011D0E1D030400010A0E0400010E0A0600030E0E1C1C0420001D1C05200012808505200201081C0620010112808510070C02128085020202020202020202020420011C080406128161070001116D1180C10306116D0C07071180C10202080E08127104061180C10E0709021180C1020202080E08127108070502080E0812710A07031D1280C51280C5080620010111813D0D07041D1280C51280C51280C5083F07221280C90E151180D1010E0E1280D50202113C151280CD010E113C1140114011441180D91180D91148080811400E0E09021280DD0202020202020202020206151280CD010E092000151180D101130006151180D1010E04200013000620011280DD080520001281650620011280F50E080002020E101180D904061181090800011180D911810904061180D9090002021180D91180D9060002020E10080406118091060001081180911207071D1280C50E1280C51280C51280C502080520001D13000600020E0E1D0E21070D113C1280811280DD151280CD011180C102021D0E080E021180C1128089113C08151280CD011180C10520010113000507020E12790520010112710620010111816D0D070412791280E11280A11280A10520001280E1062001011280E11607071280C51280C51280C51280C51280C51280C512790820011280C51280C50500001281751307061D1280C51280C51280C51280C51280C5081607071D1280C51280C51280C51280C51280C51280C5081007051D1280C51280C51280C51280C508100707127512791D1280C5081280C5020816070A12750E12711279127D1D1280C5081280C50212751E0709151280CD010E12750E1279127D128081128085128089151280CD010E4707191280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D51280D50E0E091280D50620011280D50E1B0705151280CD011180C11280811280DD128089151280CD011180C1170705151280CD01081280811280DD128089151280CD010806151280CD0108040001080E12070611401280811280DD1180E5128089114004061180E513070711401280811280DD021180E5128089114013070911441280811280DD0202020212808911440600011181090E0600011180E50E13070911481280811280DD0202020212808911480600011180910E060001118091081B0706151280CD0111401280811280DD1140128089151280CD01114007151280CD0111400E0705124D1280811280851280890E110705124D02151180D1011180C11180C10E08151180D1011180C10E0705124D1280811280DD1280890E1A0706151280CD010E1280D51280811280DD128089151280CD010E1B0705151280CD011180C1128081128085128089151280CD011180C10F07051280C91280811280DD1280890E0820011280DD1280DD0E07071280950E124D02081180C102082004124D0E1C1C1C072002124D0E1D1C0400010802040701124D080703124D081180C1072003124D0E1C1C0D0708114C1280E9020202020202062001011280E90E07081280C91280DD0202020202020620011280DD0E0A0703128095124D1180C105200011810D0507011180C15107270212711280C91280C91280D5124D0E12750E1279127D0E1280C90E0E0E0E0E127902021280811280DD128089021280811280851280D51280DD1180ED1280811280DD0E020E1280811280851280F1020520001280FD0500001180ED0420010E0E0520010E1D03052002010E1C072002010E1280F105200011817961072D0212711280C91280C91280D5124D0E12750E1279127D0E1280C91280C90E0E127902021280811280DD128089021280811280851280D51280DD1180ED1280811280DD0E1280D51280D5020208021280811280DD02021280811280851280F1020520001280DD2D071912710E1280C91280C90E12790E1280DD1280F51180C1020202020202021280DD020202021280F11280F102062001021180C166072E0212711280C91280C91280D5124D0E12750E1279127D0E1280C90E0E127902021280811280DD128089021280811280851280D51280DD1180ED1280811280DD0E021280DD1280F51180C11280D51280D508021280811280DD02021280811280851280F1020600011180A5020B00021180A51180A51180A5060001021180A50A20031280DD11817D0E0E0820011280F51280F58081074A1280F51280F51280F51280F51280F51280F502020E02020E0E0E1280DD021280D51280DD1280F51280F50E0E081280DD021280F50202021D0E1D0E1280F91D0E080E1D0E080E02021280F50202021D0E1D0E1280F91D0E080E020202021D0E1D0E1280F91D0E080E02021280F502021D0E1D0E1280F91D0E080E02021280F5042001081C042001021C082001128181128149021D0E0800011280F91281851D07111280811280DD0E0E0E1280DD1280FD020E0E0202020202021280890520001280D50B20021280DD1280DD1280DD23070D1280FD1280811280F51280F5128089021180C1021280F5021180ED1280F51280FD0620011280FD0E0500001180C10520001180ED2507111280811280F5020E1180C11280F502021280F50212808902021280F5021180ED1280F5090002021180C11180C15C07280E1280D50E1280D50E1280D51280D51280D51280D51280811280DD0E0E1280DD02021280891280811280DD1280FD1280811280DD0E0E1280DD02021280811280DD1280FD1280811280DD0E0E1280DD02021280811280DD1280FD1307090208081280F5021180ED1280F5021280F50400010E0835071D12710E1280C91280C90E12790E1280DD1180C1021280F50202020202021280D50202081280DD1280F902021280FD02021280F10800011180E51180ED66072E12710E1280C91280C90E12790E1280DD1280D5128105020202021280811280DD1280891280811280DD021280D51280811280DD0E02021280FD1280F11280811280DD0E02021280FD021280D51280811280DD1280810E1280DD02021280DD1280FD1280F10800011180A51180A5052002011C1C05200012818D0420011C1C0E07061280951280A9124D020211510520001280A90807021280E91280E9072002010E11813D082003010E11813D0A072001011D1281950E07061271127912809D1280E9020207200201081180C105200201080E052002010808060702127112790C07060E0202114C114C1280950400010E0E0C07060E020211501150128095060702021280950707030E021280950507030E020E0407020908042001030808B77A5C561934E0890401000000040200000004040000000408000000041000000004200000000306124D040611810D0306115104061280950306122403061228030612340206080306114C0306115005200101115105200101110804200011510520010112550520010112590E00051151116D115111511151116D0A000302115111511180910A00030E115111511180910C000312809911511151118091070002011C1011510C00040E1151115111809111510700020E116D116D110005021280A111511180A51180A51180A51300060E1280A1115111511180A51180A51180A50A00030E1280A91280950E1000050E1280A91280950E1280951280951100060211511151115111511180A51180A50F00050E1151115111511180A51180A50D000402115111511180A51180A50E0004021280A111511180A51180A51000050E1280A1115111511180A51180A510000502115111511180A51180A51180A51200060E1151115111511180A51180A51180A50B0003021280A111511180A50D00040E1280A1115111511180A50800021280990E116D22000B011C10116D10116D10116D10116D10116D10116D10116D10116D10116D10116D0A0003011280A1116D116D180007011280A1116D118091101180911180A5116D101280A10F0004011280A1116D1180911011809106000208081271090003080E1180C11271080002081180C11271140008011280A1116D10081008100E12711180C1020F0004080E1180C11271151280CD010E0A000308113C1180C11271070001113C1280D5070003010E0812710500010112710700011280A112710D0007011180C1080E08127102080A00030811401180C112710A00030811441180C112710A00030811481180C112710A0003080E1D1280C5127109000212750E1D1280C50A0001151280CD010E12710900021280D51280C90E0D0001151280CD011180C11280D50B0001151280CD01081280D507000111401280D507000111441280D507000111481280D50C0001151280CD0111401280D50500010E12750B00010E151280CD011180C10600010E1280D50700020E1280D50E0B0001151280CD010E1280C90C0001151280CD011180C1127510000501116D11511180A51180A51180A515000701116D1280A1115111511180A51180A51180A50D000401116D11511180A51180A512000601116D1280A1115111511180A51180A515000701116D118091115111511180A51180A51180A519000901116D1280A111511151115111511180A51180A51180A50C000401116D115111511180A50F0004011180C11280A11180C11180E5190007011180C11180C11280A11180C11180C11180E5101180C1080003010E0E1180C10E0005011180C10E0E1180C11180A5090002011280C91280DD150006011280DD1280DD1180C11180E51280C91280DD1300061280FD0E1180C11180E51280DD1280C902160007011180C11180E51280DD1280DD1280C91280F9020C0003011280FD1280DD1280C9090002011280DD1280C9190007011180C11180C11180C11180C11280A1101180C11180A51B0008011180C11180C11280A11180A51180A51180A51180C11180E507000311510E0E0E030000010500001280E9040001010E050002010E0E0A0004128095114C0E02020A000412809511500E02020700021280950E02040001090E0620010E1280A90801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F7773010801000701000000000C010007372E302E302E3000001B0100164272696467656C696E65204469676974616C20496E6300002101001C436F7079726967687420C2A9204272696467656C696E65203230313500000A01000569415050530000050100000000150100104657434C5353746F72656450726F637300008126010002005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A446174614163636573730100000054557F4D6963726F736F66742E53716C5365727665722E5365727665722E53797374656D446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038391053797374656D44617461416363657373010000005701000200540E1146696C6C526F774D6574686F644E616D651852656765785F4765744D6174636865735F46696C6C526F77540E0F5461626C65446566696E6974696F6E134D61746368206E76617263686172284D415829170100010054020F497344657465726D696E697374696301808F010001005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A4461746141636365737301000000819B010003005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A4461746141636365737301000000540E1146696C6C526F774D6574686F644E616D650746696C6C526F77540E0F5461626C65446566696E6974696F6E80DC636F6C3120756E697175656964656E7469666965722C636F6C3220756E697175656964656E7469666965722C636F6C3320756E697175656964656E7469666965722C636F6C3420756E697175656964656E7469666965722C636F6C3520756E697175656964656E7469666965722C636F6C3620756E697175656964656E7469666965722C636F6C3720756E697175656964656E7469666965722C636F6C3820756E697175656964656E7469666965722C636F6C3920756E697175656964656E7469666965722C636F6C313020756E697175656964656E746966696572808501000200000006005402174973496E76617269616E74546F4475706C696361746573005402124973496E76617269616E74546F4E756C6C73015402124973496E76617269616E74546F4F726465720054020D49734E756C6C4966456D7074790154080B4D61784279746553697A65401F0000540E044E616D650B47656E6572617465586D6C12010001005408074D617853697A65FFFFFFFF000000000000EA3D545E00000000020000001C010000FC8D0100FC6F0100525344533ED15B95D49EAE4B9CBD106A71237C5C01000000433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C6F626A5C44656275675C4272696467656C696E652E434C5246756E6374696F6E732E706462000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000408F010000000000000000005A8F01000020000000000000000000000000000000000000000000004C8F0100000000000000000000005F436F72446C6C4D61696E006D73636F7265652E646C6C0000000000FF250020001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058A00100BC0300000000000000000000BC0334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100000007000000000000000700000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B0041C030000010053007400720069006E006700460069006C00650049006E0066006F000000F802000001003000300030003000300034006200300000003A001100010043006F006D006D0065006E007400730000004600570043004C005300530074006F00720065006400500072006F0063007300000000004E001700010043006F006D00700061006E0079004E0061006D006500000000004200720069006400670065006C0069006E00650020004400690067006900740061006C00200049006E006300000000004A0011000100460069006C0065004400650073006300720069007000740069006F006E00000000004600570043004C005300530074006F00720065006400500072006F006300730000000000300008000100460069006C006500560065007200730069006F006E000000000037002E0030002E0030002E003000000058001C00010049006E007400650072006E0061006C004E0061006D00650000004200720069006400670065006C0069006E0065002E0043004C005200460075006E006300740069006F006E0073002E0064006C006C0000005C001C0001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020004200720069006400670065006C0069006E0065002000320030003100350000002A00010001004C006500670061006C00540072006100640065006D00610072006B007300000000000000000060001C0001004F0072006900670069006E0061006C00460069006C0065006E0061006D00650000004200720069006400670065006C0069006E0065002E0043004C005200460075006E006300740069006F006E0073002E0064006C006C0000002C0006000100500072006F0064007500630074004E0061006D00650000000000690041005000500053000000340008000100500072006F006400750063007400560065007200730069006F006E00000037002E0030002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000037002E0030002E0030002E0030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008001000C0000006C3F00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;


GO
ALTER ASSEMBLY [Bridgeline.CLRFunctions]
    DROP FILE ALL
    ADD FILE FROM 0x4D6963726F736F667420432F432B2B204D534620372E30300D0A1A445300000000020000020000001B010000DC0400000000000017010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3800000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0BCA3101380000000010000000100000000000002700FFFF04000000FFFF03000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000BCA3101380000000010000000100000000000002800FFFF04000000FFFF0300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000942E3101EA3D545E010000003ED15B95D49EAE4B9CBD106A71237C5C00000000000000000100000001000000000000000000000000000000DC51330100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000BCA310138000000001000000010000000000000FFFFFFFF04000000FFFF030000000000FFFFFFFF00000000FFFFFFFF00000000FFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000BCA310138000000001000000010000000000000FFFFFFFF04000000FFFF030000000000FFFFFFFF00000000FFFFFFFF00000000FFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC162000000000000000BA0FE1D06D35F116769551549B4FE7C4F06138147D9C76CA7F44C52E61B39D88000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC1620000000000000003D90258B55003CE6FFCAFFCC582B0AC5D5B0C9A2F8D400866A7B771AD06F6320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC162000000000000000804F18CC8F113762818E86C8033A1A7F7ADE6E3CA471088530622985A80ECA15000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC162000000000000000D9A37113B7C324CB1391018CFC17F32523D01D230E708F68AEC6C6E5011C19A9000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC1620000000000000002C4E8FA8331476F23F15CC0A70258682CBEFAEEB48CBD8AEEC55D487BBDBB1FB000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC1620000000000000008E0AEB687CFFF43F139C630A43F29452A558D4F3D907EA014AC6C8E04502E43C000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC1620000000000000001794485CEF3B71D949CB12FF327C33792DC480318F5BE0EF027D9DD8C210037B000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC162000000000000000D34CC735C5B7F389DE710280C7F32E196904C1D735D70613A443E5418CDB202B000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC16200000000000000012DD56C76E1A0B98BE9DB85E7D7646834FD34DB952047EF81D386B358400F38B000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC16200000000000000051B2AAA20D60B199BF594BD6812CDE611B0AF89AFB52AEF68FE4121DD36E47D5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC1620000000000000000295927C447BC06FDA4806DE714870743A3F48F8CABA57FC35E464CE67B05D36000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC1620000000000000002F19996CA5C42CCED08A32AD6F369B3977ACBC1C32B2FE324D807B3A53558EDF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC16200000000000000073092A2249632B51DD7A1E7C2CFFB60465C5F8B04A7EB4E6BF120316EF413E76000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC1620000000000000002FB04DC160D632448B90B348C1B8E9A4694E72CC07DEEBB89223AB21C0FC78EE000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC1620000000000000002E1D6FD129BE5EF151761DC2CEAEC3E0AE89978210F79DEFCA1C3139711ED3C6000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC1620000000000000001621D7B2A2A27CC870C455F7D547C0E95291E50E320B0B8A943AB2FD7A376E79000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC16200000000000000008E0966514116781BB3683E082F73A81CA15DCBC808B05BEB82E5621A7644EED000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F862513FC607D311905300C04FA302A1C4454B99E9E6D211903F00C04FA302A10B9D865A1166D311BD2A0000F80849BD0FD02988B8111342878B770E8597AC162000000000000000F6ACB4FB04ED4FA1A6C76554942A6BAE804AFD65D03E69AF1DA4AB3DD16B4FDA0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000F391000000000000F3910000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FEEFFEEF010000000C10000000433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C47656E6572617465586D6C2E63730000633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C67656E6572617465786D6C2E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C4E617646696C7465725F47657451756572792E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C6E617666696C7465725F67657471756572792E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C46756E6374696F6E735C526567756C617245787072657373696F6E732E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C66756E6374696F6E735C726567756C617265787072657373696F6E732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C46756E6374696F6E735C526573756C744C6F636174696F6E732E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C66756E6374696F6E735C726573756C746C6F636174696F6E732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C436F6E74656E74446566696E6974696F6E2E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C636F6E74656E74646566696E6974696F6E2E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C46696C6550726F706572746965732E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C66696C6570726F706572746965732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C496D61676550726F706572746965732E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C696D61676570726F706572746965732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C506167654E616D652E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C706167656E616D652E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C5061676550726F706572746965732E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C7061676570726F706572746965732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C54657874436F6E74656E742E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C74657874636F6E74656E742E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C53746F7265642050726F636564757265735C5570646174654C696E6B732E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C73746F7265642070726F636564757265735C7570646174656C696E6B732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C52656C6174696F6E4D616E616765722E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C72656C6174696F6E6D616E616765722E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C436F6E746163745F536561726368434C522E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C636F6E746163745F736561726368636C722E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C53746F7265642050726F636564757265735C46696E642E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C73746F7265642070726F636564757265735C66696E642E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C53746F7265642050726F636564757265735C5265706C6163652E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C73746F7265642070726F636564757265735C7265706C6163652E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C506167654D61704E6F64655F5570646174652E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C706167656D61706E6F64655F7570646174652E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C52656765782E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C72656765782E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C5574696C6974795C5574696C6974792E637300633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C7574696C6974795C7574696C6974792E6373003D000000000000000000000000000000B80000001A01000000000000D7090000FA0100007C010000C60C0000410D0000580900002A0F0000E90600006E030000F00300005C000000700600009B0F000000000000560A0000B50A0000010000005D000000BC0D000072040000F10400001E0E00000000000078020000F302000000000000000000000000000060080000DC0800000000000062070000E1070000000000000000000070050000D60B00004E0C0000F0050000000000000000000000000000000000000000000000000000000000000000000000000000140B0000750B00000000000000000000800E0000D50E0000000000002500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001BE230016C0300008B030D1F58EBD501010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000120000001C00000001000000FF4F2A0A00000000F0030000280000001BE230014FF1350C680000006E0300005C000000F0030000650000000000000000000000DC080000280000001BE23001A394CCEC68000000600800005C000000DC0800006500000000000000000000001A010000280000001BE23001E4CBFFCE68000000B80000005C0000001A010000650000000000000000000000FA010000280000001BE230012581F9C9680000007C0100005C000000FA010000650000000000000000000000E1070000280000001BE23001DE18C08368000000620700005C000000E1070000650000000000000000000000F1040000280000001BE23001C1F4752D68000000720400005C000000F1040000650000000000000000000000E9060000280000001BE23001203BC3E868000000700600005C000000E9060000650000000000000000000000D7090000280000001BE23001F599A50F68000000580900005C000000D7090000650000000000000000000000F0050000280000001BE2300107D4F4B468000000700500005C000000F00500006500000000000000000000005D000000280000001BE230017CD0F4F568000000010000005C0000005D000000650000000000000000000000410D0000280000001BE2300169E57AF068000000C60C00005C000000410D00006500000000000000000000001E0E0000280000001BE23001820426A068000000BC0D00005C0000001E0E00006500000000000000000000004E0C0000280000001BE23001CB180F3268000000D60B00005C0000004E0C0000650000000000000000000000D50E0000280000001BE230014A181DAE68000000800E00005C000000D50E00006500000000000000000000009B0F0000280000001BE230018F6E1350680000002A0F00005C0000009B0F0000650000000000000000000000750B0000280000001BE230014538C04968000000140B00005C000000750B0000650000000000000000000000B50A0000280000001BE230015DB958F768000000560A00005C000000B50A0000650000000000000000000000F3020000280000001BE230011B2CB08768000000780200005C000000F302000065000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000003A002A1100000000D004000000000000B1090000000000000000000007000006A900000001000000004E617646696C7465725F476574517565727900160003110400000010040000B1090000A9000000010000001E002411554D6963726F736F66742E53716C5365727665722E53657276657200120024115553797374656D2E44617461000000001A0024115553797374656D2E446174612E53716C436C69656E7400001A0024115553797374656D2E446174612E53716C54797065730000001A002011000000000300001100000000000000006F726465724279001A0020110100000003000011000000000000000071756572793100001A00201102000000030000110000000000000000717565727932000022002011030000000300001100000000000000006F7574707574537472696E670000000016000311400000000C040000DA08000073010000010000001A00201107000000030000110000000000000000636F6E6E00000000160003113C01000008040000BF0800007F010000010000002200201108000000030000110000000000000000647446696C7465724974656D730000002600201109000000030000110000000000000000647450726F64756374436F6C756D6E7300000000160020110A00000003000011000000000000000073716C00160020110B000000030000110000000000000000636D64001A0020110C0000000300001100000000000000006164617074657200220020110D0000000300001100000000000000006172436F6E646974696F6E7300000000220020110E0000000300001100000000000000006C66744F626A65637454797065000000220020110F0000000300001100000000000000007267744F626A656374547970650000002200201110000000030000110000000000000000636F6E646974696F6E466F726D61740022002011110000000300001100000000000000006861734174747269627574657300000016002011120000000300001100000000000000006900000022002011130000000300001100000000000000006F726465724279436C61757365000000160003117001000040030000860000003E020000010000001600201114000000030000110000000000000000696E6300020006001600031170010000040400004E060000E90200000100000016002011170000000300001100000000000000006472000016000311440300000004000040060000F7020000010000001E0020111800000003000011000000000000000072677456616C7565000000001E002011190000000300001100000000000000006461746174797065000000001600031174030000FC030000E00500003203000001000000160020111B00000003000011000000000000000064724300020006000200060002000600020006000200060002000600BA000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C00000001000400040600018C00000001130182630182FE0183D502340280B802818001841001846701849F0184D90186D8018753018A6D018B02018B11018B34018B66018D4701A068018BCF028BC6068D5B018D5B018E23018E45068E60018E60028EB702900A0290790290EE02914F0292780293E30296350296C902976A0299F7029A69029D900002A09302A141001601000200060036002A1100000000F8050000000000003A0000000000000000000000080000065A0A0000010000000052656765785F49734D61746368000016000311D4040000B40500003A0000005A0A0000010000001A0024115553797374656D2E446174612E53716C5479706573000000220024115553797374656D2E546578742E526567756C617245787072657373696F6E7300160024115553797374656D2E436F6C6C656374696F6E7300160003110C050000B005000025000000690A0000010000001A00201101000000040000110000000000000000726567657800000002000600020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C000000010003000406000210000000020C013E160100000200060036002A1100000000CC0600000000000051000000000000000000000009000006940A0000010000000052656765785F4765744D617463680016000311FC0500008406000051000000940A0000010000001600031134060000800600003C000000A30A0000010000001A002011010000000500001100000000000000007265676578000000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000080000060406000314000000020C013E0280961601000000020006003A002A1100000000A4070000000000004C00000000000000000000000A000006E50A0000010000000052656765785F4765744D61746368657300000016000311D00600005C0700004C000000E50A000001000000160003110C0700005807000037000000F40A0000010000001A002011010000000600001100000000000000007265676578000000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000080000060406000314000000020C013E02809616010000000200060042002A11000000001C080000000000001800000000000000000000000B000006310B0000010000000052656765785F4765744D6174636865735F46696C6C526F770000002E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004010000040100000C00000008000006020006003E002A1100000000F4080000000000004100000000000000000000000C000006490B0000010000000052656765785F5265706C6163654D6174636865730000001600031120080000B008000041000000490B0000010000001600031160080000AC0800002C000000580B0000010000001A00201101000000070000110000000000000000726567657800000002000600020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000080000060406000210000000020C013E16010000020006004A002A1100000000C40A000000000000F700000000000000000000000D0000068A0B0000010000000046696E64416E645265706C6163655F476574436F6E74656E744C6F636174696F6E000016000311F8080000700A0000F70000008A0B0000010000000A0024115553797374656D00120024115553797374656D2E44617461000000001A0024115553797374656D2E446174612E53716C436C69656E7400001A0024115553797374656D2E446174612E53716C5479706573000000120024115553797374656D2E54657874000000001E002411554D6963726F736F66742E53716C5365727665722E536572766572001E002011000000000800001100000000000000006C6F636174696F6E00000000220020110100000008000011000000000000000073716C436F6E6E656374696F6E0000001E0020110200000008000011000000000000000073716C436F6D6D616E640000220020110300000008000011000000000000000073716C44617461526561646572000000020006004E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C000000010006000406000320000000011A01530180A701852902856002859302867A16010000000200060052002A11000000006C0D0000000000004301000000000000000000000E000006810C0000010000000046696E64416E645265706C6163655F4973436F6E74656E74446566696E6974696F6E4D617463680000000016000311C80A0000080D000043010000810C0000010000000A0024115553797374656D001A0024115553797374656D2E446174612E53716C5479706573000000120024115553797374656D2E5465787400000000220024115553797374656D2E546578742E526567756C617245787072657373696F6E7300160024115546696E64416E645265706C616365434C5200001E00201100000000090000110000000000000000656469746F725265676578001E0020110100000009000011000000000000000068746D6C5265676578000000160003111C0B0000040D0000C8000000D20C0000010000001A002011030000000900001100000000000000006D6174636800000016000311EC0B0000000D0000BC000000DE0C0000010000001E0020110400000009000011000000000000000068746D6C537472696E67000016000311200C0000FC0C000087000000120D000001000000220020110800000009000011000000000000000061747472696275746552656765780000260020110900000009000011000000000000000061747472696275746556616C756552656765780016000311580C0000F80C0000290000004E0D000001000000220020110B00000009000011000000000000000061747472696275746556616C7565000002000600020006000200060002000600020006005E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C00000001000500040600023000000001120180A406812401812401817E0281C116010282140182640182D606836F01836F0283DC000000020006005E002A1100000000380F000000000000F400000000000000000000000F000006C40D0000010000000046696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E670000000016000311700D0000D00E0000F4000000C40D00000100000022002011000000000A00001100000000000000004353243C3E385F5F6C6F63616C73300016000311D00D0000CC0E0000CE000000E20D00000100000022002011020000000A00001100000000000000004353243C3E385F5F6C6F63616C7331001E002011030000000A0000110000000000000000656469746F7252656765780022002011040000000A00001100000000000000006D617463684576616C7561746F720000160003110C0E0000C80E0000520000003A0E00000100000022002011060000000A00001100000000000000004353243C3E385F5F6C6F63616C73320002000600020006000200060062000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004030000040100000C0000000E000006040600011C0000001F01020C1F29013E0181670281861F81BD1601000407000318000000090103012981BD844C03833104000000020006004A002A1100000000FC0F00000000000023000000000000000000000010000006B80E00000100000000436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F7200000000160003113C0F0000BC0F000023000000B80E0000010000001A002011000000000B00001100000000000000006F75747075740000020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E000006040600000C00000001131601020006004A002A1100000000201100000000000053000000000000000000000011000006DB0E00000100000000436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F72000000001600031100100000C810000053000000DB0E00000100000022002011000000000C00001100000000000000004353243C3E385F5F6C6F63616C7330001A002011010000000C00001100000000000000006F7574707574000022002011020000000C00001100000000000000006D617463684576616C7561746F7200000200060052000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004030000040100000C0000000E00000604060003140000001F0101130180AA160100000004070001100000000B01010180F20200020006004E002A1100000000F411000000000000890000000000000000000000120000062E0F0000010000000046696E64416E645265706C6163655F497346696C6550726F706572746965734D617463680000001600031124110000A8110000890000002E0F0000010000001A002011000000000D000011000000000000000072656765780000000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E0000060406000318000000011202809116010280E5028145000000020006004E002A1100000000DC120000000000004A000000000000000000000013000006B70F0000010000000046696E64416E645265706C6163655F4765745570646174656446696C6550726F7065727479000016000311F8110000981200004A000000B70F00000100000016000311481200009412000035000000C60F0000010000001A00201101000000070000110000000000000000726567657800000002000600020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E0000060406000210000000020C013E16010000020006004E002A1100000000B0130000000000008900000000000000000000001400000601100000010000000046696E64416E645265706C6163655F4973496D61676550726F706572746965734D61746368000016000311E0120000641300008900000001100000010000001A002011000000000D000011000000000000000072656765780000000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E0000060406000318000000011202809216010280E6028146000000020006004E002A110000000098140000000000004A0000000000000000000000150000068A100000010000000046696E64416E645265706C6163655F47657455706461746564496D61676550726F70657274790016000311B4130000541400004A0000008A100000010000001600031104140000501400003500000099100000010000001A00201101000000070000110000000000000000726567657800000002000600020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E0000060406000210000000020C013E160100000200060046002A110000000058150000000000002F000000000000000000000016000006D4100000010000000046696E64416E645265706C6163655F4973506167654E616D654D6174636800160003119C140000181500002F000000D4100000010000001A002011000000000E00001100000000000000007265676578000000020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E000006040600000C00000001121601020006004A002A11000000003C160000000000004900000000000000000000001700000603110000010000000046696E64416E645265706C6163655F47657455706461746564506167654E616D650000160003115C150000F815000049000000031100000100000016000311A8150000F41500003400000012110000010000001A00201101000000070000110000000000000000726567657800000002000600020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E0000060406000210000000020C013E16010000020006004E002A110000000004170000000000002E0000000000000000000000180000064C110000010000000046696E64416E645265706C6163655F49735061676550726F706572746965734D617463680000001600031140160000C41600002E0000004C110000010000001A002011000000000E00001100000000000000007265676578000000020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E000006040600000C000000011216010200060052002A1100000000F017000000000000470000000000000000000000190000067A110000010000000046696E64416E645265706C6163655F476574557064617465645061676550726F70657274696573000000001600031108170000AC170000470000007A11000001000000160003115C170000A81700003300000088110000010000001A00201101000000070000110000000000000000726567657800000002000600020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E0000060406000210000000020C013E16010000020006004A002A11000000009819000000000000E900000000000000000000001A000006C1110000010000000046696E64416E645265706C6163655F497354657874436F6E74656E744D61746368000016000311F417000040190000E9000000C11100000100000016000311401800003C190000D1000000D31100000100000022002011010000000F0000110000000000000000636F6E74656E745265676578000000001600031158180000381900008B000000181200000100000022002011050000000F00001100000000000000006174747269627574655265676578000026002011060000000F000011000000000000000061747472696275746556616C756552656765780016000311941800003419000028000000591200000100000022002011080000000F000011000000000000000061747472696275746556616C756500000200060002000600020006000200060052000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E0000060406000024000000020C013E0280C5160102811D01816D0181DF0682780182780282E7000200060052002A1100000000781B000000000000C900000000000000000000001B000006AA120000010000000046696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E7454657874000000160003119C190000101B0000C9000000AA1200000100000022002011000000001000001100000000000000004353243C3E385F5F6C6F63616C73300016000311F01900000C1B0000A2000000C9120000010000001A0020110200000010000011000000000000000074657874000000002200201103000000100000110000000000000000636F6E74656E74526567657800000000160003112C1A0000081B0000580000000E1300000100000022002011050000001000001100000000000000004353243C3E385F5F6C6F63616C73310022002011060000001000001100000000000000006174747269627574655265676578000022002011070000001000001100000000000000006D617463684576616C7561746F72000002000600020006000200060062000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004030000040100000C0000000E00000604060002200000001F01020C013F01690281351F816C01818501829F16010000040700031400000015010201816C82F003000000020006003E002A1100000000F81B0000000000001300000000000000000000001C0000067313000001000000005461674174747269627574654576616C7561746F7200003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000000E000006040600020C000000160100000200060052002A1100000000041E0000000000008900000000000000000000001D00000686130000010000000046696E64416E645265706C6163655F49734C696E6B496E436F6E74656E74446566696E6974696F6E00000016000311FC1B0000B41D00008900000086130000010000001A0024115553797374656D2E446174612E53716C5479706573000000160024115546696E64416E645265706C616365434C5200001E002411554D6963726F736F66742E53716C5365727665722E53657276657200120024115553797374656D2E54657874000000000A0024115553797374656D00220024115553797374656D2E546578742E526567756C617245787072657373696F6E73001E00201100000000120000110000000000000000656469746F725265676578001E002011010000001200001100000000000000006C696E6B526567657800000016000311501C0000B01D00002E000000BA130000010000001A002011030000001200001100000000000000006D6174636800000016000311401D0000AC1D000022000000C6130000010000001E0020110400000012000011000000000000000068746D6C537472696E6700000200060002000600020006004A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C00000001000600040600001C00000001120180850680D60180D6018130028173160100020006005A002A110000000004200000000000000E01000000000000000000001E0000060F140000010000000046696E64416E645265706C6163655F476574557064617465644C696E6B496E436F6E74656E74446566696E6974696F6E00000016000311081E0000AC1F00000E0100000F1400000100000016000311641E0000A81F0000F500000020140000010000001E00201101000000130000110000000000000000656469746F725265676578001E002011020000001300001100000000000000006C696E6B52656765780000001E00201103000000130000110000000000000000786D6C537472696E670000001E00201104000000130000110000000000000000706F736974696F6E00000000160003117C1E0000A41F00006A0000005E140000010000001A002011060000001300001100000000000000006D6174636800000016000311141F0000A01F00005C0000006C140000010000001E0020110700000013000011000000000000000068746D6C537472696E6700001E002011080000001300001100000000000000006E6577537472696E670000000200060002000600020006000200060052000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000001D0000060406000024000000020C013E0180B501811801814A0681670181670181C9018215001601020006003A002A110000000098230000000000004B04000000000000000000001F0000061D150000010000000047657452656C6174696F6E5461626C650000001600031108200000102300004B0400001D150000010000000A0024115553797374656D00120024115553797374656D2E44617461000000001A0024115553797374656D2E446174612E53716C436C69656E7400001A0024115553797374656D2E446174612E53716C54797065730000001E002411554D6963726F736F66742E53716C5365727665722E53657276657200160024115553797374656D2E436F6C6C656374696F6E73002600201100000000140000110000000000000000636F6E6E636574696F6E537472696E67000000001A00201101000000140000110000000000000000636F6E6E000000001E00201102000000140000110000000000000000647452656C6174696F6E00001E002011030000001400001100000000000000006474526573756C740000000016002011040000001400001100000000000000006463000022002011050000001400001100000000000000006F7574707574537472696E6700000000160020110600000014000011000000000000000073716C001600201107000000140000110000000000000000636D64001E002011080000001400001100000000000000006F757470757441727261790016000311442000000C230000E201000078170000010000001A0020110C00000014000011000000000000000061646170746572001A0020110D000000140000110000000000000000737461636B0000001E0020110E00000014000011000000000000000063757272656E74526F77000016000311FC2100000823000043010000F217000001000000160020111100000014000011000000000000000064720000160003116C22000004230000760000002E180000010000001A0020111300000014000011000000000000000064724F7574000000160003119C22000000230000430000004A18000001000000160020111400000014000011000000000000000069000000020006000200060002000600020006000200060082000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C000000010006000406000354000000011301730180B20180E30181130184810184A10185420185C30285F30286280288730189750189F9018A3C028A59068AA3018AA3028AFB018B97018C13028C0A028E49028DAC0016010000000200060032002A11000000007C24000000000000B203000000000000000000002000000668190000010000000046696C6C526F7700000000160003119C2300001C240000B2030000681900000100000016000311D0230000182400002C030000741900000100000016002011010000001500001100000000000000006472000002000600020006005A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000001F000006040600032C000000020C013D025D0281180281D302828E0283490284040284BF02857A0286350286F000000002000600F20000006C060000A900000001000100B10900000000000087000000600600000000000015000080010000001600008007000000170000801E000000EEEFFE8022000000180000802300000019000080310000001B0000804A000000EEEFFE804E0000001C000080620000001D0000807B000000EEEFFE807F0000001E000080910000001F0000809200000021000080AB00000022000080C400000024000080CA00000025000080D600000026000080D700000027000080DE00000028000080E50000002A000087FD00000033000080080100003400008010010000350000801A0100003600008023010000370000802D0100003900008A34010000450000803E0100004600008048010000480000806A01000049000080730100004B0000807A0100004C0000807D0100004D000080950100004E000080A3010000EEEFFE80A50100004F000080A601000050000080D6010000510000800602000052000080070200004E0000800D0200004E00008017020000EEEFFE801B0200005300008029020000550000802C020000560000802D020000560000803B020000EEEFFE8040020000560000804E020000570000804F020000580000806202000059000080750200005C000080760200005C00008084020000EEEFFE80890200005C000080970200005D000080980200005E000080F7020000EEEFFE80FE0200005F000080FF020000600000801703000061000080390300006200008060030000EEEFFE80640300006200008077030000630000809E030000EEEFFE80A203000063000080B503000065000080D2030000EEEFFE80D903000066000080DA0300006700008015040000EEEFFE8019040000680000808A04000069000080A9040000EEEFFE80AD0400006A0000801E0500006C0000801F0500006D00008041050000EEEFFE80450500006E000080C305000070000080270600007100008028060000730000802E06000074000080340600007500008051060000EEEFFE80580600007600008059060000770000805C0600007800008097060000EEEFFE809E060000790000809F0600007A000080AD060000EEEFFE80B10600007B000081EB0600007E000080230700007F000080290700008000008048070000EEEFFE804F0700008100008050070000820000805E070000EEEFFE8062070000830000809C07000085000080D407000087000080DA07000089000080DB0700008A000080E9070000EEEFFE80ED0700008B000081270800008E0000805F0800008F0000806008000090000080660800009100008067080000920000806808000093000080690800005C00008077080000EEEFFE808C080000EEEFFE808D080000940000808E080000560000809C080000EEEFFE80B1080000EEEFFE80B208000096000080B908000097000080D0080000EEEFFE80D408000098000080FF0800009A00008014090000EEEFFE80180900009B000080570900009D000080940900009F00008097090000EEEFFE80A3090000EEEFFE80A4090000A0000080AE090000A1000080050006000900270009003F000000000009000A000D0032000D0037000000000011008A0012004000000000001100960009000A0009009A000900D40009002C001000510009000A000D0037000D003A000D0032000D0038000D0019000D0023000D003E000D0029000D0060000D0023000D002C000D006C000D002C000D0033000D0028000D003B0012003200000000000D000E0011006F0011006F000D000E003E00430034003C00000000000D0037000D0017000D0014002400360000000000160020000D000E0011003A0011003A001100180029003E00000000001A00250011001200150092000000000015001600190052001900510019004D00000000004E006E00190051000000000052007200190041000000000019001A001D007D00000000002100A50022005300000000002100D6001D001E0021005900000000002500DF00250096001D001E001D00210019001A001E0046000000000019001A001D0032001D007D00000000001D001E00210045000000000025006D002500C4001D001E0022005300000000001D001E00210045000000000025005201250005011D001E001D001E00210045000000000025006D002500B6001D001E001D00210019001A0015001600110012002600280000000000000000000D000E002100230000000000000000000D0031000D00430000000000110067000D004B000000000011009B0011009B0009000A00000000000000000009001D0005000600F2000000780000005A0A0000010001003A00000008000000080000006C0000000000000009000080010000000A0000800C000000EEEFFE800F0000000B000080100000000C000080240000000E000080340000001100008038000000120000800500060009001C000000000009000A000D005B000D0030000900160005000600F20000009C000000940A00000100010051000000080000000B00000090000000000000001600008001000000170000800C000000EEEFFE800F000000180000801000000019000080240000001B00008032000000EEEFFE80350000001C0000804A0000001D0000804B0000001F0000804F000000200000800500060009001C000000000009000A000D005B000D002D00000000001100380009000A000900150005000600F20000009C000000E50A0000010001004C000000080000000B00000090000000000000002400008001000000250000800C000000EEEFFE800F000000260000801000000027000080240000002900008032000000EEEFFE80350000002A000080450000002B000080460000002D0000804A0000002E0000800500060009001C000000000009000A000D005B000D002D00000000001100340009000A000900150005000600F20000003C000000310B000001000100180000000800000003000000300000000000000031000080010000003200008017000000330000800500060009002C0005000600F200000078000000490B0000010001004100000008000000080000006C000000000000003800008001000000390000800C000000EEEFFE800F0000003A000080100000003B000080240000003D0000803B000000400000803F000000410000800500060009001C000000000009000A000D005B000D0043000900150005000600F20000005C0100008A0B000001000100F7000000100000001B00000050010000000000000C000080010000000D000080070000000F00008012000000110000801900000013000090250000002500008049000000260000806D00000028000080740000002A0000807B0000002C00008083000000EEEFFE80870000002D00008088000000EEEFFE808A0000002F0000808B00000030000080A700000031000080A80000002E000080B0000000EEEFFE80B400000032000080B500000034000080BC00000036000080C300000038000080CE000000EEEFFE80D200000039000080D30000003A000080EB0000003D000080F40000003E00008005000600090036000900540009003F000900350009005E0009005F0009001E0009004200090023000000000009000A00000000000D000E00110051000D000E000D0029000000000009000A0009001F0009001F00090021000000000009000A000D004A0009001D0005000600F2000000C8010000810C000001000100430100001800000024000000BC010000000000000B000080010000000C0000801D0000000D000080390000000F0000803A0000000F0000804C000000EEEFFE80510000000F0000805D000000100000805E000000110000806B0000001300008075000000EEEFFE80790000001400008081000000150000808A000000EEEFFE80910000001600008092000000170000809D00000018000080BA0000001A000080BB0000001A000080CB000000EEEFFE80CD0000001A000080DB0000001B000080DC0000001C000080EC000000EEEFFE80F00000001D000080F50000001E000080F60000001A00008001010000EEEFFE8016010000EEEFFE80170100001F0000801801000020000080190100000F00008026010000EEEFFE803A010000EEEFFE803B01000022000080400100002300008005000600090091000900830009001000210042000000000012001D0009000A000D0047000D002F000000000011001D0012003B00000000000D000E001100710011009C001100180032005400000000001A002E001100120015004B000000000019002500110012002F00310000000000000000000D000E0009000A001E00200000000000000000000900160005000600F200000020010000C40D000001000100F400000018000000160000001401000000000000EEEFFE800D000000270000800E0000002800008018000000EEEFFE801E000000EEEFFE802B000000290000802C0000002A000080480000002B000080690000002F00008072000000EEEFFE8076000000EEEFFE80850000003000008086000000310000809600000032000080B800000034000082C700000037000080CA00000039000080CB0000003A000082D90000003D000080DA0000003F000080EC00000042000080F100000043000080000000000500060009001C00000000000000000009000A000D0095000D0087000D00360000000000000000000D000E001100710011009C00110013000D000E000D000E00110013000D000E000D0046000900150005000600F200000060000000B80E00000100010023000000180000000600000054000000000000004600008001000000490000800D0000004A000080160000004B0000801D0000004D000080210000004E000080050006000900380009003900090033000900170005000600F200000084000000DB0E0000010001005300000018000000090000007800000000000000EEEFFE8015000000510000801600000054000080220000005500008030000000570000823D0000005B000080460000005C0000804D0000005E000080510000005F0000800000000005000600090038000900390009000B0009004100090033000900170005000600F2000000B40000002E0F00000100010089000000200000000D000000A8000000000000000B000080010000000C0000801E0000000E00008038000000EEEFFE803B0000000F0000803F0000001000008059000000EEEFFE805C0000001100008060000000120000807B000000EEEFFE807F0000001300008083000000150000808700000016000080050006000900820009003900000000000D00190009004500000000000D00190009003D00000000000D0019000900160005000600F200000078000000B70F0000010001004A00000020000000080000006C000000000000001A000080010000001B0000800C000000EEEFFE800F0000001C000080100000001D0000802D0000001F000080440000002200008048000000230000800500060009001C000000000009000A000D0086000D0043000900150005000600F2000000B4000000011000000100010089000000280000000D000000A8000000000000000B000080010000000C0000801E0000000E00008038000000EEEFFE803B0000000F0000803F0000001000008059000000EEEFFE805C0000001100008060000000120000807B000000EEEFFE807F0000001300008083000000150000808700000016000080050006000900830009003900000000000D00190009004500000000000D00190009003D00000000000D0019000900160005000600F2000000780000008A100000010001004A00000028000000080000006C000000000000001A000080010000001B0000800C000000EEEFFE800F0000001C000080100000001D0000802D0000001F000080440000002200008048000000230000800500060009001C000000000009000A000D0087000D0043000900150005000600F200000048000000D4100000010001002F00000030000000040000003C000000000000000B000080010000000C0000801D0000000E0000802D0000000F0000800500060009007C0009002C0005000600F20000007800000003110000010001004900000030000000080000006C000000000000001300008001000000140000800C000000EEEFFE800F0000001500008010000000160000802C00000018000080430000001B000080470000001C0000800500060009001C000000000009000A000D0080000D0043000900150005000600F2000000480000004C110000010001002E00000038000000040000003C000000000000000B000080010000000C0000801D0000000E0000802C0000000F000080050006000900820009002C0005000600F2000000780000007A110000010001004700000038000000080000006C000000000000001300008001000000140000800B000000EEEFFE800E000000150000800F000000160000802B00000018000080410000001B000080450000001C0000800500060009001C000000000009000A000D0086000D0043000900150005000600F200000074010000C111000001000100E9000000400000001D00000068010000000000000B000080010000000C0000800C000000EEEFFE80120000000D000080130000000E0000802F000000100000803D000000EEEFFE804000000011000080470000001200008050000000EEEFFE805700000013000080580000001400008063000000150000808000000017000080810000001700008096000000EEEFFE809800000017000080A600000018000080A700000019000080B7000000EEEFFE80BB0000001A000080BF0000001B000080C000000017000080CB000000EEEFFE80E0000000EEEFFE80E10000001C000080E20000001D000080E30000001F000080E7000000200000800500060009001C000000000009000A000D008A000D0034000000000011001D0012003B00000000000D000E001100710011009C001100180032005600000000001A002E001100120015004B000000000019002500110012002F00310000000000000000000D000E0009000A000900160005000600F200000008010000AA12000001000100C90000004000000014000000FC00000000000000EEEFFE800D000000240000800E0000002500008019000000EEEFFE801F0000002600008020000000270000802800000029000080440000002B000080570000002D00008060000000EEEFFE8064000000EEEFFE80730000002E000080740000002F0000807F00000030000080A100000032000082B000000036000080BB00000037000080BC00000039000080C10000003C000080C60000003D000080000000000500060009001C000000000009000A000D0028000D008A000D0042000D00360000000000000000000D000E001100710011009C0011001300110045000D000E000D0019000900150005000600F20000003C000000731300000100010013000000400000000300000030000000000000004000008001000000410000801100000042000080050006000900460005000600F2000000F00000008613000001000100890000004800000012000000E4000000000000000C000080010000000D0000800B0000000F0000801F00000011000080200000001100008032000000EEEFFE803400000011000080400000001200008041000000130000804E0000001500008058000000EEEFFE805C00000016000080610000001700008062000000110000806C000000EEEFFE8080000000EEEFFE808100000019000080860000001A00008005000600090070000900540009001000210042000000000012001D0009000A000D0047000D002F000000000011001D0009000A001E00200000000000000000000900160005000600F2000000440100000F140000010001000E010000480000001900000038010000000000001E000080010000001F0000800B000000EEEFFE80110000002000008012000000210000801C000000230000803000000025000080360000002600008039000000280000803A000000280000804D000000EEEFFE804F000000280000805D000000290000805E0000002A0000806C0000002B000080820000002D000080A70000002F000080B800000030000080B900000028000080C4000000EEEFFE80D9000000EEEFFE80DA00000032000080FC0000003400008006010000370000800B010000380000800500060009001C000000000009000A000D0074000D0058000D003B000D001E000D0014002500460000000000160021000D000E0011004B0011006A0011006800110037000D000E002200240000000000000000000D0060000D0029000900150005000600F2000000C80400001D150000010001004B0400005000000064000000BC0400000000000015000080010000001700008007000000180000800E00000019000080140000001A0000801A0000001B000080300000001C0000803E0000001D000080540000001E000080620000001F000080780000002000008086000000210000809C00000022000080AA00000023000080C000000024000080CE00000025000080E400000026000080F200000027000080080100002800008016010000290000802C0100002A0000803A0100002B000080500100002C0000805E0100002D000080740100002E00008082010000310000808F010000320000809901000033000080A001000034000080AE01000035000080C201000036000080CB010000EEEFFE80D201000037000080D301000038000080DF010000EEEFFE80E301000039000080E40100003A000080050200003B0000800F0200003C0000801D0200003D000080310200003E000080340200004000008035020000410000804A020000420000804B0200004300008054020000EEEFFE805B020000440000805C0200004500008070020000460000807A0200004700008083020000480000808C02000049000080930200004A000080A50200004C000080BB020000EEEFFE80C20200004D000080C30200004D000080D0020000EEEFFE80D50200004D000080E30200004E000080E40200004F0000800D030000EEEFFE80110300005000008012030000510000801C0300005200008025030000530000802D0300005400008030030000EEEFFE8032030000550000803303000056000080580300005700008059030000540000805F030000540000806C030000EEEFFE8070030000580000807E03000059000080860300005A0000808C0300005C0000808D030000EEEFFE808F0300005E000080900300005F0000809E03000060000080EB030000EEEFFE80EF03000061000080F103000063000080F203000064000080FA03000065000080FB03000066000080FC0300005D00008008040000EEEFFE800C04000068000080160400006A000080170400006B000080180400004D00008026040000EEEFFE803B040000EEEFFE803C0400006C0000803D0400006D0000803E0400006E000080480400006F0000800500060009003600090042000900300009002E0009003E000900220009003300090022000900330009002200090033000900220009003300090022000900330009002200090033000900220009003300090022000900330009002200090034000900220009009C0009003400090015000900370009003800090025000000000009000A000D002200000000000D000E001100E0001100270011003F00110037000D000E000D000E00110058000D000E000D002900000000000D000E0011008D00110027001100420011002A0011001E0011004000110045000000000015001C002C003B00000000001E00280015001600190046000000000019001A001D002C001D003E001D003C0022002B00000000001D001E00210064001D001E00400043002D003E00000000001D003B001D00290019001A0019001A00000000001D001E0021004400210080000000000025002B002100220025003100210022001D001E001D003400000000001D002C0019001A001500160029002B0000000000000000000D000E0009000A0009001E0005000600F2000000DC0200006819000001000100B2030000500000003B000000D00200000000000073000080010000007400008006000000EEEFFE800C000000750000800D0000007600008014000000770000803A000000EEEFFE803D00000078000080560000007A000080610000007C00008087000000EEEFFE808A0000007D000080A30000007F000080AE00000081000080D5000000EEEFFE80D900000082000080F200000084000080FD0000008600008024010000EEEFFE80280100008700008042010000890000804E0100008B00008075010000EEEFFE80790100008C000080930100008E0000809F01000090000080C6010000EEEFFE80CA01000091000080E401000093000080F00100009500008017020000EEEFFE801B020000960000803502000098000080410200009A00008068020000EEEFFE806C0200009B000080860200009D000080920200009F000080B9020000EEEFFE80BD020000A0000080D7020000A2000080E3020000A40000800C030000EEEFFE8010030000A50000802B030000A700008037030000A80000803A030000AA0000803B030000AB00008046030000AC00008051030000AD0000805C030000AE00008068030000AF00008074030000B000008080030000B10000808C030000B200008098030000B3000080A4030000B4000080B0030000B5000080B1030000B600008005000600090019000000000009000A000D0027000D0052000000000011002D00110025000D0052000000000011002D00110025000D0052000000000011002D00110025000D0052000000000011002D00110025000D0052000000000011002D00110025000D0052000000000011002D00110025000D0052000000000011002D00110025000D0052000000000011002D00110025000D0052000000000011002D00110025000D0052000000000011002E001100260009000A0009000A000D0021000D0021000D0021000D0021000D0021000D0021000D0021000D0021000D0021000D00220009000A0005000600F400000058000000B8000000000000007C0100000000000078020000000000006E03000000000000720400000000000070050000000000007006000000000000620700000000000060080000000000005809000000000000560A000000000000D000000000000000240000003C000000580000007000000090000000A8000000C8000000E00000000801000020010000440100005C0100008C010000A4010000DC010000F401000038020000500200008002000098020000C8020000E0020000140300002C0300006003000078030000AC030000C4030000FC03000014040000440400005C0400008C040000A4040000D8040000F004000028050000400500007005000088050000C0050000D8050000FC050000140600004C06000064060000A4060000BC060000DC060000F40600000C07000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000400000036002A1100000000900100000000000065000000000000000000000079000006498C00000100000000526567657853656C656374416C6C0016000311040000004401000065000000498C0000010000000A0024115553797374656D001E0024115553797374656D2E436F6C6C656374696F6E732E47656E6572696300120024115553797374656D2E54657874000000001A0024115553797374656D2E446174612E53716C54797065730000001E002411554D6963726F736F66742E53716C5365727665722E53657276657200220024115553797374656D2E546578742E526567756C617245787072657373696F6E73001A0020110000000048000011000000000000000072656765780000001A00201101000000480000110000000000000000726573756C7473001600201102000000480000110000000000000000736200000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C0000000100060004060001180000000116014E0180890281870280B0160100020006002E002A1100000000F4010000000000000800000000000000000000007B000006AE8C000001000000002E6363746F72002E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004010000040100000C0000007900000602000600F2000000F0000000498C000001000100650000000000000012000000E4000000000000001700008001000000180000800D00000019000080150000001B0000801B000000EEEFFE801D0000001D0000801E0000001F0000802B00000021000080320000002400008039000000EEEFFE803C000000250000803D0000002600008045000000270000804600000028000080470000001C0000804F000000EEEFFE80530000002A000080620000002C00008009000A000D0037000D0030000D003400000000000D000E0011002A0011002F0011002500000000001100120015002F00110012000D000E000D002400000000000D00310009000A00F200000024000000AE8C00000100010008000000000000000100000018000000000000000B00008009008F00F400000008000000800E0000000000001000000024070000440700005C07000074070000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000002E002A110000000030010000000000000D000000000000000000000001000006000000000100000000496E69740000001600031104000000FC0000000D00000000000000010000000A0024115553797374656D00120024115553797374656D2E44617461000000001A0024115553797374656D2E446174612E53716C436C69656E7400001A0024115553797374656D2E446174612E53716C54797065730000001E002411554D6963726F736F66742E53716C5365727665722E53657276657200120024115553797374656D2E5465787400000000220024115553797374656D2E52756E74696D652E53657269616C697A6174696F6E000000020006002E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004010000040000000C000000010007000200060032002A1100000000A401000000000000280000000000000000000000020000060D0000000100000000416363756D756C617465003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600020C000000020C0000020006002E002A11000000000802000000000000140000000000000000000000030000063500000001000000004D6572676500002E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004010000040100000C000000010000060200060032002A11000000007C02000000000000390000000000000000000000040000064900000001000000005465726D696E61746500003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000001000006040600020C00000016010000020006002E002A1100000000E00200000000000013000000000000000000000005000006820000000100000000526561640000002E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004010000040100000C00000001000006020006002E002A1100000000440300000000000014000000000000000000000006000006950000000100000000577269746500002E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004010000040100000C0000000100000602000600F20000003C00000000000000010001000D000000000000000300000030000000000000001500008001000000160000800C00000017000080050006000900260005000600F2000000540000000D0000000100010028000000000000000500000048000000000000001E000080010000001F0000800C000000EEEFFE800F0000002000008027000000210000800500060009001B00000000000D00420005000600F20000003C00000035000000010001001400000000000000030000003000000000000000280000800100000029000080130000002A000080050006000900250005000600F200000054000000490000000100010039000000000000000500000048000000000000003100008001000000320000801300000033000080240000003400008037000000350000800500060009003700090035000900310005000600F20000003C000000820000000100010013000000000000000300000030000000000000004A000080010000004B000080120000004C000080050006000900340005000600F20000003C000000950000000100010014000000000000000300000030000000000000005400008001000000550000801300000056000080050006000900240005000600F4000000080000000100000000000000300000008C070000A0070000B8070000D4070000EC070000000800001808000030080000480800005C080000740800008808000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000004A002A110000000050020000000000008B0000000000000000000000220000061A1D00000100000000436F6E746163744C6973745F557064617465436F6E74616374436F756E74434C5200001600031104000000000200008B0000001A1D0000010000001E002411554D6963726F736F66742E53716C5365727665722E536572766572000A0024115553797374656D001E0024115553797374656D2E436F6C6C656374696F6E732E47656E6572696300120024115553797374656D2E44617461000000001A0024115553797374656D2E446174612E53716C436C69656E7400001A0024115553797374656D2E446174612E53716C5479706573000000120024115553797374656D2E54657874000000000E0024115553797374656D2E586D6C002200201100000000160000110000000000000000636F6E746163744C69737449640000001600031150000000FC0100005E000000461D0000010000001A0020110300000016000011000000000000000073746174757300001E00201104000000160000110000000000000000736F72744F726465720000002200201105000000160000110000000000000000696E74546F74616C5265636F726473001600031148010000F801000050000000531D0000010000001E00201106000000160000110000000000000000636F6E6E656374696F6E00000200060002000600020006004A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C00000001000800040600011C000000011002370280A50180D20180F201811D01815500020006003A002A11000000000004000000000000EA000000000000000000000023000006A51D00000100000000436F6E746163745F536561726368434C5200001600031154020000AC030000EA000000A51D0000010000002600201100000000170000110000000000000000696E636C7564654164647265737356616C7565002200201101000000170000110000000000000000636F6E746163744C69737449640000001600031190020000A80300008A000000041E0000010000001A0020110500000017000011000000000000000073746174757300001E00201106000000170000110000000000000000736F72744F726465720000002200201107000000170000110000000000000000696E74546F74616C5265636F7264730016000311F4020000A40300007B000000121E0000010000001E00201108000000170000110000000000000000636F6E6E656374696F6E00000200060002000600020006004E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600002000000001100139025E0280D40281970181C40181E401820F0182470200060056002A11000000007805000000000000870000000000000000000000240000068F1E00000100000000436F6E746163745F4765744175746F446973747269627574696F6E4C697374436F6E74616374436F756E740000000016000311040400002C050000870000008F1E000001000000160003115C040000280500006C000000A91E0000010000001A0020110100000018000011000000000000000073746174757300001E00201102000000180000110000000000000000736F72744F726465720000002200201103000000180000110000000000000000696E74546F74616C5265636F7264730016000311740400002405000054000000B41E0000010000001E00201104000000180000110000000000000000636F6E6E656374696F6E000002000600020006000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000318000000023501620180820180AD0180E5000000020006003A002A1100000000540600000000000044000000000000000000000025000006161F00000100000000476574436F756E744279537461747573000000160003117C0500001006000044000000161F0000010000002200201100000000190000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000190000110000000000000000706172616D310000020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000210000000011B015A16010000020006003A002A110000000050070000000000006C0000000000000000000000260000065A1F00000100000000457865637574655369746547726F75700000001600031158060000080700006C0000005A1F00000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813416010000000200060036002A110000000048080000000000006C000000000000000000000027000006C61F000001000000004578656375746553697465496473001600031154070000000800006C000000C61F00000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813116010000000200060046002A110000000030090000000000004500000000000000000000002800000632200000010000000045786563757465427943757272656E7453697465436F6E7465787400000000160003114C080000EC0800004500000032200000010000002200201100000000190000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000190000110000000000000000706172616D310000020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000210000000011B015A16010000020006003E002A1100000000100A0000000000004500000000000000000000002900000677200000010000000045786563757465436F6E74616374436F756E74000000001600031134090000CC0900004500000077200000010000002200201100000000190000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000190000110000000000000000706172616D310000020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000210000000011B015A16010000020006003E002A1100000000100B0000000000006C00000000000000000000002A000006BC2000000100000000457865637574655369746541747472696275746573000016000311140A0000C80A00006C000000BC2000000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A0181381601000000020006003E002A1100000000C00E0000000000007909000000000000000000002B000006282100000100000000526574726965766556616C75657346726F6D584D4C000016000311140B0000200E00007909000028210000010000001E002011000000001B0000110000000000000000646F63756D656E740000000022002011010000001B00001100000000000000006F7065726174696F6E586D6C0000000016000311540B00001C0E0000030900005D210000010000001E002011030000001B00001100000000000000004F7065726174696F6E00000016000311B00B0000180E0000FB08000065210000010000001A002011040000001B00001100000000000000006E6F64657300000016000311E80B0000140E0000AA080000B5210000010000001E002011070000001B0000110000000000000000775365617263684974656D0022002011080000001B0000110000000000000000636F6E74616374536F7572636573000022002011090000001B000011000000000000000066735365617263684974656D00000000220020110A0000001B0000110000000000000000706C5365617263684974656D000000001E0020110B0000001B0000110000000000000000705365617263684974656D00220020110C0000001B00001100000000000000006F735365617263684974656D00000000220020110D0000001B00001100000000000000006D696E696D756D56616C756500000000220020110E0000001B00001100000000000000006D6178696D756D56616C756500000000220020110F0000001B00001100000000000000006F635365617263684974656D0000000022002011100000001B00001100000000000000006D696E696D756D436F756E740000000022002011110000001B00001100000000000000006D6178696D756D436F756E740000000022002011120000001B0000110000000000000000704162616E646F6E6564436172740000160003111C0C0000100E00001400000066260000010000001A002011170000001B00001100000000000000006E6F64650000000002000600020006000200060002000600020006009A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600026C0000000118016C0680CF0180CF0181A2028122028226018BDB018FBD01936D0194770195F70197790197C00198F6019BBD019C01019D2F019FE324828B02828B00028DE7018E3C0294B40296320297EB02993A029A56029C2C029D6F029E8302A02602A3610000020006003A002A11000000009C0F0000000000004500000000000000000000002C000006A12A000001000000004765744D616E75616C436F6E7461637473000016000311C40E0000580F000045000000A12A0000010000002200201100000000190000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000190000110000000000000000706172616D310000020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000210000000011B015A160100000200060046002A1100000000E410000000000000C600000000000000000000002D000006E62A0000010000000045786563757465417474726962757465436F6E74616374536561726368000016000311A00F000094100000C6000000E62A00000100000022002011000000001C0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001C0000110000000000000000736F7572636500001A002011020000001C0000110000000000000000706172616D3100001A002011030000001C0000110000000000000000706172616D3200001A002011040000001C0000110000000000000000706172616D330000020006004A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600031C000000011B015201818E018277018352027C1601000000020006003E002A1100000000E4110000000000006C00000000000000000000002E000006AC2B000001000000004578656375746550726F66696C6553656172636800000016000311E81000009C1100006C000000AC2B00000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813A1601000000020006003A002A1100000000E0120000000000007100000000000000000000002F000006182C0000010000000045786563756974655369746547726F7570000016000311E81100009812000071000000182C00000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000214000000011B018160018247160100000200060036002A110000000098140000000000006A010000000000000000000030000006892C000001000000004765745369746547726F757073000016000311E41200003C1400006A010000892C0000010000001E002011000000001D00001100000000000000007365617263684974656D0000160003111C130000381400002A0100009F2C0000010000001A002011020000001D00001100000000000000006E6F6465000000001600031154130000341400001E010000AB2C0000010000001E002011030000001D000011000000000000000073656C6563746564496473001600031188130000301400002E0000006E2D0000010000001E002011080000001D00001100000000000000006964537472696E670000000016000311C01300002C14000014000000872D0000010000001A0020110A0000001D00001100000000000000007761746368496400020006000200060002000600020006000200060056000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000128000000012706610161018180000281AF07823A09823A01823A0282AF0182FF001601000200060036002A1100000000641500000000000059000000000000000000000031000006F32D0000010000000057726974654F757470757400000000160003119C1400002015000059000000F32D0000010000001A002011000000001E0000110000000000000000717565727900000016002011010000001E0000110000000000000000636D6400020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000002200000604060003100000000113018084000000020006003E002A110000000064160000000000006C0000000000000000000000320000064C2E0000010000000045786563757465436F6E74616374536F7572636500000016000311681500001C1600006C0000004C2E00000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813816010000000200060042002A11000000003C170000000000002D000000000000000000000033000006B82E0000010000000043726561746554656D706F726172794F75747075745461626C65001600031168160000F81600002D000000B82E0000010000001A002011000000001E0000110000000000000000717565727900000016002011010000001E0000110000000000000000636D6400020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600031000000001130181830000000200060042002A110000000010180000000000002D000000000000000000000034000006E52E0000010000000044726F7054656D706F726172794F75747075745461626C650000001600031140170000D01700002D000000E52E0000010000001A002011000000001E0000110000000000000000717565727900000016002011010000001E0000110000000000000000636D6400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600000C000000011301560200060046002A1100000000081900000000000037000000000000000000000035000006122F0000010000000045786563757465476574436F6E74616374536F75726365436F756E740000001600031114180000C018000037000000122F00000100000016002011000000001F0000110000000000000000636D64001A002011010000001F00001100000000000000006F7574707574000016002011020000001F0000110000000000000000786D6C000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000214000000011701822901825916010000020006003A002A1100000000701A00000000000086010000000000000000000036000006492F0000010000000045786563757465476574436F6E746163747300160003110C190000201A000086010000492F0000010000001A00201100000000200000110000000000000000706172616D3200001A00201101000000200000110000000000000000706172616D3300001A00201102000000200000110000000000000000706172616D3400001A00201103000000200000110000000000000000706172616D3500001A00201104000000200000110000000000000000706172616D3600001A00201105000000200000110000000000000000706172616D3700001600201106000000200000110000000000000000636D6400020006004A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600001C00000001190180DE0181880182570183090183C3018477020006003A002A1100000000A81B000000000000D7000000000000000000000037000006CF30000001000000004578656375746550757263686173650000000016000311741A00005C1B0000D7000000CF300000010000002200201100000000210000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000210000110000000000000000706172616D3000001A00201102000000210000110000000000000000706172616D3100001A00201103000000210000110000000000000000706172616D3200001A00201104000000210000110000000000000000706172616D3300000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000118000000011B015A01815401823801831A160100020006003A002A1100000000001D00000000000013010000000000000000000038000006A63100000100000000457865637574654F7264657253697A6500000016000311AC1B0000B01C000013010000A6310000010000002200201100000000220000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000220000110000000000000000706172616D3000001A00201102000000220000110000000000000000706172616D3100001A00201103000000220000110000000000000000706172616D3200001A00201104000000220000110000000000000000706172616D3300001A00201105000000220000110000000000000000706172616D340000020006004A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600021C000000011B015A0181450182300183180183FC16010000020006003A002A1100000000581E00000000000011010000000000000000000039000006B93200000100000000457865637574654F72646572436F756E74000016000311041D0000081E000011010000B9320000010000002200201100000000220000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000220000110000000000000000706172616D3000001A00201102000000220000110000000000000000706172616D3100001A00201103000000220000110000000000000000706172616D3200001A00201104000000220000110000000000000000706172616D3300001A00201105000000220000110000000000000000706172616D340000020006004A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600021C000000011B015A01814301822C0183140183F816010000020006003E002A1100000000741F0000000000009D00000000000000000000003A000006CA3300000100000000457865637574654162616E646F6E656443617274000000160003115C1E00002C1F00009D000000CA330000010000002200201100000000230000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000230000110000000000000000706172616D3100001A00201102000000230000110000000000000000706172616D3200001A00201103000000230000110000000000000000706172616D3300000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000014000000011B015A01813E0182201601020006003A002A11000000008C200000000000009D00000000000000000000003B000006673400000100000000457865637574654C6173744C6F67696E00000016000311781F0000442000009D00000067340000010000002200201100000000230000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000230000110000000000000000706172616D3100001A00201102000000230000110000000000000000706172616D3200001A00201103000000230000110000000000000000706172616D3300000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000014000000011B015A01813E01822016010200060036002A1100000000A021000000000000A800000000000000000000003C000006043500000100000000457865637574655761746368657300160003119020000058210000A800000004350000010000002200201100000000230000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000230000110000000000000000706172616D3000001A00201102000000230000110000000000000000706172616D3100001A00201103000000230000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000014000000011B015A0181530182371601020006003E002A1100000000BC22000000000000A800000000000000000000003D000006AC350000010000000045786563757465466F726D5375626D697474656400000016000311A421000074220000A8000000AC350000010000002200201100000000230000110000000000000000706172616D436F6C6C656374696F6E001A00201101000000230000110000000000000000706172616D3000001A00201102000000230000110000000000000000706172616D3100001A00201103000000230000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000014000000011B015A01815301823416010200060042002A1100000000C0230000000000006C00000000000000000000003E0000065436000001000000004578656375746550726F647563747350757263686173656400000016000311C0220000782300006C000000543600000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813416010000000200060046002A1100000000C8240000000000006C00000000000000000000003F000006C036000001000000004578656375746550726F64756374547970657350757263686173656400000016000311C4230000802400006C000000C03600000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813816010000000200060042002A1100000000CC250000000000006C0000000000000000000000400000062C370000010000000045786563757465576562736974655573657253656172636800000016000311CC240000842500006C0000002C3700000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813E16010000000200060036002A1100000000C4260000000000006C00000000000000000000004100000698370000010000000045786563757465576174636865730016000311D02500007C2600006C000000983700000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A0181341601000000020006003E002A1100000000C4270000000000006C00000000000000000000004200000604380000010000000045786563757465437573746F6D657247726F757073000016000311C82600007C2700006C000000043800000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813B1601000000020006003E002A1100000000C4280000000000006C0000000000000000000000430000067038000001000000004578656375746553656375697274794C6576656C73000016000311C82700007C2800006C000000703800000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813E1601000000020006003A002A1100000000C0290000000000006C000000000000000000000044000006DC380000010000000045786563757465496E6465785465726D73000016000311C8280000782900006C000000DC3800000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813816010000000200060042002A1100000000C42A0000000000006C00000000000000000000004500000648390000010000000045786563757465446973747269627574696F6E4C6973740000000016000311C42900007C2A00006C000000483900000100000022002011000000001A0000110000000000000000706172616D436F6C6C656374696F6E001A002011010000001A0000110000000000000000706172616D3100001A002011020000001A0000110000000000000000706172616D3200000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011B015A01813D16010000000200060036002A1100000000D82B00000000000070000000000000000000000046000006B43900000100000000457865637574655363616C6172000016000311C82A0000882B000070000000B4390000010000001E0020110000000024000011000000000000000064744F7574707574000000001600201101000000240000110000000000000000636D640016000311002B0000842B000020000000E8390000010000001A00201104000000240000110000000000000000706172610000000002000600020006004A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600021C0000000116014807810C09810C01810C02814E16010000020006003A002A1100000000582D0000000000008C000000000000000000000047000006243A0000010000000045786563757465446174615461626C6500000016000311DC2B0000002D00008C000000243A0000010000001E0020110000000025000011000000000000000064744F7574707574000000002600201101000000250000110000000000000000636F6E6E636574696F6E537472696E67000000001A00201102000000250000110000000000000000636F6E6E000000001600201103000000250000110000000000000000636D64001A00201104000000250000110000000000000000616461707465720016000311182C0000FC2C000022000000673A0000010000001A002011070000002500001100000000000000007061726100000000020006000200060052000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000124000000011601420180870180C901822D07818B09818B01818B0281CD160100020006003A002A1100000000C02E000000000000A5000000000000000000000048000006B03A00000100000000476574436F6E66696775726174696F6E000000160003115C2D00006C2E0000A5000000B03A0000010000001A002011000000002600001100000000000000007374724C697374001E0020110100000026000011000000000000000064744F757470757400000000160020110200000026000011000000000000000073716C001600201103000000260000110000000000000000636D64001A00201104000000260000110000000000000000616461707465720016000311982D0000682E000028000000EC3A00000100000016002011060000002600001100000000000000006472000002000600020006004E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000002200000604060003200000000119014A01780180E101811C06816B01816B0016010000000200060032002A11000000008C3200000000000026050000000000000000000049000006553B000001000000006765744E6F64657300000016000311C42E00000832000026050000553B00000100000016000311F82E00000432000022050000563B0000010000002200201100000000270000110000000000000000736974654174747269627574657300002200201101000000270000110000000000000000636F6E74616374536F757263657300001E002011020000002700001100000000000000007369746547726F75707300001A0020110300000027000011000000000000000073697465496473002600201104000000270000110000000000000000646973747269627574696F6E4C697374000000001E00201105000000270000110000000000000000696E6465785465726D730000220020110600000027000011000000000000000073656375726974794C6576656C7300002200201107000000270000110000000000000000637573746F6D657247726F75707300001A002011080000002700001100000000000000007761746368657300260020110900000027000011000000000000000077656273697465557365725365617263680000002A0020110A000000270000110000000000000000636F6E746163744174747269627574655365617263680000260020110B00000027000011000000000000000070726F64756374735075726368617365640000002A0020110C00000027000011000000000000000070726F647563745479706573507572636861736564000000220020110D000000270000110000000000000000666F726D5375626D69747465640000001E0020110E0000002700001100000000000000006C6173744C6F67696E0000001E0020110F0000002700001100000000000000007075726368617365000000001E002011100000002700001100000000000000006F7264657253697A650000001E002011110000002700001100000000000000006F72646572436F756E74000022002011120000002700001100000000000000006162616E646F6E6564436172740000001A0020111300000027000011000000000000000073746174757300001E00201114000000270000110000000000000000736F72744F7264657200000002000600020006007E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000350000000016A0181300181E201828601832A0183E301848E0185440185F40186A801877E01884B01891A0189ED018AAB018B48018BE3018C82018D27018DCE018E63240C020C0016010000000200060032002A11000000007C330000000000005500000000000000000000004A0000067B40000001000000004765744775696473000000160003119032000034330000550000007B400000010000001E00201100000000280000110000000000000000677569644C6973740000000016000311C432000030330000200000008C400000010000001A002011020000002800001100000000000000006E6F646500000000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000002200000604060003140000000117063D013D0016010000000200060032002A110000000068340000000000005500000000000000000000004B000006D04000000100000000476574496E74730000000016000311803300002034000055000000D0400000010000001A00201100000000290000110000000000000000696E744C6973740016000311B43300001C34000020000000E1400000010000001A002011020000002900001100000000000000006E6F646500000000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000002200000604060003140000000116063A013A0016010000000200060032002A11000000001C360000000000006A01000000000000000000004C0000062541000001000000004765745761746368657300160003116C340000C03500006A01000025410000010000001E002011000000001D00001100000000000000007365617263684974656D000016000311A0340000BC3500002A0100003B410000010000001A002011020000001D00001100000000000000006E6F64650000000016000311D8340000B83500001E01000047410000010000001E002011030000001D000011000000000000000073656C656374656449647300160003110C350000B43500002E0000000A420000010000001E002011080000001D00001100000000000000006964537472696E67000000001600031144350000B03500001400000023420000010000001A0020110A0000001D00001100000000000000007761746368496400020006000200060002000600020006000200060056000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000128000000012706610161018180000281AF07823A09823A01823A0282AF0182FF00160100020006003A002A1100000000D8370000000000006A01000000000000000000004D0000068F4200000100000000476574466F726D5375626D697474656400000016000311203600007C3700006A0100008F420000010000001E002011000000001D00001100000000000000007365617263684974656D0000160003115C360000783700002A010000A5420000010000001A002011020000001D00001100000000000000006E6F6465000000001600031194360000743700001E010000B1420000010000001E002011030000001D000011000000000000000073656C65637465644964730016000311C8360000703700002E00000074430000010000001E002011080000001D00001100000000000000006964537472696E670000000016000311003700006C370000140000008D430000010000001A0020110A0000001D0000110000000000000000666F726D49640000020006000200060002000600020006000200060056000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000128000000012706610161018180000281AF07823A09823A01823A0282AF0182FF001601000200060036002A1100000000CC380000000000004D01000000000000000000004E000006F943000001000000004765744C6173744C6F67696E00000016000311DC370000843800004D010000F9430000010000001E002011000000002A00001100000000000000007365617263684974656D00001600031114380000803800000D0100000F440000010000001A002011020000002A00001100000000000000006E6F646500000000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000214000000011F064F014F0000160100000200060036002A1100000000C039000000000000A401000000000000000000004F00000646450000010000000047657450757263686173650000000016000311D038000078390000A401000046450000010000001E002011000000002B00001100000000000000007365617263684974656D0000160003110839000074390000640100005C450000010000001A002011020000002B00001100000000000000006E6F646500000000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000114000000011F064F014F0000001601000200060036002A1100000000C43A000000000000ED010000000000000000000050000006EA46000001000000004765744F7264657253697A6500000016000311C4390000703A0000ED010000EA4600000100000022002011000000002C000011000000000000000073656172636856616C7565730000000016000311FC3900006C3A0000AD01000000470000010000001A002011020000002C00001100000000000000006E6F64650000000002000600020006004E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000002200000604060003200000000124065D015D0280950281CC0282FE0284270016010000000200060036002A1100000000C83B000000000000E9010000000000000000000051000006D748000001000000004765744F72646572436F756E74000016000311C83A0000743B0000E9010000D74800000100000022002011000000002D000011000000000000000073656172636856616C7565730000000016000311003B0000703B0000A9010000ED480000010000001A002011020000002D00001100000000000000006E6F64650000000002000600020006004E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000002200000604060003200000000126066101610280990281BB0282ED028416001601000000020006003A002A1100000000C03C0000000000004D010000000000000000000052000006C04A000001000000004765744162616E646F6E65644361727400000016000311CC3B0000783C00004D010000C04A0000010000001E002011000000002A00001100000000000000007365617263684974656D000016000311083C0000743C00000D010000D64A0000010000001A002011020000002A00001100000000000000006E6F646500000000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000214000000011F064F014F000016010000020006003E002A1100000000F83D000000000000B90000000000000000000000530000060D4C0000010000000047657450726F70657274795365617263684974656D000016000311C43C0000B03D0000B90000000D4C0000010000001E002011000000002E00001100000000000000007365617263684974656D730016000311043D0000AC3D00007E0000001E4C0000010000001A002011020000002E00001100000000000000006E6F646500000000160003113C3D0000A83D0000720000002A4C0000010000001E002011030000002E00001100000000000000007365617263684974656D000002000600020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000002200000604060000140000000125065C015C0180AA0016010200060036002A1100000000E43E0000000000007D000000000000000000000054000006C64C00000100000000476574586D6C466F7247756964730016000311FC3D00009C3E00007D000000C64C0000010000001A002011000000002F0000110000000000000000737472586D6C000016000311343E0000983E000026000000E84C00000100000016002011020000002F000011000000000000000064720000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011A067701770016010000000200060036002A1100000000D03F0000000000007B000000000000000000000055000006434D00000100000000476574586D6C466F7247756964730016000311E83E0000883F00007B000000434D0000010000001A00201100000000300000110000000000000000737472586D6C000016000311203F0000843F000023000000684D000001000000160020110300000030000011000000000000000069640000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000014000000011A027706809A01809A16010200060036002A1100000000484000000000000011000000000000000000000056000006BE4D00000100000000476574586D6C466F724E6F646573003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C00000022000006040600020C000000160100000200060036002A110000000038410000000000006F000000000000000000000057000006CF4D00000100000000476574586D6C466F724E6F64657300160003114C400000F04000006F000000CF4D0000010000001A00201100000000310000110000000000000000737472586D6C00001600031184400000EC4000001B000000ED4D0000010000001A002011020000003100001100000000000000006E6F646500000000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011A06720172001601000000020006003A002A110000000054420000000000005F0000000000000000000000580000063E4E00000100000000476574436F6E74616374536F75726365730000160003113C410000084200005F0000003E4E0000010000002200201100000000320000110000000000000000636F6E74616374536F757263657300001A002011010000003200001100000000000000006E6F6465730000001600031178410000044200001B0000005B4E0000010000001A002011030000003200001100000000000000006E6F646500000000020006000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000002200000604060003180000000119015306808A01808A0016010000000200060032002A11000000003C43000000000000600000000000000000000000590000069D4E0000010000000047657447756964730000001600031158420000F4420000600000009D4E0000010000001A002011000000003300001100000000000000006775696473000000160003118C420000F042000026000000B34E000001000000160020110200000033000011000000000000000064720000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000002200000604060003140000000117063A013A0016010000000200060046002A110000000038440000000000005C00000000000000000000005A000006FD4E00000100000000476574586D6C466F7250726F70657274795365617263684974656D000000001600031140430000F04300005C000000FD4E0000010000001600201100000000340000110000000000000000646F63001600031188430000EC430000160000001A4F0000010000001A002011020000003400001100000000000000006E6F646500000000020006000200060042000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C000000220000060406000314000000011806730173001601000000020006003E002A1100000000B0450000000000003701000000000000000000005B000006594F00000100000000476574436F6E74656E74446566696E6974696F6E730000160003113C4400006445000037010000594F0000010000000A0024115553797374656D001A0024115553797374656D2E446174612E53716C5479706573000000120024115553797374656D2E5465787400000000220024115553797374656D2E546578742E526567756C617245787072657373696F6E7300160024115546696E64416E645265706C616365434C5200001A0020110000000035000011000000000000000072656765780000001A0020110100000035000011000000000000000066696C74657200001E00201102000000350000110000000000000000636F6D6D616E6454657874000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C0000000100050004060001180000000112017501810802808F00000282FE000200060042002A11000000007046000000000000A000000000000000000000005C000006905000000100000000557064617465436F6E74656E74446566696E6974696F6E7300000016000311B445000030460000A000000090500000010000001E00201100000000360000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600020C000000011A0000020006003A002A11000000002847000000000000C900000000000000000000005D00000630510000010000000047657446696C6550726F7065727469657300001600031174460000E8460000C900000030510000010000001E00201100000000370000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600000C000000011A0000020006003E002A1100000000E4470000000000001601000000000000000000005E000006F9510000010000000055706461746546696C6550726F70657274696573000000160003112C470000A447000016010000F9510000010000001E00201100000000360000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600020C000000011A0000020006003A002A11000000009C48000000000000C900000000000000000000005F0000060F5300000100000000476574496D61676550726F706572746965730016000311E84700005C480000C90000000F530000010000001E00201100000000370000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600000C000000011A0000020006003E002A1100000000584900000000000016010000000000000000000060000006D85300000100000000557064617465496D61676550726F70657274696573000016000311A04800001849000016010000D8530000010000001E00201100000000360000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600020C000000011A00000200060036002A11000000000C4A000000000000BE000000000000000000000061000006EE5400000100000000476574506167654E616D6573000000160003115C490000CC490000BE000000EE540000010000001E00201100000000370000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600000C000000011A0000020006003A002A1100000000C44A0000000000008C000000000000000000000062000006AC5500000100000000557064617465506167654E616D65730000000016000311104A0000844A00008C000000AC550000010000001E00201100000000360000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600020C000000011A0000020006003A002A11000000007C4B000000000000D00000000000000000000000630000063856000001000000004765745061676550726F70657274696573000016000311C84A00003C4B0000D000000038560000010000001E00201100000000370000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600000C000000011A0000020006003E002A1100000000384C0000000000008C0000000000000000000000640000060857000001000000005570646174655061676550726F7065727469657300000016000311804B0000F84B00008C00000008570000010000001E00201100000000360000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600020C000000011A00000200060036002A1100000000344D0000000000003701000000000000000000006500000694570000010000000047657454657874436F6E74656E7400160003113C4C0000E84C00003701000094570000010000001E0020110000000035000011000000000000000066696C7465725265676578001A0020110100000035000011000000000000000066696C74657200001E00201102000000350000110000000000000000636F6D6D616E6454657874000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B00000604060001180000000112017B01811A028095000002830900020006003A002A1100000000EC4D000000000000A0000000000000000000000066000006CB580000010000000055706461746554657874436F6E74656E74000016000311384D0000AC4D0000A0000000CB580000010000001E00201100000000360000110000000000000000636F6D6D616E645465787400020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000005B000006040600020C000000011A0000020006003E002A1100000000344F000000000000DB0000000000000000000000670000066B590000010000000046696E64416E645265706C6163655F46696E640000000016000311F04D0000E04E0000DB0000006B590000010000001A0024115553797374656D2E446174612E53716C5479706573000000160024115546696E64416E645265706C616365434C5200001E002411554D6963726F736F66742E53716C5365727665722E536572766572001E002011000000003800001100000000000000006974656D5479706500000000220020110100000038000011000000000000000073716C446174615265636F7264000000020006004E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C0000000100030004060002200000000121016B0280DE02821F02837A02841A0284CB02857F0000020006003E002A110000000050500000000000001B010000000000000000000068000006465A0000010000000046696E64416E645265706C6163655F5265706C6163650016000311384F0000FC4F00001B010000465A0000010000001A0024115553797374656D2E446174612E53716C54797065730000000E0024115553797374656D2E586D6C001E00201100000000390000110000000000000000786D6C446F63756D656E74001E0020110100000039000011000000000000000063757272656E744E6F646500020006004E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C0000000100020004060002200000000118017B0280EE02829A02844B0285270286090286EB00000200060042002A1100000000CC5100000000000037010000000000000000000069000006615B0000010000000046696E64416E645265706C6163655F5570646174654C696E6B730016000311545000008851000037010000615B0000010000001A0024115553797374656D2E446174612E53716C5479706573000000160024115546696E64416E645265706C616365434C5200001E002411554D6963726F736F66742E53716C5365727665722E53657276657200120024115553797374656D2E54657874000000000A0024115553797374656D00220024115553797374656D2E546578742E526567756C617245787072657373696F6E73001E002011000000003A00001100000000000000006C696E6B52656765780000001E002011010000003A0000110000000000000000636F6D6D616E645465787400020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C000000010006000406000210000000012B01808A000000020006003E002A11000000004C530000000000001E00000000000000000000006A000006985C00000100000000434C52506167654D61704E6F64655F557064617465000016000311D05100000C5300001E000000985C0000010000000A0024115553797374656D00120024115553797374656D2E44617461000000001A0024115553797374656D2E446174612E53716C436C69656E7400001A0024115553797374656D2E446174612E53716C54797065730000001E002411554D6963726F736F66742E53716C5365727665722E536572766572000E0024115553797374656D2E586D6C00160024115553797374656D2E586D6C2E5850617468000000160024115553797374656D2E436F6C6C656374696F6E7300120024115553797374656D2E546578740000000016002011000000003B000011000000000000000069640000020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C00000001000900040600020C00000001110000020006003E002A1100000000C0530000000000001200000000000000000000006B000006B65C00000100000000434C52506167654D61704E6F64655F53617665000000002E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004010000040100000C0000006A0000060200060046002A110000000070590000000000009904000000000000000000006C000006C85C00000100000000434C52506167654D61704E6F64655F416464436F6E7461696E65720000000016000311C4530000C458000099040000C85C000001000000160003110C540000C05800007B040000E55C0000010000001A002011010000003C0000110000000000000000636F6E6E0000000022002011020000003C00001100000000000000007864436F6E7461696E6572496473000022002011030000003C0000110000000000000000786454656D706C6174654964730000001600031124540000885800002F040000FD5C00000100000026002011040000003C0000110000000000000000786D6C4E6F646554656D706C617465496473000022002011050000003C000011000000000000000074656D706C617465537472696E6700001E002011060000003C000011000000000000000074656D70537472696E6700001E002011070000003C000011000000000000000064745061676544656600000016002011080000003C000011000000000000000073716C0016002011090000003C0000110000000000000000636D64001A0020110A0000003C000011000000000000000061646170746572001A0020110B0000003C000011000000000000000078706174680000001E0020110C0000003C00001100000000000000007864506167654465664442002A0020110D0000003C000011000000000000000070616765446566696E6974696F6E586D6C537472696E6700260020110E0000003C0000110000000000000000636F6E7461696E6572586D6C537472696E6700002A0020110F0000003C00001100000000000000006E6577436F6E7461696E6572586D6C537472696E670000001E002011100000003C000011000000000000000075706461746553746D7400001A002011110000003C000011000000000000000073746D740000000022002011120000003C0000110000000000000000757064617465436F6D6D616E6400000016000311A0540000EC56000029000000AC5D00000100000026002011160000003C0000110000000000000000786D6C4E6F646554656D706C61746549640000000200060016000311A0540000505800009A0100009E5E0000010000001E0020111A0000003C000011000000000000000064725061676544656600000016000311F05600004C5800008C010000AC5E000001000000260020111B0000003C0000110000000000000000786D6C4E6F6465436F6E7461696E657249647300220020111C0000003C0000110000000000000000786D6C4E6F646550616765446566000016000311285700004858000063000000895F000001000000260020111F0000003C0000110000000000000000786D6C4E6F6465436F6E7461696E657249640000160003118C5700004458000055000000975F0000010000001A002011200000003C00001100000000000000006E6F64654964000016000311CC570000405800001B000000D05F00000100000026002011220000003C0000110000000000000000636F6E7461696E65724E6F6465537472696E6700020006000200060002000600020006000200060016000311A0540000845800007B000000856000000100000016002011240000003C00001100000000000000006472000002000600020006001600031124540000BC580000100000002E6100000100000016002011250000003C000011000000000000000065783100020006000200060002000600A6000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A0000060406000178000000020E016C0180BA0180F701839001841001853A0186230186580186FF01878701884701887A0188A60188E601892101918A01921701924802814D02825006845F01845F00028573068958018958018B2C018BAC00068CE5018CE5018D58028DA2018EB606926F01926F01949F029543000200060046002A1100000000505F0000000000007805000000000000000000006D000006616100000100000000434C52506167654D61704E6F64655F52656D6F7665436F6E7461696E6572001600031174590000905E000078050000616100000100000016000311BC5900008C5E00005A0500007E610000010000001A002011010000003D0000110000000000000000636F6E6E0000000022002011020000003D00001100000000000000007864436F6E7461696E6572496473000022002011030000003D0000110000000000000000786454656D706C61746549647300000016000311D4590000545E00000E050000966100000100000026002011040000003D0000110000000000000000786D6C4E6F646554656D706C617465496473000022002011050000003D000011000000000000000074656D706C617465537472696E6700001E002011060000003D000011000000000000000074656D70537472696E6700001E002011070000003D000011000000000000000064745061676544656600000016002011080000003D000011000000000000000073716C0016002011090000003D0000110000000000000000636D64001A0020110A0000003D000011000000000000000061646170746572001A0020110B0000003D000011000000000000000078706174680000001E0020110C0000003D0000110000000000000000786450616765446566444200220020110D0000003D00001100000000000000007864436F6E7461696E6572586D6C00001E0020110E0000003D000011000000000000000075706461746553746D7400001A0020110F0000003D000011000000000000000073746D740000000022002011100000003D0000110000000000000000757064617465436F6D6D616E6400000016000311505A0000405C000029000000456200000100000026002011140000003D0000110000000000000000786D6C4E6F646554656D706C61746549640000000200060016000311505A00001C5E00008B02000025630000010000001E002011180000003D000011000000000000000064725061676544656600000016000311445C0000185E00007D020000336300000100000026002011190000003D0000110000000000000000786D6C4E6F6465436F6E7461696E657249647300220020111A0000003D0000110000000000000000786D6C4E6F6465506167654465660000160003117C5C0000145E0000210100001564000001000000260020111D0000003D0000110000000000000000786D6C4E6F6465436F6E7461696E65724964000016000311E05C0000105E00001301000023640000010000001A0020111E0000003D00001100000000000000006E6F646549640000220020111F0000003D0000110000000000000000786E436F6E7461696E6572730000000026002011200000003D0000110000000000000000786E436F6E7461696E6572734C6F77657200000016000311205D00000C5E000089000000AC6400000100000016002011230000003D00001100000000000000006A00000016000311A05D0000085E00002F000000CD640000010000001E002011260000003D0000110000000000000000786E436F6E7461696E65720002000600020006000200060002000600020006000200060016000311505A0000505E00007B000000FD65000001000000160020112A0000003D000011000000000000000064720000020006000200060016000311D4590000885E000010000000A666000001000000160020112B0000003D000011000000000000000065783100020006000200060002000600BA000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A000006040600038C000000020C016A0180B80180F501838E01840E0185380188810188B601895C0189E4018AA4018AD7018B080195D201965F01969002814B02824E06845D01845D00028571068B32018B32018CCE018D4E00068E9D018E9D018F10018F8401901C0290B70291530191FA02924E0692C20192C20293380291F10696B70196B70198E702998B0000000200060042002A110000000010620000000000009603000000000000000000006E000006D96600000100000000434C52506167654D61704E6F64655F53617665557064617465000016000311545F00008861000096030000D9660000010000001A002011000000003E0000110000000000000000636F6E6E000000001A002011010000003E000011000000000000000066756C6C586D6C0016002011020000003E0000110000000000000000786400001A002011030000003E0000110000000000000000786444620000000016000311985F00001C6100001A030000006700000100000016002011040000003E000011000000000000000073716C0016002011050000003E0000110000000000000000636D64001A002011060000003E000011000000000000000078706174680000001A002011070000003E000011000000000000000064624E6F6465000022002011080000003E00001100000000000000006E6F64654964417474726962000000001A002011090000003E00001100000000000000006E6F646549640000160003111C60000018610000E9000000BD6800000100000022002011110000003E00001100000000000000006462506172656E744E6F646500000000020006000200060016000311985F000050610000100000001C6A00000100000016002011160000003E0000110000000000000000657800000200060016000311985F000084610000100000002F6A00000100000016002011170000003E000011000000000000000065783100020006000200060082000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A0000060406000354000000011C01610180A00180CD01819101820A01830E01833D0183980183E20284080285590285A60287A002892C028AA0028CEE018E24028E7A028FE90292300293C601962D0196B40297420000000200060046002A110000000040680000000000001206000000000000000000006F0000066F6A00000100000000434C52506167654D61704E6F64655F557064617465436F6E7461696E657200160003111462000080670000120600006F6A000001000000160003115C6200007C670000F40500008C6A0000010000001A002011010000003F0000110000000000000000636F6E6E0000000022002011020000003F00001100000000000000007864436F6E7461696E6572496473000022002011030000003F0000110000000000000000786454656D706C617465496473000000160003117462000044670000A8050000A46A00000100000026002011040000003F0000110000000000000000786D6C4E6F646554656D706C617465496473000022002011050000003F000011000000000000000074656D706C617465537472696E6700001E002011060000003F000011000000000000000074656D70537472696E6700001E002011070000003F000011000000000000000064745061676544656600000016002011080000003F000011000000000000000073716C0016002011090000003F0000110000000000000000636D64001A0020110A0000003F000011000000000000000061646170746572001A0020110B0000003F000011000000000000000078706174680000001E0020110C0000003F00001100000000000000007864506167654465664442001E0020110D0000003F000011000000000000000075706461746553746D7400001A0020110E0000003F000011000000000000000073746D7400000000220020110F0000003F0000110000000000000000757064617465436F6D6D616E6400000016000311F0620000BC64000029000000536B00000100000026002011130000003F0000110000000000000000786D6C4E6F646554656D706C61746549640000000200060016000311F06200000C6700002F0300003D6C0000010000001E002011170000003F000011000000000000000064725061676544656600000016000311C064000008670000210300004B6C00000100000026002011180000003F0000110000000000000000786D6C4E6F6465436F6E7461696E65724964730022002011190000003F0000110000000000000000786D6C4E6F646550616765446566000016000311F86400000467000035020000FE6C000001000000260020111C0000003F0000110000000000000000786D6C4E6F6465436F6E7461696E657249640000160003115C65000000670000270200000C6D0000010000001A0020111D0000003F00001100000000000000006E6F646549640000160003119C650000246600003D010000366D0000010000001E0020111F0000003F00001100000000000000007865436F6E7461696E6572001A002011200000003F0000110000000000000000786174740000000002000600160003119C650000FC660000BA000000786E00000100000022002011220000003F0000110000000000000000786E436F6E7461696E6572730000000026002011230000003F0000110000000000000000786E436F6E7461696E6572734C6F7765720000001600031128660000F866000089000000A86E00000100000016002011240000003F00001100000000000000006A000000160003118C660000F46600002F000000C96E0000010000001E002011270000003F0000110000000000000000786E436F6E7461696E6572000200060002000600020006000200060002000600020006000200060016000311F0620000406700006A000000B96F000001000000160020112B0000003F0000110000000000000000647200000200060002000600160003117462000078670000100000004E70000001000000160020112C0000003F000011000000000000000065783100020006000200060002000600BA000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A000006040600008C0000000280E70181460181940181D101846F0184EF018619018962018997018A53018ADB018B9B018BCE0199CC019A44019A7502822702832A06853E01853E00028652068BF5018BF5018CE2018D6200068E9B018E9B018F0E028F58018FB601902E0001959301961B0196B10297050697790197790297EF0296A8069A9C019A9C019CAE029D52020006004E002A1100000000407100000000000083060000000000000000000070000006817000000100000000536574757050726F7061676174696F6E416E64496E6865726974616E63654174747269627300001600031144680000287000008306000081700000010000001E0020110000000040000011000000000000000070726F7053656341747400001E00201101000000400000110000000000000000696E687453656341747400001A002011020000004000001100000000000000007365634174740000220020110300000040000011000000000000000070726F70526F6C6573417474000000002200201104000000400000110000000000000000696E6874526F6C6573417474000000001E00201105000000400000110000000000000000726F6C6573417474000000001A0020110600000040000011000000000000000070726F70536563001A00201107000000400000110000000000000000696E6874536563001E002011080000004000001100000000000000007365634C6576656C730000001E0020110900000040000011000000000000000070726F70526F6C65730000001E0020110A000000400000110000000000000000696E6874526F6C65730000001A0020110B000000400000110000000000000000726F6C65730000001A0020110C0000004000001100000000000000006E6F6465496400001A0020110D00000040000011000000000000000078706174680000001E0020110E0000004000001100000000000000007468697344624E6F6465000016000311946800002470000063050000A0710000010000001E002011100000004000001100000000000000006368696C644E6F64657300001E00201111000000400000110000000000000000706172656E744E6F646500002200201112000000400000110000000000000000706172656E74536563417474000000002200201113000000400000110000000000000000706172656E74526F6C657341747400001E00201114000000400000110000000000000000706172656E745365630000001E00201115000000400000110000000000000000706172656E74526F6C657300160003117C6A0000146E0000D2020000B371000001000000160020111600000040000011000000000000000063000000160003115C6B0000106E0000B0020000BB710000010000001E002011170000004000001100000000000000006F6E654368696C6400000000160003118C6B0000046D00005B010000D2710000010000001E002011190000004000001100000000000000006368696C645365634174740016000311C46B0000006D0000D40000002C720000010000001E0020111D0000004000001100000000000000007061724C6576656C73000000220020111E000000400000110000000000000000746869734368696C644C6576656C73002E0020111F000000400000110000000000000000746869734368696C644C6576656C7341727261794C6973740000000016000311FC6B0000C06C0000130000006B720000010000001E00201122000000400000110000000000000000746869734368696C640000000200060016000311FC6B0000FC6C00003300000096720000010000001E002011250000004000001100000000000000006F6E655061724C6576656C00020006000200060002000600160003118C6B00000C6E000032010000387300000100000022002011280000004000001100000000000000006368696C64526F6C657341747400000016000311086D0000086E0000AB00000092730000010000001E0020112C000000400000110000000000000000706172526F6C657300000000220020112D000000400000110000000000000000746869734368696C64526F6C657300002A0020112E000000400000110000000000000000746869734368696C64526F6C657341727261794C6973740016000311446D0000046E000033000000D3730000010000001E002011310000004000001100000000000000006F6E65506172526F6C6500000200060002000600020006000200060002000600160003117C6A0000206F0000F3000000FA7400000100000022002011360000004000001100000000000000007365634C6576656C734172726179000026002011370000004000001100000000000000007061725365634C6576656C73417272617900000026002011380000004000001100000000000000007365634C6576656C7341727261794C697374000016000311186E0000E06E0000330000003675000001000000220020113B0000004000001100000000000000006F6E65506172656E74536563000000000200060016000311186E00001C6F00003B000000B1750000010000001E0020113E0000004000001100000000000000006E65775365634174740000000200060002000600160003117C6A000020700000F40000000D760000010000001E00201141000000400000110000000000000000726F6C6573417272617900002200201142000000400000110000000000000000706172526F6C657341727261790000002200201143000000400000110000000000000000726F6C657341727261794C697374000016000311246F0000E06F000033000000497600000100000022002011460000004000001100000000000000006F6E65506172656E74526F6C650000000200060016000311246F00001C7000003B000000C5760000010000001E002011490000004000001100000000000000006E6577526F6C6573417474000200060002000600020006000200060012010404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A00000604060000E40000000119016A0180B90180FF0181490181910181C801821B0182700182BA01831301836E0183BA0183F201843E0284830184F001952C0195660195B80195FF01965901853401857E0285A90186180286620286AB02870901881F01886B01893207897F09897F01897F078A64098A64018A64028AD6028E2D018E71028EB4028EFF028F5901906F0190B60191120791700991700191700291E002852B0296B40296E101973801977D0197C8079815099815019815029878029974019A8C029BAC029BDB019C34019C71019CBA079CFF099CFF019CFF029D5F029E56019F63020006003A002A11000000001873000000000000DE010000000000000000000071000006047700000100000000497465726174654368696C6472656E000000001600031144710000AC720000DE01000004770000010000001600031180710000A8720000A801000017770000010000001E00201101000000410000110000000000000000696E4368696C644E6F6465001600031198710000A47200009C01000023770000010000001E002011020000004100001100000000000000006E6F64654E616D65000000001E002011030000004100001100000000000000006964417474726962000000001E00201104000000410000110000000000000000746D705870617468000000001E0020110500000041000011000000000000000064624368696C644E6F6465001A002011060000004100001100000000000000006E657758650000001E0020110700000041000011000000000000000069734E65774E6F646500000002000600020006000200060066000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A0000060406000238000000060C010C015E0180990180E601811401814001816024818002818002827E0283790287B60289AA028AD3028B130000000200060036002A11000000000C75000000000000D2010000000000000000000072000006E2780000010000000053657475704E65774E6F6465000000160003111C730000B0740000D2010000E2780000010000001600201100000000420000110000000000000000786500001600031154730000E873000036000000FB780000010000001600201102000000420000110000000000000000786100001600031184730000E47300002A000000077900000100000016002011030000004200001100000000000000006462610002000600020006001600031154730000AC740000510100005B790000010000001A002011060000004200001100000000000000006E6577496400000016000311EC7300006074000038000000D079000001000000260020110800000042000011000000000000000063726561746564427941747472696275746500000200060016000311EC730000A87400004B000000607A0000010000002A0020110B00000042000011000000000000000063726561746564446174654174747269627574650000000002000600020006000200060056000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A00000604060001280000000117064401440180980002811D01814C0281B60182920283730001845F1601000200060036002A1100000000387700000000000069020000000000000000000073000006B47A000001000000005365744174747269627574657300001600031110750000D076000069020000B47A00000100000016000311487500004076000006010000C77A00000100000016002011010000004300001100000000000000006174740016000311607500003C760000E1000000EB7A0000010000001E0020110300000043000011000000000000000073747241747456616C7565001E002011040000004300001100000000000000006741747456616C75650000001E002011050000004300001100000000000000006462417474726962000000001600031190750000387600005B000000707B00000100000016002011080000004300001100000000000000006462610002000600020006000200060016000311487500008476000038000000407C000001000000260020110D0000004300001100000000000000006D6F646966696564427941747472696275746500020006001600031148750000CC7600004B000000D07C0000010000002A002011100000004300001100000000000000006D6F64696669656444617465417474726962757465000000020006000200060062000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A0000060406000034000000060E010E02560180A50180D40181F90282320282700183840283C0000284D80285020185E80286D1000187C70200060046002A1100000000347D000000000000390400000000000000000000740000061D7D00000100000000536574757050616765446566696E6974696F6E4368696C6472656E00000000160003113C770000807C0000390400001D7D0000010000002200201100000000440000110000000000000000636F6E7461696E65725870617468000026002011010000004400001100000000000000006462436F6E7461696E65724E6F646573000000001E002011020000004400001100000000000000007374796C6558706174680000220020110300000044000011000000000000000064625374796C654E6F646573000000001E00201104000000440000110000000000000000736372697074587061746800220020110500000044000011000000000000000064625363726970744E6F6465730000002A0020110600000044000011000000000000000072656D61696E696E67436F6E7461696E65724C6973740000260020110700000044000011000000000000000072656D61696E696E675374796C654C6973740000260020110800000044000011000000000000000072656D61696E696E675363726970744C697374001600031184770000A0790000B20000007A7D000001000000220020110A00000044000011000000000000000064624F6E65436F6E7461696E6572000016000311EC7800009C790000A4000000887D0000010000001A0020110B000000440000110000000000000000696456616C7565001A0020110C000000440000110000000000000000696E585061746800220020110D000000440000110000000000000000696E4F6E65436F6E7461696E6572000002000600020006001600031184770000247A000033000000657E000001000000260020111200000044000011000000000000000072656D61696E696E67436F6E7461696E6572000016000311A4790000207A000025000000737E00000100000022002011130000004400001100000000000000006E6577436F6E7461696E65720000000002000600020006001600031184770000D47A0000B2000000C77E0000010000001E0020111500000044000011000000000000000064624F6E655374796C65000016000311287A0000D07A0000A4000000D57E0000010000001A00201116000000440000110000000000000000696456616C7565001A00201117000000440000110000000000000000696E5850617468001E00201118000000440000110000000000000000696E4F6E655374796C65000002000600020006001600031184770000507B000033000000B27F000001000000220020111C00000044000011000000000000000072656D61696E696E675374796C65000016000311D87A00004C7B000025000000C07F0000010000001E0020111D0000004400001100000000000000006E65775374796C650000000002000600020006001600031184770000007C0000B200000015800000010000001E0020111F00000044000011000000000000000064624F6E655363726970740016000311547B0000FC7B0000A400000023800000010000001A00201120000000440000110000000000000000696456616C7565001A00201121000000440000110000000000000000696E5850617468001E00201122000000440000110000000000000000696E4F6E6553637269707400020006000200060016000311847700007C7C0000330000000181000001000000220020112600000044000011000000000000000072656D61696E696E675363726970740016000311047C0000787C0000250000000F810000010000001E002011270000004400001100000000000000006E6577536372697074000000020006000200060002000600AE000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A00000604060002800000000181FB0182450182930182D501831B01835F0188C4018F0C0195440683C70183C701841B0184CA01852D02857302868D00068910018910018972068A4D018A4D018A99018B40018B9F028BE1028CED068F50018F50018FAA0690760190760190C401916D0191CD02921002931F06958A01958A0195E60000020006003E002A11000000000C7F000000000000BB01000000000000000000007500000656810000010000000053657475705075626C69736844657461696C730000000016000311387D0000B87E0000BB010000568100000100000016000311787D0000F07D00003D0000006E8100000100000022002011010000004500001100000000000000006375725075626C697368436F756E740022002011020000004500001100000000000000006E65775075626C697368436F756E74000200060016000311787D00002C7E000027000000AD810000010000001E002011030000004500001100000000000000007075624174747269620000000200060016000311787D00006C7E0000480000002A820000010000002200201106000000450000110000000000000000707562446174654174747269620000000200060016000311787D0000B47E000048000000C8820000010000002A002011080000004500001100000000000000007374617475734368616E676564446174654174747269620002000600020006004E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A0000060406000120000000020C015B0180BC01817F028222000183000283D20184B6000200060046002A1100000000B482000000000000B8030000000000000000000076000006118300000100000000434C52506167654D61705F5361766550616765446566696E6974696F6E000016000311107F000020820000B803000011830000010000001A00201100000000460000110000000000000000636F6E6E000000001E0020110100000046000011000000000000000066756C6C4462586D6C00000016000311587F0000E8810000950300002B830000010000001E00201102000000460000110000000000000000696E707574446F63000000001A002011030000004600001100000000000000006462446F63000000160020110400000046000011000000000000000073716C001600201105000000460000110000000000000000636D64001A00201106000000460000110000000000000000787061746800000022002011070000004600001100000000000000006462506172656E744E6F6465000000001E002011080000004600001100000000000000007061676544656649640000001E0020110900000046000011000000000000000069734E657750616765000000220020110A000000460000110000000000000000696E707574496441747472696200000016000311AC7F0000A881000039010000A9840000010000002200201111000000460000110000000000000000646250616765496E7374616E6365730016000311D4800000A48100009F00000038850000010000001600201114000000460000110000000000000000636F75001600031110810000A08100007D000000408500000100000022002011150000004600001100000000000000006F6E655061676544624E6F646500000022002011160000004600001100000000000000006578636C75646541747472696273000002000600020006000200060016000311AC7F0000E48100007F000000E4850000010000001E002011190000004600001100000000000000006E6577506167654465660000020006000200060016000311587F00001C82000006000000C286000001000000160020111C0000004600001100000000000000006578000002000600020006008E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A0000060406000360000000013401790180BC0180F30181460181BB0182AA0183910186DB0187050187340283DE02854402877C0287B7028992028AF3018B9B028BE8028D47018E1D018E82018FEE02916F028E140192C502939402940F01972B0000000200060046002A1100000000848900000000000080050000000000000000000077000006C98600000100000000434C53506167654D61704E6F64655F417474616368576F726B666C6F77000016000311B8820000C088000080050000C9860000010000001A00201100000000470000110000000000000000636F6E6E000000001E0020110100000047000011000000000000000066756C6C4462586D6C00000016000311008300008888000069050000D7860000010000001E00201102000000470000110000000000000000696E707574446F63000000001A002011030000004700001100000000000000006462446F63000000160020110400000047000011000000000000000073716C001600201105000000470000110000000000000000636D64001A00201106000000470000110000000000000000787061746800000022002011070000004700001100000000000000006462506167654D61704E6F64650000002A0020110800000047000011000000000000000064624578697374696E67576F726B666C6F774C6973740000260020110900000047000011000000000000000063757272656E74576F726B666C6F777300000000220020110A000000470000110000000000000000776F726B666C6F7745786973747300001600031154830000D08400001A000000E287000001000000260020110F0000004700001100000000000000006F6E654578697374696E67576F726B666C6F77000200060016000311548300000C850000300000003C880000010000001E00201112000000470000110000000000000000637572576F726B666C6F770002000600160003115483000018860000F0000000A0880000010000002E00201114000000470000110000000000000000706172656E74506167654D61704E6F6465576F726B666C6F77730000160003111085000014860000AB000000C0880000010000002200201116000000470000110000000000000000706172656E74576F726B666C6F7700001600031158850000108600009D000000CE880000010000002600201117000000470000110000000000000000706172656E74576F726B666C6F7749640000000016000311948500000C8600004B0000001F890000010000001E0020111A0000004700001100000000000000006E6577576F726B666C6F77000200060002000600020006000200060016000311548300004C86000006000000B189000001000000160020111B000000470000110000000000000000657800000200060016000311548300000C870000AB000000CF89000001000000220020111D000000470000110000000000000000696E4F6E65576F726B666C6F770000001600031150860000088700009D000000DD89000001000000220020111E000000470000110000000000000000696E576F726B666C6F77496400000000160003118C860000048700004B0000002E8A00000100000022002011210000004700001100000000000000006164646564576F726B666C6F7700000002000600020006000200060016000311548300008488000033010000AD8A0000010000002A002011230000004700001100000000000000006368696C64506167654D61704E6F64654C69737400000000160003111087000080880000EC000000CF8A00000100000026002011250000004700001100000000000000006368696C64506167654D61704E6F64650000000016000311548700007C880000A4000000F28A00000100000016002011270000004700001100000000000000006B65790016000311948700007888000096000000008B00000100000026002011280000004700001100000000000000006368696C644E6F6465576F726B666C6F7700000016000311C487000074880000360000005F8B000001000000260020112B000000470000110000000000000000696E707574576F726B666C6F774E6F64650000002E0020112C0000004700001100000000000000006164646564576F726B666C6F77546F4368696C644E6F6465000000000200060002000600020006000200060002000600020006001600031100830000BC88000006000000428C000001000000160020112D000000470000110000000000000000657800000200060002000600BE000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000006A0000060406000290000000011A015F0180A20180D901812C0181A10182900183000185C901884001897D02834E0284B80286860686C60186C6000688BB0188BB0289F0018A29068A84018A84018B5B028C0A028D1F018D6F0190630690B80190B801919902923702930C01938102956001967B06971201971206987001987001994C0299A3029B1C019BC3019C1F019FA7000002000600F2000000440100001A1D0000010001008B00000000000000190000003801000000000000130000800100000014000080070000001600008012000000EEEFFE80150000001700008016000000180000801E000000190000801F0000001B00008029000000EEEFFE802C0000001C0000802D0000001D0000802F0000001E000080360000001F0000803900000020000080450000002100008046000000220000804E0000002300008056000000240000806800000025000080710000002600008079000000270000807C000000EEEFFE8088000000EEEFFE8089000000280000808A000000290000800500060009002800090023000000000009000A000D00310009000A00090019000000000009000A000D001C000D002D000D00260014005B000D000E00110023001100380011008D0011004000110036000D000E00000000000000000009000A0005000600F2000000BC010000A51D000001000100EA0000000000000023000000B0010000000000003D000080010000003E000080030000003F000080090000004000008014000000EEEFFE8017000000410000801800000042000080200000004300008021000000450000802C000000EEEFFE802F0000004600008030000000470000803800000048000080390000004A000080450000004B0000804D0000004C00008058000000EEEFFE805F0000004D000080600000004E000080630000004F0000806A000000500000806D0000005100008079000000520000807A0000005300008082000000540000808A000000550000809C00000056000080A900000057000080B300000058000080D000000059000080D80000005A000080DB000000EEEFFE80E7000000EEEFFE80E80000005B000080E90000005C000080050006000900280009002800090024000000000009000A000D00380009000A00090023000000000009000A000D00310009000A000900280009002B00090019000000000009000A000D001C000D002D000D00260014005B000D000E00110023001100380011008C0011003E0011004F0011009000110036000D000E00000000000000000009000A0005000600F2000000140100008F1E00000100010087000000000000001500000008010000000000006000008001000000610000800D0000006200008017000000EEEFFE801A000000630000801B000000640000801D0000006500008023000000660000802500000067000080310000006800008032000000690000803A0000006A000080420000006B000080580000006C000080610000006D000080690000006E0000806C000000EEEFFE8078000000EEEFFE80790000006F000080850000007000008086000000710000800500060009002800090019000000000009000A000D001C000D002D000D00260014005B000D000E0011002300110038001100890011004800110036000D000E0000000000000000000D003A0009000A0005000600F200000084000000161F000001000100440000000000000009000000780000000000000074000080010000007500008008000000770000800E000000780000801A00000079000080220000007A0000802F0000007B000080330000007D000080420000007E0000800500060009003E00090032000900290009002A0009001F00090025000900570005000600F2000000C00000005A1F0000010001006C000000000000000E000000B40000000000000081000080010000008200008008000000840000800E000000850000801A0000008600008023000000870000802B000000880000802F0000008A000080350000008B000080410000008C0000804A0000008D000080570000008E0000805B0000008F0000806A000000900000800500060009003E000900320009002D0009002A00090025000900250009003200090030000900370009002600090025000900560005000600F2000000C0000000C61F0000010001006C000000000000000E000000B40000000000000093000080010000009400008008000000960000800E000000970000801A0000009800008023000000990000802B0000009A0000802F0000009C000080350000009D000080410000009E0000804A0000009F00008057000000A00000805B000000A10000806A000000A20000800500060009003E000900320009002A0009002A00090025000900250009003200090030000900370009002600090025000900540005000600F20000008400000032200000010001004500000000000000090000007800000000000000A600008001000000A700008008000000A90000800E000000AA0000801A000000AB00008023000000AC00008030000000AD00008034000000AF00008043000000B00000800500060009003E00090032000900300009003700090026000900250009005A0005000600F20000008400000077200000010001004500000000000000090000007800000000000000B300008001000000B400008008000000B60000800E000000B70000801A000000B800008023000000B900008030000000BA00008034000000BC00008043000000BD0000800500060009003E0009003200090030000900370009002600090025000900590005000600F2000000C0000000BC200000010001006C000000000000000E000000B400000000000000C000008001000000C100008008000000C30000800E000000C40000801A000000C500008023000000C60000802B000000C70000802F000000C900008035000000CA00008041000000CB0000804A000000CC00008057000000CD0000805B000000CF0000806A000000D00000800500060009003E00090032000900310009002A000900250009002500090032000900300009003700090026000900250009005B0005000600F20000008406000028210000010001007909000000000000890000007806000000000000D300008001000000D400008007000000D500008014000000D700008017000000D80000801F000000D900008022000000DB00008023000000DB00008030000000EEEFFE8035000000DB0000803D000000DC0000803E000000DD0000805F000000EEEFFE8063000000DE00008068000000DF00008071000000E100008084000000EEEFFE8088000000E20000808D000000E400008090000000EEEFFE8094000000EEEFFE80EC030000E7000080F9030000E80000800A040000E90000800F040000EC0000801C040000ED0000802D040000EE00008032040000F40000803A040000F50000804B040000F600008050040000F900008058040000FA00008069040000FB0000806E040000FE00008076040000FF00008087040000000100808C040000030100809404000004010080A504000005010080AA04000008010080B704000009010080C80400000A010080CD0400000D010080D50400000E010080E60400000F010080EB04000012010080F40400001301008006050000140100800B05000017010080180500001801008029050000190100802E0500001C0100803A050000EEEFFE803E0500001D0100803F0500001E010080490500001F0100805105000020010080540500002201008055050000230100805B050000240100805C05000025010080640500002601008077050000280100807C0500002B010080890500002C0100809A0500002D0100809F05000030010080AC05000031010080BD05000032010080C205000035010080CB05000036010080DD05000037010080E20500003A010080EB0500003B0100802F060000EEEFFE80330600003C010080450600003D0100804A06000040010080530600004101008097060000EEEFFE809B06000042010080AD06000043010080B206000046010080BB06000048010080C30600004901008007070000EEEFFE800B0700004A0100800C0700004B010080140700004C010080150700004E010080210700004F01008065070000EEEFFE8069070000500100806A07000051010080760700005201008077070000540100809B070000EEEFFE809F07000055010080A007000056010080B207000057010080B307000059010080B80700005C010080C10700005E010080C40700005F01008008080000EEEFFE800C080000600100800D08000061010080100800006201008011080000640100801D0800006501008061080000EEEFFE80650800006601008066080000670100807208000068010080730800006A0100808D080000EEEFFE80910800006B010080920800006C010080A40800006D010080A50800006F010080AA08000072010080B308000073010080F7080000EEEFFE80FB080000740100800D090000750100800F090000780100802309000079010080250900007C010080350900007D010080370900008201008038090000DB00008046090000EEEFFE805509000084010080600900008601008064090000EEEFFE8068090000870100807809000088010080050006000900320009002500090014000900180009001B000900100026004200000000001200220009000A000D0058000000000011001A000D003F000D0033000000000011001A001100230000000000000000001900500019006D0019001F001900590019006C0019001F0019003E001900680019001F0019003E001900660019001F0019003E0019006F0019001F0019003E001900690019001F001900590019006D0019001F0019003E0019006D0019001F00190054001900650019001F00190053001900700019001F0019002D000000000019001A001D003A001D003A0019001A0019001A001D00600019001A001D005A001D00890019001F00190053001900700019001F00190057001900740019001F0019005B0019006C0019001F0019004F0019007A00000000001D006C0019001F0019004D0019007A00000000001D006A0019001F00190054001900320019009B000000000019001A001D002E0019001A0019004B0019009B000000000019001A001D00470019001A0019005D000000000019001A001D006C0019001A0019001F001900570019002E00190097000000000019001A001D002E0019001A0019004300190097000000000019001A001D00430019001A00190059000000000019001A001D006D0019001A0019001F001900550019007A00000000001D00720019001F001900460019001F001900380019001F0009000A0023002500000000000900450009002100000000000D00590005000600F200000084000000A12A00000100010045000000000000000900000078000000000000008B010080010000008C010080080000008E0100800E0000008F0100801A0000009001008023000000910100803000000092010080340000009401008043000000950100800500060009003E0009003200090030000900370009002600090025000900580005000600F20000002C010000E62A000001000100C600000000000000170000002001000000000000980100800100000099010080080000009A0100800E0000009C0100801F000000EEEFFE80230000009D01008034000000A20100803A000000A301008046000000A40100804F000000A501008057000000A60100805B000000A801008061000000A90100806D000000AA01008076000000AB0100807E000000AC01008082000000AE01008089000000AF01008096000000B0010080A0000000B1010080AE000000B2010080B3000000B4010080C3000000B50100800500060009003E000900260009003E00000000000D0040000900320009003C0009002A000900250009002500090032000900300009002E0009001F0009002500090032000900300009003700090026000900250009005D0005000600F2000000C0000000AC2B0000010001006C000000000000000E000000B400000000000000B801008001000000B901008008000000BB0100800E000000BC0100801A000000BD01008023000000BE0100802B000000BF0100802F000000C101008035000000C201008041000000C30100804A000000C401008057000000C50100805B000000C70100806A000000C80100800500060009003E00090032000900330009002A00090025000900250009003200090030000900370009002600090025000900540005000600F2000000C0000000182C00000100010071000000000000000E000000B400000000000000CB01008001000000CC01008008000000D40100800E000000D50100801A000000D601008023000000D701008030000000D801008034000000DA0100803A000000DB01008046000000DC0100804F000000DD0100805C000000DE01008060000000E00100806F000000E10100800500060009003E000900320009002D0009002A00090032000900250009003200090030000900370009002600090025000900560005000600F2000000D4010000892C0000010001006A0100000000000025000000C801000000000000E401008001000000E501008009000000E70100800A000000E701008011000000EEEFFE8016000000E701008022000000E801008023000000E901008079000000EB0100807F000000ED010080B3000000EEEFFE80B7000000EE010080B8000000EF010080B9000000EF010080E3000000EEEFFE80E5000000EF010080EC000000F0010080ED000000F1010080FA000000EEEFFE80FE000000F2010080FF000000F301008008010000F501008011010000F601008012010000F701008013010000EEEFFE8019010000EF01008021010000F90100802E010000FA01008031010000FC01008032010000FD0100803E010000FE0100803F010000FF01008040010000E70100804D010000EEEFFE8061010000EEEFFE806201000001020080670100000202008005000600090052000900100022002A000000000012001E0009000A000D00D6000D0037000D007700000000000D000E00110018002D005C00000000001A0029001100120015002F00000000001500160019003B00190032001500160011001200000000002A002C00110046000D000E000D000E00110037000D000E0009000A001F002100000000000000000009001B0005000600F200000078000000F32D0000010001005900000000000000080000006C00000000000000050200800100000006020080330000000702008039000000080200804100000009020080490000000A020080510000000B020080580000000C0200800500060009006C0009002B00090025000900210009002C0009001F0005000600F2000000C00000004C2E0000010001006C000000000000000E000000B4000000000000000F020080010000001002008008000000120200800E000000130200801A0000001402008023000000150200802B000000160200802F000000180200803500000019020080410000001A0200804A0000001B020080570000001C0200805B0000001E0200806A0000001F0200800500060009003E00090032000900310009002A000900250009002500090032000900300009003700090026000900250009005A0005000600F200000078000000B82E0000010001002D00000000000000080000006C0000000000000022020080010000002302008307000000270200800D0000002802008015000000290200801D0000002A020080250000002B0200802C0000002C020080050006000900650009002B00090025000900210009002C0009001F0005000600F200000078000000E52E0000010001002D00000000000000080000006C000000000000002F020080010000003002008007000000310200800D0000003202008015000000330200801D0000003402008025000000350200802C000000360200800500060009003E0009002B00090025000900210009002C0009001F0005000600F200000084000000122F000001000100370000000000000009000000780000000000000039020080010000003A020080070000003B0200800F0000003C0200841B0000004102008023000000420200802A00000043020080310000004402008035000000450200800500060009002B000900250009003A0009002C0009002C00090029000900140005000600F2000000E0010000492F000001000100860100000000000026000000D4010000000000004A020080010000004B020080070000004C020080130000004D0200801C0000004E02008029000000500200802F000000510200803B0000005202008043000000530200805000000055020080560000005602008062000000570200806B0000005802008074000000590200807C0000005B020080820000005C0200808E0000005D020080960000005E020080A300000060020080AA00000061020080B700000062020080C000000063020080CF00000065020080D600000066020080E300000067020080EC00000068020080FB0000006A020080020100006B0200800C0100006C020080190100006D020080220100006E020080300100006F0200803E010000700200804C010000710200805A010000720200806901000073020080780100007502008085010000760200800500060009003200090030000900370009002600090032000900290009002A0009001F000900320009002C0009002E0009001A00090022000900320009002D0009002A0009002300090032000900310009002A00090027000900320009002F0009002A000900250009002B000900250009003D000900370009002400090024000900240009002400090024000900240009002D0005000600F200000038010000CF30000001000100D700000000000000180000002C0100000000000079020080010000007A020080080000007C0200800E0000007D0200801A0000007E020080220000007F02008039000000800200803D0000008202008043000000830200804F000000840200805700000085020080640000008602008068000000880200806E000000890200807A0000008A020080820000008B0200808F0000008C020080930000008E0200809A0000008F020080A700000090020080B100000091020080BF00000092020080C400000094020080D4000000950200800500060009003E00090032000900320009002A0009004000090025000900320009002C0009002F0009002B00090025000900320009002A0009002F0009002B000900250009003200090030000900370009002600090025000900590005000600F200000074010000A63100000100010013010000000000001D0000006801000000000000980200800100000099020080080000009B0200800E0000009C0200801A0000009D020080230000009E020080350000009F02008039000000A10200803F000000A20200804B000000A302008054000000A402008066000000A50200806A000000A702008070000000A80200807C000000A902008084000000AA02008096000000AB0200809A000000AD020080A1000000AE020080AE000000AF020080B7000000B0020080CA000000B1020080CF000000B3020080D6000000B4020080E3000000B5020080ED000000B6020080FB000000B702008000010000B902008010010000BA0200800500060009003E000900320009002F0009002C0009003200090025000900320009002F0009002C0009003200090025000900320009002C0009002F0009002F00090025000900320009002A0009002F0009002D000900250009003200090030000900370009002600090025000900560005000600F200000074010000B93200000100010011010000000000001D0000006801000000000000BD02008001000000BE02008008000000C00200800E000000C10200801A000000C202008022000000C302008034000000C402008038000000C60200803E000000C70200804A000000C802008052000000C902008064000000CA02008068000000CC0200806E000000CD0200807A000000CE02008082000000CF02008094000000D002008098000000D20200809F000000D3020080AC000000D4020080B5000000D5020080C8000000D6020080CD000000D8020080D4000000D9020080E1000000DA020080EB000000DB020080F9000000DC020080FE000000DE0200800E010000DF0200800500060009003E000900320009002F0009002A0009003200090025000900320009002F0009002A0009003200090025000900320009002C0009002F0009002F00090025000900320009002A0009002F0009002D000900250009003200090030000900370009002600090025000900570005000600F2000000FC000000CA330000010001009D0000000000000013000000F000000000000000E202008001000000E302008008000000E50200800E000000E60200801A000000E702008022000000E80200802F000000E902008033000000EB02008039000000EC02008045000000ED0200804D000000EE0200805A000000EF0200805E000000F102008064000000F202008070000000F302008079000000F402008086000000F50200808A000000F60200809A000000F70200800500060009003E000900320009002C0009002F0009002B00090025000900320009002A0009002F0009002B0009002500090032000900300009003700090026000900250009005A0005000600F2000000FC00000067340000010001009D0000000000000013000000F000000000000000FA02008001000000FB02008008000000FD0200800E000000FE0200801A000000FF02008022000000000300802F000000010300803300000003030080390000000403008045000000050300804D000000060300805A000000070300805E00000009030080640000000A030080700000000B030080790000000C030080860000000D0300808A0000000E0300809A0000000F0300800500060009003E000900320009002C0009002F0009002B00090025000900320009002A0009002F0009002B000900250009003200090030000900370009002600090025000900560005000600F2000000FC0000000435000001000100A80000000000000013000000F00000000000000012030080010000001303008008000000150300800E000000160300801A00000017030080220000001803008039000000190300803D0000001B030080430000001C0300804F0000001D030080580000001E030080650000001F03008069000000210300806F000000220300807B00000023030080840000002403008091000000250300809500000027030080A5000000280300800500060009003E00090032000900320009002A0009003F00090025000900320009002D0009002A0009002F000900250009003200090030000900370009002600090025000900540005000600F2000000FC000000AC35000001000100A80000000000000013000000F0000000000000002B030080010000002C030080080000002E0300800E0000002F0300801A00000030030080220000003103008039000000320300803D0000003403008043000000350300804F0000003603008058000000370300806500000038030080690000003A0300806F0000003B0300807B0000003C030080840000003D030080910000003E0300809500000040030080A5000000410300800500060009003E00090032000900320009002A0009003F00090025000900320009002A0009002A0009002F0009002500090032000900300009003700090026000900250009005B0005000600F2000000C000000054360000010001006C000000000000000E000000B40000000000000044030080010000004503008008000000470300800E000000480300801A00000049030080230000004A0300802B0000004B0300802F0000004D030080350000004E030080410000004F0300804A0000005003008057000000510300805B000000530300806A000000540300800500060009003E000900320009002D0009002A000900250009002500090032000900300009003700090026000900250009005E0005000600F2000000C0000000C0360000010001006C000000000000000E000000B400000000000000570300800100000058030080080000005A0300800E0000005B0300801A0000005C030080230000005D0300802B0000005E0300802F00000060030080350000006103008041000000620300804A0000006303008057000000640300805B000000660300806A000000670300800500060009003E00090032000900310009002A00090025000900250009003200090030000900370009002600090025000900620005000600F2000000C00000002C370000010001006C000000000000000E000000B4000000000000006A030080010000006B030080080000006D0300800E0000006E0300801A0000006F03008023000000700300802B000000710300802F00000073030080350000007403008041000000750300804A0000007603008057000000770300805B000000790300806A0000007A0300800500060009003E00090032000900370009002A00090025000900250009003200090030000900370009002600090025000900580005000600F2000000C000000098370000010001006C000000000000000E000000B4000000000000007D030080010000007E03008008000000800300800E000000810300801A0000008203008023000000830300802B000000840300802F00000086030080350000008703008041000000880300804A00000089030080570000008A0300805B0000008C0300806A0000008D0300800500060009003E000900320009002D0009002A00090025000900250009003200090030000900370009002600090025000900540005000600F2000000C000000004380000010001006C000000000000000E000000B40000000000000090030080010000009103008008000000930300800E000000940300801A0000009503008023000000960300802B000000970300802F00000099030080350000009A030080410000009B0300804A0000009C030080570000009D0300805B0000009E0300806A0000009F0300800500060009003E00090032000900340009002A000900250009002500090032000900300009003700090026000900250009005B0005000600F2000000C000000070380000010001006C000000000000000E000000B400000000000000A203008001000000A303008008000000A50300800E000000A60300801A000000A703008023000000A80300802B000000A90300802F000000AB03008035000000AC03008041000000AD0300804A000000AE03008057000000AF0300805B000000B10300806A000000B20300800500060009003E00090032000900330009002A000900290009002500090032000900300009003700090026000900250009005A0005000600F2000000C0000000DC380000010001006C000000000000000E000000B400000000000000B503008001000000B603008008000000B80300800E000000B90300801A000000BA03008023000000BB0300802B000000BC0300802F000000BE03008035000000BF03008041000000C00300804A000000C103008057000000C20300805B000000C30300806A000000C40300800500060009003E00090032000900300009002A00090026000900250009003200090030000900370009002600090025000900570005000600F2000000C000000048390000010001006C000000000000000E000000B400000000000000C703008001000000C803008008000000CA0300800E000000CB0300801A000000CC03008023000000CD0300802B000000CE0300802F000000D003008035000000D103008041000000D20300804A000000D303008057000000D40300805B000000D50300806A000000D60300800500060009003E00090032000900360009002A000900250009002500090032000900300009003700090026000900250009005D0005000600F200000008010000B439000001000100700000000000000014000000FC00000000000000D903008001000000DA03008007000000DC0300800D000000DD03008015000000DE0300801D000000DF03008025000000E00300802D000000E20300802E000000E203008032000000EEEFFE8034000000E203008039000000E30300803A000000E403008041000000EEEFFE8045000000E503008053000000E603008054000000EEEFFE8058000000E20300805E000000E80300806D000000E90300800500060009002E0009002B0009001F000900270009003700090020000900100027003100000000001200230009000A000D001E000000000011002A0009000A0000000000240026000900290005000600F200000038010000243A0000010001008C00000000000000180000002C01000000000000EC03008001000000ED03008007000000EE0300800D000000EF03008014000000F10300801A000000F203008022000000F30300802A000000F403008032000000F50300803A000000F60300803B000000F603008041000000EEEFFE8043000000F60300804A000000F70300804B000000F803008052000000EEEFFE8056000000F903008064000000FA03008065000000EEEFFE806B000000F603008073000000FB0300807B000000FC03008084000000FD03008089000000FE0300800500060009002E0009003D000900420009002B0009001F000900270009003700090020000900100027003100000000001200230009000A000D001E000000000011002A0009000A00000000002400260009003A00090020000900190005000600F200000014010000B03A000001000100A50000000000000015000000080100000000000001040080010000000204008007000000030400800D0000000504008013000000070400801B00000009040080230000000A0400802C0000000C0400802D0000000C0400803A000000EEEFFE803C0000000C0400804A0000000D0400804B0000000E040080630000000F040080640000000C0400806F000000EEEFFE8084000000EEEFFE80850000001004008091000000110400809D00000012040080A200000013040080050006000900330009002E00090062000900340009003A00090020000900100020002D000000000012001C0009000A000D00300009000A001D001F00000000000000000009001F00090022000900180005000600F200000058020000553B0000010001002605000000000000300000004C0200000000000016040080010000001704008004000000EEEFFE8008000000EEEFFE80600300001A0400806C0300001B040080740300001E040080800300001F040080880300002204008094030000230400809C03000026040080A803000027040080B00300002A040080BD0300002B040080C60300002E040080D30300002F040080DC03000032040080E903000033040080F203000036040080FF03000037040080080400003A040080150400003B0400801E0400003E0400802B0400003F040080340400004204008041040000430400804A040000460400805704000047040080600400004A0400806D0400004B040080760400004E040080830400004F0400808C040000520400809904000053040080A204000056040080AF04000057040080B50400005A040080C20400005B040080C80400005E040080D50400005F040080DB04000062040080E804000063040080EE04000066040080FB04000067040080010500006A0400800E0500006B040080140500006E04008023050000700400800500060009001A00000000000000000011007700110027001100670011002700110060001100230011005A001100200011006C0011002900110060001100230011006700110027001100680011002700110069001100200011007C0011002A001100730011002F001100760011002A0011007E0011002E00110075001100260011005900110022001100570011002100110059001100220011005B001100230011006100110026001100530011001F00110059001100220011003E0005000600F2000000C00000007B4000000100010055000000000000000E000000B400000000000000730400800100000074040080070000007504008008000000750400800F000000EEEFFE8011000000750400801D000000760400801E00000077040080300000007804008031000000750400803B000000EEEFFE804C000000EEEFFE804D00000079040080520000007A04008005000600090030000900100022002A000000000012001E0009000A000D00340009000A001F00210000000000000000000900190005000600F2000000C0000000D04000000100010055000000000000000E000000B4000000000000007D040080010000007E040080070000007F040080080000007F0400800F000000EEEFFE80110000007F0400801D000000800400801E000000810400803000000082040080310000007F0400803B000000EEEFFE804C000000EEEFFE804D0000008304008052000000840400800500060009002D000900100022002A000000000012001E0009000A000D00340009000A001F00210000000000000000000900180005000600F2000000D401000025410000010001006A0100000000000025000000C801000000000000870400800100000088040080090000008A0400800A0000008A04008011000000EEEFFE80160000008A040080220000008B040080230000008C040080790000008E0400807F00000090040080B3000000EEEFFE80B700000091040080B800000092040080B900000092040080E3000000EEEFFE80E500000092040080EC00000093040080ED00000094040080FA000000EEEFFE80FE00000095040080FF0000009604008008010000980400801101000099040080120100009A04008013010000EEEFFE801901000092040080210100009C0400802E0100009D040080310100009F04008032010000A00400803E010000A10400803F010000A2040080400100008A0400804D010000EEEFFE8061010000EEEFFE8062010000A404008067010000A504008005000600090052000900100022002A000000000012001E0009000A000D00D6000D0037000D007700000000000D000E00110018002D005C00000000001A0029001100120015002F00000000001500160019003B00190032001500160011001200000000002A002C00110046000D000E000D000E00110037000D000E0009000A001F002100000000000000000009001B0005000600F2000000D40100008F420000010001006A0100000000000025000000C801000000000000A804008001000000A904008009000000AB0400800A000000AB04008011000000EEEFFE8016000000AB04008022000000AC04008023000000AD04008079000000AF0400807F000000B1040080B3000000EEEFFE80B7000000B2040080B8000000B3040080B9000000B3040080E3000000EEEFFE80E5000000B3040080EC000000B4040080ED000000B5040080FA000000EEEFFE80FE000000B6040080FF000000B704008008010000B904008011010000BA04008012010000BB04008013010000EEEFFE8019010000B304008021010000BD0400802E010000BE04008031010000C004008032010000C10400803E010000C20400803F010000C304008040010000AB0400804D010000EEEFFE8061010000EEEFFE8062010000C504008067010000C604008005000600090052000900100022002A000000000012001E0009000A000D00D6000D0037000D007700000000000D000E00110018002D005C00000000001A0029001100120015002F00000000001500160019003A00190031001500160011001200000000002A002C00110046000D000E000D000E00110037000D000E0009000A001F002100000000000000000009001B0005000600F2000000E4000000F9430000010001004D0100000000000011000000D800000000000000C904008001000000CA04008009000000CB0400800A000000CB04008011000000EEEFFE8016000000CB04008022000000CC04008023000000CD0400802F000000CE04008064000000CF040080C3000000D004008022010000D104008023010000CB04008030010000EEEFFE8044010000EEEFFE8045010000D20400804A010000D304008005000600090042000900100022002A000000000012001E0009000A000D0032000D0077000D00C2000D00C20009000A001F002100000000000000000009001B0005000600F2000000F00000004645000001000100A40100000000000012000000E400000000000000D604008001000000D704008009000000D80400800A000000D804008011000000EEEFFE8016000000D804008022000000D904008023000000DA04008078000000DB04008084000000DC040080B9000000DD04008019010000DE04008079010000DF0400807A010000D804008087010000EEEFFE809B010000EEEFFE809C010000E0040080A1010000E104008005000600090042000900100022002A000000000012001E0009000A000D00D6000D0031000D0077000D00C2000D00C20009000A001F002100000000000000000009001B0005000600F200000074010000EA46000001000100ED010000000000001D0000006801000000000000E404008001000000E504008009000000E70400800A000000E704008011000000EEEFFE8016000000E704008022000000E804008023000000E904008056000000EEEFFE8059000000EA0400807C000000EC0400808D000000EE040080C1000000EEEFFE80C5000000EF040080E8000000F1040080F4000000F304008028010000EEEFFE802C010000F40400804F010000F60400805B010000F80400808F010000EEEFFE8093010000F9040080B6010000FB040080C2010000FC040080C3010000E7040080D0010000EEEFFE80E4010000EEEFFE80E5010000FE040080EA010000FF0400800500060009004E0009001000220027000000000012001E0009000A000D0079000000000011006300110044000D007900000000001100630011003F000D007300000000001100600011003F000D006F000000000011005C0011003D0009000A001F002100000000000000000009001D0005000600F200000074010000D748000001000100E9010000000000001D000000680100000000000002050080010000000305008009000000050500800A0000000505008011000000EEEFFE8016000000050500802200000006050080230000000705008056000000EEEFFE8059000000080500807C0000000A050080890000000C050080BD000000EEEFFE80C10000000D050080E40000000F050080F00000001105008024010000EEEFFE8028010000120500804B0100001405008057010000160500808B010000EEEFFE808F01000017050080B201000019050080BE0100001A050080BF01000005050080CC010000EEEFFE80E0010000EEEFFE80E10100001C050080E60100001D050080050006000900520009001000220027000000000012001E0009000A000D007900000000001100630011002F000D007900000000001100630011003F000D007300000000001100600011003F000D006F000000000011005C0011003D0009000A001F002100000000000000000009001D0005000600F2000000E4000000C04A0000010001004D0100000000000011000000D80000000000000020050080010000002105008009000000220500800A0000002205008011000000EEEFFE801600000022050080220000002305008023000000240500802F000000250500806400000026050080C3000000270500802201000028050080230100002205008030010000EEEFFE8044010000EEEFFE8045010000290500804A0100002A05008005000600090042000900100022002A000000000012001E0009000A000D0036000D0077000D00C2000D00C20009000A001F002100000000000000000009001B0005000600F2000000FC0000000D4C000001000100B90000000000000013000000F0000000000000002D050080010000002E050080070000002F050080080000002F0500800F000000EEEFFE80110000002F0500801D000000300500801E00000031050080260000003205008032000000330500804E000000340500806A0000003505008086000000360500808E000000370500808F0000002F0500809C000000EEEFFE80B0000000EEEFFE80B100000038050080B6000000390500800500060009004F000900100022002A000000000012001E0009000A000D0046000D0032000D0045000D0041000D0041000D00290009000A001F002100000000000000000009001C0005000600F2000000D8000000C64C0000010001007D0000000000000010000000CC000000000000003C050080010000003D050080070000003E050080130000003F050080140000003F05008020000000EEEFFE80220000003F0500802E000000400500802F000000410500804700000042050080480000003F05008052000000EEEFFE8063000000EEEFFE80640000004305008070000000440500807A000000450500800500060009003400090035000900100020002C000000000012001C0009000A000D00470009000A001D001F000000000000000000090036000900220005000600F2000000E4000000434D0000010001007B0000000000000011000000D800000000000000480500800100000049050080070000004A050080130000004B05008018000000EEEFFE801B0000004C0500801C0000004C05008023000000EEEFFE80250000004C0500802D0000004D0500802E0000004E050080470000004F050080480000004C05008053000000EEEFFE8062000000500500806E00000051050080780000005205008005000600090034000900350009001E00000000000D001400210029000000000016001D000D000E00110048000D000E001E00200000000000090036000900220005000600F20000003C000000BE4D00000100010011000000000000000300000030000000000000005505008001000000560500800F00000057050080050006000900420005000600F2000000D8000000CF4D0000010001006F0000000000000010000000CC000000000000005A050080010000005B050080070000005C050080140000005D050080150000005D0500801C000000EEEFFE801E0000005D0500802A0000005E0500802B0000005F0500803800000060050080390000005D05008043000000EEEFFE8054000000EEEFFE80550000006105008062000000620500806C0000006305008005000600090034000900300009001000220027000000000012001E0009000A000D002A0009000A001F0021000000000000000000090031000900220005000600F2000000CC0000003E4E0000010001005F000000000000000F000000C0000000000000006605008001000000670500800700000068050080130000006905008014000000690500801B000000EEEFFE801D00000069050080290000006A0500802A0000006B050080370000006C050080380000006905008042000000EEEFFE8056000000EEEFFE80570000006D0500805C0000006E0500800500060009003A000900420009001000220027000000000012001E0009000A000D00300009000A001F002100000000000000000009001F0005000600F2000000C00000009D4E00000100010060000000000000000E000000B4000000000000007005008001000000710500800700000072050080080000007205008014000000EEEFFE801600000072050080220000007305008023000000740500803B000000750500803C0000007205008046000000EEEFFE8057000000EEEFFE8058000000760500805D000000770500800500060009002D000900100020002C000000000012001C0009000A000D00330009000A001D001F0000000000000000000900160005000600F2000000CC000000FD4E0000010001005C000000000000000F000000C00000000000000079050080010000007A050080070000007B050080130000007C050080140000007C0500801B000000EEEFFE801D0000007C050080290000007D0500802A0000007E050080320000007F050080330000007C0500803D000000EEEFFE804E000000EEEFFE804F0000008005008059000000810500800500060009002D000900380009001000220026000000000012001E0009000A000D00230009000A001F002100000000000000000009001D0005000600F2000000E4000000594F000001000100370100000800000011000000D8000000000000006500008001000000660000800E00000068000080100000006A0000801E000000EEEFFE80210000006B000080340000006D0000803A0000006F00008070000000700000808200000071000080A400000073000080AF000000EEEFFE80B300000074000080C00000007600008018010000770000802A01000079000080360100007A0000800500060009005F0009001E0009002D00000000000D003A000900390009000B01090036000900820009002B00000000000D006400090002010900380009003A0005000600F20000006C0000009050000001000100A0000000080000000700000060000000000000007D000080010000007E000080070000008000008019000000810000807B000000820000848D000000880000809F0000008900008005000600090039000900380009003001090023000900490005000600F2000000780000003051000001000100C900000010000000080000006C0000000000000029000080010000002A000080070000002C0000803C0000002D0000804E0000002E000080AA0000002F000080BC00000031000080C800000032000080050006000900390009001A010900660009004C0109003B0009003A0005000600F2000000A8000000F95100000100010016010000100000000C0000009C0000000000000035000080010000003600008007000000380000801900000039000080670000003A00008479000000400000808B000000420000809100000044000080A300000045000080F100000046000084030100004C000080150100004D00008005000600090039000900380009004801090023000900490009002B00090035000900EA00090023000900490005000600F2000000780000000F53000001000100C900000018000000080000006C0000000000000029000080010000002A000080070000002C0000803C0000002D0000804E0000002E000080AA0000002F000080BC00000031000080C800000032000080050006000900390009001B010900660009004E0109003B0009003A0005000600F2000000A8000000D85300000100010016010000180000000C0000009C0000000000000035000080010000003600008007000000380000801900000039000080670000003A00008479000000400000808B000000420000809100000044000080A300000045000080F100000046000084030100004C000080150100004D00008005000600090039000900380009004A01090023000900490009002B00090035000900EB00090023000900490005000600F200000078000000EE54000001000100BE00000020000000080000006C000000000000002200008001000000230000800700000025000080310000002600008043000000270000809F00000028000080B10000002A000080BD0000002B00008005000600090039000900DB000900BB000900F20009003B0009003A0005000600F20000006C000000AC550000010001008C000000200000000700000060000000000000002E000080010000002F00008007000000310000801900000032000080670000003300008479000000390000808B0000003A000080050006000900390009003D000900E200090023000900490005000600F2000000840000003856000001000100D0000000280000000900000078000000000000002200008001000000230000800700000025000080310000002600008043000000270000805500000028000080B100000029000080C30000002B000080CF0000002C00008005000600090039000900E200090085000900A5000900050109003C0009003A0005000600F20000006C00000008570000010001008C000000280000000700000060000000000000002F0000800100000030000080070000003200008019000000330000806700000034000084790000003A0000808B0000003B000080050006000900390009003A000900F800090023000900490005000600F2000000E40000009457000001000100370100003000000011000000D8000000000000004800008001000000490000800E0000004B000080100000004D0000801E000000EEEFFE80210000004E00008034000000500000803A0000005200008070000000530000808200000054000080A400000056000080AF000000EEEFFE80B300000057000080C000000059000080180100005A0000802A0100005C000080360100005D000080050006000900650009001E0009003300000000000D0040000900390009000501090036000900810009002B00000000000D004A000900F9000900380009003A0005000600F20000006C000000CB58000001000100A000000030000000070000006000000000000000600000800100000061000080070000006300008019000000640000807B000000650000848D0000006B0000809F0000006C00008005000600090039000900380009001F01090023000900490005000600F2000000800100006B59000001000100DB000000380000001E000000740100000000000009000080010000000A000080090000000C0000800F0000000E0000801B0000001000008022000000EEEFFE80250000001100008026000000120000803400000013000080420000001400008043000000150000804A000000EEEFFE804D000000160000804E000000170000805C000000180000806A000000190000806B0000001A00008073000000EEEFFE80770000001B000080830000001C0000808B000000EEEFFE808F0000001D0000809B0000001E000080A5000000EEEFFE80A90000001F000080B500000020000080BF000000EEEFFE80C300000021000080CF00000023000080DA000000240000800500060009004E00090042000900390009005F000000000009000A000D005F000D006A0009000A0009006B000000000009000A000D0066000D00710009000A0009005900000000000D00450009006500000000000D004A0009006700000000000D004B0009006500000000000D004A0009002A0005000600F2000000BC010000465A0000010001001B0100004000000023000000B001000000000000080000800100000009000080070000000A000080140000000C000080160000000E000080220000000F00008027000000EEEFFE802A000000100000802B000000110000803C000000120000804D000000130000804E000000150000805A000000160000805F000000EEEFFE80620000001700008063000000180000807400000019000080850000001A000080860000001C000080920000001D00008098000000EEEFFE809C0000001E000080AB00000020000080B700000021000080BD000000EEEFFE80C100000022000080D000000024000080DC00000025000080E2000000EEEFFE80E600000026000080F500000028000080010100002900008007010000EEEFFE800B0100002A0000801A0100002B000080050006000900350009002E000900240009005300090021000000000009000A000D0085000D00900009000A0009005A00090021000000000009000A000D008C000D00970009000A000900510009002100000000000D0060000900560009002100000000000D0065000900570009002100000000000D0066000900560009002100000000000D00650005000600F2000000CC000000615B00000100010037010000480000000F000000C0000000000000003F000080010000004100008015000000430000801B000000450000802D00000046000080510000004700008072000000480000808F0000004A0000809B0000004D000080A10000004F000080B300000050000080DE0000005100008006010000520000802A010000540000803601000055000080050006000900540009003900090038000900A4000900850009007D000900390009002B00090038000900CD000900C6000900A2000900390005000600F200000048000000985C0000010001001E00000050000000040000003C0000000000000019000080010000001A000080070000001B0000801D0000001D0000800500060009001E000900720005000600F20000003C000000B65C00000100010012000000500000000300000030000000000000002C000080010000002D000080110000002E000080050006000900730005000600F20000004C050000C85C00000100010099040000500000006F000000400500000000000038000080010000003A00008017000000EEEFFE801D0000003B0000801E0000003D000080290000003E0000802F0000003F0000803500000041000080360000004300008053000000EEEFFE8057000000440000806F00000046000080770000004700008094000000EEEFFE809800000048000080B00000004A000080B80000004E000080CA0000004F000080D800000050000080D900000050000080E2000000EEEFFE80E400000050000080F200000051000080F3000000520000800C010000530000800D0100005000008018010000EEEFFE802D010000EEEFFE802E01000054000080370100005500008043010000EEEFFE8047010000560000805A01000059000080610100005A0000806F0100005B000080790100005C000080820100005D000080890100005E000080920100005F0000809C01000061000080A301000063000080AA01000064000080AD01000065000080B401000066000080BB01000067000080C201000068000080C301000068000080D1010000EEEFFE80D601000068000080E401000069000080E50100006A000080EC0100006B000080FF0100006C000080120200006E000080190200006F0000803202000070000080440200007100008057020000730000807B02000074000080B502000075000080B602000075000080BF020000EEEFFE80C102000075000080CF02000076000080D002000077000080DE0200007800008104030000EEEFFE80080300007A000080090300007B000080170300007C000080220300007D000080230300007E00008024030000750000802F030000EEEFFE8044030000EEEFFE80450300008000008059030000810000806F0300008200008070030000680000807E030000EEEFFE8093030000EEEFFE8094030000870000809B03000088000080A203000089000080A90300008A000080AC0300008B000080AD0300008B000080BB030000EEEFFE80BD0300008B000080CB0300008C000080CC0300008D000080170400008F00008021040000900000802A040000910000803704000092000080380400008B00008046040000EEEFFE805B040000EEEFFE805C04000093000080630400009600008066040000970000806804000098000080690400009900008076040000EEEFFE80780400009C000080790400009D0000808A040000EEEFFE808E0400009E00008095040000A000008097040000A100008098040000A30000800500060009003E000000000009000A000D004F000D003C000D003B000D000E0011005000000000001500600015003A0011004C000000000015005B001500370011007D0011005C001100180037004900000000001A00330011001200150062001100120034003600000000000000000011003F0011002B000000000015005100110037001100A20011003C001100280011001D00110042001100290011001E0011002D001100300011003F0011003A0011003D00110018002F003D00000000001A002B001100120015003A001500590015004F001500350015005400150083001500720015005B0015006F0015001C003C004F00000000001E003800150016001900500019004C000000000019001A001D0038011D004A0019001A001500160039003B00000000000000000015004B0015005C00110012002C002E00000000000000000011001D0011008C0011002C00110031001100180028003600000000001A002400110012001500AE001500400015003600150043001100120025002700000000000000000011001E000D000E000D0022000D000E0011004000000000000D000E0011004A0000000000150022000D000E0009000A0005000600F20000004806000061610000010001007805000050000000840000003C06000000000000AD00008001000000AE00008017000000EEEFFE801D000000AF0000801E000000B100008029000000B20000802F000000B300008035000000B500008036000000B700008053000000EEEFFE8057000000B80000806F000000BA00008077000000BB00008094000000EEEFFE8098000000BC000080B0000000BE000080B8000000C2000080CA000000C3000080D8000000C4000080D9000000C4000080E2000000EEEFFE80E4000000C4000080F2000000C5000080F3000000C60000800C010000C70000800D010000C400008018010000EEEFFE802D010000EEEFFE802E010000C800008037010000C900008043010000EEEFFE8047010000CA0000805A010000D400008061010000D50000806F010000D600008079010000D700008082010000D800008089010000D900008092010000DA0000809C010000DC000080A3010000DE000080AA010000DF000080AD010000E0000080B0010000E2000080B1010000E2000080BF010000EEEFFE80C4010000E2000080D2010000E3000080D3010000E4000080DA010000E5000080F3010000E6000080FA010000E700008022020000E800008034020000E900008047020000EC0000806B020000ED000080A5020000EE000080A6020000EE000080AF020000EEEFFE80B4020000EE000080C2020000EF000080C3020000F0000080D1020000F2000080EB020000F30000800A030000F400008022030000EEEFFE8026030000F50000802A030000F600008043030000EEEFFE8047030000F70000804B030000F90000804E030000EEEFFE8050030000FA00008051030000FB00008058030000EEEFFE805C030000FC00008060030000FD00008061030000FD0000806A030000EEEFFE806C030000FD0000807A030000FE0000807B030000FF00008087030000EEEFFE808B030000000100809A030000020100809B030000FD000080A6030000EEEFFE80BB030000EEEFFE80BC03000003010080BD030000F9000080C3030000F9000080CD030000EEEFFE80D403000007010080D5030000EE000080E3030000EEEFFE80F8030000EEEFFE80F9030000090100800D0400000A0100804E0400000B0100804F040000E20000805D040000EEEFFE8072040000EEEFFE8073040000100100807A04000011010080810400001201008088040000130100808B040000140100808C040000140100809A040000EEEFFE809C04000014010080AA04000015010080AB04000016010080F6040000180100800005000019010080090500001A010080160500001B010080170500001401008025050000EEEFFE803A050000EEEFFE803B0500001C010080420500001F01008045050000200100804705000021010080480500002201008055050000EEEFFE805705000025010080580500002601008069050000EEEFFE806D050000270100807405000029010080760500002A010080770500002C0100800500060009003E000000000009000A000D004F000D003C000D003B000D000E0011005000000000001500600015003A0011004C000000000015005B001500370011007D0011005C001100180037004900000000001A00330011001200150062001100120034003600000000000000000011003F0011002B000000000015005100110037001100A10011003C001100280011001D00110042001100290011001E0011002D001100300011003300110018002F003D00000000001A002B001100120015003500150054001500380015007500150083001500720015005B0015006F0015001C003C004F00000000001E00380015001600190050001D0097001D00A6001D00580000000000210042001D005800000000002100420022002B00000000001D001E0021002C0000000000250046002100280041004D00000000002A003D0021002200250048000000000029005900210022003E00400000000000000000001D001E00350038002D003300000000001500160039003B00000000000000000015004B001500BD00110012002C002E00000000000000000011001D0011008C0011002C00110031001100180028003600000000001A002400110012001500AE001500400015003600150043001100120025002700000000000000000011001E000D000E000D0022000D000E0011004000000000000D000E0011004A0000000000150022000D000E0009000A0005000600F200000058050000D9660000010001009603000050000000700000004C050000000000003101008001000000330100800C000000340100800E000000350100801A0000003601008020000000370100802600000039010080270000003C010080280000003D01008035000000400100804E0000004101008058000000420100806100000043010080680000004401008075000000450100807C0000004601008084000000480100808B000000490100808E0000004C010080A50000004D010080AC0000004E010080B3000000EEEFFE80B70000004F010080B800000051010080B900000052010080C700000053010080CA00000054010080CB00000055010080CC00000056010080D300000057010080D600000058010080D70000005A010080E5000000EEEFFE80EC0000005B010080ED0000005C010080FB000000EEEFFE80FF0000005D010080000100005E0100800B01000060010080290100006201008038010000640100803F010000EEEFFE804301000065010080440100006701008062010000680100807101000069010080720100006B01008079010000EEEFFE807D0100006C0100807E0100006E0100809701000070010080A4010000EEEFFE80A801000071010080A901000073010080BD01000074010080BE01000075010080C701000076010080C801000078010080CE0100007A010080CF0100007D010080DD010000EEEFFE80E40100007E010080E5010000800100800302000081010080120200008201008019020000EEEFFE801D020000830100801E020000850100803C020000860100804B020000870100804C0200008801008053020000EEEFFE805702000089010080580200008A010080630200008D0100807A0200008E010080840200009001008091020000EEEFFE8095020000910100809602000093010080A902000094010080AA02000095010080CC02000096010080CD02000098010080CE02000099010080D5020000EEEFFE80D90200009A010080DA0200009B010080E30200009D010080110300009E0100801B0300009F01008024030000A00100802B030000A101008038030000A20100803F030000A301008040030000A501008043030000A601008045030000A701008046030000A801008053030000AA01008056030000AB01008058030000AC01008059030000AD01008066030000EEEFFE8068030000B101008069030000B20100807A030000EEEFFE807E030000B301008085030000B50100808C030000B601008093030000B801008095030000B90100800500060009004B0009001F000900190009002C0009002E0009000A000D000E0011002F001100740011003C001100280011001D0011003A0011001E001100270011002D00110027001100510011002A0011002A0000000000110012001500160019003F001500160015001A001500160019002D001500160011001200110035000000000011001200150033000000000015001600190064001500670015004B0015002800000000001500160019006B0019004F001500160015002800000000001500160019007A0019003E000000000019001A001D006F0019001A00190025001500160011001200110012001500330000000000150016001900710019005D00190032000000000019001A001D0075001D00590019001A00190032000000000019001A001D0056001900770019003A0019003E000000000019001A001D006E0019001A001900460015001600110012001100240000000000110012001500490015009B001500350015002C00150021001500390015002200110012000D000E000D0021000D000E0011003F0009000A0009001E0009000A000D003C000000000009000A000D0046000000000011001E000D001C000D001E0009000A0005000600F2000000080700006F6A000001000100120600005000000094000000FC06000000000000BD01008001000000C501008017000000EEEFFE801D000000C60100801E000000C801008029000000C90100802F000000CA01008035000000CC01008036000000CE01008053000000EEEFFE8057000000CF0100806F000000D101008077000000D201008094000000EEEFFE8098000000D3010080B0000000D5010080B8000000D9010080CA000000DA010080D8000000DB010080D9000000DB010080E2000000EEEFFE80E4000000DB010080F2000000DC010080F3000000DD0100800C010000DE0100800D010000DB01008018010000EEEFFE802D010000EEEFFE802E010000DF01008037010000E001008043010000EEEFFE8047010000E10100805A010000EB01008061010000EC0100807C010000ED01008086010000EE0100808F010000EF01008096010000F00100809F010000F1010080A9010000F3010080B0010000F5010080B7010000F6010080BA010000F8010080BB010000F8010080C9010000EEEFFE80CE010000F8010080DC010000F9010080DD010000FA010080E4010000FB010080FD010000FC0100800F020000FD01008022020000FF0100804602000000020080800200000102008081020000010200808A020000EEEFFE808F020000010200809D020000020200809E02000003020080AC02000004020080C0020000EEEFFE80C702000005020080C802000006020080D802000007020080E602000008020080F002000009020080FF0200000A0200800D0300000B0200802E0300000C0200803D0300000D0200804B0300000E0200806C0300000F0200807B0300001002008089030000110200809603000012020080A503000013020080B303000014020080C003000015020080CF03000016020080DD03000017020080EA03000018020080F903000019020080030400001A020080090400001C0200800A0400001D0200801F0400001E02008039040000200200803C040000EEEFFE803E040000210200803F0400002202008046040000EEEFFE804A040000230200804E040000240200804F0400002402008058040000EEEFFE805A040000240200806804000025020080690400002602008075040000EEEFFE8079040000270200808804000029020080890400002402008094040000EEEFFE80A9040000EEEFFE80AA0400002A020080AB04000020020080B104000020020080BB040000EEEFFE80C20400002C020080C30400002E020080C404000001020080D2040000EEEFFE80E7040000EEEFFE80E804000030020080FC04000031020080FD040000F80100800B050000EEEFFE8020050000EEEFFE80210500003602008028050000370200802F050000380200803605000039020080390500003A0200803A0500003A02008048050000EEEFFE804A0500003A020080580500003B020080590500003C020080930500003E0200809D0500003F020080A605000040020080B305000041020080B40500003A020080BF050000EEEFFE80D4050000EEEFFE80D505000042020080DC05000045020080DF05000046020080E105000047020080E205000048020080EF050000EEEFFE80F10500004B020080F20500004C02008003060000EEEFFE80070600004D0200800E0600004F020080100600005002008011060000510200800500060009003F000000000009000A000D004F000D003C000D003B000D000E0011005000000000001500600015003A0011004E000000000015005D001500380011007D0011005C001100180037004900000000001A00330011001200150062001100120034003600000000000000000011003F0011002B000000000015005100110037001100B70011003C001100280011001D00110042001100290011001E0011002D0011003000110018002F003D00000000001A002B00110012001500350015005400150083001500720015005B0015006F0015001C003C004F00000000001E0038001500160019005000190036000000000019001A001D0072001D0053001D0031001D0041001D004D001D004A001D0041001D0051001D004A001D0041001D0051001D002E001D0041001D004E001D0032001D0041001D004D001D0032001D0041001D00450019001A0019001A001D0087001D00960022002B00000000001D001E0021002C0000000000250046002100280041004D00000000002A003D0021002200250048000000000029005900210022003E00400000000000000000001D001E00350038002D0033000000000019001A001500160039003B00000000000000000015004B00110012002C002E00000000000000000011001D001100770011002C00110031001100180028003600000000001A00240011001200150090001500400015003600150043001100120025002700000000000000000011001E000D000E000D0022000D000E0011004000000000000D000E0011004A0000000000150022000D000E0009000A0005000600F20000005409000081700000010001008306000050000000C500000048090000000000005702008001000000580200801200000059020080230000005A020080340000005C020080450000005D020080570000005E02008069000000600200807C000000610200808F00000062020080A100000064020080B400000065020080C900000066020080DD00000068020080F400000069020080020100006A020080110100006B02008018010000EEEFFE801F0100006C020080200100006D020080270100006E020080320100007002008035010000EEEFFE803A010000710200803B0100007202008046010000730200804A010000EEEFFE805101000074020080520100007502008065010000760200806C010000EEEFFE807301000077020080740100007802008080010000EEEFFE808701000079020080880100007A02008096010000EEEFFE809A0100007B0200809B0100007C020080A50100007D020080AB0100007F020080AC01000080020080C001000081020080D901000083020080E001000084020080E101000084020080E8010000EEEFFE80EA01000084020080F101000085020080F201000086020080FC01000087020080FD010000EEEFFE8003020000840200800B020000890200800C0200008902008013020000EEEFFE8015020000890200801C0200008A0200801D0200008B02008037020000EEEFFE803B0200008C0200803C0200008D020080460200008E020080470200008F02008048020000EEEFFE804E0200008902008056020000920200807E020000930200807F0200009402008080020000950200808302000097020080840200009802008091020000990200809B0200009A020080AA0200009B020080AB0200009C020080AC0200009F020080B0020000EEEFFE80B7020000A0020080B8020000A1020080CB020000A2020080D2020000EEEFFE80D9020000A3020080DA020000A4020080E6020000EEEFFE80ED020000A5020080EE020000A6020080FC020000EEEFFE8000030000A702008001030000A80200800B030000A902008011030000AB02008012030000AC02008026030000AD0200803F030000AE02008048030000AF02008049030000AF02008050030000EEEFFE8052030000AF02008059030000B00200805A030000B102008074030000EEEFFE8078030000B202008079030000B302008083030000B402008084030000B502008085030000EEEFFE808B030000AF02008093030000B7020080BB030000B8020080BC030000B9020080BD030000BA020080C0030000BC020080C1030000BD020080CE030000BE020080D8030000BF020080E7030000C0020080E8030000C1020080E9030000C3020080EA03000070020080F003000070020080FD030000EEEFFE8004040000C50200800C040000C60200801F040000C702008032040000C902008046040000CA0200805A040000CC0200805E040000EEEFFE8065040000CD02008066040000CE02008072040000EEEFFE8079040000CF0200807A040000D00200808E040000D1020080A2040000D3020080AB040000D4020080AC040000D4020080B3040000EEEFFE80B5040000D4020080BC040000D5020080BD040000D6020080D7040000EEEFFE80DB040000D7020080DC040000D8020080E6040000D9020080E7040000DA020080E8040000EEEFFE80EE040000D4020080F6040000DB020080FC040000EEEFFE8000050000DC02008001050000DD0200802D050000DE02008030050000E002008031050000E10200803E050000E20200805C050000E30200806A050000E40200806B050000E50200806C050000E60200806D050000E802008071050000EEEFFE8078050000E902008079050000EA02008085050000EEEFFE808C050000EB0200808D050000EC020080A1050000ED020080B5050000EF020080BE050000F0020080BF050000F0020080C6050000EEEFFE80C8050000F0020080CF050000F1020080D0050000F2020080EA050000EEEFFE80EE050000F3020080EF050000F4020080F9050000F5020080FA050000F6020080FB050000EEEFFE8001060000F002008009060000F702008010060000EEEFFE8014060000F802008015060000F902008041060000FA02008044060000FC02008045060000FD02008052060000FE02008070060000FF0200807E060000000300807F06000001030080800600000203008081060000030300808206000004030080050006000900500009004E0009004300090049000900470009003C000900520009005200090049000900580009005800090049000900370009004A0009004C00090020000000000009000A000D0030000D00440012001B00000000000D000E001100320011001D0000000000110012001500560015002D00000000001500160019003E000000000019001A001D004900000000001D001E0021003F001D001E001D001E0021004B002100590021005600210028003E004D00000000002A003A002100220025004D0021002200000000003B003D002100280040004900000000002A003C002100220025007F000000000025002600290053002500260021002200000000003D003F00210082001D001E0019001A00150016001500160019004F001900370019004100150016001100120011001F00000000001100120015004F0015002F00000000001500160019003A000000000019001A001D004B00000000001D001E0021003D001D001E001D001E002100460021005A0021006700210028003F004700000000002A003B002100220025007C000000000025002600290051002500260021002200000000003C003E00210083001D001E0019001A00150016001500160019004800190035001900430015001600110012000D000E00330036001D003100000000000D0034000D0051000D004A000D0059000D005F000D001900000000000D000E0011003600000000001100120015004400150047001500560015001C0035004600000000001E0031001500160019006F000000000019001A001D00420019001A0015001600000000003200340015002800000000001500160019007E00150016001500160019005A001900640019003D0015001600110012000D000E000D001B00000000000D000E0011003800000000001100120015003C001500450015004E0015001C0036004300000000001E0032001500160019006D000000000019001A001D003F0019001A0015001600000000003300350015002A00000000001500160019007100150016001500160019005C001900620019003F0015001600110012000D000E0009000A0005000600F2000000300300000477000001000100DE010000500000004200000024030000000000000F030080010000001003008002000000100300800E000000EEEFFE8013000000100300801F0000001103008020000000120300802C0000001303008047000000140300804E000000150300805100000016030080540000001703008057000000180300805A000000EEEFFE805E000000EEEFFE809A0000001B030080A70000001C030080B10000001D030080B8000000EEEFFE80BC0000001E030080BD0000001F030080D000000020030080D400000021030080DC000000EEEFFE80E000000022030080EF00000023030080F200000025030080F300000026030080F600000027030080080100002803008009010000290300800B0100002C030080180100002D030080220100003203008029010000EEEFFE802D010000330300802E01000034030080310100003503008043010000360300804401000037030080460100003A030080530100003B0300805D0100003C03008064010000EEEFFE80680100003D030080690100003E0300806C0100003F0300807E010000400300807F0100004103008081010000440300808F010000EEEFFE80930100004503008094010000460300809B010000EEEFFE809F01000047030080A001000048030080AB01000049030080AE0100004B030080AF0100004C030080B80100004D030080B90100004E030080BA01000050030080BB01000010030080C8010000EEEFFE80DC010000EEEFFE80DD01000051030080050006000900100029003A00000000001200250009000A000D003A000D004C000D002C000D0028000D0025000D0024000D001E00000000000000000015005C001500450015002D00000000001500160019007B001900350019003700000000001D007700150016001500160019002A00190073001500160015001B0015005F001500450015002D00000000001500160019002A00190076001500160015001B00150064001500450015002D00000000001500160019002A0019007C001500160015001B000D002C00000000000D000E0011002B00000000001100120015003E00110012001100120015002F00110012000D000E0009000A0026002800000000000000000005000600F200000010020000E278000001000100D2010000500000002A00000004020000000000005503008001000000560300800A000000570300800B0000005703008017000000EEEFFE80190000005703008025000000580300802600000059030080340000005A030080410000005B0300804E0000005C0300804F0000005703008059000000EEEFFE806D000000EEEFFE806E0000005D03008072000000EEEFFE80790000005E0300807A0000005F0300808100000060030080A900000061030080BE000000EEEFFE80C200000062030080C300000063030080EB00000064030080EE00000066030080EF00000067030080FD000000680300801701000069030080250100006A030080260100006C0300803B010000EEEFFE803F0100006D030080400100006E0300807B0100006F0300807E010000710300807F010000720300808D01000073030080BA01000074030080C801000075030080C901000076030080CA01000077030080CF0100007803008005000600090037000900100025003600000000001200210009000A000D003F000D0022000D00270009000A0022002400000000000000000009001A000000000009000A000D0029000D0044000D003400000000000D000E00110056000D000E000D000E001100560011004E0011003A000D000E000D003600000000000D000E00110064000D000E000D000E0011005A0011005C0011003C000D000E0009000A000900130005000600F200000024030000B47A00000100010069020000500000004100000018030000000000007C030080010000007E030080020000007E0300800E000000EEEFFE80130000007E0300801F0000007F030080200000008003008031000000EEEFFE80370000008103008038000000820300803F00000083030080460000008503008047000000860300804F0000008703008052000000880300805300000089030080540000008A0300805B0000008B0300805E0000008C030080710000008D03008078000000EEEFFE807C0000008E0300807D0000008F0300808B000000EEEFFE808F00000090030080AB00000092030080B900000093030080BC00000095030080BD00000096030080CC00000097030080DA000000EEEFFE80DE00000098030080FA0000009A030080080100009B030080160100009C030080170100009D030080180100009E030080190100007E03008026010000EEEFFE803A010000EEEFFE803B010000A00300803F010000EEEFFE8046010000A103008047010000A20300805C010000EEEFFE8060010000A303008061010000A403008089010000A50300808C010000A70300808D010000A80300809B010000A9030080B5010000AA030080C3010000AB030080C4010000AD030080D9010000EEEFFE80DD010000AE030080DE010000AF03008019020000B00300801C020000B20300801D020000B30300802B020000B403008058020000B503008066020000B603008067020000B703008068020000B8030080050006000900100026003700000000001200220009000A000D003400000000000D000E001100300011002D00110012001500370011001200110028001100120015002C0011001200110045001100260000000000110012001500310000000000190049001900340011001200110012001500480015003100000000001900440019002F0015003300110012000D000E0009000A0023002500000000000000000009001A000000000009000A000D003900000000000D000E0011005B000D000E000D000E001100580011004F0011003F000D000E000D003B00000000000D000E00110069000D000E000D000E0011005C0011005D00110041000D000E0009000A0005000600F2000000F40500001D7D00000100010039040000500000007D000000E805000000000000BC03008001000000C503008012000000C60300801A000000C70300802B000000C803008033000000C903008045000000CA0300804F000000CD03008050000000CD03008058000000EEEFFE805D000000CD0300806B000000CE0300806C000000CF03008089000000D10300809C000000D2030080A6000000D3030080AD000000EEEFFE80B1000000D4030080B2000000D6030080C5000000D7030080CF000000D8030080D0000000D9030080D7000000EEEFFE80DB000000DA030080DC000000DB030080F7000000DC03008000010000DD03008003010000DF03008004010000E10300800D010000E20300800E010000E30300800F010000CD0300801D010000EEEFFE8032010000EEEFFE8033010000E50300803C010000E60300803D010000E603008046010000EEEFFE8048010000E603008056010000E703008057010000E803008071010000E90300807A010000EA0300807B010000E603008086010000EEEFFE809B010000EEEFFE809C010000ED0300809D010000ED030080A5010000EEEFFE80AA010000ED030080B8010000EE030080B9010000EF030080D6010000F1030080E9010000F2030080F3010000F3030080FA010000EEEFFE80FE010000F4030080FF010000F603008012020000F70300801C020000F80300801D020000F903008024020000EEEFFE8028020000FA03008029020000FB03008044020000FC0300804D020000FD03008050020000FF03008051020000010400805A020000020400805B020000030400805C020000ED0300806A020000EEEFFE807F020000EEEFFE80800200000504008089020000060400808A0200000604008093020000EEEFFE809502000006040080A302000007040080A402000008040080BE02000009040080C70200000A040080C802000006040080D3020000EEEFFE80E8020000EEEFFE80E90200000D040080EA0200000D040080F3020000EEEFFE80F80200000D040080060300000E040080070300000F04008024030000110400803703000012040080410300001304008048030000EEEFFE804C030000140400804D0300001604008060030000170400806A030000180400806B0300001904008072030000EEEFFE80760300001A040080770300001B040080920300001C0400809B0300001D0400809E0300001F0400809F03000021040080A803000022040080A903000023040080AA0300000D040080B8030000EEEFFE80CD030000EEEFFE80CE03000025040080D803000026040080D903000026040080E2030000EEEFFE80E403000026040080F203000027040080F3030000280400800D04000029040080160400002A040080170400002604008022040000EEEFFE8037040000EEEFFE80380400002B04008005000600090044000900520009003C0009004A0009003E0009004C00090010002C003C00000000001200280009000A000D004F000D0061000D004D000D002800000000000D000E0011005E00110049000D000E000D002800000000000D000E001100800011003A000D000E000D000E0011003B000D000E0009000A0029002B0000000000000000000900570009001000300046000000000012002C0009000A000D0085000D00350009000A002D002F0000000000000000000900100028003400000000001200240009000A000D004B000D005D000D0049000D002400000000000D000E0011005A00110045000D000E000D002400000000000D000E0011007800110036000D000E000D000E00110037000D000E0009000A0025002700000000000000000009004F00090010002C003E00000000001200280009000A000D0079000D00310009000A0029002B0000000000000000000900100029003600000000001200250009000A000D004C000D005E000D004A000D002500000000000D000E0011005B00110046000D000E000D002500000000000D000E0011007A00110037000D000E000D000E00110038000D000E0009000A0026002800000000000000000009005100090010002D004000000000001200290009000A000D007C000D00320009000A002A002C00000000000000000005000600F2000000B00100005681000001000100BB0100005000000022000000A4010000000000002F040080010000003004008015000000EEEFFE8018000000310400801900000032040080340000003304008038000000340400805400000035040080570000003704008058000000380400806400000039040080700000003A0400807D0000003B0400807E0000003D04008093000000EEEFFE80970000003E040080980000003F040080D100000040040080D400000042040080D500000043040080E2000000440400800D010000450400801B010000460400801C0100004804008031010000EEEFFE803501000049040080360100004A0400806F0100004B040080720100004D040080730100004E040080800100004F040080AB01000050040080B901000051040080BA010000540400800500060009003B000000000009000A000D0060000D0037000D005D0009000A0009000A000D004C000D0023000D00350009000A0009003A000000000009000A000D00620009000A0009000A000D004F000D004B000D00390009000A00090040000000000009000A000D00620009000A0009000A000D005F000D0055000D00430009000A0005000600F2000000880500001183000001000100B803000050000000740000007C050000000000006F04008001000000700400800D0000007104008018000000720400801A000000740400801B0000007504008021000000760400802700000079040080400000007A0400804A0000007B040080530000007C0400805A0000007D040080670000007E0400806E0000007F04008076000000800400809400000084040080A300000086040080AA000000EEEFFE80AE00000087040080AF00000089040080CD0000008A040080DC0000008B040080DD0000008D040080E4000000EEEFFE80E80000008E040080E90000008F040080F4000000EEEFFE80F500000094040080F60000009504008004010000960400800701000097040080080100009804008009010000990400800B0100009C040080120100009D040080150100009F0400802C010000A004008033010000EEEFFE8037010000A104008038010000A204008046010000EEEFFE804A010000A30400804B010000A40400804E010000A504008051010000A704008052010000A904008053010000AA04008061010000AB04008064010000AC04008065010000AD04008066010000AE04008071010000B10400807F010000EEEFFE8083010000B204008084010000B304008087010000B404008088010000B504008089010000B60400808A010000BB04008091010000EEEFFE8098010000BC04008099010000BD040080B7010000BF040080C6010000C0040080D9010000EEEFFE80DD010000C1040080DE010000C3040080FC010000C40400800B020000C50400800C020000C70400801F020000EEEFFE8026020000C804008027020000CA0400802A020000EEEFFE802F020000CB04008030020000CC0400803B020000D004008042020000D10400804F020000D20400805C020000D304008069020000D404008086020000D50400808F020000EEEFFE8093020000D60400809C020000D7040080AB020000D8040080AC020000CA040080B2020000CA040080BF020000EEEFFE80C6020000D9040080C7020000DA040080D0020000DB040080D3020000DD040080D4020000DE040081F3020000E0040080FA020000EEEFFE80FE020000E1040080FF020000E204008009030000E304008012030000EEEFFE8016030000E40400801F030000E50400802E030000E704008050030000E904008051030000EB04008052030000EF04008080030000F00400808A030000F104008093030000F20400809A030000F3040080A7030000F4040080AE030000F6040080B1030000F7040080B3030000F8040080B4030000F9040080B7030000FB040080050006000900190009004B000900210009000A000D0036000D0033000D0070000D0038000D0024000D0019000D0038000D001A000D0026000D006C000D0052000D002600000000000D000E001100690011004E000D000E000D002600000000000D000E0011005100000000000D000E0011003B000D000E000D0012000D000E00110017000D0029000D0024000D0054000D002700000000000D000E0011003F00000000001100120015002600110012001100120015001600190043001500160015001A001500160019004A0015003600000000001500160019002A0015001600110012000D000E000D001C00000000000D000E00110069001100580011004B00000000001100120015006D00150050001100120011004A0000000000110012001A0025000000000015001600190046001900440019003C0019003B001900410019008A0019002900000000001D004700190065001500160044004900270042000000000011001200110020000D000E000D000E0011005B0011002800000000001100120015003A0015002500000000001900400015005E0015004600110012000D000E000D0094000D002D000D0024000D0019000D0031000D001A0009000A0009001D0009000A000D00160005000600F2000000BC070000C9860000010001008005000050000000A3000000B0070000000000000C050080010000000D0500800C0000000E0500800E000000100500800F0000001105008015000000120500801B0000001505008034000000160500803E0000001705008047000000180500804E000000190500805B0000001A050080620000001B0500806A0000001C050080880000001D050080970000001F0500809E000000EEEFFE80A200000020050080A300000022050080C100000023050080D000000024050080D100000026050080D8000000EEEFFE80DC00000027050080DD00000028050080E80000002D050080EF0000002E050080FA0000003005008008010000EEEFFE800C010000310500800D010000320500800E0100003205008017010000EEEFFE80190100003205008027010000330500802801000034050080320100003505008033010000320500803E010000EEEFFE8053010000EEEFFE805401000036050080550100003A0500805C0100003C050080670100003D050080680100003D05008071010000EEEFFE80730100003D050080810100003E050080820100003F050080A201000040050080A30100003D050080AE010000EEEFFE80C3010000EEEFFE80C401000042050080C701000044050080D0010000EEEFFE80D701000045050080D801000046050080E801000047050080E901000047050080F2010000EEEFFE80F7010000470500800502000048050080060200004A0500801E0200004B0500802E0200004C05008035020000EEEFFE80390200004D0500803A0200004E0500804A0200004F0500804B0200005105008052020000EEEFFE805602000052050080570200005305008171020000550500807B02000057050080A002000058050080A102000059050080A202000047050080B0020000EEEFFE80C5020000EEEFFE80C60200005A050080C7020000EEEFFE80C80200005E050080C90200005F050080E502000060050080E802000061050080EA02000062050080EB02000063050080EE02000065050080EF0200006505008001030000EEEFFE800603000065050080140300006605008015030000690500802D0300006A0500803D0300006B05008044030000EEEFFE80480300006C050080490300006E050080590300006F0500805A0300007105008061030000EEEFFE806503000072050080660300007405008180030000760500808A03000077050080AF03000078050080B003000079050080B103000065050080BF030000EEEFFE80D4030000EEEFFE80D50300007D050080DD030000EEEFFE80E40300007E050080E503000081050080EC03000082050080F703000084050080F80300008405008001040000EEEFFE80060400008405008014040000850500801504000089050080160400008905008024040000EEEFFE802904000089050080370400008A050080380400008B050080500400008C0500805B0400008D05008062040000EEEFFE80660400008E05008067040000900500807F040000910500808A040000920500808B0400009305008092040000EEEFFE8096040000940500809704000096050080A704000097050081C104000099050080CB0400009A050080CC0400009B050080CD04000089050080DB040000EEEFFE80F0040000EEEFFE80F10400009C050080F20400008405008000050000EEEFFE8015050000EEEFFE80160500009D050080170500009F0500801A050000A305008048050000A405008052050000A50500805B050000A605008062050000A70500806F050000A805008076050000A905008079050000AA0500807B050000AB0500807C050000AC0500807F050000AE0500800500060009004B000900210009000A000D0036000D0033000D0070000D0038000D0024000D0019000D0038000D001A000D0026000D006E000D0053000D002700000000000D000E0011006B0011004F000D000E000D002700000000000D000E00110043000D0032000D0053000D002C00000000000D000E001100180039004F00000000001A0035001100120015004400110012003600380000000000000000000D000E000D003A000D0047000D0014002D00430000000000160029000D000E00110057000D000E002A002C0000000000000000000D0029000D001900000000000D000E00110066001100180034004E00000000001A003000110012001500550015005F00150029000000000015001600190063001500160015002900000000001500160019007F00190040001900690015001600110012003100330000000000000000000D000E00000000000D000E0011005E000D000E000D0021000D000E0011001A000D0014002F0052000000000016002B000D000E0011004C001100570011002500000000001100120015005B00110012001100250000000000110012001500820015003E0015006900110012000D000E002C002E0000000000000000000D001B00000000000D000E0011003400110055001100180036004A00000000001A0032001100120015001C002C004100000000001E002800150016001900750019005E00190037000000000019001A001D0079001D005A0019001A00190037000000000019001A001D0058001D0085001D00540019001A001500160029002B000000000000000000110012003300350000000000000000000D000E000D0025000D0094000D002D000D0024000D0019000D0031000D001A0009000A0009001D0009000A000D00160005000600F400000058000000140B0000000000006E0300000000000072040000000000007005000000000000700600000000000062070000000000006008000000000000D60B000000000000C60C0000000000005809000000000000BC0D000000000000B0020000A0080000D0080000E808000008090000200900005C0900007409000094090000AC090000CC090000E4090000040A00001C0A0000480A0000600A0000840A00009C0A0000C00A0000D80A0000FC0A0000140B0000340B00004C0B0000780B0000900B0000B40B0000CC0B0000EC0B0000040C0000200C0000380C0000540C00006C0C0000900C0000A80C0000D40C0000EC0C0000140D00002C0D0000580D0000700D0000940D0000AC0D0000CC0D0000E40D0000040E00001C0E00003C0E0000540E0000780E0000900E0000B00E0000C80E0000E80E0000000F0000240F00003C0F0000640F00007C0F0000A80F0000C00F0000E80F00000010000020100000381000005C1000007410000098100000B0100000D0100000E81000001011000028110000441100005C1100007C11000094110000B4110000CC110000E4110000FC110000141200002C120000441200005C1200007812000090120000B0120000C8120000E4120000FC12000018130000301300004C130000641300008013000098130000B8130000D0130000F41300000C1400002C14000044140000641400007C1400009C140000B4140000D4140000EC1400000C150000241500003C150000541500008015000098150000BC150000D4150000FC15000014160000341600004C1600007016000088160000AC160000C4160000E8160000001700001C17000034170000541700006C1700008C170000A4170000C8170000E017000000180000181800003818000050180000741800008C180000B4180000CC180000F818000010190000341900004C1900007019000088190000B4190000CC190000FC190000141A00003C1A0000541A0000841A00009C1A0000D01A0000E81A0000081B0000201B00003C1B0000541B0000701B0000881B0000B41B0000CC1B0000F01B0000081C0000341C00004C1C0000781C00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000003A002A110000000040010000000000006100000000000000000000007C000006B68C0000010000000047657453716C446174615265636F72640000001600031104000000FC00000061000000B68C0000010000000A0024115553797374656D00120024115553797374656D2E44617461000000001A0024115553797374656D2E446174612E53716C436C69656E740000220024115553797374656D2E546578742E526567756C617245787072657373696F6E73001E002411554D6963726F736F66742E53716C5365727665722E53657276657200220020110000000049000011000000000000000073716C446174615265636F7264000000020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D004400320000000402000004000000100000000200000005000000040600000C000000011E1601020006003A002A11000000007002000000000000E900000000000000000000007D000006178D00000100000000476574536561726368526573756C7473000000160003114401000024020000E9000000178D00000100000022002011000000004A000011000000000000000073716C436F6E6E656374696F6E0000001E002011010000004A000011000000000000000073716C436F6D6D616E64000022002011020000004A000011000000000000000073716C4461746152656164657200000022002011030000004A000011000000000000000073716C446174615265636F72640000000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C0000060406000018000000011E017801811E0181670281A20281DD0200060036002A110000000048030000000000004A00000000000000000000007E000006008E00000100000000506572666F726D5265706C616365001600031174020000080300004A000000008E00000100000022002011000000004B000011000000000000000073716C436F6E6E656374696F6E0000001E002011010000004B000011000000000000000073716C436F6D6D616E640000020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C000006040600000C000000011E0178020006003A002A110000000024040000000000003100000000000000000000007F0000064A8E00000100000000457865637574654E6F6E517565727900000000160003114C030000E4030000310000004A8E00000100000022002011000000004B000011000000000000000073716C436F6E6E656374696F6E0000001E002011010000004B000011000000000000000073716C436F6D6D616E640000020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C000006040600000C000000011E01780200060032002A1100000000E404000000000000A10000000000000000000000800000067B8E000001000000004765745265676578000000160003112804000098040000A10000007B8E00000100000022002011000000004C000011000000000000000072656765785061747465726E000000000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C000006040600001800000001813902100280E124815D02815D16010200060032002A1100000000A4050000000000007E0000000000000000000000810000061C8F00000100000000476574526567657800000016000311E8040000580500007E0000001C8F00000100000022002011000000004D000011000000000000000072656765785061747465726E000000000200060046000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C000006040600001800000001812D02100280D524815102815116010200060032002A11000000001806000000000000210000000000000000000000820000069A8F0000010000000047657452656765780000003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C000006040600000C000000021016010200060036002A1100000000CC060000000000002D000000000000000000000083000006BB8F000001000000004765744C696E6B5265676578000000160003111C060000880600002D000000BB8F0000010000001A002011000000004F00001100000000000000007061747465726E00020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C00000604060002100000000117022F16010000020006003A002A110000000080070000000000001A000000000000000000000084000006E88F00000100000000466F726D61745365617263685465726D00000016000311D0060000400700001A000000E88F0000010000001A002011000000000B00001100000000000000006F75747075740000020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C000006040600000C000000011716010200060032002A11000000002C080000000000001A00000000000000000000008500000602900000010000000048746D6C456E636F6465001600031184070000EC0700001A00000002900000010000001A002011000000000B00001100000000000000006F75747075740000020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C000006040600000C00000001171601020006003A002A1100000000EC08000000000000B40000000000000000000000860000061C9000000100000000436F6E76657274586D6C546F48746D6C0000001600031130080000A8080000B40000001C90000001000000220020110000000050000011000000000000000075706461746564537472696E67000000020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C00000604060002100000000117023D16010000020006003A002A1100000000AC090000000000008F000000000000000000000087000006D09000000100000000436F6E7665727448746D6C546F586D6C00000016000311F0080000680900008F000000D090000001000000220020110000000050000011000000000000000075706461746564537472696E67000000020006003E000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000007C00000604060002100000000117023D1601000002000600F200000048000000B68C0000010001006100000000000000040000003C000000000000000E000080010000000F0000865B000000170000805F0000001800008009000A000D000F000D00220009000A00F200000044010000178D000001000100E9000000000000001900000038010000000000001B000080010000001C0000800C0000001E00008013000000200000801B00000022000080220000002400008029000000260000802F0000002800008037000000EEEFFE803E000000290000803F000000EEEFFE80440000002B000080450000002C0000805D0000002D000080750000002E0000808D0000002F000080A500000030000080BD00000032000080C900000033000080CA0000002A000080D2000000EEEFFE80D900000034000080DA00000036000080E100000038000080E80000003900008009000A000D005A000D0043000D0035000D0022000D0046000D0046000D002700000000000D000E000000000011001200150049001500500015004F0015004F0015005300150043001100120011002D00000000000D000E000D0023000D00230009000A00F200000084000000008E0000010001004A000000000000000900000078000000000000003C000080010000003D0000800C0000003F00008013000000410000801B0000004200008034000000440000803B000000460000804200000048000080490000004900008009000A000D005A000D0043000D0035000D0056000D0022000D002A000D00230009000A00F2000000780000004A8E0000010001003100000000000000080000006C000000000000004C000080010000004D0000800C0000004F00008013000000510000801B0000005300008022000000550000802900000057000080300000005800008009000A000D005A000D0043000D0035000D0022000D002A000D00230009000A00F2000000440100007B8E000001000100A1000000000000001900000038010000000000005F000080010000006000008006000000EEEFFE800900000061000080100000006400008018000000670000801A000000EEEFFE801D000000680000802F0000006A000080310000006C00008034000000EEEFFE8037000000EEEFFE805B0000006F000080670000007000008069000000720000806B000000730000806D000000750000807E0000007600008080000000780000808200000079000080840000007B000080860000007C000080880000007E00008093000000810000809E0000008200008009000A000D00200000000000110027000D002B000D001C0000000000110031000D0028000D001A00000000000000000015004C0015001B0015002B0015001B0015007B0015001B0015002B0015001B0015002B0015001B0015003D000D00360009000A00F2000000140100001C8F0000010001007E0000000000000015000000080100000000000085000080010000008600008006000000EEEFFE800900000087000080100000008A000080180000008D0000801A000000EEEFFE801D0000008E0000802F00000090000080310000009200008034000000EEEFFE8037000000EEEFFE805100000095000080570000009600008059000000980000805B000000990000805D0000009B000080630000009C000080650000009E00008070000000A10000807B000000A200008009000A000D00200000000000110027000D002B000D001C0000000000110031000D0028000D001A0000000000000000001500500015001B0015002B0015001B001500790015001B0015003E000D00360009000A00F2000000600000009A8F0000010001002100000000000000060000005400000000000000A500008001000000A600008006000000EEEFFE8009000000A700008014000000A90000801F000000AA00008009000A000D001C0000000000110063001100490009000A00F20000006C000000BB8F0000010001002D00000000000000070000006000000000000000AD00008001000000B000008003000000EEEFFE8006000000B100008014000000B300008020000000B50000802B000000B600008009000A000D0020000000000011007200110074000D005A0009000A00F200000054000000E88F0000010001001A00000000000000050000004800000000000000BD00008001000000BE00008003000000C000008014000000C200008018000000C300008009000A000D0023000D0030000D001B0009000A00F20000005400000002900000010001001A00000000000000050000004800000000000000C600008001000000C700008003000000C900008014000000CB00008018000000CC00008009000A000D0023000D0033000D001B0009000A00F2000000E40000001C90000001000100B40000000000000011000000D800000000000000CF00008001000000D000008003000000D20000800D000000EEEFFE8013000000D300008014000000D400008025000000D500008036000000D600008047000000D700008058000000D800008069000000D90000807A000000DA0000808B000000DB0000809C000000DC000080AD000000DD000080AE000000DF000080B2000000E000008009000A000D002A000D003600000000000D000E001100450011004B00110049001100440011004400110051001100470011004600110048000D000E000D00220009000A00F2000000CC000000D0900000010001008F000000000000000F000000C000000000000000E300008001000000E400008003000000E60000800D000000EEEFFE8010000000E700008011000000E800008022000000E900008033000000EA00008044000000EB00008055000000EC00008066000000ED00008077000000EE00008088000000EF00008089000000F10000808D000000F200008009000A000D002A000D003600000000000D000E00110048001100460011004D001100460011004B0011004500110044000D000E000D00220009000A00F4000000080000002A0F00000000000060000000901C0000B01C0000C81C0000E81C0000001D0000201D0000381D0000581D0000701D0000881D0000A01D0000B81D0000D01D0000E81D0000001E00001C1E0000341E0000541E00006C1E0000881E0000A01E0000C01E0000D81E0000F81E0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000400000062002A110000000038010000000000002200000000000000000000008B0000065F91000001000000003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F3000001600031104000000F8000000220000005F910000010000000A0024115553797374656D001A0024115553797374656D2E446174612E53716C5479706573000000120024115553797374656D2E5465787400000000220024115553797374656D2E546578742E526567756C617245787072657373696F6E7300160024115546696E64416E645265706C616365434C520000020006003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040000000C00000001000500040600010C00000016844C0002000600F20000003C0000005F9100000100010022000000000000000300000030000000000000003B000080010000003B000080200000003B0000802B002C002D0079007A007B00F4000000080000006E0300000000000008000000101F0000581F0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000400000062002A1100000000A4000000000000003800000000000000000000008D0000068191000001000000003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F3100003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000008B000006040600010C0000001683310002000600F20000003C0000008191000001000100380000000000000003000000300000000000000035000080010000003500008036000000350000802B002C002D009E009F00A000F4000000080000006E0300000000000008000000701F0000B81F00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000004E002A110000000090000000000000001800000000000000000000008F000006B991000001000000003C436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F723E625F5F3000003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000008B000006040600010C0000001680F20002000600F20000003C000000B991000001000100180000000000000003000000300000000000000058000080010000005800008016000000580000802300240025006B006C006D00F4000000080000006E0300000000000008000000D01F000004200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000400000056002A1100000000980000000000000022000000000000000000000092000006D191000001000000003C46696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E74546578743E625F5F30003A000404C93FEAC6B359D649BC250902BBABB460000000004D0044003200000004020000040100000C0000008B000006040600010C0000001682F00002000600F20000003C000000D191000001000100220000000000000003000000300000000000000033000080010000003300008020000000330000802C002D002E007A007B007C00F4000000080000006008000000000000080000001C2000005C2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFF1A092FF1800800001C060000D5070000010000001D1E0000010000000901000001000000E1020000010000001503000001000000C105000001000000D108000001000000FD0A000001000000910C000001000000B10E0000010000005D010000010000005D100000010000004512000001000000B913000001000000BD1500000100000055170000010000007119000001000000610A0000010000003D1B00000100000075070000010000000108000001000000591F000001000000E91D00000100000045010000010000003D00000001000000D51500000100000061030000010000005D20000001000000FD05000001000000F10400000100000009090000010000001D0E000001000000350B00000100000015160000010000005D04000001000000D50C000001000000E90E00000100000099100000010000007912000001000000F513000001000000FD1500000100000065130000010000008D17000001000000B5190000010000002911000001000000E910000001000000711B000001000000B11C000001000000B91D000001000000911C0000010000008102000001000000C9120000010000000D07000001000000210C0000010000009D1A000001000000E90F000001000000910E0000010000004D13000001000000B907000001000000E9160000010000006506000001000000D11A000001000000A107000001000000710D000001000000891D000001000000C900000001000000C90200000100000071050000010000003910000001000000C10A000001000000E100000001000000E908000001000000ED14000001000000550C000001000000790E00000100000021100000010000006D0C0000010000001512000001000000811300000100000081150000010000001D170000010000003519000001000000091B0000010000008908000001000000F91E000001000000390200000100000045040000010000001908000001000000DD06000001000000CD09000001000000ED0B000001000000950D000001000000A90F00000100000045110000010000001913000001000000E91A0000010000009D14000001000000B110000001000000AD160000010000003918000001000000851A000001000000351C000001000000591D00000100000025000000010000000520000001000000C11E0000010000008D04000001000000050A000001000000011E000001000000CD0D000001000000551A0000010000007D11000001000000D51400000100000011190000010000007518000001000000791C000001000000BD060000010000003108000001000000891E0000010000008D01000001000000AD09000001000000AD030000010000004D060000010000001D0A000001000000391D00000100000015040000010000005D09000001000000790B000001000000150D000001000000250F000001000000D1100000010000002D12000001000000B1120000010000001918000001000000A5170000010000002D140000010000003516000001000000C917000001000000050C000001000000FD19000001000000B51B000001000000E91C0000010000005D08000001000000B91F000001000000351E000001000000551B000001000000551E000001000000DD01000001000000FD030000010000008D07000001000000A5060000010000009509000001000000B50B00000100000089160000010000007903000001000000590D000001000000CD18000001000000650F00000100000071000000010000001111000001000000F5060000010000004D0B000001000000E51200000100000089050000010000006514000001000000711600000100000001180000010000003D1A000001000000D9050000010000009915000001000000F11B000001000000211D0000010000009511000001000000151A0000010000009D0A0000010000002D03000001000000A90C000001000000CD190000010000002109000001000000B5140000010000007D140000010000002101000001000000CD0B000001000000011D000001000000111F000001000000711F000001000000D113000001000000D91E0000010000005D0700000100000075100000010000006D1E0000010000005D110000010000005900000001000000D9040000010000003D0F000001000000CD110000010000005118000001000000490A000001000000C503000001000000050E000001000000AD0D000001000000B511000001000000750900000100000089190000010000000D15000001000000B5180000010000004D1C000001000000150B000001000000FD120000010000005515000001000000F50100000100000091000000010000002905000001000000550E0000010000000110000001000000C90E000001000000850A00000100000035170000010000001D20000001000000C5160000010000003D0E00000100000075080000010000004105000001000000E5110000010000003D1500000100000099130000010000005D12000001000000F918000001000000E1170000010000004507000001000000A504000001000000ED070000010000004D16000001000000D90A0000010000006D17000001000000A11E000001000000891B0000010000000117000001000000A9000000010000007D0F000001000000D11F0000010000001506000001000000C91C000001000000A5010000010000004908000001000000990200000100000051020000010000003113000001000000211B0000010000004D19000001000000910B000001000000E50D0000010000008D180000010000002515000001000000FD11000001000000D11D000001000000A11D000001000000711D000001000000091C000001000000A108000001000000C10F000001000000E50900000100000091120000010000000100000001000000390C000001000000250700000100000045140000010000000D14000001000000010F0000010000002D0D000001000000ED0C000001000000CD1B000001000000011500000208000004010000080600001040000020800000401000008120000005051008020A0002840120400802000010400000208000006050000084200000000108000A000000040000000800000014000000220000804000010080000000810500000208000800410024080200001050000020800000401000008000000001050000020A000004010000080200001040000820900000401000008020000005010000020000000400010008020000100000002000100040000000800000800105000202080808050100000802000030408004208000004011000080200000250500000A08000004010004180220005040100020800000401080008220400000000100000000800000000000000002000800000000080200000000000100000040002001000C4000000000800400040010000000000010000000000000000002000000210000140800010004010000200040401000000080000000091400080200000021000020080500000500002820000000900000808000002040000000800000040000000040000048060000000000080404000040000000004002040000000000000008000000000040800000000000000000000100100002002000040200000000000400000200000000000000000000000000000008040000000400000000000000000800010000000000000800008000000802000010410000000000000000000000000C0000001800000024000000300000003C0000004800000054000000600000006C0000007800000084000000900000009C000000A8000000B4000000C0000000CC000000D8000000E4000000F0000000FC0000000801000014010000200100002C0100003801000044010000500100005C0100006801000074010000800100008C01000098010000A4010000B0010000BC010000C8010000D4010000E0010000EC010000F801000004020000100200001C0200002802000034020000400200004C0200005802000064020000700200007C0200008802000094020000A0020000AC020000B8020000C4020000D0020000DC020000E8020000F4020000000300000C0300001803000024030000300300003C0300004803000054030000600300006C0300007803000084030000900300009C030000A8030000B4030000C0030000CC030000D8030000E4030000F0030000FC0300000804000014040000200400002C0400003804000044040000500400005C0400006804000074040000800400008C04000098040000A4040000B0040000BC040000C8040000D4040000E0040000EC040000F804000004050000100500001C0500002805000034050000400500004C0500005805000064050000700500007C0500008805000094050000A0050000AC050000B8050000C4050000D0050000DC050000E8050000F4050000000600000C0600001806000024060000300600003C0600004806000054060000600600006C0600007806000084060000900600009C060000A8060000B4060000CC060000D8060000E4060000F0060000FC0600000807000014070000200700002C07000044070000500700005C07000068070000740700008007000098070000A4070000B0070000BC070000C8070000D4070000E0070000EC070000F807000004080000100800001C0800002808000034080000400800004C08000058080000640800007C0800008808000094080000A0080000AC080000B8080000C4080000D0080000DC080000E8080000F4080000000900000C0900001809000024090000300900003C0900004809000054090000600900006C0900007809000084090000900900009C090000A8090000B4090000C0090000CC090000D8090000E4090000F0090000FC090000080A0000200A00002C0A0000380A0000440A0000500A00005C0A0000680A0000740A0000800A00008C0A0000980A0000A40A0000B00A0000BC0A0000C80A0000D40A0000E00A0000EC0A0000F80A0000040B0000100B00001C0B0000280B0000340B0000400B00004C0B0000580B0000640B0000700B00007C0B0000940B0000A00B0000AC0B0000B80B0000C40B0000D00B0000DC0B0000F40B0000180C0000240C0000300C00003C0C0000480C0000540C0000600C00006C0C0000780C0000900C00009C0C0000A80C0000B40C00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000022002511000000000400000002004E617646696C7465725F4765745175657279000000001600291100000000040000000200303630303030303700001A00251100000000D4040000020052656765785F49734D61746368001600291100000000D40400000200303630303030303800001E00251100000000FC050000020052656765785F4765744D61746368000000001600291100000000FC0500000200303630303030303900001E00251100000000D0060000020052656765785F4765744D61746368657300001600291100000000D00600000200303630303030306100002600251100000000A8070000020052656765785F4765744D6174636865735F46696C6C526F7700001600291100000000A8070000020030363030303030620000220025110000000020080000020052656765785F5265706C6163654D61746368657300001600291100000000200800000200303630303030306300002E00251100000000F8080000020046696E64416E645265706C6163655F476574436F6E74656E744C6F636174696F6E001600291100000000F80800000200303630303030306400003600251100000000C80A0000020046696E64416E645265706C6163655F4973436F6E74656E74446566696E6974696F6E4D617463680000001600291100000000C80A00000200303630303030306500004200251100000000700D000010000000000000000000000000000000000000000000000000000000FFFFFFFF1A092FF10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020046696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E670000001600291100000000700D00000200303630303030306600002E002511000000003C0F00000200436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F7200000016002911000000003C0F00000200303630303030313000002E00251100000000001000000200436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F72000000160029110000000000100000020030363030303031310000320025110000000024110000020046696E64416E645265706C6163655F497346696C6550726F706572746965734D6174636800001600291100000000241100000200303630303030313200003200251100000000F8110000020046696E64416E645265706C6163655F4765745570646174656446696C6550726F7065727479001600291100000000F81100000200303630303030313300003200251100000000E0120000020046696E64416E645265706C6163655F4973496D61676550726F706572746965734D61746368001600291100000000E01200000200303630303030313400003600251100000000B4130000020046696E64416E645265706C6163655F47657455706461746564496D61676550726F7065727479000000001600291100000000B41300000200303630303030313500002E002511000000009C140000020046696E64416E645265706C6163655F4973506167654E616D654D617463680000000016002911000000009C1400000200303630303030313600002E002511000000005C150000020046696E64416E645265706C6163655F47657455706461746564506167654E616D650016002911000000005C150000020030363030303031370000320025110000000040160000020046696E64416E645265706C6163655F49735061676550726F706572746965734D617463680000160029110000000040160000020030363030303031380000360025110000000008170000020046696E64416E645265706C6163655F476574557064617465645061676550726F706572746965730000001600291100000000081700000200303630303030313900002E00251100000000F4170000020046696E64416E645265706C6163655F497354657874436F6E74656E744D61746368001600291100000000F417000002003036303030303161000036002511000000009C190000020046696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E7454657874000016002911000000009C19000002003036303030303162000022002511000000007C1B000002005461674174747269627574654576616C7561746F720016002911000000007C1B00000200303630303030316300003600251100000000FC1B0000020046696E64416E645265706C6163655F49734C696E6B496E436F6E74656E74446566696E6974696F6E00001600291100000000FC1B00000200303630303030316400003E00251100000000081E0000020046696E64416E645265706C6163655F476574557064617465644C696E6B496E436F6E74656E74446566696E6974696F6E00001600291100000000081E00000200303630303030316500001E0025110000000008200000020047657452656C6174696F6E5461626C65000016002911000000000820000002003036303030303166000016002511000000009C230000020046696C6C526F7700000016002911000000009C2300000200303630303030323000001E00251100000000040000000400526567657853656C656374416C6C0000000016002911000000000400000004003036303030303739000016002511000000009401000004002E6363746F72000000001600291100000000940100000400303630303030376200001200251100000000040000000100496E697400001600291100000000040000000100303630303030303100001A00251100000000340100000100416363756D756C617465000000001600291100000000340100000100303630303030303200001200251100000000A801000001004D65726765001600291100000000A801000001003036303030303033000016002511000000000C02000001005465726D696E6174650016002911000000000C02000001003036303030303034000012002511000000008002000001005265616400001600291100000000800200000100303630303030303500001200251100000000E402000001005772697465001600291100000000E40200000100303630303030303600002E00251100000000040000000300436F6E746163744C6973745F557064617465436F6E74616374436F756E74434C52001600291100000000040000000300303630303030323200001E00251100000000540200000300436F6E746163745F536561726368434C52001600291100000000540200000300303630303030323300003A00251100000000040400000300436F6E746163745F4765744175746F446973747269627574696F6E4C697374436F6E74616374436F756E740000001600291100000000040400000300303630303030323400001E002511000000007C0500000300476574436F756E744279537461747573000016002911000000007C0500000300303630303030323500001E00251100000000580600000300457865637574655369746547726F757000001600291100000000580600000300303630303030323600001E002511000000005407000003004578656375746553697465496473000000001600291100000000540700000300303630303030323700002A002511000000004C080000030045786563757465427943757272656E7453697465436F6E7465787400000016002911000000004C080000030030363030303032380000220025110000000034090000030045786563757465436F6E74616374436F756E740000001600291100000000340900000300303630303030323900002200251100000000140A00000300457865637574655369746541747472696275746573001600291100000000140A00000300303630303030326100002200251100000000140B00000300526574726965766556616C75657346726F6D584D4C001600291100000000140B00000300303630303030326200001E00251100000000C40E000003004765744D616E75616C436F6E7461637473001600291100000000C40E00000300303630303030326300002A00251100000000A00F0000030045786563757465417474726962757465436F6E74616374536561726368001600291100000000A00F00000300303630303030326400002200251100000000E810000003004578656375746550726F66696C6553656172636800001600291100000000E81000000300303630303030326500001E00251100000000E8110000030045786563756974655369746547726F7570001600291100000000E81100000300303630303030326600001A00251100000000E412000003004765745369746547726F757073001600291100000000E41200000300303630303030333000001A002511000000009C140000030057726974654F757470757400000016002911000000009C140000030030363030303033310000220025110000000068150000030045786563757465436F6E74616374536F7572636500001600291100000000681500000300303630303030333200002A0025110000000068160000030043726561746554656D706F726172794F75747075745461626C6500000000160029110000000068160000030030363030303033330000260025110000000040170000030044726F7054656D706F726172794F75747075745461626C6500001600291100000000401700000300303630303030333400002A0025110000000014180000030045786563757465476574436F6E74616374536F75726365436F756E74000016002911000000001418000003003036303030303335000022002511000000000C190000030045786563757465476574436F6E74616374730000000016002911000000000C1900000300303630303030333600001E00251100000000741A000003004578656375746550757263686173650000001600291100000000741A00000300303630303030333700001E00251100000000AC1B00000300457865637574654F7264657253697A6500001600291100000000AC1B00000300303630303030333800001E00251100000000041D00000300457865637574654F72646572436F756E74001600291100000000041D000003003036303030303339000022002511000000005C1E00000300457865637574654162616E646F6E656443617274000016002911000000005C1E00000300303630303030336100001E00251100000000781F00000300457865637574654C6173744C6F67696E00001600291100000000781F00000300303630303030336200001E002511000000009020000003004578656375746557617463686573000000001600291100000000902000000300303630303030336300002200251100000000A4210000030045786563757465466F726D5375626D697474656400001600291100000000A42100000300303630303030336400002600251100000000C022000003004578656375746550726F647563747350757263686173656400001600291100000000C02200000300303630303030336500002A00251100000000C423000003004578656375746550726F64756374547970657350757263686173656400001600291100000000C42300000300303630303030336600002600251100000000CC240000030045786563757465576562736974655573657253656172636800001600291100000000CC2400000300303630303030343000001E00251100000000D025000003004578656375746557617463686573000000001600291100000000D02500000300303630303030343100002200251100000000C8260000030045786563757465437573746F6D657247726F757073001600291100000000C82600000300303630303030343200002200251100000000C827000003004578656375746553656375697274794C6576656C73001600291100000000C82700000300303630303030343300001E00251100000000C8280000030045786563757465496E6465785465726D73001600291100000000C82800000300303630303030343400002600251100000000C4290000030045786563757465446973747269627574696F6E4C6973740000001600291100000000C42900000300303630303030343500001A00251100000000C82A00000300457865637574655363616C6172001600291100000000C82A00000300303630303030343600001E00251100000000DC2B0000030045786563757465446174615461626C6500001600291100000000DC2B00000300303630303030343700001E002511000000005C2D00000300476574436F6E66696775726174696F6E000016002911000000005C2D00000300303630303030343800001600251100000000C42E000003006765744E6F64657300001600291100000000C42E00000300303630303030343900001600251100000000903200000300476574477569647300001600291100000000903200000300303630303030346100001600251100000000803300000300476574496E74730000001600291100000000803300000300303630303030346200001A002511000000006C3400000300476574576174636865730000000016002911000000006C3400000300303630303030346300001E00251100000000203600000300476574466F726D5375626D697474656400001600291100000000203600000300303630303030346400001A00251100000000DC37000003004765744C6173744C6F67696E00001600291100000000DC3700000300303630303030346500001A00251100000000D0380000030047657450757263686173650000001600291100000000D03800000300303630303030346600001A00251100000000C439000003004765744F7264657253697A6500001600291100000000C43900000300303630303030353000001A00251100000000C83A000003004765744F72646572436F756E74001600291100000000C83A00000300303630303030353100001E00251100000000CC3B000003004765744162616E646F6E65644361727400001600291100000000CC3B00000300303630303030353200002200251100000000C43C0000030047657450726F70657274795365617263684974656D001600291100000000C43C00000300303630303030353300001E00251100000000FC3D00000300476574586D6C466F724775696473000000001600291100000000FC3D00000300303630303030353400001E00251100000000E83E00000300476574586D6C466F724775696473000000001600291100000000E83E00000300303630303030353500001E00251100000000D43F00000300476574586D6C466F724E6F646573000000001600291100000000D43F00000300303630303030353600001E002511000000004C4000000300476574586D6C466F724E6F6465730000000016002911000000004C4000000300303630303030353700001E002511000000003C4100000300476574436F6E74616374536F75726365730016002911000000003C4100000300303630303030353800001600251100000000584200000300476574477569647300001600291100000000584200000300303630303030353900002A00251100000000404300000300476574586D6C466F7250726F70657274795365617263684974656D00000016002911000000004043000003003036303030303561000022002511000000003C4400000300476574436F6E74656E74446566696E6974696F6E730016002911000000003C4400000300303630303030356200002600251100000000B44500000300557064617465436F6E74656E74446566696E6974696F6E7300001600291100000000B44500000300303630303030356300001E0025110000000074460000030047657446696C6550726F706572746965730016002911000000007446000003003036303030303564000022002511000000002C470000030055706461746546696C6550726F70657274696573000016002911000000002C4700000300303630303030356500002200251100000000E84700000300476574496D61676550726F70657274696573000000001600291100000000E84700000300303630303030356600002200251100000000A04800000300557064617465496D61676550726F70657274696573001600291100000000A04800000300303630303030363000001A002511000000005C4900000300476574506167654E616D6573000016002911000000005C4900000300303630303030363100001E00251100000000104A00000300557064617465506167654E616D65730000001600291100000000104A00000300303630303030363200001E00251100000000C84A000003004765745061676550726F70657274696573001600291100000000C84A00000300303630303030363300002200251100000000804B000003005570646174655061676550726F7065727469657300001600291100000000804B00000300303630303030363400001E002511000000003C4C0000030047657454657874436F6E74656E740000000016002911000000003C4C00000300303630303030363500001E00251100000000384D0000030055706461746554657874436F6E74656E74001600291100000000384D00000300303630303030363600002200251100000000F04D0000030046696E64416E645265706C6163655F46696E640000001600291100000000F04D00000300303630303030363700002600251100000000384F0000030046696E64416E645265706C6163655F5265706C616365000000001600291100000000384F00000300303630303030363800002A0025110000000054500000030046696E64416E645265706C6163655F5570646174654C696E6B73000000001600291100000000545000000300303630303030363900002200251100000000D05100000300434C52506167654D61704E6F64655F557064617465001600291100000000D05100000300303630303030366100002200251100000000505300000300434C52506167654D61704E6F64655F536176650000001600291100000000505300000300303630303030366200002A00251100000000C45300000300434C52506167654D61704E6F64655F416464436F6E7461696E65720000001600291100000000C45300000300303630303030366300002E00251100000000745900000300434C52506167654D61704E6F64655F52656D6F7665436F6E7461696E6572000000001600291100000000745900000300303630303030366400002600251100000000545F00000300434C52506167654D61704E6F64655F53617665557064617465001600291100000000545F00000300303630303030366500002E00251100000000146200000300434C52506167654D61704E6F64655F557064617465436F6E7461696E6572000000001600291100000000146200000300303630303030366600003200251100000000446800000300536574757050726F7061676174696F6E416E64496E6865726974616E636541747472696273001600291100000000446800000300303630303030373000001E00251100000000447100000300497465726174654368696C6472656E0000001600291100000000447100000300303630303030373100001A002511000000001C730000030053657475704E65774E6F6465000016002911000000001C7300000300303630303030373200001A0025110000000010750000030053657441747472696275746573001600291100000000107500000300303630303030373300002A002511000000003C7700000300536574757050616765446566696E6974696F6E4368696C6472656E00000016002911000000003C7700000300303630303030373400002200251100000000387D0000030053657475705075626C69736844657461696C730000001600291100000000387D00000300303630303030373500002A00251100000000107F00000300434C52506167654D61705F5361766550616765446566696E6974696F6E001600291100000000107F00000300303630303030373600002A00251100000000B88200000300434C53506167654D61704E6F64655F417474616368576F726B666C6F77001600291100000000B88200000300303630303030373700001E0025110000000004000000050047657453716C446174615265636F726400001600291100000000040000000500303630303030376300001E00251100000000440100000500476574536561726368526573756C747300001600291100000000440100000500303630303030376400001E00251100000000740200000500506572666F726D5265706C616365000000001600291100000000740200000500303630303030376500001E002511000000004C0300000500457865637574654E6F6E517565727900000016002911000000004C0300000500303630303030376600001600251100000000280400000500476574526567657800001600291100000000280400000500303630303030383000001600251100000000E80400000500476574526567657800001600291100000000E80400000500303630303030383100001600251100000000A80500000500476574526567657800001600291100000000A80500000500303630303030383200001A002511000000001C06000005004765744C696E6B5265676578000016002911000000001C0600000500303630303030383300001E00251100000000D00600000500466F726D61745365617263685465726D00001600291100000000D00600000500303630303030383400001A0025110000000084070000050048746D6C456E636F6465000000001600291100000000840700000500303630303030383500001E00251100000000300800000500436F6E76657274586D6C546F48746D6C00001600291100000000300800000500303630303030383600001E00251100000000F00800000500436F6E7665727448746D6C546F586D6C00001600291100000000F008000005003036303030303837000046002511000000000400000006003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F300016002911000000000400000006003036303030303862000046002511000000000400000007003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F310016002911000000000400000007003036303030303864000032002511000000000400000008003C436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F723E625F5F30001600291100000000040000000800303630303030386600003E002511000000000400000009003C46696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E74546578743E625F5F3000000000160029110000000004000000090030363030303039320000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFF770931010100000024000C8E2500E66426000200A0030000E40E00002C000000A4080000000000000000000016000000190000000000EEC00000000000000000FFFF000000000000FFFFFFFF00000000FFFF0000000000000000000000001D004803000000000000D80100000100000000000000000000000000000047656E6572617465586D6C0032393631454139300000000000000000FFFF000000000000FFFFFFFF00000000FFFF0000000000000000000000001B008024000000000000901F00000B00000000000000000000000000000055736572446566696E656446756E6374696F6E7300373141414145453800000000000000FFFF000000000000FFFFFFFF00000000FFFF0000000000000000000000001E008889000000000000808900000B00000000000000000000000000000053746F72656450726F6365647572657300434441353133364600000000000000FFFF000000000000FFFFFFFF00000000FFFF0000000000000000000000001C00F801000000000000340100000100000000000000000000000000000052656765785365617263680037373942344442370000000000000000FFFF000000000000FFFFFFFF00000000FFFF0000000000000000000000001F00B009000000000000740800000100000000000000000000000000000046696E64416E645265706C616365434C522E5574696C697479003142443933303035000000000000FFFF000000000000FFFFFFFF00000000FFFF00000000000000000000000020003C01000000000000540000000100000000000000000000000000000055736572446566696E656446756E6374696F6E732E3C3E635F5F446973706C6179436C617373385F31003834353336463244000000000000FFFF000000000000FFFFFFFF00000000FFFF0000000000000000000000002100A800000000000000540000000100000000000000000000000000000055736572446566696E656446756E6374696F6E732E3C3E635F5F446973706C6179436C617373385F32004146374533434545000000000000FFFF000000000000FFFFFFFF00000000FFFF00000000000000000000000022009400000000000000540000000100000000000000000000000000000055736572446566696E656446756E6374696F6E732E3C3E635F5F446973706C6179436C61737331305F300031463735423231320000000000FFFF000000000000FFFFFFFF00000000FFFF00000000000000000000000023009C00000000000000540000000100000000000000000000000000000055736572446566696E656446756E6374696F6E732E3C3E635F5F446973706C6179436C61737332305F31003838453138344230002DBA2EF101000000000000000D00000000000000000000000000000000000000010000000D00000028000000000000000000000000000000000000000100000035000000140000000000000000000000000000000000000001000000490000003900000000000000000000000000000000000000010000008200000013000000000000000000000000000000000000000100000095000000140000000000000000000000000000000000000001000000A9000000B109000000000000010000000000000000000000010000005A0A00003A0000000000000001000000000000000000000001000000940A0000510000000000000001000000000000000000000001000000E50A00004C0000000000000001000000000000000000000001000000310B0000180000000000000001000000000000000000000001000000490B00004100000000000000010000000000000000000000010000008A0B0000F70000000000000001000000000000000000000001000000810C0000430100000000000001000000000000000000000001000000C40D0000F40000000000000001000000000000000000000001000000B80E0000230000000000000001000000000000000000000001000000DB0E00005300000000000000010000000000000000000000010000002E0F0000890000000000000001000000000000000000000001000000B70F00004A0000000000000001000000000000000000000001000000011000008900000000000000010000000000000000000000010000008A1000004A0000000000000001000000000000000000000001000000D41000002F0000000000000001000000000000000000000001000000031100004900000000000000010000000000000000000000010000004C1100002E00000000000000010000000000000000000000010000007A110000470000000000000001000000000000000000000001000000C1110000E90000000000000001000000000000000000000001000000AA120000C9000000000000000100000000000000000000000100000073130000130000000000000001000000000000000000000001000000861300008900000000000000010000000000000000000000010000000F1400000E01000000000000010000000000000000000000010000001D1500004B040000000000000100000000000000000000000100000068190000B203000000000000010000000000000000000000010000001A1D00008B0000000000000002000000000000000000000001000000A51D0000EA00000000000000020000000000000000000000010000008F1E0000870000000000000002000000000000000000000001000000161F00004400000000000000020000000000000000000000010000005A1F00006C0000000000000002000000000000000000000001000000C61F00006C00000000000000020000000000000000000000010000003220000045000000000000000200000000000000000000000100000077200000450000000000000002000000000000000000000001000000BC2000006C000000000000000200000000000000000000000100000028210000790900000000000002000000000000000000000001000000A12A0000450000000000000002000000000000000000000001000000E62A0000C60000000000000002000000000000000000000001000000AC2B00006C0000000000000002000000000000000000000001000000182C0000710000000000000002000000000000000000000001000000892C00006A0100000000000002000000000000000000000001000000F32D00005900000000000000020000000000000000000000010000004C2E00006C0000000000000002000000000000000000000001000000B82E00002D0000000000000002000000000000000000000001000000E52E00002D0000000000000002000000000000000000000001000000122F0000370000000000000002000000000000000000000001000000492F0000860100000000000002000000000000000000000001000000CF300000D70000000000000002000000000000000000000001000000A6310000130100000000000002000000000000000000000001000000B9320000110100000000000002000000000000000000000001000000CA3300009D0000000000000002000000000000000000000001000000673400009D000000000000000200000000000000000000000100000004350000A80000000000000002000000000000000000000001000000AC350000A80000000000000002000000000000000000000001000000543600006C0000000000000002000000000000000000000001000000C03600006C00000000000000020000000000000000000000010000002C3700006C0000000000000002000000000000000000000001000000983700006C0000000000000002000000000000000000000001000000043800006C0000000000000002000000000000000000000001000000703800006C0000000000000002000000000000000000000001000000DC3800006C0000000000000002000000000000000000000001000000483900006C0000000000000002000000000000000000000001000000B4390000700000000000000002000000000000000000000001000000243A00008C0000000000000002000000000000000000000001000000B03A0000A50000000000000002000000000000000000000001000000553B00002605000000000000020000000000000000000000010000007B400000550000000000000002000000000000000000000001000000D0400000550000000000000002000000000000000000000001000000254100006A01000000000000020000000000000000000000010000008F4200006A0100000000000002000000000000000000000001000000F94300004D010000000000000200000000000000000000000100000046450000A40100000000000002000000000000000000000001000000EA460000ED0100000000000002000000000000000000000001000000D7480000E90100000000000002000000000000000000000001000000C04A00004D01000000000000020000000000000000000000010000000D4C0000B90000000000000002000000000000000000000001000000C64C00007D0000000000000002000000000000000000000001000000434D00007B0000000000000002000000000000000000000001000000BE4D0000110000000000000002000000000000000000000001000000CF4D00006F00000000000000020000000000000000000000010000003E4E00005F00000000000000020000000000000000000000010000009D4E0000600000000000000002000000000000000000000001000000FD4E00005C0000000000000002000000000000000000000001000000594F000037010000000000000200000000000000000000000100000090500000A0000000000000000200000000000000000000000100000030510000C90000000000000002000000000000000000000001000000F95100001601000000000000020000000000000000000000010000000F530000C90000000000000002000000000000000000000001000000D8530000160100000000000002000000000000000000000001000000EE540000BE0000000000000002000000000000000000000001000000AC5500008C000000000000000200000000000000000000000100000038560000D00000000000000002000000000000000000000001000000085700008C000000000000000200000000000000000000000100000094570000370100000000000002000000000000000000000001000000CB580000A000000000000000020000000000000000000000010000006B590000DB0000000000000002000000000000000000000001000000465A00001B0100000000000002000000000000000000000001000000615B0000370100000000000002000000000000000000000001000000985C00001E0000000000000002000000000000000000000001000000B65C0000120000000000000002000000000000000000000001000000C85C000099040000000000000200000000000000000000000100000061610000780500000000000002000000000000000000000001000000D96600009603000000000000020000000000000000000000010000006F6A00001206000000000000020000000000000000000000010000008170000083060000000000000200000000000000000000000100000004770000DE0100000000000002000000000000000000000001000000E2780000D20100000000000002000000000000000000000001000000B47A00006902000000000000020000000000000000000000010000001D7D000039040000000000000200000000000000000000000100000056810000BB010000000000000200000000000000000000000100000011830000B80300000000000002000000000000000000000001000000C9860000800500000000000002000000000000000000000001000000498C0000650000000000000003000000000000000000000001000000AE8C0000080000000000000003000000000000000000000001000000B68C0000610000000000000004000000000000000000000001000000178D0000E90000000000000004000000000000000000000001000000008E00004A00000000000000040000000000000000000000010000004A8E00003100000000000000040000000000000000000000010000007B8E0000A100000000000000040000000000000000000000010000001C8F00007E00000000000000040000000000000000000000010000009A8F0000210000000000000004000000000000000000000001000000BB8F00002D0000000000000004000000000000000000000001000000E88F00001A0000000000000004000000000000000000000001000000029000001A00000000000000040000000000000000000000010000001C900000B40000000000000004000000000000000000000001000000D09000008F00000000000000040000000000000000000000010000005F91000022000000000000000500000000000000000000000100000081910000380000000000000006000000000000000000000001000000B9910000180000000000000007000000000000000000000001000000D19100002200000000000000080000000000000000000000020002000D01000000000100FFFFFFFF00000000F39100000802000000000000FFFFFFFF00000000FFFFFFFF09001D00000001000C001700180019001A001B001C0001000B000B00010001000100010001000100830500000000000062000000E00000005B010000DD0100005C020000DC02000055030000D403000050040000CF040000DE0500005B010000DD0100005C020000DC02000055030000D40300003F060000B706000050040000320700002E050000940700005B0100005B0100005B010000D4030000433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C4E617646696C7465725F47657451756572792E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C46756E6374696F6E735C526567756C617245787072657373696F6E732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C46756E6374696F6E735C526573756C744C6F636174696F6E732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C436F6E74656E74446566696E6974696F6E2E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C46696C6550726F706572746965732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C496D61676550726F706572746965732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C506167654E616D652E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C5061676550726F706572746965732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C536561726368204F7074696F6E735C54657874436F6E74656E742E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C53746F7265642050726F636564757265735C5570646174654C696E6B732E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C52656C6174696F6E4D616E616765722E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C52656765782E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C47656E6572617465586D6C2E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C436F6E746163745F536561726368434C522E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C53746F7265642050726F636564757265735C46696E642E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C53746F7265642050726F636564757265735C5265706C6163652E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C506167654D61704E6F64655F5570646174652E637300433A5C50726F64756374735C69617070735C72656C656173655C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C46696E64416E645265706C616365434C525C5574696C6974795C5574696C6974792E637300000000FEEFFEEF010000000100000000010000000000000000000000FFFFFFFFFFFFFFFFFFFF1A00FFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000942E3101EA3D545E010000003ED15B95D49EAE4B9CBD106A71237C5CED0800002F4C696E6B496E666F002F6E616D6573002F7372632F686561646572626C6F636B002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C67656E6572617465786D6C2E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C6E617666696C7465725F67657471756572792E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C66756E6374696F6E735C726567756C617265787072657373696F6E732E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C66756E6374696F6E735C726573756C746C6F636174696F6E732E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C636F6E74656E74646566696E6974696F6E2E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C66696C6570726F706572746965732E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C696D61676570726F706572746965732E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C706167656E616D652E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C7061676570726F706572746965732E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C736561726368206F7074696F6E735C74657874636F6E74656E742E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C73746F7265642070726F636564757265735C7570646174656C696E6B732E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C72656C6174696F6E6D616E616765722E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C636F6E746163745F736561726368636C722E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C73746F7265642070726F636564757265735C66696E642E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C73746F7265642070726F636564757265735C7265706C6163652E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C706167656D61706E6F64655F7570646174652E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C72656765782E6373002F7372632F66696C65732F633A5C70726F64756374735C69617070735C72656C656173655C6272696467656C696E652E69617070732E64617461626173655C6272696467656C696E652E636C7266756E6374696F6E735C66696E64616E647265706C616365636C725C7574696C6974795C7574696C6974792E637300150000002600000002000000DB4325C52F00000000000000A60300000F0000001B0300000E0000002200000008000000910200000D0000001108000018000000C505000013000000710800001900000011000000070000002F06000014000000F50000000A000000B4040000110000000A00000006000000040200000C0000002A040000100000009B060000150000003B05000012000000A4070000170000007E0100000B00000000000000050000001E07000016000000880000000900000000000000DC513301000000000000000000000000000000000000000000000000000000000000000000000000000000000000002700000020000000D509000038000000C31B00003800000000000000141100006C03000068000000680000006800000068000000680000006800000068000000680000006800000068000000680000006800000068000000680000006800000068000000680000006800000028000000E44400004003000054050000BC150100881200009C01000008010000F4000000FC000000AC0E00002C00000074200000030000000F01000010010000110100001201000013010000060000000101000002010000030100000401000005010000060100000701000008010000090100000A0100000B0100000C0100000D0100000E010000070000001B0000001C0000001D0000001E0000001F00000020000000210000002200000023000000240000002500000008000000090000000A0000000B0000000C0000000D0000000E0000000F000000100000001100000012000000130000001400000015000000160000001700000018000000190000001A000000260000002700000028000000290000002A0000002B0000002C0000002D0000002E0000002F000000300000003100000032000000330000003400000035000000360000003700000038000000390000003A0000003B0000003C0000003D0000003E0000003F000000400000004100000042000000430000004400000045000000460000004700000048000000490000004A0000004B0000004C0000004D0000004E0000004F000000500000005100000052000000530000005400000055000000560000005700000058000000590000005A0000005B0000005C0000005D0000005E0000005F000000600000006100000062000000630000006400000065000000660000006700000068000000690000006A0000006B0000006C0000006D0000006E0000006F000000700000007100000072000000730000007400000075000000760000007700000078000000790000007A0000007B0000007C0000007D0000007E0000007F000000800000008100000082000000830000008400000085000000860000008700000088000000890000008A0000008B0000008C0000008D0000008E0000008F000000900000009100000092000000930000009400000095000000960000009700000098000000990000009A0000009B0000009C0000009D0000009E0000009F000000A0000000A1000000A2000000A3000000A4000000A5000000A6000000A7000000A8000000A9000000AA000000AB000000AC000000AD000000AE000000AF000000B0000000B1000000B2000000B3000000B4000000B5000000B6000000B7000000B8000000B9000000BA000000BB000000BC000000BD000000BE000000BF000000C0000000C1000000C2000000C3000000C4000000C5000000C6000000C7000000C8000000C9000000CA000000CB000000CC000000CD000000CE000000CF000000D0000000D1000000D2000000D3000000D4000000D5000000D6000000D7000000D8000000D9000000DA000000DB000000DC000000DD000000DE000000DF000000E0000000E1000000E2000000E3000000E4000000E5000000E6000000E7000000E8000000E9000000EA000000EB000000EC000000ED000000EE000000F0000000EF000000F1000000F2000000F3000000F4000000F5000000F6000000F7000000F8000000F9000000FA000000FB000000FC000000FD000000FE000000FF00000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001401000015010000160100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 AS N'Bridgeline.CLRFunctions.pdb';

GO



PRINT N'Creating [dbo].[GenerateXml]...';


GO
CREATE AGGREGATE [dbo].[GenerateXml](@Value NVARCHAR (MAX))
    RETURNS NVARCHAR (MAX)
    EXTERNAL NAME [Bridgeline.CLRFunctions].[GenerateXml];


GO
PRINT N'Creating [dbo].[FindAndReplace_IsImagePropertiesMatch]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_IsImagePropertiesMatch]
(@title NVARCHAR (MAX), @description NVARCHAR (MAX), @altText NVARCHAR (MAX), @searchTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsImagePropertiesMatch]


GO
PRINT N'Creating [dbo].[FindAndReplace_IsLinkInContentDefinition]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_IsLinkInContentDefinition]
(@source XML, @oldUrl NVARCHAR (MAX), @matchWholeLink BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsLinkInContentDefinition]


GO
PRINT N'Creating [dbo].[FindAndReplace_IsPageNameMatch]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_IsPageNameMatch]
(@source NVARCHAR (MAX), @searchTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPageNameMatch]


GO
PRINT N'Creating [dbo].[FindAndReplace_IsPagePropertiesMatch]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_IsPagePropertiesMatch]
(@source XML, @searchTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPagePropertiesMatch]


GO
PRINT N'Creating [dbo].[FindAndReplace_IsTextContentMatch]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_IsTextContentMatch]
(@source NVARCHAR (MAX), @searchTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsTextContentMatch]


GO
PRINT N'Creating [dbo].[NavFilter_GetQuery]...';


GO
CREATE FUNCTION [dbo].[NavFilter_GetQuery]
(@queryId UNIQUEIDENTIFIER, @condition NVARCHAR (MAX), @OrderByProperty NVARCHAR (MAX), @SortOrder NVARCHAR (MAX), @SiteId UNIQUEIDENTIFIER)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[NavFilter_GetQuery]


GO
PRINT N'Creating [dbo].[Regex_GetMatch]...';


GO
CREATE FUNCTION [dbo].[Regex_GetMatch]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (MAX), @regexOptions INT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatch]


GO
PRINT N'Creating [dbo].[Regex_IsMatch]...';


GO
CREATE FUNCTION [dbo].[Regex_IsMatch]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (MAX), @regexOptions INT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_IsMatch]


GO
PRINT N'Creating [dbo].[Regex_ReplaceMatches]...';


GO
CREATE FUNCTION [dbo].[Regex_ReplaceMatches]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (MAX), @regexOptions INT, @replacement NVARCHAR (MAX))
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_ReplaceMatches]


GO
PRINT N'Creating [dbo].[RegexSelectAll]...';


GO
CREATE FUNCTION [dbo].[RegexSelectAll]
(@input NVARCHAR (MAX), @pattern NVARCHAR (MAX), @matchDelimiter NVARCHAR (MAX))
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[RegexSearch].[RegexSelectAll]


GO
PRINT N'Creating [dbo].[FindAndReplace_GetContentLocation]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_GetContentLocation]
(@contentId UNIQUEIDENTIFIER, @siteId UNIQUEIDENTIFIER)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetContentLocation]


GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]
(@source XML, @searchTerm NVARCHAR (MAX), @replaceTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedContentDefinitionXmlString]


GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedFileProperty]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedFileProperty]
(@source NVARCHAR (MAX), @searchTerm NVARCHAR (MAX), @replaceTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedFileProperty]


GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedImageProperty]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedImageProperty]
(@source NVARCHAR (MAX), @searchTerm NVARCHAR (MAX), @replaceTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedImageProperty]


GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]
(@source XML, @oldUrl NVARCHAR (MAX), @newUrl NVARCHAR (MAX), @matchWholeLink BIT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedLinkInContentDefinition]


GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedPageName]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedPageName]
(@source NVARCHAR (MAX), @searchTerm NVARCHAR (MAX), @replaceTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageName]


GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedPageProperties]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedPageProperties]
(@source XML, @searchTerm NVARCHAR (MAX), @replaceTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageProperties]


GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedTextContentText]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedTextContentText]
(@source NVARCHAR (MAX), @searchTerm NVARCHAR (MAX), @replaceTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedTextContentText]


GO
PRINT N'Creating [dbo].[FindAndReplace_IsContentDefinitionMatch]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_IsContentDefinitionMatch]
(@source XML, @searchTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsContentDefinitionMatch]


GO
PRINT N'Creating [dbo].[FindAndReplace_IsFilePropertiesMatch]...';


GO
CREATE FUNCTION [dbo].[FindAndReplace_IsFilePropertiesMatch]
(@title NVARCHAR (MAX), @description NVARCHAR (MAX), @altText NVARCHAR (MAX), @searchTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsFilePropertiesMatch]


GO
PRINT N'Creating [dbo].[GetRelationTable]...';


GO
CREATE FUNCTION [dbo].[GetRelationTable]
(@relationType NVARCHAR (MAX), @objectId UNIQUEIDENTIFIER)
RETURNS 
     TABLE (
        [col1]  UNIQUEIDENTIFIER NULL,
        [col2]  UNIQUEIDENTIFIER NULL,
        [col3]  UNIQUEIDENTIFIER NULL,
        [col4]  UNIQUEIDENTIFIER NULL,
        [col5]  UNIQUEIDENTIFIER NULL,
        [col6]  UNIQUEIDENTIFIER NULL,
        [col7]  UNIQUEIDENTIFIER NULL,
        [col8]  UNIQUEIDENTIFIER NULL,
        [col9]  UNIQUEIDENTIFIER NULL,
        [col10] UNIQUEIDENTIFIER NULL)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[GetRelationTable]


GO
PRINT N'Creating [dbo].[Regex_GetMatches]...';


GO
CREATE FUNCTION [dbo].[Regex_GetMatches]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (MAX), @regexOptions INT)
RETURNS 
     TABLE (
        [Match] NVARCHAR (MAX) NULL)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatches]


GO
PRINT N'Creating [dbo].[CLRPageMap_SavePageDefinition]...';


GO
CREATE PROCEDURE [dbo].[CLRPageMap_SavePageDefinition]
@siteId UNIQUEIDENTIFIER, @parentNodeId UNIQUEIDENTIFIER, @CreatedBy UNIQUEIDENTIFIER, @ModifiedBy UNIQUEIDENTIFIER, @pageDefinitionXml XML, @Id UNIQUEIDENTIFIER OUTPUT, @publishPage BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMap_SavePageDefinition]


GO
PRINT N'Creating [dbo].[CLRPageMapNode_AddContainer]...';


GO
CREATE PROCEDURE [dbo].[CLRPageMapNode_AddContainer]
@templateId NVARCHAR (MAX), @containerIds NVARCHAR (MAX), @modifiedBy UNIQUEIDENTIFIER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_AddContainer]


GO
PRINT N'Creating [dbo].[CLRPageMapNode_RemoveContainer]...';


GO
CREATE PROCEDURE [dbo].[CLRPageMapNode_RemoveContainer]
@templateId NVARCHAR (MAX), @containerIds NVARCHAR (MAX), @modifiedBy UNIQUEIDENTIFIER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_RemoveContainer]


GO
PRINT N'Creating [dbo].[CLRPageMapNode_Save]...';


GO
CREATE PROCEDURE [dbo].[CLRPageMapNode_Save]
@siteId UNIQUEIDENTIFIER, @parentNodeId UNIQUEIDENTIFIER, @pageMapNode XML, @createdBy UNIQUEIDENTIFIER, @modifiedBy UNIQUEIDENTIFIER, @modifiedDate DATETIME, @id UNIQUEIDENTIFIER OUTPUT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_Save]


GO
PRINT N'Creating [dbo].[CLRPageMapNode_Update]...';


GO
CREATE PROCEDURE [dbo].[CLRPageMapNode_Update]
@siteId UNIQUEIDENTIFIER, @pageMapNode XML, @modifiedBy UNIQUEIDENTIFIER, @modifiedDate DATETIME
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_Update]


GO
PRINT N'Creating [dbo].[CLSPageMapNode_AttachWorkflow]...';


GO
CREATE PROCEDURE [dbo].[CLSPageMapNode_AttachWorkflow]
@siteId UNIQUEIDENTIFIER, @pageMapNodeId UNIQUEIDENTIFIER, @pageMapNodeWorkFlow XML, @propogate BIT, @inherit BIT, @appendToExistingWorkflows BIT, @modifiedBy UNIQUEIDENTIFIER, @modifiedDate DATETIME
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLSPageMapNode_AttachWorkflow]


GO
PRINT N'Creating [dbo].[Contact_GetAutoDistributionListContactCount]...';


GO
CREATE PROCEDURE [dbo].[Contact_GetAutoDistributionListContactCount]
@xml XML, @ApplicationId UNIQUEIDENTIFIER, @MaxRecords INT, @TotalRecords INT OUTPUT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[Contact_GetAutoDistributionListContactCount]


GO
PRINT N'Creating [dbo].[Contact_SearchCLR]...';


GO
CREATE PROCEDURE [dbo].[Contact_SearchCLR]
@xml XML, @ApplicationId UNIQUEIDENTIFIER, @MaxRecords INT, @TotalRecords INT OUTPUT, @includeAddress BIT, @ContactListId UNIQUEIDENTIFIER, @ContactSourceCount XML OUTPUT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[Contact_SearchCLR]


GO
PRINT N'Creating [dbo].[ContactList_UpdateContactCountCLR]...';


GO
CREATE PROCEDURE [dbo].[ContactList_UpdateContactCountCLR]
@xml XML, @ApplicationId UNIQUEIDENTIFIER, @ContactListId UNIQUEIDENTIFIER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[ContactList_UpdateContactCountCLR]


GO
PRINT N'Creating [dbo].[FindAndReplace_Find]...';


GO
CREATE PROCEDURE [dbo].[FindAndReplace_Find]
@siteId UNIQUEIDENTIFIER, @searchIn INT, @searchTerm NVARCHAR (MAX), @htmlEncodedSearchTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_Find]


GO
PRINT N'Creating [dbo].[FindAndReplace_Replace]...';


GO
CREATE PROCEDURE [dbo].[FindAndReplace_Replace]
@siteId UNIQUEIDENTIFIER, @objectIds XML, @searchTerm NVARCHAR (MAX), @htmlEncodedSearchTerm NVARCHAR (MAX), @replaceTerm NVARCHAR (MAX), @htmlEncodedReplaceTerm NVARCHAR (MAX), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_Replace]


GO
PRINT N'Creating [dbo].[FindAndReplace_UpdateLinks]...';


GO
CREATE PROCEDURE [dbo].[FindAndReplace_UpdateLinks]
@siteId UNIQUEIDENTIFIER, @oldUrl NVARCHAR (MAX), @newUrl NVARCHAR (MAX), @matchWholeLink BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_UpdateLinks]


GO
PRINT N'Update complete.';


GO

PRINT 'Fixing primary key constraint on COContent'
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_MKCampaignFileAttachment_COContent' AND parent_object_id = OBJECT_ID('dbo.MKCampaignFileAttachment'))
	ALTER TABLE MKCampaignFileAttachment DROP CONSTRAINT FK_MKCampaignFileAttachment_COContent
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'fk_COContentId_COVideoContentId' AND parent_object_id = OBJECT_ID('dbo.COVideo'))
	ALTER TABLE COVideo DROP CONSTRAINT fk_COContentId_COVideoContentId
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'fk_COContentId_COFileContentId' AND parent_object_id = OBJECT_ID('dbo.COFile'))
	ALTER TABLE COFile DROP CONSTRAINT fk_COContentId_COFileContentId
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_BLPost_COContent' AND parent_object_id = OBJECT_ID('dbo.BLPost'))
	ALTER TABLE BLPost DROP CONSTRAINT FK_BLPost_COContent
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_COContentId_NVNavNodeContentContentId' AND parent_object_id = OBJECT_ID('dbo.NVNavNodeContent'))
	ALTER TABLE NVNavNodeContent DROP CONSTRAINT FK_COContentId_NVNavNodeContentContentId
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'pk_COContent_Id' AND object_id = OBJECT_ID('dbo.COContent'))
	ALTER TABLE COContent DROP CONSTRAINT pk_COContent_Id
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = '_dta_index_COContent' AND object_id = OBJECT_ID('dbo.COContent'))
	DROP INDEX _dta_index_COContent ON COContent
GO

ALTER TABLE COContent ADD CONSTRAINT [pk_COContent_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
GO

ALTER TABLE MKCampaignFileAttachment ADD CONSTRAINT [FK_MKCampaignFileAttachment_COContent] FOREIGN KEY ([ContentId]) REFERENCES [dbo].[COContent] ([Id])
GO

ALTER TABLE COVideo ADD CONSTRAINT [fk_COContentId_COVideoContentId] FOREIGN KEY ([ContentId]) REFERENCES [dbo].[COContent] ([Id])
GO

ALTER TABLE COFile ADD CONSTRAINT [fk_COContentId_COFileContentId] FOREIGN KEY ([ContentId]) REFERENCES [dbo].[COContent] ([Id])
GO

ALTER TABLE BLPost ADD CONSTRAINT [FK_BLPost_COContent] FOREIGN KEY ([ContentId]) REFERENCES [dbo].[COContent] ([Id])
GO

ALTER TABLE NVNavNodeContent ADD CONSTRAINT [FK_COContentId_NVNavNodeContentContentId] FOREIGN KEY ([ContentId]) REFERENCES [dbo].[COContent] ([Id])
GO

CREATE NONCLUSTERED INDEX [_dta_index_COContent]
    ON [dbo].[COContent]([ParentId] ASC);
GO

 
PRINT 'Create ProductImage_GetList to bypass dapper generic script generation (sql optimization)'
GO
 
IF (OBJECT_ID('ProductImage_GetList') IS NOT NULL)
	DROP PROCEDURE ProductImage_GetList
GO
  
CREATE PROCEDURE [dbo].[ProductImage_GetList]
/*list is based on ProductImageListDto and base class*/
 @Title nvarchar(4000)=null
,@FileName nvarchar(4000)=null
,@ProductName nvarchar(4000)=null
,@SiteId uniqueidentifier= null
,@ProductTypeId uniqueidentifier=null
,@TypeNumber int=null
,@Status  int=1
,@offset int=0
,@rowcount int=10
,@getrecordCount bit=0
,@ProductId nvarchar(max)=null
,@sortby nvarchar(100)=null
,@parentimage nvarchar(max)=null
,@alttext nvarchar(max)=null
,@RelativePath nvarchar(max)=null
,@size int=null
,@sequence int=null
,@description nvarchar(max)=null

AS  
BEGIN


declare @items as TABLE (Items CHAR(36))

insert @items
select Items from dbo.SplitGUID(@ProductId,',')
 

IF @getrecordCount=1
BEGIN
	SELECT count(*)
	FROM   [vwProductImage]  
	WHERE
	(	@Title is null or  [Title] LIKE  @Title OR @FileName is null or [FileName] LIKE (@FileName) OR   [ProductName] is null or [ProductName] LIKE (@ProductName)) 
	AND	(@SiteId IS NULL OR [SiteId] = @SiteId )
	AND (@ProductTypeId is null or  [ProductTypeId] =  @ProductTypeId  )
	AND (@TypeNumber is null OR [TypeNumber]= @TypeNumber )
	AND [Status] = @Status
	AND  (@ProductId is null or  ProductId IN (select Items from @items  )  )

	AND (@parentimage is null or @parentimage=ParentImage)
	AND (@alttext is null or @alttext=AltText)
	AND (@RelativePath is null or @RelativePath=RelativePath)
	AND (@size is null or @size=Size)
	AND (@sequence is null or @sequence=[sequence])
	AND (@description is null or @description=[description])

	RETURN

END



DECLARE @t TABLE 
	(CreatedById	uniqueidentifier,
	ModifiedById	uniqueidentifier,
	Id	uniqueidentifier,
	Title	varchar(max),
	Description	varchar(max),
	FileName	varchar(max),
	ProductName	varchar(max),
	ProductTypeId	uniqueidentifier,
	RelativePath	varchar(max),
	Size	int,
	Sequence	int,
	AltText	varchar(max),
	TypeNumber	int,
	Status	int,
	ParentImage	uniqueidentifier,
	SiteId	uniqueidentifier,
	ProductId	uniqueidentifier,
	CreatedDate	datetime,
	ModifiedDate	datetime,
	CreatedByFullName	varchar(max),
	ModifiedByFullName	varchar(max)
)

INSERT  @t
SELECT [CreatedBy] AS [CreatedById], [ModifiedBy] AS [ModifiedById], 
	[Id], [Title], [Description], [FileName], [ProductName], [ProductTypeId], [RelativePath], [Size], [Sequence], 
	[AltText], [TypeNumber], [Status], [ParentImage], [SiteId], [ProductId], [CreatedDate], [ModifiedDate], [CreatedByFullName], 
	[ModifiedByFullName]  
FROM   [vwProductImage]   
WHERE
	(	@Title is null or  [Title] LIKE  @Title OR @FileName is null or [FileName] LIKE (@FileName) OR   [ProductName] is null or [ProductName] LIKE (@ProductName)) 
	AND	(@SiteId IS NULL OR [SiteId] = @SiteId ) 
	AND (@ProductTypeId is null or  [ProductTypeId] =  @ProductTypeId  )
	AND  (@TypeNumber is null OR [TypeNumber]= @TypeNumber )
	AND [Status] = @Status
	AND (@ProductId is null or  ProductId IN (select Items from @items  )  )

	AND (@parentimage is null or @parentimage=ParentImage)
	AND (@alttext is null or @alttext=AltText)
	AND (@RelativePath is null or @RelativePath=RelativePath)
	AND (@size is null or @size=Size)
	AND (@sequence is null or @sequence=[sequence])
	AND (@description is null or @description=[description])

if  @sortby='title' 
	SELECT * FROM  @t
	ORDER BY title ASC  
	OFFSET @offset ROWS FETCH NEXT @rowcount ROWS ONLY
else if  @sortby='filename' 
	SELECT * FROM  @t
	ORDER BY filename ASC  
	OFFSET @offset ROWS FETCH NEXT @rowcount ROWS ONLY

else --@sortby='sequence'
	SELECT * FROM  @t
	ORDER BY [Sequence] ASC  
	OFFSET @offset ROWS FETCH NEXT @rowcount ROWS ONLY

END
go

PRINT 'Creating vwPageAttributeValue'
GO
IF(OBJECT_ID('vwPageAttributeValue') IS NOT NULL)
	DROP VIEW vwPageAttributeValue
GO	
CREATE VIEW [dbo].[vwPageAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V

	UNION ALL

	SELECT ROW_NUMBER() over (PARTITION BY V.AttributeId, C.PageDefinitionId ORDER BY S.LftValue DESC) AS vRank,
		V.Id,
		C.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V
		JOIN PageDefinition P ON V.PageDefinitionId = P.PageDefinitionId
		JOIN PageDefinition C ON C.SourcePageDefinitionId = P.PageDefinitionId
		JOIN SISite S ON S.Id = P.SiteId
	WHERE V.IsShared = 1
		AND S.DistributionEnabled = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT V.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	ISNULL(V.Notes, C.Notes) AS Notes,
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATPageAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.PageDefinitionId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1
GO
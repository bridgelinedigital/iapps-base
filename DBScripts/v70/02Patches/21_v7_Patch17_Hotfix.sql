PRINT 'Add Site Settings'
DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int

SET @SettingGroupId = 1
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'SiteAttribute.DisablePropagate')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('SiteAttribute.DisablePropagate', 'Disable Proagate option for site attributes',  @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Images.EnableCompression')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Images.EnableCompression', 'Enable compression on file upload',  @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, 'true' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
GO
PRINT 'Creating view vwSiteAttributeSimple'
GO
IF(OBJECT_ID('vwSiteAttributeSimple') IS NOT NULL)
	DROP VIEW vwSiteAttributeSimple
GO	
CREATE VIEW [dbo].[vwSiteAttributeSimple]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle,
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.SiteId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 1
	LEFT JOIN ATSiteAttributeValue V ON A.Id = V.AttributeId
GO
PRINT 'Creating view vwSiteAttributeValueSimple'
GO
IF(OBJECT_ID('vwSiteAttributeValueSimple') IS NOT NULL)
	DROP VIEW vwSiteAttributeValueSimple
GO	
CREATE VIEW [dbo].[vwSiteAttributeValueSimple]
AS
SELECT V.Id,
	V.SiteId AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	V.Value, 
	V.IsShared,
	V.IsEditable,
	0 AS IsReadOnly,
	V.SiteId,
	V.CreatedDate, 
	V.CreatedBy,
	A.Title as AttributeTitle
FROM ATSiteAttributeValue V 
	LEFT JOIN ATAttribute A ON V.AttributeId = A.Id
GO


IF NOT EXISTS (Select 1 from SESearchSetting Where Title ='Search.EnableCelebrosSearch')
BEGIN 
INSERT INTO [dbo].[SESearchSetting]
           ([SiteId]
           ,[Title]
           ,[Value]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[Description]
           ,[Sequence])
     VALUES
           ('8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
           ,'Search.EnableCelebrosSearch'
           ,'false'
           ,'ABB055FD-A0B4-455C-9F8B-9707A56F899A'
           ,GETDATE()
           ,'Set true to turn on Celebros Search for Product Catalog'
           ,(select max(Sequence)+1 from SESearchSetting)
		   )
END
GO


IF NOT EXISTS (Select 1 from SESearchSetting Where Title ='Search.CelebrosFTP')
BEGIN 
INSERT INTO [dbo].[SESearchSetting]
           ([SiteId]
           ,[Title]
           ,[Value]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[Description]
           ,[Sequence])
     VALUES
           ('8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
           ,'Search.CelebrosFTP'
           ,''
           ,'ABB055FD-A0B4-455C-9F8B-9707A56F899A'
           ,GETDATE()
           ,'Celebros FTP Path, Product Catalog is exported to this path'
           ,(select max(Sequence)+1 from SESearchSetting)
		   )
END

GO

IF NOT EXISTS (Select 1 from SESearchSetting Where Title ='Search.CelebrosFTPUserName')
BEGIN 
INSERT INTO [dbo].[SESearchSetting]
           ([SiteId]
           ,[Title]
           ,[Value]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[Description]
           ,[Sequence])
     VALUES
           ('8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
           ,'Search.CelebrosFTPUserName'
           ,''
           ,'ABB055FD-A0B4-455C-9F8B-9707A56F899A'
           ,GETDATE()
           ,'Celebros FTP User name'
           ,(select max(Sequence)+1 from SESearchSetting)
		   )
END


GO

IF NOT EXISTS (Select 1 from SESearchSetting Where Title ='Search.CelebrosFTPPassword')
BEGIN 
INSERT INTO [dbo].[SESearchSetting]
           ([SiteId]
           ,[Title]
           ,[Value]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[Description]
           ,[Sequence])
     VALUES
           ('8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
           ,'Search.CelebrosFTPPassword'
           ,''
           ,'ABB055FD-A0B4-455C-9F8B-9707A56F899A'
           ,GETDATE()
           ,'Celebros FTP Password'
           ,(select max(Sequence)+1 from SESearchSetting)
		   )
END

GO


IF(OBJECT_ID('BLD_ProductImport_Attribute') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_Attribute
GO

CREATE PROCEDURE [dbo].[BLD_ProductImport_Attribute]

@ProductIDUser nvarchar(max),
@AttrGroupId uniqueidentifier,
@AttrId uniqueidentifier,
@AttrEnum bit = 0,
@AttrValue nvarchar(max),
@SKU nvarchar(max) = null,
@OperatorId uniqueidentifier = null,
@IsDefault bit = 0

AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
        BEGIN TRAN

		declare @NumericValue int;

		if lower(@AttrValue) like 'true' set @NumericValue = 1;
		if lower(@AttrValue) like 'false' set @NumericValue = 0;

		declare @ProductId uniqueidentifier;
		declare @ProductTypeId uniqueidentifier;
		declare @ProductAttrId uniqueidentifier;
		declare @ProductTypeAttributeId uniqueidentifier;
		declare @ProductAttrValueId uniqueidentifier;
		declare @ProductSKUAttrValueId uniqueidentifier;
		declare @AttrEnumId uniqueidentifier;
		declare @ProductSKUId uniqueidentifier;
		declare @IsSKULevel bit = iif(@SKU is null, 0, 1);
		declare @IsMultiValued bit =0

		select 
			@ProductId = p.Id, 
			@ProductAttrId = pa.Id,
			@ProductTypeId = p.ProductTypeId, 
			@ProductTypeAttributeId = pta.Id,
			@IsMultiValued = a.IsMultiValued
		from PRProduct p
			left join PRProductAttribute pa on p.id=pa.ProductId and pa.AttributeId=@AttrId and IsSKULevel = @IsSKULevel
			left join ATATtribute a on a.Id = pa.AttributeId
			left join ATAttributeCategoryItem ai on pa.AttributeId=ai.AttributeId
			left join ATAttributeCategory c on ai.CategoryId=c.Id and c.GroupId=@AttrGroupId
			left join PRProductTypeAttribute pta on p.ProductTypeID=pta.ProductTypeId and pa.AttributeId=pta.AttributeId
		where p.ProductIDUser=@ProductIDUser

		declare @Output varchar(max)
		set @Output = concat('product not found: ', @ProductIDUser)
		if(@ProductId is null)RAISERROR (@Output, 16, 1);

		-- ** 0 **
		if @ProductTypeAttributeId is null
		begin
			set @ProductTypeAttributeId = newid()
			insert into PRProductTypeAttribute (id, ProductTypeId, AttributeId, Sequence, IsSKULevel, IsRequired)
				values (@ProductTypeAttributeId, @ProductTypeId, @AttrId, 1, @IsSKULevel, 0)
		end

		-- ** 1 **
		select @AttrEnumId = Id from ATAttributeEnum where AttributeID = @AttrId and Title like @AttrValue;

		if @ProductAttrId is null
		begin
			set @ProductAttrId = newid();
			declare @Seq int;
			select @Seq = isnull(max(Sequence)+1,0) from PRProductAttribute where ProductId = @ProductId;
			if @SKU is not null
			begin
				select @Seq = isnull(max(Sequence)+1,0) from PRProductAttribute where ProductId = @ProductId and IsSKULevel = 1;
			end
			insert into PRProductAttribute
				select @ProductAttrId, @ProductId, @AttrId, @Seq, (case when @SKU is null then 0 else 1 end), 0;

		end

		-- ** 2 **  TODO: FIX THIS!!!

		-- IF (enumeration not found AND attribute is flagged for enumeration)
		if (@AttrEnumId is null AND @AttrEnum = 1)
		begin
			-- auto insert new enumeration
			set @AttrEnumId = newid();
			insert into ATAttributeEnum select
			@AttrEnumId, @AttrId, @AttrValue, @AttrValue, null, @AttrValue, @IsDefault, (select isnull(max(Sequence),0)+1 from ATAttributeEnum where AttributeID = @AttrId), GETUTCDATE(), null, null, null, 1;

			insert into PRProductTypeAttributeValue 
				select 
					newid(), 
					@ProductTypeAttributeId, 
					@AttrId, 
					@AttrEnumId, 
					(case when @SKU is null then 0 else 1 end), 
					(select isnull(max(Sequence),0)+1 from PRProductTypeAttributeValue where ProductTypeAttributeId = @ProductTypeAttributeId),
					@ProductTypeId

		end else
		begin

			if (@AttrEnum = 0)
			begin
			set @AttrEnumId = null
			end


		end


		if @SKU is null
		begin

			if @AttrEnumId is not null AND @IsMultiValued =1
			begin

				print '@AttrEnumId EXISTS!'


				select @ProductAttrValueId = Id from PRProductAttributeValue 
				where ProductId = @ProductId 
				and AttributeId = @AttrId 
				and ProductAttributeId = @ProductAttrId
				and AttributeEnumId = @AttrEnumId;

			end
			else begin

				print '@AttrEnumId NULL!'

				select @ProductAttrValueId = Id from PRProductAttributeValue 
				where ProductId = @ProductId 
				and AttributeId = @AttrId 
				and ProductAttributeId = @ProductAttrId;

			end



			if @ProductAttrValueId is null
			begin

				set @ProductAttrValueId = newid();
				insert into PRProductAttributeValue
					select @ProductAttrValueId, @ProductAttrId, @ProductId, @AttrId, @AttrEnumId, @AttrValue, GETUTCDATE(), @OperatorId, null, null, 1;

			end else
			begin
				
				print @AttrValue

				update PRProductAttributeValue set
				Value = @AttrValue,
				AttributeEnumId = @AttrEnumId,
				ModifiedDate = GetUTCDate(),
				ModifiedBy = @OperatorId
				where Id = @ProductAttrValueId;
			end
		end else
		begin

			print 'Sku Exists'

			select @ProductSKUId = Id FROM PRProductSKU where ProductId = @ProductId and SKU = @SKU;
			if @ProductSKUId is not null
			begin
				
				--select @ProductSKUAttrValueId = Id from PRProductSKUAttributeValue where SKUId = @ProductSKUId and AttributeId = @AttrId and ProductAttributeId = @ProductAttrId;

				if @AttrEnumId is not null AND @IsMultiValued=1
				begin

					print '@AttrEnumId EXISTS!'

					select @ProductSKUAttrValueId = Id from PRProductSKUAttributeValue where SKUId = @ProductSKUId 
					and AttributeId = @AttrId 
					and ProductAttributeId = @ProductAttrId
					and AttributeEnumId = @AttrEnumId;

				end
				else begin

					print '@AttrEnumId NULL!'

					select @ProductSKUAttrValueId = Id from PRProductSKUAttributeValue where SKUId = @ProductSKUId 
					and AttributeId = @AttrId 
					and ProductAttributeId = @ProductAttrId

				end


				if @ProductSKUAttrValueId is null
				begin
					set @ProductSKUAttrValueId = newid()
					insert into PRProductSKUAttributeValue
						select @ProductSKUAttrValueId, @ProductAttrId, @ProductSKUId, @AttrId, @AttrEnumId, @AttrValue, getUTCdate(), @OperatorId, null, null, 1;
				end else
				begin
					update PRProductSKUAttributeValue set
					Value = @AttrValue,
					AttributeEnumId = @AttrEnumId,
					ModifiedDate = GETUTCDATE(),
					ModifiedBy = @OperatorId
					where Id = @ProductSKUAttrValueId;
				end
			end
		end

		if @SKU is null select @ProductAttrValueId
		else select @ProductSKUAttrValueId
		COMMIT TRAN
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX)
        SET @ErrorMessage = ERROR_MESSAGE()
        RAISERROR(@ErrorMessage,16, 1)
    END CATCH

END
go


IF(OBJECT_ID('BLD_ProductImport_AttributeEnumVariant') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_AttributeEnumVariant
GO

CREATE PROCEDURE [dbo].[BLD_ProductImport_AttributeEnumVariant]
(
	@SiteId					uniqueidentifier,
	@AttributeEnumId		uniqueidentifier,
	@Value					nvarchar(50),
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	begin try
		
		declare @AttributeEnumVariantId		uniqueidentifier,
				@MasterAttributeEnumId		uniqueidentifier,
				@MasterAttributeDataType	nvarchar(50),
				@MasterAttributeId			uniqueidentifier,
				@MasterTitle				nvarchar(max),
				@MasterCode					nvarchar(200),
				@Code						nvarchar(200),
				@Output						nvarchar(512),
				@IsSkipped					bit = 1

		if @SiteId is null or @AttributeEnumId is null
			raiserror(15600, -1, -1, 'SiteId, ModifiedBy, or AttributeEnumId is null')

		begin tran
		select
			@MasterAttributeEnumId=ae.Id,
			@MasterAttributeId=a.Id,
			@MasterAttributeDataType = a.AttributeDataType,
			@MasterTitle=iif(v.Title is null, ae.Value, null)
		from ATAttributeEnum ae
			left join ATAttributeEnumVariant v on ae.Id=v.Id and v.SiteId=@SiteId
			join ATAttribute a on ae.AttributeID=a.Id
			join ATAttributeCategoryItem i on a.Id = i.AttributeId
			join ATAttributeCategory c on i.CategoryId=c.Id
		where
			ae.Id=@AttributeEnumId

		if @MasterAttributeEnumId is null
		begin
			set @Output = concat('Unable to find attribute enum with id=', @AttributeEnumId)
			raiserror(15600, -1, -1, @Output)
		end

		select 
			@Code = 
				case 
					when charindex('Boolean', @MasterAttributeDataType) > 0 then				
						case when lower(@Value)='true' then '1' else '0' end
					else @MasterCode
				end

		select @AttributeEnumVariantId=Id from ATAttributeEnumVariant where Id=@MasterAttributeEnumId
		select
			@Value = case when @MasterTitle is null or @Value <> @MasterTitle and @Value is not null then @Value else null end,
			@Code = case when @MasterCode is null or @Code <> @MasterCode and @Code is not null then @Code else null end

		if @AttributeEnumVariantId is not null
		begin
			update ATAttributeEnumVariant set 
				Title=@Value
				where Id=@AttributeEnumVariantId and SiteId=@SiteId
			set @Output=concat('updated ATAttributeEnumVariant id=', @AttributeEnumVariantId, ' Value=', @Value, ' code=', @Code)
		end
		else
		begin
			if @Value is not null
			begin
				insert into ATAttributeEnumVariant (Id, SiteId, AttributeId, Title, ModifiedDate, ModifiedBy)
					values (@MasterAttributeEnumId, @SiteId, @MasterAttributeId, @Value, getutcdate(), @ModifiedBy)
				set @IsSkipped=0
			end

			set @Output = concat('ATAttributeEnumVariant ', iif(@IsSkipped=1, 'skipped', 'inserted'), ' id=', @MasterAttributeEnumId, ' Value=', @Value, ' code=', @Code)
		end

		commit tran
		select @MasterAttributeEnumId, @Output
	end try
	begin catch
		rollback tran
		DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE(),
				@ErrorState int				= ERROR_STATE(),
				@ErrorSeverity int			= ERROR_SEVERITY()
        RAISERROR(@ErrorMessage,@ErrorSeverity, @ErrorState)

	end catch
END

GO

IF(OBJECT_ID('BLD_ProductImport_AttributeVariant') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_AttributeVariant
GO
CREATE PROCEDURE [dbo].[BLD_ProductImport_AttributeVariant]
(
	@SiteId					uniqueidentifier,
	@AttributeId			uniqueidentifier,
	@Title					nvarchar(4000),
	@Description			nvarchar(50),
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	BEGIN TRY
		declare @AttributeVariantId			uniqueidentifier
		declare @MasterTitle				nvarchar(4000)
		declare @MasterDescription			nvarchar(2000)
		declare @Output						nvarchar(512)
		declare @IsSkipped					bit = 1

		if @SiteId is null or @AttributeId is null or @Title is null
			raiserror(15600, -1, -1, 'SiteId, AttributeId, or Title is null')

		BEGIN TRAN
		select
			@MasterTitle=iif(v.Title is null, a.Title, null),
			@MasterDescription=iif(v.Description is null, a.Description, null)
		from ATAttributeCategoryItem i
			join ATAttributeCategory c on i.CategoryId=c.Id
			join ATAttribute a	on i.AttributeId=a.Id
			left join ATAttributeVariant v on a.Id=v.Id and v.SiteId=@SiteId
		where a.Id=@AttributeId

		if @MasterTitle is null
		begin
			set @Output = concat('Unable to find master attribute with id=', @AttributeId)
			raiserror(15600, -1, -1, @Output)
		end
		
		select @AttributeVariantId=Id from ATAttributeVariant where Id=@AttributeId
		select
			@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
			@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end

		if @AttributeVariantId is not null
		begin
			update ATAttributeVariant set Description=@Description where Id=@AttributeVariantId and SiteId=@SiteId
			set @Output=concat('updated ATAttributeVariant id=', @AttributeVariantId)
		end
		else
		begin
			if @Title is not null or @Description is not null
			begin
				insert into ATAttributeVariant (Id, SiteId, Title, Description, ModifiedDate, ModifiedBy)
					values (@AttributeId, @SiteId, @Title, @Description, getutcdate(), @ModifiedBy)
				set @IsSkipped=0
			end

			set @Output = concat(
				'ATAttributeVariant ', 
				iif(@IsSkipped=1, 'skipped', concat('inserted Title=', @Title, ' | Description=', @Description)))
		end
		commit tran
		select @AttributeId, @Output
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE(),
				@ErrorState int				= ERROR_STATE(),
				@ErrorSeverity int			= ERROR_SEVERITY()
        RAISERROR(@ErrorMessage,@ErrorSeverity, @ErrorState)
    END CATCH

END
GO
print 'Created stored procedure BLD_ProductImport_AttributeVariant'
go



IF(OBJECT_ID('BLD_ProductImport_ImageVariant') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_ImageVariant
GO

CREATE PROCEDURE [dbo].[BLD_ProductImport_ImageVariant]
(
	@SiteId					uniqueidentifier,
	@ProductIdUser			nvarchar(100),
	@Title					nvarchar(256),
	@Description			nvarchar(4000),
	@AltText				nvarchar(1024),
	@ObjectId				uniqueidentifier,
	@ObjectType				int,
	@Sequence				int,
	@IsShared				bit,
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	begin try
		declare @MasterSiteid				uniqueidentifier,
				@MasterMainImageId			uniqueidentifier,
				@VariantMainImageId			uniqueidentifier,
				@MainImageTypeId			uniqueidentifier	= '396B425C-E197-4F2D-A4A3-25448CA366F8',
				@MasterPreviewImageId		uniqueidentifier,
				@VariantPreviewImageId		uniqueidentifier,
				@PreviewImageTypeId			uniqueidentifier	= '2770534E-4675-4764-A620-56776398E03E',
				@MasterMiniImageId			uniqueidentifier,
				@VariantMiniImageId			uniqueidentifier,
				@MiniImageTypeId			uniqueidentifier	= 'E02ED0F3-1271-4218-B81E-816E458434A1',
				@MasterSystemThumbImageId	uniqueidentifier,
				@VariantSystemThumbImageId	uniqueidentifier,
				@SystemThumbImageTypeId		uniqueidentifier	= '63CF2418-684A-4D1F-88B2-34968C6F139F',
				@MasterThumbnailImageId		uniqueidentifier,
				@VariantThumbnailImageId	uniqueidentifier,
				@ThumbnailImageTypeId		uniqueidentifier	= 'BA9B3922-4F9F-4B21-A5CF-E03B26B588AD',
				@Output						varchar(512)		= ''

		if @SiteId is null or @ObjectId is null or @ObjectType is null or @Sequence is null
			raiserror(15600, -1, -1, 'SiteId, ObjectId, ObjectType, or Sequence is null')

		begin tran
		select @MasterSiteId=Id from SISite where id=MasterSiteId

		--get master image IDs for each image type
		select
			@MasterMainImageId=main,
			@MasterPreviewImageId=preview,
			@MasterMiniImageId=mini,
			@MasterSystemThumbImageId=system_thumb,
			@MasterThumbnailImageId=thumbnail
		from 
			(select o.SiteId, o.ObjectId, o.Sequence, i.id as ImageId, t.Title as ImageType
			from CLImage i
				join CLObjectImage o on o.ImageId=i.Id and o.siteid=@MasterSiteId and o.Sequence=@Sequence
				join CLImageType t on i.ImageType=t.id
			) as src
			pivot (max(src.ImageId) for ImageType in ([main], [preview], [mini], [system_thumb], [thumbnail])) as piv
		where ObjectId=@ObjectId

		--get variant image IDs for each image type
		select
			@VariantMainImageId=main,
			@VariantPreviewImageId=preview,
			@VariantMiniImageId=mini,
			@VariantSystemThumbImageId=system_thumb,
			@VariantThumbnailImageId=thumbnail
		from 
			(select o.SiteId, o.ObjectId, o.Sequence, i.id as ImageId, t.Title as ImageType
			from CLImageVariant i
				join CLObjectImage o on o.ImageId=i.Id and o.siteid=@MasterSiteId and o.Sequence=@Sequence
				join CLImageType t on i.ImageType=t.id
			) as src
			pivot (max(src.ImageId) for ImageType in ([main], [preview], [mini], [system_thumb], [thumbnail])) as piv
		where ObjectId=@ObjectId


		if @MasterMainImageId is null begin
			set @Output = concat(
				'Could not find master main image for product=', @ProductIdUser,
				' | sequence=', @Sequence)
			raiserror(15600, -1, -1, @Output)
		end

		set @Output = concat(@Output, 'inserting image for product=', @ProductIdUser)
		declare @MasterTitle		nvarchar(256),
				@MasterDescription	nvarchar(4000),
				@MasterAltText		nvarchar(1024)

		select 
			@MasterTitle=iif(v.Title is null, i.Title, null),
			@MasterDescription=iif(v.Description is null, i.Description, null),
			@MasterAltText=iif(v.AltText is null, i.AltText, null)
		from CLImage i
			left join CLImageVariant v on i.Id=v.Id and v.SiteId=@SiteId
		where i.Id=@MasterMainImageId

		select
			@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
			@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end,
			@AltText = case when @MasterAltText is null or @AltText <> @MasterAltText and @AltText is not null then @AltText else null end

		--main
		set @Output = concat(@Output, ' | main=')
		if @VariantMainImageId is not null begin
			set @Output = concat(@Output, 'updated')
			update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantMainImageId and SiteId=@SiteId
		end else begin
			set @Output = concat(@Output, 'inserted')
			insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
				values(@MasterMainImageId, @SiteId, @Title, @Description, @AltText, @MainImageTypeId, NULL, getutcdate(), @ModifiedBy)
		end

		--preview
		set @Output = concat(@Output, ' | preview=')
		if @MasterPreviewImageId is null begin
			set @Output = concat(@Output, 'skipped')
		end else begin
			if @VariantPreviewImageId is not null begin
				set @Output = concat(@Output, 'updated')
				update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantPreviewImageId and SiteId=@SiteId
			end else begin
				set @Output = concat(@Output, 'inserted')
				insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
					values(@MasterPreviewImageId, @SiteId, @Title, @Description, @AltText, @PreviewImageTypeId, NULL, getutcdate(), @ModifiedBy)
			end
		end

		--mini
		set @Output = concat(@Output, ' | mini=')
		if @MasterMiniImageId is null begin
			set @Output = concat(@Output, 'skipped')
		end else begin
			if @VariantMiniImageId is not null begin
				set @Output = concat(@Output, 'updated')
				update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantMiniImageId and SiteId=@SiteId
			end else begin
				set @Output = concat(@Output, 'inserted')
				insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
					values(@MasterMiniImageId, @SiteId, @Title, @Description, @AltText, @MiniImageTypeId, NULL, getutcdate(), @ModifiedBy)
			end
		end

		--system thumb
		set @Output = concat(@Output, ' | systemthumb=')
		if @MasterSystemThumbImageId is null begin
			set @Output = concat(@Output, 'skipped')
		end else begin
			if @VariantSystemThumbImageId is not null begin
				set @Output = concat(@Output, 'updated')
				update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantSystemThumbImageId and SiteId=@SiteId
			end else begin
				set @Output = concat(@Output, 'inserted')
				insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
					values(@MasterSystemThumbImageId, @SiteId, @Title, @Description, @AltText, @SystemThumbImageTypeId, NULL, getutcdate(), @ModifiedBy)
			end
		end

		--thumbnail
		set @Output = concat(@Output, ' | thumbnail=')
		if @MasterThumbnailImageId is null begin
			set @Output = concat(@Output, 'skipped')
		end else begin
			if @VariantThumbnailImageId is not null begin
				set @Output = concat(@Output, 'updated')
				update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantThumbnailImageId and SiteId=@SiteId
			end else begin
				set @Output = concat(@Output, 'inserted')
				insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
					values(@MasterThumbnailImageId, @SiteId, @Title, @Description, @AltText, @ThumbnailImageTypeId, NULL, getutcdate(), @ModifiedBy)
			end
		end

		commit tran
		select @MasterMainImageId, @Output
	end try
	begin catch
		rollback tran
		DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE(),
				@ErrorState int				= ERROR_STATE(),
				@ErrorSeverity int			= ERROR_SEVERITY()
        RAISERROR(@ErrorMessage,@ErrorSeverity, @ErrorState)
	end catch
end

GO
IF(OBJECT_ID('BLD_ProductImport_Product') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_Product
GO
CREATE PROCEDURE [dbo].[BLD_ProductImport_Product]
	@ProductIDUser nvarchar(max),
	@ProductType nvarchar(max) = null,
	@UrlFriendlyTitle nvarchar(max) = null,
	@Title nvarchar(max) = null,
	@ShortDescription nvarchar(max) = null,
	@Description nvarchar(max) = null,
	@LongDescription nvarchar(max) = null,
	@SEODescription nvarchar(max) = null,
	@SEOKeywords nvarchar(max) = null,
	@SEOTitle nvarchar(max) = null,
	@SEOH1 nvarchar(max) = null,
	@SEOFriendlyURL nvarchar(max) = null,
	@IsActive bit = null,
	@IsOnline bit = null,
	@IsBundle bit = null,
	@TaxExempt bit = null,
	@OperatorId uniqueidentifier = null,
	@ProductIDUserOld nvarchar(max) = null,
	@ProductIdGuid uniqueidentifier = null,
	@IsShared bit
AS
BEGIN
	SET NOCOUNT ON;

	declare @actionType int = 0

	BEGIN TRY
        BEGIN TRAN

		if @OperatorId is null
		begin
			set @OperatorId = (select top 1 CreatedBy from PRProductType where CreatedBy is not null);
		end
		

		declare @ProductId uniqueidentifier;
		declare @IsNewProduct bit
		declare @ProductTypeId uniqueidentifier;
		declare @TaxCategoryId uniqueidentifier;
		Set @IsNewProduct = 1

		If (@ProductIdGuid IS NOT NULL)
			SET @ProductId = @ProductIdGuid

		-- TODO: fix this since title may not be unique
		select @ProductTypeId = Id from PRProductType where Title like @ProductType

		IF @ProductId IS NULL
		BEGIN 
			if @ProductIDUserOld is null
			begin
		
				select @ProductId = Id FROM PRProduct where ProductIDUser like @ProductIDUser;
			
			end else
			begin
				select @ProductId = Id FROM PRProduct where ProductIDUser like @ProductIDUserOld;
			end
		END

		IF EXISTS(Select 1 From PRProduct where Id = @ProductId)
			SET @IsNewProduct = 0

		--INSERTING THE PRODUCT
		if @IsNewProduct = 1
		begin
		
			if(@TaxExempt=1)
			begin
				select @TaxCategoryId = Id from PRTaxCategory where Title like '%non%';
			end else
			begin
				select @TaxCategoryId = Id from PRTaxCategory where Title not like '%non%';
			end


			if(@ProductTypeId is null)RAISERROR ('Unknown product type!', 16, 1);
			if(@TaxCategoryId is null)RAISERROR ('Unknown tax category!', 16, 1);
		
			IF(@ProductId IS NULL)
				SET @ProductId = newid();

			insert into PRProduct (
				Id, 
				ProductIdUser, 
				Title, 
				UrlFriendlyTitle, 
				ShortDescription, 
				LongDescription, 
				Description, 
				KeyWord, 
				IsActive, 
				ProductTypeId, 
				ProductStyle, 
				SiteId, 
				CreatedDate, 
				CreatedBy, 
				ModifiedDate, 
				ModifiedBy, 
				Status, 
				IsBundle, 
				BundleCompositionLastModified, 
				TaxCategoryId,
				TaxExempt,
				SEOTitle,
				SEOH1, 
				SEODescription, 
				SEOKeywords, 
				SEOFriendlyUrl,
				IsShared)
			values (
				@ProductId, 
				@ProductIDUser, 
				@Title, 
				@UrlFriendlyTitle, 
				@ShortDescription, 
				@LongDescription, 
				@Description, 
				'', 
				@IsActive, 
				@ProductTypeId, 
				null, 
				'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', 
				GETUTCDATE(), 
				@OperatorId, 
				null, 
				null, 
				1, 
				@IsBundle, 
				null, 
				@TaxCategoryId, 
				@TaxExempt, 
				@SEOTitle,
				@SEOH1,
				@SEODescription, 
				@SEOKeywords, 
				@SEOFriendlyURL,
				@IsShared);

			set @actionType = 1

		end else
		begin
		
			update PRProduct set
				ProductIDUser = @ProductIDUser,
				Title = isnull(@Title, Title),
				UrlFriendlyTitle = isnull(@UrlFriendlyTitle, UrlFriendlyTitle),
				ShortDescription = isnull(@ShortDescription, ShortDescription),
				LongDescription = isnull(@LongDescription, LongDescription),
				Description = isnull(@Description, Description),
				-- Keyword = isnull(@Keyword, Keyword),
				IsActive = isnull(@IsActive, IsActive),
				ProductTypeID = isnull(@ProductTypeId, ProductTypeID),
				ModifiedDate = GETUTCDATE(),
				ModifiedBy = isnull(@OperatorId, ModifiedBy),
				IsBundle = isnull(@IsBundle, IsBundle),
				TaxCategoryID = isnull(@TaxCategoryID, TaxCategoryID),
				TaxExempt = isnull(@TaxExempt, TaxExempt),
				SEOTitle = @SEOTitle,
				SEOH1 = @SEOH1,
				SEODescription = @SEODescription,
				SEOKeywords = @SEOKeywords,
				SEOFriendlyURL = @SEOFriendlyURL,
				IsShared = @IsShared
			where Id = @ProductId;

			set @actionType = 2
			
		end

		COMMIT TRAN

		select @actionType, @ProductId

    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX)
        SET @ErrorMessage = ERROR_MESSAGE()
        RAISERROR(@ErrorMessage,16, 1)
    END CATCH

END

GO

IF(OBJECT_ID('BLD_ProductImport_ProductAttributeVariant') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_ProductAttributeVariant
GO
CREATE PROCEDURE [dbo].[BLD_ProductImport_ProductAttributeVariant]
(
	@SiteId					uniqueidentifier,
	@ProductIdUser			nvarchar(50),
	@AttributeTitle			nvarchar(4000),
	@AttributeCategoryName	nvarchar(2000),
	@Sku					nvarchar(255),
	@IsSkuLevel				bit,
	@Value					nvarchar(max),
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	begin try
		
		declare @MasterProductId			uniqueidentifier,
				@MasterProductAttributeId	uniqueidentifier,
				@MasterAttributeId			uniqueidentifier,
				@MasterAttributeEnumId		uniqueidentifier,
				@MasterSkuId				uniqueidentifier,
				@MasterValue				nvarchar(max),
				@NumbericValue				int = iif(@Value like 'true', 1, iif(@Value like 'false', 0, null)),
				@IsSkipped					bit = 1,
				@Output						varchar(512),
				@IsEnum bit

		if @SiteId is null or @ProductIdUser is null or @AttributeTitle is null or @AttributeCategoryName is null or (@IsSkuLevel=1 and @Sku is null)
			raiserror(15600, -1, -1, 'SiteId, ProductIdUser, AttributeTitle, AttributeCategoryName, or Sku is null')

		begin tran
		select
			@MasterProductId=p.Id,
			@MasterProductAttributeId=pa.Id,
			@MasterAttributeId=pa.AttributeId,
			@MasterAttributeEnumId = e.Id,
			@MasterSkuId=s.Id,
			@IsEnum = a.IsEnum
		from PRProductAttribute pa
			join PRProduct p on pa.ProductId=p.Id
			left join PRProductSKU s on p.Id=s.ProductId and s.SKU=@Sku
			join ATAttribute a on pa.AttributeId=a.Id
			left join ATAttributeVariant av on a.Id=av.Id and av.SiteId=@SiteId
			join ATAttributeCategoryItem i on a.Id=i.AttributeId
			join ATAttributeCategory c on i.CategoryId=c.Id and c.Name=@AttributeCategoryName
			left join ATAttributeEnum e on a.Id=e.AttributeID
			left join ATAttributeEnumVariant ev on e.Id=ev.Id and ev.SiteId=@SiteId
		where
			p.ProductIDUser like @ProductIdUser and
			pa.IsSKULevel = isnull(@IsSkuLevel, 0) and
			((a.IsEnum=1 and isnull(ev.Title, e.Title) like @Value and e.Status = 1) or
			(a.IsEnum=0 and isnull(av.Title, a.Title) like @AttributeTitle))

		if @MasterProductId is null
		begin
			set @Output = concat(
				'Could not find product attribute for ProductId=', @ProductIdUser, 
				' | sku=', @Sku,
				' | category=', @AttributeCategoryName, 
				' | attribute title=', @AttributeTitle,
				' | value=', @Value)
			raiserror(15600, -1, -1, @Output)
		end

		if @IsSkuLevel=1
		begin
			declare @MasterProductSkuAttributeValueId	uniqueidentifier,
					@ProductSkuAttributeValueVariantId	uniqueidentifier

			select 
				@MasterProductSkuAttributeValueId=m.Id, 
				@MasterValue=iif(v.Value is null, m.Value, null),
				@ProductSkuAttributeValueVariantId=v.Id
			from PRProductSkuAttributeValue m
				left join PRProductSkuAttributeValueVariant v on m.Id=v.Id and v.SiteId=@SiteId
				left join ATAttributeEnum ae on m.attributeId = ae.attributeId
				left join ATAttributeEnumVariant aev on m.attributeId = aev.attributeId
			where m.SKUId=@MasterSkuId and m.AttributeId=@MasterAttributeId and ProductAttributeId=@MasterProductAttributeId
			      AND ((@IsEnum = 1 and (ae.Title = @Value or aev.Title = @Value) and m.AttributeEnumId = ae.id) or @IsEnum=0)

			if @MasterProductSkuAttributeValueId is null
			begin
				set @Output = concat('Unable to find PRProductSkuAttributeValue with sku/attribute/productattribute/isenum/value/siteid: ', @MasterSkuId, '|', @MasterAttributeId, '|', @MasterProductAttributeId, '|', @IsEnum, '|', @Value, '|', @SiteId)
				raiserror(15600, -1, -1, @Output)
			end
			
			select @Value = case when @MasterValue is null or @Value <> @MasterValue and @Value is not null then @Value else null end

			if @ProductSkuAttributeValueVariantId is not null
			begin
				update PRProductSkuAttributeValueVariant set
					Value=@Value,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getutcdate()
				where Id=@ProductSkuAttributeValueVariantId and SiteId=@SiteId

				set @Output = concat('updated PRProductSkuAttributeValueVariant id=', @MasterProductSkuAttributeValueId, ' value=', @Value)
			end
			else
			begin
				if @Value is not null
				begin
					insert into PRProductSkuAttributeValueVariant (Id, SiteId, SkuId, AttributeId, AttributeEnumId, Value, ModifiedBy, ModifiedDate)
						values (@MasterProductSkuAttributeValueId, @SiteId, @MasterSkuId, @MasterAttributeId, @MasterAttributeEnumId, @Value, @ModifiedBy, getutcdate())
					set @IsSkipped = 0
				end
				set @Output = concat(
					'Sku level attribute value variant ', 
					iif(@IsSkipped=1, 'skipped', 'inserted '), 
					' id=', @ProductIdUser, 
					' | sku', @Sku,
					' | category', @AttributeCategoryName, 
					' | attribute', @AttributeTitle, 
					' | value', @Value)
			end
		end
		else
		begin
			--Product Level Attribute
			declare @MasterProductAttributeValueId	uniqueidentifier,
					@ProductAttributeValueVariantId	uniqueidentifier

			select 
				@MasterProductAttributeValueId=m.Id, 
				@MasterValue=iif(v.Value is null, m.Value, null), 
				@ProductAttributeValueVariantId=v.Id
			from PRProductAttributeValue m
				left join PRProductAttributeValueVariant v on m.Id=v.Id and v.SiteId=@SiteId
				left join ATAttributeEnum ae on m.attributeId = ae.attributeId
				left join ATAttributeEnumVariant aev on m.attributeId = aev.attributeId
			where m.ProductId=@MasterProductId and m.AttributeId=@MasterAttributeId and ProductAttributeId=@MasterProductAttributeId
			      AND ((@IsEnum = 1 and (ae.Title = @Value or aev.Title = @Value) and m.AttributeEnumId = ae.id) or @IsEnum=0)

			if @MasterProductAttributeValueId is null
			begin
				set @Output = concat('Unable to find PRProductAttributeValue with product/attribute/productattribute/isenum/value/siteid: ', @MasterProductId, '|', @MasterAttributeId, '|', @MasterProductAttributeId, '|', @IsEnum, '|', @Value, '|', @SiteId)
				raiserror(15600, -1, -1, @Output)
			end

			select @Value = case when (@MasterValue is null or @Value <> @MasterValue) and @Value is not null then @Value else null end
			if @ProductAttributeValueVariantId is not null
			begin
				update PRProductAttributeValueVariant set 
					Value = @Value, 
					ModifiedBy = @ModifiedBy, 
					ModifiedDate = getutcdate()
				where Id=@ProductAttributeValueVariantId and SiteId=@SiteId
				set @Output = concat('updated PRProductAttributeValueVariant id=', @ProductAttributeValueVariantId, ' value=', @Value, ' mastervalue=', @MasterValue)
			end
			else
			begin
				if @Value is not null
				begin
					insert into PRProductAttributeValueVariant (Id, SiteId, ProductId, AttributeId, AttributeEnumId, Value, ModifiedBy, ModifiedDate) 
						values (@MasterProductAttributeValueId, @SiteId, @MasterProductId, @MasterAttributeId, @MasterAttributeEnumId, @Value, @ModifiedBy, getutcdate())
					set @IsSkipped = 0
				end
				
				set @Output = concat(
					'Product level attribute value variant ', 
					iif(@IsSkipped=1, 'skipped', 'inserted '), 
					'id=', @ProductIdUser, 
					' | category=', @AttributeCategoryName, 
					' | attribute', @AttributeTitle, 
					' | value', @Value)
			end
		end

		commit tran
		select @MasterProductAttributeValueId, @MasterProductSkuAttributeValueId, @Output
	end try
	begin catch
		rollback tran
		DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE(),
				@ErrorState int				= ERROR_STATE(),
				@ErrorSeverity int			= ERROR_SEVERITY()
        RAISERROR(@ErrorMessage,@ErrorSeverity, @ErrorState)

	end catch
END
go
print 'Created stored procedure BLD_ProductImport_ProductAttributeVariant'
go



GO
IF(OBJECT_ID('BLD_ProductImport_ProductSkuVariant') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_ProductSkuVariant
GO
CREATE procedure [dbo].[BLD_ProductImport_ProductSkuVariant]
(
	@SiteId						uniqueidentifier,
	@ProductId					nvarchar(100),
	@Sku						nvarchar(255),
	@Title						nvarchar(555),
	--@Description				nvarchar(max),
	--@Sequence					int,
	@IsActive					bit,
	@IsOnline					bit,
	@ListPrice					money,
	@UnitCost					money,
	--@WholesalePrice				money,
	@Measure					nvarchar(50),
	@OrderMinimum				decimal(18,4),
	@OrderIncrement				decimal(18,4),
	@HandlingCharges			decimal(18,4),
	@SelfShippable				bit,
	@AvailableForBackOrder		bit,
	@BackOrderLimit				decimal(18,4),
	@IsUnlimitedQuantity		bit,
	--@MaximumDiscountPercentage	money,
	--@FreeShipping				bit,
	--@UserDefinedPrice			bit,
	@ModifiedBy					uniqueidentifier
)
as
begin
	begin try

		declare @ProductSkuVariantId				uniqueidentifier
		declare @MasterProductSkuId					uniqueidentifier
		declare @MasterTitle						nvarchar(555)
		declare @MasterSequence						int
		declare @MasterIsActive						bit
		declare @MasterIsOnline						bit
		declare @MasterListPrice					money
		declare @MasterUnitCost						money
		declare @MasterWholesalePrice				money
		declare @MasterMeasure						nvarchar(50)
		declare @MasterOrderMinimum					decimal(18,4)
		declare @MasterOrderIncrement				decimal(18,4)
		declare @MasterHandlingCharges				decimal(18,4)
		declare @MasterSelfShippable				bit
		declare @MasterAvailableForBackOrder		bit
		declare @MasterBackOrderLimit				decimal(18,4)
		declare @MasterIsUnlimitedQuantity			bit
		declare @MasterMaximumDiscountPercentage	money
		declare @MasterFreeShipping					bit
		declare @MasterUserDefinedPrice				bit
		declare @Output								nvarchar(512) = ''
		declare @IsSkipped							bit = 1

		if (@SiteId is null or @ProductId is null or @ModifiedBy is null)
			raiserror(15600, -1, -1, 'BLD_ProductImport_ProductSkuVariant SiteId, ModifiedBy, or ProductId is null')

		begin tran
		select 
			@MasterProductSkuId=ps.Id,
			@MasterTitle=iif(v.Title is null, ps.Title, null),
			@MasterSequence=ps.Sequence,
			@MasterIsActive=iif(v.IsActive is null, ps.IsActive, null),
			@MasterIsOnline=iif(v.IsOnline is null, ps.IsOnline, null),
			@MasterListPrice=iif(v.ListPrice is null, ps.ListPrice, null),
			@MasterUnitCost=iif(v.UnitCost is null, ps.UnitCost, null),
			@MasterWholesalePrice=ps.WholesalePrice,
			@MasterMeasure=iif(v.Measure is null, ps.Measure, null),
			@MasterOrderMinimum=iif(v.OrderMinimum is null, ps.OrderMinimum, null),
			@MasterOrderIncrement=iif(v.OrderIncrement is null, ps.OrderIncrement, null),
			@MasterHandlingCharges=iif(v.HandlingCharges is null, ps.HandlingCharges, null),
			@MasterSelfShippable=iif(v.SelfShippable is null, ps.SelfShippable, null),
			@MasterAvailableForBackOrder=iif(v.AvailableForBackOrder is null, ps.AvailableForBackOrder, null),
			@MasterBackOrderLimit=iif(v.BackOrderLimit is null, ps.BackOrderLimit, null),
			@MasterIsUnlimitedQuantity=iif(v.IsUnlimitedQuantity is null, ps.IsUnlimitedQuantity, null),
			@MasterMaximumDiscountPercentage=ps.MaximumDiscountPercentage,
			@MasterFreeShipping=ps.FreeShipping,
			@MasterUserDefinedPrice=ps.UserDefinedPrice
		from PRProductSku ps
			left join PRProductSKUVariant v on ps.Id=v.Id and v.SiteId=@SiteId
			join PRProduct p on ps.ProductId=p.Id
			join SISite s on ps.SiteId=s.Id
		where p.ProductIDUser=@ProductId and ps.SKU=@Sku and s.Id=s.MasterSiteId

		if @MasterProductSkuId is null
		begin
			set @Output = concat('Product Id [', @ProductId, '] with SKU [', @Sku, 'does not exist on master site')
		end 
		else
		begin
			select @ProductSkuVariantId=Id from PRProductSKUVariant where Id=@MasterProductSkuId and SiteId=@SiteId
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@IsActive = case when @MasterIsActive is null or @IsActive <> @MasterIsActive and @IsActive is not null then @IsActive else null end,
				@IsOnline = case when @MasterIsOnline is null or @IsOnline <> @MasterIsOnline and @IsOnline is not null then @IsOnline else null end,
				@ListPrice = case when @MasterListPrice is null or @ListPrice <> @MasterListPrice and @ListPrice is not null then @ListPrice else null end,
				@UnitCost = case when @MasterUnitCost is null or @UnitCost <> @MasterUnitCost and @UnitCost is not null then @UnitCost else null end,
				@Measure = case when @MasterMeasure is null or @Measure <> @MasterMeasure and @Measure is not null then @Measure else null end,
				@OrderMinimum = case when @MasterOrderMinimum is null or @OrderMinimum <> @MasterOrderMinimum and @OrderMinimum is not null then @OrderMinimum else null end,
				@OrderIncrement = case when @MasterOrderIncrement is null or @OrderIncrement <> @MasterOrderIncrement and @OrderIncrement is not null then @OrderIncrement else null end,
				@HandlingCharges = case when @MasterHandlingCharges is null or @HandlingCharges <> @MasterHandlingCharges and @HandlingCharges is not null then @HandlingCharges else null end,
				@SelfShippable = case when @MasterSelfShippable is null or @SelfShippable <> @MasterSelfShippable and @SelfShippable is not null then @SelfShippable else null end,
				@AvailableForBackOrder = case when @MasterAvailableForBackOrder is null or @AvailableForBackOrder <> @MasterAvailableForBackOrder and @AvailableForBackOrder is not null then @AvailableForBackOrder else null end,
				@BackOrderLimit = case when @MasterBackOrderLimit is null or @BackOrderLimit <> @MasterBackOrderLimit and @BackOrderLimit is not null then @BackOrderLimit else null end,
				@IsUnlimitedQuantity = case when @MasterIsUnlimitedQuantity is null or @IsUnlimitedQuantity <> @MasterIsUnlimitedQuantity and @IsUnlimitedQuantity is not null then @IsUnlimitedQuantity else null end

			if @ProductSkuVariantId is not null
			begin
				update PRProductSKUVariant set
					Title=@Title,
					IsActive=@IsActive,
					IsOnline=@IsOnline,
					ListPrice=@ListPrice,
					UnitCost=@UnitCost,
					Measure=@Measure,
					OrderMinimum=@OrderMinimum,
					OrderIncrement=@OrderIncrement,
					HandlingCharges=@HandlingCharges,
					SelfShippable=@SelfShippable,
					AvailableForBackOrder=@AvailableForBackOrder,
					BackOrderLimit=@BackOrderLimit,
					IsUnlimitedQuantity=@IsUnlimitedQuantity,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getutcdate()
				where Id=@ProductSkuVariantId and SiteId=@SiteId

				set @Output = concat('updated PRProductSkuVariant Id [', @ProductSkuVariantId, ']')
			end
			else
			begin
				if coalesce(cast(@Title as sql_variant), @IsActive, @IsOnline, @ListPrice, @UnitCost, @Measure, @OrderMinimum, @OrderIncrement, @HandlingCharges, @SelfShippable, @AvailableForBackOrder, @BackOrderLimit, @IsUnlimitedQuantity) is not null
				begin
					insert into PRProductSKUVariant (SiteId, Id, Title, IsActive, IsOnline, ListPrice, UnitCost, Measure, OrderMinimum, OrderIncrement, HandlingCharges, SelfShippable, AvailableForBackOrder, BackOrderLimit, IsUnlimitedQuantity, ModifiedBy, ModifiedDate)
						values (@SiteId, @MasterProductSkuId, @Title, @IsActive, @IsOnline, @ListPrice, @UnitCost, @Measure, @OrderMinimum, @OrderIncrement, @HandlingCharges, @SelfShippable, @AvailableForBackOrder, @BackOrderLimit, @IsUnlimitedQuantity, @ModifiedBy, getutcdate())
					set @IsSkipped = 0
				end

				set @Output = concat(
					'PRProductSkuVariant ', 
					iif(@IsSkipped=1, 'skipped', concat('inserted Title=', @Title, ' | sku=', @Sku)))
			end
			
		end
		commit tran
		select @Output
	end try
	begin catch
		rollback tran
		declare @ErrorMessage nvarchar(4000) = error_message(),
				@ErrorSeverity int = error_severity(),
				@ErrorState int = error_state()
		print concat('BLD_ProductImport_ProductSkuVariant error: severity= ', @ErrorSeverity, ' state=', @ErrorState, ' message=', @ErrorMessage)
		raiserror(@ErrorMessage, @ErrorSeverity, @ErrorState)
	end catch
end
go
print 'Created stored procedure BLD_ProductImport_ProductSkuVariant'
go

IF(OBJECT_ID('BLD_ProductImport_ProductTypeVariant') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_ProductTypeVariant
GO
create procedure [dbo].[BLD_ProductImport_ProductTypeVariant] 
(
	@SiteId					uniqueidentifier,
	@ProductTypeId			uniqueidentifier,
	@Title					nvarchar(50),
	@Description			nvarchar(200),
	@IsDownloadableMedia	bit,
	--@FriendlyName			nvarchar(50),
	--@CMSPageId			uniqueidentifier,
	--@Status				int,
	@ModifiedBy				uniqueidentifier
)
as
begin
	begin try
		declare @MasterProductTypeId		uniqueidentifier
		declare @ProductTypeVariantId		uniqueidentifier
		declare @MasterTitle				nvarchar(50)
		declare @MasterDescription			nvarchar(200)
		declare @MasterFriendlyName			nvarchar(50)
		declare @MasterStatus				int
		declare @MasterIsDownloadableMedia	bit
		declare @Output						nvarchar(512) = ''
		declare @IsSkipped					bit = 1

		if (@SiteId is null or @ModifiedBy is null)
			raiserror(15600, -1, -1, 'BLD_ProductImport_ProductTypeVariant SiteId, ModifiedBy or ProductTypeId is null')
		
		begin tran
		select 
			@MasterProductTypeId=pt.Id, 
			@MasterTitle=iif(v.Title is null, pt.Title,  null),
			@MasterDescription=iif(v.Description is null, pt.Description,  null),
			@MasterFriendlyName=pt.FriendlyName, 
			@MasterIsDownloadableMedia=pt.IsDownloadableMedia,
			@MasterStatus=pt.Status
		from PRProductType pt
			left join PRProductTypeVariant v on pt.Id=v.Id and v.SiteId=@SiteId
			join SISite s on pt.SiteId=s.Id
		where pt.Id=@ProductTypeId and s.Id=s.MasterSiteId

		if @MasterProductTypeId is null
		begin
			set @Output = concat('Product Type id=[', @ProductTypeId, '] does not exist on master site')
		end
		else
		begin
			select @ProductTypeVariantId=Id from PRProductTypeVariant where id=@MasterProductTypeId and SiteId=@SiteId
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end,
				@IsDownloadableMedia = case when @MasterIsDownloadableMedia is null or @IsDownloadableMedia <> @MasterIsDownloadableMedia and @IsDownloadableMedia is not null then @IsDownloadableMedia else null end

			if @ProductTypeVariantId is not null 
			begin
				update PRProductTypeVariant set
					Title=@Title,
					Description=@Description,
					FriendlyName=@MasterFriendlyName,
					--CMSPageId=@CMSPageId,
					--Status=@Status
					ModifiedDate=getutcdate(),
					ModifiedBy=@ModifiedBy
				where Id=@MasterProductTypeId and SiteId=@SiteId

				set @Output = concat('updated PRProductTypeVariant ', @MasterProductTypeId, ' ', @Title, ' ', @Description)
			end
			else
			begin
				if coalesce(@Title, @Description) is not null
				begin
					insert into PRProductTypeVariant (Id, Title, Description, FriendlyName, SiteId, ModifiedDate, ModifiedBy) 
						values (@MasterProductTypeId, @Title, @Description, null, @SiteId, getutcdate(), @ModifiedBy)
					set @IsSkipped = 0
				end

				set @Output = concat('PRProductTypeVariant ', iif(@IsSkipped=1, 'skipped', concat('inserted. title=', @Title)))
			end

		end
		commit tran
		select @MasterProductTypeId, @Output
	end try
	begin catch
		rollback tran
		declare @ErrorMessage nvarchar(4000) = error_message()
		declare @ErrorSeverity int = error_severity()
		declare @ErrorState int = error_state()
		print concat('BLD_ProductImport_ProductTypeVariant error: severity= ', @ErrorSeverity, ' state=', @ErrorState, ' message=', @ErrorMessage)
		raiserror(@ErrorMessage, @ErrorSeverity, @ErrorState)
	end catch
end
go
print 'Created stored procedure BLD_ProductImport_ProductTypeVariant'
go


IF(OBJECT_ID('BLD_ProductImport_ProductVariant') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_ProductVariant
GO
CREATE PROCEDURE [dbo].[BLD_ProductImport_ProductVariant]
	@SiteId				uniqueidentifier,
	@ProductIDUser		nvarchar(100),
	@ProductTypeName	nvarchar(50),
	@ProductTypeId		uniqueidentifier,
	@Title				nvarchar(555),
	@ShortDescription	nvarchar(4000),
	@Description	nvarchar(4000),
	@LongDescription	nvarchar(4000),
	@IsActive			bit,
	@SEOTitle			nvarchar(255),
	@SEOH1				nvarchar(255),
	@SEODescription		nvarchar(4000),
	@SEOKeywords		nvarchar(4000),
	@SEOFriendlyURL		nvarchar(4000),
	@URLFriendlyTitle	nvarchar(255),
	@ModifiedBy			uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	declare @ActionType				int					= 0
	declare @MasterSiteId			uniqueidentifier
	declare @MasterProductId		uniqueidentifier
	declare @MasterProductTypeId	uniqueidentifier
	declare @MasterTitle			nvarchar(555)
	declare @MasterShortDescription nvarchar(4000)
	declare @MasterDescription nvarchar(4000)
	declare @MasterLongDescription	nvarchar(4000)
	declare @MasterIsActive			bit
	declare @MasterSEOTitle			nvarchar(255)
	declare @MasterSEOH1			nvarchar(255)
	declare @MasterSEODescription	nvarchar(4000)
	declare @MasterSEOKeywords		nvarchar(4000)
	declare @MasterSEOFriendlyURL	nvarchar(255)
	declare @MasterURLFriendlyTitle	nvarchar(255)
	declare @IsSkipped				bit = 1
	declare @Output					nvarchar(512)

	if coalesce(cast(@SiteId as sql_variant), @ProductIdUser, @ProductTypeId) is null
		raiserror(15600, -1, -1, 'SiteId, ProductIdUser, or ProductTypeId is null')

	BEGIN TRY
        BEGIN TRAN
		
		select @MasterSiteId=Id from SISite where Id=MasterSiteId

		select 
			@MasterProductId=p.Id, 
			@MasterProductTypeId=p.ProductTypeID,
			@MasterTitle=iif(v.Title is null, p.Title, null),
			@MasterShortDescription=iif(v.ShortDescription is null, p.ShortDescription, null),
			@MasterDescription=iif(v.Description is null, p.Description, null),
			@MasterLongDescription=iif(v.LongDescription is null, p.LongDescription, null),
			@MasterIsActive=iif(v.IsActive is null, p.IsActive, null),
			@MasterSEOTitle=iif(v.SEOTitle is null, p.SEOTitle, null),
			@MasterSEOH1=iif(v.SEOH1 is null, p.SEOH1, null),
			@MasterSEODescription=iif(v.SEODescription is null, p.SEODescription, null),
			@MasterSEOKeywords=iif(v.SEOKeywords is null, p.SEOKeywords, null),
			@MasterSEOFriendlyURL=iif(v.SEOFriendlyUrl is null, p.SEOFriendlyUrl, null),
			@MasterURLFriendlyTitle=iif(v.UrlFriendlyTitle is null, p.UrlFriendlyTitle, null)
		from PRProduct p
			left join PRProductVariant v on p.Id=v.Id and v.SiteId=@SiteId
		where p.ProductIDUser like @ProductIDUser and p.ProductTypeID like @ProductTypeId and p.SiteId=@MasterSiteId

		if @MasterProductId is null or @MasterProductTypeId is null
		begin
			set @Output = concat('Product Id: [', @ProductIDUser, ']/[', @ProductTypeId, '] does not exist.')
			raiserror(15600, -1, -1, @Output)
		end
		else
		begin
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@ShortDescription = case when @MasterShortDescription is null or @ShortDescription <> @MasterShortDescription and @ShortDescription is not null then @ShortDescription else null end,
				@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end,
				@LongDescription = case when @MasterLongDescription is null or @LongDescription <> @MasterLongDescription and @LongDescription is not null then @LongDescription else null end,
				@IsActive = case when @MasterIsActive is null or @IsActive <> @MasterIsActive and @IsActive is not null then @IsActive else null end,
				@SEOTitle = case when @MasterSEOTitle is null or @SEOTitle <> @MasterSEOTitle and @SEOTitle is not null then @SEOTitle else null end,
				@SEOH1 = case when @MasterSEOH1 is null or @SEOH1 <> @MasterSEOH1 and @SEOH1 is not null then @SEOH1 else null end,
				@SEODescription = case when @MasterSEODescription is null or @SEODescription <> @MasterSEODescription and @SEODescription is not null then @SEODescription else null end,
				@SEOKeywords = case when @MasterSEOKeywords is null or @SEOKeywords <> @MasterSEOKeywords and @SEOKeywords is not null then @SEOKeywords else null end,
				@SEOFriendlyURL = case when @MasterSEOFriendlyURL is null or @SEOFriendlyURL <> @MasterSEOFriendlyURL and @SEOFriendlyURL is not null then @SEOFriendlyURL else null end,
				@URLFriendlyTitle = case when @MasterURLFriendlyTitle is null or @URLFriendlyTitle <> @MasterURLFriendlyTitle and @URLFriendlyTitle is not null then @URLFriendlyTitle else null end

			if exists (select 1 from PRProductVariant where Id=@MasterProductId and SiteId=@SiteId)
			begin
				update PRProductVariant set
					Title=@Title,
					ShortDescription=@ShortDescription,
					LongDescription=@LongDescription,
					Description=@Description,
					IsActive=@IsActive,
					SEOTitle=@SEOTitle,
					SEOH1=@SEOH1,
					SEODescription=@SEODescription,
					SEOKeywords=@SEOKeywords,
					SEOFriendlyURL=@SEOFriendlyURL,
					UrlFriendlyTitle=@URLFriendlyTitle,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getutcdate()
				where Id=@MasterProductId and SiteId=@SiteId

				set @Output = concat('updated PRProductVariant id=', @MasterProductId)
				set @actionType = 2
			end
			else
			begin
				if coalesce(cast(@Title as sql_variant), @ShortDescription, @LongDescription, @IsActive, @SEOTitle, @SEODescription, @SEOKeywords, @SEOFriendlyURL) is not null
				begin
					insert into PRProductVariant (SiteId, Id, ProductTypeId, Title, ShortDescription, LongDescription, Description, IsActive, SEOTitle, SEOH1, SEODescription, SEOKeywords, SEOFriendlyURL, URLFriendlyTitle, ModifiedBy, ModifiedDate)
						values (@SiteId, @MasterProductId, @MasterProductTypeId, @Title, @ShortDescription, @LongDescription, @Description, @IsActive, @SEOTitle, @SEOH1, @SEODescription, @SEOKeywords, @SEOFriendlyURL, @URLFriendlyTitle, @Modifiedby, getutcdate())
					set @IsSkipped = 0
				end

				set @Output = concat(
					'PRProductVariant ', 
					iif(
						@IsSkipped=1, 
						'skipped', 
						concat('inserted Name=', @Title, ' | product type=', @ProductTypeName)))
				set @actionType = 2
			end

		end
		COMMIT TRAN
		select @MasterProductid, @ActionType, @Output
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		declare @ErrorState int = ERROR_STATE()
		declare @ErrorSeverity int = ERROR_SEVERITY()
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
    END CATCH

END
GO

IF(OBJECT_ID('BLD_ProductImport_Sku') IS NOT NULL)
	DROP PROCEDURE BLD_ProductImport_Sku
GO

CREATE PROCEDURE [dbo].[BLD_ProductImport_Sku]

@ProductIDUser nvarchar(max),
@IsActive bit = 1,
@IsOnline bit = 1,
@TaxExempt bit = 0,
@SKU nvarchar(max),
@SKUTitle nvarchar(max),
@Quantity decimal(18,4) = 0,
@OperatorId uniqueidentifier = null,
@ListPrice money,
@UnitCost money,
@Measure nvarchar(max) = null,
@OrderMinimum DECIMAL(18,4) = 1,
@OrderIncrement decimal(18,4) = 1,
@Handling decimal(18,4) = 0,
@Length decimal(18,4) = 0,
@Height decimal(18,4) = 0,
@Width decimal(18,4) = 0,
@Weight decimal(18,4) = 0,
@Selfshippable bit = 0,
@AvailableForBackOrder bit = 0,
@BackOrderLimit decimal(18,4) = 0,
@IsUnlimitedQuantity bit = 0,
@FreeShipping bit =0,
@UserDefinedPrice bit =0,
@ProductSkuIdGuid uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;

	declare @actionType int = 0

	set @Length = case when @Length = -1 then null else @Length end
	set @Width = case when @Width = -1 then null else @Width end
	set @Height = case when @Height = -1 then null else @Height end
	set @Weight = case when @Weight = -1 then null else @Weight end

	if @OrderMinimum<1 RAISERROR ('OrderQuantityMinimum must be greater than or equal to 1!', 16, 1);
	if @OrderIncrement<1 RAISERROR ('OrderQuantityIncrement must be greater than or equal to 1!', 16, 1);
	if @AvailableForBackOrder=1 and @BackOrderLimit<1  RAISERROR ('ackOrderLimit must be greater than or equal to 1!', 16, 1);

	BEGIN TRY
        BEGIN TRAN

		if @OperatorId is null
		begin
			set @OperatorId = (select top 1 CreatedBy from PRProductType where CreatedBy is not null);
		end

		declare @ProductId uniqueidentifier;
		declare @ProductSKUId uniqueidentifier;
		declare @TaxCategoryId uniqueidentifier;
		declare @InventoryId uniqueidentifier;
		declare @IsNewSku bit
		Set @IsNewSku = 1

		If(@ProductSkuIdGuid IS NOT NULL)
			SET @ProductSKUId = @ProductSkuIdGuid 

		select @ProductId = Id FROM PRProduct where ProductIDUser like @ProductIDUser;
		
		IF (@ProductSKUId IS NULL)
			select @ProductSKUId = Id FROM PRProductSKU where ProductId = @ProductId and SKU = @SKU;

		if(@TaxExempt=1)
		begin
			select @TaxCategoryId = Id from PRTaxCategory where Title like '%non%';
		end else
		begin
			select @TaxCategoryId = Id from PRTaxCategory where Title not like '%non%';
		end

		if(@TaxCategoryId is null)RAISERROR ('Unknown tax category!', 16, 1);

		IF EXISTS(Select 1 From PRProductSKU where Id = @ProductSKUId)
			SET @IsNewSku = 0 -- This is required, as in case of variant site we will be passing the ProductSkuId, and we want to use that while creating the new SKU

		--INSERTING THE PRODUCT SKU
		if @IsNewSku = 1
		begin

			If(@ProductSKUId IS NULL) -- If the ProductSKUIDGuid was not passed, then it will be Master site and the new SKU has to be created 
				set @ProductSKUId = newid();
			insert into PRProductSKU
			   ([Id]
           ,[ProductId]
           ,[SiteId]
           ,[Title]
           ,[SKU]
           ,[Description]
           ,[Sequence]
           ,[IsActive]
           ,[IsOnline]
           ,[ListPrice]
           ,[UnitCost]
           ,[WholesalePrice]
           ,[PreviousSoldCount]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[Measure]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[HandlingCharges]
           ,[Length]
           ,[Height]
           ,[Width]
           ,[Weight]
           ,[SelfShippable]
           ,[AvailableForBackOrder]
           ,[BackOrderLimit]
           ,[IsUnlimitedQuantity]
           ,[MaximumDiscountPercentage]
           ,[FreeShipping]
           ,[UserDefinedPrice])
			select
			@ProductSKUId,
			@ProductId,
			'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',
			@SKUTitle,
			@SKU,
			'', --Description
			(select isnull(max(Sequence)+1,0) from PRProductSKU where ProductId = @ProductId), --Sequence
			@IsActive,
			@IsOnline,
			@ListPrice,
			@UnitCost,
			0, --WholesalePrice
			0,
			GETUTCDATE(),
			@OperatorId,
			null,
			null,
			1,
			@Measure,
			@OrderMinimum,
			@OrderIncrement,
			@Handling,
			isnull(@Length, 0),
			isnull(@Height,0),
			isnull(@Width,0),
			isnull(@Weight,0),
			@Selfshippable,
			@AvailableForBackOrder,
			@BackOrderLimit,
			@IsUnlimitedQuantity,
			null,
			@FreeShipping,
			@UserDefinedPrice;

			if (exists(select * from PRProductSKU where Id = @ProductSKUId)) -- don't insert inventory record unless sku record exists
			Begin
			-- Check if the product is digital then insert to digital warehouse or else insert to all warehouses.

			
				IF Exists(Select 1 from PRProductType T INNER JOIN PRProduct P on T.Id = P.ProductTypeID Where P.Id =@ProductId And T.IsDownloadableMedia=1)
				BEGIN
				DECLARE @warehouseId uniqueidentifier
				Select @warehouseId = Id from INWarehouse Where [TypeId]=2
				INSERT INTO INInventory(
				   [Id]
				  ,[WarehouseId]
				  ,[SKUId]
				  ,[Quantity]
				  ,[LowQuantityAlert]
				  ,[FirstTimeOrderMinimum]
				  ,[OrderMinimum]
				  ,[OrderIncrement]
				  ,[ReasonId]
				  ,[ReasonDetail]
				  ,[Status]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate])
				SELECT
				newid(),
				@warehouseId,
				@ProductSKUId,
				0, 
				0, 
				1, 
				1, 
				1,
				'F2BB4870-3D47-497D-9810-D2B322C3C006',
				'Initial Data When SKU is created.',
				1,
				@OperatorId,
				GETUTCDATE(),
				NULL, 
				NULL
				END
				ELSE
				BEGIN
				INSERT INTO INInventory(
				   [Id]
				  ,[WarehouseId]
				  ,[SKUId]
				  ,[Quantity]
				  ,[LowQuantityAlert]
				  ,[FirstTimeOrderMinimum]
				  ,[OrderMinimum]
				  ,[OrderIncrement]
				  ,[ReasonId]
				  ,[ReasonDetail]
				  ,[Status]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate])
				 SELECT
					newid(),
					Id,
					@ProductSKUId,
					0, 
					0, 
					1, 
					1, 
					1,
					'F2BB4870-3D47-497D-9810-D2B322C3C006',
					'Initial Data When SKU is created.',
					1,
					@OperatorId,
					GETUTCDATE(),
					NULL, 
					NULL
					FROM INWarehouse
					Where TypeId!=2
				  
				END

			End

			set @actionType = 1

		end else
		begin
			update PRProductSKU set
			Title = isnull(@SKUTitle, Title),
			IsActive = isnull(@IsActive, IsActive),
			IsOnline = isnull(@IsOnline, IsOnline),
			ListPrice = isnull(@ListPrice, ListPrice),
			UnitCost = isnull(@UnitCost, UnitCost),
			ModifiedDate = GETUTCDATE(),
			ModifiedBy = isnull(@OperatorId, ModifiedBy),
			Measure = isnull(@Measure, Measure),
			OrderMinimum = isnull(@OrderMinimum, OrderMinimum),
			OrderIncrement = isnull(@OrderIncrement, OrderIncrement),
			HandlingCharges = isnull(@Handling, HandlingCharges),
			Length = isnull(@Length, Length),
			Height = isnull(@Height, Height),
			Width = isnull(@Width, Width),
			Weight = isnull(@Weight, Weight),
			SelfShippable = isnull(@Selfshippable, SelfShippable),
			AvailableForBackOrder = isnull(@AvailableForBackOrder, AvailableForBackOrder),
			BackOrderLimit = isnull(@BackOrderLimit, BackOrderLimit),
			IsUnlimitedQuantity = isnull(@IsUnlimitedQuantity, IsUnlimitedQuantity),
			FreeShipping=@FreeShipping,
			UserDefinedPrice =@UserDefinedPrice
			where Id = @ProductSKUId;


			select @InventoryId = Id FROM INInventory where SKUId = @ProductSKUId;
			if @InventoryId is null
			begin
				INSERT INTO INInventory
				SELECT
				newid(),
				Id,
				@ProductSKUId,
				0, 0, 1, 1, 1,
				'F2BB4870-3D47-497D-9810-D2B322C3C006',
				'Initial Data When SKU is created.',
				1,
				@OperatorId,
				GETUTCDATE(),
				NULL, NULL
				FROM INWarehouse
				Where TypeId!=2
			end

			set @actionType = 2

		end

		COMMIT TRAN

		select @actionType, @ProductId, @ProductSKUId

    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX)
        SET @ErrorMessage = ERROR_MESSAGE()
        RAISERROR(@ErrorMessage,16, 1)
    END CATCH

END
GO

IF(OBJECT_ID('Product_GetMinPrice') IS NOT NULL)
	DROP PROCEDURE Product_GetMinPrice
GO

CREATE  PROCEDURE [dbo].[Product_GetMinPrice](
			 @CustomerId uniqueidentifier = null,
			 @IdQuantity  TYIdNumber READONLY,
			 @ApplicationId uniqueidentifier=null
			)
AS

BEGIN
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId')

			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice,ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY P.Id ORDER BY case when PST.EffectivePrice IS NULL THEN 1 ELSE 0 END,  PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @IdQuantity IQ
				INNER JOIN  PRProduct P ON IQ.Id =P.Id
				INNER JOIN PRProductSKU S on P.Id = S.ProductId
				LEFT JOIN PRProductSKUVariantCache SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					Where CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= S.Id AND IQ.Number between minQuantity and maxQuantity
					Where P.Id=IQ.Id
					AND S.IsActive = 1				
			) EFPS
			WHERE EFPS.Sno=1


		End
		else
		Begin
			Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice,ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @IdQuantity IQ
				INNER JOIN  PRProduct P ON IQ.Id =P.Id
				INNER JOIN PRProductSKU S on P.Id = S.ProductId
				LEFT JOIN PRProductSKUVariantCache SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					Where USMG.MemberId = @CustomerId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= S.Id AND IQ.Number between minQuantity and maxQuantity
					Where P.Id =IQ.Id	
					AND S.IsActive = 1					
		End
END
GO

GO

IF(OBJECT_ID('Product_GetEffectivePrice') IS NOT NULL)
	DROP PROCEDURE Product_GetEffectivePrice
GO

CREATE PROCEDURE [dbo].[Product_GetEffectivePrice](
			 @CustomerId uniqueidentifier = null,
			 @Quantity float = 1,
			 @ProductId uniqueidentifier = null,
			 @ProductIdList Xml = null,
			 @ProductIdQuantityList XML =null,
			 @ApplicationId uniqueidentifier=null
										)

AS

BEGIN
--<dictionary><SerializableDictionary><item><key><guid>75610a9c-cc9d-4da5-893a-05fd0056d7cd</guid></key><value><double>2</double></value></item><item><key><guid>c15dd7bf-ce90-4a7b-be78-20b24324957e</guid></key><value><double>1</double></value></item><item><key><guid>1cc48359-eec5-44b4-9518-0c06398f5b92</guid></key><value><double>1</double></value></item><item><key><guid>0ae206dc-1b24-4f75-a174-56cdb59d5346</guid></key><value><double>2</double></value></item></SerializableDictionary></dictionary>
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)
IF @Quantity Is NULL
SET @Quantity=1
IF @ProductIdQuantityList is null
BEGIN

	if @ProductIdList is not null
		begin
				Insert Into @tempProductTable(ProductId)
					Select tab.col.value('text()[1]','uniqueidentifier')
					FROM @ProductIdList.nodes('/ProductCollection/Product')tab(col)
		end
	else if(@ProductId is not null)
		begin
			
			Insert Into @tempProductTable(ProductId)
			Select @ProductId

		end

	if @@ROWCOUNT = 0 
		BEGIN
			RAISERROR ( 'No Product Id supplied' , 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			
			 Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId' and 
				(@ApplicationId is null or STS.SiteId = @ApplicationId))
				--STS.SiteId=IsNull(@ApplicationId,STS.SiteId))

			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId, IsSale ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join @tempProductTable inProduct on PSC.ProductId = inProduct.ProductId
				Where 
					CGPS.CustomerGroupId = @DefaultGroupId and 
					ps.SiteId = @ApplicationId and
					MinQuantity <= @Quantity and MaxQuantity >= @Quantity
					AND @Today >= PS.StartDate AND @today <= PS.EndDate 
					
			) EFPS
			WHERE EFPS.Sno=1	
			OPTION (RECOMPILE)
		End
	else
		Begin
			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId, IsSale ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG WITH (INDEX(IX_USMemberGroup_MemberId)) on CGPS.CustomerGroupId = USMG.GroupId
					inner join @tempProductTable inProduct on PSC.ProductId = inProduct.ProductId
				Where USMG.MemberId = @CustomerId  and
					ps.SiteId = @ApplicationId 
					and MinQuantity <= @Quantity and MaxQuantity >= @Quantity
					AND @Today Between PS.StartDate AND PS.EndDate 		
			) EFPS
			WHERE EFPS.Sno=1				
			OPTION (RECOMPILE)
		End
		-- in any scenario - return List Price for all the SKUs		
		Select prSku.ProductId, Id as SkuId, ListPrice 
			From PRProductSKU prSku 
			where prSku.ProductId in(Select ProductId from @tempProductTable)
	
	END
	ELSE
	BEGIN
--<dictionary><SerializableDictionary><item><key><guid>75610a9c-cc9d-4da5-893a-05fd0056d7cd</guid></key><value><double>2</double></value></item><item><key><guid>c15dd7bf-ce90-4a7b-be78-20b24324957e</guid></key><value><double>1</double></value></item><item><key><guid>1cc48359-eec5-44b4-9518-0c06398f5b92</guid></key><value><double>1</double></value></item><item><key><guid>0ae206dc-1b24-4f75-a174-56cdb59d5346</guid></key><value><double>2</double></value></item></SerializableDictionary></dictionary>
	Declare @tableproductIdQnty	table(Sno int identity(1,1),ProductId uniqueidentifier,Quantity decimal(18,2))
	--Declare @ProductIdQuantityList XML 
	--Set @ProductIdQuantityList ='<dictionary><SerializableDictionary><item><key><guid>75610a9c-cc9d-4da5-893a-05fd0056d7cd</guid></key><value><double>2</double></value></item><item><key><guid>c15dd7bf-ce90-4a7b-be78-20b24324957e</guid></key><value><double>1</double></value></item><item><key><guid>1cc48359-eec5-44b4-9518-0c06398f5b92</guid></key><value><double>1</double></value></item><item><key><guid>0ae206dc-1b24-4f75-a174-56cdb59d5346</guid></key><value><double>2</double></value></item></SerializableDictionary></dictionary>'
	Insert into @tableproductIdQnty(ProductId,Quantity)
	Select tab.col.value('(key/guid/text())[1]','uniqueidentifier'),tab.col.value('(value/double/text())[1]','decimal(18,2)')
	From @ProductIdQuantityList.nodes('dictionary/SerializableDictionary/item') tab(col)
	
	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId' and 
					(@ApplicationId is null or STS.SiteId = @ApplicationId))
					--STS.SiteId=IsNull(@ApplicationId,STS.SiteId))

			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join @tableproductIdQnty inProduct on PSC.ProductId = inProduct.ProductId AND inProduct.Quantity between minQuantity and maxQuantity
				Where 
					CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					--and (MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))		
					AND @Today Between PS.StartDate AND PS.EndDate 		
			) EFPS
			WHERE EFPS.Sno=1	
			OPTION (RECOMPILE)
		End
		else
		Begin
			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG WITH (INDEX(IX_USMemberGroup_MemberId)) on CGPS.CustomerGroupId = USMG.GroupId
					inner join @tableproductIdQnty inProduct on PSC.ProductId = inProduct.ProductId AND inProduct.Quantity between minQuantity and maxQuantity
				Where USMG.MemberId = @CustomerId 
				and PS.SiteId = @ApplicationId 
					--and (MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))		
					AND @Today Between PS.StartDate AND PS.EndDate 		
			) EFPS
			WHERE EFPS.Sno=1		
			OPTION (RECOMPILE)		
		End
-- in any scenario - return List Price for all the SKUs		
		Select prSku.ProductId, Id as SkuId, ListPrice 
			From PRProductSKU prSku 
			where prSku.ProductId in(Select ProductId from @tableproductIdQnty)
	
	END

END

GO
IF NOT EXISTS (
    SELECT 1
    FROM sys.indexes 
    WHERE object_id = OBJECT_ID('PSPriceSetManualSKU')
    AND name='IX_PSPriceSetManualSku_PriceSetId_SkuId')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_PSPriceSetManualSku_PriceSetId_SkuId]
	ON [dbo].[PSPriceSetManualSKU] ([PriceSetId],[SKUId])
	INCLUDE ([MinQuantity],[MaxQuantity],[Price])
END

GO

IF NOT EXISTS (
    SELECT 1
    FROM sys.indexes 
    WHERE object_id = OBJECT_ID('PSPriceSetManualSKU')
    AND name='IX_PSPriceSetManualSku_MinQuantity_MaxQuantity')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_PSPriceSetManualSku_MinQuantity_MaxQuantity]
	ON [dbo].[PSPriceSetManualSKU] ([MinQuantity],[MaxQuantity])
	INCLUDE ([PriceSetId],[SKUId],[Price])
END

GO

PRINT 'Creating View vwAvailableToSellPerWarehouse'
GO
IF (OBJECT_ID('vwAvailableToSellPerWarehouse') IS NOT NULL)
	DROP View vwAvailableToSellPerWarehouse	
GO
CREATE VIEW [dbo].[vwAvailableToSellPerWarehouse]
AS

SELECT inv.SKUId AS SKUId, inv.WarehouseId,
	inv.Quantity Inventory,
	Quantity = CASE AvailableForBackOrder WHEN 0 THEN  (inv.Quantity - 	ISNULL(allocated.Quantity, 0)) ELSE prs.BackOrderLimit + (inv.Quantity - 	ISNULL(allocated.Quantity, 0))END ,
	BackOrderLimit = 
	CASE AvailableForBackOrder
		WHEN 1 THEN prs.BackOrderLimit 
		ELSE 0
		END,
		ws.SiteId
FROM dbo.PRProductSKU prs
LEFT JOIN dbo.INInventory inv ON prs.Id=inv.SKUId
LEFT JOIN dbo.vwAllocatedQuantityperWarehouse allocated 
ON inv.SKUId = allocated.ProductSKUId AND inv.WarehouseId = allocated.WarehouseId
INNER JOIN dbo.INWarehouse w ON inv.WarehouseId = w.Id
INNER JOIN dbo.INWarehouseSite ws ON w.Id = ws.WarehouseId
WHERE prs.IsUnlimitedQuantity =0 AND w.Status=1 AND WS.Status=1

UNION

SELECT inv.SKUId AS SKUId, inv.WarehouseId, 
999999999
	 AS Inventory,
999999999
	 AS Quantity,
	BackOrderLimit = 
	CASE AvailableForBackOrder
		WHEN 1 THEN prs.BackOrderLimit 
		ELSE 0
		END,
ws.WarehouseId
FROM dbo.PRProductSKU prs
LEFT JOIN dbo.INInventory inv ON prs.Id=inv.SKUId
LEFT JOIN dbo.INWarehouseSite ws ON inv.WarehouseId = ws.WarehouseId
WHERE prs.IsUnlimitedQuantity =1
GO

PRINT 'Creating BLD_ExportAttributes'
GO
IF(OBJECT_ID('BLD_ExportAttributes') IS NOT NULL)
	DROP PROCEDURE BLD_ExportAttributes
GO

CREATE PROCEDURE [dbo].[BLD_ExportAttributes]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

		IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
			BEGIN
				select 
					 a.id as AttributeId
					,adt.Title as AttributeDataType
					,ac.Name as ATtributeCategoryName
					,a.Title
					,coalesce(a.Description, '') as Description
					,coalesce(a.IsFaceted, 0) as IsFaceted
					,coalesce(a.IsSearched, 0) as IsSearched
					,coalesce(a.IsDisplayed, 0) as IsDisplayed
					,coalesce(a.IsEnum, 0) as IsEnum
					,coalesce(a.IsPersonalized, 0) as IsPersonalized
					,coalesce(a.IsMultiValued, 0) as IsMultiValued
					,0 as UpdateFlag
				from dbo.ATAttributeCategoryItem aci
					join dbo.ATAttributeCategory ac on (ac.id = aci.CategoryId)
					join dbo.ATAttribute a on (aci.AttributeId = a.Id)
					join dbo.ATAttributeDataType adt on (a.AttributeDataTypeId = adt.Id)
				where 
					a.IsSystem ! = 1 and
					(ac.IsGlobal != 1 or ac.IsGlobal is null)
			END
		ELSE
			BEGIN
				select 
					 a.id as AttributeId
					,adt.Title as AttributeDataType
					,ac.Name as ATtributeCategoryName
					,avc.Title
					,coalesce(avc.Description, a.Description) as Description
					,coalesce(a.IsFaceted, 0) as IsFaceted
					,coalesce(a.IsSearched, 0) as IsSearched
					,coalesce(a.IsDisplayed, 0) as IsDisplayed
					,coalesce(a.IsEnum, 0) as IsEnum
					,coalesce(a.IsPersonalized, 0) as IsPersonalized
					,coalesce(a.IsMultiValued, 0) as IsMultiValued
					,0 as UpdateFlag
				from dbo.ATAttributeVariantCache avc
				left join dbo.ATAttribute a on (avc.Id = a.Id)
				join dbo.ATAttributeCategoryItem aci on (a.id = aci.AttributeId)
				join dbo.ATAttributeCategory ac on (ac.id = aci.CategoryId)
				join dbo.ATAttributeDataType adt on (a.AttributeDataTypeId = adt.Id)
				where 
       				 a.IsSystem ! = 1 and
					(ac.IsGlobal != 1 or ac.IsGlobal is null)
					and avc.siteid = @siteId
			END
END
GO



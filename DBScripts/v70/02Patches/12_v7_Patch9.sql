PRINT 'Add InWFTemplateId column in PageDefinition'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PageDefinition' AND COLUMN_NAME = 'InWFTemplateId')
BEGIN	
	ALTER TABLE PageDefinition ADD InWFTemplateId uniqueidentifier NULL
END
GO
IF (OBJECT_ID('PageDefinition_Get') IS NOT NULL)
	DROP PROCEDURE PageDefinition_Get
GO
CREATE PROCEDURE [dbo].[PageDefinition_Get]
(
	@siteId		uniqueidentifier,
	@startDate	datetime = null
)
AS
BEGIN
	DECLARE @PageDefinitionIds TABLE(Id uniqueidentifier)
	INSERT INTO @PageDefinitionIds
	SELECT PageDefinitionId FROM PageDefinition
		WHERE SiteId = @siteId AND PageStatus != 3 AND (@startDate IS NULL OR ModifiedDate > @startDate)

	SELECT M.PageDefinitionId, M.PageMapNodeId, D.SiteId, M.DisplayOrder, --PageDefinitionXml,
		TemplateId, InWFTemplateId, D.Title, Description, PageStatus, WorkflowState, PublishCount, PublishDate, FriendlyName,
		WorkflowId, D.CreatedBy, D.CreatedDate, D.ModifiedBy, D.ModifiedDate, ArchivedDate, StatusChangedDate, 
		AuthorId, EnableOutputCache, OutputCacheProfileName, ExcludeFromSearch,ExcludeFromExternalSearch, IsDefault, IsTracked,
		HasForms, TextContentCounter, CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,
		ScheduledPublishDate, ScheduledArchiveDate, NextVersionToPublish, E.CampaignId, EA.Id AS EmailId, D.PageSegmentCount, D.ExcludeFromTranslation
	FROM PageDefinition D 
		JOIN PageMapNodePageDef M ON D.PageDefinitionId = M.PageDefinitionId
		JOIN @PageDefinitionIds T ON D.PageDefinitionId = T.Id
		Left JOIN MKCampaignEmail E ON D.PageDefinitionId = E.CMSPageId
		Left JOIN MAEmail EA ON D.PageDefinitionId = EA.PageId
	ORDER BY DisplayOrder ASC

	SELECT S.ObjectId, 
		S.ObjectTypeId, S.SecurityLevelId
	FROM USSecurityLevelObject S
		JOIN @PageDefinitionIds T ON S.ObjectId = T.Id
	WHERE ObjectTypeId = 8
		
	SELECT PageDefinitionId, 
		StyleId, CustomAttributes 
	FROM SIPageStyle S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id
		
	SELECT PageDefinitionId, 
		ScriptId, CustomAttributes 
	FROM SIPageScript S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id

	SELECT P.PageDefinitionId, 
		P.PageMapNodeId, P.DisplayOrder 
	FROM PageMapNodePageDef P
		JOIN @PageDefinitionIds T ON P.PageDefinitionId = T.Id
END
GO
IF (OBJECT_ID('PageDefinition_GetByWorkflow') IS NOT NULL)
	DROP PROCEDURE PageDefinition_GetByWorkflow
GO
CREATE PROCEDURE [dbo].[PageDefinition_GetByWorkflow]
(
	@WorkflowId		uniqueidentifier
)
AS
BEGIN		
	;WITH Pages AS
	(
		SELECT ObjectId, 
			ROW_NUMBER() OVER(PARTITION BY ObjectId ORDER BY ActionedDate desc) AS Ord 
		FROM WFWorkflowExecution
		WHERE WorkflowId = @WorkflowId
	)

	SELECT 			
		M.PageDefinitionId, 
		M.PageMapNodeId, 
		D.SiteId, 
		M.DisplayOrder,
		TemplateId,
		InWFTemplateId,
		D.Title,
		Description,
		PageStatus,
		WorkflowState,
		PublishCount,
		PublishDate,
		FriendlyName,
		WorkflowId,
		D.CreatedBy,
		D.CreatedDate,
		D.ModifiedBy,
		D.ModifiedDate,
		ArchivedDate,
		StatusChangedDate,
		AuthorId,
		EnableOutputCache,
		OutputCacheProfileName,
		ExcludeFromSearch,
		ExcludeFromExternalSearch,
		IsDefault,
		IsTracked,
		HasForms,
		TextContentCounter,
		CustomAttributes,
		SourcePageDefinitionId,
		IsInGroupPublish,
		ScheduledPublishDate,
		ScheduledArchiveDate,
		NextVersionToPublish,
		D.PageSegmentCount,
		D.ExcludeFromTranslation,
		E.CampaignId, 
		EA.Id AS EmailId
	FROM PageDefinition D
		INNER JOIN PageMapNodePageDef M ON D.PageDefinitionId =M.PageDefinitionId
		INNER JOIN Pages O ON O.ObjectId = D.PageDefinitionId
		LEFT JOIN MKCampaignEmail E ON D.PageDefinitionId = E.CMSPageId
		LEFT JOIN MAEmail EA ON D.PageDefinitionId = EA.PageId
	WHERE O.Ord = 1
END
GO
IF (OBJECT_ID('PageDefinition_GetOne') IS NOT NULL)
	DROP PROCEDURE PageDefinition_GetOne
GO
CREATE PROCEDURE [dbo].[PageDefinition_GetOne]
(
	@PageId uniqueidentifier, 
	@PageMapNodeId uniqueidentifier = null
)
AS
BEGIN
	SELECT 
		M.PageDefinitionId, 
		M.PageMapNodeId, 
		SiteId, 
		M.DisplayOrder, 
		TemplateId,
		InWFTemplateId,
		Title,
		Description,
		PageStatus,
		WorkflowState,
		PublishCount,
		PublishDate,
		FriendlyName,
		WorkflowId,
		CreatedBy,
		CreatedDate,
		ModifiedBy,
		ModifiedDate,
		ArchivedDate,
		StatusChangedDate,
		AuthorId,
		EnableOutputCache,
		OutputCacheProfileName,
		ExcludeFROMSearch,
		ExcludeFROMExternalSearch,
		IsDefault,
		IsTracked,
		HasForms,
		TextContentCounter,
		CustomAttributes,
		SourcePageDefinitionId,
		IsInGroupPublish,
		ScheduledPublishDate,
		ScheduledArchiveDate,
		NextVersionToPublish,
		D.PageSegmentCount,
		D.ExcludeFROMTranslation
	FROM PageDefinition D
		INNER JOIN PageMapNodePageDef M ON D.PageDefinitionId = M.PageDefinitionId
	WHERE D.PageDefinitionId = @PageId
		AND (@PageMapNodeId IS NULL OR M.PageMapNodeId = @PageMapNodeId )
		
	SELECT ObjectId, ObjectTypeId, SecurityLevelId FROM USSecurityLevelObject Where ObjectId = @PageId
		
	SELECT PageDefinitionId, StyleId, CustomAttributes FROM SIPageStyle Where PageDefinitionId = @PageId
		
	SELECT PageDefinitionId, ScriptId, CustomAttributes FROM SIPageScript Where PageDefinitionId = @PageId

END
GO
IF (OBJECT_ID('PageMap_SavePageDefinition') IS NOT NULL)
	DROP PROCEDURE PageMap_SavePageDefinition
GO
CREATE PROCEDURE [dbo].[PageMap_SavePageDefinition]
(
	@Id					uniqueidentifier output,
	@SiteId				uniqueidentifier,
	@ParentNodeId		uniqueidentifier,
	@CreatedBy			uniqueidentifier,
	@ModifiedBy			uniqueidentifier,
	@PublishPage		bit,
	@PageDefinitionXml	xml,
	@ContainerXml		xml = null,
	@UnmanagedContentXml xml = null,
	@ZonesXml			xml = null,
	@UpdatePageName bit = null
)
AS
BEGIN
	
	IF(@PublishPage = 1)
		SET @UpdatePageName = 1
		
	IF(@UpdatePageName IS NULL)
		SET @UpdatePageName = 0
	
		
	IF NOT EXISTS(Select 1 from PageMapNode Where PageMapNodeId = @ParentNodeId and SiteId = @SiteId)
	BEGIN
		RAISERROR('NOTEXISTS||ParentId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	DECLARE @pageDefinitionSegmentIds table(Id uniqueidentifier)	
	DECLARE @PublishCount int
	DECLARE @PublishDate datetime
	DECLARE @PageDefinitionSegmentId uniqueidentifier 
	
	IF @PublishPage = 1
		SET @PublishDate = GETUTCDATE()
	ELSE
		SET @PublishDate = @PageDefinitionXml.value('(pageDefinition/@publishDate)[1]','datetime')
		
	DECLARE @ActionType int
	SET @ActionType = 2
	IF @Id IS NULL OR (Select count(*) FROM PageDefinition Where PageDefinitionId=@Id)=0 -- New Page
	BEGIN
		SET @ActionType = 1
		-- get the highest value of the DisplayOrder currently in the DB for this menuId
		DECLARE @CurrentDisplayOrder int
		SELECT @CurrentDisplayOrder = ISNULL(MAX(DisplayOrder),0) FROM PageMapNodePageDef M WHERE M.PageMapNodeId = @ParentNodeId
		IF @CurrentDisplayOrder IS NULL
			SET @CurrentDisplayOrder = 1
		
		IF @PublishPage = 1
			SET @PublishCount = 1
		ELSE
			SET @PublishCount = 0
		IF @Id IS NULL 	
			SET @Id = NEWID()

		IF NOT EXISTS (Select 1 from PageDefinition Where PageDefinitionId = @Id)
		BEGIN
			INSERT INTO PageDefinition(PageDefinitionId, SiteId, TemplateId, InWFTemplateId, Title, Description, 
						PublishDate, PageStatus, WorkflowState, PublishCount, --ContainerXml,UnmanagedContentXml,
						FriendlyName, WorkflowId,
						CreatedBy, CreatedDate, ArchivedDate, StatusChangedDate, AuthorId, EnableOutputCache,
						OutputCacheProfileName, ExcludeFromSearch,ExcludeFromExternalSearch, ExcludeFromTranslation,
						IsDefault, IsTracked, HasForms, TextContentCounter, 
						CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,ScheduledPublishDate,ScheduledArchiveDate,NextVersionToPublish,PageSegmentCount)
			VALUES(@Id,  @SiteId, 
					@PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@inWFTemplateId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)'), 
					@PublishDate,
					@PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
					@PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'),
					@PublishCount,
					--@ContainerXml,
					--isnull(@UnmanagedContentXml,'<unmanagedPageContents/>'),
					@PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
					@CreatedBy,
					GETUTCDATE(),
					@PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(1000)'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromExternalSearch)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromTranslation)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
					dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
					@PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
					isnull(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
					@PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier'),
					1
				)
		END

		IF NOT EXISTS (Select 1 from PageMapNodePageDef Where PageDefinitionId =@Id AND PageMapNodeId=@ParentNodeId)
			INSERT INTO PageMapNodePageDef(PageDefinitionId,PageMapNodeId,DisplayOrder)
			VALUES (@Id,@ParentNodeId,@CurrentDisplayOrder + 1)
			
			
		INSERT INTO PageDefinitionSegment(
			Id
			,PageDefinitionId
			,DeviceId
			,AudienceSegmentId
			,UnmanagedXml
			,ZonesXml
			)
			 output inserted.Id into @pageDefinitionSegmentIds
		VALUES (
			NEWID()
			,@Id
			,dbo.GetDesktopDeviceId(@SiteId)
			,dbo.GetDesktopAudienceSegmentId(@SiteId)
			,isnull(@UnmanagedContentXml,'<unmanagedPageContents/>')
			,@ZonesXml
			)
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			INSERT INTO [PageDefinitionContainer]
				   (
					[PageDefinitionSegmentId]
				   ,[PageDefinitionId]
				   ,[ContainerId]
				   ,[ContentId]
				   ,[InWFContentId]
				   ,[ContentTypeId]
				   ,[IsModified]
				   ,[IsTracked]
				   ,[Visible]
				   ,[InWFVisible]
				   ,[DisplayOrder]
				   ,[InWFDisplayOrder]
				   ,[CustomAttributes]
				   ,ContainerName)
			 SELECT
					S.Id -- as this would be called only for desktop version, we can come in a better way
					,@Id
					,X.value('(@id)[1]','uniqueidentifier') as ContainerId
					,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
					,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
					,X.value('(@contentTypeId)[1]','int') as contentTypeId
					,X.value('(@isModified)[1]','bit') as isModified
					,X.value('(@isTracked)[1]','bit') as isTracked
					,X.value('(@visible)[1]','bit') as Visible
					,X.value('(@inWFVisible)[1]','bit') as InWFVisible
					,X.value('(@displayOrder)[1]','int') as DisplayOrder
					,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
					,dbo.GetCustomAttributeXml(X.query('.'),'container')
					,X.value('(@name)[1]','nvarchar(100)') as ContainerName
			From @ContainerXml.nodes('/Containers/container') temp(X)
			CROSS JOIN @pageDefinitionSegmentIds S
		END
	END
	ELSE
	BEGIN
		SELECT @PublishCount = PublishCount FROM PageDefinition WHERE PageDefinitionId = @Id
		IF @PublishPage = 1
			SELECT @PublishCount = @PublishCount + 1
			
		UPDATE PageDefinition Set 
			Title = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)') ELSE title END,
			TemplateId = @PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
			InWFTemplateId = @PageDefinitionXml.value('(pageDefinition/@inWFTemplateId)[1]','uniqueidentifier'),
			description = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)') ELSE description END, 
			publishDate = @PublishDate, 
			PageStatus = @PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
			WorkflowState = @PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'), 
			PublishCount = @PublishCount,
			--ContainerXml = @containerXml,
			--UnmanagedContentXml = isnull(@unmanagedContentXml, UnmanagedContentXml),
			FriendlyName = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)') ELSE FriendlyName END,
			WorkflowId = @PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = GETUTCDATE(),
			ArchivedDate = @PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
			StatusChangedDate = @PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
			AuthorId = @PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
			EnableOutputCache = @PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
			OutputCacheProfileName = @PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(100)'),
			ExcludeFromSearch = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
		    ExcludeFromExternalSearch = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromExternalSearch)[1]','bit'),
		    ExcludeFromTranslation = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromTranslation)[1]','bit'),
			IsDefault = @PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
			IsTracked = @PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
			HasForms = @PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
			TextContentCounter = @PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
			SourcePageDefinitionId = @PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
			CustomAttributes = dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
			IsInGroupPublish  =ISNULL(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
			ScheduledPublishDate = @PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
			ScheduledArchiveDate = @PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
			NextVersionToPublish =@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier')
		WHERE PageDefinitionId = @Id AND SiteId = @SiteId
		
		DECLARE @DeskTopDeviceId uniqueidentifier
		DECLARE @DeskTopAudienceId uniqueidentifier
		SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@SiteId)
		SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@SiteId)
		
		SELECT @PageDefinitionSegmentId = Id 
			FROM PageDefinitionSegment WHERE DeviceId = @DeskTopDeviceId AND 
				AudienceSegmentId = @DeskTopAudienceId AND
				PageDefinitionId = @Id
		
		UPDATE PageDefinitionSegment Set 
			UnmanagedXml = isnull(@unmanagedContentXml, UnmanagedXml),
			ZonesXml = isnull(@ZonesXml, ZonesXml)
		WHERE PageDefinitionId = @Id AND Id = @PageDefinitionSegmentId
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			UPDATE [PageDefinitionContainer] SET 
			   [ContentId] = X.value('(@contentId)[1]','uniqueidentifier')
			  ,[InWFContentId] = X.value('(@inWFContentId)[1]','uniqueidentifier')
			  ,[ContentTypeId] = X.value('(@contentTypeId)[1]','int') 
			  ,[IsModified] = X.value('(@isModified)[1]','bit')
			  ,[IsTracked] = X.value('(@isTracked)[1]','bit')
			  ,[Visible] = X.value('(@visible)[1]','bit')
			  ,[InWFVisible] = X.value('(@inWFVisible)[1]','bit')
			  ,[CustomAttributes] = dbo.GetCustomAttributeXml(X.query('.'),'container')
			  ,[DisplayOrder] = X.value('(@displayOrder)[1]','int')
			  ,[InWFDisplayOrder] = X.value('(@inWFDisplayOrder)[1]','int')
			From @ContainerXml.nodes('/Containers/container') temp(X)
			WHERE [PageDefinitionId] = @Id 
			AND PageDefinitionSegmentId = @PageDefinitionSegmentId
			AND [ContainerId] = X.value('(@id)[1]','uniqueidentifier')
			--If any containers added to pagedefinition insert those(ex. marketier template switching)
			INSERT INTO [PageDefinitionContainer]
						   (
							[PageDefinitionSegmentId]
						   ,[PageDefinitionId]
						   ,[ContainerId]
						   ,[ContentId]
						   ,[InWFContentId]
						   ,[ContentTypeId]
						   ,[IsModified]
						   ,[IsTracked]
						   ,[Visible]
						   ,[InWFVisible]
						   ,[DisplayOrder]
						   ,[InWFDisplayOrder]
						   ,[CustomAttributes]
						   ,[ContainerName])
					 SELECT
							@PageDefinitionSegmentId
							,@Id
							,X.value('(@id)[1]','uniqueidentifier') as ContainerId
							,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
							,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
							,X.value('(@contentTypeId)[1]','int') as contentTypeId
							,X.value('(@isModified)[1]','bit') as isModified
							,X.value('(@isTracked)[1]','bit') as isTracked
							,X.value('(@visible)[1]','bit') as Visible
							,X.value('(@inWFVisible)[1]','bit') as InWFVisible
							,X.value('(@displayOrder)[1]','int') as DisplayOrder
							,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
							,dbo.GetCustomAttributeXml(X.query('.'),'container')
							,X.value('(@name)[1]','nvarchar(100)') as ContainerName
					From @ContainerXml.nodes('/Containers/container') temp(X)					
					where X.value('(@id)[1]','uniqueidentifier') not in (select ContainerId from PageDefinitionContainer where PageDefinitionSegmentId=@PageDefinitionSegmentId and PageDefinitionId=@Id)
		
		END
		
	END
	
	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO USSecurityLevelObject 
		SELECT @Id, 8, SL.Id
	FROM dbo.SplitComma(@PageDefinitionXml.value('(pageDefinition/@securityLevels)[1]','nvarchar(max)'), ',') S 
		JOIN USSecurityLevel SL ON S.Value = SL.Id
	
	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO SIPageStyle 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'style')
	FROM @PageDefinitionXml.nodes('/pageDefinition/style') T(C) JOIN SIStyle S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO SIPageScript 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'script')
	FROM @PageDefinitionXml.nodes('/pageDefinition/script') T(C) JOIN SIScript S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	UPDATE D SET D.PageSegmentCount = T.Cnt
	FROM PageDefinition D
	INNER JOIN (SELECT PageDefinitionId, Count(PageDefinitionId) cnt FROM PageDefinitionSegment
	Where PageDefinitionId=@Id
	Group by PageDefinitionId
	) T
	ON T.PageDefinitionId =D.PageDefinitionId 


	EXEC [PageDefinition_AdjustDisplayOrder] @ParentNodeId

	EXEC PageMapBase_UpdateLastModification @SiteId
	EXEC PageMap_UpdateHistory @SiteId, @Id, 8, @ActionType
END
GO
IF (OBJECT_ID('GetCustomAttributeXml') IS NOT NULL)
	DROP FUNCTION GetCustomAttributeXml
GO
CREATE FUNCTION [dbo].[GetCustomAttributeXml] 
(
	@Xml xml,
	@TagName nvarchar(100)
)
RETURNS xml
AS
BEGIN

IF lower(@TagName) = 'style'
BEGIN
	SET @Xml.modify('delete /style/@id[1]')
	SET @Xml = cast(replace(cast(@Xml as nvarchar(max)), '<style', '<customAttributes') as xml)
END
ELSE IF lower(@TagName) = 'script'
BEGIN
	SET @Xml.modify('delete /script/@id[1]')
	SET @Xml = cast(replace(cast(@Xml as nvarchar(max)), '<script', '<customAttributes') as xml)
END
ELSE IF lower(@TagName) = 'pagedefinition'
BEGIN
	SET @Xml.modify('delete /pageDefinition/@id[1]')
	SET @Xml.modify('delete /pageDefinition/@friendlyName[1]')
	SET @Xml.modify('delete /pageDefinition/@templateId[1]')
	SET @Xml.modify('delete /pageDefinition/@inWFTemplateId[1]')
	SET @Xml.modify('delete /pageDefinition/@isDefault[1]')
	SET @Xml.modify('delete /pageDefinition/@isTracked[1]')
	SET @Xml.modify('delete /pageDefinition/@workflowId[1]')
	SET @Xml.modify('delete /pageDefinition/@title[1]')
	SET @Xml.modify('delete /pageDefinition/@description[1]')
	SET @Xml.modify('delete /pageDefinition/@status[1]')
	SET @Xml.modify('delete /pageDefinition/@createdDate[1]')
	SET @Xml.modify('delete /pageDefinition/@createdBy[1]')
	SET @Xml.modify('delete /pageDefinition/@modifiedDate[1]')
	SET @Xml.modify('delete /pageDefinition/@modifiedBy[1]')
	SET @Xml.modify('delete /pageDefinition/@archiveDate[1]')
	SET @Xml.modify('delete /pageDefinition/@publishDate[1]')
	SET @Xml.modify('delete /pageDefinition/@statusChangedDate[1]')
	SET @Xml.modify('delete /pageDefinition/@securityLevels[1]')
	SET @Xml.modify('delete /pageDefinition/@authorId[1]')
	SET @Xml.modify('delete /pageDefinition/@publishCount[1]')
	SET @Xml.modify('delete /pageDefinition/@workflowState[1]')
	SET @Xml.modify('delete /pageDefinition/@TextContentCounter[1]')
	SET @Xml.modify('delete /pageDefinition/@DisplayOrder[1]')
	SET @Xml.modify('delete /pageDefinition/@HasForms[1]')
	SET @Xml.modify('delete /pageDefinition/@EnableOutputCache[1]')
	SET @Xml.modify('delete /pageDefinition/@OutputCacheProfileName[1]')
	SET @Xml.modify('delete /pageDefinition/@ExcludeFromSearch[1]')
	SET @Xml.modify('delete /pageDefinition/@sourcePageDefinitionId[1]')
	SET @Xml.modify('delete /pageDefinition/@isInGroupPublish[1]')
	SET @Xml.modify('delete /pageDefinition/@scheduledPublishDate[1]')
	SET @Xml.modify('delete /pageDefinition/@scheduledArchiveDate[1]')
	SET @Xml.modify('delete /pageDefinition/@nextVersionToPublish[1]')
	
	SET  @Xml.modify('delete /pageDefinition/*')
	
	SET @Xml = cast(replace(replace(cast(@Xml as nvarchar(max)), '<pageDefinition', '<customAttributes'), '</pageDefinition', '</customAttributes') as xml)
END
ELSE IF lower(@TagName) = 'container'
BEGIN
	SET @Xml.modify('delete /container/@id[1]')
	SET @Xml.modify('delete /container/@name[1]')
	SET @Xml.modify('delete /container/@contentId[1]')
	SET @Xml.modify('delete /container/@inWFContentId[1]')
	SET @Xml.modify('delete /container/@contentTypeId[1]')
	SET @Xml.modify('delete /container/@isModified[1]')
	SET @Xml.modify('delete /container/@isTracked[1]')
	SET @Xml.modify('delete /container/@visible[1]')
	SET @Xml.modify('delete /container/@inWFVisible[1]')
	SET @Xml.modify('delete /container/@displayOrder[1]')
	SET @Xml.modify('delete /container/@inWFDisplayOrder[1]')

	SET @Xml = cast(replace(cast(@Xml as nvarchar(max)), '<container', '<customAttributes') as xml)
END
ELSE IF lower(@TagName) = 'pagemapnode'
BEGIN
	SET @Xml.modify('delete /pageMapNode/pageMapNodeWorkFlow')
	SET @Xml.modify('delete /pageMapNode/@id[1]')
	SET @Xml.modify('delete /pageMapNode/@parentId[1]')
	SET @Xml.modify('delete /pageMapNode/@description[1]')
	SET @Xml.modify('delete /pageMapNode/@displayTitle[1]')
	SET @Xml.modify('delete /pageMapNode/@friendlyUrl[1]')
	SET @Xml.modify('delete /pageMapNode/@menuStatus[1]')
	SET @Xml.modify('delete /pageMapNode/@targetId[1]')
	SET @Xml.modify('delete /pageMapNode/@target[1]')
	SET @Xml.modify('delete /pageMapNode/@targetUrl[1]')
	SET @Xml.modify('delete /pageMapNode/@createdBy[1]')
	SET @Xml.modify('delete /pageMapNode/@createdDate[1]')
	SET @Xml.modify('delete /pageMapNode/@modifiedBy[1]')
	SET @Xml.modify('delete /pageMapNode/@modifiedDate[1]')
	SET @Xml.modify('delete /pageMapNode/@propogateWorkFlow[1]')
	SET @Xml.modify('delete /pageMapNode/@inheritWorkFlow[1]')
	SET @Xml.modify('delete /pageMapNode/@roles[1]')
	SET @Xml.modify('delete /pageMapNode/@propogateSecurityLevels[1]')
	SET @Xml.modify('delete /pageMapNode/@inheritSecurityLevels[1]')
	SET @Xml.modify('delete /pageMapNode/@propogateRoles[1]')
	SET @Xml.modify('delete /pageMapNode/@inheritRoles[1]')
	SET @Xml.modify('delete /pageMapNode/@locationIdentifier[1]')
	SET @Xml.modify('delete /pageMapNode/@hasRolloverImages[1]')
	SET @Xml.modify('delete /pageMapNode/@rolloverOnImage[1]')
	SET @Xml.modify('delete /pageMapNode/@rolloverOffImage[1]')
	SET @Xml.modify('delete /pageMapNode/@securityLevels[1]')
	SET @Xml.modify('delete /pageMapNode/@IsCommerceNav[1]')
	SET @Xml.modify('delete /pageMapNode/@DefaultContentId[1]')
	SET @Xml.modify('delete /pageMapNode/@AssociatedQueryId[1]')
	SET @Xml.modify('delete /pageMapNode/@AssociatedContentFolderId[1]')
	
	SET  @Xml.modify('delete /pageMapNode/*')
	
	SET @Xml = cast(replace(cast(@Xml as nvarchar(max)), '<pageMapNode', '<customAttributes') as xml)
END
RETURN @Xml
END
GO
IF (OBJECT_ID('PageDefinition_Rollback') IS NOT NULL)
	DROP PROCEDURE PageDefinition_Rollback
GO
CREATE PROCEDURE [dbo].[PageDefinition_Rollback]
(	@ApplicationId	uniqueidentifier,
	@VersionId		uniqueidentifier = null,
	@PageId			uniqueidentifier = null,
	@VersionNumber	int = null,
	@RevisionNumber int = null,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	DECLARE @OldVersionId uniqueidentifier
	DECLARE	@Xmlstring xml
	
	IF @VersionId IS NULL
		Select Top 1 @VersionId = Id,@Xmlstring = XMLString,@PageId = ObjectId
			FROM VEVersion Where ObjectId = @PageId AND VersionNumber = @VersionNumber AND RevisionNumber = @RevisionNumber
	ELSE
		Select Top 1 @Xmlstring = XMLString,@PageId = ObjectId
			FROM VEVersion Where ObjectId = @PageId AND Id = @VersionId
	
	IF @VersionId IS NULL
	BEGIN
		RAISERROR('DBERROR||%s',16,1,'Cannot find version')
		RETURN dbo.GetDataBaseErrorCode()	
	END

	SELECT TOP (1) @VersionNumber = VersionNumber, @RevisionNumber = RevisionNumber
		FROM VEVersion A
	WHERE ObjectId = @PageId  and ApplicationId = @ApplicationId
	ORDER BY RevisionNumber DESC, VersionNumber DESC


	SET @OldVersionId =@VersionId
	SET @VersionId = NEWID()
	SET @XMLString.modify('replace value of (/page/pageDefinition/@authorId)[1] with sql:variable("@ModifiedBy")')
	SET @XMLString.modify('replace value of (/page/pageDefinition/@workflowState)[1] with "2"')

  
	DECLARE @tmpPageDefSegmentVersion TABLE(Id uniqueidentifier,oldId uniqueidentifier)
	INSERT INTO @tmpPageDefSegmentVersion 
	SELECT NEWID(),Id
	FROM [dbo].[VEPageDefSegmentVersion]
	Where VersionId=@OldVersionId
  

	IF (SELECT count(*) FROM @tmpPageDefSegmentVersion) = 0
	BEGIN
		INSERT INTO [dbo].[VEVersion]
		(
			[Id]
			,[ApplicationId]
			,[ObjectTypeId]
			,[ObjectId]
			,[CreatedDate]
			,[CreatedBy]
			,[VersionNumber]
			,[RevisionNumber]
			,[Status]
			,[ObjectStatus]
			,[VersionStatus]
			,[XMLString]
			,[Comments]
			,[TriggeredObjectId]
			,[TriggeredObjectTypeId]
		)
		SELECT @VersionId
			,[ApplicationId]
			,[ObjectTypeId]
			,[ObjectId]
			,GETUTCDATE()
			,@ModifiedBy
			,@VersionNumber + 1
			,@RevisionNumber 
			,0
			,2
			,[VersionStatus]
			,dbo.SetContainersModified(@Xmlstring,null)
			,[Comments]
			,[TriggeredObjectId]
			,[TriggeredObjectTypeId]
		FROM [dbo].[VEVersion]
		WHERE Id = @OldVersionId
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[VEVersion]
		(
			[Id]
			,[ApplicationId]
			,[ObjectTypeId]
			,[ObjectId]
			,[CreatedDate]
			,[CreatedBy]
			,[VersionNumber]
			,[RevisionNumber]
			,[Status]
			,[ObjectStatus]
			,[VersionStatus]
			,[XMLString]
			,[Comments]
			,[TriggeredObjectId]
			,[TriggeredObjectTypeId]
		)
		SELECT @VersionId
			,[ApplicationId]
			,[ObjectTypeId]
			,[ObjectId]
			,GETUTCDATE()
			,@ModifiedBy
			,@VersionNumber + 1
			,@RevisionNumber 
			,0
			,2
			,[VersionStatus]
			,@Xmlstring
			,[Comments]
			,[TriggeredObjectId]
			,[TriggeredObjectTypeId]
		FROM [dbo].[VEVersion]
		WHERE Id = @OldVersionId
		
		INSERT INTO [dbo].[VEPageDefSegmentVersion]
			([Id]
			,[VersionId]
			,[PageDefinitionId]
			,[MinorVersionNumber]
			,[DeviceID]
			,[AudienceSegmentId]
			,[ContainerContentXML]
			,[UnmanagedXml]
			,[ZonesXml]
			,[CreatedBy]
			,[CreatedDate])
		SELECT T.Id
			,@VersionId
			,[PageDefinitionId]
			,[MinorVersionNumber]
			,[DeviceID]
			,[AudienceSegmentId]
			,dbo.SetContainersModified(null, ContainerContentXML)
			,[UnmanagedXml]
			,[ZonesXml]
			,@ModifiedBy
			,GETUTCDATE()
		FROM [dbo].[VEPageDefSegmentVersion] S
		INNER JOIN @tmpPageDefSegmentVersion T on S.Id = T.oldId

		INSERT INTO [dbo].[VEPageContentVersion]
			([Id]
			,[ContentVersionId]
			,[IsChanged]
			,[PageDefSegmentVersionId])
		SELECT NEWID()
			,[ContentVersionId]
			,1
			,T.Id
		FROM [dbo].[VEPageContentVersion] C
			INNER JOIN @tmpPageDefSegmentVersion T on C.PageDefSegmentVersionId = T.oldId
	END

	DECLARE @InWFTemplateId uniqueidentifier
	SET @InWFTemplateId = @XMLString.value('(/page/pageDefinition/@inWFTemplateId)[1]','uniqueidentifier')
	IF @InWFTemplateId IS NULL
		SET @InWFTemplateId = @XMLString.value('(/page/pageDefinition/@templateId)[1]','uniqueidentifier')
	
	UPDATE PageDefinition SET 
		WorkflowState = 2, 
		WorkflowId = dbo.GetEmptyGUID(),
		InWFTemplateId = @InWFTemplateId
	WHERE PageDefinitionId = @PageId 

	EXEC PageMapBase_UpdateLastModification @ApplicationId
END
GO
IF (OBJECT_ID('PageDto_Import') IS NOT NULL)
	DROP PROCEDURE PageDto_Import
GO
CREATE PROCEDURE [dbo].[PageDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@PageMapNodeId			uniqueidentifier = NULL OUTPUT,
	@Publish				bit = NULL,	
	@Overwrite				bit = NULL,		
	@ModifiedBy				uniqueidentifier,
	@PreserveVariantContent bit = 1,
	@IgnoreExisting			bit = NULL,
	@InvalidateCache		bit = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier, @DisplayOrder int

	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @Id = [dbo].[Page_GetVariantId](@SourceId, @TargetSiteId)

	IF @PageMapNodeId IS NULL
	BEGIN
		DECLARE @ParentPageMapNode uniqueidentifier
		SELECT TOP 1 @ParentPageMapNode = PageMapNodeId, @DisplayOrder = DisplayOrder FROM PageMapNodePageDef 
			WHERE PageDefinitionId = @SourceId
		SET @PageMapNodeId = [dbo].[Menu_GetVariantId](@ParentPageMapNode, @TargetSiteId)
	END

	IF @PageMapNodeId IS NULL OR (@Id IS NOT NULL AND @IgnoreExisting = 1)
		RETURN

	SELECT @ModifiedBy = ModifiedBy FROM PageDefinition WHERE PageDefinitionId = @SourceId 
	IF @Publish IS NULL AND EXISTS (SELECT 1 FROM PageDefinition WHERE PageDefinitionId = @SourceId AND PageStatus = 8)
		SET @Publish = 1

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()
		INSERT INTO [dbo].[PageDefinition]
		(
			[PageDefinitionId],
			[TemplateId],
			[SiteId],
			[Title],
			[Description],
			[PageStatus],
			[WorkflowState],
			[PublishCount],
			[PublishDate],
			[FriendlyName],
			[WorkflowId],
			[CreatedBy],
			[CreatedDate],
			[ModifiedBy],
			[ModifiedDate],
			[ArchivedDate],
			[StatusChangedDate],
			[AuthorId],
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			[SourcePageDefinitionId],
			[CustomAttributes]
		)
		SELECT @Id,
			[TemplateId],
			@TargetSiteId,
			[Title],
			D.[Description],
			CASE WHEN @Publish = 1 THEN 8 ELSE PageStatus END,
			1,
			0,
			'1900-01-01 00:00:00.000',
			[FriendlyName],
			@EmptyGuid,
			@ModifiedBy,
			@UtcNow,
			@ModifiedBy,
			@UtcNow,
			[ArchivedDate],
			[StatusChangedDate],
			@ModifiedBy,
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			PageDefinitionId,
			D.[CustomAttributes]
		FROM [dbo].[PageDefinition] D
		WHERE PageDefinitionId = @SourceId				

		UPDATE PageMapNode SET TargetId = @Id
		WHERE TargetId = @SourceId AND SiteId = @TargetSiteId
		 
		INSERT INTO [dbo].[PageDefinitionSegment]
		(
			[Id],
			[PageDefinitionId],
			[DeviceId],
			[AudienceSegmentId],
			[UnmanagedXml],
			[ZonesXml]
		)
		SELECT NEWID(), 
			@Id,
			[DeviceId],
			[AudienceSegmentId],
			dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
			[ZonesXml]
		FROM [dbo].[PageDefinitionSegment] PS 
		WHERE PageDefinitionId = @SourceId

		INSERT INTO [dbo].[PageDetails]
		(
			[PageId],
			[PageDetailXML]
		)
		SELECT 
			@Id,
			[PageDetailXML]
		FROM [dbo].[PageDetails] PD
		WHERE PD.PageId = @SourceId
	END
	ELSE
	BEGIN
		UPDATE C SET
			C.Title = P.Title,
			C.TemplateId = P.TemplateId,
			C.InWFTemplateId = P.InWFTemplateId,
			C.Description = P.Description,
			C.PageStatus = CASE WHEN @Publish = 1 THEN 8 ELSE P.PageStatus END,
			C.FriendlyName = CASE WHEN @PreserveVariantContent = 0 THEN P.FriendlyName ELSE C.FriendlyName END,
			C.EnableOutputCache = P.EnableOutputCache,
			C.OutputCacheProfileName = P.OutputCacheProfileName,
			C.ExcludeFromSearch = P.ExcludeFromSearch,
			C.HasForms = P.HasForms,
			C.ScheduledPublishDate = P.ScheduledPublishDate,
			C.ScheduledArchiveDate = P.ScheduledArchiveDate,
			C.ModifiedDate = @UtcNow,
			C.ModifiedBy = P.ModifiedBy
		FROM PageDefinition P, PageDefinition C
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.UnmanagedXml = dbo.Page_GetUnmanagedXml(P.[UnmanagedXml], @TargetSiteId),
			C.ZonesXml = P.ZonesXml
		FROM PageDefinitionSegment P
			JOIN PageDefinitionSegment C ON P.DeviceId = C.DeviceId AND P.AudienceSegmentId = C.AudienceSegmentId
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.ContentId = P.ContentId,
			C.InWFContentId = P.InWFContentId,
			C.ContentTypeId = P.ContentTypeId,
			C.IsModified = P.IsModified,
			C.[IsTracked] = P.[IsTracked],
			C.[Visible] = P.[Visible],
			C.[InWFVisible] = P.[InWFVisible],
			C.[CustomAttributes] = P.[CustomAttributes],
			C.[DisplayOrder] = P.[DisplayOrder],
			C.[InWFDisplayOrder] = P.[InWFDisplayOrder]
		FROM PageDefinitionContainer P
			JOIN PageDefinitionContainer C ON P.ContainerName = C.ContainerName
			LEFT JOIN COContent CO ON C.ContentId = CO.Id
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId
			AND (@Overwrite = 1 OR CO.Id IS NULL OR CO.ApplicationId != @TargetSiteId)
		
		IF @PreserveVariantContent = 0
		BEGIN
			UPDATE C 
			SET	C.PageDetailXML = P.PageDetailXML
			FROM PageDetails P, PageDetails C
			WHERE C.PageId = @Id AND P.PageId = @SourceId	
		END

		DELETE FROM PageMapNodePageDef WHERE PageDefinitionId = @Id
	END

	INSERT INTO PageMapNodePageDef
	(
		PageDefinitionId,
		PageMapNodeId,
		DisplayOrder
	)
	SELECT
		@Id,
		@PageMapNodeId,
		@DisplayOrder

	INSERT INTO [PageDefinitionContainer]
	(
		[Id],
		PageDefinitionSegmentId ,
		[PageDefinitionId],
		[ContainerId],
		[ContentId],
		[InWFContentId],
		[ContentTypeId],
		[IsModified],
		[IsTracked],
		[Visible],
		[InWFVisible],
		DisplayOrder,
		InWFDisplayOrder,
		[CustomAttributes],
		[ContainerName]
	)
	SELECT NEWID(),
		CS.Id,
		@Id,
		ContainerId,
		ContentId,
		InWFContentId,
		ContentTypeId,
		IsModified,
		PC.IsTracked,
		Visible,
		InWFVisible,
		PC.DisplayOrder, 
		PC.InWFDisplayOrder, 
		PC.CustomAttributes,
		ContainerName
	FROM PageDefinitionContainer PC
		JOIN PageDefinitionSegment PS ON PC.PageDefinitionSegmentId = PS.Id
		JOIN PageDefinitionSegment CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
	WHERE PC.PageDefinitionId = @SourceId
		AND (@Id IS NULL OR ContainerName NOT IN (SELECT ContainerName FROM PageDefinitionContainer WHERE PageDefinitionId = @Id))

	DECLARE @SourceTargetMenuId uniqueidentifier
	SELECT TOP 1 @SourceTargetMenuId = PageMapNodeId FROM PageMapNode PN
		JOIN PageDefinition PD ON PN.TargetId = PD.PageDefinitionId AND PD.SiteId = PN.SiteId
	WHERE PD.PageDefinitionId = @SourceId

	IF @SourceTargetMenuId IS NOT NULL
	BEGIN
		UPDATE PageMapNode SET
			TargetId = @Id,
			Target = 1,
			TargetUrl = ''
		WHERE SourcePageMapNodeId = @SourceTargetMenuId
			AND SiteId = @TargetSiteId
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[USSecurityLevelObject]
	(
		[ObjectId],
		[ObjectTypeId],
		[SecurityLevelId]
	)
	SELECT @Id,
		ObjectTypeId,
		SecurityLevelId
	FROM [USSecurityLevelObject] S
	WHERE S.ObjectId = @SourceId

	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageScript]
	(
		[PageDefinitionId],
		[ScriptId],
		[CustomAttributes]
	)
	SELECT @Id,
		[ScriptId],
		[CustomAttributes]
	FROM [dbo].[SIPageScript] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageStyle]
	(
		[PageDefinitionId],
		[StyleId],
		[CustomAttributes]
	)
	SELECT @Id,
		[StyleId],
		[CustomAttributes]
	FROM [dbo].[SIPageStyle] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM COTaxonomyObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[COTaxonomyObject]
	(
		[Id],
		[TaxonomyId],
		[ObjectId],
		[ObjectTypeId],
		[SortOrder],
		[Status],
		[CreatedBy],
		[CreatedDate],
		[ModifiedBy],
		[ModifiedDate]
	)
	SELECT NEWID(),
		[TaxonomyId],
		@Id,
		[ObjectTypeId],
		[SortOrder],
		[Status],
		@ModifiedBy,
		@UtcNow,
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[COTaxonomyObject] OT
	WHERE OT.ObjectId = @SourceId

	DECLARE @VersionId uniqueidentifier, @SourceVersionId uniqueidentifier, 
		@VersionNumber int, @RevisionNumber int, @VersionStatus int
	SET @VersionId = NEWID()
	SELECT TOP 1 @SourceVersionId = Id FROM VEVersion 
	WHERE ObjectId = @SourceId ORDER BY CreatedDate DESC	

	SELECT TOP 1 @VersionNumber = VersionNumber, @RevisionNumber = RevisionNumber FROM VEVersion
	WHERE ObjectId = @Id ORDER BY CreatedDate DESC

	IF @Publish = 1
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0) + 1
		SET @VersionNumber = 0
		SET @VersionStatus = 1
	END
	ELSE
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0)
		SET @VersionNumber = ISNULL(@VersionNumber, 0) + 1
		SET @VersionStatus = 0
	END

	INSERT INTO [dbo].[VEVersion]
	(
		[Id],
		[ApplicationId],
		[ObjectTypeId],
		[ObjectId],
		[CreatedDate],
		[CreatedBy],
		[VersionNumber],
		[RevisionNumber],
		[Status],
		[ObjectStatus],
		[VersionStatus],
		[XMLString],
		[Comments],
		[TriggeredObjectId],
		[TriggeredObjectTypeId]
	)          
	SELECT TOP 1 @VersionId,
		@TargetSiteId,
		8, 
		@Id, 
		@UtcNow,
		@ModifiedBy,
		@VersionNumber,
		@RevisionNumber,
		@VersionStatus,
		ObjectStatus,
		VersionStatus, 
		dbo.UpdatePageXml(XMLString, @Id, P.WorkflowId, P.WorkflowState, P.PageStatus, 1, 
			P.CreatedBy, P.CreatedDate, P.AuthorId, P.SiteId, P.SourcePageDefinitionId, @PageMapNodeId), 
		'Imported from master',
		TriggeredObjectId,
		TriggeredObjectTypeId       
	FROM VEVersion V, 
		PageDefinition P
	WHERE V.Id = @SourceVersionId
		AND P.PageDefinitionId = @Id

	IF @Publish = 1
		UPDATE VEVersion SET Status = 1 WHERE ObjectId = @Id

	INSERT INTO [dbo].[VEPageDefSegmentVersion]
	(
		[Id],
		[VersionId],
		[PageDefinitionId],
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],  
		[ContainerContentXML],        
		[UnmanagedXml],
		[ZonesXml],
		[CreatedBy],
		[CreatedDate]
	)
	SELECT NEWID(),
		@VersionId,
		@Id,
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],
		CASE WHEN @Overwrite = 1 THEN [ContainerContentXML] 
			ELSE [dbo].[Page_GetContainerXml](@Id, AudienceSegmentId, DeviceId) END,
		dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
		[ZonesXml],
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[VEPageDefSegmentVersion]
	WHERE VersionId = @SourceVersionId

	INSERT INTO [dbo].[VEPageContentVersion]
	(
		[Id],
		[ContentVersionId],
		[IsChanged],
		[PageDefSegmentVersionId]
	)
	SELECT NEWID(),
		ContentVersionId,
		IsChanged,
		CS.Id
	FROM [dbo].[VEPageContentVersion] PC
		JOIN VEPageDefSegmentVersion PS ON PC.PageDefSegmentVersionId = PS.Id
		JOIN VEPageDefSegmentVersion CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
			AND PS.VersionId = @SourceVersionId
			AND CS.VersionId = @VersionId

	IF @Publish = 1
	BEGIN
		DECLARE @PublishCount int
		SET @PublishCount = (SELECT ISNULL(PublishCount, 0) + 1 FROM PageDefinition WHERE PageDefinitionId = @Id)

		UPDATE PageDefinition SET
			PublishCount = @PublishCount,
			PublishDate = @UtcNow,
			PageStatus = 8
		WHERE PageDefinitionId = @Id

		DECLARE @WorkflowId uniqueidentifier
		SELECT TOP 1 @WorkflowId = WorkflowId FROM WFWorkflowExecution WHERE ObjectId = @Id AND Completed = 0

		IF @WorkflowId IS NOT NULL
		BEGIN
			EXEC WorkflowExecution_Save
				@WorkflowId = @WorkflowId,
				@SequenceId = @EmptyGuid,
				@ObjectId = @Id,
				@ObjectTypeId = 8,
				@ActorId = @ModifiedBy,
				@ActorType = 1,
				@ActionId = 8,
				@Remarks = N'Page imported from master',
				@Completed = 1
		END

		DELETE
		FROM	PageObjectReferences
		WHERE	PageId = @Id

		INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
			SELECT	NEWID(), @Id, ObjectId, ObjectType, ObjectSiteId
			FROM	PageObjectReferences
			WHERE	PageId = @SourceId
	END

	IF @InvalidateCache = 1
	BEGIN
		EXEC PageDto_Invalidate @SiteId = @TargetSiteId, @ModifiedBy = @ModifiedBy
	END
END
GO
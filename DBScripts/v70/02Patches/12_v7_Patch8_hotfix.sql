DECLARE @CreatedBy uniqueidentifier
SELECT TOP 1 @CreatedBy = Id FROM USUser WHERE UserName like 'IAppsSystemUser'

IF NOT EXISTS (Select 1 FROM IAInsightsSetting where Title = 'Insights.SiteIdDimension')
BEGIN
INSERT INTO [dbo].[IAInsightsSetting]
           ([SiteId]
           ,[Title]
           ,[Value]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[Description]
           ,[Sequence])
     VALUES
           ('8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
           ,'Insights.SiteIdDimension'
           ,'false'
           ,@CreatedBy
           ,GetUTCDate()
           ,'Name of the custom siteId dimension. False to disable'
           ,2)
END
GO
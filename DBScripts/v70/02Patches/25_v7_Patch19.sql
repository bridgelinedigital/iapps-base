IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'JQueryUI.CDNPath' AND SettingGroupId = 3 AND IsVisible = 1)
BEGIN
	UPDATE STSettingType SET FriendlyName = 'Frontend jQuery CDN path', SettingGroupId = 3, IsVisible = 1 WHERE Name = 'JQuery.CDNPath'
	UPDATE STSettingType SET FriendlyName = 'Include jQuery script in frontend pages', SettingGroupId = 3, IsVisible = 1 WHERE Name = 'JQuery.Include'
	UPDATE STSettingType SET FriendlyName = 'Use CDN for jQuery', SettingGroupId = 3, IsVisible = 1 WHERE Name = 'JQuery.UseCDN'
	UPDATE STSettingType SET FriendlyName = 'Frontend jQueryUI CDN path', SettingGroupId = 3, IsVisible = 1 WHERE Name = 'JQueryUI.CDNPath'
	UPDATE STSettingType SET FriendlyName = 'Include jQueryUI script in frontend pages', SettingGroupId = 3, IsVisible = 1 WHERE Name = 'JQueryUI.Include'
	UPDATE STSettingType SET FriendlyName = 'Use CDN for jQueryUI', SettingGroupId = 3, IsVisible = 1 WHERE Name = 'JQueryUI.UseCDN'
END
GO
PRINT 'Add Site Settings'
DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int

SET @SettingGroupId = 1
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Dashboard.HideWoorankWidget')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Dashboard.HideWoorankWidget', 'Hide Woorank widget in dashboard',  @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, 'false' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Script.CacheBustingEnabled')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Script.CacheBustingEnabled', 'Add modified date querystring to all script links',  @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, 'true' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Style.CacheBustingEnabled')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Style.CacheBustingEnabled', 'Add modified date querystring to all style links',  @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, 'true' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
GO
print 'Creating table CLObjectImageSequence'
GO
IF OBJECT_ID('CLObjectImageSequence') IS NULL
CREATE TABLE CLObjectImageSequence
(
	CLObjectImageId uniqueidentifier,
	Sequence INT,
	SiteId uniqueidentifier
)
GO
print 'Creating Custom type TYProductImageSequence'
GO
IF NOT EXISTS (SELECT 1 FROM sys.types  where name='TYProductImageSequence')
BEGIN
CREATE TYPE TYProductImageSequence AS TABLE (
	SiteId UNIQUEIDENTIFIER,
	ImageId UNIQUEIDENTIFIER,
	Sequence INT
	)
	END

GO
print 'Creating procedure Product_UpdateImageSequence'
GO
IF OBJECT_ID('Product_UpdateImageSequence','P') IS NOT NULL
	DROP PROCEDURE Product_UpdateImageSequence
GO
CREATE PROCEDURE Product_UpdateImageSequence
(
	@ProductId uniqueidentifier,
	@ProductImageSequence TYProductImageSequence READONLY
)
AS
BEGIN
	--delete the records from the CLObjectImageSequence table
	DELETE ObjSeq 
	FROM CLObjectImageSequence ObjSeq JOIN CLObjectImage ObjImage ON ObjSeq.CLObjectImageId=ObjImage.Id
	JOIN @ProductImageSequence PS ON PS.ImageId = ObjImage.ImageId   
	WHERE ObjImage.ObjectId=@ProductId AND ObjSeq.SiteId = PS.SiteId

	--insert the new records into it
	INSERT INTO CLObjectImageSequence
	(
		CLObjectImageId,
		Sequence,
		SiteId
	)
	SELECT Cl.Id, 
	Ty.Sequence, 
	Ty.SiteId
	FROM @ProductImageSequence Ty JOIN CLObjectImage CL
	ON Ty.ImageId = CL.ImageId 
	JOIN CLImage CLI ON CLI.Id = CL.ImageId AND CL.SiteId=CLI.SiteId
	WHERE CL.ObjectId=@ProductId
END
GO
PRINT 'Altering view vwProductImage'
GO
IF(OBJECT_ID('vwProductImage') IS NOT NULL)
	DROP VIEW vwProductImage
GO	
CREATE VIEW [dbo].[vwProductImage]  
AS  
WITH CTE AS  
(  
 SELECT Id, SiteId, Title, AltText, Description, Status, ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy    
 FROM CLImage  
  
 UNION ALL  
  
 SELECT Id, SiteId, Title, AltText, Description, Status, ModifiedDate, ModifiedBy  
 FROM CLImageVariantCache  
)  
  
SELECT I2.Id,  
 C.Title,  
 I2.FileName,  
 C.AltText,  
 C.Description,  
 I.Size,  
 ISNULL(SEQ.Sequence, OI.Sequence) Sequence,  
 P.Id AS ProductId,  
 P.Title AS ProductName,  
 I.CreatedBy,  
 I.CreatedDate,  
 C.ModifiedBy,  
 C.ModifiedDate,  
 FN.UserFullName CreatedByFullName,  
 MN.UserFullName ModifiedByFullName,  
 P.ProductTypeId,  
 '/productimages/' + CAST(P.Id AS varchar(36)) + '/images' AS RelativePath,  
 C.SiteId,  
 I.SiteId AS SourceSiteId,  
 IT.TypeNumber,  
 C.Status,  
 I2.ParentImage,  
 I.IsShared  
FROM PRProduct P  
 JOIN CLObjectImage OI ON OI.ObjectId = P.Id  
 JOIN CTE C ON OI.ImageId = C.Id --AND OI.SiteId = C.SiteId  
 JOIN CLImage I ON I.Id = C.Id  
 JOIN CLImage I2 ON I.Id = I2.ParentImage OR I.Id = I2.Id  
 JOIN CLImageType IT ON IT.Id = I2.ImageType  
 LEFT JOIN VW_UserFullName FN on FN.UserId = I.CreatedBy  
 LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy  
 LEFT JOIN CLObjectImageSequence SEQ On SEQ.CLObjectImageId = OI.Id AND SEQ.SiteId=C.SiteId
 WHERE C.SiteId = I.SiteId or (C.SiteId != I.SiteId and I.IsShared = 1) 
GO
PRINT 'Altering view vwSKU'
GO
IF(OBJECT_ID('vwSKU') IS NOT NULL)
	DROP VIEW vwSKU
GO	
CREATE VIEW [dbo].[vwSKU]
AS
WITH CTE AS
(
	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		ISNULL(P.ModifiedDate, P.CreatedDate) AS ModifiedDate, 
		ISNULL(P.ModifiedBy, P.CreatedBy) AS  ModifiedBy
	FROM PRProductSku P

	UNION ALL

	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		P.ModifiedDate, 
		P.ModifiedBy
	FROM PRProductSKUVariantCache P
)

SELECT	C.Id, SK.ProductId, C.SiteId, SK.SiteId AS SourceSiteId, C.Title, C.[Description], SK.SKU, C.IsActive, C.IsOnline, C.UnitCost,
		C.WholesalePrice, C.PreviousSoldCount, C.Measure, C.OrderMinimum, C.OrderIncrement, C.HandlingCharges,
		SK.Height, SK.Width, SK.[Length], SK.[Weight], C.SelfShippable, C.AvailableForBackOrder, C.BackOrderLimit, C.IsUnlimitedQuantity,
		C.MaximumDiscountPercentage, C.FreeShipping, C.UserDefinedPrice, C.[Sequence], SK.[Status],
		C.ListPrice, 
		ISNULL(ATS.Inventory, 0) AS Quantity, 
		CASE WHEN P.IsBundle = 0 THEN ISNULL(ATS.Quantity, 0) ELSE dbo.GetBundleQuantity(SK.Id) END AS AvailableToSell,
		SK.CreatedDate, SK.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		C.ModifiedDate, C.ModifiedBy, MFN.UserFullName AS ModifiedByFullName,
		ISNULL(PC.Title, P.Title) AS ProductName, P.ProductTypeId, PT.IsDownloadableMedia,
		case when ISNULL(PC.IsActive,P.IsActive)=1 then C.IsActive else 0 end IsSellable
FROM	CTE AS C
		INNER JOIN PRProductSku AS SK ON SK.Id = C.Id
		INNER JOIN PRProduct AS P ON P.Id = SK.ProductId
		LEFT JOIN PRProductVariantCache AS PC ON PC.Id = SK.ProductId AND PC.SiteId = C.SiteId
		INNER JOIN PRProductType AS PT ON PT.Id = P.ProductTypeID
		LEFT JOIN vwAvailableToSellPerSite AS ATS ON ATS.SKUId = SK.Id AND ATS.SiteId = SK.SiteId AND c.IsActive = 1
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = SK.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = SK.ModifiedBy
GO
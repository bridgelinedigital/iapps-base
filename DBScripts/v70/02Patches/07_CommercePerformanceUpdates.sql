IF(OBJECT_ID('OrderItem_ClearCouponValueMulti') IS NOT NULL)
	DROP PROCEDURE  OrderItem_ClearCouponValueMulti
GO	
CREATE PROCEDURE [dbo].[OrderItem_ClearCouponValueMulti]
	@OrderItems xml
AS

Declare @orderIds table (Id uniqueidentifier)

INSERT @orderIds(Id)
select T.col.value('text()[1]','uniqueidentifier')
	from @OrderItems.nodes('/GenericCollectionOfGuid/guid') T(col) 

DELETE CC FROM OROrderItemCouponCode CC 
INNER JOIN @orderIds OI ON CC.OrderItemId =OI.Id

DELETE CC FROM OROrderItemCouponVersion CC
INNER JOIN @orderIds OI ON CC.OrderItemId =OI.Id

GO

IF(OBJECT_ID('OrderItem_SaveMulti') IS NOT NULL)
	DROP PROCEDURE  OrderItem_SaveMulti
GO	

CREATE PROCEDURE [dbo].[OrderItem_SaveMulti](@OrderItems TYOrderItem READONLY)
AS
BEGIN

Declare @update table (Id uniqueidentifier)

UPDATE OI SET 
   OI.[OrderId]=TI.OrderId ,  
   OI.[OrderShippingId]=TI.OrderShippingId ,  
   OI.[OrderItemStatusId]=TI.OrderItemStatusId ,  
   OI.[ProductSKUId]=TI.ProductSKUId ,  
   OI.[Quantity]=TI.Quantity ,  
   OI.[Price]=TI.Price ,  
   OI.[UnitCost]=TI.UnitCost ,  
   OI.[IsFreeShipping]=TI.IsFreeShipping ,  
   OI.[Sequence]=TI.Sequence ,  
   OI.[Status]=TI.Status ,  
   OI.[CreatedBy]=TI.CreatedBy ,  
   OI.[ModifiedDate]=GetUTCDate() ,  
   OI.[ModifiedBy]=TI.ModifiedBy ,
   OI.[HandlingCharge] = TI.HandlingCharge,
   OI.[ParentOrderItemId]=TI.ParentOrderItemId,
   OI.[BundleItemId]=TI.BundleItemId,
   OI.[CartItemId]=TI.CartItemId,
   OI.[Tax] = TI.Tax,
	OI.[TaxableDiscount] = TI.Discount,
	OI.[Discount]=TI.Discount,
	OI.[HandlingTax] = TI.HandlingTax
	OUTPUT inserted.Id
	INTO @update
FROM OROrderItem OI
INNER JOIN @OrderItems TI ON OI.Id =TI.Id

INSERT INTO [dbo].[OROrderItem]
           ([Id]
           ,[OrderId]
           ,[OrderShippingId]
           ,[OrderItemStatusId]
           ,[ProductSKUId]
           ,[Quantity]
           ,[Price]
           ,[UnitCost]
           ,[IsFreeShipping]
           ,[Sequence]
           ,[Status]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[ParentOrderItemId]
           ,[HandlingCharge]
           ,[BundleItemId]
           ,[CartItemId]
           ,[Tax]
           ,[Discount]
           ,[HandlingTax]
           ,[TaxableDiscount]
           ,[NonTaxableDiscount])
		SELECT [Id]
			  ,[OrderId]
			  ,[OrderShippingId]
			  ,[OrderItemStatusId]
			  ,[ProductSKUId]
			  ,[Quantity]
			  ,[Price]
			  ,[UnitCost]
			  ,[IsFreeShipping]
			  ,[Sequence]
			  ,[Status]
			  ,[CreatedDate]
			  ,[CreatedBy]
			  ,[ModifiedDate]
			  ,[ModifiedBy]
			  ,[ParentOrderItemId]
			  ,[HandlingCharge]
			  ,[BundleItemId]
			  ,[CartItemId]
			  ,[Tax]
			  ,[Discount]
			  ,[HandlingTax]
			  ,[TaxableDiscount]
			  ,[NonTaxableDiscount]
		  FROM @OrderItems TI
		  WHERE ID NOT IN (SELECT Id FROM @update)


		  
	CREATE Table #UPAttributeValue(OrderId uniqueidentifier,OrderItemId Uniqueidentifier,OrderItemAttributeValueId uniqueidentifier)
	
	INSERT INTO #UPAttributeValue(OrderId,OrderItemId,OrderItemAttributeValueId)
	SELECT b.OrderId,b.Id,a.OrderItemAttributeValueId
	from OROrderItemAttributeValue a
	INNER JOIN  OROrderItem b ON  	a.CartItemId = b.CartItemId
	INNER JOIN @OrderItems TI ON TI.Id = b.Id
	where a.OrderItemId is null and b.CartItemId= TI.CartItemId

	Update AV WITH (ROWLOCK) SET AV.OrderId =T.OrderId,AV.OrderItemId=T.OrderItemId
	FROM OROrderItemAttributeValue AV
	INNER JOIN #UPAttributeValue T on AV.OrderItemAttributeValueId = T.OrderItemAttributeValueId


	DECLARE @OrderItemQuantity money
	DECLARE @OrderShipmentItemQuantity money
	DECLARE @tblShipped table (Id uniqueidentifier,OrderItemQuantity money,OrderShipmentItemQuantity money)

	Select @OrderItemQuantity = TI.Quantity
	FROM  OROrderItem  OI
	INNER JOIN @OrderItems TI ON OI.Id=TI.Id

	INSERT INTO @tblShipped(Id,OrderItemQuantity,OrderShipmentItemQuantity)
	Select OrderItemId,max(OI.Quantity), sum(TI.Quantity)
	FROM  FFOrderShipmentItem  SI
	INNER JOIN OROrderItem OI ON SI.OrderItemId=OI.Id
	INNER JOIN FFOrderShipmentPackage  S ON SI.OrderShipmentContainerId=S.Id
	INNER JOIN @OrderItems TI ON TI.Id =SI.OrderItemId
	Where  ShipmentStatus=2
	Group By OrderItemId

		Update OI WITH (ROWLOCK)
		Set OrderItemStatusId=Case When SI.OrderItemQuantity = SI.OrderShipmentItemQuantity Then 2 Else 7 End
		FROM OROrderItem OI 
		INNER JOIN @tblShipped SI ON OI.Id=SI.Id
		WHERE SI.OrderItemQuantity = SI.OrderShipmentItemQuantity OR SI.OrderShipmentItemQuantity>0.0

		if @@error <> 0
		begin
			raiserror('DBERROR||%s',16,1,'Update Status Shipped OrderShipmentItem')
			return dbo.GetDataBaseErrorCode()	
		end

END


PRINT 'Alter ProductImageListDto_Get'
GO
IF (OBJECT_ID('ProductImageListDto_Get') IS NOT NULL)
	DROP PROCEDURE ProductImageListDto_Get
GO
Create PROCEDURE [dbo].[ProductImageListDto_Get]  
(  
 @SearchProductId uniqueidentifier,  
 @SiteId  uniqueidentifier  
)  
AS  
BEGIN  
 DECLARE @tbSkuImageIds TABLE(Id uniqueidentifier)  
  
 INSERT INTO @tbSkuImageIds  
 SELECT ImageId FROM CLObjectImage   
 WHERE ObjectId IN (SELECT Id FROM PRProductSKU WHERE ProductId = @SearchProductId)  
  
 ;WITH CTE AS   
 (  
  SELECT I.Id FROM CLImage I  
   JOIN CLObjectImage OI ON I.Id = OI.ImageId  
  WHERE OI.ObjectId = @SearchProductId  
   AND I.ParentImage IS NULL  
   AND I.Id NOT IN (SELECT Id FROM @tbSkuImageIds)  
  
  UNION ALL  
  
  SELECT I.Id FROM CLImage I  
   JOIN CLObjectImage OI ON I.Id = OI.ImageId  
  WHERE I.ParentImage IS NULL  
   AND OI.ObjectId IN (  
    SELECT Id FROM vwSKU   
    WHERE ProductId = @SearchProductId   
     AND SiteId = @SiteId AND IsActive = 1 AND IsOnline = 1  
   )  
 )  
  
 SELECT * FROM vwProductImage I  
  JOIN CTE C ON I.Id = C.Id OR I.ParentImage = C.Id  
 WHERE SiteId = @SiteId  
 ORDER BY Sequence
  
END  
GO
PRINT 'Alter CartItem_Delete'
GO
IF (OBJECT_ID('CartItem_Delete') IS NOT NULL)
	DROP PROCEDURE CartItem_Delete
GO
CREATE PROCEDURE [dbo].[CartItem_Delete]
(
	@Id		uniqueidentifier,
	@CartId uniqueidentifier
)
AS
BEGIN
	DECLARE @CurrentDate datetime
	SET @CurrentDate = GETUTCDATE()

	IF(@Id IS NULL OR  @Id = dbo.GetEmptyGUID())
	BEGIN
		DELETE FROM CSCartItem WHERE CartId = @CartId

		DELETE FROM OROrderItemAttributeValue WHERE CartId = @CartId
	END
	ELSE
	BEGIN
		--Get the cart id for the given cartitem id
		IF @CartId IS NULL
		BEGIN
			SELECT @CartId = CartId FROM CSCartItem WHERE Id=@Id
		END

		DECLARE @ThisItemSequence int, @MaxSequence int

		SELECT @ThisItemSequence = Sequence FROM CSCartItem WHERE Id = @Id
		SELECT @MaxSequence = ISNULL(MAX(Sequence), 1) FROM CSCartItem WHERE CartId = @CartId
	
		DELETE FROM CSCartItem WHERE CartId=@CartId AND ParentCartItemId = @Id
		DELETE FROM CSCartItem WHERE Id = @Id

		DELETE FROM OROrderItemAttributeValue WHERE CartItemId = @Id
		
		IF @ThisItemSequence < @MaxSequence
		BEGIN	
			UPDATE CSCartItem SET Sequence = Sequence - 1 
			WHERE Sequence > @ThisItemSequence and CartId = @CartId			
		END
	END
		
	UPDATE CSCart Set ModifiedDate = @CurrentDate WHERE Id = @CartId
END
GO
PRINT 'Alter PageDto_Import'
GO
IF (OBJECT_ID('PageDto_Import') IS NOT NULL)
	DROP PROCEDURE PageDto_Import
GO
CREATE PROCEDURE [dbo].[PageDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@PageMapNodeId			uniqueidentifier = NULL OUTPUT,
	@Publish				bit = NULL,	
	@Overwrite				bit = NULL,		
	@ModifiedBy				uniqueidentifier,
	@PreserveVariantContent bit = 1,
	@IgnoreExisting			bit = NULL,
	@InvalidateCache		bit = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier, @DisplayOrder int

	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @Id = [dbo].[Page_GetVariantId](@SourceId, @TargetSiteId)

	IF @PageMapNodeId IS NULL
	BEGIN
		DECLARE @ParentPageMapNode uniqueidentifier
		SELECT TOP 1 @ParentPageMapNode = PageMapNodeId, @DisplayOrder = DisplayOrder FROM PageMapNodePageDef 
			WHERE PageDefinitionId = @SourceId
		SET @PageMapNodeId = [dbo].[Menu_GetVariantId](@ParentPageMapNode, @TargetSiteId)
	END

	IF @PageMapNodeId IS NULL OR (@Id IS NOT NULL AND @IgnoreExisting = 1)
		RETURN

	SELECT @ModifiedBy = ModifiedBy FROM PageDefinition WHERE PageDefinitionId = @SourceId 
	IF @Publish IS NULL AND EXISTS (SELECT 1 FROM PageDefinition WHERE PageDefinitionId = @SourceId AND PageStatus = 8)
		SET @Publish = 1

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()
		INSERT INTO [dbo].[PageDefinition]
		(
			[PageDefinitionId],
			[TemplateId],
			[SiteId],
			[Title],
			[Description],
			[PageStatus],
			[WorkflowState],
			[PublishCount],
			[PublishDate],
			[FriendlyName],
			[WorkflowId],
			[CreatedBy],
			[CreatedDate],
			[ModifiedBy],
			[ModifiedDate],
			[ArchivedDate],
			[StatusChangedDate],
			[AuthorId],
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[ExcludeFromExternalSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			[SourcePageDefinitionId],
			[CustomAttributes]
		)
		SELECT @Id,
			[TemplateId],
			@TargetSiteId,
			[Title],
			D.[Description],
			CASE WHEN @Publish = 1 THEN 8 ELSE PageStatus END,
			1,
			0,
			'1900-01-01 00:00:00.000',
			[FriendlyName],
			@EmptyGuid,
			@ModifiedBy,
			@UtcNow,
			@ModifiedBy,
			@UtcNow,
			[ArchivedDate],
			[StatusChangedDate],
			@ModifiedBy,
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[ExcludeFromExternalSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			PageDefinitionId,
			D.[CustomAttributes]
		FROM [dbo].[PageDefinition] D
		WHERE PageDefinitionId = @SourceId				

		UPDATE PageMapNode SET TargetId = @Id
		WHERE TargetId = @SourceId AND SiteId = @TargetSiteId
		 
		INSERT INTO [dbo].[PageDefinitionSegment]
		(
			[Id],
			[PageDefinitionId],
			[DeviceId],
			[AudienceSegmentId],
			[UnmanagedXml],
			[ZonesXml]
		)
		SELECT NEWID(), 
			@Id,
			[DeviceId],
			[AudienceSegmentId],
			dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
			[ZonesXml]
		FROM [dbo].[PageDefinitionSegment] PS 
		WHERE PageDefinitionId = @SourceId

		INSERT INTO [dbo].[PageDetails]
		(
			[PageId],
			[PageDetailXML]
		)
		SELECT 
			@Id,
			[PageDetailXML]
		FROM [dbo].[PageDetails] PD
		WHERE PD.PageId = @SourceId
	END
	ELSE
	BEGIN
		UPDATE C SET
			C.Title = P.Title,
			C.TemplateId = P.TemplateId,
			C.InWFTemplateId = P.InWFTemplateId,
			C.Description = P.Description,
			C.PageStatus = CASE WHEN @Publish = 1 THEN 8 ELSE P.PageStatus END,
			C.FriendlyName = CASE WHEN @PreserveVariantContent = 0 THEN P.FriendlyName ELSE C.FriendlyName END,
			C.EnableOutputCache = P.EnableOutputCache,
			C.OutputCacheProfileName = P.OutputCacheProfileName,
			C.ExcludeFromSearch = P.ExcludeFromSearch,
			C.ExcludeFromExternalSearch = P.ExcludeFromExternalSearch,
			C.HasForms = P.HasForms,
			C.ScheduledPublishDate = P.ScheduledPublishDate,
			C.ScheduledArchiveDate = P.ScheduledArchiveDate,
			C.ModifiedDate = @UtcNow,
			C.ModifiedBy = P.ModifiedBy
		FROM PageDefinition P, PageDefinition C
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.UnmanagedXml = dbo.Page_GetUnmanagedXml(P.[UnmanagedXml], @TargetSiteId),
			C.ZonesXml = P.ZonesXml
		FROM PageDefinitionSegment P
			JOIN PageDefinitionSegment C ON P.DeviceId = C.DeviceId AND P.AudienceSegmentId = C.AudienceSegmentId
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.ContentId = P.ContentId,
			C.InWFContentId = P.InWFContentId,
			C.ContentTypeId = P.ContentTypeId,
			C.IsModified = P.IsModified,
			C.[IsTracked] = P.[IsTracked],
			C.[Visible] = P.[Visible],
			C.[InWFVisible] = P.[InWFVisible],
			C.[CustomAttributes] = P.[CustomAttributes],
			C.[DisplayOrder] = P.[DisplayOrder],
			C.[InWFDisplayOrder] = P.[InWFDisplayOrder]
		FROM PageDefinitionContainer P
			JOIN PageDefinitionContainer C ON P.ContainerName = C.ContainerName
			LEFT JOIN COContent CO ON C.ContentId = CO.Id
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId
			AND (@Overwrite = 1 OR CO.Id IS NULL OR CO.ApplicationId != @TargetSiteId)
		
		IF @PreserveVariantContent = 0
		BEGIN
			UPDATE C 
			SET	C.PageDetailXML = P.PageDetailXML
			FROM PageDetails P, PageDetails C
			WHERE C.PageId = @Id AND P.PageId = @SourceId	
		END

		DELETE FROM PageMapNodePageDef WHERE PageDefinitionId = @Id
	END

	INSERT INTO PageMapNodePageDef
	(
		PageDefinitionId,
		PageMapNodeId,
		DisplayOrder
	)
	SELECT
		@Id,
		@PageMapNodeId,
		@DisplayOrder

	INSERT INTO [PageDefinitionContainer]
	(
		[Id],
		PageDefinitionSegmentId ,
		[PageDefinitionId],
		[ContainerId],
		[ContentId],
		[InWFContentId],
		[ContentTypeId],
		[IsModified],
		[IsTracked],
		[Visible],
		[InWFVisible],
		DisplayOrder,
		InWFDisplayOrder,
		[CustomAttributes],
		[ContainerName]
	)
	SELECT NEWID(),
		CS.Id,
		@Id,
		ContainerId,
		ContentId,
		InWFContentId,
		ContentTypeId,
		IsModified,
		PC.IsTracked,
		Visible,
		InWFVisible,
		PC.DisplayOrder, 
		PC.InWFDisplayOrder, 
		PC.CustomAttributes,
		ContainerName
	FROM PageDefinitionContainer PC
		JOIN PageDefinitionSegment PS ON PC.PageDefinitionSegmentId = PS.Id
		JOIN PageDefinitionSegment CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
	WHERE PC.PageDefinitionId = @SourceId
		AND (@Id IS NULL OR ContainerName NOT IN (SELECT ContainerName FROM PageDefinitionContainer WHERE PageDefinitionId = @Id))

	DECLARE @SourceTargetMenuId uniqueidentifier
	SELECT TOP 1 @SourceTargetMenuId = PageMapNodeId FROM PageMapNode PN
		JOIN PageDefinition PD ON PN.TargetId = PD.PageDefinitionId AND PD.SiteId = PN.SiteId
	WHERE PD.PageDefinitionId = @SourceId

	IF @SourceTargetMenuId IS NOT NULL
	BEGIN
		UPDATE PageMapNode SET
			TargetId = @Id,
			Target = 1,
			TargetUrl = ''
		WHERE SourcePageMapNodeId = @SourceTargetMenuId
			AND SiteId = @TargetSiteId
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[USSecurityLevelObject]
	(
		[ObjectId],
		[ObjectTypeId],
		[SecurityLevelId]
	)
	SELECT @Id,
		ObjectTypeId,
		SecurityLevelId
	FROM [USSecurityLevelObject] S
	WHERE S.ObjectId = @SourceId

	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageScript]
	(
		[PageDefinitionId],
		[ScriptId],
		[CustomAttributes]
	)
	SELECT @Id,
		[ScriptId],
		[CustomAttributes]
	FROM [dbo].[SIPageScript] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageStyle]
	(
		[PageDefinitionId],
		[StyleId],
		[CustomAttributes]
	)
	SELECT @Id,
		[StyleId],
		[CustomAttributes]
	FROM [dbo].[SIPageStyle] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM COTaxonomyObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[COTaxonomyObject]
	(
		[Id],
		[TaxonomyId],
		[ObjectId],
		[ObjectTypeId],
		[SortOrder],
		[Status],
		[CreatedBy],
		[CreatedDate],
		[ModifiedBy],
		[ModifiedDate]
	)
	SELECT NEWID(),
		[TaxonomyId],
		@Id,
		[ObjectTypeId],
		[SortOrder],
		[Status],
		@ModifiedBy,
		@UtcNow,
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[COTaxonomyObject] OT
	WHERE OT.ObjectId = @SourceId

	DECLARE @VersionId uniqueidentifier, @SourceVersionId uniqueidentifier, 
		@VersionNumber int, @RevisionNumber int, @VersionStatus int
	SET @VersionId = NEWID()
	SELECT TOP 1 @SourceVersionId = Id FROM VEVersion 
	WHERE ObjectId = @SourceId ORDER BY CreatedDate DESC	

	SELECT TOP 1 @VersionNumber = VersionNumber, @RevisionNumber = RevisionNumber FROM VEVersion
	WHERE ObjectId = @Id ORDER BY CreatedDate DESC

	IF @Publish = 1
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0) + 1
		SET @VersionNumber = 0
		SET @VersionStatus = 1
	END
	ELSE
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0)
		SET @VersionNumber = ISNULL(@VersionNumber, 0) + 1
		SET @VersionStatus = 0
	END

	INSERT INTO [dbo].[VEVersion]
	(
		[Id],
		[ApplicationId],
		[ObjectTypeId],
		[ObjectId],
		[CreatedDate],
		[CreatedBy],
		[VersionNumber],
		[RevisionNumber],
		[Status],
		[ObjectStatus],
		[VersionStatus],
		[XMLString],
		[Comments],
		[TriggeredObjectId],
		[TriggeredObjectTypeId]
	)          
	SELECT TOP 1 @VersionId,
		@TargetSiteId,
		8, 
		@Id, 
		@UtcNow,
		@ModifiedBy,
		@VersionNumber,
		@RevisionNumber,
		@VersionStatus,
		ObjectStatus,
		VersionStatus, 
		dbo.UpdatePageXml(XMLString, @Id, P.WorkflowId, P.WorkflowState, P.PageStatus, 1, 
			P.CreatedBy, P.CreatedDate, P.AuthorId, P.SiteId, P.SourcePageDefinitionId, @PageMapNodeId), 
		'Imported from master',
		TriggeredObjectId,
		TriggeredObjectTypeId       
	FROM VEVersion V, 
		PageDefinition P
	WHERE V.Id = @SourceVersionId
		AND P.PageDefinitionId = @Id

	IF @Publish = 1
		UPDATE VEVersion SET Status = 1 WHERE ObjectId = @Id

	INSERT INTO [dbo].[VEPageDefSegmentVersion]
	(
		[Id],
		[VersionId],
		[PageDefinitionId],
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],  
		[ContainerContentXML],        
		[UnmanagedXml],
		[ZonesXml],
		[CreatedBy],
		[CreatedDate]
	)
	SELECT NEWID(),
		@VersionId,
		@Id,
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],
		CASE WHEN @Overwrite = 1 THEN [ContainerContentXML] 
			ELSE [dbo].[Page_GetContainerXml](@Id, AudienceSegmentId, DeviceId) END,
		dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
		[ZonesXml],
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[VEPageDefSegmentVersion]
	WHERE VersionId = @SourceVersionId

	INSERT INTO [dbo].[VEPageContentVersion]
	(
		[Id],
		[ContentVersionId],
		[IsChanged],
		[PageDefSegmentVersionId]
	)
	SELECT NEWID(),
		ContentVersionId,
		IsChanged,
		CS.Id
	FROM [dbo].[VEPageContentVersion] PC
		JOIN VEPageDefSegmentVersion PS ON PC.PageDefSegmentVersionId = PS.Id
		JOIN VEPageDefSegmentVersion CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
			AND PS.VersionId = @SourceVersionId
			AND CS.VersionId = @VersionId

	IF @Publish = 1
	BEGIN
		DECLARE @PublishCount int
		SET @PublishCount = (SELECT ISNULL(PublishCount, 0) + 1 FROM PageDefinition WHERE PageDefinitionId = @Id)

		UPDATE PageDefinition SET
			PublishCount = @PublishCount,
			PublishDate = @UtcNow,
			PageStatus = 8
		WHERE PageDefinitionId = @Id

		DECLARE @WorkflowId uniqueidentifier
		SELECT TOP 1 @WorkflowId = WorkflowId FROM WFWorkflowExecution WHERE ObjectId = @Id AND Completed = 0

		IF @WorkflowId IS NOT NULL
		BEGIN
			EXEC WorkflowExecution_Save
				@WorkflowId = @WorkflowId,
				@SequenceId = @EmptyGuid,
				@ObjectId = @Id,
				@ObjectTypeId = 8,
				@ActorId = @ModifiedBy,
				@ActorType = 1,
				@ActionId = 8,
				@Remarks = N'Page imported from master',
				@Completed = 1
		END

		DELETE
		FROM	PageObjectReferences
		WHERE	PageId = @Id

		INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
			SELECT	NEWID(), @Id, ObjectId, ObjectType, ObjectSiteId
			FROM	PageObjectReferences
			WHERE	PageId = @SourceId
	END

	IF @InvalidateCache = 1
	BEGIN
		EXEC PageDto_Invalidate @SiteId = @TargetSiteId, @ModifiedBy = @ModifiedBy
	END
END
GO
PRINT 'Alter VariantSite_CopyPages'
GO
IF (OBJECT_ID('VariantSite_CopyPages') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyPages
GO

CREATE PROCEDURE [dbo].[VariantSite_CopyPages]
(  
	@SiteId UNIQUEIDENTIFIER,
	@MicroSiteId UNIQUEIDENTIFIER,
	@ModifiedBy  UNIQUEIDENTIFIER,
	@MakePageStatusAsDraft BIT
	
)
AS
BEGIN

	INSERT INTO [dbo].[PageDefinition]
					   ([PageDefinitionId]
					   ,[TemplateId]
					   ,[SiteId]
					   ,[Title]
					   ,[Description]
					   ,[PageStatus]
					   ,[WorkflowState]
					   ,[PublishCount]
					   ,[PublishDate]
					   --,[DisplayOrder]
					   ,[FriendlyName]
					   ,[WorkflowId]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate]
					   ,[ArchivedDate]
					   ,[StatusChangedDate]
					   ,[AuthorId]
					   ,[EnableOutputCache]
					   ,[OutputCacheProfileName]
					   ,[ExcludeFromSearch]
					   ,[ExcludeFromExternalSearch]
					   ,[IsDefault]
					   ,[IsTracked]
					   ,[HasForms]
					   ,[TextContentCounter]
					   ,[SourcePageDefinitionId]
					   ,[CustomAttributes]
					   )
			SELECT NEWID()
				  ,[TemplateId]
				  ,@MicroSiteId
				  ,[Title]
				  ,D.[Description]
				  ,case when @MakePageStatusAsDraft=1 then dbo.GetActiveStatus()  else [PageStatus]  end
				  ,case when @MakePageStatusAsDraft=1 then  2  else case when WorkflowState =1 then WorkflowState else 2 end end
				  ,case when @MakePageStatusAsDraft=1 then 0 else 1 end
				  ,[PublishDate]
				  --,[DisplayOrder]
				  ,[FriendlyName]
				  ,dbo.GetEmptyGUID()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,[ArchivedDate]
				  ,[StatusChangedDate]
				  ,@ModifiedBy
				  ,[EnableOutputCache]
				  ,[OutputCacheProfileName]
				  ,[ExcludeFromSearch]
				  ,[ExcludeFromExternalSearch]
				  ,[IsDefault]
				  ,[IsTracked]
				  ,[HasForms]
				  ,[TextContentCounter]
				  ,PageDefinitionId
				  ,D.[CustomAttributes]
			      
			  FROM [dbo].[PageDefinition] D
			Where D.SiteId =@SiteId and PageDefinitionId in (select PageDefinitionId  from PageMapNodePageDef PMD inner join PageMapNode PM on PMD.PageMapNodeId = PM.SourcePageMapNodeId where PM.SiteId =@MicroSiteId)

			insert into PageMapNodePageDef
			select distinct  c.PageMapNodeId ,a.PageDefinitionId, b.DisplayOrder from [PageDefinition] a , PageMapNodePageDef b  , PageMapNode c
			where a.SiteId  = @MicroSiteId and a.SourcePageDefinitionId = b.PageDefinitionId 
			and c.SourcePageMapNodeId = b.PageMapNodeId and 
			c.SiteId = @MicroSiteId

			UPDATE PN SET PN.TargetId=T.PageDefinitionId
			FROM PageMapNode PN
			INNER JOIN PageDefinition T ON PN.TargetId = T.SourcePageDefinitionId
			Where PN.Target='1' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId

		
			insert into #tempIds SELECT PS.Id, NEWID() FROM  [dbo].[PageDefinitionSegment] PS INNER JOIN PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
			 
			 INSERT INTO [dbo].[PageDefinitionSegment]
						   ([Id]
						   ,[PageDefinitionId]
						   ,[DeviceId]
						   ,[AudienceSegmentId]
						   ,[UnmanagedXml]
						   ,[ZonesXml])
						   SELECT T.NewId 
							  ,P.PageDefinitionId 
							  ,[DeviceId]
							  ,[AudienceSegmentId]
							  ,[UnmanagedXml]
							  ,[ZonesXml]
						  FROM [dbo].[PageDefinitionSegment] PS 
						  inner join #tempIds T on PS.Id= T.OldId 
						  inner join PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
						  Where P.SiteId = @MicroSiteId
						  
			INSERT INTO [PageDefinitionContainer]
				(
					[Id]
					,PageDefinitionSegmentId 
					,[PageDefinitionId]
					,[ContainerId]
					,[ContentId]
					,[InWFContentId]
					,[ContentTypeId]
					,[IsModified]
					,[IsTracked]
					,[Visible]
					,[InWFVisible]
					,DisplayOrder 
					,InWFDisplayOrder 
					,[CustomAttributes]
					,[ContainerName])
				SELECT
						NewId() 
						,T.NewId 
					,P.PageDefinitionId
					,ContainerId
					,ContentId
					,InWFContentId
					,ContentTypeId
					,IsModified
					,PC.IsTracked
					,Visible
					,InWFVisible
					,PC.DisplayOrder 
					,PC.InWFDisplayOrder 
					,PC.CustomAttributes
					,ContainerName
				FROM PageDefinitionContainer PC
				inner join #tempIds T on PC.PageDefinitionSegmentId= T.OldId
				inner join PageDefinition P ON PC.PageDefinitionId=P.SourcePageDefinitionId 
				Where P.SiteId = @MicroSiteId
						  
			truncate table #tempIds

			INSERT INTO [dbo].[PageDetails]
					   ([PageId]
					   ,[PageDetailXML])
			  SELECT 
				 P.PageDefinitionId
					   ,[PageDetailXML]
			  FROM [dbo].[PageDetails] PD
			INNER JOIN PageDefinition P ON P.SourcePageDefinitionId = PD.PageId
			 Where P.SiteId = @MicroSiteId

			 INSERT INTO [dbo].[USSecurityLevelObject]
					   ([ObjectId]
					   ,[ObjectTypeId]
					   ,[SecurityLevelId])
			  SELECT P.PageDefinitionId
					   ,ObjectTypeId
					   ,SecurityLevelId
			FROM [USSecurityLevelObject] SO
			INNER JOIN PageDefinition P ON SO.ObjectId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageScript]
					   ([PageDefinitionId]
					   ,[ScriptId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[ScriptId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageScript] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageStyle]
					   ([PageDefinitionId]
					   ,[StyleId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[StyleId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageStyle] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[COTaxonomyObject]
					   ([Id]
					   ,[TaxonomyId]
					   ,[ObjectId]
					   ,[ObjectTypeId]
					   ,[SortOrder]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate])
			 SELECT NEWID()
				  ,[TaxonomyId]
				  ,P.PageDefinitionId
				  ,[ObjectTypeId]
				  ,[SortOrder]
				  ,[Status]
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
			  FROM [dbo].[COTaxonomyObject] OT
			INNER JOIN PageDefinition P ON OT.ObjectId= P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
	
	IF @MakePageStatusAsDraft = 0
	BEGIN
		INSERT INTO PageObjectReferences (Id, PageId, ObjectId, ObjectType, ObjectSiteId)
			SELECT	NEWID(), PD1.PageDefinitionId, POR.ObjectId, POR.ObjectType, POR.ObjectSiteId
			FROM	PageDefinition AS PD1
					INNER JOIN PageDefinition AS PD2 ON PD2.PageDefinitionId = PD1.SourcePageDefinitionId
					INNER JOIN PageObjectReferences AS POR ON POR.PageId = PD2.PageDefinitionId
			WHERE	PD1.SiteId = @MicroSiteId
	END
END
GO
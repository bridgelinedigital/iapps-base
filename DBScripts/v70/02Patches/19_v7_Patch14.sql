PRINT 'Add ScheduledPublishDate column in BLPost'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'BLPost' AND COLUMN_NAME = 'ScheduledPublishDate')
BEGIN	
	ALTER TABLE BLPost ADD ScheduledPublishDate datetime NULL
END
GO
PRINT 'Add UseLocalTimeZone column in BLPost'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'BLPost' AND COLUMN_NAME = 'UseLocalTimeZone')
BEGIN	
	ALTER TABLE BLPost ADD UseLocalTimeZone bit NULL
END
GO
PRINT 'Add ScheduledTimeZone column in BLPost'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'BLPost' AND COLUMN_NAME = 'ScheduledTimeZone')
BEGIN	
	ALTER TABLE BLPost ADD ScheduledTimeZone nvarchar(100) NULL
END
GO
PRINT 'Add IsScheduled column in BLPost'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'BLPost' AND COLUMN_NAME = 'IsScheduled')
BEGIN	
	ALTER TABLE BLPost ADD IsScheduled bit NULL
END
GO
PRINT 'Creating view vwContent'
GO
IF(OBJECT_ID('vwContent') IS NOT NULL)
	DROP VIEW vwContent
GO	
CREATE VIEW [dbo].[vwContent]
AS
SELECT C.Id,
	C.ApplicationId AS SiteId,
	C.Title,
	C.Description,
	C.ParentId,
	C.SourceContentId,
	C.XmlString,
	C.Text,
	C.TemplateId,
	CASE WHEN C.ObjectTypeId = 13 THEN X.Name ELSE 'Free Form' END AS TemplateName,
	CASE WHEN C.ObjectTypeId = 13 THEN 0 ELSE 1 END IsFreeForm,
	C.CreatedBy,
	C.CreatedDate,
	C.ModifiedBy,
	C.ModifiedDate,
	C.Status,
	C.OrderNo,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName,
	CS.VirtualPath AS FolderPath
FROM [dbo].[COContent] C
	LEFT JOIN COContentStructure CS ON C.ParentId = CS.Id
	LEFT JOIN COXMLForm X ON C.TemplateId = X.Id
	LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
WHERE C.Status != 3
	AND C.ObjectTypeId IN (7, 13)
GO
PRINT 'Creating view vwPost'
GO
IF(OBJECT_ID('vwPost') IS NOT NULL)
	DROP VIEW vwPost
GO	
CREATE VIEW [dbo].[vwPost]
AS
SELECT P.Id,
	P.Title,
	P.FriendlyName,
	P.Status,
	P.IsSticky,
	P.PostDate,
	P.IsScheduled,
	P.ScheduledPublishDate,
	P.UseLocalTimeZone,
	P.ScheduledTimeZone,
	P.AuthorName,
	P.AuthorEmail,
	P.INWFFriendlyName,
	P.ApplicationId AS SiteId,
	P.CreatedBy,
	P.CreatedDate,
	P.ModifiedBy,
	P.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName,
	P.BlogId,
	CASE WHEN P.IsScheduled = 1 AND P.ScheduledPublishDate <= GETUTCDATE() THEN 1 ELSE 0 END AS IsPending,
	MONTH(PostDate) AS PostMonth,
	YEAR(PostDate) AS PostYear,
	LOWER(FORMATMESSAGE('%s/%s/%s/%s/%s', 
			PM.CompleteFriendlyUrl, 
			PD.FriendlyName, 
			FORMAT(PostDate,'yyyy'), 
			FORMAT(PostDate,'MM'), 
			P.FriendlyName)
		) AS PostUrl
FROM [dbo].[BLPost] P
	JOIN BLBlog B ON B.Id = P.BlogId
	JOIN PageDefinition PD ON B.BlogListPageId = PD.PageDefinitionId
	JOIN PageMapNodePageDef PMD ON PD.PageDefinitionId = PMD.PageDefinitionId
	JOIN PageMapNode PM ON PM.PageMapNodeId = PMD.PageMapNodeId
	LEFT JOIN VW_UserFullName FN on FN.UserId = P.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = P.ModifiedBy
WHERE P.Status != 3
GO
PRINT 'Alter PostDto_Get'
GO
IF (OBJECT_ID('PostDto_Get') IS NOT NULL)
	DROP PROCEDURE PostDto_Get
GO
CREATE PROCEDURE [dbo].[PostDto_Get] 
(
	@Id						uniqueidentifier = NULL,
	@Ids					nvarchar(max) = NULL,
	@SiteId					uniqueidentifier = NULL,
	@PostMonth				varchar(20) = NULL,
	@PostYear				varchar(20) = NULL,
	@FolderId				uniqueIdentifier = NULL,
	@BlogId					uniqueIdentifier = NULL,
	@CategoryId				uniqueIdentifier = NULL,
	@Label					nvarchar(1024) = NULL,
	@Author					nvarchar(1024) = NULL,
	@IsSticky				bit = NULL,
	@IncludeContentDetails	bit = NULL,
	@IncludeVersion			bit = NULL,
	@VersionId				uniqueidentifier = NULL,
	@IgnoreSite				bit = NULL,
	@ApprovedCommentsOnly	bit = NULL,
	@PageNumber				int = NULL,
	@PageSize				int = NULL,
	@Status					int = NULL,
	@MaxRecords				int = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@Query					nvarchar(max) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(500), @DeleteStatus int
	SET @DeleteStatus = dbo.GetDeleteStatus()
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @ApprovedCommentsOnly IS NULL 
		SET @ApprovedCommentsOnly = 0

	IF @Status = 0 SET @Status = NULL

	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbQueryIds TABLE (Id uniqueidentifier primary key, RowNumber int)

	IF @Query IS NOT NULL
	BEGIN
		INSERT INTO @tbQueryIds
		EXEC sp_executesql @Query
	END

	INSERT INTO @tbIds
	SELECT Id,  ROW_NUMBER() OVER (ORDER BY
		CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
		CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
		CASE WHEN @SortClause = 'CreatedDate ASC' THEN CreatedDate END ASC,
		CASE WHEN @SortClause = 'CreatedDate DESC' THEN CreatedDate END DESC,
		CASE WHEN @SortClause IS NULL THEN CreatedDate END DESC	
	) AS RowNumber
	FROM BLPost P
	WHERE P.Status != @DeleteStatus AND
		(@Id IS NULL OR P.Id = @Id) AND
		(@Status IS NULL OR P.Status = @Status) AND
		(@BlogId IS NULL OR P.BlogId = @BlogId) AND
		(@SiteId IS NULL OR @IgnoreSite = 1 OR P.ApplicationId = @SiteId) AND
		(@PostYear IS NULL OR YEAR(PostDate) = @PostYear) AND 
		(@PostMonth IS NULL OR MONTH(PostDate) = @PostMonth) AND 
		(@FolderId IS NULL OR HSId = @FolderId) AND 
		(@IsSticky IS NULL OR IsSticky = @IsSticky) AND
		(@Label IS NULL OR Labels LIKE '%' + @Label + '%') AND
		(@Author IS NULL OR AuthorEmail like @Author OR AuthorName like @Author) AND
		(@Query IS NULL OR P.Id IN (SELECT Id FROM @tbQueryIds))

	IF @Query IS NOT NULL
	BEGIN
		UPDATE T
		SET T.RowNumber = Q.RowNumber
		FROM @tbIds T JOIN @tbQueryIds Q ON T.Id = Q.Id
	END
	
	IF @Ids IS NOT NULL
	BEGIN
		DELETE FROM @tbIds 
		WHERE Id NOT IN (SELECT Items FROM dbo.SplitGUID(@Ids, ','))
	END

	IF @CategoryId IS NOT NULL
	BEGIN
		DELETE FROM @tbIds 
		WHERE Id NOT IN (SELECT ObjectId FROM COTaxonomyObject WHERE TaxonomyId = @CategoryId AND ObjectTypeId = 40)
	END
		
	DECLARE @tbPagedResults TABLE(Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
			COUNT(T.Id) OVER () AS TotalRecords,
			P.Id AS Id
		FROM BLPost P
			JOIN @tbIds T ON T.Id = P.Id
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)

	SELECT P.Id,			
		P.ApplicationId,
		P.Title,
		P.ShortDescription,
		P.AuthorName,
		P.AuthorEmail,
		P.Labels,
		P.AllowComments,
		P.PostDate,
		P.IsScheduled,
		P.ScheduledPublishDate,
		P.UseLocalTimeZone,
		P.ScheduledTimeZone,
		P.Location,
		P.Status,
		P.IsSticky,
		P.CreatedBy,
		P.CreatedDate,
		P.ModifiedBy,
		P.ModifiedDate,
		P.FriendlyName,
		P.PostContentId,
		P.ContentId,
		P.ImageUrl,
		P.TitleTag,
		P.H1Tag,
		P.KeywordMetaData,
		P.DescriptiveMetaData,
		P.OtherMetaData,
		P.WorkflowState,
		P.PublishDate,
		P.PublishCount,
		P.INWFFriendlyName,
		P.HSId AS MonthId,
		B.BlogListPageId,
		B.Id AS BlogId,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName,
		T.RowNumber,
		T.TotalRecords
	FROM BLPost P
		JOIN @tbPagedResults T ON T.Id = P.Id
		JOIN BLBlog B ON P.BlogId = B.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = P.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = P.ModifiedBy
	ORDER BY RowNumber ASC

	SELECT C.Id,
		C.ApplicationId,
		C.Title,
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		CASE WHEN @IncludeContentDetails = 1 THEN C.XMLString ELSE NULL END AS XMLString,
		C.ParentId,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName
	FROM COContent C
		JOIN BLPost P ON C.Id = P.PostContentId
		JOIN @tbPagedResults T ON P.Id = T.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = C.ModifiedBy

	SELECT DISTINCT B.*,
		LOWER(PM.CompleteFriendlyUrl + '/' + PD.FriendlyName) AS BlogUrl,
		CU.UserFullName CreatedByFullName,
		MU.UserFullName ModifiedByFullName
	FROM BLBlog B
		JOIN BLPost P ON B.Id = P.BlogId
		JOIN PageDefinition PD ON B.BlogListPageId = PD.PageDefinitionId
		JOIN PageMapNodePageDef PMD ON PD.PageDefinitionId = PMD.PageDefinitionId
		JOIN PageMapNode PM ON PM.PageMapNodeId = PMD.PageMapNodeId
		JOIN @tbPagedResults T ON P.Id = T.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = B.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = B.ModifiedBy

	SELECT C.Id,
		C.Title,
		T.Id AS PostId
	FROM COTaxonomy C
		JOIN COTaxonomyObject O ON C.Id = O.TaxonomyId
		JOIN @tbPagedResults T ON T.Id = O.ObjectId

	SELECT Count(*) AS NoOfComments,
		T.Id AS PostId
	FROM BLComments C
		JOIN @tbPagedResults T ON C.ObjectId = T.Id
	WHERE (@ApprovedCommentsOnly = 0 OR C.Status = 4)
	AND C.Status <> dbo.GetDeleteStatus() 
	GROUP BY T.Id

	IF @IncludeVersion = 1
	BEGIN
		DECLARE @tbVersionIds TABLE(Id uniqueidentifier)
		IF @VersionId IS NULL
		BEGIN
			;WITH CTEVersion AS (
				SELECT ROW_NUMBER() OVER (PARTITION BY P.Id ORDER BY RevisionNumber DESC, VersionNumber DESC) AS VerRank,
					V.Id FROM VEVersion V
					JOIN BLPost P On P.Id = V.ObjectId
					JOIN @tbPagedResults T ON P.Id = T.Id
			)
			INSERT INTO @tbVersionIds
			SELECT Id FROM CTEVersion WHERE VerRank = 1
		END
		ELSE
		BEGIN
			INSERT INTO @tbVersionIds VALUES (@VersionId)
		END

		SELECT V.Id AS VersionId,
			V.ObjectId AS PostId,
			V.XMLString
		FROM VEVersion V
			JOIN @tbVersionIds T On T.Id = V.Id

		SELECT 
			V.PostVersionId AS VersionId,
			C.ContentId,
			C.XMLString
		FROM VEPostContentVersion V
			JOIN @tbVersionIds T On T.Id = V.PostVersionId
			JOIN VEContentVersion C ON C.Id = V.ContentVersionId
	END
END
GO
PRINT 'Alter Post_GetPost'
GO
IF (OBJECT_ID('Post_GetPost') IS NOT NULL)
	DROP PROCEDURE Post_GetPost
GO
CREATE PROCEDURE [dbo].[Post_GetPost]
(  
 @ApplicationId  uniqueIdentifier,    
 @Id     uniqueIdentifier = null,    
 @PostMonth   varchar(20) = null,  
 @PostYear   varchar(20) = null,  
 @PageSize   int = null,  
 @PageNumber   int = null,  
 @FolderId   uniqueIdentifier = null,  
 @BlogId    uniqueIdentifier = null,  
 @Status    int = null,  
 @CategoryId   uniqueIdentifier = null,  
 @Label    nvarchar(1024) = null,  
 @Author    nvarchar(1024) = null,  
 @IncludeVersion  bit = null,  
 @VersionId   uniqueidentifier=null,  
 @IgnoreSite   bit = null,  
 @ShowOnlyApprovedComments bit = null  
)  
AS  
BEGIN  
 DECLARE @IsSticky bit  
 IF @Status = 5  
  SET @IsSticky = 1  
  
 IF @ShowOnlyApprovedComments IS NULL  
  SET @ShowOnlyApprovedComments = 0  
  
  
 DECLARE @tbPostIds TABLE(RowNo int, TotalRows int, Id uniqueidentifier, BlogId uniqueidentifier, PostContentId uniqueidentifier)  
 ;WITH PostIds AS(  
  SELECT ROW_NUMBER() over (order by IsSticky desc, PostDate desc, Title) AS RowNo,   
   COUNT(Id) over (PARTITION BY NULL) AS TotalRows,Id, BlogId, PostContentId  
  FROM BLPost   
  WHERE (@Id IS NULL OR Id = @Id) AND  
   (Status != dbo.GetDeleteStatus()) AND  
   (@ApplicationId IS NULL OR @IgnoreSite = 1 OR @ApplicationId = ApplicationId) AND  
   (@Status IS NULL OR Status = @Status) AND  
   (@PostYear IS NULL OR YEAR(PostDate) = @PostYear) AND   
   (@PostMonth IS NULL OR MONTH(PostDate) = @PostMonth) AND   
   (@FolderId IS NULL OR HSId = @FolderId) AND   
   (@BlogId IS NULL OR BlogId = @BlogId) AND  
   (@IsSticky IS NULL OR IsSticky = @IsSticky) AND  
   (@Label IS NULL OR Labels LIKE '%' + @Label + '%') AND  
   (@Author IS NULL OR AuthorEmail like @Author OR AuthorName like @Author) AND  
   (@CategoryId IS NULL OR Id IN (SELECT ObjectId FROM COTaxonomyObject WHERE TaxonomyId = @CategoryId AND ObjectTypeId = 40))  
 )  
   
 INSERT INTO @tbPostIds  
 SELECT RowNo, TotalRows, Id, BlogId, PostContentId FROM PostIds  
 WHERE (@PageSize IS NULL OR @PageNumber IS NULL OR   
  RowNo BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize))  
   
 SELECT A.Id,     
  A.ApplicationId,  
  A.Title,  
  A.Description,  
  A.ShortDescription,  
  A.AuthorName,  
  A.AuthorEmail,  
  A.Labels,  
  A.AllowComments,  
  A.PostDate,  
  A.IsScheduled,
  A.ScheduledPublishDate,  
  A.UseLocalTimeZone,
  A.ScheduledTimeZone,
  A.Location,  
  A.Status,  
  A.IsSticky,  
  A.CreatedBy,  
  A.CreatedDate,  
  A.ModifiedBy,  
  A.ModifiedDate,  
  A.FriendlyName,  
  A.PostContentId,  
  A.ContentId,  
  A.ImageUrl,  
  A.TitleTag,  
  A.H1Tag,  
  A.KeywordMetaData,  
  A.DescriptiveMetaData,  
  A.OtherMetaData,  
  B.BlogListPageId,  
  B.Id AS BlogId,  
  A.WorkflowState,  
  A.PublishDate,  
  A.PublishCount,  
  A.INWFFriendlyName,  
  A.HSId AS MonthId  
 FROM BLPost A   
  JOIN @tbPostIds T ON A.Id = T.Id  
  JOIN BLBlog B ON A.BlogId = B.Id  
 ORDER BY T.RowNo  
  
 SELECT  C.Id,  
  C.ApplicationId,  
  C.Title,  
  C.Description,   
  C.Text,   
  C.URL,  
  C.URLType,   
  C.ObjectTypeId,   
  C.CreatedDate,  
  C.CreatedBy,  
  C.ModifiedBy,  
  C.ModifiedDate,   
  C.Status,  
  C.RestoreDate,   
  C.BackupDate,   
  C.StatusChangedDate,  
  C.PublishDate,   
  C.ArchiveDate,   
  C.XMLString,  
  C.BinaryObject,   
  C.Keywords,  
  FN.UserFullName CreatedByFullName,  
  MN.UserFullName ModifiedByFullName,  
  C.OrderNo, C.SourceContentId,  
  C.ParentId,  
  sDir.VirtualPath As FolderPath  
 FROM COContent C  
  INNER JOIN @tbPostIds T ON C.Id = T.PostContentId  
  INNER JOIN COContentStructure sDir on SDir.Id = C.ParentId  
  LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy  
  LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy  
    
 SELECT Top 1 TotalRows FROM @tbPostIds  
  
 SELECT A.Id,  
  ApplicationId,  
  Title,  
  Description,  
  BlogNodeType,  
  MenuId,  
  ContentDefinitionId,  
  ShowPostDate,  
  DisplayAuthorName,  
  EmailBlogOwner,  
  ApproveComments,  
  RequiresUserInfo,  
  Status,  
  CreatedDate,  
  CreatedBy,  
  ModifiedBy,  
  FN.UserFullName CreatedByFullName,  
  MN.UserFullName ModifiedByFullName,  
  ModifiedDate,  
  BlogListPageId,  
  BlogDetailPageId,  
  ContentDefinitionId--,  
  --CASE WHEN SUBSTRING(V.Url,1,1) = '/' THEN SUBSTRING(V.Url,2,LEN(V.Url)-1) ELSE V.Url END AS BlogUrl     
 FROM BLBlog A   
  INNER JOIN @tbPostIds T ON T.BlogId = A.Id  
		--LEFT JOIN vwBlogUrl V ON V.Id = A.Id
  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
  LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
   
 SELECT T.Id,  
  I.Title,  
  C.TaxonomyId  
 FROM COTaxonomyObject C  
  JOIN @tbPostIds T ON T.Id = C.ObjectId  
  JOIN COTaxonomy I ON I.Id = C.TaxonomyId  
  
 IF (@ShowOnlyApprovedComments = 1)  
 BEGIN  
  SELECT Count(*) AS NoOfComments,  
   T.Id  
  FROM BLComments C  
   JOIN @tbPostIds T ON C.ObjectId = T.Id  
  WHERE C.Status = 4 --For approved comments  
  GROUP BY T.Id  
 END  
 ELSE  
 BEGIN  
  SELECT Count(*) AS NoOfComments,  
   T.Id  
  FROM BLComments C  
   JOIN @tbPostIds T ON C.ObjectId = T.Id  
  GROUP BY T.Id  
 END  
  
 IF @IncludeVersion = 1  
 BEGIN  
  DECLARE @tbVersionIds TABLE(Id uniqueidentifier)  
  IF @VersionId IS NULL  
  BEGIN  
   IF(@Id IS NOT NULL)  
   BEGIN  
    SELECT TOP (1) @VersionId = Id  from VEVersion A  
    WHERE ObjectId=@Id   
    ORDER BY RevisionNumber DESC, VersionNumber DESC  
  
     
    INSERT INTO @tbVersionIds VALUES (@VersionId)  
   END  
   ELSE  
   BEGIN  
    ;WITH CTEVersion AS (  
     SELECT ROW_NUMBER() OVER (PARTITION BY T.Id ORDER BY RevisionNumber DESC, VersionNumber DESC) AS VerRank,  
      V.Id FROM VEVersion V  
      JOIN BLPost T On T.Id = V.ObjectId  
    )  
  
    INSERT INTO @tbVersionIds  
    SELECT Id FROM CTEVersion WHERE VerRank = 1  
   END  
  END  
  ELSE  
  BEGIN  
   INSERT INTO @tbVersionIds VALUES (@VersionId)  
  END  
  
  SELECT V.Id AS VersionId,  
   V.ObjectId AS PostId,  
   V.XMLString  
  FROM VEVersion V  
   JOIN @tbVersionIds T On T.Id = V.Id  
  
  SELECT   
   V.PostVersionId AS VersionId,  
   C.ContentId,  
   C.XMLString  
  FROM VEPostContentVersion V  
   JOIN @tbVersionIds T On T.Id = V.PostVersionId  
   JOIN VEContentVersion C ON C.Id = V.ContentVersionId  
 END  
END  
GO
PRINT 'Alter Post_Save'
GO
IF (OBJECT_ID('Post_Save') IS NOT NULL)
	DROP PROCEDURE Post_Save
GO
CREATE PROCEDURE [dbo].[Post_Save]   
(  
	@Id						uniqueidentifier = null OUT ,  
	@BlogId					uniqueidentifier,   
	@ApplicationId			uniqueidentifier,   
	@Title					nvarchar(256) = null,  
	@Description			ntext = null,  
	@ShortDescription		nvarchar(2000) = null,  
	@AuthorName				nvarchar(256) = null,  
	@AuthorEmail			nvarchar(256) = null,  
	@AllowComments			bit = true,  
	@PostDate				datetime = null,  
	@IsScheduled			bit = null,
	@ScheduledPublishDate	datetime = null,  
	@UseLocalTimeZone		bit = null,  
	@ScheduledTimeZone		nvarchar(100) = null,  
	@Location				nvarchar(256) = null,  
	@Labels					nvarchar(Max) = null,  
	@ModifiedBy				uniqueidentifier,  
	@IsSticky				bit = null, 
	@HSId					uniqueidentifier,  
	@FriendlyName			nvarchar(256) = null,
	@PostContentId			uniqueidentifier = null,
	@ContentId				uniqueidentifier = null,
	@Imageurl				nvarchar(500) = null,
	@TitleTag				nvarchar(2000) = null,  
	@H1Tag					nvarchar(2000) = null,
	@KeywordMetaData		nvarchar(2000) = null,
	@DescriptiveMetaData	nvarchar(2000) = null,
	@OtherMetaData			nvarchar(2000) = null,
	@PublishDate			datetime = null,
	@PublishCount			int = null,
	@WorkflowState			int = null,
	@Status					int = null
)  
AS  
BEGIN  
	DECLARE  
	 @NewId  uniqueidentifier,  
	 @RowCount  INT,  
	 @Stmt   VARCHAR(25),  
	 @Now    datetime,  
	 @Error   int ,  
	 @MonthId uniqueidentifier,  
	 @return_value int --,
	 --@Status int 
  
	 /* if ID specified, ensure exists */  
	 IF (@Id IS NOT NULL)  
	  IF (NOT EXISTS(SELECT 1 FROM   BLPost WHERE  Id = @Id AND Status <> dbo.GetDeleteStatus()))  
	  BEGIN  
	   set @Stmt = convert(varchar(36),@Id)  
	   RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )  
	   RETURN dbo.GetBusinessRuleErrorCode()  
	  END    

	  IF @Status = 5
	  BEGIN
		SET @IsSticky = 1
		SET @Status = null 
		/*
		Sankar: If the @status = sticky then 
		FOR INSERT : There is no provision now to make the post as sticky at the time of creation. so in the insert we are making it to default to 1(draft).Even if we send a @Status = 5 then it will create DRAFT STICKY POST.
		FOR update : if the status is sent as sticky, then we need to maintain the same status for the post by updating the sticky bit to 1. So here I am making the @status = null
		*/
	  END
	   
	 SET @Now = getutcdate()  
	  
	 IF (@Id IS NULL)   -- new insert  
	 BEGIN  
	  SET @Stmt = 'Create Post'  
	  SELECT @NewId =newid()  
	  
	  INSERT INTO BLPost  WITH (ROWLOCK)(  
		 Id,  
		ApplicationId,  
		Title,  
		BlogId,  
		Description,  
		ShortDescription,  
		AuthorName,  
		AuthorEmail,  
		Labels,  
		AllowComments,  
		PostDate,  
		IsScheduled,
		ScheduledPublishDate,  
		UseLocalTimeZone,
		ScheduledTimeZone,
		Location,  
		Status,  
		IsSticky,
		CreatedBy,  
		CreatedDate,  
		HSId,  
		FriendlyName,
		PostContentId,
		ContentId,
		ImageUrl,
		TitleTag,
		H1Tag,
		KeywordMetaData,
		DescriptiveMetaData,
		OtherMetaData
	  )  
	  VALUES  
	   (  
			@NewId,  
			@ApplicationId,   
			@Title,  
			@BlogId,  
			@Description,  
			@ShortDescription,  
			@AuthorName,  
			@AuthorEmail,  
			@Labels,  
			@AllowComments,  
			@PostDate,  
			@IsScheduled,
			@ScheduledPublishDate,  
			@UseLocalTimeZone,  
			@ScheduledTimeZone,  
			@Location,  
			ISNULL(@Status,1), 
			@IsSticky, 
			@ModifiedBy,  
			@Now,
			@HSId ,
			@FriendlyName,
			@PostContentId,
			@ContentId,
			@ImageUrl,
			@TitleTag,
			@H1Tag,
			@KeywordMetaData,
			@DescriptiveMetaData,
			@OtherMetaData
	   )  
	  SELECT @Error = @@ERROR  
	   IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()   
	  END  
	  
	  SELECT @Id = @NewId  
	 END  
	 ELSE   -- Update  
	 BEGIN  
	  SET @Stmt = 'Modify Post'  
	  UPDATE BLPost WITH (ROWLOCK)  
	   SET   
		 Id     = @Id,  
		ApplicationId  = @ApplicationId,  
		Title    = @Title,  
		BlogId    = @BlogId,  
		Description   = @Description,  
		ShortDescription = @ShortDescription,  
		AuthorName   = @AuthorName,  
		AuthorEmail   = @AuthorEmail,  
		Labels    = @Labels,  
		AllowComments  = @AllowComments,  
		PostDate   = @PostDate,  
		IsScheduled = @IsScheduled,
		ScheduledPublishDate   = @ScheduledPublishDate,  
		UseLocalTimeZone	= @UseLocalTimeZone,  
		ScheduledTimeZone	= @ScheduledTimeZone,  
		Location   = @Location ,  
		ModifiedBy   = @ModifiedBy,  
		ModifiedDate  = GETUTCDATE(),  
		HSId = @HSId,  
		FriendlyName=@FriendlyName,
		PostContentId = @PostContentId,
		ContentId = @ContentId,
		ImageUrl = @Imageurl,
		TitleTag = @TitleTag,
		H1Tag = @H1Tag,
		IsSticky = @IsSticky,
		KeywordMetaData = @KeywordMetaData,
		DescriptiveMetaData = @DescriptiveMetaData,
		OtherMetaData = @OtherMetaData,
		PublishDate = @PublishDate,
		PublishCount = @PublishCount,
		WorkflowState = @WorkflowState,
		Status = ISNULL(@Status,Status)
	  WHERE Id = @Id  
	    
	  SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT  
	  
	  IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()  
	  END  
	  
	  IF @RowCount = 0  
	  BEGIN  
	   --concurrency error  
	   RAISERROR('DBCONCURRENCY',16,1)  
	   RETURN dbo.GetDataConcurrencyErrorCode()  
	  END   
	 END    
   
	EXEC PageMapBase_UpdateLastModification @ApplicationId
END
GO
PRINT 'Alter ProductImageListDto_Get'
GO
IF (OBJECT_ID('ProductImageListDto_Get') IS NOT NULL)
	DROP PROCEDURE ProductImageListDto_Get
GO
CREATE PROCEDURE [dbo].[ProductImageListDto_Get]
(
	@SearchProductId	uniqueidentifier,
	@SiteId		uniqueidentifier
)
AS
BEGIN
	DECLARE @tbSkuImageIds TABLE(Id uniqueidentifier)

	INSERT INTO @tbSkuImageIds
	SELECT ImageId FROM CLObjectImage 
	WHERE ObjectId IN (SELECT Id FROM PRProductSKU WHERE ProductId = @SearchProductId)

	;WITH CTE AS 
	(
		SELECT I.Id FROM CLImage I
			JOIN CLObjectImage OI ON I.Id = OI.ImageId
		WHERE OI.ObjectId = @SearchProductId
			AND I.ParentImage IS NULL
			AND I.Id NOT IN (SELECT Id FROM @tbSkuImageIds)

		UNION ALL

		SELECT I.Id FROM CLImage I
			JOIN CLObjectImage OI ON I.Id = OI.ImageId
		WHERE I.ParentImage IS NULL
			AND OI.ObjectId IN (
				SELECT Id FROM vwSKU 
				WHERE ProductId = @SearchProductId 
					AND SiteId = @SiteId AND IsActive = 1 AND IsOnline = 1
			)
	)

	SELECT * FROM vwProductImage I
		JOIN CTE C ON I.Id = C.Id OR I.ParentImage = C.Id
	WHERE SiteId = @SiteId

END
GO

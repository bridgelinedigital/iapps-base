PRINT 'Add Site Settings'
DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int
SET @SettingGroupId = NULL
SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Translation Settings'
IF @SettingGroupId IS NULL
BEGIN
	SET @SettingGroupId = 50
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingGroup WHERE IsSystem = 1
	INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem) VALUES (@SettingGroupId, 'Translation Settings', @Sequence, 1)
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Lingotek.ProjectId')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Lingotek.ProjectId', 'Id of project in Lingotek',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '5ebe6c83-d144-4c74-b6d2-ed1e8294612c' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Lingotek.AccessToken')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Lingotek.AccessToken', 'AccessToken for Lingotek',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '3eba5da6-6c30-4f18-90a8-5b2ca1313a69' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Translation.NotificationWaitInMinutes')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Translation.NotificationWaitInMinutes', 'Interval to poll lingotek instead of notification',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '30' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Translation.DisableProductImage')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Translation.DisableProductImage', 'Disable product image translation',  @SettingGroupId, @Sequence, 'Checkbox', 1, 1)
END
SET @SettingGroupId = NULL
SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Plugins'
IF @SettingGroupId IS NULL
BEGIN
	SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup WHERE Id != 50
	IF EXISTS (SELECT 1 FROM STSettingGroup WHERE Id = @SettingGroupId)
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingGroup WHERE IsSystem = 1
	INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem) VALUES (@SettingGroupId, 'Plugins', @Sequence, 1)
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'reCAPTCHA.SiteKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('reCAPTCHA.SiteKey', 'Google reCAPTCHA site key',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '6LcJUhkUAAAAABJ5-zx23XefdQex6eFnvsLVZfdm' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'reCAPTCHA.SecretKey')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('reCAPTCHA.SecretKey', 'Google reCAPTCHA secret key',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '6LcJUhkUAAAAAMT6QELapTUglraBEGBG1wIp98cJ' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'reCAPTCHA.LanguageCode')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('reCAPTCHA.LanguageCode', 'Google reCAPTCHA language',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
END
SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Cart & Order'
IF @SettingGroupId IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'CartURL.PageId')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('CartURL.PageId', 'Cart page Id',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	END
	ELSE
	BEGIN
		UPDATE STSettingType SET FriendlyName = 'Cart page Id',
			SettingGroupId = @SettingGroupId,
			ControlType = 'Textbox',
			IsVisible = 1,
			IsEnabled = 1
		WHERE Name = 'CartURL.PageId'
	END

	IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'CheckoutURL.PageId')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('CheckoutURL.PageId', 'Checkout page Id',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	END
	ELSE
	BEGIN
		UPDATE STSettingType SET FriendlyName = 'Checkout page Id',
			SettingGroupId = @SettingGroupId,
			ControlType = 'Textbox',
			IsVisible = 1,
			IsEnabled = 1
		WHERE Name = 'CheckoutURL.PageId'
	END

	IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'MyAccountURL.PageId')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('MyAccountURL.PageId', 'MyAccount page Id',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	END
	ELSE
	BEGIN
		UPDATE STSettingType SET FriendlyName = 'MyAccount page Id',
			SettingGroupId = @SettingGroupId,
			ControlType = 'Textbox',
			IsVisible = 1,
			IsEnabled = 1
		WHERE Name = 'MyAccountURL.PageId'
	END

	IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'MyOrderURL.PageId')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('MyOrderURL.PageId', 'Order history page Id',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	END
	ELSE
	BEGIN
		UPDATE STSettingType SET FriendlyName = 'Order history page Id',
			SettingGroupId = @SettingGroupId,
			ControlType = 'Textbox',
			IsVisible = 1,
			IsEnabled = 1
		WHERE Name = 'MyOrderURL.PageId'
	END

	IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'ViewOrderLink.PageId')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('ViewOrderLink.PageId', 'Order detail page Id',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	END
	ELSE
	BEGIN
		UPDATE STSettingType SET FriendlyName = 'Order detail page Id',
			SettingGroupId = @SettingGroupId,
			ControlType = 'Textbox',
			IsVisible = 1,
			IsEnabled = 1
		WHERE Name = 'ViewOrderLink.PageId'
	END
END
GO
PRINT 'Update PageMapNode CompleteFriendlyUrl as static column'
GO
IF (OBJECT_ID('vwObjectUrls_Blogs') IS NOT NULL)
	DROP VIEW vwObjectUrls_Blogs	
GO
IF (OBJECT_ID('vwBlogUrl') IS NOT NULL)
	DROP VIEW vwBlogUrl	
GO
IF (OBJECT_ID('vwPostUrl') IS NOT NULL)
	DROP VIEW vwPostUrl	
GO
IF (OBJECT_ID('vwPageUrl') IS NOT NULL)
	DROP VIEW vwPageUrl	
GO
IF (OBJECT_ID('vwMenuUrl') IS NOT NULL)
	DROP VIEW vwMenuUrl	
GO
IF (OBJECT_ID('vwSimplePageUrl') IS NOT NULL)
	DROP VIEW vwSimplePageUrl	
GO
IF (OBJECT_ID('vwObjectUrls_Pages') IS NOT NULL)
	DROP VIEW vwObjectUrls_Pages	
GO
IF (OBJECT_ID('vwObjectUrls_Menus') IS NOT NULL)
	DROP VIEW vwObjectUrls_Menus	
GO
IF EXISTS(SELECT * FROM sys.columns WHERE is_computed = 1 AND object_id = OBJECT_ID('PageMapNode') AND name = 'CompleteFriendlyUrl')
	ALTER TABLE PageMapNode DROP COLUMN CompleteFriendlyUrl
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PageMapNode' AND COLUMN_NAME = 'CalculatedUrl')
	ALTER TABLE PageMapNode DROP COLUMN CalculatedUrl
GO
IF (OBJECT_ID('GetPageMapNodeUrl') IS NOT NULL)
	DROP FUNCTION GetPageMapNodeUrl
GO
ALTER TABLE PageMapNode ADD CalculatedUrl nvarchar(100)
GO
PRINT 'Creating Function GetPageMapNodeUrl'
GO
CREATE FUNCTION [dbo].[GetPageMapNodeUrl](@Id uniqueidentifier)  
RETURNS NVarchar(2048)
AS  
BEGIN    
	DECLARE @Url nvarchar(2048)
	SET @Url = ''
	
	SELECT @Url = P.CalculatedUrl + '/' + N.FriendlyUrl FROM
		PageMapNode N JOIN PageMapNode P ON N.ParentId = P.PageMapNodeId
	WHERE  N.PageMapNodeId = @Id AND
		NULLIF(N.LocationIdentifier, '') IS NULL
	
	RETURN LOWER(@Url)
END
GO
ALTER TABLE PageMapNode DROP COLUMN CalculatedUrl
GO
ALTER TABLE PageMapNode ADD CalculatedUrl AS ([dbo].[GetPageMapNodeUrl]([PageMapNodeId]))
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PageMapNode' AND COLUMN_NAME = 'CompleteFriendlyUrl')
	ALTER TABLE PageMapNode Add CompleteFriendlyUrl nvarchar(2048)
GO
UPDATE PageMapNode SET CompleteFriendlyUrl = CalculatedUrl
GO
PRINT 'Create view vwPageUrl'
GO
IF (OBJECT_ID('vwPageUrl') IS NOT NULL)
	DROP VIEW vwPageUrl	
GO
CREATE VIEW [dbo].[vwPageUrl] WITH SCHEMABINDING 
AS
SELECT P.PageDefinitionId AS Id, 
	LOWER(N.CompleteFriendlyUrl + '/' + P.FriendlyName) AS Url,
	LOWER(N.CompleteFriendlyUrl + '/' + V.FriendlyName) AS DraftUrl,
	P.SiteId
FROM dbo.PageMapNodePageDef PD
	JOIN dbo.PageDefinition P ON PD.PageDefinitionId = P.PageDefinitionId
	JOIN dbo.PageMapNode N ON N.PageMapNodeId = PD.PageMapNodeId
	LEFT JOIN dbo.VEPageVersion V ON V.PageDefinitionId = V.PageDefinitionId
GO
PRINT 'Create view vwPostUrl'
GO
IF (OBJECT_ID('vwPostUrl') IS NOT NULL)
	DROP VIEW vwPostUrl	
GO
CREATE VIEW [dbo].[vwPostUrl] WITH SCHEMABINDING 
AS
SELECT B.Id AS Id, P.Id AS PostId,
	U.Url + '/' + CAST(YEAR(P.PostDate) AS varchar(4)) +  '/' + RIGHT('0' + CAST(DATEPART(MM, P.PostDate) AS varchar(2)), 2) +  '/' + LOWER(P.FriendlyName) AS Url,
	U.DraftUrl + '/' + CAST(YEAR(P.PostDate) AS varchar(4)) +  '/' + RIGHT('0' + CAST(DATEPART(MM, P.PostDate) AS varchar(2)), 2) +  '/' + LOWER(P.INWFFriendlyName) AS DraftUrl,
	B.ApplicationId AS SiteId
FROM dbo.BLPost P 
	JOIN dbo.BLBlog B ON B.Id = P.BlogId
	JOIN [dbo].[vwPageUrl] U ON B.BlogListPageId = U.Id
GO
PRINT 'Create view vwBlogUrl'
GO
IF (OBJECT_ID('vwBlogUrl') IS NOT NULL)
	DROP VIEW vwBlogUrl	
GO
CREATE VIEW [dbo].[vwBlogUrl] WITH SCHEMABINDING 
AS
SELECT B.Id AS Id, 
	U.Url,
	U.DraftUrl,
	B.ApplicationId AS SiteId
FROM dbo.BLBlog B
	JOIN [dbo].[vwPageUrl] U ON B.BlogListPageId = U.Id
GO
PRINT 'Create view vwMenuUrl'
GO
IF (OBJECT_ID('vwMenuUrl') IS NOT NULL)
	DROP VIEW vwMenuUrl	
GO
CREATE VIEW [dbo].[vwMenuUrl] WITH SCHEMABINDING 
AS
SELECT PageMapNodeId AS Id, 
	LOWER(CompleteFriendlyUrl) AS Url,
	SiteId
FROM dbo.PageMapNode 
WHERE LftValue > 1
	AND (LocationIdentifier IS NULL OR LocationIdentifier = '' OR LocationIdentifier = 'Unassigned')
GO
PRINT 'Create view vwProductTypePathVariant'
GO
IF (OBJECT_ID('vwProductTypePathVariant') IS NOT NULL)
	DROP VIEW vwProductTypePathVariant	
GO

GO
PRINT 'Adding computed column for PRProductType'
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PRProductType' AND COLUMN_NAME = 'CompleteFriendlyUrl')
	ALTER TABLE PRProductType DROP COLUMN CompleteFriendlyUrl
GO
ALTER TABLE PRProductType ADD CompleteFriendlyUrl nvarchar(100)
GO
PRINT 'Creating Function GetProductTypeUrl'
GO
IF (OBJECT_ID('GetProductTypeUrl') IS NOT NULL)
	DROP FUNCTION GetProductTypeUrl
GO
CREATE FUNCTION [dbo].[GetProductTypeUrl](@Id uniqueidentifier)  
RETURNS NVarchar(2000)
AS  
BEGIN    
	DECLARE @Url nvarchar(2000)
	SET @Url = ''
	
	SELECT @Url = PPT.CompleteFriendlyUrl + '/' + PT.FriendlyName FROM
		PRProductType PT JOIN PRProductType PPT ON PT.ParentId = PPT.Id
	WHERE  PT.Id = @Id AND PT.LftValue > 1
	
	RETURN LOWER(@Url)
END
GO
ALTER TABLE PRProductType DROP COLUMN CompleteFriendlyUrl
GO
ALTER TABLE PRProductType ADD CompleteFriendlyUrl AS ([dbo].[GetProductTypeUrl]([Id]))
GO

PRINT 'Adding computed column for PRProductTypeVariantCache'
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PRProductTypeVariantCache' AND COLUMN_NAME = 'CompleteFriendlyUrl')
	ALTER TABLE PRProductTypeVariantCache DROP COLUMN CompleteFriendlyUrl
GO
ALTER TABLE PRProductTypeVariantCache ADD CompleteFriendlyUrl nvarchar(100)
GO

PRINT 'Creating Function GetProductTypeVariantCacheUrl'
GO
IF (OBJECT_ID('GetProductTypeVariantCacheUrl') IS NOT NULL)
	DROP FUNCTION GetProductTypeVariantCacheUrl
GO
CREATE FUNCTION [dbo].[GetProductTypeVariantCacheUrl](@Id uniqueidentifier, @SiteId uniqueidentifier)  
RETURNS NVarchar(2000)
AS  
BEGIN    
	DECLARE @Url nvarchar(2000)
	SET @Url = ''
	
	SELECT @Url = PPTV.CompleteFriendlyUrl + '/' + PTV.FriendlyName 
	FROM PRProductTypeVariantCache PTV 
		JOIN PRProductType PT ON PT.Id = PTV.Id AND PTV.SiteId = @SiteId
		JOIN PRProductTypeVariantCache PPTV ON PT.ParentId = PPTV.Id AND PPTV.SiteId = @SiteId
	WHERE  PTV.Id = @Id AND PT.LftValue > 1
	
	RETURN  LOWER(@Url)
END
GO
ALTER TABLE PRProductTypeVariantCache DROP COLUMN CompleteFriendlyUrl
GO
ALTER TABLE PRProductTypeVariantCache ADD CompleteFriendlyUrl AS ([dbo].[GetProductTypeVariantCacheUrl]([Id], [SiteId]))
GO
PRINT 'Create view vwProductTypePathVariant'
GO
IF (OBJECT_ID('vwProductTypePathVariant') IS NOT NULL)
	DROP VIEW vwProductTypePathVariant	
GO
CREATE VIEW [dbo].[vwProductTypePathVariant]  
AS  
SELECT	Id AS ProductTypeId, Title As ProductTypeName, CompleteFriendlyUrl AS ProductTypePath, SiteId  
FROM    dbo.PRProductType

UNION ALL 

SELECT  P.Id AS ProductTypeId, P.Title As ProductTypeName, CompleteFriendlyUrl AS ProductTypePath, P.SiteId 
FROM    dbo.PRProductTypeVariantCache P
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ATAttributeEnumVariant' AND COLUMN_NAME = 'Code')
	ALTER TABLE ATAttributeEnumVariant DROP COLUMN Code
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ATAttributeEnumVariantCache' AND COLUMN_NAME = 'Code')
	ALTER TABLE ATAttributeEnumVariantCache DROP COLUMN Code
GO

PRINT 'Add ExcludeFromTranslation column in PageDefinition'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PageDefinition' AND COLUMN_NAME = 'ExcludeFromTranslation')
	ALTER TABLE PageDefinition ADD [ExcludeFromTranslation] bit NULL
GO
IF (OBJECT_ID('dbo.[DF_PageDefinition_ExcludeFromTranslation]') IS NULL)
BEGIN
	UPDATE PageDefinition SET ExcludeFromTranslation = 0 WHERE ExcludeFromTranslation IS NULL

	ALTER TABLE PageDefinition ADD CONSTRAINT [DF_PageDefinition_ExcludeFromTranslation] DEFAULT 0 FOR [ExcludeFromTranslation]
END
GO
IF (OBJECT_ID('dbo.[FK_PRProductTypeAttributeValue_ATAttribute]') IS NOT NULL)
	ALTER TABLE PRProductTypeAttributeValue DROP CONSTRAINT [FK_PRProductTypeAttributeValue_ATAttribute] 
GO
IF (OBJECT_ID('dbo.[FK_PRProductTypeAttributeValue_ATAttributeEnum]') IS NOT NULL)
	ALTER TABLE PRProductTypeAttributeValue DROP CONSTRAINT [FK_PRProductTypeAttributeValue_ATAttributeEnum] 
GO
IF (OBJECT_ID('dbo.[FK_PRProductTypeAttributeValue_PRProductTypeAttribute]') IS NOT NULL)
	ALTER TABLE PRProductTypeAttributeValue DROP CONSTRAINT [FK_PRProductTypeAttributeValue_PRProductTypeAttribute] 
GO

PRINT 'ALTER Title, Description in PRProductType'
GO
IF (OBJECT_ID('vwObjectUrls') IS NOT NULL)
	DROP VIEW vwObjectUrls	
GO
IF (OBJECT_ID('vwObjectUrls_Blogs') IS NOT NULL)
	DROP VIEW vwObjectUrls_Blogs	
GO
IF (OBJECT_ID('vwObjectUrls_Files') IS NOT NULL)
	DROP VIEW vwObjectUrls_Files	
GO
IF (OBJECT_ID('vwObjectUrls_Menus') IS NOT NULL)
	DROP VIEW vwObjectUrls_Menus	
GO
IF (OBJECT_ID('vwObjectUrls_Pages') IS NOT NULL)
	DROP VIEW vwObjectUrls_Pages
GO
IF (OBJECT_ID('vwObjectUrls_Products') IS NOT NULL)
	DROP VIEW vwObjectUrls_Products
GO
ALTER TABLE PRProduct ALTER COLUMN SEOTitle nvarchar(555) NULL
GO
ALTER TABLE PRProduct ALTER COLUMN SEOH1 nvarchar(555) NULL
GO
ALTER TABLE PRProductType ALTER COLUMN Title nvarchar(555) NOT NULL
GO
ALTER TABLE PRProductType ALTER COLUMN Description nvarchar(max) NULL
GO
ALTER TABLE PRProductType ALTER COLUMN FriendlyName nvarchar(555) NULL
GO
ALTER TABLE PRProductTypeVariant ALTER COLUMN Title nvarchar(555) NULL
GO
ALTER TABLE PRProductTypeVariant ALTER COLUMN Description nvarchar(max) NULL
GO
ALTER TABLE PRProductTypeVariant ALTER COLUMN FriendlyName nvarchar(555) NULL
GO
ALTER TABLE PRProductTypeVariantCache ALTER COLUMN Title nvarchar(555) NULL
GO
ALTER TABLE PRProductTypeVariantCache ALTER COLUMN Description nvarchar(max) NULL
GO
ALTER TABLE PRProductTypeVariantCache ALTER COLUMN FriendlyName nvarchar(555) NULL
GO
ALTER TABLE BLD_Product ALTER COLUMN SEOTitle nvarchar(555) NULL
GO
ALTER TABLE ATFacetRangeProduct_Cache ALTER COLUMN DisplayText nvarchar(555) NOT NULL
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'BLD_Product' AND COLUMN_NAME = 'SEOH1')
	ALTER TABLE BLD_Product ADD [SEOH1] nvarchar(555) NULL
GO
PRINT 'Creating table TLTranslation'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLTranslationLocale'))
BEGIN
	CREATE TABLE [dbo].[TLTranslationLocale]
	(
		[Id] INT NOT NULL IDENTITY , 
		[DisplayName] NVARCHAR(555) NOT NULL, 
		[Code] NVARCHAR(10) NOT NULL, 
		[Status] INT NOT NULL, 
		CONSTRAINT [PK_TLTranslationLocale] PRIMARY KEY ([Id])
	)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English, Belgium', N'en-BE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Abkhaz, Georgia', N'ab-GE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Akan Asante, Ghana', N'ak-GH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Amharic, Ethiopia', N'am-ET', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Apache, United States', N'apa-US', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English, United States', N'en-US', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, United Arab Emirates', N'ar-AE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Afghanistan', N'ar-AF', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Bahrain', N'ar-BH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Algeria', N'ar-DZ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Egypt', N'ar-EG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Iraq', N'ar-IQ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Jordan', N'ar-JO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Libya', N'ar-LY', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Morocco', N'ar-MA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Mauritania', N'ar-MR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Oman', N'ar-OM', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Saudi Arabia', N'ar-SA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Sudan', N'ar-SD', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Syria', N'ar-SY', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Chad', N'ar-TD', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Tunisia', N'ar-TN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Uzbekistan', N'ar-UZ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic, Yemen', N'ar-YE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Assamese, India', N'as-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Asturian, Spain', N'ast-ES', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Aymara, Bolivia', N'ay-BO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Azerbaijani, Azerbaijan', N'az-AZ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Bashkir, Russia', N'ba-RU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Belarusian, Belarus', N'be-BY', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Bislama, Vanuatu', N'bi-VU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Bikol / Bicolano, Philippines', N'bik-PH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Bambara, Mali', N'bm-ML', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Bengali, Bangladesh', N'bn-BD', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tibetan, China', N'bo-CN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Breton, France', N'br-FR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Bosnian, Bosnia', N'bs-BA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Chechen, Russia', N'ce-RU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Cebuano / Bisayan, Philippines', N'ceb-PH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Chamorro, Guam', N'ch-GU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Cherokee, United States', N'chr-US', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Cheyenne, United States', N'chy-US', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Corsican, France', N'co-FR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Divehi, Maldives', N'dv-MV', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Dzongkha, Bhutan', N'dz-BT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Ewe, Ghana', N'ee-GH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Efik, Nigeria', N'efi-NG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Estonian, Estonia', N'et-EE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Basque, Spain', N'eu-ES', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Finnish, Finland', N'fi-FI', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Fijian, Fiji', N'fj-FJ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Fijian, India', N'fj-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Fon, Benin', N'fon-BJ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Ga, Ghana', N'gaa-GH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Dari, Iran', N'gbz-IR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Gilbertese, Kiribati', N'gil-KI', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Guarani, Bolivia', N'gn-BO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Gujarati, India', N'gu-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Hausa, Nigeria', N'ha-NG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Hawaiian, United States', N'haw-US', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Hindi, India', N'hi-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Hiligaynon / Ilonn, Philippines', N'hil-PH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Hmong, Laos', N'hmn-LA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Haitian Creole, Haiti', N'ht-HT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Armenian, Armenia', N'hy-AM', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Igbo, Nigeria', N'ig-NG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Icelandic, Iceland', N'is-IS', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Japanese, Japan', N'ja-JP', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Javanese, Indonesia', N'jv-ID', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Georgian, Georgia', N'ka-GE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Q''eqchi'' / Kekchi'', Guatemala', N'kek-GT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Kon / Kikon, Con', N'kg-CD', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Kikuyu / Gikuyu, Kenya', N'kik-KE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Rwanda / Kinyarwanda, Rwanda', N'kin-RW', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Kwanyama, Anla', N'kj-AO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Kazakh, Kazakhstan', N'kk-KZ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Cambodian / Khmer, Cambodia', N'km-KH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Kannada, India', N'kn-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Korean, Korea', N'ko-KR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Kosraean, Micronesia', N'kos-FM', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Kashmiri, India', N'ks-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Cornish, United Kingdom', N'kw-GB', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Kirghiz, Kyrgyzstan', N'ky-KG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Luxembourgeois, Luxembourg', N'lb-LU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Ganda, Uganda', N'lg-UG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Lingala, Con', N'ln-CD', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Lao, Laos', N'lo-LA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Luba_Katanga, Con', N'lu-CD', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Marshallese, Marshal Islands', N'mh-MH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Macedonian, Macedonia', N'mk-MK', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Malayalam, India', N'ml-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Monlian, Monlia', N'mn-MN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Moldavian, Moldova', N'mo-MD', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Marathi, India', N'mr-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Maltese, Malta', N'mt-MT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Burmese, Myanmar', N'my-MM', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Nauruan, Nauru', N'na-NR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Nepali, Nepal', N'ne-NP', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Ndonga, Nambia', N'ng-NA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Niuean, Niue', N'niu-NU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Norwegian, Norway', N'no-NO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Sotho Northern, South Africa', N'nso-ZA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Navajo, United States', N'nv-US', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Oromo, Ethiopia', N'om-ET', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Oriya, India', N'or-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English, South Africa', N'en-ZA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Esperanto, France', N'eo-FR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Pangasinan, Philippines', N'pag-PH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Papiamentu, Netherland Antilles', N'pap-AN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Palauan, Palau', N'pau-PW', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Pushto / Pashto, Afghanistan', N'ps-AF', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Quechua, Bolivian', N'qu-BO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Rarotongan, Cook Islands', N'rar-CK', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Rundi, Burundi', N'rn-BI', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Sanskrit, India', N'sa-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Sardinian, Italy', N'sc-IT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Sicilian, Italy', N'scn-IT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Sindhi, Pakistan', N'sd-PK', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Ndebele, South Africa', N'nr-ZA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'San, Central African Rep.', N'sg-CF', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Sinhala, Sri Lanka', N'si-LK', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Samoan, Samoa', N'sm-WS', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Shona, Zimbabwe', N'sn-ZW', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Somali, Somalia', N'so-SO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Albanian, Albania', N'sq-SQ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Serbian, Serbia', N'sr-CS', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Swati, Swaziland', N'ss-SZ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Sotho Southern, Lesotho', N'st-LS', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Swahili, Tanzania', N'sw-TZ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tamil, India', N'ta-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Telugu, India', N'te-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tajiki, Tajikistan', N'tg-TJ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Thai, Thailand', N'th-TH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tigrigna / Tigrinya, Eritrea', N'ti-ER', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Turkmen, Turkmenistan', N'tk-TM', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tswana, Botswana', N'tn-BW', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tongan, Tonga', N'to-TO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tok Pisin, Papua New Guinea', N'tpi-PG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Turkish, Turkey', N'tr-TR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tsonga, South Africa', N'ts-ZA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tumbuku, Malawi', N'tum-MW', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tuvaluan, Tuvalu', N'tvl-TV', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Twi (Akan), Ghana', N'tw-GH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tahitian, French Polynesia', N'ty-PF', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Uyghur / Uighur, China', N'ug-CN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Umbundu, Anla', N'um-AO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Urdu, Pakistan', N'ur-PK', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Uzbek, Uzbekistan', N'uz-UZ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Venda, South Africa', N've-ZA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Vietnamese, Viet Nam', N'vi-VN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Waray, Philippines', N'war-PH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Wolof, Senegal', N'wo-SN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Xhosa, South Africa', N'xh-ZA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Yapese, Micronesia', N'yap-FM', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Yiddish, Israel', N'yi-IL', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Yoruba, Nigeria', N'yo-NG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Chinese, China', N'zh-CN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Chinese, Hong Kong', N'zh-HK', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Chinese, Singapore', N'zh-SG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Chinese, Taiwan', N'zh-TW', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Afrikaans, South Africa', N'af-ZA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Bulgarian, Bulgaria', N'bg-BG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Catalan, Spain', N'ca-ES', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Czech, Czech Republic', N'cs-CZ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Welsh, United Kingdom', N'cy-GB', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Danish, Denmark', N'da-DK', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'German, Germany', N'de-DE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Greek, Greece', N'el-GR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English, Australia', N'en-AU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English, Canada', N'en-CA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English, United Kingdom', N'en-GB', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Quechua, Peru', N'qu-PE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Argentina', N'es-AR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Bolivia', N'es-BO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Chile', N'es-CL', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Colombia', N'es-CO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Costa Rica', N'es-CR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Cuba', N'es-CU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Dominican Republic', N'es-DO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Ecuador', N'es-EC', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Spain', N'es-ES', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Guatemala', N'es-GT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Honduras', N'es-HN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Mexico', N'es-MX', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Nicaragua', N'es-NI', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Panama', N'es-PA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Peru', N'es-PE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Puerto Rico', N'es-PR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Paraguay', N'es-PY', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, El Salvador', N'es-SV', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, United States', N'es-US', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Uruguay', N'es-UY', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Venezuela', N'es-VE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'French, Canadian', N'fr-CA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'French, France', N'fr-FR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'French (Cajun), United States', N'fr-US', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Irish, Ireland', N'ga-IE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Gaelic (Scottish), United Kingdom', N'gd-GB', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Galician, Spain', N'gl-ES', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Hebrew, Israel', N'he-IL', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Croatian, Croatia', N'hr-HR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Hungarian, Hungary', N'hu-HU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Indonesian, Indonesia', N'id-ID', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Italian, Italy', N'it-IT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Kurdish, Iraq', N'ku-IQ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Latin, Vatican City', N'la-VA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Lithuanian, Lithuania', N'lt-LT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Latvian, Latvia', N'lv-LV', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Malagasy, Madagascar', N'mg-MG', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Maori, New Zealand', N'mi-NZ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Malay Bahasa Melayu, Malaysia', N'ms-MY', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Norwegian Bokmal, Norway', N'nb-NO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Dutch, Netherlands', N'nl-NL', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Norwegian Nynorsk, Norway', N'nn-NO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Chichewa / Nyanja, Malawi', N'ny-MW', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Polish, Poland', N'pl-PL', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Portuguese, Brazil', N'pt-BR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Portuguese, Portugal', N'pt-PT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Romanian, Romania', N'ro-RO', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Russian, Russia', N'ru-RU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Slovak, Slovakia', N'sk-SK', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Slovenian, Slovenia', N'sl-SI', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Swedish, Sweden', N'sv-SE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Tagalog, Philippines', N'tl-PH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Ukrainian, Ukraine', N'uk-UA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Zulu, South Africa', N'zu-ZA', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Farsi (Persian), Iran', N'fa-IR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Fanti (Akan), Ghana', N'fat-GH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Sundanese, Sunda', N'su-ID', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Ndebele, Zimbabwe', N'nd-ZW', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Punjabi (Indian script), India', N'pa-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Punjabi (Latin script), Pakistan', N'pa-PK', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Dutch, Belgium', N'nl-BE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'German, Switzerland', N'de-CH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Italian, Switzerland', N'it-CH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'French, Switzerland', N'fr-CH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'German, Austria', N'de-AT', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Chuukese, Micronesia', N'chk-FM', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Pampangan, Philippines', N'pam-PH', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Pohnpeian, Micronesia', N'pon-FM', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Dari, Afghanistan', N'prs-AF', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Aimaq, Afghanistan', N'aiq-AF', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Hazaragi, Afghanistan', N'haz-AF', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Arabic (Standard)', N'ar', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English Creoles and Pidgins', N'cpe-US', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'French Creoles and Pidgins', N'cpf-MU', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Portuguese Creoles and Pidgins', N'cpp-BR', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Afar, Djibouti', N'aa-DJ', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English, Ireland', N'en-IE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Spanish, Latin America', N'es-419', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English, India', N'en-IN', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'French, Belgium', N'fr-BE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Vlaams (West Flemish), Belgium', N'vls-BE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'English, Sweden', N'en-SE', 1)

	INSERT [dbo].[TLTranslationLocale] ([DisplayName], [Code], [Status]) VALUES (N'Iloko / Ilocano, Philippines', N'ilo-PH', 1)
END
GO
PRINT 'Creating table TLTranslation'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLTranslation'))
BEGIN
	CREATE TABLE [dbo].[TLTranslation]
	(
		[Id] [uniqueidentifier] NOT NULL,
		[Title] [nvarchar](256) NULL,
		[ObjectId] [uniqueidentifier] NOT NULL,
		[ObjectType] [int] NOT NULL,
		[SiteId] [uniqueidentifier] NOT NULL,
		[ExternalId] NVARCHAR(100) NULL,
		[CreatedBy] [uniqueidentifier] NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[ModifiedBy] [uniqueidentifier] NULL,
		[ModifiedDate] [datetime] NULL, 
		CONSTRAINT [PK_TLTranslation] PRIMARY KEY ([Id])
	)
END
GO
PRINT 'Creating table TLTranslationTarget'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLTranslationTarget'))
BEGIN
	CREATE TABLE [dbo].[TLTranslationTarget]
	(
		[Id] [uniqueidentifier] NOT NULL,
		[TranslationId] [uniqueidentifier] NOT NULL,
		[TargetId] [uniqueidentifier] NOT NULL,
		[TargetTypeId] [int] NOT NULL,
		CONSTRAINT [PK_TLTranslationTarget] PRIMARY KEY ([Id]), 
		CONSTRAINT [FK_TLTranslationTarget_TLTranslation] FOREIGN KEY ([TranslationId]) REFERENCES [TLTranslation]([Id]) 
	)
END
GO
PRINT 'Creating table TLDocument'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLDocument'))
BEGIN
	CREATE TABLE [dbo].[TLDocument]
	(
		[Id] UNIQUEIDENTIFIER NOT NULL, 
		[TranslationId] UNIQUEIDENTIFIER NOT NULL, 
		[Locale] NVARCHAR(10) NOT NULL, 
		[Title] NVARCHAR(2000) NOT NULL, 
		[Content] NVARCHAR(MAX) NULL, 
		[ExternalId] NVARCHAR(100) NULL, 
		[Status] INT NOT NULL, 
		[ProcessingStatus] INT NOT NULL, 
		[LogMessage] NVARCHAR(MAX) NULL, 
		[CreatedBy] [uniqueidentifier] NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[ModifiedBy] [uniqueidentifier] NULL,
		[ModifiedDate] [datetime] NULL, 
		CONSTRAINT [PK_TLDocument] PRIMARY KEY ([Id]), 
		CONSTRAINT [FK_TLDocument_TLTranslation] FOREIGN KEY ([TranslationId]) REFERENCES [TLTranslation]([Id])
	)
END
GO
IF (COL_LENGTH('TLDocument', 'ObjectUrl') IS NULL)
	ALTER TABLE TLDocument ADD ObjectUrl nvarchar(2000) NULL
GO
PRINT 'Creating table TLDocumentTarget'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLDocumentTarget'))
BEGIN
	CREATE TABLE [dbo].[TLDocumentTarget]
	(
		[Id] [uniqueidentifier] NOT NULL,
		[TranslationDocumentId] [uniqueidentifier] NOT NULL,
		[TargetId] [uniqueidentifier] NOT NULL,
		[TargetTypeId] [int] NOT NULL, 
		CONSTRAINT [PK_TLDocumentTarget] PRIMARY KEY ([Id]), 
		CONSTRAINT [FK_TLDocumentTarget_TLDocument] FOREIGN KEY ([TranslationDocumentId]) REFERENCES [TLDocument]([Id])
	)
END
GO
PRINT 'Creating table TLDocumentItem'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLDocumentItem'))
BEGIN
	CREATE TABLE [dbo].[TLDocumentItem]
	(
		[Id] UNIQUEIDENTIFIER NOT NULL, 
		[TranslationDocumentId] UNIQUEIDENTIFIER NOT NULL, 
		[SiteId] UNIQUEIDENTIFIER NOT NULL, 
		[Locale] NCHAR(10) NOT NULL, 
		[Status] INT NOT NULL, 
		[Content] NVARCHAR(MAX) NULL, 
		[LogMessage] NVARCHAR(MAX) NULL, 
		[CreatedBy] [uniqueidentifier] NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[ModifiedBy] [uniqueidentifier] NULL,
		[ModifiedDate] [datetime] NULL, 
		CONSTRAINT [PK_TLDocumentItem] PRIMARY KEY ([Id]), 
		CONSTRAINT [FK_TLDocumentItem_TLDocument] FOREIGN KEY ([TranslationDocumentId]) REFERENCES [TLDocument]([Id]), 
		CONSTRAINT [FK_TLDocumentItem_SISite] FOREIGN KEY ([SiteId]) REFERENCES [SISite]([Id])
	)
END
GO
PRINT 'Creating table TLDocumentWorkTable'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLDocumentWorkTable'))
BEGIN
	CREATE TABLE [dbo].[TLDocumentWorkTable]
	(
		[BatchId] UNIQUEIDENTIFIER NOT NULL, 
		[DocumentId] UNIQUEIDENTIFIER NOT NULL, 
		[Content] NVARCHAR(MAX) NULL, 
		[Status] INT NOT NULL, 
		[ProcessingStatus] INT NOT NULL, 
		[ExternalId] NVARCHAR(100) NULL, 
		[LogMessage] NVARCHAR(MAX) NULL, 
		CONSTRAINT [PK_TLDocumentWorkTable] PRIMARY KEY ([BatchId], [DocumentId])
	)
END
GO
IF (COL_LENGTH('TLDocumentWorkTable', 'ObjectUrl') IS NULL)
	ALTER TABLE TLDocumentWorkTable ADD ObjectUrl nvarchar(2000) NULL
GO
PRINT 'Creating table TLDocumentItemWorkTable'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLDocumentItemWorkTable'))
BEGIN
	CREATE TABLE [dbo].[TLDocumentItemWorkTable]
	(
		[BatchId] UNIQUEIDENTIFIER NOT NULL , 
		[DocumentItemId] UNIQUEIDENTIFIER NOT NULL, 
		[DocumentId] UNIQUEIDENTIFIER NULL, 
		[SiteId] UNIQUEIDENTIFIER NOT NULL, 
		[Status] INT NOT NULL, 
		[Content] NVARCHAR(MAX) NULL, 
		[LogMessage] NVARCHAR(MAX) NULL, 
		CONSTRAINT [PK_TLDocumentItemWorkTable] PRIMARY KEY ([BatchId], [DocumentItemId]) 
	)
END
GO
PRINT 'Creating table TLTranslationNotification'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLTranslationNotification'))
BEGIN
CREATE TABLE [dbo].[TLTranslationNotification]
(
	[Id] INT NOT NULL,
    [BatchId] UNIQUEIDENTIFIER NOT NULL, 
	[DocumentId] NVARCHAR(100) NOT NULL , 
    [Locale] NVARCHAR(10) NOT NULL, 
    [Status] INT NOT NULL, 
    CONSTRAINT [PK_TLTranslationNotification] PRIMARY KEY ([Id])
)
END
GO
PRINT 'Creating table TLTranslationNotification_Log'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TLTranslationNotification_Log'))
BEGIN
CREATE TABLE [dbo].[TLTranslationNotification_Log]
(
	[Id] INT NOT NULL,
    [BatchId] UNIQUEIDENTIFIER NOT NULL, 
	[DocumentId] NVARCHAR(100) NOT NULL , 
    [Locale] NVARCHAR(10) NOT NULL, 
    [Status] INT NOT NULL
)
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_TLDocument_TranslationId' AND object_id = OBJECT_ID('TLDocument'))
	CREATE INDEX [IX_TLDocument_TranslationId] ON [dbo].[TLDocument] ([TranslationId])
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_TLDocumentItem_TranslationDocumentId' AND object_id = OBJECT_ID('TLDocumentItem'))
	CREATE INDEX [IX_TLDocumentItem_TranslationDocumentId] ON [dbo].[TLDocumentItem] ([TranslationDocumentId])
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_TLDocumentItem_SiteId' AND object_id = OBJECT_ID('TLDocumentItem'))
	CREATE INDEX [IX_TLDocumentItem_SiteId] ON [dbo].[TLDocumentItem] ([SiteId])
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_TLDocumentTarget_TranslationDocumentId' AND object_id = OBJECT_ID('TLDocumentTarget'))
	CREATE INDEX [IX_TLDocumentTarget_TranslationDocumentId] ON [dbo].[TLDocumentTarget] ([TranslationDocumentId])
GO
PRINT 'UPDATE Rule controls Ids for SiteList'
GO
UPDATE RERuleValue SET ControlId = 'ddlSiteProperties' WHERE ControlId = 'ddlGEOProperties'
GO

UPDATE RERuleValue SET ControlId = 'ddSiteProperyOperator' WHERE ControlId = 'ddGEOProperyOperator'
GO

UPDATE RERuleValue SET ControlValue = 'opSitePropertyEqual' WHERE ControlValue = 'opGEOOperatorEqual'
GO

UPDATE RERuleValue SET ControlValue = 'opSitePropertyNotEqual' WHERE ControlValue = 'opGEOOperatorNotEqual'
GO

UPDATE RERuleValue SET ControlId = 'SiteProperyValue' WHERE ControlId = 'TimeZoneValue'
GO
PRINT 'Adding column IsShared to ORShippingOption'
GO
IF (COL_LENGTH('ORShippingOption', 'IsShared') IS NULL)
	ALTER TABLE ORShippingOption ADD IsShared bit
GO
UPDATE ORShippingOption SET IsShared = 0 WHERE IsShared IS NULL
GO
PRINT 'Create view vwShipper'
GO
IF (OBJECT_ID('vwShipper') IS NOT NULL)
	DROP VIEW vwShipper	
GO
CREATE VIEW vwShipper
AS
SELECT S.[Id],
	S.[Name] AS Title,
	S.[Description],
	S.[DotNetProviderName],
	S.[TrackingURL],
	S.[SiteId],
	S.[SiteId] AS SourceSiteId,
	S.[Sequence],
	S.[Status],
	S.CreatedDate,
	S.CreatedBy AS CreatedById,
	CFN.UserFullName AS CreatedByFullName,    
	S.ModifiedDate,
	S.ModifiedBy AS ModifiedById,     
	MFN.UserFullName AS ModifiedByFullName,    
	S.IsShared
FROM [dbo].[ORShipper] S
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = S.CreatedBy    
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = S.ModifiedBy  
WHERE S.IsShared = 0

UNION ALL

SELECT S.[Id],
	S.[Name] AS Title,
	S.[Description],
	S.[DotNetProviderName],
	S.[TrackingURL],
	CSI.Id AS [SiteId],
	S.[SiteId] AS SourceSiteId,
	S.[Sequence],
	S.[Status],
	S.CreatedDate,
	S.CreatedBy AS CreatedById,
	CFN.UserFullName AS CreatedByFullName,    
	S.ModifiedDate,
	S.ModifiedBy AS ModifiedById,     
	MFN.UserFullName AS ModifiedByFullName,    
	S.IsShared
FROM [dbo].[ORShipper] S
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = S.CreatedBy    
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = S.ModifiedBy  
	CROSS APPLY SISite CSI
	INNER JOIN SISite SI ON S.SiteId = SI.Id
WHERE S.IsShared = 1 AND CSI.Status = 1
	AND CSI.LftValue >= SI.LftValue AND CSI.RgtValue <= SI.RgtValue AND SI.MasterSiteId = CSI.MasterSiteId
GO
PRINT 'Create view vwShippingOption'
GO
IF (OBJECT_ID('vwShippingOption') IS NOT NULL)
	DROP VIEW vwShippingOption	
GO
CREATE VIEW vwShippingOption 
AS
SELECT SO.[Id],
	SO.[ShipperId],
	SO.[Name],
	SO.[Description],
	SO.[CustomId],
	SO.[NumberOfDaysToDeliver],
	SO.[CutOffHour],
	SO.[SaturdayDelivery],
	SO.[AllowShippingCoupon],
	SO.[IsPublic],
	SO.[IsInternational],
	SO.[ServiceCode],
	SO.[BaseRate],
	SO.[SiteId],
	SO.[SiteId] AS SourceSiteId,
	SO.[Sequence],
	SO.[Status],
	SO.CreatedDate,
	SO.CreatedBy AS CreatedById,
	CFN.UserFullName AS CreatedByFullName,    
	SO.ModifiedDate,
	SO.ModifiedBy AS ModifiedById,     
	MFN.UserFullName AS ModifiedByFullName,    
	SO.[ExternalShipCode],
	SO.IsShared,
	O.Name AS ShipperName,
	SOR.StartAmount,
	CASE SOR.EndAmount WHEN 0 THEN 922337203685477.5807 ELSE SOR.EndAmount END AS EndAmount,
	SOR.ShippingPrice
FROM [dbo].[ORShippingOption] SO
	INNER JOIN ORShipper O ON O.Id = SO.ShipperId
	LEFT JOIN ORShippingOptionRate SOR ON SOR.ShippingOptionId = SO.Id
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = SO.CreatedBy    
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = SO.ModifiedBy  
WHERE SO.IsShared = 0 AND O.Status = 1

UNION ALL

SELECT SO.[Id],
	SO.[ShipperId],
	SO.[Name],
	SO.[Description],
	SO.[CustomId],
	SO.[NumberOfDaysToDeliver],
	SO.[CutOffHour],
	SO.[SaturdayDelivery],
	SO.[AllowShippingCoupon],
	SO.[IsPublic],
	SO.[IsInternational],
	SO.[ServiceCode],
	SO.[BaseRate],
	CS.Id AS [SiteId],
	SO.[SiteId] AS SourceSiteId,
	SO.[Sequence],
	SO.[Status],
	SO.CreatedDate,
	SO.CreatedBy AS CreatedById,
	CFN.UserFullName AS CreatedByFullName,    
	SO.ModifiedDate,
	SO.ModifiedBy AS ModifiedById,     
	MFN.UserFullName AS ModifiedByFullName,    
	SO.[ExternalShipCode],
	SO.IsShared,
	O.Name AS ShipperName,
	SOR.StartAmount,
	CASE SOR.EndAmount WHEN 0 THEN 922337203685477.5807 ELSE SOR.EndAmount END AS EndAmount,
	SOR.ShippingPrice
FROM [dbo].[ORShippingOption] SO
	INNER JOIN ORShipper O ON O.Id = SO.ShipperId
	LEFT JOIN ORShippingOptionRate SOR ON SOR.ShippingOptionId = SO.Id
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = SO.CreatedBy    
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = SO.ModifiedBy  
	CROSS APPLY SISite CS
	INNER JOIN SISite S ON SO.SiteId = S.Id
WHERE SO.IsShared = 1 AND O.Status = 1 AND CS.Status = 1
	AND CS.LftValue >= S.LftValue AND CS.RgtValue <= S.RgtValue AND S.MasterSiteId = CS.MasterSiteId
GO
PRINT 'Create view vwProduct'
GO
IF (OBJECT_ID('vwProduct') IS NOT NULL)
	DROP VIEW vwProduct	
GO
CREATE VIEW [dbo].[vwProduct]  
AS  
WITH CTE AS  
(  
 SELECT P.[Id],   
	P.SiteId,  
	P.Title,   
	P.ShortDescription,   
	P.LongDescription,   
	P.[Description],   
	P.ProductStyle,  
	P.UrlFriendlyTitle,   
	P.TaxCategoryID,   
	P.IsActive,   
	P.PromoteAsNewItem,   
	P.PromoteAsTopSeller,  
	P.ExcludeFromDiscounts,   
	P.ExcludeFromShippingPromotions,   
	P.Freight,   
	P.Refundable,   
	P.TaxExempt,  
	P.SEOTitle,
	P.SEOH1,   
	P.SEODescription,   
	P.SEOKeywords,   
	P.SEOFriendlyUrl,  
	P.IsShared,  
	ISNULL(P.ModifiedDate, P.CreatedDate) AS ModifiedDate,   
	ISNULL(P.ModifiedBy, P.CreatedBy) AS  ModifiedBy  
FROM PRProduct P  
  
UNION ALL  
  
SELECT P.[Id],   
	P.SiteId,  
	P.Title,   
	P.ShortDescription,   
	P.LongDescription,   
	P.[Description],   
	P.ProductStyle,  
	P.UrlFriendlyTitle,   
	P.TaxCategoryID,   
	P.IsActive,   
	P.PromoteAsNewItem,   
	P.PromoteAsTopSeller,  
	P.ExcludeFromDiscounts,   
	P.ExcludeFromShippingPromotions,   
	P.Freight,   
	P.Refundable,   
	P.TaxExempt,  
	P.SEOTitle,
	P.SEOH1,   
	P.SEODescription,   
	P.SEOKeywords,   
	P.SEOFriendlyUrl,  
	0,  
	P.ModifiedDate,   
	P.ModifiedBy  
 FROM PRProductVariantCache P  
)  
  
SELECT P.[Id],   
	C.SiteId,  
	P.SiteId AS SourceSiteId,  
	C.Title,   
	C.ShortDescription,   
	C.LongDescription,   
	C.[Description],   
	P.ProductIDUser,   
	P.Keyword,   
	C.ProductStyle,  
	P.BundleCompositionLastModified,   
	C.UrlFriendlyTitle,   
	C.TaxCategoryID,   
	CAST(C.IsActive AS BIT) AS IsActive,   
	C.PromoteAsNewItem,   
	C.PromoteAsTopSeller,  
	C.ExcludeFromDiscounts,   
	C.ExcludeFromShippingPromotions,   
	C.Freight,   
	C.Refundable,   
	C.TaxExempt,  
	P.ProductTypeID,   
	P.[Status],   
	CAST(P.IsBundle AS BIT) AS IsBundle,   
	PTP.ProductTypeName,  
	LOWER(CASE WHEN C.SEOFriendlyUrl IS NOT NULL AND C.SEOFriendlyUrl != '' THEN (CASE WHEN LEFT(C.SEOFriendlyUrl, 1) = '/' THEN C.SEOFriendlyUrl ELSE CONCAT('/', C.SEOFriendlyUrl) END) ELSE CONCAT(PTP.ProductTypePath, '/id-', P.ProductIdUser, '/', C.UrlFriendlyTitle) END) AS ProductUrl,
	C.SEOTitle,
	C.SEOH1,   
	C.SEODescription,   
	C.SEOKeywords,   
	C.SEOFriendlyUrl,  
	P.IsShared,  
	P.CreatedDate,   
	P.CreatedBy AS CreatedById,   
	CFN.UserFullName AS CreatedByFullName,  
	C.ModifiedDate,   
	C.ModifiedBy AS ModifiedById,   
	MFN.UserFullName AS ModifiedByFullName
FROM CTE C  
	INNER JOIN PRProduct AS P ON P.Id = C.Id  
	INNER JOIN vwProductTypePathVariant AS PTP ON PTP.ProductTypeId = P.ProductTypeID AND PTP.SiteId = C.SiteId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy  
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
GO
PRINT 'Create view vwSKU'
GO
IF (OBJECT_ID('vwSKU') IS NOT NULL)
	DROP VIEW vwSKU	
GO
CREATE VIEW [dbo].[vwSKU]
AS
WITH CTE AS
(
	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		ISNULL(P.ModifiedDate, P.CreatedDate) AS ModifiedDate, 
		ISNULL(P.ModifiedBy, P.CreatedBy) AS  ModifiedBy
	FROM PRProductSku P

	UNION ALL

	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		P.ModifiedDate, 
		P.ModifiedBy
	FROM PRProductSKUVariantCache P
)

SELECT	C.Id, SK.ProductId, C.SiteId, SK.SiteId AS SourceSiteId, C.Title, C.[Description], SK.SKU, C.IsActive, C.IsOnline, C.UnitCost,
		C.WholesalePrice, C.PreviousSoldCount, C.Measure, C.OrderMinimum, C.OrderIncrement, C.HandlingCharges,
		SK.Height, SK.Width, SK.[Length], SK.[Weight], C.SelfShippable, C.AvailableForBackOrder, C.BackOrderLimit, C.IsUnlimitedQuantity,
		C.MaximumDiscountPercentage, C.FreeShipping, C.UserDefinedPrice, C.[Sequence], SK.[Status],
		C.ListPrice, ISNULL(ATS.Inventory, 0) AS Quantity, ISNULL(ATS.Quantity, 0) AS AvailableToSell,
		SK.CreatedDate, SK.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		C.ModifiedDate, C.ModifiedBy, MFN.UserFullName AS ModifiedByFullName,
		ISNULL(PC.Title, P.Title) AS ProductName, P.ProductTypeId, PT.IsDownloadableMedia
FROM	CTE AS C
		INNER JOIN PRProductSku AS SK ON SK.Id = C.Id
		INNER JOIN PRProduct AS P ON P.Id = SK.ProductId
		LEFT JOIN PRProductVariantCache AS PC ON PC.Id = SK.ProductId AND PC.SiteId = C.SiteId
		INNER JOIN PRProductType AS PT ON PT.Id = P.ProductTypeID
		LEFT JOIN vwAvailableToSellPerSite AS ATS ON ATS.SKUId = SK.Id AND ATS.SiteId = SK.SiteId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = SK.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = SK.ModifiedBy

GO
PRINT 'Create view vwSimplePageUrl'
GO
IF (OBJECT_ID('vwSimplePageUrl') IS NOT NULL)
	DROP VIEW vwSimplePageUrl	
GO
CREATE VIEW [dbo].[vwSimplePageUrl] WITH SCHEMABINDING 
AS 
SELECT	PD.SiteId, PD.PageDefinitionId AS Id, LOWER(PMN.CompleteFriendlyUrl + '/' + PD.FriendlyName) AS [Url]
FROM	dbo.PageDefinition AS PD
		INNER JOIN dbo.PageMapNodePageDef AS PMNPD ON PMNPD.PageDefinitionId = PD.PageDefinitionId
		INNER JOIN dbo.PageMapNode AS PMN ON PMN.PageMapNodeId = PMNPD.PageMapNodeId
GO
PRINT 'Create view vwMasterSourceContentId'
GO
IF (OBJECT_ID('vwMasterSourceContentId') IS NOT NULL)
	DROP VIEW vwMasterSourceContentId	
GO
CREATE VIEW [dbo].[vwMasterSourceContentId]
AS

WITH RecCTE AS
(
	SELECT C.Id, C.SourceContentId, C.ApplicationId, 0 AS Level FROM COContent P
		JOIN COContent C ON P.Id = C.SourceContentId
	WHERE C.Id != C.SourceContentId
		AND C.Id IS NOT NULL

	UNION ALL

	SELECT C.Id, P.SourceContentId, C.ApplicationId, Level + 1 FROM RecCTE P
		JOIN COContent C ON P.Id = C.SourceContentId
), CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY Id ORDER BY Level DESC) AS ValueRank,
		*
	FROM RecCTE
)

SELECT Id, SourceContentId, ApplicationId FROM CTE
UNION
SELECT Id, Id, ApplicationId FROM COContent
WHERE Id = SourceContentId OR SourceContentId IS NULL
GO
PRINT 'Create view vwAttributeEnum'
GO
IF (OBJECT_ID('vwAttributeEnum') IS NOT NULL)
	DROP VIEW vwAttributeEnum	
GO
CREATE VIEW [dbo].[vwAttributeEnum]
AS
WITH CTE AS
(
	SELECT E.Id, 
		A.SiteId,
		E.AttributeID,
		E.Title,
		ISNULL(E.ModifiedDate, E.CreatedDate) AS ModifiedDate, 
		ISNULL(E.ModifiedBy, E.CreatedBy) AS  ModifiedBy
	FROM ATAttributeEnum AS E
		JOIN ATAttribute A ON A.Id = E.AttributeID

	UNION ALL

	SELECT E.Id, 
		E.SiteId,
		E.AttributeId,
		E.Title,
		E.ModifiedDate, 
		E.ModifiedBy
	FROM ATAttributeEnumVariantCache AS E
)

SELECT E.Id, 
	C.SiteId,
	A.SiteId AS SourceSiteId,
	C.Title,
	E.Code,
	E.Status,
	E.NumericValue,
	E.Sequence,
	E.Value,
	A.Id AS AttributeId,
	E.IsDefault,
	E.CreatedDate, 
	E.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	C.ModifiedDate, 
	C.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName
FROM CTE C
	INNER JOIN ATAttributeEnum AS E ON E.Id = C.Id
	INNER JOIN ATAttribute AS A ON A.Id = E.AttributeID
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = E.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
WHERE E.Status = 1
GO
PRINT 'Create view vwProductAttributeValue'
GO
IF (OBJECT_ID('vwProductAttributeValue') IS NOT NULL)
	DROP VIEW vwProductAttributeValue	
GO
CREATE VIEW [dbo].[vwProductAttributeValue]
AS
SELECT V.Id, 
	P.SiteId,
	P.Id AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	ISNULL(E.Title, V.Value) AS [Value],
	V.CreatedBy,
	V.CreatedDate,
	A.Title as AttributeTitle
FROM PRProductAttributeValue V
	JOIN PRProduct P ON P.Id = V.ProductId
	JOIN ATAttribute A ON A.Id = V.AttributeId
	LEFT JOIN vwAttributeEnum E ON E.Id = V.AttributeEnumId AND E.SiteId = P.SiteId

UNION ALL

SELECT V.Id,
	V.SiteId,
	V.ProductId AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	ISNULL(E.Title, V.Value) AS [Value],
	V.ModifiedBy,
	V.ModifiedDate,
	A.Title as AttributeTitle
FROM PRProductAttributeValueVariantCache V
	JOIN ATAttribute A ON A.Id = V.AttributeId
	LEFT JOIN vwAttributeEnum E ON E.Id = V.AttributeEnumId AND E.SiteId = V.SiteId
GO
PRINT 'Create view vwSkuAttributeValue'
GO
IF (OBJECT_ID('vwSkuAttributeValue') IS NOT NULL)
	DROP VIEW vwSkuAttributeValue	
GO
CREATE VIEW [dbo].[vwSkuAttributeValue]
AS
SELECT V.Id,
	P.SiteId,
	P.Id AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	ISNULL(E.Title, V.Value) AS [Value],
	V.CreatedBy,
	V.CreatedDate,
	A.Title as AttributeTitle
FROM PRProductSKUAttributeValue V
	JOIN PRProductSKU P ON P.Id = V.SKUId
	JOIN ATAttribute A ON A.Id = V.AttributeId
	LEFT JOIN vwAttributeEnum E ON E.Id = V.AttributeEnumId AND E.SiteId = P.SiteId

UNION ALL

SELECT V.Id,
	V.SiteId,
	V.SKUId,
	V.AttributeId, 
	V.AttributeEnumId, 
	ISNULL(E.Title, V.Value) AS [Value],
	V.ModifiedBy,
	V.ModifiedDate,
	A.Title as AttributeTitle
FROM PRProductSKUAttributeValueVariantCache V
	JOIN ATAttribute A ON A.Id = V.AttributeId
	LEFT JOIN vwAttributeEnum E ON E.Id = V.AttributeEnumId AND E.SiteId = V.SiteId
GO
PRINT 'Create view vwProductImage'
GO
IF (OBJECT_ID('vwProductImage') IS NOT NULL)
	DROP VIEW vwProductImage	
GO
CREATE VIEW [dbo].[vwProductImage]
AS
WITH CTE AS
(
	SELECT Id, SiteId, Title, AltText, Description, Status, ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy  
	FROM CLImage

	UNION ALL

	SELECT Id, SiteId, Title, AltText, Description, Status, ModifiedDate, ModifiedBy
	FROM CLImageVariantCache
)

SELECT I2.Id,
	C.Title,
	I2.FileName,
	C.AltText,
	C.Description,
	I.Size,
	OI.Sequence,
	P.Id AS ProductId,
	P.Title AS ProductName,
	I.CreatedBy,
	I.CreatedDate,
	C.ModifiedBy,
	C.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName,
	P.ProductTypeId,
	'/productimages/' + CAST(P.Id AS varchar(36)) + '/images' AS RelativePath,
	C.SiteId,
	I.SiteId AS SourceSiteId,
	IT.TypeNumber,
	C.Status,
	I2.ParentImage,
	I.IsShared
FROM PRProduct P
	JOIN CLObjectImage OI ON OI.ObjectId = P.Id
	JOIN CTE C ON OI.ImageId = C.Id --AND OI.SiteId = C.SiteId
	JOIN CLImage I ON I.Id = C.Id
	JOIN CLImage I2 ON I.Id = I2.ParentImage OR I.Id = I2.Id
	JOIN CLImageType IT ON IT.Id = I2.ImageType
	LEFT JOIN VW_UserFullName FN on FN.UserId = I.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
 WHERE C.SiteId = I.SiteId or (C.SiteId != I.SiteId and I.IsShared = 1)
GO
PRINT 'Create view vwTranslation'
GO
IF (OBJECT_ID('vwTranslation') IS NOT NULL)
	DROP VIEW vwTranslation	
GO
CREATE VIEW [dbo].[vwTranslation]
AS
SELECT T.Id, 
	T.Title, 
	T.ObjectId, 
	T.ObjectType,
	T.SiteId,
	T.ExternalId,
	T.CreatedBy,
	T.CreatedDate,
	T.ModifiedBy,
	T.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM [dbo].[TLTranslation] T 
	 LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
	 LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
GO
PRINT 'Create view vwTranslationTarget'
GO
IF (OBJECT_ID('vwTranslationTarget') IS NOT NULL)
	DROP VIEW vwTranslationTarget	
GO
CREATE VIEW [dbo].[vwTranslationTarget]
AS 
SELECT T.Id,
	T.TranslationId,
	T.TargetId,
	T.TargetTypeId,
	S.Title AS TargetTitle,
	TL.ObjectId,
	TL.SiteId
FROM TLTranslationTarget T
	JOIN TLTranslation TL ON TL.Id = T.TranslationId
	LEFT JOIN SISiteList S ON T.TargetId = S.Id AND T.TargetTypeId = 1 
GO
PRINT 'Create view vwTranslationDocumentSite'
GO
IF (OBJECT_ID('vwTranslationDocumentSite') IS NOT NULL)
	DROP VIEW vwTranslationDocumentSite	
GO
CREATE VIEW [dbo].[vwTranslationDocumentSite]
AS
WITH CTE AS
(	
	-- Specific site only
	SELECT
		D.Id AS TranslationDocumentId,
		T.TargetId AS SiteId
	FROM TLDocument D
		JOIN TLDocumentTarget T ON D.Id = T.TranslationDocumentId
	WHERE T.TargetTypeId = 2

	UNION ALL

	-- Site List
	SELECT
		D.Id,
		S.SiteId
	FROM TLDocument D 
		JOIN TLDocumentTarget T ON D.Id = T.TranslationDocumentId
		JOIN SISiteListSite S ON S.SiteListId = T.TargetId
	WHERE T.TargetTypeId = 1

	UNION ALL

	-- Child Sites
	SELECT
		D.Id,
		CS.Id
	FROM TLDocument D 
		JOIN TLDocumentTarget T ON D.Id = T.TranslationDocumentId
		JOIN SISite PS ON PS.Id = T.TargetId
		JOIN SISite CS ON PS.LftValue < CS.LftValue AND PS.RgtValue > CS.RgtValue
	WHERE T.TargetTypeId = 3
		AND CS.Status = 1
)

SELECT DISTINCT * FROM CTE

GO
PRINT 'Create view vwTranslationDocument'
GO
IF (OBJECT_ID('vwTranslationDocument') IS NOT NULL)
	DROP VIEW vwTranslationDocument	
GO
CREATE VIEW [dbo].[vwTranslationDocument]
AS
SELECT D.Id,
	D.TranslationId, 
	T.ObjectId, 
	T.ObjectType, 
	D.ObjectUrl,
	T.SiteId,
	D.Status,
	D.Locale,
	D.Content,
	D.ExternalId,
	D.Title,
	D.ProcessingStatus,
	D.LogMessage,
	D.CreatedBy,
	D.CreatedDate,
	D.ModifiedBy,
	D.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM [dbo].[TLTranslation] T 
     JOIN [dbo].[TLDocument] D ON T.Id = D.TranslationId
	 LEFT JOIN VW_UserFullName FN on FN.UserId = D.CreatedBy
	 LEFT JOIN VW_UserFullName MN on MN.UserId = D.ModifiedBy
GO
PRINT 'Create view vwTranslationDocumentTarget'
GO
IF (OBJECT_ID('vwTranslationDocumentTarget') IS NOT NULL)
	DROP VIEW vwTranslationDocumentTarget	
GO
CREATE VIEW [dbo].[vwTranslationDocumentTarget]
AS 
SELECT T.Id,
	T.TranslationDocumentId,
	T.TargetId,
	T.TargetTypeId,
	ISNULL(SL.Title, S.Title) AS TargetTitle
FROM TLDocumentTarget T
	JOIN TLDocument TL ON TL.Id = T.TranslationDocumentId
	LEFT JOIN SISiteList SL ON T.TargetId = SL.Id AND T.TargetTypeId = 1 
	LEFT JOIN SISite S ON T.TargetId = S.Id AND T.TargetTypeId = 2
GO
PRINT 'Create view vwTranslationDocumentItem'
GO
IF (OBJECT_ID('vwTranslationDocumentItem') IS NOT NULL)
	DROP VIEW vwTranslationDocumentItem	
GO
CREATE VIEW [dbo].[vwTranslationDocumentItem]
AS 
SELECT DI.Id,
	DI.TranslationDocumentId,
	DI.SiteId,
	DI.Locale,
	DI.Status,
	DI.Content,
	DI.LogMessage,
	S.Title AS SiteName,
	DI.CreatedBy,
	DI.CreatedDate,
	DI.ModifiedBy,
	DI.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM TLDocumentItem DI
	JOIN SISite S ON S.Id = DI.SiteId
	LEFT JOIN VW_UserFullName FN on FN.UserId = DI.CreatedBy
	 LEFT JOIN VW_UserFullName MN on MN.UserId = DI.ModifiedBy
GO

PRINT 'Create view vwObjectUrls_Blogs'
GO
CREATE VIEW [dbo].[vwObjectUrls_Blogs] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS
SELECT	SiteId, 39, Id, Id, LOWER([Url])
FROM	dbo.vwBlogUrl

UNION ALL

SELECT	SiteId, 40, PostId, PostId, LOWER([Url])
FROM	dbo.vwPostUrl
GO

PRINT 'Create view vwObjectUrls_Files'
GO
CREATE VIEW [dbo].[vwObjectUrls_Files] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS
SELECT	C.ApplicationId, C.ObjectTypeId, C.Id, C.Id, LOWER(REPLACE((
			CASE WHEN C.ObjectTypeId = 9 THEN '/file%20library' WHEN C.ObjectTypeId = 33 THEN '/image%20library' +
				(CASE WHEN NULLIF(S.ParentSiteId, '00000000-0000-0000-0000-000000000000') IS NOT NULL THEN '/' + CAST(S.Id AS NVARCHAR(36)) ELSE '' END)
			END +
			REPLACE(REPLACE(F.RelativePath, '~/File Library', ''), '~/Image Library', '') +
			'/' + F.[FileName]
		), ' ', '%20'))
FROM	dbo.COFile AS F
		INNER JOIN dbo.COContent AS C ON C.Id = F.ContentId
		INNER JOIN dbo.SISite AS S ON S.Id = C.ApplicationId
GO

PRINT 'Create view vwObjectUrls_Menus'
GO
CREATE VIEW [dbo].[vwObjectUrls_Menus] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS
WITH MenuCTE (MasterId, Id, SiteId, CompleteFriendlyUrl) AS (
	SELECT	PMN.PageMapNodeId, PMN.PageMapNodeId, PMN.SiteId, PMN.CompleteFriendlyUrl
	FROM	dbo.PageMapNode AS PMN
			LEFT JOIN dbo.PageMapNode AS SPMN ON SPMN.SourcePageMapNodeId = NULLIF(PMN.SourcePageMapNodeId, '00000000-0000-0000-0000-000000000000')
	WHERE	SPMN.PageMapNodeId IS NULL AND
			NULLIF(PMN.CompleteFriendlyUrl, '') IS NOT NULL

	UNION ALL

	SELECT	CTE.MasterId, PMN.PageMapNodeId, PMN.SiteId, PMN.CompleteFriendlyUrl
	FROM	dbo.PageMapNode AS PMN
			INNER JOIN MenuCTE AS CTE ON CTE.Id = PMN.SourcePageMapNodeId
)
	SELECT	CTE.SiteId, 2, CTE.Id, CTE.MasterId,
			LOWER(CTE.CompleteFriendlyUrl)
	FROM	MenuCTE AS CTE
GO

PRINT 'Create view vwObjectUrls_Pages'
GO
CREATE VIEW [dbo].[vwObjectUrls_Pages] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
WITH SCHEMABINDING
AS
WITH PageCTE (MasterId, Id, SiteId) AS (
	SELECT	PD.PageDefinitionId, PD.PageDefinitionId, PD.SiteId
	FROM	dbo.PageDefinition AS PD
			LEFT JOIN dbo.PageDefinition AS SPD ON SPD.PageDefinitionId = NULLIF(PD.SourcePageDefinitionId, '00000000-0000-0000-0000-000000000000')
	WHERE	SPD.PageDefinitionId IS NULL

	UNION ALL

	SELECT	CTE.MasterId, PD.PageDefinitionId, PD.SiteId
	FROM	dbo.PageDefinition AS PD
			INNER JOIN PageCTE AS CTE ON CTE.Id = PD.SourcePageDefinitionId
)
	SELECT	CTE.SiteId, 8, CTE.Id, CTE.MasterId,
			LOWER(PMN.CompleteFriendlyUrl + '/' + PD.FriendlyName)
	FROM	PageCTE AS CTE
			INNER JOIN dbo.PageDefinition AS PD ON PD.PageDefinitionId = CTE.Id
			INNER JOIN dbo.PageMapNodePageDef AS PMNPD ON PMNPD.PageDefinitionId = PD.PageDefinitionId
			INNER JOIN dbo.PageMapNode AS PMN ON PMN.PageMapNodeId = PMNPD.PageMapNodeId
GO

PRINT 'Create view vwObjectUrls_Products'
GO
CREATE VIEW [dbo].[vwObjectUrls_Products] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
AS
SELECT	P.SiteId, 205, P.Id, P.Id, CASE WHEN LEFT(P.ProductUrl, 1) = '/' THEN P.ProductUrl ELSE CONCAT('/', P.ProductUrl) END
FROM	vwProduct AS P
GO

PRINT 'Create view vwObjectUrls'
GO
CREATE VIEW [dbo].[vwObjectUrls] (SiteId, ObjectType, ObjectId, MasterObjectId, [Url])
AS
SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Menus

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Pages

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Blogs

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Files

UNION ALL

SELECT	SiteId, ObjectType, ObjectId, MasterObjectId, [Url]
FROM	dbo.vwObjectUrls_Products
GO

IF (OBJECT_ID('ObjectUrlDto_GetByUrl') IS NOT NULL)
	DROP PROCEDURE ObjectUrlDto_GetByUrl
GO

IF (OBJECT_ID('ObjectUrlDto_GetById') IS NOT NULL)
	DROP PROCEDURE ObjectUrlDto_GetById
GO

IF TYPE_ID('TYObjectUrlById') IS NOT NULL
	DROP TYPE [dbo].[TYObjectUrlById]
GO

IF TYPE_ID('TYObjectUrlByUrl') IS NOT NULL
	DROP TYPE [dbo].[TYObjectUrlByUrl]
GO

PRINT 'Create type TYObjectUrlById'
GO
CREATE TYPE [dbo].[TYObjectUrlById] AS TABLE
(
	SiteId		UNIQUEIDENTIFIER,
	ObjectId	UNIQUEIDENTIFIER,
	ObjectType	INT
)
GO

PRINT 'Create type TYObjectUrlByUrl'
GO
CREATE TYPE [dbo].[TYObjectUrlByUrl] AS TABLE
(
	SiteId	UNIQUEIDENTIFIER,
	[Url]	NVARCHAR(256)
)
GO

PRINT 'Create procedure ObjectUrlDto_GetById'
GO
CREATE PROCEDURE [dbo].[ObjectUrlDto_GetById]
(
	@Urls		TYObjectUrlById READONLY,
	@SiteId		UNIQUEIDENTIFIER
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ObjectUrls TABLE (
		TokenSiteId		UNIQUEIDENTIFIER,
		TokenObjectId	UNIQUEIDENTIFIER,
		TokenObjectType	INT,
		SiteId			UNIQUEIDENTIFIER,
		ObjectId		UNIQUEIDENTIFIER,
		ObjectType		INT,
		[Url]			NVARCHAR(2048)
	)

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType = 2)
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) AS SiteId,
					COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) AS ObjectId,
					U.ObjectType,
					COALESCE(OU2.[Url], OU1.[Url]) AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Menus AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Menus AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType = 2
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType = 8)
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) AS SiteId,
					COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) AS ObjectId,
					U.ObjectType,
					COALESCE(OU2.[Url], OU1.[Url]) AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Pages AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Pages AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType = 8
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType IN (9, 33))
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) AS SiteId,
					COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) AS ObjectId,
					U.ObjectType,
					COALESCE(OU2.[Url], OU1.[Url]) AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Files AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Files AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType IN (9, 33)
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType IN (39, 40))
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) AS SiteId,
					COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) AS ObjectId,
					U.ObjectType,
					COALESCE(OU2.[Url], OU1.[Url]) AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Blogs AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Blogs AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType IN (39, 40)
	END

	IF EXISTS (SELECT ObjectId FROM @Urls WHERE ObjectType = 205)
	BEGIN
		INSERT INTO @ObjectUrls (TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url])
			SELECT	U.SiteId AS TokenSiteId,
					U.ObjectId AS TokenObjectId,
					U.ObjectType as TokenObjectType,
					COALESCE(OU2.SiteId, OU1.SiteId, U.SiteId) AS SiteId,
					COALESCE(OU2.ObjectId, OU1.ObjectId, U.ObjectId) AS ObjectId,
					U.ObjectType,
					COALESCE(OU2.[Url], OU1.[Url]) AS [Url]
			FROM	@Urls AS U
					LEFT JOIN vwObjectUrls_Products AS OU1 ON (
						OU1.SiteId = U.SiteId AND
						OU1.ObjectId = U.ObjectId
					)
					LEFT JOIN vwObjectUrls_Products AS OU2 ON (
						OU2.SiteId = @SiteId AND
						OU2.MasterObjectId = OU1.MasterObjectId
					)
			WHERE	U.ObjectType = 205
	END

	SELECT	TokenSiteId, TokenObjectId, TokenObjectType, SiteId, ObjectId, ObjectType, [Url]
	FROM	@ObjectUrls
END
GO

PRINT 'Create procedure ObjectUrlDto_GetByUrl'
GO
CREATE PROCEDURE [dbo].[ObjectUrlDto_GetByUrl]
(
	@Urls	TYObjectUrlByUrl READONLY
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT	OU.SiteId, OU.ObjectId, OU.ObjectType, OU.[Url]
	FROM	@Urls AS U
			INNER JOIN vwObjectUrls_Blogs AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = U.[Url]

	UNION ALL

	SELECT	OU.SiteId, OU.ObjectId, OU.ObjectType, OU.[Url]
	FROM	@Urls AS U
			INNER JOIN vwObjectUrls_Files AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = REPLACE(U.[Url], ' ', '%20')

	UNION ALL

	SELECT	OU.SiteId, OU.Id, 2, LOWER(OU.[Url])
	FROM	@Urls AS U
			INNER JOIN vwMenuUrl AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = U.[Url]

	UNION ALL

	SELECT	OU.SiteId, OU.Id, 8, OU.[Url]
	FROM	@Urls AS U
			INNER JOIN vwSimplePageUrl AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = U.[Url]

	UNION ALL

	SELECT	OU.SiteId, OU.ObjectId, OU.ObjectType, OU.[Url]
	FROM	@Urls AS U
			INNER JOIN vwObjectUrls_Products AS OU ON OU.SiteId = U.SiteId AND OU.[Url] = U.[Url]
END
GO
PRINT 'Creating Function Form_GetVariantId'
GO
IF (OBJECT_ID('Form_GetVariantId') IS NOT NULL)
	DROP FUNCTION Form_GetVariantId
GO
CREATE FUNCTION [dbo].[Form_GetVariantId]
(
	@SourceFormId	uniqueidentifier,
	@TargetSiteId	uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @FormId uniqueidentifier, @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @SourceFormId IS NULL OR @SourceFormId = @EmptyGuid
		RETURN @FormId

	SELECT TOP 1 @FormId = Id FROM Forms
		WHERE SourceFormId = @SourceFormId AND ApplicationId = @TargetSiteId

	IF @FormId IS NULL
	BEGIN
		SELECT TOP 1 @FormId = C.Id FROM Forms P
			JOIN Forms C ON P.SourceFormId = C.SourceFormId 
				AND C.ApplicationId = @TargetSiteId AND P.Id = @SourceFormId
				AND P.SourceFormId != @EmptyGuid
	END

	IF @FormId IS NULL
	BEGIN
		SELECT TOP 1 @FormId = Id FROM vwMasterSourceFormId
		WHERE SourceFormId = @SourceFormId AND ApplicationId = @TargetSiteId
	END

	IF @FormId IS NULL
	BEGIN
		SELECT TOP 1 @FormId = C.Id FROM [vwMasterSourceFormId] P
			JOIN [vwMasterSourceFormId] C ON P.SourceFormId = C.SourceFormId
		WHERE P.Id = @SourceFormId AND C.ApplicationId = @TargetSiteId
			AND P.SourceFormId != @EmptyGuid
	END

	RETURN @FormId
END
GO
PRINT 'Creating Function Menu_GetVariantId'
GO
IF (OBJECT_ID('Menu_GetVariantId') IS NOT NULL)
	DROP FUNCTION Menu_GetVariantId
GO
CREATE FUNCTION [dbo].[Menu_GetVariantId]
(
	@SourceMenuId	uniqueidentifier,
	@TargetSiteId	uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @MenuId uniqueidentifier, @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF EXISTS(SELECT 1 FROM PageMapNode WHERE PageMapNodeId = SiteId AND PageMapNodeId = @SourceMenuId)
	BEGIN
		SET @MenuId = @TargetSiteId
	END
	ELSE
	BEGIN
		SELECT TOP 1 @MenuId = PageMapNodeId FROM PageMapNode
			WHERE SourcePageMapNodeId = @SourceMenuId AND SiteId = @TargetSiteId

		IF @MenuId IS NULL
		BEGIN
			SELECT TOP 1 @MenuId = C.PageMapNodeId FROM PageMapNode P
				JOIN PageMapNode C ON P.SourcePageMapNodeId = C.SourcePageMapNodeId 
					AND C.SiteId = @TargetSiteId AND P.PageMapNodeId = @SourceMenuId
					AND P.SourcePageMapNodeId != @EmptyGuid
		END

		IF @MenuId IS NULL
		BEGIN
			SELECT TOP 1 @MenuId = PageMapNodeId FROM vwMasterSourceMenuId
				WHERE SourcePageMapNodeId = @SourceMenuId AND SiteId = @TargetSiteId
		END

		IF @MenuId IS NULL
		BEGIN
			SELECT TOP 1 @MenuId = C.PageMapNodeId FROM [vwMasterSourceMenuId] P
				JOIN [vwMasterSourceMenuId] C ON P.SourcePageMapNodeId = C.SourcePageMapNodeId
			WHERE P.PageMapNodeId = @SourceMenuId AND C.SiteId = @TargetSiteId
				AND P.SourcePageMapNodeId != @EmptyGuid
		END
	END

	RETURN @MenuId
END
GO
PRINT 'Creating Function Page_GetVariantId'
GO
IF (OBJECT_ID('Page_GetVariantId') IS NOT NULL)
	DROP FUNCTION Page_GetVariantId
GO
CREATE FUNCTION [dbo].[Page_GetVariantId]
(
	@SourcePageId	uniqueidentifier,
	@TargetSiteId	uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @PageId uniqueidentifier, @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	SELECT TOP 1 @PageId = PageDefinitionId FROM PageDefinition
			WHERE SourcePageDefinitionId = @SourcePageId AND SiteId = @TargetSiteId

	IF @PageId IS NULL
	BEGIN
		SELECT TOP 1 @PageId = C.PageDefinitionId FROM PageDefinition P
			JOIN PageDefinition C ON P.SourcePageDefinitionId = C.SourcePageDefinitionId 
				AND C.SiteId = @TargetSiteId AND P.PageDefinitionId = @SourcePageId
				AND P.SourcePageDefinitionId != @EmptyGuid
	END

	IF @PageId IS NULL
	BEGIN
		SELECT TOP 1 @PageId = PageDefinitionId FROM vwMasterSourcePageId
			WHERE SourcePageDefinitionId = @SourcePageId AND SiteId = @TargetSiteId
	END

	IF @PageId IS NULL
	BEGIN
		SELECT TOP 1 @PageId = C.PageDefinitionId FROM [vwMasterSourcePageId] P
			JOIN [vwMasterSourcePageId] C ON P.SourcePageDefinitionId = C.SourcePageDefinitionId
		WHERE P.PageDefinitionId = @SourcePageId AND C.SiteId = @TargetSiteId
			AND P.SourcePageDefinitionId != @EmptyGuid
	END

	RETURN @PageId
END
GO
PRINT 'Creating Function Content_GetVariantId'
GO
IF (OBJECT_ID('Content_GetVariantId') IS NOT NULL)
	DROP FUNCTION Content_GetVariantId
GO
CREATE FUNCTION [dbo].[Content_GetVariantId]
(
	@SourceContentId	uniqueidentifier,
	@TargetSiteId		uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @ContentId uniqueidentifier, @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @SourceContentId IS NULL OR @SourceContentId = @EmptyGuid
		RETURN @ContentId

	SELECT TOP 1 @ContentId = Id FROM COContent
		WHERE SourceContentId = @SourceContentId AND ApplicationId = @TargetSiteId

	IF @ContentId IS NULL
	BEGIN
		SELECT TOP 1 @ContentId = C.Id FROM COContent P
			JOIN COContent C ON P.SourceContentId = C.SourceContentId 
				AND C.ApplicationId = @TargetSiteId AND P.Id = @SourceContentId
				AND P.SourceContentId != @EmptyGuid
	END

	IF @ContentId IS NULL
	BEGIN
		SELECT TOP 1 @ContentId = Id FROM vwMasterSourceContentId
			WHERE SourceContentId = @SourceContentId AND ApplicationId = @TargetSiteId
	END

	IF @ContentId IS NULL
	BEGIN
		SELECT TOP 1 @ContentId = C.Id FROM [vwMasterSourceContentId] P
			JOIN [vwMasterSourceContentId] C ON P.SourceContentId = C.SourceContentId
		WHERE P.Id = @SourceContentId AND C.ApplicationId = @TargetSiteId
			AND P.SourceContentId != @EmptyGuid
	END

	RETURN @ContentId
END
GO
PRINT 'Create procedure OrderItem_GetProductName'
GO
IF (OBJECT_ID('OrderItem_GetProductName') IS NOT NULL)
	DROP PROCEDURE OrderItem_GetProductName
GO
CREATE PROCEDURE [dbo].[OrderItem_GetProductName]
(
	@OrderItemId	uniqueidentifier
)
AS
BEGIN
	DECLARE @SiteId uniqueidentifier, @ProductId uniqueidentifier

	SELECT TOP 1 @SiteId = O.SiteId FROM OROrder O
		JOIN OROrderItem OI ON O.Id = OI.OrderId
	WHERE OI.Id = @OrderItemId

	SELECT TOP 1 @ProductId = S.ProductId FROM PRProductSKU S
		JOIN OROrderItem OI ON OI.ProductSKUId = S.Id
	WHERE OI.Id = @OrderItemId

	SELECT Title FROM vwProduct WHERE Id = @ProductId AND SiteId = @SiteId
END
GO
PRINT 'Create procedure ReturnItem_GetSkuName'
GO
IF (OBJECT_ID('ReturnItem_GetSkuName') IS NOT NULL)
	DROP PROCEDURE ReturnItem_GetSkuName
GO
CREATE PROCEDURE [dbo].[ReturnItem_GetSkuName]
(
	@ReturnItemId	uniqueidentifier
)
AS
BEGIN
	DECLARE @SiteId uniqueidentifier, @SkuId uniqueidentifier

	SELECT TOP 1 @SiteId = O.SiteId FROM OROrder O
		JOIN OROrderItem OI ON O.Id = OI.OrderId
		JOIN ORReturnItem RI ON RI.OrderItemId = OI.Id
	WHERE RI.Id = @ReturnItemId

	SELECT TOP 1 @SkuId = RI.SKUId FROM ORReturnItem RI
	WHERE RI.Id = @ReturnItemId

	SELECT Title FROM vwSKU WHERE Id = @SkuId AND SiteId = @SiteId
END
GO
PRINT 'Create procedure ShippingOption_GetActiveShippingOptions'
GO
IF (OBJECT_ID('ShippingOption_GetActiveShippingOptions') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetActiveShippingOptions
GO
CREATE PROCEDURE [dbo].[ShippingOption_GetActiveShippingOptions]
(@Id uniqueidentifier=null,@ApplicationId uniqueidentifier=null)
AS
SELECT 
	OSO.[Id],
	OSO.[ShipperId],
	OSO.[Name],
	OSO.[Description],
	OSO.[CustomId],
	OSO.[NumberOfDaysToDeliver],
	OSO.[CutOffHour],
	OSO.[SaturdayDelivery],
	OSO.[AllowShippingCoupon],
	OSO.[IsPublic],
	OSO.[IsInternational],
	OSO.[ServiceCode],
	OSO.[ExternalShipCode],
	OSO.[BaseRate],
	OSO.[SiteId],
	OSO.[Status],
	OSO.[Sequence],
	OSO.[CreatedBy],
	OSO.[CreatedDate] AS CreatedDate,
	OSO.[ModifiedBy],
	OSO.ModifiedDate AS ModifiedDate,
	1  IsActive
FROM ORShippingOption OSO
	INNER JOIN ORShipper OS ON OS.Id = OSO.ShipperId
Where --OSO.Id =isnull(@Id,OSO.Id) 
	(@Id is null or OSO.Id = @Id)
	And OS.Status= 1 AND OSO.[Status] = 1
	--And OS.SiteId =@ApplicationId
	and (@ApplicationId is null or (OSO.SiteId = @ApplicationId) OR (OSO.IsShared = 1 AND OSO.SiteId in (select SiteId from dbo.GetParentSites(@ApplicationId, 1, 1)))) 
GO
PRINT 'Create procedure ShippingOption_GetAllActiveShippingOptionsWithPrice'
GO
IF (OBJECT_ID('ShippingOption_GetAllActiveShippingOptionsWithPrice') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetAllActiveShippingOptionsWithPrice
GO
CREATE PROCEDURE [dbo].[ShippingOption_GetAllActiveShippingOptionsWithPrice]
(	
	@OrderTotal money,
	@ApplicationId uniqueidentifier=null,
	@IncludeVariantSites bit = 0,
	@ShipperId uniqueidentifier=null
)
AS
BEGIN
 IF @IncludeVariantSites IS NULL
	SET @IncludeVariantSites = 0
	Select  OSO.Id
		,OSO.ShipperId
		,OSO.Name
		,OSO.Description
		,OSO.CustomId
		,OSO.NumberOfDaysToDeliver
		,OSO.CutOffHour
		,OSO.SaturdayDelivery
		,OSO.AllowShippingCoupon
		,OSO.IsPublic
		,OSO.IsInternational
		,OSO.ServiceCode
		,OSO.[ExternalShipCode]
		,OSO.BaseRate
		,OSO.IsShared
		,OS.SiteId AS ShipperSiteId
		,OSO.SiteId	
		,OSO.Sequence
		,OSO.Status
		,OSO.CreatedBy
		,OSO.CreatedDate AS CreatedDate
		,OSO.ModifiedBy
		,OSO.ModifiedDate AS ModifiedDate
		,OSR.ShippingPrice  
	FROM ORShippingOption OSO
		INNER JOIN ORShipper OS ON OS.Id = OSO.ShipperId
		INNER JOIN ORShippingOptionRate OSR ON OSO.Id = OSR.ShippingOptionId
	Where OS.Status= 1 AND OSO.[Status] = 1  AND
			@OrderTotal between OSR.StartAmount and 
					CASE OSR.EndAmount when 0 then  922337203685477.5807 
										else OSR.EndAmount end
	AND (@ApplicationId is null OR (@IncludeVariantSites = 0 AND OSO.[SiteId] = @ApplicationId) OR (@IncludeVariantSites = 1 AND OSO.SiteId in (select SiteId from dbo.GetVariantSites(@ApplicationId))))
	AND (@ShipperId is null OR OS.Id = @ShipperId)
	ORDER BY OSR.ShippingPrice ASC
END
GO
PRINT 'Create procedure ShippingOption_Save'
GO
IF (OBJECT_ID('ShippingOption_Save') IS NOT NULL)
	DROP PROCEDURE ShippingOption_Save
GO
CREATE PROCEDURE [dbo].[ShippingOption_Save]
(
	@Id uniqueidentifier output,
	@ApplicationId uniqueidentifier =null,
	@ShipperId uniqueidentifier,
	@Name nvarchar(255),
	@Description nvarchar(1024)= null,
	@CustomId nvarchar(50) =null,
	@NumberOfDaysToDeliver int =null,
	@CutOffHour int =null,
	@SaturdayDelivery tinyint =null,
	@AllowShippingCoupon tinyint =null,
    @IsPublic tinyint =null,
    @IsInternational tinyint =null,
    @ServiceCode nvarchar(100) =null,
	@ExternalShipCode nvarchar(100) = null,
    @BaseRate money =null,
    @Status int =null,
	@Sequence int =null,
	@IsShared bit =null,
    @CreatedBy uniqueidentifier,
    @ModifiedBy uniqueidentifier =null 
)
as
begin
	DECLARE	@NewId	uniqueidentifier
	DECLARE @Now dateTime
	DECLARE @Stmt VARCHAR(36)	
	DECLARE @Error int		
	
	SET @Now = getutcdate()
	
	if(@Id is null)
	begin
		set @NewId = newid()
		set @Id  =@NewId	
		Declare @currentSequence int     
		Select @currentSequence = Isnull(max(Sequence),0) from ORShippingOption where ShipperId=@ShipperId 

		SET @Stmt = 'Insert ShippingOption'
		INSERT INTO [ORShippingOption]
           ([Id]
           ,[ShipperId]
           ,[Name]
           ,[Description]
           ,[CustomId]
           ,[NumberOfDaysToDeliver]
           ,[CutOffHour]
           ,[SaturdayDelivery]
           ,[AllowShippingCoupon]
           ,[IsPublic]
           ,[IsInternational]
           ,[ServiceCode]
		   ,[ExternalShipCode]
           ,[BaseRate]
           ,[SiteId]
           ,[Status]
           ,[IsShared]
		   ,[Sequence]
           ,[CreatedBy]
           ,[CreatedDate])
           
     VALUES
           (@NewId
           ,@ShipperId
           ,@Name
           ,@Description      
           ,@CustomId
           ,@NumberOfDaysToDeliver
           ,@CutOffHour
           ,@SaturdayDelivery
           ,@AllowShippingCoupon
           ,@IsPublic
           ,@IsInternational
           ,@ServiceCode
		   ,@ExternalShipCode
           ,@BaseRate
           ,@ApplicationId
           ,@Status
           ,@IsShared
		   ,@currentSequence+1
           ,@CreatedBy
           ,@Now
           )	

			SELECT @Error = @@ERROR
 			IF @Error <> 0
			BEGIN
				RAISERROR('DBERROR||%s',16,1,@Stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END
	end
	else
	begin
		IF (NOT EXISTS(SELECT 1 FROM  ORShippingOption WHERE  Id = @Id))
		BEGIN			
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END	

		SET @Stmt = 'Update Shipper'
		UPDATE [ORShippingOption]
		   SET [ShipperId] = @ShipperId
			  ,[Name] = @Name
			  ,[Description] = @Description
			  ,[CustomId] = @CustomId
			  ,[NumberOfDaysToDeliver] = @NumberOfDaysToDeliver
			  ,[CutOffHour] = @CutOffHour
			  ,[SaturdayDelivery] = @SaturdayDelivery
			  ,[AllowShippingCoupon] = @AllowShippingCoupon
			  ,[IsPublic] = @IsPublic
			  ,[IsInternational] = @IsInternational
			  ,[ServiceCode] = @ServiceCode
			  ,[ExternalShipCode] = @ExternalShipCode
			  ,[BaseRate] = @BaseRate
			  ,[SiteId] = @ApplicationId
			  ,[Sequence] = @Sequence
			  ,[Status] = @Status			 
			  ,[IsShared] = @IsShared			 
			  ,[ModifiedBy] = @ModifiedBy
			  ,[ModifiedDate] = @Now
		 WHERE Id=@Id
		
		SELECT @Error = @@ERROR
 		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END			  
	end
end
GO
PRINT 'Create procedure Product_GetEffectivePriceVariant'
GO
IF (OBJECT_ID('Product_GetEffectivePriceVariant') IS NOT NULL)
	DROP PROCEDURE Product_GetEffectivePriceVariant
GO
CREATE PROCEDURE [dbo].[Product_GetEffectivePriceVariant](
			 @CustomerId uniqueidentifier = null,
			 @Quantity float = 1,
			 @ProductId uniqueidentifier = null,
			 @ProductIdList Xml = null,
			 @ProductIdQuantityList XML =null,
			 @ApplicationId uniqueidentifier=null
										)

AS

BEGIN
--<dictionary><SerializableDictionary><item><key><guid>75610a9c-cc9d-4da5-893a-05fd0056d7cd</guid></key><value><double>2</double></value></item><item><key><guid>c15dd7bf-ce90-4a7b-be78-20b24324957e</guid></key><value><double>1</double></value></item><item><key><guid>1cc48359-eec5-44b4-9518-0c06398f5b92</guid></key><value><double>1</double></value></item><item><key><guid>0ae206dc-1b24-4f75-a174-56cdb59d5346</guid></key><value><double>2</double></value></item></SerializableDictionary></dictionary>
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)
IF @ProductIdQuantityList is null
BEGIN

	if @ProductIdList is not null
		begin
				Insert Into @tempProductTable(ProductId)
					Select tab.col.value('text()[1]','uniqueidentifier')
					FROM @ProductIdList.nodes('/ProductCollection/Product')tab(col)
		end
	else if(@ProductId is not null)
		begin
			
			Insert Into @tempProductTable(ProductId)
			Select @ProductId

		end

	if @@ROWCOUNT = 0 
		BEGIN
			RAISERROR ( 'No Product Id supplied' , 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			
			 Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId' and 
				(@ApplicationId is null or STS.SiteId = @ApplicationId))
				--STS.SiteId=IsNull(@ApplicationId,STS.SiteId))

			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId, IsSale ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join @tempProductTable inProduct on PSC.ProductId = inProduct.ProductId
				Where 
					CGPS.CustomerGroupId = @DefaultGroupId and 
					ps.SiteId = @ApplicationId and
					((@Quantity is null and MinQuantity <= 1 and MaxQuantity >= 1) OR
						(@Quantity is not null and MinQuantity <= @Quantity and MaxQuantity >= @Quantity))
					--(MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))	
					AND @Today Between PS.StartDate AND PS.EndDate 
					
			) EFPS
			WHERE EFPS.Sno=1	
		End
	else
		Begin
			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId, IsSale ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					inner join @tempProductTable inProduct on PSC.ProductId = inProduct.ProductId
				Where USMG.MemberId = @CustomerId  and
					ps.SiteId = @ApplicationId 
					and ((@Quantity is null and MinQuantity <= 1 and MaxQuantity >= 1) OR
						(@Quantity is not null and MinQuantity <= @Quantity and MaxQuantity >= @Quantity))
					--and (MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))		
					AND @Today Between PS.StartDate AND PS.EndDate 		
			) EFPS
			WHERE EFPS.Sno=1				
		End
		-- in any scenario - return List Price for all the SKUs		
		Select prSku.ProductId, prSku.Id as SkuId, ISNULL(prSkuVariant.ListPrice,prSku.ListPrice) ListPrice
			From PRProductSKU prSku  LEFT JOIN PRProductSKUVariantCache prSkuVariant ON prSku.Id = prSkuVariant.Id and prSkuVariant.SiteId= @ApplicationId
			where prSku.ProductId in(Select ProductId from @tempProductTable)
	
	END
	ELSE
	BEGIN
--<dictionary><SerializableDictionary><item><key><guid>75610a9c-cc9d-4da5-893a-05fd0056d7cd</guid></key><value><double>2</double></value></item><item><key><guid>c15dd7bf-ce90-4a7b-be78-20b24324957e</guid></key><value><double>1</double></value></item><item><key><guid>1cc48359-eec5-44b4-9518-0c06398f5b92</guid></key><value><double>1</double></value></item><item><key><guid>0ae206dc-1b24-4f75-a174-56cdb59d5346</guid></key><value><double>2</double></value></item></SerializableDictionary></dictionary>
	Declare @tableproductIdQnty	table(Sno int identity(1,1),ProductId uniqueidentifier,Quantity decimal(18,2))
	--Declare @ProductIdQuantityList XML 
	--Set @ProductIdQuantityList ='<dictionary><SerializableDictionary><item><key><guid>75610a9c-cc9d-4da5-893a-05fd0056d7cd</guid></key><value><double>2</double></value></item><item><key><guid>c15dd7bf-ce90-4a7b-be78-20b24324957e</guid></key><value><double>1</double></value></item><item><key><guid>1cc48359-eec5-44b4-9518-0c06398f5b92</guid></key><value><double>1</double></value></item><item><key><guid>0ae206dc-1b24-4f75-a174-56cdb59d5346</guid></key><value><double>2</double></value></item></SerializableDictionary></dictionary>'
	Insert into @tableproductIdQnty(ProductId,Quantity)
	Select tab.col.value('(key/guid/text())[1]','uniqueidentifier'),tab.col.value('(value/double/text())[1]','decimal(18,2)')
	From @ProductIdQuantityList.nodes('dictionary/SerializableDictionary/item') tab(col)
	
	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId' and 
					(@ApplicationId is null or STS.SiteId = @ApplicationId))
					--STS.SiteId=IsNull(@ApplicationId,STS.SiteId))

			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join @tableproductIdQnty inProduct on PSC.ProductId = inProduct.ProductId AND inProduct.Quantity between minQuantity and maxQuantity
				Where 
					CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					--and (MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))		
					AND @Today Between PS.StartDate AND PS.EndDate 		
			) EFPS
			WHERE EFPS.Sno=1	
		End
		else
		Begin
			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					inner join @tableproductIdQnty inProduct on PSC.ProductId = inProduct.ProductId AND inProduct.Quantity between minQuantity and maxQuantity
				Where USMG.MemberId = @CustomerId 
				and PS.SiteId = @ApplicationId 
					--and (MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))		
					AND @Today Between PS.StartDate AND PS.EndDate 		
			) EFPS
			WHERE EFPS.Sno=1				
		End
-- in any scenario - return List Price for all the SKUs		
		Select prSku.ProductId, prSku.Id as SkuId, ISNULL(prSkuVariant.ListPrice,prSku.ListPrice) ListPrice
			From PRProductSKU prSku  LEFT JOIN PRProductSKUVariantCache prSkuVariant ON prSku.Id = prSkuVariant.Id AND prSku.SiteId =@ApplicationId
			where prSku.ProductId in(Select ProductId from @tableproductIdQnty)
	
	END

END
GO
PRINT 'Create procedure Product_GetMinPrice'
GO
IF (OBJECT_ID('Product_GetMinPrice') IS NOT NULL)
	DROP PROCEDURE Product_GetMinPrice
GO
CREATE PROCEDURE [dbo].[Product_GetMinPrice](
			 @CustomerId uniqueidentifier = null,
			 @IdQuantity  TYIdNumber READONLY,
			 @ApplicationId uniqueidentifier=null
			)
AS

BEGIN
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId')

			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice,ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY P.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @IdQuantity IQ
				INNER JOIN  PRProduct P ON IQ.Id =P.Id
				INNER JOIN PRProductSKU S on P.Id = S.ProductId
				LEFT JOIN PRProductSKUVariantCache SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					Where CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= S.Id AND IQ.Number between minQuantity and maxQuantity
					Where P.Id=IQ.Id
				
			) EFPS
			WHERE EFPS.Sno=1

		End
		else
		Begin
			Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice,ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @IdQuantity IQ
				INNER JOIN  PRProduct P ON IQ.Id =P.Id
				INNER JOIN PRProductSKU S on P.Id = S.ProductId
				LEFT JOIN PRProductSKUVariantCache SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					Where USMG.MemberId = @CustomerId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= S.Id AND IQ.Number between minQuantity and maxQuantity
					Where P.Id =IQ.Id					
		End
END
GO
PRINT 'Create procedure Sku_GetPrice'
GO
IF (OBJECT_ID('Sku_GetPrice') IS NOT NULL)
	DROP PROCEDURE Sku_GetPrice
GO
CREATE PROCEDURE [dbo].[Sku_GetPrice](
			 @CustomerId uniqueidentifier = null,
			 @SKUQuantity  TYSKUQuantity READONLY,
			 @ApplicationId uniqueidentifier=null
										)
AS

BEGIN
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId')

			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale,  ISNULL(PST.EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY S.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @SKUQuantity inSKU 
				INNER JOIN PRProductSKU S on inSKU.SKUId = S.Id
				LEFT JOIN PRProductSKUVariantCache SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					Where CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= inSKU.SKUId AND inSKU.Quantity between minQuantity and maxQuantity
			) EFPS
			WHERE EFPS.Sno=1				

		End
		else
		Begin
			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY S.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @SKUQuantity inSKU 
				INNER JOIN PRProductSKU S on inSKU.SKUId = S.Id
				LEFT JOIN PRProductSKUVariantCache SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					Where USMG.MemberId = @CustomerId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= inSKU.SKUId AND inSKU.Quantity between minQuantity and maxQuantity
			) EFPS
			WHERE EFPS.Sno=1				
		End
END
GO
PRINT 'Create procedure FormDto_Import'
GO
IF (OBJECT_ID('FormDto_Import') IS NOT NULL)
	DROP PROCEDURE FormDto_Import
GO
CREATE PROCEDURE [dbo].[FormDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @ParentId uniqueidentifier

	SET @UtcNow = GETUTCDATE()
	SET @Id = (SELECT [dbo].[Form_GetVariantId](@SourceId, @TargetSiteId))

	IF EXISTS (SELECT 1 FROM SISite WHERE Id = @TargetSiteId AND (TranslationEnabled = 1 OR SubmitToTranslation = 1))
		SELECT TOP 1 @ParentId = ParentId FROM Forms WHERE Id = @SourceId
	
	IF @ParentId IS NULL OR @ParentId = dbo.GetEmptyGUID()
		SELECT TOP 1 @ParentId = Id FROM COFormStructure WHERE Title = 'Distributed Forms' AND SiteId = @TargetSiteId

	IF @ParentId IS NULL
		RAISERROR ('Shared Forms folder does not exists.', 16, 1, '')

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()

		INSERT INTO Forms 
		(
			Id,
			ApplicationId,
			Title,
			Description,
			FormType,
			XMLString,
			XSLTString,
			XSLTFileName,
			Status,
			CreatedBy,
			CreatedDate,
			ThankYouURL,
			Email,
			SendEmailYesNo,
			SubmitButtonText,
			PollResultType,
			PollVotingType,
			AddAsContact,
			ContactEmailField,
			WatchId,
			EmailSubjectText,
			EmailSubjectField,
			SourceFormId,
			ParentId
		)
		SELECT
			@Id,
			@TargetSiteId,
			Title,
			Description,
			FormType,
			XMLString,
			XSLTString,
			XSLTFileName,
			Status,
			ISNULL(ModifiedBy, CreatedBy),
			@UtcNow,
			ThankYouURL,
			Email,
			SendEmailYesNo,
			SubmitButtonText,
			PollResultType,
			PollVotingType,
			AddAsContact,
			ContactEmailField,
			WatchId,
			EmailSubjectText,
			EmailSubjectField,
			Id,
			@ParentId
		FROM Forms
		WHERE Id = @SourceId
	END
	ELSE
	BEGIN
		UPDATE C SET
			C.Title = P.Title,
			C.Description = P.Description,
			C.FormType = P.FormType,
			C.XMLString = P.XMLString,
			C.XSLTString = P.XSLTString,
			C.XSLTFileName = P.XSLTFileName,
			C.ThankYouURL = P.ThankYouURL,
			C.Email = P.Email,
			C.SendEmailYesNo = P.SendEmailYesNo,
			C.Status = P.Status,
			C.SubmitButtonText = P.SubmitButtonText,
			C.PollResultType = P.PollResultType,
			C.PollVotingType = P.PollVotingType,
			C.AddAsContact = P.AddAsContact,
			C.ContactEmailField = P.ContactEmailField,
			C.WatchId = P.WatchId,
			C.EmailSubjectText = P.EmailSubjectText,
			C.EmailSubjectField = P.EmailSubjectField,
			C.ModifiedBy = ISNULL(P.ModifiedBy, P.CreatedBy),
			C.ModifiedDate = @UtcNow
		FROM Forms P, Forms C
		WHERE P.Id = @SourceId
			AND C.Id = @Id
	END	
END
GO
PRINT 'Create procedure ContentDto_GetVariantId'
GO
IF (OBJECT_ID('ContentDto_GetVariantId') IS NOT NULL)
	DROP PROCEDURE ContentDto_GetVariantId
GO
CREATE PROCEDURE [dbo].[ContentDto_GetVariantId]
(
	@SourceContentId	uniqueidentifier,
	@TargetSiteId		uniqueidentifier
)
AS
BEGIN
	SELECT [dbo].[Content_GetVariantId](@SourceContentId, @TargetSiteId)
END
GO
PRINT 'Create procedure Site_CreateMicrositeDataForCommerce'
GO
IF (OBJECT_ID('Site_CreateMicrositeDataForCommerce') IS NOT NULL)
	DROP PROCEDURE Site_CreateMicrositeDataForCommerce
GO
CREATE PROCEDURE [dbo].[Site_CreateMicrositeDataForCommerce]
(
	@MasterSiteId UNIQUEIDENTIFIER,
	@MicroSiteId UNIQUEIDENTIFIER
)
AS
BEGIN
	
	Declare @MasterProductTypePageMapNodeId uniqueidentifier 
	Declare @MicroSiteProductTypePageMapNodeId uniqueidentifier 

	Declare @SettingTypeId int
	Select @SettingTypeId = Id From STSettingType Where Name = 'ProductTypePageMapNodeId'

	Set @MasterProductTypePageMapNodeId = NULL
	Select @MasterProductTypePageMapNodeId = CAST(ST.Value AS UNIQUEIDENTIFIER)  From 
	STSiteSetting ST where SettingTypeId = @SettingTypeId
	and ST.SiteId =@MasterSiteId 


	select @MicroSiteProductTypePageMapNodeId =  P1.PageMapNodeId from PageMapNode P 
	INNER JOIN PageMapNode P1 ON P.PageMapNodeId = P1.SourcePageMapNodeId  and P1.SiteId = @MicroSiteId
	Where P.PageMapNodeId = @MasterProductTypePageMapNodeId

	--Set the ProductTypePageMapNodeId for the variant site in site settings
	IF (@MicroSiteProductTypePageMapNodeId IS NOT NULL)
		INSERT INTO [dbo].[STSiteSetting]
			   ([SiteId]
			   ,[SettingTypeId]
			   ,[Value])
			   SELECT @MicroSiteId, @SettingTypeId, @MicroSiteProductTypePageMapNodeId

		-- Create product Type in Variant site and populate with correct CMS PageId	
		IF EXISTS(Select 1 From PRProductType WHERE SIteId = @MasterSiteId)
			INSERT INTO PRProductTypeVariant (Id, SiteId, CMSPageId)
			SELECT PT.Id, @MicroSiteId, P1.PageDefinitionId FROM PRProductType PT
			LEFT JOIN PageDefinition P ON PT.CMSPageId = P.PageDefinitionId 
			LEFT JOIN PageDefinition P1 ON P.PageDefinitionId = P1.SourcePageDefinitionId and P1.SiteId = @MicroSiteId
			WHERE PT.SiteId = @MasterSiteId
		ELSE
			INSERT INTO PRProductTypeVariant (Id, SiteId, CMSPageId)
			SELECT PT.Id, @MicroSiteId, P1.PageDefinitionId FROM PRProductTypeVariant PT
			LEFT JOIN PageDefinition P ON PT.CMSPageId = P.PageDefinitionId 
			LEFT JOIN PageDefinition P1 ON P.PageDefinitionId = P1.SourcePageDefinitionId and P1.SiteId = @MicroSiteId
			WHERE PT.SiteId = @MasterSiteId

			--Inserting into PRProductTypeVariantCache table
			INSERT INTO [dbo].[PRProductTypeVariantCache]
				   ([Id]
				   ,[SiteId]
				   ,[Title]
				   ,[Description]
				   ,[FriendlyName]
				   ,[CMSPageId]
				   ,[ModifiedDate]
				   ,[ModifiedBy]
				   ,[Status])
			SELECT P.Id,
				PV.SiteId,
				P.Title,
				P.Description,
				P.FriendlyName,
				PV.CMSPageId,
				P.ModifiedDate,
				P.ModifiedBy,
				P.Status
			FROM PRProductTypeVariant PV
			INNER JOIN PRProductType P ON PV.Id = P.Id AND PV.SiteId = @MicroSiteId

	--Inserting into PRProductVariantCache
	INSERT INTO [dbo].[PRProductVariantCache]
           ([Id]
           ,[SiteId]
           ,[Title]
           ,[UrlFriendlyTitle]
           ,[ShortDescription]
           ,[LongDescription]
           ,[Description]
           ,[Keyword]
           ,[IsActive]
           ,[ProductTypeID]
           ,[ProductStyle]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[TaxCategoryID]
           ,[PromoteAsNewItem]
           ,[PromoteAsTopSeller]
           ,[ExcludeFromDiscounts]
           ,[ExcludeFromShippingPromotions]
           ,[Freight]
           ,[Refundable]
           ,[TaxExempt]
		   ,[SEOTitle]
           ,[SEOH1]
           ,[SEODescription]
           ,[SEOKeywords]
           ,[SEOFriendlyUrl])
     SELECT DISTINCT P.Id,
		@MicroSiteId,
		P.Title,
		P.UrlFriendlyTitle,
		P.ShortDescription,
		P.LongDescription,
		P.Description,
		P.Keyword,
		P.IsActive,
		P.ProductTypeID,
		P.ProductStyle,
		P.ModifiedDate,
		P.ModifiedById,
		P.TaxCategoryID,
		P.PromoteAsNewItem,
		P.PromoteAsTopSeller,
		P.ExcludeFromDiscounts,
		P.ExcludeFromShippingPromotions,
		P.Freight,
		P.Refundable,
		P.TaxExempt,
		P.SEOTitle,
		P.SEOH1,
		P.SEODescription,
		P.SEOKeywords,
		P.SEOFriendlyUrl
	FROM vwProduct P where P.IsShared = 1 AND P.SiteId = @MasterSiteId


	--Inserting Product SKU Variant cache
	INSERT INTO [dbo].[PRProductSKUVariantCache]
           ([Id]
           ,[SiteId]
           ,[Title]
           ,[Description]
           ,[Sequence]
           ,[IsActive]
           ,[IsOnline]
           ,[ListPrice]
           ,[UnitCost]
           ,[WholesalePrice]
           ,[PreviousSoldCount]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Measure]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[HandlingCharges]
           ,[SelfShippable]
           ,[AvailableForBackOrder]
           ,[BackOrderLimit]
           ,[IsUnlimitedQuantity]
           ,[MaximumDiscountPercentage]
           ,[FreeShipping]
           ,[UserDefinedPrice])
		SELECT DISTINCT P.Id,
			@MicroSiteId,
			P.Title,
			P.Description,
			P.Sequence,
			P.IsActive,
			P.IsOnline,
			P.ListPrice,
			P.UnitCost,
			P.WholesalePrice,
			P.PreviousSoldCount,
			P.ModifiedDate,
			P.ModifiedBy,
			P.Measure,
			P.OrderMinimum,
			P.OrderIncrement,
			P.HandlingCharges,
			P.SelfShippable,
			P.AvailableForBackOrder,
			P.BackOrderLimit,
			P.IsUnlimitedQuantity,
			P.MaximumDiscountPercentage,
			P.FreeShipping,
			P.UserDefinedPrice
		FROM vwSKU P 
		INNER JOIN vwProduct VP ON VP.Id = P.ProductId 
		WHERE P.SiteId = @MasterSiteId and VP.IsShared = 1


	-- Inserting into Attribute cache	
	INSERT INTO [dbo].[ATAttributeVariantCache]
           ([Id]
           ,[SiteId]
           ,[Title]
           ,[Description]
           ,[ModifiedDate]
           ,[ModifiedBy])
		SELECT DISTINCT A.Id,
			@MicroSiteId,
			A.Title,
			A.Description,
			A.ModifiedDate,
			A.ModifiedBy
		FROM vwAttribute A WHERE A.SiteId = @MasterSiteId


	-- Inserting into Product Attribute value cache	
	INSERT INTO [dbo].[PRProductAttributeValueVariantCache]
           ([Id]
           ,[SiteId]
           ,[ProductId]
           ,[AttributeId]
           ,[Value]
           ,[AttributeEnumId]
           ,[ModifiedDate]
           ,[ModifiedBy])
		SELECT A.Id,
			@MicroSiteId,
			A.ObjectId,
			A.AttributeId,
			A.Value,
			A.AttributeEnumId,
			A.CreatedDate,
			A.CreatedBy
		FROM vwProductAttributeValue A
		WHERE A.SiteId = @MasterSiteId


	-- Inserting into Product Attribute SKU value cache	
	INSERT INTO [dbo].[PRProductSKUAttributeValueVariantCache]
           ([Id]
           ,[SiteId]
           ,[SkuId]
           ,[AttributeId]
           ,[Value]
           ,[AttributeEnumId]
           ,[ModifiedDate]
           ,[ModifiedBy])
    	SELECT A.Id,
		@MicroSiteId,
		A.ObjectId,
		A.AttributeId,
		A.Value,
		A.AttributeEnumId,
		A.CreatedDate,
		A.CreatedBy
		FROM vwSkuAttributeValue A
		WHERE A.SiteId = @MasterSiteId
		
		
	-- Inserting into Attribute Enum cache	
		INSERT INTO [dbo].[ATAttributeEnumVariantCache]
		([Id]
		,[SiteId]
		,[AttributeId]
		,[Title]
		,[ModifiedDate]
		,[ModifiedBy])
		SELECT A.[Id]
		  ,@MicroSiteId
		  ,A.AttributeId
		  ,A.Title
		  ,ModifiedDate
		  ,ModifiedBy
		FROM vwAttributeEnum A  WHERE A.SiteId = @MasterSiteId

		Declare @ImageType uniqueidentifier
		Select @ImageType = Id from CLImageType where Title = 'main'

		--Inserting in image cache table and only the main image and the shared copy. 
		INSERT INTO [dbo].[CLImageVariantCache]
           ([Id]
           ,[SiteId]
           ,[Title]
           ,[Description]
           ,[AltText]
           ,[ImageType]
           ,[Status]
           ,[ModifiedDate]
           ,[ModifiedBy])
	SELECT DISTINCT [Id]
      ,@MicroSiteId
      ,[Title]
      ,[Description]
      ,[AltText]
      ,@ImageType
      ,[Status]
      ,[ModifiedDate]
      ,[ModifiedBy]
    FROM vwProductImage where SiteId = @MasterSiteId and ParentImage IS NULL and IsShared = 1

	--Inserting in Product Media Cache
	INSERT INTO [dbo].[PRProductMediaVariantCache]
           ([Id]
           ,[SiteId]
           ,[Title]
           ,[Description]
           ,[FileName]
           ,[MaxNumDownloads]
           ,[MaxDaysAvailable]
           ,[ExpirationDate]
           ,[Status]
           ,[Size]
           ,[ModifiedBy]
           ,[ModifiedDate])
     SELECT
           Id 
           ,@MicroSiteId
           ,Title
           ,Description
           ,FileName
           ,MaxNumDownloads
           ,MaxDaysAvailable
           ,ExpirationDate
           ,Status
           ,Size
           ,ModifiedBy
           ,ModifiedDate
		   FROM vwProductMedia where SiteId = @MasterSiteId

		--Inserting in FFShippingContainer for variant site 
		INSERT INTO [dbo].[FFShippingContainer]
			   ([Id]
			   ,[Title]
			   ,[Description]
			   ,[Length]
			   ,[Width]
			   ,[Height]
			   ,[Weight]
			   ,[MaxWeight]
			   ,[ShipperId]
			   ,[BoxCode]
			   ,[SiteId]
			   ,[Sequence]
			   ,[PackingPreference]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[MaxValue])
		SELECT NEWID()
			  ,[Title]
			  ,[Description]
			  ,[Length]
			  ,[Width]
			  ,[Height]
			  ,[Weight]
			  ,[MaxWeight]
			  ,[ShipperId]
			  ,[BoxCode]
			  ,@MicroSiteId
			  ,[Sequence]
			  ,[PackingPreference]
			  ,[Status]
			  ,[CreatedBy]
			  ,GETUTCDATE()
			  ,[ModifiedBy]
			  ,GETUTCDATE()
			  ,[MaxValue]
		  FROM [dbo].[FFShippingContainer]
		Where SiteId=@MasterSiteId

		--Inserting in [PTPaymentTypeSite] for variant site 
		INSERT INTO [dbo].[PTPaymentTypeSite]
           ([PaymentTypeId]
           ,[SiteId]
           ,[Status])
		Select 
           PaymentTypeId
           ,@MicroSiteId
           ,Status
		   From PTPaymentTypeSite where SIteId = @MasterSiteId

		   --Inserting in [[PTGatewaySite]] for variant site 
		   INSERT INTO [dbo].[PTGatewaySite]
           ([GatewayId]
           ,[SiteId]
           ,[IsActive]
           ,[PaymentTypeId])
     Select 
           GatewayId
           ,@MicroSiteId
           ,IsActive
           ,PaymentTypeId
	From PTGatewaySite where SIteId = @MasterSiteId

	--Inserting in Email Template Cache
	INSERT INTO TAEmailTemplateVariantCache (Id, SiteId, Title, [Description], [Subject], Body, SenderDefaultEmail, MailMergeTags, ModifiedBy, ModifiedDate)
		SELECT	ET.Id, @MicroSiteId, ET.Title, ET.[Description], ET.[Subject], Body, ET.SenderDefaultEmail, ET.MailMergeTags, ET.ModifiedBy, ET.ModifiedDate
		FROM	vwEmailTemplate AS ET
		WHERE	ET.SiteId = @MasterSiteId

	--Inserting in Facet Variant Cache
	INSERT INTO ATFacetVariantCache (Id, SiteId, Title, [Description], ModifiedBy, ModifiedDate)
		SELECT	F.Id, @MicroSiteId, F.Title, F.[Description], F.ModifiedBy, F.ModifiedDate
		FROM	vwFacet AS F
		WHERE	F.SiteId = @MasterSiteId

	--Inserting in Facet Range Variant Cache
	INSERT INTO ATFacetRangeVariantCache (Id, SiteId, DisplayText, AlternateText, ModifiedBy, ModifiedDate)
		SELECT	FR.Id, @MicroSiteId, FR.DisplayText, FR.AlternateText, FR.ModifiedBy, FR.ModifiedDate
		FROM	vwFacetRange AS FR
		WHERE	FR.SiteId = @MasterSiteId
END
GO
PRINT 'Create procedure BLD_UpdateVariantCacheTables'
GO
IF (OBJECT_ID('BLD_UpdateVariantCacheTables') IS NOT NULL)
	DROP PROCEDURE BLD_UpdateVariantCacheTables
GO
CREATE PROCEDURE BLD_UpdateVariantCacheTables
(
	@MicroSiteId uniqueidentifier,--Variant Site ID for which cache table has to be populated
	@ProductTypeIds as SiteTableType READONLY,--Product Type IDs which are imported and cache has to be updated 
	@ProductIds AS SiteTableType READONLY, -- Product IDs which are imported and cache has to be updated 
	@AttributeIds AS SiteTableType READONLY, -- AttributeIds which are imported and cache has to be updated 
	@AttributeEnumIds AS SiteTableType READONLY, -- AttributeEnumIds which are imported and cache has to be updated 
	@ProductAttributeIds AS SiteTableType READONLY, -- ProductAttributeIds which are imported and cache has to be updated 
	@SkuAttributeIds AS SiteTableType READONLY, -- SkuAttributeIds which are imported and cache has to be updated 
	@ImageIds AS SiteTableType READONLY, -- ImageIds which are imported and cache has to be updated 
	@ProductMediaIds AS SiteTableType READONLY -- Product MediaIDs which are imported and cache has to be updated 
)
AS
BEGIN
/*
Test Execution script
Declare @ImageIds AS SiteTableType
	INSERT INTO @ImageIds
		VALUES ('FE4D86C7-CD4D-4C4D-8722-082D643FDABC')
	INSERT INTO @ImageIds
		VALUES ('F0206817-43A9-46C8-8E99-93AFB20C957D')
exec BLD_UpdateVariantCacheTables @MicroSiteId = '66BA57F4-0B0D-41D7-BE33-EE7517F93F8F' , @ImageIds=@ImageIds

*/
SET NOCOUNT ON;

Declare @ParentSiteId uniqueidentifier
Select @ParentSiteId = ParentSiteId from SISIte where Id = @MicroSiteId

IF (@MicroSiteId is null or @ParentSiteId is null)
BEGIN
			RAISERROR(15600, -1, -1, 'BLD_UpdateVariantCacheTables @MicroSiteId or @ParentSiteId is null')
			Return
END


BEGIN /* Populating PRProductTypeVariantCache table  */
	PRINT 'Product Type Cache Started for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

	--Updating the records which are imported and already in PRProductTypeVariantCache
	
	UPDATE PRProductTypeVariantCache
		  SET 
			Title = ISNULL(AV.Title, V.ProductTypeName)
			,Description =ISNULL(AV.Description, V.Description) 
			,FriendlyName = ISNULL(AV.FriendlyName, V.FriendlyName)
			,CMSPageId = ISNULL(AV.CMSPageId, V.CMSPageId) 
			,ModifiedDate = ISNULL(AV.ModifiedDate, V.ModifiedDate)
			,ModifiedBy = ISNULL(AV.ModifiedBy, V.ModifiedBy)
			FROM vwProductTypeImport V
		   INNER JOIN @ProductTypeIds AID ON V.ProductTypeId = AID.ObjectId
		   LEFT JOIN PRProductTypeVariant AV ON AV.Id = V.ProductTypeId AND AV.SiteId = @MicroSiteId
		   INNER JOIN PRProductTypeVariantCache AVC ON AVC.Id = V.ProductTypeId AND AVC.SiteId = @MicroSiteId
		   WHERE V.SiteId = @ParentSiteId  

		INSERT INTO [dbo].[PRProductTypeVariantCache]
				   ([Id]
				   ,[SiteId]
				   ,[Title]
				   ,[Description]
				   ,[FriendlyName]
				   ,[CMSPageId]
				   ,[ModifiedDate]
				   ,[ModifiedBy]
				   ,[Status])
			 SELECT 
				   V.ProductTypeId 
				   ,@MicroSiteId
				   ,ISNULL(AV.Title, V.ProductTypeName)
				   ,ISNULL(AV.Description, V.Description) 
				   ,ISNULL(AV.FriendlyName, V.FriendlyName)
				   ,ISNULL(AV.CMSPageId, V.CMSPageId) 
				   ,ISNULL(AV.ModifiedDate, V.ModifiedDate)
				   ,ISNULL(AV.ModifiedBy, V.ModifiedBy)
				   ,ISNULL(AV.Status, V.Status)
				   FROM vwProductTypeImport V
				   INNER JOIN @ProductTypeIds AID ON V.ProductTypeId = AID.ObjectId
				   LEFT JOIN PRProductTypeVariant AV ON AV.Id = V.ProductTypeId AND AV.SiteId = @MicroSiteId
				   LEFT JOIN PRProductTypeVariantCache AVC ON AVC.Id = AID.ObjectId AND AVC.SiteId = @MicroSiteId
				   WHERE V.SiteId = @ParentSiteId   AND AVC.Id IS NULL

				   PRINT 'Product Type Cache Completed for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)
END


BEGIN /* Populating PRProductVariantCache table  */
	PRINT 'Product Cache Started for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

	--Updating the records which are imported and already in PRProductVariantCache
	UPDATE [PRProductVariantCache]
          Set [Title] = ISNULL(PV.Title, P.Title)
           ,[UrlFriendlyTitle] = ISNULL(PV.UrlFriendlyTitle, P.UrlFriendlyTitle)
           ,[ShortDescription] = ISNULL(PV.ShortDescription, P.ShortDescription)
           ,[LongDescription] = ISNULL(PV.LongDescription, P.LongDescription)
           ,[Description] = ISNULL(PV.Description, P.Description)
           ,[Keyword] = ISNULL(PV.Keyword, P.Keyword)
           ,[IsActive] = ISNULL(PV.IsActive, P.IsActive)
           ,[ProductTypeID] = ISNULL(PV.ProductTypeID, P.ProductTypeID)
           ,[ProductStyle] = ISNULL(PV.ProductStyle, P.ProductStyle)
           ,[ModifiedDate] = ISNULL(PV.ModifiedDate, P.ModifiedDate)
           ,[ModifiedBy] = ISNULL(PV.ModifiedBy, P.ModifiedById)
           ,[TaxCategoryID] = ISNULL(PV.TaxCategoryID, P.TaxCategoryID)
           ,[PromoteAsNewItem] = ISNULL(PV.PromoteAsNewItem, P.PromoteAsNewItem)
           ,[PromoteAsTopSeller] = ISNULL(PV.PromoteAsTopSeller, P.PromoteAsTopSeller)
           ,[ExcludeFromDiscounts] = ISNULL(PV.ExcludeFromDiscounts, P.ExcludeFromDiscounts)
           ,[ExcludeFromShippingPromotions] = ISNULL(PV.ExcludeFromShippingPromotions, P.ExcludeFromShippingPromotions)
           ,[Freight] = ISNULL(PV.Freight, P.Freight)
           ,[Refundable] = ISNULL(PV.Refundable, P.Refundable)
           ,[TaxExempt] = ISNULL(PV.TaxExempt, P.TaxExempt)
		   ,[SEOTitle] = ISNULL(PV.SEOTitle, P.SEOTitle)
           ,[SEOH1] = ISNULL(PV.SEOH1, P.SEOH1)
           ,[SEODescription] = ISNULL(PV.SEODescription, P.SEODescription)
           ,[SEOKeywords] = ISNULL(PV.SEOKeywords, P.SEOKeywords)
           ,[SEOFriendlyUrl] = ISNULL(PV.SEOFriendlyUrl, P.SEOFriendlyUrl)
		   FROM vwProduct P
		   INNER JOIN @ProductIds PID ON  PID.ObjectId = P.Id 
		   LEFT JOIN PRProductVariant PV ON P.Id = PV.Id AND PV.SiteId = @MicroSiteId
		   INNER JOIN PRProductVariantCache PRV ON PRV.Id = P.Id AND PRV.SiteId = @MicroSiteId
		   WHERE P.IsShared = 1 and P.SiteId = @ParentSiteId


	--Inserting the records which are imported and NOT in PRProductVariantCache
	INSERT INTO [dbo].[PRProductVariantCache]
           ([Id]
           ,[SiteId]
           ,[Title]
           ,[UrlFriendlyTitle]
           ,[ShortDescription]
           ,[LongDescription]
           ,[Description]
           ,[Keyword]
           ,[IsActive]
           ,[ProductTypeID]
           ,[ProductStyle]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[TaxCategoryID]
           ,[PromoteAsNewItem]
           ,[PromoteAsTopSeller]
           ,[ExcludeFromDiscounts]
           ,[ExcludeFromShippingPromotions]
           ,[Freight]
           ,[Refundable]
           ,[TaxExempt]
           ,[SEOTitle]
		   ,[SEOH1]
           ,[SEODescription]
           ,[SEOKeywords]
           ,[SEOFriendlyUrl])
     SELECT DISTINCT
            P.Id
           ,@MicroSiteId
           ,ISNULL(PV.Title, P.Title)
           ,ISNULL(PV.UrlFriendlyTitle, P.UrlFriendlyTitle)
           ,ISNULL(PV.ShortDescription, P.ShortDescription)
           ,ISNULL(PV.LongDescription, P.LongDescription)
           ,ISNULL(PV.Description, P.Description)
           ,ISNULL(PV.Keyword, P.Keyword)
           ,ISNULL(PV.IsActive, P.IsActive)
           ,ISNULL(PV.ProductTypeID, P.ProductTypeID)
           ,ISNULL(PV.ProductStyle, P.ProductStyle)
           ,ISNULL(PV.ModifiedDate, P.ModifiedDate)
           ,ISNULL(PV.ModifiedBy, P.ModifiedById)
           ,ISNULL(PV.TaxCategoryID, P.TaxCategoryID)
           ,ISNULL(PV.PromoteAsNewItem, P.PromoteAsNewItem)
           ,ISNULL(PV.PromoteAsTopSeller, P.PromoteAsTopSeller)
           ,ISNULL(PV.ExcludeFromDiscounts, P.ExcludeFromDiscounts)
           ,ISNULL(PV.ExcludeFromShippingPromotions, P.ExcludeFromShippingPromotions)
           ,ISNULL(PV.Freight, P.Freight)
           ,ISNULL(PV.Refundable, P.Refundable)
           ,ISNULL(PV.TaxExempt, P.TaxExempt)
           ,ISNULL(PV.SEOTitle, P.SEOTitle)
		   ,ISNULL(PV.SEOH1, P.SEOH1)
           ,ISNULL(PV.SEODescription, P.SEODescription)
           ,ISNULL(PV.SEOKeywords, P.SEOKeywords)
           ,ISNULL(PV.SEOFriendlyUrl, P.SEOFriendlyUrl)
		   FROM vwProduct P
		   INNER JOIN @ProductIds PID ON  PID.ObjectId = P.Id 
		   LEFT JOIN PRProductVariant PV ON P.Id = PV.Id AND PV.SiteId = @MicroSiteId
		   LEFT JOIN PRProductVariantCache PRV ON PRV.Id = PID.ObjectId AND PRV.SiteId = @MicroSiteId
		   WHERE P.IsShared = 1 and P.SiteId = @ParentSiteId AND PRV.Id IS NULL

	
	PRINT 'Product Cache completed for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)
END

BEGIN /* Populating PRProductSKUVariantCache table  */

	PRINT 'SKU Cache Started for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

	--Updating the records which are imported and already in PRProductSKUVariantCache
	UPDATE PRProductSKUVariantCache 
	SET
	    Title = ISNULL(PSV.Title, S.Title)
        ,Description = ISNULL(PSV.Description, S.Description)
        ,Sequence = ISNULL(PSV.Sequence, S.Sequence)
        ,IsActive = ISNULL(PSV.IsActive, S.IsActive)
        ,IsOnline = ISNULL(PSV.IsOnline, S.IsOnline)
        ,ListPrice = ISNULL(PSV.ListPrice, S.ListPrice)
        ,UnitCost = ISNULL(PSV.UnitCost, S.UnitCost)
        ,WholesalePrice = ISNULL(PSV.WholesalePrice, S.WholesalePrice)
        ,PreviousSoldCount = ISNULL(PSV.PreviousSoldCount, S.PreviousSoldCount)
        ,ModifiedDate = ISNULL(PSV.ModifiedDate, S.ModifiedDate)
        ,ModifiedBy = ISNULL(PSV.ModifiedBy, S.ModifiedBy)
        ,Measure = ISNULL(PSV.Measure, S.Measure)
        ,OrderMinimum = ISNULL(PSV.OrderMinimum, S.OrderMinimum)
        ,OrderIncrement = ISNULL(PSV.OrderIncrement, S.OrderIncrement)
        ,HandlingCharges = ISNULL(PSV.HandlingCharges, S.HandlingCharges)
        ,SelfShippable = ISNULL(PSV.SelfShippable, S.SelfShippable)
        ,AvailableForBackOrder = ISNULL(PSV.AvailableForBackOrder, S.AvailableForBackOrder)
        ,BackOrderLimit = ISNULL(PSV.BackOrderLimit, S.BackOrderLimit)
        ,IsUnlimitedQuantity = ISNULL(PSV.IsUnlimitedQuantity, S.IsUnlimitedQuantity)
        ,MaximumDiscountPercentage = ISNULL(PSV.MaximumDiscountPercentage, S.MaximumDiscountPercentage)
        ,FreeShipping = ISNULL(PSV.FreeShipping, S.FreeShipping)
        ,UserDefinedPrice = ISNULL(PSV.UserDefinedPrice, S.UserDefinedPrice)
		FROM vwSKU S
		   INNER JOIN PRProductSKU PS ON PS.Id = S.Id 
		   INNER JOIN PRProduct P ON P.Id = PS.ProductId
		   INNER JOIN @ProductIds PID ON P.Id = PID.ObjectId
		   LEFT JOIN PRProductSKUVariant PSV ON PSV.Id = S.Id AND PSV.SiteId = @MicroSiteId
		   INNER JOIN PRProductSKUVariantCache PSVC ON PSVC.Id = S.Id AND PSVC.SiteId = @MicroSiteId
		   WHERE S.SiteId = @ParentSiteId AND P.IsShared = 1


	--Inserting the records which are imported and NOT in PRProductSKUVariantCache
	INSERT INTO [dbo].[PRProductSKUVariantCache]
           ([Id]
           ,[SiteId]
           ,[Title]
           ,[Description]
           ,[Sequence]
           ,[IsActive]
           ,[IsOnline]
           ,[ListPrice]
           ,[UnitCost]
           ,[WholesalePrice]
           ,[PreviousSoldCount]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Measure]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[HandlingCharges]
           ,[SelfShippable]
           ,[AvailableForBackOrder]
           ,[BackOrderLimit]
           ,[IsUnlimitedQuantity]
           ,[MaximumDiscountPercentage]
           ,[FreeShipping]
           ,[UserDefinedPrice])
     SELECT DISTINCT
            S.Id
           ,@MicroSiteId
           ,ISNULL(PSV.Title, S.Title)
           ,ISNULL(PSV.Description, S.Description)
           ,ISNULL(PSV.Sequence, S.Sequence)
           ,ISNULL(PSV.IsActive, S.IsActive)
           ,ISNULL(PSV.IsOnline, S.IsOnline)
           ,ISNULL(PSV.ListPrice, S.ListPrice)
           ,ISNULL(PSV.UnitCost, S.UnitCost)
           ,ISNULL(PSV.WholesalePrice, S.WholesalePrice)
           ,ISNULL(PSV.PreviousSoldCount, S.PreviousSoldCount)
           ,ISNULL(PSV.ModifiedDate, S.ModifiedDate)
           ,ISNULL(PSV.ModifiedBy, S.ModifiedBy)
           ,ISNULL(PSV.Measure, S.Measure)
           ,ISNULL(PSV.OrderMinimum, S.OrderMinimum)
           ,ISNULL(PSV.OrderIncrement, S.OrderIncrement)
           ,ISNULL(PSV.HandlingCharges, S.HandlingCharges)
           ,ISNULL(PSV.SelfShippable, S.SelfShippable)
           ,ISNULL(PSV.AvailableForBackOrder, S.AvailableForBackOrder)
           ,ISNULL(PSV.BackOrderLimit, S.BackOrderLimit)
           ,ISNULL(PSV.IsUnlimitedQuantity, S.IsUnlimitedQuantity)
           ,ISNULL(PSV.MaximumDiscountPercentage, S.MaximumDiscountPercentage)
           ,ISNULL(PSV.FreeShipping, S.FreeShipping)
           ,ISNULL(PSV.UserDefinedPrice, S.UserDefinedPrice)
		   FROM vwSKU S
		   INNER JOIN PRProductSKU PS ON PS.Id = S.Id 
		   INNER JOIN PRProduct P ON P.Id = PS.ProductId
		   INNER JOIN @ProductIds PID ON P.Id = PID.ObjectId
		   LEFT JOIN PRProductSKUVariant PSV ON PSV.Id = S.Id AND PSV.SiteId = @MicroSiteId
		   LEFT JOIN PRProductSKUVariantCache PSVC ON PSVC.Id = S.Id AND PSVC.SiteId = @MicroSiteId
		   WHERE S.SiteId = @ParentSiteId AND P.IsShared = 1 AND PSVC.Id IS NULL
		   

	PRINT 'SKU Cache Completed for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)
END

BEGIN /* Populating ATAttributeVariantCache table  */

	PRINT 'Attribute Cache Started for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)
	
	--Updating the records which are imported and already in ATAttributeVariantCache
	  UPDATE ATAttributeVariantCache
		  SET 
			 Title = ISNULL(AV.Title, V.Title)
			,Description = ISNULL(AV.Description, V.Description)
			,ModifiedDate = ISNULL(AV.ModifiedDate, V.ModifiedDate)
			,ModifiedBy = ISNULL(AV.ModifiedBy, V.ModifiedBy)
		 FROM vwAttribute V
			   INNER JOIN @AttributeIds AID ON V.Id = AID.ObjectId
			   LEFT JOIN ATAttributeVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
			   INNER JOIN ATAttributeVariantCache AVC ON AVC.Id = V.Id AND AVC.SiteId = @MicroSiteId
			   WHERE V.SiteId = @ParentSiteId 

	--Inserting the records which are imported and NOT in ATAttributeVariantCache
	INSERT INTO [dbo].[ATAttributeVariantCache]
			   ([Id]
			   ,[SiteId]
			   ,[Title]
			   ,[Description]
			   ,[ModifiedDate]
			   ,[ModifiedBy])
		 Select Distinct
			   V.Id
			   ,@MicroSiteId
			   ,ISNULL(AV.Title, V.Title)
			   ,ISNULL(AV.Description, V.Description)
			   ,ISNULL(AV.ModifiedDate, V.ModifiedDate)
			   ,ISNULL(AV.ModifiedBy, V.ModifiedBy)
			 FROM vwAttribute V
		   INNER JOIN @AttributeIds AID ON V.Id = AID.ObjectId
		   LEFT JOIN ATAttributeVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
		   LEFT JOIN ATAttributeVariantCache AVC ON AVC.Id = AID.ObjectId AND AVC.SiteId = @MicroSiteId
		   WHERE V.SiteId = @ParentSiteId  AND AVC.Id IS NULL	

	PRINT 'Attribute Cache Completed for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

END

BEGIN /* Populating ATAttributeEnumVariantCache table  */

	PRINT 'Attribute Enum Cache Started for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

	  --Updating the records which are imported and already in ATAttributeEnumVariantCache
	  UPDATE ATAttributeEnumVariantCache
		  SET 
			AttributeId = ISNULL(AV.AttributeId, V.AttributeId)
			,Title = ISNULL(AV.Title, V.Title)
			,ModifiedDate = ISNULL(AV.ModifiedDate, V.ModifiedDate)
			,ModifiedBy = ISNULL(AV.ModifiedBy, V.ModifiedBy)
		 FROM vwAttributeEnum V
		   INNER JOIN @AttributeEnumIds AID ON V.Id = AID.ObjectId
		   LEFT JOIN ATAttributeEnumVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
		   INNER JOIN ATAttributeEnumVariantCache AVC ON AVC.Id = V.Id AND AVC.SiteId = @MicroSiteId
		   WHERE V.SiteId = @ParentSiteId  


		--Inserting the records which are imported and NOT in ATAttributeEnumVariantCache
		INSERT INTO [dbo].[ATAttributeEnumVariantCache]
			   ([Id]
			   ,[SiteId]
			   ,[AttributeId]
			   ,[Title]
			   ,[ModifiedDate]
			   ,[ModifiedBy])
		 Select Distinct
			   V.Id
			   ,@MicroSiteId
			   ,ISNULL(AV.AttributeId, V.AttributeId)
			   ,ISNULL(AV.Title, V.Title)
			   ,ISNULL(AV.ModifiedDate, V.ModifiedDate)
			   ,ISNULL(AV.ModifiedBy, V.ModifiedBy)
			 FROM vwAttributeEnum V
		   INNER JOIN @AttributeEnumIds AID ON V.Id = AID.ObjectId
		   LEFT JOIN ATAttributeEnumVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
		   LEFT JOIN ATAttributeEnumVariantCache AVC ON AVC.Id = AID.ObjectId AND AVC.SiteId = @MicroSiteId
		   WHERE V.SiteId = @ParentSiteId AND AVC.Id IS NULL

	PRINT 'Attribute Enum Cache Completed for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)
	
END	

BEGIN /* Populating PRProductAttributeValueVariantCache table  */

	  PRINT 'Product Attribute Cache Started for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

	   --Updating the records which are imported and already in PRProductAttributeValueVariantCache
	  UPDATE PRProductAttributeValueVariantCache
		  SET 
			ProductId = ISNULL(AV.ProductId, V.ObjectId)
			,AttributeId =  ISNULL(AV.AttributeId, V.AttributeId)
			,Value = ISNULL(AV.Value, V.Value)
			,AttributeEnumId = ISNULL(AV.AttributeEnumId, V.AttributeEnumId)
			,ModifiedDate = ISNULL(AV.ModifiedDate, V.CreatedDate)
			,ModifiedBy = ISNULL(AV.ModifiedBy, V.CreatedBy)
		  FROM vwProductAttributeValue V
		   INNER JOIN @ProductAttributeIds AID ON V.Id = AID.ObjectId
		   LEFT JOIN PRProductAttributeValueVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
		   INNER JOIN PRProductAttributeValueVariantCache AVC ON AVC.Id = V.Id AND AVC.SiteId = @MicroSiteId
		   WHERE V.SiteId = @ParentSiteId  



		 --Inserting the records which are imported and NOT in PRProductAttributeValueVariantCache
		INSERT INTO [dbo].[PRProductAttributeValueVariantCache]
				   ([Id]
				   ,[SiteId]
				   ,[ProductId]
				   ,[AttributeId]
				   ,[Value]
				   ,[AttributeEnumId]
				   ,[ModifiedDate]
				   ,[ModifiedBy])
			 Select Distinct
				   V.Id
				   ,@MicroSiteId
				   ,ISNULL(AV.ProductId, V.ObjectId)
				   ,ISNULL(AV.AttributeId, V.AttributeId)
				   ,ISNULL(AV.Value, V.Value)
				   ,ISNULL(AV.AttributeEnumId, V.AttributeEnumId)
				   ,ISNULL(AV.ModifiedDate, V.CreatedDate)
				   ,ISNULL(AV.ModifiedBy, V.CreatedBy)
				   FROM vwProductAttributeValue V
				   INNER JOIN @ProductAttributeIds AID ON V.Id = AID.ObjectId
				   LEFT JOIN PRProductAttributeValueVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
				   LEFT JOIN PRProductAttributeValueVariantCache AVC ON AVC.Id = AID.ObjectId AND AVC.SiteId = @MicroSiteId
				   WHERE V.SiteId = @ParentSiteId AND AVC.Id IS NULL

	

	 PRINT 'Product Attribute Cache Completed for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

END			   

BEGIN /* Populating PRProductSKUAttributeValueVariantCache table  */
	
	PRINT 'Product Attribute SKU Cache Started for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

	   --Updating the records which are imported and already in PRProductSKUAttributeValueVariantCache
	  UPDATE PRProductSKUAttributeValueVariantCache
		  SET 
			SkuId = ISNULL(AV.SkuId, V.ObjectId)
			,AttributeId =  ISNULL(AV.AttributeId, V.AttributeId)
			,Value = ISNULL(AV.Value, V.Value)
			,AttributeEnumId = ISNULL(AV.AttributeEnumId, V.AttributeEnumId)
			,ModifiedDate = ISNULL(AV.ModifiedDate, V.CreatedDate)
			,ModifiedBy = ISNULL(AV.ModifiedBy, V.CreatedBy)
			FROM vwSkuAttributeValue V
		   INNER JOIN @SkuAttributeIds AID ON V.Id = AID.ObjectId
		   LEFT JOIN PRProductSKUAttributeValueVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
		   INNER JOIN PRProductSKUAttributeValueVariantCache AVC ON AVC.Id = V.Id AND AVC.SiteId = @MicroSiteId
		   WHERE V.SiteId = @ParentSiteId 

		   --Inserting the records which are imported and NOT in PRProductSKUAttributeValueVariantCache
		INSERT INTO [dbo].[PRProductSKUAttributeValueVariantCache]
				   ([Id]
				   ,[SiteId]
				   ,[SkuId]
				   ,[AttributeId]
				   ,[Value]
				   ,[AttributeEnumId]
				   ,[ModifiedDate]
				   ,[ModifiedBy])
			 Select Distinct
				   V.Id
				   ,@MicroSiteId
				   ,ISNULL(AV.SkuId, V.ObjectId)
				   ,ISNULL(AV.AttributeId, V.AttributeId)
				   ,ISNULL(AV.Value, V.Value)
				   ,ISNULL(AV.AttributeEnumId, V.AttributeEnumId)
				   ,ISNULL(AV.ModifiedDate, V.CreatedDate)
				   ,ISNULL(AV.ModifiedBy, V.CreatedBy)
			 FROM vwSkuAttributeValue V
			 INNER JOIN @SkuAttributeIds AID ON V.Id = AID.ObjectId
		     LEFT JOIN PRProductSKUAttributeValueVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
		     LEFT JOIN PRProductSKUAttributeValueVariantCache AVC ON AVC.Id = AID.ObjectId AND AVC.SiteId = @MicroSiteId
		     WHERE V.SiteId = @ParentSiteId AND AVC.Id IS NULL

	PRINT 'Product Attribute SKU Cache Completed for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)
	
END	

BEGIN /* Populating CLImageVariantCache table  */

	PRINT 'Image Cache Started for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

	   --Updating the records which are imported and already in CLImageVariantCache		
	  UPDATE CLImageVariantCache
		  SET 
			Title = ISNULL(AV.Title, V.Title)
			,Description = ISNULL(AV.Description, V.Description)
			,AltText = ISNULL(AV.AltText, V.AltText)
			,ImageType = AV.ImageType
			,Status = ISNULL(AV.Status, V.Status)
			,ModifiedDate = ISNULL(AV.ModifiedDate, V.ModifiedDate)
			,ModifiedBy = ISNULL(AV.ModifiedBy, V.ModifiedBy)
			FROM vwProductImage V
			INNER JOIN @ImageIds AID ON V.Id = AID.ObjectId
			LEFT JOIN CLImageVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
			INNER JOIN CLImageVariantCache AVC ON AVC.Id = V.Id AND AVC.SiteId = @MicroSiteId
			WHERE V.SiteId = @ParentSiteId 

		--Inserting the records which are imported and NOT in CLImageVariantCache		
		INSERT INTO [dbo].[CLImageVariantCache]
			   ([Id]
			   ,[SiteId]
			   ,[Title]
			   ,[Description]
			   ,[AltText]
			   ,[ImageType]
			   ,[Status]
			   ,[ModifiedDate]
			   ,[ModifiedBy])
			 SELECT DISTINCT
				   V.Id
				   ,@MicroSiteId
				   ,ISNULL(AV.Title, V.Title)
				   ,ISNULL(AV.Description, V.Description)
				   ,ISNULL(AV.AltText, V.AltText)
				   ,AV.ImageType
				   ,ISNULL(AV.Status, V.Status)
				   ,ISNULL(AV.ModifiedDate, V.ModifiedDate)
				   ,ISNULL(AV.ModifiedBy, V.ModifiedBy)
			FROM vwProductImage V
		   INNER JOIN @ImageIds AID ON V.Id = AID.ObjectId
		   LEFT JOIN CLImageVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
		   LEFT JOIN CLImageVariantCache AVC ON AVC.Id = AID.ObjectId AND AVC.SiteId = @MicroSiteId
		   WHERE V.SiteId = @ParentSiteId  AND AVC.Id IS NULL

	PRINT 'Image Cache Completd for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

END	

BEGIN /* Populating [PRProductMediaVariantCache] table  */

	PRINT 'Product Media Cache Started for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)

	   --Updating the records which are imported and already in [PRProductMediaVariantCache]		
	UPDATE [PRProductMediaVariantCache]
		  SET 
           Title = ISNULL(AV.Title, V.Title)
           ,Description = ISNULL(AV.Description, V.Description)
           ,FileName = ISNULL(AV.FileName, V.FileName)
           ,MaxNumDownloads = ISNULL(AV.MaxNumDownloads, V.MaxNumDownloads)
           ,MaxDaysAvailable = ISNULL(AV.MaxDaysAvailable, V.MaxDaysAvailable)
           ,ExpirationDate = ISNULL(AV.ExpirationDate, V.ExpirationDate)
           ,Status = ISNULL(AV.Status, V.Status)
           ,Size = ISNULL(AV.Size, V.Size)
           ,ModifiedBy = ISNULL(AV.ModifiedBy, V.ModifiedBy)
           ,ModifiedDate = ISNULL(AV.ModifiedDate, V.ModifiedDate)
		   FROM vwProductMedia V
		   INNER JOIN @ProductMediaIds AID ON V.Id = AID.ObjectId
		   LEFT JOIN PRProductMediaVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
		   INNER JOIN PRProductMediaVariantCache AVC ON AVC.Id = V.Id AND AVC.SiteId = @MicroSiteId
		   WHERE V.SiteId = @ParentSiteId 

		   --Inserting the records which are imported and Not in [PRProductMediaVariantCache]	
		INSERT INTO [dbo].[PRProductMediaVariantCache]
           ([Id]
           ,[SiteId]
           ,[Title]
           ,[Description]
           ,[FileName]
           ,[MaxNumDownloads]
           ,[MaxDaysAvailable]
           ,[ExpirationDate]
           ,[Status]
           ,[Size]
           ,[ModifiedBy]
           ,[ModifiedDate])
     SELECT DISTINCT
           V.Id 
           ,@MicroSiteId
           ,ISNULL(AV.Title, V.Title)
           ,ISNULL(AV.Description, V.Description)
           ,ISNULL(AV.FileName, V.FileName)
           ,ISNULL(AV.MaxNumDownloads, V.MaxNumDownloads)
           ,ISNULL(AV.MaxDaysAvailable, V.MaxDaysAvailable)
           ,ISNULL(AV.ExpirationDate, V.ExpirationDate)
           ,ISNULL(AV.Status, V.Status)
           ,ISNULL(AV.Size, V.Size)
           ,ISNULL(AV.ModifiedBy, V.ModifiedBy)
           ,ISNULL(AV.ModifiedDate, V.ModifiedDate)
		   FROM vwProductMedia V
		   INNER JOIN @ProductMediaIds AID ON V.Id = AID.ObjectId
		   LEFT JOIN PRProductMediaVariant AV ON AV.Id = V.Id AND AV.SiteId = @MicroSiteId
		   LEFT JOIN PRProductMediaVariantCache AVC ON AVC.Id = AID.ObjectId AND AVC.SiteId = @MicroSiteId
		   WHERE V.SiteId = @ParentSiteId AND AVC.Id IS NULL

		PRINT 'Product Media Cache Completed for Site: ' + CONVERT(nvarchar(36), @MicroSiteId)	
END

END
GO
PRINT 'Create procedure Content_GetContentsByDictionary'
GO
IF (OBJECT_ID('Content_GetContentsByDictionary') IS NOT NULL)
	DROP PROCEDURE Content_GetContentsByDictionary
GO
CREATE PROCEDURE [dbo].[Content_GetContentsByDictionary] 
--********************************************************************************
--Parameters
--********************************************************************************
(
@xmlGuids xml,
@ThemeId uniqueidentifier = null,
@ApplicationId uniqueidentifier = null,
@ResolveThemeId bit = 0
)
AS
BEGIN

	IF(@ThemeId IS NOT NULL AND @ResolveThemeId = 1)
			SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 13) 

	DECLARE @tbContentIds TABLE (Id uniqueidentifier, ContentTypeId int)
	INSERT INTO @tbContentIds
	SELECT DISTINCT T.c.value('@Id','uniqueidentifier'), T.c.value('@ContentTypeID','int')
		FROM @xmlGuids.nodes('/Contents/Content') T(c)
		
	DECLARE @tbParentFolder TABLE(ContentId uniqueidentifier primary key, ParentId uniqueidentifier, FolderPath nvarchar(1000), ContentTypeID int)
	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COContentStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3

	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COImageStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3


	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COFileStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3
		
	DECLARE @tbContentDefinitionIds TABLE(TemplateId uniqueidentifier, SiteId uniqueidentifier)
	INSERT INTO @tbContentDefinitionIds
		SELECT DISTINCT XMLString.value('(//contentDefinition/@TemplateId)[1]','uniqueidentifier'), C.ApplicationId  
			FROM COContent C INNER JOIN @tbParentFolder t on C.Id = t.ContentId 
		WHERE ObjectTypeId = 13
			
	
	SELECT Id,
		ApplicationId,
		Title,
		Description, 
		[Text], 
		URL,
		URLType, 
		ObjectTypeId, 
		CreatedDate,
		CreatedBy,
		ModifiedBy,
		ModifiedDate, 
		Status,
		RestoreDate, 
		BackupDate, 
		StatusChangedDate,
		PublishDate, 
		ArchiveDate, 
		XMLString,
		BinaryObject, 
		Keywords,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		OrderNo, SourceContentId,
		PA.ParentId ,
		PA.FolderPath,
		CO.TemplateId
	FROM COContent CO
		INNER JOIN @tbParentFolder PA ON CO.Id = PA.ContentId AND 
			(pa.ContentTypeID = 7 or pa.ContentTypeID = 13 ) and Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = CO.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = CO.ModifiedBy
	Order by OrderNo

	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ExcludeFromExternalSearch,            
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		C.ParentId,
		PA.VirtualPath  
	FROM COContent C 
		INNER JOIN COFile F ON C.Id = F.ContentId
		INNER JOIN @tbContentIds T ON T.Id = C.Id
		INNER JOIN COFileStructure PA ON C.ParentId = PA.Id	
				AND C.ObjectTypeId = 9 AND C.Status = 1
		LEFT JOIN VW_UserFullName FN ON FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN ON MN.UserId = C.ModifiedBy
	Order by C.OrderNo
		
	IF(@ThemeId IS NOT NULL AND @ThemeId <> dbo.GetEmptyGUID())
	BEGIN
	SELECT  xF.Id,
			xF.ApplicationId,
			xF.Name,
			xF.XMLString,
			CASE WHEN @ThemeId IS NULL THEN xF.XsltString ELSE ISNULL(OT.CodeFile,xF.XsltString) END AS XsltString, 
			--ISNULL(VX.XsltString, xF.XsltString) As XsltString,
			xF.Status,
			xF.Description,
			xF.XMLFileName,
			CASE WHEN @ThemeId IS NULL THEN xF.XSLTFileName ELSE ISNULL(OT.FileName,xF.XSLTFileName) END AS XSLTFileName, 
			CASE WHEN @ThemeId IS NULL THEN xF.CreatedDate ELSE ISNULL(OT.CreatedDate,xF.CreatedDate) END AS CreatedDate, 
			CASE WHEN @ThemeId IS NULL THEN xF.CreatedBy ELSE ISNULL(OT.CreatedBy,xF.CreatedBy) END AS CreatedBy, 
			CASE WHEN @ThemeId IS NULL THEN xF.ModifiedDate ELSE ISNULL(OT.ModifiedDate,xF.ModifiedDate) END AS ModifiedDate, 
			 CASE WHEN @ThemeId IS NULL THEN xF.ModifiedBy ELSE ISNULL(OT.ModifiedBy,xF.ModifiedBy) END AS ModifiedBy,  
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			OT.ThemeId,
			xF.ImageUrl,
			xf.AmpEnabled,
			xf.AmpView
	FROM COXMLForm xF
		INNER JOIN @tbContentDefinitionIds cD  On xF.Id  = cD.templateId
		LEFT JOIN VW_UserFullName FN on FN.UserId = xF.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = xF.ModifiedBy
		LEFT JOIN COXMLFormXslt VX on VX.SiteId = cD.SiteId And VX.XmlFormId = cD.templateId
		LEFT JOIN SIObjectTheme OT ON OT.ObjectId = xF.Id AND OT.ObjectTypeId = 13
		AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId = @ThemeId)
		
	END
	ELSE
	BEGIN
		SELECT  xF.Id,
			xF.ApplicationId,
			xF.Name,
			xF.XMLString,
			ISNULL(VX.XsltString, xF.XsltString) As XsltString,
			xF.Status,
			xF.Description,
			xF.XMLFileName,
			xF.XSLTFileName, xF.CreatedDate, xF.CreatedBy, 
			xF.ModifiedDate, xF.ModifiedBy, 
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			xF.ImageUrl,
			xf.AmpEnabled,
			xf.AmpView
	FROM COXMLForm xF
		INNER JOIN @tbContentDefinitionIds cD  On xF.Id  = cD.templateId
		LEFT JOIN VW_UserFullName FN on FN.UserId = xF.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = xF.ModifiedBy
		LEFT JOIN COXMLFormXslt VX on VX.SiteId = cD.SiteId And VX.XmlFormId = cD.templateId AND VX.Status != 3
	END
	
	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ExcludeFromExternalSearch,
		F.ImageHeight, F.ImageWidth,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo,	C.SourceContentId,
		C.ParentId,
		PA.VirtualPath  
	FROM COContent C 
		INNER JOIN COFile F On C.Id = F.ContentId
		INNER JOIN @tbContentIds T ON T.Id = C.Id
		INNER JOIN COImageStructure PA ON C.ParentId = PA.Id
				AND C.ObjectTypeId = 33 AND C.Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
	ORDER BY C.OrderNo
		
	SELECT C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ExcludeFromExternalSearch,
		F.ImageHeight, F.ImageWidth,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		dbo.GetEmptyGUID() ParentId ,
		'' FolderPath,
		V.BackgroundColor,
		V.DynamicStreaming,
		V.Height,
		V.IsUI,
		V.IsVideo,
		V.PlayerId,
		V.PlayerKey,
		V.VideoId,
		V.PlaylistId,
		V.VideoType,
		V.Width  
	FROM COContent C 
		INNER JOIN COFile F On C.Id = F.ContentId
		INNER JOIN COVideo V on V.ContentId=C.Id
		INNER JOIN @tbContentIds T ON T.Id = C.Id
				AND C.Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
	ORDER BY C.OrderNo
END
GO
PRINT 'Create procedure CommerceCacheDto_Invalidate'
GO
IF (OBJECT_ID('CommerceCacheDto_Invalidate') IS NOT NULL)
	DROP PROCEDURE CommerceCacheDto_Invalidate
GO
CREATE PROCEDURE [dbo].[CommerceCacheDto_Invalidate]
(
	@Key		nvarchar(500),
	@SiteId		uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	IF @Key = 'InvalidateNavFilter' OR @Key = 'All'
	BEGIN
		UPDATE NVFilterQuery SET IsLatest = 0 

		UPDATE GLCacheUpdate SET NavFilterCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END
	
	IF @Key = 'InvalidateFacet' OR @Key = 'All'
	BEGIN
		EXEC Facet_ReloadProductFacetCache

		UPDATE GLCacheUpdate SET FacetCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END
	
	IF @Key = 'InvalidateFeatureOutput' OR @Key = 'All'
	BEGIN
		EXEC Feature_PopulateFeatureOutput @ApplicationId = @SiteId

		UPDATE GLCacheUpdate SET FeatureOutputCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END
	
	IF @Key = 'InvalidatePriceSet' OR @Key = 'All'
	BEGIN
		EXEC Cache_RefreshFlatPriceSets
		EXEC Cache_RefreshProductMinEffectivePrice @ApplicationId = @SiteId

		UPDATE GLCacheUpdate SET PriceSetCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END

	IF @Key = 'Application' OR @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET ApplicationCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END

	IF @Key = 'ApplicationBaseData' OR @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET ApplicationBaseDataCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END

	IF @Key = 'SiteSettings' OR @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET SiteSettingsCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END

	IF @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET AllCacheDate = @UtcNow --WHERE ApplicationId = @SiteId
	END
END
GO
PRINT 'Create procedure BLD_UpdateTranslationDocuments'
GO
IF (OBJECT_ID('BLD_UpdateTranslationDocuments') IS NOT NULL)
	DROP PROCEDURE BLD_UpdateTranslationDocuments
GO
CREATE PROCEDURE BLD_UpdateTranslationDocuments
(
	@ObjectIds		SiteTableType READONLY,
	@TargetId		uniqueidentifier, 
	@TargetTypeId	int, 
	@SiteId			uniqueidentifier,
	@Locale			nvarchar(10)
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, 
		@ListType int
	SET @UtcNow = GETUTCDATE()

	DECLARE @tbDocument TABLE (
		Id uniqueidentifier, 
		TranslationId uniqueidentifier,
		Title nvarchar(max),
		ModifiedBy uniqueidentifier
	)

	INSERT INTO @tbDocument
	SELECT NEWID(),
		T.Id,
		T.Title,
		ISNULL(T.ModifiedBy, T.CreatedBy)
	FROM TLTranslation T
		JOIN @ObjectIds C ON T.ObjectId = C.ObjectId
	WHERE T.SiteId = @SiteId

	INSERT INTO TLDocument
	(
		Id,
		TranslationId,
		Locale,
		Title,
		Status,
		ProcessingStatus,
		CreatedBy,
		CreatedDate
	)
	SELECT Id,
		TranslationId,
		@Locale,
		Title,
		2,
		1,
		ModifiedBy,
		@UtcNow
	FROM @tbDocument

	INSERT INTO TLDocumentTarget
	(	
		Id,
		TranslationDocumentId,
		TargetId,
		TargetTypeId
	)
	SELECT NEWID(),
		D.Id,
		@TargetId,
		@TargetTypeId
	FROM @tbDocument D
END
GO
PRINT 'Create procedure BLD_UpdateTranslationTables'
GO
IF (OBJECT_ID('BLD_UpdateTranslationTables') IS NOT NULL)
	DROP PROCEDURE BLD_UpdateTranslationTables
GO
CREATE PROCEDURE BLD_UpdateTranslationTables
(
	@SiteId					uniqueidentifier,
	@SiteListId				uniqueidentifier,
	@TranslateProductTypes	bit,
	@TranslateProducts		bit,
	@ProductTypeIds			SiteTableType READONLY,
	@ProductIds				SiteTableType READONLY 
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, @TargetId uniqueidentifier, @TargetTypeId int, 
		@ListType int, @Locale nvarchar(10)
	SET @UtcNow = GETUTCDATE()

	SELECT TOP 1 @Locale = DefaultTranslateToLanguage FROM SISite WHERE Id = @SiteId
	SELECT TOP 1 @ListType = ListType, @TargetId = Id FROM SISiteList WHERE Id = @SiteListId
	IF @ListType = 1
	BEGIN
		SET @TargetTypeId = 3
		SET @TargetId = @SiteId
	END
	ELSE
	BEGIN
		SET @TargetTypeId = 1
	END
	
	-- Product Type
	IF @TranslateProductTypes = 1 AND EXISTS(SELECT 1 FROM @ProductTypeIds)
	BEGIN
		INSERT INTO TLTranslation
		(
			Id,
			Title,
			ObjectId,
			ObjectType,
			SiteId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			PT.Title,
			PT.Id,
			204,
			@SiteId,
			ISNULL(PT.ModifiedBy, PT.CreatedBy),
			@UtcNow
		FROM vwProductTypeList PT
			JOIN @ProductTypeIds C ON PT.Id = C.ObjectId
			LEFT JOIN TLTranslation T ON PT.Id = T.ObjectId AND T.SiteId = PT.SiteId
		WHERE PT.SiteId = @SiteId
			AND T.Id IS NULL

		EXEC BLD_UpdateTranslationDocuments
		@ObjectIds = @ProductTypeIds,
		@TargetId = @TargetId,
		@TargetTypeId = @TargetTypeId,
		@SiteId = @SiteId,
		@Locale = @Locale
	END

	-- Product
	IF @TranslateProducts = 1 AND EXISTS(SELECT 1 FROM @ProductIds)
	BEGIN
		INSERT INTO TLTranslation
		(
			Id,
			Title,
			ObjectId,
			ObjectType,
			SiteId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			P.Title,
			P.Id,
			205,
			@SiteId,
			ISNULL(P.ModifiedById, P.CreatedById),
			@UtcNow
		FROM vwProduct P
			JOIN @ProductIds C ON P.Id = C.ObjectId
			LEFT JOIN TLTranslation T ON P.Id = T.ObjectId AND T.SiteId = P.SiteId
		WHERE  P.SiteId = @SiteId
			AND T.Id IS NULL

		EXEC BLD_UpdateTranslationDocuments
		@ObjectIds = @ProductIds,
		@TargetId = @TargetId,
		@TargetTypeId = @TargetTypeId,
		@SiteId = @SiteId,
		@Locale = @Locale
	END
END
GO
PRINT 'Create procedure Translation_GetDocuments'
GO
IF (OBJECT_ID('Translation_GetDocuments') IS NOT NULL)
	DROP PROCEDURE Translation_GetDocuments
GO
CREATE PROCEDURE [dbo].[Translation_GetDocuments]
(
	@BatchSize			int,
	@Status				int = NULL,
	@ProcessingStatus	int = NULL,
	@LastUpdatedDate	datetime = NULL
)
AS
BEGIN
	DECLARE @tbDocument TABLE (Id uniqueidentifier primary key)

	IF @ProcessingStatus = 1 --Not processed
	BEGIN
		INSERT INTO @tbDocument
		SELECT TOP (@BatchSize) Id FROM TLDocument
		WHERE (@ProcessingStatus IS NULL OR ProcessingStatus = @ProcessingStatus)
			AND (@Status IS NULL OR Status = @Status)
		ORDER BY CreatedDate

		UPDATE TLDocument
		SET ProcessingStatus = 2 --Processing
		WHERE Id IN (SELECT Id FROM @tbDocument)

		SELECT T.Id,
			T.TranslationId,
			T.Title,
			T.ObjectId,
			T.ObjectType,
			T.ObjectUrl,
			T.Status,
			T.ProcessingStatus,
			T.SiteId,
			S.DefaultTranslateToLanguage AS Locale,
			T.ExternalId,
			T.CreatedBy AS CreatedById,
			FN.UserFullName CreatedByFullName,
			GETUTCDATE() AS CreatedDate
		FROM vwTranslationDocument T
			JOIN @tbDocument D ON D.Id = T.Id
			JOIN SISite S ON S.Id = T.SiteId
			LEFT JOIN VW_UserFullName FN on FN.UserId = T.ModifiedBy

		SELECT D.TranslationDocumentId,
			D.SiteId,
			S.DefaultTranslateToLanguage AS Locale
		FROM vwTranslationDocumentSite D
			JOIN @tbDocument C ON C.Id = D.TranslationDocumentId
			JOIN SISite S ON S.Id = D.SiteId
		WHERE S.SubmitToTranslation = 1
	END
	ELSE
	BEGIN
		INSERT INTO @tbDocument
		SELECT TOP (@BatchSize) Id FROM vwTranslationDocument
		WHERE (@ProcessingStatus IS NULL OR ProcessingStatus = @ProcessingStatus)
			AND (@Status IS NULL OR Status = @Status)
			AND (@LastUpdatedDate IS NULL OR ModifiedDate < @LastUpdatedDate)
		ORDER BY ModifiedDate

		SELECT T.Id,
			T.TranslationId,
			T.Title,
			T.ObjectId,
			T.ObjectType,
			T.ObjectUrl,
			T.Status,
			T.ProcessingStatus,
			T.SiteId,
			T.Locale,
			T.ExternalId,
			T.Content,
			T.CreatedBy AS CreatedById,
			T.CreatedByFullName,
			T.CreatedDate
		FROM vwTranslationDocument T
			JOIN @tbDocument C ON C.Id = T.Id

		SELECT T.Id,
			T.TranslationDocumentId,
			T.SiteId,
			T.Locale,
			T.Status,
			T.Content,
			T.LogMessage
		FROM TLdocumentItem T
			JOIN @tbDocument D ON D.Id = T.TranslationDocumentId
	END
END
GO
PRINT 'Create procedure TranslationDocumentDto_BulkUpdate'
GO
IF (OBJECT_ID('TranslationDocumentDto_BulkUpdate') IS NOT NULL)
	DROP PROCEDURE TranslationDocumentDto_BulkUpdate
GO
CREATE PROCEDURE [dbo].[TranslationDocumentDto_BulkUpdate]
(
	@BatchId	uniqueidentifier
)
AS
BEGIN
	UPDATE D
	SET D.ExternalId = W.ExternalId,
		D.Status = W.Status,
		D.ProcessingStatus = W.ProcessingStatus,
		D.Content = ISNULL(W.Content, D.Content),
		D.LogMessage = ISNULL(W.LogMessage, D.LogMessage),
		D.ObjectUrl = ISNULL(W.ObjectUrl, D.ObjectUrl),
		D.ModifiedBy = D.CreatedBy,
		D.ModifiedDate = GETUTCDATE()
	FROM TLDocument D
		JOIN TLDocumentWorkTable W ON D.Id = W.DocumentId
	WHERE W.BatchId = @BatchId

	UPDATE T
	SET T.ExternalId = W.ExternalId
	FROM TLTranslation T
		JOIN TLDocument D ON T.Id = D.TranslationId
		JOIN TLDocumentWorkTable W ON D.Id = W.DocumentId
	WHERE W.BatchId = @BatchId

	DELETE FROM TLDocumentWorkTable WHERE BatchId = @BatchId
END
GO
PRINT 'Create procedure TranslationDocumentItemDto_BulkInsert'
GO
IF (OBJECT_ID('TranslationDocumentItemDto_BulkInsert') IS NOT NULL)
	DROP PROCEDURE TranslationDocumentItemDto_BulkInsert
GO
CREATE PROCEDURE [dbo].[TranslationDocumentItemDto_BulkInsert]
(
	@BatchId	uniqueidentifier
)
AS
BEGIN
	INSERT INTO TLDocumentItem
	(
		Id,
		TranslationDocumentId,
		SiteId,
		Locale,
		Status,
		Content,
		LogMessage,
		CreatedBy,
		CreatedDate
	)
	SELECT
		W.DocumentItemId,
		W.DocumentId,
		W.SiteId,
		S.DefaultTranslateToLanguage,
		W.Status,
		W.Content,
		W.LogMessage,
		D.CreatedBy,
		D.CreatedDate
	FROM TLDocumentItemWorkTable W
		JOIN TLDocument D ON D.Id = W.DocumentId
		JOIN SISite S ON S.Id = W.SiteId
	WHERE W.BatchId = @BatchId

	UPDATE DI
	SET DI.Status = 3, -- Ignored
		DI.LogMessage = 'Excluded from Translation',
		DI.ModifiedBy = DI.CreatedBy,
		DI.ModifiedDate = GETUTCDATE()
	FROM TLDocumentItem DI
		JOIN TLDocumentItemWorkTable W ON DI.Id = W.DocumentItemId
		JOIN TLDocument D ON D.Id = DI.TranslationDocumentId
		JOIN TLTranslation T ON T.Id = D.TranslationId
		JOIN PageDefinition P ON P.SourcePageDefinitionId = T.ObjectId AND P.SiteId = DI.SiteId AND P.ExcludeFromTranslation = 1 
	WHERE W.BatchId = @BatchId
		AND T.ObjectType = 8

	DELETE FROM TLDocumentItemWorkTable WHERE BatchId = @BatchId
END
GO
PRINT 'Create procedure TranslationDocumentItemDto_BulkUpdate'
GO
IF (OBJECT_ID('TranslationDocumentItemDto_BulkUpdate') IS NOT NULL)
	DROP PROCEDURE TranslationDocumentItemDto_BulkUpdate
GO
CREATE PROCEDURE [dbo].[TranslationDocumentItemDto_BulkUpdate]
(
	@BatchId	uniqueidentifier
)
AS
BEGIN
	UPDATE DI
	SET DI.Status = W.Status,
		DI.TranslationDocumentId = ISNULL(W.DocumentId, DI.TranslationDocumentId),
		DI.Content = ISNULL(W.Content, DI.Content),
		DI.LogMessage = ISNULL(W.LogMessage, DI.LogMessage),
		DI.ModifiedBy = DI.CreatedBy,
		DI.ModifiedDate = GETUTCDATE()
	FROM TLDocumentItem DI
		JOIN TLDocumentItemWorkTable W ON DI.Id = W.DocumentItemId
	WHERE W.BatchId = @BatchId

	DELETE FROM TLDocumentItemWorkTable WHERE BatchId = @BatchId
END
GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TAEmailTemplateVariant'))
BEGIN
    update TAEmailTemplate set body=''  where body is null
	ALTER TABLE TAEmailTemplate ALTER COLUMN Body NVARCHAR(MAX) NOT NULL

	PRINT 'Creating table TAEmailTemplateVariant'

	CREATE TABLE [dbo].[TAEmailTemplateVariant] (
		[Id]					UNIQUEIDENTIFIER NOT NULL,
		[SiteId]				UNIQUEIDENTIFIER NOT NULL,
		[Title]					NVARCHAR (256)   NULL,
		[Description]			NVARCHAR (1024)  NULL,
		[Subject]				NVARCHAR (1024)  NULL,
		[ModifiedBy]			UNIQUEIDENTIFIER NULL,
		[ModifiedDate]			DATETIME         NULL,    
		[SenderDefaultEmail]	NVARCHAR (255)   NULL,
		[MailMergeTags]			NVARCHAR (4000)  NULL,
		[Body]					NVARCHAR (MAX)   NULL,
		CONSTRAINT [PK_TAEmailTemplateVariant] PRIMARY KEY CLUSTERED (
			[Id] ASC,
			[SiteId] ASC
		) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

PRINT 'Creating table TAEmailTemplateVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'TAEmailTemplateVariantCache'))
BEGIN
	CREATE TABLE [dbo].[TAEmailTemplateVariantCache] (
		[Id]					UNIQUEIDENTIFIER NOT NULL,
		[SiteId]				UNIQUEIDENTIFIER NOT NULL,
		[Title]					NVARCHAR (256)   NOT NULL,
		[Description]			NVARCHAR (1024)  NULL,
		[Subject]				NVARCHAR (1024)  NOT NULL,
		[ModifiedBy]			UNIQUEIDENTIFIER NULL,
		[ModifiedDate]			DATETIME         NULL,    
		[SenderDefaultEmail]	NVARCHAR (255)   NULL,
		[MailMergeTags]			NVARCHAR (4000)  NULL,
		[Body]					NVARCHAR (MAX)   NULL,
		CONSTRAINT [PK_TAEmailTemplateVariantCache] PRIMARY KEY ([Id], [SiteId])
	)
END
GO

PRINT 'Creating view vwEmailTemplate'
GO
IF (OBJECT_ID('vwEmailTemplate') IS NOT NULL)
	DROP VIEW vwEmailTemplate
GO
CREATE VIEW [dbo].[vwEmailTemplate]  
AS  
WITH CTE AS  
(  
	SELECT	Id, 
			ApplicationId AS SiteId, 
			Title,
			[Description],
			[Subject],
			SenderDefaultEmail,
			MailMergeTags,
			Body,
			COALESCE(ModifiedDate, CreatedDate) AS ModifiedDate,
			COALESCE(ModifiedBy, CreatedBy) AS ModifiedBy
	FROM	TAEmailTemplate 
  
	UNION ALL  
  
	SELECT	Id, 
			SiteId, 
			Title,
			[Description],
			[Subject],
			SenderDefaultEmail,
			MailMergeTags,
			Body,
			ModifiedDate,
			ModifiedBy
	FROM	TAEmailTemplateVariantCache
)  
	SELECT	ET.Id, CTE.SiteId, ET.ApplicationId AS SourceSiteId, CTE.Title, CTE.[Description], ET.[Key], ET.MimeType,
			CTE.[Subject], CTE.Body, CTE.SenderDefaultEmail, CTE.MailMergeTags, ET.ProductId, ET.[Status],
			ET.CreatedDate, ET.CreatedBy, CFN.UserFullName AS CreatedByFullName,
			CTE.ModifiedDate, CTE.ModifiedBy, MFN.UserFullName AS ModifiedByFullName
	FROM	CTE
			INNER JOIN TAEmailTemplate AS ET ON ET.Id = CTE.Id
			LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = ET.CreatedBy  
			LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = CTE.ModifiedBy  
GO

PRINT 'Modify stored procedure EmailTemplate_GetEmailTemplate'
GO
IF (OBJECT_ID('EmailTemplate_GetEmailTemplate') IS NOT NULL)
	DROP PROCEDURE EmailTemplate_GetEmailTemplate
GO
CREATE PROCEDURE [dbo].[EmailTemplate_GetEmailTemplate]
(
	@Id				UNIQUEIDENTIFIER,
	@ApplicationId	UNIQUEIDENTIFIER
)
AS
SELECT	Id,
		SiteId AS ApplicationId,
		[Key],
		MimeType,
		Title,
		[Description],
		[Subject],
		Body,
		SenderDefaultEmail,
		MailMergeTags,
		[Status],
		CreatedBy,
		CreatedDate,
		CreatedByFullName,
		ModifiedBy,
		ModifiedDate,
		ModifiedByFullName
FROM	vwEmailTemplate
WHERE	Id = @Id AND
		SiteId = @ApplicationId AND
		[Status] = dbo.GetActiveStatus()
GO

PRINT 'Migrating variant email template data'
GO

IF (NOT EXISTS(SELECT * FROM TAEmailTemplateVariantCache))
BEGIN
	INSERT INTO TAEmailTemplateVariantCache (Id, SiteId, Title, [Description], [Subject], Body, SenderDefaultEmail, MailMergeTags, ModifiedBy, ModifiedDate)
		SELECT    ET.Id, S.Id, ET.Title, ET.[Description], ET.[Subject], Body, ET.SenderDefaultEmail, ET.MailMergeTags, ET.ModifiedBy, ET.ModifiedDate
		FROM    SISite AS S
				INNER JOIN TAEmailTemplate AS ET ON ET.ApplicationId = S.MasterSiteId
		WHERE    S.Id != S.MasterSiteId
END
GO

PRINT 'Add columns to ATAttribute'
GO
IF (COL_LENGTH('ATAttribute', 'EnableValueTranslation') IS NULL)
	ALTER TABLE ATAttribute ADD [EnableValueTranslation] BIT NULL
GO
IF (OBJECT_ID('dbo.[DF_ATAttribute_EnableValueTranslation]') IS NULL)
BEGIN
	UPDATE ATAttribute SET EnableValueTranslation = 0 WHERE EnableValueTranslation IS NULL

	UPDATE A SET EnableValueTranslation = 1
	FROM ATAttribute AS A
		INNER JOIN ATAttributeDataType AS ADT ON ADT.Id = A.AttributeDataTypeId
	WHERE ADT.Title IN ('HTML', 'String')
END
GO
IF (OBJECT_ID('dbo.[DF_ATAttribute_EnableValueTranslation]') IS NULL)
	ALTER TABLE ATAttribute ALTER COLUMN EnableValueTranslation BIT NOT NULL
IF (OBJECT_ID('dbo.[DF_ATAttribute_EnableValueTranslation]') IS NULL)
	ALTER TABLE ATattribute ADD CONSTRAINT [DF_ATAttribute_EnableValueTranslation] DEFAULT 0 FOR [EnableValueTranslation]
GO

IF (COL_LENGTH('ATAttribute', 'Key') IS NULL)
	ALTER TABLE ATAttribute ADD [Key] NVARCHAR(50) NULL
GO
IF (COLUMNPROPERTY(OBJECT_ID('dbo.ATAttribute'), 'Key', 'AllowsNull') = 1)
	UPDATE ATAttribute SET [Key] = Title WHERE [Key] IS NULL
GO
IF (COLUMNPROPERTY(OBJECT_ID('dbo.ATAttribute'), 'Key', 'AllowsNull') = 1)
	ALTER TABLE ATAttribute ALTER COLUMN [Key] NVARCHAR(50) NOT NULL
GO



PRINT 'Create procedure ProductAttribute_Save'
GO
IF (OBJECT_ID('ProductAttribute_Save') IS NOT NULL)
	DROP PROCEDURE ProductAttribute_Save
GO
CREATE PROCEDURE [dbo].[ProductAttribute_Save]
(
			@Id uniqueidentifier = null OUTPUT,
			@AttributeGroupId uniqueidentifier=null,
			@Title nvarchar(max), 
			@ModifiedBy uniqueidentifier,
			@ModifiedDate datetime=null,
			@AttributeDataType nvarchar(30)=null,
			@AttributeDataTypeId uniqueidentifier=null,
			@Description nvarchar(400) =null,
			@Sequence int,
			@Code nvarchar(128) =null,
			@IsFaceted bit,
			@IsSearched bit,
			@IsSystem bit =0,
			@IsDisplayed bit=1,
			@IsEnum bit=0,
			@IsRequired bit=0,
			@Status int,
			@ApplicationId uniqueidentifier,
			@IsPersonalized  bit=0
)
AS
DECLARE @Now dateTime
DECLARE @NewGuid uniqueidentifier
DECLARE @RowCount int

--********************************************************************************
-- code
--********************************************************************************
BEGIN

	IF (@Id is null)  -- New Insert
		BEGIN
			SET @NewGuid = newid()
			set @Id= @NewGuid
			
			Declare @currentSequence int
			declare @attributeCategoryId int
			Select @currentSequence = Isnull(max(Sequence),0) from ATAttribute 
			
			IF @AttributeGroupId IS NOT NULL AND @AttributeGroupId != '00000000-0000-0000-0000-000000000000'
			BEGIN
				select @attributeCategoryId=Id from ATAttributeCategory where GroupId=@AttributeGroupId
				if (@attributeCategoryId is null)
				begin
					raiserror('NOTEXISTS||AttributeGroupId', 16, 1)
					return dbo.GetBusinessRuleErrorCode()
				end
			END
			INSERT INTO ATAttribute(
				Id, 
				AttributeDataType,
				AttributeDataTypeId, 
				Title,
				[Key],
				[Description],
				Sequence,
				Code,
				IsFaceted,
				IsSearched,
				IsSystem,
				IsDisplayed,
				IsEnum,
				CreatedDate, 
				CreatedBy, 
				[Status],
				IsPersonalized,
				SiteId)
		   VALUES(
				@NewGuid,
				@AttributeDataType,
				@AttributeDataTypeId,
				@Title,
				@Title,
				@Description,
				@currentSequence+1,
				@Code,
				@IsFaceted,
				@IsSearched,
				@IsSystem,
				@IsDisplayed,
				@IsEnum,
				getUTCDate(),
				NULL,
				@Status,
				@IsPersonalized,
				@ApplicationId
		   )
		   IF(@AttributeGroupId IS NOT NULL AND @AttributeGroupId != '00000000-0000-0000-0000-000000000000')
		   insert ATAttributeCategoryItem (Id, AttributeId, CategoryId, IsRequired) values (newid(), @NewGuid, @attributeCategoryId, @IsRequired)
		END
	ELSE				-- Update the AttributeGroup
		Begin
		   IF((SELECT Count(Title) FROM  ATAttribute WHERE Id = @Id) = 0)
		   BEGIN
				RAISERROR('NOTEXISTS||Id', 16, 1)
				RETURN dbo.GetBusinessRuleErrorCode()
		   END
		End


		BEGIN
			SET @Now = getUTCDate()
			IF (@ModifiedDate IS NULL)
				BEGIN
					UPDATE	ATAttribute  WITH (ROWLOCK)
					SET	AttributeDataType		= @AttributeDataType, 
						AttributeDataTypeId		= @AttributeDataTypeId, 
						Title					= @Title,
						[Description]			= @Description,
						Sequence				= @Sequence,
						Code					= @Code,
						IsFaceted				= @IsFaceted,
						IsSearched				= @IsSearched,
						IsSystem				= @IsSystem,
						IsDisplayed				= @IsDisplayed,
						IsEnum					= @IsEnum,
						[ModifiedDate]			= @Now,
						[ModifiedBy]			= @ModifiedBy,	
						[Status]				= @Status,
						[IsPersonalized]		=@IsPersonalized
					WHERE   [Id]           		=	@Id 
				
				END
			ELSE
				BEGIN
				Set @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId)

					UPDATE	ATAttribute  WITH (ROWLOCK)
					SET	AttributeDataType		= @AttributeDataType, 
						AttributeDataTypeId		= @AttributeDataTypeId, 
						Title					= @Title,
						[Description]			= @Description,
						Sequence				= @Sequence,
						Code					= @Code,
						IsFaceted				= @IsFaceted,
						IsSearched				= @IsSearched,
						IsSystem				= @IsSystem,
						IsDisplayed				= @IsDisplayed,
						IsEnum					= @IsEnum,
						[ModifiedDate]			= @Now,
						[ModifiedBy]			= @ModifiedBy,	
						[Status]				= @Status,
						[IsPersonalized]		=@IsPersonalized
					WHERE   
						[Id]           		=	@Id 
						AND
						[ModifiedDate]		= @ModifiedDate				


					-- last modified date when record was read should match with the current Modified Date 
					-- in the table for the record otherwise it means that someone has modified it already.

					SELECT  @RowCount = @@ROWCOUNT
					IF @@ERROR <> 0
					BEGIN
						RETURN dbo.GetDataBaseErrorCode()
					END
					  PRINT @RowCount
					IF @RowCount = 0
					BEGIN
						/* concurrency error */
						RAISERROR('DBCONCURRENCY',16,1)
						RETURN dbo.GetDataConcurrencyErrorCode()                                   
					END  

				END
				--update attribute name referred in navfilter items table
				update NVFilterItem set LftOpndName= @Title where LftOpnd=@Id
		END

END
GO

PRINT 'Create procedure Translation_UpdateNotification'
GO
IF (OBJECT_ID('Translation_UpdateNotification') IS NOT NULL)
	DROP PROCEDURE Translation_UpdateNotification
GO
CREATE PROCEDURE [dbo].[Translation_UpdateNotification]
(
	@BatchId	uniqueidentifier
)
AS
BEGIN
	DECLARE @tbDocument TABLE (DocumentId uniqueidentifier, Locale nvarchar(10), Status int)

	;WITH CTE AS
	(
		SELECT ROW_NUMBER() OVER(PARTITION BY D.ExternalId, N.Locale ORDER BY D.CreatedDate DESC) AS RowNumber,
			D.Id,
			N.Locale,
			N.Status
		FROM TLDocument D 
			JOIN TLTranslationNotification N ON N.DocumentId = D.ExternalId
		WHERE N.BatchId = @BatchId
	)

	INSERT INTO @tbDocument
	SELECT Id, Locale, Status FROM CTE WHERE RowNumber = 1

	UPDATE DI
	SET DI.Status = N.Status,
		DI.ModifiedDate = GETUTCDATE()
	FROM TLDocumentItem DI
		JOIN @tbDocument N ON N.DocumentId = DI.TranslationDocumentId AND DI.Locale = N.Locale

	UPDATE D
	SET D.ProcessingStatus = 5, --Translated
		D.ModifiedDate = GETUTCDATE()
	FROM TLDocument D
		JOIN @tbDocument N ON N.DocumentId = D.Id
		LEFT JOIN TLDocumentItem DI ON D.Id = DI.TranslationDocumentId AND DI.Status != 5 -- TranslationCompleted
			AND DI.Status != 3 -- Ignored
			AND DI.Status != 8 -- Error
	WHERE DI.Id IS NULL
		
	UPDATE D
	SET D.Status = 5, --SubmittedFortranslation
		D.ModifiedDate = GETUTCDATE()
	FROM TLDocument D
		JOIN @tbDocument N ON N.DocumentId = D.Id
	WHERE D.ProcessingStatus = 5 --Translated
		AND D.Status != 5 --SubmittedFortranslation

	DELETE TN 
		OUTPUT deleted.* INTO TLTranslationNotification_Log
	FROM TLTranslationNotification TN 
	WHERE TN.BatchId = @BatchId
END
GO
PRINT 'Create procedure PageDefinition_Get'
GO
IF (OBJECT_ID('PageDefinition_Get') IS NOT NULL)
	DROP PROCEDURE PageDefinition_Get
GO
CREATE PROCEDURE [dbo].[PageDefinition_Get]
(
	@siteId		uniqueidentifier,
	@startDate	datetime = null
)
AS
BEGIN
	DECLARE @PageDefinitionIds TABLE(Id uniqueidentifier)
	INSERT INTO @PageDefinitionIds
	SELECT PageDefinitionId FROM PageDefinition
		WHERE SiteId = @siteId AND PageStatus != 3 AND (@startDate IS NULL OR ModifiedDate > @startDate)

	SELECT M.PageDefinitionId, M.PageMapNodeId, D.SiteId, M.DisplayOrder, --PageDefinitionXml,
		TemplateId, D.Title, Description, PageStatus, WorkflowState, PublishCount, PublishDate, FriendlyName,
		WorkflowId, D.CreatedBy, D.CreatedDate, D.ModifiedBy, D.ModifiedDate, ArchivedDate, StatusChangedDate, 
		AuthorId, EnableOutputCache, OutputCacheProfileName, ExcludeFromSearch,ExcludeFromExternalSearch, IsDefault, IsTracked,
		HasForms, TextContentCounter, CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,
		ScheduledPublishDate, ScheduledArchiveDate, NextVersionToPublish, E.CampaignId, EA.Id AS EmailId, D.PageSegmentCount, D.ExcludeFromTranslation
	FROM PageDefinition D 
		JOIN PageMapNodePageDef M ON D.PageDefinitionId = M.PageDefinitionId
		JOIN @PageDefinitionIds T ON D.PageDefinitionId = T.Id
		Left JOIN MKCampaignEmail E ON D.PageDefinitionId = E.CMSPageId
		Left JOIN MAEmail EA ON D.PageDefinitionId = EA.PageId
	ORDER BY DisplayOrder ASC

	SELECT S.ObjectId, 
		S.ObjectTypeId, S.SecurityLevelId
	FROM USSecurityLevelObject S
		JOIN @PageDefinitionIds T ON S.ObjectId = T.Id
	WHERE ObjectTypeId = 8
		
	SELECT PageDefinitionId, 
		StyleId, CustomAttributes 
	FROM SIPageStyle S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id
		
	SELECT PageDefinitionId, 
		ScriptId, CustomAttributes 
	FROM SIPageScript S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id

	SELECT P.PageDefinitionId, 
		P.PageMapNodeId, P.DisplayOrder 
	FROM PageMapNodePageDef P
		JOIN @PageDefinitionIds T ON P.PageDefinitionId = T.Id
END
GO
PRINT 'Create procedure PageDefinition_GetByWorkflow'
GO
IF (OBJECT_ID('PageDefinition_GetByWorkflow') IS NOT NULL)
	DROP PROCEDURE PageDefinition_GetByWorkflow
GO
CREATE PROCEDURE [dbo].[PageDefinition_GetByWorkflow]
(
	@WorkflowId		uniqueidentifier
)
AS
BEGIN		
	;WITH Pages AS
	(
		SELECT ObjectId, 
			ROW_NUMBER() OVER(PARTITION BY ObjectId ORDER BY ActionedDate desc) AS Ord 
		FROM WFWorkflowExecution
		WHERE WorkflowId = @WorkflowId
	)

	SELECT 			
		M.PageDefinitionId, 
		M.PageMapNodeId, 
		D.SiteId, 
		M.DisplayOrder,
		TemplateId,
		D.Title,
		Description,
		PageStatus,
		WorkflowState,
		PublishCount,
		PublishDate,
		FriendlyName,
		WorkflowId,
		D.CreatedBy,
		D.CreatedDate,
		D.ModifiedBy,
		D.ModifiedDate,
		ArchivedDate,
		StatusChangedDate,
		AuthorId,
		EnableOutputCache,
		OutputCacheProfileName,
		ExcludeFromSearch,
		ExcludeFromExternalSearch,
		IsDefault,
		IsTracked,
		HasForms,
		TextContentCounter,
		CustomAttributes,
		SourcePageDefinitionId,
		IsInGroupPublish,
		ScheduledPublishDate,
		ScheduledArchiveDate,
		NextVersionToPublish,
		D.PageSegmentCount,
		D.ExcludeFromTranslation,
		E.CampaignId, 
		EA.Id AS EmailId
	FROM PageDefinition D
		INNER JOIN PageMapNodePageDef M ON D.PageDefinitionId =M.PageDefinitionId
		INNER JOIN Pages O ON O.ObjectId = D.PageDefinitionId
		LEFT JOIN MKCampaignEmail E ON D.PageDefinitionId = E.CMSPageId
		LEFT JOIN MAEmail EA ON D.PageDefinitionId = EA.PageId
	WHERE O.Ord = 1
END
GO
PRINT 'Create procedure PageDefinition_GetOne'
GO
IF (OBJECT_ID('PageDefinition_GetOne') IS NOT NULL)
	DROP PROCEDURE PageDefinition_GetOne
GO
CREATE PROCEDURE [dbo].[PageDefinition_GetOne](@pageId uniqueidentifier, @pageMapNodeId uniqueidentifier = null)
As

Begin
		Select 
			M.PageDefinitionId, 
			M.PageMapNodeId, 
			SiteId, 
			M.DisplayOrder, --PageDefinitionXml,
			TemplateId,
			Title,
			Description,
			PageStatus,
			WorkflowState,
			PublishCount,
			PublishDate,
			FriendlyName,
			WorkflowId,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			ArchivedDate,
			StatusChangedDate,
			AuthorId,
			EnableOutputCache,
			OutputCacheProfileName,
			ExcludeFromSearch,
			ExcludeFromExternalSearch,
			IsDefault,
			IsTracked,
			HasForms,
			TextContentCounter,
			CustomAttributes,
			SourcePageDefinitionId,
			IsInGroupPublish,
			ScheduledPublishDate,
			ScheduledArchiveDate,
			NextVersionToPublish,
			D.PageSegmentCount,
			D.ExcludeFromTranslation
		From PageDefinition D
		INNER JOIN PageMapNodePageDef M ON D.PageDefinitionId =M.PageDefinitionId
		where D.PageDefinitionId = @pageId
		AND (@pageMapNodeId is null OR M.PageMapNodeId = @pageMapNodeId )
		
		Select ObjectId, ObjectTypeId, SecurityLevelId
			from USSecurityLevelObject Where ObjectId = @pageId
		
		Select PageDefinitionId, StyleId, CustomAttributes from SIPageStyle Where PageDefinitionId = @pageId
		
		Select PageDefinitionId, ScriptId, CustomAttributes from SIPageScript Where PageDefinitionId = @pageId

end
GO
PRINT 'Create procedure PageMap_SavePageDefinition'
GO
IF (OBJECT_ID('PageMap_SavePageDefinition') IS NOT NULL)
	DROP PROCEDURE PageMap_SavePageDefinition
GO
CREATE PROCEDURE [dbo].[PageMap_SavePageDefinition]
(
	@Id					uniqueidentifier output,
	@SiteId				uniqueidentifier,
	@ParentNodeId		uniqueidentifier,
	@CreatedBy			uniqueidentifier,
	@ModifiedBy			uniqueidentifier,
	@PublishPage		bit,
	@PageDefinitionXml	xml,
	@ContainerXml		xml = null,
	@UnmanagedContentXml xml = null,
	@ZonesXml			xml = null,
	@UpdatePageName bit = null
)
AS
BEGIN
	
	IF(@PublishPage = 1)
		SET @UpdatePageName = 1
		
	IF(@UpdatePageName IS NULL)
		SET @UpdatePageName = 0
	
		
	IF NOT EXISTS(Select 1 from PageMapNode Where PageMapNodeId = @ParentNodeId and SiteId = @SiteId)
	BEGIN
		RAISERROR('NOTEXISTS||ParentId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	DECLARE @pageDefinitionSegmentIds table(Id uniqueidentifier)	
	DECLARE @PublishCount int
	DECLARE @PublishDate datetime
	DECLARE @PageDefinitionSegmentId uniqueidentifier 
	
	IF @PublishPage = 1
		SET @PublishDate = GETUTCDATE()
	ELSE
		SET @PublishDate = @PageDefinitionXml.value('(pageDefinition/@publishDate)[1]','datetime')
		
	DECLARE @ActionType int
	SET @ActionType = 2
	IF @Id IS NULL OR (Select count(*) FROM PageDefinition Where PageDefinitionId=@Id)=0 -- New Page
	BEGIN
		SET @ActionType = 1
		-- get the highest value of the DisplayOrder currently in the DB for this menuId
		DECLARE @CurrentDisplayOrder int
		SELECT @CurrentDisplayOrder = ISNULL(MAX(DisplayOrder),0) FROM PageMapNodePageDef M WHERE M.PageMapNodeId = @ParentNodeId
		IF @CurrentDisplayOrder IS NULL
			SET @CurrentDisplayOrder = 1
		
		IF @PublishPage = 1
			SET @PublishCount = 1
		ELSE
			SET @PublishCount = 0
		IF @Id IS NULL 	
			SET @Id = NEWID()

		IF NOT EXISTS (Select 1 from PageDefinition Where PageDefinitionId =@Id)
			INSERT INTO PageDefinition(PageDefinitionId, SiteId, TemplateId, Title, Description, 
						PublishDate, PageStatus, WorkflowState, PublishCount, --ContainerXml,UnmanagedContentXml,
						FriendlyName, WorkflowId,
						CreatedBy, CreatedDate, ArchivedDate, StatusChangedDate, AuthorId, EnableOutputCache,
						OutputCacheProfileName, ExcludeFromSearch,ExcludeFromExternalSearch, ExcludeFromTranslation,
						IsDefault, IsTracked, HasForms, TextContentCounter, 
						CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,ScheduledPublishDate,ScheduledArchiveDate,NextVersionToPublish,PageSegmentCount)
			VALUES(@Id,  @SiteId, 
					@PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)'), 
					@PublishDate,
					@PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
					@PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'),
					@PublishCount,
					--@ContainerXml,
					--isnull(@UnmanagedContentXml,'<unmanagedPageContents/>'),
					@PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
					@CreatedBy,
					GETUTCDATE(),
					@PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(1000)'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromExternalSearch)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromTranslation)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
					dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
					@PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
					isnull(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
					@PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier'),
					1
				)
				
		IF NOT EXISTS (Select 1 from PageMapNodePageDef Where PageDefinitionId =@Id AND PageMapNodeId=@ParentNodeId)
			INSERT INTO PageMapNodePageDef(PageDefinitionId,PageMapNodeId,DisplayOrder)
			VALUES (@Id,@ParentNodeId,@CurrentDisplayOrder + 1)
			
			
		INSERT INTO PageDefinitionSegment(
			Id
			,PageDefinitionId
			,DeviceId
			,AudienceSegmentId
			,UnmanagedXml
			,ZonesXml
			)
			 output inserted.Id into @pageDefinitionSegmentIds
		VALUES (
			NEWID()
			,@Id
			,dbo.GetDesktopDeviceId(@SiteId)
			,dbo.GetDesktopAudienceSegmentId(@SiteId)
			,isnull(@UnmanagedContentXml,'<unmanagedPageContents/>')
			,@ZonesXml
			)
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			INSERT INTO [PageDefinitionContainer]
				   (
					[PageDefinitionSegmentId]
				   ,[PageDefinitionId]
				   ,[ContainerId]
				   ,[ContentId]
				   ,[InWFContentId]
				   ,[ContentTypeId]
				   ,[IsModified]
				   ,[IsTracked]
				   ,[Visible]
				   ,[InWFVisible]
				   ,[DisplayOrder]
				   ,[InWFDisplayOrder]
				   ,[CustomAttributes]
				   ,ContainerName)
			 SELECT
					S.Id -- as this would be called only for desktop version, we can come in a better way
					,@Id
					,X.value('(@id)[1]','uniqueidentifier') as ContainerId
					,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
					,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
					,X.value('(@contentTypeId)[1]','int') as contentTypeId
					,X.value('(@isModified)[1]','bit') as isModified
					,X.value('(@isTracked)[1]','bit') as isTracked
					,X.value('(@visible)[1]','bit') as Visible
					,X.value('(@inWFVisible)[1]','bit') as InWFVisible
					,X.value('(@displayOrder)[1]','int') as DisplayOrder
					,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
					,dbo.GetCustomAttributeXml(X.query('.'),'container')
					,X.value('(@name)[1]','nvarchar(100)') as ContainerName
			From @ContainerXml.nodes('/Containers/container') temp(X)
			CROSS JOIN @pageDefinitionSegmentIds S
		END
	END
	ELSE
	BEGIN
		SELECT @PublishCount = PublishCount FROM PageDefinition WHERE PageDefinitionId = @Id
		IF @PublishPage = 1
			SELECT @PublishCount = @PublishCount + 1
			
		UPDATE PageDefinition Set 
			Title = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)') ELSE title END,
			TemplateId =@PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
			description = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)') ELSE description END, 
			publishDate = @PublishDate, 
			PageStatus = @PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
			WorkflowState = @PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'), 
			PublishCount = @PublishCount,
			--ContainerXml = @containerXml,
			--UnmanagedContentXml = isnull(@unmanagedContentXml, UnmanagedContentXml),
			FriendlyName = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)') ELSE FriendlyName END,
			WorkflowId = @PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = GETUTCDATE(),
			ArchivedDate = @PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
			StatusChangedDate = @PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
			AuthorId = @PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
			EnableOutputCache = @PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
			OutputCacheProfileName = @PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(100)'),
			ExcludeFromSearch = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
		    ExcludeFromExternalSearch = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromExternalSearch)[1]','bit'),
		    ExcludeFromTranslation = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromTranslation)[1]','bit'),
			IsDefault = @PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
			IsTracked = @PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
			HasForms = @PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
			TextContentCounter = @PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
			SourcePageDefinitionId = @PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
			CustomAttributes = dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
			IsInGroupPublish  =ISNULL(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
			ScheduledPublishDate = @PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
			ScheduledArchiveDate = @PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
			NextVersionToPublish =@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier')
		WHERE PageDefinitionId = @Id AND SiteId = @SiteId
		
		DECLARE @DeskTopDeviceId uniqueidentifier
		DECLARE @DeskTopAudienceId uniqueidentifier
		SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@SiteId)
		SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@SiteId)
		
		SELECT @PageDefinitionSegmentId = Id 
			FROM PageDefinitionSegment WHERE DeviceId = @DeskTopDeviceId AND 
				AudienceSegmentId = @DeskTopAudienceId AND
				PageDefinitionId = @Id
		
		UPDATE PageDefinitionSegment Set 
			UnmanagedXml = isnull(@unmanagedContentXml, UnmanagedXml),
			ZonesXml = isnull(@ZonesXml, ZonesXml)
		WHERE PageDefinitionId = @Id AND Id = @PageDefinitionSegmentId
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			UPDATE [PageDefinitionContainer] SET 
			   [ContentId] = X.value('(@contentId)[1]','uniqueidentifier')
			  ,[InWFContentId] = X.value('(@inWFContentId)[1]','uniqueidentifier')
			  ,[ContentTypeId] = X.value('(@contentTypeId)[1]','int') 
			  ,[IsModified] = X.value('(@isModified)[1]','bit')
			  ,[IsTracked] = X.value('(@isTracked)[1]','bit')
			  ,[Visible] = X.value('(@visible)[1]','bit')
			  ,[InWFVisible] = X.value('(@inWFVisible)[1]','bit')
			  ,[CustomAttributes] = dbo.GetCustomAttributeXml(X.query('.'),'container')
			  ,[DisplayOrder] = X.value('(@displayOrder)[1]','int')
			  ,[InWFDisplayOrder] = X.value('(@inWFDisplayOrder)[1]','int')
			From @ContainerXml.nodes('/Containers/container') temp(X)
			WHERE [PageDefinitionId] = @Id 
			AND PageDefinitionSegmentId = @PageDefinitionSegmentId
			AND [ContainerId] = X.value('(@id)[1]','uniqueidentifier')
			--If any containers added to pagedefinition insert those(ex. marketier template switching)
			INSERT INTO [PageDefinitionContainer]
						   (
							[PageDefinitionSegmentId]
						   ,[PageDefinitionId]
						   ,[ContainerId]
						   ,[ContentId]
						   ,[InWFContentId]
						   ,[ContentTypeId]
						   ,[IsModified]
						   ,[IsTracked]
						   ,[Visible]
						   ,[InWFVisible]
						   ,[DisplayOrder]
						   ,[InWFDisplayOrder]
						   ,[CustomAttributes]
						   ,[ContainerName])
					 SELECT
							@PageDefinitionSegmentId
							,@Id
							,X.value('(@id)[1]','uniqueidentifier') as ContainerId
							,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
							,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
							,X.value('(@contentTypeId)[1]','int') as contentTypeId
							,X.value('(@isModified)[1]','bit') as isModified
							,X.value('(@isTracked)[1]','bit') as isTracked
							,X.value('(@visible)[1]','bit') as Visible
							,X.value('(@inWFVisible)[1]','bit') as InWFVisible
							,X.value('(@displayOrder)[1]','int') as DisplayOrder
							,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
							,dbo.GetCustomAttributeXml(X.query('.'),'container')
							,X.value('(@name)[1]','nvarchar(100)') as ContainerName
					From @ContainerXml.nodes('/Containers/container') temp(X)					
					where X.value('(@id)[1]','uniqueidentifier') not in (select ContainerId from PageDefinitionContainer where PageDefinitionSegmentId=@PageDefinitionSegmentId and PageDefinitionId=@Id)
		
		END
		
	END
	
	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO USSecurityLevelObject 
		SELECT @Id, 8, SL.Id
	FROM dbo.SplitComma(@PageDefinitionXml.value('(pageDefinition/@securityLevels)[1]','nvarchar(max)'), ',') S 
		JOIN USSecurityLevel SL ON S.Value = SL.Id
	
	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO SIPageStyle 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'style')
	FROM @PageDefinitionXml.nodes('/pageDefinition/style') T(C) JOIN SIStyle S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO SIPageScript 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'script')
	FROM @PageDefinitionXml.nodes('/pageDefinition/script') T(C) JOIN SIScript S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	UPDATE D SET D.PageSegmentCount = T.Cnt
	FROM PageDefinition D
	INNER JOIN (SELECT PageDefinitionId, Count(PageDefinitionId) cnt FROM PageDefinitionSegment
	Where PageDefinitionId=@Id
	Group by PageDefinitionId
	) T
	ON T.PageDefinitionId =D.PageDefinitionId 


	EXEC [PageDefinition_AdjustDisplayOrder] @ParentNodeId

	EXEC PageMapBase_UpdateLastModification @SiteId
	EXEC PageMap_UpdateHistory @SiteId, @Id, 8, @ActionType

	EXEC [PageDefinition_UpdateAssetReference] @Id
END
GO
PRINT 'Create procedure SecurityLevelDto_Get'
GO
IF (OBJECT_ID('SecurityLevelDto_Get') IS NOT NULL)
	DROP PROCEDURE SecurityLevelDto_Get
GO
CREATE PROCEDURE [dbo].[SecurityLevelDto_Get]
(
	@SiteId		uniqueidentifier,
	@ObjectId	uniqueidentifier = NULL
)
AS
BEGIN
	IF @ObjectId IS NOT NULL
	BEGIN
		IF EXISTS(SELECT 1 FROM USUser WHERE Id = @ObjectId)
		BEGIN
			SELECT 
				Id,
				Title,
				Description,
				CreatedBy,
				CreatedDate,
				ModifiedBy,
				ModifiedDate,
				IsSystem,
				S.Status,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName
			FROM USSecurityLevel S
				JOIN USUserSecurityLevel US ON S.Id = US.SecurityLevelId
				LEFT JOIN VW_UserFullName FN ON FN.UserId = S.CreatedBy
				LEFT JOIN VW_UserFullName MN ON MN.UserId = S.ModifiedBy          
			WHERE US.UserId = @ObjectId AND      
				S.ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@SiteId))
		END
		ELSE
		BEGIN
			SELECT 
				Id,
				Title,
				Description,
				CreatedBy,
				CreatedDate,
				ModifiedBy,
				ModifiedDate,
				IsSystem,
				S.Status,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName
			FROM USSecurityLevel S
				JOIN USSecurityLevelObject SO ON S.Id = SO.SecurityLevelId
				LEFT JOIN VW_UserFullName FN ON FN.UserId = S.CreatedBy
				LEFT JOIN VW_UserFullName MN ON MN.UserId = S.ModifiedBy
			WHERE S.Status != 3 AND
				SO.ObjectId = @ObjectId AND
				S.ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@SiteId))
		END
	END
	ELSE
	BEGIN
		SELECT 
			Id,
			Title,
			Description,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			IsSystem,
			S.Status,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName
		FROM USSecurityLevel S
			LEFT JOIN VW_UserFullName FN ON FN.UserId = S.CreatedBy
			LEFT JOIN VW_UserFullName MN ON MN.UserId = S.ModifiedBy
		WHERE S.Status != 3 AND
			S.ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@SiteId))
	END
END
GO
PRINT 'Create procedure PageMapNode_SaveWithParams'
GO
IF (OBJECT_ID('PageMapNode_SaveWithParams') IS NOT NULL)
	DROP PROCEDURE PageMapNode_SaveWithParams
GO
CREATE PROCEDURE [dbo].[PageMapNode_SaveWithParams] 
(
	@PageMapNodeId [uniqueidentifier],
	@ParentId [uniqueidentifier],
	@SiteId [uniqueidentifier] ,
	@ExcludeFromSiteMap bit = 0,
	@Description [nvarchar](500) =NULL,
	@DisplayTitle [nvarchar](256) =NULL,
	@FriendlyUrl [nvarchar](500) =NULL,
	@MenuStatus [int] =NULL,
	@TargetId [uniqueidentifier] =NULL,
	@Target [int]= NULL,
	@TargetUrl [nvarchar](500) =NULL,
	@ModifiedBy [uniqueidentifier] =NULL,
	@ModifiedDate [datetime]= NULL,
	@PropogateWorkflow [bit]= 0,
	@InheritWorkflow [bit]= 0,
	@PropogateSecurityLevels [bit]= NULL,
	@InheritSecurityLevels [bit]= 0,
	@PropogateRoles [bit]= NULL,
	@InheritRoles [bit]= 0,
	@Roles [nvarchar](max)= NULL,
	@LocationIdentifier [nvarchar](50)= NULL,
	@HasRolloverImages [bit]= 0,
	@RolloverOnImage [nvarchar](256)= NULL,
	@RolloverOffImage [nvarchar](256)= NULL,
	@IsCommerceNav [bit] =0,
	@AssociatedQueryId [uniqueidentifier]= NULL,
	@DefaultContentId [uniqueidentifier]= NULL,
	@AssociatedContentFolderId [uniqueidentifier]= NULL,
	@SecurityLevels				[varchar](max)=NULL,
	@PageMapNodeWorkFlow [varchar](max)=NULL,
	@CustomAttributes [xml] =NULL,
	@HiddenFromNavigation [bit] = 0,
	@CssClass [nvarchar](125) = NULL,
	@AllowAccessInChildrenSites [bit] = 1 ,
	@SourcePageMapNodeId uniqueidentifier = NULL
)	
AS
BEGIN
	Declare @LftValue int,@RgtValue int, @DisplayOrder int
	Declare @P_PropogateWorkflow bit,
			@P_PropogateSecurityLevels bit,
			@P_PropogateRoles bit

	Set @ModifiedDate = cast(isnull(@ModifiedDate,getdate()) as varchar(50))
	Set @LftValue=0
	Set @RgtValue=0
	Set @DisplayOrder = 1
	
	IF (@ParentId is not null) 
	BEGIN
		Select 
			@LftValue =LftValue,
			@RgtValue=RgtValue, 
			@P_PropogateWorkflow =PropogateWorkflow,
			@P_PropogateSecurityLevels =PropogateSecurityLevels,
			@P_PropogateRoles=PropogateRoles
		from PageMapNode 
		Where PageMapNodeId=@ParentId and SiteId=@SiteId
	END
	ELSE 
	BEGIN
		Set @LftValue=1
		Set @RgtValue=1		
	END
	
	IF @LftValue <=0
	begin
		RAISERROR('NOTEXISTS||ParentId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
	end

	if((@ParentId is not null) AND @P_PropogateSecurityLevels=1 OR @InheritSecurityLevels=1)
	BEGIN
		INSERT INTO USSecurityLevelObject(ObjectId,ObjectTypeId,SecurityLevelId)
		Select @PageMapNodeId,ObjectTypeId,SecurityLevelId from USSecurityLevelObject
		Where ObjectId=@ParentId
	END
			
	INSERT INTO USSecurityLevelObject(ObjectId,ObjectTypeId,SecurityLevelId)
	Select @PageMapNodeId,2,Value from SplitComma(@SecurityLevels,',')
	Where len(Value)>0
	except 
	Select @PageMapNodeId,ObjectTypeId,SecurityLevelId from USSecurityLevelObject
	Where ObjectId=@PageMapNodeId

	Update PageMapNode Set  LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,
							RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End 
	Where SiteId=@SiteId

	SELECT @DisplayOrder = ISNULL(MAX(DisplayOrder), 0) FROM PageMapNode WHERE ParentId = @ParentId

	INSERT INTO [dbo].[PageMapNode]
           ([PageMapNodeId]
           ,[ParentId]
           ,[LftValue]
           ,[RgtValue]
           ,[SiteId]
           ,[ExcludeFromSiteMap]
           ,[Description]
           ,[DisplayTitle]
           ,[FriendlyUrl]
           ,[MenuStatus]
           ,[TargetId]
           ,[Target]
           ,[TargetUrl]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[PropogateWorkflow]
           ,[InheritWorkflow]
           ,[PropogateSecurityLevels]
           ,[InheritSecurityLevels]
           ,[PropogateRoles]
           ,[InheritRoles]
           ,[Roles]
           ,[LocationIdentifier]
           ,[HasRolloverImages]
           ,[RolloverOnImage]
           ,[RolloverOffImage]
           ,[IsCommerceNav]
           ,[AssociatedQueryId]
           ,[DefaultContentId]
           ,[AssociatedContentFolderId]
           ,[CustomAttributes]
           ,[HiddenFromNavigation]
		   ,[CssClass]
		   ,[DisplayOrder]
		   ,[AllowAccessInChildrenSites]
		   ,[SourcePageMapNodeId])
     VALUES
           (@PageMapNodeId
           ,@ParentId
           ,@RgtValue
           ,@RgtValue+1
           ,@SiteId
           ,@ExcludeFromSiteMap
           ,@Description
           ,@DisplayTitle
           ,@FriendlyUrl
           ,@MenuStatus
           ,@TargetId
           ,@Target
           ,@TargetUrl
           ,@ModifiedBy
           ,@ModifiedDate
           ,@ModifiedBy
           ,@ModifiedDate
           ,@PropogateWorkflow
           ,@InheritWorkflow
           ,@PropogateSecurityLevels
           ,@InheritSecurityLevels
           ,@PropogateRoles
           ,@InheritRoles
           ,@Roles
           ,@LocationIdentifier
           ,@HasRolloverImages
           ,@RolloverOnImage
           ,@RolloverOffImage
           ,@IsCommerceNav
           ,@AssociatedQueryId
           ,@DefaultContentId
           ,@AssociatedContentFolderId
           ,@CustomAttributes
           ,@HiddenFromNavigation
		   ,@CssClass
		   ,@DisplayOrder + 1
		   ,@AllowAccessInChildrenSites
		   ,@SourcePageMapNodeId)

	-- Moved down after the Insert page map node as there is a relationship with workflowId
	if((@ParentId is not null) AND @P_PropogateWorkflow=1 OR @InheritWorkflow=1)
	BEGIN
		--PRINT 'In propogate workflow'
		Insert into PageMapNodeWorkflow(Id,PageMapNodeId,WorkflowId,CustomAttributes)
		Select NEWID(),@PageMapNodeId,WorkflowId,CustomAttributes from PageMapNodeWorkflow
		Where PageMapNodeId=@ParentId
	END
	
	Insert into PageMapNodeWorkflow(Id,PageMapNodeId,WorkflowId,CustomAttributes)
	Select NEWID(),@PageMapNodeId,WorkflowId,NULL
	from (
	select Value WorkflowId
	from SplitComma(@PageMapNodeWorkFlow,',')
	Where LEN(Value)>0
	except
	Select WorkflowId 
	from PageMapNodeWorkflow
	Where PageMapNodeId=@PageMapNodeId
	)F

	exec PageMapBase_UpdateLastModification @SiteId
	exec PageMap_UpdateHistory @SiteId, @PageMapNodeId, 2, 1

END
GO
PRINT 'Create procedure PageMapBase_UpdateLastModification'
GO
IF (OBJECT_ID('PageMapBase_UpdateLastModification') IS NOT NULL)
	DROP PROCEDURE PageMapBase_UpdateLastModification
GO
CREATE PROCEDURE [dbo].[PageMapBase_UpdateLastModification]
(
	@SiteId			uniqueidentifier = null
)
AS
BEGIN
	DECLARE @ModifiedDate datetime
	SET @ModifiedDate =  DATEADD(SECOND, DATEDIFF(SECOND, 39000, GETUTCDATE()), 39000)

	UPDATE PageMapNode SET CompleteFriendlyUrl = CalculatedUrl
	WHERE @SiteId IS NULL OR SiteId = @SiteId

	UPDATE PageMapBase SET LastPageMapModificationDate = @ModifiedDate
	WHERE @SiteId IS NULL OR SiteId = @SiteId
END
GO
PRINT 'Create procedure PageMapNode_Update'
GO
IF (OBJECT_ID('PageMapNode_Update') IS NOT NULL)
	DROP PROCEDURE PageMapNode_Update
GO
CREATE PROCEDURE [dbo].[PageMapNode_Update]
(
	@PageMapNodeId [uniqueidentifier],
	@ParentId [uniqueidentifier],
	@SiteId [uniqueidentifier] ,
	@ExcludeFromSiteMap bit = 0,
	@Description [nvarchar](500) =NULL,
	@DisplayTitle [nvarchar](256) =NULL,
	@FriendlyUrl [nvarchar](500) =NULL,
	@MenuStatus [int] =NULL,
	@TargetId [uniqueidentifier] =NULL,
	@Target [int]= NULL,
	@TargetUrl [nvarchar](500) =NULL,
	@ModifiedBy [uniqueidentifier] =NULL,
	@ModifiedDate [datetime]= NULL,
	@PropogateWorkflow [bit]= 0,
	@InheritWorkflow [bit]= 0,
	@PropogateSecurityLevels [bit]= NULL,
	@InheritSecurityLevels [bit]= 0,
	@PropogateRoles [bit]= NULL,
	@InheritRoles [bit]= 0,
	@Roles [nvarchar](max)= NULL,
	@LocationIdentifier [nvarchar](50)= NULL,
	@HasRolloverImages [bit]= 0,
	@RolloverOnImage [nvarchar](256)= NULL,
	@RolloverOffImage [nvarchar](256)= NULL,
	@IsCommerceNav [bit] =0,
	@AssociatedQueryId [uniqueidentifier]= NULL,
	@DefaultContentId [uniqueidentifier]= NULL,
	@AssociatedContentFolderId [uniqueidentifier]= NULL,
	@SecurityLevels				[varchar](max)=NULL,
	@PageMapNodeWorkFlow [varchar](max)=NULL,
	@CustomAttributes [xml] =NULL,
	@HiddenFromNavigation [bit] = 0,
	@CssClass [nvarchar](125) = NULL,
	@AllowAccessInChildrenSites [bit] = 1,
	@DisplayOrder int = NULL
	)
AS
BEGIN
	Declare @Old_PropogateWorkflow bit,
		@Old_InheritWorkflow bit,
		@Old_PropogateSecurityLevels bit,
		@Old_InheritSecurityLevels bit,
		@Old_PropogateRoles bit,
		@Old_InheritRoles bit,
		@LftValue bigint,
		@RgtValue bigint

	if(@PageMapNodeId='00000000-0000-0000-0000-000000000000')
	BEGIN
		RAISERROR('INVALID||PageMapNodeId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	if not exists(Select 1 from PageMapNode where PageMapNodeId =@pageMapNodeId and SiteId = @SiteId)
	BEGIN
		RAISERROR('NOEXISTS||PageMapNodeId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
		
	Select @Old_PropogateWorkflow =PropogateWorkflow,
			@Old_InheritWorkflow =InheritWorkflow,
			@Old_PropogateSecurityLevels =PropogateSecurityLevels,
			@Old_InheritSecurityLevels =InheritSecurityLevels,
			@Old_PropogateRoles=PropogateRoles,
			@Old_InheritRoles =InheritRoles,
			@LftValue =LftValue,
			@RgtValue=RgtValue
	FROM PageMapNode Where PageMapNodeId=@PageMapNodeId
			
	UPDATE [dbo].[PageMapNode]
	SET [SiteId] = @SiteId
      ,[ExcludeFromSiteMap] = @ExcludeFromSiteMap
      ,[Description] = @Description
      ,[DisplayTitle] = @DisplayTitle
      ,[FriendlyUrl] = @FriendlyUrl
      ,[MenuStatus] = @MenuStatus
      ,[TargetId] = @TargetId
      ,[Target] = @Target
      ,[TargetUrl] = @TargetUrl
      ,[ModifiedBy] = @ModifiedBy
      ,[ModifiedDate] = @ModifiedDate
      ,[PropogateWorkflow] = @PropogateWorkflow
      ,[InheritWorkflow] = @InheritWorkflow
      ,[PropogateSecurityLevels] = @PropogateSecurityLevels
      ,[InheritSecurityLevels] = @InheritSecurityLevels
      ,[PropogateRoles] = @PropogateRoles
      ,[InheritRoles] = @InheritRoles
      ,[Roles] = @Roles
      ,[LocationIdentifier] =@LocationIdentifier
      ,[HasRolloverImages] = @HasRolloverImages
      ,[RolloverOnImage] = @RolloverOnImage
      ,[RolloverOffImage] = @RolloverOffImage
      ,[IsCommerceNav] = @IsCommerceNav
      ,[AssociatedQueryId] = @AssociatedQueryId
      ,[DefaultContentId] = @DefaultContentId
      ,[AssociatedContentFolderId] = @AssociatedContentFolderId
      ,[CustomAttributes] = @CustomAttributes
      ,[HiddenFromNavigation] = @HiddenFromNavigation
	  ,[CssClass] = @CssClass
	  ,[AllowAccessInChildrenSites] = @AllowAccessInChildrenSites
	  ,[DisplayOrder] = ISNULL(@DisplayOrder, [DisplayOrder])
	WHERE  [PageMapNodeId] = @PageMapNodeId

	if (@ExcludeFromSiteMap=1)
	BEGIN
		Update C set C.ExcludeFromSiteMap = @ExcludeFromSiteMap
		FROM PageMapNode C 
		Inner Join PageMapNode P on C.LftValue>P.LftValue AND C.RgtValue < P.RgtValue and P.SiteId=C.SiteId
		Where  P.SiteId = @SiteId AND P.PageMapNodeId=@PageMapNodeId
	END			
	
	IF (@Old_PropogateSecurityLevels != @PropogateSecurityLevels 
		OR @Old_InheritSecurityLevels!=@InheritSecurityLevels
		OR @SecurityLevels!=''	)
	BEGIN
		DELETE FROM USSecurityLevelObject Where ObjectId=@PageMapNodeId
		EXEC dbo.PageMapNode_AttachSecurityLevels @SiteId=@SiteId
			,@PageMapNodeId=@PageMapNodeId
			,@SecurityLevels=@SecurityLevels
			,@Propogate= @PropogateSecurityLevels
			,@Inherit= @InheritSecurityLevels
			,@ModifiedBy=@ModifiedBy
			,@ModifiedDate=@ModifiedDate
	END
	ELSE IF (@SecurityLevels = '')
		DELETE FROM USSecurityLevelObject Where ObjectId=@PageMapNodeId

		
	IF (@Old_PropogateWorkflow != @PropogateWorkflow 
	OR @Old_InheritWorkflow!=@InheritWorkflow
	OR @PageMapNodeWorkFlow!='')
	BEGIN
		EXECUTE [dbo].[PageMapNode_AttachWorkFlow] 
		   @SiteId =@SiteId
		  ,@PageMapNodeId=@PageMapNodeId
		  ,@PageMapNodeWorkFlow=@PageMapNodeWorkFlow
		  ,@Propogate=@PropogateWorkflow
		  ,@Inherit=@InheritWorkflow
		  ,@AppendToExistingWorkflows =0
		  ,@ModifiedBy=@ModifiedBy
		  ,@ModifiedDate=@ModifiedDate
			  
	END

	exec PageMapBase_UpdateLastModification @SiteId
	exec PageMap_UpdateHistory @SiteId, @PageMapNodeId, 2, 2
END
GO

PRINT 'Add VariantCache table indexes'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_PRProductAttributeValueVariantCache_ProductIdAttributeId' AND object_id = OBJECT_ID('dbo.PRProductAttributeValueVariantCache'))
	CREATE NONCLUSTERED INDEX [IX_PRProductAttributeValueVariantCache_ProductIdAttributeId] ON [dbo].[PRProductAttributeValueVariantCache] ([ProductId],[AttributeId]) INCLUDE ([Value],[AttributeEnumId])
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_PRProductSKUAttributeValueVariantCache_SkuIdAttributeId' AND object_id = OBJECT_ID('dbo.PRProductSKUAttributeValueVariantCache'))
	CREATE NONCLUSTERED INDEX [IX_PRProductSKUAttributeValueVariantCache_SkuIdAttributeId] ON [dbo].[PRProductSKUAttributeValueVariantCache] ([SkuId],[AttributeId]) INCLUDE ([Value],[AttributeEnumId])
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_PRProductVariantCache_IsActive' AND object_id = OBJECT_ID('dbo.PRProductVariantCache'))
	CREATE NONCLUSTERED INDEX [IX_PRProductVariantCache_IsActive]
		ON [dbo].[PRProductVariantCache]([IsActive] ASC)
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_PRProductVariantCache_Title' AND object_id = OBJECT_ID('dbo.PRProductVariantCache'))
	CREATE NONCLUSTERED INDEX [IX_PRProductVariantCache_Title]
		ON [dbo].[PRProductVariantCache]([Title] ASC)
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_PRProductSKUVariantCache_Id_IsActive_IsOnline' AND object_id = OBJECT_ID('dbo.PRProductSKUVariantCache'))
	CREATE NONCLUSTERED INDEX [IX_PRProductSKUVariantCache_Id_IsActive_IsOnline]
		ON [dbo].[PRProductSKUVariantCache]([Id] ASC, [IsActive] ASC, [IsOnline] ASC)
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_PRProductSKUVariantCache_ListPrice' AND object_id = OBJECT_ID('dbo.PRProductSKUVariantCache'))
	CREATE NONCLUSTERED INDEX [IX_PRProductSKUVariantCache_ListPrice]
		ON [dbo].[PRProductSKUVariantCache]([ListPrice] ASC)
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_PRProductSKUVariantCache_Title' AND object_id = OBJECT_ID('dbo.PRProductSKUVariantCache'))
	CREATE NONCLUSTERED INDEX [IX_PRProductSKUVariantCache_Title]
		ON [dbo].[PRProductSKUVariantCache]([Title] ASC)
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_PRProductTypeVariantCache_SiteId_ID' AND object_id = OBJECT_ID('dbo.PRProductTypeVariantCache'))
	DROP INDEX dbo.PRProductTypeVariantCache.IX_PRProductTypeVariantCache_SiteId_ID
GO

PRINT 'Add SourcePageMapNodeId index to PageMapNode'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'IX_PageMapNode_SourcePageMapNodeId' AND object_id = OBJECT_ID('dbo.PageMapNode'))
	DROP INDEX [IX_PageMapNode_SourcePageMapNodeId] ON PageMapNode
GO
CREATE NONCLUSTERED INDEX [IX_PageMapNode_SourcePageMapNodeId] ON [dbo].[PageMapNode] ([SourcePageMapNodeId], [SiteId])
	INCLUDE ([PageMapNodeId])
GO
PRINT 'Add SiteId index to ATFacetRangeProduct_Cache'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_ATFacetRangeProduct_Cache_SiteId' AND object_id = OBJECT_ID('dbo.ATFacetRangeProduct_Cache'))
	CREATE NONCLUSTERED INDEX [IX_ATFacetRangeProduct_Cache_SiteId] ON [dbo].[ATFacetRangeProduct_Cache]([SiteId] ASC) INCLUDE ([FacetID],[ProductID])
GO
PRINT 'Add SourcePageDefinitionId index to PageDefinition'
GO
IF EXISTS (
	SELECT	*
	FROM	sys.indexes
	WHERE	name = 'IX_PageDefinition_SourcePageDefinitionId' AND
			object_id = OBJECT_ID('dbo.PageDefinition')
)
	DROP INDEX [IX_PageDefinition_SourcePageDefinitionId] ON PageDefinition
GO
CREATE NONCLUSTERED INDEX [IX_PageDefinition_SourcePageDefinitionId]
    ON [dbo].[PageDefinition]([SourcePageDefinitionId] ASC);
GO
PRINT 'Modify ApplicationId index on COContent'
GO
IF EXISTS (
	SELECT	*
	FROM	sys.indexes
	WHERE	name = 'IX_COContent_ApplicationId' AND
			object_id = OBJECT_ID('dbo.COContent')
)
	DROP INDEX [IX_COContent_ApplicationId] ON COContent
GO
CREATE NONCLUSTERED INDEX [IX_COContent_ApplicationId]
    ON [dbo].[COContent]([ApplicationId] ASC)
	INCLUDE ([Id],[ObjectTypeId]);
GO
PRINT 'Add SiteId index to PRProduct'
GO
IF EXISTS (
	SELECT	*
	FROM	sys.indexes
	WHERE	name = 'IX_PRProduct_SiteId' AND
			object_id = OBJECT_ID('dbo.PRProduct')
)
	DROP INDEX [IX_PRProduct_SiteId] ON PRProduct
GO
CREATE NONCLUSTERED INDEX [IX_PRProduct_SiteId]
	ON [dbo].[PRProduct] ([SiteId])
	INCLUDE ([Id],[ProductIDUser],[UrlFriendlyTitle],[ProductTypeID],[SEOFriendlyUrl])
GO
PRINT 'Create procedure MenuDto_Import'
GO
IF (OBJECT_ID('MenuDto_Import') IS NOT NULL)
	DROP PROCEDURE MenuDto_Import
GO
CREATE PROCEDURE [dbo].[MenuDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@IgnoreExisting			bit = NULL,
	@InvalidateCache		bit = NULL,
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @SourceParentId uniqueidentifier, @ParentId uniqueidentifier, @LftValue int, @RgtValue int

	SET @UtcNow = GETUTCDATE()
	SET @Id = (SELECT [dbo].[Menu_GetVariantId](@SourceId, @TargetSiteId))

	IF @Id IS NOT NULL AND @IgnoreExisting = 1
		RETURN

	IF @Id IS NULL
	BEGIN
		SET @SourceParentId = (SELECT TOP 1 ParentId FROM PageMapNode WHERE PageMapNodeId = @SourceId)
		SET @ParentId = (SELECT [dbo].[Menu_GetVariantId](@SourceParentId, @TargetSiteId))
		IF (@ParentId IS NOT NULL)
		BEGIN
			SET @Id = NEWID()

			SELECT @LftValue = LftValue, @RgtValue = RgtValue FROM PageMapNode
			WHERE PageMapNodeId = @ParentId

			UPDATE PageMapNode SET  
				LftValue = CASE WHEN LftValue > @RgtValue THEN LftValue + 2 ELSE LftValue END,
				RgtValue = CASE WHEN RgtValue >= @RgtValue THEN RgtValue + 2 ELSE RgtValue END 
			WHERE SiteId = @TargetSiteId

			INSERT INTO [dbo].[PageMapNode]
			(
				[PageMapNodeId],
				[ParentId],
				[LftValue],
				[RgtValue],
				[SiteId],
				[ExcludeFromSiteMap],
				[Description],
				[DisplayTitle],
				[FriendlyUrl],
				[MenuStatus],
				[TargetId],
				[Target],
				[TargetUrl],
				[CreatedBy],
				[CreatedDate],
				[ModifiedBy],
				[ModifiedDate],
				[PropogateWorkflow],
				[InheritWorkflow],
				[PropogateSecurityLevels],
				[InheritSecurityLevels],
				[PropogateRoles],
				[InheritRoles],
				[Roles],
				[LocationIdentifier],
				[HasRolloverImages],
				[RolloverOnImage],
				[RolloverOffImage],
				[IsCommerceNav],
				[AssociatedQueryId],
				[DefaultContentId],
				[AssociatedContentFolderId],
				[CustomAttributes],
				[HiddenFromNavigation],
				[CssClass],
				[SourcePageMapNodeId],
				[DisplayOrder]
			)
			SELECT
				@Id,
				@ParentId,
				@RgtValue,
				@RgtValue + 1,
				@TargetSiteId,
				[ExcludeFromSiteMap],
				[Description],
				[DisplayTitle],
				[FriendlyUrl],
				[MenuStatus],
				[TargetId],
				[Target],
				[TargetUrl],
				ISNULL([ModifiedBy], [CreatedBy]),
				@UtcNow,
				[ModifiedBy],
				[ModifiedDate],
				[PropogateWorkflow],
				[InheritWorkflow],
				[PropogateSecurityLevels],
				[InheritSecurityLevels],
				[PropogateRoles],
				[InheritRoles],
				[Roles],
				[LocationIdentifier],
				[HasRolloverImages],
				[RolloverOnImage],
				[RolloverOffImage],
				[IsCommerceNav],
				[AssociatedQueryId],
				[DefaultContentId],
				[AssociatedContentFolderId],
				[CustomAttributes],
				[HiddenFromNavigation],
				[CssClass],
				PageMapNodeId,
				DisplayOrder
			FROM PageMapNode
			WHERE PageMapNodeId = @SourceId
		END
	END
	ELSE
	BEGIN
		UPDATE C SET
		  C.[ExcludeFromSiteMap] = P.ExcludeFromSiteMap,
		  C.[Description] = P.Description,
		  C.[DisplayTitle] = P.DisplayTitle,
		  C.[FriendlyUrl] = P.FriendlyUrl,
		  C.[MenuStatus] = P.MenuStatus,
		  C.[ModifiedBy] = P.ModifiedBy,
		  C.[ModifiedDate] = @UtcNow,
		  C.[PropogateWorkflow] = P.PropogateWorkflow,
		  C.[InheritWorkflow] = P.InheritWorkflow,
		  C.[PropogateSecurityLevels] = P.PropogateSecurityLevels,
		  C.[InheritSecurityLevels] = P.InheritSecurityLevels,
		  C.[PropogateRoles] = P.PropogateRoles,
		  C.[InheritRoles] = P.InheritRoles,
		  C.[LocationIdentifier] = P.LocationIdentifier,
		  C.[HasRolloverImages] = P.HasRolloverImages,
		  C.[RolloverOnImage] = P.RolloverOnImage,
		  C.[RolloverOffImage] = P.RolloverOffImage,
		  C.[HiddenFromNavigation] = P.HiddenFromNavigation,
		  C.[CssClass] = P.CssClass,
		  C.TargetId = P.TargetId,
		  C.Target = P.Target,
		  C.TargetUrl = P.TargetUrl,
		  C.DisplayOrder = P.DisplayOrder
		FROM PageMapNode P, PageMapNode C
		WHERE P.PageMapNodeId = @SourceId
			AND C.PageMapNodeId = @Id
	END

	IF @Id IS NOT NULL
	BEGIN
		UPDATE P
		SET P.TargetId = CP.PageDefinitionId 
		FROM PageMapNode P
			JOIN PageDefinition PP ON P.TargetId = PP.PageDefinitionId
			JOIN PageDefinition CP ON CP.SourcePageDefinitionId = PP.PageDefinitionId
		WHERE P.Target = 1 AND P.PageMapNodeId = @Id AND CP.SiteId = @TargetSiteId

		UPDATE P
		SET P.TargetId = CP.PageMapNodeId 
		FROM PageMapNode P
			JOIN PageMapNode PP ON P.TargetId = PP.PageMapNodeId
			JOIN PageMapNode CP ON CP.SourcePageMapNodeId = PP.PageMapNodeId
		WHERE P.Target = 2 AND P.PageMapNodeId = @Id AND CP.SiteId = @TargetSiteId

		IF EXISTS (SELECT 1 FROM PageMapBase WHERE HomePageDefinitionId = @SourceId)
		BEGIN
			UPDATE PageMapBase SET HomePageDefinitionId = @Id WHERE SiteId = @TargetSiteId
		END
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO USSecurityLevelObject
	SELECT @Id, 2, SecurityLevelId FROM USSecurityLevelObject
	WHERE ObjectId = @SourceId

	IF @InvalidateCache = 1
	BEGIN
		EXEC PageDto_Invalidate @SiteId = @TargetSiteId, @ModifiedBy = @ModifiedBy
	END
END
GO
PRINT 'Create procedure MenuDto_Translate'
GO
IF (OBJECT_ID('MenuDto_Translate') IS NOT NULL)
	DROP PROCEDURE MenuDto_Translate
GO
CREATE PROCEDURE [dbo].[MenuDto_Translate]
(
	@Id				uniqueidentifier,
	@Title			nvarchar(max) = NULL,
	@Description	nvarchar(max) = NULL,
	@FriendlyName	nvarchar(max) = NULL,
	@DisplayOrder	int = NULL,
	@ModifiedBy		uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	IF @Title = '' OR @Title = ' ' SET @Title = NULL
	IF @FriendlyName = '' OR @FriendlyName = ' ' SET @FriendlyName = NULL
	
	UPDATE PageMapNode
	SET DisplayTitle = ISNULL(@Title, DisplayTitle),
		[Description] = @Description,
		FriendlyUrl = ISNULL(@FriendlyName, FriendlyUrl),
		DisplayOrder = ISNULL(@DisplayOrder, DisplayOrder),
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @UtcNow
	WHERE PageMapNodeId = @Id
END
GO
PRINT 'Create procedure MenuDto_Save'
GO
IF (OBJECT_ID('MenuDto_Save') IS NOT NULL)
	DROP PROCEDURE MenuDto_Save
GO
CREATE PROCEDURE [dbo].[MenuDto_Save]
(
	@PageMapNodeId				uniqueidentifier = NULL OUTPUT,
	@ParentId					uniqueidentifier = NULL,
	@SiteId						uniqueidentifier = NULL,
	@DisplayTitle				nvarchar(max) = NULL,
	@Description				nvarchar(max) = NULL,
	@FriendlyUrl				nvarchar(max) = NULL,
	@MenuStatus					int = NULL,
	@TargetId					uniqueidentifier = NULL,
	@Target						int = NULL,
	@TargetUrl					nvarchar(max) = NULL,
	@PropogateWorkFlow			bit = NULL,
	@InheritWorkFlow			bit = NULL,
	@PropogateSecurityLevels	bit = NULL,
	@InheritSecurityLevels		bit = NULL,
	@PropogateRoles				bit = NULL,
	@InheritRoles				bit = NULL,
	@ExcludeFromSiteMap			bit = NULL,
	@HiddenFromNavigation		bit = NULL,
	@DisplayOrder				bit = NULL,
	@LocationIdentifier			nvarchar(max) = NULL,
	@HasRolloverImages			bit = NULL,
	@RolloverOnImage			nvarchar(max) = NULL,
	@RolloverOffImage			nvarchar(max) = NULL,
	@IsCommerceNav				bit = NULL,
	@AssociatedQueryId			uniqueidentifier = NULL,
	@DefaultContentId			uniqueidentifier = NULL,
	@AssociatedContentFolderId	uniqueidentifier = NULL,
	@SourcePageMapNodeId		uniqueidentifier = NULL,
	@CustomAttributes			xml = NULL,
	@ModifiedBy					uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UtcNow datetime, @LftValue int, @RgtValue int
	SET @UtcNow = GETUTCDATE()
	
	IF @PageMapNodeId IS NULL OR @PageMapNodeId = dbo.GetEmptyGUID() OR NOT EXISTS (SELECT 1 FROM PageMapNode WHERE PageMapNodeId = @PageMapNodeId)
	BEGIN
		IF @PageMapNodeId IS NULL SET @PageMapNodeId = NEWID()

		IF (@ParentId IS NOT NULL) 
		BEGIN
			SELECT @LftValue = LftValue, @RgtValue = RgtValue
			FROM PageMapNode 
			WHERE PageMapNodeId = @ParentId and SiteId = @SiteId
		END
		ELSE 
		BEGIN
			SET @LftValue = 1
			SET @RgtValue = 1		
		END

		UPDATE PageMapNode 
		SET LftValue = CASE WHEN LftValue > @RgtValue THEN LftValue + 2 ELSE LftValue END,
			RgtValue = CASE WHEN RgtValue >= @RgtValue THEN RgtValue +2 ELSE RgtValue END 
		WHERE SiteId = @SiteId
		
		INSERT INTO PageMapNode
		(
			PageMapNodeId,
			ParentId,
			LftValue,
			RgtValue,
			SiteId,
			DisplayTitle,
			Description,
			FriendlyUrl,
			MenuStatus,
			TargetId,
			Target,
			TargetUrl,
			PropogateWorkFlow,
			InheritWorkFlow,
			PropogateSecurityLevels,
			InheritSecurityLevels,
			PropogateRoles,
			InheritRoles,
			ExcludeFromSiteMap,
			HiddenFromNavigation,
			LocationIdentifier,
			HasRolloverImages,
			RolloverOnImage,
			RolloverOffImage,
			IsCommerceNav,
			AssociatedQueryId,
			DefaultContentId,
			AssociatedContentFolderId,
			SourcePageMapNodeId,
			CustomAttributes,
			CreatedBy,
			CreatedDate,
			DisplayOrder
		)
		VALUES
		(
			@PageMapNodeId,
			@ParentId,
			@RgtValue,
			@RgtValue + 1,
			@SiteId,
			@DisplayTitle,
			@Description,
			@FriendlyUrl,
			ISNULL(@MenuStatus, 0),
			@TargetId,
			ISNULL(@Target, 0),
			ISNULL(@TargetUrl, ''),
			ISNULL(@PropogateWorkFlow, 0),
			ISNULL(@InheritWorkFlow, 0),
			ISNULL(@PropogateSecurityLevels, 0),
			ISNULL(@InheritSecurityLevels, 0),
			ISNULL(@PropogateRoles, 0),
			ISNULL(@InheritRoles, 0),
			ISNULL(@ExcludeFromSiteMap, 0),
			ISNULL(@HiddenFromNavigation, 0),
			@LocationIdentifier,
			ISNULL(@HasRolloverImages, 0),
			@RolloverOnImage,
			@RolloverOffImage,
			ISNULL(@IsCommerceNav, 0),
			@AssociatedQueryId,
			@DefaultContentId,
			@AssociatedContentFolderId,
			@SourcePageMapNodeId,
			@CustomAttributes,
			@ModifiedBy,
			@UtcNow,
			ISNULL(@DisplayOrder, 1)
		)
	END
	ELSE
	BEGIN
		UPDATE PageMapNode
		SET DisplayTitle = @DisplayTitle,
			[Description] = ISNULL(@Description, [Description]),
			FriendlyUrl = ISNULL(@FriendlyUrl, FriendlyUrl),
			MenuStatus = ISNULL(@MenuStatus, MenuStatus),
			TargetId = ISNULL(@TargetId, TargetId),
			Target = ISNULL(@Target, Target),
			TargetUrl = ISNULL(@TargetUrl, TargetUrl),
			PropogateWorkFlow = ISNULL(@PropogateWorkFlow, PropogateWorkFlow),
			InheritWorkFlow = ISNULL(@InheritWorkFlow, InheritWorkFlow),
			PropogateSecurityLevels = ISNULL(@PropogateSecurityLevels, PropogateSecurityLevels),
			InheritSecurityLevels = ISNULL(@InheritSecurityLevels, InheritSecurityLevels),
			PropogateRoles = ISNULL(@PropogateRoles, PropogateRoles),
			InheritRoles = ISNULL(@InheritRoles, InheritRoles),
			ExcludeFromSiteMap = ISNULL(@ExcludeFromSiteMap, ExcludeFromSiteMap),
			HiddenFromNavigation = ISNULL(@HiddenFromNavigation, HiddenFromNavigation),
			LocationIdentifier = ISNULL(@LocationIdentifier, LocationIdentifier),
			HasRolloverImages = ISNULL(@HasRolloverImages, HasRolloverImages),
			RolloverOnImage = ISNULL(@RolloverOnImage, RolloverOnImage),
			RolloverOffImage = ISNULL(@RolloverOffImage, RolloverOffImage),
			IsCommerceNav = ISNULL(@IsCommerceNav, IsCommerceNav),
			AssociatedQueryId = ISNULL(@AssociatedQueryId, AssociatedQueryId),
			DefaultContentId = ISNULL(@DefaultContentId, DefaultContentId),
			AssociatedContentFolderId = ISNULL(@AssociatedContentFolderId, AssociatedContentFolderId),
			SourcePageMapNodeId = ISNULL(@SourcePageMapNodeId, SourcePageMapNodeId),
			CustomAttributes = ISNULL(@CustomAttributes, CustomAttributes),
			DisplayOrder = ISNULL(@DisplayOrder, DisplayOrder),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @UtcNow
		WHERE PageMapNodeId = @PageMapNodeId
	END
END
GO
PRINT 'Create procedure PageDto_Import'
GO
IF (OBJECT_ID('PageDto_Import') IS NOT NULL)
	DROP PROCEDURE PageDto_Import
GO
CREATE PROCEDURE [dbo].[PageDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@PageMapNodeId			uniqueidentifier = NULL OUTPUT,
	@Publish				bit = NULL,	
	@Overwrite				bit = NULL,		
	@ModifiedBy				uniqueidentifier,
	@PreserveVariantContent bit = 1,
	@IgnoreExisting			bit = NULL,
	@InvalidateCache		bit = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier, @DisplayOrder int

	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @Id = [dbo].[Page_GetVariantId](@SourceId, @TargetSiteId)

	IF @PageMapNodeId IS NULL
	BEGIN
		DECLARE @ParentPageMapNode uniqueidentifier
		SELECT TOP 1 @ParentPageMapNode = PageMapNodeId, @DisplayOrder = DisplayOrder FROM PageMapNodePageDef 
			WHERE PageDefinitionId = @SourceId
		SET @PageMapNodeId = [dbo].[Menu_GetVariantId](@ParentPageMapNode, @TargetSiteId)
	END

	IF @PageMapNodeId IS NULL OR (@Id IS NOT NULL AND @IgnoreExisting = 1)
		RETURN

	SELECT @ModifiedBy = ModifiedBy FROM PageDefinition WHERE PageDefinitionId = @SourceId 
	IF @Publish IS NULL AND EXISTS (SELECT 1 FROM PageDefinition WHERE PageDefinitionId = @SourceId AND PageStatus = 8)
		SET @Publish = 1

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()
		INSERT INTO [dbo].[PageDefinition]
		(
			[PageDefinitionId],
			[TemplateId],
			[SiteId],
			[Title],
			[Description],
			[PageStatus],
			[WorkflowState],
			[PublishCount],
			[PublishDate],
			[FriendlyName],
			[WorkflowId],
			[CreatedBy],
			[CreatedDate],
			[ModifiedBy],
			[ModifiedDate],
			[ArchivedDate],
			[StatusChangedDate],
			[AuthorId],
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			[SourcePageDefinitionId],
			[CustomAttributes]
		)
		SELECT @Id,
			[TemplateId],
			@TargetSiteId,
			[Title],
			D.[Description],
			CASE WHEN @Publish = 1 THEN 8 ELSE PageStatus END,
			1,
			0,
			'1900-01-01 00:00:00.000',
			[FriendlyName],
			@EmptyGuid,
			@ModifiedBy,
			@UtcNow,
			@ModifiedBy,
			@UtcNow,
			[ArchivedDate],
			[StatusChangedDate],
			@ModifiedBy,
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			PageDefinitionId,
			D.[CustomAttributes]
		FROM [dbo].[PageDefinition] D
		WHERE PageDefinitionId = @SourceId				

		UPDATE PageMapNode SET TargetId = @Id
		WHERE TargetId = @SourceId AND SiteId = @TargetSiteId
		 
		INSERT INTO [dbo].[PageDefinitionSegment]
		(
			[Id],
			[PageDefinitionId],
			[DeviceId],
			[AudienceSegmentId],
			[UnmanagedXml],
			[ZonesXml]
		)
		SELECT NEWID(), 
			@Id,
			[DeviceId],
			[AudienceSegmentId],
			dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
			[ZonesXml]
		FROM [dbo].[PageDefinitionSegment] PS 
		WHERE PageDefinitionId = @SourceId

		INSERT INTO [dbo].[PageDetails]
		(
			[PageId],
			[PageDetailXML]
		)
		SELECT 
			@Id,
			[PageDetailXML]
		FROM [dbo].[PageDetails] PD
		WHERE PD.PageId = @SourceId
	END
	ELSE
	BEGIN
		UPDATE C SET
			C.Title = P.Title,
			C.Description = P.Description,
			C.PageStatus = CASE WHEN @Publish = 1 THEN 8 ELSE P.PageStatus END,
			C.FriendlyName = CASE WHEN @PreserveVariantContent = 0 THEN P.FriendlyName ELSE C.FriendlyName END,
			C.EnableOutputCache = P.EnableOutputCache,
			C.OutputCacheProfileName = P.OutputCacheProfileName,
			C.ExcludeFromSearch = P.ExcludeFromSearch,
			C.HasForms = P.HasForms,
			C.ScheduledPublishDate = P.ScheduledPublishDate,
			C.ScheduledArchiveDate = P.ScheduledArchiveDate,
			C.ModifiedDate = @UtcNow,
			C.ModifiedBy = P.ModifiedBy
		FROM PageDefinition P, PageDefinition C
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.UnmanagedXml = dbo.Page_GetUnmanagedXml(P.[UnmanagedXml], @TargetSiteId),
			C.ZonesXml = P.ZonesXml
		FROM PageDefinitionSegment P
			JOIN PageDefinitionSegment C ON P.DeviceId = C.DeviceId AND P.AudienceSegmentId = C.AudienceSegmentId
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.ContentId = P.ContentId,
			C.InWFContentId = P.InWFContentId,
			C.ContentTypeId = P.ContentTypeId,
			C.IsModified = P.IsModified,
			C.[IsTracked] = P.[IsTracked],
			C.[Visible] = P.[Visible],
			C.[InWFVisible] = P.[InWFVisible],
			C.[CustomAttributes] = P.[CustomAttributes],
			C.[DisplayOrder] = P.[DisplayOrder],
			C.[InWFDisplayOrder] = P.[InWFDisplayOrder]
		FROM PageDefinitionContainer P
			JOIN PageDefinitionContainer C ON P.ContainerName = C.ContainerName
			LEFT JOIN COContent CO ON C.ContentId = CO.Id
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId
			AND (@Overwrite = 1 OR CO.Id IS NULL OR CO.ApplicationId != @TargetSiteId)
		
		IF @PreserveVariantContent = 0
		BEGIN
			UPDATE C 
			SET	C.PageDetailXML = P.PageDetailXML
			FROM PageDetails P, PageDetails C
			WHERE C.PageId = @Id AND P.PageId = @SourceId	
		END

		DELETE FROM PageMapNodePageDef WHERE PageDefinitionId = @Id
	END

	IF EXISTS (SELECT 1 FROM ATPageAttributeValue WHERE PageDefinitionId = @SourceId AND IsShared = 1)
	BEGIN
		WITH AttributeCTE AS
		(
			SELECT V.AttributeId FROM ATPageAttributeValue V  
			WHERE V.PageDefinitionId = @SourceId AND IsShared = 1
		)

		DELETE P FROM ATPageAttributeValue P
			JOIN AttributeCTE C ON C.AttributeId = P.AttributeId
		WHERE P.PageDefinitionId = @Id 

		INSERT INTO ATPageAttributeValue
		(
			Id,   
			PageDefinitionId,   
			AttributeId,  
			AttributeEnumId,  
			[Value],
			Notes,  
			CreatedDate,  
			CreatedBy
		)
		SELECT NEWID(),
			@Id,
			AttributeId,
			AttributeEnumId,
			Value,
			Notes,
			@UtcNow,  
			CreatedBy
		FROM ATPageAttributeValue
		WHERE PageDefinitionId = @SourceId AND IsShared = 1
	END

	INSERT INTO PageMapNodePageDef
	(
		PageDefinitionId,
		PageMapNodeId,
		DisplayOrder
	)
	SELECT
		@Id,
		@PageMapNodeId,
		@DisplayOrder

	INSERT INTO [PageDefinitionContainer]
	(
		[Id],
		PageDefinitionSegmentId ,
		[PageDefinitionId],
		[ContainerId],
		[ContentId],
		[InWFContentId],
		[ContentTypeId],
		[IsModified],
		[IsTracked],
		[Visible],
		[InWFVisible],
		DisplayOrder,
		InWFDisplayOrder,
		[CustomAttributes],
		[ContainerName]
	)
	SELECT NEWID(),
		CS.Id,
		@Id,
		ContainerId,
		ContentId,
		InWFContentId,
		ContentTypeId,
		IsModified,
		PC.IsTracked,
		Visible,
		InWFVisible,
		PC.DisplayOrder, 
		PC.InWFDisplayOrder, 
		PC.CustomAttributes,
		ContainerName
	FROM PageDefinitionContainer PC
		JOIN PageDefinitionSegment PS ON PC.PageDefinitionSegmentId = PS.Id
		JOIN PageDefinitionSegment CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
	WHERE PC.PageDefinitionId = @SourceId
		AND (@Id IS NULL OR ContainerName NOT IN (SELECT ContainerName FROM PageDefinitionContainer WHERE PageDefinitionId = @Id))

	DECLARE @SourceTargetMenuId uniqueidentifier
	SELECT TOP 1 @SourceTargetMenuId = PageMapNodeId FROM PageMapNode PN
		JOIN PageDefinition PD ON PN.TargetId = PD.PageDefinitionId AND PD.SiteId = PN.SiteId
	WHERE PD.PageDefinitionId = @SourceId

	IF @SourceTargetMenuId IS NOT NULL
	BEGIN
		UPDATE PageMapNode SET
			TargetId = @Id,
			Target = 1,
			TargetUrl = ''
		WHERE SourcePageMapNodeId = @SourceTargetMenuId
			AND SiteId = @TargetSiteId
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[USSecurityLevelObject]
	(
		[ObjectId],
		[ObjectTypeId],
		[SecurityLevelId]
	)
	SELECT @Id,
		ObjectTypeId,
		SecurityLevelId
	FROM [USSecurityLevelObject] S
	WHERE S.ObjectId = @SourceId

	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageScript]
	(
		[PageDefinitionId],
		[ScriptId],
		[CustomAttributes]
	)
	SELECT @Id,
		[ScriptId],
		[CustomAttributes]
	FROM [dbo].[SIPageScript] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageStyle]
	(
		[PageDefinitionId],
		[StyleId],
		[CustomAttributes]
	)
	SELECT @Id,
		[StyleId],
		[CustomAttributes]
	FROM [dbo].[SIPageStyle] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM COTaxonomyObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[COTaxonomyObject]
	(
		[Id],
		[TaxonomyId],
		[ObjectId],
		[ObjectTypeId],
		[SortOrder],
		[Status],
		[CreatedBy],
		[CreatedDate],
		[ModifiedBy],
		[ModifiedDate]
	)
	SELECT NEWID(),
		[TaxonomyId],
		@Id,
		[ObjectTypeId],
		[SortOrder],
		[Status],
		@ModifiedBy,
		@UtcNow,
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[COTaxonomyObject] OT
	WHERE OT.ObjectId = @SourceId

	DECLARE @VersionId uniqueidentifier, @SourceVersionId uniqueidentifier, 
		@VersionNumber int, @RevisionNumber int, @VersionStatus int
	SET @VersionId = NEWID()
	SELECT TOP 1 @SourceVersionId = Id FROM VEVersion 
	WHERE ObjectId = @SourceId ORDER BY CreatedDate DESC	

	SELECT TOP 1 @VersionNumber = VersionNumber, @RevisionNumber = RevisionNumber FROM VEVersion
	WHERE ObjectId = @Id ORDER BY CreatedDate DESC

	IF @Publish = 1
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0) + 1
		SET @VersionNumber = 0
		SET @VersionStatus = 1
	END
	ELSE
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0)
		SET @VersionNumber = ISNULL(@VersionNumber, 0) + 1
		SET @VersionStatus = 0
	END

	INSERT INTO [dbo].[VEVersion]
	(
		[Id],
		[ApplicationId],
		[ObjectTypeId],
		[ObjectId],
		[CreatedDate],
		[CreatedBy],
		[VersionNumber],
		[RevisionNumber],
		[Status],
		[ObjectStatus],
		[VersionStatus],
		[XMLString],
		[Comments],
		[TriggeredObjectId],
		[TriggeredObjectTypeId]
	)          
	SELECT TOP 1 @VersionId,
		@TargetSiteId,
		8, 
		@Id, 
		@UtcNow,
		@ModifiedBy,
		@VersionNumber,
		@RevisionNumber,
		@VersionStatus,
		ObjectStatus,
		VersionStatus, 
		dbo.UpdatePageXml(XMLString, @Id, P.WorkflowId, P.WorkflowState, P.PageStatus, 1, 
			P.CreatedBy, P.CreatedDate, P.AuthorId, P.SiteId, P.SourcePageDefinitionId, @PageMapNodeId), 
		'Imported from master',
		TriggeredObjectId,
		TriggeredObjectTypeId       
	FROM VEVersion V, 
		PageDefinition P
	WHERE V.Id = @SourceVersionId
		AND P.PageDefinitionId = @Id

	IF @Publish = 1
		UPDATE VEVersion SET Status = 1 WHERE ObjectId = @Id

	INSERT INTO [dbo].[VEPageDefSegmentVersion]
	(
		[Id],
		[VersionId],
		[PageDefinitionId],
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],  
		[ContainerContentXML],        
		[UnmanagedXml],
		[ZonesXml],
		[CreatedBy],
		[CreatedDate]
	)
	SELECT NEWID(),
		@VersionId,
		@Id,
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],
		CASE WHEN @Overwrite = 1 THEN [ContainerContentXML] 
			ELSE [dbo].[Page_GetContainerXml](@Id, AudienceSegmentId, DeviceId) END,
		dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
		[ZonesXml],
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[VEPageDefSegmentVersion]
	WHERE VersionId = @SourceVersionId

	INSERT INTO [dbo].[VEPageContentVersion]
	(
		[Id],
		[ContentVersionId],
		[IsChanged],
		[PageDefSegmentVersionId]
	)
	SELECT NEWID(),
		ContentVersionId,
		IsChanged,
		CS.Id
	FROM [dbo].[VEPageContentVersion] PC
		JOIN VEPageDefSegmentVersion PS ON PC.PageDefSegmentVersionId = PS.Id
		JOIN VEPageDefSegmentVersion CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
			AND PS.VersionId = @SourceVersionId
			AND CS.VersionId = @VersionId

	IF @Publish = 1
	BEGIN
		DECLARE @PublishCount int
		SET @PublishCount = (SELECT ISNULL(PublishCount, 0) + 1 FROM PageDefinition WHERE PageDefinitionId = @Id)

		UPDATE PageDefinition SET
			PublishCount = @PublishCount,
			PublishDate = @UtcNow,
			PageStatus = 8
		WHERE PageDefinitionId = @Id

		DECLARE @WorkflowId uniqueidentifier
		SELECT TOP 1 @WorkflowId = WorkflowId FROM WFWorkflowExecution WHERE ObjectId = @Id AND Completed = 0

		IF @WorkflowId IS NOT NULL
		BEGIN
			EXEC WorkflowExecution_Save
				@WorkflowId = @WorkflowId,
				@SequenceId = @EmptyGuid,
				@ObjectId = @Id,
				@ObjectTypeId = 8,
				@ActorId = @ModifiedBy,
				@ActorType = 1,
				@ActionId = 8,
				@Remarks = N'Page imported from master',
				@Completed = 1
		END
	END

	IF @InvalidateCache = 1
	BEGIN
		EXEC PageDto_Invalidate @SiteId = @TargetSiteId, @ModifiedBy = @ModifiedBy
	END
END
GO
PRINT 'Create procedure Site_CreateMicroSiteData'
GO
IF (OBJECT_ID('Site_CreateMicroSiteData') IS NOT NULL)
	DROP PROCEDURE Site_CreateMicroSiteData
GO
CREATE PROCEDURE [dbo].[Site_CreateMicroSiteData]
(
	@ProductId						uniqueidentifier,
	@MasterSiteId					uniqueidentifier,
	@MicroSiteId					uniqueidentifier,
	@ModifiedBy						uniqueidentifier = null,
	@ImportAllAdminUserAndPermission bit ,
	@ImportAllWebSiteUser			bit,
	@PageImportOptions				int = 2,
	@ImportContentStructure			bit = 0,
	@ImportFileStructure			bit = 0,
	@ImportImageStructure			bit = 0,
	@ImportFormsStructure			bit = 0
)
AS
BEGIN
	-- Import Front end and admin users
	EXEC [dbo].[VariantSite_ImportUser] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllWebSiteUser, @ImportAllAdminUserAndPermission, @ProductId
	-- Import Menu and Pages
	EXEC [dbo].[Site_CopyMenusAndPages] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, @PageImportOptions
	--content structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 7, @ImportContentStructure
	--File structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 9, @ImportFileStructure
	--Image structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 33, @ImportImageStructure
	--Forms structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 38, @ImportFormsStructure
	
	-- Import Tag, Form, etc. 
	EXEC [VariantSite_CopyMisc] @MicroSiteId, @ModifiedBy
	
	-- Changing the status of the site		
	UPDATE SISite SET Status = 1 WHERE Id = @MicroSiteId		
	

	--CREATE MARKETIER RELATED DEFAULT DATA FOR VARIANT SITES
	EXEC Site_CreateMicrositeDataForMarketier @MicroSiteId = @MicroSiteId, @ModifiedBy = @ModifiedBy

	--CREATE COMMERCE RELATED DEFAULT DATA FOR VARIANT SITES
	EXEC Site_CreateMicrositeDataForCommerce @MasterSiteId, @MicroSiteId

	;WITH CTE AS
	(
		SELECT PageMapNodeId, ROW_NUMBER() OVER(PARTITION BY ParentId ORDER BY LftValue) AS DisplayOrder
		FROM PageMapNode WHERE SiteId = @MicroSiteId
	)

	UPDATE P
	SET P.DisplayOrder = C.DisplayOrder
	FROM PageMapNode P JOIN CTE C ON P.PageMapNodeId = C.PageMapNodeId	

	--Inserting in Country Cache
	INSERT INTO [dbo].[GLCountryVariantCache]
    (
		[Id],
        [SiteId],
        [CountryName],
        [Status],
        [ModifiedBy],
        [ModifiedDate]
	)
    SELECT
		Id,
		@MicroSiteId,
		CountryName,
		Status,
		ModifiedBy,
		ModifiedDate
	FROM vwCountry where SiteId = @MasterSiteId

	--Inserting in State Cache
	INSERT INTO [dbo].[GLStateVariantCache]
    (
		[Id],
        [SiteId],
        [State],
        [Status],
        [ModifiedBy],
        [ModifiedDate]
	)
    SELECT
		Id,
		@MicroSiteId,
		State,
		Status,
		ModifiedBy,
		ModifiedDate
	FROM vwState where SiteId = @MasterSiteId
END
GO
PRINT 'Create procedure Site_CreateMicrositeDataForMarketier'
GO
IF (OBJECT_ID('Site_CreateMicrositeDataForMarketier') IS NOT NULL)
	DROP PROCEDURE Site_CreateMicrositeDataForMarketier
GO
CREATE PROCEDURE [dbo].[Site_CreateMicrositeDataForMarketier]
(
	@MicroSiteId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier = NULL
)
AS
BEGIN
	IF @ModifiedBy IS NULL SET @ModifiedBy = '798613EE-7B85-4628-A6CC-E17EE80C09C5'

	--Pointer used for text / image updates. This might not be needed, but is declared here just in case
	DECLARE @pv binary(16)
	DECLARE @ApplicationId uniqueidentifier
	SET @ApplicationId = @MicroSiteId

	DECLARE @CommerceProductId uniqueidentifier
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'

	DECLARE @MarketierProductId uniqueidentifier
	SET @MarketierProductId='CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E'

	--DECLARE @UnsubscribePageId  uniqueidentifier
	--SET @UnsubscribePageId='18F27A17-03B2-4482-B375-3C48577B5702'

	--if not exists (select 1 from COContentStructure where VirtualPath='~/Content Library/MarketierLibrary' and SiteId=@MicroSiteId)
	--begin
	BEGIN TRANSACTION

	--========== Creating ManageContentLibrary folder for marketier
		DECLARE @ContentLibraryId uniqueidentifier, @MarketierContentLibrary uniqueidentifier, @unassignedFoldeId uniqueidentifier
		SELECT @ContentLibraryId = Id from COContentStructure where Title = 'Content Library' AND SiteId=@MicroSiteId
		SET @ContentLibraryId = Isnull(@ContentLibraryId, @ApplicationId)
		SET @MarketierContentLibrary = newid()--'E196A2C5-C070-4904-B93D-DFECED4ADABF'
		SET @unassignedFoldeId = newid() --'B843F49B-1427-48D9-A439-CBD265B36077'


		
			declare @p1 xml
			set @p1=convert(xml,N'<SiteDirectoryCollection><SiteDirectory Id="' + cast(@MarketierContentLibrary as char(36)) + '" Title="MarketierLibrary" Description="" CreatedBy="00000000-0000-0000-0000-000000000000" CreatedDate="" ModifiedBy="00000000-0000-0000-0000-000000000000" ModifiedDate="" Status="0" CreatedByFullName="" ModifiedByFullName="" ObjectTypeId="7" PhysicalPath="" VirtualPath="" Attributes="0" ThumbnailImagePath="" FolderIconPath="" Exists="0" Size="0" NumberOfFiles="0" CompleteURL="" IsSystem="True" IsMarketierDir="True" AccessRoles="" ParentId="' + cast(@ContentLibraryId as char(36)) + '" Lft="1" Rgt="2"/></SiteDirectoryCollection>')
			declare @p5 nvarchar(max)
			set @p5=N'<SISiteDirectory Id="'+ cast(@MarketierContentLibrary as char(36)) +'" VirtualPath="~/Content Library/MarketierLibrary" Attributes="0"/>'
			declare @p6 nvarchar(256)
			set @p6=N'ACME'
			IF NOT EXISTS (Select 1 from COContentStructure Where Title ='MarketierLibrary' And SiteId=@MicroSiteId)
			exec SqlDirectoryProvider_Save @XmlString=@p1,@ParentId=@ContentLibraryId,@ApplicationId=@MicroSiteId,@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MicroSiteId=null,@VirtualPath=@p5 output,@SiteName=@p6 output
			ELSE
				SET @MarketierContentLibrary = (Select top 1 Id  from COContentStructure Where Title ='MarketierLibrary' And SiteId=@MicroSiteId)

		  --UPDATING SITE SETTINGS FOR MARKETIER CONTENT LIBRARY
		  declare @MarketierContentLibSettingTypeId int 
		  SELECT @MarketierContentLibSettingTypeId = Id  FROM STSettingType Where Name = 'MarketierContentLibraryId'

		  IF (@MarketierContentLibSettingTypeId IS NOT NULL)
		  BEGIN
			IF ( NOT EXISTS (SELECT 1 FROM STSiteSetting WHERE SiteId=@MicroSiteId AND SettingTypeId=@MarketierContentLibSettingTypeId) )
			BEGIN
				INSERT INTO STSiteSetting (SiteId,SettingTypeId,Value)
				VALUES (@MicroSiteId,@MarketierContentLibSettingTypeId,@MarketierContentLibrary)
			END 
			ELSE
			BEGIN
				update [dbo].[STSiteSetting] set Value = @MarketierContentLibrary where SettingTypeId=@MarketierContentLibSettingTypeId AND SiteId=@MicroSiteId
			END
		 END
		
		--select @p5, @p6
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
		--declare @p1 xml
		set @p1=convert(xml,N'<SiteDirectoryCollection><SiteDirectory Id="'+ cast(@unassignedFoldeId as char(36)) + '" Title="Unassigned" Description="" CreatedBy="00000000-0000-0000-0000-000000000000" CreatedDate="" ModifiedBy="00000000-0000-0000-0000-000000000000" ModifiedDate="" Status="0" CreatedByFullName="" ModifiedByFullName="" ObjectTypeId="7" PhysicalPath="" VirtualPath="" Attributes="0" ThumbnailImagePath="" FolderIconPath="" Exists="0" Size="0" NumberOfFiles="0" CompleteURL="" IsSystem="True" IsMarketierDir="True" AccessRoles="" ParentId="' + cast(@MarketierContentLibrary as char(36)) + '" Lft="1" Rgt="2"/></SiteDirectoryCollection>')
		--declare @p5 nvarchar(max)
		set @p5=N'<SISiteDirectory Id="'+cast(@unassignedFoldeId as char(36)) +'" VirtualPath="~/Content Library/MarketierLibrary/Unassigned" Attributes="0"/>'
		--declare @p6 nvarchar(256)
		set @p6=N'ACME'
		IF NOT EXISTS (Select 1 from COContentStructure Where Title ='Unassigned' And SiteId=@MicroSiteId)
		exec SqlDirectoryProvider_Save @XmlString=@p1,@ParentId=@MarketierContentLibrary,@ApplicationId=@MicroSiteId,@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MicroSiteId=null,@VirtualPath=@p5 output,@SiteName=@p6 output
		ELSE 
			SET @unassignedFoldeId =( Select top 1 Id   from COContentStructure Where Title ='Unassigned' And SiteId=@MicroSiteId)
		--select @p5, @p6
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
	--============= Creating Maketier PageMapNodes 
		DECLARE @MarkierPageMapNodeRootId uniqueidentifier, @MarkierPageMapNodeId uniqueidentifier
		DECLARE @pmnXml xml
		DECLARE	@return_value int

		set @MarkierPageMapNodeRootId = newid() 
		print @MarkierPageMapNodeRootId

		set @pmnXml= '<pageMapNode id="' + convert(nvarchar(36),@MarkierPageMapNodeRootId) +'" description="MARKETIERPAGES" displayTitle="MARKETIERPAGES" friendlyUrl="MARKETIERPAGES" menuStatus="1" targetId="00000000-0000-0000-0000-000000000000" parentId="' + convert(nvarchar(36),@ApplicationId) +'" target="0" targetUrl="" createdBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" createdDate="Feb  9 2010  1:18PM" modifiedBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" modifiedDate="Feb  9 2010  1:18PM" propogateWorkFlow="True" inheritWorkFlow="False" propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="False" inheritRoles="False" roles="" securityLevels="" locationIdentifier="MARKETIERPAGES"/>'

		IF NOT EXISTS (Select 1 from PageMapNode Where description ='MARKETIERPAGES' AND displayTitle='MARKETIERPAGES' 
		AND friendlyUrl='MARKETIERPAGES' And SiteId=@MicroSiteId)
		EXEC @return_value = [dbo].[PageMapNode_Save]
					@Id=@MarkierPageMapNodeRootId,
					@SiteId = @ApplicationId,
					@ParentNodeId = @ApplicationId,
					@PageMapNode = @pmnXml,
					@CreatedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedDate=null
		SELECT 'Return Value' = @return_value


		
		IF @@ERROR<>0 
		Begin
			ROLLBACK TRANSACTION
			GOTO QuitWithErrors
		END

		

	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END

		DECLARE @ParentSiteId uniqueidentifier, @SourcePageMapNodeId uniqueidentifier
		SELECT TOP 1 @ParentSiteId = ParentSiteId FROM SISite WHERE Id = @MicroSiteId
		SELECT TOP 1 @SourcePageMapNodeId = PageMapNodeId FROM PageMapNode WHERE SiteId = @ParentSiteId AND LocationIdentifier like 'MARKETIERPAGES'
		UPDATE PageMapNode SET SourcePageMapNodeId = @SourcePageMapNodeId WHERE PageMapNodeId = @MarkierPageMapNodeRootId
 
		set @MarkierPageMapNodeId = newid() 
		
		Select @MarkierPageMapNodeRootId = PageMapNodeId from PageMapNode Where description ='MARKETIERPAGES' AND displayTitle='MARKETIERPAGES' 
		AND friendlyUrl='MARKETIERPAGES' And SiteId=@MicroSiteId

		set @pmnXml= '<pageMapNode id="' + convert(nvarchar(36),@MarkierPageMapNodeId) +'" description="MARKETIERCAMPAIGN" displayTitle="MARKETIERCAMPAIGN" friendlyUrl="MARKETIERCAMPAIGN" menuStatus="0" targetId="00000000-0000-0000-0000-000000000000" parentId="' + convert(nvarchar(36),@MarkierPageMapNodeRootId) +'" target="0" targetUrl="" createdBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" createdDate="Feb  9 2010  1:18PM" modifiedBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" modifiedDate="Feb  9 2010  6:18PM" propogateWorkFlow="True" inheritWorkFlow="False" propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="False" inheritRoles="False" roles="" securityLevels=""/>'
		
		IF NOT EXISTS (Select 1 from PageMapNode Where description ='MARKETIERCAMPAIGN' AND displayTitle='MARKETIERCAMPAIGN' 
		AND friendlyUrl='MARKETIERCAMPAIGN' And SiteId=@MicroSiteId)
		EXEC @return_value = [dbo].[PageMapNode_Save]
					@Id=@MarkierPageMapNodeId,
					@SiteId = @ApplicationId,
					@ParentNodeId = @MarkierPageMapNodeRootId,
					@PageMapNode = @pmnXml,
					@CreatedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedDate=null
		SELECT 'Return Value' = @return_value


		IF @@ERROR<>0 
		Begin
			ROLLBACK TRANSACTION
			GOTO QuitWithErrors
		END

		SELECT TOP 1 @SourcePageMapNodeId = PageMapNodeId FROM PageMapNode WHERE SiteId = @ParentSiteId AND FriendlyUrl like 'MARKETIERCAMPAIGN'
		UPDATE PageMapNode SET SourcePageMapNodeId = @SourcePageMapNodeId WHERE PageMapNodeId = @MarkierPageMapNodeId

		--UPDATING SITE SETTINGS FOR MARKETIER Campaign Page map node id
		declare @MarketierPageMapNodeSettingTypeId int 
		SELECT @MarketierPageMapNodeSettingTypeId = Id  FROM STSettingType Where Name = 'CampaignPageMapNodeId'
		
		Select @MarkierPageMapNodeId = PageMapNodeId from PageMapNode Where description ='MARKETIERCAMPAIGN' AND displayTitle='MARKETIERCAMPAIGN' 
		AND friendlyUrl='MARKETIERCAMPAIGN' And SiteId=@MicroSiteId

		IF (@MarketierContentLibSettingTypeId IS NOT NULL)
		BEGIN
		IF ( NOT EXISTS (SELECT 1 FROM STSiteSetting WHERE SiteId=@MicroSiteId AND SettingTypeId=@MarketierPageMapNodeSettingTypeId) )
		BEGIN
			INSERT INTO STSiteSetting (SiteId,SettingTypeId,Value)
			VALUES (@MicroSiteId,@MarketierPageMapNodeSettingTypeId,@MarkierPageMapNodeId)
		END 
		ELSE
		BEGIN
			update [dbo].[STSiteSetting] set Value = @MarkierPageMapNodeId where SettingTypeId=@MarketierPageMapNodeSettingTypeId AND SiteId=@MicroSiteId
		END
		END

		DECLARE @SourceLandingPageNodeId uniqueidentifier, @StrSourceLandingPageNodeId nvarchar(100), @LandingPageNodeId uniqueidentifier, 
			@LandingPageSettingTypeId int, @LandingPageSettingSequence int

		IF NOT EXISTS (SELECT 1 FROM STSettingType T JOIN STSiteSetting S ON T.Id = S.SettingTypeId
			WHERE T.Name = 'Marketier.LandingPageNodeId' AND S.SiteId = @MicroSiteId)
		BEGIN
			SET @LandingPageNodeId = NULL

			SELECT TOP 1 @StrSourceLandingPageNodeId = Value FROM STSettingType T JOIN STSiteSetting S ON T.Id = S.SettingTypeId
				WHERE T.Name = 'Marketier.LandingPageNodeId' AND S.SiteId = @ParentSiteId

			IF @StrSourceLandingPageNodeId IS NOT NULL
			BEGIN
				SET @SourceLandingPageNodeId = CAST(@StrSourceLandingPageNodeId as uniqueidentifier)

				EXEC MenuDto_Save
					@PageMapNodeId = @LandingPageNodeId OUTPUT,
					@ParentId = @MarkierPageMapNodeRootId,
					@SiteId = @MicroSiteId,
					@DisplayTitle = 'Landing Pages',
					@FriendlyUrl = 'landing-pages',
					@ModifiedBy = @ModifiedBy,
					@SourcePageMapNodeId = @SourceLandingPageNodeId

				SET @LandingPageSettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Marketier.LandingPageNodeId')
				IF @LandingPageSettingTypeId IS NULL
				BEGIN
					SELECT @LandingPageSettingSequence = MAX(sequence) + 1 FROM dbo.STSettingType
					INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, Sequence)
					VALUES('Marketier.LandingPageNodeId', 'Marketier.LandingPageNodeId',1, @LandingPageSettingSequence)

					SET @LandingPageSettingTypeId = @@IDENTITY
				END

				INSERT INTO STSiteSetting
				SELECT @MicroSiteId, @LandingPageSettingTypeId, @LandingPageNodeId
			END
		END
	--end
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END

	--===== Updaging existing commerce group 
		-- Before Commerce custom settings group was 2(i.e Custom) and now it changed to 4 (i.e CommerceCustomSettings)
		--update STSettingType set SettingGroupId=(select Id from STSettingGroup where Name='CommerceCustomSettings') where SettingGroupId=2

	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
	
IF @@ERROR<>0 
Begin
	ROLLBACK TRANSACTION
	GOTO QuitWithErrors
END
ELSE
BEGIN
	COMMIT TRANSACTION
END

-- Creating default email group
DECLARE @CreatedBy uniqueidentifier
SELECT TOP 1 @CreatedBy = Id FROM USUser WHERE UserName like 'IAppsSystemUser'

IF NOT EXISTS(SELECT 1 FROM MKCampaignGroup WHERE Name ='My first email group' AND ApplicationId = @MicroSiteId)
BEGIN
	INSERT INTO MKCampaignGroup(Id, Name, CreatedBy, CreatedDate, ApplicationId)
	SELECT NEWID(), 'My first email group', @CreatedBy, getdate(), @MicroSiteId
END

-- Creating shared email group
IF NOT EXISTS(SELECT 1 FROM MKCampaignGroup WHERE Id = @MicroSiteId and ApplicationId = @MicroSiteId)
BEGIN
	IF EXISTS(SELECT 1 FROM SISite WHERE Id = @MicroSiteId AND Id != MasterSiteId)
	BEGIN
		INSERT INTO MKCampaignGroup(Id, Name, CreatedBy, CreatedDate, ApplicationId, NodeType, DisplayOrder)
		SELECT @MicroSiteId, 'Shared Campaigns', @CreatedBy, getdate(), @MicroSiteId, 2, 9998
	END
END

-- Creating Deleted Email Campaigns group for this site
IF NOT EXISTS(SELECT 1 FROM MKCampaignGroup WHERE ApplicationId = @MicroSiteId and Name='Deleted Email Campaigns')
BEGIN
	INSERT INTO MKCampaignGroup(Id, Name, CreatedBy, CreatedDate, ApplicationId, NodeType, DisplayOrder)
	SELECT newid(), 'Deleted Email Campaigns', @CreatedBy, getdate(), @MicroSiteId, 11, 9999
END

--Create an entry in the TADistributionList table for all the shared contactlists so that the next run of statsrollup updates the count properly


if(not exists (select 1 from TADistributionListSite where SiteId=@MicroSiteId))
begin
	insert into TADistributionListSite 
	(
	Id,
	DistributionListId,
	SiteId,
	[Count],
	LastSynced
	)
	SELECT newid(),
	Id,
	@MicroSiteId,
	0,
	NULL
	FROM TADistributionLists 
	WHERE IsSystem=1 and IsGlobal=1
end


IF (NOT EXISTS (SELECT 1 FROM TADistributionGroup WHERE Title='Marketer Lists' AND ApplicationId = @MicroSiteId))
BEGIN
	INSERT INTO dbo.TADistributionGroup(Id, Title, Description, CreatedDate, CreatedBy, ApplicationId, NodeType, DisplayOrder)
	SELECT NEWID(), 'Marketer Lists', 'Marketer Lists', GETUTCDATE(), @CreatedBy, @MicroSiteId, 1, 2

	DECLARE @MarSettingTypeId int
	SELECT TOP 1 @MarSettingTypeId= Id FROM STSettingType WHERE Name='Marketier.MarketerContactListGroupId'

	INSERT INTO STSiteSetting
	(
		[SiteId],
		[SettingTypeId],
		[Value]
	)
	SELECT
	@MicroSiteId,
	@MarSettingTypeId,
	(SELECT TOP 1 G.Id FROM TADistributionGroup G WHERE G.ApplicationId=@MicroSiteId AND G.Title = 'Marketer Lists' )
END

IF NOT EXISTS (SELECT 1 FROM MKLinkGroup WHERE Title = 'Autogenerated Links' AND SiteId = @MicroSiteId)
BEGIN
	INSERT INTO MKLinkGroup
	(
		Id, 
		Title, 
		Description, 
		SiteId, 
		CreatedBy, 
		CreatedDate,
		NodeType,
		DisplayOrder)
	SELECT
		NEWID(), 
		'Autogenerated Links', 
		'Generated when sending email', 
		@MicroSiteId, 
		'00000000-0000-0000-0000-000000000000', 
		GETUTCDATE(),
		2,
		1
END



QuitWithErrors:

END
GO
PRINT 'Create procedure VariantSite_CopyDefaultMenus'
GO
IF (OBJECT_ID('VariantSite_CopyDefaultMenus') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyDefaultMenus
GO
CREATE PROCEDURE [dbo].[VariantSite_CopyDefaultMenus]
(  
	@MicroSiteId uniqueidentifier,
	@ModifiedBy  uniqueidentifier
)
AS
BEGIN
	INSERT INTO PageMapBase(SiteId, ModifiedDate) VALUES (@MicroSiteId, GETUTCDATE())

	DECLARE @PageMapNodeId uniqueidentifier, @SourcePageMapNodeId uniqueidentifier, @SiteTitle nvarchar(max), @FriendlyUrl nvarchar(max)

	SET @PageMapNodeId = @MicroSiteId
	SELECT TOP 1 @SiteTitle = Title, @SourcePageMapNodeId = ParentSiteId from SISite where Id = @MicroSiteId
	SET @FriendlyUrl = replace(replace(replace(@SiteTitle,' ',''),'''',''),'"','')

	EXEC MenuDto_Save
		@PageMapNodeId = @PageMapNodeId OUTPUT,
		@SiteId = @MicroSiteId,
		@DisplayTitle = @SiteTitle,
		@Description = @SiteTitle,
		@FriendlyUrl = @FriendlyUrl,
		@ModifiedBy = @ModifiedBy,
		@SourcePageMapNodeId = @SourcePageMapNodeId

	--Unassigned
	SET @PageMapNodeId = NULL
	SELECT TOP 1 @SourcePageMapNodeId = PageMapNodeId from PageMapNode where LftValue = 2 AND RgtValue = 3
	
	EXEC MenuDto_Save
		@PageMapNodeId = @PageMapNodeId OUTPUT,
		@SiteId = @MicroSiteId,
		@ParentId = @MicroSiteId,
		@DisplayTitle = 'Unassigned',
		@Description = 'Unassigned',
		@FriendlyUrl = 'unassigned',
		@ModifiedBy = @ModifiedBy,
		@SourcePageMapNodeId = @SourcePageMapNodeId
END
GO
PRINT 'Create procedure VariantSite_CopyMenus'
GO
IF (OBJECT_ID('VariantSite_CopyMenus') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyMenus
GO
CREATE PROCEDURE [dbo].[VariantSite_CopyMenus]
(  
	@SiteId			uniqueidentifier,
	@MicroSiteId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	Declare @NotAllowAccessInChildrenSites Table (PageMapNodeId uniqueIdentifier, Process bit)
	Declare @ExcludeNodes Table (PageMapNodeId uniqueIdentifier, Title varchar(1000))
	Declare @LftValue int, @RgtValue int

	INSERT INTO @NotAllowAccessInChildrenSites  (PageMapNodeId, Process)
	Select PageMapnodeId, 0 from PageMapNode where AllowAccessInChildrenSites = 0 and SiteId = @SiteId
	Declare @ParentPageMapNodeIdToExclude uniqueidentifier
	While EXISTS( Select 1 from @NotAllowAccessInChildrenSites  where Process = 0)
	BEGIN
		Select top 1 @ParentPageMapNodeIdToExclude  = PageMapNodeId from @NotAllowAccessInChildrenSites  where Process = 0

		Select @LftValue = LftValue, @RgtValue = RgtValue from PageMapNode where PageMapNodeId = @ParentPageMapNodeIdToExclude and SiteId = @SiteId

		INSERT INTO @ExcludeNodes (PageMapNodeId, Title) 
		Select PageMapNodeId, DisplayTitle  from PageMapNode where LftValue Between @LftValue and @RgtValue and SiteId = @SiteId

		Update @NotAllowAccessInChildrenSites Set Process = 1 where PageMapNodeId = @ParentPageMapNodeIdToExclude
	END

	DECLARE @CampaignPageMapNodeId uniqueidentifier, @StrCampaignPageMapNodeId nvarchar(100)
	SELECT TOP 1 @StrCampaignPageMapNodeId = Value FROM STSettingType T JOIN STSiteSetting S ON T.Id = S.SettingTypeId
		WHERE T.Name = 'CampaignPageMapNodeId' AND S.SiteId = @SiteId

	IF @StrCampaignPageMapNodeId IS NOT NULL AND @StrCampaignPageMapNodeId != ''
		SET @CampaignPageMapNodeId = CAST(@StrCampaignPageMapNodeId as uniqueidentifier)
	ELSE
		SET @CampaignPageMapNodeId = dbo.GetEmptyGUID()

	INSERT INTO [dbo].[PageMapNode]
			   ([PageMapNodeId]
			   ,[ParentId]
			   ,[LftValue]
			   ,[RgtValue]
			   ,[SiteId]
			   ,[ExcludeFromSiteMap]
			   ,[Description]
			   ,[DisplayTitle]
			   ,[FriendlyUrl]
			   ,[MenuStatus]
			   ,[TargetId]
			   ,[Target]
			   ,[TargetUrl]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PropogateWorkflow]
			   ,[InheritWorkflow]
			   ,[PropogateSecurityLevels]
			   ,[InheritSecurityLevels]
			   ,[PropogateRoles]
			   ,[InheritRoles]
			   ,[Roles]
			   ,[LocationIdentifier]
			   ,[HasRolloverImages]
			   ,[RolloverOnImage]
			   ,[RolloverOffImage]
			   ,[IsCommerceNav]
			   ,[AssociatedQueryId]
			   ,[DefaultContentId]
			   ,[AssociatedContentFolderId]
			   ,[CustomAttributes]
			   ,[HiddenFromNavigation]
			   ,SourcePageMapNodeId
			   ,CssClass)
			   SELECT  case when parentId is null then @MicroSiteId else NEWID()end
				  ,[ParentId]
				  ,[LftValue]
				  ,[RgtValue]
				  ,@MicroSiteId
				  ,[ExcludeFromSiteMap]
				  ,[Description]
				  ,[DisplayTitle]
				  ,[FriendlyUrl]
				  ,[MenuStatus]
				  ,[TargetId]
				  ,[Target]
				  ,[TargetUrl]
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,[PropogateWorkflow]
				  ,[InheritWorkflow]
				  ,[PropogateSecurityLevels]
				  ,[InheritSecurityLevels]
				  ,[PropogateRoles]
				  ,[InheritRoles]
				  ,[Roles]
				  ,[LocationIdentifier]
				  ,[HasRolloverImages]
				  ,[RolloverOnImage]
				  ,[RolloverOffImage]
				  ,[IsCommerceNav]
				  ,[AssociatedQueryId]
				  ,[DefaultContentId]
				  ,[AssociatedContentFolderId]
				  ,[CustomAttributes]
				  ,[HiddenFromNavigation]
				  ,PageMapNodeId
				  ,CssClass
			  FROM [dbo].[PageMapNode]
			   Where SiteId=@SiteId
			   AND PageMapNodeId not in    
			   (
				SELECT distinct P.PageMapNodeId 
				FROM [dbo].[PageMapNode] P
				cross  join PageMapNode DP 
				Where P.SiteId=@SiteId and P.LftValue between DP.LftValue and DP.RgtValue
				AND DP.PageMapNodeId   in 
				 (
					select @CampaignPageMapNodeId
					UNION 
					select distinct PageMapNodeId from @ExcludeNodes
				 )   
			   )
	  /* END Modification for not copying the node which are not shared in variant sites*/
	  
		--remove marketierpages menu group   
		DELETE FROM PageMapNode WHERE SiteId = @MicroSiteId AND SourcePageMapNodeId IN 
		(SELECT ParentId FROM PageMapNode WHERE PageMapNodeId = @CampaignPageMapNodeId)
		
		--remove marketierpages menu group   subnodes if any exist(generally it should not)
		DELETE FROM PageMapNode WHERE SiteId = @MicroSiteId AND ParentId IN 
		(SELECT ParentId FROM PageMapNode WHERE PageMapNodeId = @CampaignPageMapNodeId)

		
	    
	   Update  M Set M.ParentId = P.PageMapNodeId
	   FROM PageMapNode M 
	   INNER JOIN PageMapNode P ON P.SourcePageMapNodeId = M.ParentId
	   Where P.SiteId =@MicroSiteId and M.SiteId=@MicroSiteId
	   
	   UPdate PN SET PN.TargetId=T.PageMapNodeId
		FROM PageMapNode PN
		INNER JOIN PageMapNode T ON PN.TargetId = T.SourcePageMapNodeId
		Where PN.Target='2' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId


		 --import shared workflow mapping of pagemapnode and workflow
	INSERT INTO [dbo].[PageMapNodeWorkflow]
           ([Id]
           ,[PageMapNodeId]
           ,[WorkflowId]
           ,[CustomAttributes])
	select NEWID(),
		P.PageMapNodeId,
		PW.WorkflowId,
		null
	FROM [dbo].[PageMapNodeWorkflow] PW
		INNER JOIN PageMapNode P ON PW.PageMapNodeId = P.SourcePageMapNodeId
		inner join WFWorkflow W on PW.WorkflowId = W.Id 
		 Where P.SiteId = @MicroSiteId  and W.IsShared=1 


	IF exists(select 1 from  [dbo].PageMapBase B INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId Where P.SiteId = @MicroSiteId)
	begin
	 INSERT INTO [dbo].[PageMapBase]
					   ([SiteId]
					   ,[HomePageDefinitionId]
					   ,[ModifiedDate]
					   ,[ModifiedBy]
					   ,[LastPageMapModificationDate])
			SELECT top 1 @MicroSiteId
				  ,P.PageMapNodeId
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,[LastPageMapModificationDate]
			FROM [dbo].PageMapBase B
			INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId
			 Where P.SiteId = @MicroSiteId
	end
	else	
		Insert into PageMapBase (SiteId, ModifiedDate,ModifiedBy)	Values(@MicroSiteId, GETUTCDATE(),@ModifiedBy) 

	EXEC PageMapBase_UpdateLastModification @MicroSiteId
END
GO

PRINT 'Modify view vwNavFacet'
GO
IF(OBJECT_ID('vwNavFacet') IS NOT NULL)
	DROP VIEW vwNavFacet
GO
CREATE VIEW vwNavFacet
AS
SELECT	N.NavNodeId AS NavId, N.[Sequence],
		F.Id, F.SiteId, F.SourceSiteId, F.AttributeID, F.Limit, F.IsRange, F.Title, F.[Description],
		F.DisplayOrder, F.IsManualOrder, F.AllowMultiple, F.AttributeName, F.DataType, F.[Status],
		F.CreatedDate, F.CreatedBy, F.CreatedByFullName,
		F.ModifiedDate, F.ModifiedBy, F.ModifiedByFullName
FROM	vwFacet AS F
		INNER JOIN NVNavNodeFacet N ON F.Id = N.FacetId
GO

PRINT 'Modify view vwNavProduct'
GO
IF (OBJECT_ID('vwNavProduct') IS NOT NULL)
	DROP VIEW vwNavProduct	
GO
CREATE VIEW vwNavProduct
AS
SELECT	N.NavNodeId AS NavId,
		N.NavNodeUrl,
		FO.SerialNo AS DisplayOrder,
		CASE WHEN E.Id IS NULL THEN 0 ELSE 1 END AS ExcludedFromNav,	
		P.[Id],   
		P.SiteId,  
		P.SourceSiteId,  
		P.Title,   
		P.ShortDescription,   
		P.LongDescription,   
		P.[Description],   
		P.ProductIDUser,   
		P.Keyword,   
		P.ProductStyle,  
		P.BundleCompositionLastModified,   
		P.UrlFriendlyTitle,   
		P.TaxCategoryID,   
		P.IsActive,
		P.PromoteAsNewItem,   
		P.PromoteAsTopSeller,  
		P.ExcludeFromDiscounts,   
		P.ExcludeFromShippingPromotions,   
		P.Freight,   
		P.Refundable,   
		P.TaxExempt,  
		P.ProductTypeID,   
		P.[Status],   
		P.IsBundle,   
		P.ProductTypeName,  
		P.ProductUrl,
		P.SEOTitle,
		P.SEOH1,   
		P.SEODescription,   
		P.SEOKeywords,   
		P.SEOFriendlyUrl,  
		P.IsShared,  
		P.CreatedDate,   
		P.CreatedById,   
		P.CreatedByFullName,  
		P.ModifiedDate,   
		P.ModifiedById,   
		P.ModifiedByFullName
FROM	NVFilterOutput AS FO
		INNER JOIN NVNavNodeNavFilterMap AS N ON N.QueryId = FO.QueryId
		INNER JOIN vwProduct AS P ON P.Id = FO.ObjectId
		LEFT JOIN NVFilterExclude AS E ON E.QueryId = N.QueryId AND E.ObjectId = P.Id
GO

PRINT 'Modify view vwNavProductSearch'
GO
IF (OBJECT_ID('vwNavProductSearch') IS NOT NULL)
	DROP VIEW vwNavProductSearch	
GO
CREATE VIEW [dbo].[vwNavProductSearch]  
AS  
SELECT	P.NavId,
		P.NavNodeUrl,
		P.DisplayOrder,
		P.ExcludedFromNav,	
		P.[Id],   
		P.SiteId,  
		P.SourceSiteId,  
		P.Title,   
		P.ShortDescription,   
		P.LongDescription,   
		P.[Description],   
		P.ProductIDUser,   
		P.Keyword,   
		P.ProductStyle,  
		P.BundleCompositionLastModified,   
		P.UrlFriendlyTitle,   
		P.TaxCategoryID,   
		P.IsActive,
		P.PromoteAsNewItem,   
		P.PromoteAsTopSeller,  
		P.ExcludeFromDiscounts,   
		P.ExcludeFromShippingPromotions,   
		P.Freight,   
		P.Refundable,   
		P.TaxExempt,  
		P.ProductTypeID,   
		P.[Status],   
		P.IsBundle,   
		P.ProductTypeName,  
		P.ProductUrl,
		P.SEOTitle,
		P.SEOH1,   
		P.SEODescription,   
		P.SEOKeywords,   
		P.SEOFriendlyUrl,  
		P.IsShared,  
		P.CreatedDate,   
		P.CreatedById,   
		P.CreatedByFullName,  
		P.ModifiedDate,   
		P.ModifiedById,   
		P.ModifiedByFullName,
		SKU.SKU,
		SKU.Title AS SkuTitle
FROM	vwNavProduct AS P
		LEFT JOIN vwSKU AS SKU ON SKU.ProductId = P.Id AND SKU.SiteId = P.SiteId  
GO


PRINT 'Modify view vwAttribute'
GO
IF(OBJECT_ID('vwAttribute') IS NOT NULL)
	DROP VIEW vwAttribute
GO
CREATE VIEW [dbo].[vwAttribute]
AS
WITH CTE AS
(
	SELECT A.Id, 
		A.SiteId,
		A.Title,
		A.Description,
		ISNULL(A.ModifiedDate, A.CreatedDate) AS ModifiedDate, 
		ISNULL(A.ModifiedBy, A.CreatedBy) AS  ModifiedBy
	FROM ATAttribute AS A

	UNION ALL

	SELECT A.Id, 
		A.SiteId,
		A.Title,
		A.Description,
		A.ModifiedDate, 
		A.ModifiedBy
	FROM ATAttributeVariantCache AS A
)

SELECT A.Id, 
	C.SiteId,
	A.SiteId AS SourceSiteId,
	A.[Key],
	C.Title,
	C.Description,
	ADT.Id AS AttributeDataTypeId,
	ADT.Title AS AttributeDataTypeTitle, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsSearched as IsSearchable,
	A.Code,
	A.IsDisplayed,
	A.IsEnum,
	A.IsPersonalized,
	A.EnableValueTranslation,
	A.Status,
	A.CreatedDate, 
	A.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	C.ModifiedDate, 
	C.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName,
	CAT.CategoryId,
	V.ObjectId
FROM CTE C
	INNER JOIN ATAttribute AS A ON A.Id = C.Id
	INNER JOIN ATAttributeDataType AS ADT ON ADT.Id = A.AttributeDataTypeId
	LEFT JOIN ATAttributeCategoryItem CAT ON A.Id = CAT.AttributeId
	LEFT JOIN ATAttributeValue V ON A.Id = V.AttributeId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = A.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
WHERE A.Status != 3
GO

PRINT 'Modify stored procedure AttributeDto_Save'
GO
IF (OBJECT_ID('AttributeDto_Save') IS NOT NULL)
	DROP PROCEDURE AttributeDto_Save
GO
CREATE PROCEDURE [dbo].[AttributeDto_Save]  
(  
 @Id      UNIQUEIDENTIFIER = NULL OUTPUT,  
 @SiteId     UNIQUEIDENTIFIER,  
 @AttributeDataTypeId UNIQUEIDENTIFIER = NULL,  
 @Key		NVARCHAR(50) = NULL,
 @Title     NVARCHAR(500) = NULL,  
 @Description   NVARCHAR(200) = NULL,  
 @Code     NVARCHAR(18) = NULL,  
 @Sequence    INT = NULL,  
 @IsFaceted    BIT = NULL,  
 @IsSearchable   BIT = NULL,  
 @IsDisplayed   BIT = NULL,  
 @IsPersonalized   BIT = NULL,  
 @IsMultiValued   BIT = NULL,  
 @IsSystem    BIT = NULL,
 @EnableValueTranslation BIT = NULL,
 @Status     INT = NULL,  
 @ModifiedBy    UNIQUEIDENTIFIER,  
 @AttributeCategoryIds NVARCHAR(MAX) = NULL,  
 @EnumValues    TYAttributeEnum READONLY,  
 @ValidationExpressionId UNIQUEIDENTIFIER = NULL,  
 @ValidationMinValue  NVARCHAR(500) = NULL,  
 @ValidationMaxValue  NVARCHAR(500) = NULL
)  
AS  
BEGIN  
 DECLARE @UtcNow DATETIME = GETUTCDATE()  
 DECLARE @EnumDataTypeId UNIQUEIDENTIFIER = (SELECT TOP 1 Id FROM ATAttributeDataType WHERE Title = 'Enumerated')  
 DECLARE @IsNew bit
 SET @IsNew = 0

 IF NOT EXISTS (SELECT 1 FROM ATAttribute WHERE Id = @Id)
	SET @IsNew = 1	  
 -- Insert/update attribute record  
 IF (@IsNew = 1)  
 BEGIN  
  --SET @Id = NEWID()  
      
  INSERT INTO ATAttribute  
  (  
   Id,  
   AttributeDataTypeId,
   [Key],
   Title,  
   [Description],  
   [Sequence],  
   Code,  
   IsFaceted,  
   IsSearched,  
   IsDisplayed,  
   IsPersonalized,  
   IsEnum,  
   IsMultiValued,  
   IsSystem,
   EnableValueTranslation,
   [Status],  
   CreatedDate,  
   CreatedBy,  
   ModifiedDate,  
   ModifiedBy,
   SiteId  
  )  
  VALUES  
  (  
   @Id,  
   @AttributeDataTypeId,
   @Key,
   @Title,  
   @Description,  
   @Sequence,  
   @Code,  
   ISNULL(@IsFaceted, 0),  
   ISNULL(@IsSearchable, 1),  
   ISNULL(@IsDisplayed, 1),  
   ISNULL(@IsPersonalized, 0),  
   CASE WHEN @AttributeDataTypeId = @EnumDataTypeId THEN 1 ELSE 0 END,  
   ISNULL(@IsMultiValued, 0),   
   ISNULL(@IsSystem, 0),  
   ISNULL(@EnableValueTranslation, 0),
   1,  
   @UtcNow,  
   @ModifiedBy,  
   @UtcNow,  
   @ModifiedBy,
   @SiteId  
  )  
 END  
 ELSE  
 BEGIN  
  IF @Status <= 0  
   SET @Status = NULL  
    
  UPDATE ATAttribute  
  SET  AttributeDataTypeId = ISNULL(@AttributeDataTypeId, AttributeDataTypeId),  
    [Key] = ISNULL(@Key, [Key]),
	Title = ISNULL(@Title, Title),  
    [Description] = ISNULL(@Description, [Description]),  
    [Sequence] = ISNULL(@Sequence, [Sequence]),  
    Code = ISNULL(@Code, Code),  
    IsFaceted = ISNULL(@IsFaceted, IsFaceted),  
    IsSearched = ISNULL(@IsSearchable, IsSearched),  
    IsDisplayed = ISNULL(@IsDisplayed, IsDisplayed),  
    IsPersonalized = ISNULL(@IsPersonalized, IsPersonalized),  
    IsEnum = CASE WHEN ISNULL(@AttributeDataTypeId, AttributeDataTypeId) = @EnumDataTypeId THEN 1 ELSE 0 END,  
    IsMultiValued = ISNULL(@IsMultiValued, IsMultiValued),  
    IsSystem = ISNULL(@IsSystem, IsSystem),  
	EnableValueTranslation = ISNULL(@EnableValueTranslation, EnableValueTranslation),
    [Status] = ISNULL(@Status, [Status]),  
    ModifiedDate = @UtcNow,  
    ModifiedBy = @ModifiedBy,
	SiteId = @SiteId  
  WHERE Id = @Id   
 END  
  
 -- Sync enum values if attribute is Enumerable type  
 IF EXISTS (  
  SELECT Id  
  FROM ATAttribute  
  WHERE Id = @Id AND  
    AttributeDataTypeId = @EnumDataTypeId  
 )  
 BEGIN  
  -- Clean up enum data  
  DECLARE @SanitizedEnumValues TYAttributeEnum  
  
  
  INSERT INTO @SanitizedEnumValues  
   SELECT *  
   FROM @EnumValues  
  
  UPDATE @SanitizedEnumValues  
  SET  Id = CASE WHEN Id IS NULL OR Id = dbo.GetEmptyGUID() THEN NEWID() ELSE Id END,  
    AttributeId = @Id,  
    [Status] = ISNULL([Status], 1),  
    CreatedDate = ISNULL(CreatedDate, @UtcNow),  
    CreatedBy = ISNULL(CreatedBy, @ModifiedBy),  
    ModifiedDate = @UtcNow,  
    ModifiedBy = ISNULL(ModifiedBy, @ModifiedBy)  
  
  -- Insert new records  
  INSERT INTO ATAttributeEnum(Id, AttributeId, Title, Code, NumericValue, [Value], IsDefault, [Sequence], [Status], CreatedDate, CreatedBy, ModifiedDate, ModifiedBy)  
   SELECT EV.Id, EV.AttributeId, EV.Title, EV.Code, EV.NumericValue, EV.[Value], EV.IsDefault, EV.[Sequence], EV.[Status], EV.CreatedDate, EV.CreatedBy, EV.ModifiedDate, EV.ModifiedBy  
   FROM @SanitizedEnumValues AS EV  
     LEFT JOIN ATAttributeEnum AS AE ON AE.Id = EV.Id  
   WHERE AE.Id IS NULL  
  
  -- Update existing records  
  UPDATE AE  
  SET  Title = EV.Title, Code = EV.Code, NumericValue = EV.NumericValue, [Value] = EV.[Value], [Sequence] = EV.[Sequence], [Status] = EV.[Status],  
    CreatedDate = EV.CreatedDate, CreatedBy = EV.CreatedBy, ModifiedDate = EV.ModifiedDate, ModifiedBy = EV.ModifiedBy  
  FROM @SanitizedEnumValues AS EV  
    INNER JOIN ATAttributeEnum AS AE ON AE.Id = EV.Id  
  
  -- Mark enum values not passed in table as deleted  
  UPDATE AE  
  SET  [Status] = 3, ModifiedDate = @UtcNow, ModifiedBy = @ModifiedBy  
  FROM ATAttributeEnum AS AE  
    LEFT JOIN @SanitizedEnumValues AS EV ON EV.Id = AE.Id  
  WHERE AE.AttributeId = @Id AND  
    EV.Id IS NULL  
 END  
 -- Mark enum values for non-enum types as deleted  
 ELSE  
 BEGIN  
  UPDATE ATAttributeEnum  
  SET  [Status] = 3, ModifiedDate = @UtcNow, ModifiedBy = @ModifiedBy  
  WHERE AttributeId = @Id  
 END  
  
 -- Map attribute to categories  
 DELETE  
 FROM ATAttributeCategoryItem  
 WHERE AttributeId = @Id  
  
 IF @AttributeCategoryIds IS NOT NULL  
 BEGIN  
  INSERT INTO ATAttributeCategoryItem(Id, AttributeId, CategoryId)  
   SELECT NEWID(), @Id, CONVERT(INT, [Value])  
   FROM dbo.SplitComma(@AttributeCategoryIds, ',')  
 END  
  
 -- Create validation record  
 DELETE  
 FROM ATAttributeValidation  
 WHERE AttributeId = @Id  
  
 INSERT INTO ATAttributeValidation(Id, AttributeId, ExpressionId, MinValue, MaxValue)  
  VALUES (NEWID(), @Id, @ValidationExpressionId, @ValidationMinValue, @ValidationMaxValue)  
END
GO

PRINT 'Modify stored procedure AttributeDto_Get'
GO
IF (OBJECT_ID('AttributeDto_Get') IS NOT NULL)
	DROP PROCEDURE AttributeDto_Get
GO
CREATE PROCEDURE [dbo].[AttributeDto_Get]
(
	@Id				UNIQUEIDENTIFIER = NULL,
	@Ids			NVARCHAR(MAX) = NULL,
	@CategoryId		INT = NULL,
	@GroupId		UNIQUEIDENTIFIER = NULL,
	@SiteId			UNIQUEIDENTIFIER = NULL,
	@ObjectId		UNIQUEIDENTIFIER = NULL,
	@IsFaceted		BIT = NULL,
	@IsSearchable	BIT = NULL,
	@IsDisplayed	BIT = NULL,
	@IsPersonalized	BIT = NULL,
	@IsSystem		BIT = NULL,
	@EnableValueTranslation BIT = NULL,
	@PageNumber		INT = NULL,
	@PageSize		INT = NULL,
	@Status			INT = 1,
	@MaxRecords		INT = NULL,
	@SortBy			NVARCHAR(100) = NULL,  
	@SortOrder		NVARCHAR(10) = NULL,
	@Keyword		NVARCHAR(1000) = NULL,
	@TotalRecords	INT = NULL OUTPUT
)
AS
BEGIN
	DECLARE	@StartRow	INT,
			@EndRow		INT,
			@SortClause	NVARCHAR(100)

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END

	IF @MaxRecords IS NULL OR @MaxRecords <= 0
		SET @MaxRecords = 2147483647

	IF (@SortOrder IS NULL OR @SortClause != 'DESC')
		SET @SortOrder = 'ASC'

	IF (@SortBy IS NOT NULL)
		SET @SortClause = @SortBy + ' ' + @SortOrder	

	SELECT		A.*,
				ROW_NUMBER() OVER (ORDER BY
					CASE WHEN @SortClause = 'Title ASC' THEN A.Title END ASC,
					CASE WHEN @SortClause = 'Title DESC' THEN A.Title END DESC,
					CASE WHEN @SortClause = 'CreatedDate ASC' THEN A.CreatedDate END ASC,  
					CASE WHEN @SortClause = 'CreatedDate DESC' THEN A.CreatedDate END DESC,  
					CASE WHEN @SortClause = 'ModifiedDate ASC' THEN ISNULL(A.ModifiedDate, A.CreatedDate) END ASC,  
					CASE WHEN @SortClause = 'ModifiedDate DESC' THEN ISNULL(A.ModifiedDate, A.CreatedDate) END DESC,  
					CASE WHEN @SortClause IS NULL THEN A.Title END ASC	
				) AS RowNumber
	INTO		#Attributes
	FROM		(
					SELECT		DISTINCT A.Id, A.AttributeDataType, A.AttributeDataTypeId, ADT.Title AS AttributeDataTypeTitle, A.Title, A.[Description], A.[Sequence], A.Code,
								A.IsFaceted, A.IsSearched AS IsSearchable, A.IsDisplayed, A.IsPersonalized, A.IsEnum, A.IsMultiValued, A.IsSystem, A.EnableValueTranslation, A.[Status],
								A.CreatedDate, A.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
								A.ModifiedDate, A.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
					FROM		ATAttribute AS A
								INNER JOIN ATAttributeDataType AS ADT ON ADT.Id = A.AttributeDataTypeId
								LEFT JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
								LEFT JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId
								LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = A.CreatedBy
								LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = A.ModifiedBy
					WHERE		(@Id IS NULL OR A.Id = @Id) AND
								(@Ids IS NULL OR A.Id IN (SELECT [Value] FROM dbo.SplitComma(@Ids, ','))) AND
								--(@SiteId IS NULL OR...) AND
								(@ObjectId IS NULL OR (A.Id IN 
									(SELECT AttributeId FROM ATAttributeValue WHERE ObjectId=@ObjectId
										UNION
									SELECT AttributeId FROM ATSiteAttributeValue WHERE SiteId=@ObjectId
										UNION
									SELECT AttributeId FROM ATPageAttributeValue WHERE PageDefinitionId=@ObjectId
										UNION
									 SELECT AttributeId FROM PRProductTypeAttribute WHERE ProductTypeId=@ObjectId
										UNION	
									SELECT AttributeId FROM PRProductAttribute PA INNER JOIN PRProductSKU S ON PA.ProductId = S.ProductId WHERE S.ProductId=@ObjectId OR S.Id=@ObjectId
										) OR ACI.CategoryId = @CategoryId)) 
								AND
								(@CategoryId IS NULL OR @ObjectId IS NOT NULL OR ACI.CategoryId = @CategoryId) AND
								(@GroupId IS NULL OR AC.GroupId = @GroupId) AND
								(@IsFaceted IS NULL OR A.IsFaceted = @IsFaceted) AND
								(@IsSearchable IS NULL OR A.IsSearched = @IsSearchable) AND
								(@IsDisplayed IS NULL OR A.IsDisplayed = @IsDisplayed) AND
								(@IsPersonalized IS NULL OR A.IsPersonalized = @IsPersonalized) AND
								(@IsSystem IS NULL OR A.IsSystem = @IsSystem) AND
								(@EnableValueTranslation IS NULL OR A.EnableValueTranslation = @EnableValueTranslation) AND
								(@Keyword IS NULL OR A.Title LIKE '%' + @Keyword + '%' OR A.[Description] LIKE '%' + @Keyword + '%') AND	
								(@Status IS NULL OR A.[Status] = @Status)
			) AS A

	SELECT		TOP(@MaxRecords) *
	FROM		#Attributes
	WHERE		@StartRow IS NULL OR
				@EndRow IS NULL OR 
				RowNumber BETWEEN @StartRow AND @EndRow
	ORDER BY	RowNumber ASC

	SELECT		AE.Id, AE.AttributeId, AE.Title, AE.Code, AE.NumericValue, AE.[Value], AE.IsDefault, AE.[Sequence], AE.[Status],
				AE.CreatedDate, AE.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
				AE.ModifiedDate, AE.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
	FROM		#Attributes AS A
				INNER JOIN ATAttributeEnum AS AE ON AE.AttributeId = A.Id
				LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = AE.CreatedBy
				LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = AE.ModifiedBy
	WHERE		A.IsEnum = 1 AND (
					@StartRow IS NULL OR
					@EndRow IS NULL OR 
					A.RowNumber BETWEEN @StartRow AND @EndRow
				) AND
				AE.[Status] = 1
	ORDER BY	A.RowNumber ASC

	SELECT		ACI.Id, ACI.AttributeId, ACI.CategoryId, AC.[Name] AS CategoryName
	FROM		#Attributes AS A
				INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
				INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId
	WHERE		(
					@StartRow IS NULL OR
					@EndRow IS NULL OR 
					A.RowNumber BETWEEN @StartRow AND @EndRow
				) AND
				A.[Status] = 1

	ORDER BY	A.RowNumber ASC

	SELECT		AV.Id, AV.AttributeId, AV.ExpressionId, AV.MinValue, AV.MaxValue, AE.Title AS ExpressionName, AE.RegularExpression
	FROM		#Attributes AS A
				INNER JOIN ATAttributeValidation AS AV ON AV.AttributeId = A.Id
				LEFT JOIN ATAttributeExpression AS AE ON AE.Id = AV.ExpressionId
	WHERE		@StartRow IS NULL OR
				@EndRow IS NULL OR 
				A.RowNumber BETWEEN @StartRow AND @EndRow
	ORDER BY	A.RowNumber ASC

	SET @TotalRecords = (
		SELECT	COUNT(Id)
		FROM	#Attributes
	)
END
GO

PRINT 'Modify view vwContactAttribute'
GO
IF(OBJECT_ID('vwContactAttribute') IS NOT NULL)
	DROP VIEW vwContactAttribute
GO
CREATE VIEW [dbo].[vwContactAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ContactId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 3
	LEFT JOIN ATContactAttributeValue V ON A.Id = V.AttributeId
GO

PRINT 'Modify view vwCustomerAttribute'
GO
IF(OBJECT_ID('vwCustomerAttribute') IS NOT NULL)
	DROP VIEW vwCustomerAttribute
GO
CREATE VIEW [dbo].[vwCustomerAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.CustomerId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 4
	LEFT JOIN CSCustomerAttributeValue V ON A.Id = V.AttributeId
GO

PRINT 'Modify view vwOrderAttribute'
GO
IF(OBJECT_ID('vwOrderAttribute') IS NOT NULL)
	DROP VIEW vwOrderAttribute
GO
CREATE VIEW [dbo].[vwOrderAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.OrderId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 8
	LEFT JOIN OROrderAttributeValue V ON A.Id = V.AttributeId
GO
PRINT 'Modify view vwOrderItem'
GO
IF(OBJECT_ID('vwOrderItem') IS NOT NULL)
	DROP VIEW vwOrderItem
GO
CREATE VIEW vwOrderItem
AS
SELECT  OI.[Id],
	O.[SiteId],
	O.[OrderStatusId],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	S.[SKU],
	ISNULL(SC.[Title], S.[Title]) AS SKUTitle,
	OI.[Quantity],
	ISNULL(SQ.Quantity, 0) AS ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	OI.CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName ,
	LN.UserFullName ModifiedByFullName ,
	OI.ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount],
	OI.[TaxableDiscount],
	OI.[NonTaxableDiscount],
	OI.[HandlingTax],
	OI.[Status],
	P.Id AS ProductId,
	ISNULL(PC.Title, P.Title) AS ProductTitle,
	ISNULL(RQ.Quantity, 0) AS ReturnedQuantity,
	PT.IsDownloadableMedia
FROM OROrderItem OI
	INNER JOIN OROrder O ON O.Id = OI.OrderId
	INNER JOIN PRProductSKU S ON S.Id = OI.ProductSKUId
	LEFT JOIN PRProductSKUVariantCache SC ON SC.Id = OI.ProductSKUId AND SC.SiteId = O.SiteId
	INNER JOIN PRProduct P ON P.Id = S.ProductId
	LEFT JOIN PRProductVariantCache PC ON PC.Id = S.ProductId AND PC.SiteId = O.SiteId
	INNER JOIN PRProductType PT ON PT.Id = P.ProductTypeID
	LEFT JOIN vwOrderItemShippedQuantity SQ ON SQ.Id = OI.Id
	LEFT JOIN vwOrderItemReturnedQuantity RQ ON RQ.Id = OI.Id
	LEFT JOIN VW_UserFullName FN ON FN.UserId = OI.CreatedBy
	LEFT JOIN VW_UserFullName LN ON LN.UserId = OI.ModifiedBy
GO
PRINT 'Modify view vwOrderItemAttribute'
GO
IF(OBJECT_ID('vwOrderItemAttribute') IS NOT NULL)
	DROP VIEW vwOrderItemAttribute
GO
CREATE VIEW [dbo].[vwOrderItemAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.OrderItemId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 9
	LEFT JOIN OROrderItemAttributeValue V ON A.Id = V.AttributeId
GO

PRINT 'Modify view vwPageAttribute'
GO
IF(OBJECT_ID('vwPageAttribute') IS NOT NULL)
	DROP VIEW vwPageAttribute
GO
CREATE VIEW [dbo].[vwPageAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 2
	LEFT JOIN vwPageAttributeValue V ON A.Id = V.AttributeId
GO

PRINT 'Modify view vwProductAttribute'
GO
IF(OBJECT_ID('vwProductAttribute') IS NOT NULL)
	DROP VIEW vwProductAttribute
GO
CREATE VIEW [dbo].[vwProductAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ProductId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 6
	LEFT JOIN PRProductAttribute V ON A.Id = V.AttributeId AND V.IsSKULevel = 0
GO

PRINT 'Modify view vwProductTypeProductAttribute'
GO
IF(OBJECT_ID('vwProductTypeProductAttribute') IS NOT NULL)
	DROP VIEW vwProductTypeProductAttribute
GO
CREATE VIEW [dbo].[vwProductTypeProductAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ProductTypeId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 5
	LEFT JOIN PRProductTypeAttribute V ON A.Id = V.AttributeId AND V.IsSKULevel = 0
GO

PRINT 'Modify view vwProductTypeSkuAttribute'
GO
IF(OBJECT_ID('vwProductTypeSkuAttribute') IS NOT NULL)
	DROP VIEW vwProductTypeSkuAttribute
GO
CREATE VIEW [dbo].[vwProductTypeSkuAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ProductTypeId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 10
	LEFT JOIN PRProductTypeAttribute V ON A.Id = V.AttributeId AND V.IsSKULevel = 1
GO
PRINT 'Modify view vwSite'
GO
IF(OBJECT_ID('vwSite') IS NOT NULL)
	DROP VIEW vwSite
GO
CREATE VIEW [dbo].[vwSite]
AS
SELECT S.Id,
	S.Title,
	S.Description,
	S.ParentSiteId,
	S.MasterSiteId,
	S.PrimarySiteUrl,
	S.ExternalCode,
	S.Status,
	S.DistributionEnabled,
	S.AllowSynchronization,
	S.TranslationEnabled,
	S.SubmitToTranslation AS AllowTranslation,
	S.CreatedBy,
	S.CreatedDate,
	S.ModifiedBy,
	S.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM [dbo].[SISite] S
	LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
WHERE S.Status != 3
GO
PRINT 'Modify view vwSiteAttribute'
GO
IF(OBJECT_ID('vwSiteAttribute') IS NOT NULL)
	DROP VIEW vwSiteAttribute
GO
CREATE VIEW [dbo].[vwSiteAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle,
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 1
	LEFT JOIN vwSiteAttributeValue V ON A.Id = V.AttributeId
GO

PRINT 'Modify view vwSkuAttribute'
GO
IF(OBJECT_ID('vwSkuAttribute') IS NOT NULL)
	DROP VIEW vwSkuAttribute
GO
CREATE VIEW [dbo].[vwSkuAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.[Key],
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	S.Id AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed,
	A.EnableValueTranslation
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 7
	LEFT JOIN PRProductAttribute V ON A.Id = V.AttributeId AND V.IsSKULevel = 1
	LEFT JOIN PRProductSku S ON S.ProductId = V.ProductId
GO
PRINT 'Modify view vwCustomer'
GO
IF(OBJECT_ID('vwCustomer') IS NOT NULL)
	DROP VIEW vwCustomer
GO
CREATE VIEW [dbo].[vwCustomer] 
AS         
SELECT	CUP.Id, 
	U.FirstName, 
	U.LastName, 
	U.MiddleName, 
	U.UserName, 
	NULL AS [Password], 
	M.Email, 
	U.EmailNotification, 
	U.CompanyName, 
	U.BirthDate, 
	U.Gender, 
	U.LeadScore,
	CUP.IsBadCustomer, 
	CUP.IsActive, 
	M.IsApproved, 
	M.IsLockedOut, 
	CUP.IsOnMailingList, 
	CUP.[Status], 
	CUP.CSRSecurityQuestion, 
	CUP.CSRSecurityPassword, 
	U.HomePhone,
	U.MobilePhone, 
	U.OtherPhone, 
	U.ImageId, 
	CUP.AccountNumber, 
	CUP.IsExpressCustomer, 
	CUP.ExternalId, 
	CUP.ExternalProfileId,
	(SELECT	COUNT(*) FROM CSCustomerLogin WHERE	CustomerId = CUP.Id) + 1 AS NumberOfLoginAccounts,
	CASE WHEN EXISTS (SELECT Id FROM PTLineOfCreditInfo WHERE CustomerId = CUP.Id AND [Status] != 3) THEN 1 ELSE 0 END AS HasLOC,
	A.Id AS AddressId, 
	A.AddressLine1, 
	A.AddressLine2, 
	A.AddressLine3, 
	A.City, 
	A.StateId, 
	ST.[State], 
	ST.StateCode,
	A.CountryId, 
	C.CountryName, 
	C.CountryCode, 
	A.Zip, 
	A.County, 
	A.Phone,
	U.ExpiryDate AS ExpirationDate, 
	U.LastActivityDate,
	CASE WHEN EXISTS (SELECT 1 FROM	USSiteUser WHERE UserId = U.Id AND IsSystemUser = 1) THEN 1 ELSE 0 END AS IsSystemUser,
	CUP.IsTaxExempt,
	CUP.TaxId,
	U.CreatedDate,  
	U.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	ISNULL(U.ModifiedDate, U.CreatedDate) AS ModifiedDate, 
	ISNULL(U.ModifiedBy, U.CreatedBy) AS ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName
FROM USCommerceUserProfile AS CUP
	INNER JOIN USUser AS U ON U.Id = CUP.Id
	INNER JOIN USMembership AS M ON M.UserId = U.Id
	LEFT JOIN USUserShippingAddress AS USA ON USA.UserId = U.Id AND USA.IsPrimary = 1
	LEFT JOIN GLAddress AS A ON A.Id = USA.AddressId
	LEFT JOIN GLState AS ST ON ST.Id = A.StateId
	LEFT JOIN GLCountry AS C ON C.Id = A.CountryId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = U.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = U.ModifiedBy
WHERE U.[Status] !=3
GO
PRINT 'Modify view vwCustomerList'
GO
IF(OBJECT_ID('vwCustomerList') IS NOT NULL)
	DROP VIEW vwCustomerList
GO
CREATE VIEW [dbo].[vwCustomerList] 
AS    
SELECT	C.Id, 
	C.FirstName, 
	C.LastName, 
	C.MiddleName, 
	C.UserName, 
	C.Password, 
	C.Email, 
	C.EmailNotification, 
	C.CompanyName, 
	C.BirthDate, 
	C.Gender, 
	C.LeadScore,
	C.IsBadCustomer, 
	C.IsActive, 
	C.IsApproved, 
	C.IsLockedOut, 
	C.IsOnMailingList, 
	C.[Status], 
	C.CSRSecurityQuestion, 
	C.CSRSecurityPassword, 
	C.HomePhone,
	C.MobilePhone, 
	C.OtherPhone, 
	C.ImageId, 
	C.AccountNumber, 
	C.IsExpressCustomer, 
	C.ExternalId, 
	C.ExternalProfileId,
	C.NumberOfLoginAccounts,
	C.HasLOC,
	C.AddressId, 
	C.AddressLine1, 
	C.AddressLine2, 
	C.AddressLine3, 
	C.City, 
	C.StateId, 
	C.[State], 
	C.StateCode,
	C.CountryId, 
	C.CountryName, 
	C.CountryCode, 
	C.Zip, 
	C.County, 
	C.Phone,
	C.ExpirationDate, 
	C.LastActivityDate,
	C.IsSystemUser,
	C.IsTaxExempt,
	C.TaxId,
	C.CreatedDate,  
	C.CreatedBy, 
	C.CreatedByFullName,
	C.ModifiedDate, 
	C.ModifiedBy, 
	C.ModifiedByFullName,
	S.Id AS SiteId, 
	CASE WHEN CO.OrderSiteId = S.Id THEN CO.TotalOrders ELSE 0 END AS TotalOrders, 
	CASE WHEN CO.OrderSiteId = S.Id THEN CO.TotalSpend ELSE 0 END AS TotalSpend, 
	CO.LastPurchase
FROM vwCustomer C
	CROSS JOIN SISite S
	LEFT JOIN  
		(
			SELECT CustomerId, 
				SiteId AS OrderSiteId, 
				COUNT(*) AS TotalOrders, 
				SUM(GrandTotal) AS TotalSpend, 
				MAX(OrderDate) AS LastPurchase
			FROM OROrder
			GROUP BY CustomerId, SiteId
		) AS CO ON CO.CustomerId = C.Id and CO.OrderSiteId = S.Id
WHERE S.Status = 1 
GO
print 'altering BLD_ExportProductTypes'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportProductTypes')
    drop procedure BLD_ExportProductTypes
go

CREATE PROCEDURE [dbo].[BLD_ExportProductTypes]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	declare @rootId uniqueidentifier
	select @rootId=Id from PRProductType where ParentId=SiteId
	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		BEGIN
			;with cte as (
				select pt1.Id as ProductTypeId
					,pt1.Title as ProductTypeName
					,cast(N'' as nvarchar(555)) as ParentProductTypeName 
					,cast(NULL as uniqueidentifier) as ParentProductTypeId 
					,pt1.Description
					,pt1.IsDownloadableMedia
					,0 as UpdateFlag
				from dbo.PRPRoductType pt1
				where pt1.ParentId=@rootId

				union all

				select pt.Id as ProductTypeId
					,pt.Title as ProductTypeName
					,c.ProductTypeName
					,c.ProductTypeId
					,pt.Description
					,pt.IsDownloadableMedia
					,0 as UpdateFlag
				from PRPRoductType pt
					join cte c on (pt.ParentId = c.ProductTypeId)
			)
			select * from cte
		END
	ELSE
		BEGIN
			;with cte as (
				select pt.Id as ProductTypeId
					,isnull(c.Title, pt.Title) as ProductTypeName
					,cast(N'' as nvarchar(555)) as ParentProductTypeName 
					,cast(NULL as uniqueidentifier) as ParentProductTypeId 
					,pt.Description
					,pt.IsDownloadableMedia
					,0 as UpdateFlag
					,c.SiteId
				from PRProductTypeVariantCache c
					left join dbo.PRPRoductType pt on c.Id=pt.Id
				where pt.Id=@rootId and c.SiteId=@SiteId

				union all

				select c.Id as ProductTypeId
					,c.Title as ProductTypeName
					,case when cte.ProductTypeId=@rootId then N'' else cte.ProductTypeName end
					,case when cte.ProductTypeId=@rootId then NULL else cte.ProductTypeId end
					,c.Description
					,pt.IsDownloadableMedia
					,0 as UpdateFlag
					,c.SiteId
				from PRProductTypeVariantCache c
					join PRPRoductType pt on c.Id=pt.Id
					join cte on (pt.ParentId = cte.ProductTypeId)
				where c.SiteId=@SiteId
			)
			select * from cte
		END
END
GO

PRINT 'Modify view vwAttributeSimple'
GO
IF(OBJECT_ID('vwAttributeSimple') IS NOT NULL)
	DROP VIEW vwAttributeSimple
GO
CREATE VIEW [dbo].[vwAttributeSimple]
AS
SELECT DISTINCT A.Id, 
	A.SiteId,
	A.Title,
	A.EnableValueTranslation
FROM vwAttribute A
GO

PRINT 'Modify stored procedure SiteDto_SubmitTranslation'
GO
IF (OBJECT_ID('SiteDto_SubmitTranslation') IS NOT NULL)
	DROP PROCEDURE SiteDto_SubmitTranslation
GO
CREATE PROCEDURE [dbo].[SiteDto_SubmitTranslation]
(
	@SourceSiteId			uniqueidentifier,
	@TargetSiteIds			nvarchar(max),
	@TranslateMenu			bit = NULL,
	@TranslatePage			bit = NULL,
	@TranslateAttributes	bit = NULL,
	@TranslateProduct		bit = NULL,
	@TranslateProductType	bit = NULL,
	@TranslateEmailTemplate	bit = NULL,
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	DECLARE @tbSiteIds TABLE(Id uniqueidentifier primary key)
	INSERT INTO @tbSiteIds
	SELECT Items FROM dbo.SplitGUID(@TargetSiteIds, ',')

	-- Menu Start
	IF @TranslateMenu != 0
	BEGIN
		INSERT INTO TLTranslation
		(	
			Id,
			Title,
			ObjectId,
			ObjectType,
			SiteId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			P.DisplayTitle,
			P.PageMapNodeId,
			2,
			@SourceSiteId,
			@ModifiedBy,
			@UtcNow
		FROM PageMapNode P
			LEFT JOIN TLTranslation T ON T.ObjectId = P.PageMapNodeId
		WHERE P.PageMapNodeId = @SourceSiteId
			AND T.Id IS NULL
		
		INSERT INTO TLDocument
		(
			Id,
			TranslationId,
			Locale,
			Title,
			ExternalId,
			Status,
			ProcessingStatus,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			T.Id,
			S.DefaultTranslateToLanguage,
			T.Title,
			T.ExternalId,
			2,
			1,
			@ModifiedBy,
			@UtcNow
		FROM TLTranslation T
			JOIN SISite S ON T.SiteId = S.Id
		WHERE T.ObjectId = @SourceSiteId

		INSERT INTO TLDocumentTarget
		(
			Id,
			TranslationDocumentId,
			TargetId,
			TargetTypeId
		)
		SELECT NEWID(),
			D.Id,
			S.Id,
			2
		FROM TLDocument D
			JOIN TLTranslation T ON T.Id = D.TranslationId
			CROSS APPLY @tbSiteIds S
		WHERE T.ObjectId = @SourceSiteId AND T.ObjectType = 2
			AND D.Status = 2
	END
	-- Menu End

	-- Page Start
	IF @TranslatePage != 0
	BEGIN
		INSERT INTO TLTranslation
		(	
			Id,
			Title,
			ObjectId,
			ObjectType,
			SiteId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			P.Title,
			P.PageDefinitionId,
			8,
			@SourceSiteId,
			@ModifiedBy,
			@UtcNow
		FROM PageDefinition P
			LEFT JOIN TLTranslation T ON T.ObjectId = P.PageDefinitionId
		WHERE P.SiteId = @SourceSiteId AND P.PageStatus = 8 AND NULLIF(P.ExcludeFromTranslation, 0) IS NULL
			AND T.Id IS NULL
		
		INSERT INTO TLDocument
		(
			Id,
			TranslationId,
			Locale,
			Title,
			ExternalId,
			Status,
			ProcessingStatus,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			T.Id,
			S.DefaultTranslateToLanguage,
			T.Title,
			T.ExternalId,
			2,
			1,
			@ModifiedBy,
			@UtcNow
		FROM TLTranslation T
			JOIN SISite S ON T.SiteId = S.Id
			JOIN PageDefinition P ON P.PageDefinitionId = T.ObjectId AND T.ObjectType = 8
		WHERE T.SiteId = @SourceSiteId AND P.PageStatus = 8 AND NULLIF(P.ExcludeFromTranslation, 0) IS NULL

		INSERT INTO TLDocumentTarget
		(
			Id,
			TranslationDocumentId,
			TargetId,
			TargetTypeId
		)
		SELECT NEWID(),
			D.Id,
			S.Id,
			2
		FROM TLDocument D
			JOIN TLTranslation T ON T.Id = D.TranslationId
			CROSS APPLY @tbSiteIds S
			LEFT JOIN PageDefinition P ON P.SourcePageDefinitionId = T.ObjectId AND P.SiteId = S.Id
		WHERE T.SiteId = @SourceSiteId AND T.ObjectType = 8
			AND D.Status = 2
			AND NULLIF(P.ExcludeFromTranslation, 0) IS NULL
		END
	-- Page End

	--Attributes Start
	IF @TranslateAttributes != 0
	BEGIN
		INSERT INTO TLTranslation
		(	
			Id,
			Title,
			ObjectId,
			ObjectType,
			SiteId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			A.Title,
			A.Id,
			51,
			@SourceSiteId,
			@ModifiedBy,
			@UtcNow
		FROM vwAttributeSimple A
			LEFT JOIN TLTranslation T ON T.ObjectId = A.Id AND T.SiteId = A.SiteId
		WHERE A.SiteId = @SourceSiteId
			AND T.Id IS NULL
		
		INSERT INTO TLDocument
		(
			Id,
			TranslationId,
			Locale,
			Title,
			ExternalId,
			Status,
			ProcessingStatus,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			T.Id,
			S.DefaultTranslateToLanguage,
			T.Title,
			T.ExternalId,
			2,
			1,
			@ModifiedBy,
			@UtcNow
		FROM TLTranslation T
			JOIN SISite S ON T.SiteId = S.Id
		WHERE T.SiteId = @SourceSiteId AND T.ObjectType = 51

		INSERT INTO TLDocumentTarget
		(
			Id,
			TranslationDocumentId,
			TargetId,
			TargetTypeId
		)
		SELECT NEWID(),
			D.Id,
			S.Id,
			2
		FROM TLDocument D
			JOIN TLTranslation T ON T.Id = D.TranslationId
			CROSS APPLY @tbSiteIds S
		WHERE T.SiteId = @SourceSiteId AND T.ObjectType = 51
			AND D.Status = 2
		END
	--Attributes End

	-- ProductType Start
	IF @TranslateProductType != 0
	BEGIN
		INSERT INTO TLTranslation
		(	
			Id,
			Title,
			ObjectId,
			ObjectType,
			SiteId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			P.Title,
			P.Id,
			204,
			@SourceSiteId,
			@ModifiedBy,
			@UtcNow
		FROM vwProductTypeList P
			LEFT JOIN TLTranslation T ON T.ObjectId = P.Id AND T.SiteId = P.SiteId
		WHERE P.SiteId = @SourceSiteId
			AND T.Id IS NULL
		
		INSERT INTO TLDocument
		(
			Id,
			TranslationId,
			Locale,
			Title,
			ExternalId,
			Status,
			ProcessingStatus,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			T.Id,
			S.DefaultTranslateToLanguage,
			T.Title,
			T.ExternalId,
			2,
			1,
			@ModifiedBy,
			@UtcNow
		FROM TLTranslation T
			JOIN SISite S ON T.SiteId = S.Id
		WHERE T.SiteId = @SourceSiteId AND T.ObjectType = 204

		INSERT INTO TLDocumentTarget
		(
			Id,
			TranslationDocumentId,
			TargetId,
			TargetTypeId
		)
		SELECT NEWID(),
			D.Id,
			S.Id,
			2
		FROM TLDocument D
			JOIN TLTranslation T ON T.Id = D.TranslationId
			CROSS APPLY @tbSiteIds S
		WHERE T.SiteId = @SourceSiteId AND T.ObjectType = 204
			AND D.Status = 2
		END
	-- ProductType End

	-- Product Start
	IF @TranslateProduct != 0
	BEGIN
		INSERT INTO TLTranslation
		(	
			Id,
			Title,
			ObjectId,
			ObjectType,
			SiteId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			P.Title,
			P.Id,
			205,
			@SourceSiteId,
			@ModifiedBy,
			@UtcNow
		FROM vwProduct P
			LEFT JOIN TLTranslation T ON T.ObjectId = P.Id AND T.SiteId = P.SiteId
		WHERE P.SiteId = @SourceSiteId AND P.IsActive = 1
			AND T.Id IS NULL
			AND EXISTS (SELECT 1 FROM vwSKU S WHERE S.ProductId = P.Id AND S.SiteId = P.SiteId AND S.IsActive = 1 AND S.IsOnline = 1)
		
		INSERT INTO TLDocument
		(
			Id,
			TranslationId,
			Locale,
			Title,
			ExternalId,
			Status,
			ProcessingStatus,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			T.Id,
			S.DefaultTranslateToLanguage,
			T.Title,
			T.ExternalId,
			2,
			1,
			@ModifiedBy,
			@UtcNow
		FROM TLTranslation T
			JOIN SISite S ON T.SiteId = S.Id
			JOIN vwProduct P ON P.Id = T.ObjectId AND T.ObjectType = 205 AND P.SiteId = S.Id
		WHERE P.SiteId = @SourceSiteId AND P.IsActive = 1
			AND EXISTS (SELECT 1 FROM vwSKU S WHERE S.ProductId = P.Id AND S.SiteId = P.SiteId AND S.IsActive = 1 AND S.IsOnline = 1)

		INSERT INTO TLDocumentTarget
		(
			Id,
			TranslationDocumentId,
			TargetId,
			TargetTypeId
		)
		SELECT NEWID(),
			D.Id,
			S.Id,
			2
		FROM TLDocument D
			JOIN TLTranslation T ON T.Id = D.TranslationId
			CROSS APPLY @tbSiteIds S
		WHERE T.SiteId = @SourceSiteId AND T.ObjectType = 205
			AND D.Status = 2
		END
	-- Product End

	-- Email Template Start
	IF @TranslateEmailTemplate != 0
	BEGIN
		INSERT INTO TLTranslation
		(	
			Id,
			Title,
			ObjectId,
			ObjectType,
			SiteId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			P.Title,
			P.Id,
			230,
			@SourceSiteId,
			@ModifiedBy,
			@UtcNow
		FROM vwEmailTemplate P
			LEFT JOIN TLTranslation T ON T.ObjectId = P.Id AND T.SiteId = P.SiteId
		WHERE P.SiteId = @SourceSiteId
			AND T.Id IS NULL
		
		INSERT INTO TLDocument
		(
			Id,
			TranslationId,
			Locale,
			Title,
			ExternalId,
			Status,
			ProcessingStatus,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			T.Id,
			S.DefaultTranslateToLanguage,
			T.Title,
			T.ExternalId,
			2,
			1,
			@ModifiedBy,
			@UtcNow
		FROM TLTranslation T
			JOIN SISite S ON T.SiteId = S.Id
		WHERE T.SiteId = @SourceSiteId AND T.ObjectType = 230

		INSERT INTO TLDocumentTarget
		(
			Id,
			TranslationDocumentId,
			TargetId,
			TargetTypeId
		)
		SELECT NEWID(),
			D.Id,
			S.Id,
			2
		FROM TLDocument D
			JOIN TLTranslation T ON T.Id = D.TranslationId
			CROSS APPLY @tbSiteIds S
		WHERE T.SiteId = @SourceSiteId AND T.ObjectType = 230
			AND D.Status = 2
		END
	-- Email Template End
END
GO
PRINT 'Modify view vwFeatureProduct'
GO
IF (OBJECT_ID('vwFeatureProduct') IS NOT NULL)
	DROP VIEW vwFeatureProduct	
GO
CREATE VIEW vwFeatureProduct
AS
SELECT	FO.FeatureId,
		CASE WHEN E.FeatureId IS NULL THEN 0 ELSE 1 END AS ExcludedFromFeature,
		0 AS [Sequence],
		P.[Id],   
		P.SiteId,  
		P.SourceSiteId,  
		P.Title,   
		P.ShortDescription,   
		P.LongDescription,   
		P.[Description],
		P.ProductIDUser,   
		P.Keyword,   
		P.ProductStyle,  
		P.BundleCompositionLastModified,   
		P.UrlFriendlyTitle,   
		P.TaxCategoryID,   
		P.IsActive,   
		P.PromoteAsNewItem,   
		P.PromoteAsTopSeller,  
		P.ExcludeFromDiscounts,   
		P.ExcludeFromShippingPromotions,   
		P.Freight,   
		P.Refundable,   
		P.TaxExempt,  
		P.ProductTypeID,   
		P.[Status],   
		P.IsBundle,   
		P.ProductTypeName,  
		P.ProductUrl,
		P.SEOTitle,
		P.SEOH1,   
		P.SEODescription,   
		P.SEOKeywords,   
		P.SEOFriendlyUrl,  
		P.IsShared,  
		P.CreatedDate,   
		P.CreatedById,   
		P.CreatedByFullName,  
		P.ModifiedDate,   
		P.ModifiedById,   
		P.ModifiedByFullName
FROM	MRFeatureOutput AS FO
		INNER JOIN MRFeature AS F ON F.Id = FO.FeatureId
		INNER JOIN vwProduct AS P ON P.Id = FO.ProductId
		LEFT JOIN MRFeatureAutoExcludedItems AS E ON E.FeatureId = FO.FeatureId AND E.ProductId = P.Id
WHERE	F.FeatureTypeId != 3

UNION

SELECT	FO.FeatureId,
		NULL,
		FO.[Sequence],
		P.[Id],   
		P.SiteId,  
		P.SourceSiteId,  
		P.Title,   
		P.ShortDescription,   
		P.LongDescription,   
		P.[Description],
		P.ProductIDUser,   
		P.Keyword,   
		P.ProductStyle,  
		P.BundleCompositionLastModified,   
		P.UrlFriendlyTitle,   
		P.TaxCategoryID,   
		P.IsActive,   
		P.PromoteAsNewItem,   
		P.PromoteAsTopSeller,  
		P.ExcludeFromDiscounts,   
		P.ExcludeFromShippingPromotions,   
		P.Freight,   
		P.Refundable,   
		P.TaxExempt,  
		P.ProductTypeID,   
		P.[Status],   
		P.IsBundle,   
		P.ProductTypeName,  
		P.ProductUrl,
		P.SEOTitle,
		P.SEOH1,   
		P.SEODescription,   
		P.SEOKeywords,   
		P.SEOFriendlyUrl,  
		P.IsShared,  
		P.CreatedDate,   
		P.CreatedById,   
		P.CreatedByFullName,  
		P.ModifiedDate,   
		P.ModifiedById,   
		P.ModifiedByFullName
FROM	MRFeatureManualItems AS FO
		INNER JOIN MRFeature AS F ON F.Id = FO.FeatureId
		INNER JOIN vwProduct AS P ON FO.ProductId = P.Id
WHERE	F.FeatureTypeId = 3
GO

PRINT 'Modify view vwFeatureProductSearch'
GO
IF (OBJECT_ID('vwFeatureProductSearch') IS NOT NULL)
	DROP VIEW vwFeatureProductSearch	
GO
CREATE VIEW [dbo].[vwFeatureProductSearch]  
AS  
SELECT	P.FeatureId,
		P.ExcludedFromFeature,
		P.[Sequence],
		P.[Id],   
		P.SiteId,  
		P.SourceSiteId,  
		P.Title,   
		P.ShortDescription,   
		P.LongDescription,   
		P.[Description],
		P.ProductIDUser,   
		P.Keyword,   
		P.ProductStyle,  
		P.BundleCompositionLastModified,   
		P.UrlFriendlyTitle,   
		P.TaxCategoryID,   
		P.IsActive,   
		P.PromoteAsNewItem,   
		P.PromoteAsTopSeller,  
		P.ExcludeFromDiscounts,   
		P.ExcludeFromShippingPromotions,   
		P.Freight,   
		P.Refundable,   
		P.TaxExempt,  
		P.ProductTypeID,   
		P.[Status],   
		P.IsBundle,   
		P.ProductTypeName,  
		P.ProductUrl,
		P.SEOTitle,
		P.SEOH1,   
		P.SEODescription,   
		P.SEOKeywords,   
		P.SEOFriendlyUrl,  
		P.IsShared,  
		P.CreatedDate,   
		P.CreatedById,   
		P.CreatedByFullName,  
		P.ModifiedDate,   
		P.ModifiedById,   
		P.ModifiedByFullName,
		SKU.SKU,   
		SKU.Title AS SkuTitle
FROM	vwFeatureProduct AS P
		LEFT JOIN vwSKU AS SKU ON SKU.ProductId = P.Id AND SKU.SiteId = P.SiteId  
GO

PRINT 'Modify stored procedure SiteDto_Get'
GO
IF (OBJECT_ID('SiteDto_Get') IS NOT NULL)
	DROP PROCEDURE SiteDto_Get
GO
CREATE PROCEDURE [dbo].[SiteDto_Get]
(
	@Id						uniqueidentifier = NULL,
	@Ids					nvarchar(max)=NULL,
	@MasterSiteId			uniqueidentifier = NULL,
	@IncludeVariantSites	bit = NULL,
	@CurrentMasterSiteOnly	bit = NULL,
	@AllowTranslation		bit = NULL,
	@CampaignId				uniqueidentifier = NULL,
	@SiteListId				uniqueidentifier = NULL,
	@UserId					uniqueidentifier = NULL,
	@SiteId					uniqueidentifier = NULL,
	@Status					int = NULL,
	@PageNumber				int = NULL,
	@PageSize				int = NULL,
	@MaxRecords				int = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@Keyword				nvarchar(MAX) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50), 
		@IdsExist bit, @LftValue int, @RgtValue int
	SET @PageLowerBound = @PageSize * @PageNumber

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @IncludeVariantSites IS NULL SET @IncludeVariantSites = 1
	IF @Status IS NULL SET @Status = 1
	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'

	IF @MasterSiteId IS NULL AND @CurrentMasterSiteOnly = 1
		SELECT TOP 1 @MasterSiteId = MasterSiteId FROM SISite WHERE Id = @SiteId
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbSiteIds TABLE (Id uniqueidentifier)
	
	IF @Ids IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT Items FROM dbo.SplitGUID(@Ids, ',') 
	END
	IF @SiteListId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM SISiteListSite WHERE SiteListId = @SiteListId
	END
	
	IF @CampaignId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		DECLARE @CampaignSiteId uniqueidentifier
		SELECT TOP 1 @CampaignSiteId = ApplicationId FROM MKCampaign WHERE Id = @CampaignId
		IF @CampaignSiteId = @SiteId SET @CampaignSiteId = NULL

		INSERT INTO @tbSiteIds
		SELECT SiteId FROM MKCampaignRunHistory H
		WHERE EXISTS (SELECT 1 FROM MKCampaignSend S WHERE H.CampaignSendId = S.Id 
				AND CampaignId = @CampaignId AND (@CampaignSiteId IS NULL OR SiteId = @SiteId))
	END

	IF @UserId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM [dbo].[GetAccessibleSitesForUser](@UserId)
	END
	
	INSERT INTO @tbIds
	SELECT S.Id AS Id, ROW_NUMBER() OVER (ORDER BY
							CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
							CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
							CASE WHEN @SortClause IS NULL THEN CreatedDate END ASC	
						) AS RowNumber
	FROM SISite AS S
	WHERE (@Id IS NULL OR S.Id = @Id) AND
		(@AllowTranslation IS NULL OR S.SubmitToTranslation = @AllowTranslation) AND
		(@Status IS NULL OR S.Status = @Status) AND
		(@MasterSiteId IS NULL OR S.Id = @MasterSiteId OR S.MasterSiteId = @MasterSiteId) AND
		(@IncludeVariantSites = 1 OR S.MasterSiteId = S.Id OR S.MasterSiteId IS NULL) AND
		(@IdsExist IS NULL OR EXISTS (Select 1 FROM @tbSiteIds T Where T.Id = S.Id)) AND
		(@Keyword IS NULL OR (S.Title like @Keyword OR S.Description like @Keyword OR S.PrimarySiteUrl like @Keyword OR S.ExternalCode like @Keyword))
		
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				S.Id AS Id
		FROM SISite S
			JOIN @tbIds T ON T.Id = S.Id
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT		S.*,
				S.SubmitToTranslation AS AllowTranslation,
				S.DefaultTranslateToLanguage AS TranslationLocale,
				SiteHierarchyPath AS SitePath,
				T.RowNumber,
				T.TotalRecords
	FROM SISite AS S
		JOIN @tbPagedResults AS T ON T.Id = S.Id
	ORDER BY RowNumber

	SELECT V.*, A.Title AS AttributeTitle
	FROM ATSiteAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbPagedResults T ON V.SiteId = T.Id
END
GO
PRINT 'Modify stored procedure BLD_ExportProducts'
GO
IF(OBJECT_ID('BLD_ExportProducts') IS NOT NULL)
	DROP PROCEDURE BLD_ExportProducts
GO
CREATE PROCEDURE [dbo].[BLD_ExportProducts]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		BEGIN
			select 
				 p.ProductIDUser as ProductId
				,p.Title as ProductName
				,cast(p.ProductTypeID as varchar(max)) as ProductTypeId
				,pt.Title as ProductType
				,p.ShortDescription
				,p.Description
				,p.LongDescription
				,p.IsActive
				,0 as UpdateFlag
				,coalesce(SEOTitle, '') as 'SEO Title'
				,coalesce(SEODescription, '') as 'SEO Description'
				,coalesce(SEOKeywords, '') as 'SEO Keywords'
				,coalesce(SEOFriendlyUrl, '') as 'SEO Friendly URL'
				,coalesce(SEOH1, '') as 'SEO H1'
				,p.IsShared
			from dbo.PRProduct p
			join dbo.PRProductType pt on (p.ProductTypeID = pt.Id)
			order by p.Title
		END
	ELSE
		BEGIN
			select
				 p.ProductIDUser as ProductId
				,ISNULL(prvc.Title, p.Title) as ProductName
				,cast(p.ProductTypeID as varchar(max)) as ProductTypeId
				,pt.Title as ProductType
				,ISNULL(prvc.ShortDescription ,p.ShortDescription) as ShortDescription
				,ISNULL(prvc.Description ,p.Description) as Description
				,ISNULL(prvc.LongDescription, p.LongDescription) as LongDescription
				,ISNULL(prvc.IsActive, p.IsActive) as IsActive
				,0 as UpdateFlag
				,coalesce(prvc.SEOTitle, p.SEOTitle) as [SEO Title]
				,coalesce(prvc.SEODescription, p.SEODescription) as 'SEO Description'
				,coalesce(prvc.SEOKeywords, p.SEOKeywords) as 'SEO Keywords'
				,coalesce(prvc.SEOFriendlyUrl, p.SEOFriendlyUrl) as 'SEO Friendly URL'
				,coalesce(prvc.SEOH1, p.SEOH1) as [SEO H1]
				,p.IsShared
			from dbo.PRProductVariantCache prvc
			left join dbo.PRProduct p on (prvc.Id = p.Id)
			left join dbo.PRProductType pt on (p.ProductTypeID = pt.Id)
			where prvc.SiteId = @siteId
			order by p.Title
		END
END
GO
PRINT 'Modify stored procedure Facet_GetFacetsByNavigation'
GO
IF(OBJECT_ID('Facet_GetFacetsByNavigation') IS NOT NULL)
	DROP PROCEDURE Facet_GetFacetsByNavigation
GO
CREATE PROCEDURE [dbo].[Facet_GetFacetsByNavigation]
    (
      @Id UNIQUEIDENTIFIER ,
      @ExistingFacetValueIds XML = NULL ,
      @ExistingFacets XML = NULL ,
      @UseLimit BIT = 1,
	  @ApplicationId uniqueidentifier=NULL
    )
AS 
    BEGIN

        DECLARE @filterId UNIQUEIDENTIFIER
        SELECT  @filterId = QueryId
        FROM    NVNavNodeNavFilterMap
        WHERE   NavNodeId = @Id

        DECLARE @totProd INT
        SELECT  @totProd = COUNT(C.Id)
        FROM    ATFacetRangeProduct_Cache C
		INNER JOIN VWProduct P ON C.ProductId=P.Id AND P.SiteId=@ApplicationId  
		Where C.SiteId=@ApplicationId
		Group By P.Id

        SET @UseLimit = ISNULL(@UseLimit, 0)
        IF ( @ExistingFacetValueIds IS NULL
             AND @ExistingFacets IS NULL
           ) 
            BEGIN

                SELECT 
			C.FacetID,
			C.FacetValueID,
			MIN(C.DisplayText) DisplayText,
			Count(*) ProductCount,
			MIN(NF.Sequence) FacetSequence,
			Case FR.Sequence 
				WHEN NULL  THEN @totProd - COUNT(*)
				ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
			END AS SortOrder,
			MIN(AF.AllowMultiple) AllowMultiple
		FROM 
			ATFacetRangeProduct_Cache C 
			INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
			INNER JOIN vwFacet AF ON AF.Id = NF.FacetId	AND AF.SiteId = C.SiteId
			INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
			INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
			LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
			LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
						AND C.SiteId=@ApplicationId
                GROUP BY C.FacetValueID ,
                        C.FacetID
                UNION ALL
                SELECT  C.FacetID ,
                        NULL FacetValueID ,
                        MIN(AF.Title) DisplayText ,
                        COUNT(*) ,
                        MIN(Sequence) FacetSequence ,
                        0 SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN vwFacet AF ON AF.Id = NF.FacetId AND AF.SiteId = C.SiteId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
						INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
						AND C.SiteId=@ApplicationId
                GROUP BY C.FacetID 
            END
        ELSE 
            IF ( @ExistingFacetValueIds IS NOT NULL ) 
                BEGIN
 

                    CREATE TABLE #FilterProductIds
                        (
                          Id UNIQUEIDENTIFIER
                            CONSTRAINT [PK_#cteValueToIdValue]
                            PRIMARY KEY CLUSTERED ( [Id] ASC )
                            WITH ( PAD_INDEX = OFF,
                                   STATISTICS_NORECOMPUTE = OFF,
                                   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                                   ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
                        )
                    ON  [PRIMARY]

                    INSERT  INTO #FilterProductIds
                            SELECT  C.ProductID
                            FROM    ATFacetRangeProduct_Cache C
							INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    INNER JOIN @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON tab.col.value('text()[1]',
                                                              'uniqueidentifier') = C.FacetValueID
                            Where C.SiteId=@ApplicationId
							GROUP BY C.ProductID
                            HAVING  COUNT(C.FacetValueID) >= ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab1 ( col1 )
                                                             )

				SELECT 
					C.FacetID,
					C.FacetValueID,
					MIN(C.DisplayText) DisplayText,
					Count(*) ProductCount,
					MIN(NF.Sequence) FacetSequence,
					Case FR.Sequence 
								WHEN NULL  THEN @totProd - COUNT(*)
								ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
							END AS SortOrder,
					MIN(AF.AllowMultiple) AllowMultiple
				FROM 
					ATFacetRangeProduct_Cache C 
					INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
					INNER JOIN vwFacet AF ON AF.Id = NF.FacetId	AND AF.SiteId = C.SiteId
					INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
					INNER JOIN #FilterProductIds P ON P.ID=C.ProductID
					LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
					LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
							AND C.SiteId=@ApplicationId
                    GROUP BY C.FacetValueID ,
                            C.FacetID
                    UNION ALL
                    SELECT  C.FacetID ,
                            NULL FacetValueID ,
                            MIN(AF.Title) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            0 SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProductTop_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN vwFacet AF ON AF.Id = NF.FacetId AND AF.SiteId = C.SiteId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
							AND C.SiteId=@ApplicationId
                    GROUP BY C.FacetID 


                    DROP TABLE #FilterProductIds

                END
            ELSE 
                BEGIN
                   ------------------NEW---------------------------------
                    DECLARE @FacetValues TABLE
                        (
                          FacetValueID UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER
                        )
                    INSERT  INTO @FacetValues
                            SELECT  tab.col.value('(value/guid/text())[1]',
                                                  'uniqueidentifier') ,
                                    tab.col.value('(key/guid/text())[1]',
                                                  'uniqueidentifier')
                            FROM    @ExistingFacets.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item') tab ( col ) ;
                 

                 
                 

                    DECLARE @FilteredProducts TABLE
                        (
                          ProductId UNIQUEIDENTIFIER
                        )           
                    
                    DECLARE @RequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
       
                    INSERT  INTO @RequiredProduct
                            SELECT DISTINCT
                                    C.ProductId ,
                                    Row_Number() OVER ( PARTITION BY ProductId ORDER BY ProductID ) AS RowNumber
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.vwFacet F ON F.Id = FV.FacetId AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                            WHERE   F.AllowMultiple = 0 

                    DELETE  FROM @RequiredProduct
                    WHERE   RowNumber NOT IN ( SELECT   MAX(RowNumber)
                                               FROM     @RequiredProduct )


                    DECLARE @MultiRequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
                     INSERT  INTO @MultiRequiredProduct
                            SELECT  DISTINCT
                                    ProductID ,
                                    FacetID ,
                                    Row_Number() OVER ( PARTITION BY ProductID ORDER BY ProductID ) AS RowNumber
                                    
                                    FROM (
                                    SELECT DISTINCt C.ProductID,
                                    C.FacetID
                                    
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.vwFacet F ON F.Id = FV.FacetId AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                            WHERE   F.AllowMultiple = 1)TV
                           



                    IF NOT EXISTS ( SELECT  *
                                    FROM    @RequiredProduct ) 
                        BEGIN
                            INSERT  INTO @FilteredProducts
                                    SELECT  ProductId
                                    FROM    @MultiRequiredProduct
                                    WHERE   RowNumber IN (
                                            SELECT  MAX(RowNumber)
                                            FROM    @MultiRequiredProduct )
                       
                        END
                    ELSE 
                        IF NOT EXISTS ( SELECT  *
                                        FROM    @MultiRequiredProduct ) 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  ProductId
                                        FROM    @RequiredProduct
                            END
                        ELSE 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  RP.ProductId
                                        FROM    @RequiredProduct RP
                                                INNER JOIN ( SELECT
                                                              ProductId
                                                             FROM
                                                              @MultiRequiredProduct
                                                             WHERE
                                                              RowNumber IN (
                                                              SELECT
                                                              MAX(RowNumber)
                                                              FROM
                                                              @MultiRequiredProduct )
                                                           ) TV ON TV.ProductId = RP.ProductId


                            END

                    DECLARE @FacetResults TABLE
                        (
                          FacetId UNIQUEIDENTIFIER ,
                          FacetValueId UNIQUEIDENTIFIER ,
                          DisplayText NVARCHAR(250) ,
                          FacetName NVARCHAR(250) ,
                          ProductCount INT ,
                          FacetSequence INT ,
                          SortOrder INT ,
                          AllowMultiple INT ,
                          ENABLED INT
                        )
                    INSERT  INTO @FacetResults
                            ( FacetId ,
                              FacetValueId ,
                              DisplayText ,
                              FacetName ,
                              ProductCount ,
                              FacetSequence ,
                              SortOrder ,
                              AllowMultiple ,
                              ENABLED
                            )
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.vwFacet F ON F.Id = C.FacetID AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @FilteredProducts RP ON RP.ProductId = C.ProductID
									LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 0
							        AND NFE.QueryId IS NULL
                                    AND ( RP.ProductId IS NOT NULL
                                          OR NOT EXISTS ( SELECT
                                                              ProductId
                                                          FROM
                                                              @FilteredProducts )
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
--Close now we need to get the other multi values that are not currently in the required products but would be filtered by any required products. 
--This query should be the multi facets that have no products currently showing in the grid. 
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.vwFacet F ON F.Id = C.FacetID AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( 
																		--MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
                                                              ( SELECT
                                                              COUNT(DISTINCT FacetId)
                                                              FROM
                                                              @MultiRequiredProduct
                                                              ) = 1 )
																	---End of expensive
                                                              )
							        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 1
							        AND NFE.QueryId IS NULL  
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.vwFacet F ON F.Id = C.FacetID AND F.SiteId = C.SiteId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
																		--( SELECT
																		--COUNT(DISTINCT FacetId)
																		--FROM
																		--@MultiRequiredProduct
																		--) = 1
                                                              )
																	---End of expensive
                                                              )
									LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 1
							        AND NFE.QueryId IS NULL
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                             
                                        )
                                        
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
        
                        
        
        
        
                    SELECT  FacetId ,
                            FacetValueId ,
                            DisplayText ,
                            ProductCount ,
                            FacetSequence ,
                            SortOrder ,
                            AllowMultiple ,
                            ENABLED
                    FROM    @FacetResults
                    UNION
                    SELECT  FacetId ,
                            NULL ,
                            FacetName AS DisplayText ,
                            SUM(ProductCount) ,
                            MAX(FacetSequence) ,
                            MAX(SortOrder) ,
                            MAX(AllowMultiple) ,
                            MAX(ENABLED)
                    FROM    @FacetResults
                    GROUP BY FacetId ,
                            FacetName
                    ORDER BY FacetValueId DESC
                   
                   
                   
                   
                   ----------------END OF NEW------------------------------
                        
                        
                        
                        
                        

                END
    END
GO
PRINT 'Modify stored procedure Facet_ReloadProductFacetCache'
GO
IF(OBJECT_ID('Facet_ReloadProductFacetCache') IS NOT NULL)
	DROP PROCEDURE Facet_ReloadProductFacetCache
GO
CREATE PROCEDURE [dbo].[Facet_ReloadProductFacetCache]
(
	@ApplicationId	uniqueidentifier = null  
)
AS  
BEGIN 
	IF @ApplicationId = dbo.GetEmptyGUID() SET @ApplicationId = NULL

	CREATE TABLE #tempCache
	(
		FacetId			uniqueidentifier,  
		AttributeId		uniqueidentifier,  
		FacetValueId	uniqueidentifier,  
		DisplayText		nvarchar(555),  
		ProductId		uniqueidentifier,  
		SortOrder		int Default(0),
		SiteId			uniqueidentifier
	)  
  
	--Enumerated Facets  
	INSERT INTO #tempCache 
	(
		FacetId,
		AttributeId,
		FacetValueId,
		DisplayText,
		ProductId,
		SortOrder,
		SiteId
	)  
	SELECT FacetId, 
		PSFAV.AttributeId, 
		AttributeEnumId, 
		PSFAV.Value, 
		ProductId,
		AE.Sequence,
		PSFAV.SiteId
	FROM vwProductSKUFacetAttributeValue_Enumerated PSFAV   
		LEFT JOIN ATAttributeEnum AE ON AE.Id = PSFAV.AttributeEnumId  
	WHERE (@ApplicationId IS NULL OR PSFAV.SiteId = @ApplicationId)

	--Range Facets
	SELECT AttributeId, 
		FacetId,
		ProductId, 
		CONVERT(Decimal(14, 4), value) AS Value,
		SiteId
	INTO #PSFAV
	FROM vwProductSKUFacetAttributeValue_RANGE 
	WHERE ISNUMERIC(Value) = 1
		AND (@ApplicationId IS NULL OR SiteId = @ApplicationId)

	--Int Range  
	INSERT INTO #tempCache 
	(
		FacetId,
		AttributeId,
		FacetValueId,
		DisplayText,
		ProductId,
		SortOrder,
		SiteId
	)  
	SELECT F.Id, 
		F.AttributeId, 
		FR.Id, 
		FR.DisplayText, 
		PSFAV.ProductId,
		FR.Sequence,
		PSFAV.SiteId  
	FROM ATFacet F   
		INNER JOIN vwFacetRange FR ON F.Id = FR.FacetId
		INNER JOIN ATFacetRangeInt FRX ON FR.Id = FRX.FacetRangeId   
		INNER JOIN #PSFAV PSFAV ON F.AttributeId = PSFAV.AttributeId AND  
			PSFAV.FacetId = F.Id AND
			PSFAV.Value BETWEEN ISNULL(FRX.[From], PSFAV.Value) AND 
			ISNULL(FRX.[To], PSFAV.Value) AND   
			ISNUMERIC(PSFAV.Value) = 1  
	WHERE FR.SiteId = PSFAV.SiteId 
		AND (@ApplicationId IS NULL OR PSFAV.SiteId = @ApplicationId)

	--Decimal Range  
	INSERT INTO #tempCache 
	(
		FacetId,
		AttributeId,
		FacetValueId,
		DisplayText,
		ProductId,
		SortOrder,
		SiteId
	)  
	SELECT F.Id, 
		F.AttributeId, 
		FR.Id, 
		FR.DisplayText, 
		PSFAV.ProductId,
		FR.Sequence,
		PSFAV.SiteId
	FROM ATFacet F   
		INNER JOIN vwFacetRange FR ON F.Id = FR.FacetId 
		INNER JOIN ATFacetRangeDecimal FRX ON FR.Id = FRX.FacetRangeId   
		INNER JOIN #PSFAV PSFAV ON F.AttributeId = PSFAV.AttributeId AND 
			PSFAV.FacetId = F.Id AND
			PSFAV.Value BETWEEN ISNULL(FRX.[From], PSFAV.Value) AND
			ISNULL(FRX.[To], PSFAV.Value) AND 
			ISNUMERIC(PSFAV.Value) = 1  
	WHERE FR.SiteId = PSFAV.SiteId 
		AND (@ApplicationId IS NULL OR PSFAV.SiteId = @ApplicationId)

	DROP TABLE #PSFAV

	--String Range  
	SELECT F.AttributeId, 
		ProductId,
		V1.FacetId,
		Value,
		V1.SiteId
	INTO #PSFAVString
	FROM vwProductSKUFacetAttributeValue_RANGE V1 
		INNER JOIN ATFacet F ON V1.AttributeId = F.AttributeId
		INNER JOIN ATFacetRange FR ON F.Id = FR.FacetId AND FR.SiteId = V1.SiteId     
		INNER JOIN ATFacetRangeString FRX ON FR.Id = FRX.FacetRangeId   
	WHERE (@ApplicationId IS NULL OR V1.SiteId = @ApplicationId)

	INSERT INTO #tempCache 
	(
		FacetId,
		AttributeId,
		FacetValueId,
		DisplayText,
		ProductId,
		SortOrder,
		SiteId
	)  
	SELECT F.Id, 
		F.AttributeId, 
		FR.Id, 
		FR.DisplayText, 
		PSFAV.ProductId,
		FR.Sequence,
		PSFAV.SiteId
	FROM ATFacet F   
		INNER JOIN vwFacetRange FR ON F.Id = FR.FacetId
		INNER JOIN ATFacetRangeString FRX ON FR.Id = FRX.FacetRangeId   
		INNER JOIN #PSFAVString PSFAV ON F.AttributeId = PSFAV.AttributeId AND 
			PSFAV.FacetId = F.Id AND
			PSFAV.Value BETWEEN ISNULL(FRX.[From], PSFAV.Value) AND 
			ISNULL(FRX.[To], PSFAV.Value) 
	WHERE FR.SiteId = PSFAV.SiteId 
		AND (@ApplicationId IS NULL OR PSFAV.SiteId = @ApplicationId)

	DROP TABLE #PSFAVString

	SELECT AttributeId, 
		ProductId, 
		FacetId,
		CONVERT(DateTime, value) as Value,
		SiteId
	INTO #PSFAVDate
	FROM vwProductSKUFacetAttributeValue_RANGE 
	WHERE ISDATE(Value) = 1
		AND (@ApplicationId IS NULL OR SiteId = @ApplicationId)

	--Date Range  
	INSERT INTO #tempCache 
	(
		FacetId,
		AttributeId,
		FacetValueId,
		DisplayText,
		ProductId,
		SortOrder,
		SiteId
	)  
	SELECT F.Id, 
		F.AttributeId, 
		FR.Id, 
		FR.DisplayText, 
		PSFAV.ProductId,
		FR.Sequence,
		PSFAV.SiteId
	FROM ATFacet F   
		INNER JOIN vwFacetRange FR ON F.Id = FR.FacetId 
		INNER JOIN ATFacetRangeDate FRX ON FR.Id = FRX.FacetRangeId  
		INNER JOIN #PSFAVDate PSFAV ON F.AttributeId = PSFAV.AttributeId AND
			PSFAV.FacetId = F.Id AND
			PSFAV.Value BETWEEN ISNULL(FRX.[From], PSFAV.Value) AND 
			ISNULL(FRX.[To], PSFAV.Value) AND 
			ISDATE(PSFAV.Value) = 1  
	WHERE FR.SiteId = PSFAV.SiteId 
		AND (@ApplicationId IS NULL OR PSFAV.SiteId = @ApplicationId)

	DROP TABLE #PSFAVDate

	IF @ApplicationId IS NOT NULL
	BEGIN
		INSERT INTO #tempCache 
		(
			FacetId,
			AttributeId,
			FacetValueId,
			DisplayText,
			ProductId,
			SortOrder,
			SiteId
		)  
		SELECT FacetId,
			AttributeId,
			FacetValueId,
			DisplayText,
			ProductId,
			SortOrder,
			SiteId
		FROM ATFacetRangeProduct_Cache
		WHERE SiteId != @ApplicationId
	END

	TRUNCATE TABLE ATFacetRangeProduct_Cache  
 
	INSERT INTO dbo.ATFacetRangeProduct_Cache   
	(
		Id,
		FacetId,
		AttributeId,
		FacetValueId,
		DisplayText,
		ProductId,
		SortOrder,
		SiteId
	)  
	SELECT NEWID(),
		T.FacetId,
		T.AttributeId,
		T.FacetValueId,
		T.DisplayText,
		T.ProductId,
		T.SortOrder,
		T.SiteId   
	FROM #tempCache T  
  
	TRUNCATE TABLE ATFacetRangeProductTop_Cache  
  
	INSERT INTO ATFacetRangeProductTop_Cache
	(
		FacetId,
		ProductId,
		SiteId
	) 
	SELECT   
		[FacetId],      
		[ProductId],
		SiteId
	FROM ATFacetRangeProduct_Cache
	GROUP BY [FacetId], [ProductId], [SiteId]
END  
GO
PRINT 'Creating Function User_GetUsersInGroup'
GO
IF (OBJECT_ID('User_GetUsersInGroup') IS NOT NULL)
	DROP FUNCTION User_GetUsersInGroup
GO
CREATE FUNCTION [dbo].[User_GetUsersInGroup]  
(  
	@ProductId		uniqueidentifier = NULL,   
	@ApplicationId	uniqueidentifier,  
	@Id				uniqueidentifier,   -- GroupId  
	@MemberType		smallint,  
	@MemberId		uniqueidentifier = NULL, -- Either userId or groupid.  
	@IsSystemUser	bit = 0  
)  
RETURNS @ResultTable TABLE       
(      
	 Id							uniqueidentifier,      
	 UserName					nvarchar(256),        
	 Email						nvarchar(512),      
	 PasswordQuestion			nvarchar(512),        
	 IsApproved					bit,      
	 CreatedDate				datetime,      
	 LastLoginDate				datetime,      
	 LastActivityDate			datetime,      
	 LastPasswordChangedDate	datetime,      
	 IsLockedOut				bit,      
	 LastLockoutDate			datetime,    
	 FirstName					nvarchar(256),    
	 LastName					nvarchar(256),    
	 MiddleName					nvarchar(256) ,    
	 ExpiryDate					datetime ,    
	 EmailNotification			bit ,    
	 TimeZone					nvarchar(100),    
	 ReportRangeSelection		varchar(50),    
	 ReportStartDate			datetime ,    
	 ReportEndDate				datetime,     
	 BirthDate					datetime,    
	 CompanyName				nvarchar(1024),    
	 Gender						nvarchar(50),    
	 HomePhone					varchar(50),    
	 MobilePhone				varchar(50),    
	 OtherPhone					varchar(50),    
	 ImageId					uniqueidentifier,
	 LeadScore					int,  
	 Status						int
)   
AS  
BEGIN  
	DECLARE @tbUser TYUser  
	
	INSERT INTO @tbUser  
	SELECT DISTINCT   
		U.Id,  
		U.UserName,  
		M.Email,   
		M.PasswordQuestion,    
		M.IsApproved,  
		U.CreatedDate,  
		M.LastLoginDate,   
		U.LastActivityDate,  
		M.LastPasswordChangedDate,   
		M.IsLockedOut,  
		M.LastLockoutDate,  
		U.FirstName,  
		U.LastName,  
		U.MiddleName,  
		U.ExpiryDate,  
		U.EmailNotification,  
		U.TimeZone,  
		U.ReportRangeSelection,  
		U.ReportStartDate,  
		U.ReportEndDate,  
		U.BirthDate,  
		U.CompanyName,  
		U.Gender,  
		U.HomePhone,  
		U.MobilePhone,  
		U.OtherPhone,  
		U.ImageId,
		U.LeadScore,
		U.Status
	FROM dbo.USUser U  
		INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
		INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
			(@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser)   
		INNER JOIN dbo.USMemberGroup UG ON UG.MemberId = U.Id
		INNER JOIN dbo.USGroup G ON G.Id = UG.GroupId AND
			(@ApplicationId IS NULL OR G.ApplicationId IN (Select SiteId From dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser, 0)))   
	WHERE G.Id = @Id AND  
		U.Status != 3 AND  
		(@ProductId IS NULL OR S.ProductId = @ProductId) AND  
		(@MemberId IS NULL OR UG.MemberId = @MemberId)  
   
	INSERT INTO @ResultTable  
	SELECT * FROM @tbUser  
  
	RETURN  
END
GO
PRINT 'Modify stored procedure Membership_GetUsersInGroup'
GO
IF(OBJECT_ID('Membership_GetUsersInGroup') IS NOT NULL)
	DROP PROCEDURE Membership_GetUsersInGroup
GO
CREATE PROCEDURE [dbo].[Membership_GetUsersInGroup]
(
	@ProductId				uniqueidentifier = NULL,
	@ApplicationId			uniqueidentifier = NULL,
	@Id				        uniqueidentifier,
	@MemberType				smallint,
	@IsSystemUser			bit = 0,
	@MemberId				uniqueidentifier = NULL, --either userid or groupid.
	@IgnoreProduct			bit = NULL,
	@WithProfile			bit = 0,
	@PageNumber				int,
	@PageSize				int
)
AS
BEGIN

	IF(@Id IS NULL)
	BEGIN
		RAISERROR ('ISNULL||GroupId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	IF @PageNumber IS NULL SET @PageNumber = 0
	IF @IgnoreProduct = 1 SET @ProductId = NULL
		
	IF (@MemberType = 1)
	BEGIN
		DECLARE @tbUser TYUser
		INSERT INTO @tbUser
		SELECT * FROM [dbo].[User_GetUsersInGroup] 
			(
				@ProductId, 
				@ApplicationId,
				@Id,
				@MemberType,
				@MemberId,
				@IsSystemUser
			)

		EXEC [dbo].[Membership_BuildUser] 
			@tbUser = @tbUser, 
			@SortBy = NULL, 
			@SortOrder = NULL,
			@PageNumber = @PageNumber,
			@PageSize = @PageSize
	END
	ELSE IF (@MemberType = 2) -- Group
	BEGIN
		DECLARE @PageLowerBound int
		DECLARE @PageUpperBound int
		SET @PageLowerBound = @PageSize * @PageNumber
		IF (@PageNumber > 0)
			SET @PageUpperBound = @PageLowerBound - @PageSize + 1
		ELSE
			SET @PageUpperBound = 0

		;WITH ResultEntries AS
		(
			SELECT ROW_NUMBER() OVER (ORDER BY G.Title) AS RowNumber,
				G.ProductId,
				UG.ApplicationId,
				Id,
				Title,
				Description,
				CreatedBy,
				CreatedDate,
				ModifiedBy,
				ModifiedDate,
				Status,
				ExpiryDate,
				G.GroupType
			FROM dbo.USMemberGroup UG
				INNER JOIN dbo.USGroup G ON G.Id = UG.MemberId	
			WHERE Status != dbo.GetDeleteStatus() AND
				UG.GroupId = @Id AND 
				(@MemberId IS NULL OR UG.MemberId = @MemberId)	AND
				(G.ApplicationId = G.ProductId OR @ApplicationId IS NULL OR UG.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser,0))) AND
				(G.ExpiryDate IS NULL OR G.ExpiryDate >= GETUTCDATE()) AND
				(@ProductId IS NULL OR G.ProductId = @ProductId)
		)

		SELECT DISTINCT ProductId,
			ApplicationId,
			Id,
			Title,
			Description,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			Status,
			ExpiryDate,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			GroupType
		FROM ResultEntries g
			LEFT JOIN VW_UserFullName FN on FN.UserId = G.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = G.ModifiedBy
		WHERE (@PageNumber = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)
	END 
END
GO
PRINT 'Modify stored procedure DistributionRunDto_Save'
GO
IF(OBJECT_ID('DistributionRunDto_Save') IS NOT NULL)
	DROP PROCEDURE DistributionRunDto_Save
GO
CREATE PROCEDURE [dbo].[DistributionRunDto_Save]
(
	@Id					uniqueidentifier = NULL OUTPUT,
	@DistributionId		uniqueidentifier = NULL,
	@Distribution		xml = NULL,
	@Status				int = NULL,
	@Action				int = NULL,
	@Targets			xml = NULL,
	@SiteId				uniqueidentifier,
	@ModifiedBy			uniqueidentifier,
	@Update				bit = NULL,
	@PreserveVariantContent bit = NULL
)
AS
BEGIN
	DECLARE @UtcNow datetime, @DistributionType int
	SET @UtcNow = GETUTCDATE()

	IF @DistributionId IS NULL AND @Distribution IS NOT NULL
	BEGIN		
		SET @DistributionId = (SELECT TOP 1 A.n.value('@Id', 'uniqueidentifier') FROM 
			@Distribution.nodes('/Distribution') AS A(n))

		IF @DistributionId = dbo.GetEmptyGUID()
			SET @DistributionId = (SELECT TOP 1 D.Id FROM GLDistribution D
				JOIN @Distribution.nodes('/Distribution') AS A(n) ON D.ObjectId = A.n.value('@ObjectId', 'uniqueidentifier'))
	END

	IF @DistributionId IS NULL
	BEGIN
		SET @DistributionId = NEWID()
		
		INSERT INTO GLDistribution
		(
			Id,
			Title,
			ObjectId,
			ObjectTypeId,
			SiteId,
			CreatedBy,
			CreatedDate,
			DistributionType
		)
		SELECT @DistributionId,
			A.n.value('@Title', 'nvarchar(max)'),
			A.n.value('@ObjectId', 'uniqueidentifier'),
			A.n.value('@ObjectTypeId', 'int'),
			@SiteId,
			@ModifiedBy,
			@UtcNow,
			A.n.value('@DistributionType', 'int')
		FROM @Distribution.nodes('/Distribution') AS A(n) 
	END

	IF @Id IS NULL
		SELECT TOP 1 @Id = Id FROM GLDistributionRun WHERE DistributionId = @DistributionId AND Status <= 2

	DECLARE @tbOptInSites TABLE (SiteId uniqueidentifier)
	SELECT TOP 1 @DistributionType = DistributionType FROM GLDistribution WHERE Id = @DistributionId
	IF @DistributionType = 2
	BEGIN
		INSERT INTO @tbOptInSites
		SELECT SiteId FROM GLDistributionOptIn WHERE DistributionId = @DistributionId

		IF @@ROWCOUNT = 0 RETURN
	END

	IF @Id IS NULL OR @Id = dbo.GetEmptyGUID()
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO GLDistributionRun
		(
			Id,
			DistributionId,
			Status,
			Action,
			CreatedBy,
			CreatedDate,
			SiteId,
			PreserveVariantContent
		)
		VALUES
		(
			@Id,
			@DistributionId,
			@Status,
			@Action,
			@ModifiedBy,
			@UtcNow,
			@SiteId,
			@PreserveVariantContent
		)
	END
	ELSE IF @Update = 1
	BEGIN
		UPDATE GLDistributionRun
		SET Status = ISNULL(@Status, Status),
			Action = ISNULL(@Action, Action),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @UtcNow
		WHERE Id = @Id
	END

	IF EXISTS (SELECT 1 FROM GLDistribution WHERE Id = @DistributionId AND ObjectTypeId = 1)
	BEGIN
		DELETE FROM GLDistributionRunTarget WHERE DistributionRunId = @Id
		INSERT INTO GLDistributionRunTarget
		(
			Id,
			DistributionRunId,
			TargetId,
			TargetTypeId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			@Id,
			ObjectId,
			2,
			@ModifiedBy,
			@UtcNow
		FROM GLDistribution
		WHERE Id = @DistributionId 
	END
	ELSE IF EXISTS (SELECT 1 FROM @tbOptInSites)
	BEGIN
		DELETE FROM GLDistributionRunTarget WHERE DistributionRunId = @Id
		INSERT INTO GLDistributionRunTarget
		(
			Id,
			DistributionRunId,
			TargetId,
			TargetTypeId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			@Id,
			SiteId,
			2,
			@ModifiedBy,
			@UtcNow
		FROM @tbOptInSites
	END
	ELSE IF @Targets IS NOT NULL
	BEGIN
		DELETE FROM GLDistributionRunTarget WHERE DistributionRunId = @Id
		INSERT INTO GLDistributionRunTarget
		(
			Id,
			DistributionRunId,
			TargetId,
			TargetTypeId,
			CreatedBy,
			CreatedDate
		)
		SELECT NEWID(),
			@Id,
			A.n.value('@TargetId','uniqueidentifier'),
			A.n.value('@TargetTypeId','int'),
			@ModifiedBy,
			@UtcNow
		FROM @Targets.nodes('/Targets/Targets') AS A(n)
	END
	ELSE IF NOT EXISTS (SELECT 1 FROM GLDistributionRunTarget WHERE DistributionRunId = @Id)
	BEGIN 
		IF EXISTS (SELECT 1 FROM GLDistributionTarget WHERE DistributionId = @DistributionId)
		BEGIN
			INSERT INTO GLDistributionRunTarget
			(
				Id,
				DistributionRunId,
				TargetId,
				TargetTypeId,
				CreatedBy,
				CreatedDate
			)
			SELECT NEWID(),
				@Id,
				TargetId,
				TargetTypeId,
				@ModifiedBy,
				@UtcNow
			FROM GLDistributionTarget 
			WHERE DistributionId = @DistributionId
		END
		ELSE
		BEGIN
			INSERT INTO GLDistributionRunTarget
			(
				Id,
				DistributionRunId,
				TargetId,
				TargetTypeId,
				CreatedBy,
				CreatedDate
			)
			SELECT NEWID(),
				@Id,
				@SiteId,
				3,
				@ModifiedBy,
				@UtcNow
		END
	END
	 
END
GO
PRINT 'Modify view vwProductSKUFacetAttributeValue_Enumerated'
GO
IF(OBJECT_ID('vwProductSKUFacetAttributeValue_Enumerated') IS NOT NULL)
	DROP VIEW vwProductSKUFacetAttributeValue_Enumerated
GO
CREATE VIEW [dbo].[vwProductSKUFacetAttributeValue_Enumerated]     
AS  
  
SELECT F.Id AS FacetId, 
	F.IsRange, 
	PAV.ObjectId AS ProductId, 
	AE.AttributeId AttributeId, 
	AE.Title Value, 
	PAV.AttributeEnumId AttributeEnumId, 
	PAV.SiteId  
FROM [dbo].ATFacet F   
	INNER JOIN [dbo].ATAttribute A ON A.Id = F.AttributeID 
	INNER JOIN [dbo].vwAttributeEnum AE ON A.Id = AE.AttributeId  
	INNER JOIN [dbo].vwProductAttributeValue PAV ON PAV.AttributeEnumId = AE.Id  
	INNER JOIN [dbo].PRProduct P on P.Id = PAV.ObjectId AND P.ISActive = 1  
	INNER JOIN [dbo].PRProductSKU PS on PS.Productid = P.Id AND PS.IsActive = 1 AND PS.ISOnline = 1  
WHERE A.Status = 1 AND AE.SiteId = PAV.SiteId

UNION  

SELECT F.Id AS FacetId, 
	F.IsRange IsRange, 
	PS.ProductId ProductId, 
	AE.AttributeId AttributeId, 
	AE.Title Value, 
	PSAV.AttributeEnumId AttributeEnumId, 
	PSAV.SiteId  
FROM [dbo].[ATFacet] F   
	INNER JOIN [dbo].ATAttribute A ON A.Id = F.AttributeID 
	INNER JOIN [dbo].vwAttributeEnum AE ON A.Id = AE.AttributeId  
	INNER JOIN [dbo].vwSkuAttributeValue PSAV ON PSAV.AttributeEnumId = AE.Id 
	INNER JOIN [dbo].PRProductSKU PS ON PS.Id = PSAV.ObjectId AND PS.IsActive = 1 AND PS.IsOnline = 1  
	INNER JOIN [dbo].PRPRoduct P on P.Id = PS.ProductId AND P.IsActive = 1  
WHERE A.Status = 1 AND AE.SiteId = PSAV.SiteId
GO

PRINT 'Modify view vwProductSKUFacetAttributeValue_Range'
GO
IF(OBJECT_ID('vwProductSKUFacetAttributeValue_Range') IS NOT NULL)
	DROP VIEW vwProductSKUFacetAttributeValue_Range
GO
CREATE VIEW [dbo].[vwProductSKUFacetAttributeValue_Range]  
AS  
  
--Non Enumerated Products   
SELECT distinct F.id AS FacetId, PAV.ObjectId AS ProductId, PAV.AttributeId, PAV.Value, NULL AS AttributeEnumId, PAV.SiteId  
FROM  
 vwProductAttributeValue PAV   
 INNER JOIN ATAttribute A ON PAV.AttributeId = A.Id AND A.IsEnum = 0 
 INNER JOIN ATFacet F ON A.Id = F.AttributeID  
 INNER JOIN PRProduct P on P.Id = PAV.ObjectId and P.ISActive=1  
 INNER JOIN PRProductSKU PS on PS.Productid = P.Id and PS.IsActive=1 and PS.ISOnline=1  
WHERE   
 A.Status = dbo.GetActiveStatus()   AND IsRange=1  
   
  
UNION ALL  
  
--Non Enumerated SKUs  
SELECT F.id AS FacetId,  PS.ProductId AS ProductId, PSAV.AttributeId, PSAV.Value, NULL AS AttributeEnumId, PSAV.SiteId  
FROM  
 PRProductSKU PS   
 INNER JOIN vwSkuAttributeValue PSAV ON PS.Id = PSAV.ObjectId   
 INNER JOIN ATAttribute A ON PSAV.AttributeId = A.Id AND A.IsEnum = 0    
 INNER JOIN ATFacet F ON A.Id = F.AttributeID  
 INNER JOIN PRProduct P on P.Id = PS.ProductID and PS.IsActive=1 and PS.IsOnline=1 and P.IsActive=1  
WHERE   
 A.Status=dbo.GetActiveStatus()  AND IsRange=1  
  
UNION ALL   
  
SELECT FacetId, ProductId, AttributeId, Value, NULL AS AttributeEnumId, SiteId  
FROM [vwProductSKUFacetAttributeValue_Enumerated] WHERE IsRange=1  
GO

-- Cleanup Facet data caused by earlier bug

DECLARE @tbFacetRangeInt TABLE(Id uniqueidentifier, FacetRangeId uniqueidentifier, [From] nvarchar(100), [To] nvarchar(100))

INSERT INTO @tbFacetRangeInt
SELECT FRS.Id, FRS.FacetRangeID, FRS.[From], FRS.[To] FROM ATAttribute A
	JOIN ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId
	JOIN ATFacet F ON F.AttributeID = A.Id
	JOIN ATFacetRange FR ON FR.FacetID = F.Id
	JOIN ATFacetRangeString FRS ON FRS.FacetRangeID = FR.Id
	LEFT JOIN ATFacetRangeInt FRD ON FRD.FacetRangeID = FR.Id
WHERE ADT.Title = 'Integer' AND FRD.Id IS NULL

INSERT INTO ATFacetRangeInt (Id, FacetRangeID, [FROM], [To])
SELECT Id, FacetRangeID, CAST([From] as int) , CAST([To] as int)FROM @tbFacetRangeInt
GO

DELETE FRS FROM ATAttribute A
	JOIN ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId
	JOIN ATFacet F ON F.AttributeID = A.Id
	JOIN ATFacetRange FR ON FR.FacetID = F.Id
	JOIN ATFacetRangeString FRS ON FRS.FacetRangeID = FR.Id
WHERE ADT.Title = 'Integer'
GO

DECLARE @tbFacetRangeDecimal TABLE(Id uniqueidentifier, FacetRangeId uniqueidentifier, [From] nvarchar(100), [To] nvarchar(100))

INSERT INTO @tbFacetRangeDecimal
SELECT FRS.Id, FRS.FacetRangeID, FRS.[From], FRS.[To] FROM ATAttribute A
	JOIN ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId
	JOIN ATFacet F ON F.AttributeID = A.Id
	JOIN ATFacetRange FR ON FR.FacetID = F.Id
	JOIN ATFacetRangeString FRS ON FRS.FacetRangeID = FR.Id
	LEFT JOIN ATFacetRangeDecimal FRD ON FRD.FacetRangeID = FR.Id
WHERE ADT.Title = 'Decimal' AND FRD.Id IS NULL

INSERT INTO ATFacetRangeDecimal (Id, FacetRangeID, [FROM], [To])
SELECT Id, FacetRangeID, CAST([From] as int) , CAST([To] as int)FROM @tbFacetRangeDecimal
GO

DELETE FRS FROM ATAttribute A
	JOIN ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId
	JOIN ATFacet F ON F.AttributeID = A.Id
	JOIN ATFacetRange FR ON FR.FacetID = F.Id
	JOIN ATFacetRangeString FRS ON FRS.FacetRangeID = FR.Id
WHERE ADT.Title = 'Decimal'
GO
PRINT 'Modify stored procedure Product_GetOnlineProductByFilter'
GO
IF(OBJECT_ID('Product_GetOnlineProductByFilter') IS NOT NULL)
	DROP PROCEDURE Product_GetOnlineProductByFilter
GO
CREATE PROCEDURE [dbo].[Product_GetOnlineProductByFilter]
(
	@FilterId uniqueidentifier,
    @SortCol VARCHAR(25)=null,
    @PageSize INT=null, 
	@PageNumber INT=1,
	@ApplicationId uniqueidentifier = null,
	@FacetValueIds Xml=null,
	@CustomerId uniqueidentifier= null,
	@IsActiveAndIsOnline TINYINT=1,
	@IsOnline TINYINT=0
)

AS
BEGIN
	Declare @Rowcount int
	Declare @FacetValues table(FacetValueID uniqueidentifier)

SET @SortCol = lower(@SortCol)



DECLARE @GroupIds TABLE (
GroupId uniqueidentifier
 )

IF @CustomerId IS NOT NULL
	INSERT INTO @GroupIds
	select GroupId from USMemberGroup WHERE MemberId = @CustomerId
ELSE
BEGIN
	
	DECLARE @GId uniqueidentifier
		SET @GId =(SELECT top 1 Value from STSiteSetting SS  
		INNER  JOIN STSettingType ST ON SS.SettingTypeId = ST.Id where ST.Name = 'CommerceEveryOneGroupId')
		
			INSERT INTO @GroupIds(GroupId) Values (@GId)
END

IF @PageSize IS NULL
	SET @PageSize = 2147483647

DECLARE @SQL nvarchar(max),@params nvarchar(100)
declare @isLatest bit

	Select @IsLatest =IsLatest from NVFilterQuery 
	Where Id=@FilterId And 
	(@ApplicationId IS NULL OR SiteId = @ApplicationId)
	--SiteId=ISNULL(@ApplicationId, SiteId)
	if (@IsLatest=0)
	Begin
		Exec NavFilter_ExecuteQuery @FilterId 
	End

	DECLARE @ProductIDs Table
	(
		Row_ID int IDENTITY(1,1) PRIMARY KEY,
		ProductID uniqueidentifier
	)

IF(@FacetValueIds is not null)
BEGIN
			Insert into @FacetValues
			Select tab.col.value('text()[1]','uniqueidentifier')	
			FROM @FacetValueIds.nodes('/GenericCollectionOfGuid/guid')tab(col) 
			Set @Rowcount =@@RowCount		 
END


IF @SortCol = 'price asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice ASC			
												) AS [Row_ID],
										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice 
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId  and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID

									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId 

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1  AND P.SiteId=@ApplicationId   
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Asc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											 MinPrice ASC			
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,   
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId )
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId
									GROUP BY   cte.ProductId
						) iq	
						
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice asc
						

					END 
END
IF @SortCol = 'price desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID
								
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId   
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											MinPrice DESC
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId

									GROUP BY   cte.ProductId
						) iq	
					
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END 
END

IF @SortCol = 'soldcount desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId   
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount DESC		
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc


					END 
END


IF @SortCol = 'soldcount asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
							
							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate 
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
								--	INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId   
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount ASC				
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC


					END 
END

IF @SortCol = 'createddate asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId   
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate ASC			
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

					END 
END

IF @SortCol = 'createddate desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
						
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId and C.SiteId=@ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId  
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate DESC

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate desc	
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate Desc
						
					END 
END

IF @SortCol = 'title desc' 
    BEGIN
        IF ( @FacetValueIds IS NOT NULL ) 
            BEGIN							
                
					;WITH    PagingCTE (Row_ID,ProductId,CreatedDate, Title)
                              AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY Title DESC ) AS [Row_ID] ,
                                            PRS.Id ProductId ,
                                            PRS.CreatedDate,
											P.Title	
                                   FROM     ( SELECT    P.Id ,
                                                        SUM(PS.PreviousSoldCount) TotalSoldCount ,
                                                        MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
                                              FROM      ( SELECT
                                                              ProductID
                                                          FROM
                                                              ( SELECT DISTINCT
                                                              ProductID ,
                                                              FV.FacetValueID
                                                              FROM
                                                              @FacetValues FV
                                                              INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID
                                                              INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              WHERE
                                                              QueryId = @FilterId and C.SiteId=@ApplicationId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                              ) PF
                                                          GROUP BY ProductID
                                                          HAVING
                                                              COUNT(FacetValueID) = @RowCount
                                                        ) PID
                                                        INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
                                                        INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId
                                              WHERE     P.IsActive = 1 AND P.SiteId=@ApplicationId
                                              GROUP BY  P.Id
                                            ) PRS
											INNER JOIN dbo.PRProduct P ON PRS.Id = P.Id
                                 )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title DESC

            END
        ELSE 
            BEGIN 

						;
                WITH    PagingCTE ( Row_ID, ProductId, CreatedDate, Title )
                          AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY p.Title DESC ) AS [Row_ID] ,
                                        iq.ProductId ,
                                        iq.CreatedDate,
										P.Title
                               FROM     ( SELECT    ProductId ,
                                                    MIN(ListPrice) AS MinPrice ,
                                                    SUM(PreviousSoldCount) AS TotalSoldCount ,
                                                    MIN(CreatedDate) CreatedDate
                                          FROM      ( SELECT  P.Id AS ProductId ,
                                                              PS.ListPrice ,
                                                              PS.PreviousSoldCount ,
                                                              P.CreatedDate
                                                      FROM    dbo.vwProduct P
                                                              INNER JOIN NVFilterOutput FnFilter ON P.Id = FnFilter.ObjectId
                                                              INNER JOIN dbo.PRProductSKU PS ON ( P.Id = PS.ProductId )
                                                      WHERE   P.IsActive = 1 AND P.SiteId=@ApplicationId
                                                              AND FnFilter.QueryId = @FilterId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                    ) cte
                                          GROUP BY  ProductId
                                        ) iq	
										INNER JOIN Prproduct P ON iq.ProductId = p.ID
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
                                        
                             )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title DESC
						
            END 
    END
  
  IF @SortCol = 'title asc' 
    BEGIN
        IF ( @FacetValueIds IS NOT NULL ) 
            BEGIN							
                
					;WITH    PagingCTE (Row_ID,ProductId,CreatedDate, Title)
                              AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY Title asc ) AS [Row_ID] ,
                                            PRS.Id ProductId ,
                                            PRS.CreatedDate,
											P.Title	
                                   FROM     ( SELECT    P.Id ,
                                                        SUM(PS.PreviousSoldCount) TotalSoldCount ,
                                                        MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
                                              FROM      ( SELECT
                                                              ProductID
                                                          FROM
                                                              ( SELECT DISTINCT
                                                              ProductID ,
                                                              FV.FacetValueID
                                                              FROM
                                                              @FacetValues FV
                                                              INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID
                                                              INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              WHERE
                                                              QueryId = @FilterId and C.SiteId=@ApplicationId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                              ) PF
                                                          GROUP BY ProductID
                                                          HAVING
                                                              COUNT(FacetValueID) = @RowCount
                                                        ) PID
                                                        INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
                                                        INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId
                                              WHERE     P.IsActive = 1 AND P.SiteId=@ApplicationId
                                              GROUP BY  P.Id
                                            ) PRS
											INNER JOIN dbo.PRProduct P ON PRS.Id = P.Id
                                 )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title asc

            END
        ELSE 
            BEGIN 

						;
                WITH    PagingCTE ( Row_ID, ProductId, CreatedDate, Title )
                          AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY p.Title asc ) AS [Row_ID] ,
                                        iq.ProductId ,
                                        iq.CreatedDate,
										P.Title
                               FROM     ( SELECT    ProductId ,
                                                    MIN(ListPrice) AS MinPrice ,
                                                    SUM(PreviousSoldCount) AS TotalSoldCount ,
                                                    MIN(CreatedDate) CreatedDate
                                          FROM      ( SELECT  P.Id AS ProductId ,
                                                              PS.ListPrice ,
                                                              PS.PreviousSoldCount ,
                                                              P.CreatedDate
                                                      FROM    dbo.vwProduct P
                                                              INNER JOIN NVFilterOutput FnFilter ON P.Id = FnFilter.ObjectId
                                                              INNER JOIN dbo.PRProductSKU PS ON ( P.Id = PS.ProductId )
                                                      WHERE   P.IsActive = 1 AND P.SiteId=@ApplicationId
                                                              AND FnFilter.QueryId = @FilterId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                    ) cte
                                          GROUP BY  ProductId
                                        ) iq	
										INNER JOIN Prproduct P ON iq.ProductId = p.ID
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
                                        
                             )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title asc
						
            END 
    END  




--START
	IF (@IsActiveAndIsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN vwSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1 AND PS.IsActive = 1 AND PS.SiteId=@ApplicationId) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber
			
		END
	ELSE IF (@IsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber

		END
--END


END
GO
PRINT 'Modify stored procedure Product_GetOnlineProductByFilterMulti'
GO
IF(OBJECT_ID('Product_GetOnlineProductByFilterMulti') IS NOT NULL)
	DROP PROCEDURE Product_GetOnlineProductByFilterMulti
GO
CREATE PROCEDURE [dbo].[Product_GetOnlineProductByFilterMulti]
(
	@FilterId uniqueidentifier,
    @SortCol VARCHAR(25)=null,
    @PageSize INT=null, 
	@PageNumber INT=1,
	@ApplicationId uniqueidentifier = null,
	@FacetValueIds Xml=null,
	@CustomerId uniqueidentifier= null,
	@IsActiveAndIsOnline TINYINT=1,
	@IsOnline TINYINT=0	
	--,@VirtualCount int output
)

AS
BEGIN
	Declare @Rowcount int
	Declare @FacetValues table(FacetValueID uniqueidentifier,FacetId Uniqueidentifier)


SET @SortCol = lower(@SortCol)



DECLARE @GroupIds TABLE (
GroupId uniqueidentifier
 )

IF @CustomerId IS NOT NULL
	INSERT INTO @GroupIds
	select GroupId from USMemberGroup WHERE MemberId = @CustomerId
ELSE
BEGIN
	
	DECLARE @GId uniqueidentifier
		SET @GId =(SELECT top 1 Value from STSiteSetting SS  
		INNER  JOIN STSettingType ST ON SS.SettingTypeId = ST.Id where ST.Name = 'CommerceEveryOneGroupId')
		
			INSERT INTO @GroupIds(GroupId) Values (@GId)
END

IF @PageSize IS NULL
	SET @PageSize = 2147483647

DECLARE @SQL nvarchar(max),@params nvarchar(100)
declare @isLatest bit

	Select @IsLatest =IsLatest from NVFilterQuery 
	Where Id=@FilterId And 
	(@ApplicationId is null or SiteId = @ApplicationId)
	--SiteId=ISNULL(@ApplicationId, SiteId)
	if (@IsLatest=0)
	Begin
		Exec NavFilter_ExecuteQuery @FilterId 
	End

	DECLARE @ProductIDs Table
	(
		Row_ID int IDENTITY(1,1) PRIMARY KEY,
		ProductID uniqueidentifier
	)
	
IF(@FacetValueIds is not null)
BEGIN
		Insert into @FacetValues
		Select tab.col.value('(value/guid/text())[1]','uniqueidentifier') ,tab.col.value('(key/guid/text())[1]','uniqueidentifier')
		FROM @FacetValueIds.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item')tab(col) 
END

IF @SortCol = 'price asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	
							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice ASC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice 
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID

									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId 

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId     
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice ASC
							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											 MinPrice ASC			
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,   
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId
									GROUP BY   cte.ProductId
						) iq	
						
					)

					
						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice ASC

					END 
END
IF @SortCol = 'price desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID
								
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1  AND P.SiteId=@ApplicationId   
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											MinPrice DESC
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId

									GROUP BY   cte.ProductId
						) iq	
					
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc


					END 
END

IF @SortCol = 'soldcount desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
									
				Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1  AND P.SiteId=@ApplicationId   
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc
							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount DESC		
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc

					END 
END


IF @SortCol = 'soldcount asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
										
			Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate 
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
								--	INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount ASC				
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC


					END 
END

IF @SortCol = 'createddate asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate ASC			
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC


					END 
END

IF @SortCol = 'createddate desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate DESC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate desc	
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate Desc


					END 
END

IF @SortCol = 'title asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													Title ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 Title
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.Title) Title
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,Title)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											Title ASC			
											) AS [Row_ID],
									iq.ProductId,
									Title
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(Title) Title
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.Title
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title ASC


					END 
END

IF @SortCol = 'title desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													Title desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 Title
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.Title) Title
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId AND SiteId = @ApplicationId
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title DESC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,Title)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											Title desc	
											) AS [Row_ID],
									iq.ProductId,
									Title
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(Title) Title
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.Title
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title Desc


					END 
END

--START
	IF (@IsActiveAndIsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN vwSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1 AND PS.IsActive = 1 AND PS.SiteId=@ApplicationId ) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber
			
		END
	ELSE IF (@IsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN vwSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1 AND PS.SiteId=@ApplicationId ) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber

		END
--END

END
GO
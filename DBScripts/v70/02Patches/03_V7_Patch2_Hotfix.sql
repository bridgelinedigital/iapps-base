PRINT 'Modify View vwSiteAttributeValue'
GO
IF(OBJECT_ID('vwSiteAttributeValue') IS NOT NULL)
	DROP View vwSiteAttributeValue
GO
CREATE VIEW [dbo].[vwSiteAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.SiteId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V

	UNION ALL

	SELECT ROW_NUMBER() over (PARTITION BY V.AttributeId, C.Id ORDER BY P.LftValue DESC) AS vRank,
		V.Id,
		C.Id AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V
		JOIN SISite P ON V.SiteId = P.Id
		JOIN SISite C ON C.LftValue > P.LftValue AND C.RgtValue < P.RgtValue
	WHERE V.IsShared = 1
		AND P.DistributionEnabled = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT V.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.SiteId,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATSiteAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.SiteId
		AND C.SiteId = V.SiteId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1
GO
PRINT 'Modify stored procedure Membership_GetUserWithProfile'
GO
IF(OBJECT_ID('Membership_GetUserWithProfile') IS NOT NULL)
	DROP PROCEDURE Membership_GetUserWithProfile
GO
CREATE PROCEDURE [dbo].[Membership_GetUserWithProfile]  
(
	@ProductId     uniqueidentifier = NULL,  
	@ApplicationId    uniqueidentifier,  
	@UserId      uniqueidentifier,  
	@UserName     nvarchar(256) = NULL  
)
AS   
BEGIN  
	DECLARE @EmptyGuid uniqueidentifier  
	SELECT @EmptyGuid = dbo.GetEmptyGUID()  

	DECLARE @DeleteStatus int
	SELECT @DeleteStatus = dbo.GetDeleteStatus()
  
	IF(@UserId IS NULL)  
	BEGIN  
		SELECT @UserId = Id FROM dbo.USUser U, dbo.USSiteUser S 
		WHERE LOWER(U.UserName) = LOWER(@UserName) 
			AND (@ApplicationId IS NULL OR S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
			AND U.Id = S.UserId   
			AND (@ProductId IS NULL OR @ProductId = @EmptyGuid OR @ProductId = S.ProductId)  
			AND Status != @DeleteStatus
	END    
  
	SELECT  U.UserName,  
		U.CreatedDate,  
		M.Email,   
		M.PasswordQuestion,    
		M.IsApproved,  
		M.LastLoginDate,   
		U.LastActivityDate,  
		M.LastPasswordChangedDate,   
		U.Id,   
		M.IsLockedOut,  
		M.LastLockoutDate,
		U.FirstName,
		U.LastName,
		U.MiddleName,
		U.ExpiryDate,
		U.EmailNotification,
		U.TimeZone,
		U.ReportRangeSelection,
		U.ReportStartDate,
		U.ReportEndDate,
		U.BirthDate,
		U.CompanyName,
		U.Gender,
		U.HomePhone,
		U.MobilePhone,
		U.OtherPhone,
		U.ImageId,
		U.LeadScore,
		U.DashboardData,
		S.IsSystemUser
	FROM dbo.USUser U, dbo.USMembership M, dbo.USSiteUser S
	WHERE   U.Id = @UserId  
		AND U.Id = M.UserId     
		AND (@ApplicationId IS NULL OR S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
		AND M.UserId = S.UserId  
		AND U.Status != @DeleteStatus
		AND (@ProductId IS NULL OR @ProductId = @EmptyGuid or @ProductId = S.ProductId)  
	ORDER BY S.IsSystemUser DESC
  
	SELECT UserId,  
		PropertyName,  
		PropertyValueString,  
		PropertyValuesBinary,  
		LastUpdatedDate  
	FROM dbo.USMemberProfile  
	WHERE UserId = @UserId  
END
GO
PRINT 'Modify stored procedure SiteDto_Get'
GO
IF(OBJECT_ID('SiteDto_Get') IS NOT NULL)
	DROP PROCEDURE SiteDto_Get
GO
CREATE PROCEDURE [dbo].[SiteDto_Get]
(
	@Id						uniqueidentifier = NULL,
	@Ids					nvarchar(max)=NULL,
	@MasterSiteId			uniqueidentifier = NULL,
	@IncludeVariantSites	bit = NULL,
	@CurrentMasterSiteOnly	bit = NULL,
	@CampaignId				uniqueidentifier = NULL,
	@SiteListId				uniqueidentifier = NULL,
	@ParentSiteId			uniqueidentifier = NULL,
	@UserId					uniqueidentifier = NULL,
	@SiteId					uniqueidentifier = NULL,
	@Status					int = NULL,
	@PageNumber				int = NULL,
	@PageSize				int = NULL,
	@MaxRecords				int = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@Keyword				nvarchar(MAX) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50), 
		@IdsExist bit, @LftValue int, @RgtValue int
	SET @PageLowerBound = @PageSize * @PageNumber

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @IncludeVariantSites IS NULL SET @IncludeVariantSites = 1
	IF @Status IS NULL SET @Status = 1
	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'	

	IF @MasterSiteId IS NULL AND @CurrentMasterSiteOnly = 1
		SELECT TOP 1 @MasterSiteId = MasterSiteId FROM SISite WHERE Id = @SiteId
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbSiteIds TABLE (Id uniqueidentifier)
	
	IF @Ids IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT Items FROM dbo.SplitGUID(@Ids, ',') 
	END
	IF @SiteListId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM SISiteListSite WHERE SiteListId = @SiteListId
	END
	
	IF @CampaignId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		DECLARE @CampaignSiteId uniqueidentifier
		SELECT TOP 1 @CampaignSiteId = ApplicationId FROM MKCampaign WHERE Id = @CampaignId
		IF @CampaignSiteId = @SiteId SET @CampaignSiteId = NULL

		INSERT INTO @tbSiteIds
		SELECT SiteId FROM MKCampaignRunHistory H
		WHERE EXISTS (SELECT 1 FROM MKCampaignSend S WHERE H.CampaignSendId = S.Id 
				AND CampaignId = @CampaignId AND (@CampaignSiteId IS NULL OR SiteId = @SiteId))
	END

	IF @UserId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM [dbo].[GetAccessibleSitesForUser](@UserId)
	END
	ELSE IF @ParentSiteId IS NOT NULL AND @ParentSiteId != dbo.GetEmptyGUID()
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM [dbo].[GetChildrenSites](@ParentSiteId, 1)
	END
	
	INSERT INTO @tbIds
	SELECT S.Id AS Id, ROW_NUMBER() OVER (ORDER BY
							CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
							CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
							CASE WHEN @SortClause IS NULL THEN CreatedDate END ASC	
						) AS RowNumber
	FROM SISite AS S
	WHERE (@Id IS NULL OR S.Id = @Id) AND
		(@Status IS NULL OR S.Status = @Status) AND
		(@MasterSiteId IS NULL OR S.Id = @MasterSiteId OR S.MasterSiteId = @MasterSiteId) AND
		(@IncludeVariantSites = 1 OR S.MasterSiteId = S.Id OR S.MasterSiteId IS NULL) AND
		(@IdsExist IS NULL OR EXISTS (Select 1 FROM @tbSiteIds T Where T.Id = S.Id)) AND
		(@Keyword IS NULL OR (S.Title like @Keyword OR S.Description like @Keyword OR S.PrimarySiteUrl like @Keyword OR S.ExternalCode like @Keyword))
		
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				S.Id AS Id
		FROM SISite S
			JOIN @tbIds T ON T.Id = S.Id
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT		S.*,
				SiteHierarchyPath AS SitePath,
				T.RowNumber,
				T.TotalRecords
	FROM SISite AS S
		JOIN @tbPagedResults AS T ON T.Id = S.Id
	ORDER BY RowNumber

	SELECT V.*, A.Title AS AttributeTitle
	FROM ATSiteAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbPagedResults T ON V.SiteId = T.Id
END
GO
Print 'CSCustomerAttributeValue Index Creation on AttributeId column'
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_CSCustomerAttributeValue_AttributeId' AND object_id = OBJECT_ID('CSCustomerAttributeValue'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_CSCustomerAttributeValue_AttributeId
	ON [dbo].[CSCustomerAttributeValue] ([AttributeId]) INCLUDE ([CustomerId])
END
GO
PRINT 'Creating View vwContactFullName'
GO
IF (OBJECT_ID('vwContactFullName') IS NOT NULL)
	DROP View vwContactFullName	
GO
CREATE VIEW [dbo].[vwContactFullName]
AS
SELECT 
	Id AS UserId, 
	CASE 
		WHEN LastName IS NULL Then ' , '
		WHEN LEN(LTRIM(LastName)) = 0 THEN '' 
		ELSE LastName + ', '  
	END + ISNULL(FirstName, ' ') AS UserFullName
FROM USUser

UNION ALL

SELECT 
	Id AS UserId, 
	CASE 
		WHEN LastName IS NULL Then ' , '
		WHEN LEN(LTRIM(LastName)) = 0 THEN '' 
		ELSE LastName + ', '  
	END + ISNULL(FirstName, ' ') AS UserFullName
FROM MKContact
GO
PRINT 'Creating View vwFormResponse'
GO
IF (OBJECT_ID('vwFormResponse') IS NOT NULL)
	DROP View vwFormResponse	
GO
CREATE VIEW [dbo].[vwFormResponse]
AS
SELECT	R.Id,
		R.FormsId AS FormId,
		R.FormsResponseXML AS ResponseXml,
		R.ResponseDate,
		R.IPAddress,
		R.SiteId,
		R.UserId AS ResponseById,
		RN.UserFullName AS ResponseByFullName,
		LFR.Email,
		LFR.FirstName,
		LFR.MiddleName,
		LFR.LastName,
		LFR.CompanyName,
		LFR.Gender,
		LFR.BirthDate,
		LFR.HomePhone,
		LFR.MobilePhone,
		LFR.OtherPhone,
		LFR.Attributes
FROM	FormsResponse AS R
		LEFT JOIN MKLeadFormResponse AS LFR ON LFR.FormResponseId = R.Id
		LEFT JOIN vwContactFullName AS RN ON RN.UserId = R.UserId
GO
PRINT 'Creating View vwCustomer'
GO
IF (OBJECT_ID('vwCustomer') IS NOT NULL)
	DROP View vwCustomer	
GO
CREATE VIEW [dbo].[vwCustomer] 
AS         
SELECT	CUP.Id, U.FirstName, U.LastName, U.MiddleName, U.UserName, NULL AS [Password], M.Email, U.EmailNotification, U.CompanyName, U.BirthDate, U.Gender, U.LeadScore,
		CUP.IsBadCustomer, CUP.IsActive, M.IsApproved, M.IsLockedOut, CUP.IsOnMailingList, CUP.[Status], CUP.CSRSecurityQuestion, CUP.CSRSecurityPassword, U.HomePhone,
		U.MobilePhone, U.OtherPhone, U.ImageId, CUP.AccountNumber, CUP.IsExpressCustomer, CUP.ExternalId, CUP.ExternalProfileId,
		(
			SELECT	COUNT(*)
			FROM	CSCustomerLogin
			WHERE	CustomerId = CUP.Id
		) + 1 AS NumberOfLoginAccounts,
		CASE WHEN EXISTS
		(
			SELECT	Id
			FROM	PTLineOfCreditInfo
			WHERE	CustomerId =  CUP.Id AND
					[Status] != 3
		) THEN 1 ELSE 0 END AS HasLOC,
		A.Id AS AddressId, A.AddressLine1, A.AddressLine2, A.AddressLine3, A.City, A.StateId, ST.[State], ST.StateCode,
		A.CountryId, C.CountryName, C.CountryCode, A.Zip, A.County, A.Phone,
		U.ExpiryDate AS ExpirationDate, U.LastActivityDate,
		CASE WHEN EXISTS (
			SELECT	*
			FROM	USSiteUser
			WHERE	UserId = U.Id AND
					IsSystemUser = 1
		) THEN 1 ELSE 0 END AS IsSystemUser,
		CUP.IsTaxExempt,
		CUP.TaxId,
		U.CreatedDate,  U.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		ISNULL(U.ModifiedDate, U.CreatedDate) AS ModifiedDate, ISNULL(U.ModifiedBy, U.CreatedBy) AS ModifiedBy, MFN.UserFullName AS ModifiedByFullName
FROM	USCommerceUserProfile AS CUP
        INNER JOIN USUser AS U ON U.Id = CUP.Id
		INNER JOIN USMembership AS M ON M.UserId = U.Id
		LEFT JOIN USUserShippingAddress AS USA ON USA.UserId = U.Id AND USA.IsPrimary = 1
		LEFT JOIN GLAddress AS A ON A.Id = USA.AddressId
		LEFT JOIN GLState AS ST ON ST.Id = A.StateId
		LEFT JOIN GLCountry AS C ON C.Id = A.CountryId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = U.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = U.ModifiedBy
WHERE	U.[Status] !=3
GO
PRINT 'Creating View vwCustomerInfo'
GO
IF (OBJECT_ID('vwCustomerInfo') IS NOT NULL)
	DROP View vwCustomerInfo	
GO
CREATE VIEW [dbo].[vwCustomerInfo] 
AS         
SELECT CUP.Id, 
	U.FirstName, 
	U.LastName, 
	U.MiddleName, 
	M.Email
FROM dbo.USCommerceUserProfile AS CUP
	INNER JOIN dbo.USUser AS U ON U.Id = CUP.Id
	INNER JOIN dbo.USMembership AS M ON M.UserId = U.Id
WHERE U.[Status] !=3
GO
PRINT 'Creating View vwCustomerOrderSummary'
GO
IF (OBJECT_ID('vwCustomerOrderSummary') IS NOT NULL)
	DROP View vwCustomerOrderSummary	
GO
CREATE VIEW dbo.vwCustomerOrderSummary
WITH SCHEMABINDING
AS

SELECT CustomerId, 
	SiteId, 
	COUNT_BIG(*) AS TotalOrders, 
	SUM(GrandTotal) AS TotalSpend
FROM dbo.OROrder O 
	JOIN dbo.SISITE S ON O.SiteId = S.Id
WHERE S.Status = 1
GROUP BY CustomerId, SiteId
GO
CREATE UNIQUE CLUSTERED INDEX IDX_vwCustomerOrderSummary_CustomerIdSiteId
   ON dbo.vwCustomerOrderSummary (CustomerId, SiteId)
GO
PRINT 'Creating View vwCustomerList'
GO
IF (OBJECT_ID('vwCustomerList') IS NOT NULL)
	DROP View vwCustomerList	
GO
CREATE VIEW [dbo].[vwCustomerList] 
AS    
WITH CTE
AS
(     
SELECT	CUP.Id, U.FirstName, U.LastName, U.MiddleName, U.UserName, NULL AS [Password], M.Email, U.EmailNotification, U.CompanyName, U.BirthDate, U.Gender, U.LeadScore,
		CUP.IsBadCustomer, CUP.IsActive, M.IsApproved, M.IsLockedOut, CUP.IsOnMailingList, CUP.[Status], CUP.CSRSecurityQuestion, CUP.CSRSecurityPassword, U.HomePhone,
		U.MobilePhone, U.OtherPhone, U.ImageId, CUP.AccountNumber, CUP.IsExpressCustomer, CUP.ExternalId, CUP.ExternalProfileId,
		(
			SELECT	COUNT(*)
			FROM	CSCustomerLogin
			WHERE	CustomerId = CUP.Id
		) + 1 AS NumberOfLoginAccounts,
		CASE WHEN EXISTS
		(
			SELECT	Id
			FROM	PTLineOfCreditInfo
			WHERE	CustomerId =  CUP.Id AND
					[Status] != 3
		) THEN 1 ELSE 0 END AS HasLOC,
		A.Id AS AddressId, A.AddressLine1, A.AddressLine2, A.AddressLine3, A.City, A.StateId, ST.[State], ST.StateCode,
		A.CountryId, C.CountryName, C.CountryCode, A.Zip, A.County, A.Phone,
		U.ExpiryDate AS ExpirationDate, U.LastActivityDate,
		CASE WHEN EXISTS (
			SELECT	*
			FROM	USSiteUser
			WHERE	UserId = U.Id AND
					IsSystemUser = 1
		) THEN 1 ELSE 0 END AS IsSystemUser,
		CUP.IsTaxExempt,
		CUP.TaxId,
		U.CreatedDate,  U.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		ISNULL(U.ModifiedDate, U.CreatedDate) AS ModifiedDate, ISNULL(U.ModifiedBy, U.CreatedBy) AS ModifiedBy, MFN.UserFullName AS ModifiedByFullName
FROM	USCommerceUserProfile AS CUP
        INNER JOIN USUser AS U ON U.Id = CUP.Id
		INNER JOIN USMembership AS M ON M.UserId = U.Id
		LEFT JOIN USUserShippingAddress AS USA ON USA.UserId = U.Id AND USA.IsPrimary = 1
		LEFT JOIN GLAddress AS A ON A.Id = USA.AddressId
		LEFT JOIN GLState AS ST ON ST.Id = A.StateId
		LEFT JOIN GLCountry AS C ON C.Id = A.CountryId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = U.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = U.ModifiedBy
WHERE	U.[Status] !=3
)

SELECT C.Id, C.FirstName, C.LastName, C.MiddleName, C.UserName, NULL AS [Password], C.Email, C.EmailNotification, C.CompanyName, C.BirthDate, C.Gender, C.LeadScore,
		C.IsBadCustomer, C.IsActive, C.IsApproved, C.IsLockedOut, C.IsOnMailingList, C.[Status], C.CSRSecurityQuestion, C.CSRSecurityPassword, C.HomePhone,
		C.MobilePhone, C.OtherPhone, C.ImageId, C.AccountNumber, C.IsExpressCustomer, C.ExternalId, C.ExternalProfileId,
		C.NumberOfLoginAccounts, C.HasLOC, C.AddressId, C.AddressLine1, C.AddressLine2, C.AddressLine3, C.City, C.StateId, C.[State],C.StateCode,
		C.CountryId, C.CountryName, C.CountryCode, C.Zip, C.County, C.Phone, C.ExpirationDate, C.LastActivityDate, C.IsSystemUser,
		C.IsTaxExempt, C.TaxId,	C.CreatedDate,  C.CreatedBy, C.CreatedByFullName, C.ModifiedDate, C.ModifiedBy, C.ModifiedByFullName,
        S.Id AS SiteId, 
		CASE WHEN CO.SiteId = S.Id THEN CAST(CO.TotalOrders AS int) ELSE 0 END AS TotalOrders, 
		CASE WHEN CO.SiteId = S.Id THEN CO.TotalSpend ELSE 0 END AS TotalSpend, 
		(
			SELECT	TOP 1 OrderDate
			FROM	OROrder
			WHERE	CustomerId  = C.Id
			ORDER BY OrderDate DESC
		)  AS LastPurchase
FROM CTE  C 
	CROSS JOIN SISite S
    LEFT JOIN  vwCustomerOrderSummary AS CO ON CO.CustomerId = C.Id and CO.SiteId = S.Id
WHERE S.Status = 1 
GO

PRINT 'Creating View vwOrder'
GO
IF (OBJECT_ID('vwOrder') IS NOT NULL)
	DROP View vwOrder	
GO
CREATE View vwOrder
AS
SELECT	O.Id, O.CartId, O.OrderTypeId, O.OrderStatusId, O.SiteId, O.OrderDate, O.PurchaseOrderNumber, O.ExternalOrderNumber,
		O.CustomerId, CASE WHEN LEN(LTRIM(C.LastName)) = 0 THEN ''  ELSE C.LastName + ', ' END + C.FirstName AS CustomerName, C.Email AS CustomerEmail,
		O.OrderTotal, O.TaxableOrderTotal, O.TotalTax, O.TotalShippingDiscount, O.TotalShippingCharge, O.TotalDiscount, O.CODCharges, O.PaymentTotal, O.PaymentRemaining, O.RefundTotal, O.GrandTotal,
		O.CreatedDate, O.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
		O.ModifiedDate, O.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
FROM	OROrder AS O
		LEFT JOIN vwCustomerInfo AS C ON C.Id = O.CustomerId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = O.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = O.ModifiedBy
GO
PRINT 'Creating View vwWebsiteUser'
GO
IF (OBJECT_ID('vwWebsiteUser') IS NOT NULL)
	DROP View vwWebsiteUser	
GO
CREATE VIEW [dbo].[vwWebsiteUser]
AS 
SELECT U.Id,
	U.FirstName,
	U.LastName,
	U.MiddleName,
	U.UserName,
	M.Email,
	U.LastActivityDate,
	U.EmailNotification,
	U.LeadScore,
	U.ExpiryDate AS ExpirationDate,
	CASE WHEN U.[Status] = 3 THEN 3
		WHEN M.IsLockedOut = 1 THEN 2
		WHEN M.IsApproved = 0 THEN 4
		ELSE 1 END AS [Status],
	U.[CreatedDate],
	U.[ModifiedDate],
	SU.SiteId
FROM USUser U
	JOIN USMembership M ON U.Id = M.UserId
	JOIN USSiteUser SU ON U.Id = SU.UserId
	LEFT JOIN USCommerceUserProfile CU ON CU.Id = U.Id
WHERE SU.ProductId = 'CBB702AC-C8F1-4C35-B2DC-839C26E39848' 
	AND SU.IsSystemUser = 0
	AND CU.Id IS NULL
GO
PRINT 'Creating View VW_UserFullName'
GO
IF (OBJECT_ID('VW_UserFullName') IS NOT NULL)
	DROP View VW_UserFullName	
GO
CREATE VIEW [dbo].[VW_UserFullName]
AS
SELECT 
	Id AS UserId, 
	CASE 
		WHEN LastName IS NULL Then ' , '
		WHEN LEN(LTRIM(LastName)) = 0 THEN '' 
		ELSE LastName + ', '  
	END + ISNULL(FirstName, ' ') AS UserFullName
FROM dbo.USUser U 
GO
PRINT 'Modify stored procedure Membership_GetUserMembership'
GO
IF(OBJECT_ID('Membership_GetUserMembership') IS NOT NULL)
	DROP PROCEDURE Membership_GetUserMembership
GO
CREATE PROCEDURE [dbo].[Membership_GetUserMembership]
(
	@ProductId			uniqueidentifier = NULL,
	@ApplicationId		uniqueidentifier = NULL, --This is for getting applicationIds for the user and role.
	@UserId				uniqueidentifier,
	@RoleId				int = NULL,
	@ObjectTypeId		int,
	@ParentSiteId		uniqueidentifier = NULL,
	@OnlyParentSite		bit = NULL,
	@OnlySystemUser		bit = NULL,
	@OriginalMembership bit = NULL
)
AS
BEGIN
	DECLARE @tbSites AS SiteTableType 
	
	IF(@OnlyParentSite IS NULL) SET @OnlyParentSite = 0

	IF(@OriginalMembership = 1)
	BEGIN
		SELECT DISTINCT SiteId AS ObjectId
		FROM dbo.USSiteUser US
		WHERE ProductId = @ProductId
			AND UserId = @UserId
	END
	ELSE
	BEGIN
		--If Role Id is null then get all the sites in which the given user is having membership.
		IF (@RoleId IS NULL)
		BEGIN
			DECLARE @EmptyGuid uniqueidentifier
			SET @EmptyGuid = dbo.GetEmptyGUID()
	
			INSERT INTO @tbSites
			SELECT DISTINCT SiteId AS ObjectId
			FROM dbo.USSiteUser US
				JOIN SISite S ON US.SiteId = S.Id
			WHERE UserId = @UserId 
				AND (@ProductId IS NULL OR ProductId = @ProductId)
				AND (@OnlySystemUser IS NULL OR US.IsSystemUser = 1)
				AND ((@OnlyParentSite = 0 AND S.ParentSiteId = ISNULL(@ParentSiteId, S.ParentSiteId)) 
					OR (@OnlyParentSite = 1 AND S.ParentSiteId = @EmptyGuid))
			
			IF(@OnlyParentSite = 0)
				SELECT * FROM [dbo].[GetVariantSitesHavingPermission](@tbSites)
			ELSE	
				SELECT * FROM @tbSites
		END
		ELSE IF (@RoleId IS NOT NULL AND @UserId IS NOT NULL) --Gets all the sites in which the user is having the given role
		BEGIN
			INSERT INTO @tbSites
			SELECT DISTINCT SU.SiteId
			FROM USMemberRoles MR
				JOIN USMemberGroup MG ON MR.MemberId = MG.GroupId
				JOIN USSiteUser SU ON MG.MemberId = SU.UserId
			WHERE RoleId = @RoleId 
				AND SU.UserId = @UserId
				AND MG.ApplicationId = SU.SiteId  
				AND (@ObjectTypeId IS NULL OR MR.ObjectTypeId = @ObjectTypeId) 
				AND (@ProductId IS NULL OR SU.ProductId = @ProductId) 
				AND (@OnlySystemUser IS NULL OR SU.IsSystemUser = 1)
				
			SELECT * FROM [dbo].[GetVariantSitesHavingPermission](@tbSites)
		END
	END
END
GO
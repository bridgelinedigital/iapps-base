PRINT 'Add Site Settings'
DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int

SET @SettingGroupId = 1
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'ImageLibrary.MaxSize')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('ImageLibrary.MaxSize', 'Max file size for image library',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	SET @SettingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId, SettingTypeId, Value)
	SELECT Id, @SettingTypeId, '0' FROM SISite
	WHERE Id = MasterSiteId AND Status = 1
END
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'Unbound.404PageUrl')
BEGIN
	SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
	VALUES ('Unbound.404PageUrl', 'Relative URL of 404 Page',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
END
GO
PRINT 'Add ImageAltText column in BLPost'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'BLPost' AND COLUMN_NAME = 'ImageAltText')
BEGIN	
	ALTER TABLE BLPost ADD ImageAltText nvarchar(max) NULL
END
GO
PRINT 'Add ImageName column in BLPost'
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'BLPost' AND COLUMN_NAME = 'ImageName')
BEGIN	
	ALTER TABLE BLPost ADD ImageName nvarchar(max) NULL
END
GO

PRINT 'Altering PostDto_Get'
GO

IF (OBJECT_ID('PostDto_Get') IS NOT NULL)
	DROP PROCEDURE PostDto_Get
GO
CREATE PROCEDURE [dbo].[PostDto_Get] 
(
	@Id						uniqueidentifier = NULL,
	@Ids					nvarchar(max) = NULL,
	@SiteId					uniqueidentifier = NULL,
	@PostMonth				varchar(20) = NULL,
	@PostYear				varchar(20) = NULL,
	@FolderId				uniqueIdentifier = NULL,
	@BlogId					uniqueIdentifier = NULL,
	@CategoryId				uniqueIdentifier = NULL,
	@Label					nvarchar(1024) = NULL,
	@Author					nvarchar(1024) = NULL,
	@IsSticky				bit = NULL,
	@IncludeContentDetails	bit = NULL,
	@IncludeVersion			bit = NULL,
	@VersionId				uniqueidentifier = NULL,
	@IgnoreSite				bit = NULL,
	@ApprovedCommentsOnly	bit = NULL,
	@PageNumber				int = NULL,
	@PageSize				int = NULL,
	@Status					int = NULL,
	@MaxRecords				int = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@Query					nvarchar(max) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(500), @DeleteStatus int
	SET @DeleteStatus = dbo.GetDeleteStatus()
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @ApprovedCommentsOnly IS NULL 
		SET @ApprovedCommentsOnly = 0

	IF @Status = 0 SET @Status = NULL

	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbQueryIds TABLE (Id uniqueidentifier primary key, RowNumber int)

	IF @Query IS NOT NULL
	BEGIN
		INSERT INTO @tbQueryIds
		EXEC sp_executesql @Query
	END

	INSERT INTO @tbIds
	SELECT Id,  ROW_NUMBER() OVER (ORDER BY
		CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
		CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
		CASE WHEN @SortClause = 'CreatedDate ASC' THEN CreatedDate END ASC,
		CASE WHEN @SortClause = 'CreatedDate DESC' THEN CreatedDate END DESC,
		CASE WHEN @SortClause IS NULL THEN CreatedDate END DESC	
	) AS RowNumber
	FROM BLPost P
	WHERE P.Status != @DeleteStatus AND
		(@Id IS NULL OR P.Id = @Id) AND
		(@Status IS NULL OR P.Status = @Status) AND
		(@BlogId IS NULL OR P.BlogId = @BlogId) AND
		(@SiteId IS NULL OR @IgnoreSite = 1 OR P.ApplicationId = @SiteId) AND
		(@PostYear IS NULL OR YEAR(PostDate) = @PostYear) AND 
		(@PostMonth IS NULL OR MONTH(PostDate) = @PostMonth) AND 
		(@FolderId IS NULL OR HSId = @FolderId) AND 
		(@IsSticky IS NULL OR IsSticky = @IsSticky) AND
		(@Label IS NULL OR Labels LIKE '%' + @Label + '%') AND
		(@Author IS NULL OR AuthorEmail like @Author OR AuthorName like @Author) AND
		(@Query IS NULL OR P.Id IN (SELECT Id FROM @tbQueryIds))

	IF @Query IS NOT NULL
	BEGIN
		UPDATE T
		SET T.RowNumber = Q.RowNumber
		FROM @tbIds T JOIN @tbQueryIds Q ON T.Id = Q.Id
	END
	
	IF @Ids IS NOT NULL
	BEGIN
		DELETE FROM @tbIds 
		WHERE Id NOT IN (SELECT Items FROM dbo.SplitGUID(@Ids, ','))
	END

	IF @CategoryId IS NOT NULL
	BEGIN
		DELETE FROM @tbIds 
		WHERE Id NOT IN (SELECT ObjectId FROM COTaxonomyObject WHERE TaxonomyId = @CategoryId AND ObjectTypeId = 40)
	END
		
	DECLARE @tbPagedResults TABLE(Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
			COUNT(T.Id) OVER () AS TotalRecords,
			P.Id AS Id
		FROM BLPost P
			JOIN @tbIds T ON T.Id = P.Id
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)

	SELECT P.Id,			
		P.ApplicationId,
		P.Title,
		P.ShortDescription,
		P.AuthorName,
		P.AuthorEmail,
		P.Labels,
		P.AllowComments,
		P.PostDate,
		P.IsScheduled,
		P.ScheduledPublishDate,
		P.UseLocalTimeZone,
		P.ScheduledTimeZone,
		P.Location,
		P.Status,
		P.IsSticky,
		P.CreatedBy,
		P.CreatedDate,
		P.ModifiedBy,
		P.ModifiedDate,
		P.FriendlyName,
		P.PostContentId,
		P.ContentId,
		P.ImageUrl,
		P.TitleTag,
		P.H1Tag,
		P.KeywordMetaData,
		P.DescriptiveMetaData,
		P.OtherMetaData,
		P.WorkflowState,
		P.PublishDate,
		P.PublishCount,
		P.INWFFriendlyName,
		P.HSId AS MonthId,
		B.BlogListPageId,
		B.Id AS BlogId,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName,
		T.RowNumber,
		T.TotalRecords,
		ISNULL(P.ImageAltText, '') ImageAltText,
		ISNULL(P.ImageName,'') ImageName

	FROM BLPost P
		JOIN @tbPagedResults T ON T.Id = P.Id
		JOIN BLBlog B ON P.BlogId = B.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = P.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = P.ModifiedBy
	ORDER BY RowNumber ASC

	SELECT C.Id,
		C.ApplicationId,
		C.Title,
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		CASE WHEN @IncludeContentDetails = 1 THEN C.XMLString ELSE NULL END AS XMLString,
		C.ParentId,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName
	FROM COContent C
		JOIN BLPost P ON C.Id = P.PostContentId
		JOIN @tbPagedResults T ON P.Id = T.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = C.ModifiedBy

	SELECT DISTINCT B.*,
		LOWER(PM.CompleteFriendlyUrl + '/' + PD.FriendlyName) AS BlogUrl,
		CU.UserFullName CreatedByFullName,
		MU.UserFullName ModifiedByFullName
	FROM BLBlog B
		JOIN BLPost P ON B.Id = P.BlogId
		JOIN PageDefinition PD ON B.BlogListPageId = PD.PageDefinitionId
		JOIN PageMapNodePageDef PMD ON PD.PageDefinitionId = PMD.PageDefinitionId
		JOIN PageMapNode PM ON PM.PageMapNodeId = PMD.PageMapNodeId
		JOIN @tbPagedResults T ON P.Id = T.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = B.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = B.ModifiedBy

	SELECT C.Id,
		C.Title,
		T.Id AS PostId
	FROM COTaxonomy C
		JOIN COTaxonomyObject O ON C.Id = O.TaxonomyId
		JOIN @tbPagedResults T ON T.Id = O.ObjectId

	SELECT Count(*) AS NoOfComments,
		T.Id AS PostId
	FROM BLComments C
		JOIN @tbPagedResults T ON C.ObjectId = T.Id
	WHERE (@ApprovedCommentsOnly = 0 OR C.Status = 4)
	AND C.Status <> dbo.GetDeleteStatus() 
	GROUP BY T.Id

	IF @IncludeVersion = 1
	BEGIN
		DECLARE @tbVersionIds TABLE(Id uniqueidentifier)
		IF @VersionId IS NULL
		BEGIN
			;WITH CTEVersion AS (
				SELECT ROW_NUMBER() OVER (PARTITION BY P.Id ORDER BY RevisionNumber DESC, VersionNumber DESC) AS VerRank,
					V.Id FROM VEVersion V
					JOIN BLPost P On P.Id = V.ObjectId
					JOIN @tbPagedResults T ON P.Id = T.Id
			)
			INSERT INTO @tbVersionIds
			SELECT Id FROM CTEVersion WHERE VerRank = 1
		END
		ELSE
		BEGIN
			INSERT INTO @tbVersionIds VALUES (@VersionId)
		END

		SELECT V.Id AS VersionId,
			V.ObjectId AS PostId,
			V.XMLString
		FROM VEVersion V
			JOIN @tbVersionIds T On T.Id = V.Id

		SELECT 
			V.PostVersionId AS VersionId,
			C.ContentId,
			C.XMLString
		FROM VEPostContentVersion V
			JOIN @tbVersionIds T On T.Id = V.PostVersionId
			JOIN VEContentVersion C ON C.Id = V.ContentVersionId
	END
END

GO
PRINT 'Alter Post_GetPost'
GO
IF (OBJECT_ID('Post_GetPost') IS NOT NULL)
	DROP PROCEDURE Post_GetPost
GO
CREATE PROCEDURE [dbo].[Post_GetPost]
(  
 @ApplicationId  uniqueIdentifier,    
 @Id     uniqueIdentifier = null,    
 @PostMonth   varchar(20) = null,  
 @PostYear   varchar(20) = null,  
 @PageSize   int = null,  
 @PageNumber   int = null,  
 @FolderId   uniqueIdentifier = null,  
 @BlogId    uniqueIdentifier = null,  
 @Status    int = null,  
 @CategoryId   uniqueIdentifier = null,  
 @Label    nvarchar(1024) = null,  
 @Author    nvarchar(1024) = null,  
 @IncludeVersion  bit = null,  
 @VersionId   uniqueidentifier=null,  
 @IgnoreSite   bit = null,  
 @ShowOnlyApprovedComments bit = null  
)  
AS  
BEGIN  
 DECLARE @IsSticky bit  
 IF @Status = 5  
  SET @IsSticky = 1  
  
 IF @ShowOnlyApprovedComments IS NULL  
  SET @ShowOnlyApprovedComments = 0  
  
  
 DECLARE @tbPostIds TABLE(RowNo int, TotalRows int, Id uniqueidentifier, BlogId uniqueidentifier, PostContentId uniqueidentifier)  
 ;WITH PostIds AS(  
  SELECT ROW_NUMBER() over (order by IsSticky desc, PostDate desc, Title) AS RowNo,   
   COUNT(Id) over (PARTITION BY NULL) AS TotalRows,Id, BlogId, PostContentId  
  FROM BLPost   
  WHERE (@Id IS NULL OR Id = @Id) AND  
   (Status != dbo.GetDeleteStatus()) AND  
   (@ApplicationId IS NULL OR @IgnoreSite = 1 OR @ApplicationId = ApplicationId) AND  
   (@Status IS NULL OR Status = @Status) AND  
   (@PostYear IS NULL OR YEAR(PostDate) = @PostYear) AND   
   (@PostMonth IS NULL OR MONTH(PostDate) = @PostMonth) AND   
   (@FolderId IS NULL OR HSId = @FolderId) AND   
   (@BlogId IS NULL OR BlogId = @BlogId) AND  
   (@IsSticky IS NULL OR IsSticky = @IsSticky) AND  
   (@Label IS NULL OR Labels LIKE '%' + @Label + '%') AND  
   (@Author IS NULL OR AuthorEmail like @Author OR AuthorName like @Author) AND  
   (@CategoryId IS NULL OR Id IN (SELECT ObjectId FROM COTaxonomyObject WHERE TaxonomyId = @CategoryId AND ObjectTypeId = 40))  
 )  
   
 INSERT INTO @tbPostIds  
 SELECT RowNo, TotalRows, Id, BlogId, PostContentId FROM PostIds  
 WHERE (@PageSize IS NULL OR @PageNumber IS NULL OR   
  RowNo BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize))  
   
 SELECT A.Id,     
  A.ApplicationId,  
  A.Title,  
  (CASE WHEN @Id IS NULL THEN '' ELSE A.Description END) Description,  
  A.ShortDescription,  
  A.AuthorName,  
  A.AuthorEmail,  
  A.Labels,  
  A.AllowComments,  
  CONVERT(DATE, A.PostDate) PostDate,   
  A.IsScheduled,
  A.ScheduledPublishDate,  
  A.UseLocalTimeZone,
  A.ScheduledTimeZone,
  A.Location,  
  A.Status,  
  A.IsSticky,  
  A.CreatedBy,  
  A.CreatedDate,  
  A.ModifiedBy,  
  A.ModifiedDate,  
  A.FriendlyName,  
  A.PostContentId,  
  A.ContentId,  
  A.ImageUrl,  
  A.TitleTag,  
  A.H1Tag,  
  A.KeywordMetaData,  
  A.DescriptiveMetaData,  
  A.OtherMetaData,  
  B.BlogListPageId,  
  B.Id AS BlogId,  
  A.WorkflowState,  
  A.PublishDate,  
  A.PublishCount,  
  A.INWFFriendlyName,  
  A.HSId AS MonthId,
  ISNULL(A.ImageAltText,'') ImageAltText,
  ISNULL(A.ImageName,'') ImageName
 FROM BLPost A   
  JOIN @tbPostIds T ON A.Id = T.Id  
  JOIN BLBlog B ON A.BlogId = B.Id  
 ORDER BY T.RowNo  
  
 SELECT  C.Id,  
  C.ApplicationId,  
  C.Title,  
  C.Description,   
  C.Text,   
  C.URL,  
  C.URLType,   
  C.ObjectTypeId,   
  C.CreatedDate,  
  C.CreatedBy,  
  C.ModifiedBy,  
  C.ModifiedDate,   
  C.Status,  
  C.RestoreDate,   
  C.BackupDate,   
  C.StatusChangedDate,  
  C.PublishDate,   
  C.ArchiveDate,   
  C.XMLString,  
  C.BinaryObject,   
  C.Keywords,  
  FN.UserFullName CreatedByFullName,  
  MN.UserFullName ModifiedByFullName,  
  C.OrderNo, C.SourceContentId,  
  C.ParentId,  
  sDir.VirtualPath As FolderPath  
 FROM COContent C  
  INNER JOIN @tbPostIds T ON C.Id = T.PostContentId  
  INNER JOIN COContentStructure sDir on SDir.Id = C.ParentId  
  LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy  
  LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy  
    
 SELECT Top 1 TotalRows FROM @tbPostIds  
  
 SELECT A.Id,  
  ApplicationId,  
  Title,  
  Description,  
  BlogNodeType,  
  MenuId,  
  ContentDefinitionId,  
  ShowPostDate,  
  DisplayAuthorName,  
  EmailBlogOwner,  
  ApproveComments,  
  RequiresUserInfo,  
  Status,  
  CreatedDate,  
  CreatedBy,  
  ModifiedBy,  
  FN.UserFullName CreatedByFullName,  
  MN.UserFullName ModifiedByFullName,  
  ModifiedDate,  
  BlogListPageId,  
  BlogDetailPageId,  
  ContentDefinitionId--,  
  --CASE WHEN SUBSTRING(V.Url,1,1) = '/' THEN SUBSTRING(V.Url,2,LEN(V.Url)-1) ELSE V.Url END AS BlogUrl     
 FROM BLBlog A   
  INNER JOIN @tbPostIds T ON T.BlogId = A.Id  
		--LEFT JOIN vwBlogUrl V ON V.Id = A.Id
  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
  LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
   
 SELECT T.Id,  
  I.Title,  
  C.TaxonomyId  
 FROM COTaxonomyObject C  
  JOIN @tbPostIds T ON T.Id = C.ObjectId  
  JOIN COTaxonomy I ON I.Id = C.TaxonomyId  
  
 IF (@ShowOnlyApprovedComments = 1)  
 BEGIN  
  SELECT Count(*) AS NoOfComments,  
   T.Id  
  FROM BLComments C  
   JOIN @tbPostIds T ON C.ObjectId = T.Id  
  WHERE C.Status = 4 --For approved comments  
  GROUP BY T.Id  
 END  
 ELSE  
 BEGIN  
  SELECT Count(*) AS NoOfComments,  
   T.Id  
  FROM BLComments C  
   JOIN @tbPostIds T ON C.ObjectId = T.Id  
  GROUP BY T.Id  
 END  
  
 IF @IncludeVersion = 1  
 BEGIN  
  DECLARE @tbVersionIds TABLE(Id uniqueidentifier)  
  IF @VersionId IS NULL  
  BEGIN  
   IF(@Id IS NOT NULL)  
   BEGIN  
    SELECT TOP (1) @VersionId = Id  from VEVersion A  
    WHERE ObjectId=@Id   
    ORDER BY RevisionNumber DESC, VersionNumber DESC  
  
     
    INSERT INTO @tbVersionIds VALUES (@VersionId)  
   END  
   ELSE  
   BEGIN  
    ;WITH CTEVersion AS (  
     SELECT ROW_NUMBER() OVER (PARTITION BY T.Id ORDER BY RevisionNumber DESC, VersionNumber DESC) AS VerRank,  
      V.Id FROM VEVersion V  
      JOIN BLPost T On T.Id = V.ObjectId  
    )  
  
    INSERT INTO @tbVersionIds  
    SELECT Id FROM CTEVersion WHERE VerRank = 1  
   END  
  END  
  ELSE  
  BEGIN  
   INSERT INTO @tbVersionIds VALUES (@VersionId)  
  END  
  
  SELECT V.Id AS VersionId,  
   V.ObjectId AS PostId,  
   V.XMLString  
  FROM VEVersion V  
   JOIN @tbVersionIds T On T.Id = V.Id  
  
  SELECT   
   V.PostVersionId AS VersionId,  
   C.ContentId,  
   C.XMLString  
  FROM VEPostContentVersion V  
   JOIN @tbVersionIds T On T.Id = V.PostVersionId  
   JOIN VEContentVersion C ON C.Id = V.ContentVersionId  
 END  
END  
PRINT 'Alter Post_Save'
GO
IF (OBJECT_ID('Post_Save') IS NOT NULL)
	DROP PROCEDURE Post_Save
GO
CREATE PROCEDURE [dbo].[Post_Save]   
(  
	@Id						uniqueidentifier = null OUT ,  
	@BlogId					uniqueidentifier,   
	@ApplicationId			uniqueidentifier,   
	@Title					nvarchar(256) = null,  
	@Description			ntext = null,  
	@ShortDescription		nvarchar(2000) = null,  
	@AuthorName				nvarchar(256) = null,  
	@AuthorEmail			nvarchar(256) = null,  
	@AllowComments			bit = true,  
	@PostDate				datetime = null,  
	@IsScheduled			bit = null,
	@ScheduledPublishDate	datetime = null,  
	@UseLocalTimeZone		bit = null,  
	@ScheduledTimeZone		nvarchar(100) = null,  
	@Location				nvarchar(256) = null,  
	@Labels					nvarchar(Max) = null,  
	@ModifiedBy				uniqueidentifier,  
	@IsSticky				bit = null, 
	@HSId					uniqueidentifier,  
	@FriendlyName			nvarchar(256) = null,
	@PostContentId			uniqueidentifier = null,
	@ContentId				uniqueidentifier = null,
	@Imageurl				nvarchar(500) = null,
	@TitleTag				nvarchar(2000) = null,  
	@H1Tag					nvarchar(2000) = null,
	@KeywordMetaData		nvarchar(2000) = null,
	@DescriptiveMetaData	nvarchar(2000) = null,
	@OtherMetaData			nvarchar(2000) = null,
	@PublishDate			datetime = null,
	@PublishCount			int = null,
	@WorkflowState			int = null,
	@Status					int = null
)  
AS  
BEGIN  
	DECLARE  
	 @NewId  uniqueidentifier,  
	 @RowCount  INT,  
	 @Stmt   VARCHAR(25),  
	 @Now    datetime,  
	 @Error   int ,  
	 @MonthId uniqueidentifier,  
	 @return_value int --,
	 --@Status int 
  
	 /* if ID specified, ensure exists */  
	 IF (@Id IS NOT NULL)  
	  IF (NOT EXISTS(SELECT 1 FROM   BLPost WHERE  Id = @Id AND Status <> dbo.GetDeleteStatus()))  
	  BEGIN  
	   set @Stmt = convert(varchar(36),@Id)  
	   RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )  
	   RETURN dbo.GetBusinessRuleErrorCode()  
	  END    

	  /*
	  Since we need the alt text and/or image name for the given post to have it for SEO purpose, this is being taken from the vwimage
	  This was not added directly on the Post Get as that would slow down the post retrieval. 
	  */
	  DECLARE @ImgAltText NVARCHAR(MAX), @ImgName NVARCHAR(MAX), @ImgUrlToCompare nvarchar(max)

	   IF(PATINDEX('%~%',@Imageurl) <> 1)
	   BEGIN
			SET @ImgUrlToCompare = CONCAT('~',@Imageurl)
	   END

	   SELECT TOP 1 @ImgAltText=AltText,@ImgName=FileName FROM vwImage where CONCAT(RelativePath,'/',FileName) = @ImgUrlToCompare AND ApplicationId=@ApplicationId

	  IF @Status = 5
	  BEGIN
		SET @Status = null 
		/*
		Sankar: If the @status = sticky then 
		FOR INSERT : There is no provision now to make the post as sticky at the time of creation. so in the insert we are making it to default to 1(draft).Even if we send a @Status = 5 then it will create DRAFT STICKY POST.
		FOR update : if the status is sent as sticky, then we need to maintain the same status for the post by updating the sticky bit to 1. So here I am making the @status = null
		*/
	  END
	   
	 SET @Now = getutcdate()  
	  
	 IF (@Id IS NULL)   -- new insert  
	 BEGIN  
	  SET @Stmt = 'Create Post'  
	  SELECT @NewId =newid()  
	  
	  INSERT INTO BLPost  WITH (ROWLOCK)(  
		 Id,  
		ApplicationId,  
		Title,  
		BlogId,  
		Description,  
		ShortDescription,  
		AuthorName,  
		AuthorEmail,  
		Labels,  
		AllowComments,  
		PostDate,  
		IsScheduled,
		ScheduledPublishDate,  
		UseLocalTimeZone,
		ScheduledTimeZone,
		Location,  
		Status,  
		IsSticky,
		CreatedBy,  
		CreatedDate,  
		HSId,  
		FriendlyName,
		PostContentId,
		ContentId,
		ImageUrl,
		TitleTag,
		H1Tag,
		KeywordMetaData,
		DescriptiveMetaData,
		OtherMetaData,
		ImageAltText,
		ImageName
	  )  
	  VALUES  
	   (  
			@NewId,  
			@ApplicationId,   
			@Title,  
			@BlogId,  
			@Description,  
			@ShortDescription,  
			@AuthorName,  
			@AuthorEmail,  
			@Labels,  
			@AllowComments,  
			@PostDate,  
			@IsScheduled,
			@ScheduledPublishDate,  
			@UseLocalTimeZone,  
			@ScheduledTimeZone,  
			@Location,  
			ISNULL(@Status,1), 
			@IsSticky, 
			@ModifiedBy,  
			@Now,
			@HSId ,
			@FriendlyName,
			@PostContentId,
			@ContentId,
			@ImageUrl,
			@TitleTag,
			@H1Tag,
			@KeywordMetaData,
			@DescriptiveMetaData,
			@OtherMetaData,
			@ImgAltText,
			@ImgName
	   )  
	  SELECT @Error = @@ERROR  
	   IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()   
	  END  
	  
	  SELECT @Id = @NewId  
	 END  
	 ELSE   -- Update  
	 BEGIN  
	  SET @Stmt = 'Modify Post'  
	  UPDATE BLPost WITH (ROWLOCK)  
	   SET   
		 Id     = @Id,  
		ApplicationId  = @ApplicationId,  
		Title    = @Title,  
		BlogId    = @BlogId,  
		Description   = @Description,  
		ShortDescription = @ShortDescription,  
		AuthorName   = @AuthorName,  
		AuthorEmail   = @AuthorEmail,  
		Labels    = @Labels,  
		AllowComments  = @AllowComments,  
		PostDate   = @PostDate,  
		IsScheduled = @IsScheduled,
		ScheduledPublishDate   = @ScheduledPublishDate,  
		UseLocalTimeZone	= @UseLocalTimeZone,  
		ScheduledTimeZone	= @ScheduledTimeZone,  
		Location   = @Location ,  
		ModifiedBy   = @ModifiedBy,  
		ModifiedDate  = GETUTCDATE(),  
		HSId = @HSId,  
		FriendlyName=@FriendlyName,
		PostContentId = @PostContentId,
		ContentId = @ContentId,
		ImageUrl = @Imageurl,
		TitleTag = @TitleTag,
		H1Tag = @H1Tag,
		IsSticky = @IsSticky,
		KeywordMetaData = @KeywordMetaData,
		DescriptiveMetaData = @DescriptiveMetaData,
		OtherMetaData = @OtherMetaData,
		PublishDate = @PublishDate,
		PublishCount = @PublishCount,
		WorkflowState = @WorkflowState,
		Status = ISNULL(@Status,Status),
		ImageAltText=@ImgAltText,
		ImageName=@ImgName
	  WHERE Id = @Id  
	    
	  SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT  
	  
	  IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()  
	  END  
	  
	  IF @RowCount = 0  
	  BEGIN  
	   --concurrency error  
	   RAISERROR('DBCONCURRENCY',16,1)  
	   RETURN dbo.GetDataConcurrencyErrorCode()  
	  END   
	 END    
   
	EXEC PageMapBase_UpdateLastModification @ApplicationId
END
GO
PRINT 'Create procedure AssetImage_SaveImage'
GO
IF (OBJECT_ID('AssetImage_SaveImage') IS NOT NULL)
	DROP PROCEDURE AssetImage_SaveImage
GO
CREATE PROCEDURE [dbo].[AssetImage_SaveImage]   
--********************************************************************************  
-- Parameters  
--********************************************************************************  
(  
 @Id  uniqueidentifier,  
 @FileName  nvarchar(256),  
 @FileSize  int = null,  
 @Extension  nvarchar(10),  
 @FolderName  nvarchar(256) = null,  
 @Attributes  nvarchar(256) = null,  
 @RelativePath  nvarchar(4000) = null,  
 @PhysicalPath  nvarchar(4000)  = null,  
 @MimeType nvarchar(256) = null,  
    @AltText nvarchar(1024) = null,  
    @FileIcon nvarchar(1024)= null,   
    @DescriptiveMetadata nvarchar(4000)= null,  
    @ImageHeight int = null,  
    @ImageWidth int = null  
)  
as  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE  
 @Stmt   VARCHAR(50),   
 @Error   int,  
 @ExcludeFromExternalSearch bit = 0  
--********************************************************************************  
-- code  
--********************************************************************************  
BEGIN  
  
 IF @RelativePath IS NULL OR @RelativePath = ''  
 BEGIN  
  DECLARE @ParentId uniqueidentifier  
  DECLARE @ObjectTypeId int  
  SELECT @ParentId = ParentId, @ObjectTypeId = ObjectTypeId FROM COContent WHERE Id = @Id  
    
  SET @RelativePath = dbo.GetVirtualPathByObjectType(@ParentId, @ObjectTypeId)  
 END  
   
 DECLARE @SiteId  UNIQUEIDENTIFIER,  
   @ObjectType INT  
     
 SELECT @SiteId = ApplicationId,  
   @ObjectType = ObjectTypeId  
 FROM COContent  
 WHERE Id = @Id  
  
    IF (EXISTS(SELECT 1 FROM COFile WHERE ContentId = @Id))  
 BEGIN     
  SET @Stmt = 'Update AssetImage'  
  UPDATE COFile WITH (ROWLOCK)  
  SET  [FileName] = @FileName,  
    FileSize = @FileSize,  
    Extension = @Extension,  
    FolderName = @FolderName,  
    Attributes = @Attributes,  
    RelativePath = @RelativePath,  
    PhysicalPath = @PhysicalPath,   
    MimeType = @MimeType,  
    AltText = @AltText,  
    FileIcon = @FileIcon,   
    DescriptiveMetadata = @DescriptiveMetadata,  
    ImageHeight = @ImageHeight,  
    ImageWidth = @ImageWidth,  
    ExcludeFromExternalSearch = @ExcludeFromExternalSearch,  
    [Url] = dbo.AssetFile_GetUrl(@SiteId, @ObjectType, @RelativePath, @FileName)  
  WHERE ContentId = @Id  

  --UPdate the Asset image's alt text to Post column
  ;WITH CTE AS(
	SELECT Id FROM BLPost WHERE CONCAT('~',Imageurl) = CONCAT(@RelativePath,'/',@FileName)
  )
  UPDATE P SET ImageAltText = @AltText, ImageName=@FileName 
  FROM BLPost P JOIN CTE C ON P.Id=C.Id

 END    
 ELSE  
 BEGIN    
  SET @Stmt = 'Create Asset Image'  
  INSERT INTO COFile(  
    ContentId,  
    [FileName],  
    FileSize,  
    Extension,  
    FolderName,  
    Attributes,  
    RelativePath,  
    PhysicalPath,   
    MimeType,  
    AltText,  
    FileIcon,   
    DescriptiveMetadata,  
    ImageHeight,  
    ImageWidth,  
    ExcludeFromExternalSearch,  
    [Url]  
  )  
  values  
  (  
   @Id,  
   @FileName,  
   @FileSize,  
   @Extension,  
   @FolderName,  
   @Attributes,  
   @RelativePath,  
   @PhysicalPath,  
   @MimeType,  
   @AltText,  
   @FileIcon,   
   @DescriptiveMetadata,  
   @ImageHeight,  
   @ImageWidth,  
   @ExcludeFromExternalSearch,  
   dbo.AssetFile_GetUrl(@SiteId, @ObjectType, @RelativePath, @FileName)  
  )  
 END  
  
 SELECT @Error = @@ERROR  
 IF @Error <> 0  
 BEGIN  
  RAISERROR('DBERROR||%s',16,1,@Stmt)  
  RETURN dbo.GetDataBaseErrorCode()  
 END  
   
END  
GO
PRINT 'Check and create OrderId type'
IF NOT EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name='OrderId')
CREATE TYPE [dbo].[OrderId] AS TABLE(
	[OrderId] [uniqueidentifier] NULL
)

GO
PRINT 'Create procedure Product_GetDetail'
GO
IF (OBJECT_ID('Product_GetDetail') IS NOT NULL)
	DROP PROCEDURE Product_GetDetail
GO
CREATE PROCEDURE [dbo].[Product_GetDetail]
(
	@ProductIds	OrderId ReadOnly, -- Re-using UDT OrderId. The actual values passed are productIds.
	@SiteId		uniqueidentifier
)
AS
BEGIN
		-- product
			SELECT P.[Id], P.[ProductIdUser], P.[Title], P.[IsActive], P.[PromoteAsNewItem], P.[PromoteAsTopSeller], P.[ExcludeFromDiscounts], 
				P.[ExcludeFromShippingPromotions], P.[Freight], P.[Refundable], P.[TaxExempt], P.ProductTypeId, 
				ISNULL(NULLIF(SEOFriendlyUrl,''), '/' + PTP.ProductTypePath + N'id-' + P.ProductIDUser + '/' + UrlFriendlyTitle) AS ProductUrl,
				P.[IsBundle], P.[UrlFriendlyTitle], P.[ShortDescription], P.[LongDescription], P.[Description], P.[Keyword], P.[ProductStyle], 
				P.[BundleCompositionLastModified], P.[TaxCategoryId], P.[SEOTitle], P.[SEOH1], P.[SEODescription], P.[SEOKeywords], P.[SEOFriendlyUrl], P.[Status], 
				P.[IsShared], P.[SiteId], P.[CreatedBy] AS [CreatedById], P.[CreatedDate], P.[ModifiedBy] AS [ModifiedById], P.[ModifiedDate]
			FROM [PRProduct] P 
			INNER JOIN vwProductTypePath PTP ON PTP.ProductTypeId = P.ProductTypeID
			INNER JOIN @ProductIds PC ON P.Id = PC.OrderId 
			Where P.SiteId=@SiteId

			--product type
			SELECT PT.[CreatedBy] AS [CreatedById], PT.[ModifiedBy] AS [ModifiedById], PT.[Id], [ParentId], PT.[Title], PT.[Description], [FriendlyName], PT.[SiteId], PT.[CreatedDate], PT.[ModifiedDate],PT.TaxCategoryId,CMSPageId,P.Id ProductId
			FROM [vwProductTypeList] PT
			INNER JOIN [PRProduct] P ON P.ProductTypeID=PT.Id AND PT.SiteId = P.SiteId
			INNER JOIN @ProductIds PC ON P.Id = PC.OrderId 
			WHERE   PT.[SiteId] = @SiteId

			-- tags
			SELECT DISTINCT  OT.[CreatedBy] AS [CreatedById], OT.[ModifiedBy] AS [ModifiedById], OT.[Id], [ParentId], OT.[Title], OT.[Description], [DisplayOrder], OT.[Status], OT.[CreatedDate], 
				OT.[ModifiedDate], OT.[CreatedByFullName], OT.[ModifiedByFullName] 
			FROM [vwTagObject] OT
			INNER JOIN PRProduct P ON OT.ObjectId = P.Id
			INNER JOIN @ProductIds PC ON P.Id = PC.OrderId 
			Where OT.SiteId = @SiteId
					
			-- product nav
			;With queryCTE(QueryId,ProductId) 
			AS
			(Select Distinct QueryId,ObjectId ProductId  from NVFilterOutput FO INNER JOIN @ProductIds P ON FO.ObjectId=P.OrderId)

			SELECT DISTINCT PageMapNodeId NavigationCategoryId,P.DisplayTitle NavigationCategoryTitle, P.ExcludeFromSiteMap Excluded,CTE.ProductId, SiteId 
			FROM PageMapNode	P 
			INNER JOIN NVNavNodeNavFilterMap M ON P.PageMapNodeId=M.NavNodeId
			INNER JOIN queryCTE CTE ON M.QueryId = CTE.QueryId
			WHERE  SiteId = @SiteId

			-- product image
			SELECT [CreatedBy] AS [CreatedById], [ModifiedBy] AS [ModifiedById], 
				[Id], [Title], [Description], [FileName], [ProductName], [ProductTypeId], [RelativePath], [Size], [Sequence], 
				[AltText], [TypeNumber], [Status], [ParentImage], [SiteId], [ProductId], [CreatedDate], [ModifiedDate], [CreatedByFullName], 
				[ModifiedByFullName]  
			FROM   [vwProductImage]  I
			INNER JOIN @ProductIds P ON I.ProductId = P.OrderId AND TypeNumber=1
			Where I.SiteId=@SiteId

			-- product attribute
			SELECT PA.[CreatedBy] AS [CreatedById], [AttributeTitle], PA.[Id], [ObjectId], [AttributeId], [AttributeEnumId], [Value], PA.[CreatedDate] ,PA.ObjectId ProductId, A.AttributeDataType,A.IsSearched AttributeSearchable
			FROM [vwProductAttributeValue] PA
			INNER JOIN ATAttribute A ON PA.AttributeId = A.Id
			INNER JOIN @ProductIds PC ON PA.ObjectId= PC.OrderId 
			WHERE PA.SiteId = @SiteId

			-- SKU
			SELECT [CreatedBy] AS [CreatedById], [ModifiedBy] AS [ModifiedById], [SourceSiteId] AS [SiteId], 
				[ProductName], [ProductTypeId], [Quantity], [AvailableToSell], [IsSellable], [IsDownloadableMedia], 
				[Id], [Title], [Description], [Sequence], [SKU], [IsActive], [IsOnline], [ListPrice], [UnitCost], [WholesalePrice], 
				[PreviousSoldCount], [Measure], [OrderMinimum], [OrderIncrement], [Length], [Height], [Width], [Weight], 
				[SelfShippable], [AvailableForBackOrder], [BackOrderLimit], [MaximumDiscountPercentage], [IsUnlimitedQuantity], 
				[FreeShipping], [UserDefinedPrice], [HandlingCharges], [Status], [ProductId], [CreatedDate], [ModifiedDate] 
			FROM [vwSku] S
			INNER JOIN @ProductIds PC ON S.ProductId= PC.OrderId 
			WHERE [SiteId] = @SiteId 
			

			--sku image
			SELECT I.[CreatedBy] AS [CreatedById], I.[ModifiedBy] AS [ModifiedById], 
				I.[Id], I.[Title], I.[Description], [FileName], [ProductName], [ProductTypeId], [RelativePath], [Size], I.[Sequence], 
				[AltText], [TypeNumber], I.[Status], [ParentImage], I.[SiteId], I.[ProductId], I.[CreatedDate], I.[ModifiedDate], [CreatedByFullName], 
				[ModifiedByFullName] , SI.Id ProductId
			from vwProductImageSku SI
			INNER JOIN vwProductImage I ON I.Id = SI.ImageId AND SI.SiteId=I.SiteId
			INNER JOIN @ProductIds P ON I.ProductId= P.OrderId AND TypeNumber=1
			Where TypeNumber=1
			AND SI.SiteId=@SiteId AND I.Status=1

			--sku attribute
			SELECT SA.[CreatedBy] AS [CreatedById], [AttributeTitle], SA.[Id], [ObjectId], [AttributeId], [AttributeEnumId], [Value], SA.[CreatedDate] ,SA.ObjectId ProductId,A.AttributeDataType,A.IsSearched AttributeSearchable
			FROM [vwSkuAttributeValue] SA
			INNER JOIN ATAttribute A ON SA.AttributeId = A.Id
			INNER JOIN PRProductSKU S ON SA.ObjectId = S.Id
			INNER JOIN @ProductIds PC ON S.ProductId= PC.OrderId 
			WHERE SA.SiteId = @SiteId


			-- Min price

			Declare @Today DateTime
			SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@SiteId)) as datetime)
			Declare @DefaultGroupId uniqueidentifier
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId')

			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId')

				Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale,  ISNULL(PST.EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY S.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @ProductIds IQ
				INNER JOIN PRProductSKU S on IQ.OrderId= S.ProductId
				LEFT JOIN PRProductSKUVariantCache SV on S.Id =SV.Id and SV.SiteId=@SiteId
				LEFT JOIN (SELECT PSC.ProductId, PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					Where CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @SiteId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.ProductId= IQ.OrderId AND 1 between minQuantity and maxQuantity
			) EFPS
			WHERE EFPS.Sno=1	
END
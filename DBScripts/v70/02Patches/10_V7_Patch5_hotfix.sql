IF NOT EXISTS (Select 1 from NVFilterQuery Where ActualQuery like 'SELECT FilterId, Id,ROW_NUMBER()%' )
BEGIN
Declare @sqlQuery varchar(max) 
Declare @Id uniqueidentifier
Declare @Condition			varchar(max),
	@OrderByProperty	varchar(50),
	@SortOrder			varchar(10),
	@MaxNoOfRecord		int,
	@ApplicationId uniqueidentifier

DECLARE cursor_FilterQuery CURSOR
    FOR SELECT Id,LogicalCondition,OrderByProperty,SortOrder,MaxNoOfRecord,SiteId from NVFilterQuery;

OPEN cursor_FilterQuery;
FETCH NEXT FROM cursor_FilterQuery INTO 
    @Id,@Condition,@OrderByProperty,@SortOrder,@MaxNoOfRecord,@Applicationid;

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @sqlQuery = [dbo].[NavFilter_GetQuery](@Id,@Condition,@OrderByProperty,@SortOrder,@ApplicationId)
	--PRINT @sqlQuery 

	IF(@MaxNoOfRecord >-1)
		SET @sqlQuery = 'Select Top '+  cast(@MaxNoOfRecord as nvarchar(20)) + ' ' + substring(@sqlQuery,7,len(@sqlQuery)-6)
		
	UPDATE NVFilterQuery Set ActualQuery = @sqlQuery,IsLatest=0 Where Id =@Id
	
 FETCH NEXT FROM cursor_FilterQuery INTO 
            @Id,@Condition,@OrderByProperty,@SortOrder,@MaxNoOfRecord,@Applicationid;
END
CLOSE cursor_FilterQuery;
DEALLOCATE cursor_FilterQuery;
END

GO





IF (OBJECT_ID('vwSKU') IS NOT NULL)
	DROP View vwSKU	
GO
CREATE VIEW [dbo].[vwSKU]
AS
WITH CTE AS
(
	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		ISNULL(P.ModifiedDate, P.CreatedDate) AS ModifiedDate, 
		ISNULL(P.ModifiedBy, P.CreatedBy) AS  ModifiedBy
	FROM PRProductSku P

	UNION ALL

	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		P.ModifiedDate, 
		P.ModifiedBy
	FROM PRProductSKUVariantCache P
)

SELECT	C.Id, SK.ProductId, C.SiteId, SK.SiteId AS SourceSiteId, C.Title, C.[Description], SK.SKU, C.IsActive, C.IsOnline, C.UnitCost,
		C.WholesalePrice, C.PreviousSoldCount, C.Measure, C.OrderMinimum, C.OrderIncrement, C.HandlingCharges,
		SK.Height, SK.Width, SK.[Length], SK.[Weight], C.SelfShippable, C.AvailableForBackOrder, C.BackOrderLimit, C.IsUnlimitedQuantity,
		C.MaximumDiscountPercentage, C.FreeShipping, C.UserDefinedPrice, C.[Sequence], SK.[Status],
		C.ListPrice, 
		ISNULL(ATS.Inventory, 0) AS Quantity, 
		CASE WHEN P.IsBundle = 0 THEN ISNULL(ATS.Quantity, 0) ELSE dbo.GetBundleQuantity(SK.Id) END AS AvailableToSell,
		SK.CreatedDate, SK.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		C.ModifiedDate, C.ModifiedBy, MFN.UserFullName AS ModifiedByFullName,
		ISNULL(PC.Title, P.Title) AS ProductName, P.ProductTypeId, PT.IsDownloadableMedia,
		case when ISNULL(PC.IsActive,P.IsActive)=1 then C.IsActive else 0 end IsSellable
FROM	CTE AS C
		INNER JOIN PRProductSku AS SK ON SK.Id = C.Id
		INNER JOIN PRProduct AS P ON P.Id = SK.ProductId
		LEFT JOIN PRProductVariantCache AS PC ON PC.Id = SK.ProductId AND PC.SiteId = C.SiteId
		INNER JOIN PRProductType AS PT ON PT.Id = P.ProductTypeID
		LEFT JOIN vwAvailableToSellPerSite AS ATS ON ATS.SKUId = SK.Id AND ATS.SiteId = SK.SiteId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = SK.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = SK.ModifiedBy
GO

PRINT 'Modify stored procedure PaymentDto_Get'
GO
IF(OBJECT_ID('PaymentDto_Get') IS NOT NULL)
	DROP PROCEDURE PaymentDto_Get
GO
CREATE PROCEDURE [dbo].[PaymentDto_Get]
(		
	@Id						UNIQUEIDENTIFIER = NULL,
	@OrderId				UNIQUEIDENTIFIER = NULL,
	@CustomerId				UNIQUEIDENTIFIER = NULL,
	@ProcessorTransactionId	NVARCHAR(256) = NULL,
	@PaymentTypeId			INT = NULL,
	@PaymentStatusId		INT = NULL,
	@IsRefund				BIT = NULL,
	@StartDate				DATETIME = NULL,
	@EndDate				DATETIME = NULL,
	@PageNumber				INT = NULL,
	@PageSize				INT = NULL,
	@SiteId					UNIQUEIDENTIFIER = NULL,
	@TotalRecords			INT = NULL OUTPUT
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	DECLARE	@StartRow	INT,
			@EndRow		INT

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END

	SELECT	P.Id, OP.Id AS OrderPaymentId, OP.OrderId, OP.[Sequence], P.PaymentTypeId AS PaymentType, P.PaymentStatusId AS PaymentStatus, P.ProcessorTransactionId,P.AuthCode,
			P.IsRefund, RE.Comments AS RefundReason, P.Amount, P.CapturedAmount, COALESCE(PR.AllocatedRefundAmount, 0) AS AllocatedRefundAmount,
			O.PurchaseOrderNumber, P.CustomerId, CASE WHEN LEN(LTRIM(C.LastName)) = 0 THEN ''  ELSE C.LastName + ', ' END + C.FirstName AS CustomerName,
			CCT.Title AS CreditCardType, CCI.NameOnCard AS CreditCardName,
			CONVERT(VARCHAR(MAX), DecryptByKey(CCI.Number)) AS CreditCardNumber,
			CAST(CONVERT(VARCHAR(2), DecryptByKey(CCI.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,
			CAST(CONVERT(VARCHAR(4), DecryptByKey(CCI.ExpirationYear)) AS INT) AS CreditCardExpirationYear,
			PCC.ExternalProfileId CreditCardExternalProfileId,
			PCC.ExternalProfileInfo,
			PCC.IsValidated,
			GCI.GiftCardNumber AS GiftCardNumber, GCI.BalanceAfterLastTransaction AS GiftCardBalance, U.AccountNumber,P.AdditionalInfo,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.BillingAddressId
				WHEN 2 THEN LOC.BillingAddressId
				WHEN 3 THEN PPI.BillingAddressId
				WHEN 4 THEN CODI.BillingAddressId
				WHEN 5 THEN GCI.BillingAddressId
			END AS BillingAddressId,
			CASE P.PaymentTypeId
				WHEN 1 THEN CCI.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentInfoId,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentTypeInfoId,
			P.CreatedDate, P.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName, P.ModifiedDate, P.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName,
			ROW_NUMBER() OVER (ORDER BY P.CreatedDate Desc) AS RowNumber
	INTO	#OrderPayments
	FROM	PTOrderPayment AS OP
			INNER JOIN PTPayment AS P ON P.Id = OP.PaymentId
			INNER JOIN OROrder AS O ON O.Id = OP.OrderId AND (@SiteId IS NULL OR O.SiteId = @SiteId)
			INNER JOIN vwCustomer AS C ON C.Id = P.CustomerId
			LEFT JOIN PTRefundPayment AS RP ON RP.RefundOrderPaymentId = OP.Id
			LEFT JOIN ORRefund AS RE ON RE.Id = RP.RefundId
			LEFT JOIN (
				SELECT		RP.OrderPaymentId, SUM(RP.RefundAmount) AS AllocatedRefundAmount
				FROM		PTRefundPayment AS RP
							INNER JOIN PTOrderPayment AS ROP ON ROP.Id = RP.RefundOrderPaymentId
							INNER JOIN PTPayment AS P ON P.Id = ROP.PaymentId
				WHERE		P.PaymentStatusId NOT IN (7, 14)
				GROUP BY	RP.OrderPaymentId
			) AS PR ON PR.OrderPaymentId = OP.Id
			LEFT JOIN PTPaymentCreditCard AS PCC ON P.PaymentTypeId = 1 AND PCC.PaymentId = P.Id
			LEFT JOIN PTPaymentInfo AS CCI ON P.PaymentTypeId = 1 AND CCI.Id = PCC.PaymentInfoId
			LEFT JOIN PTCreditCardType AS CCT ON P.PaymentTypeId = 1 AND CCT.Id = CCI.CardType
			LEFT JOIN PTPaymentLineOfCredit AS PLOC ON P.PaymentTypeId = 2 AND PLOC.PaymentId = P.Id
			LEFT JOIN PTLineOfCreditInfo AS LOC ON P.PaymentTypeId = 2 AND LOC.Id = PLOC.LOCId
			LEFT JOIN USCommerceUserProfile U ON P.PaymentTypeId = 2 AND U.Id = LOC.CustomerId
			LEFT JOIN PTPaymentPaypal AS PPP ON P.PaymentTypeId = 3 AND PPP.PaymentId = P.Id
			LEFT JOIN PTPaypalInfo AS PPI ON P.PaymentTypeId = 3 AND PPI.Id = PPP.PaypalInfoId
			LEFT JOIN PTPaymentCOD AS PCOD ON P.PaymentTypeId = 4 AND PCOD.PaymentId = P.Id
			LEFT JOIN PTCODInfo AS CODI ON P.PaymentTypeId = 4 AND CODI.Id = PCOD.CODId
			LEFT JOIN PTPaymentGiftCard AS PGC ON P.PaymentTypeId = 5 AND PGC.PaymentId = P.Id
			LEFT JOIN PTGiftCardInfo AS GCI ON P.PaymentTypeId = 5 AND GCI.Id = PGC.GiftCardId
			LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy
			LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = P.ModifiedBy
	WHERE	(@Id IS NULL OR P.Id = @Id) AND
			(@OrderId IS NULL OR OP.OrderId = @OrderId) AND
			(@CustomerId IS NULL OR P.CustomerId = @CustomerId) AND
			(@ProcessorTransactionId IS NULL OR P.ProcessorTransactionId = @ProcessorTransactionId) AND
			(@PaymentTypeId IS NULL OR P.PaymentTypeId = @PaymentTypeId) AND
			(@PaymentStatusId IS NULL OR P.PaymentStatusId = @PaymentStatusId) AND
			(@IsRefund IS NULL OR P.IsRefund = @IsRefund) AND
			(@StartDate IS NULL OR P.CreatedDate >= @StartDate) AND
			(@EndDate IS NULL OR P.CreatedDate <= @EndDate) AND
			(@SiteId IS NULL OR O.SiteId = @SiteId)
	ORDER BY P.CreatedDate DESC
	OPTION (RECOMPILE)

	SELECT	*
	FROM	#OrderPayments
	WHERE	@StartRow IS NULL OR
			@EndRow IS NULL OR 
			RowNumber BETWEEN @StartRow AND @EndRow

	SET @TotalRecords = (
		SELECT	COUNT(Id)
		FROM	#OrderPayments
	)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO

PRINT 'Modify stored procedure BundleItem_Get'
GO
IF(OBJECT_ID('BundleItem_Get') IS NOT NULL)
	DROP PROCEDURE BundleItem_Get
GO
CREATE PROCEDURE [dbo].[BundleItem_Get](  
--********************************************************************************  
-- Parameters  
--********************************************************************************  
   @Id uniqueidentifier = null  
   ,@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
 SELECT s.[Id]  
      ,s.[ProductId]  
      ,s.[SiteId]  
      ,s.[SKU]  
	  ,s.[Id] As SkuId
	  ,s.[Title]  
      ,s.[Description]  
      ,s.[Sequence]  
      ,s.[IsActive]  
      ,s.[IsOnline]  
      ,s.[ListPrice]  
      ,s.[UnitCost]  
      ,s.[PreviousSoldCount]  
   ,s.[CreatedBy]  
   ,s.[CreatedDate]
   ,s.[ModifiedBy]  
   ,s.ModifiedDate
   ,s.[Status]  
   ,s.[Measure]  
   ,s.[OrderMinimum]  
   ,s.[OrderIncrement]  
   ,s.[Length]
	  ,s.[Height]
	  ,s.[Width]
	  ,s.[Weight]
	  ,s.[SelfShippable]  
	  ,s.AvailableForBackOrder
	  ,s.MaximumDiscountPercentage
	  ,s.BackOrderLimit
	,s.IsUnlimitedQuantity
	,s.FreeShipping
	,s.UserDefinedPrice
   ,bi.Id BundleItemId  
   ,bi.[BundleProductId]  
   ,bi.[ChildSkuId]  
   ,bi.[ChildSkuQuantity]  
   ,bi.[CreatedBy] as [BundleSkuCreatedBy]  
   ,dbo.ConvertTimeFromUtc(bi.[CreatedDate],@ApplicationId) as [BundleSkuCreatedDate]  
   ,bi.[ModifiedBy] as [BundleSkuModifiedBy]  
   ,dbo.ConvertTimeFromUtc(bi.[ModifiedDate] ,@ApplicationId)as [BundleSkuModifiedDate],
   (SELECT sum(Quantity) From vwAllocatedQunatity Where ProductSKUId = s.[Id] ) as AllocatedQuantity,
   (SELECT sum(Quantity) From INInventory Where SKUId = s.[Id] ) as QuantityOnHand    --- sum of all warehouse -- find bundle item wise minimum in API or helper
 FROM   PRBundleItem bi left join PRProductSKU s on bi.[ChildSkuId]=s.[Id] 
 WHERE  
  bi.[Id]=@Id  
  AND  
  Status = dbo.GetActiveStatus() -- Select only Active Attributes  
 Order By bi.[Id]  
END

GO
GO

IF (OBJECT_ID('vwOrderItemAttributeValue') IS NOT NULL)
	DROP View vwOrderItemAttributeValue	
GO
CREATE VIEW [dbo].[vwOrderItemAttributeValue]
AS
SELECT V.OrderItemAttributeValueId AS Id, 
	O.SiteId,
	V.OrderItemId AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	A.Title AttributeTitle,
	V.Value,
	V.CreatedBy,
	V.CreatedDate
FROM OROrderItemAttributeValue V
	JOIN OROrder O ON O.Id = V.OrderId
	JOIN ATAttribute A ON A.Id = V.AttributeId
GO
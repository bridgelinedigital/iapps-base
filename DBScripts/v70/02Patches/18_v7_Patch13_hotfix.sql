PRINT 'Create procedure PageMapBase_UpdateLastModification'
GO
IF (OBJECT_ID('PageMapBase_UpdateLastModification') IS NOT NULL)
	DROP PROCEDURE PageMapBase_UpdateLastModification
GO
CREATE PROCEDURE [dbo].[PageMapBase_UpdateLastModification]
(
	@SiteId			uniqueidentifier = null
)
AS
BEGIN
	DECLARE @ModifiedDate datetime
	SET @ModifiedDate =  DATEADD(SECOND, DATEDIFF(SECOND, 39000, GETUTCDATE()), 39000)

	IF @SiteId IS NOT NULL
	BEGIN
		DECLARE @LatestPageMapDate datetime, @LastCacheDate datetime

		SELECT TOP 1 @LatestPageMapDate = ModifiedDate FROM PageMapNode
		WHERE SiteId = @SiteId
		ORDER BY ModifiedDate DESC

		SELECT @LastCacheDate = LastPageMapModificationDate FROM PageMapBase
		WHERE SiteId = @SiteId

		IF @LatestPageMapDate >= @LastCacheDate
			UPDATE PageMapNode SET CompleteFriendlyUrl = CalculatedUrl
			WHERE SiteId = @SiteId
	END

	IF EXISTS (SELECT 1 FROM PageMapNode WHERE MasterPageMapNodeId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageMapNodeId = S.SourcePageMapNodeId 
		FROM PageMapNode P
			JOIN vwMasterSourceMenuId S ON S.PageMapNodeId = P.PageMapNodeId
		WHERE P.MasterPageMapNodeId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageMapNode SET MasterPageMapNodeId = PageMapNodeId 
		WHERE MasterPageMapNodeId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	IF EXISTS (SELECT 1 FROM PageDefinition WHERE MasterPageDefinitionId IS NULL)
	BEGIN
		UPDATE P SET P.MasterPageDefinitionId = S.SourcePageDefinitionId 
		FROM PageDefinition P
			JOIN vwMasterSourcePageId S ON S.PageDefinitionId = P.PageDefinitionId
		WHERE P.MasterPageDefinitionId IS NULL
			AND (@SiteId IS NULL OR P.SiteId = @SiteId)

		UPDATE PageDefinition SET MasterPageDefinitionId = PageDefinitionId 
		WHERE MasterPageDefinitionId IS NULL AND (@SiteId IS NULL OR SiteId = @SiteId)
	END

	UPDATE PageMapBase SET LastPageMapModificationDate = @ModifiedDate
	WHERE @SiteId IS NULL OR SiteId = @SiteId
END
GO

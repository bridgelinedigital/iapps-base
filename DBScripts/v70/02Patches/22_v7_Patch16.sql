PRINT 'Creating Function Menu_GetVariantId'
GO
IF (OBJECT_ID('Menu_GetVariantId') IS NOT NULL)
	DROP FUNCTION Menu_GetVariantId
GO
CREATE FUNCTION [dbo].[Menu_GetVariantId]
(
	@SourceMenuId	uniqueidentifier,
	@TargetSiteId	uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @MenuId uniqueidentifier, @MasterMenuId uniqueidentifier, @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF EXISTS(SELECT 1 FROM PageMapNode WHERE PageMapNodeId = SiteId AND PageMapNodeId = @SourceMenuId)
	BEGIN
		SET @MenuId = @TargetSiteId
	END
	ELSE
	BEGIN
		SELECT TOP 1 @MenuId = PageMapNodeId FROM PageMapNode
		WHERE SourcePageMapNodeId = @SourceMenuId AND SiteId = @TargetSiteId

		IF @MenuId IS NULL
		BEGIN
			SELECT TOP 1 @MasterMenuId = MasterPageMapNodeId FROM PageMapNode
			WHERE PageMapNodeId = @SourceMenuId

			SELECT TOP 1 @MenuId = PageMapNodeId FROM PageMapNode
			WHERE MasterPageMapNodeId = @MasterMenuId AND SiteId = @TargetSiteId
		END
	END

	RETURN @MenuId
END
GO
PRINT 'Creating Function Page_GetVariantId'
GO
IF (OBJECT_ID('Page_GetVariantId') IS NOT NULL)
	DROP FUNCTION Page_GetVariantId
GO
CREATE FUNCTION [dbo].[Page_GetVariantId]
(
	@SourcePageId	uniqueidentifier,
	@TargetSiteId	uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @PageId uniqueidentifier, @MasterPageId uniqueidentifier, @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	SELECT TOP 1 @PageId = PageDefinitionId FROM PageDefinition
	WHERE SourcePageDefinitionId = @SourcePageId AND SiteId = @TargetSiteId

	IF @PageId IS NULL
	BEGIN
		SELECT TOP 1 @MasterPageId = MasterPageDefinitionId FROM PageDefinition
		WHERE PageDefinitionId = @SourcePageId

		SELECT TOP 1 @PageId = PageDefinitionId FROM PageDefinition
		WHERE MasterPageDefinitionId = @MasterPageId AND SiteId = @TargetSiteId
	END

	RETURN @PageId
END
GO
PRINT 'Create procedure DistributionRunItemDto_Fill'
GO
IF (OBJECT_ID('DistributionRunItemDto_Fill') IS NOT NULL)
	DROP PROCEDURE DistributionRunItemDto_Fill
GO
CREATE PROCEDURE [dbo].[DistributionRunItemDto_Fill]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tbSiteIds TABLE(Id uniqueidentifier)
	DECLARE @DistributionRunId uniqueidentifier, @DistributionTargetId uniqueidentifier, @ApplicationId uniqueidentifier, 
		@CreatedBy uniqueidentifier, @TargetId uniqueidentifier, @TargetType int, @UtcNow datetime

	SET @UtcNow = GETUTCDATE()

	DECLARE @tbStats TYDistributionRunStats
	DECLARE Distribution_Cursor CURSOR FOR SELECT D.Id FROM GLDistributionRun D 
		WHERE Status = 2 ORDER BY ModifiedDate

	OPEN Distribution_Cursor 
	FETCH NEXT FROM Distribution_Cursor INTO @DistributionRunId

	WHILE (@@FETCH_STATUS != -1)
	BEGIN
		DECLARE DistributionRun_Target CURSOR FOR 
			SELECT Id FROM GLDistributionRunTarget WHERE DistributionRunId = @DistributionRunId
		
		OPEN DistributionRun_Target 
		FETCH NEXT FROM DistributionRun_Target INTO @DistributionTargetId
		WHILE (@@FETCH_STATUS != -1)
		BEGIN
			SELECT @TargetId = TargetId, @TargetType = TargetTypeId, 
				@CreatedBy = CreatedBy FROM GLDistributionRunTarget
			WHERE Id = @DistributionTargetId 

			DELETE FROM @tbSiteIds
			
			IF @TargetType = 2 --Site
			BEGIN
				INSERT INTO @tbSiteIds
				SELECT @TargetId
			END
			ELSE
			BEGIN
				SET @ApplicationId = (SELECT TOP 1 R.SiteId FROM GLDistribution D
					JOIN GLDistributionRun R ON R.DistributionId = D.Id WHERE R.Id = @DistributionRunId)

				DECLARE @tbVariants TABLE(Id uniqueidentifier)
				INSERT INTO @tbVariants
				SELECT * FROM dbo.GetChildrenSites(@ApplicationId, 1)

				DELETE FROM @tbVariants WHERE Id = @ApplicationId

				IF @TargetType = 1 --Site List
				BEGIN
					INSERT INTO @tbSiteIds
					SELECT DISTINCT SiteId FROM SISiteListSite S
						JOIN @tbVariants V ON S.SiteId = V.Id
					WHERE SiteListId = @TargetId
				END
				ELSE IF @TargetType = 3 -- All Child sites
				BEGIN
					INSERT INTO @tbSiteIds
					SELECT DISTINCT Id FROM @tbVariants
				END
			END

			INSERT INTO GLDistributionRunItem
			(
				Id,
				DistributionRunId,
				SiteId,
				Status,
				CreatedBy,
				CreatedDate
			)
			SELECT
				NEWID(),
				@DistributionRunId,
				Id,
				1,			
				@CreatedBy,
				@UtcNow
			FROM @tbSiteIds

			FETCH NEXT FROM DistributionRun_Target INTO @DistributionTargetId 	
		END

		CLOSE DistributionRun_Target
		DEALLOCATE DistributionRun_Target

		DECLARE @TotalItems int, @Status int
		SELECT @TotalItems = COUNT(Id) FROM GLDistributionRunItem WHERE DistributionRunId = @DistributionRunId
		IF @TotalItems > 0
			SET @Status = 5
		ELSE
			SET @Status = 6

		UPDATE GLDistributionRun 
		SET Status = @Status,
			ModifiedBy = ISNULL(ModifiedBy, CreatedBy),
			ModifiedDate = @UtcNow
		WHERE Id = @DistributionRunId

		--INSERT INTO @tbStats
		--(
		--	Id,
		--	Title,
		--	Status,
		--	CreatedBy,
		--	ModifiedDate,
		--	CreatedByEmail,
		--	SiteId,
		--	SiteUrl,
		--	TotalItems
		--)
		--SELECT R.Id,
		--	D.Title,
		--	R.Status,
		--	R.CreatedBy,
		--	R.ModifiedDate,
		--	M.Email,
		--	S.Id,
		--	S.PrimarySiteUrl,
		--	@TotalItems
		--FROM GLDistributionRun R
		--	JOIN GLDistribution D ON R.DistributionId = D.Id
		--	JOIN USMembership M ON R.CreatedBy = M.UserId
		--	JOIN SISite S ON R.SiteId = S.Id
		--WHERE R.Id = @DistributionRunId

		FETCH NEXT FROM Distribution_Cursor INTO @DistributionRunId 	
	END

	CLOSE Distribution_Cursor
	DEALLOCATE Distribution_Cursor		

	SELECT * FROM @tbStats
END
GO
PRINT 'Create procedure DistributionRunItemDto_Get'
GO
IF (OBJECT_ID('DistributionRunItemDto_Get') IS NOT NULL)
	DROP PROCEDURE DistributionRunItemDto_Get
GO
CREATE PROCEDURE [dbo].[DistributionRunItemDto_Get]
(
	@Id					uniqueidentifier = NULL,
	@DistributionRunId	uniqueidentifier = NULL,
	@Status				int = NULL,
	@MarkAsProcessing	bit = NULL,
	@PageNumber			int = NULL,
	@PageSize			int = NULL,
	@MaxRecords			int = NULL,
	@SortBy				nvarchar(100) = NULL,  
	@SortOrder			nvarchar(10) = NULL,
	@Query				nvarchar(max) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(500)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY ModifiedDate DESC) AS RowNumber,
			COUNT(D.Id) OVER () AS TotalRecords,
			D.Id AS Id			
		FROM GLDistributionRunItem D
		WHERE (@Id IS NULL OR D.Id = @Id)
			AND (@Status IS NULL OR D.Status = @Status)
			AND (@DistributionRunId IS NULL OR D.DistributionRunId = @DistributionRunId)
	)
	
	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT DI.*,
		T.RowNumber,
		T.TotalRecords
	FROM GLDistributionRunItem DI
		JOIN @tbPagedResults T ON DI.Id = T.Id
	ORDER BY RowNumber

	SELECT R.*
	FROM GLDistributionRunItem D
		JOIN GLDistributionRun R ON R.Id = D.DistributionRunId
		JOIN @tbPagedResults T ON T.Id = D.Id

	SELECT D.*, 
		RI.DistributionRunId
	FROM GLDistributionRunItem RI
		JOIN GLDistributionRun R ON R.Id = RI.DistributionRunId
		JOIN GLDistribution D ON D.Id = R.DistributionId
		JOIN @tbPagedResults T ON T.Id = RI.Id

	SELECT R.*
	FROM GLDistributionRunItem RI
		JOIN GLDistributionRunResult R ON RI.Id = R.DistributionRunItemId
		JOIN @tbPagedResults T ON T.Id = RI.Id

	SELECT DT.*, 
		RI.DistributionRunId
	FROM GLDistributionRunItem RI
		JOIN GLDistributionRun R ON R.Id = RI.DistributionRunId
		JOIN GLDistributionTarget DT ON R.DistributionId = DT.DistributionId
		JOIN @tbPagedResults T ON T.Id = RI.Id

	IF @MarkAsProcessing = 1
	BEGIN
		UPDATE RI
			SET RI.Status = 5
		FROM GLDistributionRunItem RI
			JOIN @tbPagedResults T ON T.Id = RI.Id
	END
END
GO
PRINT 'Create procedure Distribution_Purge'
GO
IF (OBJECT_ID('Distribution_Purge') IS NOT NULL)
	DROP PROCEDURE Distribution_Purge
GO
CREATE PROCEDURE [dbo].[Distribution_Purge]
(
	@Months		int = NULL
)
AS
BEGIN
	IF @Months IS NULL
		SET @Months = 24

	DECLARE @tbIds TABLE(Id uniqueidentifier)

	INSERT INTO @tbIds 
	SELECT Id FROM GLDistributionRun WHERE DATEDIFF(MONTH, ModifiedDate, GETUTCDATE()) > @Months

	DELETE FROM GLDistributionRunResult WHERE DistributionRunItemId IN 
		(SELECT Id FROM GLDistributionRunItem WHERE DistributionRunId IN (SELECT Id FROM @tbIds))

	DELETE FROM GLDistributionRunItem WHERE DistributionRunId IN (SELECT Id FROM @tbIds)

	DELETE FROM GLDistributionRunTarget WHERE DistributionRunId IN (SELECT Id FROM @tbIds)

	DELETE FROM GLDistributionRun WHERE Id IN (SELECT Id FROM @tbIds)
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_ATPageAttributeValue_AttributeId' AND object_id = OBJECT_ID('ATPageAttributeValue'))
BEGIN
	PRINT 'Creating Index IX_ATPageAttributeValue_AttributeId on ATPageAttributeValue'

	CREATE NONCLUSTERED INDEX [IX_ATPageAttributeValue_AttributeId] ON [dbo].[ATPageAttributeValue] ([AttributeId])
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_ATPageAttributeValue_PageDefinitionId' AND object_id = OBJECT_ID('ATPageAttributeValue'))
BEGIN
	PRINT 'Creating Index IX_ATPageAttributeValue_PageDefinitionId on ATPageAttributeValue'

	CREATE NONCLUSTERED INDEX [IX_ATPageAttributeValue_PageDefinitionId] ON [dbo].[ATPageAttributeValue] ([PageDefinitionId]) INCLUDE ([IsShared])
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_GLDistribution_SiteId' AND object_id = OBJECT_ID('GLDistribution'))
BEGIN
	PRINT 'Creating Index IX_GLDistribution_SiteId on GLDistribution'

	CREATE NONCLUSTERED INDEX [IX_GLDistribution_SiteId] ON [dbo].[GLDistribution] ([SiteId]) INCLUDE (ObjectId, ObjectTypeId, ModifiedDate)
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_GLDistributionRun_DistributionId' AND object_id = OBJECT_ID('GLDistributionRun'))
BEGIN
	PRINT 'Creating Index IX_GLDistributionRun_DistributionId on GLDistributionRun'

	CREATE NONCLUSTERED INDEX [IX_GLDistributionRun_DistributionId] ON [dbo].[GLDistributionRun] ([DistributionId]) INCLUDE (SiteId)
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_GLDistributionRun_Status' AND object_id = OBJECT_ID('GLDistributionRun'))
BEGIN
	PRINT 'Creating Index IX_GLDistributionRun_Status on GLDistributionRun'

	CREATE NONCLUSTERED INDEX [IX_GLDistributionRun_Status] ON [dbo].[GLDistributionRun] ([Status]) INCLUDE ([Id], ModifiedDate)
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_GLDistributionRunItem_DistributionRunId' AND object_id = OBJECT_ID('GLDistributionRunItem'))
BEGIN
	PRINT 'Creating Index IX_GLDistributionRunItem_DistributionRunId on GLDistributionRunItem'

	CREATE NONCLUSTERED INDEX [IX_GLDistributionRunItem_DistributionRunId] ON [dbo].[GLDistributionRunItem] ([DistributionRunId]) INCLUDE ([Status], ModifiedDate)
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_GLDistributionRunResult_DistributionRunItemId' AND object_id = OBJECT_ID('GLDistributionRunResult'))
BEGIN
	PRINT 'Creating Index IX_GLDistributionRunResult_DistributionRunItemId on GLDistributionRunResult'

	CREATE NONCLUSTERED INDEX [IX_GLDistributionRunResult_DistributionRunItemId] ON [dbo].[GLDistributionRunResult] ([DistributionRunItemId])
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_GLDistributionRunTarget_DistributionRunId' AND object_id = OBJECT_ID('GLDistributionRunTarget'))
BEGIN
	PRINT 'Creating Index IX_GLDistributionRunTarget_DistributionRunId on GLDistributionRunTarget'

	CREATE NONCLUSTERED INDEX [IX_GLDistributionRunTarget_DistributionRunId] ON [dbo].[GLDistributionRunTarget] ([DistributionRunId])
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_GLDistributionTarget_DistributionId' AND object_id = OBJECT_ID('GLDistributionTarget'))
BEGIN
	PRINT 'Creating Index IX_GLDistributionTarget_DistributionId on GLDistributionTarget'

	CREATE NONCLUSTERED INDEX [IX_GLDistributionTarget_DistributionId] ON [dbo].[GLDistributionTarget] ([DistributionId])
END
GO
PRINT 'Create procedure Post_GetPost'
GO
IF (OBJECT_ID('Post_GetPost') IS NOT NULL)
	DROP PROCEDURE Post_GetPost
GO
CREATE PROCEDURE [dbo].[Post_GetPost]
(  
 @ApplicationId  uniqueIdentifier,    
 @Id     uniqueIdentifier = null,    
 @PostMonth   varchar(20) = null,  
 @PostYear   varchar(20) = null,  
 @PageSize   int = null,  
 @PageNumber   int = null,  
 @FolderId   uniqueIdentifier = null,  
 @BlogId    uniqueIdentifier = null,  
 @Status    int = null,  
 @CategoryId   uniqueIdentifier = null,  
 @Label    nvarchar(1024) = null,  
 @Author    nvarchar(1024) = null,  
 @IncludeVersion  bit = null,  
 @VersionId   uniqueidentifier=null,  
 @IgnoreSite   bit = null,  
 @ShowOnlyApprovedComments bit = null  
)  
AS  
BEGIN  
 DECLARE @IsSticky bit  
 IF @Status = 5  
  SET @IsSticky = 1  
  
 IF @ShowOnlyApprovedComments IS NULL  
  SET @ShowOnlyApprovedComments = 0  
  
  
 DECLARE @tbPostIds TABLE(RowNo int, TotalRows int, Id uniqueidentifier, BlogId uniqueidentifier, PostContentId uniqueidentifier)  
 ;WITH PostIds AS(  
  SELECT ROW_NUMBER() over (order by IsSticky desc, PostDate desc, Title) AS RowNo,   
   COUNT(Id) over (PARTITION BY NULL) AS TotalRows,Id, BlogId, PostContentId  
  FROM BLPost   
  WHERE (@Id IS NULL OR Id = @Id) AND  
   (Status != dbo.GetDeleteStatus()) AND  
   (@ApplicationId IS NULL OR @IgnoreSite = 1 OR @ApplicationId = ApplicationId) AND  
   (@Status IS NULL OR Status = @Status) AND  
   (@PostYear IS NULL OR YEAR(PostDate) = @PostYear) AND   
   (@PostMonth IS NULL OR MONTH(PostDate) = @PostMonth) AND   
   (@FolderId IS NULL OR HSId = @FolderId) AND   
   (@BlogId IS NULL OR BlogId = @BlogId) AND  
   (@IsSticky IS NULL OR IsSticky = @IsSticky) AND  
   (@Label IS NULL OR Labels LIKE '%' + @Label + '%') AND  
   (@Author IS NULL OR AuthorEmail like @Author OR AuthorName like @Author) AND  
   (@CategoryId IS NULL OR Id IN (SELECT ObjectId FROM COTaxonomyObject WHERE TaxonomyId = @CategoryId AND ObjectTypeId = 40))  
 )  
   
 INSERT INTO @tbPostIds  
 SELECT RowNo, TotalRows, Id, BlogId, PostContentId FROM PostIds  
 WHERE (@PageSize IS NULL OR @PageNumber IS NULL OR   
  RowNo BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize))  
   
 SELECT A.Id,     
  A.ApplicationId,  
  A.Title,  
  (CASE WHEN @Id IS NULL THEN '' ELSE A.Description END) Description,  
  A.ShortDescription,  
  A.AuthorName,  
  A.AuthorEmail,  
  A.Labels,  
  A.AllowComments,  
  CONVERT(DATE, A.PostDate) PostDate,   
  A.IsScheduled,
  A.ScheduledPublishDate,  
  A.UseLocalTimeZone,
  A.ScheduledTimeZone,
  A.Location,  
  A.Status,  
  A.IsSticky,  
  A.CreatedBy,  
  A.CreatedDate,  
  A.ModifiedBy,  
  A.ModifiedDate,  
  A.FriendlyName,  
  A.PostContentId,  
  A.ContentId,  
  A.ImageUrl,  
  A.TitleTag,  
  A.H1Tag,  
  A.KeywordMetaData,  
  A.DescriptiveMetaData,  
  A.OtherMetaData,  
  B.BlogListPageId,  
  B.Id AS BlogId,  
  A.WorkflowState,  
  A.PublishDate,  
  A.PublishCount,  
  A.INWFFriendlyName,  
  A.HSId AS MonthId  
 FROM BLPost A   
  JOIN @tbPostIds T ON A.Id = T.Id  
  JOIN BLBlog B ON A.BlogId = B.Id  
 ORDER BY T.RowNo  
  
 SELECT  C.Id,  
  C.ApplicationId,  
  C.Title,  
  C.Description,   
  C.Text,   
  C.URL,  
  C.URLType,   
  C.ObjectTypeId,   
  C.CreatedDate,  
  C.CreatedBy,  
  C.ModifiedBy,  
  C.ModifiedDate,   
  C.Status,  
  C.RestoreDate,   
  C.BackupDate,   
  C.StatusChangedDate,  
  C.PublishDate,   
  C.ArchiveDate,   
  C.XMLString,  
  C.BinaryObject,   
  C.Keywords,  
  FN.UserFullName CreatedByFullName,  
  MN.UserFullName ModifiedByFullName,  
  C.OrderNo, C.SourceContentId,  
  C.ParentId,  
  sDir.VirtualPath As FolderPath  
 FROM COContent C  
  INNER JOIN @tbPostIds T ON C.Id = T.PostContentId  
  INNER JOIN COContentStructure sDir on SDir.Id = C.ParentId  
  LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy  
  LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy  
    
 SELECT Top 1 TotalRows FROM @tbPostIds  
  
 SELECT A.Id,  
  ApplicationId,  
  Title,  
  Description,  
  BlogNodeType,  
  MenuId,  
  ContentDefinitionId,  
  ShowPostDate,  
  DisplayAuthorName,  
  EmailBlogOwner,  
  ApproveComments,  
  RequiresUserInfo,  
  Status,  
  CreatedDate,  
  CreatedBy,  
  ModifiedBy,  
  FN.UserFullName CreatedByFullName,  
  MN.UserFullName ModifiedByFullName,  
  ModifiedDate,  
  BlogListPageId,  
  BlogDetailPageId,  
  ContentDefinitionId--,  
  --CASE WHEN SUBSTRING(V.Url,1,1) = '/' THEN SUBSTRING(V.Url,2,LEN(V.Url)-1) ELSE V.Url END AS BlogUrl     
 FROM BLBlog A   
  INNER JOIN @tbPostIds T ON T.BlogId = A.Id  
		--LEFT JOIN vwBlogUrl V ON V.Id = A.Id
  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
  LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
   
 SELECT T.Id,  
  I.Title,  
  C.TaxonomyId  
 FROM COTaxonomyObject C  
  JOIN @tbPostIds T ON T.Id = C.ObjectId  
  JOIN COTaxonomy I ON I.Id = C.TaxonomyId  
  
 IF (@ShowOnlyApprovedComments = 1)  
 BEGIN  
  SELECT Count(*) AS NoOfComments,  
   T.Id  
  FROM BLComments C  
   JOIN @tbPostIds T ON C.ObjectId = T.Id  
  WHERE C.Status = 4 --For approved comments  
  GROUP BY T.Id  
 END  
 ELSE  
 BEGIN  
  SELECT Count(*) AS NoOfComments,  
   T.Id  
  FROM BLComments C  
   JOIN @tbPostIds T ON C.ObjectId = T.Id  
  GROUP BY T.Id  
 END  
  
 IF @IncludeVersion = 1  
 BEGIN  
  DECLARE @tbVersionIds TABLE(Id uniqueidentifier)  
  IF @VersionId IS NULL  
  BEGIN  
   IF(@Id IS NOT NULL)  
   BEGIN  
    SELECT TOP (1) @VersionId = Id  from VEVersion A  
    WHERE ObjectId=@Id   
    ORDER BY RevisionNumber DESC, VersionNumber DESC  
  
     
    INSERT INTO @tbVersionIds VALUES (@VersionId)  
   END  
   ELSE  
   BEGIN  
    ;WITH CTEVersion AS (  
     SELECT ROW_NUMBER() OVER (PARTITION BY T.Id ORDER BY RevisionNumber DESC, VersionNumber DESC) AS VerRank,  
      V.Id FROM VEVersion V  
      JOIN BLPost T On T.Id = V.ObjectId  
    )  
  
    INSERT INTO @tbVersionIds  
    SELECT Id FROM CTEVersion WHERE VerRank = 1  
   END  
  END  
  ELSE  
  BEGIN  
   INSERT INTO @tbVersionIds VALUES (@VersionId)  
  END  
  
  SELECT V.Id AS VersionId,  
   V.ObjectId AS PostId,  
   V.XMLString  
  FROM VEVersion V  
   JOIN @tbVersionIds T On T.Id = V.Id  
  
  SELECT   
   V.PostVersionId AS VersionId,  
   C.ContentId,  
   C.XMLString  
  FROM VEPostContentVersion V  
   JOIN @tbVersionIds T On T.Id = V.PostVersionId  
   JOIN VEContentVersion C ON C.Id = V.ContentVersionId  
 END  
END  
GO
PRINT 'Create procedure Post_Save'
GO
IF (OBJECT_ID('Post_Save') IS NOT NULL)
	DROP PROCEDURE Post_Save
GO
CREATE PROCEDURE [dbo].[Post_Save]   
(  
	@Id						uniqueidentifier = null OUT ,  
	@BlogId					uniqueidentifier,   
	@ApplicationId			uniqueidentifier,   
	@Title					nvarchar(256) = null,  
	@Description			ntext = null,  
	@ShortDescription		nvarchar(2000) = null,  
	@AuthorName				nvarchar(256) = null,  
	@AuthorEmail			nvarchar(256) = null,  
	@AllowComments			bit = true,  
	@PostDate				datetime = null,  
	@IsScheduled			bit = null,
	@ScheduledPublishDate	datetime = null,  
	@UseLocalTimeZone		bit = null,  
	@ScheduledTimeZone		nvarchar(100) = null,  
	@Location				nvarchar(256) = null,  
	@Labels					nvarchar(Max) = null,  
	@ModifiedBy				uniqueidentifier,  
	@IsSticky				bit = null, 
	@HSId					uniqueidentifier,  
	@FriendlyName			nvarchar(256) = null,
	@PostContentId			uniqueidentifier = null,
	@ContentId				uniqueidentifier = null,
	@Imageurl				nvarchar(500) = null,
	@TitleTag				nvarchar(2000) = null,  
	@H1Tag					nvarchar(2000) = null,
	@KeywordMetaData		nvarchar(2000) = null,
	@DescriptiveMetaData	nvarchar(2000) = null,
	@OtherMetaData			nvarchar(2000) = null,
	@PublishDate			datetime = null,
	@PublishCount			int = null,
	@WorkflowState			int = null,
	@Status					int = null
)  
AS  
BEGIN  
	DECLARE  
	 @NewId  uniqueidentifier,  
	 @RowCount  INT,  
	 @Stmt   VARCHAR(25),  
	 @Now    datetime,  
	 @Error   int ,  
	 @MonthId uniqueidentifier,  
	 @return_value int --,
	 --@Status int 
  
	 /* if ID specified, ensure exists */  
	 IF (@Id IS NOT NULL)  
	  IF (NOT EXISTS(SELECT 1 FROM   BLPost WHERE  Id = @Id AND Status <> dbo.GetDeleteStatus()))  
	  BEGIN  
	   set @Stmt = convert(varchar(36),@Id)  
	   RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )  
	   RETURN dbo.GetBusinessRuleErrorCode()  
	  END    

	  IF @Status = 5
	  BEGIN
		SET @Status = null 
		/*
		Sankar: If the @status = sticky then 
		FOR INSERT : There is no provision now to make the post as sticky at the time of creation. so in the insert we are making it to default to 1(draft).Even if we send a @Status = 5 then it will create DRAFT STICKY POST.
		FOR update : if the status is sent as sticky, then we need to maintain the same status for the post by updating the sticky bit to 1. So here I am making the @status = null
		*/
	  END
	   
	 SET @Now = getutcdate()  
	  
	 IF (@Id IS NULL)   -- new insert  
	 BEGIN  
	  SET @Stmt = 'Create Post'  
	  SELECT @NewId =newid()  
	  
	  INSERT INTO BLPost  WITH (ROWLOCK)(  
		 Id,  
		ApplicationId,  
		Title,  
		BlogId,  
		Description,  
		ShortDescription,  
		AuthorName,  
		AuthorEmail,  
		Labels,  
		AllowComments,  
		PostDate,  
		IsScheduled,
		ScheduledPublishDate,  
		UseLocalTimeZone,
		ScheduledTimeZone,
		Location,  
		Status,  
		IsSticky,
		CreatedBy,  
		CreatedDate,  
		HSId,  
		FriendlyName,
		PostContentId,
		ContentId,
		ImageUrl,
		TitleTag,
		H1Tag,
		KeywordMetaData,
		DescriptiveMetaData,
		OtherMetaData
	  )  
	  VALUES  
	   (  
			@NewId,  
			@ApplicationId,   
			@Title,  
			@BlogId,  
			@Description,  
			@ShortDescription,  
			@AuthorName,  
			@AuthorEmail,  
			@Labels,  
			@AllowComments,  
			@PostDate,  
			@IsScheduled,
			@ScheduledPublishDate,  
			@UseLocalTimeZone,  
			@ScheduledTimeZone,  
			@Location,  
			ISNULL(@Status,1), 
			@IsSticky, 
			@ModifiedBy,  
			@Now,
			@HSId ,
			@FriendlyName,
			@PostContentId,
			@ContentId,
			@ImageUrl,
			@TitleTag,
			@H1Tag,
			@KeywordMetaData,
			@DescriptiveMetaData,
			@OtherMetaData
	   )  
	  SELECT @Error = @@ERROR  
	   IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()   
	  END  
	  
	  SELECT @Id = @NewId  
	 END  
	 ELSE   -- Update  
	 BEGIN  
	  SET @Stmt = 'Modify Post'  
	  UPDATE BLPost WITH (ROWLOCK)  
	   SET   
		 Id     = @Id,  
		ApplicationId  = @ApplicationId,  
		Title    = @Title,  
		BlogId    = @BlogId,  
		Description   = @Description,  
		ShortDescription = @ShortDescription,  
		AuthorName   = @AuthorName,  
		AuthorEmail   = @AuthorEmail,  
		Labels    = @Labels,  
		AllowComments  = @AllowComments,  
		PostDate   = @PostDate,  
		IsScheduled = @IsScheduled,
		ScheduledPublishDate   = @ScheduledPublishDate,  
		UseLocalTimeZone	= @UseLocalTimeZone,  
		ScheduledTimeZone	= @ScheduledTimeZone,  
		Location   = @Location ,  
		ModifiedBy   = @ModifiedBy,  
		ModifiedDate  = GETUTCDATE(),  
		HSId = @HSId,  
		FriendlyName=@FriendlyName,
		PostContentId = @PostContentId,
		ContentId = @ContentId,
		ImageUrl = @Imageurl,
		TitleTag = @TitleTag,
		H1Tag = @H1Tag,
		IsSticky = @IsSticky,
		KeywordMetaData = @KeywordMetaData,
		DescriptiveMetaData = @DescriptiveMetaData,
		OtherMetaData = @OtherMetaData,
		PublishDate = @PublishDate,
		PublishCount = @PublishCount,
		WorkflowState = @WorkflowState,
		Status = ISNULL(@Status,Status)
	  WHERE Id = @Id  
	    
	  SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT  
	  
	  IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()  
	  END  
	  
	  IF @RowCount = 0  
	  BEGIN  
	   --concurrency error  
	   RAISERROR('DBCONCURRENCY',16,1)  
	   RETURN dbo.GetDataConcurrencyErrorCode()  
	  END   
	 END    
   
	EXEC PageMapBase_UpdateLastModification @ApplicationId
END
GO
GO

GO
PRINT 'Create procedure CommerceReport_SalesByPromoCode'
GO
IF (OBJECT_ID('CommerceReport_SalesByPromoCode') IS NOT NULL)
	DROP PROCEDURE CommerceReport_SalesByPromoCode
GO
CREATE PROCEDURE [dbo].[CommerceReport_SalesByPromoCode]
(	
	@couponCode nvarchar(255) = null,
	@startDate datetime,@endDate datetime, @maxReturnCount int,
	@pageSize int = 10,@pageNumber int, 
	@sortBy nvarchar(50) = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier=null,
	@IncludeChildrenSites Bit = 0 
) 
AS
BEGIN

			if @maxReturnCount = 0
				set @maxReturnCount = null

		IF @pageSize IS NULL
				SET @PageSize = 2147483647
		IF @endDate IS NULL
				SET @endDate = GetDate()

		declare @tmpCouponSale table(
							CouponId uniqueidentifier,
							Title nvarchar(255),
							Code nvarchar(255),
							OrderCount int,
							TotalDiscount money
										)

		INSERT INTO @tmpCouponSale

			SELECT CP.ID, CP.Title, CPC.Code, 
					 Count(Distinct O.Id) As OrderCount , 
					Sum(O.TotalDiscount + O.TotalShippingDiscount) As TotalDiscount
			FROM OROrder O 
					INNER JOIN CPOrderCouponCode CPO on O.Id = CPO.OrderId
					INNER JOIN CPCouponCode CPC on CPC.Id = CPO.CouponCodeId 
					INNER JOIN CPCoupon CP on CPC.CouponId = CP.ID
			WHERE 
					O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
					AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
					AND O.OrderStatusId in (1,2,11,4)
					AND  (
			(@IncludeChildrenSites = 0 AND O.SiteId = @ApplicationId )
			OR
			(@IncludeChildrenSites = 1 AND O.SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites( @ApplicationId,1)))
		)
					AND
					(@couponCode is null or CPC.Code = @couponCode) 
			GROUP BY
				  CPC.Code, CP.ID,  CP.Title

			Set @virtualCount = @@ROWCOUNT

			if @sortBy is null or (@sortBy) = 'code asc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by Code asc) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.Code ASC
				end
			else if @sortBy is null or (@sortBy) = 'code desc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by Code desc) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.Code desc
				end
			else if @sortBy is null or (@sortBy) = 'ordercount asc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by OrderCount asc) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.OrderCount asc
				end
			else if @sortBy is null or (@sortBy) = 'ordercount desc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by OrderCount desc) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.OrderCount desc
				end
			else if @sortBy is null or (@sortBy) = 'totaldiscount asc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by TotalDiscount ASC) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.TotalDiscount ASC
				end	
			else if @sortBy is null or (@sortBy) = 'totaldiscount desc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by TotalDiscount DESC) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.TotalDiscount DESC
				end	
END
GO
PRINT 'Creating view vwPageAttributeValue'
GO
IF(OBJECT_ID('vwPageAttributeValue') IS NOT NULL)
	DROP VIEW vwPageAttributeValue
GO	
CREATE VIEW [dbo].[vwPageAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V

	UNION ALL

	SELECT 1 AS vRank,
		V.Id,
		P.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V
		JOIN PageDefinition P ON V.PageDefinitionId = P.SourcePageDefinitionId
	WHERE V.IsShared = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT V.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	ISNULL(V.Notes, C.Notes) AS Notes,
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.CreatedDate, 
	C.CreatedBy,
	A.Title as AttributeTitle
FROM CTE C
	LEFT JOIN ATPageAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.PageDefinitionId
	LEFT JOIN ATAttribute A ON C.AttributeId = A.Id
WHERE C.ValueRank = 1
GO
PRINT 'Create procedure PageDto_Invalidate'
GO
IF (OBJECT_ID('PageDto_Invalidate') IS NOT NULL)
	DROP PROCEDURE PageDto_Invalidate
GO
CREATE PROCEDURE [dbo].[PageDto_Invalidate]
(
	@SiteId				uniqueidentifier = NULL,
	@SiteIds			varchar(max) = NULL,
	@ModifiedBy			uniqueidentifier
)
AS
BEGIN
	IF @SiteIds IS NOT NULL
	BEGIN
		DECLARE @ModifiedDate datetime
		SET @ModifiedDate =  DATEADD(SECOND, DATEDIFF(SECOND, 39000, GETUTCDATE()), 39000)

		DECLARE @tbSites TABLE(Id uniqueidentifier)
		INSERT INTO @tbSites
		SELECT Items FROM dbo.SplitGUID(@SiteIds,',')

		UPDATE P SET P.LastPageMapModificationDate = @ModifiedDate
		FROM PageMapBase P JOIN @tbSites S ON P.SiteId = S.Id
	END
	ELSE
	BEGIN
		EXEC PageMapBase_UpdateLastModification @SiteId = @SiteId
	END
END
GO


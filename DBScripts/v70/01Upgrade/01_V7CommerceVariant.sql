﻿GO
PRINT 'Updating the version in the productsuite table'
GO
UPDATE iAppsProductSuite SET UpdateStartDate = GETUTCDATE(), ProductVersion='7.0.0609.0200'
GO


IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SQLExecution'))
BEGIN
	CREATE TABLE SQLExecution
	(
		Id uniqueidentifier, 
		Title varchar(100),
		DBVersion varchar(20),
		CreatedDate DateTime,
		PRIMARY KEY(Id) 
	)
END
GO
PRINT 'Creating View vwCouponCodeUseCount'
GO
IF (OBJECT_ID('vwCouponCodeUseCount') IS NOT NULL)
	DROP View vwCouponCodeUseCount	
GO
CREATE VIEW [dbo].[vwCouponCodeUseCount]
AS 
SELECT ORDCode.CouponCodeId, 
	COUNT(1) AS UseCount 
FROM CPOrderCouponCode ORDCode 
	INNER JOIN OROrder O ON O.Id = ORDCode.OrderId
WHERE O.OrderStatusId NOT IN (12, 3)
GROUP BY ORDCode.CouponCodeId
GO
PRINT 'Creating View vwTagContent'
GO
IF (OBJECT_ID('vwTagContent') IS NOT NULL)
	DROP View vwTagContent	
GO
CREATE VIEW [dbo].[vwTagContent]
AS
SELECT C.*,
	T.TaxonomyId AS TagId
FROM vwContent C
	JOIN COTaxonomyObject T ON T.ObjectId = C.Id
GO
PRINT 'Creating View vwTagFile'
GO
IF (OBJECT_ID('vwTagFile') IS NOT NULL)
	DROP View vwTagFile	
GO
CREATE VIEW [dbo].[vwTagFile]
AS
SELECT C.*,
	T.TaxonomyId AS TagId
FROM vwFile C
	JOIN COTaxonomyObject T ON T.ObjectId = C.Id
GO
PRINT 'Creating View vwTagImage'
GO
IF (OBJECT_ID('vwTagImage') IS NOT NULL)
	DROP View vwTagImage	
GO
CREATE VIEW [dbo].[vwTagImage]
AS
SELECT C.*,
	T.TaxonomyId AS TagId
FROM vwImage C
	JOIN COTaxonomyObject T ON T.ObjectId = C.Id
GO
PRINT 'Creating View vwAvailableToSellPerWarehouse'
GO
IF (OBJECT_ID('vwAvailableToSellPerWarehouse') IS NOT NULL)
	DROP View vwAvailableToSellPerWarehouse	
GO
CREATE VIEW [dbo].[vwAvailableToSellPerWarehouse]
AS

SELECT inv.SKUId AS SKUId, inv.WarehouseId,
	inv.Quantity Inventory,
	Quantity = CASE AvailableForBackOrder WHEN 0 THEN  (inv.Quantity - 	ISNULL(allocated.Quantity, 0)) ELSE prs.BackOrderLimit + (inv.Quantity - 	ISNULL(allocated.Quantity, 0))END ,
	BackOrderLimit = 
	CASE AvailableForBackOrder
		WHEN 1 THEN prs.BackOrderLimit 
		ELSE 0
		END,
		ws.SiteId
FROM dbo.PRProductSKU prs
LEFT JOIN dbo.INInventory inv ON prs.Id=inv.SKUId
LEFT JOIN dbo.vwAllocatedQuantityperWarehouse allocated 
ON inv.SKUId = allocated.ProductSKUId AND inv.WarehouseId = allocated.WarehouseId
INNER JOIN dbo.INWarehouse w ON inv.WarehouseId = w.Id
INNER JOIN dbo.INWarehouseSite ws ON w.Id = ws.WarehouseId
WHERE prs.IsUnlimitedQuantity =0 AND w.Status=1

UNION

SELECT inv.SKUId AS SKUId, inv.WarehouseId, 
999999999
	 AS Inventory,
999999999
	 AS Quantity,
	BackOrderLimit = 
	CASE AvailableForBackOrder
		WHEN 1 THEN prs.BackOrderLimit 
		ELSE 0
		END,
ws.WarehouseId
FROM dbo.PRProductSKU prs
LEFT JOIN dbo.INInventory inv ON prs.Id=inv.SKUId
LEFT JOIN dbo.INWarehouseSite ws ON inv.WarehouseId = ws.WarehouseId
WHERE prs.IsUnlimitedQuantity =1

GO



PRINT 'Alter ModifyDate column in SISiteList'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SISiteList' AND COLUMN_NAME = 'ModifiedDate' AND DATA_TYPE = 'date')
	ALTER TABLE SISiteList ALTER COLUMN [ModifiedDate] datetime NULL
GO

PRINT 'Creating table CPCouponCustomerGroup'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'CPCouponCustomerGroup'))
BEGIN
	CREATE TABLE [dbo].[CPCouponCustomerGroup] (
		[Id]           UNIQUEIDENTIFIER NOT NULL,
		[CouponId]     UNIQUEIDENTIFIER NOT NULL,
		[CustomerGroupId]   UNIQUEIDENTIFIER NOT NULL,
		[Status]       INT              NOT NULL,
		[CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
		[CreatedDate]  DATETIME         NOT NULL,
		[ModifiedBy]   UNIQUEIDENTIFIER NULL,
		[ModifiedDate] DATETIME         NULL,
		CONSTRAINT [PK_CPCouponCustomerGroup] PRIMARY KEY CLUSTERED ([Id] ASC),
		CONSTRAINT [FK_CPCouponCustomerGroup_CPCoupon] FOREIGN KEY ([CouponId]) REFERENCES [dbo].[CPCoupon] ([ID]),
		CONSTRAINT [FK_CPCouponCustomerGroup_USCommerceUserProfile] FOREIGN KEY ([CustomerGroupId]) REFERENCES [dbo].[USGroup] ([Id])
	)
END
GO

PRINT 'Creating table PRProductMediaVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PRProductMediaVariant'))
BEGIN
	CREATE TABLE [dbo].[PRProductMediaVariant] 
	(
		[Id]               UNIQUEIDENTIFIER NOT NULL,
		[SiteId]        UNIQUEIDENTIFIER NOT NULL,
		[Title]				NVARCHAR(256) NOT NULL,
		[Description]      NVARCHAR (MAX)   NULL,
		[FileName]			nvarchar(256)	NULL,
		[MaxNumDownloads]  INT              NOT NULL,
		[MaxDaysAvailable] INT              NULL,
		[ExpirationDate]   DATETIME         NULL,
		[Status]			INT              NULL,
		[Size]				INT              NULL,
		[ModifiedBy]       UNIQUEIDENTIFIER NULL,
		[ModifiedDate]     DATETIME         NULL, 
		CONSTRAINT [PK_PRProductMediaVariant] PRIMARY KEY ([Id], [SiteId])
	)
END
GO

PRINT 'Creating table PRProductMediaVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PRProductMediaVariantCache'))
BEGIN
	CREATE TABLE [dbo].[PRProductMediaVariantCache] 
	(
		[Id]               UNIQUEIDENTIFIER NOT NULL,
		[SiteId]        UNIQUEIDENTIFIER NOT NULL,
		[Title]				NVARCHAR(256) NOT NULL,
		[Description]      NVARCHAR (MAX)   NULL,
		[FileName]			nvarchar(256)	NULL,
		[MaxNumDownloads]  INT              NOT NULL,
		[MaxDaysAvailable] INT              NULL,
		[ExpirationDate]   DATETIME         NULL,
		[Status]			INT              NULL,
		[Size]				INT              NULL,
		[ModifiedBy]       UNIQUEIDENTIFIER NULL,
		[ModifiedDate]     DATETIME         NULL, 
		CONSTRAINT [PK_PRProductMediaVariantCache] PRIMARY KEY ([Id], [SiteId])
	)
END
GO

PRINT 'Add columns to COTaxonomyObject'
GO
IF(COL_LENGTH('COTaxonomyObject', 'SiteId') IS NULL)
	ALTER TABLE COTaxonomyObject ADD SiteId uniqueidentifier
GO

IF EXISTS (SELECT 1 FROM COTaxonomyObject WHERE SiteId IS NULL)
	UPDATE O SET O.SiteId = T.SiteId
	FROM COTaxonomyObject O
		JOIN COTaxonomy T ON T.Id = O.TaxonomyId
	WHERE O.SiteId IS NULL
GO

PRINT 'Add columns to ORTaxLog'
GO
IF(COL_LENGTH('ORTaxLog', 'OrderShippingId') IS NULL)
	ALTER TABLE ORTaxLog ADD OrderShippingId uniqueidentifier
GO

IF EXISTS (SELECT 1 FROM ORTaxLog WHERE OrderShippingId IS NULL)
	UPDATE L SET L.OrderShippingId = OS.Id
	FROM ORTaxLog L
		JOIN OROrderShipping OS ON OS.OrderId = L.OrderId
	WHERE L.OrderShippingId IS NULL
GO

PRINT 'Add columns to PRProductMedia'
GO
IF(COL_LENGTH('PRProductMedia', 'Title') IS NULL)
	ALTER TABLE PRProductMedia ADD Title nvarchar(256)
GO

IF(COL_LENGTH('PRProductMedia', 'FileName') IS NULL)
	ALTER TABLE PRProductMedia ADD FileName nvarchar(256)
GO

IF(COL_LENGTH('PRProductMedia', 'Status') IS NULL)
	ALTER TABLE PRProductMedia ADD Status int
GO

IF(COL_LENGTH('PRProductMedia', 'Size') IS NULL)
	ALTER TABLE PRProductMedia ADD Size int
GO

PRINT 'UPDATE Product Media Tables'
GO
IF(OBJECT_ID('PRMedia') IS NOT NULL)
BEGIN
	UPDATE PM
	SET PM.Title = M.Title,
		PM.FileName = M.FileName,
		PM.Status = M.Status,
		PM.Size = M.Size
	FROM PRProductMedia PM
		JOIN PRMedia M ON M.Id = PM.MediaId

	IF(OBJECT_ID('FK_PRProductMedia_PRMedia') IS NOT NULL)
		ALTER TABLE PRProductMedia DROP CONSTRAINT FK_PRProductMedia_PRMedia

	IF(COL_LENGTH('PRProductMedia', 'MediaId') IS NOT NULL)
		ALTER TABLE PRProductMedia DROP COLUMN MediaId

	DROP TABLE PRMedia
END
GO
PRINT 'Add columns to ATAttribute'
GO
IF(COL_LENGTH('ATAttribute', 'SiteId') IS NULL)
BEGIN
	ALTER TABLE ATAttribute ADD SiteId uniqueidentifier
END
GO
IF(COL_LENGTH('ATAttribute', 'SiteId') IS NOT NULL)
BEGIN
	UPDATE ATAttribute SET SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' where SiteId IS NULL
END
GO
--IF(COL_LENGTH('ATAttribute', 'IsShared') IS NULL)
--BEGIN
--	ALTER TABLE ATAttribute ADD IsShared BIT  DEFAULT (1) NOT NULL
--END
--GO
--IF(COL_LENGTH('ATAttribute', 'IsEditable') IS NULL)
--BEGIN
--	ALTER TABLE ATAttribute ADD IsEditable BIT  DEFAULT (1) NOT NULL
--END
--GO

IF COL_LENGTH('Staging_FlatFile','ProductId') IS NULL
BEGIN
	ALTER TABLE Staging_FlatFile ADD [ProductId] nvarchar(max) NULL 
END
GO

IF COL_LENGTH('Staging_ProductAttributeValues','ProductId') IS NULL
BEGIN
	ALTER TABLE Staging_ProductAttributeValues ADD [ProductId] float NULL 
END
GO

IF COL_LENGTH('Staging_SKUProductAttributeValues','ProductId') IS NULL
BEGIN
	ALTER TABLE Staging_SKUProductAttributeValues ADD [ProductId] float NULL 
END
GO

IF(COL_LENGTH('PRProductTypeAttributeValue', 'ProductTypeId') IS NULL)
BEGIN
ALTER TABLE PRProductTypeAttributeValue ADD [ProductTypeId] UNIQUEIDENTIFIER NULL
END
GO



IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE TYPE_DESC =  'DEFAULT_CONSTRAINT' AND NAME = 'DF_STSettingType_IsSiteOnly')
BEGIN 
	Alter table STSettingType DROP Constraint DF_STSettingType_IsSiteOnly
END 
GO

IF(COL_LENGTH('Staging_ATAttribute', 'AttributeGroupId') IS NOT NULL)
BEGIN
	ALTER TABLE Staging_ATAttribute DROP COLUMN AttributeGroupId
END

GO

IF(COL_LENGTH('STSettingType', 'IsSiteOnly') IS NOT NULL)
BEGIN
	ALTER TABLE STSettingType DROP COLUMN IsSiteOnly
END
GO
IF(COL_LENGTH('STSettingType', 'IsEnabled') IS NULL)
BEGIN
	ALTER TABLE STSettingType ADD IsEnabled bit
END
GO
IF(COL_LENGTH('STSettingType', 'IsVisible') IS NULL)
BEGIN
	ALTER TABLE STSettingType ADD IsVisible bit
END
GO
IF(COL_LENGTH('STSettingType', 'ControlType') IS NULL)
BEGIN
	ALTER TABLE STSettingType ADD ControlType nvarchar(50) NULL
END
GO
IF(COL_LENGTH('STSettingType', 'ControlData') IS NULL)
BEGIN
	ALTER TABLE STSettingType ADD ControlData nvarchar(max)
END

GO
IF(COL_LENGTH('GLDistributionRun', 'PreserveVariantContent') IS NULL)
BEGIN
	ALTER TABLE GLDistributionRun ADD PreserveVariantContent [bit] NULL
END

GO
IF(COL_LENGTH('STSettingGroup', 'IsSystem') IS NULL)
BEGIN
	ALTER TABLE STSettingGroup ADD IsSystem bit
	Declare @sql varchar(max)
	Set @sql = 'UPDATE STSettingGroup SET IsSystem = 1
				UPDATE STSettingGroup SET IsSystem = 0, Sequence = 99 WHERE Name = ''Custom'' '

	exec (@sql)
END 
GO 

IF EXISTS (SELECT 1 FROM USUser WHERE LastInteractionDate IS NULL)
BEGIN
	UPDATE USUser SET LastInteractionDate = LastActivityDate WHERE LastInteractionDate IS NULL

	UPDATE D SET D.[66666666-0000-0000-0000-000000000000] = U.LastInteractionDate
	FROM DNContactAttribute D JOIN USUser U ON U.Id = D.ContactId
END
GO

IF(COL_LENGTH('STSettingGroup', 'ProductId') IS NULL)
BEGIN
	Declare @sql nvarchar(max)
	ALTER TABLE STSettingGroup ADD ProductId uniqueidentifier

	UPDATE STSettingType SET FriendlyName = NULL, IsEnabled = 1, ControlType = 'Textbox'
	UPDATE STSettingType SET IsVisible = 1 WHERE SettingGroupId IN (SELECT Id FROM STSettingGroup WHERE ISSystem = 0)
	 
	DELETE FROM STSettingGroup WHERE Id NOT IN (SELECT SettingGroupId FROM STSettingType)

	DECLARE @SettingTypeId int, @SettingGroupId int, @Sequence int

	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'General'
	IF @SettingGroupId IS NULL
	BEGIN
		SET @Sequence = 1
		SET @SettingGroupId = 1
		INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem) VALUES (@SettingGroupId, 'General', @Sequence, 1)
	END

	UPDATE STSettingType SET FriendlyName = 'Enable Debug Info modal in front end website',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'PublicSite.EnableDebugInfo'

	UPDATE STSettingType SET FriendlyName = 'List of file extensions to be open in new window',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'FilesToOpenInBrowser'

	UPDATE STSettingType SET FriendlyName = 'Enables tracking for Analyzer',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'EnableAnalyticsTracking'

	UPDATE STSettingType SET FriendlyName = 'Enables to push objects to targetted child sites',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'Distribution.Enabled'

	UPDATE STSettingType SET FriendlyName = 'Enables to synchronize current site data with its parent',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'Distribution.AllowSynchronization'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'Dropdownlist',
		ControlData = '{ "ELASTIC" : "ELASTIC", "ISYS" : "ISYS" }',
		IsVisible = 1
	WHERE Name = 'Search.Provider'

	UPDATE STSettingType SET FriendlyName = 'All supported TLS versions',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkboxlist',
		ControlData = '{ "Ssl3" : "Ssl3", "Tls" : "Tls", "Tls11" : "Tls11", "Tls12" : "Tls12", "Tls13" : "Tls13" }',
		IsVisible = 1
	WHERE Name = 'SecurityProtocol'

	--jQuery
	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'jQuery'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem) VALUES (@SettingGroupId, 'jQuery', @Sequence, 1)
	END

	UPDATE STSettingType SET FriendlyName = 'Include jQuery script in frontend pages',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'JQuery.Include'

	UPDATE STSettingType SET FriendlyName = 'Use CDN for jQuery',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'JQuery.UseCDN'

	UPDATE STSettingType SET FriendlyName = 'jQuery CDN path',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'JQuery.CDNPath'

	UPDATE STSettingType SET FriendlyName = 'Include jQuery UI script in frontend pages',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'JQuery.Include'

	UPDATE STSettingType SET FriendlyName = 'Use CND for jQuery UI',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'JQuery.UseCDN'

	UPDATE STSettingType SET FriendlyName = 'jQuery UI CDN path',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'JQuery.CDNPath'

	-- WYSIWYG
	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'WYSIWYG Editor'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem) VALUES (@SettingGroupId, 'WYSIWYG Editor', @Sequence, 1)
	END

	UPDATE STSettingType SET FriendlyName = 'Use Content Area CSS for editor',
		SettingGroupId = @SettingGroupId,
		ControlType = 'CheckBox',
		IsVisible = 1
	WHERE Name = 'WYSIWYG.UseContentAreaStyles'

	UPDATE STSettingType SET FriendlyName = 'Path of Content Area CSS file',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'WYSIWYG.ContentAreaStylePath'

	/****************** COMMERCE STARTS ****************************/

	-- Cart & Order
	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Store Settings'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		Set @sql =N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''Store Settings'', @Sequence, 1, ''ccf96e1d-84db-46e3-b79e-c7392061219b'')'
		exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
	END

	UPDATE STSettingType SET FriendlyName = 'Store admin email (comma delimited)',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Textbox',
		IsVisible = 1
	WHERE Name = 'StoreAdminEmail'
	
	-- Cart & Order
	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Cart & Order'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		Set @sql =N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''Cart & Order'', @Sequence, 1, ''ccf96e1d-84db-46e3-b79e-c7392061219b'')'
		exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
	END

	UPDATE STSettingType SET FriendlyName = 'Abandoned Cart time',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'AbandonedCartTime'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'BackOrderSupport'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'CreateOrderNumberInSequence'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'Textbox',
		IsVisible = 1
	WHERE Name = 'OrderNumberPrefix'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'Textbox',
		IsVisible = 1
	WHERE Name = 'RMACodePrefix'	

	-- Shipper Settings
	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Shipper Settings'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		Set @sql =N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''Shipper Settings'', @Sequence, 1, ''ccf96e1d-84db-46e3-b79e-c7392061219b'')'
		exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
	END

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'FedexKey'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'FedexPassword'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'FedexAccountNumber'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'FedexMeterNumber'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'UPSLicenseNumber'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'UPSPassword'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'UPSUserName'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'UPSShipperNumber'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'UPSShippingProdURL'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'UPSShippingDevURL'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'USPSDevelopmentURL'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'USPSProductionURL'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'USPSUserName'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'USPSPassword'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'UnitOfMeasureLength'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'UnitOfMeasureWeight'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'DefaultProductLength'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'DefaultProductWidth'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'DefaultProductHeight'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'DefaultProductWeight'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'FreeShippingOptionId'

	-- Payment Settings
	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Payment Settings'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		Set @sql =N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''Payment Settings'', @Sequence, 1, ''ccf96e1d-84db-46e3-b79e-c7392061219b'')'
		exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
	END

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'CurrencyISOCode'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'CurrencySymbol'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'AuthorizeNetAPILogin'

	UPDATE STSettingType SET FriendlyName = NULL,
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'AuthorizeNetTransactionKey'

	UPDATE STSettingType SET FriendlyName = 'CyberSource Merchant ID',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'cybs.merchantID'

	UPDATE STSettingType SET FriendlyName = 'CyberSource Transaction Key',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'cybs.transactionKey'

	UPDATE STSettingType SET FriendlyName = 'CyberSource Secret Key',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'cybs.secretKey'

	UPDATE STSettingType SET FriendlyName = 'CyberSource Access Key',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'cybs.accessKey'

	UPDATE STSettingType SET FriendlyName = 'CyberSource Profile ID',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'cybs.profileId'

	UPDATE STSettingType SET FriendlyName = 'Payflow Pro Is Sandbox?',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'PayFlowPro.IsSandbox'

	UPDATE STSettingType SET FriendlyName = 'Payflow Pro Sandbox Url',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PayFlowPro.SandboxUrl'

	UPDATE STSettingType SET FriendlyName = 'Payflow Pro Production Url',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PayFlowPro.ProductionUrl'

	UPDATE STSettingType SET FriendlyName = 'Payflow Pro Vendor',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PayFlowPro.Vendor'

	UPDATE STSettingType SET FriendlyName = 'Payflow Pro Partner',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PayFlowPro.Partner'

	UPDATE STSettingType SET FriendlyName = 'Payflow Pro Merchant User',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PayFlowPro.MerchantUser'

	UPDATE STSettingType SET FriendlyName = 'Payflow Pro Password',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PayFlowPro.Password'

	UPDATE STSettingType SET FriendlyName = 'Paypal API Sandbox Host',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PaypalAPISandboxHost'

	UPDATE STSettingType SET FriendlyName = 'Paypal API Sandbox EndPoint URL',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PaypalAPISandboxEndPointURL'

	UPDATE STSettingType SET FriendlyName = 'Paypal API LiveHost',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PaypalAPILiveHost'

	UPDATE STSettingType SET FriendlyName = 'Paypal API Live EndPoint URL',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PaypalAPILiveEndPointURL'

	UPDATE STSettingType SET FriendlyName = 'Paypal API User Name',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PaypalAPIUserName'

	UPDATE STSettingType SET FriendlyName = 'Paypal API Password',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PaypalAPIPassword'

	UPDATE STSettingType SET FriendlyName = 'Paypal API Signature',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PaypalAPISignature'

	UPDATE STSettingType SET FriendlyName = 'Paypal ExpressCheckout Return URL',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PaypalExpressCheckoutReturnUrl'

	UPDATE STSettingType SET FriendlyName = 'Paypal ExpressCheckout Cancel URL',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'PaypalExpressCheckoutCancelUrl'

	UPDATE STSettingType SET FriendlyName = 'Website Payment Pro Api Environment',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'WPPApiEnvironment'

	UPDATE STSettingType SET FriendlyName = 'Website Payment Pro Api Username',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'WPPApiUsername'

	UPDATE STSettingType SET FriendlyName = 'Website Payment Pro Api Password',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'WPPApiPassword'

	UPDATE STSettingType SET FriendlyName = 'Website Payment Pro Api Signature',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'WPPSignature'

	-- B2B Settings
	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'B2B Settings'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		Set @sql =N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''B2B Settings'', @Sequence, 1, ''ccf96e1d-84db-46e3-b79e-c7392061219b'')'
		exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
	END

	UPDATE STSettingType SET FriendlyName = 'Allow Multiple Logins',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'AllowMultipleLoginsPerCustomer'

	UPDATE STSettingType SET FriendlyName = 'Require Customer Line of Credit',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'ReqCustomerLineOfCredit'

	UPDATE STSettingType SET FriendlyName = 'Allow On Account Payment',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'AllowOnAccountPayment'

	-- Tax Settings
	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Tax Settings'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		Set @sql =N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''Tax Settings'', @Sequence, 1, ''ccf96e1d-84db-46e3-b79e-c7392061219b'')'
		exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
	END

	UPDATE STSettingType SET FriendlyName = 'Taxable States',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Textbox',
		IsVisible = 1
	WHERE Name = 'TaxableStates'

	UPDATE STSettingType SET FriendlyName = 'Tax Shipping & Handling',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Dropdownlist',
		IsVisible = 1,
		ControlData = '{"0" : "None", "1" : "Shipping Only", "2" : "Handling Only", "3" : "Shipping and Handling"}'
	WHERE Name = 'TaxShippingHandling'

	UPDATE STSettingType SET FriendlyName = 'Seller VAT Registration Number',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Textbox',
		IsVisible = 1
	WHERE Name = 'SellerVATRegistration'

	/****************** MARKETIER STARTS ****************************/

	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Email Settings'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		Set @sql = N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''Email Settings'', @Sequence, 1, ''CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E'')'
		exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
	END

	UPDATE STSettingType SET FriendlyName = 'Unsubscribe Page Id',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'UnsubscribePageId'

	UPDATE STSettingType SET FriendlyName = 'Allowed default filters for marketer contact list',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'Marketer.ContactList.AllowedFilters'

	UPDATE STSettingType SET FriendlyName = 'Allowed contact attributes for marketer contact list',
		SettingGroupId = @SettingGroupId,
		ControlType = 'TextBox',
		IsVisible = 1
	WHERE Name = 'Marketer.ContactList.AllowedAttributes'

	UPDATE STSettingType SET FriendlyName = 'Opt-out of All Emails',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'OptOutOfAllCampaigns'

	UPDATE STSettingType SET FriendlyName = 'Double Opt-in',
		SettingGroupId = @SettingGroupId,
		ControlType = 'Checkbox',
		IsVisible = 1
	WHERE Name = 'Marketier.EnableDoubleOptin'

	IF NOT EXISTS (Select 1 from STSettingType where Name = 'CampaignSettings.SendLimitPerMonth')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('CampaignSettings.SendLimitPerMonth', 'Maximum number of campaigns allowed for Marketer per month',  @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	END

	SET @SettingGroupId = NULL
	SELECT @SettingGroupId = Id FROM STSettingGroup WHERE Name = 'Email Processor Settings'
	IF @SettingGroupId IS NULL
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM STSettingGroup WHERE Sequence != 99
		SELECT @SettingGroupId = ISNULL(MAX(Id), 0) + 1 FROM STSettingGroup
		Set @sql = N'INSERT INTO STSettingGroup(Id, Name, Sequence, IsSystem, ProductId) VALUES (@SettingGroupId, ''Email Processor Settings'', @Sequence, 1, ''CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E'')'
		exec sp_executesql @sql,N'@SettingGroupId  int,  @Sequence int', @SettingGroupId = @SettingGroupId, @Sequence = @Sequence
	END

	IF NOT EXISTS (Select 1 from STSettingType where Name = 'EmailProcessor.SenderEmailOverride')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('EmailProcessor.SenderEmailOverride', @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	END

	IF NOT EXISTS (Select 1 from STSettingType where Name = 'EmailProcessor.ReplyToEmail')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('EmailProcessor.ReplyToEmail', @SettingGroupId, @Sequence, 'Textbox', 1, 1)
	END

	IF NOT EXISTS (Select 1 from STSettingType where Name = 'EmailProcessor.TestMode')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('EmailProcessor.TestMode', @SettingGroupId, @Sequence, 'Checkbox', 0, 1)
	END

	IF NOT EXISTS (Select 1 from STSettingType where Name = 'EmailProcessor.TestDomain')
	BEGIN
		SELECT @Sequence = ISNULL(MAX(Sequence), 0) + 1 FROM dbo.STSettingType WHERE SettingGroupId = @SettingGroupId
		INSERT INTO STSettingType(Name, SettingGroupId, Sequence, ControlType, IsEnabled, IsVisible)
		VALUES ('EmailProcessor.TestDomain', @SettingGroupId, @Sequence, 'Textbox', 0, 1)
	END
END
GO
PRINT 'Updating Coupon CreatedBy'
GO
UPDATE CPCoupon SET CreatedBy = ModifiedBy 
WHERE CreatedBy = '00000000-0000-0000-0000-000000000000' AND ModifiedBy IS NOT NULL
GO
UPDATE CPCoupon SET CreatedBy = (SELECT TOP 1 Id FROM USUser WHERE UserName='iappssystemuser')
WHERE CreatedBy = '00000000-0000-0000-0000-000000000000'
GO
UPDATE CC SET CC.CreatedBy = C.CreatedBy
FROM CPCouponCode CC
	JOIN CPCoupon C ON C.Id = CC.CouponId
WHERE CC.CreatedBy = '00000000-0000-0000-0000-000000000000'
GO
PRINT 'Add columns to PRProduct'
GO
IF(COL_LENGTH('PRProduct', 'PromoteAsNewItem') IS NULL)
BEGIN
	ALTER TABLE PRProduct ADD
		PromoteAsNewItem BIT NOT NULL DEFAULT 0,
		PromoteAsTopSeller BIT NOT NULL DEFAULT 0,
		ExcludeFromDiscounts BIT NOT NULL DEFAULT 0,
		ExcludeFromShippingPromotions BIT NOT NULL DEFAULT 0,
		Freight BIT NOT NULL DEFAULT 0,
		Refundable BIT NOT NULL DEFAULT 0,
		TaxExempt BIT NOT NULL DEFAULT 0,
		SEOH1 NVARCHAR(255) NULL,
		SEODescription NVARCHAR(MAX) NULL,
		SEOKeywords NVARCHAR(MAX) NULL,
		SEOFriendlyUrl NVARCHAR(255) NULL,
		IsShared BIT NOT NULL DEFAULT 1
END
GO

PRINT 'Add IsShared to PRProduct'
GO
IF(COL_LENGTH('PRProduct', 'IsShared') IS NULL)
BEGIN
	ALTER TABLE PRProduct ADD IsShared BIT NOT NULL DEFAULT 1
END
GO

PRINT 'Creating Function GetUniqueFacetedAttributesForFilter'
GO
IF (OBJECT_ID('GetUniqueFacetedAttributesForFilter') IS NOT NULL)
	DROP FUNCTION GetUniqueFacetedAttributesForFilter
GO
CREATE FUNCTION [dbo].[GetUniqueFacetedAttributesForFilter]
(
	@FilterId	uniqueidentifier
)
RETURNS 
@ResultAttributes TABLE
(
	Id					uniqueidentifier,
	CategoryId			int,
	AttributeDataType	nvarchar(50),
	AttributeDataTypeId uniqueidentifier,
	Title				nvarchar(50),
	Description			nvarchar(200),
	Sequence			int,
	Code				nvarchar(64),
	IsFaceted			bit,
	IsSearched			bit,
	IsSystem			bit,
	IsDisplayed			bit,
	IsEnum				bit,			
	CreatedDate			datetime,
	CreatedBy			uniqueidentifier ,
	ModifiedDate		datetime,
	ModifiedBy			uniqueidentifier,
	Status				int,
	AttributeGroupName	nvarchar(255),
	IsPersonalized		bit
)
AS
BEGIN
	INSERT INTO @ResultAttributes
	SELECT A.Id,
		AG.CategoryId,			
		A.AttributeDataType,
		A.AttributeDataTypeId,
		A.Title,
		A.Description,
		A.Sequence,
		A.Code,
		A.IsFaceted,
		A.IsSearched,
		A.IsSystem,
		A.IsDisplayed,
		A.IsEnum,	
		A.CreatedDate,
		A.CreatedBy,
		A.ModifiedDate,
		A.ModifiedBy,
		A.Status,
		AG.Title as [AttributeGroupName], 
		A.IsPersonalized
	FROM ATAttribute A 
		LEFT JOIN (SELECT CI.AttributeId, 
					MIN(CI.CategoryId) CategoryId, 
					MIN(C.Name) Title
				FROM ATAttributeCategory C
					INNER JOIN ATAttributeCategoryItem CI on C.Id = CI.CategoryId
				GROUP BY AttributeId) AG ON A.Id = AG.AttributeId  
	WHERE A.Status = dbo.GetActiveStatus() -- Select only Active Attributes
		AND A.IsFaceted =1
		AND A.Id IN (SELECT DISTINCT AttributeId 
						FROM PRProductAttribute PAT 
							INNER JOIN GetNavFilterResults(@FilterId, 205, 1, NULL) F ON PAT.ProductId = F.ProductId)
	
	RETURN
END

GO
PRINT 'Creating Function GetProductTypePath'
GO
IF (OBJECT_ID('GetProductTypePath') IS NOT NULL)
	DROP FUNCTION GetProductTypePath
GO
CREATE Function [dbo].[GetProductTypePath]
(@ProductTypeId uniqueidentifier, @SiteId uniqueidentifier)
RETURNS nvarchar(4000)
AS  
BEGIN   
declare @String varchar(8000)
set @String = ''

IF EXISTS ( Select 1 From PRProductType where Id = @ProductTypeId and SiteId = @SIteId)
BEGIN 
	SELECT @String = IsNull(P2.FriendlyName,'') + '/' + @String 
	FROM PRProductType AS P1, PRProductType AS P2
	WHERE P1.LftValue between P2.LftValue AND P2.RgtValue
	AND P1.Id = @ProductTypeId 
	AND P1.Siteid = P2.SiteId
	order by P2.LftValue desc
END
ELSE
BEGIN
	SELECT @String = IsNull(PV.FriendlyName,'') + '/' + @String 
	FROM PRProductType P1
	INNER JOIN PRProductType AS P2 ON P1.LftValue between P2.LftValue AND P2.RgtValue
	INNER JOIN PRProductTypeVariantCache AS PV ON PV.Id = P2.Id
	Where
	P1.Id = @ProductTypeId 	AND P1.Siteid = P2.SiteId
	AND PV.SiteId = @SiteId
	order by P2.LftValue desc

END
IF charindex('/',@String,0) =1
	set @String = SUBSTRING(@String,2,len(@String)-1)
Return @String
END
GO
PRINT 'Alter Function SiteList_GetCount'
GO
ALTER FUNCTION [dbo].[SiteList_GetCount](@SiteListId uniqueidentifier, @SiteListType int, @SiteId uniqueidentifier)  
RETURNS int 
BEGIN  
	DECLARE @SiteCount int, @LftValue int, @RgtValue int

	IF @SiteListId IS NOT NULL
	BEGIN
		SELECT @LftValue = LftValue, @RgtValue = RgtValue FROM SISite WHERE Id = @SiteId

		IF @SiteListType = 1
			SELECT @SiteCount = COUNT(S.Id) FROM SISite S
			WHERE  S.LftValue >= @LftValue AND S.RgtValue <= @RgtValue AND S.Status = 1
		ELSE
			SELECT @SiteCount = COUNT(S.Id) FROM SISiteListSite SL
				JOIN SISite S ON S.Id = SL.SiteId
			WHERE SL.SiteListId = @SiteListId AND S.LftValue >= @LftValue AND S.RgtValue <= @RgtValue AND S.Status = 1
	END
		
	RETURN @SiteCount
END
GO
PRINT 'Creating View vwAttributeLinkValue'
GO
IF (OBJECT_ID('vwAttributeLinkValue') IS NOT NULL)
	DROP View vwAttributeLinkValue	
GO
CREATE VIEW [dbo].[vwAttributeLinkValue]
AS 
SELECT Id, Title FROM COContent

UNION ALL

SELECT PageDefinitionId, Title FROM PageDefinition
GO
PRINT 'Creating View vwContent'
GO
IF (OBJECT_ID('vwContent') IS NOT NULL)
	DROP View vwContent	
GO
CREATE VIEW [dbo].[vwContent]
AS
SELECT C.Id,
	C.ApplicationId AS SiteId,
	C.Title,
	C.Description,
	C.ParentId,
	C.SourceContentId,
	C.XmlString,
	C.Text,
	C.TemplateId,
	X.Name AS TemplateName,
	CASE WHEN C.ObjectTypeId = 13 THEN 0 ELSE 1 END IsFreeForm,
	C.CreatedBy,
	C.CreatedDate,
	C.ModifiedBy,
	C.ModifiedDate,
	C.Status,
	C.OrderNo,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName,
	CS.VirtualPath AS FolderPath
FROM [dbo].[COContent] C
	LEFT JOIN COContentStructure CS ON C.ParentId = CS.Id
	LEFT JOIN COXMLForm X ON C.TemplateId = X.Id
	LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
WHERE C.Status != 3
	AND C.ObjectTypeId IN (7, 13)
GO
PRINT 'Creating View vwNavFilter'
GO
IF (OBJECT_ID('vwNavFilter') IS NOT NULL)
	DROP View vwNavFilter	
GO
CREATE VIEW [dbo].[vwNavFilter]
AS 
SELECT 
	QueryId AS Id,
	NavNodeId AS NavigationCategoryId,
	0 AS RelatedProductType
FROM NVNavNodeNavFilterMap

UNION ALL

SELECT
	QueryId AS Id,
	NavNodeId AS NavigationCategoryId,
	ProductRelationTypeId
FROM PRNavToFilterRel 
GO
PRINT 'Creating View vwSiteList'
GO
IF (OBJECT_ID('vwSiteList') IS NOT NULL)
	DROP View vwSiteList	
GO
CREATE VIEW [dbo].[vwSiteList]
AS 
SELECT SL.Id,
	SL.Title,
	SL.Description,
	SL.Status,
	SL.ShareType,
	S.Id AS SiteId,
	SL.ListType,
	SL.CreatedDate,
	SL.CreatedBy,
	CU.UserFullName AS CreatedByFullName,
	SL.ModifiedDate,
	SL.ModifiedBy,
	MU.UserFullName AS ModifiedByFullName,
	[dbo].[SiteList_GetCount](SL.Id, SL.ListType, S.Id) AS SiteCount
FROM SISiteList SL
	INNER JOIN SISite S ON SL.SiteId = S.Id
	LEFT JOIN VW_UserFullName CU on CU.UserId = SL.CreatedBy
	LEFT JOIN VW_UserFullName MU on MU.UserId = SL.ModifiedBy
WHERE SL.ShareType = 0 AND SL.Status != 3

UNION ALL

SELECT SL.Id,
	SL.Title,
	SL.Description,
	SL.Status,
	SL.ShareType,
	CS.Id AS SiteId,
	SL.ListType,
	SL.CreatedDate,
	SL.CreatedBy,
	CU.UserFullName AS CreatedByFullName,
	SL.ModifiedDate,
	SL.ModifiedBy,
	MU.UserFullName AS ModifiedByFullName,
	[dbo].[SiteList_GetCount](SL.Id, SL.ListType, CS.Id) AS SiteCount
FROM SISiteList SL
	CROSS APPLY SISite CS
	LEFT JOIN VW_UserFullName CU on CU.UserId = SL.CreatedBy
	LEFT JOIN VW_UserFullName MU on MU.UserId = SL.ModifiedBy
WHERE SL.ShareType = 1 AND SL.Status != 3 AND CS.Status = 1

UNION ALL

SELECT SL.Id,
	SL.Title,
	SL.Description,
	SL.Status,
	SL.ShareType,
	CS.Id AS SiteId,
	SL.ListType,
	SL.CreatedDate,
	SL.CreatedBy,
	CU.UserFullName AS CreatedByFullName,
	SL.ModifiedDate,
	SL.ModifiedBy,
	MU.UserFullName AS ModifiedByFullName,
	[dbo].[SiteList_GetCount](SL.Id, SL.ListType, CS.Id) AS SiteCount
FROM SISiteList SL
	CROSS APPLY SISite CS
	INNER JOIN SISite S ON SL.SiteId = S.Id
	LEFT JOIN VW_UserFullName CU on CU.UserId = SL.CreatedBy
	LEFT JOIN VW_UserFullName MU on MU.UserId = SL.ModifiedBy
WHERE SL.ShareType = 2 AND SL.Status != 3 AND CS.Status = 1
	AND CS.LftValue >= S.LftValue AND CS.RgtValue <= S.RgtValue AND S.MasterSiteId = CS.MasterSiteId
GO
PRINT 'Creating View vwUserContactList'
GO
IF (OBJECT_ID('vwUserContactList') IS NOT NULL)
	DROP View vwUserContactList	
GO
CREATE VIEW [dbo].[vwUserContactList]
AS 
SELECT DL.Id,
	DL.Title,
	DL.Description,
	DL.ListType,
	DL.IsGlobal,
	DL.TrackingId,
	DL.Status,
	DL.IsSystem,
	DL.Priority,
	DL.ApplicationId AS SiteId,
	DL.CreatedDate,
	DL.CreatedBy,
	CU.UserFullName AS CreatedByFullName,
	DL.ModifiedDate,
	DL.ModifiedBy,
	MU.UserFullName AS ModifiedByFullName,
	U.UserId
FROM TADistributionLists DL
	INNER JOIN TADistributionListUser U ON DL.Id = U.DistributionListId
	LEFT JOIN VW_UserFullName CU on CU.UserId = DL.CreatedBy
	LEFT JOIN VW_UserFullName MU on MU.UserId = DL.ModifiedBy
WHERE ContactListSubscriptionType != 3
GO
PRINT 'Creating View vwSessionListFilter'
GO
IF (OBJECT_ID('vwSessionListFilter') IS NOT NULL)
	DROP View vwSessionListFilter	
GO
CREATE VIEW [dbo].[vwSessionListFilter]
AS 
SELECT SL.Id,
	DL.Applicationid AS SiteId,
	DL.Id AS ContactListId,
	DL.Priority,
	SL.CountryId,
	SL.StateId,
	ISNULL(SL.StateName, S.State) AS StateName,
	S.StateCode,
	C.CountryName,
	C.CountryCode,
	SL.City,
	SL.Zip,
	SL.Radius,
	SL.GeoLocation.Lat Latitude, 
	SL.GeoLocation.Long Longitude,
	SL.RequestParamKey,
	SL.RequestParamValue,
	SL.Type,
	SL.SortOrder,
	SL.CreatedBy,
	SL.CreatedDate
FROM TASessionListDetail SL
	INNER JOIN TADistributionLists DL ON SL.DistributionListId = DL.Id
	LEFT JOIN GLState S ON S.Id = SL.StateId 
	LEFT JOIN GLCountry C ON C.Id = SL.CountryId 
GO
PRINT 'Creating View vwContactListFilter'
GO
IF (OBJECT_ID('vwContactListFilter') IS NOT NULL)
	DROP View vwContactListFilter	
GO
CREATE VIEW [dbo].[vwContactListFilter]
AS 
SELECT S.Id,
	DLS.DistributionListId AS ContactListId,
	S.SearchXml,
	S.Status,
	S.CreatedBy,
	S.CreatedDate,
	S.ModifiedBy,
	S.ModifiedDate
FROM CTSearchQuery S
	JOIN TADistributionListSearch DLS ON S.Id = DLS.SearchQueryId
GO
PRINT 'Creating View vwContactList'
GO
IF (OBJECT_ID('vwContactList') IS NOT NULL)
	DROP View vwContactList	
GO
CREATE VIEW [dbo].[vwContactList]
AS 
SELECT DL.Id,
	DL.Title,
	DL.Description,
	DL.ListType,
	DL.IsGlobal,
	DL.TrackingId,
	DL.Status,
	DL.IsSystem,
	DL.Priority,
	DL.ApplicationId AS SiteId,
	DLG.DistributionGroupId AS ContactListGroupId,
	DG.Title AS GroupName,
	DL.CreatedDate,
	DL.CreatedBy,
	CU.UserFullName AS CreatedByFullName,
	DL.ModifiedDate,
	DL.ModifiedBy,
	MU.UserFullName AS ModifiedByFullName,
	TLS.Count AS TotalContacts,
	TLS.LastSynced AS LastCountUpdated
FROM TADistributionLists DL
	INNER JOIN SISite S ON DL.ApplicationId = S.Id
	INNER JOIN TADistributionListGroup DLG ON DL.Id = DLG.DistributionId
	INNER JOIN TADistributionGroup DG ON DG.Id = DLG.DistributionGroupId
	LEFT JOIN TADistributionListSite TLS ON TLS.DistributionListId = DL.Id AND TLS.SiteId = DL.ApplicationId
	LEFT JOIN VW_UserFullName CU on CU.UserId = DL.CreatedBy
	LEFT JOIN VW_UserFullName MU on MU.UserId = DL.ModifiedBy
WHERE DL.IsGlobal = 0

UNION ALL

SELECT DL.Id,
	DL.Title,
	DL.Description,
	DL.ListType,
	DL.IsGlobal,
	DL.TrackingId,
	DL.Status,
	DL.IsSystem,
	DL.Priority,
	CS.Id AS SiteId,
	DLG.DistributionGroupId AS ContactListGroupId,
	DG.Title AS GroupName,
	DL.CreatedDate,
	DL.CreatedBy,
	CU.UserFullName AS CreatedByFullName,
	DL.ModifiedDate,
	DL.ModifiedBy,
	MU.UserFullName AS ModifiedByFullName,
	TLS.Count AS TotalContacts,
	TLS.LastSynced AS LastCountUpdated
FROM TADistributionLists DL
	CROSS APPLY SISite CS
	INNER JOIN SISite S ON DL.ApplicationId = S.Id
	INNER JOIN TADistributionListGroup DLG ON DL.Id = DLG.DistributionId
	INNER JOIN TADistributionGroup DG ON DG.Id = DLG.DistributionGroupId
	LEFT JOIN TADistributionListSite TLS ON TLS.DistributionListId = DL.Id AND TLS.SiteId = CS.Id
	LEFT JOIN VW_UserFullName CU on CU.UserId = DL.CreatedBy
	LEFT JOIN VW_UserFullName MU on MU.UserId = DL.ModifiedBy
WHERE DL.IsGlobal = 1
	AND CS.LftValue >= S.LftValue AND CS.RgtValue <= S.RgtValue AND S.MasterSiteId = CS.MasterSiteId
GO
PRINT 'Creating View vwVersion'
GO
IF (OBJECT_ID('vwVersion') IS NOT NULL)
	DROP View vwVersion	
GO
CREATE VIEW [dbo].[vwVersion]
AS 
SELECT Id,
	ApplicationId AS SiteId,
	ObjectId,
	ObjectTypeId,
	XMLString,
	VersionNumber,
	RevisionNumber,
	Status,
	ObjectStatus,
	VersionStatus,
	Comments,
	TriggeredObjectId,
	TriggeredObjectTypeId,
	CreatedBy,
	CFN.UserFullName AS CreatedByFullName,
	CreatedDate
FROM VEVersion V
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = V.CreatedBy
GO
PRINT 'Creating View vwVersionLatest'
GO
IF (OBJECT_ID('vwVersionLatest') IS NOT NULL)
	DROP View vwVersionLatest	
GO
CREATE VIEW [dbo].[vwVersionLatest]
AS 
SELECT Id,
	ApplicationId AS SiteId,
	ObjectId,
	ObjectTypeId,
	XMLString,
	VersionNumber,
	RevisionNumber,
	Status,
	ObjectStatus,
	VersionStatus,
	Comments,
	TriggeredObjectId,
	TriggeredObjectTypeId,
	CreatedBy,
	CFN.UserFullName AS CreatedByFullName,
	CreatedDate
FROM (SELECT ROW_NUMBER() OVER (PARTITION BY ObjectId ORDER BY CreatedDate DESC) AS RowNumber, *
			FROM dbo.VEVersion) V
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = V.CreatedBy
WHERE RowNumber = 1
GO
PRINT 'Creating View vwTask'
GO
IF (OBJECT_ID('vwTask') IS NOT NULL)
	DROP View vwTask	
GO
CREATE VIEW [dbo].[vwTask]
AS 
SELECT T.Id,
	T.Title,
	T.Description,
	T.Path AS TaskPath,
	T.Parameter AS TaskParameter,
	T.TaskType,
	T.TaskStatus,
	T.ObjectId,
	T.ScheduleId,
	S.StartDate,
	S.EndDate,
	S.NextRunTime,
	S.LastRunTime,
	S.MaxOccurrence,
	S.Type AS ScheduleType,
	T.CreatedBy,
	CFN.UserFullName AS CreatedByFullName,
	T.CreatedDate,
	S.ModifiedBy,
	MFN.UserFullName AS ModifiedByFullName,
	S.ModifiedDate,
	S.ApplicationId AS SiteId
FROM TATask T
	JOIN TASchedule S ON T.ScheduleId = S.Id
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = T.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = S.ModifiedBy
GO
PRINT 'Creating View vwFormResponse'
GO
IF (OBJECT_ID('vwFormResponse') IS NOT NULL)
	DROP View vwFormResponse	
GO
CREATE VIEW [dbo].[vwFormResponse]
AS
SELECT R.Id,
	R.FormsId AS FormId,
	R.FormsResponseXML AS ResponseXml,
	R.ResponseDate,
	R.IPAddress,
	R.SiteId,
	R.UserId AS ResponseById,
	RN.UserFullName AS ResponseByFullName
FROM [dbo].[FormsResponse] R
  LEFT JOIN VW_UserFullName RN on RN.UserId = R.UserId
GO
PRINT 'Creating View vwEventLog'
GO
IF (OBJECT_ID('vwEventLog') IS NOT NULL)
	DROP View vwEventLog
GO
CREATE VIEW [dbo].[vwEventLog]
AS 
SELECT L.Id,
	L.Priority,
	L.Severity,
	dbo.ConvertTimeFromUtc(L.CreatedDate, L.SiteId) AS LogDate,
	L.MachineName,
	L.ActivityId,
	L.SiteId,
	L.Message,
	E.Stacktrace,
	E.InnerException,
	L.AdditionalInfo
FROM LGLog L
	LEFT JOIN LGExceptionLog E ON L.Id = E.LogId
GO
PRINT 'Creating View vwCouponSku'
GO
IF (OBJECT_ID('vwCouponSku') IS NOT NULL)
	DROP View vwCouponSku
GO
CREATE VIEW [dbo].[vwCouponSku]
AS 
SELECT P.Id,
	P.Title + ' (' + P.SKU + ')' AS Title,
	C.CouponId,
	C.CreatedBy,
	C.CreatedDate
FROM CPCouponSKU C
	JOIN PRProductSKU P ON C.SKUId = P.Id
WHERE C.Status = 1
GO
PRINT 'Creating View vwCouponProductType'
GO
IF (OBJECT_ID('vwCouponProductType') IS NOT NULL)
	DROP View vwCouponProductType
GO
CREATE VIEW [dbo].[vwCouponProductType]
AS 
SELECT P.Id,
	P.Title,
	C.CouponId,
	C.CreatedBy,
	C.CreatedDate
FROM CPCouponProductType C
	JOIN PRProductType P ON C.ProductTypeId = P.Id
WHERE C.Status = 1
GO
PRINT 'Creating View vwCouponProduct'
GO
IF (OBJECT_ID('vwCouponProduct') IS NOT NULL)
	DROP View vwCouponProduct
GO
CREATE VIEW [dbo].[vwCouponProduct]
AS 
SELECT P.Id,
	P.Title,
	C.CouponId,
	C.CreatedBy,
	C.CreatedDate
FROM CPCouponProduct C
	JOIN PRProduct P ON C.ProductId = P.Id
WHERE C.Status = 1
GO
PRINT 'Creating View vwCouponNavCategory'
GO
IF (OBJECT_ID('vwCouponNavCategory') IS NOT NULL)
	DROP View vwCouponNavCategory
GO
CREATE VIEW [dbo].[vwCouponNavCategory]
AS 
SELECT P.PageMapNodeId AS Id,
	P.DisplayTitle AS Title,
	C.CouponId,
	C.CreatedBy,
	C.CreatedDate
FROM CPCouponNavCategory C
	JOIN PageMapNode P ON C.NavCategoryId = P.PageMapNodeId
WHERE C.Status = 1
GO
PRINT 'Creating View vwCouponCustomerGroup'
GO
IF (OBJECT_ID('vwCouponCustomerGroup') IS NOT NULL)
	DROP View vwCouponCustomerGroup
GO
CREATE VIEW [dbo].[vwCouponCustomerGroup]
AS 
SELECT G.Id,
	G.Title,
	C.CouponId,
	C.CreatedBy,
	C.CreatedDate
FROM CPCouponCustomerGroup C
	JOIN USGroup G ON C.CustomerGroupId = G.Id
WHERE C.Status = 1
GO
PRINT 'Creating View vwCouponCustomer'
GO
IF (OBJECT_ID('vwCouponCustomer') IS NOT NULL)
	DROP View vwCouponCustomer
GO
CREATE VIEW [dbo].[vwCouponCustomer]
AS 
SELECT U.Id,
	M.Email AS Title,
	C.CouponId,
	C.CreatedBy,
	C.CreatedDate
FROM CPCouponCustomer C
	JOIN USUser U ON C.CustomerId = U.Id
	JOIN USMembership M ON M.UserId = U.Id
WHERE C.Status = 1
GO
PRINT 'Creating View vwTag'
GO
IF (OBJECT_ID('vwTag') IS NOT NULL)
	DROP View vwTag
GO
CREATE VIEW [dbo].[vwTag]
AS
SELECT T.Id,
	T.SiteId,
	T.Title,
	T.Description,
	T.ParentId,
	T.DisplayOrder,
	T.CreatedBy,
	T.CreatedDate,
	T.ModifiedBy,
	T.ModifiedDate,
	T.Status,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM COTaxonomy T
	LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
WHERE Status = 1
GO
PRINT 'Creating View vwTagObject'
GO
IF (OBJECT_ID('vwTagObject') IS NOT NULL)
	DROP View vwTagObject
GO
CREATE VIEW [dbo].[vwTagObject]
AS
SELECT T.Id,
	O.SiteId,
	T.Title,
	T.Description,
	T.ParentId,
	T.DisplayOrder,
	T.CreatedBy,
	T.CreatedDate,
	T.ModifiedBy,
	T.ModifiedDate,
	T.Status,
	O.ObjectId,
	O.SiteId AS ObjectSiteId,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM COTaxonomy T
	LEFT JOIN COTaxonomyObject O ON T.Id = O.TaxonomyId
	LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
GO
PRINT 'Creating View vwOrderTaxNotes'
GO
IF (OBJECT_ID('vwOrderTaxNotes') IS NOT NULL)
	DROP View vwOrderTaxNotes
GO
CREATE VIEW [dbo].[vwOrderTaxNotes]
AS 
SELECT N.Id,
	N.OrderShippingId AS ObjectId,
	N.Log AS Message,
	N.CreatedDate,
	N.CreatedBy,
	CFN.UserFullName AS CreatedByFullName
FROM ORTaxLog N
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = N.CreatedBy
GO
PRINT 'Creating View vwPaymentNotes'
GO
IF (OBJECT_ID('vwPaymentNotes') IS NOT NULL)
	DROP View vwPaymentNotes
GO
CREATE VIEW [dbo].[vwPaymentNotes]
AS 
SELECT N.Id,
	N.PaymentId AS ObjectId,
	N.Log AS Message,
	N.CreatedDate,
	N.CreatedBy,
	CFN.UserFullName AS CreatedByFullName
FROM PTPaymentLog N
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = N.CreatedBy
GO
PRINT 'Creating View vwShippingContainer'
GO
IF (OBJECT_ID('vwShippingContainer') IS NOT NULL)
	DROP View vwShippingContainer
GO
CREATE VIEW [dbo].[vwShippingContainer]
AS 
SELECT C.[ID],        
	C.[Title],        
	C.[Description],        
	C.[CreatedDate],        
	C.[CreatedBy] AS CreatedById,   
	CFN.UserFullName AS CreatedByFullName,          
	C.[ModifiedDate],        
	C.[ModifiedBy] AS ModifiedById,   
	MFN.UserFullName AS ModifiedByFullName,       
	C.[Status],
	C.[Weight],        
	C.[Length], 
	C.[Width], 
	C.[Height], 
	C.[MaxWeight], 
	C.[MaxValue], 
	C.[ShipperId], 
	C.[BoxCode], 
	C.[PackingPreference],
	C.[SiteId], 
	C.[Sequence], 
	S.Name As ShipperName      
FROM FFShippingContainer C      
	LEFT JOIN ORShipper S ON S.Id = C.ShipperId  
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = C.CreatedBy  
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
GO
PRINT 'Creating View vwProductMedia'
GO
IF (OBJECT_ID('vwProductMedia') IS NOT NULL)
	DROP View vwProductMedia
GO
CREATE VIEW [dbo].[vwProductMedia]
AS
WITH CTE AS
(
	SELECT Id, 
		SiteId, 
		Title, 
		Description,
		FileName, 
		MaxNumDownloads, 
		MaxDaysAvailable, 
		Size, 
		ExpirationDate, 
		Status, 
		ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, 
		ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy  
	FROM PRProductMedia

	UNION ALL

	SELECT Id, 
		SiteId, 
		Title, 
		Description,
		FileName, 
		MaxNumDownloads, 
		MaxDaysAvailable, 
		Size, 
		ExpirationDate, 
		Status, 
		ModifiedDate, 
		ModifiedBy
	FROM PRProductMediaVariantCache
)

SELECT C.Id,
	C.SiteId,
	M.SiteId AS SourceSiteId,
	M.ProductId,
	M.SKUId,
	C.Title,
	C.Description,
	C.FileName, 
	C.MaxNumDownloads, 
	C.MaxDaysAvailable, 
	C.Size, 
	C.ExpirationDate, 
	C.Status, 
	M.Sequence,
	M.CreatedBy,
	M.CreatedDate,
	C.ModifiedBy,
	C.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM PRProductMedia M
	JOIN CTE C ON C.Id = M.Id
	LEFT JOIN VW_UserFullName FN on FN.UserId = M.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
GO
PRINT 'Modify view vwPayment'
GO
IF(OBJECT_ID('vwPayment') IS NOT NULL)
	DROP VIEW vwPayment
GO
CREATE VIEW [dbo].[vwPayment]
AS 
SELECT P.Id,
	P.CustomerId,
	P.Amount,
	P.CapturedAmount,
	P.Status,
	P.PaymentTypeId,
	CASE P.PaymentTypeId
		WHEN 1 THEN PCC.BillingAddressId
		WHEN 2 THEN LOC.BillingAddressId
		WHEN 3 THEN PPI.BillingAddressId
		WHEN 4 THEN CODI.BillingAddressId
		WHEN 5 THEN GCI.BillingAddressId
	END AS BillingAddressId
FROM PTPayment P
	LEFT JOIN PTPaymentCreditCard AS PCC ON P.PaymentTypeId = 1 AND PCC.PaymentId = P.Id
	LEFT JOIN PTPaymentLineOfCredit AS PLOC ON P.PaymentTypeId = 2 AND PLOC.PaymentId = P.Id
	LEFT JOIN PTLineOfCreditInfo AS LOC ON P.PaymentTypeId = 2 AND LOC.Id = PLOC.LOCId
	LEFT JOIN PTPaymentPaypal AS PPP ON P.PaymentTypeId = 3 AND PPP.PaymentId = P.Id
	LEFT JOIN PTPaypalInfo AS PPI ON P.PaymentTypeId = 3 AND PPI.Id = PPP.PaypalInfoId
	LEFT JOIN PTPaymentCOD AS PCOD ON P.PaymentTypeId = 4 AND PCOD.PaymentId = P.Id
	LEFT JOIN PTCODInfo AS CODI ON P.PaymentTypeId = 4 AND CODI.Id = PCOD.CODId
	LEFT JOIN PTPaymentGiftCard AS PGC ON P.PaymentTypeId = 5 AND PGC.PaymentId = P.Id
	LEFT JOIN PTGiftCardInfo AS GCI ON P.PaymentTypeId = 5 AND GCI.Id = PGC.GiftCardId
GO
PRINT 'Modify view vwAddress'
GO
IF(OBJECT_ID('vwAddress') IS NOT NULL)
	DROP VIEW vwAddress
GO
CREATE VIEW [dbo].[vwAddress]
AS 
SELECT	A.Id, A.Id AS RecordId, AO.ObjectId, AO.ObjectType, A.FirstName, A.LastName, A.AddressType, A.AddressLine1, A.AddressLine2, A.AddressLine3,
		A.City, A.StateId, A.Zip, A.CountryId, A.Phone, A.CreatedDate, A.CreatedBy, A.ModifiedDate, A.ModifiedBy, A.[Status],
		A.StateName, A.County, A.Latitude, A.Longitude, '' AS NickName, 0 AS IsPrimary
FROM	GLAddress AS A
		LEFT JOIN GLAddressObject AS AO ON AO.AddressId = A.Id
WHERE	A.[Status] != 3
GO
PRINT 'Creating View vwOrderShippingAddress'
GO
IF (OBJECT_ID('vwOrderShippingAddress') IS NOT NULL)
	DROP View vwOrderShippingAddress
GO
CREATE VIEW [dbo].[vwOrderShippingAddress]
AS 
SELECT	A.Id, OSA.Id AS RecordId, OS.Id AS ObjectId, 213 AS ObjectType, A.FirstName, A.LastName, A.AddressType, A.AddressLine1, A.AddressLine2, A.AddressLine3,
		A.City, A.StateId, A.Zip, A.CountryId, A.Phone, A.CreatedDate, A.CreatedBy, A.ModifiedDate, A.ModifiedBy, OSA.[Status],
		A.StateName, A.County, A.Latitude, A.Longitude, '' AS NickName, 0 AS IsPrimary
FROM	GLAddress AS A
		INNER JOIN OROrderShippingAddress AS OSA ON OSA.AddressId = A.Id
		INNER JOIN OROrderShipping AS OS ON OS.OrderShippingAddressId = OSA.Id
WHERE	A.[Status] != 3 AND
		OSA.[Status] != 3
GO
PRINT 'Creating View vwPaymentAddress'
GO
IF (OBJECT_ID('vwPaymentAddress') IS NOT NULL)
	DROP View vwPaymentAddress
GO
CREATE VIEW [dbo].[vwPaymentAddress]
AS 
SELECT	A.Id, A.Id AS RecordId, P.Id AS ObjectId, 234 AS ObjectType, A.FirstName, A.LastName, A.AddressType, A.AddressLine1, A.AddressLine2, A.AddressLine3,
		A.City, A.StateId, A.Zip, A.CountryId, A.Phone, A.CreatedDate, A.CreatedBy, A.ModifiedDate, A.ModifiedBy, A.[Status],
		A.StateName, A.County, A.Latitude, A.Longitude, '' AS NickName, 0 AS IsPrimary
FROM	GLAddress AS A
		INNER JOIN vwPayment AS P ON P.BillingAddressId = A.Id
WHERE	A.[Status] != 3
GO
PRINT 'Creating View vwUserShippingAddress'
GO
IF (OBJECT_ID('vwUserShippingAddress') IS NOT NULL)
	DROP View vwUserShippingAddress
GO
CREATE VIEW [dbo].[vwUserShippingAddress]
AS 
SELECT	A.Id, USA.Id AS RecordId, USA.UserId AS ObjectId, 211 AS ObjectType, A.FirstName, A.LastName, A.AddressType, A.AddressLine1, A.AddressLine2, A.AddressLine3,
		A.City, A.StateId, A.Zip, A.CountryId, A.Phone, A.CreatedDate, A.CreatedBy, A.ModifiedDate, A.ModifiedBy, USA.[Status],
		A.StateName, A.County, A.Latitude, A.Longitude, USA.NickName, USA.IsPrimary
FROM	GLAddress AS A
		INNER JOIN USUserShippingAddress AS USA ON USA.AddressId = A.Id
WHERE	A.[Status] != 3 AND
		USA.[Status] != 3
GO
PRINT 'Creating View vwWarehouseAddress'
GO
IF (OBJECT_ID('vwWarehouseAddress') IS NOT NULL)
	DROP View vwWarehouseAddress
GO
CREATE VIEW [dbo].[vwWarehouseAddress]
AS 
SELECT	A.Id, A.Id AS RecordId, WH.Id AS ObjectId, 234 AS ObjectType, A.FirstName, A.LastName, A.AddressType, A.AddressLine1, A.AddressLine2, A.AddressLine3,
		A.City, A.StateId, A.Zip, A.CountryId, A.Phone, A.CreatedDate, A.CreatedBy, A.ModifiedDate, A.ModifiedBy, A.[Status],
		A.StateName, A.County, A.Latitude, A.Longitude, '' AS NickName, 0 AS IsPrimary
FROM	GLAddress AS A
		INNER JOIN INWarehouse AS WH ON WH.AddressId = A.Id
WHERE	A.[Status] != 3
GO
PRINT 'Creating View vwContactAddress'
GO
IF (OBJECT_ID('vwContactAddress') IS NOT NULL)
	DROP View vwContactAddress
GO
CREATE VIEW [dbo].[vwContactAddress]
AS 
SELECT	A.Id, 
	A.Id AS RecordId,
	C.Id AS ObjectId, 
	302 AS ObjectType,
	A.FirstName, 
	A.LastName, 
	A.AddressType, 
	A.AddressLine1, 
	A.AddressLine2, 
	A.AddressLine3,
	A.City, 
	A.StateId, 
	A.Zip, 
	A.CountryId, 
	A.Phone, 
	A.CreatedDate, 
	A.CreatedBy, 
	A.ModifiedDate, 
	A.ModifiedBy, 
	A.[Status],
	A.StateName, 
	A.County, 
	A.Latitude, 
	A.Longitude
FROM GLAddress AS A
	INNER JOIN vw_contacts AS C ON C.AddressId = A.Id
GO
PRINT 'Creating View vwAttributeValue'
GO
IF (OBJECT_ID('vwAttributeValue') IS NOT NULL)
	DROP View vwAttributeValue
GO
CREATE VIEW [dbo].[vwAttributeValue]
AS 
SELECT V.Id,
	V.ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	V.Value,
	V.CreatedBy,
	V.CreatedDate
FROM ATAttributeValue V
GO
PRINT 'Creating View vwOrderAttributeValues'
GO
IF (OBJECT_ID('vwOrderAttributeValues') IS NOT NULL)
	DROP View vwOrderAttributeValues
GO
CREATE VIEW [dbo].[vwOrderAttributeValues]
AS
SELECT	OAV.Id, OAV.OrderId, OAV.Value, OAV.CreatedBy, OAV.CreatedDate, CFN.UserFullName AS CreatedByFullName, OAV.ModifiedBy, OAV.ModifiedDate, MFN.UserFullName AS ModifiedByFullName,
		OAV.AttributeId, A.Title AS AttributeTitle, A.AttributeDataType, AG.CategoryId,
		OAV.AttributeEnumId, AE.Value AS AttributeEnumValue, AE.Title AS AttributeEnumTitle
FROM	OROrderAttributeValue AS OAV
		INNER JOIN ATAttribute AS A ON A.Id = OAV.AttributeId
		LEFT JOIN ATAttributeEnum AS AE ON AE.Id = OAV.AttributeEnumId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = OAV.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = OAV.ModifiedBy
		LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
GO
IF (OBJECT_ID('VWOrderItemPersonalizedAttribute') IS NOT NULL)
	DROP View VWOrderItemPersonalizedAttribute
GO
CREATE VIEW [dbo].[VWOrderItemPersonalizedAttribute]
AS
(
SELECT   V.OrderId,V.OrderItemId,V.SKUId,P.Id as ProductId,V.CustomerId, P.Title AS ProductName,A.Id as AttributeId, A.Title AS AttributeName, 
                      AttributeEnumId,Isnull(AE.Value,V.Value ) AS Value,AE.Code,AE.NumericValue,AE.Sequence,AE.IsDefault, V.CartId,V.CartItemID,
					AG.CategoryId,A.IsFaceted,A.IsPersonalized,A.IsSearched,A.IsSystem,A.IsDisplayed,A.IsEnum,A.AttributeDataTypeId
FROM         dbo.PRProduct AS P INNER JOIN
                      dbo.OROrderItemAttributeValue AS V ON P.Id = V.ProductId INNER JOIN
                      dbo.ATAttribute AS A ON A.Id = V.AttributeId left JOIN
                      dbo.ATAttributeEnum AS AE ON V.AttributeEnumId = AE.Id
					  LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
                      Where A.IsPersonalized =1
union
SELECT    V.OrderId,V.OrderItemId,V.SKUId, P.Id as ProductId,V.CustomerId, P.Title AS ProductName,A.Id as AttributeId,A.Title AS AttributeName, 
                      AttributeEnumId,Isnull(AE.Value,V.Value ) AS Value,AE.Code,AE.NumericValue,AE.Sequence,AE.IsDefault, V.CartId,V.CartItemID
				,AG.CategoryId,A.IsFaceted,A.IsPersonalized,A.IsSearched,A.IsSystem,A.IsDisplayed,A.IsEnum,
				A.AttributeDataTypeId
FROM         dbo.PRProduct AS P INNER JOIN
					  PRProductSKU as PS on P.Id =PS.ProductId inner join
                      dbo.OROrderItemAttributeValue AS V ON PS.Id = V.SKUId INNER JOIN
                      dbo.ATAttribute AS A ON A.Id = V.AttributeId Left  JOIN
                      dbo.ATAttributeEnum AS AE ON V.AttributeEnumId = AE.Id
					  LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
Where A.IsPersonalized =1
)

GO

PRINT 'Creating View vwProductTypePath'
GO
IF (OBJECT_ID('vwProductTypePath') IS NOT NULL)
	DROP View vwProductTypePath
GO
CREATE VIEW [dbo].[vwProductTypePath]
AS
SELECT     Id AS ProductTypeId, Title As ProductTypeName, dbo.GetProductTypePath(Id, SiteId) AS ProductTypePath, ISNULL(IsDownloadableMedia,0) AS IsDownloadableMedia
FROM         dbo.PRProductType
GO

PRINT 'Creating View vwCustomerAttributeValue'
GO
IF (OBJECT_ID('vwCustomerAttributeValue') IS NOT NULL)
	DROP View vwCustomerAttributeValue
GO
CREATE VIEW [dbo].[vwCustomerAttributeValue]
AS 
SELECT V.Id,
	V.CustomerId AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	V.Value,
	V.CreatedBy,
	V.CreatedDate
FROM CSCustomerAttributeValue V
GO
PRINT 'Creating View vwCustomerLogin'
GO
IF (OBJECT_ID('vwCustomerLogin') IS NOT NULL)
	DROP View vwCustomerLogin
GO
CREATE VIEW [dbo].[vwCustomerLogin]
AS         
SELECT	U.Id, 
	CL.CustomerId,
	U.FirstName, 
	U.LastName, 
	U.UserName, 
	NULL AS [Password], 
	M.Email, 
	M.IsApproved,
	M.IsLockedOut,
	M.PasswordQuestion AS SecurityQuestion,
	M.PasswordAnswer AS SecurityAnswer,
	M.LastLoginDate,
	U.CreatedDate,  
	U.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	ISNULL(U.ModifiedDate, U.CreatedDate) AS ModifiedDate, 
	ISNULL(U.ModifiedBy, U.CreatedBy) AS ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName
FROM USUser AS U 
	INNER JOIN CSCustomerLogin CL ON CL.UserId = U.Id
	INNER JOIN USMembership AS M ON M.UserId = U.Id
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = U.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = U.ModifiedBy
WHERE	U.[Status] !=3

GO      



PRINT 'Creating View vwContactAttributeValue'
GO
IF (OBJECT_ID('vwContactAttributeValue') IS NOT NULL)
	DROP View vwContactAttributeValue
GO
CREATE VIEW [dbo].[vwContactAttributeValue]
AS 
SELECT V.Id,
	V.ContactId AS ObjectId,
	V.AttributeId, 
	V.AttributeEnumId, 
	V.Value,
	V.CreatedBy,
	V.CreatedDate
FROM ATContactAttributeValue V

GO
PRINT 'Creating View vwOrderAttributeValue'
GO
IF (OBJECT_ID('vwOrderAttributeValue') IS NOT NULL)
	DROP View vwOrderAttributeValue
GO

CREATE VIEW [dbo].[vwOrderAttributeValue]
AS
SELECT A.Id, 
	O.SiteId,
	O.Id AS ObjectId,
	A.AttributeId, 
	A.AttributeEnumId, 
	A.Value,
	A.CreatedBy,
	A.CreatedDate
FROM OROrderAttributeValue A
	JOIN OROrder O ON O.Id = A.OrderId
GO
PRINT 'Creating View vwOrderItemAttributeValue'
GO
IF (OBJECT_ID('vwOrderItemAttributeValue') IS NOT NULL)
	DROP View vwOrderItemAttributeValue
GO

CREATE VIEW [dbo].[vwOrderItemAttributeValue]
AS
SELECT A.OrderItemAttributeValueId AS Id, 
	O.SiteId,
	O.Id AS ObjectId,
	A.AttributeId, 
	A.AttributeEnumId, 
	A.Value,
	A.CreatedBy,
	A.CreatedDate
FROM OROrderItemAttributeValue A
	JOIN OROrder O ON O.Id = A.OrderId
GO


PRINT 'Add columns to PRProductSKU'
GO

IF(COL_LENGTH('PRProductSKU', 'FreeShipping') IS NULL)
BEGIN
	ALTER TABLE PRProductSKU ADD
		FreeShipping		BIT NULL,
		UserDefinedPrice	BIT NULL
END
GO

PRINT 'Modify columns on PRProductSKU'
GO
IF(OBJECT_ID('DF_PRProductSKU_Description') IS NOT NULL)
	ALTER TABLE PRProductSKU DROP CONSTRAINT [DF_PRProductSKU_Description]
	ALTER TABLE PRProductSKU ALTER COLUMN [Description] NVARCHAR(MAX) NULL
GO

PRINT 'Updating Bundle tables'
GO
IF(COL_LENGTH('PRBundleItem', 'ChildSkuId') IS NULL)
BEGIN
	ALTER TABLE PRBundleItem DROP Column ModifiedBy
	
	ALTER TABLE PRBundleItem ADD
		ChildSkuId UNIQUEIDENTIFIER NULL,
		ModifiedBy UNIQUEIDENTIFIER NULL
declare @sql Nvarchar(max)

Set @sql =	'UPDATE BI
	SET BI.ChildSkuId = BS.ChildSkuId
	FROM PRBundleItem BI
		JOIN PRBundleItemSku BS ON BI.Id = BS.BundleItemId'
exec (@sql)
END
GO

PRINT 'Modify columns on ATAttributeCategory'
GO
IF COL_LENGTH('dbo.ATAttributeCategory', 'IsSystem') IS NULL
BEGIN 
	ALTER TABLE ATAttributeCategory
	ADD 
		[SiteId] [uniqueidentifier],
		[IsSystem] [bit],
		[CreatedDate] [datetime],
		[CreatedBy] [uniqueidentifier],
		[ModifiedDate] [datetime],
		[ModifiedBy] [uniqueidentifier],
		[Status] [int],
		[IsGlobal] [bit],
		[GroupId]  [uniqueidentifier]
END
GO

IF COL_LENGTH('dbo.ATAttributeCategory', 'IsSystem') IS NOT NULL
BEGIN
	UPDATE ATAttributeCategory SET IsSystem = 1
END
GO
UPDATE ATAttributeCategory SET Status = 1 WHERE Status IS NULL
GO

PRINT 'Modify columns on ATPageAttributeValue'
GO
IF COL_LENGTH('dbo.ATPageAttributeValue', 'IsShared') IS NULL
	ALTER TABLE ATPageAttributeValue 
	ADD 
		IsShared bit,
		IsEditable bit

GO

IF COL_LENGTH('dbo.ATPageAttributeValue', 'IsShared') IS NOT NULL AND
	EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ATAttributeobject')
BEGIN
	UPDATE V SET V.IsShared = O.IsShared, V.IsEditable = O.IsEditable
	FROM ATAttributeobject O
		JOIN ATPageAttributeValue V ON V.PageDefinitionId = O.ObjectId
END
GO

PRINT 'Modify columns on ATSiteAttributeValue'
GO
IF COL_LENGTH('dbo.ATSiteAttributeValue', 'IsShared') IS NULL 
	ALTER TABLE ATSiteAttributeValue 
	ADD 
		IsShared bit,
		IsEditable bit
GO

IF COL_LENGTH('dbo.ATSiteAttributeValue', 'IsShared') IS NOT NULL AND
	EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ATAttributeobject')
BEGIN 
	UPDATE V SET V.IsShared = O.IsShared, V.IsEditable = O.IsEditable
	FROM ATAttributeobject O
		JOIN ATSiteAttributeValue V ON V.SiteId = O.ObjectId
END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE name = N'FK_ATAttribute_ATAttributeGroup')
	ALTER TABLE ATAttribute DROP CONSTRAINT FK_ATAttribute_ATAttributeGroup
GO
PRINT 'Add columns to PRProduct'
GO

IF(COL_LENGTH('PRProduct', 'PromoteAsNewItem') IS NULL)
BEGIN
	ALTER TABLE PRProduct ADD
		PromoteAsNewItem BIT NOT NULL DEFAULT 0,
		PromoteAsTopSeller BIT NOT NULL DEFAULT 0,
		ExcludeFromDiscounts BIT NOT NULL DEFAULT 0,
		ExcludeFromShippingPromotions BIT NOT NULL DEFAULT 0,
		Freight BIT NOT NULL DEFAULT 0,
		Refundable BIT NOT NULL DEFAULT 0,
		TaxExempt BIT NOT NULL DEFAULT 0,
		SEOH1 NVARCHAR(255) NULL,
		SEODescription NVARCHAR(MAX) NULL,
		SEOKeywords NVARCHAR(MAX) NULL,
		SEOFriendlyUrl NVARCHAR(255) NULL
END
GO

PRINT 'Modify columns on PRProductVariant'
GO

IF COL_LENGTH('PRProductVariant', 'CreatedBy') IS NOT NULL
BEGIN
	ALTER TABLE		PRProductVariant
	DROP CONSTRAINT	DF_PRProductVariant_CreatedDate

	ALTER TABLE	PRProductVariant
	DROP COLUMN	ProductIdUser, CreatedDate, CreatedBy, IsBundle, BundleCompositionLastModified, [Status]

	ALTER TABLE	PRProductVariant
	ADD			[PromoteAsNewItem] BIT NULL, 
				[PromoteAsTopSeller] BIT NULL, 
				[ExcludeFromDiscounts] BIT NULL,
				[ExcludeFromShippingPromotions] BIT NULL,
				[Freight] BIT NULL, 
				[Refundable] BIT NULL, 
				[TaxExempt] BIT NULL,
				[SEOH1] NVARCHAR(255) NULL,
				[SEODescription] NVARCHAR(MAX) NULL,
				[SEOKeywords] NVARCHAR(MAX) NULL,
				[SEOFriendlyUrl] NVARCHAR(255) NULL

	ALTER TABLE		PRProductVariant
	ALTER COLUMN	ShortDescription NVARCHAR(MAX) NULL


	ALTER TABLE [dbo].[PRProductVariant] ADD  CONSTRAINT [PK_PRProductVariant] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC,
		[SiteId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


END
GO

PRINT 'Modify columns on PRProductSkuVariant'
GO

IF COL_LENGTH('PRProductSkuVariant', 'CreatedBy') IS NOT NULL
BEGIN
	DROP INDEX IX_PRProductSKUVariant ON PRProductSkuVariant

	ALTER TABLE PRProductSkuVariant
	DROP CONSTRAINT FK_PRProductSKUVariant_PRProduct

	ALTER TABLE	PRProductSkuVariant
	DROP COLUMN	SKU, ProductId, [Length], [Width], [Height], [Weight], CreatedDate, CreatedBy, [Status]

	ALTER TABLE	PRProductSkuVariant
	ADD			FreeShipping		BIT NULL, 
				UserDefinedPrice	BIT NULL

	ALTER TABLE		PRProductSkuVariant
	ALTER COLUMN	SiteId UNIQUEIDENTIFIER NOT NULL

	ALTER TABLE		PRProductSkuVariant
	ALTER COLUMN	[Description] NVARCHAR(MAX) NULL

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE name = N'PK_PRProductSKUVariant')
	ALTER TABLE PRProductSkuVariant DROP CONSTRAINT PK_PRProductSKUVariant
GO
	ALTER TABLE		PRProductSkuVariant
	ADD	CONSTRAINT	PK_PRProductSKUVariant PRIMARY KEY ([SiteId], [Id])
GO

PRINT 'Creating table LGOrderNotes'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'LGOrderNotes'))
BEGIN
	CREATE TABLE [dbo].[LGOrderNotes]
	(
		[Id] UNIQUEIDENTIFIER NOT NULL , 
		[OrderId] UNIQUEIDENTIFIER NOT NULL, 
		[Message] XML NOT NULL, 
		[CreatedBy] UNIQUEIDENTIFIER NOT NULL, 
		[CreatedDate] DATETIME NOT NULL, 
		CONSTRAINT [PK_LGOrderNotes] PRIMARY KEY ([Id])
	)
END
GO
PRINT 'Creating table LGCustomerNotes'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'LGCustomerNotes'))
BEGIN
	CREATE TABLE [dbo].[LGCustomerNotes]
	(
		[Id] UNIQUEIDENTIFIER NOT NULL , 
		[CustomerId] UNIQUEIDENTIFIER NOT NULL, 
		[Message] XML NOT NULL, 
		[CreatedBy] UNIQUEIDENTIFIER NOT NULL, 
		[CreatedDate] DATETIME NOT NULL, 
		CONSTRAINT [PK_LGCustomerNotes] PRIMARY KEY ([Id])
	)
END
GO
PRINT 'Creating table ATAttributeEnumVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ATAttributeEnumVariant'))
BEGIN
CREATE TABLE [dbo].[ATAttributeEnumVariant](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[AttributeId] [uniqueidentifier] NULL,
	[Title] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ATAttributeEnumVariant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[SiteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

PRINT 'Creating table ATAttributeEnumVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ATAttributeEnumVariantCache'))
BEGIN
CREATE TABLE [dbo].[ATAttributeEnumVariantCache](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[AttributeId] [uniqueidentifier] NULL,
	[Title] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [uniqueidentifier] NULL, 
    CONSTRAINT [PK_ATAttributeEnumVariantCache] PRIMARY KEY ([Id], [SiteId])
)
END
GO

PRINT 'Creating table PRProductAttributeValueVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PRProductAttributeValueVariant'))
BEGIN
CREATE TABLE [dbo].[PRProductAttributeValueVariant](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[ProductId] [uniqueidentifier] NOT NULL,
	[AttributeId] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](max) NULL,
	[AttributeEnumId] [uniqueidentifier] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
 CONSTRAINT [PK_PRProductAttributeValueVariant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[SiteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END
GO
PRINT 'Creating table PRProductAttributeValueVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PRProductAttributeValueVariantCache'))
BEGIN
	CREATE TABLE [dbo].[PRProductAttributeValueVariantCache](
		[Id] [uniqueidentifier] NOT NULL,
		[SiteId] [uniqueidentifier] NOT NULL,
		[ProductId] [uniqueidentifier] NOT NULL,
		[AttributeId] [uniqueidentifier] NOT NULL,
		[Value] [nvarchar](max) NULL,
		[AttributeEnumId] [uniqueidentifier] NULL,
		[ModifiedDate] [datetime] NULL,
		[ModifiedBy] [uniqueidentifier] NULL, 
		CONSTRAINT [PK_PRProductAttributeValueVariantCache] PRIMARY KEY ([Id], [SiteId])
	)

	

END
GO
PRINT 'Creating table PRProductSKUAttributeValueVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PRProductSKUAttributeValueVariant'))
BEGIN
CREATE TABLE [dbo].[PRProductSKUAttributeValueVariant](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[SkuId] [uniqueidentifier] NOT NULL,
	[AttributeId] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](max) NULL,
	[AttributeEnumId] [uniqueidentifier] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [uniqueidentifier] NULL, 
    CONSTRAINT [PK_PRProductSKUAttributeValueVariant] PRIMARY KEY ([Id], [SiteId]))
END
GO
PRINT 'Creating table PRProductSKUAttributeValueVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PRProductSKUAttributeValueVariantCache'))
BEGIN
	CREATE TABLE [dbo].[PRProductSKUAttributeValueVariantCache](
		[Id] [uniqueidentifier] NOT NULL,
		[SiteId] [uniqueidentifier] NOT NULL,
		[SkuId] [uniqueidentifier] NOT NULL,
		[AttributeId] [uniqueidentifier] NOT NULL,
		[Value] [nvarchar](max) NULL,
		[AttributeEnumId] [uniqueidentifier] NULL,
		[ModifiedDate] [datetime] NULL,
		[ModifiedBy] [uniqueidentifier] NULL, 
		CONSTRAINT [PK_PRProductSKUAttributeValueVariantCache] PRIMARY KEY ([Id], [SiteId])
	)

	
END
GO
PRINT 'Creating table ATAttributeVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ATAttributeVariant'))
BEGIN
	CREATE TABLE [dbo].[ATAttributeVariant](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ATAttributeVariant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[SiteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
END
GO
PRINT 'Creating table ATAttributeVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ATAttributeVariantCache'))
BEGIN
	CREATE TABLE [dbo].[ATAttributeVariantCache]
	(
		[Id] [uniqueidentifier] NOT NULL,
		[SiteId] uniqueidentifier NOT NULL,
		[Title] [nvarchar](max) NULL,
		[Description] [nvarchar](max) NULL,
		[ModifiedDate] [datetime] NULL,
		[ModifiedBy] [uniqueidentifier] NULL, 
		CONSTRAINT [PK_ATAttributeVariantCache] PRIMARY KEY ([Id], [SiteId])
	)

	

END
GO
PRINT 'Creating table PRProductSKUVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PRProductSKUVariantCache'))
BEGIN
	CREATE TABLE [dbo].[PRProductSKUVariantCache] (
		[Id]                        UNIQUEIDENTIFIER NOT NULL,
		[SiteId]                    UNIQUEIDENTIFIER NOT NULL,
		[Title]                     NVARCHAR (555)   NULL,
		[Description]               NVARCHAR(MAX)    NULL,
		[Sequence]                  INT              NULL,
		[IsActive]                  BIT              NULL,
		[IsOnline]                  BIT              NULL,
		[ListPrice]                 MONEY            NULL,
		[UnitCost]                  MONEY            NULL,
		[WholesalePrice]            MONEY            NULL,
		[PreviousSoldCount]         DECIMAL (18, 4)  NULL,
		[ModifiedDate]              DATETIME         NULL,
		[ModifiedBy]                UNIQUEIDENTIFIER NULL,
		[Measure]                   NVARCHAR (50)    NULL,
		[OrderMinimum]              DECIMAL (18, 4)  NULL,
		[OrderIncrement]            DECIMAL (18, 4)  NULL,
		[HandlingCharges]           DECIMAL (18, 4)  NULL,
		[SelfShippable]             BIT              NULL,
		[AvailableForBackOrder]     BIT              NULL,
		[BackOrderLimit]            DECIMAL (18)     NULL,
		[IsUnlimitedQuantity]       BIT              NULL,
		[MaximumDiscountPercentage] MONEY            NULL,
		[FreeShipping]				BIT NULL, 
		[UserDefinedPrice]			BIT NULL, 
		CONSTRAINT [PK_PRProductSKUVariantCache] PRIMARY KEY ([SiteId], [Id])
	)

	
END
GO

IF NOT EXISTS(SELECT *
          FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'PRProductType'
                 AND COLUMN_NAME = 'CMSPageId') 
Alter Table PRProductType Add CMSPageId uniqueidentifier NULL
GO


PRINT 'Creating table PRProductTypeVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PRProductTypeVariantCache'))
BEGIN
	CREATE TABLE [dbo].[PRProductTypeVariantCache](
		[Id] [uniqueidentifier] NOT NULL,
		[SiteId] [uniqueidentifier] NOT NULL,
		[Title] [nvarchar](50) NULL,
		[Description] [nvarchar](200) NULL,
		[FriendlyName] [nvarchar](50) NULL,
		[CMSPageId] [uniqueidentifier] NULL,
		[ModifiedDate] [datetime] NULL,
		[ModifiedBy] [uniqueidentifier] NULL,
		[Status] [int] NULL, 
		CONSTRAINT [PK_PRProductTypeVariantCache] PRIMARY KEY ([Id], [SiteId])
	)

	
END
GO

PRINT 'Creating View vwProductTypePathVariant'
GO
IF (OBJECT_ID('vwProductTypePathVariant') IS NOT NULL)
	DROP View vwProductTypePathVariant
GO
CREATE VIEW [dbo].[vwProductTypePathVariant]  
AS  
SELECT     Id AS ProductTypeId, Title As ProductTypeName, dbo.GetProductTypePath(Id, SiteId) AS ProductTypePath, ISNULL(IsDownloadableMedia,0) AS IsDownloadableMedia, SIteId  
FROM         dbo.PRProductType
UNION ALL 
SELECT     P.Id AS ProductTypeId, P.Title As ProductTypeName, dbo.GetProductTypePath(P.Id, P.SiteId) AS ProductTypePath, ISNULL(IsDownloadableMedia,0) AS IsDownloadableMedia , P.SiteId 
FROM         dbo.PRProductTypeVariantCache P
INNER JOIN PRProductType P1 ON P1.Id = P.Id
GO

PRINT 'Creating table PRProductVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PRProductVariantCache'))
BEGIN
	CREATE TABLE [dbo].[PRProductVariantCache] (
		[Id]                            UNIQUEIDENTIFIER NOT NULL,
		[SiteId]                        UNIQUEIDENTIFIER NOT NULL,
		[Title]                         NVARCHAR (555)   NULL,
		[UrlFriendlyTitle]              NVARCHAR (255)   NULL,
		[ShortDescription]              NVARCHAR (MAX)   NULL,
		[LongDescription]               NVARCHAR (MAX)   NULL,
		[Description]                   NVARCHAR (MAX)   NULL,
		[Keyword]                       NVARCHAR (900)   NULL,
		[IsActive]                      TINYINT          NULL,
		[ProductTypeID]                 UNIQUEIDENTIFIER NULL,
		[ProductStyle]                  NVARCHAR (50)    NULL,
		[ModifiedDate]                  DATETIME         NULL,
		[ModifiedBy]                    UNIQUEIDENTIFIER NULL,
		[TaxCategoryID]                 UNIQUEIDENTIFIER NULL,
		[PromoteAsNewItem] BIT NULL, 
		[PromoteAsTopSeller] BIT NULL, 
		[ExcludeFromDiscounts] BIT NULL,
		[ExcludeFromShippingPromotions] BIT NULL,
		[Freight] BIT NULL, 
		[Refundable] BIT NULL, 
		[TaxExempt] BIT NULL,
		[SEOH1] NVARCHAR(255) NULL,
		[SEODescription] NVARCHAR(MAX) NULL,
		[SEOKeywords] NVARCHAR(MAX) NULL,
		[SEOFriendlyUrl] NVARCHAR(255) NULL, 
		CONSTRAINT [PK_PRProductVariantCache] PRIMARY KEY ([Id], [SiteId])
	)

	
END
GO
PRINT 'Modify view vwSiteSetting'
GO
IF(OBJECT_ID('vwSiteSetting') IS NOT NULL)
	DROP VIEW vwSiteSetting
GO
CREATE VIEW [dbo].[vwSiteSetting]
AS 

SELECT DISTINCT V.Id,
	T.Name,
	T.Sequence,
	T.ControlType,
	T.FriendlyName,
	T.SettingGroupId,
	S.Id AS SiteId,
	ISNULL(V.SiteId, MV.SiteId) AS SourceSiteId,
	V.SettingTypeId,
	ISNULL(V.Value, MV.Value) AS Value,
	T.ControlData,
	T.IsEnabled
FROM STSettingType T
	CROSS APPLY SISite S 
	INNER JOIN STSettingGroup G ON G.Id = T.SettingGroupId
	LEFT JOIN STSiteSetting V ON V.SettingTypeId = T.Id AND V.SiteId = S.Id
	LEFT JOIN STSiteSetting MV ON MV.SettingTypeId = T.Id AND MV.SiteId = S.MasterSiteId
WHERE T.IsVisible = 1 AND G.IsSystem = 1

UNION

SELECT DISTINCT V.Id,
	T.Name,
	T.Sequence,
	T.ControlType,
	T.FriendlyName,
	T.SettingGroupId,
	S.Id AS SiteId,
	ISNULL(V.SiteId, MV.SiteId) AS SourceSiteId,
	V.SettingTypeId,
	ISNULL(V.Value, MV.Value) AS Value,
	T.ControlData,
	T.IsEnabled
FROM STSettingType T
	CROSS APPLY SISite S 
	INNER JOIN STSettingGroup G ON G.Id = T.SettingGroupId
	LEFT JOIN STSiteSetting V ON V.SettingTypeId = T.Id AND V.SiteId = S.Id
	LEFT JOIN STSiteSetting MV ON MV.SettingTypeId = T.Id AND MV.SiteId = S.MasterSiteId
WHERE T.IsVisible = 1 AND G.IsSystem = 0
GO
PRINT 'Modify view vwSiteSettingGroup'
GO
IF(OBJECT_ID('vwSiteSettingGroup') IS NOT NULL)
	DROP VIEW vwSiteSettingGroup
GO
CREATE VIEW [dbo].[vwSiteSettingGroup]
AS 
SELECT DISTINCT G.Id,
	G.Name,
	G.Sequence,
	G.IsSystem,
	G.ProductId
FROM STSettingGroup G
WHERE G.IsSystem = 1
	AND G.ProductId IS NOT NULL

UNION

SELECT DISTINCT G.Id,
	G.Name,
	G.Sequence,
	G.IsSystem,
	P.Id AS ProductId
FROM STSettingGroup G
	CROSS APPLY iAppsProductSuite P
WHERE G.IsSystem = 1
	AND G.ProductId IS NULL

UNION

SELECT DISTINCT G.Id,
	G.Name,
	G.Sequence,
	G.IsSystem,
	P.Id AS ProductId
FROM STSettingGroup G
	CROSS APPLY iAppsProductSuite P
WHERE G.IsSystem = 0

GO
PRINT 'Modify view vwInventoryList'
GO
IF(OBJECT_ID('vwInventoryList') IS NOT NULL)
	DROP VIEW vwInventoryList
GO
CREATE VIEW vwInventoryList
AS
SELECT I.[Id],
    I.[WarehouseId],
    I.[SKUId],
    I.[Quantity],
    I.[LowQuantityAlert],
    I.[FirstTimeOrderMinimum],
    I.[OrderMinimum],
    I.[OrderIncrement],
    I.[ReasonId],
    I.[ReasonDetail],
    I.[Status],
    I.[CreatedBy],
    I.[CreatedDate],
    I.[ModifiedBy],
    I.[ModifiedDate],
	S.ProductId,
	W.Title AS WarehouseName,
	AQ.Quantity AS AllocatedQuantity,
	BAQ.BackOrderQuantity AS BackOrderAllocatedQuantity,
	ATS.BackOrderLimit,
	ATS.Quantity AS AvailableToSell,
	ATS.SiteId
FROM INInventory I
	INNER JOIN INWarehouse W ON W.Id = I.WarehouseId
	INNER JOIN PRProductSKU S ON S.Id = I.SKUId
	LEFT JOIN dbo.vwAllocatedQuantityperWarehouse AQ ON I.SKUId = AQ.ProductSKUId AND AQ.WarehouseId = W.Id
	LEFT JOIN dbo.vwBackOrderAllocatedQuantityPerWarehouse BAQ ON I.SKUId = BAQ.ProductSKUId AND BAQ.WarehouseId = W.Id
	LEFT JOIN dbo.vwAvailableToSellPerWarehouse ATS ON I.SKUId = ATS.SKUId AND ATS.WarehouseId = W.Id
WHERE W.Status = 1
GO
PRINT 'Modify view vwBundleItem'
GO
IF(OBJECT_ID('vwBundleItem') IS NOT NULL)
	DROP VIEW vwBundleItem
GO
CREATE VIEW vwBundleItem
AS
SELECT  BI.[Id],
	BI.[BundleProductId],
	BI.[ChildSkuId] AS SkuId,
	BI.[ChildSkuQuantity] AS Quantity,
	S.[SKU],
	S.[Title] SKUTitle,
	S.ListPrice AS Price,
	BI.CreatedDate,
	BI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	LN.UserFullName ModifiedByFullName,
	BI.ModifiedDate,
	BI.[ModifiedBy],
	P.Id AS ProductId,
	P.Title AS ProductTitle
FROM PRBundleItem BI
	INNER JOIN PRProductSKU S ON S.Id = BI.ChildSkuId
	INNER JOIN PRProduct P ON P.Id = S.ProductId
	LEFT JOIN VW_UserFullName FN ON FN.UserId = BI.CreatedBy
	LEFT JOIN VW_UserFullName LN ON LN.UserId = BI.ModifiedBy
GO
PRINT 'Modify view vwBundleItem'
GO
IF(OBJECT_ID('vwBundleItem') IS NOT NULL)
	DROP VIEW vwBundleItem
GO
CREATE VIEW vwBundleItem
AS
SELECT  BI.[Id],
	BI.[BundleProductId],
	BI.[ChildSkuId] AS SkuId,
	BI.[ChildSkuQuantity] AS Quantity,
	S.[SKU],
	S.[Title] SKUTitle,
	S.ListPrice AS Price,
	BI.CreatedDate,
	BI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	LN.UserFullName ModifiedByFullName,
	BI.ModifiedDate,
	BI.[ModifiedBy],
	P.Id AS ProductId,
	P.Title AS ProductTitle
FROM PRBundleItem BI
	INNER JOIN PRProductSKU S ON S.Id = BI.ChildSkuId
	INNER JOIN PRProduct P ON P.Id = S.ProductId
	LEFT JOIN VW_UserFullName FN ON FN.UserId = BI.CreatedBy
	LEFT JOIN VW_UserFullName LN ON LN.UserId = BI.ModifiedBy
GO
PRINT 'Modify view vwOrderNotes'
GO
IF(OBJECT_ID('vwOrderNotes') IS NOT NULL)
	DROP VIEW vwOrderNotes
GO
CREATE VIEW [dbo].[vwOrderNotes]
AS 
SELECT N.Id,
	N.OrderId AS ObjectId,
	N.Message,
	N.CreatedDate,
	N.CreatedBy,
	CFN.UserFullName AS CreatedByFullName
FROM LGOrderNotes N
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = N.CreatedBy
GO
PRINT 'Modify view vwCustomerNotes'
GO
IF(OBJECT_ID('vwCustomerNotes') IS NOT NULL)
	DROP VIEW vwCustomerNotes
GO
CREATE VIEW [dbo].[vwCustomerNotes]
AS 
SELECT N.Id,
	N.CustomerId AS ObjectId,
	N.Message,
	N.CreatedDate,
	N.CreatedBy,
	CFN.UserFullName AS CreatedByFullName
FROM LGCustomerNotes N
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = N.CreatedBy
GO
PRINT 'Modify view vwOrderTracking'
GO
IF(OBJECT_ID('vwOrderTracking') IS NOT NULL)
	DROP VIEW vwOrderTracking
GO
CREATE VIEW [dbo].[vwOrderTracking]
AS 
SELECT OP.Id AS OrderShipmentPackageId,
	DotNetProviderName AS ProviderName, 
    OP.TrackingNumber, 
    A.AddressId AS ShippingAddressId, 
    OS.ShippedDate AS ShipDate, 
	O.OrderDate,
	SH.Name AS ShipperName,
	SO.Name AS ShippingOptionName
FROM FFOrderShipmentPackage OP
	JOIN FFOrderShipment OS ON OP.OrderShipmentId = OS.Id
	JOIN ORShippingOption SO ON SO.Id = OS.ShippingOptionId
	JOIN ORShipper SH ON SH.Id = SO.ShipperId
	JOIN OROrderShipping S ON S.Id = OS.OrderShippingId
	JOIN OROrderShippingAddress A ON A.Id = S.OrderShippingAddressId
    JOIN OROrder O ON O.Id = S.OrderId
GO



PRINT 'Modify view vwAvailableToSellPerSite'
GO

IF(OBJECT_ID('vwAvailableToSellPerSite') IS NOT NULL)
	DROP VIEW vwAvailableToSellPerSite
GO
CREATE VIEW [dbo].[vwAvailableToSellPerSite]
AS
SELECT vw.SKUId,
	SUM(vw.Quantity) AS Quantity ,
	MIN(vw.BackOrderLimit) BackOrderLimit,
	SUM(vw.Inventory) Inventory,
	vw.SiteId
FROM vwAvailableToSellPerWarehouse vw 
GROUP BY vw.SKUId, vw.SiteId
GO

PRINT 'Modify view vwSKU'
GO

IF(OBJECT_ID('vwSKU') IS NOT NULL)
	DROP VIEW vwSKU
GO
CREATE VIEW [dbo].[vwSKU]
AS
WITH CTE AS
(
	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		ISNULL(P.ModifiedDate, P.CreatedDate) AS ModifiedDate, 
		ISNULL(P.ModifiedBy, P.CreatedBy) AS  ModifiedBy
	FROM PRProductSku P

	UNION ALL

	SELECT P.Id, 
		P.SiteId, 
		P.Title, 
		P.[Description], 
		P.IsActive, 
		P.IsOnline, 
		P.UnitCost,
		P.ListPrice,
		P.WholesalePrice, 
		P.PreviousSoldCount, 
		P.Measure, 
		P.OrderMinimum, 
		P.OrderIncrement, 
		P.HandlingCharges,
		P.SelfShippable, 
		P.AvailableForBackOrder, 
		P.BackOrderLimit, 
		P.IsUnlimitedQuantity,
		P.MaximumDiscountPercentage, 
		P.FreeShipping, 
		P.UserDefinedPrice, 
		P.[Sequence], 
		P.ModifiedDate, 
		P.ModifiedBy
	FROM PRProductSKUVariantCache P
)

SELECT	C.Id, SK.ProductId, C.SiteId, SK.SiteId AS SourceSiteId, C.Title, C.[Description], SK.SKU, C.IsActive, C.IsOnline, C.UnitCost,
		C.WholesalePrice, C.PreviousSoldCount, C.Measure, C.OrderMinimum, C.OrderIncrement, C.HandlingCharges,
		SK.Height, SK.Width, SK.[Length], SK.[Weight], C.SelfShippable, C.AvailableForBackOrder, C.BackOrderLimit, C.IsUnlimitedQuantity,
		C.MaximumDiscountPercentage, C.FreeShipping, C.UserDefinedPrice, C.[Sequence], SK.[Status],
		C.ListPrice, ISNULL(ATS.Inventory, 0) AS Quantity, ISNULL(ATS.Quantity, 0) AS AvailableToSell,
		SK.CreatedDate, SK.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		C.ModifiedDate, C.ModifiedBy, MFN.UserFullName AS ModifiedByFullName,
		P.Title AS ProductName, P.ProductTypeId, PT.IsDownloadableMedia
FROM	CTE AS C
		INNER JOIN PRProductSku AS SK ON SK.Id = C.Id
		INNER JOIN PRProduct AS P ON P.Id = SK.ProductId
		INNER JOIN PRProductType AS PT ON PT.Id = P.ProductTypeID
		LEFT JOIN vwAvailableToSellPerSite AS ATS ON ATS.SKUId = SK.Id AND ATS.SiteId = SK.SiteId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = SK.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = SK.ModifiedBy
GO

PRINT 'Modify View vwProduct'
GO
IF(OBJECT_ID('vwProduct') IS NOT NULL)
	DROP View vwProduct
GO
CREATE VIEW [dbo].[vwProduct]  
AS  
WITH CTE AS  
(  
 SELECT P.[Id],   
	P.SiteId,  
	P.Title,   
	P.ShortDescription,   
	P.LongDescription,   
	P.[Description],   
	P.ProductStyle,  
	P.UrlFriendlyTitle,   
	P.TaxCategoryID,   
	P.IsActive,   
	P.PromoteAsNewItem,   
	P.PromoteAsTopSeller,  
	P.ExcludeFromDiscounts,   
	P.ExcludeFromShippingPromotions,   
	P.Freight,   
	P.Refundable,   
	P.TaxExempt,  
	P.SEOH1,   
	P.SEODescription,   
	P.SEOKeywords,   
	P.SEOFriendlyUrl,  
	P.IsShared,  
	ISNULL(P.ModifiedDate, P.CreatedDate) AS ModifiedDate,   
	ISNULL(P.ModifiedBy, P.CreatedBy) AS  ModifiedBy  
FROM PRProduct P  
  
UNION ALL  
  
SELECT P.[Id],   
	P.SiteId,  
	P.Title,   
	P.ShortDescription,   
	P.LongDescription,   
	P.[Description],   
	P.ProductStyle,  
	P.UrlFriendlyTitle,   
	P.TaxCategoryID,   
	P.IsActive,   
	P.PromoteAsNewItem,   
	P.PromoteAsTopSeller,  
	P.ExcludeFromDiscounts,   
	P.ExcludeFromShippingPromotions,   
	P.Freight,   
	P.Refundable,   
	P.TaxExempt,  
	P.SEOH1,   
	P.SEODescription,   
	P.SEOKeywords,   
	P.SEOFriendlyUrl,  
	0,  
	P.ModifiedDate,   
	P.ModifiedBy  
 FROM PRProductVariantCache P  
)  
  
SELECT P.[Id],   
	C.SiteId,  
	P.SiteId AS SourceSiteId,  
	C.Title,   
	C.ShortDescription,   
	C.LongDescription,   
	C.[Description],   
	P.ProductIDUser,   
	P.Keyword,   
	C.ProductStyle,  
	P.BundleCompositionLastModified,   
	C.UrlFriendlyTitle,   
	C.TaxCategoryID,   
	CAST(C.IsActive AS BIT) AS IsActive,   
	C.PromoteAsNewItem,   
	C.PromoteAsTopSeller,  
	C.ExcludeFromDiscounts,   
	C.ExcludeFromShippingPromotions,   
	C.Freight,   
	C.Refundable,   
	C.TaxExempt,  
	P.ProductTypeID,   
	P.[Status],   
	CAST(P.IsBundle AS BIT) AS IsBundle,   
	PTP.ProductTypeName,  
	CASE WHEN C.SEOFriendlyUrl IS NULL OR C.SEOFriendlyUrl = '' THEN  '/' + PTP.ProductTypePath +  N'id-' + P.ProductIDUser + '/' + C.UrlFriendlyTitle ELSE C.SEOFriendlyUrl END AS ProductUrl,
	C.SEOH1,   
	C.SEODescription,   
	C.SEOKeywords,   
	C.SEOFriendlyUrl,  
	P.IsShared,  
	P.CreatedDate,   
	P.CreatedBy AS CreatedById,   
	CFN.UserFullName AS CreatedByFullName,  
	C.ModifiedDate,   
	C.ModifiedBy AS ModifiedById,   
	MFN.UserFullName AS ModifiedByFullName
FROM CTE C  
	INNER JOIN PRProduct AS P ON P.Id = C.Id  
	INNER JOIN vwProductTypePathVariant AS PTP ON PTP.ProductTypeId = P.ProductTypeID AND PTP.SiteId = C.SiteId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy  
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
GO
PRINT 'Creating view vwFeatureProduct'
GO
IF(OBJECT_ID('vwFeatureProduct') IS NOT NULL)
	DROP VIEW vwFeatureProduct
GO
CREATE VIEW vwFeatureProduct
AS
SELECT FO.FeatureId,
	CASE WHEN E.FeatureId IS NULL THEN 0 ELSE 1 END AS ExcludedFromFeature,
	0 AS Sequence,
	P.*
FROM MRFeatureOutput FO
	INNER JOIN MRFeature F ON F.Id = FO.FeatureId
	INNER JOIN vwProduct P ON FO.ProductId = P.Id
	LEFT JOIN MRFeatureAutoExcludedItems E ON FO.FeatureId = E.FeatureId AND P.Id = E.ProductId
WHERE F.FeatureTypeId != 3

UNION

SELECT FO.FeatureId,
	NULL,
	FO.Sequence,
	P.*
FROM MRFeatureManualItems FO
	INNER JOIN MRFeature F ON F.Id = FO.FeatureId
	INNER JOIN vwProduct P ON FO.ProductId = P.Id
WHERE F.FeatureTypeId = 3
GO
PRINT 'Creating View vwNavProduct'
GO
IF (OBJECT_ID('vwNavProduct') IS NOT NULL)
	DROP View vwNavProduct
GO
CREATE VIEW vwNavProduct
AS
SELECT N.NavNodeId NavId,
	N.NavNodeUrl,
	FO.SerialNo AS DisplayOrder,
	CASE WHEN E.Id IS NULL THEN 0 ELSE 1 END AS ExcludedFromNav,
	P.*
FROM NVFilterOutput FO
	INNER JOIN NVNavNodeNavFilterMap N ON FO.QueryId = N.QueryId
	INNER JOIN vwProduct P ON FO.ObjectId = P.Id
	LEFT JOIN NVFilterExclude E ON N.QueryId = E.QueryId AND P.Id = E.ObjectId
GO
PRINT 'Creating View vwFeatureProductSearch'
GO
IF (OBJECT_ID('vwFeatureProductSearch') IS NOT NULL)
	DROP View vwFeatureProductSearch
GO
CREATE VIEW [dbo].[vwFeatureProductSearch]  
AS  
SELECT P.*,  
	SKU.SKU,   
	SKU.Title AS SkuTitle
FROM vwFeatureProduct AS P
	LEFT JOIN vwSKU AS SKU ON SKU.ProductId = P.Id AND SKU.SiteId = P.SiteId  
GO
PRINT 'Creating View vwNavProductSearch'
GO
IF (OBJECT_ID('vwNavProductSearch') IS NOT NULL)
	DROP View vwNavProductSearch
GO
CREATE VIEW [dbo].[vwNavProductSearch]  
AS  
SELECT P.*,  
	SKU.SKU,   
	SKU.Title AS SkuTitle
FROM vwNavProduct AS P
	LEFT JOIN vwSKU AS SKU ON SKU.ProductId = P.Id AND SKU.SiteId = P.SiteId  
GO
PRINT 'Creating View vwProductSearch'
GO
IF (OBJECT_ID('vwProductSearch') IS NOT NULL)
	DROP View vwProductSearch
GO
CREATE VIEW [dbo].[vwProductSearch]  
AS  
SELECT P.*,  
	SKU.SKU,   
	SKU.Title AS SkuTitle
FROM vwProduct AS P
	LEFT JOIN vwSKU AS SKU ON SKU.ProductId = P.Id AND SKU.SiteId = P.SiteId  
GO
PRINT 'Migrating Data to ATAttributeCategory from ATAttributeGroup'
GO
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ATAttributeGroup')
BEGIN
	INSERT INTO ATAttributeCategory
	(
		Id
		,[GroupId]
        ,[Name]
        ,[SiteId]
        ,[IsSystem]
        ,[CreatedDate]
        ,[CreatedBy]
        ,[ModifiedDate]
        ,[ModifiedBy]
        ,[Status]
        ,[ProductId]
        ,[IsGlobal]
        ,[Description]
	)
	SELECT 4,
		[Id]
        ,[Title]
        ,[SiteId]
        ,1
        ,[CreatedDate]
        ,[CreatedBy]
        ,[ModifiedDate]
        ,[ModifiedBy]
        ,[Status]
        ,[ProductId]
        ,[IsGlobal]
        ,[Description]
	FROM ATAttributeGroup
	Where Id NOT IN (Select GroupId from ATAttributeCategory where GroupId IS NOT NULL)
	AND Title = 'Customer Attributes'
END
	
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ATAttributeGroup')
	DELETE FROM ATAttributeGroup WHERE Title = 'Customer Attributes'

IF NOT EXISTS(Select 1 FROM ATAttributeCategory Where Id = 5)
BEGIN
	INSERT INTO ATAttributeCategory
	(
		Id
        ,[Name]
        ,[IsSystem]
        ,[Status]
        ,[ProductId]
		,[SiteId]
		,IsHidden
		,[CreatedDate]
	)
	SELECT 5
        ,'Product Type Product Attributes'
        ,1
        ,1
		,'CCF96E1D-84DB-46E3-B79E-C7392061219B'
		,'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
		,1
		,getdate()
END

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ATAttributeGroup')
BEGIN
	IF EXISTS (SELECT 1 FROM ATAttributeGroup WHERE Title = 'Product Properties')
	BEGIN
		INSERT INTO ATAttributeCategory
		(
			Id
			,[GroupId]
			,[Name]
			,[SiteId]
			,[IsSystem]
			,[IsHidden]
			,[CreatedDate]
			,[CreatedBy]
			,[ModifiedDate]
			,[ModifiedBy]
			,[Status]
			,[ProductId]
			,[IsGlobal]
			,[Description]
		)
		SELECT 6,
			[Id]
			,'Product Attributes'
			,[SiteId]
			,1
			,1
			,[CreatedDate]
			,[CreatedBy]
			,[ModifiedDate]
			,[ModifiedBy]
			,[Status]
			,[ProductId]
			,[IsGlobal]
			,[Description]
		FROM ATAttributeGroup
		Where Id NOT IN (Select GroupId from ATAttributeCategory where GroupId IS NOT NULL)
		AND Title = 'Product Properties'
	END
	ELSE
	BEGIN
		INSERT INTO ATAttributeCategory
		(
			Id
			,[Name]
			,[IsSystem]
			,[Status]
			,[ProductId]
			,[SiteId]
			,IsHidden
			,[CreatedDate]
		)
		SELECT 6
			,'Product Attributes'
			,1
			,1
			,'CCF96E1D-84DB-46E3-B79E-C7392061219B'
			,'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
			,1
			,getdate()
	END
END
	
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_NAME = N'ATAttributeGroup')
	DELETE FROM ATAttributeGroup WHERE Title = 'Product Properties'

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_NAME = N'ATAttributeGroup')
BEGIN
	IF EXISTS (SELECT 1 FROM ATAttributeGroup WHERE Title = 'SKULevel Attributes')
	BEGIN
		INSERT INTO ATAttributeCategory
		(
			Id
			,[GroupId]
			,[Name]
			,[SiteId]
			,[IsSystem]
			,[IsHidden]
			,[CreatedDate]
			,[CreatedBy]
			,[ModifiedDate]
			,[ModifiedBy]
			,[Status]
			,[ProductId]
			,[IsGlobal]
			,[Description]
		)
		SELECT 7,
			[Id]
			,[Title]
			,[SiteId]
			,1
			,1
			,[CreatedDate]
			,[CreatedBy]
			,[ModifiedDate]
			,[ModifiedBy]
			,[Status]
			,[ProductId]
			,[IsGlobal]
			,[Description]
		FROM ATAttributeGroup
		Where Id NOT IN (Select GroupId from ATAttributeCategory where GroupId IS NOT NULL)
		AND Title = 'SKULevel Attributes'
	END
	ELSE
	BEGIN
		INSERT INTO ATAttributeCategory
		(
			Id
			,[Name]
			,[IsSystem]
			,[Status]
			,[ProductId]
			,[SiteId]
			,IsHidden
			,[CreatedDate]
		)
		SELECT 7
			,'SKU Attributes'
			,1
			,1
			,'CCF96E1D-84DB-46E3-B79E-C7392061219B'
			,'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
			,1
			,getdate()
	END
END

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ATAttributeGroup')
	DELETE FROM ATAttributeGroup WHERE Title = 'SKULevel Attributes'

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ATAttributeGroup')
BEGIN
	INSERT INTO ATAttributeCategory
	(
		Id
		,[GroupId]
        ,[Name]
        ,[SiteId]
        ,[IsSystem]
        ,[CreatedDate]
        ,[CreatedBy]
        ,[ModifiedDate]
        ,[ModifiedBy]
        ,[Status]
        ,[ProductId]
        ,[IsGlobal]
        ,[Description]
	)
	SELECT 8,
		[Id]
        ,[Title]
        ,[SiteId]
        ,1
        ,[CreatedDate]
        ,[CreatedBy]
        ,[ModifiedDate]
        ,[ModifiedBy]
        ,[Status]
        ,[ProductId]
        ,[IsGlobal]
        ,[Description]
	FROM ATAttributeGroup
	Where Id NOT IN (Select GroupId from ATAttributeCategory where GroupId IS NOT NULL)
	AND Title = 'Order Attributes'
END
	
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ATAttributeGroup')
	DELETE FROM ATAttributeGroup WHERE Title = 'Order Attributes'

IF NOT EXISTS (select 1 from ATAttributeCategory Where Id=9)
BEGIN
	INSERT INTO ATAttributeCategory
	(
		Id
        ,[Name]
        ,[IsSystem]
        ,[Status]
        ,[ProductId]
		,[SiteId]
		,[CreatedDate]
	)
	SELECT 9
        ,'Order Item'
        ,1
        ,1
		,'CCF96E1D-84DB-46E3-B79E-C7392061219B'
		,'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
		,getdate()
END

IF NOT EXISTS (select 1 from ATAttributeCategory Where Id=10)
BEGIN
	INSERT INTO ATAttributeCategory
	(
		Id
        ,[Name]
        ,[IsSystem]
        ,[Status]
        ,[ProductId]
		,[SiteId]
		,IsHidden
		,[CreatedDate]
	)
	SELECT 10
        ,'Product Type SKU Attributes'
        ,1
        ,1
		,'CCF96E1D-84DB-46E3-B79E-C7392061219B'
		,'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
		,1
		,getdate()
END


DECLARE @maxId int
SELECT @maxId = 50
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ATAttributeGroup')
BEGIN
	INSERT INTO [dbo].[ATAttributeCategory]
           (Id
		   ,[GroupId]
           ,[Name]
           ,[SiteId]
           ,[IsSystem]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[ProductId]
           ,[IsGlobal]
           ,[Description])
     SELECT ROW_NUMBER() OVER(order by CreatedDate) + @maxId,
		[Id]
           ,[Title]
           ,[SiteId]
           ,[IsSystem]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[ProductId]
           ,[IsGlobal]
           ,[Description]
		   FROM ATAttributeGroup
		   Where Id NOT IN (Select GroupId from ATAttributeCategory where GroupId IS NOT NULL)
END

IF EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AttributeGroupId' AND Object_ID = Object_ID(N'ATAttribute'))
BEGIN
	declare @sql Nvarchar(max)
	SET @sql ='INSERT INTO [dbo].[ATAttributeCategoryItem]
		([Id]
		,[AttributeId]
		,[CategoryId]
		,[IsRequired]
		,[MinValue]
		,[MaxValue]
		,[ErrorMessage])
		select NEWID(),AttributeId,CategoryId,0,NULL,NULL,NULL 
		FROM
		(
			SELECT A.Id AttributeId,C.Id CategoryId
			FROM ATAttribute A
			INNER JOIN ATAttributeCategory C on A.AttributeGroupId = C.GroupId
			EXCEPT
			SELECT AttributeId,CategoryId FROM [ATAttributeCategoryItem]
		)T'
	
	exec sp_executesql @sql

END
GO
PRINT 'Update IsDisplayed'
GO
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ATAttributeGroup')
BEGIN
	UPDATE ATAttribute SET IsDisplayed = 1

	UPDATE A SET A.IsDisplayed = 0 FROM ATAttribute A 
		JOIN ATAttributeCategoryItem ACI ON ACI.AttributeId = A.Id
	WHERE ACI.CategoryId = 6
END
GO

PRINT 'Setting IsMultiValued for MultiStrings'
GO

Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateMultiStrings'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN
	PRINT 'IN UpdateMultiStrings';
	WITH MultiString_CTE AS(
		SELECT A.Id from PRProductAttributeValue V
			JOIN PRProductAttribute PR ON PR.Id = V.ProductAttributeId
			JOIN ATAttribute A ON A.Id = PR.AttributeId
			JOIN ATAttributeDataType D ON D.Id = A.AttributeDataTypeId
			JOIN PRProduct P ON P.Id = PR.ProductId
		WHERE D.Title = 'String' AND V.AttributeEnumId is null AND A.Title != 'Product Name' AND A.IsEnum = 0
		GROUP BY A.Id, A.Title, D.Title, P.Id, P.Title, PR.Id
		Having count(V.Id) > 1
	)

	UPDATE A SET A.IsMultiValued = 1
	FROM ATAttribute A
		JOIN MultiString_CTE C ON A.Id = C.Id;

	WITH MultiStringSku_CTE AS(
		SELECT A.Id from PRProductSkuAttributeValue V
			JOIN PRProductAttribute PR ON PR.Id = V.ProductAttributeId
			JOIN ATAttribute A ON A.Id = PR.AttributeId
			JOIN ATAttributeDataType D ON D.Id = A.AttributeDataTypeId
			JOIN PRProductSku P ON P.Id = V.SkuId
		WHERE D.Title = 'String' AND V.AttributeEnumId is null AND A.IsEnum = 0
		GROUP BY A.Id, A.Title, D.Title, P.Id, P.Title, PR.Id
		Having count(V.Id) > 1
	)

	UPDATE A SET A.IsMultiValued = 1
	FROM ATAttribute A
		JOIN MultiStringSku_CTE C ON A.Id = C.Id

	INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(),@ScriptName,@Version,GetDate())

END
GO

Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateMultiStringsForLibrary'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN
	WITH MultiString_CTE AS(
		SELECT A.Id from PRProductAttributeValue V
			JOIN PRProductAttribute PR ON PR.Id = V.ProductAttributeId
			JOIN ATAttribute A ON A.Id = PR.AttributeId
			JOIN ATAttributeDataType D ON D.Id = A.AttributeDataTypeId
			JOIN PRProduct P ON P.Id = PR.ProductId
		WHERE D.Title IN ('Page Library', 'Content Library', 'Asset Library File', 'Image Library') AND V.AttributeEnumId IS NULL AND A.IsEnum = 0
		GROUP BY A.Id, A.Title, D.Title, P.Id, P.Title, PR.Id
		Having count(V.Id) > 1
	)

	UPDATE A SET A.IsMultiValued = 1
	FROM ATAttribute A
		JOIN MultiString_CTE C ON A.Id = C.Id;

	WITH MultiStringSku_CTE AS(
		SELECT A.Id from PRProductSkuAttributeValue V
			JOIN PRProductAttribute PR ON PR.Id = V.ProductAttributeId
			JOIN ATAttribute A ON A.Id = PR.AttributeId
			JOIN ATAttributeDataType D ON D.Id = A.AttributeDataTypeId
			JOIN PRProductSku P ON P.Id = V.SkuId
		WHERE D.Title IN ('Page Library', 'Content Library', 'Asset Library File', 'Image Library') AND V.AttributeEnumId IS NULL AND A.IsEnum = 0
		GROUP BY A.Id, A.Title, D.Title, P.Id, P.Title, PR.Id
		Having count(V.Id) > 1
	)

	UPDATE A SET A.IsMultiValued = 1
	FROM ATAttribute A
		JOIN MultiStringSku_CTE C ON A.Id = C.Id

	INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(),@ScriptName,@Version,GetDate())

END
GO

PRINT 'Updating Enumerated DatatType'
GO
Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateEnumeratedDatatTypes'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN
	PRINT 'IN Updating Enumerated DatatType';
	DECLARE @EnumeratedDataTypeId uniqueidentifier
	SELECT TOP 1 @EnumeratedDataTypeId = Id FROM ATAttributeDataType WHERE Title = 'Enumerated'
	
	UPDATE A SET A.AttributeDataTypeId = @EnumeratedDataTypeId FROM ATAttribute A 
	WHERE AttributeDataTypeId != @EnumeratedDataTypeId
	AND EXISTS (SELECT 1 FROM ATAttributeEnum WHERE AttributeId = A.Id)

	UPDATE ATAttribute SET IsEnum = 1 WHERE AttributeDataTypeId = @EnumeratedDataTypeId AND IsEnum = 0

	UPDATE ATAttribute SET IsMultiValued = 0 WHERE IsMultiValued IS NULL

	INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(),@ScriptName,@Version,GetDate())
END
GO

PRINT 'Updating IsMultivalued Product Attribute'
GO

Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateIsMultiValuedProductAttribute'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN
	WITH ProductAttributeCTE AS
	(
		SELECT DISTINCT AttributeId FROM PRProductAttributeValue
		GROUP BY AttributeId, ProductId
		HAVING COUNT(AttributeId) > 1
	)

	UPDATE A SET A.IsMultiValued = 1 FROM ATAttribute A
		JOIN ProductAttributeCTE C ON A.Id = C.AttributeId
	WHERE A.IsEnum = 1 AND A.IsMultiValued != 1

	INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(),@ScriptName,@Version,GetDate())
END
GO

PRINT 'Updating IsMultivalued SKU Attribute'
GO
Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateIsMultiValuedSkuAttribute'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN
	WITH SKUAttributeCTE AS
	(
		SELECT DISTINCT AttributeId FROM PRProductSKUAttributeValue
		GROUP BY AttributeId, SkuId
		HAVING COUNT(AttributeId) > 1
	)

	UPDATE A SET A.IsMultiValued = 1 FROM ATAttribute A
		JOIN SKUAttributeCTE C ON A.Id = C.AttributeId
	WHERE A.IsEnum = 1 AND A.IsMultiValued != 1

	INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(),@ScriptName,@Version,GetDate())
END
GO

PRINT 'Updating IsMultivalued Customer Attribute'
GO
Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateIsMultiValuedCustomerAttribute'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN
	WITH CustomerAttributeCTE AS
	(
		SELECT DISTINCT AttributeId FROM CSCustomerAttributeValue
		GROUP BY AttributeId, CustomerId
		HAVING COUNT(AttributeId) > 1
	)

	UPDATE A SET A.IsMultiValued = 1 FROM ATAttribute A
		JOIN CustomerAttributeCTE C ON A.Id = C.AttributeId
	WHERE A.IsEnum = 1 AND A.IsMultiValued != 1

	INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(),@ScriptName,@Version,GetDate())
END
GO

PRINT 'Dropping AttributeGroupId column and ATAttributeGroup Table'
GO

IF EXISTS(SELECT *
          FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'ATAttribute'
                 AND COLUMN_NAME = 'AttributeGroupId') 
ALTER TABLE dbo.ATAttribute DROP COLUMN AttributeGroupId;
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'ATAttributeGroup')
BEGIN
	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ATAttributeGroupV6BKUP')
		SELECT * INTO ATAttributeGroupV6BKUP FROM ATAttributeGroup

	DROP TABLE ATAttributeGroup

	UPDATE ATAttributeCategory SET SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' WHERE SiteId IS NULL
	UPDATE ATAttributeCategory SET CreatedDate = getdate() WHERE CreatedDate IS NULL

	DELETE FROM ATAttributeCategoryItem WHERE AttributeId = 'f68ceb85-922b-4023-a6f9-20025f7fdc94'
END
GO

PRINT 'Create type TYAttributeEnum'
GO

IF TYPE_ID('TYAttributeEnum') IS NULL
	CREATE TYPE [dbo].[TYAttributeEnum] AS TABLE(
		[Id]           UNIQUEIDENTIFIER NULL,
		[AttributeID]  UNIQUEIDENTIFIER NULL,
		[Title]        NVARCHAR (500)   NULL,
		[Code]         NVARCHAR (200)   NULL,
		[NumericValue] DECIMAL (18)     NULL,
		[Value]        NVARCHAR (MAX)   NULL,
		[IsDefault]    BIT              NOT NULL,
		[Sequence]     INT              NULL,
		[CreatedDate]  DATETIME         NULL,
		[CreatedBy]    UNIQUEIDENTIFIER NULL,
		[ModifiedDate] DATETIME         NULL,
		[ModifiedBy]   UNIQUEIDENTIFIER NULL,
		[Status]       INT              NULL
	)
GO



IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PRProductTypeVariant')

CREATE TABLE [dbo].[PRProductTypeVariant](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Description] [nvarchar](200) NULL,
	[FriendlyName] [nvarchar](50) NULL,
	[CMSPageId] [uniqueidentifier] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[Status] [int] NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_PRProductTypeVariant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[SiteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



-- Migration PRProductTypeCMSPage to PRProductType and PRProductTypeVariant
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PRProductTypeMigrateBKUP')
BEGIN 

	Select * INTO PRProductTypeMigrateBKUP from PRProductType

	Update PRProductType Set CMSPageId = C.CMSPageId 
	From PRProductType P
	INNER JOIN PRProductTypeCMSPage C ON P.Id = C.ProductTypeId 
	Where P.SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' and C.SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'

	-- To update any ProProductType with the first valid CMSPageId,  which dont have corresponding CMSPageId PRProductTypeCMSPage,  This anyway should not happen
	Update PRProductType Set CMSPageId = (Select top 1 CMSPageId from PRProductTypeCMSPage where SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' and CMSPageId != '00000000-0000-0000-0000-000000000000')
	where CMSPageId IS NULL

	-- Insert into PRProductTypeVariant for other multi site 
	INSERT INTO PRProductTypeVariant (Id, Status, SiteId, ModifiedDate, CMSPageId)
	Select ProductTypeId, PR.Status, P.SiteId, GetUtcDate(), P.CMSPageId  from PRProductTypeCMSPage P 
	INNER JOIN PRProductType PR ON P.ProductTypeId = PR.Id
	where P.SiteId != '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'

	Select * INTO PRProductTypeCMSPageMigrateBKUP from PRProductTypeCMSPage
	DROP Table PRProductTypeCMSPage
	--EXEC sp_rename 'PRProductTypeCMSPage', 'PRProductTypeCMSPageMigrateBKUP'; 

END
GO
PRINT 'Modify stored procedure PageDto_GetPagesUsingObject'
GO
IF(OBJECT_ID('PageDto_GetPagesUsingObject') IS NOT NULL)
	DROP PROCEDURE PageDto_GetPagesUsingObject
GO
CREATE PROCEDURE [dbo].[PageDto_GetPagesUsingObject]  
(
	@ObjectType		int,
	@ObjectId		uniqueidentifier,
	@PageSiteId		uniqueidentifier = NULL
)  
AS  
BEGIN  
	DECLARE @tbPageIds TABLE(PageId uniqueidentifier)
	
	IF @ObjectType = 7 OR @ObjectType = 9 OR @ObjectType = 33
	BEGIN
		INSERT INTO @tbPageIds
		SELECT P.PageDefinitionId FROM PageDefinition P
			JOIN PageDefinitionContainer C ON P.PageDefinitionId = C.PageDefinitionId
		WHERE C.ContentId = @ObjectId OR C.InWFContentId = @ObjectId
	END

	IF @ObjectType = 9 OR @ObjectType = 33
	BEGIN
		DECLARE @Url nvarchar(max)
		SELECT @Url = RelativePath + '/' + [FileName] FROM COFile WHERE ContentId = @ObjectId
		SET @Url = '%' + REPLACE(@Url, '~', '') + '%'

		INSERT INTO @tbPageIds
		SELECT PageId FROM Content_AssetLinks L
		WHERE L.ContentId = @ObjectId 
			OR L.AssetLink like @Url

	END

	IF @ObjectType = 4
	BEGIN
		INSERT INTO @tbPageIds
		SELECT P.PageDefinitionId
		FROM PageDefinition P
			JOIN SIPageStyle S ON P.PageDefinitionId = S.PageDefinitionId
		WHERE S.StyleId = @ObjectId
	END

	IF @ObjectType = 35
	BEGIN
		INSERT INTO @tbPageIds
		SELECT P.PageDefinitionId
		FROM PageDefinition P
			JOIN SIPageScript S ON P.PageDefinitionId = S.PageDefinitionId
		WHERE S.ScriptId = @ObjectId
	END

	IF @ObjectType = 3
	BEGIN
		INSERT INTO @tbPageIds
		SELECT P.PageDefinitionId
		FROM PageDefinition P
		WHERE p.TemplateId = @ObjectId
	END
		
	SELECT DISTINCT P.PageDefinitionId,
		P.TemplateId,
		P.SiteId,
		P.title,
		P.FriendlyName,
		CASE WHEN P.PageStatus > 0 THEN PageStatus 
			WHEN P.WorkflowState > 1 THEN 11 
			ELSE 1 END AS PageStatus,
		S.LftValue
	FROM PageDefinition P
		JOIN @tbPageIds T ON P.PageDefinitionId = T.PageId
		JOIN SISite S ON S.Id = P.SiteId
		LEFT JOIN MKCampaignEmail E ON E.CMSPageId = P.PageDefinitionId
	WHERE E.Id IS NULL AND
		(@PageSiteId IS NULL OR P.SiteId = @PageSiteId) AND S.Status = 1
	ORDER BY S.LftValue
END
GO
PRINT 'Modify stored procedure CampaignRunReportDto_Get'
GO
IF(OBJECT_ID('CampaignRunReportDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignRunReportDto_Get
GO
CREATE PROCEDURE [dbo].[CampaignRunReportDto_Get]  
(  
	@StartDate			datetime,
	@EndDate			datetime,
	@ReportType			int,
	@CampaignName		nvarchar(500) = NULL,
	@SiteId				uniqueidentifier = NULL,
	@IgnoreSite			bit = NULL,
	@PageNumber			int = NULL,
	@PageSize			int = NULL,	
	@SortBy				nvarchar(100) = NULL,  
	@SortOrder			nvarchar(10) = NULL
)  
AS  
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(500)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL) SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	
	IF (@CampaignName = '') SET @CampaignName = NULL
	IF (@CampaignName IS NOT NULL) SET @CampaignName = '%' + @CampaignName + '%'
	IF (@IgnoreSite = 1) SET @SiteId = NULL
	SET @EndDate = DATEADD(DD, 1, @EndDate)

	IF @ReportType = 1
	BEGIN
		;WITH CTE AS(
			SELECT ROW_NUMBER() OVER (ORDER BY 
					CASE WHEN @SortClause like 'RunDate ASC' THEN R.RunDate END ASC,
					CASE WHEN @SortClause like 'RunDate DESC' THEN R.RunDate END DESC,
					CASE WHEN @SortClause like 'Sends ASC' THEN R.Sends END ASC,
					CASE WHEN @SortClause like 'Sends DESC' THEN R.Sends END DESC,
					CASE WHEN @SortClause like 'Opens ASC' THEN R.Opens END ASC,
					CASE WHEN @SortClause like 'Opens DESC' THEN R.Opens END DESC,
					CASE WHEN @SortClause like 'OpenRate ASC' THEN R.OpenRate END ASC,
					CASE WHEN @SortClause like 'OpenRate DESC' THEN R.OpenRate END DESC,
					CASE WHEN @SortClause like 'Bounces ASC' THEN R.Bounces END ASC,
					CASE WHEN @SortClause like 'Bounces DESC' THEN R.Bounces END DESC,
					CASE WHEN @SortClause like 'BounceRate ASC' THEN R.BounceRate END ASC,
					CASE WHEN @SortClause like 'BounceRate DESC' THEN R.BounceRate END DESC,
					CASE WHEN @SortClause like 'Unsubscribes ASC' THEN R.Unsubscribes END ASC,
					CASE WHEN @SortClause like 'Unsubscribes DESC' THEN R.Unsubscribes END DESC,
					CASE WHEN @SortClause like 'UnsubscribeRate ASC' THEN R.UnsubscribeRate END ASC,
					CASE WHEN @SortClause like 'UnsubscribeRate DESC' THEN R.UnsubscribeRate END DESC,
					CASE WHEN @SortClause IS NULL THEN R.RunDate END DESC		
				) AS RowNumber,
				COUNT(R.Id) OVER () AS TotalRecords,
				R.Id AS Id,
				R.CampaignId,
				R.CampaignName,
				R.RunDate,
				R.Sends,
				R.Opens,
				R.Clicks,
				R.Bounces,
				R.Unsubscribes,
				R.OpenRate,
				R.BounceRate,
				R.UnsubscribeRate,
				R.SiteId
			FROM vwCampaignRun R
			WHERE RunDate BETWEEN @StartDate AND @EndDate
				AND (@CampaignName IS NULL OR CampaignName like @CampaignName)
				AND (@SiteId IS NULL OR R.SiteId = @SiteId)
				AND R.Status = 2
		)

		SELECT C.*, 
			CASE WHEN S.ExternalCode IS NOT NULL AND S.ExternalCode != '' THEN S.Title + ' (' + S.ExternalCode + ')'
				ELSE S.Title END AS SiteName
		FROM CTE C
			JOIN SISite S ON C.SiteId = S.Id
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		ORDER BY RowNumber
	END
	ELSE IF @ReportType = 2
	BEGIN
		DECLARE @tbCampaigns TABLE
		(
			RunDate date,
			Sends int,
			Opens int,
			Clicks int,
			Bounces int,
			Unsubscribes int
		)

		INSERT INTO @tbCampaigns
		SELECT CAST((CAST(YEAR(RunDate) AS VARCHAR(4)) + '-' + CAST(MONTH(RunDate) AS VARCHAR(2)) + '-10') AS DATE) AS RunDate,
			Sends,
			UniqueOpens AS Opens,
			Clicks,
			Bounces,
			Unsubscribes
		FROM dbo.MKCampaignRunHistory R
			JOIN dbo.MKCampaign C ON R.CampaignId = C.Id
		WHERE C.Type = 1 
			AND RunDate BETWEEN @StartDate AND @EndDate
			AND (@CampaignName IS NULL OR C.Title like @CampaignName)
			AND (@SiteId IS NULL OR R.SiteId = @SiteId)
			AND R.Status = 2

		;WITH CTE AS(
			SELECT RunDate,
				COUNT(1) AS NoOfCampaigns,
				SUM(Sends) AS Sends,
				SUM(Opens) AS Opens,
				SUM(Clicks) AS Clicks,
				SUM(Bounces) AS Bounces,
				SUM(Unsubscribes) AS Unsubscribes
			FROM @tbCampaigns
			GROUP BY RunDate
		), 
		RATECTE AS (
			SELECT RunDate,
				NoOfCampaigns,
				Sends,
				Opens,
				Clicks,
				Bounces,
				Unsubscribes,
				CASE WHEN Opens > 0 THEN CEILING((100.0 * Opens)/Sends) ELSE 0 END AS OpenRate,
				CASE WHEN Bounces > 0 THEN CEILING((100.0 * Bounces)/Sends) ELSE 0 END AS BounceRate,
				CASE WHEN Unsubscribes > 0 THEN CEILING((100.0 * Unsubscribes)/Sends) ELSE 0 END AS UnsubscribeRate
			FROM CTE
		),
		PagedCTE AS (
			SELECT ROW_NUMBER() OVER (ORDER BY 
					CASE WHEN @SortClause like 'RunDate ASC' THEN R.RunDate END ASC,
					CASE WHEN @SortClause like 'RunDate DESC' THEN R.RunDate END DESC,
					CASE WHEN @SortClause like 'Sends ASC' THEN R.Sends END ASC,
					CASE WHEN @SortClause like 'Sends DESC' THEN R.Sends END DESC,
					CASE WHEN @SortClause like 'Opens ASC' THEN R.Opens END ASC,
					CASE WHEN @SortClause like 'Opens DESC' THEN R.Opens END DESC,
					CASE WHEN @SortClause like 'OpenRate ASC' THEN R.OpenRate END ASC,
					CASE WHEN @SortClause like 'OpenRate DESC' THEN R.OpenRate END DESC,
					CASE WHEN @SortClause like 'Bounces ASC' THEN R.Bounces END ASC,
					CASE WHEN @SortClause like 'Bounces DESC' THEN R.Bounces END DESC,
					CASE WHEN @SortClause like 'BounceRate ASC' THEN R.BounceRate END ASC,
					CASE WHEN @SortClause like 'BounceRate DESC' THEN R.BounceRate END DESC,
					CASE WHEN @SortClause like 'Unsubscribes ASC' THEN R.Unsubscribes END ASC,
					CASE WHEN @SortClause like 'Unsubscribes DESC' THEN R.Unsubscribes END DESC,
					CASE WHEN @SortClause like 'UnsubscribeRate ASC' THEN R.UnsubscribeRate END ASC,
					CASE WHEN @SortClause like 'UnsubscribeRate DESC' THEN R.UnsubscribeRate END DESC,
					CASE WHEN @SortClause IS NULL THEN R.RunDate END DESC		
				) AS RowNumber,
				COUNT(R.RunDate) OVER () AS TotalRecords,
				R.NoOfCampaigns,
				R.RunDate,
				R.Sends,
				R.Opens,
				R.Clicks,
				R.Bounces,
				R.Unsubscribes,
				R.OpenRate,
				R.BounceRate,
				R.UnsubscribeRate
			FROM RATECTE R
		)

		SELECT * FROM PagedCTE
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		ORDER BY RowNumber
	END
END
GO
PRINT 'Modify stored procedure CampaignReportDto_Get'
GO
IF(OBJECT_ID('CampaignReportDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignReportDto_Get
GO
CREATE PROCEDURE [dbo].[CampaignReportDto_Get]  
(  
	@SiteId							uniqueidentifier,
	@StartDate						datetime,
	@EndDate						datetime,
	@IncludeCampaignEmails			bit = 0,
	@IncludeCampaignBlackoutDates	bit = 0,
	@IncludeAutomationFlows			bit = 0
)  
AS  
BEGIN
	DECLARE @tbCampaigns TABLE
	(
		Id					uniqueidentifier,
		Title				nvarchar(max),
		[Description]		nvarchar(max),
		SiteId				uniqueidentifier,
		SiteName			nvarchar(2000),
		EventDate			datetime,
		EventUrl			nvarchar(max),
		EventType			nvarchar(50),
		TotalContacts		int,
		CreatedBy			uniqueidentifier,
		CreatedDate			datetime,
		CreatedByFullName	nvarchar(1000)
	)

	IF @IncludeCampaignEmails = 1
	BEGIN
		-- Scheduled
		INSERT INTO @tbCampaigns
		SELECT DISTINCT C.Id,
			C.Title,
			C.[Description],
			S.Id,
			S.Title,
			SO.RunDate,
			PU.[Url],
			'Campaign Email',
			0,
			CS.ModifiedBy,
			CS.ModifiedDate,
			MU.UserFullName
		FROM MKCampaign C
			JOIN MKCampaignSend CS ON C.Id = CS.CampaignId
			JOIN MKCampaignSendTarget T ON T.CampaignSendId = CS.Id
			JOIN MKCampaignEmail AS CE ON CE.CampaignId = C.Id
			JOIN SISite S ON S.Id = CS.SiteId
			JOIN vwPageUrl AS PU ON PU.Id = CE.CMSPageId
			LEFT JOIN VW_UserFullName MU on MU.UserId = CS.ModifiedBy
			CROSS APPLY dbo.Schedule_GetOccurrencesInRange(CS.ScheduleId, @StartDate, @EndDate) AS SO
		WHERE C.Type = 1 AND
			CS.Status = 2 AND
			CS.ScheduleId IS NOT NULL AND
			EXISTS (SELECT 1 FROM vwCampaignSendTarget CT WHERE CT.CampaignSendId = CS.Id AND CT.SiteId = @SiteId)

		UPDATE T
		SET T.TotalContacts = ISNULL(DLS.Count, 0)
		FROM MKCampaignDistributionList CDL
			JOIN @tbCampaigns T ON T.Id = CDL.CampaignId
			JOIN MKCampaignSend S ON S.CampaignId = T.Id AND S.SiteId = @SiteId
			JOIN TADistributionLists D on D.Id = CDL.DistributionListId
			JOIN TADistributionListSite DLS ON DLS.DistributionListId = D.Id AND DLS.SiteId = @SiteId
		WHERE CDL.CampaignSendId = S.Id

		--Sent
		INSERT INTO @tbCampaigns
		SELECT DISTINCT C.Id,
			C.Title,
			C.[Description],
			S.Id,
			S.Title,
			R.RunDate,
			PU.[Url],
			'Campaign Email',
			R.Sends,
			CS.ModifiedBy,
			CS.ModifiedDate,
			MU.UserFullName
		FROM MKCampaign C
			JOIN MKCampaignSend CS ON C.Id = CS.CampaignId
			JOIN MKCampaignSendTarget T ON T.CampaignSendId = CS.Id
			JOIN MKCampaignRunHistory R ON R.CampaignSendId = CS.Id
			JOIN SISite S ON S.Id = CS.SiteId
			JOIN vwPageUrl AS PU ON PU.Id = R.CMSPageId
			LEFT JOIN VW_UserFullName MU on MU.UserId = CS.ModifiedBy
		WHERE R.RunDate BETWEEN @StartDate AND @EndDate
			AND R.SiteId = @SiteId
			AND CS.Status != 4
	END

	IF @IncludeAutomationFlows = 1
	BEGIN
		INSERT INTO @tbCampaigns
		SELECT	F.Id, 
			F.Title,
			F.[Description],
			S.Id AS SiteId,
			S.Title,
			D.[Date],
			'#',
			'Automation Flow',
			0,
			F.ModifiedBy,
			F.ModifiedDate,
			MU.UserFullName
		FROM	MAFlow AS F
				JOIN SISite S ON S.Id = F.SiteId
				LEFT JOIN VW_UserFullName MU on MU.UserId = F.ModifiedBy
				CROSS APPLY dbo.Schedule_GetDatesInRange(
					CASE WHEN F.StartDateTime >= @StartDate THEN F.StartDateTime ELSE @StartDate END,
					CASE WHEN F.EndDateTime IS NOT NULL AND F.EndDateTime <= @EndDate THEN F.EndDateTime ELSE @EndDate END
				) AS D
		WHERE	F.Status IN (8, 9, 10) AND
				F.SiteId = @SiteId AND
				F.StartDateTime <= @EndDate AND
				(F.EndDateTime IS NULL OR F.EndDateTime >= @StartDate)
	END

	IF @IncludeCampaignBlackoutDates = 1
	BEGIN
		INSERT INTO @tbCampaigns
		SELECT	CB.Id, 
			CB.Title, 
			CB.[Description],
			S.Id,
			S.Title,
			D.[Date],
			'#',
			'Campaign Blackout Date', 
			0,
			CB.CreatedBy,
			CB.CreatedDate,
			CU.UserFullName
		FROM	vwCampaignBlackoutTarget AS CBT
				JOIN MKCampaignBlackout AS CB ON CB.Id = CBT.CampaignBlackoutId
				JOIN MKCampaignSetting CS ON CB.CampaignSettingId = CS.Id 
				JOIN SISite S ON S.Id = CS.SiteId
				LEFT JOIN VW_UserFullName CU on CU.UserId = CB.CreatedBy
				CROSS APPLY dbo.Schedule_GetDatesInRange(
					CASE WHEN CB.StartDate >= @StartDate THEN CB.StartDate ELSE @StartDate END,
					CASE WHEN CB.EndDate <= @EndDate THEN CB.EndDate ELSE @EndDate END
				) AS D
		WHERE	CBT.SiteId = @SiteId
	END


	SELECT * FROM @tbCampaigns
	ORDER BY EventDate ASC, EventType ASC, Title ASC
END
GO
PRINT 'Modify stored procedure Membership_UpdateUserInfo'
GO
IF(OBJECT_ID('Membership_UpdateUserInfo') IS NOT NULL)
	DROP PROCEDURE Membership_UpdateUserInfo
GO
CREATE PROCEDURE [dbo].[Membership_UpdateUserInfo]
    
--********************************************************************************
-- PARAMETERS
	@ProductId						uniqueidentifier = NULL,
	@ApplicationId					uniqueidentifier,
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable declaration
--********************************************************************************
	DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime
	
	DECLARE @CurrentTimeUtc							datetime
	SET		@CurrentTimeUtc = GetUTCDate()

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

	IF (@ProductId = '00000000-0000-0000-0000-000000000000')
	BEGIN
		SET @ProductId = null
	END

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.Id,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.USSiteUser s, dbo.USUser u, dbo.USMembership m WITH ( UPDLOCK )
    WHERE   --s.SiteId = @ApplicationId	AND
			s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND
            u.Id = m.UserId AND
			u.Id = s.UserId AND 
			s.ProductId = ISNULL(@ProductId,s.ProductId) AND
            LOWER(@UserName) = u.LoweredUserName

    --User not found
	IF ( @@rowcount = 0 )
    BEGIN
        RAISERROR ('NOTEXISTS||%s', 16, 1, @UserName)
        GOTO Cleanup
    END

    --User is locked out
	IF( @IsLockedOut = 1 )
    BEGIN
        RAISERROR ('USERLOCKED||%s', 16, 1, @UserName)
		GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @LastLockoutDate = CONVERT( datetime, '17540101', 112 )
        END
    END

	-- update the last activity date and last login date if the password is correct and update last login activity is set ot true
    IF( @UpdateLastLoginActivityDate = 1 AND @IsPasswordCorrect = 1)
    BEGIN
        UPDATE  dbo.USUser
        SET     LastActivityDate = @CurrentTimeUtc, 
				LastInteractionDate = @CurrentTimeUtc
        WHERE   @UserId = [Id]

		UPDATE DNContactAttribute
		SET [66666666-0000-0000-0000-000000000000] = @CurrentTimeUtc
		WHERE ContactId = @UserId 

        IF( @@ERROR <> 0 )
        BEGIN
            RAISERROR ('OPERATIONFAILED||%s', 16, 1, 'LastLoginActivityDate Update')
            GOTO Cleanup
        END

        UPDATE  dbo.USMembership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            RAISERROR ('OPERATIONFAILED||%s', 16, 1, 'LastLoginDate Update')
            GOTO Cleanup
        END
    END


    UPDATE dbo.USMembership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        RAISERROR ('OPERATIONFAILED||%s', 16, 1, 'Membership Update')
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN dbo.GetBusinessRuleErrorCode()

END
--***********************************************************************************
GO
PRINT 'Modify stored procedure SkuAttributeValue_Delete'
GO
IF(OBJECT_ID('SkuAttributeValue_Delete') IS NOT NULL)
	DROP PROCEDURE SkuAttributeValue_Delete
GO
CREATE PROCEDURE [dbo].[SkuAttributeValue_Delete]
(
	@SkuId			uniqueidentifier,
	@AttributeIds	nvarchar(max)
)
AS
BEGIN
	DECLARE @ProductId uniqueidentifier
	DECLARE @tbAttributeIds TABLE(Id uniqueidentifier primary key)

	INSERT INTO @tbAttributeIds
	SELECT Items FROM dbo.SplitGUID(@AttributeIds, ',')

	SELECT TOP 1 @ProductId = ProductId FROM PRProductSku WHERE Id = @SkuId

	DELETE FROM PRProductSKUAttributeValue WHERE SKUId = @SkuId

	DELETE SAV FROM PRProductSKUAttributeValue SAV
		JOIN PRProductSku S ON S.Id = SAV.SKUId
	WHERE S.ProductId = @ProductId
		AND SAV.AttributeId NOT IN (SELECT Id FROM @tbAttributeIds)

	DELETE FROM PRProductAttribute
	WHERE ProductId = @ProductId AND IsSKULevel = 1
		AND AttributeId NOT IN (SELECT Id FROM @tbAttributeIds)
END
GO
PRINT 'Modify stored procedure CampaignContact_Get'
GO
IF(OBJECT_ID('CampaignContact_Get') IS NOT NULL)
	DROP PROCEDURE CampaignContact_Get
GO
CREATE PROCEDURE [dbo].[CampaignContact_Get]
(
	@SiteId					uniqueidentifier,
	@CampaignRunId			uniqueidentifier,
	@BatchSize				int,
	@ProcessingStatus		int = NULL,
	@MarkAsProcessing		bit = NULL
)
AS
BEGIN
	IF NOT EXISTS(SELECT 1 FROM MKCampaignRunWorkTable WHERE @CampaignRunId = CampaignRunId)
		RETURN

	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	IF @ProcessingStatus IS NULL SET @ProcessingStatus = 0

	DECLARE @tbContacts TABLE 
	(
		ContactId		uniqueidentifier primary key,
		AddressId		uniqueidentifier, 
		SiteId			uniqueidentifier,
		TotalRecords	int
	)
		
	;WITH CTE AS
	(
		SELECT 
			UserId, 
			AddressId, 
			SiteId,
			COUNT(C.UserId) OVER () AS TotalRecords,
			ROW_NUMBER() OVER (ORDER BY SendDate) AS RowNumber
		FROM MKCampaignRunWorkTable C
		WHERE @CampaignRunId = CampaignRunId 
			AND ProcessingStatus = @ProcessingStatus
			AND (SendDate IS NULL OR SendDate <= @UtcNow)
	)

	INSERT INTO @tbContacts (ContactId, AddressId, SiteId, TotalRecords)
	SELECT TOP (@BatchSize) UserId, AddressId, SiteId, TotalRecords FROM CTE
	ORDER BY RowNumber

	IF @MarkAsProcessing = 1
	BEGIN
		UPDATE MKCampaignRunWorkTable 
			SET ProcessingCount = ISNULL(ProcessingCount, 0) + 1, 
				ProcessingStatus = 1,
				ProcessingTime = GETUTCDATE()
		WHERE UserId IN (SELECT ContactId FROM @tbContacts)
			AND CampaignRunId = @CampaignRunId
	END

	-- Contacts
	SELECT W.CampaignRunId,
		W.UserId Id,
		W.FirstName,
		W.MiddleName,
		W.LastName,
		W.CompanyName,
		W.BirthDate,
		W.Gender,
		W.AddressId,
		W.Status,
		W.HomePhone,
		W.MobilePhone,
		W.OtherPhone,
		W.Email,
		W.Notes,
		W.ContactType,
		ISNULL(T.SiteId, @SiteId) AS SiteId,
		ISNULL(T.SiteId, @SiteId) AS PrimarySiteId,
		T.TotalRecords
	FROM @tbContacts T 
		JOIN MKCampaignRunWorkTable W ON T.ContactId = W.UserId 
			AND W.CampaignRunId = @CampaignRunId

	-- Address
	SELECT A.Id,
		A.FirstName,
		A.LastName,
		A.AddressType,
		A.AddressLine1,
		A.AddressLine2,
		A.AddressLine3,
		A.City,
		A.StateId,
		A.Zip,		
		A.CountryId,
		A.Phone,
		A.CreatedDate,
		A.CreatedBy,
		A.ModifiedDate,
		A.ModifiedBy,
		A.Status,
		A.County,
		A.CountryName,
		A.CountryCode,
		A.StateCode,
		A.StateName
	FROM vw_Address A
		JOIN @tbContacts C ON C.AddressId = A.Id

	SELECT V.*, 
		A.Title AS AttributeTitle
	FROM ATContactAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbContacts T ON V.ContactId = T.ContactId
END	
GO
PRINT 'Modify stored procedure CampaignRunHistoryDto_CheckWorkTable'
GO
IF(OBJECT_ID('CampaignRunHistoryDto_CheckWorkTable') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_CheckWorkTable
GO
CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_CheckWorkTable]
(
	@CampaignRunId	uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime, @CampaignId uniqueidentifier
	SET @UtcNow = GETUTCDATE()

	SELECT TOP 1 @CampaignId = CampaignId FROM MKCampaignRunHistory WHERE Id = @CampaignRunId

	--Check for failed records (To avoid campaign to be running forever)
	UPDATE MKCampaignRunWorkTable SET
		ProcessingStatus = CASE WHEN ProcessingCount < 2 THEN 0 ELSE 4 END, --To avoid any infinite loop
		ProcessingCount = ProcessingCount + 1
	WHERE CampaignRunId = @CampaignRunId 
		AND ProcessingStatus = 1 
		AND DATEDIFF(MINUTE, ProcessingTime, @UtcNow) > 60

	INSERT INTO MKCampaignRunErrorLog 
	(
		Id,
		CampaignId,
		CampaignRunId,
		ContactId,
		ErrorMessage
	)
	SELECT NEWID(),
		@CampaignId,
		@CampaignRunId,
		UserId,
		LogMessage 
	FROM MKCampaignRunWorkTable 
	WHERE CampaignRunId = @CampaignRunId
		AND ProcessingStatus = 4

	DELETE FROM  MKCampaignRunWorkTable 
	WHERE CampaignRunId = @CampaignRunId AND ProcessingStatus = 4
END
GO
PRINT 'Modify stored procedure CampaignRunHistoryDto_UpdateResponse'
GO
IF(OBJECT_ID('CampaignRunHistoryDto_UpdateResponse') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_UpdateResponse
GO
CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_UpdateResponse]
(
	@Id		uniqueidentifier --CampaignRunId
	--This is dependent on MKCampaignRunResponseWorktable. So the response status should be updated on this worktable
)
AS
BEGIN
	DECLARE @UtcNow datetime, @RetryCount int
	SET @UtcNow = GETUTCDATE()
	SET @RetryCount = 4

	DECLARE @tbResponses TABLE (ContactId uniqueidentifier primary key)
	INSERT INTO @tbResponses
	SELECT DISTINCT ContactId FROM MKCampaignRunResponseWorktable WHERE CampaignRunId = @Id

	;WITH ResponseCTE (StatusToUpdate, ContactId, LogMessage) AS
	(
		SELECT 
			CASE WHEN ResponseStatus = 3 AND C.ProcessingCount < @RetryCount THEN 0
				 WHEN ResponseStatus = 2 OR ResponseStatus = 4 THEN ResponseStatus
				 WHEN C.ProcessingCount >= @RetryCount THEN ResponseStatus END StatusToUpdate,
			WT.ContactId,
			WT.LogMessage 
		FROM MKCampaignRunResponseWorktable WT 
			JOIN @tbResponses T ON T.ContactId = WT.ContactId
			JOIN MKCampaignRunWorkTable C ON C.CampaignRunId = WT.CampaignRunId AND C.UserId = WT.ContactId
		WHERE WT.CampaignRunId = @Id
	)

	UPDATE C 
		SET ProcessingStatus = StatusToUpdate,
		C.LogMessage = R.LogMessage
	FROM MKCampaignRunWorkTable C 
		JOIN ResponseCTE R ON R.ContactId = C.UserId
	WHERE C.CampaignRunId = @Id

	DECLARE @SendCount int
	SELECT @SendCount = COUNT(1) FROM MKCampaignRunResponseWorktable WT 
			JOIN @tbResponses T ON T.ContactId = WT.ContactId
	WHERE CampaignRunId = @Id AND ResponseStatus = 2	
	
	DECLARE @CampaignId uniqueidentifier, @Type int

	SELECT TOP 1 @Type = C.Type, @CampaignId = C.Id 
	FROM MKCampaign C 
		JOIN MKCampaignRunHistory H On C.Id = H.CampaignId 
	WHERE H.Id = @Id

	if(@Type = 2) --response
	BEGIN
		--for successful sends and hard error update the last sent date on MKResponseFlow table
		UPDATE MKResponseFlow SET 
			LastSent = @UtcNow,
			Completed = CASE WHEN dbo.Campaign_VerifyAutoReponsesCompleted(ResponseId, UserId, @UtcNow) = 1 THEN @UtcNow  
							 ELSE Completed END
		WHERE UserId IN (SELECT UserId FROM MKCampaignRunWorkTable WHERE CampaignRunId = @Id 
							AND (ProcessingStatus = 2 OR ProcessingStatus = 4 OR (ProcessingStatus = 3 AND ProcessingCount >= @RetryCount))) 
			AND ResponseId = @CampaignId 
			AND Completed IS NULL
	END

	EXEC [CampaignRunHistoryDto_CheckWorkTable] 
		@CampaignRunId = @Id
	
	--Update the errored records from the MKCampaignRunWorkTable to MKCampaignRunErrorLog table; success emails are already logged by different process
	INSERT INTO MKCampaignRunErrorLog 
	(
		Id,
		CampaignId,
		CampaignRunId,
		ContactId,
		ErrorMessage
	)
	SELECT NEWID(),
		@CampaignId,
		@Id,
		UserId,
		LogMessage 
	FROM MKCampaignRunWorkTable 
	WHERE CampaignRunId = @Id AND (ProcessingStatus = 4 OR ProcessingStatus = 3)

	--in case of AUTO Responders and campaigns , REMOVE all the processed contacts; 
	--this deletes all the contacts those are errored out OR those are processed with error after 3 reties OR successfullt sent
	-- Successfully sent statuses are getting updated to MKEmailSendLog table using different procedure. 
	DELETE FROM  MKCampaignRunWorkTable 
	WHERE CampaignRunId = @Id 
		AND (ProcessingStatus = 2 OR ProcessingStatus = 4 OR (ProcessingStatus = 3 AND ProcessingCount >= @RetryCount))

	--Cleanup the ResponseWorktable
	DELETE WT FROM MKCampaignRunResponseWorktable WT 
		JOIN @tbResponses T ON WT.ContactId = T.ContactId
	WHERE CampaignRunId = @Id
	
	--To avoid sending multiple times
	DELETE W FROM MKCampaignRunWorkTable W WHERE CampaignRunId = @Id
		AND EXISTS (SELECT 1 FROM MKEmailSendLog S WHERE S.CampaignRunId = @Id AND UserId = W.UserId)

	UPDATE H SET H.Sends = H.Sends + @SendCount
	FROM MKCampaignRunHistory H 
	WHERE Id = @Id

	SELECT @@ROWCOUNT
END
GO
PRINT 'Modify stored procedure CampaignRunHistoryDto_UpdateStatus'
GO
IF(OBJECT_ID('CampaignRunHistoryDto_UpdateStatus') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_UpdateStatus
GO
CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_UpdateStatus]  
(  
	@Id				uniqueidentifier,
	@Status			int ,
	@ModifiedBy		uniqueidentifier
)  
AS 
BEGIN
	DECLARE @UtcNow datetime, @EndTime datetime, @SendCount int
	SET @UtcNow = GETUTCDATE()

	IF @Status = 2 SET @EndTime = @UtcNow

	UPDATE MKCampaignRunHistory
	SET Status = @Status,
		EndDateTime = ISNULL(@EndTime, EndDateTime)
	WHERE Id = @Id

	IF @Status = 2
	BEGIN
		SELECT @SendCount = COUNT(1) FROM MKEmailSendLog WHERE CampaignRunId = @Id

		UPDATE MKCampaignRunHistory 
		SET Sends = @SendCount,
			Delivered = @SendCount - Bounces
		WHERE Id = @Id
	END
END
GO
PRINT 'Modify stored procedure CommerceCacheDto_Invalidate'
GO
IF(OBJECT_ID('CommerceCacheDto_Invalidate') IS NOT NULL)
	DROP PROCEDURE CommerceCacheDto_Invalidate
GO
CREATE PROCEDURE [dbo].[CommerceCacheDto_Invalidate]
(
	@Key		nvarchar(500),
	@SiteId		uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	IF @Key = 'InvalidateNavFilter' OR @Key = 'All'
	BEGIN
		UPDATE NVFilterQuery SET IsLatest = 0 

		UPDATE GLCacheUpdate SET NavFilterCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END
	
	IF @Key = 'InvalidateFacet' OR @Key = 'All'
	BEGIN
		EXEC Facet_ReloadProductFacetCache @ApplicationId = @SiteId

		UPDATE GLCacheUpdate SET FacetCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END
	
	IF @Key = 'InvalidateFeatureOutput' OR @Key = 'All'
	BEGIN
		EXEC Feature_PopulateFeatureOutput @ApplicationId = @SiteId

		UPDATE GLCacheUpdate SET FeatureOutputCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END
	
	IF @Key = 'InvalidatePriceSet' OR @Key = 'All'
	BEGIN
		EXEC Cache_RefreshFlatPriceSets
		EXEC Cache_RefreshProductMinEffectivePrice @ApplicationId = @SiteId

		UPDATE GLCacheUpdate SET PriceSetCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END

	IF @Key = 'Application' OR @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET ApplicationCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END

	IF @Key = 'ApplicationBaseData' OR @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET ApplicationBaseDataCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END

	IF @Key = 'SiteSettings' OR @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET SiteSettingsCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END

	IF @Key = 'All'
	BEGIN
		UPDATE GLCacheUpdate SET AllCacheDate = @UtcNow WHERE ApplicationId = @SiteId
	END
END
GO
PRINT 'Modify stored procedure DistributionDto_Get'
GO
IF(OBJECT_ID('DistributionDto_Get') IS NOT NULL)
	DROP PROCEDURE DistributionDto_Get
GO
CREATE PROCEDURE [dbo].[DistributionDto_Get]
(
	@Id				uniqueidentifier = NULL,
	@SiteId			uniqueidentifier = NULL,
	@ObjectId		uniqueidentifier = NULL,
	@ObjectTypeId	int = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@MaxRecords		int = NULL,
	@Query			nvarchar(max) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	IF @Query IS NULL
	BEGIN
		INSERT INTO @tbIds
		SELECT Id, ROW_NUMBER() OVER (ORDER BY ModifiedDate DESC, CreatedDate DESC) 
		FROM GLDistribution
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END

	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY Temp.RowNumber) AS RowNumber,
			COUNT(D.Id) OVER () AS TotalRecords,
			D.Id AS Id			
		FROM GLDistribution D
			JOIN @tbIds Temp ON D.Id = Temp.Id
		WHERE (@Id IS NULL OR D.Id = @Id)
			AND (@ObjectId IS NULL OR D.ObjectId = @ObjectId)
			AND (@ObjectTypeId IS NULL OR D.ObjectTypeId = @ObjectTypeId)
			AND (@SiteId IS NULL OR D.SiteId = @SiteId)
	)
	
	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT D.*,
		CU.UserFullName AS CreatedByFullName,
		T.RowNumber,
		T.TotalRecords
	FROM GLDistribution D
		JOIN @tbPagedResults T ON D.Id = T.Id
		LEFT JOIN VW_UserFullName CU ON CU.UserId = D.CreatedBy
	ORDER BY RowNumber

	SELECT DISTINCT DT.*, D.Id AS DistributionId,
		S.Title AS TargetTitle,
		[dbo].[SiteList_GetCount](S.Id, S.ListType, @SiteId) AS TargetCount
	FROM GLDistributionTarget DT
		JOIN GLDistribution D ON D.Id = DT.DistributionId
		JOIN @tbPagedResults T ON T.Id = D.Id
		LEFT JOIN SISiteList S ON DT.TargetId = S.Id AND DT.TargetTypeId = 1
END
GO
PRINT 'Modify stored procedure ContactListDto_GetContact'
GO
IF(OBJECT_ID('ContactListDto_GetContact') IS NOT NULL)
	DROP PROCEDURE ContactListDto_GetContact
GO
CREATE PROCEDURE [dbo].[ContactListDto_GetContact]
(
	@ContactListId	uniqueidentifier = NULL,
	@SearchXml		xml = NULL,
	@MaxRecords		int,
	@PageSize		int = NULL,   
	@PageNumber		int = NULL,
	@SiteId			uniqueidentifier
)
AS
BEGIN 
	DECLARE @ListType int, @TotalRecords int
	DECLARE @tbContactIds TABLE(Id uniqueidentifier primary key)

	IF @SearchXml IS NOT NULL
	BEGIN
		INSERT INTO @tbContactIds
		EXEC Contact_SearchContact 
			@Xml = @SearchXml,
			@ApplicationId = @SiteId, 
			@MaxRecords = @MaxRecords, 
			@ContactListId = NULL,
			@TotalRecords = @TotalRecords output, 
			@IncludeAddress = 0
	END
	ELSE IF @ContactListId IS NOT NULL OR @ContactListId != dbo.GetEmptyGUID()
	BEGIN
		SELECT @ListType = ListType from TADistributionLists WHERE Id = @ContactListId

		IF (@ListType = 0) --For manual
		BEGIN
			IF @MaxRecords IS NULL OR @MaxRecords = 0
			BEGIN
				INSERT INTO @tbContactIds
				SELECT DISTINCT UserId FROM TADistributionListUser
				WHERE DistributionListId = @ContactListId
					AND ContactListSubscriptionType != 3
			END
			ELSE
			BEGIN
				INSERT INTO @tbContactIds
				SELECT DISTINCT TOP (@MaxRecords) UserId FROM TADistributionListUser
				WHERE DistributionListId = @ContactListId
					AND ContactListSubscriptionType != 3
			END

			SELECT @TotalRecords = COUNT(DISTINCT(UserId)) FROM TADistributionListUser
			WHERE DistributionListId = @ContactListId
				AND ContactListSubscriptionType != 3
		END
		ELSE IF (@ListType = 1) --For auto list
		BEGIN
			DECLARE @QueryXml xml
			SELECT @QueryXml = Q.SearchXml FROM dbo.TADistributionListSearch S
				JOIN CTSearchQuery Q ON S.SearchQueryId = Q.Id
			WHERE S.DistributionListId = @ContactListId 
				
			INSERT INTO @tbContactIds
			EXEC Contact_SearchContact 
				@Xml = @queryXml,
				@ApplicationId = @SiteId, 
				@MaxRecords = @MaxRecords, 
				@ContactListId = @ContactListId,
				@TotalRecords = @TotalRecords output, 
				@IncludeAddress = 0
		END

		DELETE FROM TADistributionListSite WHERE DistributionListId = @ContactListId AND SiteId = @SiteId
		INSERT INTO TADistributionListSite
		(
			Id,
			DistributionListId,
			SiteId,
			Count,
			LastSynced
		)
		VALUES
		(
			NEWID(),
			@ContactListId,
			@SiteId,
			@TotalRecords,
			GETUTCDATE()
		)
	END

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SELECT DISTINCT C.Id,
			C.Email,
			C.FirstName,
			C.LastName,
			C.LeadScore,
			C.LastInteractionDate,
			C.ContactType,
			C.Status,
			C.CreatedDate
		FROM vw_contacts C
			JOIN @tbContactIds T ON C.Id = T.Id
		ORDER BY Email ASC
		OFFSET (@PageNumber - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY
	END
	ELSE
	BEGIN
		SELECT DISTINCT C.Id,
			C.Email,
			C.FirstName,
			C.LastName,
			C.LeadScore,
			C.LastInteractionDate,
			C.ContactType,
			C.Status,
			C.CreatedDate
		FROM vw_contacts C
			JOIN @tbContactIds T ON C.Id = T.Id
		ORDER BY Email ASC
	END

	SELECT @TotalRecords AS TotalRecords
END
GO
PRINT 'Modify stored procedure Contact_UpdateContactCount'
GO
IF(OBJECT_ID('Contact_UpdateContactCount') IS NOT NULL)
	DROP PROCEDURE Contact_UpdateContactCount
GO
CREATE PROCEDURE [dbo].[Contact_UpdateContactCount]
(
    @ContactListId	uniqueidentifier
)
AS 
BEGIN
	
	DECLARE @IsGlobal bit, @SiteId uniqueidentifier
	SELECT TOP 1 @IsGlobal = IsGlobal, @SiteId = ApplicationId
	FROM TADistributionLists WHERE Id = @ContactListId

	CREATE TABLE #TBSites (ParentSiteId uniqueidentifier, SiteId uniqueidentifier)
	CREATE CLUSTERED INDEX IX_TBSites_ParentSiteId_SiteId ON #TBSites (ParentSiteId, SiteId)

	INSERT INTO #TBSites
	SELECT S.Id, C.SiteId FROM SISite S
		CROSS APPLY dbo.GetChildrenSites(S.Id, 1) C

	DELETE T
	FROM #tempContactSearchOutput T
		JOIN vw_contacts C ON T.Id = C.Id
	WHERE C.Status != 1

	DECLARE @tbContactCount TABLE (SiteId uniqueidentifier, ContactCount int)
	INSERT INTO @tbContactCount
	SELECT S.ParentSiteId, COUNT(DISTINCT(T.Id)) AS ContactCount FROM #tempContactSearchOutput T
		JOIN vw_contactSite CS ON T.Id = CS.UserId
		JOIN #TBSites S ON S.SiteId = CS.SiteId
	WHERE @IsGlobal = 1 OR S.ParentSiteId = @SiteId
	GROUP BY S.ParentSiteId

	DROP TABLE #TBSites
		
	DELETE FROM TADistributionListSite WHERE DistributionListId = @ContactListId
	INSERT INTO TADistributionListSite
	(
		Id,
		DistributionListId,
		SiteId,
		Count,
		LastSynced
	)
	SELECT
		NEWID(),
		@ContactListId,
		SiteId,
		ContactCount,
		GETUTCDATE()
	FROM @tbContactCount

	SELECT @@ROWCOUNT

END
GO
PRINT 'Modify stored procedure ContactListDto_ExecuteFilter'
GO
IF(OBJECT_ID('ContactListDto_ExecuteFilter') IS NOT NULL)
	DROP PROCEDURE ContactListDto_ExecuteFilter
GO
CREATE PROCEDURE [dbo].[ContactListDto_ExecuteFilter]
(
    @Id			uniqueidentifier,
	@SiteId		uniqueidentifier
)
AS
BEGIN
	DECLARE @SearchXml xml, @TotalRecords int

	SELECT TOP 1 @SearchXml = Q.SearchXml FROM TADistributionLists L
		JOIN TADistributionListSearch SQ ON L.Id = SQ.DistributionListId
		JOIN CTSearchQuery Q ON SQ.SearchQueryId = Q.Id
	WHERE L.Id = @Id AND L.ListType = 0

	IF @SearchXml IS NOT NULL
	BEGIN
		DELETE FROM TADistributionListUser WHERE DistributionListId = @Id AND ContactListSubscriptionType = 1 

		CREATE TABLE #tbContacts (Id uniqueidentifier primary key)
		INSERT INTO #tbContacts
		EXEC Contact_SearchContact 
			@Xml = @SearchXml,
			@ApplicationId = @SiteId, 
			@MaxRecords = 0,
			@TotalRecords = @TotalRecords OUTPUT,
			@IncludeAddress = 0

		IF EXISTS (SELECT TOP 1 Id FROM TADistributionListUser WHERE DistributionListId = @Id)
		BEGIN
			DELETE DLU FROM TADistributionListUser DLU
				JOIN #tbContacts C ON C.Id = DLU.UserId
			WHERE DLU.DistributionListId = @Id
		END

		INSERT INTO TADistributionListUser
        ( 
			Id,
            DistributionListId,
            UserId,
			ContactListSubscriptionType
        )
        SELECT  
			NEWID(),
            @Id DistributionListId,
            C.Id,
			1
        FROM #tbContacts C

		INSERT INTO TREventQueue
		(
			Id,
			ContactId,
			VisitorId,
			EventType,
			ObjectId,
			DataEnvelope,
			CreatedDate,
			SiteId
		)
		SELECT NEWID(),
			C.Id,
			C.Id,
			501,
			@Id,
			NULL,
			GETUTCDATE(),
			@SiteId
		FROM #tbContacts C

		DELETE FROM TADistributionListUser WHERE DistributionListId = @Id 
			AND UserId NOT IN (SELECT Id FROM vw_contacts)

		SELECT @TotalRecords = COUNT(1) FROM TADistributionListUser WHERE DistributionListId = @Id AND ContactListSubscriptionType != 3
		
		DELETE FROM TADistributionListSite WHERE DistributionListId = @Id
		INSERT INTO TADistributionListSite
		(
			Id,
			DistributionListId,
			SiteId,
			Count,
			LastSynced
		)
		VALUES
		(
			NEWID(),
			@Id,
			@SiteId,
			@TotalRecords,
			GETUTCDATE()
		)
	END
END
GO
PRINT 'Modify stored procedure Contact_GetContactByIdsFromTemp'
GO
IF(OBJECT_ID('Contact_GetContactByIdsFromTemp') IS NOT NULL)
	DROP PROCEDURE Contact_GetContactByIdsFromTemp
GO
CREATE PROCEDURE [dbo].[Contact_GetContactByIdsFromTemp]
(
	@ApplicationId	uniqueidentifier,
    @Status			int,
    @SortOrder		varchar(25) = NULL ,
    @MaxRecords		int,
	@IncludeAddress bit = 1,
	@TotalRecords	int = null
)
AS 
BEGIN 
	IF(@TotalRecords IS NULL)
		SELECT @TotalRecords = COUNT(Id) from #tempContactSearchOutput

    IF (@MaxRecords = 0) 
		SELECT DISTINCT Id FROM #tempContactSearchOutput 
    ELSE 
        SELECT DISTINCT TOP (@MaxRecords) Id FROM #tempContactSearchOutput
END
GO
PRINT 'Modify stored procedure Contact_FilterBySiteContext'
GO
IF(OBJECT_ID('Contact_FilterBySiteContext') IS NOT NULL)
	DROP PROCEDURE Contact_FilterBySiteContext
GO
CREATE PROCEDURE [dbo].[Contact_FilterBySiteContext]
(
	@ApplicationId	uniqueidentifier,
	@ReturnScalar	bit = 1
)
AS 
BEGIN
IF EXISTS (SELECT TOP 1 Id FROM #tempContactSearchOutput) 
BEGIN
	DECLARE @Lft bigint, @Rgt bigint, @MasterSiteId uniqueidentifier
    DECLARE @SiteIds TABLE(SiteId uniqueidentifier)
	
	SELECT @Lft = LftValue, @Rgt = RgtValue, @MasterSiteId = MasterSiteId
    FROM SISite WHERE Id = @ApplicationId
                
	INSERT INTO @SiteIds
    SELECT DISTINCT Id FROM dbo.SISite
    WHERE LftValue >= @Lft AND RgtValue <= @Rgt AND MasterSiteId = @MasterSiteId
                        
    DELETE T
    FROM #tempContactSearchOutput T
		LEFT JOIN (	SELECT CS.ContactId AS UserId FROM dbo.MKContactSite CS WITH (NOLOCK)
						INNER JOIN @SiteIds S ON CS.SiteId = S.SiteId
                    UNION
                    SELECT UserId FROM dbo.USSiteUser CS WITH (NOLOCK)
                        INNER JOIN @SiteIds S ON CS.SiteId = S.SiteId
                ) TV ON TV.UserId = t.Id
    WHERE TV.UserId IS NULL

	DELETE T
	FROM #tempContactSearchOutput T
		LEFT JOIN vw_contacts C ON T.Id = C.Id
	WHERE C.Status != 1 OR C.Id IS NULL

    IF(@ReturnScalar = 1)
		SELECT COUNT(Id) FROM #tempContactSearchOutput
END
ELSE
	SELECT  0
END
GO
PRINT 'Modify stored procedure CampaignRunHistory_GetOverlayData'
GO
IF(OBJECT_ID('CampaignRunHistory_GetOverlayData') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistory_GetOverlayData
GO
CREATE PROCEDURE [dbo].[CampaignRunHistory_GetOverlayData] 
(
	@CampaignRunId	uniqueidentifier
)  
AS
BEGIN  
	SELECT	TargetId, ContainerId, ControlId, Clicks
	FROM	MKCampaignRunLinkTracking
	WHERE	CampaignRunId = @CampaignRunId
END
GO
PRINT 'Modify stored procedure ContactListDto_Delete'
GO
IF(OBJECT_ID('ContactListDto_Delete') IS NOT NULL)
	DROP PROCEDURE ContactListDto_Delete
GO
CREATE PROCEDURE [dbo].[ContactListDto_Delete]
(
	@Id				uniqueidentifier,
	@SiteId			uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	DECLARE @DeletedGroupId uniqueidentifier, @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	SELECT @DeletedGroupId = Id FROM [dbo].[TADistributionGroup] WHERE NodeType = 11 AND ApplicationId = @SiteId

	IF @DeletedGroupId IS NULL
	BEGIN
		SET @DeletedGroupId = NEWID()

		INSERT INTO [TADistributionGroup](Id, Title, CreatedBy, CreatedDate, ApplicationId, NodeType, DisplayOrder)
		SELECT @DeletedGroupId, 'Deleted Contact Lists', @ModifiedBy, @UtcNow, @SiteId, 11, 9999
	END

	UPDATE TADistributionLists 
	SET Status = dbo.GetDeleteStatus(),
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @UtcNow
	WHERE Id=@Id

	
	UPDATE [dbo].[TADistributionListGroup]
	SET [DistributionGroupId] = @DeletedGroupId 
	WHERE DistributionId = @Id
END
GO
PRINT 'Modify stored procedure SiteDto_Get'
GO
IF(OBJECT_ID('SiteDto_Get') IS NOT NULL)
	DROP PROCEDURE SiteDto_Get
GO
CREATE PROCEDURE [dbo].[SiteDto_Get]
(
	@Id						uniqueidentifier = NULL,
	@Ids					nvarchar(max)=NULL,
	@MasterSiteId			uniqueidentifier = NULL,
	@IncludeVariantSites	bit = NULL,
	@CurrentMasterSiteOnly	bit = NULL,
	@CampaignId				uniqueidentifier = NULL,
	@SiteListId				uniqueidentifier = NULL,
	@UserId					uniqueidentifier = NULL,
	@SiteId					uniqueidentifier = NULL,
	@Status					int = NULL,
	@PageNumber				int = NULL,
	@PageSize				int = NULL,
	@MaxRecords				int = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@Keyword				nvarchar(MAX) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50), 
		@IdsExist bit, @LftValue int, @RgtValue int
	SET @PageLowerBound = @PageSize * @PageNumber

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @IncludeVariantSites IS NULL SET @IncludeVariantSites = 1
	IF @Status IS NULL SET @Status = 1
	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'

	IF @MasterSiteId IS NULL AND @CurrentMasterSiteOnly = 1
		SELECT TOP 1 @MasterSiteId = MasterSiteId FROM SISite WHERE Id = @SiteId
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbSiteIds TABLE (Id uniqueidentifier)
	
	IF @Ids IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT Items FROM dbo.SplitGUID(@Ids, ',') 
	END
	IF @SiteListId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM SISiteListSite WHERE SiteListId = @SiteListId
	END
	
	IF @CampaignId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		DECLARE @CampaignSiteId uniqueidentifier
		SELECT TOP 1 @CampaignSiteId = ApplicationId FROM MKCampaign WHERE Id = @CampaignId
		IF @CampaignSiteId = @SiteId SET @CampaignSiteId = NULL

		INSERT INTO @tbSiteIds
		SELECT SiteId FROM MKCampaignRunHistory H
		WHERE EXISTS (SELECT 1 FROM MKCampaignSend S WHERE H.CampaignSendId = S.Id 
				AND CampaignId = @CampaignId AND (@CampaignSiteId IS NULL OR SiteId = @SiteId))
	END

	IF @UserId IS NOT NULL
	BEGIN
		SET @IdsExist = 1
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM [dbo].[GetAccessibleSitesForUser](@UserId)
	END
	
	INSERT INTO @tbIds
	SELECT S.Id AS Id, ROW_NUMBER() OVER (ORDER BY
							CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
							CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
							CASE WHEN @SortClause IS NULL THEN CreatedDate END ASC	
						) AS RowNumber
	FROM SISite AS S
	WHERE (@Id IS NULL OR S.Id = @Id) AND
		(@Status IS NULL OR S.Status = @Status) AND
		(@MasterSiteId IS NULL OR S.Id = @MasterSiteId OR S.MasterSiteId = @MasterSiteId) AND
		(@IncludeVariantSites = 1 OR S.MasterSiteId = S.Id OR S.MasterSiteId IS NULL) AND
		(@IdsExist IS NULL OR EXISTS (Select 1 FROM @tbSiteIds T Where T.Id = S.Id)) AND
		(@Keyword IS NULL OR (S.Title like @Keyword OR S.Description like @Keyword OR S.PrimarySiteUrl like @Keyword OR S.ExternalCode like @Keyword))
		
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				S.Id AS Id
		FROM SISite S
			JOIN @tbIds T ON T.Id = S.Id
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT		S.*,
				SiteHierarchyPath AS SitePath,
				T.RowNumber,
				T.TotalRecords
	FROM SISite AS S
		JOIN @tbPagedResults AS T ON T.Id = S.Id
	ORDER BY RowNumber

	SELECT V.*, A.Title AS AttributeTitle
	FROM ATSiteAttributeValue V
		JOIN ATAttribute A ON A.Id = V.AttributeId
		JOIN @tbPagedResults T ON V.SiteId = T.Id
END
GO
PRINT 'Modify stored procedure DistributionRunDto_Get'
GO
IF(OBJECT_ID('DistributionRunDto_Get') IS NOT NULL)
	DROP PROCEDURE DistributionRunDto_Get
GO
CREATE PROCEDURE [dbo].[DistributionRunDto_Get]
(
	@Id				uniqueidentifier = NULL,
	@SiteId			uniqueidentifier = NULL,
	@ObjectId		uniqueidentifier = NULL,
	@ObjectTypeId	int = NULL,
	@Status			int = NULL,
	@TargetId		uniqueidentifier = NULL,
	@LatestOnly		bit = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@MaxRecords		int = NULL,
	@Query			nvarchar(max) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @EmptyGuid uniqueidentifier, @SortClause nvarchar(50)
	SET @EmptyGuid = dbo.GetEmptyGUID()
	
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	IF (@ObjectTypeId = 0) SET @ObjectTypeId = NULL 

	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	IF @Query IS NULL
	BEGIN
		INSERT INTO @tbIds
		SELECT Id, ROW_NUMBER() OVER (ORDER BY ModifiedDate DESC, CreatedDate DESC) 
		FROM GLDistributionRun
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END

	IF @LatestOnly = 1
	BEGIN
		;WITH CTE AS(
			SELECT ROW_NUMBER() OVER (PARTITION BY R.DistributionId ORDER BY R.CreatedDate DESC) AS ObjectRank,
				R.Id AS Id			
			FROM GLDistributionRun R
				JOIN @tbIds Temp ON R.Id = Temp.Id
		)

		DELETE I
		FROM @tbIds I
			JOIN CTE T ON I.Id = T.Id AND T.ObjectRank != 1
	END
	
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY Temp.RowNumber) AS RowNumber,
			COUNT(R.Id) OVER () AS TotalRecords,
			R.Id AS Id			
		FROM GLDistribution D
			JOIN GLDistributionRun R ON R.DistributionId = D.Id
			JOIN @tbIds Temp ON R.Id = Temp.Id
		WHERE (@Id IS NULL OR R.Id = @Id)
			AND (@ObjectId IS NULL OR D.ObjectId = @ObjectId)
			AND (@ObjectTypeId IS NULL OR D.ObjectTypeId = @ObjectTypeId)
			AND (@Status IS NULL OR R.Status = @Status)
			AND (@SiteId IS NULL OR R.SiteId = @SiteId)
			AND (@TargetId IS NULL OR EXISTS(SELECT 1 FROM GLDistributionRunTarget T WHERE T.DistributionRunId = R.Id))
	)
	
	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT R.*,
		CU.UserFullName AS CreatedByFullName,
		T.RowNumber,
		T.TotalRecords
	FROM GLDistributionRun R
		JOIN @tbPagedResults T ON R.Id = T.Id
		LEFT JOIN VW_UserFullName CU ON CU.UserId = R.CreatedBy
	ORDER BY RowNumber

	SELECT D.*,
		CU.UserFullName AS CreatedByFullName
	FROM GLDistribution D
		JOIN GLDistributionRun R ON D.Id = R.DistributionId
		JOIN @tbPagedResults T ON R.Id = T.Id
		LEFT JOIN VW_UserFullName CU ON CU.UserId = R.CreatedBy

	SELECT DISTINCT RT.*, R.Id AS DistributionId,
		S.Title AS TargetTitle,
		[dbo].[SiteList_GetCount](S.Id, S.ListType, @SiteId) AS TargetCount
	FROM GLDistributionRunTarget RT
		JOIN GLDistributionRun R ON R.Id = RT.DistributionRunId
		JOIN @tbPagedResults T ON T.Id = R.Id
		LEFT JOIN SISiteList S ON RT.TargetId = S.Id AND RT.TargetTypeId = 1
END
GO
PRINT 'Modify stored procedure PageDto_Import'
GO
IF(OBJECT_ID('PageDto_Import') IS NOT NULL)
	DROP PROCEDURE PageDto_Import
GO
CREATE PROCEDURE [dbo].[PageDto_Import]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SourceId				uniqueidentifier,
	@TargetSiteId			uniqueidentifier,
	@PageMapNodeId			uniqueidentifier = NULL,
	@Publish				bit = NULL,	
	@Overwrite				bit = NULL,		
	@ModifiedBy				uniqueidentifier,
	@PreserveVariantContent bit = 1
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @UtcNow datetime, @EmptyGuid uniqueidentifier

	SET @UtcNow = GETUTCDATE()
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @Id = [dbo].[Page_GetVariantId](@SourceId, @TargetSiteId)

	DECLARE @DisplayOrder int
	IF @PageMapNodeId IS NULL
	BEGIN
		DECLARE @ParentPageMapNode uniqueidentifier
		SELECT TOP 1 @ParentPageMapNode = PageMapNodeId, @DisplayOrder = DisplayOrder FROM PageMapNodePageDef 
			WHERE PageDefinitionId = @SourceId
		SET @PageMapNodeId = [dbo].[Menu_GetVariantId](@ParentPageMapNode, @TargetSiteId)
	END

	IF @PageMapNodeId IS NULL
		RETURN

	SELECT @ModifiedBy = ModifiedBy FROM PageDefinition WHERE PageDefinitionId = @SourceId 
	IF @Publish IS NULL AND EXISTS (SELECT 1 FROM PageDefinition WHERE PageDefinitionId = @SourceId AND PageStatus = 8)
		SET @Publish = 1

	IF @Id IS NULL
	BEGIN
		SET @Id = NEWID()
		INSERT INTO [dbo].[PageDefinition]
		(
			[PageDefinitionId],
			[TemplateId],
			[SiteId],
			[Title],
			[Description],
			[PageStatus],
			[WorkflowState],
			[PublishCount],
			[PublishDate],
			[FriendlyName],
			[WorkflowId],
			[CreatedBy],
			[CreatedDate],
			[ModifiedBy],
			[ModifiedDate],
			[ArchivedDate],
			[StatusChangedDate],
			[AuthorId],
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			[SourcePageDefinitionId],
			[CustomAttributes]
		)
		SELECT @Id,
			[TemplateId],
			@TargetSiteId,
			[Title],
			D.[Description],
			CASE WHEN @Publish = 1 THEN 8 ELSE PageStatus END,
			1,
			0,
			'1900-01-01 00:00:00.000',
			[FriendlyName],
			@EmptyGuid,
			@ModifiedBy,
			@UtcNow,
			@ModifiedBy,
			@UtcNow,
			[ArchivedDate],
			[StatusChangedDate],
			@ModifiedBy,
			[EnableOutputCache],
			[OutputCacheProfileName],
			[ExcludeFromSearch],
			[IsDefault],
			[IsTracked],
			[HasForms],
			[TextContentCounter],
			PageDefinitionId,
			D.[CustomAttributes]
		FROM [dbo].[PageDefinition] D
		WHERE PageDefinitionId = @SourceId				

		UPDATE PageMapNode SET TargetId = @Id
		WHERE TargetId = @SourceId AND SiteId = @TargetSiteId
		 
		INSERT INTO [dbo].[PageDefinitionSegment]
		(
			[Id],
			[PageDefinitionId],
			[DeviceId],
			[AudienceSegmentId],
			[UnmanagedXml],
			[ZonesXml]
		)
		SELECT NEWID(), 
			@Id,
			[DeviceId],
			[AudienceSegmentId],
			dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
			[ZonesXml]
		FROM [dbo].[PageDefinitionSegment] PS 
		WHERE PageDefinitionId = @SourceId

		INSERT INTO [dbo].[PageDetails]
		(
			[PageId],
			[PageDetailXML]
		)
		SELECT 
			@Id,
			[PageDetailXML]
		FROM [dbo].[PageDetails] PD
		WHERE PD.PageId = @SourceId
	END
	ELSE
	BEGIN
		UPDATE C SET
			C.Title = P.Title,
			C.Description = P.Description,
			C.PageStatus = CASE WHEN @Publish = 1 THEN 8 ELSE P.PageStatus END,
			C.FriendlyName = CASE WHEN @PreserveVariantContent = 0 THEN P.FriendlyName ELSE C.FriendlyName END,
			C.EnableOutputCache = P.EnableOutputCache,
			C.OutputCacheProfileName = P.OutputCacheProfileName,
			C.ExcludeFromSearch = P.ExcludeFromSearch,
			C.HasForms = P.HasForms,
			C.ScheduledPublishDate = P.ScheduledPublishDate,
			C.ScheduledArchiveDate = P.ScheduledArchiveDate,
			C.ModifiedDate = @UtcNow,
			C.ModifiedBy = P.ModifiedBy
		FROM PageDefinition P, PageDefinition C
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.UnmanagedXml = dbo.Page_GetUnmanagedXml(P.[UnmanagedXml], @TargetSiteId),
			C.ZonesXml = P.ZonesXml
		FROM PageDefinitionSegment P
			JOIN PageDefinitionSegment C ON P.DeviceId = C.DeviceId AND P.AudienceSegmentId = C.AudienceSegmentId
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId

		UPDATE C SET 
			C.ContentId = P.ContentId,
			C.InWFContentId = P.InWFContentId,
			C.ContentTypeId = P.ContentTypeId,
			C.IsModified = P.IsModified,
			C.[IsTracked] = P.[IsTracked],
			C.[Visible] = P.[Visible],
			C.[InWFVisible] = P.[InWFVisible],
			C.[CustomAttributes] = P.[CustomAttributes],
			C.[DisplayOrder] = P.[DisplayOrder],
			C.[InWFDisplayOrder] = P.[InWFDisplayOrder]
		FROM PageDefinitionContainer P
			JOIN PageDefinitionContainer C ON P.ContainerName = C.ContainerName
			LEFT JOIN COContent CO ON C.ContentId = CO.Id
		WHERE C.PageDefinitionId = @Id AND P.PageDefinitionId = @SourceId
			AND (@Overwrite = 1 OR CO.Id IS NULL OR CO.ApplicationId != @TargetSiteId)
		
		IF @PreserveVariantContent = 0
		BEGIN
			UPDATE C 
			SET	C.PageDetailXML = P.PageDetailXML
			FROM PageDetails P, PageDetails C
			WHERE C.PageId = @Id AND P.PageId = @SourceId	
		END

		DELETE FROM PageMapNodePageDef WHERE PageDefinitionId = @Id
	END

	IF EXISTS (SELECT 1 FROM ATPageAttributeValue WHERE PageDefinitionId = @SourceId AND IsShared = 1)
	BEGIN
		WITH AttributeCTE AS
		(
			SELECT V.AttributeId FROM ATPageAttributeValue V  
			WHERE V.PageDefinitionId = @SourceId AND IsShared = 1
		)

		DELETE P FROM ATPageAttributeValue P
			JOIN AttributeCTE C ON C.AttributeId = P.AttributeId
		WHERE P.PageDefinitionId = @Id 

		INSERT INTO ATPageAttributeValue
		(
			Id,   
			PageDefinitionId,   
			AttributeId,  
			AttributeEnumId,  
			[Value],
			Notes,  
			CreatedDate,  
			CreatedBy
		)
		SELECT NEWID(),
			@Id,
			AttributeId,
			AttributeEnumId,
			Value,
			Notes,
			@UtcNow,  
			CreatedBy
		FROM ATPageAttributeValue
		WHERE PageDefinitionId = @SourceId AND IsShared = 1
	END

	INSERT INTO PageMapNodePageDef
	(
		PageDefinitionId,
		PageMapNodeId,
		DisplayOrder
	)
	SELECT
		@Id,
		@PageMapNodeId,
		@DisplayOrder

	INSERT INTO [PageDefinitionContainer]
	(
		[Id],
		PageDefinitionSegmentId ,
		[PageDefinitionId],
		[ContainerId],
		[ContentId],
		[InWFContentId],
		[ContentTypeId],
		[IsModified],
		[IsTracked],
		[Visible],
		[InWFVisible],
		DisplayOrder,
		InWFDisplayOrder,
		[CustomAttributes],
		[ContainerName]
	)
	SELECT NEWID(),
		CS.Id,
		@Id,
		ContainerId,
		ContentId,
		InWFContentId,
		ContentTypeId,
		IsModified,
		PC.IsTracked,
		Visible,
		InWFVisible,
		PC.DisplayOrder, 
		PC.InWFDisplayOrder, 
		PC.CustomAttributes,
		ContainerName
	FROM PageDefinitionContainer PC
		JOIN PageDefinitionSegment PS ON PC.PageDefinitionSegmentId = PS.Id
		JOIN PageDefinitionSegment CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
	WHERE PC.PageDefinitionId = @SourceId
		AND (@Id IS NULL OR ContainerName NOT IN (SELECT ContainerName FROM PageDefinitionContainer WHERE PageDefinitionId = @Id))

	DECLARE @SourceTargetMenuId uniqueidentifier
	SELECT TOP 1 @SourceTargetMenuId = PageMapNodeId FROM PageMapNode PN
		JOIN PageDefinition PD ON PN.TargetId = PD.PageDefinitionId AND PD.SiteId = PN.SiteId
	WHERE PD.PageDefinitionId = @SourceId

	IF @SourceTargetMenuId IS NOT NULL
	BEGIN
		UPDATE PageMapNode SET
			TargetId = @Id,
			Target = 1,
			TargetUrl = ''
		WHERE SourcePageMapNodeId = @SourceTargetMenuId
			AND SiteId = @TargetSiteId
	END

	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[USSecurityLevelObject]
	(
		[ObjectId],
		[ObjectTypeId],
		[SecurityLevelId]
	)
	SELECT @Id,
		ObjectTypeId,
		SecurityLevelId
	FROM [USSecurityLevelObject] S
	WHERE S.ObjectId = @SourceId

	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageScript]
	(
		[PageDefinitionId],
		[ScriptId],
		[CustomAttributes]
	)
	SELECT @Id,
		[ScriptId],
		[CustomAttributes]
	FROM [dbo].[SIPageScript] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO [dbo].[SIPageStyle]
	(
		[PageDefinitionId],
		[StyleId],
		[CustomAttributes]
	)
	SELECT @Id,
		[StyleId],
		[CustomAttributes]
	FROM [dbo].[SIPageStyle] PS
	WHERE PS.PageDefinitionId = @SourceId

	DELETE FROM COTaxonomyObject WHERE ObjectId = @Id
	INSERT INTO [dbo].[COTaxonomyObject]
	(
		[Id],
		[TaxonomyId],
		[ObjectId],
		[ObjectTypeId],
		[SortOrder],
		[Status],
		[CreatedBy],
		[CreatedDate],
		[ModifiedBy],
		[ModifiedDate]
	)
	SELECT NEWID(),
		[TaxonomyId],
		@Id,
		[ObjectTypeId],
		[SortOrder],
		[Status],
		@ModifiedBy,
		@UtcNow,
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[COTaxonomyObject] OT
	WHERE OT.ObjectId = @SourceId

	DECLARE @VersionId uniqueidentifier, @SourceVersionId uniqueidentifier, 
		@VersionNumber int, @RevisionNumber int, @VersionStatus int
	SET @VersionId = NEWID()
	SELECT TOP 1 @SourceVersionId = Id FROM VEVersion 
	WHERE ObjectId = @SourceId ORDER BY CreatedDate DESC	

	SELECT TOP 1 @VersionNumber = VersionNumber, @RevisionNumber = RevisionNumber FROM VEVersion
	WHERE ObjectId = @Id ORDER BY CreatedDate DESC

	IF @Publish = 1
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0) + 1
		SET @VersionNumber = 0
		SET @VersionStatus = 1
	END
	ELSE
	BEGIN
		SET @RevisionNumber = ISNULL(@RevisionNumber, 0)
		SET @VersionNumber = ISNULL(@VersionNumber, 0) + 1
		SET @VersionStatus = 0
	END

	INSERT INTO [dbo].[VEVersion]
	(
		[Id],
		[ApplicationId],
		[ObjectTypeId],
		[ObjectId],
		[CreatedDate],
		[CreatedBy],
		[VersionNumber],
		[RevisionNumber],
		[Status],
		[ObjectStatus],
		[VersionStatus],
		[XMLString],
		[Comments],
		[TriggeredObjectId],
		[TriggeredObjectTypeId]
	)          
	SELECT TOP 1 @VersionId,
		@TargetSiteId,
		8, 
		@Id, 
		@UtcNow,
		@ModifiedBy,
		@VersionNumber,
		@RevisionNumber,
		@VersionStatus,
		ObjectStatus,
		VersionStatus, 
		dbo.UpdatePageXml(XMLString, @Id, P.WorkflowId, P.WorkflowState, P.PageStatus, 1, 
			P.CreatedBy, P.CreatedDate, P.AuthorId, P.SiteId, P.SourcePageDefinitionId, @PageMapNodeId), 
		'Imported from master',
		TriggeredObjectId,
		TriggeredObjectTypeId       
	FROM VEVersion V, 
		PageDefinition P
	WHERE V.Id = @SourceVersionId
		AND P.PageDefinitionId = @Id

	IF @Publish = 1
		UPDATE VEVersion SET Status = 1 WHERE ObjectId = @Id

	INSERT INTO [dbo].[VEPageDefSegmentVersion]
	(
		[Id],
		[VersionId],
		[PageDefinitionId],
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],  
		[ContainerContentXML],        
		[UnmanagedXml],
		[ZonesXml],
		[CreatedBy],
		[CreatedDate]
	)
	SELECT NEWID(),
		@VersionId,
		@Id,
		[MinorVersionNumber],
		[DeviceId],
		[AudienceSegmentId],
		CASE WHEN @Overwrite = 1 THEN [ContainerContentXML] 
			ELSE [dbo].[Page_GetContainerXml](@Id, AudienceSegmentId, DeviceId) END,
		dbo.Page_GetUnmanagedXml([UnmanagedXml], @TargetSiteId),
		[ZonesXml],
		@ModifiedBy,
		@UtcNow
	FROM [dbo].[VEPageDefSegmentVersion]
	WHERE VersionId = @SourceVersionId

	INSERT INTO [dbo].[VEPageContentVersion]
	(
		[Id],
		[ContentVersionId],
		[IsChanged],
		[PageDefSegmentVersionId]
	)
	SELECT NEWID(),
		ContentVersionId,
		IsChanged,
		CS.Id
	FROM [dbo].[VEPageContentVersion] PC
		JOIN VEPageDefSegmentVersion PS ON PC.PageDefSegmentVersionId = PS.Id
		JOIN VEPageDefSegmentVersion CS ON PS.DeviceId = CS.DeviceId 
			AND PS.AudienceSegmentId = CS.AudienceSegmentId
			AND CS.PageDefinitionId = @Id
			AND PS.VersionId = @SourceVersionId
			AND CS.VersionId = @VersionId

	IF @Publish = 1
	BEGIN
		DECLARE @PublishCount int
		SET @PublishCount = (SELECT ISNULL(PublishCount, 0) + 1 FROM PageDefinition WHERE PageDefinitionId = @Id)

		UPDATE PageDefinition SET
			PublishCount = @PublishCount,
			PublishDate = @UtcNow,
			PageStatus = 8
		WHERE PageDefinitionId = @Id

		DECLARE @WorkflowId uniqueidentifier
		SELECT TOP 1 @WorkflowId = WorkflowId FROM WFWorkflowExecution WHERE ObjectId = @Id AND Completed = 0

		IF @WorkflowId IS NOT NULL
		BEGIN
			EXEC WorkflowExecution_Save
				@WorkflowId = @WorkflowId,
				@SequenceId = @EmptyGuid,
				@ObjectId = @Id,
				@ObjectTypeId = 8,
				@ActorId = @ModifiedBy,
				@ActorType = 1,
				@ActionId = 8,
				@Remarks = N'Page imported from master',
				@Completed = 1
		END
	END
END
GO
PRINT 'Modify stored procedure SiteListDto_Fill'
GO
IF(OBJECT_ID('SiteListDto_Fill') IS NOT NULL)
	DROP PROCEDURE SiteListDto_Fill
GO
CREATE PROCEDURE [dbo].[SiteListDto_Fill]
(
	@SiteListId		uniqueidentifier = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tbSite TABLE(SiteListId uniqueidentifier, SiteId uniqueidentifier, RowNumber int)
	DECLARE @UtcNow datetime, @Id uniqueidentifier
	SET @UtcNow = GETUTCDATE()

	IF @SiteListId IS NULL
	BEGIN
		DECLARE SITELIST_CURSOR CURSOR FOR SELECT Id FROM SISiteList WHERE Status = 1

		OPEN SITELIST_CURSOR 
		FETCH NEXT FROM SITELIST_CURSOR INTO @Id

		WHILE (@@FETCH_STATUS != -1)
		BEGIN
		
			INSERT INTO @tbSite (SiteId, RowNumber)
			EXEC RuleResultDto_Get @ObjectId = @Id, @ObjectTypeId = 50

			UPDATE @tbSite SET SiteListId = @Id WHERE SiteListId IS NULL

			FETCH NEXT FROM SITELIST_CURSOR INTO @Id 		
		END

		CLOSE SITELIST_CURSOR
		DEALLOCATE SITELIST_CURSOR
	END
	ELSE
	BEGIN
		INSERT INTO @tbSite (SiteId, RowNumber)
		EXEC RuleResultDto_Get @ObjectId = @SiteListId, @ObjectTypeId = 50

		UPDATE @tbSite SET SiteListId = @SiteListId WHERE SiteListId IS NULL
	END	

	DELETE FROM SISiteListSite WHERE @SiteListId IS NULL OR SiteListId = @SiteListId
	INSERT INTO SISiteListSite(SiteListId, SiteId)
	SELECT DISTINCT SiteListId, SiteId FROM @tbSite T
		JOIN SISite S ON T.SiteId = S.Id AND S.Status = 1
END
GO
PRINT 'Modify stored procedure SiteListDto_Get'
GO
IF(OBJECT_ID('SiteListDto_Get') IS NOT NULL)
	DROP PROCEDURE SiteListDto_Get
GO
CREATE PROCEDURE [dbo].[SiteListDto_Get]
(
	@Id				uniqueidentifier = NULL,
	@Ids			nvarchar(max) = NULL,
	@SiteId			uniqueidentifier = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@Status			int = NULL,
	@MaxRecords		int = NULL,
	@SortBy			nvarchar(100) = NULL,  
	@SortOrder		nvarchar(10) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(500)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	
	
	IF @Id IS NOT NULL
	BEGIN
		INSERT INTO @tbIds SELECT @Id, 0
	END
	ELSE IF @Ids IS NOT NULL
	BEGIN
		INSERT INTO @tbIds
		SELECT Items, 0 FROM dbo.SplitGUID(@Ids, ',')
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		SELECT Id,  ROW_NUMBER() OVER (ORDER BY
				CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
				CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
				CASE WHEN @SortClause IS NULL THEN CreatedDate END ASC	
			) AS RowNumber
		FROM SISiteList
	END
		
	DECLARE @tbPagedResults TABLE(Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS (
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY S.ListType, T.RowNumber) AS RowNumber,
			COUNT(T.Id) OVER () AS TotalRecords,
			S.Id AS Id
		FROM SISiteList S
			JOIN @tbIds T ON S.Id = T.Id
		WHERE ((@Status IS NULL AND S.Status != 3) OR S.Status = @Status) AND
			(
				(ShareType = 0 AND SiteId = @SiteId) OR
				(ShareType = 1) OR
				(ShareType = 2 AND @SiteId IN (SELECT SiteId FROM GetChildrenSites(SiteId, 1)))
			)
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT S.*,
		[dbo].[SiteList_GetCount](S.Id, S.ListType, @SiteId) AS SiteCount,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName,
		T.RowNumber,
		T.TotalRecords
	FROM SISiteList S
		JOIN @tbPagedResults T ON T.Id = S.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = S.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = S.ModifiedBy
	ORDER BY RowNumber ASC
	
END
GO
PRINT 'Modify stored procedure SiteListDto_GetSite'
GO
IF(OBJECT_ID('SiteListDto_GetSite') IS NOT NULL)
	DROP PROCEDURE SiteListDto_GetSite
GO
CREATE PROCEDURE [dbo].[SiteListDto_GetSite]
(
	@SiteListId		uniqueidentifier = NULL,
	@Query			nvarchar(max) = NULL,
	@MaxRecords		int,
	@SiteId			uniqueidentifier
)
AS
BEGIN 
	DECLARE @ListType int, @TotalRecords int
	DECLARE @tbSite TABLE(Id uniqueidentifier primary key, RowNumber int)

	IF @Query IS NOT NULL
	BEGIN
		INSERT INTO @tbSite (Id, RowNumber)
		EXEC sp_executesql @Query
	END
	ELSE IF @SiteListId IS NOT NULL OR @SiteListId != dbo.GetEmptyGUID()
	BEGIN
		INSERT INTO @tbSite (Id)
		SELECT SiteId FROM SISiteListSite WHERE SiteListId = @SiteListId
	END

	SELECT TOP (@MaxRecords) S.Id,
		S.Title,
		S.ExternalCode,
		S.PrimarySiteUrl,
		S.Status,
		S.ParentSiteId,
		S.MasterSiteId
	FROM SISite S
		JOIN @tbSite T ON S.Id = T.Id
	WHERE S.Status = 1
	ORDER BY S.Title

	SELECT COUNT(1) AS TotalRecords 
	FROM SISite S JOIN @tbSite T ON S.Id = T.Id
	WHERE S.Status = 1
END
GO
PRINT 'Modify stored procedure DistributionRunDto_UpdateStatus'
GO
IF(OBJECT_ID('DistributionRunDto_UpdateStatus') IS NOT NULL)
	DROP PROCEDURE DistributionRunDto_UpdateStatus
GO
CREATE PROCEDURE [dbo].[DistributionRunDto_UpdateStatus]
(
	@Id				uniqueidentifier,
	@Status			int,
	@ModifiedBy		uniqueidentifier	
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	UPDATE D SET
		D.Status = @Status,
		D.ModifiedBy = @ModifiedBy,
		D.ModifiedDate = @UtcNow
	FROM GLDistributionRun D
	WHERE D.Id = @Id
			
END
GO
PRINT 'Modify stored procedure DistributionRunItemDto_UpdateStatus'
GO
IF(OBJECT_ID('DistributionRunItemDto_UpdateStatus') IS NOT NULL)
	DROP PROCEDURE DistributionRunItemDto_UpdateStatus
GO
CREATE PROCEDURE [dbo].[DistributionRunItemDto_UpdateStatus]
(
	@Id				uniqueidentifier,
	@Status			int,
	@ModifiedBy		uniqueidentifier	
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	UPDATE D SET
		D.Status = @Status,
		D.ModifiedBy = @ModifiedBy,
		D.ModifiedDate = @UtcNow
	FROM GLDistributionRunItem D
	WHERE D.Id = @Id
			
END
GO
PRINT 'Modify stored procedure TaxLogItem_Save'
GO
IF(OBJECT_ID('TaxLogItem_Save') IS NOT NULL)
	DROP PROCEDURE TaxLogItem_Save
GO
CREATE PROCEDURE [dbo].[TaxLogItem_Save]
(
	@Id					uniqueidentifier = null OUT ,
	@OrderId			uniqueidentifier,
	@OrderShippingId	uniqueidentifier = null,
	@Log				xml,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate		datetime = null
)
AS
BEGIN 
	SET @Id = NEWID()

	INSERT INTO ORTaxLog
	(
		Id,
		OrderId,
		OrderShippingId,
		[Log],
		CreatedDate,
		CreatedBy
	)
	VALUES
	(
		@Id,
		@OrderId,
		@OrderShippingId,
		@Log,
		GETUTCDATE(),
		@ModifiedBy
	)
END
GO
PRINT 'Modify stored procedure Group_SetUserGroups'
GO
IF(OBJECT_ID('Group_SetUserGroups') IS NOT NULL)
	DROP PROCEDURE Group_SetUserGroups
GO
CREATE PROCEDURE [dbo].[Group_SetUserGroups]    
	@UserId			UNIQUEIDENTIFIER,
	@SystemUser		BIT,
	@Mappings		XML,
	@ProductId		uniqueidentifier,
	@SiteIds		nvarchar(max) = NULL
AS    
BEGIN  
	-- Delete existing group mappings  
	DELETE
	FROM	USMemberGroup
	WHERE	MemberId = @UserId

	-- Delete existing site mappings
	DELETE
	FROM	USSiteUser
	WHERE	UserId = @UserId AND IsSystemUser = @SystemUser

	-- Load values from XML into temporary table
	DECLARE	@Groups TABLE (
		SiteId	UNIQUEIDENTIFIER,
		GroupId	UNIQUEIDENTIFIER
	)

	IF @Mappings IS NOT NULL
	BEGIN
		INSERT INTO @Groups (SiteId, GroupId)
		SELECT  DISTINCT
				t.c.value('@siteId','uniqueidentifier'),
				t.c.value('@groupId','uniqueidentifier')
		FROM	@Mappings.nodes('/Mappings/Mapping') AS t(c)
	END

	-- Remove EveryOne groups from temporary table
	DELETE	T
	FROM	@Groups AS T
			INNER JOIN USGroup AS G ON G.Id = T.GroupId
	WHERE	G.ApplicationId = G.ProductId AND
			G.Title LIKE '%everyone%'

	-- Add user to groups
	INSERT INTO USMemberGroup (MemberId, MemberType, ApplicationId, GroupId)
	SELECT  DISTINCT @UserId, 1, SiteId, GroupId
	FROM	@Groups

	IF @SystemUser = 1
	BEGIN
		-- Add EveryOne permissions
		DECLARE		@CMSEveryOneId	UNIQUEIDENTIFIER = (
			SELECT	TOP 1 Id
			FROM	USGroup
			WHERE	ProductId = 'CBB702AC-C8F1-4C35-B2DC-839C26E39848' AND
					ApplicationId = ProductId AND
					Title = 'EveryOne'
		)

		INSERT INTO USMemberGroup (MemberId, MemberType, ApplicationId, GroupId)
		SELECT	DISTINCT @UserId, 1, SiteId, @CMSEveryOneId
		FROM	@Groups
	END

	-- Add user to sites
	INSERT INTO USSiteUser (UserId, IsSystemUser, SiteId, ProductId)
	SELECT	DISTINCT @UserId, @SystemUser, MG.ApplicationId, G.ProductId
	FROM	USMemberGroup AS MG
			INNER JOIN USGroup AS G ON G.Id = MG.GroupId
	WHERE	MemberId = @UserId
	
	IF @SystemUser != 1 AND @SiteIds IS NOT NULL
	BEGIN
		INSERT INTO USSiteUser (UserId, IsSystemUser, SiteId, ProductId)
		SELECT DISTINCT @UserId, @SystemUser, Items, @ProductId FROM dbo.SplitGUID(@SiteIds, ',')
		WHERE Items NOT IN (SELECT SiteId FROM USSiteUser WHERE UserId = @UserId AND ProductId = @ProductId)
	END
END  
GO
PRINT 'Modify stored procedure OrderShipping_RemoveOrderItem'
GO
IF(OBJECT_ID('OrderShipping_RemoveOrderItem') IS NOT NULL)
	DROP PROCEDURE OrderShipping_RemoveOrderItem
GO
CREATE PROCEDURE [dbo].[OrderShipping_RemoveOrderItem]  
(   
	@OrderItemId	uniqueidentifier
)  
AS  
BEGIN       
	DELETE FROM OROrderItemCouponVersion WITH (ROWLOCK) WHERE OrderItemId = @OrderItemId
	DELETE FROM OROrderItemCouponCode WITH (ROWLOCK) WHERE  OrderItemId = @OrderItemId
	DELETE FROM OROrderItemAttributeValue WITH (ROWLOCK) where  OrderItemId = @OrderItemId 
	DELETE FROM dbo.FFOrderShipmentItem WITH (ROWLOCK) WHERE OrderItemId IN (SELECT Id FROM dbo.OROrderItem WHERE ParentOrderItemId = @OrderItemId)
	DELETE FROM FFOrderShipmentItem WITH (ROWLOCK) WHERE OrderItemId = @OrderItemId 
	DELETE FROM OROrderItemCouponCode WITH (ROWLOCK) Where OrderItemId = @OrderItemId
	DELETE FROM OROrderItem WITH (ROWLOCK) WHERE Id = @OrderItemId OR ParentOrderItemId = @OrderItemId
	DELETE FROM OROrderItemCouponVersion WITH (ROWLOCK) WHERE OrderItemId = @OrderItemId
END
GO
PRINT 'Modify stored procedure Order_RemoveOrderShipping'
GO
IF(OBJECT_ID('Order_RemoveOrderShipping') IS NOT NULL)
	DROP PROCEDURE Order_RemoveOrderShipping
GO
CREATE PROCEDURE [dbo].[Order_RemoveOrderShipping]  
(   
	@OrderShippingId uniqueidentifier
)  
AS  
BEGIN       
	DELETE FROM OROrderItemCouponVersion WHERE OrderItemId IN (SELECT Id FROM OROrderItem WHERE OrderShippingId = @OrderShippingId)
	DELETE FROM OROrderItemCouponCode WHERE OrderItemId IN (SELECT Id FROM OROrderItem WHERE OrderShippingId = @OrderShippingId)
	DELETE FROM OROrderItemAttributeValue WHERE OrderItemId IN (SELECT Id FROM OROrderItem WHERE OrderShippingId = @OrderShippingId)
	DELETE FROM FFOrderShipmentItem WHERE OrderShipmentId IN (SELECT Id FROM FFOrderShipment WHERE OrderShippingId = @OrderShippingId)
	DELETE FROM FFOrderShipmentPackage WHERE OrderShipmentId IN (SELECT Id FROM FFOrderShipment WHERE OrderShippingId = @OrderShippingId)
	DELETE FROM FFOrderShipment WHERE OrderShippingId = @OrderShippingId
	DELETE FROM OROrderItemCouponCode WHERE OrderItemId IN (SELECT Id FROM dbo.OROrderItem WHERE OrderShippingId = @OrderShippingId)
	DELETE FROM OROrderItem WHERE OrderShippingId = @OrderShippingId
	DELETE FROM OROrderShipping WHERE Id = @OrderShippingId
END
GO
PRINT 'Modify stored procedure Membership_GetAllUserPermissions'
GO
IF(OBJECT_ID('Membership_GetAllUserPermissions') IS NOT NULL)
	DROP PROCEDURE Membership_GetAllUserPermissions
GO
CREATE PROCEDURE [dbo].[Membership_GetAllUserPermissions]
(
	@ProductId				uniqueidentifier,
	@ApplicationId			uniqueidentifier = NULL,
	@UserId					uniqueidentifier = NULL,
	@ExcludeHiddenGroups	bit = NULL,
	@RequireProductId		bit = 1
)
AS
BEGIN
	IF @RequireProductId IS NULL SET @RequireProductId = 1
	IF @RequireProductId != 1 SET @ProductId = NULL
	IF @ExcludeHiddenGroups = 0 SET @ExcludeHiddenGroups = NULL

	SELECT DISTINCT SiteId, S.Id, S.Title, US.UserId, S.LftValue AS SiteOrder
	FROM dbo.USSiteUser US
		JOIN SISite S ON US.SiteId = S.Id AND S.Status != dbo.GetDeleteStatus()
	WHERE (@ProductId IS NULL OR ProductId = @ProductId)
		AND (@UserId IS NULL OR UserId = @UserId)
				
	SELECT DISTINCT	
		G.ProductId,			
		G.Id,
		G.Title,
		G.Description,
		G.GroupType,			
		MG.MemberId AS UserId,
		MG.ApplicationId AS SiteId,
		S.Title AS SiteName,
		S.LftValue AS SiteOrder
	FROM dbo.USGroup G 
		JOIN dbo.USMemberGroup MG ON G.Id = MG.GroupId
		JOIN SISite S ON S.Id = MG.ApplicationId AND S.Status != dbo.GetDeleteStatus()
		LEFT JOIN USMemberRoles MR ON MR.MemberId = MG.GroupId
	WHERE MG.MemberType = 1	
		AND (@ProductId IS NULL OR G.ProductId = @ProductId)
		AND (@ApplicationId IS NULL OR MG.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
		AND (@UserId IS NULL OR MG.MemberId = @UserId)
		AND (@ExcludeHiddenGroups IS NULL OR MR.RoleId NOT IN (15, 29))
END
GO
PRINT 'Modify stored procedure Order_GetDigitalLibrary'
GO
IF(OBJECT_ID('Order_GetDigitalLibrary') IS NOT NULL)
	DROP PROCEDURE Order_GetDigitalLibrary
GO
--exec Order_GetDigitalLibrary 'C3E4D193-86C5-4AAA-86D0-AEC68DA4F60C'
CREATE PROCEDURE [dbo].[Order_GetDigitalLibrary]
(
	@CustomerId UniqueIdentifier
)
AS
BEGIN


;WITH media_CTE
AS(
SELECT OI.Id AS OrderItemId, PM.Id AS ProductMediaId, Count(PRMD.DownloadCompleted) AS DownloadCompleted
FROM OROrderItem OI
INNER JOIN ORMediaDownloadHistory PRMD ON PRMD.OrderItemId = OI.Id
INNER JOIN OROrder O ON O.Id = OI.OrderId 
INNER JOIN PRProductMedia PM ON PM.Id = PRMD.ProductMediaId
WHERE O.CustomerId = @CustomerId
GROUP BY OI.Id, PM.Id, PRMD.DownloadCompleted
Having PRMD.DownloadCompleted = 1
)

SELECT Distinct PM.[Id]
	  ,OI.Id AS OrderItemId	
	  ,O.OrderDate AS DatePurchased
      ,PS.[ProductId]
      ,[SKUId]
      ,PM.[Title]
      ,PM.[Description]
      ,PM.[FileName]
      ,PM.[Status]
      ,PM.Size
      ,PM.[Sequence]
      ,[MaxNumDownloads]
      ,[MaxDaysAvailable]
      ,[ExpirationDate]
      ,PM.[SiteId]
      ,PM.[CreatedBy]
      ,ISNULL(MCT.DownloadCompleted,0) AS DownloadCompleted
  FROM OROrderItem OI
  INNER JOIN PRProductSKU PS ON OI.ProductSKUID = PS.Id
  INNER JOIN PRProductMedia PM ON PM.SKUID = PS.Id
  INNER JOIN OROrder O ON O.Id = OI.OrderId
  LEFT JOIN media_CTE MCT ON MCT.ProductMediaId = PM.Id AND MCT.OrderItemId = OI.Id
  WHERE O.CustomerId = @CustomerId
  AND PM.Status = 1 AND O.OrderStatusId = 2 -- shipped 
  Order by DatePurchased Desc		
END
GO
PRINT 'Modify stored procedure Order_GetOrderItemMedia'
GO
IF(OBJECT_ID('Order_GetOrderItemMedia') IS NOT NULL)
	DROP PROCEDURE Order_GetOrderItemMedia
GO
--exec Order_GetOrderItemMedia 'C5F9252B-FE9F-413C-A151-63B27C81661B'
CREATE PROCEDURE [dbo].[Order_GetOrderItemMedia]
(
	@orderItemId UniqueIdentifier,
	@ApplicationId uniqueidentifier=null
)
AS
BEGIN

;WITH media_CTE
AS(
SELECT PM.Id AS ProductMediaId, Count(PRMD.DownloadCompleted) AS DownloadCompleted
FROM OROrderItem OI
INNER JOIN ORMediaDownloadHistory PRMD ON PRMD.OrderItemId = OI.Id
INNER JOIN PRProductMedia PM ON PM.Id = PRMD.ProductMediaId
WHERE OI.Id = @orderItemId 
GROUP BY  PM.Id, PRMD.DownloadCompleted
Having PRMD.DownloadCompleted = 1
)

SELECT PM.[Id]
	  ,OI.Id AS OrderItemId	
	  ,O.OrderDate AS DatePurchased
      ,PS.[ProductId]
      ,[SKUId]
      ,PM.[Title]
      ,PM.[Description]
      ,PM.[FileName]
      ,PM.[Status]
      ,PM.Size
      ,PM.[Sequence]
      ,[MaxNumDownloads]
      ,[MaxDaysAvailable]
      ,[ExpirationDate]
      ,PM.[SiteId]
      ,PM.[CreatedBy]
      ,PM.CreatedDate AS CreatedDate   
      ,PM.[ModifiedBy]
      ,PM.ModifiedDate AS ModifiedDate 
      ,ISNULL(MCT.DownloadCompleted,0) AS DownloadCompleted
  FROM OROrderItem OI
  INNER JOIN PRProductSKU PS ON OI.ProductSKUID = PS.Id
  INNER JOIN PRProductMedia PM ON PM.SKUID = PS.Id
  INNER JOIN OROrder O ON O.Id = OI.OrderId
  LEFT JOIN media_CTE MCT ON MCT.ProductMediaId = PM.Id
  WHERE OI.Id = @orderItemId
END
GO
PRINT 'Modify stored procedure Product_DeleteProductMedia'
GO
IF(OBJECT_ID('Product_DeleteProductMedia') IS NOT NULL)
	DROP PROCEDURE Product_DeleteProductMedia
GO
CREATE PROCEDURE [dbo].[Product_DeleteProductMedia]
(
	@ProductMediaId		uniqueidentifier
)
AS
BEGIN
	DELETE FROM PRProductMedia
	WHERE Id = @ProductMediaId
END
GO
PRINT 'Modify stored procedure Product_GetProductMedia'
GO
IF(OBJECT_ID('Product_GetProductMedia') IS NOT NULL)
	DROP PROCEDURE Product_GetProductMedia
GO
CREATE PROCEDURE [dbo].[Product_GetProductMedia]
(
	@ProductMediaId UniqueIdentifier,
	@ApplicationId uniqueidentifier=null
)
AS

BEGIN
SELECT PM.[Id]
      ,[ProductId]
      ,[SKUId]
      ,PM.[Title]
      ,PM.[Description]
      ,PM.[FileName]
      ,PM.[Status]
      ,PM.Size
      ,[Sequence]
      ,[MaxNumDownloads]
      ,[MaxDaysAvailable]
      ,[ExpirationDate]
      ,[SiteId]
      ,PM.[CreatedBy]
      ,PM.CreatedDate AS CreatedDate   
      ,PM.[ModifiedBy]
      ,PM.ModifiedDate AS ModifiedDate  
  FROM [PRProductMedia] PM
		WHERE PM.Id = @ProductMediaId

END
GO
PRINT 'Modify stored procedure Product_GetProductMediaByProduct'
GO
IF(OBJECT_ID('Product_GetProductMediaByProduct') IS NOT NULL)
	DROP PROCEDURE Product_GetProductMediaByProduct
GO
CREATE PROCEDURE [dbo].[Product_GetProductMediaByProduct]
(
	@ProductId UniqueIdentifier,
	@ApplicationId uniqueidentifier=null
)
AS
BEGIN
SELECT PM.[Id]
      ,[ProductId]
      ,[SKUId]
      ,PM.[Title]
      ,PM.[Description]
      ,PM.[FileName]
      ,PM.[Status]
      ,PM.Size
      ,[Sequence]
      ,[MaxNumDownloads]
      ,[MaxDaysAvailable]
      ,[ExpirationDate]
      ,[SiteId]
      ,PM.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(PM.CreatedDate,@ApplicationId)CreatedDate   
      ,PM.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(PM.ModifiedDate,@ApplicationId)ModifiedDate  
  FROM [PRProductMedia] PM
		WHERE PM.ProductId = @ProductId

END
GO
PRINT 'Modify stored procedure Product_GetProductMediaBySKU'
GO
IF(OBJECT_ID('Product_GetProductMediaBySKU') IS NOT NULL)
	DROP PROCEDURE Product_GetProductMediaBySKU
GO
--exec Product_GetProductMediaBySKU '8B3BA9EB-C204-47FB-89AC-0008CC23BE9E'
CREATE PROCEDURE [dbo].[Product_GetProductMediaBySKU]
(
	@SkuId UniqueIdentifier,
	@ApplicationId uniqueidentifier=null
)
AS
BEGIN
SELECT PM.[Id]
      ,[ProductId]
      ,[SKUId]
      ,PM.[Title]
      ,PM.[Description]
      ,PM.[FileName]
      ,PM.[Status]
      ,StatusDescription = CASE PM.[Status] WHEN 1 THEN 'Active' ELSE 'InActive' END 
      ,PM.Size
      ,[Sequence]
      ,[MaxNumDownloads]
      ,[MaxDaysAvailable]
      ,[ExpirationDate]
      ,[SiteId]
      ,PM.[CreatedBy]
      , PM.CreatedDate AS CreatedDate   
      ,PM.[ModifiedBy]
      ,PM.ModifiedDate AS ModifiedDate  
  FROM [PRProductMedia] PM
		WHERE PM.SKUId = @SkuId

END
GO
PRINT 'Modify stored procedure Product_GetProductMediaByXmlGuids'
GO
IF(OBJECT_ID('Product_GetProductMediaByXmlGuids') IS NOT NULL)
	DROP PROCEDURE Product_GetProductMediaByXmlGuids
GO
CREATE PROCEDURE [dbo].[Product_GetProductMediaByXmlGuids]
(
	@Id Xml,
	@ApplicationId uniqueidentifier=null)
AS
BEGIN 

DECLARE @tempXml TABLE (Sno int Identity(1,1),ProductMediaId uniqueidentifier)    
    
INSERT INTO @tempXml(ProductMediaId)    
SELECT tab.col.value('text()[1]','uniqueidentifier')    
FROM @Id.nodes('/ProductMediaCollection/ProductMedia')tab(col)    


SELECT PM.[Id]
      ,[ProductId]
      ,[SKUId]
      ,PM.[Title]
      ,PM.[Description]
      ,PM.[FileName]
      ,PM.[Status]
      ,PM.Size
      ,[Sequence]
      ,[MaxNumDownloads]
      ,[MaxDaysAvailable]
      ,[ExpirationDate]
      ,[SiteId]
      ,PM.[CreatedBy]
      ,PM.CreatedDate AS CreatedDate   
      ,PM.[ModifiedBy]
      ,PM.ModifiedDate AS ModifiedDate 
  FROM @tempXml T 
  INNER JOIN [PRProductMedia] PM ON PM.Id = T.ProductMediaID 
END
GO
PRINT 'Modify stored procedure Product_MakeProductMediaInactive'
GO
IF(OBJECT_ID('Product_MakeProductMediaInactive') IS NOT NULL)
	DROP PROCEDURE Product_MakeProductMediaInactive
GO
CREATE PROCEDURE [dbo].[Product_MakeProductMediaInactive]  
(  
	@ProductMediaId uniqueidentifier OUTPUT,  
	@ModifiedBy uniqueidentifier   
)  
AS
BEGIN
	DECLARE @CurrentDate datetime  
	SET @CurrentDate = GetUTCDate()   

	UPDATE PM Set 
		PM.Status = 2, --Inactive
		PM.ModifiedBy = @ModifiedBy,
		PM.ModifiedDate = @CurrentDate 
	FROM PRProductMedia PM 
	WHERE PM.Id = @ProductMediaId

END
GO
PRINT 'Modify stored procedure Product_UpdateMediaStatus'
GO
IF(OBJECT_ID('Product_UpdateMediaStatus') IS NOT NULL)
	DROP PROCEDURE Product_UpdateMediaStatus
GO
CREATE PROCEDURE [dbo].[Product_UpdateMediaStatus]  
(  
	@ProductMediaId uniqueidentifier OUTPUT,  
	@Status int,
	@ModifiedBy uniqueidentifier   
)  
AS
BEGIN
	DECLARE @CurrentDate datetime  
	SET @CurrentDate = GetUTCDate()   

	UPDATE PM Set 
		PM.Status = @Status, --Inactive
		PM.ModifiedBy = @ModifiedBy,
		PM.ModifiedDate = @CurrentDate 
	FROM PRProductMedia PM 
	WHERE PM.Id = @ProductMediaId

END
GO
PRINT 'Modify stored procedure CommerceReport_ProductMediaDownloads'
GO
IF(OBJECT_ID('CommerceReport_ProductMediaDownloads') IS NOT NULL)
	DROP PROCEDURE CommerceReport_ProductMediaDownloads
GO
CREATE PROCEDURE [dbo].[CommerceReport_ProductMediaDownloads]  
(   
 @startDate datetime,
 @endDate datetime, 
 @maxReturnCount int,  
 @pageSize int = 10,@pageNumber int,   
 @sortBy nvarchar(50) = null,  
 @virtualCount int output, 
 @orderNumber nvarchar(200) = null,
 @customerEmail nvarchar(200) = null,
 @ApplicationId uniqueidentifier=null  ,
 @IncludeChildrenSites Bit = 0
)   
AS  
  
BEGIN  
	if @maxReturnCount = 0  
		set @maxReturnCount = null  
	  
	declare @tmpMedia table(  
		OrderNumber nvarchar(500),
		FileTitle nvarchar(500),
		UserName nvarchar(500), 
		Email nvarchar(500),
		StartDateTime DateTime,  
		EndDateTime DateTime,  
		BytesTransferred numeric(18,0),  
		IsDownloaded bit  
	)  
	
	INSERT INTO @tmpMedia  
	SELECT  
			o.PurchaseOrderNumber,
			PM.Title,
			USU.LastName + ' ' + USU.FirstName,
			UMember.Email,
			MDH.StartDateTime,
			MDH.EndDateTime,
			MDH.BytesTransferred,
			MDH.DownloadCompleted
	FROM  
		ORMediaDownloadHistory MDH 
			INNER JOIN PRProductMedia PM ON MDH.ProductMediaId = PM.Id
			INNER JOIN dbo.USUser AS USU ON MDH.UserId = USU.Id
			INNER JOIN dbo.USCommerceUserProfile AS CUP ON CUP.Id = USU.Id
			INNER JOIN dbo.USMembership AS UMember ON UMember.UserId = USU.Id
			INNER JOIN dbo.OROrderItem OI ON OI.Id = MDH.OrderItemId
			INNER JOIN dbo.OROrder O ON O.Id = OI.OrderId
	WHERE
		((MDH.StartDateTime >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId) AND
		MDH.StartDateTime < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)) OR
		(MDH.EndDateTime >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId) AND
		MDH.EndDateTime < dbo.ConvertTimeToUtc(@endDate,@ApplicationId))) AND
		(@customerEmail is null or UMember.Email like '%' + @customerEmail + '%') AND
		(@orderNumber is null or o.PurchaseOrderNumber like '%' + @orderNumber + '%') 
		AND (
			(@IncludeChildrenSites = 0 AND O.SiteId = @ApplicationId )
			OR
			(@IncludeChildrenSites = 1 AND O.SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites(@ApplicationId,1)))
		)
  
   Set @virtualCount = @@ROWCOUNT  
	
  if @sortBy is null or (@sortBy) = 'filetitle desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by FileTitle desc) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.FileTitle DESC  
   end  
  else if (@sortBy) = 'filetitle asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by FileTitle ASC) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.FileTitle ASC  
   end  
  else if (@sortBy) = 'startdatetime asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by StartDateTime asc) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.StartDateTime ASC  
   end  
  else if (@sortBy) = 'startdatetime desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by StartDateTime DESC) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.StartDateTime DESC  
   end  
  else if (@sortBy) = 'enddatetime asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by EndDateTime asc) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.EndDateTime ASC  
   end  
  else if (@sortBy) = 'enddatetime desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by EndDateTime DESC) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.EndDateTime DESC  
   end  
  else if (@sortBy) = 'bytestransferred asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by BytesTransferred asc) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.BytesTransferred ASC  
   end  
  else if (@sortBy) = 'bytestransferred desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by BytesTransferred DESC) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.BytesTransferred DESC  
   end  
  else if (@sortBy) = 'isdownloaded asc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by IsDownloaded asc) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.IsDownloaded ASC  
   end  
  else if (@sortBy) = 'isdownloaded desc'  
   Begin  
	Select A.*   
	FROM  
	 (  
	 Select *,  ROW_NUMBER() OVER(Order by IsDownloaded DESC) as RowNumber  
	  from @tmpMedia  
	 ) A  
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	  A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
	Order by A.IsDownloaded DESC  
   end  
  
END
GO
PRINT 'Modify stored procedure Warehouse_Delete'
GO
IF(OBJECT_ID('Warehouse_Delete') IS NOT NULL)
	DROP PROCEDURE Warehouse_Delete
GO
CREATE PROCEDURE [dbo].[Warehouse_Delete]
(
	@Id uniqueidentifier
)
AS
BEGIN
	DELETE FROM INWarehouseSite Where WarehouseId = @Id
	DELETE FROM INInventory WHERE WarehouseId = @Id
	DELETE FROM INWarehouseDeliveryZone WHERE WarehouseId = @Id
	DELETE FROM INRestocking WHERE WarehouseId = @Id

	DELETE A FROM GLAddress A INNER JOIN INWarehouse W ON A.Id = W.AddressId WHERE W.Id = @Id
	
	DELETE INWarehouse Where Id = @Id
	
END
GO
PRINT 'Modify stored procedure ContactDto_Get'
GO
IF(OBJECT_ID('ContactDto_Get') IS NOT NULL)
	DROP PROCEDURE ContactDto_Get
GO
CREATE PROCEDURE [dbo].[ContactDto_Get]
(
	@Id				uniqueidentifier = NULL,
	@Ids			nvarchar(max)=NULL,
	@Status			int = NULL ,
	@SiteId			uniqueidentifier = NULL,
	@ContactListId	uniqueidentifier = NULL, 
	@FormId			uniqueidentifier = NULL,
	@ContactType	int = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@MaxRecords		int = NULL,
	@SortBy			nvarchar(100) = NULL,  
	@SortOrder		nvarchar(10) = NULL,
	@Query			nvarchar(max) = NULL,
	@Email			nvarchar(max) = NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @LocalId		uniqueidentifier,
		@LocalIds			nvarchar(max),
		@LocalStatus		int,
		@LocalSiteId		uniqueidentifier,
		@LocalContactListId	uniqueidentifier, 
		@LocalFormId		uniqueidentifier,
		@LocalContactType	int,
		@LocalPageNumber	int,
		@LocalPageSize		int,
		@LocalMaxRecords	int,
		@LocalSortBy		nvarchar(100),  
		@LocalSortOrder		nvarchar(10),
		@LocalEmail			nvarchar(max),
		@LocalQuery			nvarchar(max)

	SELECT @LocalId = @Id,
		@LocalIds = @Ids,
		@LocalStatus = @Status,
		@LocalSiteId = @SiteId,
		@LocalContactListId	= @ContactListId, 
		@LocalFormId = @FormId,
		@LocalContactType = @ContactType,
		@LocalPageNumber = @PageNumber,
		@LocalPageSize = @PageSize,
		@LocalMaxRecords = @MaxRecords,
		@LocalSortBy = @SortBy,  
		@LocalSortOrder = @SortOrder,
		@LocalEmail = @Email,
		@LocalQuery = @Query

	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50), 
		@EmptyGuid uniqueidentifier, @TotalRecords int 
		
	SET @EmptyGuid = dbo.GetEmptyGUID()
	SET @PageLowerBound = @LocalPageSize * @LocalPageNumber
	IF (@PageLowerBound IS NOT NULL AND @LocalPageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @LocalPageSize + 1
	
	SET @LocalEmail = LTRIM(RTRIM(@LocalEmail))
	IF (@LocalEmail = '') SET @LocalEmail = NULL
	IF @LocalContactListId = @EmptyGuid SET @LocalContactListId = NULL
	IF @LocalFormId = @EmptyGuid SET @LocalFormId = NULL

	IF (@LocalSortOrder IS NULL) SET @LocalSortOrder = 'ASC'
	IF (@LocalSortBy IS NOT NULL)
		SET @SortClause = UPPER(@LocalSortBy + ' ' + @LocalSortOrder)

	DECLARE @tbContacts TABLE (Id int, RowNumber int, TotalRecords int)
	DECLARE @tbContactIds TABLE (Id uniqueidentifier, AddressId uniqueidentifier)
	IF @LocalId IS NOT NULL
	BEGIN
		INSERT INTO @tbContactIds (Id, AddressId)
		SELECT Id, AddressId FROM vw_contacts WHERE Id = @LocalId OPTION (RECOMPILE)
	END
	ELSE IF @LocalEmail IS NOT NULL
	BEGIN
		INSERT INTO @tbContactIds (Id, AddressId)
		SELECT Id, AddressId FROM vw_contacts WHERE Email like @LocalEmail OPTION (RECOMPILE)
	END
	ELSE IF @LocalIds IS NOT NULL
	BEGIN
		INSERT INTO @tbContactIds (Id, AddressId)
		SELECT Id, AddressId FROM vw_contacts C 
		WHERE EXISTS (SELECT 1 FROM dbo.SplitGUID(@LocalIds, ',') T WHERE C.Id = T.Items)
		OPTION (RECOMPILE)
	END
	ELSE IF @LocalContactListId IS NULL OR @LocalFormId IS NOT NULL
	BEGIN
		CREATE TABLE #TBQuery (UniqueId int, RowNumber int)
		IF @LocalQuery IS NOT NULL
		BEGIN
			INSERT INTO #TBQuery
			EXEC sp_executesql @LocalQuery 

			CREATE CLUSTERED INDEX IX_TBQuery_UniqueId ON #TBQuery (UniqueId)
		END

		CREATE TABLE #TBFormUsers (UniqueId int primary key)
		IF @LocalFormId IS NOT NULL
		BEGIN
			;WITH CTE AS 
			(
				SELECT C.UniqueId AS Id, 
					ROW_NUMBER() OVER (PARTITION BY C.Id ORDER BY C.UniqueId) AS SiteRank
				FROM dbo.DNContactSite C
				WHERE EXISTS (SELECT 1 FROM FormsResponse R WHERE R.UserId = C.Id AND R.FormsId = @LocalFormId)					
			), DISTINCTCTE AS
			(
				SELECT Id FROM CTE WHERE SiteRank = 1
			)

			INSERT INTO #TBFormUsers
			SELECT Id FROM DISTINCTCTE

			SET @LocalSiteId = NULL
		END

		;WITH CTE AS 
		(
			SELECT C.UniqueId AS Id, ROW_NUMBER() OVER (ORDER BY
				CASE WHEN @SortClause = 'LastName ASC' THEN LastName END ASC,
				CASE WHEN @SortClause = 'LastName DESC' THEN LastName END DESC,
				CASE WHEN @SortClause = 'Email ASC' THEN Email END ASC,
				CASE WHEN @SortClause = 'Email DESC' THEN Email END DESC,
				CASE WHEN @SortClause = 'CreatedDate ASC' THEN CreatedDay END ASC,
				CASE WHEN @SortClause = 'CreatedDate DESC' THEN CreatedDay END DESC,
				CASE WHEN @SortClause = 'ModifiedDate ASC' THEN UniqueId END ASC,
				CASE WHEN @SortClause = 'ModifiedDate DESC' THEN UniqueId END DESC,
				CASE WHEN @SortClause IS NULL THEN UniqueId END DESC
			) AS RowNumber
			FROM dbo.DNContactSite C
			WHERE (@LocalSiteId IS NULL OR C.SiteId = @LocalSiteId)
				AND (@LocalStatus IS NULL OR C.Status like @LocalStatus)
				AND (@LocalContactType IS NULL OR C.ContactType = @LocalContactType)	
				AND (@LocalQuery IS NULL OR EXISTS (SELECT 1 FROM #TBQuery Q WHERE Q.UniqueId = C.UniqueId))
				AND (@LocalFormId IS NULL OR EXISTS (SELECT 1 FROM #TBFormUsers F WHERE F.UniqueId = C.UniqueId))
		), CNT AS (SELECT COUNT(1) AS TotalRecords FROM CTE)

		INSERT INTO @tbContacts
		SELECT Id, RowNumber, CNT.TotalRecords FROM CTE 
			CROSS JOIN CNT
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
			AND (@LocalMaxRecords IS NULL OR @LocalMaxRecords = 0 OR RowNumber <= @LocalMaxRecords)
		OPTION (RECOMPILE)
	END
	ELSE
	BEGIN
		--Get all the contacts for the given contact list
		DECLARE @ListType int
		SELECT @ListType = ListType from TADistributionLists WHERE Id = @LocalContactListId

		IF (@ListType = 0) --For manual
		BEGIN
			;WITH CTE AS
			(
				SELECT C.UniqueId As Id, ROW_NUMBER() OVER (ORDER BY UniqueId) AS RowNumber
				FROM TADistributionListUser DLU 
					JOIN DNContactSite C ON C.Id = DLU.UserId
					JOIN TADistributionLists DL ON DLU.DistributionListId = DL.Id
				WHERE DL.Id = @LocalContactListId 
					AND DL.ListType = @ListType 
					AND ContactListSubscriptionType != 3
			), CNT AS (SELECT COUNT(1) AS TotalRecords FROM CTE)

			INSERT INTO @tbContacts
			SELECT Id, RowNumber, CNT.TotalRecords FROM CTE 
				CROSS JOIN CNT
			WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
				OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
				AND (@LocalMaxRecords IS NULL OR @LocalMaxRecords = 0 OR RowNumber <= @LocalMaxRecords)
		END
		ELSE IF (@ListType = 1) --For auto list
		BEGIN
			DECLARE @queryXml xml
			SELECT @queryXml = SearchXml FROM dbo.TADistributionListSearch a, CTSearchQuery b 
			WHERE b.Id = a.SearchQueryId AND a.DistributionListId=@LocalContactListId 
				
			DECLARE @tbIds TABLE (Id uniqueidentifier primary key)	
			INSERT INTO @tbIds
			EXEC Contact_SearchContact 
				@Xml = @queryXml,
				@ApplicationId = @LocalSiteId, 
				@MaxRecords = 0, 
				@ContactListId = @LocalContactListId,
				@TotalRecords = @TotalRecords output, 
				@IncludeAddress = 0

			;WITH CTE AS
			(
				SELECT C.UniqueId As Id, ROW_NUMBER() OVER (ORDER BY UniqueId) AS RowNumber
				FROM DNContactSite C 
					JOIN @tbIds T ON T.Id = C.Id
			)

			INSERT INTO @tbContacts
			SELECT Id, RowNumber, @TotalRecords FROM CTE 
			WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
				OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
				AND (@LocalMaxRecords IS NULL OR @LocalMaxRecords = 0 OR RowNumber <= @LocalMaxRecords)
		END
	END

	IF EXISTS (SELECT 1 FROM @tbContactIds)
	BEGIN
		SELECT @TotalRecords = COUNT(1) FROM @tbContactIds
		IF @TotalRecords = 1
		BEGIN
			DECLARE @PrimarySiteId uniqueidentifier, @ContactId uniqueidentifier, @AddressId uniqueidentifier
			SELECT TOP 1 @ContactId = Id, @AddressId = AddressId FROM @tbContactIds
			SELECT TOP 1 @PrimarySiteId = SiteId FROM vw_contactSite WHERE UserId = @ContactId AND IsPrimary = 1

			SELECT 1 AS RowNumber,
				C.Id,
				C.Email,
				C.FirstName,
				C.MiddleName,
				C.LastName,
				C.CompanyName,
				C.Gender,
				C.BirthDate,
				C.HomePhone,
				C.MobilePhone,
				C.OtherPhone,
				C.ImageId,
				C.AddressId,
				C.Notes,
				C.LeadScore,
				C.ContactSourceId,
				C.CreatedBy,
				dbo.ConvertTimeFromUtc(C.CreatedDate, @SiteId) CreatedDate,
				C.ModifiedBy,
				dbo.ConvertTimeFromUtc(C.ModifiedDate, @SiteId) ModifiedDate,
				@TotalRecords AS TotalRecords,
				@SiteId AS SiteId,
				@PrimarySiteId AS PrimarySiteId,
				Status,
				LastInteractionDate
			FROM vw_Contacts C 
			WHERE Id = @ContactId

			SELECT A.Id,
				A.FirstName,
				A.LastName,
				A.AddressType,
				A.AddressLine1,
				A.AddressLine2,
				A.AddressLine3,
				A.City,
				A.StateId,
				A.StateCode,
				A.StateName,
				A.State,
				A.CountryId,
				A.CountryCode,
				A.CountryName,
				A.County,
				A.Zip,
				A.Phone,
				A.Status
			FROM vw_Address A
			WHERE Id = @AddressId

			SELECT V.*, 
				A.Title AS AttributeTitle
			FROM ATContactAttributeValue V
				JOIN ATAttribute A ON A.Id = V.AttributeId
			WHERE V.ContactId = @ContactId
		END
		ELSE
		BEGIN
			;WITH CTE AS (
				SELECT ROW_NUMBER() OVER (PARTITION BY C.Id ORDER BY ContactType) AS RowNumber,
					C.Id,
					C.Email,
					C.FirstName,
					C.MiddleName,
					C.LastName,
					C.CompanyName,
					C.Gender,
					C.BirthDate,
					C.HomePhone,
					C.MobilePhone,
					C.OtherPhone,
					C.ImageId,
					C.AddressId,
					C.Notes,
					C.LeadScore,
					C.ContactSourceId,
					C.CreatedBy,
					dbo.ConvertTimeFromUtc(C.CreatedDate, @SiteId) CreatedDate,
					C.ModifiedBy,
					dbo.ConvertTimeFromUtc(C.ModifiedDate, @SiteId) ModifiedDate,
					@TotalRecords AS TotalRecords,
					@SiteId AS SiteId,
					ISNULL(CS.SiteId, @SiteId) AS PrimarySiteId,
					ISNULL(CS.Status, 1) AS Status,
					LastInteractionDate
				FROM vw_Contacts C 
					LEFT JOIN vw_contactSite CS ON C.Id = CS.UserId AND IsPrimary = 1
				WHERE EXISTS (SELECT 1 FROM @tbContactIds T WHERE T.Id = C.Id)
			)

			SELECT * FROM CTE WHERE RowNumber = 1

			SELECT A.Id,
				A.FirstName,
				A.LastName,
				A.AddressType,
				A.AddressLine1,
				A.AddressLine2,
				A.AddressLine3,
				A.City,
				A.StateId,
				A.StateCode,
				A.StateName,
				A.State,
				A.CountryId,
				A.CountryCode,
				A.CountryName,
				A.County,
				A.Zip,
				A.Phone,
				A.Status
			FROM vw_Address A
			WHERE EXISTS (SELECT 1 FROM @tbContactIds T WHERE T.AddressId = A.Id)
		END
	END
	ELSE IF EXISTS (SELECT 1 FROM @tbContacts)
	BEGIN
		SELECT TOP 1 @TotalRecords = TotalRecords FROM @tbContacts
		IF @TotalRecords IS NULL SELECT @TotalRecords = COUNT(1) FROM @tbContacts

		SELECT C.Id,
			C.Email,
			C.FirstName,
			C.MiddleName,
			C.LastName,
			C.CompanyName,
			C.Gender,
			C.BirthDate,
			C.HomePhone,
			C.MobilePhone,
			C.OtherPhone,
			C.ImageId,
			C.AddressId,
			C.Notes,
			C.LeadScore,
			C.ContactSourceId,
			C.Status,
			C.CreatedBy,
			dbo.ConvertTimeFromUtc(C.CreatedDate, C.SiteId) CreatedDate,
			C.ModifiedBy,
			dbo.ConvertTimeFromUtc(C.ModifiedDate, C.SiteId) ModifiedDate,
			C.CreatedByFullName,
			C.ModifiedByFullName,
			@TotalRecords AS TotalRecords,
			C.SiteId,
			C.PrimarySiteId,
			C.LastInteractionDate
		FROM DNContactSite C 
			JOIN @tbContacts T ON T.Id = C.UniqueId
		ORDER BY RowNumber ASC

		SELECT A.Id,
			A.FirstName,
			A.LastName,
			A.AddressType,
			A.AddressLine1,
			A.AddressLine2,
			A.AddressLine3,
			A.City,
			A.StateId,
			A.StateCode,
			A.StateName,
			A.State,
			A.CountryId,
			A.CountryCode,
			A.CountryName,
			A.County,
			A.Zip,
			A.Phone,
			A.Status
		FROM vw_Address A
			JOIN DNContactSite C ON C.AddressId = A.Id
			JOIN @tbContacts T ON T.Id = C.UniqueId 
	END
END
GO
PRINT 'Modify stored procedure Bundle_GetBundleItems'
GO
IF(OBJECT_ID('Bundle_GetBundleItems') IS NOT NULL)
	DROP PROCEDURE Bundle_GetBundleItems
GO
CREATE PROCEDURE [dbo].[Bundle_GetBundleItems]
(  
	@Id				uniqueidentifier = null,
	@ApplicationId	uniqueidentifier = null
)  
AS  
BEGIN  
	SELECT S.[Id]  
		,S.[ProductId]  
		,S.[SiteId]  
		,S.[SKU]  
		,S.[Id] As SkuId
		,S.[Title]  
		,S.[Description]  
		,S.[Sequence]  
		,S.[IsActive]  
		,S.[IsOnline]  
		,S.[ListPrice]  
		,S.[UnitCost]  
		,S.[PreviousSoldCount]  
		,S.[CreatedBy]  
		,S.[CreatedDate]
		,S.[ModifiedBy]  
		,S.ModifiedDate
		,S.[Status]  
		,S.[Measure]  
		,S.[OrderMinimum]  
		,S.[OrderIncrement] 
		,S.[Length]
		,S.[Height]
		,S.[Width]
		,S.[Weight]
		,S.[SelfShippable]  
		,S.AvailableForBackOrder
		,S.MaximumDiscountPercentage
		,S.BackOrderLimit
		,S.IsUnlimitedQuantity
		,S.FreeShipping
		,S.UserDefinedPrice
		,BI.Id AS [BundleItemId]  
		,BI.[BundleProductId]  
		,BI.[ChildSkuId]  
		,BI.[ChildSkuQuantity]  
		,BI.[CreatedBy] as [BundleSkuCreatedBy]  
		,BI.[CreatedDate] as [BundleSkuCreatedDate]  
		,BI.[ModifiedBy] as [BundleSkuModifiedBy]  
		,BI.[ModifiedDate] as [BundleSkuModifiedDate]
		,(SELECT SUM(Quantity) FROM [dbo].[vwAllocatedQuantityPerWarehouse] A 
			INNER JOIN INWarehouseSite SI ON A.WarehouseId = SI.WarehouseId
			WHERE ProductSKUId = S.[Id] AND SI.SiteId = @ApplicationId) AS AllocatedQuantity
		,(SELECT CASE S.IsUnlimitedQuantity WHEN 1 THEN 9999999 ELSE SUM(Quantity) END		
			FROM INInventory I INNER JOIN INWarehouseSite SI ON I.WarehouseId = SI.WarehouseId 
			Where SKUId = S.[Id]  AND SI.SiteId = @ApplicationId) AS QuantityOnHand    --- sum of all warehouse -- find bundle item wise minimum in API or helper
	FROM PRBundleItem BI
		LEFT JOIN PRProductSKU S ON S.Id = BI.ChildSkuId
	WHERE BI.[BundleProductId]=@Id  
	  AND Status = dbo.GetActiveStatus() -- Select only Active Attributes  
	Order By BI.[Id]  
END

GO
PRINT 'Modify stored procedure CreditCardPaymentDto_Get'
GO
IF(OBJECT_ID('CreditCardPaymentDto_Get') IS NOT NULL)
	DROP PROCEDURE CreditCardPaymentDto_Get
GO
CREATE PROCEDURE [dbo].[CreditCardPaymentDto_Get]
(		
	@PaymentInfoId		UNIQUEIDENTIFIER
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	IF NOT EXISTS (SELECT 1 FROM PTPaymentInfo WHERE Id = @PaymentInfoId)
		SELECT @PaymentInfoId = PaymentInfoId FROM USUserPaymentInfo WHERE Id = @PaymentInfoId

	SELECT P.Id,
		CCT.Title AS CardType,
		CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth,
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,
		P.NameOnCard,
		P.CreatedBy,
		P.CreatedDate,
		P.Status,
		P.ModifiedBy,
		P.ModifiedDate,
		PCC.ExternalProfileId
	FROM PTPaymentInfo P
		LEFT JOIN PTCreditCardType AS CCT ON CCT.Id = P.CardType
		LEFT JOIN PTPaymentCreditCard PCC ON P.Id = PCC.PaymentInfoId
	WHERE P.Id = @PaymentInfoId

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO
PRINT 'Modify stored procedure ProductType_Delete'
GO
IF(OBJECT_ID('ProductType_Delete') IS NOT NULL)
	DROP PROCEDURE ProductType_Delete
GO
CREATE PROCEDURE [dbo].[ProductType_Delete](
--********************************************************************************
-- Parameters
--********************************************************************************
		@Id					uniqueidentifier,
		@ModifiedBy			uniqueidentifier,
		@Status				int OUTPUT
)
AS
--********************************************************************************
-- Procedure Body
--********************************************************************************
DECLARE
	@DeleteStatus			int, @lft int, @rgt int,
	@Stmt					nvarchar(max)
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
IF EXISTS (SELECT Id FROM PRProductType WHERE Id=@Id)
	BEGIN
	
		Set @Stmt ='Delete'
		IF ( (SELECT Count(Sequence) FROM PRProductTypeAttribute Where ProductTypeId = @Id ) > 0)
		BEGIN
			 RAISERROR('Attributes are allocated to this product type||%s',16,1,@Stmt)
			 RETURN dbo.GetDataBaseErrorCode()
		END

		  IF ( (SELECT Count(Title) FROM PRProduct Where ProductTypeID = @Id ) > 0)  
		  BEGIN  
			RAISERROR('Product type is being used in Product||%s',16,1,@Stmt)  
			RETURN dbo.GetDataBaseErrorCode()  
		  END  


		SELECT @lft=LftValue,@rgt=RgtValue from PRProductType  where Id=@Id 
	
		DELETE FROM PRProductTypeAttribute Where ProductTypeId = @Id 
		
		IF @@ERROR <> 0
		  BEGIN
			 RAISERROR('DBERROR||%s',16,1,@Stmt)
			 RETURN dbo.GetDataBaseErrorCode()
		  END

            
		DELETE  PRProductType  where LftValue between @lft and @rgt  
		IF @@ERROR <> 0
		  BEGIN
			 RAISERROR('DBERROR||%s',16,1,@Stmt)
			 RETURN dbo.GetDataBaseErrorCode()
		  END

		Set @Stmt ='Adjust Tree'
		UPDATE PRProductType  
			SET LftValue = CASE WHEN LftValue > @lft THEN LftValue - (@rgt - @lft + 1) ELSE LftValue END, 
			RgtValue = CASE WHEN RgtValue > @lft THEN RgtValue - (@rgt - @lft + 1) ELSE RgtValue END	
		IF @@ERROR <> 0
		  BEGIN
			  RAISERROR('DBERROR||%s',16,1,@Stmt)
			  RETURN dbo.GetDataBaseErrorCode()
		  END
      END     
END

GO

PRINT 'Modify stored procedure ProductType_GetProductType'
GO
IF(OBJECT_ID('ProductType_GetProductType') IS NOT NULL)
	DROP PROCEDURE ProductType_GetProductType
GO
CREATE PROCEDURE [dbo].[ProductType_GetProductType](
--********************************************************************************
-- Parameters
--********************************************************************************
			@Id				uniqueidentifier = null,
			@ApplicationId	uniqueidentifier = null,				
			@PageIndex		int = null,
			@PageSize		int = null,
			@Level			int =null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
	IF (@PageIndex is not null AND @PageSize is not null)
	BEGIN
		SET @PageLowerBound = Isnull(@PageSize,0) * Isnull(@PageIndex,0)
		SET @PageUpperBound = @PageLowerBound - Isnull(@PageSize,0) + 1;
	
		WITH ResultEntries AS
		(
            SELECT 
					ROW_NUMBER() OVER (ORDER BY A.Title) AS RowNumber,
					A.Id, 
					A.Title,  
					A.Description, A.FriendlyName, A.IsDownloadableMedia,A.TaxCategoryId,
					A.CreatedDate AS CreatedDate,
					A.CreatedBy,
					A.ModifiedDate AS ModifiedDate,
					A.ModifiedBy,
					A.Status,
					A.LftValue,
					A.RgtValue	,
					A.ParentId,
					A.CMSPageId
            FROM	dbo.PRProductType A
            WHERE	
					--A.[Id]			=	Isnull(@Id,[Id])			AND 
					--A.SiteId		=	Isnull(@ApplicationId, SiteId)		AND
					(@Id is null or A.[Id] = @Id) AND
					--(@ApplicationId is null or A.SiteId = @ApplicationId) AND
					A.Status		=	dbo.GetActiveStatus()	--- This will return only active .. 
		)

		SELECT * 
		FROM ResultEntries 
		WHERE ((@PageIndex is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		
	END
	ELSE
	BEGIN
			SELECT 
					A.Id, 
					A.Title,  
					A.Description, A.FriendlyName, A.IsDownloadableMedia,A.TaxCategoryId,
					 A.CreatedDate AS CreatedDate,
					A.CreatedBy,
					A.ModifiedDate AS ModifiedDate,
					A.ModifiedBy,
					A.Status,
					A.LftValue,
					A.RgtValue,
					A.ParentId,
					A.CMSPageId	
            FROM	dbo.PRProductType A
            WHERE	
					(@Id is null or A.[Id] = @Id) AND
					A.Status		=	dbo.GetActiveStatus()
			ORDER BY LftValue

	END

END
GO

PRINT 'Modify stored procedure ProductType_GetProductTypes'
GO
IF(OBJECT_ID('ProductType_GetProductTypes') IS NOT NULL)
	DROP PROCEDURE ProductType_GetProductTypes
GO
PRINT 'Modify stored procedure ProductType_GetProductTypes'
GO
CREATE PROCEDURE [dbo].[ProductType_GetProductTypes](
--********************************************************************************
-- Parameters
--********************************************************************************
			@Ids XML=NULL,
			@ApplicationId	UNIQUEIDENTIFIER = NULL
)
AS
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
	IF (@Ids IS NOT NULL )
	BEGIN
        SELECT 
				ROW_NUMBER() OVER (ORDER BY A.Title) AS RowNumber,
				A.Id, 
				A.Title,  
				A.Description, A.FriendlyName, A.IsDownloadableMedia, A.TaxCategoryId,
				A.CreatedDate AS CreatedDate,
				A.CreatedBy,
				A.ModifiedDate AS ModifiedDate,
				A.ModifiedBy,
				A.Status,
				A.LftValue,
				A.RgtValue	,
				A.ParentId,
				A.CMSPageId
        FROM	dbo.PRProductType A
        WHERE	A.[Id]			IN 	(SELECT tab.col.value('text()[1]','uniqueidentifier') FROM @Ids.nodes('/GenericCollectionOfGuid/guid')tab(col) ) AND 
				--A.SiteId		=	Isnull(@ApplicationId, SiteId)		AND
				--(@ApplicationId is null or A.SiteId = @ApplicationId) AND
				A.Status		=	dbo.GetActiveStatus()	--- This will return only active .. 
		
	END
	ELSE
	BEGIN
			SELECT 
					A.Id, 
					A.Title,  
					A.Description, A.FriendlyName, A.IsDownloadableMedia,A.TaxCategoryId,
					A.CreatedDate AS CreatedDate,
					A.CreatedBy,
					A.ModifiedDate AS ModifiedDate,
					A.ModifiedBy,
					A.Status,
					A.LftValue,
					A.RgtValue,
					A.ParentId,
					A.CMSPageId	
            FROM	dbo.PRProductType A
            WHERE	
					--A.SiteId		=	Isnull(@ApplicationId, SiteId)		AND
					--(@ApplicationId is null or A.SiteId = @ApplicationId) AND
					A.Status		=	dbo.GetActiveStatus()
			ORDER BY LftValue
			

	END

END


GO
IF(OBJECT_ID('AttributeDto_Get') IS NOT NULL)
	DROP PROCEDURE AttributeDto_Get
GO
PRINT 'Modify stored procedure AttributeDto_Get'
GO

CREATE PROCEDURE [dbo].[AttributeDto_Get]
(
	@Id				UNIQUEIDENTIFIER = NULL,
	@Ids			NVARCHAR(MAX) = NULL,
	@CategoryId		INT = NULL,
	@GroupId		UNIQUEIDENTIFIER = NULL,
	@SiteId			UNIQUEIDENTIFIER = NULL,
	@ObjectId		UNIQUEIDENTIFIER = NULL,
	@IsFaceted		BIT = NULL,
	@IsSearchable	BIT = NULL,
	@IsDisplayed	BIT = NULL,
	@IsPersonalized	BIT = NULL,
	@IsSystem		BIT = NULL,
	@PageNumber		INT = NULL,
	@PageSize		INT = NULL,
	@Status			INT = 1,
	@MaxRecords		INT = NULL,
	@SortBy			NVARCHAR(100) = NULL,  
	@SortOrder		NVARCHAR(10) = NULL,
	@Keyword		NVARCHAR(1000) = NULL,
	@TotalRecords	INT = NULL OUTPUT
)
AS
BEGIN
	DECLARE	@StartRow	INT,
			@EndRow		INT,
			@SortClause	NVARCHAR(100)

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END

	IF @MaxRecords IS NULL OR @MaxRecords <= 0
		SET @MaxRecords = 2147483647

	IF (@SortOrder IS NULL OR @SortClause != 'DESC')
		SET @SortOrder = 'ASC'

	IF (@SortBy IS NOT NULL)
		SET @SortClause = @SortBy + ' ' + @SortOrder	

	SELECT		A.*,
				ROW_NUMBER() OVER (ORDER BY
					CASE WHEN @SortClause = 'Title ASC' THEN A.Title END ASC,
					CASE WHEN @SortClause = 'Title DESC' THEN A.Title END DESC,
					CASE WHEN @SortClause = 'CreatedDate ASC' THEN A.CreatedDate END ASC,  
					CASE WHEN @SortClause = 'CreatedDate DESC' THEN A.CreatedDate END DESC,  
					CASE WHEN @SortClause = 'ModifiedDate ASC' THEN ISNULL(A.ModifiedDate, A.CreatedDate) END ASC,  
					CASE WHEN @SortClause = 'ModifiedDate DESC' THEN ISNULL(A.ModifiedDate, A.CreatedDate) END DESC,  
					CASE WHEN @SortClause IS NULL THEN A.Title END ASC	
				) AS RowNumber
	INTO		#Attributes
	FROM		(
					SELECT		DISTINCT A.Id, A.AttributeDataType, A.AttributeDataTypeId, ADT.Title AS AttributeDataTypeTitle, A.Title, A.[Description], A.[Sequence], A.Code,
								A.IsFaceted, A.IsSearched AS IsSearchable, A.IsDisplayed, A.IsPersonalized, A.IsEnum, A.IsMultiValued, A.IsSystem, A.[Status],
								A.CreatedDate, A.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
								A.ModifiedDate, A.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
					FROM		ATAttribute AS A
								INNER JOIN ATAttributeDataType AS ADT ON ADT.Id = A.AttributeDataTypeId
								LEFT JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
								LEFT JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId
								LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = A.CreatedBy
								LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = A.ModifiedBy
					WHERE		(@Id IS NULL OR A.Id = @Id) AND
								(@Ids IS NULL OR A.Id IN (SELECT [Value] FROM dbo.SplitComma(@Ids, ','))) AND
								--(@SiteId IS NULL OR...) AND
								(@ObjectId IS NULL OR (A.Id IN 
									(SELECT AttributeId FROM ATAttributeValue WHERE ObjectId=@ObjectId
										UNION
									SELECT AttributeId FROM ATSiteAttributeValue WHERE SiteId=@ObjectId
										UNION
									SELECT AttributeId FROM ATPageAttributeValue WHERE PageDefinitionId=@ObjectId
										UNION
									 SELECT AttributeId FROM PRProductTypeAttribute WHERE ProductTypeId=@ObjectId
										UNION	
									SELECT AttributeId FROM PRProductAttribute PA INNER JOIN PRProductSKU S ON PA.ProductId = S.ProductId WHERE S.ProductId=@ObjectId OR S.Id=@ObjectId
										) OR ACI.CategoryId = @CategoryId)) 
								AND
								(@CategoryId IS NULL OR @ObjectId IS NOT NULL OR ACI.CategoryId = @CategoryId) AND
								(@GroupId IS NULL OR AC.GroupId = @GroupId) AND
								(@IsFaceted IS NULL OR A.IsFaceted = @IsFaceted) AND
								(@IsSearchable IS NULL OR A.IsSearched = @IsSearchable) AND
								(@IsDisplayed IS NULL OR A.IsDisplayed = @IsDisplayed) AND
								(@IsPersonalized IS NULL OR A.IsPersonalized = @IsPersonalized) AND
								(@IsSystem IS NULL OR A.IsSystem = @IsSystem) AND
								(@Keyword IS NULL OR A.Title LIKE '%' + @Keyword + '%' OR A.[Description] LIKE '%' + @Keyword + '%') AND	
								(@Status IS NULL OR A.[Status] = @Status)
			) AS A

	SELECT		TOP(@MaxRecords) *
	FROM		#Attributes
	WHERE		@StartRow IS NULL OR
				@EndRow IS NULL OR 
				RowNumber BETWEEN @StartRow AND @EndRow
	ORDER BY	RowNumber ASC

	SELECT		AE.Id, AE.AttributeId, AE.Title, AE.Code, AE.NumericValue, AE.[Value], AE.IsDefault, AE.[Sequence], AE.[Status],
				AE.CreatedDate, AE.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
				AE.ModifiedDate, AE.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
	FROM		#Attributes AS A
				INNER JOIN ATAttributeEnum AS AE ON AE.AttributeId = A.Id
				LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = AE.CreatedBy
				LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = AE.ModifiedBy
	WHERE		A.IsEnum = 1 AND (
					@StartRow IS NULL OR
					@EndRow IS NULL OR 
					A.RowNumber BETWEEN @StartRow AND @EndRow
				) AND
				AE.[Status] = 1
	ORDER BY	A.RowNumber ASC

	SELECT		ACI.Id, ACI.AttributeId, ACI.CategoryId, AC.[Name] AS CategoryName
	FROM		#Attributes AS A
				INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
				INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId
	WHERE		(
					@StartRow IS NULL OR
					@EndRow IS NULL OR 
					A.RowNumber BETWEEN @StartRow AND @EndRow
				) AND
				A.[Status] = 1

	ORDER BY	A.RowNumber ASC

	SELECT		AV.Id, AV.AttributeId, AV.ExpressionId, AV.MinValue, AV.MaxValue, AE.Title AS ExpressionName, AE.RegularExpression
	FROM		#Attributes AS A
				INNER JOIN ATAttributeValidation AS AV ON AV.AttributeId = A.Id
				LEFT JOIN ATAttributeExpression AS AE ON AE.Id = AV.ExpressionId
	WHERE		@StartRow IS NULL OR
				@EndRow IS NULL OR 
				A.RowNumber BETWEEN @StartRow AND @EndRow
	ORDER BY	A.RowNumber ASC

	SET @TotalRecords = (
		SELECT	COUNT(Id)
		FROM	#Attributes
	)
END
GO

PRINT 'Modify stored procedure ProductType_Save'
GO
IF(OBJECT_ID('ProductType_Save') IS NOT NULL)
	DROP PROCEDURE ProductType_Save
GO
CREATE PROCEDURE [dbo].[ProductType_Save]    
(    
 @Id [uniqueidentifier] = null out,    
 @Title [nvarchar](max),    
 @Description [nvarchar](max)=null,    
 @CMSPageId uniqueidentifier =null,    
 @FriendlyName [nvarchar] (max),
 @IsDownloadableMedia bit = 0,
 @ParentId uniqueidentifier =null,    
 @ApplicationId uniqueidentifier,    
 @Status int =null,    
 @ModifiedBy  uniqueidentifier,    
 @ModifiedDate datetime =null,    
 @Copy int = 0,   -- if it is one means copy else no.    
 @TaxCategoryId uniqueidentifier =null 
 )    
AS    
    
--********************************************************************************    
-- Variable declaration    
--********************************************************************************    
DECLARE     
  @Now   dateTime,    
  @NewGuid  uniqueidentifier,    
        @RowCount       int,    
  @LftValue  int,    
  @RgtValue int,
  @GrantParentId uniqueidentifier      
BEGIN    
--********************************************************************************    
-- code    
--********************************************************************************    
    
 Set @LftValue=0      
 Set @RgtValue=0      
    
 IF (@Id is null)    
 Begin
   IF((SELECT Count(FriendlyName) FROM  PRProductType WHERE ParentId = @ParentId And FriendlyName = @FriendlyName) > 0)    
   BEGIN    
	   RAISERROR('Seo url existing already||Id', 16, 1)    
	   RETURN dbo.GetBusinessRuleErrorCode()    
   END      
 END
 ELSE
 Begin
   IF((SELECT Count(FriendlyName) FROM  PRProductType WHERE ParentId = @ParentId And FriendlyName = @FriendlyName And Id <> @Id ) > 0)    
   BEGIN    
	   RAISERROR('Seo url existing already||Id', 16, 1)    
	   RETURN dbo.GetBusinessRuleErrorCode()    
   END 
 End
    
 /* IF @Id specified, ensure exists */    
 IF (@Id is not null)    
 Begin    
       IF((SELECT Count(Title) FROM  PRProductType WHERE Id = @Id ) = 0)    
       BEGIN    
		   RAISERROR('NOTEXISTS||Id', 16, 1)    
		   RETURN dbo.GetBusinessRuleErrorCode()    
       END    
 End    
 ELSE    
 BEGIN    
  Select @LftValue =LftValue ,@RgtValue=RgtValue,@GrantParentId=ParentId     
  FROM PRProductType       
  Where Id= @ParentId    
    
  IF @LftValue <=0      
  Begin      
   RAISERROR('NOTEXISTS||ParentId', 16, 1)      
   RETURN dbo.GetBusinessRuleErrorCode()      
  End     
 END    
 
 /* It should take from the parent only in insert but, if the user deleted the attached page and edit and save this record, that time, it won't have cms page id.. so let it take parent always*/   
 IF (@CMSPageId Is null OR @CMSPageId = '00000000-0000-0000-0000-000000000000')
 BEGIN
	SELECT @CMSPageId = T.CMSPageId FROM PRProductType T WHERE T.Id = @ParentId AND T.SiteId =@ApplicationId
	IF @CMSPageId IS NULL
	BEGIN
		SET @CMSPageId =( Select  Top 1 D.PageDefinitionId
		from STSettingType T
		Inner join STSiteSetting S on T.Id = S.SettingTypeId
		iNNER JOIN PageMapNodePageDef M on M.PageMapNodeId =cast(S.Value  as uniqueidentifier)
		INNER JOIN PageDefinition D on D.PageDefinitionId =M.PageDefinitionId
		Where T.Name='ProductTypePageMapNodeId'  AND S.SiteId =@ApplicationId)
	END
 END  
    
 SET @Now = getUTCDate()    
 IF (@Id is null)   -- New INSERT    
 BEGIN    
  SET @NewGuid = newid()    
  SET @Status= dbo.GetActiveStatus()    
  
  IF @Copy!=1
  BEGIN  
  /* Update Left and Right values */    
  UPDATE PRProductType Set      
   LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,      
   RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End       
  --WHERE SiteId=@ApplicationId      
    
   
  INSERT INTO PRProductType (    
                    [Id],    
                    [Title],    
                    [Description],   FriendlyName, IsDownloadableMedia, TaxCategoryId, CMSPageId,
     [ParentId],    
                    [CreatedBy],    
                    [CreatedDate],    
                    [Status],    
                    [SiteId],LftValue,RgtValue)     
    
        VALUES (    
                    @NewGuid,    
                    @Title,    
                    @Description,    @FriendlyName, @IsDownloadableMedia, @TaxCategoryId, @CMSPageId,
     @ParentId,     
                    @ModifiedBy,    
                    @Now,    
                    isnull(@Status,dbo.GetActiveStatus()),    
                    @ApplicationId,@RgtValue,@RgtValue+1)    
                    
   END                 
    
  IF(@copy =1)    
  BEGIN    
   -- copy all the attributes of the parent    
 
  UPDATE PRProductType Set      
   LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,      
   RgtValue = case when RgtValue >@RgtValue  then RgtValue +2 ELse RgtValue End       
  --WHERE SiteId=@ApplicationId      
 
   
   INSERT INTO PRProductType (    
                    [Id],    
                    [Title],    
                    [Description],    FriendlyName, IsDownloadableMedia, TaxCategoryId, CMSPageId,
					[ParentId],    
                    [CreatedBy],    
                    [CreatedDate],    
                    [Status],    
                    [SiteId],LftValue,RgtValue)     
    
        VALUES (    
                    @NewGuid,    
                    @Title,    
                    @Description,     @FriendlyName, @IsDownloadableMedia, @TaxCategoryId, @CMSPageId,
					@GrantParentId,     
                    @ModifiedBy,    
                    @Now,    
                    isnull(@Status,dbo.GetActiveStatus()),    
                    @ApplicationId,@RgtValue+1,@RgtValue+2)    
    
   INSERT INTO [dbo].[PRProductTypeAttribute]    
   (    
    [Id]    
    ,[ProductTypeId]    
    ,[AttributeId]    
    ,[Sequence]    
    ,[IsSKULevel]    
   )    
   SELECT    
    newid()    
    ,@NewGuid    
    ,AttributeId    
    ,Sequence    
    ,IsSKULevel    
   FROM [dbo].[PRProductTypeAttribute]    
   WHERE ProductTypeId=@ParentId    
      
  INSERT INTO [dbo].[PRProductTypeAttributeValue]    
           (    
    [Id]    
    ,[ProductTypeAttributeId]    
    ,[AttributeId]    
    ,[AttributeValueId]    
    ,[IsSKULevel]    
    ,[Sequence])    
  SELECT    
    newid()    
    ,ANew.Id As ProductTypeAttributeId    
    ,AV.AttributeId    
    ,AV.AttributeValueId    
    ,AV.IsSKULevel    
    ,AV.Sequence    
   FROM [dbo].[PRProductTypeAttributeValue] AV    
   INNER JOIN [dbo].[PRProductTypeAttribute] AOld ON AOld.Id=AV.ProductTypeAttributeId    
   INNER JOIN [dbo].[PRProductTypeAttribute] ANew ON AOld.AttributeId = ANew.AttributeId
   WHERE AOld.ProductTypeId=@ParentId AND ANew.ProductTypeId = @NewGuid    
      
      
  END    
    
        IF @@ERROR <> 0    
        BEGIN    
   RETURN dbo.GetDataBaseErrorCode()             
        END      
  SELECT @Id = @NewGuid         
      
      
 --   IF(Select count(*) from STSettingType T
	--	Inner join STSiteSetting S on T.Id = S.SettingTypeId
	--	Where T.Name='ProductTypePageMapNodeId'  AND SiteId !=@ApplicationId
	--	)>0
	--	BEGIN
	--	;with ctePageMapNode as(
	--	Select cast(S.Value  as uniqueidentifier) PagemapNodeId,SiteId
	--	from STSettingType T
	--	Inner join STSiteSetting S on T.Id = S.SettingTypeId
	--	Where T.Name='ProductTypePageMapNodeId'  AND SiteId !=@ApplicationId
	--	)
	--	INSERT INTO PRProductTypeCMSPage(Id,ProductTypeId,CMSPageId,SiteId,CreatedBy,CreatedDate)
	--	Select NEWID(),@Id, CAST(MIN(CAST(M.PageDefinitionId as CHAR(36))) as uniqueidentifier) PageDefinitionId,P.SiteId,@ModifiedBy,@Now 
	--	from ctePageMapNode T
	--	INNER JOIN PageMapNodePageDef M ON  T.PagemapNodeId = M.PageMapNodeId 
	--	Inner Join PageDefinition P on M.PageDefinitionId = P.PageDefinitionId and T.SiteId = P.SiteId
	--	Group By P.SiteId
	--END
    
 END    
 ELSE    
 BEGIN    
  -- we are not handling move    
            UPDATE PRProductType  WITH (ROWLOCK)    
            SET     [Title]   =  @Title,     
                    [Description]  =  @Description,    
				    FriendlyName	= @FriendlyName,
					IsDownloadableMedia = @IsDownloadableMedia,
					TaxCategoryId = @TaxCategoryId,
					CMSPageId = @CMSPageId,
     [ParentId]  = ISNULL(@ParentId,ParentId),    
                    [ModifiedBy]  =  @ModifiedBy,    
                    [ModifiedDate] =  @Now,    
                    [Status]  = ISNULL(@Status,[Status]),    
                    [SiteId]  = @ApplicationId    
            WHERE   [Id]            = @Id     
        
-- not doing concurrency check. need to relaod object. not possible due to ajax     
   -- last modified date when record was read should match with the current Modified Date     
   -- in the table for the record otherwise it means that someone has modified it already.    
    
            SELECT  @RowCount = @@ROWCOUNT    
            IF @@ERROR <> 0    
            BEGIN    
    RETURN dbo.GetDataBaseErrorCode()    
            END    
            IF @RowCount = 0    
            BEGIN    
                /* concurrency error */    
                RAISERROR('DBCONCURRENCY',16,1)    
                RETURN dbo.GetDataConcurrencyErrorCode()                                       
            END      
 END    
END
GO

PRINT 'Modify View vwProductType'
GO
IF(OBJECT_ID('vwProductType') IS NOT NULL)
	DROP View vwProductType
GO
CREATE VIEW [dbo].[vwProductType]
AS
SELECT	PT.Id,
		CASE WHEN EXISTS (SELECT 1 FROM SISite WHERE Id = PT.ParentId) THEN NULL ELSE PT.ParentId END AS ParentId,
		PT.Title, PT.[Description], S.Id AS SiteId, PT.CMSPageId, PT.FriendlyName, PT.IsDownloadableMedia, PT.TaxCategoryId, PT.[Status],
		PT.CreatedDate, PT.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
		PT.ModifiedDate, PT.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
FROM	SISite AS S
		CROSS JOIN PRProductType AS PT
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = PT.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = PT.ModifiedBy
GO


PRINT 'Modify View vwProductTypeList'
GO
IF(OBJECT_ID('vwProductTypeList') IS NOT NULL)
	DROP View vwProductTypeList
GO
CREATE VIEW [dbo].[vwProductTypeList]  
AS
WITH CTE AS
(
	SELECT P.Id,   
		P.SiteId,  
		P.Title,  
		P.Description,  
		P.FriendlyName,
		P.CMSPageId,
		ISNULL(P.ModifiedDate, P.CreatedDate) AS ModifiedDate, 
		ISNULL(P.ModifiedBy, P.CreatedBy) AS  ModifiedBy
	FROM PRProductType AS P

	UNION ALL

	SELECT P.Id,   
		P.SiteId,  
		P.Title,  
		P.Description,  
		P.FriendlyName,   
		P.CMSPageId,
		P.ModifiedDate,   
		P.ModifiedBy
	FROM PRProductTypeVariantCache AS P
)

SELECT P.Id,   
	C.SiteId,  
	P.SiteId AS SourceSiteId,  
	C.Title,  
	C.Description,  
	C.FriendlyName,
	P.ParentId,
	P.Status,  
	P.CreatedDate,   
	P.CreatedBy,   
	CFN.UserFullName AS CreatedByFullName,  
	C.ModifiedDate,   
	C.ModifiedBy,   
	MFN.UserFullName AS ModifiedByFullName,  
	P.IsDownloadableMedia,  
	P.TaxCategoryId,
	C.CMSPageId
FROM CTE C  
	INNER JOIN PRProductType AS P ON P.Id = C.Id  
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy  
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy  
GO
PRINT 'Modify stored procedure AttributeDto_Get'
GO
IF(OBJECT_ID('AttributeDto_Get') IS NOT NULL)
	DROP PROCEDURE AttributeDto_Get
GO

CREATE PROCEDURE [dbo].[AttributeDto_Get]
(
	@Id				UNIQUEIDENTIFIER = NULL,
	@Ids			NVARCHAR(MAX) = NULL,
	@CategoryId		INT = NULL,
	@GroupId		UNIQUEIDENTIFIER = NULL,
	@SiteId			UNIQUEIDENTIFIER = NULL,
	@ObjectId		UNIQUEIDENTIFIER = NULL,
	@IsFaceted		BIT = NULL,
	@IsSearchable	BIT = NULL,
	@IsDisplayed	BIT = NULL,
	@IsPersonalized	BIT = NULL,
	@IsSystem		BIT = NULL,
	@PageNumber		INT = NULL,
	@PageSize		INT = NULL,
	@Status			INT = 1,
	@MaxRecords		INT = NULL,
	@SortBy			NVARCHAR(100) = NULL,  
	@SortOrder		NVARCHAR(10) = NULL,
	@Keyword		NVARCHAR(1000) = NULL,
	@TotalRecords	INT = NULL OUTPUT
)
AS
BEGIN
	DECLARE	@StartRow	INT,
			@EndRow		INT,
			@SortClause	NVARCHAR(100)

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END

	IF @MaxRecords IS NULL OR @MaxRecords <= 0
		SET @MaxRecords = 2147483647

	IF (@SortOrder IS NULL OR @SortClause != 'DESC')
		SET @SortOrder = 'ASC'

	IF (@SortBy IS NOT NULL)
		SET @SortClause = @SortBy + ' ' + @SortOrder	

	SELECT		A.*,
				ROW_NUMBER() OVER (ORDER BY
					CASE WHEN @SortClause = 'Title ASC' THEN A.Title END ASC,
					CASE WHEN @SortClause = 'Title DESC' THEN A.Title END DESC,
					CASE WHEN @SortClause = 'CreatedDate ASC' THEN A.CreatedDate END ASC,  
					CASE WHEN @SortClause = 'CreatedDate DESC' THEN A.CreatedDate END DESC,  
					CASE WHEN @SortClause = 'ModifiedDate ASC' THEN ISNULL(A.ModifiedDate, A.CreatedDate) END ASC,  
					CASE WHEN @SortClause = 'ModifiedDate DESC' THEN ISNULL(A.ModifiedDate, A.CreatedDate) END DESC,  
					CASE WHEN @SortClause IS NULL THEN A.Title END ASC	
				) AS RowNumber
	INTO		#Attributes
	FROM		(
					SELECT		DISTINCT A.Id, A.AttributeDataType, A.AttributeDataTypeId, ADT.Title AS AttributeDataTypeTitle, A.Title, A.[Description], A.[Sequence], A.Code,
								A.IsFaceted, A.IsSearched AS IsSearchable, A.IsDisplayed, A.IsPersonalized, A.IsEnum, A.IsMultiValued, A.IsSystem, A.[Status],
								A.CreatedDate, A.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
								A.ModifiedDate, A.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
					FROM		ATAttribute AS A
								INNER JOIN ATAttributeDataType AS ADT ON ADT.Id = A.AttributeDataTypeId
								LEFT JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
								LEFT JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId
								LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = A.CreatedBy
								LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = A.ModifiedBy
					WHERE		(@Id IS NULL OR A.Id = @Id) AND
								(@Ids IS NULL OR A.Id IN (SELECT [Value] FROM dbo.SplitComma(@Ids, ','))) AND
								--(@SiteId IS NULL OR...) AND
								(@ObjectId IS NULL OR A.Id IN (SELECT AttributeId FROM ATAttributeValue WHERE ObjectId = @ObjectId)) AND
								(@CategoryId IS NULL OR ACI.CategoryId = @CategoryId) AND
								(@GroupId IS NULL OR AC.GroupId = @GroupId) AND
								(@IsFaceted IS NULL OR A.IsFaceted = @IsFaceted) AND
								(@IsSearchable IS NULL OR A.IsSearched = @IsSearchable) AND
								(@IsDisplayed IS NULL OR A.IsDisplayed = @IsDisplayed) AND
								(@IsPersonalized IS NULL OR A.IsPersonalized = @IsPersonalized) AND
								(@IsSystem IS NULL OR A.IsSystem = @IsSystem) AND
								(@Keyword IS NULL OR A.Title LIKE '%' + @Keyword + '%' OR A.[Description] LIKE '%' + @Keyword + '%') AND	
								(@Status IS NULL OR A.[Status] = @Status)
			) AS A

	SELECT		TOP(@MaxRecords) *
	FROM		#Attributes
	WHERE		@StartRow IS NULL OR
				@EndRow IS NULL OR 
				RowNumber BETWEEN @StartRow AND @EndRow
	ORDER BY	RowNumber ASC

	SELECT		AE.Id, AE.AttributeId, AE.Title, AE.Code, AE.NumericValue, AE.[Value], AE.IsDefault, AE.[Sequence], AE.[Status],
				AE.CreatedDate, AE.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
				AE.ModifiedDate, AE.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
	FROM		#Attributes AS A
				INNER JOIN ATAttributeEnum AS AE ON AE.AttributeId = A.Id
				LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = AE.CreatedBy
				LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = AE.ModifiedBy
	WHERE		A.IsEnum = 1 AND (
					@StartRow IS NULL OR
					@EndRow IS NULL OR 
					A.RowNumber BETWEEN @StartRow AND @EndRow
				) AND
				AE.[Status] = 1
	ORDER BY	A.RowNumber ASC

	SELECT		ACI.Id, ACI.AttributeId, ACI.CategoryId, AC.[Name] AS CategoryName
	FROM		#Attributes AS A
				INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
				INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId
	WHERE		(
					@StartRow IS NULL OR
					@EndRow IS NULL OR 
					A.RowNumber BETWEEN @StartRow AND @EndRow
				) AND
				A.[Status] = 1

	ORDER BY	A.RowNumber ASC

	SELECT		AV.Id, AV.AttributeId, AV.ExpressionId, AV.MinValue, AV.MaxValue, AE.Title AS ExpressionName, AE.RegularExpression
	FROM		#Attributes AS A
				INNER JOIN ATAttributeValidation AS AV ON AV.AttributeId = A.Id
				LEFT JOIN ATAttributeExpression AS AE ON AE.Id = AV.ExpressionId
	WHERE		@StartRow IS NULL OR
				@EndRow IS NULL OR 
				A.RowNumber BETWEEN @StartRow AND @EndRow
	ORDER BY	A.RowNumber ASC

	SET @TotalRecords = (
		SELECT	COUNT(Id)
		FROM	#Attributes
	)
END
GO

PRINT 'Modify stored procedure AttributeDto_Save'
GO

IF(OBJECT_ID('AttributeDto_Save') IS NOT NULL)
	DROP PROCEDURE AttributeDto_Save
GO
CREATE PROCEDURE [dbo].[AttributeDto_Save]  
(  
 @Id      UNIQUEIDENTIFIER = NULL OUTPUT,  
 @SiteId     UNIQUEIDENTIFIER,  
 @AttributeDataTypeId UNIQUEIDENTIFIER = NULL,  
 @Title     NVARCHAR(500) = NULL,  
 @Description   NVARCHAR(200) = NULL,  
 @Code     NVARCHAR(18) = NULL,  
 @Sequence    INT = NULL,  
 @IsFaceted    BIT = NULL,  
 @IsSearchable   BIT = NULL,  
 @IsDisplayed   BIT = NULL,  
 @IsPersonalized   BIT = NULL,  
 @IsMultiValued   BIT = NULL,  
 @IsSystem    BIT = NULL,  
 @Status     INT = NULL,  
 @ModifiedBy    UNIQUEIDENTIFIER,  
 @AttributeCategoryIds NVARCHAR(MAX) = NULL,  
 @EnumValues    TYAttributeEnum READONLY,  
 @ValidationExpressionId UNIQUEIDENTIFIER = NULL,  
 @ValidationMinValue  NVARCHAR(500) = NULL,  
 @ValidationMaxValue  NVARCHAR(500) = NULL

)  
AS  
BEGIN  
 DECLARE @UtcNow DATETIME = GETUTCDATE()  
 DECLARE @EnumDataTypeId UNIQUEIDENTIFIER = (SELECT TOP 1 Id FROM ATAttributeDataType WHERE Title = 'Enumerated')  
 DECLARE @IsNew bit
 SET @IsNew = 0

 IF NOT EXISTS (SELECT 1 FROM ATAttribute WHERE Id = @Id)
	SET @IsNew = 1	  
 -- Insert/update attribute record  
 IF (@IsNew = 1)  
 BEGIN  
  --SET @Id = NEWID()  
      
  INSERT INTO ATAttribute  
  (  
   Id,  
   AttributeDataTypeId,  
   Title,  
   [Description],  
   [Sequence],  
   Code,  
   IsFaceted,  
   IsSearched,  
   IsDisplayed,  
   IsPersonalized,  
   IsEnum,  
   IsMultiValued,  
   IsSystem,  
   [Status],  
   CreatedDate,  
   CreatedBy,  
   ModifiedDate,  
   ModifiedBy,
   SiteId  
  )  
  VALUES  
  (  
   @Id,  
   @AttributeDataTypeId,  
   @Title,  
   @Description,  
   @Sequence,  
   @Code,  
   ISNULL(@IsFaceted, 0),  
   ISNULL(@IsSearchable, 1),  
   ISNULL(@IsDisplayed, 1),  
   ISNULL(@IsPersonalized, 0),  
   CASE WHEN @AttributeDataTypeId = @EnumDataTypeId THEN 1 ELSE 0 END,  
   ISNULL(@IsMultiValued, 0),   
   ISNULL(@IsSystem, 0),  
   1,  
   @UtcNow,  
   @ModifiedBy,  
   @UtcNow,  
   @ModifiedBy,
   @SiteId  
  )  
 END  
 ELSE  
 BEGIN  
  IF @Status <= 0  
   SET @Status = NULL  
    
  UPDATE ATAttribute  
  SET  AttributeDataTypeId = ISNULL(@AttributeDataTypeId, AttributeDataTypeId),  
    Title = ISNULL(@Title, Title),  
    [Description] = ISNULL(@Description, [Description]),  
    [Sequence] = ISNULL(@Sequence, [Sequence]),  
    Code = ISNULL(@Code, Code),  
    IsFaceted = ISNULL(@IsFaceted, IsFaceted),  
    IsSearched = ISNULL(@IsSearchable, IsSearched),  
    IsDisplayed = ISNULL(@IsDisplayed, IsDisplayed),  
    IsPersonalized = ISNULL(@IsPersonalized, IsPersonalized),  
    IsEnum = CASE WHEN ISNULL(@AttributeDataTypeId, AttributeDataTypeId) = @EnumDataTypeId THEN 1 ELSE 0 END,  
    IsMultiValued = ISNULL(@IsMultiValued, IsMultiValued),  
    IsSystem = ISNULL(@IsSystem, IsSystem),  
    [Status] = ISNULL(@Status, [Status]),  
    ModifiedDate = @UtcNow,  
    ModifiedBy = @ModifiedBy,
	SiteId = @SiteId  
  WHERE Id = @Id   
 END  
  
 -- Sync enum values if attribute is Enumerable type  
 IF EXISTS (  
  SELECT Id  
  FROM ATAttribute  
  WHERE Id = @Id AND  
    AttributeDataTypeId = @EnumDataTypeId  
 )  
 BEGIN  
  -- Clean up enum data  
  DECLARE @SanitizedEnumValues TYAttributeEnum  
  
  
  INSERT INTO @SanitizedEnumValues  
   SELECT *  
   FROM @EnumValues  
  
  UPDATE @SanitizedEnumValues  
  SET  Id = CASE WHEN Id IS NULL OR Id = dbo.GetEmptyGUID() THEN NEWID() ELSE Id END,  
    AttributeId = @Id,  
    [Status] = ISNULL([Status], 1),  
    CreatedDate = ISNULL(CreatedDate, @UtcNow),  
    CreatedBy = ISNULL(CreatedBy, @ModifiedBy),  
    ModifiedDate = @UtcNow,  
    ModifiedBy = ISNULL(ModifiedBy, @ModifiedBy)  
  
  -- Insert new records  
  INSERT INTO ATAttributeEnum(Id, AttributeId, Title, Code, NumericValue, [Value], IsDefault, [Sequence], [Status], CreatedDate, CreatedBy, ModifiedDate, ModifiedBy)  
   SELECT EV.Id, EV.AttributeId, EV.Title, EV.Code, EV.NumericValue, EV.[Value], EV.IsDefault, EV.[Sequence], EV.[Status], EV.CreatedDate, EV.CreatedBy, EV.ModifiedDate, EV.ModifiedBy  
   FROM @SanitizedEnumValues AS EV  
     LEFT JOIN ATAttributeEnum AS AE ON AE.Id = EV.Id  
   WHERE AE.Id IS NULL  
  
  -- Update existing records  
  UPDATE AE  
  SET  Title = EV.Title, Code = EV.Code, NumericValue = EV.NumericValue, [Value] = EV.[Value], [Sequence] = EV.[Sequence], [Status] = EV.[Status],  
    CreatedDate = EV.CreatedDate, CreatedBy = EV.CreatedBy, ModifiedDate = EV.ModifiedDate, ModifiedBy = EV.ModifiedBy  
  FROM @SanitizedEnumValues AS EV  
    INNER JOIN ATAttributeEnum AS AE ON AE.Id = EV.Id  
  
  -- Mark enum values not passed in table as deleted  
  UPDATE AE  
  SET  [Status] = 3, ModifiedDate = @UtcNow, ModifiedBy = @ModifiedBy  
  FROM ATAttributeEnum AS AE  
    LEFT JOIN @SanitizedEnumValues AS EV ON EV.Id = AE.Id  
  WHERE AE.AttributeId = @Id AND  
    EV.Id IS NULL  
 END  
 -- Mark enum values for non-enum types as deleted  
 ELSE  
 BEGIN  
  UPDATE ATAttributeEnum  
  SET  [Status] = 3, ModifiedDate = @UtcNow, ModifiedBy = @ModifiedBy  
  WHERE AttributeId = @Id  
 END  
  
 -- Map attribute to categories  
 DELETE  
 FROM ATAttributeCategoryItem  
 WHERE AttributeId = @Id  
  
 IF @AttributeCategoryIds IS NOT NULL  
 BEGIN  
  INSERT INTO ATAttributeCategoryItem(Id, AttributeId, CategoryId)  
   SELECT NEWID(), @Id, CONVERT(INT, [Value])  
   FROM dbo.SplitComma(@AttributeCategoryIds, ',')  
 END  
  
 -- Create validation record  
 DELETE  
 FROM ATAttributeValidation  
 WHERE AttributeId = @Id  
  
 INSERT INTO ATAttributeValidation(Id, AttributeId, ExpressionId, MinValue, MaxValue)  
  VALUES (NEWID(), @Id, @ValidationExpressionId, @ValidationMinValue, @ValidationMaxValue)  
END  
GO

PRINT 'Create stored procedure AttributeDto_Delete'
GO

IF(OBJECT_ID('AttributeDto_Delete') IS NOT NULL)
	DROP PROCEDURE AttributeDto_Delete
GO

CREATE PROCEDURE [dbo].[AttributeDto_Delete]
(
	@Id				uniqueidentifier = NULL,
	@ModifiedBy		uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UtcNow DATETIME = GETUTCDATE()
	
	UPDATE	ATAttribute
	SET		[Status] = 3,
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @UtcNow
	WHERE	Id = @Id
	
	DELETE
	FROM	ATAttributeCategoryItem
	WHERE	AttributeId = @Id
END
GO

PRINT 'Create stored procedure AttributeCategoryDto_Delete'
GO

IF(OBJECT_ID('AttributeCategoryDto_Delete') IS NOT NULL)
	DROP PROCEDURE AttributeCategoryDto_Delete
GO

CREATE PROCEDURE [dbo].[AttributeCategoryDto_Delete]
(
	@Id INT
)
AS
BEGIN
	DELETE
	FROM	ATAttributeCategoryItem
	WHERE	CategoryId = @Id

	DELETE
	FROM	ATAttributeCategory
	WHERE	Id = @Id
END
GO

PRINT 'Modify trigger tgrATAttributeCategoryItem'
GO

IF(OBJECT_ID('tgrATAttributeCategoryItem') IS NOT NULL)
	DROP TRIGGER tgrATAttributeCategoryItem
GO

CREATE TRIGGER [dbo].[tgrATAttributeCategoryItem] ON [dbo].[ATAttributeCategoryItem]
AFTER INSERT
AS 
IF EXISTS(SELECT 1 FROM inserted WHERE CategoryId = 3)
BEGIN	
	DELETE
	FROM	TRKAttribute
	WHERE	AttributeId IN (
		SELECT	Id
		FROM	inserted
	) 

	INSERT INTO TRKAttribute(AttributeId, CreatedDate, [Status]) 
		SELECT	DISTINCT AttributeId, GETUTCDATE(), 1
		FROM	inserted

	EXEC Contact_AddColumnsToFlatTable
END

PRINT 'Modify stored procedure AttributeItem_GetAttributeEnum'
GO

IF(OBJECT_ID('AttributeItem_GetAttributeEnum') IS NOT NULL)
	DROP PROCEDURE AttributeItem_GetAttributeEnum
GO

CREATE PROCEDURE [dbo].[AttributeItem_GetAttributeEnum]
(
	@AttributeGroupId uniqueidentifier = null,
	@AttributeId uniqueidentifier=null,
	@ApplicationId uniqueidentifier=null
)
as
Begin
Declare @activeStatus int
set @activeStatus = dbo.GetActiveStatus()

if @AttributeGroupId is null and @AttributeId is null
	BEGIN
		RAISERROR('At least one of AttributeGroup or Attribute Id is required', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END
    
    
SELECT DISTINCT
		AV.Id as Id,
		AV.Id as AttributeEnumId,
		A.Id as AttributeId, 
		AV.Code,
		IsNull(AV.[IsDefault], 0) as [IsDefault],
		AV.Sequence,
		AV.NumericValue,
		AV.[Value], 		
		AV.CreatedDate, 
		AV.CreatedBy, 
		AV.ModifiedDate, 
		AV.ModifiedBy,
		AV.Status
from ATAttribute A 
Left Join ATAttributeEnum AV on A.Id = AV.AttributeId
LEFT JOIN ATAttributeCategoryItem CI on CI.AttributeId= A.Id
LEFT JOIN ATAttributeCategory C ON C.Id = CI.CategoryId
WHERE 	 (@AttributeId is null or A.Id = @AttributeId)
		and (@AttributeGroupId is null or C.GroupId=@AttributeGroupId)
		and AV.Status =@activeStatus
Order by A.Id, AV.Sequence 	 
end
GO
PRINT 'Modify view vwProductAttributeValue'
GO
IF(OBJECT_ID('vwProductAttributeValue') IS NOT NULL)
	DROP VIEW vwProductAttributeValue
GO
CREATE VIEW [dbo].[vwProductAttributeValue]
AS
SELECT A.Id, 
	P.SiteId,
	P.Id AS ObjectId,
	A.AttributeId, 
	A.AttributeEnumId, 
	A.Value,
	A.CreatedBy,
	A.CreatedDate
FROM PRProductAttributeValue A
	JOIN PRProduct P ON P.Id = A.ProductId

UNION ALL

SELECT A.Id,
	A.SiteId,
	A.ProductId AS ObjectId,
	A.AttributeId, 
	A.AttributeEnumId, 
	A.Value,
	A.ModifiedBy,
	A.ModifiedDate
FROM PRProductAttributeValueVariantCache A
GO
PRINT 'Modify view vwSkuAttributeValue'
GO
IF(OBJECT_ID('vwSkuAttributeValue') IS NOT NULL)
	DROP VIEW vwSkuAttributeValue
GO
CREATE VIEW [dbo].[vwSkuAttributeValue]
AS
SELECT A.Id,
	P.SiteId,
	P.Id AS ObjectId,
	A.AttributeId, 
	A.AttributeEnumId, 
	A.Value,
	A.CreatedBy,
	A.CreatedDate
FROM PRProductSKUAttributeValue A
	JOIN PRProductSKU P ON P.Id = A.SKUId

UNION ALL

SELECT A.Id,
	A.SiteId,
	A.SKUId,
	A.AttributeId, 
	A.AttributeEnumId, 
	A.Value,
	A.ModifiedBy,
	A.ModifiedDate
FROM PRProductSKUAttributeValueVariantCache A
GO


PRINT 'Modify view vwProductAndSKUAttributeValue'
GO
IF(OBJECT_ID('vwProductAndSKUAttributeValue') IS NOT NULL)
	DROP VIEW vwProductAndSKUAttributeValue
GO
CREATE VIEW vwProductAndSKUAttributeValue
AS
SELECT     
	VP.ObjectId AS Id,  P.Title AS ProductName, VP.AttributeId, 
	A.Title AS AttributeName, Isnull(AE.Value,VP.Value ) AS Value  
	FROM vwProductAttributeValue VP
	INNER JOIN PRProduct P ON P.Id = VP.ObjectId
	INNER JOIN ATAttribute A ON A.Id = VP.AttributeId
	LEFT JOIN ATAttributeEnum AS AE ON VP.AttributeEnumId = AE.Id

UNION 

SELECT
	VP.ObjectId AS Id,  P.Title AS ProductName, VP.AttributeId, 
	A.Title AS AttributeName, Isnull(AE.Value,VP.Value ) AS Value  
	FROM vwSkuAttributeValue VP
	INNER JOIN PRProduct P ON P.Id = VP.ObjectId
	INNER JOIN ATAttribute A ON A.Id = VP.AttributeId
	LEFT JOIN ATAttributeEnum AS AE ON VP.AttributeEnumId = AE.Id
GO


PRINT 'Modify view vwPageAttributeValue'
GO
IF(OBJECT_ID('vwPageAttributeValue') IS NOT NULL)
	DROP VIEW vwPageAttributeValue
GO
CREATE VIEW [dbo].[vwPageAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value,
		V.Notes, 
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V

	UNION

	SELECT ROW_NUMBER() over (PARTITION BY V.AttributeId, C.PageDefinitionId ORDER BY S.CreatedDate DESC) AS vRank,
		V.Id,
		C.PageDefinitionId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		V.Notes,
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATPageAttributeValue V
		JOIN PageDefinition P ON V.PageDefinitionId = P.PageDefinitionId
		JOIN PageDefinition C ON C.SourcePageDefinitionId = P.PageDefinitionId
		JOIN SISite S ON S.Id = P.SiteId
	WHERE V.IsShared = 1
		AND S.DistributionEnabled = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT V.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	ISNULL(V.Notes, C.Notes) AS Notes,
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.CreatedDate, 
	C.CreatedBy
FROM CTE C
	LEFT JOIN ATPageAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.PageDefinitionId
WHERE C.ValueRank = 1
GO
PRINT 'Modify view vwPageAttribute'
GO
IF(OBJECT_ID('vwPageAttribute') IS NOT NULL)
	DROP VIEW vwPageAttribute
GO
CREATE VIEW [dbo].[vwPageAttribute]
AS 
SELECT		
	A.Id, 
	ADT.Title AS AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.CreatedDate, 
	A.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	A.ModifiedDate, 
	A.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName,
	C.CategoryId,
	V.ObjectId
FROM ATAttribute AS A
	INNER JOIN ATAttributeDataType AS ADT ON ADT.Id = A.AttributeDataTypeId
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 2
	LEFT JOIN vwPageAttributeValue V ON A.Id = V.AttributeId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = A.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = A.ModifiedBy
GO
PRINT 'Modify view vwSiteAttributeValue'
GO
IF(OBJECT_ID('vwSiteAttributeValue') IS NOT NULL)
	DROP VIEW vwSiteAttributeValue
GO
CREATE VIEW [dbo].[vwSiteAttributeValue]
AS

WITH tempCTE AS
(	
	SELECT
		0 AS vRank,
		V.Id,
		V.SiteId AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		ISNULL(V.IsShared, 0) AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		0 AS IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V

	UNION

	SELECT ROW_NUMBER() over (PARTITION BY V.AttributeId, C.Id ORDER BY P.LftValue DESC) AS vRank,
		V.Id,
		C.Id AS ObjectId, 
		V.AttributeId, 
		V.AttributeEnumId, 
		V.Value, 
		0 AS IsShared,
		ISNULL(V.IsEditable, 0) AS IsEditable,
		CASE WHEN V.IsEditable != 1 THEN 1 ELSE 0 END IsReadOnly,
		V.SiteId,
		V.CreatedDate, 
		V.CreatedBy
	FROM  dbo.ATSiteAttributeValue V
		JOIN SISite P ON V.SiteId = P.Id
		JOIN SISite C ON C.LftValue > P.LftValue AND C.RgtValue < P.RgtValue
	WHERE V.IsShared = 1
		AND P.DistributionEnabled = 1
),
CTE AS
(
	SELECT ROW_NUMBER() over (PARTITION BY ObjectId, AttributeId ORDER BY vRank) AS ValueRank,
		*
	FROM tempCTE
)

SELECT V.Id,
	C.ObjectId,
	C.AttributeId, 
	ISNULL(V.AttributeEnumId, C.AttributeEnumId) AS AttributeEnumId, 
	ISNULL(V.Value, C.Value) AS Value, 
	C.IsShared,
	C.IsEditable,
	C.IsReadOnly,
	C.SiteId,
	C.CreatedDate, 
	C.CreatedBy
FROM CTE C
	LEFT JOIN ATSiteAttributeValue V ON C.AttributeId = V.AttributeId
		AND C.ObjectId = V.SiteId
		AND C.SiteId = V.SiteId
WHERE C.ValueRank = 1

GO
PRINT 'Modify view vwSiteAttribute'
GO
IF(OBJECT_ID('vwSiteAttribute') IS NOT NULL)
	DROP VIEW vwSiteAttribute
GO
CREATE VIEW [dbo].[vwSiteAttribute]
AS 
SELECT		
	A.Id, 
	ADT.Title AS AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.CreatedDate, 
	A.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	A.ModifiedDate, 
	A.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName,
	C.CategoryId,
	V.ObjectId
FROM ATAttribute AS A
	INNER JOIN ATAttributeDataType AS ADT ON ADT.Id = A.AttributeDataTypeId
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 1
	LEFT JOIN vwSiteAttributeValue V ON A.Id = V.AttributeId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = A.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = A.ModifiedBy
GO
PRINT 'Modify view vwReturns'
GO
IF(OBJECT_ID('vwReturns') IS NOT NULL)
	DROP VIEW vwReturns
GO
CREATE VIEW [dbo].[vwReturns]
AS 
SELECT	R.Id,
	R.RMACode,
	R.Status,
	R.CustomerId,
	C.UserFullName AS CustomerName,
	R.OrderId,
	O.PurchaseOrderNumber,
	R.ReasonTypeId AS ReasonType,
	R.ResolutionTypeId AS ResolutionType,
	R.CreatedDate,
	R.CreatedBy,
	CBN.UserFullName AS CreatedByFullName, 
	R.ModifiedDate,
	R.ModifiedBy,
	MBN.UserFullName AS ModifiedByFullName,
	O.SiteId 
FROM ORReturn AS R
	INNER JOIN OROrder O ON R.OrderId = O.Id
	LEFT JOIN VW_UserFullName AS C ON C.UserId = R.CustomerId
	LEFT JOIN VW_UserFullName AS CBN ON CBN.UserId = R.CreatedBy
	LEFT JOIN VW_UserFullName AS MBN ON MBN.UserId = R.ModifiedBy
GO
PRINT 'Modify view vwCouponUseCount'
GO
IF(OBJECT_ID('vwCouponUseCount') IS NOT NULL)
	DROP VIEW vwCouponUseCount
GO
CREATE VIEW [dbo].[vwCouponUseCount]
AS 
SELECT CPCode.CouponId, 
	COUNT(1) AS UseCount 
FROM CPOrderCouponCode ORDCode 
	INNER JOIN CPCouponCode CPCode ON ORDCode.CouponCodeId = CPCode.Id
	INNER JOIN OROrder O ON O.Id = ORDCode.OrderId
WHERE O.OrderStatusId NOT IN (12, 3)
GROUP BY CPCode.CouponId
GO
PRINT 'Modify view vwCouponCodeCount'
GO
IF(OBJECT_ID('vwCouponCodeCount') IS NOT NULL)
	DROP VIEW vwCouponCodeCount
GO
CREATE VIEW [dbo].[vwCouponCodeCount]
AS 
SELECT CouponId, 
	COUNT(1) AS NoOfCodes
FROM CPCouponCode 
GROUP BY CouponId
GO
PRINT 'Modify view vwCoupon'
GO
IF(OBJECT_ID('vwCoupon') IS NOT NULL)
	DROP VIEW vwCoupon
GO
CREATE VIEW [dbo].[vwCoupon]
AS 
SELECT		
	C.Id, 
	C.Title, 
	C.Status,
	C.StartDate,
	C.EndDate,
	T.Title AS CouponTypeTitle,
	CC.NoOfCodes,
	U.UseCount,
	C.SiteId,
	C.IsGenerated,
	C.AutoApply,
	C.Stackable,
	C.AppliedSequence,
	C.CreatedDate, 
	C.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	C.ModifiedDate, 
	C.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName
FROM CPCoupon AS C
	INNER JOIN CPCouponType T ON C.CouponTypeId = T.Id
	LEFT JOIN vwCouponUseCount U ON C.Id = U.CouponId
	LEFT JOIN vwCouponCodeCount CC ON C.Id = CC.CouponId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = C.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
GO
PRINT 'Modify view vwCreditCardType'
GO
IF(OBJECT_ID('vwCreditCardType') IS NOT NULL)
	DROP VIEW vwCreditCardType
GO
CREATE VIEW [dbo].[vwCreditCardType]
AS 
SELECT P.Id,
	P.Title,
	P.Description,
	CASE WHEN S.Status = 1 THEN 1 ELSE 0 END AS IsActive,
	S.SiteId
FROM PTCreditCardType P
	JOIN PTCreditCardTypeSite S ON P.Id = S.CreditCardTypeId
GO
PRINT 'Modify view vwPaymentGateway'
GO
IF(OBJECT_ID('vwPaymentGateway') IS NOT NULL)
	DROP VIEW vwPaymentGateway
GO
CREATE VIEW [dbo].[vwPaymentGateway]
AS 
SELECT P.Id,
	P.Name AS Title,
	P.Description,
	P.Sequence,
	P.DotNetProviderName,
	S.IsActive,
	S.SiteId,
	S.PaymentTypeId
FROM PTGateway P
	JOIN PTGatewaySite S ON P.Id = S.GatewayId
GO
PRINT 'Modify view vwPaymentType'
GO
IF(OBJECT_ID('vwPaymentType') IS NOT NULL)
	DROP VIEW vwPaymentType
GO
CREATE VIEW [dbo].[vwPaymentType]
AS 
SELECT P.Id,
	P.Name AS Title,
	P.Description,
	P.Sequence,
	CASE WHEN S.Status = 1 THEN 1 ELSE 0 END AS IsActive,
	S.SiteId
FROM PTPaymentType P
	JOIN PTPaymentTypeSite S ON P.Id = S.PaymentTypeId
GO
PRINT 'Modify view vwCouponCode'
GO
IF(OBJECT_ID('vwCouponCode') IS NOT NULL)
	DROP VIEW vwCouponCode
GO
CREATE VIEW [dbo].[vwCouponCode]
AS 
SELECT		
	CC.Id, 
	CC.CouponId,
	CC.Code,
	CC.Status,
	C.Status AS CouponStatus,
	C.Title AS CouponName,
	T.Title AS CouponTypeTitle,
	U.UseCount,
	CAC.CartId,
	OC.OrderId,
	C.SiteId,
	C.MinPurchase AS MinimumPurchase,
	C.AutoApply,
	C.IsGenerated,
	CC.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	CC.CreatedDate
FROM CPCouponCode AS CC
	INNER JOIN CPCoupon C ON C.Id = CC.CouponId
	INNER JOIN CPCouponType T ON C.CouponTypeId = T.Id
	LEFT JOIN vwCouponCodeUseCount U ON CC.Id = U.CouponCodeId
	LEFT JOIN CSCartCoupon AS CAC ON CAC.CouponCodeId = CC.Id
	LEFT JOIN CPOrderCouponCode OC ON CC.Id = OC.CouponCodeId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = CC.CreatedBy
GO

PRINT 'Modify view vwContactSimple'
GO
IF(OBJECT_ID('vwContactSimple') IS NOT NULL)
	DROP VIEW vwContactSimple
GO
CREATE VIEW [dbo].[vwContactSimple]
AS 
SELECT 
	C.Id,
	C.FirstName,
	C.LastName,
	C.Email,
	C.Gender,
	C.ContactType,
	C.Status,
	C.LastInteractionDate,
	C.LeadScore,
	CS.SiteId,
	C.CreatedDate, 
	C.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	C.ModifiedDate, 
	C.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName
FROM vw_contacts C
	JOIN vw_contactSite CS ON C.Id = CS.UserId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = C.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy

GO
PRINT 'Modify view vwFormContact'
GO
IF(OBJECT_ID('vwFormContact') IS NOT NULL)
	DROP VIEW vwFormContact
GO
CREATE VIEW [dbo].[vwFormContact]
AS 
SELECT 
	C.Id,
	C.FirstName,
	C.LastName,
	C.Email,
	C.Gender,
	C.ContactType,
	C.Status,
	C.LastInteractionDate,
	R.FormsId AS FormId,
	C.LeadScore,
	CS.SiteId,
	C.CreatedDate, 
	C.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	C.ModifiedDate, 
	C.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName
FROM vw_contacts C
	JOIN vw_contactSite CS ON C.Id = CS.UserId
	JOIN FormsResponse R ON C.Id = R.UserId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = C.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
GO
PRINT 'Modify view vwAttribute'
GO
IF(OBJECT_ID('vwAttribute') IS NOT NULL)
	DROP VIEW vwAttribute
GO
CREATE VIEW [dbo].[vwAttribute]
AS
WITH CTE AS
(
	SELECT A.Id, 
		A.SiteId,
		A.Title,
		A.Description,
		ISNULL(A.ModifiedDate, A.CreatedDate) AS ModifiedDate, 
		ISNULL(A.ModifiedBy, A.CreatedBy) AS  ModifiedBy
	FROM ATAttribute AS A

	UNION ALL

	SELECT A.Id, 
		A.SiteId,
		A.Title,
		A.Description,
		A.ModifiedDate, 
		A.ModifiedBy
	FROM ATAttributeVariantCache AS A
)

SELECT A.Id, 
	C.SiteId,
	A.SiteId AS SourceSiteId,
	C.Title,
	C.Description,
	ADT.Id AS AttributeDataTypeId,
	ADT.Title AS AttributeDataTypeTitle, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsSearched as IsSearchable,
	A.Code,
	A.IsDisplayed,
	A.IsEnum,
	A.IsPersonalized,
	A.Status,
	A.CreatedDate, 
	A.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	C.ModifiedDate, 
	C.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName,
	CAT.CategoryId,
	V.ObjectId
FROM CTE C
	INNER JOIN ATAttribute AS A ON A.Id = C.Id
	INNER JOIN ATAttributeDataType AS ADT ON ADT.Id = A.AttributeDataTypeId
	LEFT JOIN ATAttributeCategoryItem CAT ON A.Id = CAT.AttributeId
	LEFT JOIN ATAttributeValue V ON A.Id = V.AttributeId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = A.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
WHERE A.Status != 3
GO
PRINT 'Modify view vwAttributeCategory'
GO
IF(OBJECT_ID('vwAttributeCategory') IS NOT NULL)
	DROP VIEW vwAttributeCategory
GO
CREATE VIEW [dbo].[vwAttributeCategory]
AS 
SELECT C.Id,
	C.Name AS Title,
	C.Description,
	C.IsGlobal,
	ISNULL(C.IsHidden, 0) AS IsHidden,
	ISNULL(C.IsSystem, 0) AS IsSystem,
	C.Status,
	C.CreatedBy,
	CFN.UserFullName AS CreatedByFullName,
	C.CreatedDate,
	C.ModifiedBy,
	MFN.UserFullName AS ModifiedByFullName,
	C.ModifiedDate,
	CI.AttributeId,
	C.GroupId
FROM ATAttributeCategory C
	LEFT JOIN ATAttributeCategoryItem CI ON C.Id = CI.CategoryId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = C.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
GO
PRINT 'Modify view vwContactAttribute'
GO
IF(OBJECT_ID('vwContactAttribute') IS NOT NULL)
	DROP VIEW vwContactAttribute
GO
CREATE VIEW [dbo].[vwContactAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ContactId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 3
	LEFT JOIN ATContactAttributeValue V ON A.Id = V.AttributeId
GO
PRINT 'Modify view vwCustomerAttribute'
GO
IF(OBJECT_ID('vwCustomerAttribute') IS NOT NULL)
	DROP VIEW vwCustomerAttribute
GO
CREATE VIEW [dbo].[vwCustomerAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.CustomerId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 4
	LEFT JOIN CSCustomerAttributeValue V ON A.Id = V.AttributeId

GO
PRINT 'Modify view vwOrderAttribute'
GO
IF(OBJECT_ID('vwOrderAttribute') IS NOT NULL)
	DROP VIEW vwOrderAttribute
GO
CREATE VIEW [dbo].[vwOrderAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.OrderId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 8
	LEFT JOIN OROrderAttributeValue V ON A.Id = V.AttributeId
GO

PRINT 'Modify view vwOrderItemShippedQuantity'
GO
IF(OBJECT_ID('vwOrderItemShippedQuantity') IS NOT NULL)
	DROP VIEW vwOrderItemShippedQuantity
GO

CREATE VIEW [dbo].[vwOrderItemShippedQuantity]
AS 
SELECT OI.Id,
	SUM(SI.Quantity) Quantity
FROM FFOrderShipmentItem SI
	INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId = S.Id
	INNER JOIN OROrderItem OI ON SI.OrderItemId = OI.Id
WHERE ShipmentStatus = 2
GROUP BY OI.Id
GO

PRINT 'Modify view vwOrderItemReturnedQuantity'
GO
IF(OBJECT_ID('vwOrderItemReturnedQuantity') IS NOT NULL)
	DROP VIEW vwOrderItemReturnedQuantity
GO
CREATE VIEW [dbo].[vwOrderItemReturnedQuantity]
AS 
WITH CTE AS
(
	SELECT RI.OrderItemId, RI.ExpectedReturnQty AS Quantity FROM ORReturn R
	JOIN ORReturnItem RI ON R.Id = RI.ReturnId
	WHERE Status != 2

	UNION ALL

	SELECT RI.OrderItemId, RI.ReceivedQty AS Quantity FROM ORReturn R
		JOIN ORReturnItem RI ON R.Id = RI.ReturnId
	WHERE Status = 1
)

SELECT C.OrderItemId AS Id,
	SUM(C.Quantity) Quantity
FROM CTE C
GROUP BY C.OrderItemId
GO
PRINT 'Modify view vwOrderItem'
GO
IF(OBJECT_ID('vwOrderItemList') IS NOT NULL)
	DROP VIEW vwOrderItemList
GO
IF(OBJECT_ID('vwOrderItem') IS NOT NULL)
	DROP VIEW vwOrderItem
GO
CREATE VIEW vwOrderItem
AS
SELECT  OI.[Id],
	O.[SiteId],
	O.[OrderStatusId],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	S.[SKU],
	S.[Title] SKUTitle,
	OI.[Quantity],
	ISNULL(SQ.Quantity, 0) AS ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	OI.CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName ,
	LN.UserFullName ModifiedByFullName ,
	OI.ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount],
	OI.[TaxableDiscount],
	OI.[NonTaxableDiscount],
	OI.[HandlingTax],
	OI.[Status],
	P.Id AS ProductId,
	P.Title AS ProductTitle,
	ISNULL(RQ.Quantity, 0) AS ReturnedQuantity,
	PT.IsDownloadableMedia
FROM OROrderItem OI
	INNER JOIN OROrder O ON O.Id = OI.OrderId
	INNER JOIN PRProductSKU S ON S.Id = OI.ProductSKUId
	INNER JOIN PRProduct P ON P.Id = S.ProductId
	INNER JOIN PRProductType PT ON PT.Id = P.ProductTypeID
	LEFT JOIN vwOrderItemShippedQuantity SQ ON SQ.Id = OI.Id
	LEFT JOIN vwOrderItemReturnedQuantity RQ ON RQ.Id = OI.Id
	LEFT JOIN VW_UserFullName FN ON FN.UserId = OI.CreatedBy
	LEFT JOIN VW_UserFullName LN ON LN.UserId = OI.ModifiedBy
GO
PRINT 'Modify view vwOrderShipment'
GO
IF(OBJECT_ID('vwOrderShipment') IS NOT NULL)
	DROP VIEW vwOrderShipment
GO
CREATE View [dbo].[vwOrderShipment]
AS
SELECT OS.[Id],	
	OS.[OrderShippingId],
	OS.[ShippedDate],
	OS.[ShippingOptionId],
	OS.[EstimatedShipmentTotal],
	OS.[ShipmentStatus],
	OS.[WarehouseId],
	OS.[ExternalReferenceNumber],
	S.OrderId,
	W.Title AS WarehouseName,
	ROW_NUMBER() OVER (PARTITION BY S.Id ORDER BY OS.[CreateDate]) AS Sequence
FROM FFOrderShipment OS
	INNER JOIN OROrderShipping S ON S.Id = OS.OrderShippingId
	LEFT JOIN INWarehouse W ON W.Id = OS.WarehouseId
	LEFT JOIN ORShippingOption SO ON SO.Id = OS.ShippingOptionId
	LEFT JOIN ORShipper SH ON SH.Id = SO.ShipperId
GO
PRINT 'Modify view vwOrderShipmentItem'
GO

IF(OBJECT_ID('vwProductSkuCount') IS NOT NULL)
	DROP VIEW vwProductSkuCount
GO
CREATE VIEW [dbo].[vwProductSkuCount]
AS 
SELECT ProductId,
	COUNT(ProductId) AS SkuCount 
FROM PRProductSKU 
GROUP BY ProductId
GO

IF(OBJECT_ID('vwProductSKUDefaultAttributeValue') IS NOT NULL)
	DROP VIEW vwProductSKUDefaultAttributeValue
GO

CREATE VIEW [dbo].[vwProductSKUDefaultAttributeValue]
AS 
select P.Id ProductId, PTA.Id,PA.Id ProductAttributeId,'00000000-0000-0000-0000-000000000000' SKUId,PA.AttributeId,PTA.AttributeValueId AttributeEnumId,NULL Value,P.CreatedDate,P.CreatedBy,P.ModifiedDate,P.ModifiedBy,P.Status 
from PRProductAttribute PA
INNER JOIN PRProduct P ON PA.ProductId=P.Id
LEFT JOIN PRProductTypeAttributeValue PTA ON PA.AttributeId=PTA.AttributeId AND P.ProductTypeID=PTA.ProductTypeId
WHERE PA.IsSKULevel=1

GO

IF(OBJECT_ID('vwOrderShipmentItem') IS NOT NULL)
	DROP VIEW vwOrderShipmentItem
GO
CREATE VIEW [dbo].[vwOrderShipmentItem]
AS 
SELECT SI.Id,
	SI.OrderShipmentId,
	SI.OrderShipmentContainerId,
	SI.OrderItemId,
	SI.Quantity,
	SI.ExternalReferenceNumber,
	S.SKU,
	S.Title AS SKUTitle,
	P.Title AS ProductTitle,
	SI.CreateDate AS CreatedDate,
	SI.ModifiedDate
FROM FFOrderShipmentItem SI
	JOIN OROrderItem OI ON OI.Id = SI.OrderItemId
	JOIN PRProductSKU S ON S.Id = OI.ProductSKUId
	JOIN PRProduct P ON P.Id = S.ProductId
GO
PRINT 'Creating Function GetSQLDataType'
GO
IF (OBJECT_ID('GetSQLDataType') IS NOT NULL)
	DROP FUNCTION GetSQLDataType
GO
CREATE FUNCTION GetSQLDataType(@Type varchar(100))
Returns varchar(100)
AS 
BEGIN 
	Declare @ReturnType varchar(100)
	Select @ReturnType  =
		Case @Type
			When  'System.String' then 'nvarchar(max)' 
			When  'System.Boolean' then 'bit' 
			When  'System.Double' then 'float' 
			When  'System.Int32' then 'int' 
			When  'System.DateTime' then 'DateTime' 
			Else 'nvarchar(4000)' 
			END

	Return @ReturnType
END
GO
PRINT 'Creating Function OrderShipping_IsShippingInProgress'
GO
IF (OBJECT_ID('OrderShipping_IsShippingInProgress') IS NOT NULL)
	DROP FUNCTION OrderShipping_IsShippingInProgress
GO
CREATE FUNCTION [dbo].[OrderShipping_IsShippingInProgress]
(
	@OrderShippingId uniqueidentifier
)
RETURNS BIT
AS
BEGIN
	DECLARE @ShippingInProgressItems int, @CancelInProgressItems int, @IsShippingInProgress bit

	SELECT @ShippingInProgressItems = SUM(SI.Quantity) FROM FFOrderShipment S
		JOIN FFOrderShipmentItem SI ON SI.OrderShipmentId = S.Id
	WHERE OrderShippingId = @OrderShippingId
		AND S.ShipmentStatus IN (3, 4, 5, 6)

	SELECT @CancelInProgressItems = SUM(SI.Quantity) FROM FFOrderShipment S
		JOIN FFOrderShipmentItem SI ON SI.OrderShipmentId = S.Id
	WHERE OrderShippingId = @OrderShippingId
		AND S.ShipmentStatus IN (9)

	SET @IsShippingInProgress = 0
	IF (ISNULL(@ShippingInProgressItems, 0) > 0 AND ISNULL(@ShippingInProgressItems, 0) > ISNULL(@CancelInProgressItems, 0))
		SET @IsShippingInProgress = 1

	RETURN @IsShippingInProgress
END
GO
PRINT 'Creating Function OrderShipping_IsCancelInProgress'
GO
IF (OBJECT_ID('OrderShipping_IsCancelInProgress') IS NOT NULL)
	DROP FUNCTION OrderShipping_IsCancelInProgress
GO
CREATE FUNCTION [dbo].[OrderShipping_IsCancelInProgress]
(
	@OrderShippingId uniqueidentifier
)
RETURNS BIT
AS
BEGIN
	DECLARE @CancelInProgressItems int, @IsCancelInProgress bit

	SELECT @CancelInProgressItems = SUM(SI.Quantity) FROM FFOrderShipment S
		JOIN FFOrderShipmentItem SI ON SI.OrderShipmentId = S.Id
	WHERE OrderShippingId = @OrderShippingId
		AND S.ShipmentStatus IN (9)

	SET @IsCancelInProgress = 0
	IF (ISNULL(@CancelInProgressItems, 0) > 0)
		SET @IsCancelInProgress = 1

	RETURN @IsCancelInProgress
END
GO
PRINT 'Modify view vwOrderShipping'
GO
IF(OBJECT_ID('vwOrderShippingList') IS NOT NULL)
	DROP VIEW vwOrderShippingList
GO
IF(OBJECT_ID('vwOrderShipping') IS NOT NULL)
	DROP VIEW vwOrderShipping
GO
CREATE View [dbo].[vwOrderShipping]
AS
SELECT OS.[Id],	
	OS.[OrderId],
	OS.[ShippingOptionId],
	OS.[OrderShippingAddressId],
	OS.[ShipOnDate],
	OS.[ShipDeliveryDate],
	OS.[TaxPercentage],
	OS.[ShippingTotal],
	OS.[Sequence],
	OS.[IsManualTotal],
	OS.[Greeting],
	OS.[Status],
	OS.[CreatedBy],
	OS.[CreatedDate],
	OS.[ModifiedBy],
	OS.[ModifiedDate],
	OS.[Tax],
	OS.[SubTotal],
	OS.[TotalDiscount],
	OS.[ShippingCharge],
	OS.[ShippingDiscount],
	OS.[ShipmentHash],
	OS.[IsBackOrder],
	OS.[TaxableTotal],
	OS.[IsHandlingIncluded],
	OS.[ShippingChargeTax],
	OS.[CustomStatus],
	SA.ParentAddressId,
	SA.AddressId,
	FN.UserFullName AS CreatedByFullName,
	LN.UserFullName AS ModifiedByFullName,
	dbo.OrderShipping_IsShippingInProgress(OS.Id) AS IsShippingInProgress,
	dbo.OrderShipping_IsCancelInProgress(OS.Id) AS IsCancelInProgress
FROM OROrderShipping OS
	LEFT JOIN OROrderShippingAddress SA ON OS.OrderShippingAddressId = SA.Id
	LEFT JOIN VW_UserFullName FN ON FN.UserId = OS.CreatedBy
	LEFT JOIN VW_UserFullName LN ON LN.UserId = OS.ModifiedBy
GO
PRINT 'Modify view vwOrderItemAttribute'
GO
IF(OBJECT_ID('vwOrderItemAttribute') IS NOT NULL)
	DROP VIEW vwOrderItemAttribute
GO
CREATE VIEW [dbo].[vwOrderItemAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.OrderItemId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 9
	LEFT JOIN OROrderItemAttributeValue V ON A.Id = V.AttributeId

GO
PRINT 'Modify view vwPageAttribute'
GO
IF(OBJECT_ID('vwPageAttribute') IS NOT NULL)
	DROP VIEW vwPageAttribute
GO
CREATE VIEW [dbo].[vwPageAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 2
	LEFT JOIN vwPageAttributeValue V ON A.Id = V.AttributeId

GO
PRINT 'Modify view vwProductAttribute'
GO
IF(OBJECT_ID('vwProductAttribute') IS NOT NULL)
	DROP VIEW vwProductAttribute
GO
CREATE VIEW [dbo].[vwProductAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ProductId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 6
	LEFT JOIN PRProductAttribute V ON A.Id = V.AttributeId AND V.IsSKULevel = 0
GO
PRINT 'Modify view vwProductTypeProductAttribute'
GO
IF(OBJECT_ID('vwProductTypeProductAttribute') IS NOT NULL)
	DROP VIEW vwProductTypeProductAttribute
GO
CREATE VIEW [dbo].[vwProductTypeProductAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ProductTypeId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 5
	LEFT JOIN PRProductTypeAttribute V ON A.Id = V.AttributeId AND V.IsSKULevel = 0

GO
PRINT 'Modify view vwProductTypeSkuAttribute'
GO
IF(OBJECT_ID('vwProductTypeSkuAttribute') IS NOT NULL)
	DROP VIEW vwProductTypeSkuAttribute
GO
CREATE VIEW [dbo].[vwProductTypeSkuAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ProductTypeId AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 10
	LEFT JOIN PRProductTypeAttribute V ON A.Id = V.AttributeId AND V.IsSKULevel = 1

GO
PRINT 'Modify view vwSiteAttribute'
GO
IF(OBJECT_ID('vwSiteAttribute') IS NOT NULL)
	DROP VIEW vwSiteAttribute
GO
CREATE VIEW [dbo].[vwSiteAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	V.ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 1
	LEFT JOIN vwSiteAttributeValue V ON A.Id = V.AttributeId

GO
PRINT 'Modify view vwSkuAttribute'
GO
IF(OBJECT_ID('vwSkuAttribute') IS NOT NULL)
	DROP VIEW vwSkuAttribute
GO
CREATE VIEW [dbo].[vwSkuAttribute]
AS 
SELECT		
	A.Id, 
	A.AttributeDataTypeTitle, 
	A.Title, 
	A.IsSystem,
	A.IsMultiValued,
	A.IsFaceted,
	A.IsEnum,
	C.CategoryId,
	S.Id AS ObjectId,
	A.SiteId,
	A.IsSearchable,
	A.IsDisplayed
FROM vwAttribute AS A
	LEFT JOIN ATAttributeCategoryItem C ON A.Id = C.AttributeId AND C.CategoryId = 7
	LEFT JOIN PRProductAttribute V ON A.Id = V.AttributeId AND V.IsSKULevel = 1
	LEFT JOIN PRProductSku S ON S.ProductId = V.ProductId


GO
PRINT 'Modify view vwAutoPriceSetObject'
GO

IF(OBJECT_ID('vwAutoPriceSetObject') IS NOT NULL)
	DROP VIEW vwAutoPriceSetObject
GO
CREATE VIEW [dbo].[vwAutoPriceSetObject]
AS 
SELECT 	
	PPT.Id,
	PPT.PriceSetId,
	PPT.ProductTypeId AS ObjectId,
	PT.Title AS ObjectTitle,
	204 AS ObjectType
FROM PSPriceSetProductType PPT
	JOIN PRProductType PT ON PPT.ProductTypeId = PT.Id

UNION

SELECT 	
	PS.Id,
	PS.PriceSetId,
	PS.SKUId AS ObjectId,
	S.Title + ' (' + S.SKU + ')' AS ObjectTitle,
	206 AS ObjectType
FROM PSPriceSetSKU PS
	JOIN PRProductSKU S ON PS.SKUId = S.Id
	
GO
PRINT 'Modify view vwManualPriceSetSku'
GO

IF(OBJECT_ID('vwManualPriceSetSku') IS NOT NULL)
	DROP VIEW vwManualPriceSetSku
GO
CREATE VIEW [dbo].[vwManualPriceSetSku]
AS 
SELECT 	
	M.Id, 
	M.SKUId,
	M.PriceSetId , 
	M.MinQuantity, 
	M.MaxQuantity, 
	M.Price,
	M.CreatedDate,
	M.CreatedBy,
	CFN.UserFullName AS CreatedByFullName,
	M.ModifiedDate,
	M.ModifiedBy,
	MFN.UserFullName AS ModifiedByFullName,
	P.Title AS ProductName,
	S.SKU,
	S.ListPrice
FROM PSPriceSetManualSKU M
	INNER JOIN PRProductSKU S ON M.SKUId = S.Id
	INNER JOIN PRPRoduct P ON P.Id = S.ProductId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = M.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = M.ModifiedBy
		
GO


PRINT 'Modify view vwProductImageSku'
GO
IF(OBJECT_ID('vwProductImageSku') IS NOT NULL)
	DROP VIEW vwProductImageSku
GO
CREATE VIEW [dbo].[vwProductImageSku]
AS 
with CTE as
(
SELECT Id, Title, SiteId
FROM PRProductSKU

union

SELECT Id, Title, SiteId
FROM PRProductSKUVariantCache
)

select C.Id, C.Title, PS.SKU, PS.Status, C.SiteId, OI.ImageId
from CTE C join CLObjectImage OI ON C.Id = OI.ObjectId
           join PRProductSKU PS ON C.Id = PS.Id
GO

PRINT 'Modify view vwFacet'
GO
IF(OBJECT_ID('vwFacet') IS NOT NULL)
	DROP VIEW vwFacet
GO
CREATE VIEW [dbo].[vwFacet]
AS
select isnull(DT.Type,A.AttributeDataType) DataType,A.Title AttributeName, F.* from ATFacet F
INNER JOIN ATAttribute A ON F.AttributeID = A.Id
LEFT JOIN ATAttributeDataType DT ON A.AttributeDataTypeId = DT.Id

GO


PRINT 'Modify view vwNavFacet'
GO
IF(OBJECT_ID('vwNavFacet') IS NOT NULL)
	DROP VIEW vwNavFacet
GO
CREATE VIEW vwNavFacet
AS
Select N.NavNodeId NavId, N.Sequence, F.* from vwFacet F
INNER JOIN NVNavNodeFacet N ON F.Id = N.FacetId
GO

PRINT 'Modify view vwFacetRange'
GO
IF(OBJECT_ID('vwFacetRange') IS NOT NULL)
	DROP VIEW vwFacetRange
GO
CREATE View [dbo].[vwFacetRange]
AS
select R.*,RV.[From],RV.[To] from ATFacetRange R
INNER JOIN
(
	SELECT FacetRangeId, CAST([From] AS nvarchar(50)) [From], CAST([To] AS nvarchar(50)) [To] FROM ATFacetRangeDate
	UNION ALL 
	SELECT FacetRangeId, CAST([From] AS nvarchar(50)) [From], CAST([To] AS nvarchar(50)) [To] FROM ATFacetRangeDecimal
	UNION ALL 
	SELECT FacetRangeId, CAST([From] AS nvarchar(50)) [From], CAST([To] AS nvarchar(50)) [To] FROM ATFacetRangeInt
	UNION ALL 
	SELECT FacetRangeId, [From], [To] FROM ATFacetRangeString
) RV ON RV.FacetRangeID = R.Id

GO

PRINT 'Modify view vwCustomer'
GO
IF(OBJECT_ID('vwCustomer') IS NOT NULL)
	DROP VIEW vwCustomer
GO
CREATE VIEW [dbo].[vwCustomer] 
AS         
SELECT	CUP.Id, U.FirstName, U.LastName, U.MiddleName, U.UserName, NULL AS [Password], M.Email, U.EmailNotification, U.CompanyName, U.BirthDate, U.Gender, U.LeadScore,
		CUP.IsBadCustomer, CUP.IsActive, M.IsApproved, M.IsLockedOut, CUP.IsOnMailingList, CUP.[Status], CUP.CSRSecurityQuestion, CUP.CSRSecurityPassword, U.HomePhone,
		U.MobilePhone, U.OtherPhone, U.ImageId, CUP.AccountNumber, CUP.IsExpressCustomer, CUP.ExternalId, CUP.ExternalProfileId,
		(
			SELECT	COUNT(*)
			FROM	CSCustomerLogin
			WHERE	CustomerId = CUP.Id
		) + 1 AS NumberOfLoginAccounts,
		CASE WHEN EXISTS
		(
			SELECT	Id
			FROM	PTLineOfCreditInfo
			WHERE	CustomerId =  CUP.Id AND
					[Status] <> dbo.GetDeleteStatus()
		) THEN 1 ELSE 0 END AS HasLOC,
		COALESCE(CO.TotalOrders, 0) AS TotalOrders, COALESCE(CO.TotalSpend, 0) AS TotalSpend, CO.LastPurchase,
		A.Id AS AddressId, A.AddressLine1, A.AddressLine2, A.AddressLine3, A.City, A.StateId, ST.[State], ST.StateCode,
		A.CountryId, C.CountryName, C.CountryCode, A.Zip, A.County, A.Phone,
		U.ExpiryDate AS ExpirationDate, U.LastActivityDate,
		CASE WHEN EXISTS (
			SELECT	*
			FROM	USSiteUser
			WHERE	UserId = U.Id AND
					IsSystemUser = 1
		) THEN 1 ELSE 0 END AS IsSystemUser,
		U.CreatedDate,  U.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		ISNULL(U.ModifiedDate, U.CreatedDate) AS ModifiedDate, ISNULL(U.ModifiedBy, U.CreatedBy) AS ModifiedBy, MFN.UserFullName AS ModifiedByFullName
FROM	USCommerceUserProfile AS CUP
        INNER JOIN USUser AS U ON U.Id = CUP.Id
		INNER JOIN USMembership AS M ON M.UserId = U.Id
		LEFT JOIN (
			SELECT		CustomerId, COUNT(*) AS TotalOrders, SUM(GrandTotal) AS TotalSpend, MAX(OrderDate) AS LastPurchase
			FROM		OROrder
			GROUP BY	CustomerId
		) AS CO ON CO.CustomerId = U.Id
		LEFT JOIN USUserShippingAddress AS USA ON USA.UserId = U.Id AND USA.IsPrimary = 1
		LEFT JOIN GLAddress AS A ON A.Id = USA.AddressId
		LEFT JOIN GLState AS ST ON ST.Id = A.StateId
		LEFT JOIN GLCountry AS C ON C.Id = A.CountryId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = U.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = U.ModifiedBy
WHERE	U.[Status] !=3
GO
GO
IF(OBJECT_ID('vwDistributionSite') IS NOT NULL)
	DROP VIEW vwDistributionSite
GO
CREATE VIEW [dbo].[vwDistributionSite]
AS

WITH CTE AS
(	
	-- Specific site only
	SELECT
		D.ObjectId,
		T.TargetId AS SiteId
	FROM GLDistribution D
		JOIN GLDistributionTarget T ON D.Id = T.DistributionId
	WHERE T.TargetTypeId = 2

	UNION ALL

	-- Site List
	SELECT
		D.ObjectId,
		S.SiteId
	FROM GLDistribution D 
		JOIN GLDistributionTarget T ON D.Id = T.DistributionId
		JOIN SISiteListSite S ON S.SiteListId = T.TargetId
	WHERE T.TargetTypeId = 1

	UNION ALL

	-- Child Sites
	SELECT
		D.ObjectId,
		CS.Id
	FROM GLDistribution D 
		JOIN GLDistributionTarget T ON D.Id = T.DistributionId
		JOIN SISite PS ON PS.Id = T.TargetId
		JOIN SISite CS ON PS.LftValue < CS.LftValue AND PS.RgtValue > CS.RgtValue
	WHERE T.TargetTypeId = 3
		AND CS.Status = 1
)

SELECT DISTINCT * FROM CTE
GO
PRINT 'Modify stored procedure ContactListDto_UpdatePriority'
GO
IF(OBJECT_ID('ContactListDto_UpdatePriority') IS NOT NULL)
	DROP PROCEDURE ContactListDto_UpdatePriority
GO
CREATE PROCEDURE [dbo].[ContactListDto_UpdatePriority]  
(  
	@Sequence	TYSequence READONLY
)  
AS 
BEGIN
	UPDATE T Set T.Priority = P.Sequence
	FROM TADistributionLists T
	INNER JOIN @Sequence P ON P.Id = T.Id
END
GO
PRINT 'Modify stored procedure CouponDto_UpdatePriority'
GO
IF(OBJECT_ID('CouponDto_UpdatePriority') IS NOT NULL)
	DROP PROCEDURE CouponDto_UpdatePriority
GO
CREATE PROCEDURE [dbo].[CouponDto_UpdatePriority]  
(  
	@Sequence	TYSequence READONLY
)  
AS 
BEGIN
	UPDATE C Set C.AppliedSequence = P.Sequence
	FROM CPCoupon C
	INNER JOIN @Sequence P ON P.Id = C.Id
END
GO
PRINT 'Modify stored procedure Payment_Save'
GO
IF(OBJECT_ID('Payment_Save') IS NOT NULL)
	DROP PROCEDURE Payment_Save
GO
CREATE PROCEDURE [dbo].[Payment_Save]
(
      @Id UNIQUEIDENTIFIER OUTPUT ,
      @PaymentInfoId UNIQUEIDENTIFIER OUTPUT ,
      @OrderId UNIQUEIDENTIFIER ,
      @PaymentTypeId INT ,
      @PaymentStatusId INT = 1 ,
      @CardTypeId UNIQUEIDENTIFIER ,
      @CardNumber VARCHAR(MAX) ,
      @ExpirationMonth INT ,
      @ExpirationYear INT ,
      @NameOnCard NVARCHAR(256) ,
      @LastName NVARCHAR(256) = NULL ,
      @AuthCode NVARCHAR(256) ,
      @Description NVARCHAR(MAX) ,
      @AddressLine1 NVARCHAR(256) ,
      @AddressLine2 NVARCHAR(256) ,
      @City NVARCHAR(256) ,
      @StateId UNIQUEIDENTIFIER ,
      @StateName NVARCHAR(255) ,
      @CountryId UNIQUEIDENTIFIER ,
      @Zip NVARCHAR(50) ,
      @Phone NVARCHAR(50) ,
      @Amount MONEY ,
      @CustomerId UNIQUEIDENTIFIER ,
      @UpdateCustomerPayment BIT ,
      @SavedCardId UNIQUEIDENTIFIER ,
      @ModifiedBy UNIQUEIDENTIFIER ,
      @OrderPaymentId UNIQUEIDENTIFIER OUTPUT ,
      @IsRefund TINYINT = 0 ,
      @ExternalProfileId NVARCHAR(255)
)
AS 
BEGIN      
	DECLARE @CreatedDate DATETIME, @ModifiedDate DATETIME, @AddressId UNIQUEIDENTIFIER
    DECLARE @error INT
    DECLARE @stmt VARCHAR(256)	
    DECLARE @sequence INT
    
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID()) 
    BEGIN
        SET @Id = NEWID()  
        SET @PaymentInfoId = NEWID()
        SET @CreatedDate = GETUTCDATE()
        SET @AddressId = NEWID()

        INSERT  INTO PTPayment
        ( 
			Id ,
            PaymentTypeId ,
            PaymentStatusId ,
            CustomerId ,
            Amount ,
            Description ,
            CreatedDate ,
            CreatedBy ,
            ModifiedDate ,
            ModifiedBy ,
            AuthCode ,
            Status ,
            IsRefund
	    )
        VALUES  
		( 
			@Id,
            @PaymentTypeId ,
            @PaymentStatusId ,
            @CustomerId ,
            @Amount ,
            @Description ,
            @CreatedDate ,
            @ModifiedBy ,
            @CreatedDate ,
            @ModifiedBy ,
            @AuthCode ,
            dbo.GetActiveStatus() ,
            @IsRefund
        )

        OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
        OPEN SYMMETRIC KEY [key_DataShare] 
		DECRYPTION BY CERTIFICATE cert_keyProtection ;

        INSERT  INTO PTPaymentInfo
        ( 
			Id ,
            CardType ,
            Number ,
            ExpirationMonth ,
            ExpirationYear ,
            NameOnCard ,
            CreatedBy ,
            CreatedDate ,
            ModifiedBy ,
            ModifiedDate ,
            Status
	    )
        VALUES  
		( 
			@PaymentInfoId ,
            @CardTypeId ,
            ENCRYPTBYKEY(KEY_GUID('key_DataShare'), @CardNumber) ,
            ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
            CAST(@ExpirationMonth AS VARCHAR(2))) ,
            ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
            CAST(@ExpirationYear AS VARCHAR(4))) ,
            @NameOnCard ,
            @ModifiedBy ,
            @CreatedDate ,
            @ModifiedBy ,
            @CreatedDate ,
            dbo.GetActiveStatus()
        )

        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            CLOSE SYMMETRIC KEY [key_DataShare] ;
            CLOSE MASTER KEY
		
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END

        INSERT  INTO GLAddress
        ( 
			Id ,
            FirstName ,
            LastName ,
            AddressType ,
            AddressLine1 ,
            AddressLine2 ,
            City ,
            StateId ,
            StateName ,
            CountryId ,
            Zip ,
            Phone ,
            CreatedDate ,
            CreatedBy ,
            ModifiedDate ,
            ModifiedBy ,
            Status
	    )
        VALUES  
		( 
			@AddressId ,
            @NameOnCard ,
            @LastName ,
            0 ,
            @AddressLine1 ,
            @AddressLine2 ,
            @City ,
            @StateId ,
            @StateName ,
            @CountryId ,
            @Zip ,
            @Phone ,
            @CreatedDate ,
            @ModifiedBy ,
            @CreatedDate ,
            @ModifiedBy ,
            dbo.GetActiveStatus()
        )
                
		SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END
	
        INSERT  INTO PTPaymentCreditCard
        ( 
			Id ,
            PaymentInfoId ,
            ParentPaymentInfoId ,
            PaymentId ,
            BillingAddressId ,
            Phone ,
            ExternalProfileId
	    )
        VALUES  
		( 
			NEWID() ,
            @PaymentInfoId ,
            @SavedCardId ,
            @Id ,
            @AddressId ,
            @Phone ,
            @ExternalProfileId
	    )
	
        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END

        SELECT  @sequence = MAX(ISNULL(Sequence, 0)) + 1
        FROM    PTOrderPayment
        WHERE   OrderId = @OrderId


        SET @OrderPaymentId = NEWID()
        INSERT  INTO PTOrderPayment
        ( 
			Id ,
            OrderId ,
            PaymentId ,
            Amount ,
            Sequence
        )
        VALUES  
		( 
			@OrderPaymentId ,
            @OrderId ,
            @Id ,
            @Amount ,
            @sequence
        )

        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END

    END  
    ELSE 
    BEGIN 
        SET @CreatedDate = GETUTCDATE()
        SET @AddressId = NEWID()

        UPDATE  PTPayment
        SET     PaymentTypeId = @PaymentTypeId ,
                PaymentStatusId = @PaymentStatusId ,
                CustomerId = @CustomerId ,
                Amount = @Amount ,
                Description = @Description ,
                ModifiedBy = @ModifiedBy ,
                ModifiedDate = @CreatedDate ,
                AuthCode = @AuthCode ,
                Status = dbo.GetActiveStatus() ,
                IsRefund = @IsRefund
        WHERE   Id = @Id

        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END

        OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
        OPEN SYMMETRIC KEY [key_DataShare] 
		DECRYPTION BY CERTIFICATE cert_keyProtection ;

        UPDATE  PTPaymentInfo
        SET     CardType = @CardTypeId ,
                Number = ENCRYPTBYKEY(KEY_GUID('key_DataShare'), @CardNumber) ,
                ExpirationMonth = ENCRYPTBYKEY(KEY_GUID('key_DataShare'), CAST(@ExpirationMonth AS VARCHAR(2))) ,
                ExpirationYear = ENCRYPTBYKEY(KEY_GUID('key_DataShare'), CAST(@ExpirationYear AS VARCHAR(4))) ,
                NameOnCard = @NameOnCard ,
                ModifiedBy = @ModifiedBy ,
                ModifiedDate = GETUTCDATE() ,
                Status = dbo.GetActiveStatus()
        WHERE   Id = @PaymentInfoId
				
        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            CLOSE SYMMETRIC KEY [key_DataShare] ;
            CLOSE MASTER KEY
			
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END

		UPDATE  dbo.PTPaymentCreditCard
        SET     ExternalProfileId = @ExternalProfileId
        WHERE   PaymentInfoId = @PaymentInfoId

        SELECT  @AddressId = BillingAddressId
        FROM    PTPaymentCreditCard
        WHERE   PaymentId = @Id
		
		
        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END

        UPDATE  GLAddress
        SET     FirstName = @NameOnCard ,
                LastName = @LastName ,
                AddressType = 0 ,
                AddressLine1 = @AddressLine1 ,
                AddressLine2 = @AddressLine2 ,
                City = @City ,
                StateId = @StateId ,
                StateName = @StateName ,
                CountryId = @CountryId ,
                Zip = @Zip ,
                Phone = @Phone ,
                ModifiedDate = GETUTCDATE() ,
                ModifiedBy = @ModifiedBy ,
                Status = dbo.GetActiveStatus()
        WHERE   Id = @AddressId
		
        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END	


        UPDATE  PTOrderPayment
        SET     Amount = @Amount
        WHERE   Id = @OrderPaymentId
		
        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
		
        END
    END 

    IF @UpdateCustomerPayment = 1 
    BEGIN 
		DECLARE @NewAddressId UNIQUEIDENTIFIER
	
        SET @NewAddressId = NEWID()
        INSERT  INTO GLAddress
        ( 
			Id ,
            FirstName ,
            LastName ,
            AddressType ,
            AddressLine1 ,
            AddressLine2 ,
            City ,
            StateId ,
            StateName ,
            CountryId ,
            Zip ,
            Phone ,
            CreatedDate ,
            CreatedBy ,
            ModifiedDate ,
            ModifiedBy ,
            Status
	    )
        SELECT  @NewAddressId ,
            FirstName ,
            LastName ,
            AddressType ,
            AddressLine1 ,
            AddressLine2 ,
            City ,
            StateId ,
            StateName ,
            CountryId ,
            Zip ,
            Phone ,
            CreatedDate ,
            CreatedBy ,
            ModifiedDate ,
            ModifiedBy ,
            Status
		FROM    GLAddress
		WHERE   Id = @AddressId

        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END
                
		DECLARE @ExistingPaymentInfo UNIQUEIDENTIFIER
        
		SELECT  @ExistingPaymentInfo = PaymentInfoId
        FROM    USUserPaymentInfo
        WHERE   Id = @SavedCardId
                
		DECLARE @NewPaymentInfoId UNIQUEIDENTIFIER
        SET @NewPaymentInfoId = NEWID()
        IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000' 
        BEGIN
            UPDATE  PTPaymentInfo
            SET     CardType = @CardTypeId ,
                    Number = ENCRYPTBYKEY(KEY_GUID('key_DataShare'), @CardNumber) ,
                    ExpirationMonth = ENCRYPTBYKEY(KEY_GUID('key_DataShare'), CAST(@ExpirationMonth AS VARCHAR(2))) ,
                    ExpirationYear = ENCRYPTBYKEY(KEY_GUID('key_DataShare'), CAST(@ExpirationYear AS VARCHAR(4))) ,
                    NameOnCard = @NameOnCard ,
                    ModifiedBy = @ModifiedBy ,
                    ModifiedDate = @CreatedDate
            WHERE   Id = @ExistingPaymentInfo
        END
        ELSE 
        BEGIN
			INSERT  INTO PTPaymentInfo
			( 
				Id ,
				CardType ,
				Number ,
				ExpirationMonth ,
				ExpirationYear ,
				NameOnCard ,
				CreatedBy ,
				CreatedDate ,
				Status ,
				ModifiedBy ,
				ModifiedDate
			)
			VALUES  
			( 
				@NewPaymentInfoId ,
				@CardTypeId ,
				ENCRYPTBYKEY(KEY_GUID('key_DataShare'), @CardNumber) ,
				ENCRYPTBYKEY(KEY_GUID('key_DataShare'), CAST(@ExpirationMonth AS VARCHAR(2))) ,
				ENCRYPTBYKEY(KEY_GUID('key_DataShare'), CAST(@ExpirationYear AS VARCHAR(4))) ,
				@NameOnCard ,
				@ModifiedBy ,
				@CreatedDate ,
				1 ,
				@ModifiedBy ,
				@CreatedDate
			)
        END
	
        SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END
                
		IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000' 
        BEGIN
            UPDATE  USUserPaymentInfo
            SET     BillingAddressId = @NewAddressId ,
                    ExternalProfileId = @ExternalProfileId
            WHERE   PaymentInfoId = @ExistingPaymentInfo
                    AND UserId = @CustomerId
        END 
        ELSE 
        BEGIN 
            INSERT  INTO USUserPaymentInfo
            ( 
				Id ,
                UserId ,
                PaymentInfoId ,
                BillingAddressId ,
                Sequence ,
                IsPrimary ,
                CreatedBy ,
                CreatedDate ,
                Status ,
                ModifiedDate ,
                ModifiedBy ,
                ExternalProfileId
		    )
            VALUES  
			( 
				NEWID() ,
                @CustomerId ,
                @NewPaymentInfoId ,
                @AddressId ,
                1 ,
                0 ,
                @ModifiedBy ,
                @CreatedDate ,
                1 ,
                @CreatedDate ,
                @ModifiedBy ,
                @ExternalProfileId		
		    )
        END
        
		SELECT  @error = @@error
        IF @error <> 0 
        BEGIN
            RAISERROR('DBERROR||%s',16,1,@stmt)
            RETURN dbo.GetDataBaseErrorCode()	
        END
    END --4

    UPDATE  OROrder
    SET     PaymentTotal = dbo.Order_GetPaymentTotal(@OrderId) ,
            RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
    WHERE   Id = @OrderId
END
GO
PRINT 'Modify stored procedure CustomerPaymentInfoDto_Get'
GO
IF(OBJECT_ID('CustomerPaymentInfoDto_Get') IS NOT NULL)
	DROP PROCEDURE CustomerPaymentInfoDto_Get
GO
CREATE PROCEDURE [dbo].[CustomerPaymentInfoDto_Get]
(
	@Id					uniqueidentifier = null,
	@CustomerId			uniqueidentifier = null,
	@SiteId				uniqueidentifier,
	@PaymentType        int = null
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	WITH CTE AS
	(
	SELECT UP.Id,  
		UP.UserId AS CustomerId,  
		1 AS PaymentType,
		UP.NickName,
		UP.IsPrimary,
		UP.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		CCT.Title AS CreditCardType,  
		P.NameOnCard AS CreditCardName,
		CONVERT(varchar(max), DecryptByKey(P.Number)) AS CreditCardNumber,  
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,  
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) AS CreditCardExpirationYear,  
		UP.ExternalProfileId AS CreditCardExternalProfileId,
		NULL AS GiftCardNumber,
		NULL AS GiftCardBalance,
		NULL AS AccountNumber,
		UP.CreatedDate, 
		UP.CreatedBy AS CreatedById,
		UP.ModifiedDate, 
		UP.ModifiedBy AS ModifiedById
	FROM PTPaymentInfo P
		LEFT JOIN USUserPaymentInfo UP ON P.Id = UP.PaymentInfoId  
		LEFT JOIN PTCreditCardType AS CCT ON CCT.Id = P.CardType
		LEFT JOIN GLAddress A ON A.Id = UP.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR UP.Id = @Id)
		AND (@CustomerId IS NULL OR UP.UserId = @CustomerId) 
		AND UP.[Status] = 1

	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		5 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		P.GiftCardNumber,
		P.BalanceAfterLastTransaction AS GiftCardBalance,
		NULL AS AccountNumber,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTGiftCardInfo P  
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 5 AND Status = 1 AND SiteId = @SiteId)
	
	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		2 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		U.AccountNumber,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTLineOfCreditInfo P  
		INNER JOIN USCommerceUserProfile U ON U.Id = P.CustomerId
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 2 AND Status = 1 AND SiteId = @SiteId)
	
	UNION ALL

	SELECT P.Id,  
		P.CustomerId,  
		4 AS PaymentType,
		NULL,
		NULL,
		P.BillingAddressId,
		A.AddressLine1 AS BillingAddressLine1,
		A.AddressLine2 AS BillingAddressLine2,
		A.AddressLine3 AS BillingAddressLine3,
		A.City AS BillingCity,
		A.StateId AS BillingStateId,
		A.Zip AS BillingZip,
		A.CountryId AS BillingCountryId,
		A.Phone AS BillingPhone,
		S.State AS BillingStateName,
		S.StateCode AS BillingStateCode,
		C.CountryName AS BillingCountryName,
		C.CountryCode AS BillingCountryCode,
		P.Id AS PaymentTypeInfoId,
		NULL,  
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		P.CreatedDate, 
		P.CreatedBy AS CreatedById,
		P.ModifiedDate,
		P.ModifiedBy AS ModifiedById
	FROM PTCODInfo P  
		LEFT JOIN GLAddress A ON A.Id = P.BillingAddressId
		LEFT JOIN GLState S ON S.Id = A.StateId
		LEFT JOIN GLCountry C ON C.Id = A.CountryId
	WHERE (@Id IS NULL OR P.Id = @Id)
		AND (@CustomerId IS NULL OR P.CustomerId = @CustomerId) 
		AND P.[Status] = 1
		AND EXISTS (SELECT 1 FROM PTPaymentTypeSite WHERE PaymentTypeId = 4 AND Status = 1 AND SiteId = @SiteId)
	)

	SELECT Id,  
		CustomerId,  
		PaymentType,
		NickName,
		IsPrimary,
		BillingAddressId,
		BillingAddressLine1,
		BillingAddressLine2,
		BillingAddressLine3,
		BillingCity,
		BillingStateId,
		BillingZip,
		BillingCountryId,
		BillingPhone,
		BillingStateName,
		BillingStateCode,
		BillingCountryName,
		BillingCountryCode,
		PaymentTypeInfoId,
		CreditCardType,  
		CreditCardName,
		CreditCardNumber,  
		CreditCardExpirationMonth,  
		CreditCardExpirationYear,  
		CreditCardExternalProfileId,
		GiftCardNumber,
		GiftCardBalance,
		AccountNumber,
		CreatedDate, 
		CreatedById,
		ModifiedDate, 
		ModifiedById
	FROM CTE
	WHERE PaymentType = ISNULL(@PaymentType, PaymentType)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO
PRINT 'Modify stored procedure CustomerPaymentInfo_Save'
GO
IF(OBJECT_ID('CustomerPaymentInfo_Save') IS NOT NULL)
	DROP PROCEDURE CustomerPaymentInfo_Save
GO
CREATE PROCEDURE [dbo].[CustomerPaymentInfo_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@Status				int=null,
	@CardType			uniqueidentifier,
    @Number				varchar(max),
	@ExpirationMonth	int,
    @ExpirationYear		int,
    @NameOnCard			varchar(255),
    @BillingAddressId	uniqueidentifier,
    @Sequence			int,
    @UserId				uniqueidentifier,
    @IsPrimary			bit,
	@ApplicationId		uniqueidentifier,
	@ExternalProfileId	nvarchar(255)=null,
	@Nickname			nvarchar(255)=null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@stmt	  varchar(256),
		@rowcount	int,
		@PaymentInfoId uniqueidentifier

Begin
--********************************************************************************
-- code
--********************************************************************************


	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	DECLARE @NewCustomerPayment BIT = CASE WHEN @Id IS NULL OR @Id = dbo.GetEmptyGUID() OR NOT EXISTS (SELECT Id FROM USUserPaymentInfo WHERE Id = @Id) THEN 1 ELSE 0 END

	IF @Id IS NULL OR @Id = dbo.GetEmptyGUID()
		SET @Id = NEWID()
       
    IF @IsPrimary = 1
	BEGIN
		UPDATE [dbo].[USUserPaymentInfo]
		SET IsPrimary = 0 WHERE UserId = @UserId
	END
	
	SET @ModifiedDate = GetUTCDate()
	
		-- open the symmetric key 
		OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
		OPEN SYMMETRIC KEY [key_DataShare] 
			DECRYPTION BY CERTIFICATE cert_keyProtection;

		IF @NewCustomerPayment = 1	-- new insert
		BEGIN
			SET @stmt = 'Payment Insert'
			Declare @pId uniqueidentifier
			SET @pId =NewId();

			INSERT INTO [dbo].[PTPaymentInfo]
			   ([Id]
			   ,[CardType]
			   ,[Number]
			   ,[ExpirationMonth]
			   ,[ExpirationYear]
			   ,[NameOnCard]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[Status]
			   ,[ModifiedBy]
			   ,[ModifiedDate])
		 VALUES
			   (@pId
			   ,@CardType
			   ,EncryptByKey(Key_Guid('key_DataShare'), @Number)
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS Varchar(2)))
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS varchar(4)))
			   ,@NameOnCard
			   ,@ModifiedBy
			   ,@ModifiedDate
			   ,@Status
			   ,@ModifiedBy
			   ,@ModifiedDate)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
			INSERT INTO [dbo].[USUserPaymentInfo]
			([Id]
			,[UserId]
			,[PaymentInfoId]
			,[BillingAddressId]
			,[Sequence]
			,[IsPrimary]
			,[CreatedBy]
			,[CreatedDate]
			,[Status]
			,[ModifiedDate]
			,[ModifiedBy]
			,[ExternalProfileId]
			,[Nickname])
			VALUES
			(@Id
			,@UserId
			,@pId
			,@BillingAddressId
			,@Sequence
			,@IsPrimary
			,@ModifiedBy
			,@ModifiedDate
			,@Status
			,@ModifiedDate
			,@ModifiedBy
			,@ExternalProfileId
			,@Nickname)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
        END
        ELSE			-- update
        BEGIN
			SET @stmt = 'Payment Update'
			
			Select @PaymentInfoId=PaymentInfoId  FROM [dbo].[USUserPaymentInfo]
			Where Id=@Id

			UPDATE [dbo].[USUserPaymentInfo] WITH (ROWLOCK)
			SET [PaymentInfoId] = @PaymentInfoId
			  ,[BillingAddressId] = @BillingAddressId
			  ,[Sequence] = @Sequence
			  ,[IsPrimary] = @IsPrimary
			  ,[Status] = @Status
			  ,[ModifiedDate] = @ModifiedDate
			  ,[ModifiedBy] =@ModifiedBy
			  ,[ExternalProfileId]=@ExternalProfileId
			  ,[Nickname]=@Nickname
			WHERE Id=@Id

			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
			  RAISERROR('DBERROR||%s',16,1,@stmt)
			  RETURN dbo.GetDataBaseErrorCode()
			END

			SET @rowcount= 0	

			UPDATE [dbo].[PTPaymentInfo]
			SET [CardType] = @CardType
				,[Number]=CASE WHEN (@Number is null or len(@Number)<=0) then Number else (EncryptByKey(Key_Guid('key_DataShare'), @Number )) end
				,[ExpirationMonth] = CASE WHEN (@ExpirationMonth is null or len(@ExpirationMonth)<=0) then ExpirationMonth else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS VARCHAR(2)) )) end
				,[ExpirationYear]= CASE WHEN (@ExpirationYear is null or len(@ExpirationMonth)<=0) then ExpirationYear else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS VARCHAR(4)) )) end
				,[NameOnCard] = @NameOnCard
				,[Status] = @Status
				,[ModifiedBy] = @ModifiedBy
				,[ModifiedDate] = @ModifiedDate
			WHERE Id=@PaymentInfoId 
					
			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
				RAISERROR('DBERROR||%s',16,1,@stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END


	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO
PRINT 'Modify stored procedure Payment_UpdateStatus'
GO
IF(OBJECT_ID('Payment_UpdateStatus') IS NOT NULL)
	DROP PROCEDURE Payment_UpdateStatus
GO
CREATE PROCEDURE [dbo].[Payment_UpdateStatus]
(
	@PaymentId				uniqueidentifier,
	@ProcessorTransactionId nvarchar(256) = null,
	@AuthCode				nvarchar(256) = null,
	@PaymentStatusId		int = null,
	@FailureDate			datetime,
	@FailureDescription		nvarchar(max) = null,
	@ClearDate				datetime = null,
	@Amount					money = null,
	@ModifiedBy				uniqueidentifier,
	@ApplicationId			uniqueidentifier,
	@IsRefund				tinyint = 0
)
AS
BEGIN
	DECLARE @ModifiedDate datetime, @PaymentTypeId int, @OrderId uniqueidentifier, @PrevPaymentStatusId int
	SET @ModifiedDate = GetUTCDate()

	SELECT TOP 1 @PrevPaymentStatusId = PaymentStatusId, @PaymentTypeId = PaymentTypeId FROM PTPayment WHERE Id = @PaymentId
	IF (@PaymentStatusId = 1 AND @PrevPaymentStatusId != 1)
	BEGIN 
		DECLARE @error int, @stmt varchar(256), @msg varchar(500)
		SET @msg ='PaymentStatus ' + cast(@PrevPaymentStatusId as varchar(2)) + ' cannot be changed to Entered'
		RAISERROR ('InvalidOperation||PaymentStatusId|%s' , 16, 1, @msg)

		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE PTPayment 
	SET ProcessorTransactionId = ISNULL(@ProcessorTransactionId, ProcessorTransactionId),
		AuthCode = ISNULL(@AuthCode, AuthCode),
		PaymentStatusId = ISNULL(@PaymentStatusId, PaymentStatusId),
		FailureDate = dbo.ConvertTimeToUtc(@FailureDate, @ApplicationId),
		FailureDescription = ISNULL(@FailureDescription, FailureDescription),
		ClearDate = ISNULL(dbo.ConvertTimeToUtc(@ClearDate, @ApplicationId), ClearDate),
		Amount = ISNULL(@Amount, Amount),
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @ModifiedDate,
		IsRefund = @IsRefund
	WHERE Id = @PaymentId	

	SELECT TOP 1 @OrderId = OrderId FROM PTOrderPayment WHERE PaymentId = @PaymentId

	UPDATE OROrder SET PaymentTotal = dbo.Order_GetPaymentTotal(@OrderId), 
		RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
	WHERE Id = @OrderId

	IF (@PaymentStatusId = 7 AND @PaymentTypeId = 5)
	BEGIN
		DECLARE @GiftCardInfoId uniqueidentifier
		SELECT @GiftCardInfoId = GiftCardId FROM PTPaymentGiftCard WHERE PaymentId = @PaymentId

		UPDATE PTGiftCardInfo SET BalanceAfterLastTransaction = BalanceAfterLastTransaction + @Amount 
		WHERE Id = @GiftCardInfoId
	END
END
GO
PRINT 'Modify stored procedure Payment_ByGiftCard_Save'
GO
IF(OBJECT_ID('Payment_ByGiftCard_Save') IS NOT NULL)
	DROP PROCEDURE Payment_ByGiftCard_Save
GO
CREATE PROCEDURE [dbo].[Payment_ByGiftCard_Save]  
(   
	@Id					uniqueidentifier output,
	@GiftCardId			uniqueidentifier output,	
	@OrderPaymentId		uniqueidentifier output,
	@GiftCardNumber		nvarchar(50),
	@Last4Digits		nvarchar(50),
	@BalanceAfterLastTransaction money,
	@OrderId			uniqueidentifier,
	@PaymentTypeId		int,
	@PaymentStatusId	int =1,
	@Amount				money,
	@CustomerId			uniqueidentifier,
	@Description		nvarchar(255),
	@ModifiedBy			uniqueidentifier  ,
	@BillingAddressId	uniqueidentifier,
	@IsRefund			tinyint = 0
)  
AS  
BEGIN       
	DECLARE @CreatedDate datetime, @EmptyGuid uniqueidentifier, @Sequence int
	SET @CreatedDate = GetUTCDate()
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF (@GiftCardId IS NULL OR @GiftCardId = @EmptyGuid)
	BEGIN
		IF EXISTS (SELECT AddressId FROM OROrderShippingAddress WHERE Id = @BillingAddressId)
			SELECT @BillingAddressId = AddressId FROM OROrderShippingAddress WHERE Id = @BillingAddressId

		SELECT @GiftCardId = NEWID()
		INSERT INTO PTGiftCardInfo
		(
			Id,
			CustomerId,
			GiftCardNumber,
			Last4Digits,
			BalanceAfterLastTransaction,
			BillingAddressId,
			Status,
			CreatedDate,
			CreatedBy,
			ModifiedDate,
			ModifiedBy
		)
		Values
		(
			@GiftCardId,
			@CustomerId,
			@GiftCardNumber,
			@Last4Digits,
			@BalanceAfterLastTransaction,
			@BillingAddressId,
			1,
			@CreatedDate,
			@ModifiedBy,
			@CreatedDate,
			@ModifiedBy
		)
	END

	IF (@Id IS NULL OR @Id = @EmptyGuid)  
	BEGIN  
		SET @Id = NEWID()  

		INSERT INTO PTPayment 
		(
			Id,
			PaymentTypeId,
			PaymentStatusId,
			CustomerId,
			Amount,
			Description,
			CreatedDate,
			CreatedBy,
			Status,
			IsRefund
		)
		VALUES
		(
			@Id,
			@PaymentTypeId,
			@PaymentStatusId,
			@CustomerId,
			@Amount,
			@Description,
			@CreatedDate,
			@ModifiedBy,
			dbo.GetActiveStatus(),
			@IsRefund
		)

		INSERT INTO PTPaymentGiftCard
		(
			Id,
			PaymentId,
			GiftCardId
		)
		VALUES
		(
			NEWID(),
			@Id,
			@GiftCardId
		)

		SELECT @Sequence = MAX(ISNULL(Sequence, 0)) + 1
		FROM PTOrderPayment
		WHERE OrderId = @OrderId

		SET @OrderPaymentId = NEWID()
		INSERT INTO PTOrderPayment (Id, OrderId, PaymentId, Amount, Sequence)
		VALUES (@OrderPaymentId, @OrderId, @Id, @Amount, @Sequence)
	END  
	ELSE
	BEGIN
		UPDATE PTPayment 
		SET PaymentTypeId = @PaymentTypeId, 
			PaymentStatusId = @PaymentStatusId,
			CustomerId = @CustomerId,
			Amount = @Amount,
			Description = @Description,
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @CreatedDate,	
			Status = dbo.GetActiveStatus(),
			IsRefund = @IsRefund
		WHERE Id = @Id

		UPDATE PTOrderPayment SET Amount = @Amount
		WHERE Id = @OrderPaymentId				
	END  

	UPDATE PTGiftCardInfo
	SET BalanceAfterLastTransaction = @BalanceAfterLastTransaction
	WHERE Id = @GiftCardId

	UPDATE OROrder 
	SET PaymentTotal = dbo.Order_GetPaymentTotal(@OrderId), 
		RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
	WHERE Id = @OrderId
END
GO
PRINT 'Modify stored procedure PaymentDto_Get'
GO
IF(OBJECT_ID('PaymentDto_Get') IS NOT NULL)
	DROP PROCEDURE PaymentDto_Get
GO
CREATE PROCEDURE [dbo].[PaymentDto_Get]
(		
	@Id						UNIQUEIDENTIFIER = NULL,
	@OrderId				UNIQUEIDENTIFIER = NULL,
	@CustomerId				UNIQUEIDENTIFIER = NULL,
	@ProcessorTransactionId	NVARCHAR(256) = NULL,
	@PaymentTypeId			INT = NULL,
	@PaymentStatusId		INT = NULL,
	@IsRefund				BIT = NULL,
	@StartDate				DATETIME = NULL,
	@EndDate				DATETIME = NULL,
	@PageNumber				INT = NULL,
	@PageSize				INT = NULL,
	@TotalRecords			INT = NULL OUTPUT
)
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	DECLARE	@StartRow	INT,
			@EndRow		INT

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END

	SELECT	P.Id, OP.Id AS OrderPaymentId, OP.OrderId, OP.[Sequence], P.PaymentTypeId AS PaymentType, P.PaymentStatusId AS PaymentStatus, P.ProcessorTransactionId,
			P.IsRefund, RE.Comments AS RefundReason, P.Amount, P.CapturedAmount, COALESCE(PR.AllocatedRefundAmount, 0) AS AllocatedRefundAmount,
			O.PurchaseOrderNumber, P.CustomerId, CASE WHEN LEN(LTRIM(C.LastName)) = 0 THEN ''  ELSE C.LastName + ', ' END + C.FirstName AS CustomerName,
			CCT.Title AS CreditCardType, CCI.NameOnCard AS CreditCardName,
			CONVERT(VARCHAR(MAX), DecryptByKey(CCI.Number)) AS CreditCardNumber,
			CAST(CONVERT(VARCHAR(2), DecryptByKey(CCI.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,
			CAST(CONVERT(VARCHAR(4), DecryptByKey(CCI.ExpirationYear)) AS INT) AS CreditCardExpirationYear,
			PCC.ExternalProfileId CreditCardExternalProfileId,
			GCI.GiftCardNumber AS GiftCardNumber, GCI.BalanceAfterLastTransaction AS GiftCardBalance, U.AccountNumber,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.BillingAddressId
				WHEN 2 THEN LOC.BillingAddressId
				WHEN 3 THEN PPI.BillingAddressId
				WHEN 4 THEN CODI.BillingAddressId
				WHEN 5 THEN GCI.BillingAddressId
			END AS BillingAddressId,
			CASE P.PaymentTypeId
				WHEN 1 THEN CCI.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentInfoId,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.Id
				WHEN 2 THEN LOC.Id
				WHEN 3 THEN PPI.Id
				WHEN 4 THEN CODI.Id
				WHEN 5 THEN GCI.Id
			END AS PaymentTypeInfoId,
			P.CreatedDate, P.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName, P.ModifiedDate, P.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName,
			ROW_NUMBER() OVER (ORDER BY OP.Sequence) AS RowNumber
	INTO	#OrderPayments
	FROM	PTOrderPayment AS OP
			INNER JOIN PTPayment AS P ON P.Id = OP.PaymentId
			INNER JOIN OROrder AS O ON O.Id = OP.OrderId
			INNER JOIN vwCustomer AS C ON C.Id = P.CustomerId
			LEFT JOIN PTRefundPayment AS RP ON RP.RefundOrderPaymentId = OP.Id
			LEFT JOIN ORRefund AS RE ON RE.Id = RP.RefundId
			LEFT JOIN (
				SELECT		RP.OrderPaymentId, SUM(RP.RefundAmount) AS AllocatedRefundAmount
				FROM		PTRefundPayment AS RP
							INNER JOIN PTOrderPayment AS ROP ON ROP.Id = RP.RefundOrderPaymentId
							INNER JOIN PTPayment AS P ON P.Id = ROP.PaymentId
				WHERE		P.PaymentStatusId NOT IN (7, 14)
				GROUP BY	RP.OrderPaymentId
			) AS PR ON PR.OrderPaymentId = OP.Id
			LEFT JOIN PTPaymentCreditCard AS PCC ON P.PaymentTypeId = 1 AND PCC.PaymentId = P.Id
			LEFT JOIN PTPaymentInfo AS CCI ON P.PaymentTypeId = 1 AND CCI.Id = PCC.PaymentInfoId
			LEFT JOIN PTCreditCardType AS CCT ON P.PaymentTypeId = 1 AND CCT.Id = CCI.CardType
			LEFT JOIN PTPaymentLineOfCredit AS PLOC ON P.PaymentTypeId = 2 AND PLOC.PaymentId = P.Id
			LEFT JOIN PTLineOfCreditInfo AS LOC ON P.PaymentTypeId = 2 AND LOC.Id = PLOC.LOCId
			LEFT JOIN USCommerceUserProfile U ON P.PaymentTypeId = 2 AND U.Id = LOC.CustomerId
			LEFT JOIN PTPaymentPaypal AS PPP ON P.PaymentTypeId = 3 AND PPP.PaymentId = P.Id
			LEFT JOIN PTPaypalInfo AS PPI ON P.PaymentTypeId = 3 AND PPI.Id = PPP.PaypalInfoId
			LEFT JOIN PTPaymentCOD AS PCOD ON P.PaymentTypeId = 4 AND PCOD.PaymentId = P.Id
			LEFT JOIN PTCODInfo AS CODI ON P.PaymentTypeId = 4 AND CODI.Id = PCOD.CODId
			LEFT JOIN PTPaymentGiftCard AS PGC ON P.PaymentTypeId = 5 AND PGC.PaymentId = P.Id
			LEFT JOIN PTGiftCardInfo AS GCI ON P.PaymentTypeId = 5 AND GCI.Id = PGC.GiftCardId
			LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy
			LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = P.ModifiedBy
	WHERE	(@Id IS NULL OR P.Id = @Id) AND
			(@OrderId IS NULL OR OP.OrderId = @OrderId) AND
			(@CustomerId IS NULL OR P.CustomerId = @CustomerId) AND
			(@ProcessorTransactionId IS NULL OR P.ProcessorTransactionId = @ProcessorTransactionId) AND
			(@PaymentTypeId IS NULL OR P.PaymentTypeId = @PaymentTypeId) AND
			(@PaymentStatusId IS NULL OR P.PaymentStatusId = @PaymentStatusId) AND
			(@IsRefund IS NULL OR P.IsRefund = @IsRefund) AND
			(@StartDate IS NULL OR P.CreatedDate >= @StartDate) AND
			(@EndDate IS NULL OR P.CreatedDate <= @EndDate)
	ORDER BY P.CreatedDate DESC
	OPTION (RECOMPILE)

	SELECT	*
	FROM	#OrderPayments
	WHERE	@StartRow IS NULL OR
			@EndRow IS NULL OR 
			RowNumber BETWEEN @StartRow AND @EndRow

	SET @TotalRecords = (
		SELECT	COUNT(Id)
		FROM	#OrderPayments
	)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO
PRINT 'Modify stored procedure CustomerDto_Save'
GO
IF(OBJECT_ID('CustomerDto_Save') IS NOT NULL)
	DROP PROCEDURE CustomerDto_Save
GO
CREATE PROCEDURE [dbo].[CustomerDto_Save]
(	
	@Id						UNIQUEIDENTIFIER,
	@SiteId					UNIQUEIDENTIFIER,
	@AccountNumber			NVARCHAR(255) = NULL,
	@CSRSecurityQuestion	NVARCHAR(MAX) = NULL,
	@CSRSecurityPassword	VARBINARY(MAX) = NULL,
	@ExternalId				NVARCHAR(255),
	@ExternalProfileId		NVARCHAR(255) = NULL,
	@IsExpressCustomer		BIT = NULL,
	@IsActive				BIT = NULL,
	@IsBadCustomer			BIT = NULL,
	@IsOnMailingList		BIT = NULL,
	@IsLockedOut			BIT = NULL
)

AS
BEGIN
	IF (NOT EXISTS(SELECT Id FROM USCommerceUserProfile WHERE Id = @Id))
	BEGIN
		INSERT INTO USCommerceUserProfile (
			Id, AccountNumber, CSRSecurityQuestion, CSRSecurityPassword, ExternalId, ExternalProfileId,
			IsExpressCustomer, IsActive, IsBadCustomer, IsOnMailingList, ModifiedDate, [Status]
		)
			VALUES (
				@Id, @AccountNumber, @CSRSecurityQuestion, @CSRSecurityPassword, @ExternalId, @ExternalProfileId,
				@IsExpressCustomer, @IsActive, @IsBadCustomer, @IsOnMailingList, GETUTCDATE(), 1
			)
	END
	ELSE
	BEGIN
		UPDATE	USCommerceUserProfile
		SET		AccountNumber = @AccountNumber,
				CSRSecurityQuestion = @CSRSecurityQuestion,
				CSRSecurityPassword = @CSRSecurityPassword,
				ExternalId = @ExternalId,
				ExternalProfileId = @ExternalProfileId,
				IsExpressCustomer = @IsExpressCustomer,
				IsActive = @IsActive,
				IsBadCustomer = @IsBadCustomer,
				IsOnMailingList = @IsOnMailingList,
				ModifiedDate = GETUTCDATE()
		WHERE	Id = @Id
	END

	UPDATE	USMembership
	SET		IsLockedOut = ISNULL(@IsLockedOut, IsLockedOut)
	WHERE	UserId = @Id

	-- Add to customer security level
	DECLARE @SecurityLevelId INT = (SELECT TOP 1 Id FROM USSecurityLevel WHERE Title = 'Commerce Customer')

	IF NOT EXISTS (SELECT * FROM USUserSecurityLevel WHERE UserId = @Id AND SecurityLevelId = @SecurityLevelId)
	BEGIN
		INSERT INTO USUserSecurityLevel (UserId, SecurityLevelId, MemberType)
			VALUES (@Id, @SecurityLevelId, 1)
	END

	-- Add to customer Everyone group
	DECLARE	@EveryOneGroupId UNIQUEIDENTIFIER
	SET		@EveryOneGroupId = (
				SELECT	TOP(1) Value 
				FROM	STSettingType AS ST
						INNER JOIN STSiteSetting AS SS ON SS.SettingTypeId = ST.Id
				WHERE	ST.[Name] = 'CommerceEveryOneGroupId' AND
						SS.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@SiteId))
			)

	IF (@EveryOneGroupId IS NOT NULL AND NOT EXISTS (
		SELECT	*
		FROM	USMemberGroup
		WHERE	MemberId = @Id AND
				GroupId = @EveryOneGroupId
	))
		INSERT INTO USMemberGroup (ApplicationId, MemberId, MemberType, GroupId)
			VALUES (@SiteId, @Id, 1, @EveryOneGroupId)
	
	-- Delete user entry from Marketier if exists	
	IF EXISTS (SELECT Id FROM USUser WHERE Id = @Id)
	BEGIN
		INSERT INTO USSiteUser (SiteId, UserId, ISSystemUser, ProductId, IsPrimarySite)
			SELECT	SiteId, @Id, 0, 'CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E', IsPrimarySite
			FROM	MKContactSite
			WHERE	ContactId = @Id AND
					SiteId NOT IN (SELECT SiteId FROM USSiteUser WHERE UserId = @Id)
	END
		
	DELETE
	FROM	MKContactSite
	WHERE	ContactId = @Id

	DELETE
	FROM	MKContact
	WHERE	Id = @Id
END
GO

PRINT 'Modify stored procedure ContactListDto_Get'
GO

IF(OBJECT_ID('ContactListDto_Get') IS NOT NULL)
	DROP PROCEDURE ContactListDto_Get
GO

CREATE PROCEDURE [dbo].[ContactListDto_Get]
(
	@Id					uniqueidentifier = NULL,
	@Ids				nvarchar(max) = NULL,
	@ContactListGroupId uniqueidentifier = NULL,
	@Status				int = NULL ,
	@SiteId				uniqueidentifier = NULL,
	@TrackingId			uniqueidentifier = NULL,
	@ListType			int = NULL ,
	@PageNumber			int = NULL,
	@PageSize			int = NULL,
	@MaxRecords			int = NULL,
	@Keyword			nvarchar(1000) = NULL,
	@Query				nvarchar(max) = NULL,
	@IsGlobal			bit = NULL,
	@UserId				uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @ContactListGroupId = @EmptyGuid SET @ContactListGroupId = NULL
	
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	DECLARE @tbListIds TABLE (Id uniqueidentifier)
	DECLARE @tbQueryIds TABLE (Id uniqueidentifier, RowNumber int)
	IF @Query IS NOT NULL
	BEGIN
		INSERT INTO @tbQueryIds
		EXEC sp_executesql @Query
	END

	DECLARE @tbIdsExists bit
	IF @Ids IS NOT NULL
	BEGIN
		SET @tbIdsExists = 1

		INSERT INTO @tbListIds
		SELECT Items FROM dbo.SplitGUID(@Ids, ',')
	END

	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)

	INSERT INTO @tbIds
	SELECT DL.Id, 
		ROW_NUMBER() OVER (ORDER BY CreatedDate) AS RowNumber
	FROM TADistributionLists DL
		LEFT JOIN TADistributionListGroup DLG ON DL.Id = DLG.DistributionId
	WHERE (@Id IS NULL OR DL.Id = @Id)
		AND (@Status IS NULL OR DL.Status = @Status)
		AND (@Keyword IS NULL OR DL.Title like '%' + @Keyword + '%')
		AND (@ListType IS NULL OR DL.ListType = @ListType)
		AND (@TrackingId IS NULL OR DL.TrackingId = @TrackingId)
		AND (@ContactListGroupId IS NULL OR DLG.DistributionGroupId = @ContactListGroupId)
		AND (@tbIdsExists IS NULL OR EXISTS(SELECT 1 FROM @tbListIds T WHERE T.Id = DL.Id))
		AND 
			( 
				(DL.IsGlobal = 0 AND DL.ApplicationId = @SiteId) OR
				(DL.IsGlobal = 1 AND DL.ApplicationId IN (SELECT A.SiteId FROM dbo.GetAncestorSites(@SiteId) A))
			)
		AND (@Query IS NULL OR EXISTS (SELECT 1 FROM @tbQueryIds T WHERE T.Id = DL.Id))
		AND (@UserId IS NULL OR EXISTS (SELECT 1 FROM TADistributionListUser WHERE DistributionListId = DL.Id AND UserId = @UserId AND ContactListSubscriptionType != 3))
		AND DL.Status != 99
				 
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
			COUNT(T.Id) OVER () AS TotalRecords,
			T.Id AS Id
		FROM @tbIds T
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT DL.*,
		SQ.*,
		DLG.DistributionGroupId AS ContactListGroupId,
		DG.Title AS GroupName,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName,
		T.RowNumber,
		T.TotalRecords,
		DL.ApplicationId as SiteId,
		dbo.GetTotalContactsCount(302 ,DL.Id, @SiteId) AS TotalContacts,
		Priority,
		dbo.GetContactListSessionXml(DL.Id) SessionFilterXml
	FROM TADistributionLists DL 
		JOIN @tbPagedResults T ON T.Id = DL.Id
		JOIN TADistributionListGroup DLG ON DLG.DistributionId = T.Id
		JOIN TADistributionGroup DG ON DG.Id = DLG.DistributionGroupId
		LEFT JOIN TADistributionListSearch DLS ON DLS.DistributionListId = DL.Id
		LEFT JOIN CTSearchQuery SQ ON SQ.Id = DLS.SearchQueryId
		LEFT JOIN VW_UserFullName CU on CU.UserId = DL.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = DL.ModifiedBy
	ORDER BY T.RowNumber

END
GO

PRINT 'Add stored procedure UserContactListDto_Save'
GO

IF(OBJECT_ID('UserContactListDto_Save') IS NOT NULL)
	DROP PROCEDURE UserContactListDto_Save
GO

CREATE PROCEDURE [dbo].[UserContactListDto_Save]
(
	@UserId				UNIQUEIDENTIFIER = NULL,
	@ContactListIds		NVARCHAR(MAX) = NULL
)
AS
BEGIN
	DECLARE @Ids TABLE (
		Id	UNIQUEIDENTIFIER
	)

	IF @ContactListIds IS NOT NULL
	BEGIN
		INSERT INTO @Ids
			SELECT	[Value]
			FROM	dbo.SplitComma(@ContactListIds, ',')
	END

	-- Mark deleted lists as ManualRemove
	UPDATE	DLU
	SET		ContactListSubscriptionType = 3
	FROM	TADistributionListUser AS DLU
			INNER JOIN TADistributionLists AS DL ON DL.Id = DLU.DistributionListId
			LEFT JOIN @Ids AS I ON I.Id = DLU.DistributionListId
	WHERE	DLU.UserId = @UserId AND
			DL.[Status] = 1 AND
			I.Id IS NULL

	-- Insert added lists as ManualAdd
	DELETE	DLU
	FROM	TADistributionListUser AS DLU
			INNER JOIN @Ids AS I ON I.Id = DLU.DistributionListId
	WHERE	DLU.UserId = @UserId AND
			DLU.ContactListSubscriptionType = 3

	INSERT INTO TADistributionListUser (Id, DistributionListId, UserId, ContactListSubscriptionType)
		SELECT	NEWID(), I.Id, @UserId, 2
		FROM	@Ids AS I
				INNER JOIN TADistributionLists AS DL ON DL.Id = I.Id
				LEFT JOIN TADistributionListUser AS DLU ON DLU.DistributionListId = I.Id AND DLU.UserId = @UserId
		WHERE	DLU.Id IS NULL AND
				DL.[Status] = 1
END
GO


PRINT 'Migrate data from product attributes to PRProduct'
GO

IF EXISTS (
	SELECT	A.*
	FROM	ATAttribute AS A
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name IN ('Advanced Details', 'Features') AND AC.IsSystem = 1
	WHERE	A.Title IN ('Promote as New Item', 'Promote as Top Seller', 'Exclude from Discounts', 'Exclude from Shipping Promotions', 'Freight', 'Refundable', 'Tax Exempt')
)
BEGIN
	UPDATE	P
	SET		PromoteAsNewItem = CONVERT(BIT, PAV.[Value])
	FROM	PRProduct AS P
			INNER JOIN PRProductAttribute AS PA ON PA.ProductId = P.Id
			INNER JOIN PRProductAttributeValue AS PAV ON PAV.ProductAttributeId = PA.Id
			INNER JOIN ATAttribute AS A ON A.Id = PA.AttributeId AND A.Title = 'Promote as New Item'
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name IN ('Advanced Details', 'Features') AND AC.IsSystem = 1

	UPDATE	P
	SET		PromoteAsTopSeller = CONVERT(BIT, PAV.[Value])
	FROM	PRProduct AS P
			INNER JOIN PRProductAttribute AS PA ON PA.ProductId = P.Id
			INNER JOIN PRProductAttributeValue AS PAV ON PAV.ProductAttributeId = PA.Id
			INNER JOIN ATAttribute AS A ON A.Id = PA.AttributeId AND A.Title = 'Promote as Top Seller'
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name IN ('Advanced Details', 'Features') AND AC.IsSystem = 1

	UPDATE	P
	SET		ExcludeFromDiscounts = CONVERT(BIT, PAV.[Value])
	FROM	PRProduct AS P
			INNER JOIN PRProductAttribute AS PA ON PA.ProductId = P.Id
			INNER JOIN PRProductAttributeValue AS PAV ON PAV.ProductAttributeId = PA.Id
			INNER JOIN ATAttribute AS A ON A.Id = PA.AttributeId AND A.Title = 'Exclude from Discounts'
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name IN ('Advanced Details', 'Features') AND AC.IsSystem = 1

	UPDATE	P
	SET		ExcludeFromShippingPromotions = CONVERT(BIT, PAV.[Value])
	FROM	PRProduct AS P
			INNER JOIN PRProductAttribute AS PA ON PA.ProductId = P.Id
			INNER JOIN PRProductAttributeValue AS PAV ON PAV.ProductAttributeId = PA.Id
			INNER JOIN ATAttribute AS A ON A.Id = PA.AttributeId AND A.Title = 'Exclude from Shipping Promotions'
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name IN ('Advanced Details', 'Features') AND AC.IsSystem = 1

	UPDATE	P
	SET		Freight = CONVERT(BIT, PAV.[Value])
	FROM	PRProduct AS P
			INNER JOIN PRProductAttribute AS PA ON PA.ProductId = P.Id
			INNER JOIN PRProductAttributeValue AS PAV ON PAV.ProductAttributeId = PA.Id
			INNER JOIN ATAttribute AS A ON A.Id = PA.AttributeId AND A.Title = 'Freight'
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name IN ('Advanced Details', 'Features') AND AC.IsSystem = 1

	UPDATE	P
	SET		Refundable = CONVERT(BIT, PAV.[Value])
	FROM	PRProduct AS P
			INNER JOIN PRProductAttribute AS PA ON PA.ProductId = P.Id
			INNER JOIN PRProductAttributeValue AS PAV ON PAV.ProductAttributeId = PA.Id
			INNER JOIN ATAttribute AS A ON A.Id = PA.AttributeId AND A.Title = 'Refundable'
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name IN ('Advanced Details', 'Features') AND AC.IsSystem = 1

	UPDATE	P
	SET		TaxExempt = CONVERT(BIT, PAV.[Value])
	FROM	PRProduct AS P
			INNER JOIN PRProductAttribute AS PA ON PA.ProductId = P.Id
			INNER JOIN PRProductAttributeValue AS PAV ON PAV.ProductAttributeId = PA.Id
			INNER JOIN ATAttribute AS A ON A.Id = PA.AttributeId AND A.Title = 'Tax Exempt'
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name IN ('Advanced Details', 'Features') AND AC.IsSystem = 1

	DECLARE @Attributes TABLE (
		Id			UNIQUEIDENTIFIER,
		CategoryId	INT
	)

	INSERT INTO @Attributes
		SELECT	A.Id, AC.Id
		FROM	ATAttribute AS A
				INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
				INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name IN ('Advanced Details', 'Features') AND AC.IsSystem = 1
		WHERE	A.Title IN ('Promote as New Item', 'Promote as Top Seller', 'Exclude from Discounts', 'Exclude from Shipping Promotions', 'Freight', 'Refundable', 'Tax Exempt')

	DELETE	PAV
	FROM	PRProductAttributeValue AS PAV
			INNER JOIN @Attributes AS TMP ON TMP.Id = PAV.AttributeId

	DELETE	PAV
	FROM	PRProductAttributeValue AS PAV
			INNER JOIN ATAttributeEnum AS AE ON AE.Id = PAV.AttributeEnumId
			INNER JOIN @Attributes AS TMP ON TMP.Id = AE.AttributeId

	DELETE	PA
	FROM	PRProductAttribute AS PA
			INNER JOIN @Attributes AS TMP ON TMP.Id = PA.AttributeId

	DELETE	ACI
	FROM	ATAttributeCategoryItem AS ACI
			INNER JOIN @Attributes AS TMP ON TMP.Id = ACI.AttributeId

	DELETE	AC
	FROM	ATAttributeCategory AS AC
			INNER JOIN @Attributes AS TMP ON TMP.CategoryId = AC.Id

	DELETE	AE
	FROM	ATAttributeEnum AS AE
			INNER JOIN @Attributes AS TMP ON TMP.Id = AE.AttributeID

	DELETE	A
	FROM	ATAttribute AS A
			INNER JOIN @Attributes AS TMP ON TMP.Id = A.Id
END
GO

PRINT 'Migrate data from PRProductSEO to PRProduct'
GO

IF(OBJECT_ID('PRProductSEO') IS NOT NULL)
BEGIN
	UPDATE	P
	SET		SEOH1 = SEO.ProductTitleH1,
			SEODescription = SEO.Description,
			SEOKeywords = SEO.Keywords,
			SEOFriendlyUrl = SEO.FriendlyUrl
	FROM	PRProduct AS P
			INNER JOIN PRProductSEO AS SEO ON SEO.ProductId = P.Id
END
GO

PRINT 'Remove table PRProductSEO'
GO
IF(OBJECT_ID('PRProductSEO') IS NOT NULL)
BEGIN
	IF(OBJECT_ID('PRProductSEO_BeforeV70') IS NULL)
		SELECT * INTO PRProductSEO_BeforeV70 FROM PRProductSEO

	DROP TABLE PRProductSEO
END
GO

PRINT 'Modify column ShortDescription on PRProduct'
GO

IF(OBJECT_ID('DF_PRProduct_ShortDescription') IS NOT NULL)
	ALTER TABLE PRProduct DROP CONSTRAINT [DF_PRProduct_ShortDescription]
GO

ALTER TABLE PRProduct ALTER COLUMN ShortDescription NVARCHAR(MAX) NULL
GO

PRINT 'Modify column LongDescription on PRProduct'
GO

IF(OBJECT_ID('DF_PRProduct_LongDescription') IS NOT NULL)
	ALTER TABLE PRProduct DROP CONSTRAINT [DF_PRProduct_LongDescription]
GO

ALTER TABLE PRProduct ALTER COLUMN LongDescription NVARCHAR(MAX) NULL
GO

PRINT 'Modify column Keyword on PRProduct'
GO

IF(OBJECT_ID('DF_PRProduct_Keyword') IS NOT NULL)
	ALTER TABLE PRProduct DROP CONSTRAINT [DF_PRProduct_Keyword]
GO

ALTER TABLE PRProduct ALTER COLUMN Keyword NVARCHAR(900) NULL
GO



PRINT 'Migrate data from product attributes to PRProductSKU'
GO

IF EXISTS (
	SELECT	A.*
	FROM	ATAttribute AS A
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name = 'SKULevel Attributes' AND AC.IsSystem = 1
	WHERE	A.Title IN ('Free Shipping', 'User Defined Price')
)
BEGIN
	UPDATE	S
	SET		FreeShipping = CONVERT(BIT, SAV.[Value])
	FROM	PRProductSKU AS S
			INNER JOIN PRProductAttribute AS PA ON PA.ProductId = S.ProductId AND PA.IsSKULevel = 1			
			INNER JOIN PRProductSKUAttributeValue AS SAV ON SAV.ProductAttributeId = PA.Id AND SAV.SKUId = S.Id
			INNER JOIN ATAttribute AS A ON A.Id = PA.AttributeId AND A.Title = 'Free Shipping'			
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name = 'SKULevel Attributes' AND AC.IsSystem = 1

	UPDATE	S
	SET		UserDefinedPrice = CONVERT(BIT, SAV.[Value])
	FROM	PRProductSKU AS S
			INNER JOIN PRProductAttribute AS PA ON PA.ProductId = S.ProductId AND PA.IsSKULevel = 1			
			INNER JOIN PRProductSKUAttributeValue AS SAV ON SAV.ProductAttributeId = PA.Id AND SAV.SKUId = S.Id
			INNER JOIN ATAttribute AS A ON A.Id = PA.AttributeId AND A.Title = 'User Defined Price'			
			INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
			INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name = 'SKULevel Attributes' AND AC.IsSystem = 1

	DECLARE @Attributes TABLE (
		Id			UNIQUEIDENTIFIER,
		CategoryId	INT
	)

	INSERT INTO @Attributes
		SELECT	A.Id, AC.Id
		FROM	ATAttribute AS A
				INNER JOIN ATAttributeCategoryItem AS ACI ON ACI.AttributeId = A.Id
				INNER JOIN ATAttributeCategory AS AC ON AC.Id = ACI.CategoryId AND AC.Name = 'SKULevel Attributes' AND AC.IsSystem = 1
		WHERE	A.Title IN ('Free Shipping', 'User Defined Price')

	DELETE	SAV
	FROM	PRProductSKUAttributeValue AS SAV
			INNER JOIN @Attributes AS TMP ON TMP.Id = SAV.AttributeId

	DELETE	SAV
	FROM	PRProductSKUAttributeValue AS SAV
			INNER JOIN ATAttributeEnum AS AE ON AE.Id = SAV.AttributeEnumId
			INNER JOIN @Attributes AS TMP ON TMP.Id = AE.AttributeId

	DELETE	PA
	FROM	PRProductAttribute AS PA
			INNER JOIN @Attributes AS TMP ON TMP.Id = PA.AttributeId

	DELETE	ACI
	FROM	ATAttributeCategoryItem AS ACI
			INNER JOIN @Attributes AS TMP ON TMP.Id = ACI.AttributeId

	DELETE	AC
	FROM	ATAttributeCategory AS AC
			INNER JOIN @Attributes AS TMP ON TMP.CategoryId = AC.Id

	DELETE	AE
	FROM	ATAttributeEnum AS AE
			INNER JOIN @Attributes AS TMP ON TMP.Id = AE.AttributeID

	DELETE	A
	FROM	ATAttribute AS A
			INNER JOIN @Attributes AS TMP ON TMP.Id = A.Id
END
GO




IF(OBJECT_ID('vwFile') IS NOT NULL)
	DROP VIEW vwFile
GO

CREATE VIEW [dbo].[vwFile]
AS
SELECT C.Id,
	C.ApplicationId,
	C.Title,
	C.Description,
	C.ParentId,
	F.FileName,
	REPLACE(ISNULL(F.Extension, ''), '.', '') AS Extension,
	F.FileSize,
	ISNULL(F.RelativePath, ISNULL(S.VirtualPath, '/' + S.Title)) AS RelativePath,
	F.MimeType,
	F.AltText,
	C.SourceContentId,
	C.CreatedBy,
	C.CreatedDate,
	C.ModifiedBy,
	C.ModifiedDate,
	C.Status,
	C.OrderNo,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName,
	F.ExcludeFromExternalSearch
FROM [dbo].[COContent] C
	JOIN COFile F ON C.Id = F.ContentId AND C.ObjectTypeId = 9
	LEFT JOIN COFileStructure S ON S.Id = C.ParentId
	LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
WHERE C.Status != 3

GO

PRINT 'Modify column Description on PRProductSKU'
GO

IF(OBJECT_ID('DF_PRProductSKU_Description') IS NOT NULL)
	ALTER TABLE PRProductSKU DROP CONSTRAINT [DF_PRProductSKU_Description]
GO

ALTER TABLE PRProductSKU ALTER COLUMN [Description] NVARCHAR(MAX) NULL
GO

PRINT 'Modify column Description on PRProductSKU'
GO

IF(OBJECT_ID('DF_PRProductSKU_Description') IS NOT NULL)
	ALTER TABLE PRProductSKU DROP CONSTRAINT [DF_PRProductSKU_Description]
GO

ALTER TABLE PRProductSKU ALTER COLUMN [Description] NVARCHAR(MAX) NULL
GO

PRINT 'Modify stored procedure Product_Delete'
GO
IF(OBJECT_ID('BLD_ExportProducts') IS NOT NULL)
	DROP PROCEDURE BLD_ExportProducts
GO
CREATE PROCEDURE [dbo].[BLD_ExportProducts]
AS
BEGIN
	SET NOCOUNT ON;

	select 
		 p.ProductIDUser as ProductId
		,p.Title as ProductName
		,cast(p.ProductTypeID as varchar(max)) as ProductTypeId
		,pt.Title as ProductType
		,p.ShortDescription
		,p.LongDescription
		,p.IsActive
		,0 as UpdateFlag
		,coalesce(SEOH1, '') as 'SEO Title'
		,coalesce(SEODescription, '') as 'SEO Description'
		,coalesce(SEOKeywords, '') as 'SEO Keywords'
		,coalesce(SEOFriendlyUrl, '') as SEOFriendlyURL
	from dbo.PRProduct p
	join dbo.PRProductType pt on (p.ProductTypeID = pt.Id)
	order by p.Title
END
GO

IF(OBJECT_ID('Product_Delete') IS NOT NULL)
	DROP PROCEDURE Product_Delete
GO

CREATE PROCEDURE [dbo].[Product_Delete](@Id uniqueidentifier)
AS
BEGIN

	Declare @skuId table(Id uniqueidentifier)

	Insert @skuId(Id)
	Select Id from PRProductSKU
	Where ProductId=@Id
	
	Delete from PRProductSKUAttributeValue
	Where SKUId IN (Select Id from @skuId)

	Delete from PSPriceSetManualSKU
	Where SKUId IN (Select Id from @skuId)

	Delete from PSPriceSetSKU
	Where SKUId IN (Select Id from @skuId)

	Delete from INInventoryLog
	Where SKUId IN (Select Id from @skuId)

	Delete from INInventory
	Where SKUId IN (Select Id from @skuId)
	
	Delete from PRProductMedia
	Where SKUId IN (Select Id from @skuId)

	Delete from PRProductSKU
	Where Id IN (Select Id from @skuId)
	
	Delete from PRProductToProductRel
	Where ProductId=@Id or RelatedProduct=@Id

	Delete
	from MRFeatureAutoExcludedItems
	Where ProductId=@Id

	Delete
	From MRFeatureManualItems
	Where ProductId=@Id

	Delete
	From MRFeatureOutput
	Where ProductId=@Id
	
	Delete
	From PRProductCategory
	Where ProductId=@Id

	Delete
	From PRProductAttributeValue
	Where ProductId=@Id

	Delete
	From PRProductAttribute
	Where ProductId=@Id

	Delete 
	FROM PRProduct
	Where Id=@Id
END
GO
IF(OBJECT_ID('Product_GetProductListP') IS NOT NULL)
	DROP PROCEDURE Product_GetProductListP
GO
CREATE PROCEDURE [dbo].[Product_GetProductListP](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@pageNumber		int = null,
			@pageSize		int = null,
			@SortColumn VARCHAR(100)=null, 
			@Searchkeyword nvarchar(4000)=null,
			@WhereClause nvarchar(max)= null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN	

set @pageNumber = @pageNumber  -1
SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0) +1
SET @SortColumn = rtrim(ltrim((@SortColumn)))
SET @PageUpperBound = @PageLowerBound + Isnull(@pageSize,0) -1;

DECLARE @PageUpperBoundVar varchar(250)
DECLARE @PageLowerBoundVar varchar(250)
DECLARE @ApplicationIdVar nvarchar(64)
SET @ApplicationIdVar = cast(@ApplicationId as nvarchar(64))
SET @ApplicationIdVar = ltrim(rtrim(@ApplicationIdVar))
SET @PageLowerBoundVar = cast(@PageLowerBound as varchar(256))
SET @PageUpperBoundVar = cast(@PageUpperBound as varchar(256))
	


DECLARE  @DynamicSQL  AS NVARCHAR(MAX) 


SET @SearchKeyword = (@SearchKeyword)

     IF @SortColumn = 'producttitle desc'	 
	SET @SortColumn = 'Product.Title DESC' 	
ELSE IF @SortColumn = 'producttitle asc'	
	SET @SortColumn = 'Product.Title asc' 	  


ELSE IF @SortColumn = 'typetitle desc'
	SET @SortColumn = 'Type.Title DESC'  
ELSE IF @SortColumn = 'typetitle asc'
	SET @SortColumn = 'Type.Title ASC'  

ELSE IF @SortColumn = 'reallistprice desc'
	SET @SortColumn = 'PPR.ListPrice DESC'  
ELSE IF @SortColumn = 'reallistprice asc'
	SET @SortColumn = 'PPR.ListPrice ASC'  

ELSE IF @SortColumn = 'quantity desc'
	SET @SortColumn = 'PPR.Quantity DESC'  
ELSE IF @SortColumn = 'quantity asc'
	SET @SortColumn = 'PPR.Quantity ASC'
  
ELSE IF @SortColumn = 'isactivedescription desc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) DESC'  
ELSE IF @SortColumn = 'isactivedescription asc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) ASC'  

DECLARE @DynamicWhereClause nvarchar(max)


IF @SearchKeyword = ''
	SET @DynamicWhereClause = N''
ELSE
	SET @DynamicWhereClause = 	
					N'AND ((Product.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Product.ProductIDUser) Like ''%''+'''+@SearchKeyword+'''+''%''			
					OR (Type.Title) Like ''%''+'''+@SearchKeyword+'''+''%''			
					) '

IF @WhereClause != '' 
	SET @DynamicWhereClause = @DynamicWhereClause +
			N'AND ('+@WhereClause+')';

SET @DynamicSQL = N'
SELECT * FROM (
   SELECT   
     ROW_NUMBER() OVER (ORDER BY 	
	 '+@SortColumn+'
) AS RowNumber, 				
					Product.Id As ProductId,
					Product.SiteId,
					Product.ShortDescription As Description, 
					Product.ProductIDUser ProductIDUser,
					Product.ProductIDUser As Sequence, 
					Product.IsActive, 
					case Product.IsActive WHEN 1 THEN ''Active'' ELSE ''Inactive'' END  As IsActiveDescription ,
					PPR.PriceRange as ListPrice,
					0 As UnitCost,
					0 As SalePrice,
					0 As WholesalePrice, 
					isnull(PPR.Quantity,0) As Quantity,
					isnull(PPR.AvailableToSell,0) As AvailableToSell,
					isnull(PPR.ListPrice,0) as RealListPrice,
					Product.IsBundle as IsBundle,
					dbo.ConvertTimeFromUtc(Product.BundleCompositionLastModified, '''+@ApplicationIdVar+''') as BundleCompositionLastModified,
					Product.Title As ProductTtitle,
					Product.Title As ProductTitle,
					Type.Title As TypeTitle,  
					Type.Id as TypeId,
					dbo.GetProductCategoryTitleCollection(Product.Id)  As CategoryTitle, 
					dbo.GetProductCategoryCollection(Product.Id)  As CategoryIdsAndTitles,
					dbo.IsProductUnlimited(Product.Id) as IsUnlimited,
					ISNULL(Type.IsDownloadableMedia,0) as IsDownloadableMedia,
					Product.TaxCategoryID,
					PromoteAsNewItem,
					PromoteAsTopSeller,
					ExcludeFromDiscounts,
					ExcludeFromShippingPromotions,
					Freight,
					Refundable,
					TaxExempt,
					SEOH1,
					SEODescription,
					SEOKeywords
			FROM PRProduct As Product
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
			LEFT JOIN vwPRProductPriceRange PPR  ON PPR.Id =  Product.Id AND PPR.SiteId=''' + cast(@ApplicationId as Char(36)) + '''
            WHERE		Product.IsBundle = 0 			
				'+@DynamicWhereClause+'
		) TV
		WHERE 
		(RowNumber BETWEEN '+@PageLowerBoundVar+' AND '+@PageUpperBoundVar+')'

print @DynamicSQL
EXEC sp_executesql   @DynamicSQL 


SET @DynamicSQL = N'
SELECT 
	Count(*) 
FROM PRProduct As Product
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  

            WHERE	
					Product.IsBundle = 0 
		'+@DynamicWhereClause
EXEC sp_executesql   @DynamicSQL 

END
GO
IF(OBJECT_ID('Product_SaveProduct') IS NOT NULL)
	DROP PROCEDURE Product_SaveProduct
GO

CREATE PROCEDURE [dbo].[Product_SaveProduct]
(
	@Id uniqueidentifier output ,
	@ProductCode nvarchar(50),
	@Title	nvarchar(max),
	@ShortDescription nvarchar(max)= '',
	@LongDescription nvarchar(max)= '',
	@Description nvarchar(max),
    @Keywords nvarchar(max)= '',
    @Status int,
	@IsBundle tinyint,
	@IsActive tinyint,
    @ProductTypeId uniqueidentifier,
    @ApplicationId uniqueidentifier,
    @ModifiedBy uniqueidentifier,
	@UrlFriendlyTitle nvarchar(500)=null,
	@TaxCategoryId uniqueidentifier = null,
	@ProductURLReplacementChar char(1) ='-',
	@PromoteAsNewItem bit =0,
	@PromoteAsTopSeller bit =0,
	@ExcludeFromDiscounts bit =0,
	@ExcludeFromShippingPromotions bit=0,
	@Freight bit =0,
	@Refundable bit =0,
	@TaxExempt bit=0,
	@SEOH1 nvarchar(255) =null,
	@SEODescription nvarchar(max) =null,
	@SEOKeywords nvarchar(max) =null,
	@SEOFriendlyUrl nvarchar(255) =null
)
as
begin
	Declare @CurrentDate datetime
	set @CurrentDate = GetUTCDate() 

	if (@UrlFriendlyTitle is null OR @UrlFriendlyTitle ='')
	Begin
		set @UrlFriendlyTitle = dbo.MakeFriendlyProductTitle(@Title,@ProductURLReplacementChar)
	End

	if(@Id is null OR @Id = dbo.GetEmptyGUID())
	Begin		
			IF Exists(Select Id from PRProduct Where (ProductIDUser) =(@ProductCode))
		BEGIN
			RAISERROR('Product with this code already exists. Please choose a different product code.', 16, 1)
		END		
		Set @Id = newid()
        insert into PRProduct 
			([Id]
			,[ProductIDUser]
			,[Title], UrlFriendlyTitle 
			,[ShortDescription]
			,[LongDescription]
			,[Description]
			,[Keyword]
			,[IsBundle]
			,[IsActive]
			,[Status]
			,[ProductTypeID]
			,[SiteId]
			,[CreatedDate]
			,[CreatedBy]
			,[TaxCategoryID]
			,PromoteAsNewItem
			,PromoteAsTopSeller
			,ExcludeFromDiscounts
			,ExcludeFromShippingPromotions
			,Freight
			,Refundable
			,TaxExempt
			,SEOH1
			,SEODescription
			,SEOKeywords
			)
		 values
			(@Id,
			@ProductCode,
			@Title, @UrlFriendlyTitle, 
			@ShortDescription,
			@LongDescription,
			@Description,
			@Keywords,
			@IsBundle,
			@IsActive,
			@Status,
			@ProductTypeId,
			@ApplicationId,
			@CurrentDate,
			@ModifiedBy,
			@TaxCategoryId
			,@PromoteAsNewItem
			,@PromoteAsTopSeller
			,@ExcludeFromDiscounts
			,@ExcludeFromShippingPromotions
			,@Freight
			,@Refundable
			,@TaxExempt
			,@SEOH1
			,@SEODescription
			,@SEOKeywords)

		
		declare @defaultCategoryId uniqueidentifier
		declare @prCategorySequence int
		select @defaultCategoryId = Id from PRCategory where Title='Unassigned'
		select @prCategorySequence = max(isnull(Sequence,0)) from PRProductCategory  
		
		insert into PRProductCategory (Id,ProductId, CategoryId,Sequence) 
					values(newid(), @Id,@defaultCategoryId,@prCategorySequence+1)
		
	end
	else
	begin

		IF Exists(Select Id from PRProduct Where (ProductIDUser) =(@ProductCode) AND Id!=@Id)
		BEGIN
			RAISERROR('Product with this code already exists. Please choose a different product code.', 16, 1)
		END
		
		update PRProduct 
		set	 [ProductIDUser]=@ProductCode
			,[Title] = @Title
			,[UrlFriendlyTitle] = @UrlFriendlyTitle
			,[ShortDescription] =@ShortDescription
			,[LongDescription] =@LongDescription
			,[Description] =@Description
			,[Keyword] =@Keywords
			,[IsActive]=@IsActive
			,[IsBundle] =@IsBundle
			,[Status] =@Status
			,[ProductTypeID] =@ProductTypeId
			,[SiteId]=@ApplicationId
			,[ModifiedDate]=@CurrentDate
			,[ModifiedBy]=@ModifiedBy
			,[TaxCategoryID]=@TaxCategoryId
			,PromoteAsNewItem=@PromoteAsNewItem
			,PromoteAsTopSeller=@PromoteAsTopSeller
			,ExcludeFromDiscounts=@ExcludeFromDiscounts
			,ExcludeFromShippingPromotions=@ExcludeFromShippingPromotions
			,Freight=@Freight
			,Refundable=@Refundable
			,TaxExempt=@TaxExempt
			,SEOH1=isnull(@SEOH1,SEOH1)
			,SEODescription=isnull(@SEODescription,SEODescription)
			,SEOKeywords=isnull(@SEOKeywords,SEOKeywords)
		where Id =@Id
		
	
	End
		
	
end


GO

GO

IF(OBJECT_ID('Search_GetProducts') IS NOT NULL)
	DROP PROCEDURE Search_GetProducts
GO

CREATE PROCEDURE [dbo].[Search_GetProducts]      
(      
		@ApplicationId UNIQUEIDENTIFIER = NULL
)      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
DECLARE @tempXml TABLE(Sno int IDENTITY(1,1),ProductId UNIQUEIDENTIFIER)      
      
INSERT INTO @tempXml(ProductId)      
SELECT DISTINCT ProductId AS Id FROM PRProductSKU WHERE IsActive=1 AND IsOnline=1  
      
 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] AS ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle AS DefaultUrl, S.UrlFriendlyTitle,       
 vPT.[IsDownloadableMedia],S.[ProductStyle],      
 (SELECT Count(*) FROM PRProductSKU WHERE ProductId= S.[Id])AS NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) AS BundleCompositionLastModified,      
 SEOFriendlyUrl,
 [S].TaxCategoryID,
 PromoteAsNewItem,
 PromoteAsTopSeller,
 ExcludeFromDiscounts,
 ExcludeFromShippingPromotions,
 Freight,
 Refundable,
 TaxExempt,
 SEOH1,
 SEODescription,
 SEOKeywords
  FROM @tempXml T      
  INNER JOIN PRProduct S ON S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT ON [S].ProductTypeID = vPT.ProductTypeId      
  
--Order By T.Sno      
  
  
SELECT   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
FROM @tempXml t   
INNER JOIN PRProductAttributeValue SA  ON SA.ProductId=t.ProductId  
LEFT JOIN ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
WHERE SA.Status = dbo.GetActiveStatus()  
  
  
SELECT [Id]    
      ,S.[ProductId]    
      ,S.[SiteId]    
      ,S.[SKU]    
      ,S.[Title]    
      ,S.[Description]    
      ,S.[Sequence]    
      ,S.[IsActive]    
      ,S.[IsOnline]    
      ,S.[ListPrice]    
      ,S.[UnitCost]    
      ,S.[PreviousSoldCount]    
      ,S.[CreatedBy]    
      ,S.[CreatedDate]    
      ,S.[ModifiedBy]    
      ,S.[ModifiedDate]    
      ,S.[Status]    
      ,S.[Measure]    
      ,S.[OrderMinimum]    
      ,S.[OrderIncrement]  
      ,S.[HandlingCharges]  
      ,S.[Length]
	  ,S.[Height]
	  ,S.[Width]
	  ,S.[Weight]
	  ,S.[SelfShippable]    
      ,S.[AvailableForBackOrder]
	  ,S.[BackOrderLimit]
	  ,S.[IsUnlimitedQuantity]
	  ,FreeShipping
	  ,UserDefinedPrice
  FROM [PRProductSKU] S   
  INNER JOIN @tempXml t ON S.ProductId=t.ProductId  
  
  
SELECT   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 ISNULL(AE.[IsDefault], 0) AS [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
FROM @tempXml t   
INNER JOIN PRProductAttribute PA  ON PA.ProductId=t.ProductId  
INNER JOIN PRProductSKU PS ON PA.ProductId = PS.ProductId  
INNER JOIN PRProductSKUAttributeValue SA  ON (SA.AttributeId=PA.AttributeId AND PS.Id = SA.SKUId )  
LEFT JOIN ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
WHERE IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  
 
SELECT  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
FROM @tempXml t   
INNER JOIN PRProductAttribute PA  ON PA.ProductId=t.ProductId   
INNER JOIN ATAttribute A ON PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
WHERE  A.Status=dbo.GetActiveStatus() AND  A.IsSystem=0 AND A.IsFaceted=1
  

SELECT P.Id AS ProductId,	
CASE WHEN Min(MinimumEffectivePrice) IS NULL          THEN MIN(ListPrice) 
     WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
ELSE Min(ListPrice) END  as Price , cast( SUM(PS.PreviousSoldCount) AS INT) AS soldcount, P.CreatedDate		
			FROM			
				dbo.PRProduct P
				INNER JOIN dbo.PRProductSKU PS ON P.Id = PS.ProductId 		
				LEFT JOIN (SELECT * FROM cache_ProductMinEffectivePrice MP ) PP ON P.Id= PP.ProductId 	
				INNER JOIN @tempXml TP  ON P.Id = TP.ProductId 	
			WHERE     
				(PS.IsActive = 1) AND (PS.IsOnline = 1) AND (P.IsActive = 1)     
			GROUP BY     
				P.Id, 
				PP.MinimumEffectivePrice,
				P.CreatedDate,   
				P.ProductIDUser   
END



GO


IF(OBJECT_ID('Bundle_GetBundleItems') IS NOT NULL)
	DROP PROCEDURE Bundle_GetBundleItems
GO

CREATE PROCEDURE [dbo].[Bundle_GetBundleItems](  
--********************************************************************************  
-- Parameters  
--********************************************************************************  
   @Id uniqueidentifier = null  
   ,@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
 SELECT s.[Id]  
      ,s.[ProductId]  
      ,s.[SiteId]  
      ,s.[SKU]  
	,s.[Id] As SkuId
   ,s.[Title]  
      ,s.[Description]  
      ,s.[Sequence]  
      ,s.[IsActive]  
      ,s.[IsOnline]  
      ,s.[ListPrice]  
      ,s.[UnitCost]  
      ,s.[PreviousSoldCount]  
   ,s.[CreatedBy]  
   ,s.[CreatedDate]
   ,s.[ModifiedBy]  
   ,s.ModifiedDate
   ,s.[Status]  
   ,s.[Measure]  
   ,s.[OrderMinimum]  
   ,s.[OrderIncrement] 
   ,s.[Length]
   ,s.[Height]
   ,s.[Width]
   ,s.[Weight]
   ,s.[SelfShippable]  
   ,s.AvailableForBackOrder
   ,s.MaximumDiscountPercentage
   ,s.BackOrderLimit
	,s.IsUnlimitedQuantity
	,s.FreeShipping
	,s.UserDefinedPrice
   ,bs.[BundleItemId]  
   ,bi.[BundleProductId]  
   ,bs.[ChildSkuId]  
   ,bi.[ChildSkuQuantity]  
   ,bi.[CreatedBy] as [BundleSkuCreatedBy]  
   ,bi.[CreatedDate] as [BundleSkuCreatedDate]  
   ,bi.[ModifiedBy] as [BundleSkuModifiedBy]  
   ,bi.[ModifiedDate] as [BundleSkuModifiedDate],
   (SELECT 
		
		
		sum(Quantity)
	From 
		[dbo].[vwAllocatedQuantityPerWarehouse] A INNER JOIN INWarehouseSite SI on A.WarehouseId = SI.WarehouseId Where ProductSKUId = s.[Id] AND SI.SiteId=@ApplicationId )
	 as AllocatedQuantity,
   (SELECT		
		CASE s.IsUnlimitedQuantity
				WHEN 1 then 9999999
				ELSE sum(Quantity)
			END		
	From 
		INInventory I Inner join INWarehouseSite SI on I.WarehouseId = SI.WarehouseId Where SKUId = s.[Id]  AND SI.SiteId=@ApplicationId)
	 as QuantityOnHand    --- sum of all warehouse -- find bundle item wise minimum in API or helper
 FROM  PRBundleItemSku bs left join PRProductSKU s on bs.[ChildSkuId]=s.[Id] left join PRBundleItem bi on bs.[BundleItemId]=bi.Id 
 WHERE  
  bi.[BundleProductId]=@Id  
  AND  
  Status = dbo.GetActiveStatus() -- Select only Active Attributes  
 Order By bi.[Id]  
END

GO

IF(OBJECT_ID('BundleItem_Get') IS NOT NULL)
	DROP PROCEDURE BundleItem_Get
GO

CREATE  PROCEDURE [dbo].[BundleItem_Get](  
--********************************************************************************  
-- Parameters  
--********************************************************************************  
   @Id uniqueidentifier = null  
   ,@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
 SELECT s.[Id]  
      ,s.[ProductId]  
      ,s.[SiteId]  
      ,s.[SKU]  
	  ,s.[Id] As SkuId
	  ,s.[Title]  
      ,s.[Description]  
      ,s.[Sequence]  
      ,s.[IsActive]  
      ,s.[IsOnline]  
      ,s.[ListPrice]  
      ,s.[UnitCost]  
      ,s.[PreviousSoldCount]  
   ,s.[CreatedBy]  
   ,s.[CreatedDate]
   ,s.[ModifiedBy]  
   ,s.ModifiedDate
   ,s.[Status]  
   ,s.[Measure]  
   ,s.[OrderMinimum]  
   ,s.[OrderIncrement]  
   ,s.[Length]
	  ,s.[Height]
	  ,s.[Width]
	  ,s.[Weight]
	  ,s.[SelfShippable]  
	  ,s.AvailableForBackOrder
	  ,s.MaximumDiscountPercentage
	  ,s.BackOrderLimit
	,s.IsUnlimitedQuantity
	,s.FreeShipping
	,s.UserDefinedPrice
   ,bs.[BundleItemId]  
   ,bi.[BundleProductId]  
   ,bs.[ChildSkuId]  
   ,bi.[ChildSkuQuantity]  
   ,bi.[CreatedBy] as [BundleSkuCreatedBy]  
   ,dbo.ConvertTimeFromUtc(bi.[CreatedDate],@ApplicationId) as [BundleSkuCreatedDate]  
   ,bi.[ModifiedBy] as [BundleSkuModifiedBy]  
   ,dbo.ConvertTimeFromUtc(bi.[ModifiedDate] ,@ApplicationId)as [BundleSkuModifiedDate],
   (SELECT sum(Quantity) From vwAllocatedQunatity Where ProductSKUId = s.[Id] ) as AllocatedQuantity,
   (SELECT sum(Quantity) From INInventory Where SKUId = s.[Id] ) as QuantityOnHand    --- sum of all warehouse -- find bundle item wise minimum in API or helper
 FROM  PRBundleItemSku bs left join PRProductSKU s on bs.[ChildSkuId]=s.[Id] left join PRBundleItem bi on bs.[BundleItemId]=bi.Id 
 WHERE  
  bi.[Id]=@Id  
  AND  
  Status = dbo.GetActiveStatus() -- Select only Active Attributes  
 Order By bs.[Id]  
END

GO

IF(OBJECT_ID('Product_GetProductSKUList') IS NOT NULL)
	DROP PROCEDURE Product_GetProductSKUList
GO

CREATE  PROCEDURE [dbo].[Product_GetProductSKUList](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@PageIndex		int = null,
			@PageSize		int = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
		SET @PageLowerBound = Isnull(@PageSize,0) * Isnull(@PageIndex,0)
		SET @PageUpperBound = @PageLowerBound - Isnull(@PageSize,0) + 1;
	
		WITH ResultEntries AS
		(
          SELECT 
					ROW_NUMBER() OVER (ORDER BY SKU.Title) AS RowNumber,
					SKU.Id, SKU.ProductId, SKU.SiteId, SKU.Title, SKU.SKU, SKU.Description, 
					SKU.Sequence, SKU.IsActive, case SKU.IsActive WHEN 1 THEN 'Active' ELSE 'Inactive' END  As IsActiveDescription ,  SKU.IsOnline, SKU.ListPrice, SKU.UnitCost, 
					SKU.ListPrice As SalePrice, SKU.WholesalePrice As WholesalePrice, 
					inv.Quantity As Quantity,
					SKU.HandlingCharges,  SKU.Length,SKU.Height,SKU.Width,SKU.Weight,SKU.SelfShippable,SKU.AvailableForBackOrder, SKU.MaximumDiscountPercentage, SKU.BackOrderLimit,
					Product.Title As ProductTitle, Type.Title	As TypeTitle,  dbo.GetProductCategoryTitleCollection(Product.Id)  As CategoryTitle, dbo.GetProductCategoryCollection(Product.Id)  As CategoryIdsAndTitles,
					IsUnlimitedQuantity as IsUnlimited
					,FreeShipping
					,UserDefinedPrice
					--dbo.IsSkuUnlimited(SKU.Id) as IsUnlimited
			FROM PRProductSKU As SKU
			INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
			LEFT JOIN [vwInventoryPerSite] inv on SKU.Id = inv.SKUId AND inv.SiteId=@ApplicationId
            WHERE	
					Product.IsBundle = 0
		)

		SELECT * 
		FROM ResultEntries 
		WHERE ((@PageIndex is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))  Order by Sequence

END

GO
IF(OBJECT_ID('Product_GetProductSKUListP') IS NOT NULL)
	DROP PROCEDURE Product_GetProductSKUListP
GO

CREATE PROCEDURE [dbo].[Product_GetProductSKUListP](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@pageNumber		int = null,
			@pageSize		int = null,
			@SortColumn VARCHAR(50)=null, 
			@Searchkeyword nvarchar(4000)=null,
			@WhereClause nvarchar(max)= null	
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 


--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

		
set @pageNumber = @pageNumber  -1
SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
SET @SortColumn = lower(rtrim(ltrim((@SortColumn))))
SET @PageUpperBound = @PageLowerBound + Isnull(@pageSize,0) -1;

DECLARE @PageUpperBoundVar varchar(250)
DECLARE @PageLowerBoundVar varchar(250)
DECLARE @ApplicationIdVar nvarchar(64)
SET @ApplicationIdVar = cast(@ApplicationId as nvarchar(64))
SET @ApplicationIdVar = ltrim(rtrim(@ApplicationIdVar))
SET @PageLowerBoundVar = cast(@PageLowerBound as varchar(256)) +1
SET @PageUpperBoundVar = cast(@PageUpperBound as varchar(256)) + 1



--SET @SearchKeyword = ''

DECLARE  @DynamicSQL  AS NVARCHAR(MAX) 


SET @SearchKeyword = (@SearchKeyword)

IF @SortColumn = 'producttitle desc'	 
	SET @SortColumn = 'Product.Title DESC' 	
ELSE IF @SortColumn = 'producttitle asc'	
	SET @SortColumn = 'Product.Title asc' 	
  
ELSE IF @SortColumn = 'sku desc'
	SET @SortColumn = 'SKU.SKU DESC'  
ELSE IF @SortColumn = 'sku asc'
	SET @SortColumn = 'SKU.SKU ASC'  

ELSE IF @SortColumn = 'typetitle desc'
	SET @SortColumn = 'Type.Title DESC'  
ELSE IF @SortColumn = 'typetitle asc'
	SET @SortColumn = 'Type.Title ASC'  

ELSE IF @SortColumn = 'listprice desc'
	SET @SortColumn = 'SKU.ListPrice DESC'  
ELSE IF @SortColumn = 'listprice asc'
	SET @SortColumn = 'SKU.ListPrice ASC'  

ELSE IF @SortColumn = 'quantity desc'
	SET @SortColumn = 'inv.Quantity DESC'  
ELSE IF @SortColumn = 'quantity asc'
	SET @SortColumn = 'inv.Quantity ASC'

ELSE IF @SortColumn = 'availabletosell desc'
	SET @SortColumn = 'avail.Quantity DESC'  
ELSE IF @SortColumn = 'availabletosell asc'
	SET @SortColumn = 'avail.Quantity ASC'
  
ELSE IF @SortColumn = 'isactivedescription desc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) DESC'  
ELSE IF @SortColumn = 'isactivedescription asc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) ASC'  

DECLARE @DynamicWhereClause nvarchar(max)


IF @SearchKeyword = ''
	SET @DynamicWhereClause = N''
ELSE
	SET @DynamicWhereClause = 
					N'AND ((Product.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Product.ProductIDUser) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Type.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (SKU.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (SKU.SKU) Like ''%''+'''+@SearchKeyword+'''+''%''
					) '
					
IF @WhereClause != '' 
	SET @DynamicWhereClause = @DynamicWhereClause +
			N'AND ('+@WhereClause+')';


SET @DynamicSQL = N'
SELECT * FROM (
   SELECT   
     ROW_NUMBER() OVER (ORDER BY 	
	 '+@SortColumn+'
) AS RowNumber, 				
					SKU.Id, 
					SKU.ProductId,
					SKU.SiteId, 
					SKU.Title, 
					SKU.SKU, 
					SKU.Description, 
					SKU.Sequence, 
					SKU.IsActive, 
					case SKU.IsActive WHEN 1 THEN ''Active'' ELSE ''Inactive'' END  As IsActiveDescription ,  
					SKU.IsOnline, 
					SKU.ListPrice, 
					SKU.UnitCost, 
					SKU.ListPrice As SalePrice, 
					SKU.WholesalePrice As WholesalePrice, 
					IsNull(inv.Quantity,0) as Quantity,
					avail.Quantity as AvailableToSell,
					SKU.HandlingCharges,
					Product.Title As ProductTitle, 
					Product.ProductIDUser As ProductIDUser,
					Type.Title	As TypeTitle,  
					Type.Id as TypeId,
					dbo.GetProductCategoryTitleCollection(Product.Id)  
					As CategoryTitle, 
					dbo.GetProductCategoryCollection(Product.Id)  
					 As CategoryIdsAndTitles,
					IsUnlimitedQuantity AS IsUnlimited,
					SKU.Length,SKU.Height,SKU.Width,SKU.Weight,SKU.SelfShippable,SKU.AvailableForBackOrder, SKU.MaximumDiscountPercentage,SKU.BackOrderLimit,
					ISNULL(Type.IsDownloadableMedia,0) as IsDownloadableMedia,FreeShipping,UserDefinedPrice
			FROM PRProductSKU As SKU
			INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
			LEFT JOIN [vwInventoryPerSite] inv on SKU.Id = inv.SKUId AND inv.SiteId=''' + @ApplicationIdVar + '''
			LEFT JOIN vwAvailableToSell avail ON SKU.Id = avail.SKUId AND avail.SiteId=''' + @ApplicationIdVar + '''
            WHERE	
				Product.IsBundle = 0		
				'+@DynamicWhereClause+'
		) TV
		WHERE 
		(RowNumber BETWEEN '+@PageLowerBoundVar+' AND '+@PageUpperBoundVar+')'


EXEC sp_executesql   @DynamicSQL 

SET @DynamicSQL = N'
SELECT  
	Count(*)
FROM 
	PRProductSKU As SKU
	INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
	INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
WHERE	
		Product.IsBundle = 0
		'+@DynamicWhereClause

EXEC sp_executesql   @DynamicSQL 

END

GO

IF(OBJECT_ID('Product_GetSKU') IS NOT NULL)
	DROP PROCEDURE Product_GetSKU
GO

CREATE PROCEDURE [dbo].[Product_GetSKU]  
(  
 @ProductId uniqueidentifier  = null
 ,@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
IF @ProductId is not null
select  
  Id,  
  ProductId,  
  SiteId,  
  Title,  
  SKU,  
  Description,  
  Sequence,  
  IsActive,  
  IsOnline,  
  ListPrice,  
  UnitCost,  
  Measure,
  OrderMinimum,
  OrderIncrement,	HandlingCharges, 
  PreviousSoldCount,  
  dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,  
  CreatedBy,  
  dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,  
  ModifiedBy
    ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable] 
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
	,FreeShipping
	,UserDefinedPrice
  from PRProductSKU 
  where ProductId=@ProductId and Status=dbo.GetActiveStatus()  
  Order by Sequence, Title  
ELSE
 select  
  Id,  
  ProductId,  
  SiteId,  
  Title,  
  SKU,  
  Description,  
  Sequence,  
  IsActive,  
  IsOnline,  
  ListPrice,  
  UnitCost,  
  Measure,
  OrderMinimum,
  OrderIncrement,	HandlingCharges, 
  PreviousSoldCount,  
  dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,  
  CreatedBy,  
  dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,  
  ModifiedBy  
,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable] 
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
	,FreeShipping
	,UserDefinedPrice
  from PRProductSKU where 
		--ProductId=Isnull(@ProductId,ProductId) 
		(@ProductId is null or ProductId = @ProductId)
		and Status=dbo.GetActiveStatus()  
  Order by Sequence, Title  
END

GO
IF(OBJECT_ID('ProductType_GetSkus') IS NOT NULL)
	DROP PROCEDURE ProductType_GetSkus
GO

CREATE PROCEDURE [dbo].[ProductType_GetSkus](  
--********************************************************************************  
-- Parameters  
--********************************************************************************  
   @ProductTypeId    uniqueidentifier = null,  
   @ApplicationId uniqueidentifier = null  
)  
AS  
--********************************************************************************  
-- Procedure Body  
--********************************************************************************  
BEGIN  
		SELECT 
			sku.Id,
			sku.ProductId,
			sku.SiteId,
			sku.Title,
			sku.SKU,
			sku.Description,
			sku.Sequence,
			sku.IsActive,
			sku.IsOnline,
			sku.ListPrice,
			sku.UnitCost,
			sku.WholesalePrice,
			--sku.Quantity,
			sku.PreviousSoldCount,
			dbo.ConvertTimeFromUtc(sku.CreatedDate,@ApplicationId)CreatedDate,
			sku.CreatedBy,
			dbo.ConvertTimeFromUtc(sku.ModifiedDate,@ApplicationId)ModifiedDate,
			sku.ModifiedBy,
			sku.Status,
			sku.Measure,
			sku.OrderMinimum,
			sku.OrderIncrement,
			sku.HandlingCharges,
			sku.[Length],
			sku.[Height],
			sku.[Width],
			sku.[Weight],
			sku.[SelfShippable],  
			sku.AvailableForBackOrder,
			sku.MaximumDiscountPercentage,
			sku.BackOrderLimit,
			sku.IsUnlimitedQuantity,
			sku.FreeShipping,
			sku.UserDefinedPrice,
			Product.Title As ProductTitle
		FROM PRProductSKU sku
		INNER JOIN PRProduct Product ON sku.ProductId = Product.Id
		INNER JOIN PRProductType ON Product.ProductTypeID =  PRProductType.Id
        WHERE 
				--PRProductType.[Id]   = Isnull(@ProductTypeId,PRProductType.[Id])   AND   
				--sku.SiteId  = Isnull(@ApplicationId, sku.SiteId)  AND  
				(@ProductTypeId is null or PRProductType.[Id] = @ProductTypeId) AND
				(@ApplicationId is null or sku.SiteId = @ApplicationId) AND
				sku.Status  = dbo.GetActiveStatus() 
		Order by  Product.Title, sku.SKU	  
END
GO

IF(OBJECT_ID('BLD_ProductImport_Sku') IS NOT NULL)
BEGIN
	Drop Procedure [dbo].[BLD_ProductImport_Sku]
END
GO

CREATE PROCEDURE [dbo].[BLD_ProductImport_Sku]

@ProductIDUser nvarchar(max),
@IsActive bit = 1,
@IsOnline bit = 1,
@TaxExempt bit = 0,
@SKU nvarchar(max),
@SKUTitle nvarchar(max),
@Quantity decimal(18,4) = 0,
@OperatorId uniqueidentifier = null,
@ListPrice money,
@UnitCost money,
@Measure nvarchar(max) = null,
@OrderMinimum DECIMAL(18,4) = 1,
@OrderIncrement decimal(18,4) = 1,
@Handling decimal(18,4) = 0,
@Length decimal(18,4) = 0,
@Height decimal(18,4) = 0,
@Width decimal(18,4) = 0,
@Weight decimal(18,4) = 0,
@Selfshippable bit = 0,
@AvailableForBackOrder bit = 0,
@BackOrderLimit decimal(18,4) = 0,
@IsUnlimitedQuantity bit = 0,
@FreeShipping bit =0,
@UserDefinedPrice bit =0,
@ProductSkuIdGuid uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;

	declare @actionType int = 0

	set @Length = case when @Length = -1 then null else @Length end
	set @Width = case when @Width = -1 then null else @Width end
	set @Height = case when @Height = -1 then null else @Height end
	set @Weight = case when @Weight = -1 then null else @Weight end

	if @OrderMinimum<1 RAISERROR ('OrderQuantityMinimum must be greater than or equal to 1!', 16, 1);
	if @OrderIncrement<1 RAISERROR ('OrderQuantityIncrement must be greater than or equal to 1!', 16, 1);
	if @AvailableForBackOrder=1 and @BackOrderLimit<1  RAISERROR ('ackOrderLimit must be greater than or equal to 1!', 16, 1);

	BEGIN TRY
        BEGIN TRAN

		if @OperatorId is null
		begin
			set @OperatorId = (select top 1 CreatedBy from PRProductType where CreatedBy is not null);
		end

		declare @ProductId uniqueidentifier;
		declare @ProductSKUId uniqueidentifier;
		declare @TaxCategoryId uniqueidentifier;
		declare @InventoryId uniqueidentifier;
		declare @IsNewSku bit
		Set @IsNewSku = 1

		If(@ProductSkuIdGuid IS NOT NULL)
			SET @ProductSKUId = @ProductSkuIdGuid 

		select @ProductId = Id FROM PRProduct where ProductIDUser like @ProductIDUser;
		
		IF (@ProductSKUId IS NULL)
			select @ProductSKUId = Id FROM PRProductSKU where ProductId = @ProductId and SKU = @SKU;

		if(@TaxExempt=1)
		begin
			select @TaxCategoryId = Id from PRTaxCategory where Title like '%non%';
		end else
		begin
			select @TaxCategoryId = Id from PRTaxCategory where Title not like '%non%';
		end

		if(@TaxCategoryId is null)RAISERROR ('Unknown tax category!', 16, 1);

		IF EXISTS(Select 1 From PRProductSKU where Id = @ProductSKUId)
			SET @IsNewSku = 0 -- This is required, as in case of variant site we will be passing the ProductSkuId, and we want to use that while creating the new SKU

		--INSERTING THE PRODUCT SKU
		if @IsNewSku = 1
		begin

			If(@ProductSKUId IS NULL) -- If the ProductSKUIDGuid was not passed, then it will be Master site and the new SKU has to be created 
				set @ProductSKUId = newid();
			insert into PRProductSKU
			   ([Id]
           ,[ProductId]
           ,[SiteId]
           ,[Title]
           ,[SKU]
           ,[Description]
           ,[Sequence]
           ,[IsActive]
           ,[IsOnline]
           ,[ListPrice]
           ,[UnitCost]
           ,[WholesalePrice]
           ,[PreviousSoldCount]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[Measure]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[HandlingCharges]
           ,[Length]
           ,[Height]
           ,[Width]
           ,[Weight]
           ,[SelfShippable]
           ,[AvailableForBackOrder]
           ,[BackOrderLimit]
           ,[IsUnlimitedQuantity]
           ,[MaximumDiscountPercentage]
           ,[FreeShipping]
           ,[UserDefinedPrice])
			select
			@ProductSKUId,
			@ProductId,
			'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',
			@SKUTitle,
			@SKU,
			'', --Description
			(select isnull(max(Sequence)+1,0) from PRProductSKU where ProductId = @ProductId), --Sequence
			@IsActive,
			@IsOnline,
			@ListPrice,
			@UnitCost,
			0, --WholesalePrice
			0,
			GETDATE(),
			@OperatorId,
			null,
			null,
			1,
			@Measure,
			@OrderMinimum,
			@OrderIncrement,
			@Handling,
			isnull(@Length, 0),
			isnull(@Height,0),
			isnull(@Width,0),
			isnull(@Weight,0),
			@Selfshippable,
			@AvailableForBackOrder,
			@BackOrderLimit,
			@IsUnlimitedQuantity,
			null,
			@FreeShipping,
			@UserDefinedPrice;

			if (exists(select * from PRProductSKU where Id = @ProductSKUId)) -- don't insert inventory record unless sku record exists
			Begin
			-- Check if the product is digital then insert to digital warehouse or else insert to all warehouses.

			
				IF Exists(Select 1 from PRProductType T INNER JOIN PRProduct P on T.Id = P.ProductTypeID Where P.Id =@ProductId And T.IsDownloadableMedia=1)
				BEGIN
				DECLARE @warehouseId uniqueidentifier
				Select @warehouseId = Id from INWarehouse Where [TypeId]=2
				INSERT INTO INInventory(
				   [Id]
				  ,[WarehouseId]
				  ,[SKUId]
				  ,[Quantity]
				  ,[LowQuantityAlert]
				  ,[FirstTimeOrderMinimum]
				  ,[OrderMinimum]
				  ,[OrderIncrement]
				  ,[ReasonId]
				  ,[ReasonDetail]
				  ,[Status]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate])
				SELECT
				newid(),
				@warehouseId,
				@ProductSKUId,
				0, 
				0, 
				1, 
				1, 
				1,
				'F2BB4870-3D47-497D-9810-D2B322C3C006',
				'Initial Data When SKU is created.',
				1,
				@OperatorId,
				GETDATE(),
				NULL, 
				NULL
				END
				ELSE
				BEGIN
				INSERT INTO INInventory(
				   [Id]
				  ,[WarehouseId]
				  ,[SKUId]
				  ,[Quantity]
				  ,[LowQuantityAlert]
				  ,[FirstTimeOrderMinimum]
				  ,[OrderMinimum]
				  ,[OrderIncrement]
				  ,[ReasonId]
				  ,[ReasonDetail]
				  ,[Status]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate])
				 SELECT
					newid(),
					Id,
					@ProductSKUId,
					0, 
					0, 
					1, 
					1, 
					1,
					'F2BB4870-3D47-497D-9810-D2B322C3C006',
					'Initial Data When SKU is created.',
					1,
					@OperatorId,
					GETDATE(),
					NULL, 
					NULL
					FROM INWarehouse
					Where TypeId!=2
				  
				END

			End

			set @actionType = 1

		end else
		begin
			update PRProductSKU set
			Title = isnull(@SKUTitle, Title),
			IsActive = isnull(@IsActive, IsActive),
			IsOnline = isnull(@IsOnline, IsOnline),
			ListPrice = isnull(@ListPrice, ListPrice),
			UnitCost = isnull(@UnitCost, UnitCost),
			ModifiedDate = GETDATE(),
			ModifiedBy = isnull(@OperatorId, ModifiedBy),
			Measure = isnull(@Measure, Measure),
			OrderMinimum = isnull(@OrderMinimum, OrderMinimum),
			OrderIncrement = isnull(@OrderIncrement, OrderIncrement),
			HandlingCharges = isnull(@Handling, HandlingCharges),
			Length = isnull(@Length, Length),
			Height = isnull(@Height, Height),
			Width = isnull(@Width, Width),
			Weight = isnull(@Weight, Weight),
			SelfShippable = isnull(@Selfshippable, SelfShippable),
			AvailableForBackOrder = isnull(@AvailableForBackOrder, AvailableForBackOrder),
			BackOrderLimit = isnull(@BackOrderLimit, BackOrderLimit),
			IsUnlimitedQuantity = isnull(@IsUnlimitedQuantity, IsUnlimitedQuantity),
			FreeShipping=@FreeShipping,
			UserDefinedPrice =@UserDefinedPrice
			where Id = @ProductSKUId;


			select @InventoryId = Id FROM INInventory where SKUId = @ProductSKUId;
			if @InventoryId is null
			begin
				INSERT INTO INInventory
				SELECT
				newid(),
				Id,
				@ProductSKUId,
				0, 0, 1, 1, 1,
				'F2BB4870-3D47-497D-9810-D2B322C3C006',
				'Initial Data When SKU is created.',
				1,
				@OperatorId,
				GETDATE(),
				NULL, NULL
				FROM INWarehouse
				Where TypeId!=2
			end

			set @actionType = 2

		end

		COMMIT TRAN

		select @actionType, @ProductId, @ProductSKUId

    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX)
        SET @ErrorMessage = ERROR_MESSAGE()
        RAISERROR(@ErrorMessage,16, 1)
    END CATCH

END
GO

IF(OBJECT_ID('Sku_GetByXml') IS NOT NULL)
	DROP PROCEDURE Sku_GetByXml
GO

CREATE PROCEDURE [dbo].[Sku_GetByXml](@SKUs xml=null,@SKUString xml=null,@ApplicationId uniqueidentifier=null)  
  
AS  
  
  IF @SKUs is not null
SELECT [Id]  
      ,[ProductId]  
      ,[SiteId]  
      ,[SKU]  
	  ,[Title]  
      ,[Description]  
      ,[Sequence]  
      ,[IsActive]  
      ,[IsOnline]  
      ,[ListPrice]  
      ,[UnitCost]  
      ,[PreviousSoldCount]  
	  ,[CreatedBy]  
	  ,CreatedDate AS [CreatedDate]  
	  ,[ModifiedBy]  
	  ,ModifiedDate AS [ModifiedDate]  
	  ,[Status]  
	  ,[Measure]  
	  ,[OrderMinimum]  
	  ,[OrderIncrement] 
	  ,[HandlingCharges] 
	  ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	  ,[IsUnlimitedQuantity]
	  ,FreeShipping
	,UserDefinedPrice
  FROM [PRProductSKU] S  
Inner Join @SKUs.nodes('/GenericCollectionOfGuid/guid')tab(col)   
ON  S.Id =tab.col.value('text()[1]','uniqueidentifier')  
ELSE
SELECT [Id]  
      ,[ProductId]  
      ,[SiteId]  
      ,[SKU]  
	  ,[Title]  
      ,[Description]  
      ,[Sequence]  
      ,[IsActive]  
      ,[IsOnline]  
      ,[ListPrice]  
      ,[UnitCost]  
      ,[PreviousSoldCount]  
	  ,[CreatedBy]  
	 ,CreatedDate AS [CreatedDate]  
	  ,[ModifiedBy]  
	  ,ModifiedDate AS [ModifiedDate]  
	  ,[Status]  
	  ,[Measure]  
	  ,[OrderMinimum]  
	  ,[OrderIncrement] 
	  ,[HandlingCharges] 
	  ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	  ,[IsUnlimitedQuantity]
	  ,FreeShipping
	 ,UserDefinedPrice
  FROM [PRProductSKU] S   
Inner Join @SKUString.nodes('/GenericCollectionOfString/string')tab(col)   
ON  S.SKU=tab.col.value('text()[1]','nvarchar(256)')

GO

IF(OBJECT_ID('Sku_GetSKUByGuid') IS NOT NULL)
	DROP PROCEDURE Sku_GetSKUByGuid
GO

CREATE PROCEDURE [dbo].[Sku_GetSKUByGuid](@skuId uniqueidentifier,@ApplicationId uniqueidentifier=null)  
  
AS  
  
SELECT	 S.[Id]  
		,S.[ProductId]  
		,S.[SiteId]  
		,S.[SKU]  
		,S.[Title]  
		,S.[Description]  
		,S.[Sequence]  
		,S.[IsActive]  
		,S.[IsOnline]  
		,ISNULL(SC.ListPrice,S.ListPrice)  ListPrice
		,S.[UnitCost]  
		,S.[PreviousSoldCount]  
		,S.[CreatedBy]  
		,S.CreatedDate AS CreatedDate  
		,S.[ModifiedBy]  
		,S. ModifiedDate AS ModifiedDate  
		,S.[Status]  
		,S.[Measure]  
		,S.[OrderMinimum]  
		,S.[OrderIncrement]
		,S.[HandlingCharges]  
		,S.[Length]
		,S.[Height]
		,S.[Width]
		,S.[Weight]
		,S.[SelfShippable]
		,S.[AvailableForBackOrder]
		,S.MaximumDiscountPercentage
		,S.[BackOrderLimit]
		,S.[IsUnlimitedQuantity]
		,S.FreeShipping
		,S.UserDefinedPrice
  FROM [PRProductSKU]  S
  LEFT JOIN PRProductSKUVariantCache SC ON S.Id = SC.Id AND SC.SiteId=@ApplicationId
  Where S.Id = @skuId

GO

IF(OBJECT_ID('Sku_GetSkuProperties') IS NOT NULL)
	DROP PROCEDURE Sku_GetSkuProperties

GO

CREATE PROCEDURE [dbo].[Sku_GetSkuProperties]( @SkuIds		xml,@ApplicationId uniqueidentifier=null)
AS
SELECT S.[Id]
      ,S.[ProductId]
      ,S.[SiteId]
      ,S.[SKU]
	  ,S.[Title]
      ,S.[Description]
      ,S.[Sequence]
      ,S.[IsActive]
      ,S.[IsOnline]
      ,S.[ListPrice]
      ,S.[UnitCost]
      ,S.[PreviousSoldCount]
	  ,S.[Measure]
	  ,S.[OrderMinimum]
	  ,S.[OrderIncrement]
	  ,S.[CreatedBy]
	  ,S.CreatedDate AS CreatedDate
	  ,S.[ModifiedBy]
	  ,S.ModifiedDate AS  ModifiedDate
	  ,S.[Status]
	  ,S.[HandlingCharges]
	  ,S.[Length]
	  ,S.[Height]
	  ,S.[Width]
	  ,S.[Weight]
	  ,S.[SelfShippable]
	  ,S.[AvailableForBackOrder]
	  ,S.MaximumDiscountPercentage
	  ,S.[BackOrderLimit]
	  ,S.[IsUnlimitedQuantity]
	  ,FreeShipping
	  ,UserDefinedPrice
   FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	Inner Join PRProductSKU S on S.Id =tab.col.value('text()[1]','uniqueidentifier')

GO

IF(OBJECT_ID('SKU_Save') IS NOT NULL)
	DROP PROCEDURE SKU_Save
	GO


CREATE PROCEDURE [dbo].[SKU_Save]  
(  
	 @ApplicationId uniqueidentifier,  
	 @ProductId uniqueidentifier,  
	 @ProductSKUXml xml,  
	 @CreatedBy uniqueidentifier,  
	 @ModifiedBy uniqueidentifier,   
	 @Id uniqueidentifier output  
)  
as  
begin  
 Declare @CurrentDate datetime  
 Declare @strId char(36)  
-- Declare @WarehouseId uniqueidentifier
--
-- Select top 1 @WarehouseId =Id from INWarehouse
 set @strId = @ProductSKUXml.value('(Sku/Id)[1]','char(36)')  
 set @CurrentDate = getutcdate()   
  
 if(@strId = '00000000-0000-0000-0000-000000000000' OR @strId='')  
 Begin    
  Set @Id = newid()  
  declare @currentSequence int  
  --select @currentSequence= max(Sequence) from PRProductSKU where ProductId=@ProductId  
  
   insert into PRProductSKU  
   (Id,  
    ProductId,  
    SiteId,  
	SKU, 
    Title,  
    Description, 
--    Sequence,  
    ListPrice,  
    UnitCost,  
    CreatedDate,  
    CreatedBy,  
    ModifiedDate,  
    ModifiedBy,  
    Status, 
	Sequence, 
	Measure,
	OrderMinimum,
	OrderIncrement, 
	HandlingCharges,
	IsActive,
	IsOnline,
	Length,
	Height,
	Width,
	Weight,
	SelfShippable,
	AvailableForBackOrder,
	MaximumDiscountPercentage,
	BackOrderLimit,
	IsUnlimitedQuantity,
	FreeShipping,
	UserDefinedPrice)  
   values  
   (@Id,  
    @ProductId,  
    @ApplicationId,  
	Isnull(@ProductSKUXml.value('(Sku/SKU)[1]','varchar(max)'), ' '),  
    Isnull(@ProductSKUXml.value('(Sku/Title)[1]','char(36)'), ' '),  
    Isnull(@ProductSKUXml.value('(Sku/Description)[1]','char(36)'), ' '),  
--@currentSequence+1,  
    @ProductSKUXml.value('(Sku/ListPrice)[1]','money'),  
    @ProductSKUXml.value('(Sku/UnitPrice)[1]','money'),  
    @CurrentDate,  
       @CreatedBy,  
    @CurrentDate,  
       @ModifiedBy,  
    @ProductSKUXml.value('(Sku/Status)[1]','int'), 
	(Isnull(dbo.GetMaxSKUSequence(@ProductId),0) + 1),
	Isnull(@ProductSKUXml.value('(Sku/Measure)[1]','varchar(max)'), ' '),  
    @ProductSKUXml.value('(Sku/OrderMinimum)[1]','money'),  
    @ProductSKUXml.value('(Sku/OrderIncrement)[1]','money'), 
    @ProductSKUXml.value('(Sku/HandlingCharges)[1]','money'), 
	case when lower(@ProductSKUXml.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end, 
	case when lower(@ProductSKUXml.value('(Sku/IsOnline)[1]','varchar(5)'))='false' then 0 else 1 end, 
	@ProductSKUXml.value('(Sku/Length)[1]','decimal(18,4)'),
	@ProductSKUXml.value('(Sku/Height)[1]','decimal(18,4)'),
	@ProductSKUXml.value('(Sku/Width)[1]','decimal(18,4)'),
	@ProductSKUXml.value('(Sku/Weight)[1]','decimal(18,4)'),
	case when lower(@ProductSKUXml.value('(Sku/SelfShippable)[1]','varchar(5)'))='false' then 0 else 1 end,
	case when lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else 1 end,
	 @ProductSKUXml.value('(Sku/MaximumDiscountPercentage)[1]','money'), 
	case when  lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else @ProductSKUXml.value('(Sku/BackOrderLimit)[1]','decimal(18,3)') end,
	case when lower(@ProductSKUXml.value('(Sku/IsUnlimitedQuantity)[1]','varchar(5)'))='false' then 0 else 1 end,
	case when lower(@ProductSKUXml.value('(Sku/FreeShipping)[1]','varchar(5)'))='false' then 0 else 1 end,
	case when lower(@ProductSKUXml.value('(Sku/UserDefinedPrice)[1]','varchar(5)'))='false' then 0 else 1 end
	)   

  --Associate attributes to the Product SKU and insert attribute values   
  declare @iCount int  
  declare @i int  
  declare @attributeId varchar(36)  
  
  set @iCOunt = @ProductSKUXml.value('count(/Sku/Attributes/ProductAttribute)','int')  
  set @i = 1  
    
  while (@i <= @iCOunt)  
  begin  
   set  @attributeId = @ProductSKUXml.value('(/Sku/Attributes/ProductAttribute[sql:variable("@i")]/Id)[1]','uniqueidentifier')  
     
     insert into PRProductAttribute  
   (Id, ProductId, AttributeId,Sequence,IsSKULevel)  
     Values( newid(), @Id, @attributeId, @i, 1)    
     
      
   insert into PRProductSKUAttributeValue  
   (Id, SKUId, AttributeId,AttributeEnumId,[Value],CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)  
   SELECT newid(),  
       @Id,  
       @attributeId,  
       T.c.value('(AttributeEnumId)[1]','varchar(36)'),    
--		case A.AttributeDataType
--		 When 'System.DateTime' 
--		  Then	
--			Case ISDate(T.c.value('(Value)[1]','varchar(max)')) When  1
--				THEN  convert(varchar(max),dbo.ConvertTimeToUtc(T.c.value('(Value)[1]','varchar(max)'),@ApplicationId),126)
--			    ELSE T.c.value('(Value)[1]','varchar(max)')
--				END
--		   Else	
		T.c.value('(Value)[1]','varchar(max)')
		--End	
		,    
       @CurrentDate,  
       @CreatedBy,  
       @CurrentDate,  
       @ModifiedBy,  
       T.c.value('(Status)[1]','varchar(50)')  
   FROM @ProductSKUXml.nodes('/Sku/Attributes/ProductAttribute[sql:variable("@i")]/Values/AttributeItem') T(c)  
	---INNER JOIN ATAttribute A on A.Id = T.c.value('(AttributeId)[1]','uniqueidentifier')
  
   SET @i = @i + 1  
  end   
 end  
 else  
 begin  
  set @Id= @ProductSKUXml.value('(Sku/Id)[1]','uniqueidentifier')  
    
  update PRProductSKU   
  set  ProductId=@ProductId,  
    SiteId=@ApplicationId,  
	SKU = Isnull(@ProductSKUXml.value('(Sku/SKU)[1]','varchar(max)'), ' '), 
    Title= Isnull(@ProductSKUXml.value('(Sku/Title)[1]','char(36)'), ' '),  
    Description= Isnull(@ProductSKUXml.value('(Sku/Description)[1]','char(36)'), ' '),  
    Sequence=@ProductSKUXml.value('(Sku/Sequence)[1]','int'),  
    ListPrice =@ProductSKUXml.value('(Sku/ListPrice)[1]','money'),  
    UnitCost=@ProductSKUXml.value('(Sku/UnitPrice)[1]','money'),     
    ModifiedDate =@CurrentDate,  
    ModifiedBy=@ModifiedBy,  
    Status = @ProductSKUXml.value('(Sku/Status)[1]','int'), 
	Measure = Isnull(@ProductSKUXml.value('(Sku/Measure)[1]','varchar(max)'), ' ') ,
	OrderMinimum = @ProductSKUXml.value('(Sku/OrderMinimum)[1]','money'), 
	OrderIncrement = @ProductSKUXml.value('(Sku/OrderIncrement)[1]','money'),
	HandlingCharges = @ProductSKUXml.value('(Sku/HandlingCharges)[1]','money'),
	IsActive=case when lower(@ProductSKUXml.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end, 
	IsOnline=case when lower(@ProductSKUXml.value('(Sku/IsOnline)[1]','varchar(5)'))='false' then 0 else 1 end,
	Length=@ProductSKUXml.value('(Sku/Length)[1]','decimal(18,4)'),	
	Height=@ProductSKUXml.value('(Sku/Height)[1]','decimal(18,4)'),
	Width=@ProductSKUXml.value('(Sku/Width)[1]','decimal(18,4)'),
	Weight=@ProductSKUXml.value('(Sku/Weight)[1]','decimal(18,4)'),
	SelfShippable =case when lower(@ProductSKUXml.value('(Sku/SelfShippable)[1]','varchar(5)'))='false' then 0 else 1 end,
	AvailableForBackOrder =case when lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else 1 end,
	MaximumDiscountPercentage =  @ProductSKUXml.value('(Sku/MaximumDiscountPercentage)[1]','money'), 
	BackOrderLimit=case when  lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else @ProductSKUXml.value('(Sku/BackOrderLimit)[1]','decimal(18,3)') end,
	IsUnlimitedQuantity=case when lower(@ProductSKUXml.value('(Sku/IsUnlimitedQuantity)[1]','varchar(5)'))='false' then 0 else 1 end,
	FreeShipping=case when lower(@ProductSKUXml.value('(Sku/FreeShipping)[1]','varchar(5)'))='false' then 0 else 1 end,
	UserDefinedPrice=case when lower(@ProductSKUXml.value('(Sku/UserDefinedPrice)[1]','varchar(5)'))='false' then 0 else 1 end
	
  where Id =@Id  
    		-- make product active/inactive if the sku is that of bundle i.e. parent is a bundle
	Update PRProduct 
	set IsActive=
			case when lower(@ProductSKUXml.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end 
	where IsBundle =1 and Id=@ProductId
  --delete from PRProductSKUAttributeValue where SKUId =@Id  
  --delete from PRProductAttribute where ProductId =@ProductId and IsSKULevel=1  
 End      
   
EXEC [dbo].[Product_UpdateProductProperties] @ProductId=@ProductId,@ModifiedBy=@ModifiedBy	
end

GO

IF(OBJECT_ID('SKU_SaveMulti') IS NOT NULL)
	DROP PROCEDURE SKU_SaveMulti
GO

CREATE PROCEDURE [dbo].[SKU_SaveMulti]  
(  
	 @ApplicationId uniqueidentifier,  
	 @ProductSKUXml xml,  
	 @SkuIds		xml,	
	 @ProductId		uniqueidentifier,
	 @ModifiedBy uniqueidentifier   
)  
as  
begin  
 Declare @CurrentDate datetime  
 Declare @strId char(36)  
-- Declare @WarehouseId uniqueidentifier
--
-- Select top 1 @WarehouseId =Id from INWarehouse

 set @CurrentDate = getutcdate()   
	
	Declare @InsertSkus table(SkuId uniqueidentifier)
	Declare @UpdateSkus table(SkuId uniqueidentifier)
	Declare @InsertAttributes table(Id uniqueidentifier,SKUId uniqueidentifier,AttributeId uniqueidentifier)
	Declare @UpdateAttributes table(Id uniqueidentifier,SKUId uniqueidentifier,AttributeId uniqueidentifier)
	

	Insert into @UpdateSkus(SkuId)
	Select Distinct S.Id
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	Inner Join PRProductSKU S on S.Id =tab.col.value('text()[1]','uniqueidentifier')
	

	Insert into @InsertSkus
	Select CASE WHEN tab.col.value('text()[1]','uniqueidentifier') = dbo.GetEmptyGUID() THEN newid() else tab.col.value('text()[1]','uniqueidentifier') END
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	except
	Select SkuId from @UpdateSkus
	

 
   insert into PRProductSKU  
   (Id,  
    ProductId,  
    SiteId,  
	SKU, 
    Title,  
    Description, 
    ListPrice,  
    UnitCost,  
    CreatedDate,  
    CreatedBy,  
    Status, 
	Sequence, 
	Measure,
	OrderMinimum,
	OrderIncrement,
	HandlingCharges,
	IsActive,
	IsOnline,
	Length,
	Height,
	Width,
	Weight,
	SelfShippable,
	AvailableForBackOrder,
	MaximumDiscountPercentage,
	BackOrderLimit,
	IsUnlimitedQuantity,
	FreeShipping,
	UserDefinedPrice)  
   Select   
	I.SkuId,  
    @ProductId,  
    @ApplicationId,  
	Isnull(tab.col.value('(SKU)[1]','varchar(max)'), ' '),  
    Isnull(tab.col.value('(Title)[1]','char(36)'), ' '),  
    Isnull(tab.col.value('(Description)[1]','char(36)'), ' '),  
    tab.col.value('(ListPrice)[1]','money'),  
    tab.col.value('(UnitPrice)[1]','money'),  
    @CurrentDate,  
    @ModifiedBy,  
    tab.col.value('(Status)[1]','int'), 
	(Isnull(dbo.GetMaxSKUSequence(@ProductId),0) + 1),
	Isnull(tab.col.value('(Measure)[1]','varchar(max)'), ' '),  
    tab.col.value('(OrderMinimum)[1]','money'),  
    tab.col.value('(OrderIncrement)[1]','money'),
    tab.col.value('(HandlingCharges)[1]','money'),    
	case when (tab.col.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end, 
	case when (tab.col.value('(Sku/IsOnline)[1]','varchar(5)'))='false' then 0 else 1 end,
	tab.col.value('(Sku/Length)[1]','decimal(18,4)'),
	tab.col.value('(Sku/Height)[1]','decimal(18,4)'),
	tab.col.value('(Sku/Width)[1]','decimal(18,4)'),
	tab.col.value('(Sku/Weight)[1]','decimal(18,4)'),
	case when lower(tab.col.value('(Sku/SelfShippable)[1]','varchar(5)'))='false' then 0 else 1 end,
	case when lower(tab.col.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else 1 end,
	tab.col.value('(MaximumDiscountPercentage)[1]','money'),  
	case when lower(tab.col.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else tab.col.value('(Sku/BackOrderLimit)[1]','decimal(18,3)') end,
	case when lower(tab.col.value('(Sku/IsUnlimitedQuantity)[1]','varchar(5)'))='false' then 0 else 1 end,
	case when lower(tab.col.value('(Sku/FreeShipping)[1]','varchar(5)'))='false' then 0 else 1 end,
	case when lower(tab.col.value('(Sku/UserDefinedPrice)[1]','varchar(5)'))='false' then 0 else 1 end
	From @InsertSkus I 
	INNER JOIN @ProductSKUXml.nodes('/Sku')tab(col)	
				ON  I.SkuId =tab.col.value('(/Id)[1]','uniqueidentifier')

  update S   
  set  ProductId=@ProductId,  
    SiteId=@ApplicationId,  
	SKU = Isnull(tab.col.value('(SKU)[1]','varchar(max)'), ' '), 
    Title= Isnull(tab.col.value('(Title)[1]','char(36)'), ' '),  
    Description= Isnull(tab.col.value('(Description)[1]','char(36)'), ' '),  
    Sequence=tab.col.value('(Sequence)[1]','int'),  
    ListPrice =tab.col.value('(ListPrice)[1]','money'),  
    UnitCost=tab.col.value('(UnitPrice)[1]','money'),     
    ModifiedDate =@CurrentDate,  
    ModifiedBy=@ModifiedBy,  
    Status = tab.col.value('(Status)[1]','int'), 
	Measure = Isnull(tab.col.value('(Measure)[1]','varchar(max)'), ' ') ,
	OrderMinimum = tab.col.value('(OrderMinimum)[1]','money'), 
	OrderIncrement = tab.col.value('(OrderIncrement)[1]','money'),
	HandlingCharges = tab.col.value('(HandlingCharges)[1]','money'),
	IsActive = case when (tab.col.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end, 
	IsOnline = case when (tab.col.value('(Sku/IsOnline)[1]','varchar(5)'))='false' then 0 else 1 end,
	Length=tab.col.value('(Sku/Length)[1]','decimal(18,4)'),	
	Height=tab.col.value('(Sku/Height)[1]','decimal(18,4)'),
	Width=tab.col.value('(Sku/Width)[1]','decimal(18,4)'),
	Weight=tab.col.value('(Sku/Weight)[1]','decimal(18,4)'),
	SelfShippable =case when lower(tab.col.value('(Sku/SelfShippable)[1]','varchar(5)'))='false' then 0 else 1 end,    
	AvailableForBackOrder =case when lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else 1 end,
	MaximumDiscountPercentage = tab.col.value('(MaximumDiscountPercentage)[1]','money'),
	BackOrderLimit=case when lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else @ProductSKUXml.value('(Sku/BackOrderLimit)[1]','decimal(18,3)') end,
	IsUnlimitedQuantity =case when lower(tab.col.value('(Sku/IsUnlimitedQuantity)[1]','varchar(5)'))='false' then 0 else 1 end,
	FreeShipping =case when lower(tab.col.value('(Sku/FreeShipping)[1]','varchar(5)'))='false' then 0 else 1 end,
	UserDefinedPrice =case when lower(tab.col.value('(Sku/UserDefinedPrice)[1]','varchar(5)'))='false' then 0 else 1 end
	From PRProductSKU S
	Inner Join @UpdateSkus U on S.Id = U.SkuId 
	INNER JOIN @ProductSKUXml.nodes('/Sku')tab(col)	
				ON  U.SkuId =tab.col.value('(Id)[1]','uniqueidentifier')
  
  
  		-- make product active/inactive if the sku is that of bundle i.e. parent is a bundle
	Update PRProduct 
	set IsActive=
			case when lower(@ProductSKUXml.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end 
	where IsBundle =1 and Id=@ProductId


--  Now @InsertSkus has all the sku Id's
	Insert into @InsertSkus
	Select SkuId from @UpdateSkus

	Delete from PRProductSKUAttributeValue
	Where SKUId in (Select SkuId from @UpdateSkus)

	Delete from PRProductAttribute
	Where ProductId=@ProductId and IsSKULevel=1	
	AND AttributeId in 
	(Select T.c.value('(AttributeId)[1]','uniqueidentifier') FROM @ProductSKUXml.nodes('//AttributeItem') T(c))
	
	Declare @tempProductAttribute table(Sequence int identity(1,1),AttributeId Uniqueidentifier,IsRequired bit)
	Insert into @tempProductAttribute(AttributeId,IsRequired)
	Select T.c.value('(AttributeId)[1]','uniqueidentifier'),case when (T.c.value('(IsRequired)[1]','varchar(5)'))='false' then 0 else 1 end 
	From @ProductSKUXml.nodes('//AttributeItem') T(c)

	insert into PRProductAttribute(Id, ProductId, AttributeId,Sequence,IsSKULevel,IsRequired)
	Select newid(),@ProductId,AttributeId,Sequence,1,IsRequired
	From @tempProductAttribute
	

	Insert into PRProductSKUAttributeValue(Id,SKUId,AttributeId,AttributeEnumId,Value,CreatedDate,CreatedBy,Status)
	SELECT newid(),  
       S.SkuId,  
        T.c.value('(AttributeId)[1]','uniqueidentifier'),
       T.c.value('(AttributeEnumId)[1]','uniqueidentifier'),    
--      case A.AttributeDataType
--		 When 'System.DateTime' 
--		  Then	
--			Case ISDate(T.c.value('(Value)[1]','varchar(max)')) When  1
--				THEN  convert(varchar(max),dbo.ConvertTimeToUtc(T.c.value('(Value)[1]','varchar(max)'),@ApplicationId),126)
--			    ELSE T.c.value('(Value)[1]','varchar(max)')
--				END
--		   Else	
		T.c.value('(Value)[1]','varchar(max)')
--		End	
		,   
       @CurrentDate,  
       @ModifiedBy,  
       T.c.value('(Status)[1]','int')  
	FROM @ProductSKUXml.nodes('//AttributeItem') T(c)
	--INNER JOIN ATAttribute A on A.Id = T.c.value('(AttributeId)[1]','uniqueidentifier')
	CROSS JOIN @InsertSkus S
	
EXEC [dbo].[Product_UpdateProductProperties] @ProductId=@ProductId,@ModifiedBy=@ModifiedBy
end

GO

IF(OBJECT_ID('Sku_SaveSKUAndAttributes') IS NOT NULL)
	DROP PROCEDURE Sku_SaveSKUAndAttributes
GO
CREATE PROCEDURE [dbo].[Sku_SaveSKUAndAttributes]  
(  
	@Id				uniqueidentifier OUTPUT,  
    @ProductId		uniqueidentifier,
    @ApplicationId uniqueidentifier,  
	@SKU			nvarchar(255)=null, 
    @Title			nvarchar(255)=null,  
    @Description	text='', 
    @ListPrice		money,		
    @UnitPrice		money,  
    @Status			int, 
	@Measure		nvarchar(50),
	@OrderMinimum	money,
	@OrderIncrement	money,
	@HandlingCharges money = 0,
	@SystemAttributes xml,  
	@SKUAttributes xml,  
	@IsActive bit,
	@IsOnline bit,
	@SkuIds		xml out,	
	@ModifiedBy uniqueidentifier,
	@Length	decimal(18,4)=null,   
	@Height	decimal(18,4)=null,   
	@Width	decimal(18,4)=null,
	@Weight decimal(18,4)=null,
	@SelfShippable bit=null,
	@AvailableForBackOrder bit =null,
	@MaximumDiscountPercentage money = null,
	@BackOrderLimit decimal(18,3)=null,
	@IsUnlimitedQuantity bit = null,
	@FreeShipping bit = null,
	@UserDefinedPrice bit = null
)  
as  
begin  
 Declare @CurrentDate datetime  
 Declare @strId char(36)  

 set @CurrentDate = getutcdate()   
	
	Declare @InsertSkus table(SkuId uniqueidentifier)
	Declare @UpdateSkus table(SkuId uniqueidentifier)
	Declare @tempProductSKUAttribute table(Sequence int identity(1,1),AttributeId Uniqueidentifier,IsSKULevel bit,IsSystem bit,IsRequired bit)

	Insert into @UpdateSkus(SkuId)
	Select Distinct S.Id
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	Inner Join PRProductSKU S on S.Id =tab.col.value('text()[1]','uniqueidentifier')
	

	Insert into @InsertSkus
	Select CASE WHEN tab.col.value('text()[1]','uniqueidentifier') = dbo.GetEmptyGUID() THEN newid() else tab.col.value('text()[1]','uniqueidentifier') END
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	except
	Select SkuId from @UpdateSkus
	
	IF (Select count(*) From @InsertSkus)>0
	BEGIN
		IF Exists(Select Id from PRProductSKU Where (SKU) =(@SKU))
		BEGIN
			RAISERROR('Sku already exists, Please choose a different Sku.', 16, 1)
			Return
		END		
	END
   insert into PRProductSKU  
   (Id,  
    ProductId,  
    SiteId,  
	SKU, 
    Title,  
    Description, 
    ListPrice,  
    UnitCost,  
    CreatedDate,  
    CreatedBy,  
    Status, 
	Sequence, 
	Measure,
	OrderMinimum,
	OrderIncrement,
	HandlingCharges,
	IsActive,
	IsOnline,
	Length,
	Height,
	Width,
	Weight,
	SelfShippable,
	AvailableForBackOrder,
	MaximumDiscountPercentage,
	BackOrderLimit,
	IsUnlimitedQuantity,
	FreeShipping,
	UserDefinedPrice)  
   Select   
	I.SkuId,  
    @ProductId,  
    @ApplicationId,  
	@SKU,  
    @Title,  
    @Description,  
    @ListPrice,  
    @UnitPrice,  
    @CurrentDate,  
    @ModifiedBy,  
    @Status, 
	dbo.GetMaxSKUSequence(@ProductId) + 1,
	--(Isnull(dbo.GetMaxSKUSequence(@ProductId),0) + 1),
	@Measure,  
    @OrderMinimum,  
    @OrderIncrement,
    @HandlingCharges,
	@IsActive,
	@IsOnline,
	@Length,   
	@Height,   
	@Width,
	@Weight,
	@SelfShippable,   
	@AvailableForBackOrder, 
	@MaximumDiscountPercentage,
	case when @AvailableForBackOrder=0 then 0 else @BackOrderLimit end,
	@IsUnlimitedQuantity,
	@FreeShipping,
	@UserDefinedPrice
	From @InsertSkus I 
	
  
  update S   
  set  ProductId=@ProductId,  
    SiteId=@ApplicationId,  
	SKU = case when @SKU is not null then @SKU else sku end,
	--isnull(@SKU,SKU), 
    Title = case when @Title is not null then @Title else Title end, 
	--isnull(@Title,Title),  
    Description=@Description,  
    ListPrice =@ListPrice,  
    UnitCost=@UnitPrice,     
    ModifiedDate =@CurrentDate,  
    ModifiedBy=@ModifiedBy,  
    Status = @Status, 
	Measure = @Measure,
	OrderMinimum = @OrderMinimum, 
	OrderIncrement = @OrderIncrement,
	HandlingCharges = @HandlingCharges,
	IsActive=@IsActive,
	IsOnline=@IsOnline,
	Length=@Length,
	Height=@Height,
	Width= @width,
	Weight=@Weight,
	SelfShippable=@SelfShippable,
	AvailableForBackOrder=@AvailableForBackOrder,
	MaximumDiscountPercentage = @MaximumDiscountPercentage,
	BackOrderLimit=case when @AvailableForBackOrder=0 then 0 else @BackOrderLimit end,
	IsUnlimitedQuantity=@IsUnlimitedQuantity,
	FreeShipping =@FreeShipping,
	UserDefinedPrice =@UserDefinedPrice
	From PRProductSKU S
	Inner Join @UpdateSkus U on S.Id = U.SkuId 
	
	-- make product active/inactive if the sku is that of bundle i.e. parent is a bundle
	Update PRProduct 
	set IsActive=@IsActive
	where IsBundle =1 and Id=@ProductId
	
--  Now @InsertSkus has all the sku Id's
	Insert into @InsertSkus
	Select SkuId from @UpdateSkus

	

	delete from PRProductSKUAttributeValue
	Where SKUId in (Select SkuId from @UpdateSkus)
	AND AttributeId in -- to not delete the attribute value when 'Multi' is selected
	(
		Select T.c.value('(@Id)[1]','uniqueidentifier')
		FROM @SKUAttributes.nodes('//ProductAttribute') T(c)
		UNION ALL
		Select tab.col.value('text()[1]','uniqueidentifier')
		From @SystemAttributes.nodes('dictionary/SerializableDictionary/item/key/guid') tab(col)
	)


	Insert into @tempProductSKUAttribute(AttributeId,IsSKULevel,IsSystem,IsRequired)
	Select tab.col.value('text()[1]','uniqueidentifier'),1,1,0
	From @SystemAttributes.nodes('dictionary/SerializableDictionary/item/key/guid') tab(col)
	UNION ALL
	Select Distinct T.c.value('(@Id)[1]','uniqueidentifier'),
	1,0,case when T.c.value('(@IsRequired)[1]','varchar(5)') ='true' then 1 else 0 end 
	From @SKUAttributes.nodes('//ProductAttribute') T(c) 
	---WHERE T.c.value('(IsSKULevel)[1]','varchar(5)') ='true'  
	 

	insert into PRProductAttribute(Id, ProductId, AttributeId,Sequence,IsSKULevel,IsRequired)
	Select newid(),@ProductId,AttributeId,Sequence,IsSKULevel,IsRequired
	From @tempProductSKUAttribute
	WHERE IsSKULevel=1 AND IsSystem=0
	AND AttributeId not in 
		(Select AttributeId from PRProductAttribute Where ProductId=@ProductId AND IsSKULevel=1)

	
	Declare @tempProductAttribute table(Sequence int identity(1,1),AttributeId Uniqueidentifier,IsRequired bit)

	Insert into @tempProductAttribute(AttributeId,IsRequired)
	Select tab.col.value('text()[1]','uniqueidentifier'),0
	From @SystemAttributes.nodes('dictionary/SerializableDictionary/item/key/guid') tab(col)
	
	UNION ALL
	Select Distinct T.c.value('(@Id)[1]','uniqueidentifier'),
	case when T.c.value('(@IsRequired)[1]','varchar(5)') ='true' then 1 else 0 end 
	From @SKUAttributes.nodes('//ProductAttribute') T(c) 
	--WHERE T.c.value('(IsSKULevel)[1]','varchar(5)') ='true'  

	insert into PRProductAttribute(Id, ProductId, AttributeId,Sequence,IsSKULevel,IsRequired)
	Select newid(),@ProductId,AttributeId,Sequence,1,IsRequired
	From @tempProductAttribute Where  AttributeId not in 
		(Select AttributeId from PRProductAttribute Where ProductId=@ProductId AND IsSKULevel=1)
	
	Insert into PRProductSKUAttributeValue
	(
		Id,
		ProductAttributeId,
		SKUId,
		AttributeId,
		AttributeEnumId,
		Value,
		CreatedDate,
		CreatedBy,
		Status)
	SELECT newid(), 
			Id,
			SkuId,
			AttributeId,
			AttributeEnumId,
			Value,
	@CurrentDate,  
       @ModifiedBy,  
       [Status]
	From
		(Select distinct
		PA.Id, 
       S.SkuId,  
       T.c.value('(@AttributeId)[1]','uniqueidentifier') AttributeId,
       case when T.c.value('(@AttributeEnumId)[1]','uniqueidentifier')=dbo.GetEmptyGUID() Then null ELSE T.c.value('(@AttributeEnumId)[1]','uniqueidentifier') END AttributeEnumId ,    
		T.c.value('(@Value)[1]','varchar(max)') Value,    
       1 Status--T.c.value('(Status)[1]','int')    Status
	FROM @SKUAttributes.nodes('//ProductAttribute') T(c)
	INNER JOIN PRProductAttribute PA ON (PA.ProductId = @ProductId AND PA.AttributeId = T.c.value('(@AttributeId)[1]','uniqueidentifier') AND IsSKULevel=1)
	--INNER JOIN ATAttribute A on A.Id = T.c.value('(AttributeId)[1]','uniqueidentifier')
	CROSS JOIN @InsertSkus S
	where T.c.value('(@Value)[1]','varchar(max)') IS NOT NULL
	--Where T.c.value('(Value)[1]','varchar(max)') is not null
	--AND  T.c.value('(parent::*/parent::*/IsSystem)[1]','varchar(5)')  ='false' 
	--And T.c.value('(parent::*/parent::*/IsSKULevel)[1]','varchar(5)') ='true'
	)VV

Insert into PRProductSKUAttributeValue
	(
	Id,
	ProductAttributeId,
	SKUId,
	AttributeId,
	AttributeEnumId,
	Value,
	CreatedDate,
	CreatedBy,
	Status
	)
	Select	
	newid(),
	PA.Id,
	S.SkuId,
	tab.col.value('(key/guid/text())[1]','uniqueidentifier'),
	E.Id,
	E.Value,
	@CurrentDate,
	@ModifiedBy	,
	dbo.GetActiveStatus()
	From @SystemAttributes.nodes('dictionary/SerializableDictionary/item') tab(col)
		Inner Join ATAttributeEnum E 
			ON tab.col.value('(key/guid/text())[1]','uniqueidentifier') = E.AttributeID
		INNER JOIN PRProductAttribute PA ON (PA.ProductId = @ProductId AND PA.AttributeId = tab.col.value('(key/guid/text())[1]','uniqueidentifier') AND IsSKULevel=1)
		CROSS JOIN @InsertSkus S
		Where (tab.col.value('(value/boolean/text())[1]','varchar(5)')= 'true' AND E.Code='1')
			OR ((tab.col.value('(value/boolean/text())[1]','varchar(5)'))= 'false' AND E.Code='0')
	
Declare @char varchar(max)
Set @char ='<GenericCollectionOfGuid>'
Select @char = @char + '<guid>' + cast(SkuId as char(36))  + '</guid>' from @InsertSkus
Set @char = @char + '</GenericCollectionOfGuid>'
Set @SkuIds = cast(@char as xml)

EXEC [dbo].[Product_UpdateProductProperties] @ProductId=@ProductId,@ModifiedBy=@ModifiedBy

end
GO

PRINT 'Create view vwOrder'
GO

IF(OBJECT_ID('vwOrder') IS NOT NULL)
	DROP VIEW vwOrder
GO

CREATE View vwOrder
AS
SELECT	O.Id, O.CartId, O.OrderTypeId, O.OrderStatusId, O.SiteId, O.OrderDate, O.PurchaseOrderNumber, O.ExternalOrderNumber,
		O.CustomerId, CASE WHEN LEN(LTRIM(C.LastName)) = 0 THEN ''  ELSE C.LastName + ', ' END + C.FirstName AS CustomerName, C.Email AS CustomerEmail,
		O.OrderTotal, O.TaxableOrderTotal, O.TotalTax, O.TotalShippingDiscount, O.TotalShippingCharge, O.TotalDiscount, O.CODCharges, O.PaymentTotal, O.PaymentRemaining, O.RefundTotal, O.GrandTotal,
		O.CreatedDate, O.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
		O.ModifiedDate, O.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
FROM	OROrder AS O
		LEFT JOIN vwCustomer AS C ON C.Id = O.CustomerId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = O.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = O.ModifiedBy
GO





IF(OBJECT_ID('OrderPaymentListDto_Get') IS NOT NULL)
	DROP PROCEDURE OrderPaymentListDto_Get
GO

CREATE PROCEDURE [dbo].[OrderPaymentListDto_Get]
	@OrderId				UNIQUEIDENTIFIER = NULL,
	@CustomerId				UNIQUEIDENTIFIER = NULL,
	@ProcessorTransactionId	NVARCHAR(256) = NULL,
	@PaymentTypeId			INT = NULL,
	@PaymentStatusId		INT = NULL,
	@IsRefund				BIT = NULL,
	@StartDate				DATETIME = NULL,
	@EndDate				DATETIME = NULL,
	@PageNumber				INT = NULL,
	@PageSize				INT = NULL,
	@TotalRecords			INT = NULL OUTPUT
AS
BEGIN
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	DECLARE	@StartRow	INT,
			@EndRow		INT

	IF @PageNumber IS NOT NULL AND @PageSize IS NOT NULL
	BEGIN
		SET @StartRow = @PageSize * (@PageNumber - 1) + 1
		SET @EndRow = @StartRow + @PageSize - 1
	END

	SELECT	OP.Id, OP.OrderId, OP.PaymentId, OP.[Sequence], P.PaymentTypeId AS PaymentType, P.PaymentStatusId AS PaymentStatus, P.ProcessorTransactionId,
			P.IsRefund, RE.Comments AS RefundReason, P.Amount, P.CapturedAmount, COALESCE(PR.AllocatedRefundAmount, 0) AS AllocatedRefundAmount,
			O.PurchaseOrderNumber, P.CustomerId, CASE WHEN LEN(LTRIM(C.LastName)) = 0 THEN ''  ELSE C.LastName + ', ' END + C.FirstName AS CustomerName,
			CCT.Title AS CreditCardType, CCI.NameOnCard AS CreditCardName,
			CONVERT(VARCHAR(MAX), DecryptByKey(CCI.Number)) AS CreditCardNumber,
			CAST(CONVERT(VARCHAR(2), DecryptByKey(CCI.ExpirationMonth)) AS INT) AS CreditCardExpirationMonth,
			CAST(CONVERT(VARCHAR(4), DecryptByKey(CCI.ExpirationYear)) AS INT) AS CreditCardExpirationYear,
			GCI.GiftCardNumber AS GiftCardNumber,
			CASE P.PaymentTypeId
				WHEN 1 THEN PCC.BillingAddressId
				WHEN 2 THEN LOC.BillingAddressId
				WHEN 3 THEN PPI.BillingAddressId
				WHEN 4 THEN CODI.BillingAddressId
				WHEN 5 THEN GCI.BillingAddressId
			END AS BillingAddressId,
			P.CreatedDate, P.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName, P.ModifiedDate, P.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName,
			ROW_NUMBER() OVER (ORDER BY OP.Sequence DESC) AS RowNumber
	INTO	#OrderPayments
	FROM	PTOrderPayment AS OP
			INNER JOIN PTPayment AS P ON P.Id = OP.PaymentId
			INNER JOIN OROrder AS O ON O.Id = OP.OrderId
			INNER JOIN vwCustomer AS C ON C.Id = P.CustomerId
			LEFT JOIN PTRefundPayment AS RP ON RP.RefundOrderPaymentId = OP.Id
			LEFT JOIN ORRefund AS RE ON RE.Id = RP.RefundId
			LEFT JOIN (
				SELECT		RP.OrderPaymentId, SUM(RP.RefundAmount) AS AllocatedRefundAmount
				FROM		PTRefundPayment AS RP
							INNER JOIN PTOrderPayment AS ROP ON ROP.Id = RP.RefundOrderPaymentId
							INNER JOIN PTPayment AS P ON P.Id = ROP.PaymentId
				WHERE		P.PaymentStatusId NOT IN (7, 14)
				GROUP BY	RP.OrderPaymentId
			) AS PR ON PR.OrderPaymentId = OP.Id
			LEFT JOIN PTPaymentCreditCard AS PCC ON P.PaymentTypeId = 1 AND PCC.PaymentId = P.Id
			LEFT JOIN PTPaymentInfo AS CCI ON P.PaymentTypeId = 1 AND CCI.Id = PCC.PaymentInfoId
			LEFT JOIN PTCreditCardType AS CCT ON P.PaymentTypeId = 1 AND CCT.Id = CCI.CardType
			LEFT JOIN PTPaymentLineOfCredit AS PLOC ON P.PaymentTypeId = 2 AND PLOC.PaymentId = P.Id
			LEFT JOIN PTLineOfCreditInfo AS LOC ON P.PaymentTypeId = 2 AND LOC.Id = PLOC.LOCId
			LEFT JOIN PTPaymentPaypal AS PPP ON P.PaymentTypeId = 3 AND PPP.PaymentId = P.Id
			LEFT JOIN PTPaypalInfo AS PPI ON P.PaymentTypeId = 3 AND PPI.Id = PPP.PaypalInfoId
			LEFT JOIN PTPaymentCOD AS PCOD ON P.PaymentTypeId = 4 AND PCOD.PaymentId = P.Id
			LEFT JOIN PTCODInfo AS CODI ON P.PaymentTypeId = 4 AND CODI.Id = PCOD.CODId
			LEFT JOIN PTPaymentGiftCard AS PGC ON P.PaymentTypeId = 5 AND PGC.PaymentId = P.Id
			LEFT JOIN PTGiftCardInfo AS GCI ON P.PaymentTypeId = 5 AND GCI.Id = PGC.GiftCardId
			LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = P.CreatedBy
			LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = P.ModifiedBy
	WHERE	(@OrderId IS NULL OR OP.OrderId = @OrderId) AND
			(@CustomerId IS NULL OR P.CustomerId = @CustomerId) AND
			(@ProcessorTransactionId IS NULL OR P.ProcessorTransactionId = @ProcessorTransactionId) AND
			(@PaymentTypeId IS NULL OR P.PaymentTypeId = @PaymentTypeId) AND
			(@PaymentStatusId IS NULL OR P.PaymentStatusId = @PaymentStatusId) AND
			(@IsRefund IS NULL OR P.IsRefund = @IsRefund) AND
			(@StartDate IS NULL OR COALESCE(P.ClearDate, P.ModifiedDate, P.CreatedDate) >= @StartDate) AND
			(@EndDate IS NULL OR COALESCE(P.ClearDate, P.ModifiedDate, P.CreatedDate) <= @EndDate)
	
	SELECT	*
	FROM	#OrderPayments
	WHERE	@StartRow IS NULL OR
			@EndRow IS NULL OR 
			RowNumber BETWEEN @StartRow AND @EndRow

	SET @TotalRecords = (
		SELECT	COUNT(Id)
		FROM	#OrderPayments
	)

	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO
PRINT 'Creating Function GetBundleQuantity'
GO
IF (OBJECT_ID('GetBundleQuantity') IS NOT NULL)
	DROP FUNCTION GetBundleQuantity
GO
CREATE Function [dbo].[GetBundleQuantity](@SkuId uniqueidentifier)
RETURNS Decimal(18,2)
AS
begin
	DECLARE @IsBundle bit
	DECLARE @ProductId uniqueidentifier
	DECLARE @Quantity decimal(18,2)
	SELECT @IsBundle =IsBundle,@ProductId=P.Id from  PRProductSKU PS inner join PRProduct P on PS.ProductId=P.Id WHERE PS.Id= @SkuId

	SET @Quantity =null
	IF (@IsBundle=1)
	BEGIN

SELECT @Quantity= Floor((MIN(TV2.Quantity)) )FROM (

SELECT Sum(TV.Quantity) as Quantity,TV.Id
FROM(
	SELECT
		CASE PS.IsUnlimitedQuantity
			WHEN 1 then 9999999							
			ELSE Sum((IsNull(Inv.Quantity,0))/BI.ChildSkuQuantity)
		END as Quantity
		,BI.Id
	FROM
		PRBundleItem BI 
		LEFT JOIN PRProductSKU PS on BI.[ChildSkuId]=PS.[Id] 
		LEFT JOIN vwAvailableToSellPerWarehouse Inv on PS.Id= Inv.SKUId AND PS.IsUnlimitedQuantity = 0
	WHERE 
		 BI.ChildSkuQuantity >0 AND BI.BundleProductId =@ProductId
	GROUP BY BI.Id, PS.Id, PS.IsUnlimitedQuantity
)TV
GROUP BY TV.Id
)TV2
IF (@Quantity is null) SET @Quantity = 0
	END 
	RETURN @Quantity
END
GO
PRINT 'Creating Function GetBundleQuantityPerSite'
GO
IF (OBJECT_ID('GetBundleQuantityPerSite') IS NOT NULL)
	DROP FUNCTION GetBundleQuantityPerSite
GO
CREATE Function [dbo].[GetBundleQuantityPerSite](@SkuId uniqueidentifier,@siteId uniqueidentifier)
RETURNS Decimal(18,2)
AS
begin
	DECLARE @IsBundle bit
	DECLARE @ProductId uniqueidentifier
	DECLARE @Quantity decimal(18,2)
	SELECT @IsBundle =IsBundle,@ProductId=P.Id from  PRProductSKU PS inner join PRProduct P on PS.ProductId=P.Id WHERE PS.Id= @SkuId

	SET @Quantity =null
	IF (@IsBundle=1)
	BEGIN


SELECT @Quantity= Floor((MIN(TV2.Quantity)) )FROM (

SELECT Sum(TV.Quantity) as Quantity,TV.Id
FROM(
	SELECT
		CASE min(cast(PS.IsUnlimitedQuantity as smallint))
			WHEN 1 then 9999999							
			ELSE Sum( Isnull(O.Quantity,0)/BI.ChildSkuQuantity)
		END as Quantity
		,BI.Id
from 
PRBundleItem BI 
		LEFT JOIN PRProductSKU PS on BI.[ChildSkuId]=PS.[Id] 
		LEFT JOIN [dbo].[vwAvailableToSellPerWarehouse] O ON PS.Id = O.SKUId and O.SiteId=@siteId
WHERE 
 BI.ChildSkuQuantity >0 AND BI.BundleProductId =@ProductId
	GROUP BY   BI.Id
)TV
GROUP BY TV.Id
)TV2
IF (@Quantity is null) SET @Quantity = 0
	END 
	RETURN @Quantity
END
GO
PRINT 'Creating Function GetAncestorSitesForSystemUserSiblingAccess'
GO
IF (OBJECT_ID('GetAncestorSitesForSystemUserSiblingAccess') IS NOT NULL)
	DROP FUNCTION GetAncestorSitesForSystemUserSiblingAccess
GO
CREATE FUNCTION [dbo].[GetAncestorSitesForSystemUserSiblingAccess](@ApplicationId uniqueidentifier, @IsSystemUser bit = null, @IsShared bit = null, @IncludeSiblingAccess bit = null)

RETURNS 
@Sites TABLE 
(
	SiteId uniqueidentifier
)
AS
BEGIN
	IF(@IsSystemUser IS NULL)
		SET @IsSystemUser = 1

	IF(@IsShared IS NULL)
		SET @IsShared = 0

	IF(@IncludeSiblingAccess IS NULL)
		SET @IncludeSiblingAccess = 1
			
	IF @ApplicationId is NOT NULL
	BEGIN
	Declare @ImportAllAdminPermission bit
	SELECT @ImportAllAdminPermission = ImportAllAdminPermission FROM SISite where Id = @ApplicationId
	IF((@ImportAllAdminPermission = 1 AND @IsSystemUser = 1) OR @IsShared = 1)
	BEGIN
	
		DECLARE @AllowSiblingAccess BIT
		
		SELECT @AllowSiblingAccess = [VALUE] 
		FROM STSiteSetting SS JOIN STSettingType ST 
				ON ST.Id = SS.SettingTypeId 
					AND ST.Name='AllowSiblingSiteAccess'

		DECLARE @PARENTID UNIQUEIDENTIFIER;    
		SELECT @PARENTID = PARENTSITEID FROM SISITE WHERE ID = @ApplicationId


		;WITH siteAncestorHierarchy AS (
			SELECT [Id]
					,[ParentSiteId]
					  FROM SISite S
					  where Id = @ApplicationId
			UNION ALL 
			SELECT S1.[Id]
					  ,S1.[ParentSiteId]
					  FROM SISite S1 
					INNER JOIN siteAncestorHierarchy SA ON S1.Id = SA.ParentSiteId 
					WHERE S1.Status != 3
			)

				INSERT INTO @Sites 
				SELECT [Id]
				FROM siteAncestorHierarchy S1
				--where Id != @ApplicationId
			
			IF (@AllowSiblingAccess = 1 AND @IncludeSiblingAccess = 1)
			BEGIN
				INSERT INTO @Sites
				SELECT [ID] SITEID FROM [GetSiblingSites](@ApplicationId)
				UNION
				SELECT Id FROM SISite WHERE ParentSiteId = @ApplicationId
			END
	END
	ELSE
		BEGIN
			INSERT INTO @Sites 
					SELECT @ApplicationId
		END	
	END
	
	RETURN 
END
GO

PRINT 'Creating Function GetAncestorSitesForSystemUser'
GO
IF (OBJECT_ID('GetAncestorSitesForSystemUser') IS NOT NULL)
	DROP FUNCTION GetAncestorSitesForSystemUser
GO
CREATE FUNCTION [dbo].[GetAncestorSitesForSystemUser](@ApplicationId uniqueidentifier, @IsSystemUser bit = null, @IsShared bit = null)

RETURNS 
@Sites TABLE 
(
	SiteId uniqueidentifier
)
AS
BEGIN
	INSERT INTO @Sites Select SiteId from dbo.GetAncestorSitesForSystemUserSiblingAccess(@ApplicationId,null,0, 1)	
	
	RETURN 
END
GO


PRINT 'Modify stored procedure Site_GetAncestorSites'
GO

IF(OBJECT_ID('Site_GetAncestorSites') IS NOT NULL)
	DROP PROCEDURE Site_GetAncestorSites
GO

CREATE PROCEDURE [dbo].[Site_GetAncestorSites]            
(            
	@SiteId		uniqueIdentifier,
	@IncludeSiblings bit = null  
)            
AS            
BEGIN            
	DECLARE @DeleteStatus int          
	SET @DeleteStatus = dbo.GetDeleteStatus()   
	
	IF(@IncludeSiblings IS NULL)
		SET @IncludeSiblings = 1
	
	SELECT S.Id,        
		S.Title,      
		S.Description,     
		S.URL,
		S.PrimarySiteUrl,          
		S.ParentSiteId,         
		S.CreatedBy,        
		S.CreatedDate,
		S.ModifiedBy,
		S.ModifiedDate,
		FN.UserFullName CreatedByFullName,  
		MN.UserFullName ModifiedByFullName,
		S.Status,  
		S.LftValue,
		S.RgtValue,
		S.MasterSiteId,
		S.VirtualPath,
		S.Keywords,
		S.Type,
		S.LftValue 
	FROM SISIte S
		INNER JOIN dbo.[GetAncestorSitesForSystemUserSiblingAccess](@SITEID, null,0,@IncludeSiblings) GS ON GS.SiteId = S.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
		LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy 
	WHERE S.Status != @DeleteStatus 
		AND S.Id != @SiteId
		AND (S.ExcludeSharing IS NULL OR S.ExcludeSharing = 0)
    ORDER BY S.LftValue
END      
GO


PRINT 'Modify stored procedure Site_GetSite'
GO

IF(OBJECT_ID('Site_GetSite') IS NOT NULL)
	DROP PROCEDURE Site_GetSite
GO

CREATE PROCEDURE [dbo].[Site_GetSite]  
(  
	@Id				uniqueIdentifier = null ,  
	@Title			nvarchar(256) = null,  
	@ParentSiteId	uniqueIdentifier = null,  
	@Status			int = null,  
	@PageIndex		int,  
	@PageSize		int,
	@ForceAllSites	bit = null,
	@MasterSiteId   uniqueIdentifier = null,
	@SiteGroupId	uniqueIdentifier = null
 )  
AS  
BEGIN  
	DECLARE @EmptyGUID uniqueIdentifier  
	SET @EmptyGUID = dbo.GetEmptyGUID()  
	
	DECLARE @DeleteStatus int
	SET @DeleteStatus = dbo.GetDeleteStatus()

	DECLARE @MetaDataValues NVARCHAR(MAX)
    DECLARE @MetaValues TABLE(ObjectId NVARCHAR(MAX), MetaDataXML XML)
    INSERT @MetaValues
    EXEC Site_GetCustomMetadata NULL ,@MetaDataValues OUTPUT

	DECLARE @tbSiteIds TABLE(Id uniqueidentifier)
	IF @SiteGroupId IS NOT NULL
	BEGIN
		INSERT INTO @tbSiteIds
		SELECT SiteId FROM SISiteGroup WHERE GroupId = @SiteGroupId
	END

	IF @PageSize <= 0 SET @PageSize = NULL
	
	IF @PageSize IS NULL
	BEGIN  
		SELECT S.Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId, 
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName, 
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			ExternalCode,
			DistributionEnabled,
			AllowSynchronization,
			NotifyOnTranslationError, 
			TranslationEnabled, 
			TranslatePagePropertiesByDefault, 
			AutoImportTranslatedSubmissions, 
			DefaultTranslateToLanguage,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			SiteHierarchyPath AS SitePath,
			MasterSiteId,
			CustomAttribute,
			te.MetaDataXML,
			ThemeId,
			TimeZone,
			ExcludeSharing,
			ForceHttps,
			TrackingCodes,
			LftValue
		FROM SISite S  
			INNER JOIN @MetaValues te ON S.Id = te.ObjectId
			LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
		WHERE 
			(@Id IS NULL OR S.Id = @Id) AND
			(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
			(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
			(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
			(@Status IS NULL OR S.Status = @Status) AND
			(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus) AND
			(@SiteGroupId IS NULL OR S.Id IN (SELECT Id FROM @tbSiteIds))
	END  
	ELSE  
	BEGIN  
		WITH ResultEntries AS (   
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row, 
				S.Id, 
				Title, 
				Description, 
				Status, 
				CreatedBy, 
				CreatedDate, 
				ModifiedBy,   
				ModifiedDate, 
				URL,
				Type,
				ParentSiteId,
				Keywords,
				VirtualPath,
				HostingPackageInstanceId, 
				LicenseLevel, 
				SendNotification, 
				SubmitToTranslation,
				ImportAllAdminPermission,
				AllowMasterWebSiteUser,
				Prefix, 
				UICulture, 
				LoginURL, 
				DefaultURL,
				ExternalCode,
				DistributionEnabled,
				AllowSynchronization,
				NotifyOnTranslationError, 
				TranslationEnabled, 
				TranslatePagePropertiesByDefault, 
				AutoImportTranslatedSubmissions, 
				DefaultTranslateToLanguage,
				PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				PrimarySiteURL,
				SiteHierarchyPath AS SitePath,
				MasterSiteId,
				CustomAttribute,
				te.MetaDataXML,
				ThemeId,
				TimeZone,
				ExcludeSharing,
				ForceHttps,
				TrackingCodes,
				LftValue
			FROM SISite S
				INNER JOIN @MetaValues te ON S.Id = te.ObjectId
			WHERE 
				(@Id IS NULL OR S.Id = @Id) AND
				(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
				(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
				(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
				(@Status IS NULL OR S.Status = @Status) AND
				(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus) AND
				(@SiteGroupId IS NULL OR S.Id IN (SELECT Id FROM @tbSiteIds))
		)
		
		SELECT 
			Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId,  
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName,
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			ExternalCode,
			DistributionEnabled,
			AllowSynchronization,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			SitePath,
			MasterSiteId,
			CustomAttribute,
			MetaDataXML,
			ThemeId,
			TimeZone,
			ExcludeSharing,
			ForceHttps,
			TrackingCodes,
			LftValue
		FROM ResultEntries  A  
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
		WHERE 
			Row BETWEEN(@PageIndex - 1) * @PageSize + 1 AND @PageIndex*@PageSize  
	END  
END
GO




IF(OBJECT_ID('ProductAttribute_GetAttribute') IS NOT NULL)
	DROP PROCEDURE ProductAttribute_GetAttribute
GO

CREATE PROCEDURE [dbo].[ProductAttribute_GetAttribute](
--********************************************************************************
-- Parameters
--********************************************************************************
			@Id uniqueidentifier = null,
			@OnlySystem bit = 0,
			@ApplicationId uniqueidentifier = null,
			@CategoryId int= null,
			@GroupId uniqueidentifier=null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
BEGIN

	if(Isnull(@OnlySystem,0) =0)
		BEGIN
			SELECT 
				a.Id,
				ag.CategoryId,		
				ag.GroupId AttributeGroupId,
				a.AttributeDataType,
				a.AttributeDataTypeId,
				a.Title,
				a.Description,
				a.Sequence,
				a.Code,
				a.IsFaceted,
				a.IsSearched,
				a.IsSystem,
				a.IsDisplayed,
				a.IsEnum,	
				dbo.ConvertTimeFromUtc(a.CreatedDate,@ApplicationId)CreatedDate,
				a.CreatedBy,
				dbo.ConvertTimeFromUtc(a.ModifiedDate,@ApplicationId)ModifiedDate,
				a.ModifiedBy,
				a.Status,
				ag.Title as [AttributeGroupName],IsPersonalized
			FROM 
				ATAttribute a LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) GroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
			WHERE	
				--a.[Id]=	Isnull(@Id,a.[Id])	
				(@Id is null or a.[Id] = @Id)
				AND
				a.Status = dbo.GetActiveStatus() -- Select only Active Attributes
				--AND 
				--ag.SiteId = IsNull(@ApplicationId, ag.SiteId)
				--(@ApplicationId is null or ag.SiteId = @ApplicationId)
				AND
				--ag.Id =isnull(@CategoryId,ag.Id)
				(@CategoryId is null or ag.CategoryId = @CategoryId)
				AND
				(@GroupId is null or ag.GroupId=@GroupId)
			ORDER BY a.Sequence,a.Title
		END
	ELSE
		BEGIN
			SELECT 
				a.Id,
				ag.CategoryId,			
				a.AttributeDataType,
				a.AttributeDataTypeId,
				a.Title,
				a.Description,
				a.Sequence,
				a.Code,
				a.IsFaceted,
				a.IsSearched,
				a.IsSystem,
				a.IsDisplayed,
				a.IsEnum,
				dbo.ConvertTimeFromUtc(a.CreatedDate,@ApplicationId),
				a.CreatedBy,
				dbo.ConvertTimeFromUtc(a.ModifiedDate,@ApplicationId)ModifiedDate,
				a.ModifiedBy,
				a.Status,
				ag.Title as [AttributeGroupName],IsPersonalized
			FROM 
				ATAttribute a LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) GroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
			WHERE	
				--a.[Id]=	Isnull(@Id,a.[Id])	
				(@Id is null or a.[Id] = @Id)
				AND
				a.Status = dbo.GetActiveStatus() -- Select only Active Attributes
				AND 
				a.IsSystem = 1
				--AND 
				--ag.SiteId = IsNull(@ApplicationId, ag.SiteId)
				--(@ApplicationId is null or ag.SiteId = @ApplicationId)
				AND
				--ag.Id =isnull(@CategoryId,ag.Id)
				(@CategoryId is null or ag.CategoryId = @CategoryId)
				AND
				(@GroupId is null or ag.GroupId=@GroupId)
			ORDER BY a.Sequence,a.Title

		END
END

GO

IF(OBJECT_ID('vwFeature') IS NOT NULL)
	DROP VIEW [dbo].[vwFeature]    
GO 
CREATE VIEW [dbo].[vwFeature]
AS
SELECT F.Id,
	F.SiteId,
	F.Title,
	F.Description,
	F.FeatureTypeId,
	FT.Title as FeatureTypeTitle,
	F.Sequence,
	F.Status,
	F.CreatedBy,
	F.CreatedDate,
	F.ModifiedBy,
	F.ModifiedDate
FROM [dbo].[MRFeature] F 
	LEFT JOIN MRFeatureType FT ON F.FeatureTypeId = FT.Id  
GO

IF(OBJECT_ID('vwAutoFeatureProduct') IS NOT NULL)
	DROP VIEW [dbo].[vwAutoFeatureProduct]    
GO 
CREATE VIEW [dbo].[vwAutoFeatureProduct]
AS
    WITH tempExcluded (FeatureId, ProductId, IsIncluded)
	AS(select FeatureId, ProductId, 0 from MRFeatureAutoExcludedItems)
	
	SELECT F.SiteId, FO.FeatureId, P.Id as ProductId, P.Title, P.IsActive, P.Status, P.CreatedDate, P.CreatedBy, P.ModifiedDate, P.ModifiedBy,
	       Isnull(EI.IsIncluded ,1) as IsIncluded, (select sum(SKU.PreviousSoldCount) from PRProductSKU SKU  where SKU.ProductId =P.Id) as SoldCount
	from MRFeature F inner join MRFeatureOutput FO on F.Id = FO.FeatureId  inner join GetProducts() P on P.Id =FO.ProductId  Left Join tempExcluded EI on FO.ProductId =EI.ProductId
GO

PRINT 'Modify stored procedure PriceSet_Delete'
GO

IF(OBJECT_ID('PriceSet_Delete') IS NOT NULL)
	DROP PROCEDURE PriceSet_Delete
GO
CREATE PROCEDURE [dbo].[PriceSet_Delete]
(
	@Id		uniqueidentifier
)
AS
BEGIN
	DECLARE @Stmt nvarchar(max)
	IF EXISTS (SELECT Id FROM PSPriceSet WHERE Id=@Id)
	BEGIN
		SET @Stmt = 'Delete'

		IF ( (SELECT Count(*) FROM PSPriceSetDiscount Where PriceSetId = @Id ) > 0)  
		BEGIN  
			RAISERROR('You cannot delete this price set. There are one or more discount ranges associated with this price set.||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()  
		END 

		IF ( (SELECT Count(*) FROM PSPriceSetManualSKU Where PriceSetId = @Id ) > 0)  
		BEGIN  
			RAISERROR('You cannot delete this price set. There are one or more SKUs associated with this price set.||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()  
		END 

		IF ( (SELECT Count(*) FROM PSPriceSetSKU Where PriceSetId = @Id ) > 0)  
		BEGIN  
			RAISERROR('You cannot delete this price set. There are one or more SKUs associated with this price set.||%s',16,1,@Stmt) 
			RETURN dbo.GetDataBaseErrorCode()  
		END 

		IF ( (SELECT Count(*) FROM PSPriceSetProductType Where PriceSetId = @Id ) > 0)  
		BEGIN  
			RAISERROR('You cannot delete this price set. There are one or more product types associated with this price set.||%s',16,1,@Stmt)  
			RETURN dbo.GetDataBaseErrorCode()  
		END  

		DELETE  CSCustomerGroupPriceSet  WHERE PriceSetId = @Id
		DELETE  PSPriceSet WHERE Id = @Id
		IF @@ERROR <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END
	END     
END
GO
PRINT 'Modify view vwPriceSet'
GO
IF(OBJECT_ID('vwPriceSet') IS NOT NULL)
	DROP VIEW vwPriceSet
GO
CREATE VIEW [dbo].[vwPriceSet]
AS 
SELECT   
	A.Id,   
	A.Title,    
	A.StartDate,  
	A.EndDate,   
	A.IsSale,   
	A.IsPendingChange,   
	A.TypeId,  
	A.CreatedDate,   
	A.CreatedBy,   
	A.ModifiedDate,   
	A.ModifiedBy,  
	A.Status,  
	(SELECT COUNT(1) FROM CSCustomerGroupPriceSet WHERE PriceSetId = A.Id) AS GroupCount,
	(SELECT sum(SkuCount) FROM vwPriceSetSKUCount WHERE PriceSetId = A.Id) AS SkuCount,
	A.SiteId
FROM dbo.PSPriceSet A  
WHERE A.Status  = 1    
GO


PRINT 'Modify view VWOrderItemPersonalizedAttribute'
GO
IF(OBJECT_ID('VWOrderItemPersonalizedAttribute') IS NOT NULL)
	DROP VIEW VWOrderItemPersonalizedAttribute
GO
CREATE VIEW [dbo].[VWOrderItemPersonalizedAttribute]
AS
(
SELECT   V.OrderId,V.OrderItemId,V.SKUId,P.Id as ProductId,V.CustomerId, P.Title AS ProductName,A.Id as AttributeId, A.Title AS AttributeName, 
                      AttributeEnumId,Isnull(AE.Value,V.Value ) AS Value,AE.Code,AE.NumericValue,AE.Sequence,AE.IsDefault, V.CartId,V.CartItemID,
					AG.CategoryId,A.IsFaceted,A.IsPersonalized,A.IsSearched,A.IsSystem,A.IsDisplayed,A.IsEnum,A.AttributeDataTypeId
FROM         dbo.PRProduct AS P INNER JOIN
                      dbo.OROrderItemAttributeValue AS V ON P.Id = V.ProductId INNER JOIN
                      dbo.ATAttribute AS A ON A.Id = V.AttributeId left JOIN
                      dbo.ATAttributeEnum AS AE ON V.AttributeEnumId = AE.Id
					  LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
                      Where A.IsPersonalized =1
union
SELECT    V.OrderId,V.OrderItemId,V.SKUId, P.Id as ProductId,V.CustomerId, P.Title AS ProductName,A.Id as AttributeId,A.Title AS AttributeName, 
                      AttributeEnumId,Isnull(AE.Value,V.Value ) AS Value,AE.Code,AE.NumericValue,AE.Sequence,AE.IsDefault, V.CartId,V.CartItemID
				,AG.CategoryId,A.IsFaceted,A.IsPersonalized,A.IsSearched,A.IsSystem,A.IsDisplayed,A.IsEnum,
				A.AttributeDataTypeId
FROM         dbo.PRProduct AS P INNER JOIN
					  PRProductSKU as PS on P.Id =PS.ProductId inner join
                      dbo.OROrderItemAttributeValue AS V ON PS.Id = V.SKUId INNER JOIN
                      dbo.ATAttribute AS A ON A.Id = V.AttributeId Left  JOIN
                      dbo.ATAttributeEnum AS AE ON V.AttributeEnumId = AE.Id
					  LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
Where A.IsPersonalized =1
)
GO

PRINT 'Modify SP Order_GetOrder'
GO
IF(OBJECT_ID('Order_GetOrder') IS NOT NULL)
	DROP PROCEDURE Order_GetOrder
GO

CREATE PROCEDURE [dbo].[Order_GetOrder](@Id uniqueidentifier = null, @CartId uniqueidentifier = null,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN

IF @Id IS NULL AND @CartId IS NULL 
	begin
		raiserror('DBERROR||%s',16,1,'OrderId or CartId is required')
		return dbo.GetDataBaseErrorCode()	
	end

SELECT     
 O.[Id],   
 o.[Id] OrderId, 
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],  
 O.[ExternalOrderNumber],  
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.[PaymentRemaining],   
 O.RefundTotal,
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId) OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 FN.UserFullName CreatedByFullName,
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O  
LEFT JOIN VW_UserFullName FN on FN.UserId = O.CreatedBy  
WHERE
O.SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites( @ApplicationId,1)) AND
(@Id is null or @Id = O.Id) AND
((@CartId is null or @CartId = CartId) OR (CartId IS NULL AND @CartId IS NULL))
--O.Id = ISnull(@Id,Id) AND (CartId = Isnull(@CartId,CartId) OR (CartId IS NULL AND @CartId IS NULL)) ;    


IF @Id IS NULL
	SELECT  @Id = Id FROM OROrder WHERE CartId = @CartId;

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderId=@Id AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount],
	OI.[HandlingTax]
FROM OROrderItem OI
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
LEFT JOIN VW_UserFullName FN on FN.UserId = OI.CreatedBy  
WHERE OI.OrderId = @Id;

SELECT 
	OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId)ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId)ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
dbo.ConvertTimeFromUtc(OS.[CreatedDate],@ApplicationId) CreatedDate,
OS.[ModifiedBy],
dbo.ConvertTimeFromUtc(OS.[ModifiedDate],@ApplicationId) ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder,
OS.IsHandlingIncluded,
OS.ShippingChargeTax,
OS.TaxableTotal,
OS.CustomStatus
FROM OROrderShipping OS
WHERE OS.OrderId = @Id;

select  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized
from PRProductAttribute PA  
Inner join 
	(Select ProductId FROM OROrderItem OI
		Inner Join PRProductSKU S on OI.ProductSKUId =S.Id  
		Where OI.OrderId = @Id
		) P On PA.ProductId=P.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=1  AND PA.IsSKULevel=1 AND  A.IsPersonalized =1


SELECT [OrderId]
      ,[OrderItemId]
      ,[SKUId]
      ,[ProductId]
      ,[CustomerId]
      ,[ProductName]
      ,[AttributeId]
      ,[AttributeName]
      ,[AttributeEnumId]
      ,[Value]
      ,[Code]
      ,[NumericValue]
      ,[Sequence]
      ,[IsDefault]
      ,[CartId]
      ,[CartItemID]
      ,[CategoryId]
      ,[IsFaceted]
      ,[IsPersonalized]
      ,[IsSearched]
      ,[IsSystem]
      ,[IsDisplayed]
      ,[IsEnum]
      ,[AttributeDataTypeId]
  FROM [dbo].[VWOrderItemPersonalizedAttribute]
	Where OrderId =@Id

--- order shipments
SELECT F.[Id]
      ,F.[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(F.ShippedDate,@ApplicationId)ShippedDate
      ,F.[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(F.CreateDate,@ApplicationId)CreateDate
      ,F.[CreatedBy]
      ,F.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(F.ModifiedDate,@ApplicationId)ModifiedDate
	  ,F.[EstimatedShipmentTotal]
	  ,F.ShipmentStatus
	  ,F.WarehouseId
	,F.ExternalReferenceNumber
FROM [dbo].[FFOrderShipment] F
INNER JOIN  OROrderShipping OS ON F.OrderShippingId = OS.Id
Where OS.OrderId =@Id
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
INNER JOIN  OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId =@Id



SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId=@Id


DECLARE @XMLORderId XML
SET @XMLORderId= convert(xml,N'<GenericCollectionOfGuid><guid>'+CAST(@Id AS NVARCHAR(50))+'</guid></GenericCollectionOfGuid>')
EXEC dbo.CouponCode_Get @Code = NULL, -- xml
    @CouponId = NULL, -- xml
    @Id = null,  
    @OrderId = @XMLORderId,
    @ApplicationId = @ApplicationId -- uniqueidentifier


END
GO

PRINT 'Modify SP AttributeGroup_GetGroup'
GO
IF(OBJECT_ID('AttributeGroup_GetGroup') IS NOT NULL)
	DROP PROCEDURE AttributeGroup_GetGroup
GO
CREATE PROCEDURE [dbo].[AttributeGroup_GetGroup](
--********************************************************************************
-- Parameters Passing in Attribute Group ID
--********************************************************************************
			@Id uniqueidentifier = null,
			@IsSystem bit =null,
			@ApplicationId uniqueidentifier=null
)
AS
BEGIN

	SELECT 
		GroupId Id,
		Name Title,
		SiteId,
		IsSystem,
		CreatedDate,
		CreatedBy,
		ModifiedDate,
		ModifiedBy,
		[Status],
		Id CategoryId
	FROM 
		ATAttributeCategory    
	WHERE
		--[Id]=	Isnull(@Id,[Id])
		--and IsSystem =isnull(@IsSystem, IsSystem)
        (@Id is null or [GroupId] = @Id)
		and (@IsSystem is null or IsSystem = @IsSystem)
		AND
        [Status] = dbo.GetActiveStatus() -- Select only Active Attributes
	ORDER BY SiteId,Title, IsSystem

	SELECT 
		A.Id,
		C.GroupId AttributeGroupId,
		AttributeDataType,
		AttributeDataTypeId,
		Title,
		A.[Description],
		Sequence,
		Code,
		IsFaceted,
		IsSearched,
		A.IsSystem,
		IsDisplayed,
		IsEnum,
		A.CreatedDate,
		A.CreatedBy,
		A.ModifiedDate,
		A.ModifiedBy,
		A.[Status],
		IsPersonalized
	FROM ATAttribute A
	INNER JOIN ATAttributeCategoryItem CI ON A.Id=CI.AttributeId
	INNER JOIN ATAttributeCategory C on CI.CategoryId=C.Id
	WHERE --AttributeGroupId= Isnull(@Id, AttributeGroupId )
			(@Id is null or C.GroupId = @Id)
			AND
			A.[Status]	= dbo.GetActiveStatus() -- Select only Active Attributes
	Order By AttributeGroupId,Sequence, Title
END
GO
IF NOT EXISTS (Select 1 from PTGateway Where Name='Stripe')
BEGIN
	Declare @Id int
	Select @Id= isnull(max(id),0) +1 from PTGateway 

	Declare @GsId int
	Select @GsId= isnull(max(id),0) +1 from PTGatewaySite 

	INSERT INTO PTGateway (Id, Name,Description,Sequence,DotNetProviderName)
	Values(@Id,'Stripe','Stripe',@Id,'Stripe')

	INSERT INTO PTGatewaySite(GatewayId,SiteId,IsActive,PaymentTypeId)
	Select @Id,Id,0,1 FROM SISite  Where ParentSiteId='00000000-0000-0000-0000-000000000000'
END
GO

PRINT 'Add column to ORShipper'
GO
IF(COL_LENGTH('ORShipper', 'IsShared') IS NULL)
BEGIN
	ALTER TABLE ORShipper ADD IsShared BIT NOT NULL DEFAULT 0
END
GO

PRINT 'Modify SP Shipper_GetShippers'
GO
IF(OBJECT_ID('Shipper_GetShippers') IS NOT NULL)
	DROP PROCEDURE Shipper_GetShippers
GO

CREATE PROCEDURE [dbo].[Shipper_GetShippers](
	@Id uniqueidentifier = null
	,@ApplicationId uniqueidentifier=null
)
AS
BEGIN

	DECLARE @activeStatus INT 
	SELECT @activeStatus = dbo.GetActiveStatus()

	DECLARE @shippers TABLE(
		Id uniqueidentifier,
		Name nvarchar(255),
		Description nvarchar(1024),
		TrackingURL nvarchar(500),
		DotNetProviderName nvarchar(255),
		Status int,
		Sequence int, 
		CreatedBy uniqueidentifier,
		CreatedDate datetime,
		ModifiedBy uniqueidentifier,
		ModifiedDate datetime,
		SiteId uniqueidentifier,
		IsShared bit
	)

	INSERT INTO @shippers
	SELECT [Id]
      ,[Name] 
	  ,[Description]    
      ,[TrackingURL]      
      ,[DotNetProviderName]
      ,[Status]
	  ,[Sequence]
      ,[CreatedBy]
      , CreatedDate AS [CreatedDate]
      ,[ModifiedBy]
      , ModifiedDate AS [ModifiedDate]
	  ,SiteId
	  ,IsShared
   FROM ORShipper
   where --[Id]=	Isnull(@Id,[Id])		
		(@Id is null or [Id] = @Id)
        AND
        [Status] = @activeStatus 
        AND
        --SiteId = @ApplicationId
        (@ApplicationId is null or (SiteId = @ApplicationId) OR (IsShared = 1 AND SiteId in (select SiteId from dbo.GetParentSites(@ApplicationId, 1, 1)))) 
        order by Sequence, Name

	SELECT * FROM @shippers

	SELECT so.[Id]
      ,so.[ShipperId]
      ,so.[Name]
      ,so.[Description]
      ,so.[CustomId]
      ,so.[NumberOfDaysToDeliver]
      ,so.[CutOffHour]
      ,so.[SaturdayDelivery]
      ,so.[AllowShippingCoupon]
      ,so.[IsPublic]
      ,so.[IsInternational]
      ,so.[ServiceCode]
	  ,so.[ExternalShipCode]
      ,so.[BaseRate]      
      ,so.[Status]
	  ,so.[Sequence]
      ,so.[CreatedBy]
      ,so.[CreatedDate]
      ,so.[ModifiedBy]
      ,so.[ModifiedDate]
	  ,so.SiteId
  FROM ORShippingOption so join @shippers s on so.ShipperId = s.Id
  Where so.SiteId=@ApplicationId
  order by so.ShipperId, so.Sequence	
END
GO

PRINT 'Modify SP Shipper_Save'
GO
IF(OBJECT_ID('Shipper_Save') IS NOT NULL)
	DROP PROCEDURE Shipper_Save
GO

CREATE PROCEDURE [dbo].[Shipper_Save]
(
	@Id uniqueidentifier output,
	@Name nvarchar(255),
	@Description nvarchar(1024)= null,
	@TrackingURL nvarchar(500)=null,
	@DotNetProviderName varchar(255)=null,
	@Status int,		 
	@ApplicationId uniqueidentifier =null,	
	@CreatedBy uniqueidentifier =null,
	@ModifiedBy uniqueidentifier =null,
	@IsShared bit = null
)
as
begin
	DECLARE	@NewId	uniqueidentifier
	DECLARE @Now dateTime
	DECLARE @Stmt VARCHAR(36)	
	DECLARE @Error int		
	
	SET @Now = getutcdate()
	
	if(@Id is null)
	begin
		set @NewId = newid()
		set @Id  =@NewId
	
		
		SET @Stmt = 'Insert Shipper'
		INSERT INTO [ORShipper]
			   ([Id]
			   ,[Name]
			   ,[Description]
			   ,[TrackingURL]
			   ,[DotNetProviderName]
			   ,[Status]
			   
			   ,[CreatedBy]
			   ,[CreatedDate]			   
			   ,[SiteId]
			   ,[IsShared])
		 VALUES
			   (@NewId
			   ,@Name
			   ,@Description
			   ,@TrackingURL
			   ,@DotNetProviderName
			   ,dbo.GetActiveStatus()
			   
			   ,@CreatedBy
			   ,@Now			   
			   ,@ApplicationId
			   ,@IsShared)
			
			SELECT @Error = @@ERROR
 			IF @Error <> 0
			BEGIN
				RAISERROR('DBERROR||%s',16,1,@Stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END
	end
	else
	begin
		IF (NOT EXISTS(SELECT 1 FROM  ORShipper WHERE  Id = @Id))
		BEGIN			
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END	

		SET @Stmt = 'Update Shipper'
		UPDATE [ORShipper]
		   SET 
			  [Name] = @Name
			  ,[Description] = @Description
			  ,[TrackingURL] = @TrackingURL
			  ,[DotNetProviderName] = @DotNetProviderName
			  ,[Status] = @Status
			  	  
			  ,[ModifiedBy] = @ModifiedBy
			  ,[ModifiedDate] = @Now
			  ,[SiteId] = @ApplicationId
			  ,[IsShared] = @IsShared
		 WHERE Id=@Id
		
		SELECT @Error = @@ERROR
 		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END	  
	end
end
GO

PRINT 'Modify SP PaymentType_Get'
GO
IF(OBJECT_ID('PaymentType_Get') IS NOT NULL)
	DROP PROCEDURE PaymentType_Get
GO

CREATE PROCEDURE [dbo].[PaymentType_Get](@Id int=null,@GetAll bit=0,@ApplicationId uniqueidentifier)
AS
BEGIN
	
SELECT 
	T.Id,
	T.Name,
	T.Description,
	T.Sequence,
	IsNull(S.Status, 0) AS 'Status'
	FROM PTPaymentType T
	Left Join PTPaymentTypeSite S ON (T.Id = S.PaymentTypeId AND S.SiteId =@ApplicationId)
	WHERE --Id=ISNULL(@Id,Id)
	(@Id is null or T.Id = @Id)
	AND (S.Status=1 or @GetAll=1)
END
GO

PRINT 'Modify SP CreditCardType_Get'
GO
IF(OBJECT_ID('CreditCardType_Get') IS NOT NULL)
	DROP PROCEDURE CreditCardType_Get
GO

CREATE PROCEDURE [dbo].[CreditCardType_Get](@Id uniqueidentifier=null,@GetAll bit=0,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	
	SELECT 
	T.Id,
	T.Title,
	T.Description,
	T.CreatedBy,
	dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,
	dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,
	T.ModifiedBy,
	IsNull(S.Status, 0) AS 'Status'
	FROM PTCreditCardType T
	LEFT JOIN PTCreditCardTypeSite S on (T.Id = S.CreditCardTypeId AND S.SiteId = @ApplicationId)
	WHERE --Id=ISNULL(@Id,Id)
	(@Id is null or T.Id = @Id)
	AND (S.Status=1 or @GetAll=1)
END
GO

PRINT 'Modify SP CreditCardType_Save'
GO
IF(OBJECT_ID('CreditCardType_Save') IS NOT NULL)
	DROP PROCEDURE CreditCardType_Save
GO

CREATE PROCEDURE [dbo].[CreditCardType_Save]
	@Id				uniqueidentifier OUTPUT,
	@Status			int = NULL,
	@ApplicationId uniqueidentifier
AS
BEGIN

IF Exists(Select 1 from PTCreditCardTypeSite Where CreditCardTypeId=@Id AND SiteId = @ApplicationId)
	UPDATE 
		PTCreditCardTypeSite
	SET 
		[Status] = @Status
	WHERE 
		CreditCardTypeId = @Id AND SiteId= @ApplicationId
	ELSE	
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,Status,SiteId)
		Values(@Id,isnull(@Status,0),@ApplicationId)
End
GO

PRINT 'Modify SP Gateway_Get'
GO
IF(OBJECT_ID('Gateway_Get') IS NOT NULL)
	DROP PROCEDURE Gateway_Get
GO

CREATE PROCEDURE [dbo].[Gateway_Get](@Id int=null,@GetAll bit=0, @ApplicationId uniqueidentifier)
AS
BEGIN

	SELECT 
	PG.Id,
	Name,
	Description,
	IsNull(PGS.IsActive, 0) AS 'IsActive',
	Sequence,
	DotNetProviderName,
	PGS.PaymentTypeId
	FROM PTGateway PG
	LEFT JOIN PTGatewaySite PGS ON PG.Id = PGS.GatewayId  AND PGS.SiteId =@ApplicationId
	WHERE PG.Id=ISNULL(@Id,PG.Id)
	AND (PGS.IsActive= 1 or @GetAll=1)
	ORDER BY Sequence ASC
END
GO

PRINT 'Modify SP Gateway_Save'
GO
IF(OBJECT_ID('Gateway_Save') IS NOT NULL)
	DROP PROCEDURE Gateway_Save
GO

CREATE PROCEDURE [dbo].[Gateway_Save]
	@Id				int OUTPUT,
	@IsActive		bit,
	@ApplicationId	uniqueidentifier
AS
BEGIN
	Declare @PaymentTypeId int
	
	Select @PaymentTypeId =PaymentTypeId 
	From PTGatewaySite PG
	Where GateWayId=@Id

	IF Exists(Select 1 from PTGatewaySite Where GateWayId=@Id AND SiteId = @ApplicationId)
		UPDATE PTGatewaySite 
			SET ISActive = Case when GateWayId=@Id then  @IsActive else 0 end
		FROM PTGatewaySite PGS
		WHERE PaymentTypeId=@PaymentTypeId -- dont know why Payment Type Id is used here ??
			AND SiteId = @ApplicationId	
	ELSE	
		INSERT INTO PTGatewaySite(GateWayId,ISActive,SiteId)
		Values(@Id, @IsActive, @ApplicationId)
End
GO

PRINT 'Modify SP PaymentType_GetAll'
GO
IF(OBJECT_ID('PaymentType_GetAll') IS NOT NULL)
	DROP PROCEDURE PaymentType_GetAll
GO

CREATE PROCEDURE [dbo].[PaymentType_GetAll](@ApplicationId uniqueidentifier)
AS
BEGIN
	
	SELECT 
	T.Id,
	T.Name,
	T.Description,
	T.Sequence,
	isnull(S.Status,0) Status
	FROM PTPaymentType T
	Left Join PTPaymentTypeSite S ON T.Id = S.PaymentTypeId AND S.SiteId =@ApplicationId
	
END

GO
PRINT 'Create type TYIdNumber'
GO

IF TYPE_ID('TYIdNumber') IS NULL
CREATE TYPE [dbo].[TYIdNumber] AS TABLE
(
	Id uniqueidentifier,
	Number decimal(18,2)
)

GO

IF(OBJECT_ID('Product_GetMinPrice') IS NOT NULL)
	DROP PROCEDURE Product_GetMinPrice
GO

CREATE PROCEDURE [dbo].[Product_GetMinPrice](
			 @CustomerId uniqueidentifier = null,
			 @IdQuantity  TYIdNumber READONLY,
			 @ApplicationId uniqueidentifier=null
			)
AS

BEGIN
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId' and 
					(@ApplicationId is null or STS.SiteId = @ApplicationId))

			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice,ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY P.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @IdQuantity IQ
				INNER JOIN  PRProduct P ON IQ.Id =P.Id
				INNER JOIN PRProductSKU S on P.Id = S.ProductId
				LEFT JOIN PRProductSKUVariant SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					Where CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= S.Id AND IQ.Number between minQuantity and maxQuantity
					Where P.Id=IQ.Id
				
			) EFPS
			WHERE EFPS.Sno=1

		End
		else
		Begin
			Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice,ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @IdQuantity IQ
				INNER JOIN  PRProduct P ON IQ.Id =P.Id
				INNER JOIN PRProductSKU S on P.Id = S.ProductId
				LEFT JOIN PRProductSKUVariant SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					Where USMG.MemberId = @CustomerId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= S.Id AND IQ.Number between minQuantity and maxQuantity
					Where P.Id =IQ.Id						
		End
END


GO


IF(OBJECT_ID('vwWarehouse') IS NOT NULL)
	DROP VIEW vwWarehouse
GO

CREATE VIEW [dbo].[vwWarehouse]
AS
SELECT	W.Id, S.Id AS SiteId, W.Title, W.[Description], W.WarehouseCode, W.IsDefault, W.TypeId, W.DotNetProviderName, W.RestockingDelay,
		COALESCE(WS.[Status], CASE W.TypeId WHEN 2 THEN 1 ELSE 0 END) AS [Status],
		W.AddressId, A.AddressLine1, A.AddressLine2, A.AddressLine3, A.City, A.StateId, ST.[State], ST.StateCode,
		A.CountryId, C.CountryName, C.CountryCode, A.Zip, A.County, A.Phone,
		W.CreatedDate, W.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
		W.ModifiedDate, W.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
FROM	INWarehouse AS W
		CROSS JOIN SISite AS S
		LEFT JOIN INWarehouseSite AS WS ON WS.WarehouseId = W.Id AND WS.SiteId = S.Id
		LEFT JOIN GLAddress AS A ON A.Id = W.AddressId
		LEFT JOIN GLState AS ST ON ST.Id = A.StateId
		LEFT JOIN GLCountry AS C ON C.Id = A.CountryId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = W.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = W.ModifiedBy
WHERE	W.[Status] = 1 AND
		S.[Status] = 1
GO

PRINT 'Create view vwRestocking'
GO

IF(OBJECT_ID('vwRestocking') IS NOT NULL)
	DROP VIEW vwRestocking
GO

CREATE VIEW [dbo].[vwRestocking]
AS
SELECT	R.Id, W.SiteId, R.Title, R.[Description], R.ShipmentNumber, R.RestockingCode, R.WarehouseId, W.Title AS WarehouseName,
		R.Comments, R.ExpectedDate, R.ScheduledArrivalDate, R.ShippedDate, R.RestockingStatusId AS RestockingStatus,
		R.CreatedDate, R.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
		R.ModifiedDate, R.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
FROM	INRestocking AS R
		INNER JOIN vwWarehouse AS W ON W.Id = R.WarehouseId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = R.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = R.ModifiedBy
GO

PRINT 'Create view vwRestockingSKU'
GO

IF(OBJECT_ID('vwRestockingSKU') IS NOT NULL)
	DROP VIEW vwRestockingSKU
GO

CREATE VIEW vwRestockingSKU
AS
SELECT	RI.Id, RI.RestockingId, RI.ExpectedQty, RI.ReceivedQty, RI.Comments,
		RI.SkuId, SKU.Title AS SkuTitle, SKU.SKU,
		RI.CreatedDate, RI.CreatedBy AS CreatedById, CFN.UserFullName AS CreatedByFullName,
		RI.ModifiedDate, RI.ModifiedBy AS ModifiedById, MFN.UserFullName AS ModifiedByFullName
FROM	INRestockingItem AS RI
		INNER JOIN PRProductSKU AS SKU ON SKU.Id = RI.SKUId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = RI.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = RI.ModifiedBy
GO



Print 'Creating Procedure VariantSite_CopyDefaultMenus'
GO

IF(OBJECT_ID('VariantSite_CopyDefaultMenus') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyDefaultMenus
GO

CREATE PROCEDURE [dbo].[VariantSite_CopyDefaultMenus]
(  
	@MicroSiteId uniqueidentifier,
	@ModifiedBy  uniqueidentifier
)
AS
BEGIN

-- Create default nodes root node and unassigned when selected dont import menus and pages

	Insert into PageMapBase (SiteId, ModifiedDate)
		Values(@MicroSiteId, GETUTCDATE())
	
	DECLARE @pageMapNodeXml NVARCHAR(max)
	DECLARE @pageMapNodeId uniqueidentifier
	SET @pageMapNodeId = @MicroSiteId
	declare @SiteTitle nvarchar(max)
	set @SiteTitle = (select Title from SISite where Id = @MicroSiteId)

SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" description="' + @SiteTitle + '"  
		   displayTitle="' + @SiteTitle + '" friendlyUrl="' + replace(replace(replace(@SiteTitle,' ',''),'''',''),'"','') + '" menuStatus="0"   
		   targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
		   createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
		   propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="True"   
		   inheritRoles="False" roles="" securityLevels="" locationIdentifier="" />'  						 

	EXEC PageMapNode_Save @Id =@pageMapNodeId,
			@SiteId =@MicroSiteId,
			@ParentNodeId = NULL,
			@PageMapNode= @pageMapNodeXml,
			@CreatedBy = @ModifiedBy
			
	SET @pageMapNodeId = NEWID()
	SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" 
						 displayTitle="Unassigned" friendlyUrl="Unassigned" description="Unassigned" menuStatus="0" 
						 parentId="' + CAST(@MicroSiteId AS CHAR(36)) + '" 
					     targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
						 createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
						 propogateSecurityLevels="True" inheritSecurityLevels="False" 
						 propogateRoles="True" inheritRoles="False" roles="" securityLevels="" />'

	EXEC PageMapNode_Save @Id = @pageMapNodeId,
			@SiteId = @MicroSiteId,
			@ParentNodeId = @MicroSiteId,
			@PageMapNode = @pageMapNodeXml,
			@CreatedBy = @ModifiedBy


END
GO

Print 'Creating Procedure VariantSite_CopyMenus'
GO

IF(OBJECT_ID('VariantSite_CopyMenus') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyMenus
GO

CREATE PROCEDURE [dbo].[VariantSite_CopyMenus]
(  
	@SiteId UNIQUEIDENTIFIER,
	@MicroSiteId UNIQUEIDENTIFIER,
	@ModifiedBy  UNIQUEIDENTIFIER
	
)
AS
BEGIN

	Declare @NotAllowAccessInChildrenSites Table (PageMapNodeId uniqueIdentifier, Process bit)
	Declare @ExcludeNodes Table (PageMapNodeId uniqueIdentifier, Title varchar(1000))
	Declare @LftValue int, @RgtValue int

	INSERT INTO @NotAllowAccessInChildrenSites  (PageMapNodeId, Process)
	Select PageMapnodeId, 0 from PageMapNode where AllowAccessInChildrenSites = 0 and SiteId = @SiteId
	Declare @ParentPageMapNodeIdToExclude uniqueidentifier
	While EXISTS( Select 1 from @NotAllowAccessInChildrenSites  where Process = 0)
	BEGIN
		Select top 1 @ParentPageMapNodeIdToExclude  = PageMapNodeId from @NotAllowAccessInChildrenSites  where Process = 0

		Select @LftValue = LftValue, @RgtValue = RgtValue from PageMapNode where PageMapNodeId = @ParentPageMapNodeIdToExclude and SiteId = @SiteId

		INSERT INTO @ExcludeNodes (PageMapNodeId, Title) 
		Select PageMapNodeId, DisplayTitle  from PageMapNode where LftValue Between @LftValue and @RgtValue and SiteId = @SiteId

		Update @NotAllowAccessInChildrenSites Set Process = 1 where PageMapNodeId = @ParentPageMapNodeIdToExclude
	END

	INSERT INTO [dbo].[PageMapNode]
			   ([PageMapNodeId]
			   ,[ParentId]
			   ,[LftValue]
			   ,[RgtValue]
			   ,[SiteId]
			   ,[ExcludeFromSiteMap]
			   ,[Description]
			   ,[DisplayTitle]
			   ,[FriendlyUrl]
			   ,[MenuStatus]
			   ,[TargetId]
			   ,[Target]
			   ,[TargetUrl]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PropogateWorkflow]
			   ,[InheritWorkflow]
			   ,[PropogateSecurityLevels]
			   ,[InheritSecurityLevels]
			   ,[PropogateRoles]
			   ,[InheritRoles]
			   ,[Roles]
			   ,[LocationIdentifier]
			   ,[HasRolloverImages]
			   ,[RolloverOnImage]
			   ,[RolloverOffImage]
			   ,[IsCommerceNav]
			   ,[AssociatedQueryId]
			   ,[DefaultContentId]
			   ,[AssociatedContentFolderId]
			   ,[CustomAttributes]
			   ,[HiddenFromNavigation]
			   ,SourcePageMapNodeId)
			   SELECT  case when parentId is null then @MicroSiteId else NEWID()end
				  ,[ParentId]
				  ,[LftValue]
				  ,[RgtValue]
				  ,@MicroSiteId
				  ,[ExcludeFromSiteMap]
				  ,[Description]
				  ,[DisplayTitle]
				  ,[FriendlyUrl]
				  ,[MenuStatus]
				  ,[TargetId]
				  ,[Target]
				  ,[TargetUrl]
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,[PropogateWorkflow]
				  ,[InheritWorkflow]
				  ,[PropogateSecurityLevels]
				  ,[InheritSecurityLevels]
				  ,[PropogateRoles]
				  ,[InheritRoles]
				  ,[Roles]
				  ,[LocationIdentifier]
				  ,[HasRolloverImages]
				  ,[RolloverOnImage]
				  ,[RolloverOffImage]
				  ,[IsCommerceNav]
				  ,[AssociatedQueryId]
				  ,[DefaultContentId]
				  ,[AssociatedContentFolderId]
				  ,[CustomAttributes]
				  ,[HiddenFromNavigation]
				  ,PageMapNodeId
			  FROM [dbo].[PageMapNode]
			   Where SiteId=@SiteId
			   AND PageMapNodeId not in    
			   (
				SELECT distinct P.PageMapNodeId 
				FROM [dbo].[PageMapNode] P
				cross  join PageMapNode DP 
				Where P.SiteId=@SiteId and P.LftValue between DP.LftValue and DP.RgtValue
				AND DP.PageMapNodeId   in 
				 (
					select TOP 1 Cast(Value as Uniqueidentifier) from STSettingType T
					INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
					Where Name in ('CampaignPageMapNodeId')
					AND SiteId=@SiteId
					And Value is not NULL AND Value !=''
					UNION 
					select distinct PageMapNodeId from @ExcludeNodes
				 )   
			   )
	  /* END Modification for not copying the node which are not shared in variant sites*/
	  
	--remove commerceproduct type and marketierpages menu group   
		delete from PageMapNode where SiteId = @MicroSiteId  and  SourcePageMapNodeId in 
		(select ParentId from PageMapNode where PageMapNodeId in 
			(select Top 1 CAST(Value AS UNIQUEIDENTIFIER) FROM STSettingType T
			INNER JOIN STSiteSetting S ON T.Id = S.SettingTypeId
			WHERE Name IN ('CampaignPageMapNodeId')
			AND SiteId=@SiteId
			AND Value IS NOT NULL
			AND Value !=''))
		
			--remove commerceproduct type  and marketierpages menu group   subnodes if any exist(generally it should not)
		DELETE FROM PageMapNode WHERE SiteId = @MicroSiteId  AND  ParentId IN 
		(SELECT ParentId FROM PageMapNode WHERE PageMapNodeId IN 
			(SELECT TOP 1 CAST(Value AS Uniqueidentifier) from STSettingType T
			INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
			Where Name in ('CampaignPageMapNodeId')
			AND SiteId=@SiteId
			And Value is not null
			AND Value !=''))

		
	    
	   Update  M Set M.ParentId = P.PageMapNodeId
	   FROM PageMapNode M 
	   INNER JOIN PageMapNode P ON P.SourcePageMapNodeId = M.ParentId
	   Where P.SiteId =@MicroSiteId and M.SiteId=@MicroSiteId
	   
	   UPdate PN SET PN.TargetId=T.PageMapNodeId
		FROM PageMapNode PN
		INNER JOIN PageMapNode T ON PN.TargetId = T.SourcePageMapNodeId
		Where PN.Target='2' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId


		 --import shared workflow mapping of pagemapnode and workflow
	INSERT INTO [dbo].[PageMapNodeWorkflow]
           ([Id]
           ,[PageMapNodeId]
           ,[WorkflowId]
           ,[CustomAttributes])
	select NEWID(),
		P.PageMapNodeId,
		PW.WorkflowId,
		null
	FROM [dbo].[PageMapNodeWorkflow] PW
		INNER JOIN PageMapNode P ON PW.PageMapNodeId = P.SourcePageMapNodeId
		inner join WFWorkflow W on PW.WorkflowId = W.Id 
		 Where P.SiteId = @MicroSiteId  and W.IsShared=1 


	IF exists(select 1 from  [dbo].PageMapBase B INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId Where P.SiteId = @MicroSiteId)
	begin
	 INSERT INTO [dbo].[PageMapBase]
					   ([SiteId]
					   ,[HomePageDefinitionId]
					   ,[ModifiedDate]
					   ,[ModifiedBy]
					   ,[LastPageMapModificationDate])
			SELECT top 1 @MicroSiteId
				  ,P.PageMapNodeId
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,[LastPageMapModificationDate]
			FROM [dbo].PageMapBase B
			INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId
			 Where P.SiteId = @MicroSiteId
	end
	else	
		Insert into PageMapBase (SiteId, ModifiedDate,ModifiedBy)	Values(@MicroSiteId, GETUTCDATE(),@ModifiedBy) 

END
GO

Print 'Creating Procedure VariantSite_CopyPages'
GO

IF(OBJECT_ID('VariantSite_CopyPages') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyPages
GO


CREATE PROCEDURE [dbo].[VariantSite_CopyPages]
(  
	@SiteId UNIQUEIDENTIFIER,
	@MicroSiteId UNIQUEIDENTIFIER,
	@ModifiedBy  UNIQUEIDENTIFIER,
	@MakePageStatusAsDraft BIT
	
)
AS
BEGIN

	INSERT INTO [dbo].[PageDefinition]
					   ([PageDefinitionId]
					   ,[TemplateId]
					   ,[SiteId]
					   ,[Title]
					   ,[Description]
					   ,[PageStatus]
					   ,[WorkflowState]
					   ,[PublishCount]
					   ,[PublishDate]
					   --,[DisplayOrder]
					   ,[FriendlyName]
					   ,[WorkflowId]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate]
					   ,[ArchivedDate]
					   ,[StatusChangedDate]
					   ,[AuthorId]
					   ,[EnableOutputCache]
					   ,[OutputCacheProfileName]
					   ,[ExcludeFromSearch]
					   ,[IsDefault]
					   ,[IsTracked]
					   ,[HasForms]
					   ,[TextContentCounter]
					   ,[SourcePageDefinitionId]
					   ,[CustomAttributes]
					   )
			SELECT NEWID()
				  ,[TemplateId]
				  ,@MicroSiteId
				  ,[Title]
				  ,D.[Description]
				  ,case when @MakePageStatusAsDraft=1 then dbo.GetActiveStatus()  else [PageStatus]  end
				  ,case when @MakePageStatusAsDraft=1 then  2  else case when WorkflowState =1 then WorkflowState else 2 end end
				  ,case when @MakePageStatusAsDraft=1 then 0 else 1 end
				  ,[PublishDate]
				  --,[DisplayOrder]
				  ,[FriendlyName]
				  ,dbo.GetEmptyGUID()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,[ArchivedDate]
				  ,[StatusChangedDate]
				  ,@ModifiedBy
				  ,[EnableOutputCache]
				  ,[OutputCacheProfileName]
				  ,[ExcludeFromSearch]
				  ,[IsDefault]
				  ,[IsTracked]
				  ,[HasForms]
				  ,[TextContentCounter]
				  ,PageDefinitionId
				  ,D.[CustomAttributes]
			      
			  FROM [dbo].[PageDefinition] D
			Where D.SiteId =@SiteId and PageDefinitionId in (select PageDefinitionId  from PageMapNodePageDef PMD inner join PageMapNode PM on PMD.PageMapNodeId = PM.SourcePageMapNodeId where PM.SiteId =@MicroSiteId)

			insert into PageMapNodePageDef
			select distinct  c.PageMapNodeId ,a.PageDefinitionId, b.DisplayOrder from [PageDefinition] a , PageMapNodePageDef b  , PageMapNode c
			where a.SiteId  = @MicroSiteId and a.SourcePageDefinitionId = b.PageDefinitionId 
			and c.SourcePageMapNodeId = b.PageMapNodeId and 
			c.SiteId = @MicroSiteId

			UPDATE PN SET PN.TargetId=T.PageDefinitionId
			FROM PageMapNode PN
			INNER JOIN PageDefinition T ON PN.TargetId = T.SourcePageDefinitionId
			Where PN.Target='1' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId

		
			insert into #tempIds SELECT PS.Id, NEWID() FROM  [dbo].[PageDefinitionSegment] PS INNER JOIN PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
			 
			 INSERT INTO [dbo].[PageDefinitionSegment]
						   ([Id]
						   ,[PageDefinitionId]
						   ,[DeviceId]
						   ,[AudienceSegmentId]
						   ,[UnmanagedXml]
						   ,[ZonesXml])
						   SELECT T.NewId 
							  ,P.PageDefinitionId 
							  ,[DeviceId]
							  ,[AudienceSegmentId]
							  ,[UnmanagedXml]
							  ,[ZonesXml]
						  FROM [dbo].[PageDefinitionSegment] PS 
						  inner join #tempIds T on PS.Id= T.OldId 
						  inner join PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
						  Where P.SiteId = @MicroSiteId
						  
			INSERT INTO [PageDefinitionContainer]
				(
					[Id]
					,PageDefinitionSegmentId 
					,[PageDefinitionId]
					,[ContainerId]
					,[ContentId]
					,[InWFContentId]
					,[ContentTypeId]
					,[IsModified]
					,[IsTracked]
					,[Visible]
					,[InWFVisible]
					,DisplayOrder 
					,InWFDisplayOrder 
					,[CustomAttributes]
					,[ContainerName])
				SELECT
						NewId() 
						,T.NewId 
					,P.PageDefinitionId
					,ContainerId
					,ContentId
					,InWFContentId
					,ContentTypeId
					,IsModified
					,PC.IsTracked
					,Visible
					,InWFVisible
					,PC.DisplayOrder 
					,PC.InWFDisplayOrder 
					,PC.CustomAttributes
					,ContainerName
				FROM PageDefinitionContainer PC
				inner join #tempIds T on PC.PageDefinitionSegmentId= T.OldId
				inner join PageDefinition P ON PC.PageDefinitionId=P.SourcePageDefinitionId 
				Where P.SiteId = @MicroSiteId
						  
			truncate table #tempIds

			INSERT INTO [dbo].[PageDetails]
					   ([PageId]
					   ,[PageDetailXML])
			  SELECT 
				 P.PageDefinitionId
					   ,[PageDetailXML]
			  FROM [dbo].[PageDetails] PD
			INNER JOIN PageDefinition P ON P.SourcePageDefinitionId = PD.PageId
			 Where P.SiteId = @MicroSiteId

			 INSERT INTO [dbo].[USSecurityLevelObject]
					   ([ObjectId]
					   ,[ObjectTypeId]
					   ,[SecurityLevelId])
			  SELECT P.PageDefinitionId
					   ,ObjectTypeId
					   ,SecurityLevelId
			FROM [USSecurityLevelObject] SO
			INNER JOIN PageDefinition P ON SO.ObjectId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageScript]
					   ([PageDefinitionId]
					   ,[ScriptId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[ScriptId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageScript] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageStyle]
					   ([PageDefinitionId]
					   ,[StyleId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[StyleId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageStyle] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[COTaxonomyObject]
					   ([Id]
					   ,[TaxonomyId]
					   ,[ObjectId]
					   ,[ObjectTypeId]
					   ,[SortOrder]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate])
			 SELECT NEWID()
				  ,[TaxonomyId]
				  ,P.PageDefinitionId
				  ,[ObjectTypeId]
				  ,[SortOrder]
				  ,[Status]
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
			  FROM [dbo].[COTaxonomyObject] OT
			INNER JOIN PageDefinition P ON OT.ObjectId= P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

END

GO
Print 'Creating Procedure VariantSite_CopyPageVersion'
GO

IF(OBJECT_ID('VariantSite_CopyPageVersion') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyPageVersion
GO

CREATE PROCEDURE [dbo].[VariantSite_CopyPageVersion]
(  
	@SiteId UNIQUEIDENTIFIER,
	@MicroSiteId UNIQUEIDENTIFIER,
	@ModifiedBy  UNIQUEIDENTIFIER,
	@MakePageStatusAsDraft BIT
	
)
AS
BEGIN

	insert into #tempIds select Id, NEWID() from 
			(Select distinct Id,ObjectId
			from (
			Select ROW_NUMBER () Over (Partition By ObjectId order by CreatedDate desc) RowNum,ObjectId,Id,XMLString, ObjectStatus, VersionStatus,Comments, TriggeredObjectId,TriggeredObjectTypeId 
			FROM VEVersion 
			Where ObjectTypeId =8
			)V
			Where V.RowNum =1) LV inner  join PageDefinition pd on LV.ObjectId =pd.SourcePageDefinitionId where pd.SiteId=@MicroSiteId


			INSERT INTO [dbo].[VEVersion]
					   ([Id]
					   ,[ApplicationId]
					   ,[ObjectTypeId]
					   ,[ObjectId]
					   ,[CreatedDate]
					   ,[CreatedBy]
					   ,[VersionNumber]
					   ,[RevisionNumber]
					   ,[Status]
					   ,[ObjectStatus]
					   ,[VersionStatus]
					   ,[XMLString]
					   ,[Comments]
					   ,[TriggeredObjectId]
					   ,[TriggeredObjectTypeId])          
					   select T.NewId,
					   @MicroSiteId ,
					   8, 
					   pd.PageDefinitionId, 
					   GETUTCDATE(),
					   @ModifiedBy ,
					   1 ,
					   0,
					   'false' ,
					   LV.ObjectStatus,
					   LV.VersionStatus, 
					   case when @MakePageStatusAsDraft=1 then dbo.UpdatePageXml(LV.XMLString, pd.PageDefinitionId,pd.WorkflowId,2,dbo.GetActiveStatus(),0, pd.CreatedBy,pd.CreatedDate, pd.AuthorId,pd.SiteId,pd.SourcePageDefinitionId,null)
					   else dbo.UpdatePageXml(LV.XMLString, pd.PageDefinitionId,pd.WorkflowId, case when pd.WorkflowState =1 then pd.WorkflowState else 2 end,pd.PageStatus,1 ,pd.CreatedBy,pd.CreatedDate, pd.AuthorId,pd.SiteId,pd.SourcePageDefinitionId,null) end, 
					   LV.Comments,
					   LV.TriggeredObjectId,
					   LV.TriggeredObjectTypeId       
			  from VEVersion LV
			 inner join #tempIds T on LV.Id= T.OldId 
			 inner  join PageDefinition pd on LV.ObjectId =pd.SourcePageDefinitionId 
			 where pd.SiteId=@MicroSiteId
			 
			 create table #VPSIds (OldId uniqueidentifier, NewId uniqueidentifier)
			 
			 insert into #VPSIds select Id, NEWID() from [dbo].[VEPageDefSegmentVersion] VPS inner join #tempIds T on VPS.VersionId= T.OldId
			 
			 INSERT INTO [dbo].[VEPageDefSegmentVersion]
					   ([Id]
					   ,[VersionId]
					   ,[PageDefinitionId]
					   ,[MinorVersionNumber]
					   ,[DeviceId]
					   ,[AudienceSegmentId]   
					   ,[ContainerContentXML]        
					   ,[UnmanagedXml]
					   ,[ZonesXml]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   )
				SELECT TPS.NewId 
				  ,V.Id
				  ,V.ObjectId 
				  ,[MinorVersionNumber]
				  ,[DeviceId]
				  ,[AudienceSegmentId]
				  ,[ContainerContentXML]  
				  ,[UnmanagedXml]
				  ,[ZonesXml]
				  ,@ModifiedBy
				  ,GETUTCDATE()
			  FROM [dbo].[VEPageDefSegmentVersion] VPS
			  inner join #VPSIds TPS on VPS.Id= TPS.OldId 
			  inner join #tempIds T on VPS.VersionId = T.OldId 
			  inner join VEVersion V on T.NewId = V.Id  
			  

			 truncate table #tempIds 

			 insert into #tempIds select VC.Id, newId() from dbo.VEContentVersion VC inner join [dbo].[VEPageContentVersion] VPC on VC.Id= VPC.ContentVersionId  inner join #VPSIds VPS on VPC.PageDefSegmentVersionId = VPS.OldId

			 INSERT INTO [dbo].[VEContentVersion]
					   ([Id]
					   ,[ContentId]
					   ,[XMLString]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate])
				 SELECT T.NewId 
				  ,[ContentId]
				  ,[XMLString]
				  ,[Status]
				  ,@ModifiedBy 
				  ,GETUTCDATE()
				  ,@ModifiedBy 
				  ,GETUTCDATE()
			  FROM [dbo].[VEContentVersion] VC
			  inner join #tempIds T on VC.Id= T.OldId 


			INSERT INTO [dbo].[VEPageContentVersion]
					   ([Id]
					   ,[ContentVersionId]
					   ,[IsChanged]
					   ,[PageDefSegmentVersionId])
			   
			SELECT NewId()
				  ,T.NewId
				  ,[IsChanged]
				  ,VPS.NewId
			  FROM [dbo].[VEPageContentVersion] VPC
			inner join #VPSIds VPS on VPC.PageDefSegmentVersionId = VPS.OldId 
			inner join #tempIds T on VPC.ContentVersionId = T.OldId

			drop table #VPSIds 
END
GO

Print 'Creating Procedure VariantSite_ImportUser'
GO

IF(OBJECT_ID('VariantSite_ImportUser') IS NOT NULL)
	DROP PROCEDURE VariantSite_ImportUser
GO


CREATE PROCEDURE [dbo].[VariantSite_ImportUser]
(  
	@MasterSiteId UNIQUEIDENTIFIER,
	@MicroSiteId UNIQUEIDENTIFIER,
	@ModifiedBy  UNIQUEIDENTIFIER,
	@ImportAllWebSiteUser BIT,
	@ImportAllAdminUserAndPermission BIT,
	@ProductId UNIQUEIDENTIFIER
	
)
AS
BEGIN

DECLARE @now DATETIME
	SET @now = GETUTCDATE()

--CMSFrontUSer
 IF(@ImportAllWebSiteUser = 1)
	BEGIN			
		INSERT INTO [dbo].[USSiteUser]
		(
			[SiteId],
			[UserId],
			[IsSystemUser],
			[ProductId]
		)
		SELECT @MicroSiteId,
			SU.UserId,
			SU.ISSystemUser,
			SU.ProductId
		FROM USUser U 
			INNER JOIN USSiteUser SU ON U.Id = SU.UserId  
			INNER JOIN USMembership M ON SU.UserId = M.UserId  
		WHERE SU.SiteId = @MasterSiteId AND SU.ProductId = @ProductId  
			AND SU.IsSystemUser = 0 AND  U.Status = 1 
			AND M.IsApproved = 1 AND M.IsLockedOut= 0 AND u.ExpiryDate >= @now
	END
	
	-- This will not insert any record as we have not inserted any record in USSiteuser table
	INSERT INTO USMemberGroup(MemberId, MemberType, GroupId, ApplicationId) 
	SELECT SU.UserId, 1, MG.GroupId, @MicroSiteId  
	FROM USSiteUser SU 
		INNER JOIN USMemberGroup MG on SU.UserId = MG.MemberId 
	WHERE SU.SiteId = @MicroSiteId 
		AND MG.ApplicationId = @MasterSiteId 

	--*** Admin *** User
	IF (@ImportAllAdminUserAndPermission = 0)
	BEGIN

		DECLARE @CurrentUserName nvarchar(500)
			SELECT TOP 1 @CurrentUserName = UserName FROM USUser where Id = @ModifiedBy

			-- Giving permission to Default users. Allow only CMS , Analyzer and Social product default users
			INSERT INTO [dbo].[USSiteUser]
			(
				[SiteId],
				[UserId],
				[IsSystemUser],
				[ProductId]
			)
			SELECT @MicroSiteId AS SiteId,
				[UserId],
				[IsSystemUser],
				[ProductId] 
			FROM USSiteUser 
			WHERE UserId IN (SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin', @CurrentUserName)) AND
				SiteId = @MasterSiteId 
			EXCEPT
			SELECT @MicroSiteId AS SiteId,
				[UserId],
				[IsSystemUser],
				[ProductId]  
			FROM USSiteUser 
			WHERE UserId IN (SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin', @CurrentUserName)) AND
				SiteId = @MicroSiteId 	
			
			INSERT INTO [dbo].[USMemberGroup]
			(
				[ApplicationId],
				[MemberId],
				[MemberType],
				[GroupId]
			)   
			SELECT @MicroSiteId AS ApplicationId, 
				MemberId, 
				MemberType, 
				GroupId
			FROM USMemberGroup  
			WHERE MemberId IN (SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin', @CurrentUserName)) AND
				ApplicationId = @MasterSiteId AND
				GroupId IN (SELECT Id FROM USGroup WHERE ApplicationId = ProductId)
			EXCEPT
			SELECT @MicroSiteId AS 
				ApplicationId, 
				MemberId, 
				MemberType,
				GroupId
			FROM USMemberGroup  
			WHERE MemberId IN (SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin', @CurrentUserName)) AND
				ApplicationId = @MicroSiteId
		END
END
GO


Print 'Creating Procedure VariantSite_CopyMisc'
GO

IF(OBJECT_ID('VariantSite_CopyMisc') IS NOT NULL)
	DROP PROCEDURE VariantSite_CopyMisc
GO

CREATE PROCEDURE [dbo].[VariantSite_CopyMisc]
(  
	@MicroSiteId UNIQUEIDENTIFIER,
	@ModifiedBy  UNIQUEIDENTIFIER
)
AS
BEGIN

DECLARE @now DATETIME
	SET @now = GETUTCDATE()

	--HSStructure data for Blog
	DECLARE @HParentId uniqueidentifier, @SiteDirectoryObjectTypeId int
	DECLARE @RgtValue bigint, @SiteTitle nvarchar(1000)

	set @HParentId  = dbo.GetNetEditorId()
	SELECT @RgtValue = RgtValue FROM HSStructure WHERE Id = @HParentId
	SET @SiteDirectoryObjectTypeId= dbo.GetSiteDirectoryObjectType()

	IF NOT EXISTS(SELECT 1 FROM COTaxonomy WHERE ApplicationId = @MicroSiteId)
	BEGIN
		--Create the default node for the INDEX TERMS
		INSERT INTO COTaxonomy
		(
			Id,
			ApplicationId,
			Title,
			Description,
			CreatedDate,
			CreatedBy,
			ModifiedBy,
			ModifiedDate,
			Status,
			LftValue,
			RgtValue,
			ParentId
		)
		VALUES
		(
			NEWID(),
			@MicroSiteId,
			'Index Terms',
			'Index Terms',
			@now,
			(SELECT TOP 1 Id FROM USUser WHERE UserName='iappssystemuser'),
			NULL,
			NULL,
			1,
			1,
			2,
			NULL
		)
	END

	IF ((@RgtValue IS NOT NULL)
		AND (NOT EXISTS(SELECT 1 FROM HSStructure WHERE Title='Blog' And SiteId = @MicroSiteId )))
	BEGIN
		Set @RgtValue = 1
		
		Select @SiteTitle=Title from sisite where Id=@MicroSiteId

		if not exists(select * from Hsstructure where id=@MicroSiteId)
		begin
			INSERT INTO HSStructure(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@MicroSiteId,@SiteTitle,dbo.GetSiteObjectType(),null,@HParentId,@RgtValue,@RgtValue+37,@MicroSiteId)
		end
		Declare @L1Id uniqueidentifier
		SELECT @L1Id =newid()
		INSERT INTO HSStructure
		(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
		Values(@L1Id,'Blog',@SiteDirectoryObjectTypeId,null,@MicroSiteId,@RgtValue+35,@RgtValue+36,@MicroSiteId)

		EXEC	[dbo].[SiteDirectory_Save]
		@Id = @L1Id OUTPUT,
		@ApplicationId = @MicroSiteId,
		@Title = N'Blog',
		@Description = NULL,
		@ModifiedBy = @ModifiedBy,
		@ModifiedDate = NULL,
		@ObjectTypeId = 39,
		@PhysicalPath = NULL,
		@VirtualPath = N'~/Blog'	
		-- Inserting to default Parent table
		INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
		VALUES(@MicroSiteId,39,@L1Id)	
	End

	DECLARE @FolderXml xml, @PathXml nvarchar(max), @FormParentId uniqueidentifier, @SiteName nvarchar(max), @FormFolderId uniqueidentifier
	SELECT TOP 1 @FormParentId = Id FROM COFormStructure WHERE LftValue = 1 AND (ParentId IS NULL OR ParentId = SiteId) AND SiteId = @MicroSiteId
	
	IF @FormParentId IS NOT NULL
	BEGIN
		SET @FormFolderId = NEWID()

		SET @FolderXml = CONVERT(xml, N'<SiteDirectoryCollection><SiteDirectory Id="' + cast(@FormFolderId as char(36)) + '" Title="Distributed Forms" Description="Distributed Forms" CreatedBy="' + 
						CAST(@ModifiedBy AS CHAR(36)) + '" CreatedDate="' + CAST(GETUTCDATE() AS VARCHAR(25)) +
						'" Status="1" ObjectTypeId="38" IsSystem="True" ParentId="' + cast(@FormParentId as char(36)) + '" Lft="2" Rgt="3" IsMarketierDir="False"/></SiteDirectoryCollection>')

		SET @PathXml = N'<SISiteDirectory Id="'+ cast(@FormFolderId as char(36)) +'" VirtualPath="~/Form Library/Distributed Forms"/>'

		IF NOT EXISTS(SELECT 1 FROM COFormStructure WHERE Title = 'Distributed Forms' and SiteId = @MicroSiteId)
		BEGIN
			EXEC SqlDirectoryProvider_Save		
				@XmlString = @FolderXml,
				@ParentId = @FormParentId,
				@ApplicationId = @MicroSiteId,
				@ModifiedBy = @ModifiedBy,
				@MicroSiteId = null,
				@VirtualPath = @PathXml output,
				@SiteName = @SiteName output
		END
	END
	
END
GO

PRINT 'Creating table CLImageVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'CLImageVariant'))
BEGIN
	CREATE TABLE [dbo].[CLImageVariant](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](256) NULL,
	[Description] [nvarchar](4000) NULL,
	[FileName] [nvarchar](256) NULL,
	[Size] [int] NULL,
	[RelativePath] [nvarchar](4000) NULL,
	[AltText] [nvarchar](1024) NULL,
	[Height] [int] NULL,
	[Width] [int] NULL,
	[ImageType] [uniqueidentifier] NULL,
	[ParentImage] [uniqueidentifier] NULL,
	[Status] [int] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	CONSTRAINT [PK_CLImageVariant] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC,
		[SiteId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	

	ALTER TABLE [dbo].[CLImageVariant] ADD  CONSTRAINT [DF_CLImageVariant_Status]  DEFAULT ((1)) FOR [Status]
	
END
GO

PRINT 'Creating table CLImageVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'CLImageVariantCache'))
BEGIN
	CREATE TABLE [dbo].[CLImageVariantCache](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](256) NULL,
	[Description] [nvarchar](4000) NULL,
	[FileName] [nvarchar](256) NULL,
	[Size] [int] NULL,
	[RelativePath] [nvarchar](4000) NULL,
	[AltText] [nvarchar](1024) NULL,
	[Height] [int] NULL,
	[Width] [int] NULL,
	[ImageType] [uniqueidentifier] NULL,
	[ParentImage] [uniqueidentifier] NULL,
	[Status] [int] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	CONSTRAINT [PK_CLImageVariantCache] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC,
		[SiteId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[CLImageVariantCache] ADD  CONSTRAINT [DF_CLImageVariantCache_Status]  DEFAULT ((1)) FOR [Status]
	
END
GO
Print 'Creating Procedure Site_CopyMenusAndPages'
GO

IF(OBJECT_ID('Site_CopyMenusAndPages') IS NOT NULL)
	DROP PROCEDURE Site_CopyMenusAndPages
GO

CREATE PROCEDURE [dbo].[Site_CopyMenusAndPages]
(  
	@SiteId uniqueidentifier,
	@MicroSiteId uniqueidentifier,
	@ModifiedBy  uniqueidentifier,
	@CopiedAllUsersFromMaster bit,
	@PageImportOptions int =2
)
AS
BEGIN

if(@PageImportOptions = 0) -- Create default nodes root node and unassigned when selected dont import menus and pages
BEGIN
	EXEC [dbo].[VariantSite_CopyDefaultMenus] @MicroSiteId, @ModifiedBy
END
ELSE
BEGIN
		declare @ImportMenusOnly bit 
		Set @ImportMenusOnly =0
		declare @MakePageStatusAsDraft bit 
		Set @MakePageStatusAsDraft = 1

		if(@PageImportOptions = 1) set @ImportMenusOnly = 1  -- Import only menus
		if(@PageImportOptions = 3) set @MakePageStatusAsDraft = 0 -- @PageImportOptions =3 Import menus and pages, page status same as master site page status, @PageImportOptions =2 import all pages as created page status and workflow state as draft

		IF (Select count(*)  FROM [dbo].[PageMapNode]   Where SiteId=@MicroSiteId ) =0
		BEGIN

			  EXEC [dbo].[VariantSite_CopyMenus] @SiteId, @MicroSiteId, @ModifiedBy

			  INSERT INTO [dbo].[USSecurityLevelObject]
					   ([ObjectId]
					   ,[ObjectTypeId]
					   ,[SecurityLevelId])
			  SELECT P.PageMapNodeId
					   ,ObjectTypeId
					   ,SecurityLevelId
				FROM [USSecurityLevelObject] SO
				INNER JOIN PageMapNode P ON SO.ObjectId = P.SourcePageMapNodeId
				 Where P.SiteId = @MicroSiteId


				IF(@CopiedAllUsersFromMaster = 1)
				   BEGIN
				   -- This is updating the custome permission page map node id
					  INSERT INTO [dbo].[USMemberRoles]
							   ([ProductId]
							   ,[ApplicationId]
							   ,[MemberId]
							   ,[MemberType]
							   ,[RoleId]
							   ,[ObjectId]
							   ,[ObjectTypeId]
							   ,[Propagate]
							   )
						SELECT Distinct [ProductId]
						  ,@MicroSiteId
						  ,[MemberId]
						  ,[MemberType]
						  ,[RoleId]
						  ,P.PageMapNodeId
						  ,[ObjectTypeId]
						  ,[Propagate]
					  FROM [dbo].[USMemberRoles]M
					INNER JOIN PageMapNode P ON M.ObjectId = P.SourcePageMapNodeId
					 Where P.SiteId = @MicroSiteId 
					 and MemberId not in(select Id from USGroup where GroupType=2) -- Excluding custom groups, custom groups are not shared with variant sites
				 END 
	 
				 IF(@ImportMenusOnly = 0)
				 BEGIN
	 					CREATE TABLE  #tempIds (OldId UNIQUEIDENTIFIER, NewId UNIQUEIDENTIFIER)

						-- Create pages for the microsite
						EXEC [dbo].[VariantSite_CopyPages] @SiteId, @MicroSiteId, @ModifiedBy, @MakePageStatusAsDraft
			 
						 --create microsite pages version from master pages latest version
						 EXEC [dbo].[VariantSite_CopyPageVersion] @SiteId, @MicroSiteId, @ModifiedBy, @MakePageStatusAsDraft
			
						drop table #tempIds
					END
		 END
   END
END

GO

Print 'Creating Procedure Product_GetAttributes'
GO

IF(OBJECT_ID('Product_GetAttributes') IS NOT NULL)
	DROP PROCEDURE Product_GetAttributes
GO

CREATE PROCEDURE [dbo].[Product_GetAttributes]
(
	@ProductId uniqueidentifier,
	@SystemAttributeFlag int = 0,
	@ApplicationId uniqueidentifier=null,
	@CategoryId int = null
)
AS
BEGIN
IF (@CategoryId is not null)
BEGIN

	if(@SystemAttributeFlag=0)
		Begin
			select  Distinct
				A.Id,
				AG.CategoryId,
				A.AttributeDataType,
				A.AttributeDataTypeId,
				A.Title,
				A.Description,
				A.Sequence,
				A.Code,
				A.IsFaceted,
				A.IsSearched,
				A.IsSystem,
				A.IsDisplayed,
				A.IsEnum,	
				A.IsPersonalized,
				PA.IsRequired,			
				dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
				A.CreatedBy,
				dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
				A.ModifiedBy,
				A.Status,
				PA.IsSKULevel,
				AG.AttributeGroupId
			from PRProductAttribute PA 
				inner join ATAttribute A  ON PA.AttributeId = A.Id
				LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId  
			where PA.AttributeId=A.Id 
				and A.Status=dbo.GetActiveStatus() 
				and PA.ProductId = @ProductId
				and AG.CategoryId = @CategoryId 
		end
	else if (@SystemAttributeFlag = 1)
		begin
			select Distinct
				A.Id,
				AG.CategoryId,
				A.AttributeDataType,
				A.AttributeDataTypeId,
				A.Title,
				A.Description,
				A.Sequence,
				A.Code,
				A.IsFaceted,
				A.IsSearched,
				A.IsSystem,
				A.IsDisplayed,
				A.IsEnum,
				PA.IsRequired,				
				A.IsPersonalized,
				dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
				A.CreatedBy,
				dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
				A.ModifiedBy,
				A.Status,
				PA.IsSKULevel,
				AG.AttributeGroupId
			from PRProductAttribute PA 
				inner join ATAttribute A  ON PA.AttributeId = A.Id
				LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId  
			where A.Status=dbo.GetActiveStatus() 
				and PA.ProductId = @ProductId
				and A.IsSystem = 0
				and AG.CategoryId = @CategoryId
		end
	else 
		begin
			select  Distinct 
				A.Id,
				AG.AttributeId,
				A.AttributeDataType,
				A.AttributeDataTypeId,
				A.Title,
				A.Description,
				A.Sequence,
				A.Code,
				A.IsFaceted,
				A.IsSearched,
				A.IsSystem,
				A.IsDisplayed,
				A.IsEnum,		
				PA.IsRequired,		
				A.IsPersonalized,
				dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
				A.CreatedBy,
				dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
				A.ModifiedBy,
				A.Status,
				PA.IsSKULevel,
				AG.AttributeGroupId
			from PRProductAttribute PA 
				inner join ATAttribute A  ON PA.AttributeId = A.Id
				LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId  
			where A.Status=dbo.GetActiveStatus() 
				and PA.ProductId = @ProductId	
				and A.IsSystem = 1
				and AG.CategoryId = @CategoryId 
		end
END
ELSE -- IF Group Id is null dont even Check it
BEGIN
	if(@SystemAttributeFlag=0)
		Begin
			select  Distinct
				A.Id,
				AG.CategoryId,
				A.AttributeDataType,
				A.AttributeDataTypeId,
				A.Title,
				A.Description,
				A.Sequence,
				A.Code,
				A.IsFaceted,
				A.IsSearched,
				A.IsSystem,
				A.IsDisplayed,
				A.IsEnum,	
				A.IsPersonalized,
				PA.IsRequired,			
				dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
				A.CreatedBy,
				dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
				A.ModifiedBy,
				A.Status,
				PA.IsSKULevel,
				AG.AttributeGroupId
			from PRProductAttribute PA 
				inner join ATAttribute A  ON PA.AttributeId = A.Id
				LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId  
			where PA.AttributeId=A.Id 
				and A.Status=dbo.GetActiveStatus() 
				and PA.ProductId = @ProductId
				---and AG.Id = Isnull(@CategoryId,AG.Id) 
		end
	else if (@SystemAttributeFlag = 1)
		begin
			select Distinct
				A.Id,
				AG.CategoryId,
				A.AttributeDataType,
				A.AttributeDataTypeId,
				A.Title,
				A.Description,
				A.Sequence,
				A.Code,
				A.IsFaceted,
				A.IsSearched,
				A.IsSystem,
				A.IsDisplayed,
				A.IsEnum,
				PA.IsRequired,				
				A.IsPersonalized,
				dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
				A.CreatedBy,
				dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
				A.ModifiedBy,
				A.Status,
				PA.IsSKULevel,
				AG.AttributeGroupId
			from PRProductAttribute PA 
				inner join ATAttribute A  ON PA.AttributeId = A.Id
				LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
			where A.Status=dbo.GetActiveStatus() 
				and PA.ProductId = @ProductId
				and A.IsSystem = 0
				--and AG.Id = Isnull(@CategoryId,AG.Id) 
		end
	else 
		begin
			select  Distinct 
				A.Id,
				AG.CategoryId,
				A.AttributeDataType,
				A.AttributeDataTypeId,
				A.Title,
				A.Description,
				A.Sequence,
				A.Code,
				A.IsFaceted,
				A.IsSearched,
				A.IsSystem,
				A.IsDisplayed,
				A.IsEnum,		
				PA.IsRequired,		
				A.IsPersonalized,
				dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
				A.CreatedBy,
				dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
				A.ModifiedBy,
				A.Status,
				PA.IsSKULevel,
				AG.AttributeGroupId
			from PRProductAttribute PA 
				inner join ATAttribute A  ON PA.AttributeId = A.Id
				LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
			where A.Status=dbo.GetActiveStatus() 
				and PA.ProductId = @ProductId	
				and A.IsSystem = 1
				--and AG.Id = Isnull(@CategoryId,AG.Id) 
		end

END


END
GO


Print 'Creating Procedure Warehouse_Get'
GO

IF(OBJECT_ID('Warehouse_Get') IS NOT NULL)
	DROP PROCEDURE Warehouse_Get
GO

CREATE PROCEDURE [dbo].[Warehouse_Get](
@Id			uniqueidentifier = null,
@TypeIds	nvarchar(200) = null,
@Status		int = null,
@ApplicationId uniqueidentifier = null
)
AS
BEGIN
	SELECT W.[Id]
		  ,S.[SiteId]
		  ,[Title]
		  ,[Description]
		  ,[AddressId]
		  ,[IsDefault]
		  ,[CreatedDate]
		  ,[CreatedBy]
		  ,[ModifiedDate]
		  ,[ModifiedBy]
		  ,S.[Status]
		  ,TypeId
		  ,RestockingDelay
		  ,WarehouseCode
		  ,DotNetProviderName
	  FROM [dbo].[INWarehouse] W
	  INNER JOIN INWarehouseSite S ON W.Id = S.WarehouseId
	Where (@Id is null or Id=@Id)
		AND (@TypeIds IS NULL OR TypeId IN (SELECT Value FROM [dbo].[SplitComma](@TypeIds, ',')))
		AND (@Status IS NULL OR S.[Status] = @Status)
		AND (@ApplicationId IS NULL 
			OR @ApplicationId=dbo.GetEmptyGUID() 
			OR S.SiteId =@ApplicationId)

	Select WD.WarehouseId,C.Id,C.CountryName,C.CountryCode,R.Name Region,C.CreatedDate,C.CreatedBy,C.ModifiedDate,C.ModifiedBy,C.Status 
	from dbo.INWarehouseDeliveryZone WD
	INNER JOIN GLCountry C on WD.CountryId = C.Id
	INNER JOIN GLCountryRegion R on C.RegionId =R.Id
	Where @Id is null OR WD.WarehouseId = @Id 

END
GO

IF(OBJECT_ID('PageMap_GetReDirect') IS NOT NULL)
	DROP PROCEDURE PageMap_GetReDirect
GO

CREATE PROCEDURE [dbo].[PageMap_GetReDirect]
(
	@ApplicationId		uniqueidentifier,
	@IsGenerated		bit = NULL,
	@Keyword			nvarchar(max) = NULL,
	@PageSize			int = NULL,
	@PageNumber			int = NULL
)
AS
BEGIN
	SELECT DISTINCT
		OldUrl, 
		NewUrl, 
		IsGenerated,
		IsShared
	FROM PageRedirect
	WHERE (ApplicationId = @ApplicationId 
		OR IsShared = 1 )
		AND Status = 1
END
GO



PRINT 'Create View vwOrderShipmentPackage'
GO

IF(OBJECT_ID('vwOrderShipmentPackage') IS NOT NULL)
	DROP VIEW vwOrderShipmentPackage
GO

CREATE VIEW [dbo].[vwOrderShipmentPackage]
AS
SELECT	OSP.*, OSG.OrderId
FROM	FFOrderShipmentPackage AS OSP
		INNER JOIN FFOrderShipment AS OST ON OST.Id = OSP.OrderShipmentId
		INNER JOIN OROrderShipping AS OSG ON OSG.Id = OST.OrderShippingId
GO
IF(OBJECT_ID('Sku_GetPrice') IS NOT NULL)
	DROP PROCEDURE Sku_GetPrice
GO

CREATE PROCEDURE [dbo].[Sku_GetPrice](
			 @CustomerId uniqueidentifier = null,
			 @SKUQuantity  TYSKUQuantity READONLY,
			 @ApplicationId uniqueidentifier=null
										)
AS

BEGIN
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId' and 
					(@ApplicationId is null or STS.SiteId = @ApplicationId))

			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale,  ISNULL(PST.EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY S.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @SKUQuantity inSKU 
				INNER JOIN PRProductSKU S on inSKU.SKUId = S.Id
				LEFT JOIN PRProductSKUVariant SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					Where CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= inSKU.SKUId AND inSKU.Quantity between minQuantity and maxQuantity
			) EFPS
			WHERE EFPS.Sno=1				

		End
		else
		Begin
			Select SkuId,ProductId,MinQuantity,MaxQuantity,PriceSetId,IsSale,EffectivePrice,ListPrice 
			FROM
			(
				Select S.Id SkuId,S.ProductId,ISNULL(PST.MinQuantity,1)MinQuantity, ISNULL(PST.MaxQuantity,1.79E+308)MaxQuantity, PST.PriceSetId,
					ISNULL(PST.IsSale,0) IsSale, ISNULL(PST.EffectivePrice, ISNULL(SV.ListPrice,S.ListPrice)) EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY S.Id ORDER BY PST.EffectivePrice ASC))  as Sno,ISNULL(SV.ListPrice,S.ListPrice) ListPrice
				from @SKUQuantity inSKU 
				INNER JOIN PRProductSKU S on inSKU.SKUId = S.Id
				LEFT JOIN PRProductSkuVariant SV on S.Id =SV.Id and SV.SiteId=@ApplicationId
				LEFT JOIN (SELECT PSC.SKUId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice FROM dbo.vwEffectivePriceSets PSC 
					INNER join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					INNER join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					Where USMG.MemberId = @CustomerId 
					and PS.SiteId = @ApplicationId 
					AND @Today Between PS.StartDate AND PS.EndDate 		
					) PST  ON  PST.SkuId= inSKU.SKUId AND inSKU.Quantity between minQuantity and maxQuantity
			) EFPS
			WHERE EFPS.Sno=1				
		End
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_FFOrderShipment_OrderShippingId' AND object_id = OBJECT_ID('FFOrderShipment'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_FFOrderShipment_OrderShippingId]
		ON [dbo].[FFOrderShipment]([OrderShippingId] ASC)
		INCLUDE([Id]);
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_FFOrderShipmentItem_OrderShipmentId' AND object_id = OBJECT_ID('FFOrderShipmentItem'))
BEGIN
CREATE NONCLUSTERED INDEX [IX_FFOrderShipmentItem_OrderShipmentId]
    ON [dbo].[FFOrderShipmentItem]([OrderShipmentId] ASC)
    INCLUDE([Id], [OrderItemId], [Quantity], [CreateDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [OrderShipmentContainerId], [ExternalReferenceNumber]);
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_FFOrderShipmentPackage_OrderShipmentId' AND object_id = OBJECT_ID('FFOrderShipmentPackage'))
BEGIN
CREATE NONCLUSTERED INDEX [IX_FFOrderShipmentPackage_OrderShipmentId]
    ON [dbo].[FFOrderShipmentPackage]([OrderShipmentId] ASC)
    INCLUDE([Id], [TrackingNumber], [CreatedDate], [ChangedBy], [ModifiedDate], [ModifiedBy], [ShippingContainerId], [ShippingCharge], [ShipmentStatus], [ShippingLabel], [Weight]);
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_CPOrderCouponCode_OrderId' AND object_id = OBJECT_ID('CPOrderCouponCode'))
BEGIN
CREATE NONCLUSTERED INDEX [IX_CPOrderCouponCode_OrderId]
    ON [dbo].[CPOrderCouponCode]([OrderId] ASC)
    INCLUDE([CouponCodeId]);
END
GO

IF(OBJECT_ID('Product_SaveVariant') IS NOT NULL)
	DROP PROCEDURE Product_SaveVariant
GO
CREATE PROCEDURE [dbo].[Product_SaveVariant]
(
	@Id uniqueidentifier output ,
	@ProductCode nvarchar(50) = NULL,
	@Title	nvarchar(max) = NULL,
	@ShortDescription nvarchar(max)= NULL,
	@LongDescription nvarchar(max)= NULL,
	@Description nvarchar(max) = NULL,
    @Keywords nvarchar(max)= NULL,
    @Status int = NULL,
	@IsBundle tinyint = NULL,
	@IsActive tinyint = NULL,
    @ProductTypeId uniqueidentifier = NULL,
    @ApplicationId uniqueidentifier = NULL,
    @ModifiedBy uniqueidentifier = NULL,
	@UrlFriendlyTitle nvarchar(500)=null,
	@TaxCategoryId uniqueidentifier = null
)
as
begin
	Declare @CurrentDate datetime
	set @CurrentDate = GetUTCDate() 

	--if (@UrlFriendlyTitle is null OR @UrlFriendlyTitle ='')
	--Begin
	--	set @UrlFriendlyTitle = dbo.MakeFriendlyProductTitle(@Title,)
	--End



	if(@Id is NOT null AND NOT EXISTS(SELECT 1 FROM PRProductVariant Where Id=@Id AND SiteId=@ApplicationId))
	Begin		
		--IF Exists(Select Id from PRProduct Where (ProductIDUser) =(@ProductCode))
		--BEGIN
		--	RAISERROR('Product with this code already exists. Please choose a different product code.', 16, 1)
		--END		
		


        insert into PRProductVariant 
			([Id]
			--,[ProductIDUser]
			,[Title], UrlFriendlyTitle 
			,[ShortDescription]
			,[LongDescription]
			,[Description]
			,[Keyword]
			--,[IsBundle]
			,[IsActive]
			--,[Status]
			,[ProductTypeID]
			,[SiteId]
			--,[CreatedDate]
			--,[CreatedBy]
			,[TaxCategoryID]
			)
		 values
			(@Id,
			--@ProductCode,
			@Title, @UrlFriendlyTitle, 
			@ShortDescription,
			@LongDescription,
			@Description,
			@Keywords,
			--@IsBundle,
			@IsActive,
			--@Status,
			@ProductTypeId,
			@ApplicationId,
			--@CurrentDate,
			--@ModifiedBy,
			@TaxCategoryId)

		
		
	end
	else
	begin

		--IF (@Id is not null and Exists(Select Id from PRProduct Where (ProductIDUser) =(@ProductCode) AND Id!=@Id))
		--BEGIN
		--	RAISERROR('Product with this code already exists. Please choose a different product code.', 16, 1)
		--END
		
		if(@Id is not null and EXISTS (SELECT 1 FROM PRProductVariant  where @Id=Id AND SiteId=@ApplicationId))

		update PRProductVariant 
		set	 --[ProductIDUser]=@ProductCode
			--,
			[Title] = @Title
			,[UrlFriendlyTitle] = @UrlFriendlyTitle
			,[ShortDescription] =@ShortDescription
			,[LongDescription] =@LongDescription
			,[Description] =@Description
			,[Keyword] =@Keywords
			,[IsActive]=@IsActive
			--,[IsBundle] =@IsBundle
			--,[Status] =@Status
			,[ProductTypeID] =@ProductTypeId
			,[SiteId]=@ApplicationId
			,[ModifiedDate]=@CurrentDate
			,[ModifiedBy]=@ModifiedBy
			,[TaxCategoryID]=@TaxCategoryId
		where Id =@Id AND SiteId=@ApplicationId
		
	
	End
		
	
end

GO


IF(OBJECT_ID('Product_MakeInactiveVariant') IS NOT NULL)
	DROP PROCEDURE Product_MakeInactiveVariant
GO

CREATE PROCEDURE [dbo].[Product_MakeInactiveVariant]  
(  
	@Id				uniqueidentifier OUTPUT,  
	@ModifiedBy uniqueidentifier   
)  
as  
begin  
 Declare @CurrentDate datetime  
 set @CurrentDate = GetUTCDate()   

Update S Set S.IsActive=0
FROM PRProductSKUVariant S 
INNER JOIN PRProductSKU PS ON S.Id =PS.Id
INNER JOIN PRProduct P on P.Id=PS.ProductId
Where P.Id=@Id

Update P Set P.IsActive = 0
FROM PRProductVariant P WHERE P.Id = @Id
end


GO


IF(OBJECT_ID('Sku_SaveVariant') IS NOT NULL)
	DROP PROCEDURE Sku_SaveVariant
GO

CREATE PROCEDURE [dbo].[Sku_SaveVariant]
	(
		
	@Id				uniqueidentifier OUTPUT,  
    @ProductId		uniqueidentifier,
    @ApplicationId uniqueidentifier,  
	@SKU			nvarchar(255)=null, 
    @Title			nvarchar(255)=null,  
    @Description	text=NULL, 
    @ListPrice		money = NULL,		
    @UnitPrice		money = NULL,  
    @Status			int = NULL, 
	@Measure		nvarchar(50) = NULL,
	@OrderMinimum	money = NULL,
	@OrderIncrement	money = NULL,
	@HandlingCharges money = NULL,
	@SystemAttributes xml = NULL,  
	@SKUAttributes xml = NULL,  
	@IsActive bit = NULL,
	@IsOnline bit = NULL,
	@SkuIds		xml out,	
	@ModifiedBy uniqueidentifier = NULL,
	@Length	decimal(18,4)=null,   
	@Height	decimal(18,4)=null,   
	@Width	decimal(18,4)=null,
	@Weight decimal(18,4)=null,
	@SelfShippable bit=null,
	@AvailableForBackOrder bit =null,
	@MaximumDiscountPercentage money = null,
	@BackOrderLimit decimal(18,3)=null,
	@IsUnlimitedQuantity bit = null
)  
as  
begin  
 Declare @CurrentDate datetime  
 Declare @strId char(36)  

 set @CurrentDate = getutcdate()   
	
	Declare @InsertSkus table(SkuId uniqueidentifier)
	Declare @UpdateSkus table(SkuId uniqueidentifier)
	Declare @tempProductSKUAttribute table(Sequence int identity(1,1),AttributeId Uniqueidentifier,IsSKULevel bit,IsSystem bit,IsRequired bit)

	Insert into @UpdateSkus(SkuId)
	Select Distinct S.Id
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	Inner Join PRProductSKUVariant S on S.Id =tab.col.value('text()[1]','uniqueidentifier')
	Where S.SiteId=@ApplicationId

	Insert into @InsertSkus
	Select CASE WHEN tab.col.value('text()[1]','uniqueidentifier') = dbo.GetEmptyGUID() THEN newid() else tab.col.value('text()[1]','uniqueidentifier') END
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	except
	Select SkuId from @UpdateSkus
	
	--IF (Select count(*) From @InsertSkus)>0
	--BEGIN
	--	IF Exists(Select Id from PRProductSKUVariant Where (SKU) =(@SKU) AND @ApplicationId = SiteId)
	--	BEGIN
	--		RAISERROR('Sku already exists, Please choose a different Sku.', 16, 1)
	--		Return
	--	END		
	--END

	


	if NOT EXISTS (SELECT 1 FROM PRProductSkuVariant WHERE Id=@Id AND SiteId=@ApplicationId)
	BEGIN

	   insert into PRProductSKUVariant  
	   (Id,  
		--ProductId,  
		SiteId,  
		--SKU, 
		Title,  
		Description, 
		ListPrice,  
		UnitCost,  
		--CreatedDate,  
		--CreatedBy,  
		--Status, 
		Sequence, 
		Measure,
		OrderMinimum,
		OrderIncrement,
		HandlingCharges,
		IsActive,
		IsOnline,
		--Length,
		--Height,
		--Width,
		--Weight,
		SelfShippable,
		AvailableForBackOrder,
		MaximumDiscountPercentage,
		BackOrderLimit,
		IsUnlimitedQuantity)  
	   Select   
		@Id,  
		--@ProductId,  
		@ApplicationId,  
		--@SKU,  
		@Title,  
		@Description,  
		@ListPrice,  
		@UnitPrice,  
		--@CurrentDate,  
		--@ModifiedBy,  
		--@Status, 
		NULL,
		--(Isnull(dbo.GetMaxSKUSequence(@ProductId),0) + 1),
		@Measure,  
		@OrderMinimum,  
		@OrderIncrement,
		@HandlingCharges,
		@IsActive,
		@IsOnline,
		--@Length,   
		--@Height,   
		--@Width,
		--@Weight,
		@SelfShippable,   
		@AvailableForBackOrder, 
		@MaximumDiscountPercentage,
		NULL,
		@IsUnlimitedQuantity
		--From @InsertSkus I 
	END
	ELSE
	BEGIN
		UPDATE PRProductSKUVariant
		SET Title  =@Title,  
		Description = @Description, 
		ListPrice = @ListPrice,  
		UnitCost = @UnitPrice,  
		--Status = @Status, 
		Sequence = NULL, 
		Measure = @Measure,
		OrderMinimum = @OrderMinimum,
		OrderIncrement = @OrderIncrement,
		HandlingCharges = @HandlingCharges,
		IsActive = @IsActive,
		IsOnline = @IsOnline,
		--Length = @Length,
		--Height = @Height,
		--Width = @Width,
		--Weight = @Weight,
		SelfShippable = @SelfShippable,
		AvailableForBackOrder = @AvailableForBackOrder,
		MaximumDiscountPercentage =@MaximumDiscountPercentage ,
		BackOrderLimit = @BackOrderLimit,
		IsUnlimitedQuantity = @IsUnlimitedQuantity
		WHERE Id=@Id AND SiteId=@ApplicationId
	END

  
  update S   
  set  --ProductId=@ProductId,  
    SiteId=@ApplicationId,  
	--SKU = case when @SKU is not null then @SKU else sku end,
	--isnull(@SKU,SKU), 
    Title = case when @Title is not null then @Title else Title end, 
	--isnull(@Title,Title),  
    Description=@Description,  
    ListPrice =@ListPrice,  
    UnitCost=@UnitPrice,     
    ModifiedDate =@CurrentDate,  
    ModifiedBy=@ModifiedBy,  
    --Status = @Status, 
	Measure = @Measure,
	OrderMinimum = @OrderMinimum, 
	OrderIncrement = @OrderIncrement,
	HandlingCharges = @HandlingCharges,
	IsActive=@IsActive,
	IsOnline=@IsOnline,
	--Length=@Length,
	--Height=@Height,
	--Width= @width,
	--Weight=@Weight,
	SelfShippable=@SelfShippable,
	AvailableForBackOrder=@AvailableForBackOrder,
	MaximumDiscountPercentage = @MaximumDiscountPercentage,
	BackOrderLimit=case when @AvailableForBackOrder=0 then 0 else @BackOrderLimit end,
	IsUnlimitedQuantity=@IsUnlimitedQuantity
	From PRProductSKUVariant S
	Inner Join @UpdateSkus U on S.Id = U.SkuId 
	Where S.SiteId=@ApplicationId
	
	-- make product active/inactive if the sku is that of bundle i.e. parent is a bundle
	Update PRProductVariant 
	set IsActive=@IsActive
	where --IsBundle =1 and 
	Id=@ProductId
	AND SiteId=@ApplicationId

end


GO


PRINT 'Add column to CLObjectImage'
GO
IF(COL_LENGTH('CLObjectImage', 'SiteId') IS NULL)
BEGIN
	ALTER TABLE [dbo].[CLObjectImage] 
	ADD SiteId UNIQUEIDENTIFIER NULL
	
	Declare @sql varchar(max)
	Set @sql = 'UPDATE CLObjectImage
	SET SiteId = S.Id
	FROM SISite S
	WHERE S.id = S.MasterSiteId AND S.status  = 1'
	exec (@sql)

    ALTER TABLE CLObjectImage 
	ALTER COLUMN SiteId UNIQUEIDENTIFIER NOT NULL	

END
GO


IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_CLObjectImage_ObjectId_ImageId_SiteId' AND object_id = OBJECT_ID('CLObjectImage'))
BEGIN
Print 'Creating index IX_CLObjectImage_ObjectId_ImageId_SiteId'
CREATE NONCLUSTERED INDEX [IX_CLObjectImage_ObjectId_ImageId_SiteId] ON [dbo].[CLObjectImage]
(
	[ObjectId] ASC,
	[ImageId] ASC,
	[SiteId] ASC
)
INCLUDE ([Id], [ObjectType], [IsPrimary], [Sequence]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
END
GO

PRINT 'Add column to CLImage'
GO
IF(COL_LENGTH('CLImage', 'IsShared') IS NULL)
BEGIN
	ALTER TABLE CLImage ADD IsShared BIT NOT NULL DEFAULT 1
END
GO


PRINT 'Creating View vwProductImage'
GO
IF (OBJECT_ID('vwProductImage') IS NOT NULL)
	DROP View vwProductImage
GO
CREATE VIEW [dbo].[vwProductImage]
AS
WITH CTE AS
(
	SELECT Id, SiteId, Title, AltText, Description, Size, Status, ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy  
	FROM CLImage

	UNION ALL

	SELECT Id, SiteId, Title, AltText, Description, NULL, Status, ModifiedDate, ModifiedBy
	FROM CLImageVariantCache
)

SELECT C.Id,
	C.Title,
	I2.FileName,
	C.AltText,
	C.Description,
	C.Size,
	OI.Sequence,
	P.Id AS ProductId,
	P.Title AS ProductName,
	I.CreatedBy,
	I.CreatedDate,
	C.ModifiedBy,
	C.ModifiedDate,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName,
	P.ProductTypeId,
	'/productimages/' + CAST(P.Id AS varchar(36)) + '/images' AS RelativePath,
	C.SiteId,
	I.SiteId AS SourceSiteId,
	IT.TypeNumber,
	C.Status,
	I2.ParentImage,
	I.IsShared
FROM PRProduct P
	JOIN CLObjectImage OI ON OI.ObjectId = P.Id
	JOIN CTE C ON OI.ImageId = C.Id --AND OI.SiteId = C.SiteId
	JOIN CLImage I ON I.Id = C.Id
	JOIN CLImage I2 ON I.Id = I2.ParentImage OR I.Id = I2.Id
	JOIN CLImageType IT ON IT.Id = I2.ImageType
	LEFT JOIN VW_UserFullName FN on FN.UserId = I.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
WHERE C.SiteId = I.SiteId or (C.SiteId != I.SiteId and I.IsShared = 1)
GO


PRINT 'Creating View vwSkuImage'
GO
IF (OBJECT_ID('vwSkuImage') IS NOT NULL)
	DROP View vwSkuImage
GO
CREATE VIEW [dbo].[vwSkuImage]      
AS      
WITH CTE AS      
(      
 SELECT Id, SiteId, Title, AltText, Description, Size, Status, ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy        
 FROM CLImage      
      
 UNION ALL      
      
 SELECT Id, SiteId, Title, AltText, Description, NULL, Status, ModifiedDate, ModifiedBy      
 FROM CLImageVariantCache      
) 
      
SELECT C.Id,     
 C.Title,      
 I2.FileName,      
 C.AltText,      
 C.Description,      
 C.Size,      
 OI.Sequence,      
 P.Id AS ProductId,      
 P.Title AS ProductName,      
 I.CreatedBy,      
 I.CreatedDate,      
 C.ModifiedBy,      
 C.ModifiedDate,      
 FN.UserFullName CreatedByFullName,      
 MN.UserFullName ModifiedByFullName,      
 P.ProductTypeId,      
 '/productimages/' + CAST(P.Id AS varchar(36)) + '/images' AS RelativePath,      
 C.SiteId,      
 I.SiteId AS SourceSiteId,      
 IT.TypeNumber,      
 C.Status,      
 I.ParentImage,      
 I.IsShared,      
 PS.Id AS SkuId,
 PS.SKU AS Sku  
FROM PRProduct P  
 JOIN CLObjectImage OI ON (OI.ObjectId = P.Id  AND OI.ObjectType = 205)
 LEFT JOIN CLObjectImage OI2 ON (OI2.ImageId =OI.ImageId  AND OI2.ObjectType = 206) 
 JOIN PRProductSKU PS ON PS.ProductId = P.Id 
 JOIN CTE C ON OI.ImageId = C.Id 
 JOIN CLImage I ON I.Id = C.Id      
 JOIN CLImage I2 ON I.Id = I2.ParentImage OR I.Id = I2.Id      
 JOIN CLImageType IT ON IT.Id = I2.ImageType      
 LEFT JOIN VW_UserFullName FN on FN.UserId = I.CreatedBy      
 LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy 
 WHERE OI2.ObjectId = PS.Id OR OI2.ObjectId IS NULL
 GO



IF(OBJECT_ID('Sku_GetSkuVariant') IS NOT NULL)
	DROP PROCEDURE Sku_GetSkuVariant
GO

CREATE PROCEDURE [dbo].[Sku_GetSkuVariant]
( @SkuIds		xml,@ApplicationId uniqueidentifier=null)
AS

Declare @TabSkuIds AS Table (Id Uniqueidentifier)

insert into @TabSkuIds
select tab.col.value('text()[1]','uniqueidentifier') 
from @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	


SELECT S.[Id]
      ,PS.[ProductId]
      ,S.[SiteId]
      ,PS.[SKU]
	  ,S.[Title]
      ,S.[Description]
      ,S.[Sequence]
      ,S.[IsActive]
      ,S.[IsOnline]
      ,S.[ListPrice]
      ,S.[UnitCost]
      ,S.[PreviousSoldCount]
	  ,S.[Measure]
	  ,S.[OrderMinimum]
	  ,S.[OrderIncrement]
	  ,PS.[CreatedBy]
	  ,PS.CreatedDate AS CreatedDate
	  ,S.[ModifiedBy]
	  ,S.ModifiedDate AS  ModifiedDate
	  ,PS.[Status]
	  ,S.[HandlingCharges]
	  ,PS.[Length]
	  ,PS.[Height]
	  ,PS.[Width]
	  ,PS.[Weight]
	  ,S.[SelfShippable]
	  ,S.[AvailableForBackOrder]
	  ,S.MaximumDiscountPercentage
	  ,S.[BackOrderLimit]
	  ,S.[IsUnlimitedQuantity]
   FROM @TabSkuIds TS	
	Inner Join PRProductSKUVariant S on S.Id =TS.Id
	INNER JOIN PRProductSKU PS ON S.Id =PS.Id
	WHERE S.SiteId = @ApplicationId

	GO

PRINT 'Creating View vwWishListItem'
GO
IF (OBJECT_ID('vwWishListItem') IS NOT NULL)
	DROP View vwWishListItem
GO
CREATE VIEW [dbo].[vwWishListItem]
AS 
SELECT  wli.Id,
		ParentWishListItemId,
		WishListId,
		wl.CustomerId,
		SKUId,
		Quantity,
		wli.Sequence,
		Comment,
		wli.CreatedDate,
	    wli.CreatedBy,
		wli.ModifiedDate,
		wli.ModifiedBy,
		BundleItemId
FROM CSWishListItem wli JOIN CSWishList wl ON wli.WishListId = wl.Id
GO

PRINT 'Add SiteId column to GLCountry'
GO
IF(COL_LENGTH('GLCountry', 'IsShared') IS NULL)
BEGIN
	ALTER TABLE GLCountry ADD IsShared bit null 
END
GO
UPDATE GLCountry SET IsShared = 1 WHERE IsShared IS NULL
GO
PRINT 'Add SiteId column to GLCountry'
GO
IF(COL_LENGTH('GLCountry', 'SiteId') IS NULL)
BEGIN
	ALTER TABLE GLCountry ADD SiteId uniqueidentifier null 
END 
GO

Declare @SiteIdAdded bit
SELECT @SiteIdAdded = COLUMNPROPERTY(OBJECT_ID('dbo.GLCountry', 'U'), 'SiteId', 'AllowsNull') 
IF(@SiteIdAdded = 1)
BEGIN
	UPDATE GLCountry SET SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'

	INSERT INTO GLCountry
	(
		Id,
		CountryName,
		CountryCode,
		CreatedDate,
		CreatedBy,
		ModifiedDate,
		ModifiedBy,
		Status, 
		RegionId,
		IsTerritory,
		SiteId,
		IsShared
	)
	SELECT NEWID(),
		C.CountryName,
		C.CountryCode,
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedDate,
		C.ModifiedBy,
		C.Status, 
		C.RegionId,
		C.IsTerritory,
		S.Id,
		C.IsShared
	FROM GLCountry C 
		CROSS APPLY SISite S
	WHERE S.Id != '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
		AND S.Id = S.MasterSiteId AND S.Status = 1
	
	ALTER TABLE GLCountry ALTER COLUMN SiteId uniqueidentifier NOT NULL
END
GO

PRINT 'Add SiteId column to GLState'
GO
IF(COL_LENGTH('GLState', 'IsShared') IS NULL)
BEGIN
	ALTER TABLE GLState ADD IsShared bit null 
END
GO
UPDATE GLState SET IsShared = 1 WHERE IsShared IS NULL
GO
PRINT 'Add column to GLState'
GO
IF(COL_LENGTH('GLState', 'SiteId') IS NULL)
BEGIN
	ALTER TABLE GLState ADD SiteId uniqueidentifier null 
END 
GO 
Declare @SiteIdAdded bit
SELECT @SiteIdAdded = COLUMNPROPERTY(OBJECT_ID('dbo.GLState', 'U'), 'SiteId', 'AllowsNull') 
IF(@SiteIdAdded = 1)
BEGIN

	UPDATE GLState SET SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
	
	INSERT INTO GLState
	(
		Id,
		State,
		StateCode,
		CountryId,
		CreatedDate,
		CreatedBy,
		ModifiedDate,
		ModifiedBy,
		Status, 
		PrimaryLevelName,
		SiteId,
		IsShared
	)
	SELECT NEWID(),
		C.State,
		C.StateCode,
		C.CountryId,
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedDate,
		C.ModifiedBy,
		C.Status, 
		C.PrimaryLevelName,
		S.Id,
		C.IsShared
	FROM GLState C 
		CROSS APPLY SISite S
	WHERE S.Id != '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' 
		AND S.Id = S.MasterSiteId AND S.Status = 1 

	ALTER TABLE GLState ALTER COLUMN SiteId uniqueidentifier NOT NULL
END
GO

PRINT 'Creating table GLCountryVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'GLCountryVariant'))
BEGIN
	CREATE TABLE [dbo].[GLCountryVariant]
	(
		[Id] [uniqueidentifier] NOT NULL,
		[SiteId] [uniqueidentifier] NOT NULL,
		[CountryName] [nvarchar](255) NOT NULL,
		[Status] [int] NOT NULL,
		[ModifiedDate] [datetime] NOT NULL,
		[ModifiedBy] [uniqueidentifier] NOT NULL, 
		CONSTRAINT [PK_GLCountryVariant] PRIMARY KEY ([Id], [SiteId])
	)
END
ELSE IF (COL_LENGTH('GLCountryVariant', 'CountryCode') IS NOT NULL)
BEGIN
	ALTER TABLE GLCountryVariant DROP COLUMN CountryCode
END
GO

PRINT 'Creating table GLCountryVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'GLCountryVariantCache'))
BEGIN
	CREATE TABLE [dbo].[GLCountryVariantCache]
	(
		[Id] [uniqueidentifier] NOT NULL,
		[SiteId] [uniqueidentifier] NOT NULL,
		[CountryName] [nvarchar](255) NULL,
		[Status] [int] NULL,
		[ModifiedDate] [datetime] NULL,
		[ModifiedBy] [uniqueidentifier] NULL, 
		CONSTRAINT [PK_GLCountryVariantCache] PRIMARY KEY ([Id], [SiteId])
	);

	INSERT INTO GLCountryVariantCache
	(
		Id,
		SiteId,
		CountryName,
		Status,
		ModifiedDate,
		ModifiedBy
	)
	SELECT C.Id,
		S.Id,
		C.CountryName,
		C.Status,
		ISNULL(C.ModifiedDate, C.CreatedDate),
		ISNULL(C.ModifiedBy, C.CreatedBy)
	FROM GLCountry C
		CROSS APPLY SISite S
	WHERE S.MasterSiteId = C.SiteId AND S.Status = 1
		AND S.MasterSiteId != S.Id
END
ELSE IF (COL_LENGTH('GLCountryVariantCache', 'CountryCode') IS NOT NULL)
BEGIN
	ALTER TABLE GLCountryVariantCache DROP COLUMN CountryCode
END
GO

PRINT 'Creating table GLStateVariant'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'GLStateVariant'))
BEGIN
	CREATE TABLE [dbo].[GLStateVariant]
	(
		[Id] [uniqueidentifier] NOT NULL,
		[SiteId] [uniqueidentifier] NOT NULL,
		[State] [nvarchar](255) NOT NULL,
		[Status] [int] NOT NULL,
		[ModifiedDate] [datetime] NOT NULL,
		[ModifiedBy] [uniqueidentifier] NOT NULL, 
		CONSTRAINT [PK_GLStateVariant] PRIMARY KEY ([Id], [SiteId])
	)
END
ELSE 
BEGIN
	IF (COL_LENGTH('GLStateVariant', 'StateCode') IS NOT NULL)
		ALTER TABLE GLStateVariant DROP COLUMN StateCode

	IF (COL_LENGTH('GLStateVariant', 'CountryId') IS NOT NULL)
		ALTER TABLE GLStateVariant DROP COLUMN CountryId
END
GO

PRINT 'Creating table GLStateVariantCache'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'GLStateVariantCache'))
BEGIN
	CREATE TABLE [dbo].[GLStateVariantCache]
	(
		[Id] [uniqueidentifier] NOT NULL,
		[SiteId] [uniqueidentifier] NOT NULL,
		[State] [nvarchar](255) NOT NULL,
		[Status] [int] NOT NULL,
		[ModifiedDate] [datetime] NOT NULL,
		[ModifiedBy] [uniqueidentifier] NOT NULL, 
		CONSTRAINT [PK_GLStateVariantCache] PRIMARY KEY ([Id], [SiteId])
	)

	INSERT INTO GLStateVariantCache
	(
		Id,
		SiteId,
		State,
		Status,
		ModifiedDate,
		ModifiedBy
	)
	SELECT C.Id,
		S.Id,
		C.State,
		C.Status,
		ISNULL(C.ModifiedDate, C.CreatedDate),
		ISNULL(C.ModifiedBy, C.CreatedBy)
	FROM GLState C
		CROSS APPLY SISite S
	WHERE S.MasterSiteId = C.SiteId AND S.Status = 1
		AND S.MasterSiteId != S.Id
END
ELSE 
BEGIN
	IF (COL_LENGTH('GLStateVariantCache', 'StateCode') IS NOT NULL)
		ALTER TABLE GLStateVariantCache DROP COLUMN StateCode

	IF (COL_LENGTH('GLStateVariantCache', 'CountryId') IS NOT NULL)
		ALTER TABLE GLStateVariantCache DROP COLUMN CountryId
END
GO
PRINT 'Creating View vwCountry'
GO
IF (OBJECT_ID('vwCountry') IS NOT NULL)
	DROP View vwCountry
GO
CREATE VIEW [dbo].[vwCountry]  
AS  
WITH CTE AS  
(  
	SELECT Id, 
		SiteId, 
		CountryName, 
		Status, 
		ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, 
		ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy
	FROM GLCountry 
  
	UNION ALL  
  
	SELECT Id, 
		SiteId, 
		CountryName, 
		Status, 
		ModifiedDate, 
		ModifiedBy
	FROM GLCountryVariantCache 
)  
  
SELECT C.[Id],   
	T.SiteId,  
	C.SiteId AS SourceSiteId,  
	T.CountryName,  
	C.CountryCode,  
	T.[Status],  
	C.RegionId,
	C.CreatedDate,   
	C.CreatedBy,   
	CFN.UserFullName AS CreatedByFullName,  
	T.ModifiedDate,   
	T.ModifiedBy,   
	MFN.UserFullName AS ModifiedByFullName,
	C.IsShared,
	R.Name AS RegionName
FROM CTE T
	INNER JOIN GLCountry AS C ON C.Id = T.Id  
	LEFT JOIN GLCountryRegion R ON R.Id = C.RegionId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = C.CreatedBy  
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = T.ModifiedBy  
GO
PRINT 'Creating View vwState'
GO
IF (OBJECT_ID('vwState') IS NOT NULL)
	DROP View vwState
GO
CREATE VIEW [dbo].[vwState]  
AS  
WITH CTE AS  
(  
	SELECT Id, 
		SiteId, 
		State, 
		Status, 
		ISNULL(ModifiedDate, CreatedDate) AS ModifiedDate, 
		ISNULL(ModifiedBy, CreatedBy) AS  ModifiedBy
	FROM GLState 
  
	UNION ALL  
  
	SELECT Id, 
		SiteId, 
		State, 
		Status, 
		ModifiedDate, 
		ModifiedBy
	FROM GLStateVariantCache 
)  
  
SELECT S.[Id],   
	T.SiteId,  
	S.SiteId AS SourceSiteId, 
	S.CountryId, 
	T.State,  
	S.StateCode,  
	T.[Status],  
	S.CreatedDate,   
	S.CreatedBy,   
	CFN.UserFullName AS CreatedByFullName,  
	T.ModifiedDate,   
	T.ModifiedBy,   
	MFN.UserFullName AS ModifiedByFullName,
	S.IsShared,
	C.CountryName,
	C.CountryCode
FROM CTE T
	INNER JOIN GLState AS S ON S.Id = T.Id  
	LEFT JOIN GLCountry AS C ON C.Id = S.CountryId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId =S.CreatedBy  
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = T.ModifiedBy  
GO

PRINT 'Create view vwProductReviews'
GO

IF(OBJECT_ID('vwProductReviews') IS NOT NULL)
	DROP VIEW vwProductReviews
GO

CREATE VIEW [dbo].[vwProductReviews]
AS 

SELECT  Com.Id,
		Com.ObjectId,
		Com.PostedBy,
		Com.Email,
		Com.Title,
		Com.Comments,
		Com.ObjectTypeId,
		Com.UserId,
		Com.ParentId,		
		Com.Status,
		Com.CreatedDate,
		Pr.Title as ObjectTitle,
		Pr.ProductTypeId,
		PrType.Title as ProductTypeName,
		R.RatingValue,
		R.Id as RatingId
FROM BLComments Com inner join PRProduct Pr on Com.ObjectId = Pr.Id 
					inner join PRProductType PrType on Pr.ProductTypeId = PrType.Id
					left join BLCommentRating CR on Com.Id= CR.CommentId
					left join BLRatings R on R.Id = CR.RatingId
WHERE Com.Status  <> 3
GO

PRINT 'Modify stored procedure Comments_UpdateHierarchyStatus'
GO

IF(OBJECT_ID('Comments_UpdateHierarchyStatus') IS NOT NULL)
	DROP PROCEDURE Comments_UpdateHierarchyStatus
GO

CREATE PROCEDURE [dbo].[Comments_UpdateHierarchyStatus]
(
	@IdsAsXml	xml,	
	@Status		int = null					
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Now		datetime,
		@stmt		varchar(256),
		@rowcount	int,
		@error		int,
		@TotalIds	int
		
BEGIN
--********************************************************************************
-- code
--********************************************************************************
	
	Declare @tbIds table (Id uniqueidentifier)
	Insert into @tbIds
	Select PC.Id from BLComments P
	Inner Join BLComments PC on PC.LftValue Between P.LftValue and P.RgtValue
	Where P.Id in (Select tab.col.value('text()[1]','uniqueidentifier') 
			       from @IdsAsXml.nodes('//CommentCollection/Comment') tab(col))
		  AND P.Status <> dbo.GetDeleteStatus()
		
	UPDATE BLComments
		SET Status = @Status
	WHERE Id in (Select Id from @tbIds)
		
	Update R Set R.Status = @Status
		From BLComments B 
			JOIN BLCommentRating CR ON B.Id = CR.CommentId
			Join BLRatings R ON R.Id = CR.RatingId
		Where B.Id in (Select Id from @tbIds)
	
END
GO

PRINT 'Creating table ORTaxAPI'
GO
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ORTaxAPI'))
BEGIN
CREATE TABLE [dbo].[ORTaxAPI](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](1024) NULL,
	[DotNetProviderName] [varchar](255) NOT NULL,
	[Sequence] [int] NOT NULL
 CONSTRAINT [PK_ORTaxAPI] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END

GO

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ORTaxAPISite'))
BEGIN
CREATE TABLE [dbo].[ORTaxAPISite](
	[Id] uniqueidentifier NOT NULL Primary Key,
	[TaxAPIId] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[ModifiedDate] [datetime] NULL,
) ON [PRIMARY]
END

GO

IF(OBJECT_ID('FK_ORTaxAPISite_ORTaxAPI') IS NULL)
	ALTER TABLE [dbo].[ORTaxAPISite]  WITH CHECK ADD  CONSTRAINT [FK_ORTaxAPISite_ORTaxAPI] FOREIGN KEY([TaxAPIId])
	REFERENCES [dbo].[ORTaxAPI] ([Id])
GO
ALTER TABLE [dbo].[ORTaxAPISite] CHECK CONSTRAINT [FK_ORTaxAPISite_ORTaxAPI]

GO
IF(OBJECT_ID('vwTaxAPI') IS NOT NULL)
	DROP VIEW vwTaxAPI
GO
CREATE VIEW [dbo].[vwTaxAPI]
AS
SELECT        T.Id, T.Name, T.Description, T.DotNetProviderName,  T.Sequence,TS.ModifiedBy, TS.ModifiedDate, TS.SiteId, TS.IsActive
FROM            dbo.ORTaxAPI AS T INNER JOIN
                         dbo.ORTaxAPISite AS TS ON T.Id = TS.TaxAPIId
GO


IF NOT EXISTS (Select 1 FROM ORTaxAPI Where Name='iAPPS')
BEGIN
	Declare @Id uniqueidentifier
	SET @Id ='9566DDB5-216A-4ED5-A0B3-ACCB1506630A'
	INSERT INTO ORTaxAPI(Id,Name,Description,DotNetProviderName,Sequence)
	Values(@Id,'iAPPS', 'iAPPS Tax Service','iAppsTaxProvider',1)
	INSERT INTO ORTaxAPISite(Id,SiteId,TaxAPIId,IsActive)
	SELECT NewId(),Id,@Id,1
	FROM SISite
END
GO
IF NOT EXISTS (Select 1 FROM ORTaxAPI Where Name='Cybersource')
BEGIN
	Declare @Id uniqueidentifier
	SET @Id ='FAB0E3ED-A28B-4FA1-AB42-6D8E4FFE9A28'
	INSERT INTO ORTaxAPI(Id,Name,Description,DotNetProviderName,Sequence)
		Values(@Id,'Cybersource', 'Cybersource Tax Service','CybersourceTaxProvider',1)
	INSERT INTO ORTaxAPISite(Id,SiteId,TaxAPIId,IsActive)
	SELECT NewId(),Id,@Id,0
	FROM SISite
END
GO


PRINT 'Creating view vwAttributeValidation'
GO

IF (OBJECT_ID('vwAttributeValidation') IS NOT NULL)
	DROP VIEW vwAttributeValidation
GO

CREATE VIEW [dbo].[vwAttributeValidation]
AS
SELECT	AV.Id, AV.AttributeId, AV.ExpressionId, AV.MinValue, AV.MaxValue, AE.Title AS ExpressionName, AE.RegularExpression
FROM	ATAttributeValidation AS AV
		LEFT JOIN ATAttributeExpression AS AE ON AE.Id = AV.ExpressionId
GO

PRINT 'Creating Proc ProductImage_GetImageForProduct'
GO

IF(OBJECT_ID('ProductImage_GetImageForProduct') IS NOT NULL)
	DROP PROCEDURE ProductImage_GetImageForProduct
GO

CREATE PROCEDURE [dbo].[ProductImage_GetImageForProduct]
    (
      @ProductId UNIQUEIDENTIFIER = NULL ,
      @ProductIds XML = NULL ,
      @IncludeSKUs BIT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    ) WITH RECOMPILE
AS --********************************************************************************
-- Variable declaration
--********************************************************************************
    BEGIN
--********************************************************************************
-- code
--********************************************************************************

        IF @ProductIds IS NULL 
            BEGIN
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        @ProductId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(@ProductId,
                                                              C.Id)
                FROM    CLObjectImage OI --INNER JOIN CLImage I on I.Id=OI.ImageId
                        INNER JOIN CLImage C ON OI.ImageId = ISNULL(C.ParentImage,
                                                              C.Id)
                WHERE   --I.SiteId=Isnull(@ApplicationId,I.SiteId) 
	--(@ApplicationId is null or I.SiteId = @ApplicationId)AND
                        C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                        AND OI.ObjectId = @ProductId
                ORDER BY OI.Sequence ,
                        C.Title
            END
        ELSE 
            BEGIN
                DECLARE @ProductIdTable TABLE ( Id UNIQUEIDENTIFIER )
                INSERT  INTO @ProductIdTable
                        ( Id
                        )
                        SELECT  tab.col.value('text()[1]', 'uniqueidentifier')
                        FROM    @ProductIds.nodes('/ObjectIdCollection/ObjectId') tab ( col )
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
	--INNER JOIN CLImage I on I.Id=OI.ImageId
                        INNER JOIN CLImage C ON OI.ImageId = ISNULL(C.ParentImage,
                                                              C.Id)
	--Inner join @ProductIds.nodes('/ObjectIdCollection/ObjectId')tab(col) ON  OI.ObjectId =tab.col.value('text()[1]','uniqueidentifier')
                WHERE   --I.SiteId=Isnull(@ApplicationId,I.SiteId) 
	--(@ApplicationId is null or C.SiteId = @ApplicationId)
	--AND 
                        C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                ORDER BY OI.Sequence ,
                        C.Title
            END
    END
GO

PRINT 'Adding sitesetting for keys for front-end tokenization'
GO


Declare @sequence int
SELECT @sequence = MAX(sequence)+1 FROM dbo.STSettingType
Declare @settingTypeId int

IF NOT EXISTS(Select 1 from STSettingType Where Name ='DateFormat.UseSiteTimeZone')
BEGIN
	INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, Sequence, IsEnabled, ControlType, IsVisible)
	VALUES('DateFormat.UseSiteTimeZone', 'Display all dates in site timezone', 1, @sequence, 1, 'Checkbox', 1)

	Update STSettingType SET SettingGroupId = (SELECT TOP 1 Id FROM STSettingGroup WHERE Name='General')
	WHERE Name='DateFormat.UseSiteTimeZone'
END

IF NOT EXISTS(Select 1 from STSettingType Where Name ='TaxProvider')
BEGIN
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,Sequence,IsEnabled,ControlType)
	VALUES('TaxProvider','Tax provider to use',1,@sequence,0,'Dropdownlist')
END

IF NOT EXISTS(Select 1 from STSettingType Where Name ='cybs.keyId')
BEGIN
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,Sequence,IsEnabled,ControlType)
		Values('cybs.keyId','public keyId for frontend token creation',1,@sequence,1,'Textbox')
		SET @settingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	SELECT Id,@settingTypeId,'ffe6a8ff-980c-4f4d-ad6f-25f0c371d193' 
	FROM SISite
	Where ParentSiteId='00000000-0000-0000-0000-000000000000' AND Status = 1
	SET @sequence =@sequence +1	
END

IF NOT EXISTS(Select 1 from STSettingType Where Name ='cybs.sharedKey')
BEGIN
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,Sequence,IsEnabled,ControlType)
		Values('cybs.sharedKey','public key for frontend token creation',1,@sequence,1,'Textbox')
		SET @settingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	SELECT Id,@settingTypeId,'GRNC7gnGd8ZHNM6xVLoIXMchSl//hiKaR2x9j2cFu7k=' 
	FROM SISite
	Where ParentSiteId='00000000-0000-0000-0000-000000000000' AND Status = 1
	SET @sequence =@sequence +1	
END


IF NOT EXISTS(Select 1 from STSettingType Where Name ='AuthorizeNetClientKey')
BEGIN
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,Sequence,IsEnabled,ControlType)
		Values('AuthorizeNetClientKey','public key for frontend token creation',1,@sequence,1,'Textbox')
		SET @settingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	SELECT Id,@settingTypeId,'6E5HWV8AxtW7u8k9T9RYz5ar5psrPzX7PgKSMp5zZF7Xq2VFa3bMH9RrXzJmp37n' 
	FROM SISite
	Where ParentSiteId='00000000-0000-0000-0000-000000000000'
	SET @sequence =@sequence +1	
END


IF NOT EXISTS(Select 1 from STSettingType Where Name ='AuthorizeNetAPILogin')
BEGIN
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,Sequence,IsEnabled,ControlType)
		Values('AuthorizeNetAPILogin','API Login Id',1,@sequence,1,'Textbox')
		SET @settingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	SELECT Id,@settingTypeId,'9Zt7x2Ah8M3r' 
	FROM SISite
	Where ParentSiteId='00000000-0000-0000-0000-000000000000'
	SET @sequence =@sequence +1	
END


IF NOT EXISTS(Select 1 from STSettingType Where Name ='StripePublicKey')
BEGIN
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,Sequence,IsEnabled,ControlType)
		Values('StripePublicKey','Public key to create token in frontend',1,@sequence,1,'Textbox')
		SET @settingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	SELECT Id,@settingTypeId,'pk_test_kxP0ZXLZbHF8x2PyjpKJniVn' 
	FROM SISite
	Where ParentSiteId='00000000-0000-0000-0000-000000000000' AND Status = 1
	SET @sequence =@sequence +1	
END


IF NOT EXISTS(Select 1 from STSettingType Where Name ='StripePublicKey')
BEGIN
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,Sequence,IsEnabled,ControlType)
		Values('StripePublicKey','Public key to create token in frontend',1,@sequence,1,'Textbox')
		SET @settingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	SELECT Id,@settingTypeId,'pk_test_kxP0ZXLZbHF8x2PyjpKJniVn' 
	FROM SISite
	Where ParentSiteId='00000000-0000-0000-0000-000000000000' AND Status = 1
	SET @sequence =@sequence +1	
END

IF NOT EXISTS(Select 1 from STSettingType Where Name ='StripeSecretKey')
BEGIN
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,Sequence,IsEnabled,ControlType)
		Values('StripeSecretKey','Stripe secret key',1,@sequence,1,'Textbox')
		SET @settingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	SELECT Id,@settingTypeId,'sk_test_1aNa5bUgeUeGXvxGr2ULvGce' 
	FROM SISite
	Where ParentSiteId='00000000-0000-0000-0000-000000000000' AND Status = 1
	SET @sequence =@sequence +1	
END

IF NOT EXISTS(Select 1 from STSettingType Where Name ='AdvancedCouponTypeId')
BEGIN
	INSERT INTO STSettingType(Name,SettingGroupId,Sequence,IsEnabled,ControlType)
		Values('AdvancedCouponTypeId',1,@sequence,0,'Textbox')
		SET @settingTypeId = @@Identity

	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	SELECT Id,@settingTypeId,'54f21a2e-cf6e-4980-89e9-763cebef7b6b' 
	FROM SISite
	Where Id = MasterSiteId AND Status = 1
	SET @sequence =@sequence +1	
END
GO



IF(OBJECT_ID('ShippingOption_GetActiveShippingOptions') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetActiveShippingOptions
GO

CREATE PROCEDURE [dbo].[ShippingOption_GetActiveShippingOptions]
(@Id uniqueidentifier=null,@ApplicationId uniqueidentifier=null)
AS
SELECT 
	OSO.[Id],
	OSO.[ShipperId],
	OSO.[Name],
	OSO.[Description],
	OSO.[CustomId],
	OSO.[NumberOfDaysToDeliver],
	OSO.[CutOffHour],
	OSO.[SaturdayDelivery],
	OSO.[AllowShippingCoupon],
	OSO.[IsPublic],
	OSO.[IsInternational],
	OSO.[ServiceCode],
	OSO.[ExternalShipCode],
	OSO.[BaseRate],
	OSO.[SiteId],
	OSO.[Status],
	OSO.[Sequence],
	OSO.[CreatedBy],
	OSO.[CreatedDate] AS CreatedDate,
	OSO.[ModifiedBy],
	OSO.ModifiedDate AS ModifiedDate,
	1  IsActive
FROM ORShippingOption OSO
	INNER JOIN ORShipper OS ON OS.Id = OSO.ShipperId
Where --OSO.Id =isnull(@Id,OSO.Id) 
	(@Id is null or OSO.Id = @Id)
	And OS.Status= 1 AND OSO.[Status] = 1
	--And OS.SiteId =@ApplicationId
	and (@ApplicationId is null or (OSO.SiteId = @ApplicationId) OR (OS.IsShared = 1 AND OSO.SiteId in (select SiteId from dbo.GetParentSites(@ApplicationId, 1, 1)))) 
GO

IF(OBJECT_ID('Shipper_GetShippers') IS NOT NULL)
	DROP PROCEDURE Shipper_GetShippers
GO

CREATE PROCEDURE [dbo].[Shipper_GetShippers](
	@Id uniqueidentifier = null
	,@ApplicationId uniqueidentifier=null
)
AS
BEGIN

	DECLARE @activeStatus INT 
	SELECT @activeStatus = dbo.GetActiveStatus()

	DECLARE @shippers TABLE(
		Id uniqueidentifier,
		Name nvarchar(255),
		Description nvarchar(1024),
		TrackingURL nvarchar(500),
		DotNetProviderName nvarchar(255),
		Status int,
		Sequence int, 
		CreatedBy uniqueidentifier,
		CreatedDate datetime,
		ModifiedBy uniqueidentifier,
		ModifiedDate datetime,
		SiteId uniqueidentifier,
		IsShared bit
	)

	INSERT INTO @shippers
	SELECT [Id]
      ,[Name] 
	  ,[Description]    
      ,[TrackingURL]      
      ,[DotNetProviderName]
      ,[Status]
	  ,[Sequence]
      ,[CreatedBy]
      , CreatedDate AS [CreatedDate]
      ,[ModifiedBy]
      , ModifiedDate AS [ModifiedDate]
	  ,SiteId
	  ,IsShared
   FROM ORShipper
   where --[Id]=	Isnull(@Id,[Id])		
		(@Id is null or [Id] = @Id)
        AND
        [Status] = @activeStatus 
        AND
        --SiteId = @ApplicationId
        (@ApplicationId is null or (SiteId = @ApplicationId) OR (IsShared = 1 AND SiteId in (select SiteId from dbo.GetParentSites(@ApplicationId, 1, 1)))) 
        order by Sequence, Name

	SELECT * FROM @shippers

	SELECT so.[Id]
      ,so.[ShipperId]
      ,so.[Name]
      ,so.[Description]
      ,so.[CustomId]
      ,so.[NumberOfDaysToDeliver]
      ,so.[CutOffHour]
      ,so.[SaturdayDelivery]
      ,so.[AllowShippingCoupon]
      ,so.[IsPublic]
      ,so.[IsInternational]
      ,so.[ServiceCode]
	  ,so.[ExternalShipCode]
      ,so.[BaseRate]      
      ,so.[Status]
	  ,so.[Sequence]
      ,so.[CreatedBy]
      ,so.[CreatedDate]
      ,so.[ModifiedBy]
      ,so.[ModifiedDate]
	  ,so.SiteId
  FROM ORShippingOption so join @shippers s on so.ShipperId = s.Id
  Where so.SiteId=@ApplicationId
  order by so.ShipperId, so.Sequence	
END

GO

IF(OBJECT_ID('ShippingOption_GetOptionsWithPrice') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetOptionsWithPrice
GO

CREATE PROCEDURE [dbo].[ShippingOption_GetOptionsWithPrice]
(
	@ShipperId uniqueidentifier,
	@OrderTotal money,
	@ApplicationId uniqueidentifier=null
)
AS
BEGIN

	Select OSO.Id
		,OSO.ShipperId
		,OSO.Name
		,OSO.Description
		,OSO.CustomId
		,OSO.NumberOfDaysToDeliver
		,OSO.CutOffHour
		,OSO.SaturdayDelivery
		,OSO.AllowShippingCoupon
		,OSO.IsPublic
		,OSO.IsInternational
		,OSO.ServiceCode
		,OSO.[ExternalShipCode]
		,OSO.BaseRate
		,OSO.SiteId	
		,OSO.Sequence
		,OSO.Status
		,OSO.CreatedBy
		, OSO.CreatedDate AS CreatedDate
		,OSO.ModifiedBy
		, OSO.ModifiedDate AS ModifiedDate
		, OSR.ShippingPrice
	FROM ORShippingOption OSO
		INNER JOIN ORShipper OS ON OS.Id = OSO.ShipperId
		INNER JOIN ORShippingOptionRate OSR ON OSO.Id = OSR.ShippingOptionId
	Where OS.Status= 1 AND OSO.[Status] = 1 AND OS.Id=@ShipperId AND
			@OrderTotal between OSR.StartAmount and 
					CASE OSR.EndAmount when 0 then  922337203685477.5807 
										else OSR.EndAmount end
	AND (@ApplicationId is null or OS.SiteId =@ApplicationId)
END

GO

IF(OBJECT_ID('ShippingOption_GetShippingOptions') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetShippingOptions
GO

CREATE PROCEDURE [dbo].[ShippingOption_GetShippingOptions]
(@Id uniqueidentifier=null
,@ApplicationId uniqueidentifier=null)
AS
SELECT 
	OSO.[Id],
	OSO.[ShipperId],
	OSO.[Name],
	OSO.[Description],
	OSO.[CustomId],
	OSO.[NumberOfDaysToDeliver],
	OSO.[CutOffHour],
	OSO.[SaturdayDelivery],
	OSO.[AllowShippingCoupon],
	OSO.[IsPublic],
	OSO.[IsInternational],
	OSO.[ServiceCode],
	OSO.[ExternalShipCode],
	OSO.[BaseRate],
	OSO.[SiteId],
	OSO.[Status],
	OSO.[Sequence],
	OSO.[CreatedBy],
	OSO.CreatedDate AS CreatedDate,
	OSO.[ModifiedBy],
	 OSO.ModifiedDate AS ModifiedDate,
	1 IsActive
FROM ORShippingOption OSO
Where --Id =isnull(@Id,Id)
	(@Id is null or Id = @Id)
	and
	(@ApplicationId is null or OSO.SiteId=@ApplicationId) 

GO


IF(OBJECT_ID('ShippingOption_GetShippingOptionsByShipper') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetShippingOptionsByShipper
GO

CREATE PROCEDURE [dbo].[ShippingOption_GetShippingOptionsByShipper](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
SELECT 
	OSO.[Id],
	OSO.[ShipperId],
	OSO.[Name],
	OSO.[Description],
	OSO.[CustomId],
	OSO.[NumberOfDaysToDeliver],
	OSO.[CutOffHour],
	OSO.[SaturdayDelivery],
	OSO.[AllowShippingCoupon],
	OSO.[IsPublic],
	OSO.[IsInternational],
	OSO.[ServiceCode],
	OSO.[ExternalShipCode],
	OSO.[BaseRate],
	OSO.[SiteId],
	OSO.[Status],
	OSO.[Sequence],
	OSO.[CreatedBy],
	OSO.[CreatedDate] AS CreatedDate,
	OSO.[ModifiedBy],
	OSO.[ModifiedDate] AS ModifiedDate
FROM ORShippingOption OSO
WHERE OSO.ShipperId = @Id
AND-- OSO.SiteId = @ApplicationId
(@ApplicationId is null or oso.SiteId =@ApplicationId) 

GO

PRINT 'Modify stored procedure ShippingOption_GetShippingOptionsByShipper'
GO

IF(OBJECT_ID('ShippingOption_GetShippingOptionsByShipper') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetShippingOptionsByShipper
GO

CREATE PROCEDURE [dbo].[ShippingOption_GetShippingOptionsByShipper](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null,@IncludeVariantSites bit = 0)
AS
SELECT 
	OSO.[Id],
	OSO.[ShipperId],
	OSO.[Name],
	OSO.[Description],
	OSO.[CustomId],
	OSO.[NumberOfDaysToDeliver],
	OSO.[CutOffHour],
	OSO.[SaturdayDelivery],
	OSO.[AllowShippingCoupon],
	OSO.[IsPublic],
	OSO.[IsInternational],
	OSO.[ServiceCode],
	OSO.[ExternalShipCode],
	OSO.[BaseRate],
	OSO.[SiteId],
	OSO.[Status],
	OSO.[Sequence],
	OSO.[CreatedBy],
	OSO.[CreatedDate] AS CreatedDate,
	OSO.[ModifiedBy],
	OSO.[ModifiedDate] AS ModifiedDate
FROM ORShippingOption OSO
WHERE OSO.ShipperId = @Id
AND-- OSO.SiteId = @ApplicationId
(@ApplicationId is null OR (@IncludeVariantSites = 0 AND OSO.[SiteId] = @ApplicationId) OR (@IncludeVariantSites = 1 AND OSO.SiteId in (select SiteId from dbo.GetVariantSites(@ApplicationId))))
GO



PRINT 'Modify stored procedure ShippingOption_GetShippingOptions'
GO

IF(OBJECT_ID('ShippingOption_GetShippingOptions') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetShippingOptions
GO

CREATE PROCEDURE [dbo].[ShippingOption_GetShippingOptions]
(@Id uniqueidentifier=null
,@ApplicationId uniqueidentifier=null)
AS
SELECT 
	OSO.[Id],
	OSO.[ShipperId],
	OSO.[Name],
	OSO.[Description],
	OSO.[CustomId],
	OSO.[NumberOfDaysToDeliver],
	OSO.[CutOffHour],
	OSO.[SaturdayDelivery],
	OSO.[AllowShippingCoupon],
	OSO.[IsPublic],
	OSO.[IsInternational],
	OSO.[ServiceCode],
	OSO.[ExternalShipCode],
	OSO.[BaseRate],
	OSO.[SiteId],
	OSO.[Status],
	OSO.[Sequence],
	OSO.[CreatedBy],
	OSO.CreatedDate AS CreatedDate,
	OSO.[ModifiedBy],
	 OSO.ModifiedDate AS ModifiedDate,
	1 IsActive
FROM ORShippingOption OSO
Where --Id =isnull(@Id,Id)
	(@Id is null or Id = @Id)
	or
	(@ApplicationId is null or (@Id is null and OSO.SiteId=@ApplicationId))
	
GO

GO

IF(OBJECT_ID('ProductImage_GetImageForProducts') IS NOT NULL)
	DROP PROCEDURE ProductImage_GetImageForProducts
GO

CREATE  PROCEDURE [dbo].[ProductImage_GetImageForProducts]
    (
      @ProductIds XML = NULL ,
      @IncludeSKUs BIT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    ) 
AS --********************************************************************************
-- Variable declaration
--********************************************************************************
    BEGIN
--********************************************************************************
-- code
--********************************************************************************
                DECLARE @ProductIdTable TABLE ( Id UNIQUEIDENTIFIER )
                INSERT  INTO @ProductIdTable
                        ( Id
                        )
                        SELECT  tab.col.value('text()[1]', 'uniqueidentifier')
                        FROM    @ProductIds.nodes('/ObjectIdCollection/ObjectId') tab ( col )
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
                        INNER JOIN CLImage C ON OI.ImageId = ISNULL(C.ParentImage,C.Id) AND OI.SiteId=C.SiteId
				WHERE (@ApplicationId is null or C.SiteId = @ApplicationId)
						AND C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                ORDER BY OI.Sequence ,
                        C.Title
    END
GO
	
IF(OBJECT_ID('Site_CreateMicrositeDataForMarketier') IS NOT NULL)
	DROP PROCEDURE Site_CreateMicrositeDataForMarketier
GO

CREATE  PROCEDURE [dbo].[Site_CreateMicrositeDataForMarketier]
(
	@MicroSiteId uniqueidentifier
	--@PageImportOptions
)
AS
BEGIN

--Pointer used for text / image updates. This might not be needed, but is declared here just in case
	DECLARE @pv binary(16)
	DECLARE @ApplicationId uniqueidentifier
	SET @ApplicationId = @MicroSiteId

	DECLARE @CommerceProductId uniqueidentifier
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'

	DECLARE @MarketierProductId uniqueidentifier
	SET @MarketierProductId='CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E'

	--DECLARE @UnsubscribePageId  uniqueidentifier
	--SET @UnsubscribePageId='18F27A17-03B2-4482-B375-3C48577B5702'

	--if not exists (select 1 from COContentStructure where VirtualPath='~/Content Library/MarketierLibrary' and SiteId=@MicroSiteId)
	--begin
	BEGIN TRANSACTION

	--========== Creating ManageContentLibrary folder for marketier
		DECLARE @ContentLibraryId uniqueidentifier, @MarketierContentLibrary uniqueidentifier, @unassignedFoldeId uniqueidentifier
		SELECT @ContentLibraryId = Id from COContentStructure where Title = 'Content Library' AND SiteId=@MicroSiteId
		SET @ContentLibraryId = Isnull(@ContentLibraryId, @ApplicationId)
		SET @MarketierContentLibrary = newid()--'E196A2C5-C070-4904-B93D-DFECED4ADABF'
		SET @unassignedFoldeId = newid() --'B843F49B-1427-48D9-A439-CBD265B36077'


		
			declare @p1 xml
			set @p1=convert(xml,N'<SiteDirectoryCollection><SiteDirectory Id="' + cast(@MarketierContentLibrary as char(36)) + '" Title="MarketierLibrary" Description="" CreatedBy="00000000-0000-0000-0000-000000000000" CreatedDate="" ModifiedBy="00000000-0000-0000-0000-000000000000" ModifiedDate="" Status="0" CreatedByFullName="" ModifiedByFullName="" ObjectTypeId="7" PhysicalPath="" VirtualPath="" Attributes="0" ThumbnailImagePath="" FolderIconPath="" Exists="0" Size="0" NumberOfFiles="0" CompleteURL="" IsSystem="True" IsMarketierDir="True" AccessRoles="" ParentId="' + cast(@ContentLibraryId as char(36)) + '" Lft="1" Rgt="2"/></SiteDirectoryCollection>')
			declare @p5 nvarchar(max)
			set @p5=N'<SISiteDirectory Id="'+ cast(@MarketierContentLibrary as char(36)) +'" VirtualPath="~/Content Library/MarketierLibrary" Attributes="0"/>'
			declare @p6 nvarchar(256)
			set @p6=N'ACME'
			IF NOT EXISTS (Select 1 from COContentStructure Where Title ='MarketierLibrary' And SiteId=@MicroSiteId)
			exec SqlDirectoryProvider_Save @XmlString=@p1,@ParentId=@ContentLibraryId,@ApplicationId=@MicroSiteId,@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MicroSiteId=null,@VirtualPath=@p5 output,@SiteName=@p6 output
			ELSE
				SET @MarketierContentLibrary = (Select top 1 Id  from COContentStructure Where Title ='MarketierLibrary' And SiteId=@MicroSiteId)

		  --UPDATING SITE SETTINGS FOR MARKETIER CONTENT LIBRARY
		  declare @MarketierContentLibSettingTypeId int 
		  SELECT @MarketierContentLibSettingTypeId = Id  FROM STSettingType Where Name = 'MarketierContentLibraryId'

		  IF (@MarketierContentLibSettingTypeId IS NOT NULL)
		  BEGIN
			IF ( NOT EXISTS (SELECT 1 FROM STSiteSetting WHERE SiteId=@MicroSiteId AND SettingTypeId=@MarketierContentLibSettingTypeId) )
			BEGIN
				INSERT INTO STSiteSetting (SiteId,SettingTypeId,Value)
				VALUES (@MicroSiteId,@MarketierContentLibSettingTypeId,@MarketierContentLibrary)
			END 
			ELSE
			BEGIN
				update [dbo].[STSiteSetting] set Value = @MarketierContentLibrary where SettingTypeId=@MarketierContentLibSettingTypeId AND SiteId=@MicroSiteId
			END
		 END
		
		--select @p5, @p6
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
		--declare @p1 xml
		set @p1=convert(xml,N'<SiteDirectoryCollection><SiteDirectory Id="'+ cast(@unassignedFoldeId as char(36)) + '" Title="Unassigned" Description="" CreatedBy="00000000-0000-0000-0000-000000000000" CreatedDate="" ModifiedBy="00000000-0000-0000-0000-000000000000" ModifiedDate="" Status="0" CreatedByFullName="" ModifiedByFullName="" ObjectTypeId="7" PhysicalPath="" VirtualPath="" Attributes="0" ThumbnailImagePath="" FolderIconPath="" Exists="0" Size="0" NumberOfFiles="0" CompleteURL="" IsSystem="True" IsMarketierDir="True" AccessRoles="" ParentId="' + cast(@MarketierContentLibrary as char(36)) + '" Lft="1" Rgt="2"/></SiteDirectoryCollection>')
		--declare @p5 nvarchar(max)
		set @p5=N'<SISiteDirectory Id="'+cast(@unassignedFoldeId as char(36)) +'" VirtualPath="~/Content Library/MarketierLibrary/Unassigned" Attributes="0"/>'
		--declare @p6 nvarchar(256)
		set @p6=N'ACME'
		IF NOT EXISTS (Select 1 from COContentStructure Where Title ='Unassigned' And SiteId=@MicroSiteId)
		exec SqlDirectoryProvider_Save @XmlString=@p1,@ParentId=@MarketierContentLibrary,@ApplicationId=@MicroSiteId,@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MicroSiteId=null,@VirtualPath=@p5 output,@SiteName=@p6 output
		ELSE 
			SET @unassignedFoldeId =( Select top 1 Id   from COContentStructure Where Title ='Unassigned' And SiteId=@MicroSiteId)
		--select @p5, @p6
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
	--============= Creating Maketier PageMapNodes 
		DECLARE @MarkierPageMapNodeRootId uniqueidentifier, @MarkierPageMapNodeId uniqueidentifier
		DECLARE @pmnXml xml
		DECLARE	@return_value int

		set @MarkierPageMapNodeRootId = newid() 
		print @MarkierPageMapNodeRootId

		set @pmnXml= '<pageMapNode id="' + convert(nvarchar(36),@MarkierPageMapNodeRootId) +'" description="MARKETIERPAGES" displayTitle="MARKETIERPAGES" friendlyUrl="MARKETIERPAGES" menuStatus="1" targetId="00000000-0000-0000-0000-000000000000" parentId="' + convert(nvarchar(36),@ApplicationId) +'" target="0" targetUrl="" createdBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" createdDate="Feb  9 2010  1:18PM" modifiedBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" modifiedDate="Feb  9 2010  1:18PM" propogateWorkFlow="True" inheritWorkFlow="False" propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="False" inheritRoles="False" roles="" securityLevels="" locationIdentifier="MARKETIERPAGES"/>'

		IF NOT EXISTS (Select 1 from PageMapNode Where description ='MARKETIERPAGES' AND displayTitle='MARKETIERPAGES' 
		AND friendlyUrl='MARKETIERPAGES' And SiteId=@MicroSiteId)
		EXEC @return_value = [dbo].[PageMapNode_Save]
					@Id=@MarkierPageMapNodeRootId,
					@SiteId = @ApplicationId,
					@ParentNodeId = @ApplicationId,
					@PageMapNode = @pmnXml,
					@CreatedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedDate=null
		SELECT 'Return Value' = @return_value


		
		IF @@ERROR<>0 
		Begin
			ROLLBACK TRANSACTION
			GOTO QuitWithErrors
		END

		

	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END

		DECLARE @ParentSiteId uniqueidentifier, @SourcePageMapNodeId uniqueidentifier
		SELECT TOP 1 @ParentSiteId = ParentSiteId FROM SISite WHERE Id = @MicroSiteId
		SELECT TOP 1 @SourcePageMapNodeId = PageMapNodeId FROM PageMapNode WHERE SiteId = @ParentSiteId AND LocationIdentifier like 'MARKETIERPAGES'
		UPDATE PageMapNode SET SourcePageMapNodeId = @SourcePageMapNodeId WHERE PageMapNodeId = @MarkierPageMapNodeRootId
 
		set @MarkierPageMapNodeId = newid() 
		
		Select @MarkierPageMapNodeRootId = PageMapNodeId from PageMapNode Where description ='MARKETIERPAGES' AND displayTitle='MARKETIERPAGES' 
		AND friendlyUrl='MARKETIERPAGES' And SiteId=@MicroSiteId

		set @pmnXml= '<pageMapNode id="' + convert(nvarchar(36),@MarkierPageMapNodeId) +'" description="MARKETIERCAMPAIGN" displayTitle="MARKETIERCAMPAIGN" friendlyUrl="MARKETIERCAMPAIGN" menuStatus="0" targetId="00000000-0000-0000-0000-000000000000" parentId="' + convert(nvarchar(36),@MarkierPageMapNodeRootId) +'" target="0" targetUrl="" createdBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" createdDate="Feb  9 2010  1:18PM" modifiedBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" modifiedDate="Feb  9 2010  6:18PM" propogateWorkFlow="True" inheritWorkFlow="False" propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="False" inheritRoles="False" roles="" securityLevels=""/>'
		
		IF NOT EXISTS (Select 1 from PageMapNode Where description ='MARKETIERCAMPAIGN' AND displayTitle='MARKETIERCAMPAIGN' 
		AND friendlyUrl='MARKETIERCAMPAIGN' And SiteId=@MicroSiteId)
		EXEC @return_value = [dbo].[PageMapNode_Save]
					@Id=@MarkierPageMapNodeId,
					@SiteId = @ApplicationId,
					@ParentNodeId = @MarkierPageMapNodeRootId,
					@PageMapNode = @pmnXml,
					@CreatedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedDate=null
		SELECT 'Return Value' = @return_value


		IF @@ERROR<>0 
		Begin
			ROLLBACK TRANSACTION
			GOTO QuitWithErrors
		END

		SELECT TOP 1 @SourcePageMapNodeId = PageMapNodeId FROM PageMapNode WHERE SiteId = @ParentSiteId AND FriendlyUrl like 'MARKETIERCAMPAIGN'
		UPDATE PageMapNode SET SourcePageMapNodeId = @SourcePageMapNodeId WHERE PageMapNodeId = @MarkierPageMapNodeId

		--UPDATING SITE SETTINGS FOR MARKETIER Campaign Page map node id
		declare @MarketierPageMapNodeSettingTypeId int 
		SELECT @MarketierPageMapNodeSettingTypeId = Id  FROM STSettingType Where Name = 'CampaignPageMapNodeId'
		
		Select @MarkierPageMapNodeId = PageMapNodeId from PageMapNode Where description ='MARKETIERCAMPAIGN' AND displayTitle='MARKETIERCAMPAIGN' 
		AND friendlyUrl='MARKETIERCAMPAIGN' And SiteId=@MicroSiteId

		IF (@MarketierContentLibSettingTypeId IS NOT NULL)
		BEGIN
		IF ( NOT EXISTS (SELECT 1 FROM STSiteSetting WHERE SiteId=@MicroSiteId AND SettingTypeId=@MarketierPageMapNodeSettingTypeId) )
		BEGIN
			INSERT INTO STSiteSetting (SiteId,SettingTypeId,Value)
			VALUES (@MicroSiteId,@MarketierPageMapNodeSettingTypeId,@MarkierPageMapNodeId)
		END 
		ELSE
		BEGIN
			update [dbo].[STSiteSetting] set Value = @MarkierPageMapNodeId where SettingTypeId=@MarketierPageMapNodeSettingTypeId AND SiteId=@MicroSiteId
		END
		END

		DECLARE @LandingPageNodeId uniqueidentifier, @LandingPageSettingTypeId int, @LandingPageSettingSequence int
		IF NOT EXISTS (SELECT 1 FROM STSettingType T JOIN STSiteSetting S ON T.Id = S.SettingTypeId
			WHERE T.Name = 'Marketier.LandingPageNodeId' AND S.SiteId = @MicroSiteId)
		BEGIN
			SET @LandingPageNodeId = NULL
		
			EXEC MenuDto_Save
				@PageMapNodeId = @LandingPageNodeId OUTPUT,
				@ParentId = @MarkierPageMapNodeRootId,
				@SiteId = @MicroSiteId,
				@DisplayTitle = 'Landing Pages',
				@FriendlyUrl = 'landing-pages',
				@ModifiedBy = '798613EE-7B85-4628-A6CC-E17EE80C09C5'

			SET @LandingPageSettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Marketier.LandingPageNodeId')
			IF @LandingPageSettingTypeId IS NULL
			BEGIN
				SELECT @LandingPageSettingSequence = MAX(sequence) + 1 FROM dbo.STSettingType
				INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, Sequence)
				VALUES('Marketier.LandingPageNodeId', 'Marketier.LandingPageNodeId',1, @LandingPageSettingSequence)

				SET @LandingPageSettingTypeId = @@IDENTITY
			END

			INSERT INTO STSiteSetting
			SELECT @MicroSiteId, @LandingPageSettingTypeId, @LandingPageNodeId
		END
	--end
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END

	--===== Updaging existing commerce group 
		-- Before Commerce custom settings group was 2(i.e Custom) and now it changed to 4 (i.e CommerceCustomSettings)
		--update STSettingType set SettingGroupId=(select Id from STSettingGroup where Name='CommerceCustomSettings') where SettingGroupId=2

	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
	
IF @@ERROR<>0 
Begin
	ROLLBACK TRANSACTION
	GOTO QuitWithErrors
END
ELSE
BEGIN
	COMMIT TRANSACTION
END

-- Creating default email group
DECLARE @CreatedBy uniqueidentifier
SELECT TOP 1 @CreatedBy = Id FROM USUser WHERE UserName like 'IAppsSystemUser'

IF NOT EXISTS(SELECT 1 FROM MKCampaignGroup WHERE Name ='My first email group' AND ApplicationId = @MicroSiteId)
BEGIN
	INSERT INTO MKCampaignGroup(Id, Name, CreatedBy, CreatedDate, ApplicationId)
	SELECT NEWID(), 'My first email group', @CreatedBy, getdate(), @MicroSiteId
END

-- Creating shared email group
IF NOT EXISTS(SELECT 1 FROM MKCampaignGroup WHERE Id = @MicroSiteId and ApplicationId = @MicroSiteId)
BEGIN
	INSERT INTO MKCampaignGroup(Id, Name, CreatedBy, CreatedDate, ApplicationId)
	SELECT @MicroSiteId, 'Shared Campaigns', @CreatedBy, getdate(), @MicroSiteId
END

-- Creating Deleted Email Campaigns group for this site
IF NOT EXISTS(SELECT 1 FROM MKCampaignGroup WHERE ApplicationId = @MicroSiteId and Name='Deleted Email Campaigns')
BEGIN
	INSERT INTO MKCampaignGroup(Id, Name, CreatedBy, CreatedDate, ApplicationId, NodeType, DisplayOrder)
	SELECT newid(), 'Deleted Email Campaigns', @CreatedBy, getdate(), @MicroSiteId, 11, 9999
END

--Create an entry in the TADistributionList table for all the shared contactlists so that the next run of statsrollup updates the count properly


if(not exists (select 1 from TADistributionListSite where SiteId=@MicroSiteId))
begin
	insert into TADistributionListSite 
	(
	Id,
	DistributionListId,
	SiteId,
	[Count],
	LastSynced
	)
	SELECT newid(),
	Id,
	@MicroSiteId,
	0,
	NULL
	FROM TADistributionLists 
	WHERE IsSystem=1 and IsGlobal=1
end


IF (NOT EXISTS (SELECT 1 FROM TADistributionGroup WHERE Title='Marketer Lists' AND ApplicationId = @MicroSiteId))
BEGIN
	INSERT INTO dbo.TADistributionGroup(Id, Title, Description, CreatedDate, CreatedBy, ApplicationId, NodeType, DisplayOrder)
	SELECT NEWID(), 'Marketer Lists', 'Marketer Lists', GETUTCDATE(), @CreatedBy, @MicroSiteId, 1, 2

	DECLARE @MarSettingTypeId int
	SELECT TOP 1 @MarSettingTypeId= Id FROM STSettingType WHERE Name='Marketier.MarketerContactListGroupId'

	INSERT INTO STSiteSetting
	(
		[SiteId],
		[SettingTypeId],
		[Value]
	)
	SELECT
	@MicroSiteId,
	@MarSettingTypeId,
	(SELECT TOP 1 G.Id FROM TADistributionGroup G WHERE G.ApplicationId=@MicroSiteId AND G.Title = 'Marketer Lists' )
END

IF NOT EXISTS (SELECT 1 FROM MKLinkGroup WHERE Title = 'Autogenerated Links' AND SiteId = @MicroSiteId)
BEGIN
	INSERT INTO MKLinkGroup
	(
		Id, 
		Title, 
		Description, 
		SiteId, 
		CreatedBy, 
		CreatedDate,
		NodeType,
		DisplayOrder)
	SELECT
		NEWID(), 
		'Autogenerated Links', 
		'Generated when sending email', 
		@MicroSiteId, 
		'00000000-0000-0000-0000-000000000000', 
		GETUTCDATE(),
		2,
		1
END

QuitWithErrors:

END
GO

IF(OBJECT_ID('SiteSettings_Save') IS NOT NULL)
	DROP PROCEDURE SiteSettings_Save
GO

CREATE PROCEDURE [dbo].[SiteSettings_Save]    
(     
 @siteId uniqueidentifier,  
 @settingsXml xml,  
 @customSettingsXml xml=null,  
 @CustomSettingsGroupId int=null,
 @RemoveMissingSettings bit = 1
)    
    
AS    
BEGIN   
 declare @maxSequence int  
 DECLARE @tempSettings TABLE   
 (   
  settingType varchar(256),   
  settingValue varchar(max)  
 )  
  
 if (@CustomSettingsGroupId is  null) set @CustomSettingsGroupId = 2 -- if @CustomSettingsGroupId id null then it goes common custom settings group  
  
 DECLARE @tempInsertSettings TABLE   
 (   
  settingType varchar(256),   
  settingValue varchar(max),  
  sequence int identity(1,1)  
 )  
  
 Insert Into @tempSettings(settingType,settingValue)  
 Select tab.col.value('(key/string/text())[1]','varchar(256)'),tab.col.value('(value/string/text())[1]','varchar(max)')  
 FROM @settingsXml.nodes('dictionary/SerializableDictionary/item') tab(col)   
  
 --update existing settings  
 update S   
 set S.Value = isnull(TS.settingValue,'')  
 from STSiteSetting S   
 inner join STSettingType ST on S.SettingTypeId=ST.Id  
 inner join @tempSettings TS  
 on (ST.Name)=(TS.settingType)  
 where S.SiteId=@siteId  
  
--Inserting records which are not there in STSiteSettings but Type is already created for that
 DECLARE @tempInsertSiteSettings TABLE   
 (   
  siteId uniqueidentifier,   
  settingTypeId int,  
  value   varchar(max) 
 ) 
 
insert into @tempInsertSiteSettings  select @siteId, ty.Id, tmp.settingValue from @tempSettings tmp, STSettingType ty
where tmp.SettingType  = ty.Name and ty.Id not in (select settingTypeId from STSiteSetting Where SiteId = @siteId )  

INSERT INTO STSiteSetting (SiteId, SettingTypeId, Value) SELECT SiteId, SettingTypeId, Value FROM @tempInsertSiteSettings where Value is not null  
  
 --Custom settings  
 delete from @tempSettings  
  IF @customSettingsXml is not null
  BEGIN
	  
	 Insert Into @tempSettings(settingType,settingValue)  
	 Select tab.col.value('(key/string/text())[1]','varchar(256)'),tab.col.value('(value/string/text())[1]','varchar(max)')  
	 FROM @customSettingsXml.nodes('dictionary/SerializableDictionary/item') tab(col)   
	   
	 
	 --update existing custom settings  
	 update S   
	 set S.Value = isnull(TS.settingValue,'')  
	 from STSiteSetting S   
	 inner join STSettingType ST on S.SettingTypeId=ST.Id  
	 inner join @tempSettings TS  
	 on (ST.Name)=(TS.settingType)  
	 where S.SiteId=@siteId  

	 --insert new records from input xml  
	 select @maxSequence= max(Sequence) from STSettingType  
	 insert into @tempInsertSettings  
	 select settingType,settingValue from @tempSettings 
		where (settingType) in(select (settingType) from @tempSettings   except select (Name) from STSettingType)  
	 
	 insert into STSettingType (Name, FriendlyName, SettingGroupId,Sequence) 
		select settingType,settingType,@CustomSettingsGroupId,@maxSequence +Sequence from @tempInsertSettings  
		
	--In case the setting type already existed because it was in there for a siteid, we will not be able to insert
	-- the value in for another siteid. So we need to pull all these setting types into the @tempInsertSettings table for the following join to 
	-- work
 
		delete from @tempInsertSettings
		insert into @tempInsertSettings  select settingType,settingValue from @tempSettings 
			where (settingType) in(select (settingType) from @tempSettings)

		insert into STSiteSetting 
		select @SiteId,ST.Id,isnull(TIS.settingValue,'') 
			from STSettingType ST inner join @tempInsertSettings TIS on ST.Name = TIS.settingType
			Where ST.Id not in (select SettingTypeId from STSiteSetting Where SiteId=@siteId)
  
	 --delete the records those are not in input xml  
	 IF	@RemoveMissingSettings = 1
		BEGIN
			 delete from STSiteSetting where SettingTypeId in (select Id from STSettingType where (Name) in (select (ST.Name) from  STSettingType ST except select (settingType) from @tempSettings)  and SettingGroupId=@CustomSettingsGroupId) And SiteId=@siteId   
			 delete from STSettingType where (Name) in (select (ST.Name) from  STSettingType ST except select (settingType) from @tempSettings)  and SettingGroupId=@CustomSettingsGroupId  And Id Not IN (SELECT SettingTypeId From STSiteSetting)
		END 
  END    

  select dbo.ClearCachedTimeZone(@siteId)  -- Clear CLR Time Zone 
END
GO

PRINT 'Modify stored procedure ShippingOption_GetAllActiveShippingOptionsWithPrice'
GO

IF(OBJECT_ID('ShippingOption_GetAllActiveShippingOptionsWithPrice') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetAllActiveShippingOptionsWithPrice
GO

CREATE PROCEDURE [dbo].[ShippingOption_GetAllActiveShippingOptionsWithPrice]
(	
	@OrderTotal money,
	@ApplicationId uniqueidentifier=null,
	@IncludeVariantSites bit = 0,
	@ShipperId uniqueidentifier=null
)
AS
BEGIN

 IF @IncludeVariantSites IS NULL
	SET @IncludeVariantSites = 0

	Select  OSO.Id
		,OSO.ShipperId
		,OSO.Name
		,OSO.Description
		,OSO.CustomId
		,OSO.NumberOfDaysToDeliver
		,OSO.CutOffHour
		,OSO.SaturdayDelivery
		,OSO.AllowShippingCoupon
		,OSO.IsPublic
		,OSO.IsInternational
		,OSO.ServiceCode
		,OSO.[ExternalShipCode]
		,OSO.BaseRate
		,CASE WHEN OS.SiteId = OSO.SiteId THEN OS.IsShared ELSE 0 END AS IsShared
		,OS.SiteId AS ShipperSiteId
		,OSO.SiteId	
		,OSO.Sequence
		,OSO.Status
		,OSO.CreatedBy
		,OSO.CreatedDate AS CreatedDate
		,OSO.ModifiedBy
		,OSO.ModifiedDate AS ModifiedDate
		,OSR.ShippingPrice  
	FROM ORShippingOption OSO
		INNER JOIN ORShipper OS ON OS.Id = OSO.ShipperId
		INNER JOIN ORShippingOptionRate OSR ON OSO.Id = OSR.ShippingOptionId
	Where OS.Status= 1 AND OSO.[Status] = 1  AND
			@OrderTotal between OSR.StartAmount and 
					CASE OSR.EndAmount when 0 then  922337203685477.5807 
										else OSR.EndAmount end
	AND (@ApplicationId is null OR (@IncludeVariantSites = 0 AND OSO.[SiteId] = @ApplicationId) OR (@IncludeVariantSites = 1 AND OSO.SiteId in (select SiteId from dbo.GetVariantSites(@ApplicationId))))
	AND (@ShipperId is null OR OS.Id = @ShipperId)
	ORDER BY OSR.ShippingPrice ASC
END
GO

PRINT 'Modify stored procedure ShippingOption_GetOptionsWithPrice'
GO

IF(OBJECT_ID('ShippingOption_GetOptionsWithPrice') IS NOT NULL)
	DROP PROCEDURE ShippingOption_GetOptionsWithPrice
GO

CREATE PROCEDURE [dbo].[ShippingOption_GetOptionsWithPrice]
(
	@ShipperId uniqueidentifier,
	@OrderTotal money,
	@ApplicationId uniqueidentifier=null
)
AS
BEGIN

	Select OSO.Id
		,OSO.ShipperId
		,OSO.Name
		,OSO.Description
		,OSO.CustomId
		,OSO.NumberOfDaysToDeliver
		,OSO.CutOffHour
		,OSO.SaturdayDelivery
		,OSO.AllowShippingCoupon
		,OSO.IsPublic
		,OSO.IsInternational
		,OSO.ServiceCode
		,OSO.[ExternalShipCode]
		,OSO.BaseRate
		,OSO.SiteId	
		,OSO.Sequence
		,OSO.Status
		,OSO.CreatedBy
		, OSO.CreatedDate AS CreatedDate
		,OSO.ModifiedBy
		, OSO.ModifiedDate AS ModifiedDate
		, OSR.ShippingPrice
	FROM ORShippingOption OSO
		INNER JOIN ORShipper OS ON OS.Id = OSO.ShipperId
		INNER JOIN ORShippingOptionRate OSR ON OSO.Id = OSR.ShippingOptionId
	Where OS.Status= 1 AND OSO.[Status] = 1 AND OS.Id=@ShipperId AND
			@OrderTotal between OSR.StartAmount and 
					CASE OSR.EndAmount when 0 then  922337203685477.5807 
										else OSR.EndAmount end
	AND (@ApplicationId is null OR OSO.SiteId in (select SiteId from dbo.GetParentSites(@ApplicationId, 1, 1)))
END
GO

PRINT 'Modify stored procedure Product_GetOnlineProductByFilterMulti'
GO

IF(OBJECT_ID('Product_GetOnlineProductByFilterMulti') IS NOT NULL)
	DROP PROCEDURE Product_GetOnlineProductByFilterMulti
GO

CREATE PROCEDURE [dbo].[Product_GetOnlineProductByFilterMulti]
(
	@FilterId uniqueidentifier,
    @SortCol VARCHAR(25)=null,
    @PageSize INT=null, 
	@PageNumber INT=1,
	@ApplicationId uniqueidentifier = null,
	@FacetValueIds Xml=null,
	@CustomerId uniqueidentifier= null,
	@IsActiveAndIsOnline TINYINT=1,
	@IsOnline TINYINT=0	
	--,@VirtualCount int output
)

AS
BEGIN
	Declare @Rowcount int
	Declare @FacetValues table(FacetValueID uniqueidentifier,FacetId Uniqueidentifier)


SET @SortCol = lower(@SortCol)



DECLARE @GroupIds TABLE (
GroupId uniqueidentifier
 )

IF @CustomerId IS NOT NULL
	INSERT INTO @GroupIds
	select GroupId from USMemberGroup WHERE MemberId = @CustomerId
ELSE
BEGIN
	
	DECLARE @GId uniqueidentifier
		SET @GId =(SELECT top 1 Value from STSiteSetting SS  
		INNER  JOIN STSettingType ST ON SS.SettingTypeId = ST.Id where ST.Name = 'CommerceEveryOneGroupId')
		
			INSERT INTO @GroupIds(GroupId) Values (@GId)
END

IF @PageSize IS NULL
	SET @PageSize = 2147483647

DECLARE @SQL nvarchar(max),@params nvarchar(100)
declare @isLatest bit

	Select @IsLatest =IsLatest from NVFilterQuery 
	Where Id=@FilterId And 
	(@ApplicationId is null or SiteId = @ApplicationId)
	--SiteId=ISNULL(@ApplicationId, SiteId)
	if (@IsLatest=0)
	Begin
		Exec NavFilter_ExecuteQuery @FilterId 
	End

	DECLARE @ProductIDs Table
	(
		Row_ID int IDENTITY(1,1) PRIMARY KEY,
		ProductID uniqueidentifier
	)
	
IF(@FacetValueIds is not null)
BEGIN
		Insert into @FacetValues
		Select tab.col.value('(value/guid/text())[1]','uniqueidentifier') ,tab.col.value('(key/guid/text())[1]','uniqueidentifier')
		FROM @FacetValueIds.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item')tab(col) 
END

IF @SortCol = 'price asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	
							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice ASC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice 
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID

									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId 

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice ASC
							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											 MinPrice ASC			
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,   
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND (@IsActiveAndIsOnline = 0 OR (PS.IsActive = 1 AND PS.IsOnline = 1))
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId
									GROUP BY   cte.ProductId
						) iq	
						
					)

					
						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice ASC

					END 
END
IF @SortCol = 'price desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID
								
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											MinPrice DESC
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1 AND (@IsActiveAndIsOnline = 0 OR (PS.IsActive = 1 AND PS.IsOnline = 1))
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId

									GROUP BY   cte.ProductId
						) iq	
					
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc


					END 
END

IF @SortCol = 'soldcount desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
									
				Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1   
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc
							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount DESC		
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc

					END 
END


IF @SortCol = 'soldcount asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
										
			Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate 
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
								--	INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount ASC				
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC


					END 
END

IF @SortCol = 'createddate asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate ASC			
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC


					END 
END

IF @SortCol = 'createddate desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate DESC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate desc	
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate Desc


					END 
END

IF @SortCol = 'title asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
					Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	


							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													Title ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 Title
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.Title) Title
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,Title)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											Title ASC			
											) AS [Row_ID],
									iq.ProductId,
									Title
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(Title) Title
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.Title
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title ASC


					END 
END

IF @SortCol = 'title desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
									
		Set @Rowcount =(Select count(Distinct(FacetId)) From @FacetValues)
	

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													Title desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 Title
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.Title) Title
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON (FV.FacetValueID = C.FacetValueID AND FV.FacetId =C.FacetId)
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN PRProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title DESC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,Title)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											Title desc	
											) AS [Row_ID],
									iq.ProductId,
									Title
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(Title) Title
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.Title
												FROM         
													dbo.PRProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
														Where  P.IsActive = 1
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by Title Desc


					END 
END

--START
	IF (@IsActiveAndIsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1 AND PS.IsActive = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber
			
		END
	ELSE IF (@IsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber

		END
--END

END
GO

PRINT 'Modify stored procedure ProductAttribute_GetAttributesByXmlGuids'
GO

IF(OBJECT_ID('ProductAttribute_GetAttributesByXmlGuids') IS NOT NULL)
	DROP PROCEDURE ProductAttribute_GetAttributesByXmlGuids
GO

CREATE PROCEDURE [dbo].[ProductAttribute_GetAttributesByXmlGuids](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId uniqueidentifier=null,
			@Ids xml
)
AS
			SELECT Distinct
				a.Id,
				ag.CategoryId,			
				a.AttributeDataType,
				a.AttributeDataTypeId,
				a.Title,
				a.Description,
				a.Sequence,
				a.Code,
				a.IsFaceted,
				a.IsSearched,
				a.IsSystem,
				a.IsDisplayed,
				a.IsEnum,		
				cast(0 as bit) as IsRequired,		
				dbo.ConvertTimeFromUtc(a.CreatedDate,@ApplicationId)CreatedDate,
				a.CreatedBy,
				dbo.ConvertTimeFromUtc(a.ModifiedDate,@ApplicationId)ModifiedDate,
				a.ModifiedBy,
				a.Status,
				cast(0 as bit) as IsSKULevel , a.IsPersonalized
			FROM 
			@Ids.nodes('/GenericCollectionOfGuid/guid')tab(col)	
			Inner Join 	ATAttribute a  on a.Id =tab.col.value('text()[1]','uniqueidentifier')
		LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
GO

IF(OBJECT_ID('CommerceReport_SKUWithoutImages') IS NOT NULL)
	DROP PROCEDURE CommerceReport_SKUWithoutImages
GO

CREATE PROCEDURE [dbo].[CommerceReport_SKUWithoutImages]
(	
	@maxReturnCount int,
	@pageSize int = 10,@pageNumber int, 
	@sortBy nvarchar(50) = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier = null,
	@IncludeChildrenSites Bit = 0
) 
AS

BEGIN

if @maxReturnCount = 0
	set @maxReturnCount = null


IF @pageSize IS NULL
	SET @PageSize = 2147483647

declare @tmpProductWithoutImage table(
	ProductId uniqueidentifier,
	ProductTitle nvarchar(255),
	SKUId uniqueidentifier,
	SKUTitle nvarchar(255),
	SKU nvarchar(255),
	IsActive bit,
	IsOnline bit
	)

INSERT INTO @tmpProductWithoutImage
	SELECT	P.Id, P.Title, PSKU.Id, PSKU.Title AS ProductSKUTitle, PSKU.SKU, PSKU.IsActive, PSKU.IsOnline
	FROM	PRProduct AS P 
			INNER JOIN PRProductSKU AS PSKU ON P.Id = PSKU.ProductId
	WHERE	P.Id NOT IN (
				SELECT	ObjectId
				FROM	CLObjectImage AS OI
						INNER JOIN CLImage AS I ON OI.ImageId = ISNULL(I.ParentImage, I.Id)
				WHERE	ObjectType = 205 AND OI.SiteId=@ApplicationId
			) AND
			PSKU.Id NOT IN (
				SELECT	ObjectId
				FROM	CLObjectImage AS OI
						INNER JOIN CLImage AS I ON OI.ImageId = ISNULL(I.ParentImage, I.Id)
				WHERE	ObjectType = 206 AND OI.SiteId=@ApplicationId
			)

Set @virtualCount = @@ROWCOUNT

if @sortBy is null or (@sortBy) = 'producttitle asc'
Begin
	Select A.*
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by ProductTitle asc) as RowNumber
		 from @tmpProductWithoutImage
		) A
		where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	 Order by A.ProductTitle ASC
end
else if (@sortBy) = 'producttitle desc'
Begin
	Select A.*
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by ProductTitle desc) as RowNumber
		 from @tmpProductWithoutImage
		) A
		where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	 Order by A.ProductTitle DESC
end
END


GO


PRINT 'Modify stored procedure OrderItem_GetSKUAttributeValues'
GO
IF(OBJECT_ID('OrderItem_GetSKUAttributeValues') IS NOT NULL)
	DROP PROCEDURE OrderItem_GetSKUAttributeValues
GO
CREATE PROCEDURE [dbo].[OrderItem_GetSKUAttributeValues]
    (
      @OrderId UNIQUEIDENTIFIER = NULL ,
      @OrderItemId UNIQUEIDENTIFIER = NULL ,
      @CartId UNIQUEIDENTIFIER = NULL ,
      @CartItemId UNIQUEIDENTIFIER = NULL ,
      @ApplicationId UNIQUEIDENTIFIER
    )
AS 

    IF @CartId IS NOT NULL
        OR @OrderId IS NOT NULL 
        SELECT  @OrderId = Id ,
                @CartId = CartId
        FROM    dbo.OROrder
        WHERE   CartId = @CartId
                OR Id = @OrderId
	
    IF @CartItemId IS NOT NULL
        OR @OrderItemId IS NOT NULL 
        SELECT  @OrderItemId = Id ,
                @CartItemId = CartItemId ,
                @OrderId = OrderId
        FROM    dbo.OROrderItem
        WHERE   CartItemId = @CartItemId
                OR Id = @OrderItemId
                
    IF @CartId IS NULL AND @CartItemId IS NOT NULL
        SELECT  @CartId = CartId
        FROM    dbo.CSCartItem
        WHERE   Id = @CartItemId
		
    SELECT  [OrderId] ,
            [OrderItemId] ,
            [SKUId] ,
            [ProductId] ,
            [CustomerId] ,
            [ProductName] ,
            [AttributeId] ,
            [AttributeName] ,
            [AttributeEnumId] ,
            [Value] ,
            [Code] ,
            [NumericValue] ,
            [Sequence] ,
            [IsDefault] ,
            [CartId] ,
            [CartItemID] ,
            [CategoryId] ,
            [IsFaceted] ,
            [IsPersonalized] ,
            [IsSearched] ,
            [IsSystem] ,
            [IsDisplayed] ,
            [IsEnum] ,
            [AttributeDataTypeId]
    INTO    #itemAttribute
    FROM    [dbo].[VWOrderItemPersonalizedAttribute]
    WHERE   CartId = @CartId
            OR OrderId = @OrderId
			
			
    IF @CartItemId IS NOT NULL 
        DELETE  FROM #itemAttribute
        WHERE   CartItemId != @CartItemId 
    IF @OrderItemId IS NOT NULL 
        DELETE  FROM #itemAttribute
        WHERE   OrderItemId != @OrderItemId
        
    SELECT  *
    FROM    #itemAttribute
			
GO


PRINT 'Modify stored procedure Order_GetOrders'
GO
IF(OBJECT_ID('Order_GetOrders') IS NOT NULL)
	DROP PROCEDURE Order_GetOrders
GO

CREATE PROCEDURE [dbo].[Order_GetOrders](@OrderStatusId int,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN

Declare @tblOrders table
(
Id    uniqueidentifier
,CartId uniqueidentifier
,OrderTypeId    INT
,OrderStatusId    int
,PurchaseOrderNumber nvarchar(100)
,ExternalOrderNumber    nvarchar(100)
,SiteId    uniqueidentifier
,CustomerId     uniqueidentifier
,TotalTax    money
,OrderTotal   money
,CODCharges money
,TaxableOrderTotal   money
,GrandTotal   money
,TotalShippingCharge money
,TotalDiscount    money
,PaymentTotal    money
,RefundTotal money
,PaymentRemaining    money
,OrderDate    dateTime
,CreatedDate  dateTime  
,CreatedBy    uniqueidentifier
,CreatedByFullName nvarchar(3074)
,ModifiedDate   dateTime 
,ModifiedBy uniqueidentifier
,TotalShippingDiscount  money
);

with cteOrders
AS
(
SELECT     
 O.[Id],    
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],  
 O.[ExternalOrderNumber],  
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.RefundTotal,
 O.[PaymentRemaining],   
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId) OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 FN.UserFullName CreatedByFullName,
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O    
LEFT JOIN VW_UserFullName FN on FN.UserId = O.CreatedBy
WHERE
O.SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites( @ApplicationId,1)) AND
O.OrderStatusId = Isnull(@OrderStatusId, O.OrderStatusId)
)


Insert into @tblOrders
SELECT * FROM cteOrders 

SELECT * FROM @tblOrders
Order by OrderDate;

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderId IN (SELECT Id FROM @tblOrders) AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.Tax,
	OI.Discount,
	OI.HandlingTax
FROM OROrderItem OI
LEFT JOIN VW_UserFullName FN on FN.UserId = OI.CreatedBy
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
WHERE OI.OrderId IN (SELECT Id FROM @tblOrders);

SELECT 
	OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId)ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId)ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
dbo.ConvertTimeFromUtc(OS.[CreatedDate],@ApplicationId) CreatedDate,
OS.[ModifiedBy],
dbo.ConvertTimeFromUtc(OS.[ModifiedDate],@ApplicationId) ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder,
OS.IsHandlingIncluded,
OS.ShippingChargeTax,
OS.TaxableTotal,
OS.CustomStatus
FROM OROrderShipping OS
WHERE OS.OrderId IN (SELECT Id FROM @tblOrders);

select  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized,
  P.OrderId 
from PRProductAttribute PA  
Inner join 
	(Select ProductId, OI.OrderId  FROM OROrderItem OI
		Inner Join PRProductSKU S on OI.ProductSKUId =S.Id  
		Where OI.OrderId IN (SELECT Id FROM @tblOrders)
		) P On PA.ProductId=P.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id  
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId  
where  A.Status=1  AND PA.IsSKULevel=1 AND  A.IsPersonalized =1


SELECT [OrderId]
      ,[OrderItemId]
      ,[SKUId]
      ,[ProductId]
      ,[CustomerId]
      ,[ProductName]
      ,[AttributeId]
      ,[AttributeName]
      ,[AttributeEnumId]
      ,[Value]
      ,[Code]
      ,[NumericValue]
      ,[Sequence]
      ,[IsDefault]
      ,[CartId]
      ,[CartItemID]
      ,[CategoryId]
      ,[IsFaceted]
      ,[IsPersonalized]
      ,[IsSearched]
      ,[IsSystem]
      ,[IsDisplayed]
      ,[IsEnum]
      ,[AttributeDataTypeId]
  FROM [dbo].[VWOrderItemPersonalizedAttribute]
	Where OrderId IN (SELECT Id FROM @tblOrders)

--- order shipments
SELECT F.[Id]
      ,F.[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(F.ShippedDate,@ApplicationId)ShippedDate
      ,F.[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(F.CreateDate,@ApplicationId)CreateDate
      ,F.[CreatedBy]
      ,F.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(F.ModifiedDate,@ApplicationId)ModifiedDate
	  ,F.[EstimatedShipmentTotal]
	  ,F.ShipmentStatus
	  ,F.WarehouseId
	  ,OS.OrderId
	,F.ExternalReferenceNumber
FROM [dbo].[FFOrderShipment] F
INNER JOIN  OROrderShipping OS ON F.OrderShippingId = OS.Id
Where OS.OrderId IN (SELECT Id FROM @tblOrders)
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
      ,OS.OrderId
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
INNER JOIN  OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId IN (SELECT Id FROM @tblOrders)



SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
      ,OS.OrderId
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId IN (SELECT Id FROM @tblOrders)

END

GO
GO
PRINT 'Modify stored procedure Order_GetOrdersByXmlGuids'
GO
IF(OBJECT_ID('Order_GetOrdersByXmlGuids') IS NOT NULL)
	DROP PROCEDURE Order_GetOrdersByXmlGuids
GO
CREATE PROCEDURE [dbo].[Order_GetOrdersByXmlGuids](@Ids XML,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN
Declare @OrderIds table(Id uniqueidentifier)
INSERT INTO @OrderIds
Select tab.col.value('text()[1]','uniqueidentifier') 
from  @Ids.nodes('/GenericCollectionOfGuid/guid')tab(col)

IF (select count(*) from @OrderIds) =0
	begin
		raiserror('DBERROR||%s',16,1,'OrderId is required')
		return dbo.GetDataBaseErrorCode()	
	end

SELECT     
 O.[Id],  
 O.[Id]  OrderId,  
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],  
 O.[ExternalOrderNumber],  
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.RefundTotal,
 O.[PaymentRemaining],   
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId) OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 FN.UserFullName CreatedByFullName,
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O    
LEFT JOIN VW_UserFullName FN on FN.UserId = O.CreatedBy
WHERE
O.SiteId IN ( SELECT SiteId FROM dbo.GetChildrenSites( @ApplicationId,1)) 
AND O.Id in (Select Id from @OrderIds)

;with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderId in (Select Id from @OrderIds) AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.Tax,
	OI.Discount,
	OI.HandlingTax
FROM OROrderItem OI
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
LEFT JOIN VW_UserFullName FN on FN.UserId = OI.CreatedBy
WHERE OI.OrderId in (Select Id from @OrderIds) ;

SELECT 
	OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId)ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId)ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
dbo.ConvertTimeFromUtc(OS.[CreatedDate],@ApplicationId) CreatedDate,
OS.[ModifiedBy],
dbo.ConvertTimeFromUtc(OS.[ModifiedDate],@ApplicationId) ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder,
OS.ShippingChargeTax,
OS.TaxableTotal,
OS.CustomStatus
FROM OROrderShipping OS
WHERE OS.OrderId in (Select Id from @OrderIds) ;

select  
  A.Id,  
  Ag.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized,
  P.OrderId
from PRProductAttribute PA  
Inner join 
	(Select ProductId,OI.Orderid FROM OROrderItem OI
		Inner Join PRProductSKU S on OI.ProductSKUId =S.Id  
		Where OI.OrderId in (Select Id from @OrderIds) 
		) P On PA.ProductId=P.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=1  AND PA.IsSKULevel=1 AND  A.IsPersonalized =1


SELECT [OrderId]
      ,[OrderItemId]
      ,[SKUId]
      ,[ProductId]
      ,[CustomerId]
      ,[ProductName]
      ,[AttributeId]
      ,[AttributeName]
      ,[AttributeEnumId]
      ,[Value]
      ,[Code]
      ,[NumericValue]
      ,[Sequence]
      ,[IsDefault]
      ,[CartId]
      ,[CartItemID]
      ,[CategoryId]
      ,[IsFaceted]
      ,[IsPersonalized]
      ,[IsSearched]
      ,[IsSystem]
      ,[IsDisplayed]
      ,[IsEnum]
      ,[AttributeDataTypeId]
  FROM [dbo].[VWOrderItemPersonalizedAttribute]
	Where OrderId in (Select Id from @OrderIds) 

--- order shipments
SELECT F.[Id]
	 ,OS.OrderId
      ,F.[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(F.ShippedDate,@ApplicationId)ShippedDate
      ,F.[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(F.CreateDate,@ApplicationId)CreateDate
      ,F.[CreatedBy]
      ,F.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(F.ModifiedDate,@ApplicationId)ModifiedDate
	  ,F.[EstimatedShipmentTotal]
	  ,F.ShipmentStatus
	  ,F.WarehouseId
	,F.ExternalReferenceNumber
FROM [dbo].[FFOrderShipment] F
INNER JOIN  OROrderShipping OS ON F.OrderShippingId = OS.Id
Where OS.OrderId in (Select Id from @OrderIds) 
Order By CreateDate DESC

SELECT SI.[Id]
      ,OS.OrderId
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
INNER JOIN  OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId in (Select Id from @OrderIds) 



SELECT SP.[Id]
	  ,OS.OrderId
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId in (Select Id from @OrderIds) 

END
GO

PRINT 'Modify stored procedure ProductTypeAttribute_GetProductTypeAttribute'
GO
IF(OBJECT_ID('ProductTypeAttribute_GetProductTypeAttribute') IS NOT NULL)
	DROP PROCEDURE ProductTypeAttribute_GetProductTypeAttribute
GO
CREATE PROCEDURE [dbo].[ProductTypeAttribute_GetProductTypeAttribute](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ProductTypeAttributeId				uniqueidentifier = null				
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

		/* Return all ProductType Attributes */
        SELECT 
				PTA.Id, 
				PTA.ProductTypeId,
				PTA.AttributeId,
				PTA.Sequence,
				A.Title As AttributeTitle, 
				AG.Title As GroupTitle, AG.CategoryId,AG.CategoryId As GroupId
        FROM	dbo.PRProductTypeAttribute PTA
		INNER JOIN ATAttribute A ON PTA.AttributeId	=	A.Id		
		LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
        WHERE	(@ProductTypeAttributeId is null or PTA.[Id] = @ProductTypeAttributeId) 

END

GO
PRINT 'Modify stored procedure ProductTypeAttribute_GetProductTypeAttributeWithValues'
GO
IF(OBJECT_ID('ProductTypeAttribute_GetProductTypeAttributeWithValues') IS NOT NULL)
	DROP PROCEDURE ProductTypeAttribute_GetProductTypeAttributeWithValues
GO
CREATE PROCEDURE [dbo].[ProductTypeAttribute_GetProductTypeAttributeWithValues](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ProductTypeAttributeId				uniqueidentifier = null				
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

		/* Return all ProductType Attributes */
        SELECT 
				PTA.Id, 
				PTA.ProductTypeId,
				PTA.AttributeId,
				PTA.Sequence,
				A.Title As AttributeTitle, 
				AG.Title As GroupTitle, AG.CategoryId As GroupId,AG.CategoryId
        FROM	dbo.PRProductTypeAttribute PTA
		INNER JOIN ATAttribute A ON PTA.AttributeId=	A.Id
		LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
        WHERE	--PTA.[Id]			=	Isnull(@ProductTypeAttributeId,PTA.[Id])		AND
				(@ProductTypeAttributeId is null or PTA.[Id] = @ProductTypeAttributeId) 
		/* Return all ProductType Attributes values*/
		SELECT 
				PTV.Id, 
				PTV.ProductTypeAttributeId,
				PTV.AttributeId,
				PTV.AttributeValueId,
				PTV.IsSKULevel,
				PTV.Sequence,
				A.Title As AttributeTitle, AV.Value As Value
		FROM	PRProductTypeAttributeValue PTV, PRProductTypeAttribute PTA, ATAttribute A, ATAttributeEnum AV
		WHERE	PTV.ProductTypeAttributeId = PTA.Id		AND
				--PTA.[Id]				=	Isnull(@ProductTypeAttributeId,PTA.Id)		AND
				(@ProductTypeAttributeId is null or PTA.[Id] = @ProductTypeAttributeId) AND
				PTA.AttributeId			=	A.Id AND
				AV.AttributeID			=	A.Id AND
				AV.Id					=	PTV.AttributeValueId
END
GO
PRINT 'Modify stored procedure ProductType_GetAttributes'
GO
IF(OBJECT_ID('ProductType_GetAttributes') IS NOT NULL)
	DROP PROCEDURE ProductType_GetAttributes
GO
CREATE PROCEDURE [dbo].[ProductType_GetAttributes]
(
	@ProductTypeId uniqueidentifier
	,@ApplicationId uniqueidentifier=null
)
AS
BEGIN
select 
		A.Id,
		AG.CategoryId,
		A.AttributeDataType,
		A.AttributeDataTypeId,
		A.Title,
		A.Description,
		A.Sequence,
		A.Code,
		A.IsFaceted,
		A.IsSearched,
		A.IsSystem,
		A.IsDisplayed,
		A.IsEnum,
		A.IsPersonalized,
		PTA.IsRequired,				
		dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
		A.CreatedBy,
		dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
		A.ModifiedBy,
		A.Status,
		PTA.IsSKULevel	, A.IsPersonalized	
from PRProductTypeAttribute PTA,ATAttribute A
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where PTA.AttributeId=A.Id and A.Status=dbo.GetActiveStatus() and  PTA.ProductTypeId =@ProductTypeId 

END

GO
PRINT 'Modify stored procedure ProductType_GetProductTypeAttribute'
GO
IF(OBJECT_ID('ProductType_GetProductTypeAttribute') IS NOT NULL)
	DROP PROCEDURE ProductType_GetProductTypeAttribute
GO
CREATE PROCEDURE [dbo].[ProductType_GetProductTypeAttribute](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ProductTypeId				uniqueidentifier = null				
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

		/* Return all ProductType Attributes */
        SELECT 
				PTA.Id, 
				PTA.ProductTypeId,
				PTA.AttributeId,
				PTA.Sequence,
				A.Title As AttributeTitle, 
				AG.Title As GroupTitle, AG.CategoryId
        FROM	dbo.PRProductTypeAttribute PTA
		INNER JOIN ATAttribute A ON PTA.AttributeId = A.Id
		LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
        WHERE @ProductTypeId is null or PTA.[ProductTypeId] = @ProductTypeId 
				
END
GO
PRINT 'Modify stored procedure ProductType_GetProductTypeAttributeWithValues'
GO
IF(OBJECT_ID('ProductType_GetProductTypeAttributeWithValues') IS NOT NULL)
	DROP PROCEDURE ProductType_GetProductTypeAttributeWithValues
GO
CREATE PROCEDURE [dbo].[ProductType_GetProductTypeAttributeWithValues](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ProductTypeId				uniqueidentifier = null				
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

		/* Return all ProductType Attributes */
        SELECT 
				PTA.Id, 
				PTA.ProductTypeId,
				PTA.AttributeId,
				PTA.Sequence,
				A.Title As AttributeTitle, 
				A.AttributeDataType As AttributeDataType,
				A.AttributeDataTypeId As AttributeDataTypeId,
				AG.CategoryId,
				AG.Title As GroupTitle, 
				AG.CategoryId As GroupId
        FROM	dbo.PRProductTypeAttribute PTA 
		INNER JOIN ATAttribute A ON PTA.AttributeId=A.Id
		LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
        WHERE	
				--PTA.[ProductTypeId]			=	Isnull(@ProductTypeId,[ProductTypeId])		AND
				(@ProductTypeId is null or PTA.[ProductTypeId] = @ProductTypeId)
		ORDER BY PTA.Sequence
		
		/* Return all ProductType Attributes values; */
		SELECT 
				PTV.Id, 
				PTV.ProductTypeAttributeId,
				PTV.AttributeId,
				PTV.AttributeValueId,
				PTV.IsSKULevel,
				PTV.Sequence,
				A.Title As AttributeTitle, AV.Value As Value
		FROM	PRProductTypeAttributeValue PTV, PRProductTypeAttribute PTA, ATAttribute A, ATAttributeEnum AV
		WHERE	PTV.ProductTypeAttributeId = PTA.Id		AND
				--PTA.[ProductTypeId]		=	Isnull(@ProductTypeId,PTA.ProductTypeId)	AND
				(@ProductTypeId is null or PTA.[ProductTypeId] = @ProductTypeId) AND
				PTA.AttributeId			=	A.Id	AND
				AV.AttributeID			=	A.Id	AND
				AV.Id					=	PTV.AttributeValueId
		ORDER BY PTV.Sequence
END
GO
PRINT 'Modify stored procedure Product_CreateFlatTable'
GO
IF(OBJECT_ID('Product_CreateFlatTable') IS NOT NULL)
	DROP PROCEDURE Product_CreateFlatTable
GO
CREATE PROCEDURE [dbo].[Product_CreateFlatTable]
As
BEGIN

	if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRFlatProduct]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
		drop table [dbo].[PRFlatProduct]
	
	SELECT * INTO PRFlatProduct FROM PRProduct 

	DECLARE @DataType nvarchar(max), @ColumnName nvarchar(max), @sql nvarchar(max)

	DECLARE @ProductCursor Cursor
	SET @ProductCursor = 
		CURSOR FAST_FORWARD 
		FOR 
			SELECT Distinct AT.AttributeDataType, AG.Title + '-' + AT.Title 
			FROM PRProductAttribute PRAT
			INNER JOIN ATAttribute AT ON AT.Id = PRAT.AttributeId
			LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on AT.Id=ag.AttributeId 

	OPEN @ProductCursor
	FETCH NEXT FROM @ProductCursor INTO @DataType, @ColumnName	
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		SET @sql = ''
		IF (@DataType) = 'SYSTEM.BOOLEAN'
		BEGIN
			SET @sql = 'Alter table PRFlatProduct Add [' + @ColumnName +'] bit'   
			exec dbo.sp_executesql @sql
		END
		IF (@DataType) = 'SYSTEM.DATETIME'
		BEGIN
			SET @sql = 'Alter table PRFlatProduct Add [' + @ColumnName +'] DateTime'   
			exec dbo.sp_executesql @sql
		END
		IF (@DataType) = 'SYSTEM.STRING'
		BEGIN
			SET @sql = 'Alter table PRFlatProduct Add [' + @ColumnName +'] nvarchar(max)'   
			exec dbo.sp_executesql @sql
		END
		IF (@DataType) = 'SYSTEM.INT16' OR (@DataType) = 'SYSTEM.INT32' OR (@DataType) = 'SYSTEM.INT64'
		BEGIN
			SET @sql = 'Alter table PRFlatProduct Add [' + @ColumnName +'] int'   
			exec dbo.sp_executesql @sql
		END
		IF (@DataType) = 'SYSTEM.DOUBLE' 
		BEGIN
			SET @sql = 'Alter table PRFlatProduct Add [' + @ColumnName +'] numeric(18,4)'   
			exec dbo.sp_executesql @sql
		END
		
		FETCH NEXT FROM @ProductCursor INTO @DataType, @ColumnName
	END
	CLOSE @ProductCursor
	DEALLOCATE @ProductCursor
END

GO
PRINT 'Modify stored procedure Product_FillFlatTable'
GO
IF(OBJECT_ID('Product_FillFlatTable') IS NOT NULL)
	DROP PROCEDURE Product_FillFlatTable
GO
CREATE PROCEDURE [dbo].[Product_FillFlatTable]
As
BEGIN
	DECLARE @ProductId uniqueidentifier, @EnumId uniqueidentifier, @Value nvarchar(max), @ColumnName nvarchar(max), @DataType nvarchar(max)
	DECLARE @sql nvarchar(max)	

	DECLARE @ProductCursor Cursor
	SET @ProductCursor = 
		CURSOR FAST_FORWARD 
		FOR 
			SELECT Distinct PRAV.ProductId, AttributeEnumId, [Value], AG.Title + '-' + AT.Title , AT.AttributeDataType
			FROM PRProductAttributeValue PRAV
			INNER JOIN ATAttribute AT ON AT.Id = PRAV.AttributeId
			LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on at.Id=ag.AttributeId 
			
	OPEN @ProductCursor
	FETCH NEXT FROM @ProductCursor INTO @ProductId, @EnumId, @Value, @ColumnName, @DataType 
	WHILE @@FETCH_STATUS = 0 
	BEGIN

		IF (@Value ='' OR @Value is null)
		BEGIN
			SELECT @Value = [Value] FROM ATAttributeEnum WHERE Id = @EnumId
		END

		SET @sql = 'UPDATE PRFlatProduct SET [' + @ColumnName +'] = ' 
 
		IF (@DataType) = 'SYSTEM.BOOLEAN'
		BEGIN
			IF ((@Value) = 'TRUE')
				SET @sql = @sql + cast(1 as varchar(1))
			ELSE
				SET @sql = @sql + cast(1 as varchar(1))
		END
		IF (@DataType) = 'SYSTEM.DATETIME'
		BEGIN
			SET @sql = @sql + '''' + @Value + ''''		
		END
		IF (@DataType) = 'SYSTEM.STRING'
		BEGIN
			SET @sql = @sql + '''' + @Value + ''''
		END
		IF (@DataType) = 'SYSTEM.INT16' OR (@DataType) = 'SYSTEM.INT32' OR (@DataType) = 'SYSTEM.INT64'
		BEGIN
			SET @sql = @sql + @Value 	
		END
		IF (@DataType) = 'SYSTEM.DOUBLE' 
		BEGIN
			SET @sql = @sql + @Value 	
		END

		SET @sql = @sql + ' WHERE Id = ''' + Cast(@ProductId as varchar(36)) + ''''
print @sql
		exec dbo.sp_executesql @sql
		
		FETCH NEXT FROM @ProductCursor INTO @ProductId, @EnumId, @Value, @ColumnName, @DataType 
	END
	CLOSE @ProductCursor
	DEALLOCATE @ProductCursor
END


GO

GO
PRINT 'Modify stored procedure Site_CreateCommerceDefaultData'
GO
IF(OBJECT_ID('Site_CreateCommerceDefaultData') IS NOT NULL)
	DROP PROCEDURE Site_CreateCommerceDefaultData
GO
CREATE Procedure [dbo].[Site_CreateCommerceDefaultData](@ApplicationId uniqueidentifier, @SiteTitle Varchar(1024), @CreatedById uniqueidentifier)
As
BEGIN

	DECLARE @CommerceProductId uniqueidentifier 
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'
	DECLARE @Now DateTime, @maxId int 
	DECLARE @maxSecurityId int
	
	SET @Now = GETUTCDATE()

	-- Product Category
	IF NOT EXISTS(SELECT * FROM PRCategory WHERE Title ='Unassigned')
	BEGIN
		INSERT INTO PRCategory (Id, Title, Description, Sequence, CreatedDate, Status)
		Values('6988B7A8-7C72-4B58-9B68-258B940E2363', 'Unassigned', 'Unassigned', 1, @Now, 1)
	END

	--Insert default value for order number
	IF NOT EXISTS(SELECT * FROM GLIdentity WHERE Name ='OrderNumber' )
	BEGIN
		INSERT INTO GLIdentity (Name,Value) Values ('OrderNumber', 0)
	END

	-- Product Import Config - Did not take it.

	-- Coupon Type
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('D1CA3949-EBFA-4DEC-B120-07DAA8961F81',	'% off Order','',		1,	1,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('2C24DC1C-268E-4ACB-933C-618DEF52E434',	'$ off Order',	'',	1,	2,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('C4FE5439-0B9F-4B41-9221-0302C527DEAE',	'$ off Shipping',	'',	1,	32,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('894D41B5-27D9-4C3B-885D-18FFC565DB8B',	'% off Shipping',	'',	1,	4,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='Flat rate shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('004202FA-B429-4AB7-A59F-ED37C74C0421',	'Flat rate shipping','',		1,	5,	1)
	END

	--Customer Group.sql
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Gold')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Gold',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Silver')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Silver',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Bronze')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Bronze',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Frequent Customer')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Frequent Customer',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='B2B')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'B2B',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Friends & Family')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Friends & Family',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Employees')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Employees',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	-- EveryOne Group - We are not using this group as of now
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Everyone')
	BEGIN
		INSERT INTO USGroup (ProductId, ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, 
		ModifiedBy, ModifiedDate, Status, ExpiryDate, GroupType)
		Values
		(@CommerceProductId,	@ApplicationId,	
		NEWID(),	'Everyone',	'everyone',	
		@CreatedById,	@Now,
		@CreatedById,	@Now,	1,	NULL,	2)
	END

	--- Security Level
	SELECT @maxSecurityId = Isnull(Max(Id),0) + 1 FROM USSecurityLevel

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Commerce Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId, 'Commerce Customer', 'Commerce Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Preferred Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+1, 'Preferred Customer', 'Preferred Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Employee')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+2, 'Employee', 'Employee', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Press')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+3, 'Press', 'Press', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	-- Feature Type
	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto Top Seller')
	BEGIN
		Insert into MRFeatureType Values(1,'Auto Top Seller')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto New Products')
	BEGIN
		Insert into MRFeatureType Values(2,'Auto New Products')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Manual')
	BEGIN
		Insert into MRFeatureType Values(3,'Manual')
	END

	-- Feature
	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Top Sellers' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Top Sellers' , 'Top Sellers Auto', 1, 1, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='New Products' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'New Products' , 'New Products Auto', 2,  2, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Moving Brand' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Moving Brand' , 'Moving Brand Manual', 3, 3, 1, newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Stock Clearance' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Stock Clearance' , 'Stock Clearance Manual', 3,  4, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Christmas Sales' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Christmas Sales' , 'Christmas Sales Manual', 3,  5, 2,newId(), GetUTCDate())

	-- AttributeDataType
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Image Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Image Library', 11)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Date' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.DateTime','Date', 5)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Decimal' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Double','Decimal', 3)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Asset Library File' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Asset Library File', 10)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Page Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Page Library', 8)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='HTML' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','HTML', 6)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='File Upload' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','File Upload', 7)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Integer' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Int32','Integer', 2)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='String' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','String', 1)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Boolean')  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Boolean','Boolean', 4)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Content Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Content Library', 9)  END

	-- CommerceStatus 
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Active' )  BEGIN   INSERT INTO CMCommerceStatus values(1,'Active','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Inactive' )  BEGIN   INSERT INTO CMCommerceStatus values(2,'Inactive','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Deleted' )  BEGIN   INSERT INTO CMCommerceStatus values(3,'Deleted','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Approved' )  BEGIN   INSERT INTO CMCommerceStatus values(4,'Approved','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Rejected' )  BEGIN   INSERT INTO CMCommerceStatus values(5,'Rejected','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='PendingForApproval' )  BEGIN   INSERT INTO CMCommerceStatus values(6,'PendingForApproval','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Included' )  BEGIN   INSERT INTO CMCommerceStatus values(7,'Included','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Excluded')  BEGIN   INSERT INTO CMCommerceStatus values(8,'Excluded','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Expired')  BEGIN   INSERT INTO CMCommerceStatus values(9,'Expired','')  END

	-- GLReason
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Damaged')  BEGIN   INSERT INTO GLReason values(NEWID(),'Damaged',210,4,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Restocking')  BEGIN   INSERT INTO GLReason values(NEWID(),'Restocking',210,1,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Bad Inventory')  BEGIN   INSERT INTO GLReason values(NEWID(),'Bad Inventory',210,2,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Returned Item')  BEGIN   INSERT INTO GLReason values(NEWID(),'Returned Item',210,8,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Lost')  BEGIN   INSERT INTO GLReason values(NEWID(),'Lost',210,3,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Physical Count')  BEGIN   INSERT INTO GLReason values(NEWID(),'Physical Count',210,6,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='New Shipment')  BEGIN   INSERT INTO GLReason values(NEWID(),'New Shipment',210,5,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Other')  BEGIN   INSERT INTO GLReason values(NEWID(),'Other',210,7,1)  END

	-- GLAddress
	IF NOT EXISTS (SELECT * FROM GLAddress Where Id = 'A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F')
	BEGIN
		INSERT INTO  GLAddress(
		Id, FirstName, LastName, AddressType, AddressLine1, AddressLine2, AddressLine3, City, StateId, Zip, 
		CountryId, Phone, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, StateName, County)
		Values
		('A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F',
		'Warehouse','Location',0,'Your warehouse address', 250,	NULL	,'City',
		'7E07B39B-BC66-4969-B16A-C62E41043E39',	'30092',	'AD72C013-B99D-4513-A666-A128343CCFF0',	'7819955555',
		'2011-08-01 11:18:32.880',	'E131010D-BE50-4526-B343-B0954E924080',	'2011-08-01 11:18:32.880',	
		'E131010D-BE50-4526-B343-B0954E924080',	1,	'MA','County')	
	END

	-- INWarehouse
	Declare @WarehouseId uniqueidentifier
	DECLARE @CopyWarehouseId uniqueidentifier
		
	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId Where Title = 'Default Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Default Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
			Values(@WarehouseId,	'Default warehouse',	
			'Default warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
			null,	1, 'Internal', 'InternalWarehouseProvider', 1,1)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
		from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
		Where T.IsDownloadableMedia =0 OR T.IsDownloadableMedia is null

	END

	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId  Where Title = 'Digital Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Digital Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
		Values(@WarehouseId,	'Digital Warehouse',	
		'Internal Digital Warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
		Null,	0, Null, 'DigitalWarehouseProvider', 2,0)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
			from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
			Where T.IsDownloadableMedia =1 
	END

	-- Product Type
	--IF NOT EXISTS (SELECT * FROM PRProductType Where SiteId = @ApplicationId)
	IF ((SELECT COUNT(*) FROM PRProductType)=0)
	BEGIN
		Insert into PRProductType(Id,ParentId,LftValue,RgtValue,Title, Description, SiteId, CreatedDate, CreatedBy, Status)
		Values(newId(),@ApplicationId,1,2,@SiteTitle, @SiteTitle ,@ApplicationId, @Now,@CreatedById ,1 )
	END

	-- ORShipper
	DECLARE @Shipper_USPS uniqueidentifier, @Shipper_UPS uniqueidentifier, @Shipper_Fedex uniqueidentifier

	IF EXISTS (SELECT * FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_USPS = (SELECT Id FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId)
	END	
	ELSE
	BEGIN
		SET @Shipper_USPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_USPS,'USPS','USPS','','iAppsUSPSProvider',1,3,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END	
	IF EXISTS (SELECT * FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_UPS = (SELECT Id FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_UPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_UPS,'UPS','UPS','http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&loc=en_US&Requester=UPSHome&tracknum={0}&AgreeToTermsAndConditions=yes&ignore=&track.x=46&track.y=7',
		'iAppsUpsProvider',1,2,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END
	IF EXISTS (SELECT * FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_Fedex = (SELECT Id FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_Fedex = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_Fedex,'Fedex','Fedex','','provider',1,1,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END

	 
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpressSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpressSM',
		'Worldwide ExpressSM','',5,0,0,0,0,1,'07',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
		
	END	
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air Saver','Next Day Air Saver',
		'customid',1,5,0,0,1,0,'13',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='UPS Next Day Air Early A.M. SM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'UPS Next Day Air Early A.M. SM','UPS Next Day Air� Early A.M. SM',
		'',1,0,0,0,0,0,'14',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Next Flight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Next Flight',
		'','',5,0,0,0,0,1,'INTERNATIONAL_FIRST',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Standard','Standard','',8,0,0,0,0,1,'11',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air A.M' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air A.M','Second Day Air A.M','',2,0,0,0,0,0,'59',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First-Class Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'First-Class Mail','1st Class','customid',5,5,0,0,1,0,'0',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International  Economy' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International  Economy','','',7,0,0,0,0,1,'INTERNATIONAL_ECONOMY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Saver','Saver','',4,0,0,0,0,0,'65',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day A.M.' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day A.M.','','',0,0,0,0,0,0,'FEDEX_2_DAY_AM',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpeditedSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpeditedSM','Worldwide ExpeditedSM','',4,0,0,0,0,1,'08',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Three-Day Select' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Three-Day Select','UPS Three-Day Select','customid',3,5,0,0,1,0,'12',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air','UPS Second Day Air','customid',2,5,0,0,1,0,'02',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Priority Overnight','','',1,0,0,0,0,1,'PRIORITY_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Priority' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Priority','','',0,0,0,0,0,1,'INTERNATIONAL_PRIORITY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Priority Mail','Priority Mail','customid',2,5,0,0,1,0,'1',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Express Saver','','',3,0,0,0,0,0,'FEDEX_EXPRESS_SAVER',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'First Overnight','Coll�ge dOccitanie','',4,0,0,0,0,1,'FIRST_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Home Delivery' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Home Delivery','','customid',2,5,0,0,1,0,'GROUND_HOME_DELIVERY',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air','UPS Next Day Air','customid',1,5,0,0,1,0,'01',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide Express PlusSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide Express PlusSM','Worldwide Express PlusSM','',4,0,0,0,0,1,'54',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day','','',5,0,0,0,0,0,'FEDEX_2_DAY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Standard Overnight','','',4,0,0,0,0,0,'STANDARD_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Express','Express','customid',1,5,0,0,1,0,'2',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Ground' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Ground','UPS Ground','customid',5,5,0,0,1,0,'03',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	
	
	
	-- ATAttributeGroup
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='01614e00-86f8-4448-9a48-7daa63171c38')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'01614e00-86f8-4448-9a48-7daa63171c38', N'Features', @ApplicationId, 1, @CreatedById, @Now, @CreatedById, @Now , 1)
	END	
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='b23ffc1b-ab9d-4985-beb4-a5f303b9ef41')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'SKULevel Attributes', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Advanced Details', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='AB730D27-5CCD-49D9-B515-168290438E0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'AB730D27-5CCD-49D9-B515-168290438E0A', N'Product Properties', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='3C211CF2-A184-4375-A078-242F4387D1D2')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'3C211CF2-A184-4375-A078-242F4387D1D2', N'Customer Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='9DABE135-467C-459E-ABF2-5FBD64821989')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'9DABE135-467C-459E-ABF2-5FBD64821989', N'Order Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A', N'Advanced Details', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END


	--IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f4963a96-57dc-4c67-830e-f7426539c8ce')
	--INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	--[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	--VALUES (N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Unlimited Quantity', N'', 1, 
	--NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f68ceb85-922b-4023-a6f9-20025f7fdc94')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'System.String',  N'Others', 
	NULL, 0, N'AT1', 1, 1, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='39C2D877-525F-49A6-9E48-D3296989F83B')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('39C2D877-525F-49A6-9E48-D3296989F83B','System.String','Product Name',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='ECB9E9D1-FC3F-43A5-B40F-3840212F693C')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('ECB9E9D1-FC3F-43A5-B40F-3840212F693C','System.Double','ListPrice',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='a0c6381b-2970-4d09-a582-334b93c51442')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'a0c6381b-2970-4d09-a582-334b93c51442', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'true', N'0', 0, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='691ee3cd-58ff-42d9-a8e8-385e85648cfc')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'691ee3cd-58ff-42d9-a8e8-385e85648cfc', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'false', N'1', 2, N'false', 0, 2, @CreatedById, @Now, @CreatedById, @Now, 1)


	--- CLImageType
	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='396b425c-e197-4f2d-a4a3-25448ca366f8')
	INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('396b425c-e197-4f2d-a4a3-25448ca366f8', N'main', 600, 800, null, N'alt_mainImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 1)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='63cf2418-684a-4d1f-88b2-34968c6f139f')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('63cf2418-684a-4d1f-88b2-34968c6f139f', N'system_thumb', 60, 60, N'systhumb_', null, 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 0)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='2770534e-4675-4764-a620-56776398e03e')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('2770534e-4675-4764-a620-56776398e03e', N'preview', 260, 372, N'prev_', N'alt_PrevImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 3)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='e02ed0f3-1271-4218-b81e-816e458434a1')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('e02ed0f3-1271-4218-b81e-816e458434a1', N'mini', 80, 80, N'mini_', N'alt_miniImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 4)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='ba9b3922-4f9f-4b21-a5cf-e03b26b588ad')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('ba9b3922-4f9f-4b21-a5cf-e03b26b588ad', N'thumbnail', 120, 120, N'thumb_', N'alt_thumbImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 2)

	--OROrderStatus
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='OK')  BEGIN   INSERT INTO OROrderStatus values(1,'OK','Submitted',1,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Shipped')  BEGIN   INSERT INTO OROrderStatus values(2,'Shipped','Shipped',2,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Canceled')  BEGIN   INSERT INTO OROrderStatus values(3,'Canceled','Canceled',3,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Back Ordered')  BEGIN   INSERT INTO OROrderStatus values(4,'Back Ordered','Back Ordered',4,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Approval')  BEGIN   INSERT INTO OROrderStatus values(5,'Pending Approval','On Hold',5,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Customer Registration Approval')  BEGIN   INSERT INTO OROrderStatus values(6,'Pending Customer Registration Approval','Pending Customer Registration Approval',6,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Bad Customer Order Approval')  BEGIN   INSERT INTO OROrderStatus values(7,'Pending Bad Customer Order Approval','Pending Bad Customer Order Approval',7,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Terms Limit')  BEGIN   INSERT INTO OROrderStatus values(8,'Exceeded Terms Limit','Exceeded Terms Limit',8,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Line of Credit')  BEGIN   INSERT INTO OROrderStatus values(9,'Exceeded Line of Credit','Exceeded Line of Credit',9,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Manual Hold')  BEGIN   INSERT INTO OROrderStatus values(10,'Manual Hold','Manual Hold',10,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Partially Shipped')  BEGIN   INSERT INTO OROrderStatus values(11,'Partially Shipped','Partially Shipped',11,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Created')  BEGIN   INSERT INTO OROrderStatus values(12,'Created','Created',12,0)  END

	-- OROrderNextStatus
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderNextStatus values(1,1,10)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderNextStatus values(2,1,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderNextStatus values(3,7,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderNextStatus values(4,7,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderNextStatus values(5,10,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderNextStatus values(6,10,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderNextStatus values(7,5,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =8)  BEGIN   INSERT INTO OROrderNextStatus values(8,5,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =9)  BEGIN   INSERT INTO OROrderNextStatus values(9,6,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =10)  BEGIN   INSERT INTO OROrderNextStatus values(10,6,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =11)  BEGIN   INSERT INTO OROrderNextStatus values(11,12,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =12)  BEGIN   INSERT INTO OROrderNextStatus values(12,11,2)  END

	-- OROrderType
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =1)  BEGIN   INSERT INTO OROrderType values(1,'Web','Web Order',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =2)  BEGIN   INSERT INTO OROrderType values(2,'CSR','Web Service Order',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =3)  BEGIN   INSERT INTO OROrderType values(3,'Brochure Request','Brochure Request',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =4)  BEGIN   INSERT INTO OROrderType values(4,'Recurring Order','Recurring ORder',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =5)  BEGIN   INSERT INTO OROrderType values(5,'Exchange Order','Exchange Order',5)  END

	-- OROrderItemStatus
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderItemStatus values(1,'Ok','Submitted',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderItemStatus values(2,'Shipped','Shipped',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderItemStatus values(3,'Cancelled','Cancelled',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderItemStatus values(4,'Backordered','Backordered',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderItemStatus values(5,'Pending','Pending',5)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderItemStatus values(6,'Released','Released',6)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderItemStatus values(7,'Partially Shipped','Partially Shipped',7)  END

	-- PTCreditCardType
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE Id ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487','American Express','American Express',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='28091FD7-00C2-4E71-A266-7FA718658A66')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('28091FD7-00C2-4E71-A266-7FA718658A66','MasterCard','MasterCard',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='A98F6177-90A5-420D-96B5-A234778F8D96')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('A98F6177-90A5-420D-96B5-A234778F8D96','Visa','Visa',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E','Discover','Discover',@CreatedById, @Now, @CreatedById, @Now)  
	END

	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE CreditCardTypeId ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487' And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='28091FD7-00C2-4E71-A266-7FA718658A66'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('28091FD7-00C2-4E71-A266-7FA718658A66',@ApplicationId,1) 
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='A98F6177-90A5-420D-96B5-A234778F8D96'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('A98F6177-90A5-420D-96B5-A234778F8D96',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E',@ApplicationId,1)  
	END

	-- PTGateway
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =1)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(1,'Website Payments Pro','Website Payments Pro',2,'WebsitePaymentsProGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =2)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(2,'None','None',1,'AutoGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =3)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(3,'Cybersource','Cybersource',3,'CybersourceGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =4)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description],  [Sequence], [DotNetProviderName]) 
		Values(4,'Payflow Pro','Payflow Pro',4,'PayflowProGatewayProvider') 
	END

	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =1)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(1,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =2)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(2,0,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =3)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(3,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =4)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(4,0,1,@ApplicationId) 
	END
	
	-- PTPaymentStatus
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =1)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(1,'Entered','Entered',1,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =2)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(2,'Auth','Authorized',2,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =3)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(3,'Captured','Captured',3,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =4)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(4,'Auth Failed','Authorization Failed',4,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =5)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(5,'Capture Failed','Capture Failed',5,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =6)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(6,'Clear','Clear',6,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =7)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(7,'Manual Cancel','Manual Cancel',7,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =8)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(8,'Partially Captured','Partially Captured',8,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =9)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(9,'Void','Void',9,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =10)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(10,'Refund Entered','Refund Entered',10,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =11)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(11,'Refund Waiting Approval','Refund Waiting Approval',11,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =12)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(12,'Refunded','Refunded',12,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =13)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(13,'Refund Failed','Refund Failed',13,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =14)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(14,'Refund Rejected','Refund Rejected',14,1)  END

	-- PTPaymentType
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =1)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(1,'CreditCard','Credit Card',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =2)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(2,'OnAccount','OnAccount',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =3)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(3,'Paypal','Paypal',5)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =4)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(4,'COD','Cash On Delivery',4)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =5)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(5,'GiftCard','Gift Card',2)  END

	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =1)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(1,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =2)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(2,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =3)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(3,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =4)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(4,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =5)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(5,@ApplicationId,1)  END

	IF (SELECT Count(*) FROM ORReturnReason)=0
	BEGIN

		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES( 
			'Defective')
			
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(  
			'Customer Ordered Wrong Product')
		    
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES('Refused Delivery')


		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Missing Merchandise')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Did not Like')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Changed Mind')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Billing Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Fraudulent Order')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Warehouse Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Returned by Shipper - No Response From Customer')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Other')

	END

	-- INWarehouseType
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =1)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(1,'Internal','iAPPS internal warehouse for Non-Digital Products')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =2)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(2,'Digital','iAPPS Digital warehouse')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =3)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(3,'External','Integrated with external system')  END

	-- INRestockingStatus
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Created'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (1,'Created','Created',1)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Queued'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (2,'Queued','Queued',2)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='In Progress'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (4,'In Progress','In Progress',4)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Canceled'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (3,'Canceled','Canceled',3)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Completed'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (5,'Completed','Completed',5)

	-- ORReturnResolutionType
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] ON
	Select @maxId =Isnull(MAX(Id),0) FROM ORReturnResolutionType
	SET @maxId = @maxId + 1

	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Refund'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Refund','Part or full paymnent is returned', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Replacement'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Replacement','Replacement', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Others'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Others','Others', @maxId)        
		SET @maxId = @maxId + 1
	END
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] OFF

	-- INInventory
	IF(NOT EXISTS(SELECT 1 FROM dbo.INInventory WHERE WarehouseId ='22FAC4BD-4954-415A-9014-3598DAEAB4D0'))  
	BEGIN   
		Declare @Id UniqueIdentifier
		Set @Id= '22FAC4BD-4954-415A-9014-3598DAEAB4D0'

		INSERT INTO [INInventory]
				   ([Id]
				   ,[WarehouseId]
				   ,[SKUId]
				   ,[Quantity]
				   ,[LowQuantityAlert]
				   ,[FirstTimeOrderMinimum]
				   ,[OrderMinimum]
				   ,[OrderIncrement]
				   ,[ReasonId]
				   ,[ReasonDetail]
				   ,[Status]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate])
					SELECT newId()
					  ,@Id
					  ,Id
					  ,0
					  ,0
					  ,1
					  ,1
					  ,1
					  ,(SELECT Id FROM GLReason Where Title ='Other') As ReasonId
					  ,'Initial Data When SKU is created'
					  ,1
					  ,@CreatedById
					  ,@Now
					  ,@CreatedById
					  ,@Now
				   FROM PRProductSKU WHERE Id NOT IN 
					(SELECT SKUID FROM ININventory WHERE WarehouseId = @Id)
	END
	

	DECLARE @GroupObjectTypeId int
	DECLARE @MerchandisingRoleId int, @FulfillmentRoleId int, @MarketingRoleId int, @CSRRoleId int, 
		@EveryOneRoleId int, @CSRManagerRoleId int, @WHManagerRoleId int
	DECLARE @MerchandisingGroupId uniqueidentifier, @FulfillmentGroupId uniqueidentifier, 
		@MarketingGroupId uniqueidentifier, @CSRGroupId uniqueidentifier, @EveryOneGroupId uniqueidentifier, 
		@CSRManagerGroupId uniqueidentifier, @WHManagerGroupId uniqueidentifier

	SET @GroupObjectTypeId = 15 
	SET @MerchandisingRoleId = 19
	SET @FulfillmentRoleId = 20
	SET @MarketingRoleId = 21
	SET @CSRRoleId = 22
	SET @EveryOneRoleId = 29
	SET @CSRManagerRoleId = 32
	SET @WHManagerRoleId = 33	
	-- if the db has cms and default commerce data was missing 
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MerchandisingGroupId output,@Title = 'Merchandising',
			@Description = 'Merchandising',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
				
	END		
	ELSE
		SET @MerchandisingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')

	IF (@MerchandisingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MerchandisingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MerchandisingGroupId,2,@MerchandisingRoleId, @MerchandisingGroupId,1,1 )				
	END
	
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Fulfillment')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @FulfillmentGroupId output,@Title = 'Fulfillment',
			@Description = 'Fulfillment',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @FulfillmentGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Fulfillment')

	IF (@FulfillmentGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @FulfillmentGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@FulfillmentGroupId,2,@FulfillmentRoleId, @FulfillmentGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Marketing')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MarketingGroupId output,@Title = 'Marketing',
			@Description = 'Marketing',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @MarketingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Marketing')

	IF (@MarketingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MarketingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MarketingGroupId,2,@MarketingRoleId, @MarketingGroupId,1,1 )				
	END

	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSR')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRGroupId output,@Title = 'CSR',
			@Description = 'CSR',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSR')
	
	IF (@CSRGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRGroupId,2,@CSRRoleId, @CSRGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'EveryOne' And GroupType = 1)) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @EveryOneGroupId output,@Title = 'EveryOne',
			@Description = 'EveryOne',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @EveryOneGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'EveryOne')

	IF (@EveryOneGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @EveryOneGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@EveryOneGroupId,2,@EveryOneRoleId, @EveryOneGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSRManager')) 
	BEGIN	
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRManagerGroupId output,@Title = 'CSRManager',
			@Description = 'CSRManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSRManager')

	IF (@CSRManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRManagerGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRManagerGroupId,2,@CSRManagerRoleId, @CSRManagerGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'WHManager')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @WHManagerGroupId output,@Title = 'WHManager',
			@Description = 'WHManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @WHManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'WHManager')

	IF (@WHManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @WHManagerGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@WHManagerGroupId,2,@WHManagerRoleId, @WHManagerGroupId,1,1 )				
	END
			
END
GO
PRINT 'Modify stored procedure Sku_GetAttributes'
GO
IF(OBJECT_ID('Sku_GetAttributes') IS NOT NULL)
	DROP PROCEDURE Sku_GetAttributes
GO
CREATE PROCEDURE [dbo].[Sku_GetAttributes]
(
	@ProductTypeId uniqueidentifier=null,
	@SkuProductId uniqueidentifier=null,
	@SKUs xml=null,
	@ApplicationId uniqueidentifier=null
)
AS
BEGIN
IF @SKUs is not null
BEGIN 
select 
	Distinct
			A.Id,
		AG.CategoryId,
		A.AttributeDataType,
		A.AttributeDataTypeId,
		A.Title,
		A.Description,
		A.Sequence,
		A.Code,
		A.IsFaceted,
		A.IsSearched,
		A.IsSystem,
		A.IsDisplayed,
		A.IsEnum,	
		PA.IsRequired,	
		A.IsPersonalized,
		dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
		A.CreatedBy,
		dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
		A.ModifiedBy,
		A.Status,
		'True' IsSKULevel,	A.IsPersonalized		
from ATAttribute A 
INNER JOIN PRProductAttribute PA on A.Id = PA.AttributeId
INNER JOIN PRProductSKU S on PA.ProductId=S.ProductId
INNER JOIN @SKUs.nodes('/GenericCollectionOfGuid/guid')tab(col) ON S.Id=tab.col.value('text()[1]','uniqueidentifier')
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=dbo.GetActiveStatus()   AND IsSKULevel =1 AND A.IsSystem=0
END
ELSE If @SkuProductId is not null
BEGIN
select 
	Distinct
		A.Id,
		AG.CategoryId,
		A.AttributeDataType,
		A.AttributeDataTypeId,
		A.Title,
		A.Description,
		A.Sequence,
		A.Code,
		A.IsFaceted,
		A.IsSearched,
		A.IsSystem,
		A.IsDisplayed,
		A.IsEnum,	
		A.IsPersonalized,
		PA.IsRequired,	
		dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
		A.CreatedBy,
		dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
		A.ModifiedBy,
		A.Status,
		'True' IsSKULevel		, 	A.IsPersonalized	
from ATAttribute A 
INNER JOIN PRProductAttribute PA on A.Id = PA.AttributeId
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=dbo.GetActiveStatus()  AND IsSKULevel =1 AND A.IsSystem=0
AND ProductId =@SkuProductId
END
ELSE IF @ProductTypeId is not null
BEGIN 
select 
	Distinct
		A.Id,
		AG.CategoryId,
		A.AttributeDataType,
		A.AttributeDataTypeId,
		A.Title,
		A.Description,
		A.Sequence,
		A.Code,
		A.IsFaceted,
		A.IsSearched,
		A.IsSystem,
		A.IsDisplayed,
		A.IsEnum,	
		PA.IsRequired,	
		dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
		A.CreatedBy,
		dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
		A.ModifiedBy,
		A.Status,
		'True' IsSKULevel		, 	A.IsPersonalized	
from ATAttribute A 
INNER JOIN PRProductTypeAttribute PA on A.Id = PA.AttributeId
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=dbo.GetActiveStatus()  AND IsSKULevel =1 AND A.IsSystem=0
AND ProductTypeId =@ProductTypeId
END

END

GO

GO
PRINT 'Modify stored procedure [etl].[up_StagingInitalLoad]'
GO
IF(OBJECT_ID('etl.up_StagingInitalLoad') IS NOT NULL)
	DROP PROCEDURE [etl].[up_StagingInitalLoad]
GO
CREATE PROCEDURE [etl].[up_StagingInitalLoad]
as

BEGIN
Declare @SiteId uniqueidentifier
-- SET The default SiteId
   SET @SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
	TRUNCATE TABLE Staging_ProductExportConfig
    
	
	INSERT Staging_ProductExportConfig(Id, ColumnName)  -- SISite
	SELECT Id, 'Site' from SISite Where Id=@SiteId
	-- Enter default Created By
	INSERT Staging_ProductExportConfig(Id, ColumnName)	-- USUser
	SELECT Id, 'CreatedBy' from USUser where UserName = 'iAppsUser'
	-- Enter default Product Category
	INSERT Staging_ProductExportConfig(ColumnName)
	SELECT 'ProductCategory'

END -- END BEGIN



-- select * from Staging_ATAttributeGroup
BEGIN

	IF (SELECT Id from ATAttributeGroup WHERE Title = 'Export Default') IS NULL
	BEGIN
		TRUNCATE TABLE Staging_ATAttributeGroup

		INSERT Staging_ATAttributeGroup(Title, SiteId, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status)
		SELECT	
				'Export Default' as Title,
				(SELECT top 1 Id FROM SISite ) as SiteId,
				0 as IsSystem,
				getutcdate() as CreatedDate,
				(SELECT top 1 Id FROM Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy, 
				NULL as ModifiedDate,
				NULL as ModifiedBy,
				1 as Status
	END

	ELSE
	BEGIN
		TRUNCATE TABLE Staging_ATAttributeGroup
		
		INSERT Staging_ATAttributeGroup(Id, Title, SiteId, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status)
		SELECT 
			Id, Title, SiteId, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status
		FROM 
			ATAttributeGroup
		WHERE
			Title = 'Export Default'
	END
END  -- Begin



-- select * from PRProductType

BEGIN
	IF (SELECT Column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Staging_FlatFile' and Column_name like '%ProductType%') is null
	 BEGIN
	  TRUNCATE TABLE Staging_PRProductType

	  INSERT INTO Staging_PRProductType(Id, ParentId, LftValue, RgtValue, Title, Description, SiteId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, CMSPageId, FriendlyName)
	  SELECT  T.Id,ParentId, LftValue, RgtValue,
			  T.Title,  Description,
			  T.SiteId,
			  T.CreatedDate,
			  T.CreatedBy, 
			  T.ModifiedDate,
			  T.ModifiedBy,
			  T.Status,
			  CMSPageId,
			  FriendlyName
	 FROM 
			 PRProductType T
			 Inner Join PRProductTypeCMSPage PT ON T.Id = PT.ProductTypeId AND PT.SiteId =@SiteId
	 WHERE LftValue = 1

	 END
END	-- BEGIN


-- select * from Staging_PRProduct

BEGIN 
   TRUNCATE TABLE Staging_PRProduct
   INSERT INTO Staging_PRProduct(ProductIDUser, UrlFriendlyTitle, Title, ShortDescription, LongDescription, [Description], Keyword, IsActive, ProductTypeID, ProductStyle, SiteId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, IsBundle, BundleCompositionLastModified)
   SELECT	DISTINCT
			ProductID as  ProductIDUser, 
			'' as UrlFriendlyTitle, --ISNULL(SEOFriendlyURL, '') as UrlFriendlyTitle, -- getting error
			ISNULL(Name, 'Unknown') as Title, -- TODO: throw out these records? (DM) 
			ISNULL(ShortDescription,''), 
			ISNULL(LongDescription, ''), 
            ISNULL(ShortDescription,'') as Description, 
			ISNULL(Keyword,''), 
			IsActive as IsActive, 
			(SELECT Id from Staging_PRProductType) as ProductTypeID, 
			NULL as ProductStyle, 
			(SELECT top 1 Id from Staging_ProductExportConfig WHERE ColumnName = 'Site') as SiteId,  
			CreateDate as CreatedDate,
			(SELECT top 1 Id from Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy, 
			ChangedDate as ModifiedDate,
			NULL as ModifiedBy, 
			1 as Status,
			--IsBundle as IsBundle,		-- TODO: swap this back in once we have an export file format that links bundle items to parent products
			0 as IsBundle,
			NULL as BundleCompositionLastModified

   FROM
      Staging_FlatFile
	WHERE Name is not null and ProductID is not null -- TODO: exclude products without names or IDs (DM)
		--and IsBundle=0	--TODO: exclude bundles for now (DM)
END  -- BEGIN


-- SELECT * FROM Staging_PRProductSKU
BEGIN
   TRUNCATE TABLE Staging_PRProductSKU

   INSERT INTO Staging_PRProductSKU(ProductId, SiteId, Title, SKU, Description, Sequence, IsActive, IsOnline, ListPrice, UnitCost, WholesalePrice, Quantity, PreviousSoldCount, 
                                    CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, Measure, OrderMinimum, OrderIncrement, HandlingCharges, ProductIDUser)
   SELECT 
     b.Id as ProductId,
    (SELECT top 1 Id FROM Staging_ProductExportConfig WHERE ColumnName = 'Site') as SiteId,
	 Title,
     SKU, -- TODO: probably should just exclude anything without a SKU? (DM)
     ISNULL(a.[Description],''),
     0 as Sequence,
     1 as IsActive,
     IsOnline as Online,
	 ISNULL(ListPrice,0),
	 ISNULL(UnitCost,0),
	 0 as WholesalePRice,
	 Quantity as Quantity,
     0 as PreviousSoldCount,
	 CreateDate as CreatedDate,
     (SELECT top 1 Id from Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy,
	 ChangedDate as Modifieddate,
	 Null as ModifiedBy,
	 1 as Status,
	 UnitOfMeasure as Measure,
	 ISNULL(OrderQuantityMinimum,0) as OrderMinimum,
	 ISNULL(OrderQuantityIncrement,1) as OrderIncrement,
	 Handling as HandlingCharges,
     ProductID as ProductIDUser
   FROM 
     Staging_FlatFile a INNER JOIN
     Staging_PRProduct b 
     ON
       a.ProductID = b.ProductIDUser
	WHERE SKU is not null	-- TODO: exclude records without a SKU (DM)
   

END

-- select * from  Staging_SKUProductAttributeValues
BEGIN 
	IF EXISTS (SELECT 1
		FROM INFORMATION_SCHEMA.TABLES 
		WHERE TABLE_TYPE='BASE TABLE' 
		AND TABLE_NAME='Staging_SKUProductAttributeValues') 
		DROP TABLE Staging_SKUProductAttributeValues

		

	DECLARE @Empstring nvarchar(max), @delimiter char , @sql nvarchar(max)
	SET @delimiter = ',' 
	SELECT @Empstring = COALESCE(@Empstring  + @delimiter, '') + '[' + column_name + ']' FROM INFORMATION_SCHEMA.COLUMNS where Table_name = 'Staging_FlatFile' and column_name like 'ProductSKU-%'


	SELECT @sql = 'SELECT ProductID, SKU, SUBSTRING(AttributeName, 12, len(AttributeName)) as SKUAttributeName, Orders ' +
				  'INTO Staging_SKUProductAttributeValues ' +
				  'FROM ' +
				  '(SELECT ProductID, SKU, ' +
				  @Empstring +
				  ' FROM Staging_FlatFile) p ' +
				  'UNPIVOT ' +
				  '   (Orders FOR AttributeName IN (' +
				  @Empstring +
				  '))AS unpvt '
	exec (@sql)
END  -- BEGIN

-- select * from  Staging_ProductAttributeValues
BEGIN
	IF EXISTS (SELECT 1
		FROM INFORMATION_SCHEMA.TABLES 
		WHERE TABLE_TYPE='BASE TABLE' 
		AND TABLE_NAME='Staging_ProductAttributeValues') 
		DROP TABLE Staging_ProductAttributeValues

		

	DECLARE @Empstring2 nvarchar(max), @delimiter2 char , @sql2 nvarchar(max)
	SET @delimiter2 = ',' 
	SELECT @Empstring2 = COALESCE(@Empstring2  + @delimiter2, '') + '[' + column_name + ']' FROM INFORMATION_SCHEMA.COLUMNS where Table_name = 'Staging_FlatFile' and column_name like 'Product-%'

	SELECT @sql2 = 'SELECT ProductID, SUBSTRING(AttributeName, 9, len(AttributeName)) as AttributeName, Orders ' +
				  'INTO Staging_ProductAttributeValues ' +
				  'FROM ' +
				  '(SELECT ProductID, ' +
				  @Empstring2 +
				  ' FROM Staging_FlatFile) p ' +
				  'UNPIVOT ' +
				  '   (Orders FOR AttributeName IN (' +
				  @Empstring2 +
				  '))AS unpvt '

	exec (@sql2)

END




-- Select * from Staging_ATAttribute 

-- insert dynamically created attributes
BEGIN

	TRUNCATE TABLE Staging_ATAttribute

	INSERT INTO Staging_ATAttribute(AttributeDataType,  Title, Description, Sequence, Code, IsFaceted, IsSearched, IsSystem, IsDisplayed, IsEnum, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status)
	SELECT 'System.String' as AttributeDataType,
			AA.Attribute as Title,
			'' as Description,   --- hardcoded
			0 as Sequence,			-- hardcoded
			'' as Code,			-- hardcoded
			0 as IsFaceted,
			0 as IsSearched,
			0 as IsSystem,
			1 as IsDisplayed,
			0 as IsEnum,
			getutcdate() as CreatedDate,
			(SELECT top 1 Id from Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy,
			NULL as ModifiedDate,
			NULL as ModifiedBy,
			Status  

	FROM
	(select distinct SKUAttributeName AS Attribute, 1 as [Status] from Staging_SKUProductAttributeValues
	union
--	select distinct AttributeName AS Attribute, 2 as [Status] from Staging_ProductAttributeValues) as AA
	select distinct AttributeName AS Attribute, 1 as [Status] from Staging_ProductAttributeValues) as AA
END


--insert explicitly configured attributes
BEGIN
	INSERT INTO Staging_ATAttribute(AttributeDataType,  Title, Description, Sequence, Code, IsFaceted, IsSearched, IsSystem, IsDisplayed, IsEnum, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status)
	SELECT AA.DataType as AttributeDataType,
			AA.Attribute as Title,
			'' as Description,   --- hardcoded
			0 as Sequence,			-- hardcoded
			'' as Code,			-- hardcoded
			0 as IsFaceted,
			0 as IsSearched,
			0 as IsSystem,
			1 as IsDisplayed,
			0 as IsEnum,
			getutcdate() as CreatedDate,
			(SELECT top 1 Id from Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy,
			NULL as ModifiedDate,
			NULL as ModifiedBy,
			Status  

	FROM
	(select distinct AttributeName AS Attribute, DataType, 1 as [Status] from Staging_ProductExportAttributeConfig) as AA
END


--insert explicitly configured attribute values for SKUs
BEGIN

	IF EXISTS (SELECT 1
		FROM INFORMATION_SCHEMA.TABLES 
		WHERE TABLE_TYPE='BASE TABLE' 
		AND TABLE_NAME='Staging_ProductSKUAttributeValue_Explicit') 
		DROP TABLE Staging_ProductSKUAttributeValue_Explicit

	IF EXISTS (SELECT top 1 ColumnName from dbo.Staging_ProductExportAttributeConfig where SkuLevel=1)
	
		BEGIN
			DECLARE @Empstring3 nvarchar(max), @delimiter3 char , @sql3 nvarchar(max)
			SET @delimiter3 = ',' 
			SELECT @Empstring3 = COALESCE(@Empstring3  + @delimiter3, '') + '[' + column_name + ']' FROM INFORMATION_SCHEMA.COLUMNS where Table_name = 'Staging_FlatFile' and COLUMN_NAME COLLATE DATABASE_DEFAULT in (select ColumnName from dbo.Staging_ProductExportAttributeConfig where SkuLevel=1)

			SELECT @sql3 = 'SELECT ProductID, SKU, AttributeName as AttributeName, Orders ' +
						  'INTO Staging_ProductSKUAttributeValue_Explicit ' +
						  'FROM ' +
						  '(SELECT ProductID, SKU, ' +
						  @Empstring3 +
						  ' FROM Staging_FlatFile) p ' +
						  'UNPIVOT ' +
						  '   (Orders FOR AttributeName IN (' +
						  @Empstring3 +
						  '))AS unpvt '

			exec (@sql3)
		END

END


--insert explicitly configured attribute values for Products
BEGIN

	IF EXISTS (SELECT 1
		FROM INFORMATION_SCHEMA.TABLES 
		WHERE TABLE_TYPE='BASE TABLE' 
		AND TABLE_NAME='Staging_ProductAttributeValue_Explicit') 
		DROP TABLE Staging_ProductAttributeValue_Explicit

	IF EXISTS (SELECT top 1 ColumnName from dbo.Staging_ProductExportAttributeConfig where SkuLevel=0)
		
		BEGIN
			DECLARE @Empstring4 nvarchar(max), @delimiter4 char , @sql4 nvarchar(max)
			SET @Empstring4 = ',' 
			SELECT @Empstring4 = COALESCE(@Empstring4  + @delimiter4, '') + '[' + column_name + ']' FROM INFORMATION_SCHEMA.COLUMNS where Table_name = 'Staging_FlatFile' and COLUMN_NAME COLLATE DATABASE_DEFAULT in (select ColumnName from dbo.Staging_ProductExportAttributeConfig where SkuLevel=0)

			SELECT @sql4 = 'SELECT ProductID, SKU, AttributeName as AttributeName, Orders ' +
						  'INTO Staging_ProductAttributeValue_Explicit ' +
						  'FROM ' +
						  '(SELECT ProductID, SKU, ' +
						  @Empstring4 +
						  ' FROM Staging_FlatFile) p ' +
						  'UNPIVOT ' +
						  '   (Orders FOR AttributeName IN (' +
						  @Empstring4 +
						  '))AS unpvt '

			exec (@sql4)
		END

END




--select * from Staging_PRProductAttribute
BEGIN

	TRUNCATE TABLE Staging_PRProductAttribute

	INSERT INTO Staging_PRProductAttribute (ProductId, AttributeId, Sequence, IsSKULevel)
	SELECT DISTINCT
	   b.Id as ProductId, -- ProductID
	   c.Id as AttributeId, -- AttributeID
	   0 as Sequence,    -- Find out about Sequence
	   0 as IsSKULevel    -- Find out about ISSKULevel
	FROM
		Staging_ProductAttributeValues a 
	    INNER JOIN
	   Staging_PRProduct b ON
		a.ProductID   = b.ProductIDUser
	   INNER JOIN Staging_ATAttribute c ON
		a.AttributeName = c.Title
	WHERE
		a.Orders <> 'NULL'

	UNION

	SELECT DISTINCT
	   b.Id as ProductId, -- ProductID
	   c.Id as AttributeId, -- AttributeID
	   0 as Sequence,    -- Find out about Sequence
	   0 as IsSKULevel    -- Find out about ISSKULevel
	FROM
		Staging_SKUProductAttributeValues a
	    INNER JOIN
	   Staging_PRProduct b ON
		a.ProductID   = b.ProductIDUser
	   INNER JOIN Staging_ATAttribute c ON
		a.SKUAttributeName = c.Title
	WHERE
		a.Orders <> 'NULL'

	UNION 

	SELECT DISTINCT
	   b.Id as ProductId, -- ProductID
	   c.Id as AttributeId, -- AttributeID
	   0 as Sequence,    -- Find out about Sequence
	   0 as IsSKULevel    -- Find out about ISSKULevel
	FROM
		Staging_ProductSKUAttributeValue_Explicit a
	    INNER JOIN
	   Staging_PRProduct b ON
		a.ProductID   = b.ProductIDUser
	   INNER JOIN Staging_ATAttribute c ON
		a.AttributeName = c.Title
	WHERE
		a.Orders <> 'NULL' or a.Orders is not null
		
	UNION 

	SELECT DISTINCT
	   b.Id as ProductId, -- ProductID
	   c.Id as AttributeId, -- AttributeID
	   0 as Sequence,    -- Find out about Sequence
	   0 as IsSKULevel    -- Find out about ISSKULevel
	FROM
		Staging_ProductAttributeValue_Explicit a
	    INNER JOIN
	   Staging_PRProduct b ON
		a.ProductID   = b.ProductIDUser
	   INNER JOIN Staging_ATAttribute c ON
		a.AttributeName = c.Title
	WHERE
		a.Orders <> 'NULL' or a.Orders is not null


END  -- BEGIN



BEGIN
  
  TRUNCATE TABLE Staging_PRProductAttributeValue

  INSERT INTO Staging_PRProductAttributeValue(ProductAttributeId, ProductId, AttributeId, AttributeEnumId, Value, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status)
  SELECT DISTINCT 
		d.Id as ProductAttributeId,
		b.Id as ProductId, 
		c.Id as AttributeId, 
		NULL AS AttributeEnumId, 
		Orders as Value,
		getutcdate() as CreatedDate, 
		(SELECT Id from Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy, 
		NUll as Modifieddate, 
		NULL as ModifiedBy, 1 as Status
  FROM 
   Staging_ProductAttributeValues a INNER JOIN 
   Staging_PRProduct b
   ON a.ProductID = b.ProductIDUser
   INNER JOIN Staging_ATAttribute c
   ON a.AttributeName   = c.Title
   INNER JOIN Staging_PRProductAttribute d
   ON d.ProductId = b.Id 
      AND
	  d.AttributeId = c.Id
  WHERE
   Orders <> 'NULL' OR
   Orders is NULL

	INSERT INTO Staging_PRProductAttributeValue(ProductAttributeId, ProductId, AttributeId, AttributeEnumId, Value, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status)
	SELECT d.Id as ProductAttributeId, b.ProductId , c.Id as AttributeId, NULL as AttributeEnumId, Orders as Value,
			getutcdate() as CreatedDate, 
			(SELECT top 1 Id from Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy, 
			NULL as ModifiedDate,
			NULL as Modifiedby, 
			1 as Status
		FROM 
			Staging_ProductAttributeValue_Explicit a INNER JOIN
			Staging_PRProductSKU b
            ON a.SKU = b.SKU
            INNER JOIN Staging_ATAttribute c
			ON a.AttributeName = c.Title
			INNER JOIN Staging_PRProductAttribute d
			ON b.ProductId = d.ProductId
			   AND
			  c.Id = d.AttributeId
			   
		WHERE
			Orders <> 'NULL'



END  -- BEGIN

-- SELECT * FROM Staging_PRProductSKUAttributeValue

BEGIN
	 TRUNCATE TABLE dbo.Staging_PRProductSKUAttributeValue

	 INSERT INTO Staging_PRProductSKUAttributeValue(ProductAttributeId, SKUId, AttributeId, AttributeEnumId, Value, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status)
     SELECT d.Id as ProductAttributeId, b.Id as SKUId, c.Id as AttributeId, NULL as AttributeEnumId, Orders as Value,
			getutcdate() as CreatedDate, 
			(SELECT top 1 Id from Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy, 
			NULL as ModifiedDate,
			NULL as Modifiedby, 
			1 as Status
		FROM 
			Staging_SKUProductAttributeValues a INNER JOIN
			Staging_PRProductSKU b
            ON a.SKU = b.SKU
            INNER JOIN Staging_ATAttribute c
			ON a.SKUAttributeName = c.Title
			INNER JOIN Staging_PRProductAttribute d
			ON b.ProductId = d.ProductId
			   AND
			  c.Id = d.AttributeId
			   
		WHERE
			Orders <> 'NULL'


	UNION

	SELECT d.Id as ProductAttributeId, b.Id as SKUId, c.Id as AttributeId, NULL as AttributeEnumId, Orders as Value,
			getutcdate() as CreatedDate, 
			(SELECT top 1 Id from Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy, 
			NULL as ModifiedDate,
			NULL as Modifiedby, 
			1 as Status
		FROM 
			Staging_ProductSKUAttributeValue_Explicit a INNER JOIN
			Staging_PRProductSKU b
            ON a.SKU = b.SKU
            INNER JOIN Staging_ATAttribute c
			ON a.AttributeName = c.Title
			INNER JOIN Staging_PRProductAttribute d
			ON b.ProductId = d.ProductId
			   AND
			  c.Id = d.AttributeId
			   
		WHERE
			Orders <> 'NULL'



END

	-- Update IsSKULevel depending if it is SKU
BEGIN

	UPDATE Staging_PRProductAttribute
	SET
	  IsSKULevel = 1
	FROM
	  Staging_PRProductAttribute a INNER JOIN
	  Staging_PRProductSKUAttributeValue b 
	  ON a.AttributeId = b.AttributeId
	  INNER JOIN Staging_PRProductSKU c
	  ON a.ProductId = c.ProductId
	



	UPDATE Staging_PRProductAttribute
	SET IsSKULevel = 1
	WHERE AttributeId in 
	(select A.Id from dbo.Staging_ProductExportAttributeConfig PEAC 
	inner join Staging_ATAttribute A 
	on A.Title=PEAC.AttributeName 
	where PEAC.SkuLevel=1)

END


	-- Inventory for products which have a quantity specified in the import file

BEGIN

	TRUNCATE TABLE dbo.Staging_INInventory

	INSERT INTO dbo.Staging_INInventory(Id,WarehouseId,SKUId,Quantity,LowQuantityAlert,FirstTimeOrderMinimum,OrderMinimum,OrderIncrement,ReasonId,
		ReasonDetail,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	SELECT 
		newid() as Id,
		(select top 1 Id from INWarehouse) as WarehouseId,
		PS.Id as SKUId,
		SF.Quantity as Quantity,
		0 as LowQuantityAlert,
		--ISNULL(SF.FirstTimeOrderQuantityMinimum,0) as FirstTimeOrderMinimum,
		0 as FirstTimeOrderMinimum,								-- TODO: true check for varchar null (DM)
		ISNULL(SF.OrderQuantityMinimum,0) as OrderMinimum,
		ISNULL(SF.OrderQuantityIncrement,1) as OrderIncrement,	-- TODO: default orderincrement to 1? (DM)
		null as ReasonId,
		null as ReasonDetail,
		1 as Status,
		(SELECT top 1 Id from Staging_ProductExportConfig WHERE ColumnName = 'CreatedBy') as CreatedBy, 
		getutcdate() as CreatedDate,
		null as ModifiedBy,
		null as ModifiedDate
	FROM 
		Staging_FlatFile SF INNER JOIN 
		Staging_PRProductSKU PS 
		ON SF.SKU = PS.SKU
		
END
GO
PRINT 'Modify stored procedure [etl].[up_StagingToDestination]'
GO
IF(OBJECT_ID('etl.up_StagingToDestination') IS NOT NULL)
	DROP PROCEDURE [etl].[up_StagingToDestination]
GO

CREATE PROCEDURE [etl].[up_StagingToDestination]
as

----Clean target Tables
----BEGIN


----
--
--	--	PRProductSKUAttributeValue
--			DELETE PRProductSKUAttributeValue where AttributeId IN (Select Id FROM ATAttribute where AttributeGroupId in (select Id from ATAttributeGroup where Title = 'Export Default'))
--
--	--  PRProductAttributeValue  -- count 173
--			DELETE PRProductAttributeValue WHERE AttributeId IN (Select Id FROM ATAttribute where AttributeGroupId in (select Id from ATAttributeGroup where Title = 'Export Default'))
--
--
--	--	SELECT * FROM PRProductAttribute
--			DELETE PRProductAttribute where AttributeId In (Select Id FROM ATAttribute where AttributeGroupId in (select Id from ATAttributeGroup where Title = 'Export Default'))
--		
--	--	ATAttribute
--			DELETE ATAttribute where AttributeGroupId in (select Id from ATAttributeGroup where Title = 'Export Default') -- AttributeId in PRProductSKUAttributeValue
--
--	--	DELETE ATAttributeGroup
--			DELETE ATAttributeGroup where Title = 'Export Default'	
--
--
--	--	PRProductCategory
--			DELETE PRProductCategory where CategoryId = (Select Id from PRCategory Where Title = 'Default')
--
--	--	PRCategory
--
--			DELETE PRCategory where Title = 'Default'	
--	--	PRProductSKU
--			DELETE PRProductSKU WHERE Id in (SELECT Id from Staging_PRProductSKU)
--
--	--	PRProduct
--			DELETE PRProduct WHERE Id in (Select Id FROM Staging_PRProduct)
----END

-- select * from PRCategory
BEGIN
	INSERT INTO PRCategory(Id, Title, Description, Sequence, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status)
	SELECT 
		Id as Id, 
		'Default' as Title, 
		'Default' as Description,
		1 as Sequence,
		getutcdate() as CreatedDate,
		(Select top 1 Id from Staging_ProductExportConfig Where ColumnName = 'CreatedBy') as CreatedBy,
		NULL as ModifiedDate,
		NULL as ModifiedBy,
		1 as Status
	FROM 
		Staging_ProductExportConfig
	WHERE
		ColumnName = 'ProductCategory'
END


-- select * from Staging_PRProductType
BEGIN
	-- PRProductType
	IF (SELECT Id from PRProductType WHERE Id in (select Id from Staging_PRProductType)) IS NULL
	BEGIN
		INSERT INTO PRProductType(Id, ParentId, LftValue, RgtValue, Title, Description, SiteId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, FriendlyName)
		SELECT Id, ParentId, LftValue, RgtValue, Title, Description, SiteId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, FriendlyName
		FROM dbo.Staging_PRProductType
		
		INSERT INTO PRProductTypeCMSPage(Id,SiteId,ProductTypeId,CMSPageId,CreatedBy,CreatedDate)
		select NEWID(),SiteId,Id,CMSPageId,CreatedBy,CreatedDate
		FROM dbo.Staging_PRProductType
	END
    

	--select * from PRProduct 
	INSERT INTO PRProduct (Id, ProductIDUser, Title, UrlFriendlyTitle, ShortDescription, LongDescription, Description, Keyword, IsActive, 
						   ProductTypeID, ProductStyle, SiteId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, 
						   IsBundle, BundleCompositionLastModified)
	SELECT Id, ProductIDUser, Title, UrlFriendlyTitle, ShortDescription, LongDescription, Description, Keyword, IsActive, 
		   ProductTypeID, ProductStyle, SiteId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, IsBundle, 
		   BundleCompositionLastModified
	FROM dbo.Staging_PRProduct


	--select * from PRProductCategory
 	
	INSERT INTO PRProductCategory(Id, ProductId, CategoryId, Sequence)
	SELECT
		newid() as Id,
		Id as ProductId,
		(select top 1 Id from PRCategory where Title = 'Default') as CategoryId,
		0 as Sequence
	FROM
		PRProduct b


	--select * from ATAttributeGroup	 DONE
	
    INSERT INTO ATAttributeGroup(Id, Title, SiteId, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status)
	SELECT Id, Title, SiteId, IsSystem, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status 
	FROM Staging_ATAttributeGroup

	--ATAttribute DONE

	INSERT INTO ATAttribute(Id, AttributeDataType,  Title, Description, Sequence, Code, IsFaceted, 
                            IsSearched, IsSystem, IsDisplayed, IsEnum, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status,IsPersonalized)
    SELECT Id, AttributeDataType,  Title, Description, Sequence, Code, IsFaceted, 
               IsSearched, IsSystem, IsDisplayed, IsEnum, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status,IsPersonalized
    FROM dbo.Staging_ATAttribute

	--PRProductAttribute DONE
	INSERT INTO PRProductAttribute(Id, ProductId, AttributeId, Sequence, IsSKULevel)
    SELECT 
       Id, ProductId, AttributeId, Sequence, IsSKULevel
	FROM Staging_PRProductAttribute

    
	--PRProductAttributeValue DONE
	INSERT INTO PRProductAttributeValue(Id, ProductAttributeId, ProductId, AttributeId, AttributeEnumId, Value, CreatedDate, CreatedBy, 
										ModifiedDate, ModifiedBy, Status)
	SELECT 
       Id, ProductAttributeId, ProductId, AttributeId, AttributeEnumId, Value, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status
	FROM
	   Staging_PRProductAttributeValue

	-- PRProductSKU DONE
	INSERT INTO PRProductSKU(Id, ProductId, SiteId, Title, SKU, Description, Sequence, IsActive, IsOnline, ListPrice, 
							UnitCost, WholesalePrice, PreviousSoldCount, CreatedDate, CreatedBy, 
							 ModifiedDate, ModifiedBy, Status, Measure, OrderMinimum, OrderIncrement, HandlingCharges)
	SELECT
	    Id, ProductId, SiteId, Title, SKU, Description, Sequence, IsActive, IsOnline, ListPrice, UnitCost, WholesalePrice, 
		PreviousSoldCount, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, Measure, OrderMinimum, 
		OrderIncrement, HandlingCharges
	FROM
		Staging_PRProductSKU

	-- PRProductSKUAttributeValue ON
	INSERT INTO PRProductSKUAttributeValue(Id, ProductAttributeId, SKUId, AttributeId, AttributeEnumId, Value, CreatedDate, 
											   CreatedBy, ModifiedDate, ModifiedBy, Status)
	SELECT 
	  Id, ProductAttributeId, SKUId, AttributeId, AttributeEnumId, Value, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status
	FROM 
	  dbo.Staging_PRProductSKUAttributeValue

END -- END BEGIN
GO


	Update STSettingType SET SettingGroupId= (SELECT Id FROM STSettingGroup WHERE Name='General')
	,IsVisible=1
	,ControlType='Textbox'
	WHERE Name='AdminEmail'

	GO

	
	
	
	Update STSettingType SET SettingGroupId= (SELECT Id FROM STSettingGroup WHERE Name='General')
	,IsVisible=1
	,ControlType='Checkbox'
	WHERE Name='IsDevelopment'
	GO
	Update STSettingType SET SettingGroupId= (SELECT Id FROM STSettingGroup WHERE Name='General')
	,IsVisible=1
	,ControlType='Checkbox'
	WHERE Name='RequireRegistrationEmailConfirmation'
	GO
	
	
	Update STSettingType SET SettingGroupId= (SELECT Id FROM STSettingGroup WHERE Name='Payment Settings')
	,IsVisible=1
	,ControlType='Textbox'
	WHERE Name='cybs.keyId'
GO
	Update STSettingType SET SettingGroupId= (SELECT Id FROM STSettingGroup WHERE Name='Payment Settings')
	,IsVisible=1
	,ControlType='Textbox'
	WHERE Name='cybs.sharedKey'

GO

Update STSettingType SET SettingGroupId= (SELECT Id FROM STSettingGroup WHERE Name='Tax Settings')
,IsVisible=1
,ControlType='Dropdownlist'
WHERE Name='TaxProvider'
GO


PRINT 'Add column IsShared on ATAttributeValue'
IF(COL_LENGTH('ATAttributeValue', 'IsShared') IS NULL)
BEGIN
	ALTER TABLE ATAttributeValue ADD IsShared BIT  DEFAULT (1) NOT NULL
END
GO

PRINT 'Add column IsEditable on ATAttributeValue'
IF(COL_LENGTH('ATAttributeValue', 'IsEditable') IS NULL)
BEGIN
	ALTER TABLE ATAttributeValue ADD IsEditable BIT  DEFAULT (1) NOT NULL
END
GO

PRINT 'Add column IsReadOnly on ATAttributeValue'
IF(COL_LENGTH('ATAttributeValue', 'IsReadOnly') IS NULL)
BEGIN
	ALTER TABLE ATAttributeValue ADD IsReadOnly BIT  DEFAULT (0) NOT NULL
END
GO



IF NOT EXISTS (Select 1 from STSettingType where Name = 'Redis.Host')
BEGIN 
	DECLARE @Sequence INT
	SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
	INSERT INTO [dbo].[STSettingType]
			   ([Name]
			   ,[FriendlyName]
			   ,[SettingGroupId]
			   ,[Sequence]
			   ,[ControlData]
			   ,[ControlType]
			   ,[IsEnabled]
			   ,[IsVisible])
		 VALUES
			   ('Redis.Host'
			   ,'Redis.Host'
			   ,1
			   ,@Sequence
			   ,NULL
			   ,NULL
			   ,0
			   ,1)
END
GO

IF NOT EXISTS (Select 1 from STSettingType where Name = 'Redis.Port')
BEGIN 
	DECLARE @Sequence INT
	SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
	INSERT INTO [dbo].[STSettingType]
			   ([Name]
			   ,[FriendlyName]
			   ,[SettingGroupId]
			   ,[Sequence]
			   ,[ControlData]
			   ,[ControlType]
			   ,[IsEnabled]
			   ,[IsVisible])
		 VALUES
			   ('Redis.Port'
			   ,'Redis.Port'
			   ,1
			   ,@Sequence
			   ,NULL
			   ,NULL
			   ,0
			   ,1)
END
GO

PRINT 'Modify stored procedure Warehouse_Delete'
GO
IF(OBJECT_ID('Warehouse_Delete') IS NOT NULL)
	DROP PROCEDURE Warehouse_Delete
GO

CREATE PROCEDURE [dbo].[Warehouse_Delete]
(
	@Id uniqueidentifier
)
AS
BEGIN
	DELETE FROM INWarehouseSite Where WarehouseId = @Id
	DELETE FROM INInventory WHERE WarehouseId = @Id
	DELETE FROM INWarehouseDeliveryZone WHERE WarehouseId = @Id
	DELETE FROM INRestocking WHERE WarehouseId = @Id

	DECLARE @addressId uniqueidentifier
	SELECT @addressId = AddressId FROM INWarehouse WHERE Id = @Id
	DELETE INWarehouse Where Id = @Id

	IF @addressId IS NOT NULL
		DELETE GLAddress WHERE Id = @addressId	
END

GO

PRINT 'Updating SEIndex PK'
GO
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME='PK_SEIndex' )
BEGIN 
ALTER TABLE SEIndex Drop Constraint PK_SEIndex

ALTER TABLE SEIndex Add Constraint PK_SEIndexSiteId Primary KEY (Id, SiteId)

END
GO

PRINT 'Modify stored procedure Search_ReIndex'
GO
IF(OBJECT_ID('Search_ReIndex') IS NOT NULL)
	DROP PROCEDURE Search_ReIndex
GO
CREATE PROC [dbo].[Search_ReIndex]
(
	@ObjectType int,
	@SiteId uniqueidentifier
)
AS
BEGIN

	If(@ObjectType = 8)
	BEGIN
	-- Page

	Declare @ExcludeNodes Table (PageMapNodeId uniqueIdentifier)
	INSERT INTO @ExcludeNodes 
	SELECT  Cast(Value as Uniqueidentifier) from STSettingType T
		INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
		where Name in ('ProductTypePageMapNodeId','CampaignPageMapNodeId')
		AND SiteId=@SiteId And Value is not null AND rtrim(ltrim(Value)) !=''
	
	Delete from SEIndex where ObjectType = 8 and SiteId = @SiteId
	INSERT INTO [dbo].[SEIndex]
           ([Id]
		   ,[Title]
		   ,[DynamicObjectId]	
           ,[Url]
           ,[ObjectType]
           ,[SiteId]
           ,[Processed]
           ,[ProcessedDateTime]
           ,[ErrorMessage])
		   Select distinct P.PageDefinitionId, P.Title, null, null, 8, P.SiteId, 1, null, null  
		   from PageDefinition P
		   INNER JOIN PageMapNodePageDef PDM ON P.PageDefinitionId = PDM.PageDefinitionId
		   INNER JOIN PageMapNode PM ON PDM.PageMapNodeId = PM.PageMapNodeId
		   where PublishCount > 0 and (PageStatus = 1 OR PageStatus = 8) 
		   and MenuStatus = 0 --Visible
		   and P.SiteId = @SiteId 
		   --and PM.PageMapNodeId Not in (Select PageMapNodeId from @ExcludeNodes) TODO: Revisite the logic of exlcuding page in commerce
	END
	
	If(@ObjectType = 9)
	BEGIN
	-- File
	Delete from SEIndex where ObjectType = 9 and SiteId = @SiteId
	INSERT INTO [dbo].[SEIndex]
           ([Id]
		   ,[Title]
		   ,[DynamicObjectId]
           ,[Url]
           ,[ObjectType]
           ,[SiteId]
           ,[Processed]
           ,[ProcessedDateTime]
           ,[ErrorMessage])
		   Select C.Id, C.Title, null, null, 9, C.ApplicationId, 1, null, null from COFile F
			INNER JOIN COContent C ON F.ContentId = C.Id
			where ObjectTypeId = 9 and Status = 1 and C.ApplicationId = @SiteId 
		   
	END

	If(@ObjectType = 40)
	BEGIN
	-- POst
	Delete from SEIndex where ObjectType = 40 and SiteId = @SiteId
	INSERT INTO [dbo].[SEIndex]
           ([Id]
		   ,[Title]
		   ,[DynamicObjectId]
           ,[Url]
           ,[ObjectType]
           ,[SiteId]
           ,[Processed]
           ,[ProcessedDateTime]
           ,[ErrorMessage])
		   Select P.Id, P.Title, null, null, 40, P.ApplicationId, 1, null, null 
		   from BLPost P
			where Status = 8 and P.ApplicationId = @SiteId 
		
	END

	If(@ObjectType = 36)
	BEGIN	
	--Product 
	Delete from SEIndex where ObjectType = 36 and SiteId = @SiteId
	INSERT INTO [dbo].[SEIndex]
           ([Id]
		   ,[Title]
		   ,[DynamicObjectId]
           ,[Url]
           ,[ObjectType]
           ,[SiteId]
           ,[Processed]
           ,[ProcessedDateTime]
           ,[ErrorMessage])
		   Select Distinct Id, Title, null, null,  36, SiteId, 1, NULL, NULL from vwProduct where IsActive = 1 and SiteId = @SiteId

	END
END
GO
PRINT 'Updating stored procedure VersionDto_Get'
GO
IF(OBJECT_ID('VersionDto_Get') IS NOT NULL)
	DROP PROCEDURE  VersionDto_Get
GO	
CREATE PROCEDURE [dbo].[VersionDto_Get]
(
	@ObjectId		uniqueidentifier = NULL,
	@Ids			nvarchar(max) = NULL,
	@ObjectIds		nvarchar(max) = NULL,
	@ObjectTypes	nvarchar(max) = NULL,
	@SiteId			uniqueidentifier = NULL,
	@PageSize		int = NULL,
	@PageNumber		int = NULL
)
AS 
BEGIN   
	DECLARE @EmptyGuid uniqueidentifier, @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @EmptyGuid = dbo.GetEmptyGUID()
	
	IF @ObjectId IS NOT NULL AND @ObjectId != @EmptyGuid
	BEGIN
		SET @PageLowerBound = @PageSize * @PageNumber
		IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
			SET @PageUpperBound = @PageLowerBound - @PageSize + 1

		;WITH CTE AS(
			SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY V.CreatedDate DESC) AS RowNumber,
				COUNT(V.Id) OVER () AS TotalRecords,
				V.Id AS Id
			FROM VEVersion V
			WHERE ObjectId = @ObjectId
		)
		
		SELECT V.Id, 	
		    V.ObjectId,				
			V.XmlString,
			V.VersionNumber,
			V.RevisionNumber, 
			dbo.ConvertTimeFromUtc(V.CreatedDate, V.ApplicationId) AS CreatedDate,
			V.CreatedBy,
			CU.UserFullName AS CreatedByFullName,
			T.TotalRecords,
			T.RowNumber
		FROM VEVersion V
			JOIN CTE T ON V.Id = T.Id
			LEFT JOIN VW_UserFullName CU on CU.UserId = V.CreatedBy
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		ORDER BY RowNumber
	END
	ELSE -- Old Code
	BEGIN
		IF @Ids = '' SET @Ids = NULL
		IF @ObjectIds = '' SET @ObjectIds = NULL
		IF @ObjectTypes = '' SET @ObjectTypes = NULL
    
		DECLARE @tempResults TABLE
		(
			ID				UNIQUEIDENTIFIER ,
			ApplicationId	UNIQUEIDENTIFIER ,
			ObjectID		UNIQUEIDENTIFIER ,
			ObjectTypeId	INT ,
			CreatedDate		DATETIME ,
			CreatedBy		UNIQUEIDENTIFIER ,
			XmlString		XML
		)
    
		IF @Ids IS NOT NULL 
		BEGIN                        
			INSERT INTO @tempResults
			SELECT V.Id,
				V.ApplicationId,
				V.ObjectID,
				V.ObjectTypeId,
				V.CreatedDate,
				V.CreatedBy,
				V.XmlString
			FROM dbo.VEVersion V
				JOIN dbo.SplitGUID(@Ids, ',') T ON V.Id = T.Items
		END
		ELSE 
		BEGIN
			IF @ObjectIds IS NOT NULL 
			BEGIN                    
				INSERT INTO @tempResults
				SELECT V.Id,
					V.ApplicationId,
					V.ObjectID,
					V.ObjectTypeId,
					V.CreatedDate,
					V.CreatedBy,
					V.XmlString
				FROM dbo.VEVersion V
					JOIN dbo.SplitGUID(@ObjectIds, ',') T ON V.ObjectID = T.Items
			END
			ELSE IF @ObjectTypes IS NOT NULL 
			BEGIN
				INSERT INTO @tempResults
				SELECT V.Id,
					V.ApplicationId,
					V.ObjectID,
					V.ObjectTypeId,
					V.CreatedDate,
					V.CreatedBy,
					V.XmlString
				FROM dbo.VEVersion V
					JOIN dbo.SplitGUID(@ObjectTypes, ',') T ON V.ObjectTypeId = T.Items
			END
		END	
            
		SELECT  V.Id,
			V.ApplicationId,
			V.ObjectID,
			V.ObjectTypeId,
			V.CreatedDate,
			V.CreatedBy,
			V.XmlString
		FROM @tempResults V
		WHERE V.ApplicationId = @SiteID
	END
END
GO
Print 'Creating Procedure Product_GetOnlineProductByFilterVariant'
GO

IF(OBJECT_ID('Product_GetOnlineProductByFilterVariant') IS NOT NULL)
	DROP PROCEDURE Product_GetOnlineProductByFilterVariant
GO

CREATE PROCEDURE [dbo].[Product_GetOnlineProductByFilterVariant]
(
	@FilterId uniqueidentifier,
    @SortCol VARCHAR(25)=null,
    @PageSize INT=null, 
	@PageNumber INT=1,
	@ApplicationId uniqueidentifier = null,
	@FacetValueIds Xml=null,
	@CustomerId uniqueidentifier= null,
	@IsActiveAndIsOnline TINYINT=1,
	@IsOnline TINYINT=0
)

AS
BEGIN
	Declare @Rowcount int
	Declare @FacetValues table(FacetValueID uniqueidentifier)

SET @SortCol = lower(@SortCol)



DECLARE @GroupIds TABLE (
GroupId uniqueidentifier
 )

IF @CustomerId IS NOT NULL
	INSERT INTO @GroupIds
	select GroupId from USMemberGroup WHERE MemberId = @CustomerId
ELSE
BEGIN
	
	DECLARE @GId uniqueidentifier
		SET @GId =(SELECT top 1 Value from STSiteSetting SS  
		INNER  JOIN STSettingType ST ON SS.SettingTypeId = ST.Id where ST.Name = 'CommerceEveryOneGroupId')
		
			INSERT INTO @GroupIds(GroupId) Values (@GId)
END

IF @PageSize IS NULL
	SET @PageSize = 2147483647

DECLARE @SQL nvarchar(max),@params nvarchar(100)
declare @isLatest bit

	Select @IsLatest =IsLatest from NVFilterQuery 
	Where Id=@FilterId And 
	(@ApplicationId IS NULL OR SiteId = @ApplicationId)
	--SiteId=ISNULL(@ApplicationId, SiteId)
	if (@IsLatest=0)
	Begin
		Exec NavFilter_ExecuteQuery @FilterId 
	End

	DECLARE @ProductIDs Table
	(
		Row_ID int IDENTITY(1,1) PRIMARY KEY,
		ProductID uniqueidentifier
	)

IF(@FacetValueIds is not null)
BEGIN
			Insert into @FacetValues
			Select tab.col.value('text()[1]','uniqueidentifier')	
			FROM @FacetValueIds.nodes('/GenericCollectionOfGuid/guid')tab(col) 
			Set @Rowcount =@@RowCount
END


IF @SortCol = 'price asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice ASC			
												) AS [Row_ID],
										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice 
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID

									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId 

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId   
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Asc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											 MinPrice ASC			
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,   
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId )
													WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId
									GROUP BY   cte.ProductId
						) iq	
						
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice asc
						

					END 
END
IF @SortCol = 'price desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													MinPrice DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 MinPrice
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate,
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID
								
									LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON P.Id= TV.ProductId

									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId    
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,MinPrice)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											MinPrice DESC
											) AS [Row_ID],
									iq.ProductId,
									MinPrice
								FROM 
								(	
									SELECT  
										 cte.ProductId, 
										CASE
											WHEN 
											Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice)
											WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
											ELSE Min(ListPrice)
										END
											 as MinPrice,
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
										LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
										INNER JOIN @GroupIds gid ON MP.CustomerGroupId = gid.GroupId
									) TV ON cte.ProductId = TV.ProductId

									GROUP BY   cte.ProductId
						) iq	
					
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by MinPrice Desc

					END 
END

IF @SortCol = 'soldcount desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount DESC			
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount DESC		
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount Desc


					END 
END


IF @SortCol = 'soldcount asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													TotalSoldCount ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 TotalSoldCount
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate 
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
								--	INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
									GROUP BY P.Id
								) PRS
							)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC

							
					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,TotalSoldCount)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											TotalSoldCount ASC				
											) AS [Row_ID],
									iq.ProductId,
									TotalSoldCount
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by TotalSoldCount ASC


					END 
END

IF @SortCol = 'createddate asc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate ASC					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId   
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate ASC			
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId   
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
						--INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)


						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate ASC

					END 
END

IF @SortCol = 'createddate desc'	BEGIN
					if(@FacetValueIds is not null)
					BEGIN
							
						

							;With PagingCTE 
							AS(
							SELECT 
										 ROW_NUMBER() 

												OVER(ORDER BY 
													CreatedDate desc					
												) AS [Row_ID],

										 PRS.Id ProductId,
										 CreatedDate
								FROM
								(
									SELECT P.Id, SUM(PS.PreviousSoldCount) TotalSoldCount,MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
									FROM 
									(Select ProductID
											from (
												Select Distinct ProductID,FV.FacetValueID
												from @FacetValues FV 
												INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID 
												Inner Join  NVFilterOutput FO ON C.ProductID=FO.ObjectId
												Where QueryId=@FilterId 
												and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
												) PF
									Group By ProductID
									Having Count(FacetValueID) =@RowCount) PID
									INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
									INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId 
									WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
									GROUP BY P.Id
								) PRS
							)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate DESC

					END
					ELSE
					BEGIN 

						;WITH PagingCTE (Row_ID,ProductId,CreatedDate)
						AS
							(

								SELECT 
									ROW_NUMBER() 
										OVER(ORDER BY 
											CreatedDate desc	
											) AS [Row_ID],
									iq.ProductId,
									CreatedDate
								FROM 
								(	
									SELECT  
										 ProductId, 
										MIN(ListPrice) AS MinPrice,     
										SUM(PreviousSoldCount) AS TotalSoldCount, 
										Min(CreatedDate) CreatedDate
									FROM   
											(
												SELECT     
												P.Id AS ProductId, 
												PS.ListPrice,     
												PS.PreviousSoldCount, 
												P.CreatedDate
												FROM         
													dbo.vwProduct P 
													inner join  NVFilterOutput FnFilter	ON P.Id = FnFilter.ObjectId
													INNER JOIN    dbo.PRProductSKU PS ON (P.Id = PS.ProductId)
													WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId  
														AND FnFilter.QueryId=@FilterId and 
														ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
											) cte
									GROUP BY   ProductId
						) iq	
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
					)

						INSERT INTO @ProductIDs (ProductID)
						SELECT pcte.ProductId FROM PagingCTE pcte order by CreatedDate Desc
						
					END 
END

IF @SortCol = 'title desc' 
    BEGIN
        IF ( @FacetValueIds IS NOT NULL ) 
            BEGIN							
					;WITH    PagingCTE (Row_ID,ProductId,CreatedDate, Title)
                              AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY Title DESC ) AS [Row_ID] ,
                                            PRS.Id ProductId ,
                                            PRS.CreatedDate,
											P.Title	
                                   FROM     ( SELECT    P.Id ,
                                                        SUM(PS.PreviousSoldCount) TotalSoldCount ,
                                                        MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
                                              FROM      ( SELECT
                                                              ProductID
                                                          FROM
                                                              ( SELECT DISTINCT
                                                              ProductID ,
                                                              FV.FacetValueID
                                                              FROM
                                                              @FacetValues FV
                                                              INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID
                                                              INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              WHERE
                                                              QueryId = @FilterId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                              ) PF
                                                          GROUP BY ProductID
                                                          HAVING
                                                              COUNT(FacetValueID) = @RowCount
                                                        ) PID
                                                        INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
                                                        INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId
														WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
                                              GROUP BY  P.Id
                                            ) PRS
											INNER JOIN dbo.PRProduct P ON PRS.Id = P.Id
                                 )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title DESC

            END
        ELSE 
            BEGIN 

						;
                WITH    PagingCTE ( Row_ID, ProductId, CreatedDate, Title )
                          AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY p.Title DESC ) AS [Row_ID] ,
                                        iq.ProductId ,
                                        iq.CreatedDate,
										P.Title
                               FROM     ( SELECT    ProductId ,
                                                    MIN(ListPrice) AS MinPrice ,
                                                    SUM(PreviousSoldCount) AS TotalSoldCount ,
                                                    MIN(CreatedDate) CreatedDate
                                          FROM      ( SELECT  P.Id AS ProductId ,
                                                              PS.ListPrice ,
                                                              PS.PreviousSoldCount ,
                                                              P.CreatedDate
                                                      FROM    dbo.vwProduct P
                                                              INNER JOIN NVFilterOutput FnFilter ON P.Id = FnFilter.ObjectId
                                                              INNER JOIN dbo.PRProductSKU PS ON ( P.Id = PS.ProductId )
															  WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
                                                              AND FnFilter.QueryId = @FilterId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                    ) cte
                                          GROUP BY  ProductId
                                        ) iq	
										INNER JOIN Prproduct P ON iq.ProductId = p.ID
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
                                        
                             )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title DESC
						
            END 
    END
  
  IF @SortCol = 'title asc' 
    BEGIN
        IF ( @FacetValueIds IS NOT NULL ) 
            BEGIN							
					;WITH    PagingCTE (Row_ID,ProductId,CreatedDate, Title)
                              AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY Title asc ) AS [Row_ID] ,
                                            PRS.Id ProductId ,
                                            PRS.CreatedDate,
											P.Title	
                                   FROM     ( SELECT    P.Id ,
                                                        SUM(PS.PreviousSoldCount) TotalSoldCount ,
                                                        MIN(P.CreatedDate) CreatedDate
									--,MIN(MinimumEffectivePrice) MinPrice
                                              FROM      ( SELECT
                                                              ProductID
                                                          FROM
                                                              ( SELECT DISTINCT
                                                              ProductID ,
                                                              FV.FacetValueID
                                                              FROM
                                                              @FacetValues FV
                                                              INNER JOIN ATFacetRangeProduct_Cache C ON FV.FacetValueID = C.FacetValueID
                                                              INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              WHERE
                                                              QueryId = @FilterId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                              ) PF
                                                          GROUP BY ProductID
                                                          HAVING
                                                              COUNT(FacetValueID) = @RowCount
                                                        ) PID
                                                        INNER JOIN vwProduct P ON P.Id = PID.ProductID	
									--INNER JOIN cache_ProductMinEffectivePrice cp ON P.Id = cp.ProductId
                                                        INNER JOIN PRProductSKU PS ON P.Id = PS.ProductId
														WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
                                              GROUP BY  P.Id
                                            ) PRS
											INNER JOIN dbo.PRProduct P ON PRS.Id = P.Id
                                 )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title asc

            END
        ELSE 
            BEGIN 

						;
                WITH    PagingCTE ( Row_ID, ProductId, CreatedDate, Title )
                          AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY p.Title asc ) AS [Row_ID] ,
                                        iq.ProductId ,
                                        iq.CreatedDate,
										P.Title
                               FROM     ( SELECT    ProductId ,
                                                    MIN(ListPrice) AS MinPrice ,
                                                    SUM(PreviousSoldCount) AS TotalSoldCount ,
                                                    MIN(CreatedDate) CreatedDate
                                          FROM      ( SELECT  P.Id AS ProductId ,
                                                              PS.ListPrice ,
                                                              PS.PreviousSoldCount ,
                                                              P.CreatedDate
                                                      FROM    dbo.vwProduct P
                                                              INNER JOIN NVFilterOutput FnFilter ON P.Id = FnFilter.ObjectId
                                                              INNER JOIN dbo.PRProductSKU PS ON ( P.Id = PS.ProductId )
															WHERE P.IsActive = 1 AND P.SiteId=@ApplicationId 
                                                              AND FnFilter.QueryId = @FilterId
                                                              AND ObjectId NOT IN (
                                                              SELECT
                                                              ObjectId
                                                              FROM
                                                              NVFilterExclude
                                                              WHERE
                                                              QueryId = @FilterId )
                                                    ) cte
                                          GROUP BY  ProductId
                                        ) iq	
										INNER JOIN Prproduct P ON iq.ProductId = p.ID
					--	INNER JOIN cache_ProductMinEffectivePrice MP ON MP.ProductId = iq.ProductId
                                        
                             )
                    INSERT  INTO @ProductIDs
                            ( ProductID
                            )
                            SELECT  pcte.ProductId
                            FROM    PagingCTE pcte
                            ORDER BY Title asc
						
            END 
    END  




--START
	IF (@IsActiveAndIsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			LEFT JOIN PRProductSKUVariant PSV ON PSV.Id=PS.Id and PSV.SiteId=@ApplicationId
			WHERE PS.IsOnline = 1 AND isnull(PSV.IsActive,PS.IsActive) = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber
			
		END
	ELSE IF (@IsOnline = 1)
		BEGIN

			;With ProductPagingCTE (Row_ID, ProductID)
			AS(Select ROW_Number() over (order by P.Row_ID) AS Row_ID, P.ProductID 
			FROM 
			(SELECT DISTINCT P.Row_ID, P.ProductID FROM @ProductIDs P
			INNER JOIN PRProductSKU PS ON PS.ProductID = P.ProductID
			WHERE PS.IsOnline = 1) P
			)

			SELECT dbo.GetEmptyGUID() Id,count(Row_ID) [Count] FROM ProductPagingCTE pcte
			UNION ALL
			SELECT pcte.ProductId as Id,0 FROM ProductPagingCTE pcte
			WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber

		END
--END


END
GO

IF(OBJECT_ID('Facet_GetFacetsByNavigation') IS NOT NULL)
BEGIN
	Drop Procedure [dbo].[Facet_GetFacetsByNavigation]
END
GO


CREATE PROCEDURE [dbo].[Facet_GetFacetsByNavigation]
    (
      @Id UNIQUEIDENTIFIER ,
      @ExistingFacetValueIds XML = NULL ,
      @ExistingFacets XML = NULL ,
      @UseLimit BIT = 1,
	  @ApplicationId uniqueidentifier=NULL
    )
AS 
    BEGIN

        DECLARE @filterId UNIQUEIDENTIFIER
        SELECT  @filterId = QueryId
        FROM    NVNavNodeNavFilterMap
        WHERE   NavNodeId = @Id

        DECLARE @totProd INT
        SELECT  @totProd = COUNT(C.Id)
        FROM    ATFacetRangeProduct_Cache C
		INNER JOIN VWProduct P ON C.ProductId=P.Id AND P.SiteId=@ApplicationId  
		Group By P.Id

        SET @UseLimit = ISNULL(@UseLimit, 0)
        IF ( @ExistingFacetValueIds IS NULL
             AND @ExistingFacets IS NULL
           ) 
            BEGIN

                SELECT 
			C.FacetID,
			C.FacetValueID,
			MIN(C.DisplayText) DisplayText,
			Count(*) ProductCount,
			MIN(NF.Sequence) FacetSequence,
			Case FR.Sequence 
				WHEN NULL  THEN @totProd - COUNT(*)
				ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
			END AS SortOrder,
			MIN(AF.AllowMultiple) AllowMultiple
		FROM 
			ATFacetRangeProduct_Cache C 
			INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
			INNER JOIN ATFacet AF ON AF.Id = NF.FacetId	
			INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
			INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
			LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
			LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetValueID ,
                        C.FacetID
                UNION ALL
                SELECT  C.FacetID ,
                        NULL FacetValueID ,
                        MIN(AF.Title) DisplayText ,
                        COUNT(*) ,
                        MIN(Sequence) FacetSequence ,
                        0 SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
						INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetID 
            END
        ELSE 
            IF ( @ExistingFacetValueIds IS NOT NULL ) 
                BEGIN
 

                    CREATE TABLE #FilterProductIds
                        (
                          Id UNIQUEIDENTIFIER
                            CONSTRAINT [PK_#cteValueToIdValue]
                            PRIMARY KEY CLUSTERED ( [Id] ASC )
                            WITH ( PAD_INDEX = OFF,
                                   STATISTICS_NORECOMPUTE = OFF,
                                   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                                   ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
                        )
                    ON  [PRIMARY]

                    INSERT  INTO #FilterProductIds
                            SELECT  C.ProductID
                            FROM    ATFacetRangeProduct_Cache C
							INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    INNER JOIN @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON tab.col.value('text()[1]',
                                                              'uniqueidentifier') = C.FacetValueID
                            GROUP BY C.ProductID
                            HAVING  COUNT(C.FacetValueID) >= ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab1 ( col1 )
                                                             )

				SELECT 
					C.FacetID,
					C.FacetValueID,
					MIN(C.DisplayText) DisplayText,
					Count(*) ProductCount,
					MIN(NF.Sequence) FacetSequence,
					Case FR.Sequence 
								WHEN NULL  THEN @totProd - COUNT(*)
								ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
							END AS SortOrder,
					MIN(AF.AllowMultiple) AllowMultiple
				FROM 
					ATFacetRangeProduct_Cache C 
					INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
					INNER JOIN ATFacet AF ON AF.Id = NF.FacetId	
					INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
					INNER JOIN #FilterProductIds P ON P.ID=C.ProductID
					LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
					LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetValueID ,
                            C.FacetID
                    UNION ALL
                    SELECT  C.FacetID ,
                            NULL FacetValueID ,
                            MIN(AF.Title) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            0 SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProductTop_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetID 


                    DROP TABLE #FilterProductIds

                END
            ELSE 
                BEGIN
                   ------------------NEW---------------------------------
                    DECLARE @FacetValues TABLE
                        (
                          FacetValueID UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER
                        )
                    INSERT  INTO @FacetValues
                            SELECT  tab.col.value('(value/guid/text())[1]',
                                                  'uniqueidentifier') ,
                                    tab.col.value('(key/guid/text())[1]',
                                                  'uniqueidentifier')
                            FROM    @ExistingFacets.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item') tab ( col ) ;
                 

                 
                 

                    DECLARE @FilteredProducts TABLE
                        (
                          ProductId UNIQUEIDENTIFIER
                        )           
                    
                    DECLARE @RequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
       
                    INSERT  INTO @RequiredProduct
                            SELECT DISTINCT
                                    C.ProductId ,
                                    Row_Number() OVER ( PARTITION BY ProductId ORDER BY ProductID ) AS RowNumber
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                            WHERE   F.AllowMultiple = 0 

                    DELETE  FROM @RequiredProduct
                    WHERE   RowNumber NOT IN ( SELECT   MAX(RowNumber)
                                               FROM     @RequiredProduct )


                    DECLARE @MultiRequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
                     INSERT  INTO @MultiRequiredProduct
                            SELECT  DISTINCT
                                    ProductID ,
                                    FacetID ,
                                    Row_Number() OVER ( PARTITION BY ProductID ORDER BY ProductID ) AS RowNumber
                                    
                                    FROM (
                                    SELECT DISTINCt C.ProductID,
                                    C.FacetID
                                    
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                            WHERE   F.AllowMultiple = 1)TV
                           



                    IF NOT EXISTS ( SELECT  *
                                    FROM    @RequiredProduct ) 
                        BEGIN
                            INSERT  INTO @FilteredProducts
                                    SELECT  ProductId
                                    FROM    @MultiRequiredProduct
                                    WHERE   RowNumber IN (
                                            SELECT  MAX(RowNumber)
                                            FROM    @MultiRequiredProduct )
                       
                        END
                    ELSE 
                        IF NOT EXISTS ( SELECT  *
                                        FROM    @MultiRequiredProduct ) 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  ProductId
                                        FROM    @RequiredProduct
                            END
                        ELSE 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  RP.ProductId
                                        FROM    @RequiredProduct RP
                                                INNER JOIN ( SELECT
                                                              ProductId
                                                             FROM
                                                              @MultiRequiredProduct
                                                             WHERE
                                                              RowNumber IN (
                                                              SELECT
                                                              MAX(RowNumber)
                                                              FROM
                                                              @MultiRequiredProduct )
                                                           ) TV ON TV.ProductId = RP.ProductId


                            END

                    DECLARE @FacetResults TABLE
                        (
                          FacetId UNIQUEIDENTIFIER ,
                          FacetValueId UNIQUEIDENTIFIER ,
                          DisplayText NVARCHAR(250) ,
                          FacetName NVARCHAR(250) ,
                          ProductCount INT ,
                          FacetSequence INT ,
                          SortOrder INT ,
                          AllowMultiple INT ,
                          ENABLED INT
                        )
                    INSERT  INTO @FacetResults
                            ( FacetId ,
                              FacetValueId ,
                              DisplayText ,
                              FacetName ,
                              ProductCount ,
                              FacetSequence ,
                              SortOrder ,
                              AllowMultiple ,
                              ENABLED
                            )
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @FilteredProducts RP ON RP.ProductId = C.ProductID
									LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 0
							        AND NFE.QueryId IS NULL
                                    AND ( RP.ProductId IS NOT NULL
                                          OR NOT EXISTS ( SELECT
                                                              ProductId
                                                          FROM
                                                              @FilteredProducts )
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
--Close now we need to get the other multi values that are not currently in the required products but would be filtered by any required products. 
--This query should be the multi facets that have no products currently showing in the grid. 
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( 
																		--MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
                                                              ( SELECT
                                                              COUNT(DISTINCT FacetId)
                                                              FROM
                                                              @MultiRequiredProduct
                                                              ) = 1 )
																	---End of expensive
                                                              )
							        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 1
							        AND NFE.QueryId IS NULL  
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
									INNER JOIN (Select Distinct Id FROM VWProduct Where SiteId=@ApplicationId  ) P ON C.ProductId=P.Id 
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
																		--( SELECT
																		--COUNT(DISTINCT FacetId)
																		--FROM
																		--@MultiRequiredProduct
																		--) = 1
                                                              )
																	---End of expensive
                                                              )
									LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId
                                                             AND NFE.QueryId = FO.QueryId
                            WHERE   F.AllowMultiple = 1
							        AND NFE.QueryId IS NULL
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                             
                                        )
                                        
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
        
                        
        
        
        
                    SELECT  FacetId ,
                            FacetValueId ,
                            DisplayText ,
                            ProductCount ,
                            FacetSequence ,
                            SortOrder ,
                            AllowMultiple ,
                            ENABLED
                    FROM    @FacetResults
                    UNION
                    SELECT  FacetId ,
                            NULL ,
                            FacetName AS DisplayText ,
                            SUM(ProductCount) ,
                            MAX(FacetSequence) ,
                            MAX(SortOrder) ,
                            MAX(AllowMultiple) ,
                            MAX(ENABLED)
                    FROM    @FacetResults
                    GROUP BY FacetId ,
                            FacetName
                    ORDER BY FacetValueId DESC
                   
                   
                   
                   
                   ----------------END OF NEW------------------------------
                        
                        
                        
                        
                        

                END
    END

GO

IF(OBJECT_ID('Facet_ReloadProductFacetCache') IS NOT NULL)
BEGIN
	Drop Procedure [dbo].[Facet_ReloadProductFacetCache]
END
GO

CREATE PROCEDURE [dbo].[Facet_ReloadProductFacetCache]  
(  
 @ApplicationId uniqueidentifier=null  
)  
--now only 39 seconds, now 50 seconds with all Enumed Faceted in Shaw  
AS  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
  
--********************************************************************************  
-- Procedure Body  
--********************************************************************************  
BEGIN  
  
--create temp table to load new cache information into   
--(not sure if this is the best way to declare a large temp table - DM)  
   
Print 'Starting'
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))
Create Table #tempCache
(
   Id INT IDENTITY(1,1) Primary Key,   
    FacetId uniqueidentifier,  
 AttributeId uniqueidentifier,  
 FacetValueID uniqueidentifier,  
 DisplayText nvarchar(50),  
 ProductId uniqueidentifier,  
 SortOrder int Default(0)  
)  
  
  
--Enumerated Attributes Defined Ranges  
Print 'Insert INTO STARt #tempCache '  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))

INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder)  
SELECT DISTINCT FacetID, PSFAV.AttributeID, AttributeEnumId AS FacetValueID, PSFAV.Value AS DisplayText, ProductId,AE.Sequence  
--, AE.Value AS [From], AE.Value AS [To]  
FROM   
 vwProductSKUFacetAttributeValue_Enumerated PSFAV   
 LEFT JOIN ATAttributeEnum AE ON AE.Id=PSFAV.AttributeEnumId  
--WHERE IsRange = 0 AND (@ApplicationId IS NULL OR PSFAV.SiteId = @ApplicationId)   

Print 'Insert INTO AFTER #tempCache Enumerated'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121)) 

SELECT AttributeId, ProductId, convert(Decimal(14, 4), value) as Value
INTO #PSFAV
FROM vwProductSKUFacetAttributeValue_RANGE where ISNUMERIC(Value)=1
--Ranges Per Type:  
INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder)  
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId,FR.Sequence  
--, FRX.[From], FRX.[To]  
-- select *  
FROM   
 ATFacet F   
 INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID -- F.IsRange = 1  
 INNER JOIN ATFacetRangeInt FRX ON FR.Id = FRX.FacetRangeID   
 INNER JOIN #PSFAV PSFAV ON F.AttributeID = PSFAV.AttributeId AND  
   PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value)    
   AND ISNUMERIC(PSFAV.Value)=1  
   --Order by DisplayText
--WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)  

 
PRINT  'Insert AFTER INTO #tempCache Int'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))   

Print 'Start Insert Decimal'
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))

INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder)  
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId,FR.Sequence  
--, FRX.[From], FRX.[To]  
FROM   
 ATFacet F   
 INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --and  F.IsRange = 1   
 INNER JOIN ATFacetRangeDecimal FRX ON FR.Id = FRX.FacetRangeID   
 INNER JOIN #PSFAV PSFAV ON F.AttributeID = PSFAV.AttributeId AND PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) 
 AND IsNull(FRX.[To],PSFAV.Value) AND ISNUMERIC(PSFAV.Value)=1  
  --  WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)  


Print 'Insert AFTER INTO #tempCache Decimal'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))  

Drop table #PSFAV

SELECT F.AttributeId, ProductId, Value
INTO #PSFAVString
FROM vwProductSKUFacetAttributeValue_RANGE V1 
INNER JOIN ATFacet F ON V1.AttributeId = F.AttributeID
INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --AND F.IsRange = 1   
INNER JOIN ATFacetRangeString FRX ON FR.Id = FRX.FacetRangeID   

INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder)  
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId,FR.Sequence  
--, FRX.[From], FRX.[To]  
FROM   
 ATFacet F   
 INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --AND F.IsRange = 1   
 INNER JOIN ATFacetRangeString FRX ON FR.Id = FRX.FacetRangeID   
 INNER JOIN #PSFAVString PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value)  
    AND F.AttributeID = PSFAV.AttributeId  
  --  WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)  
--delete data from existing cache  

PRINT 'Insert AFTER INTO #tempCache String'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))  

Drop table  #PSFAVString


SELECT AttributeId, ProductId, convert(DateTime, value) as Value
INTO #PSFAVDate
FROM vwProductSKUFacetAttributeValue_RANGE where ISDATE(Value)=1

INSERT INTO #tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder)  
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId,FR.Sequence  
--, FRX.[From], FRX.[To]  
FROM   
 ATFacet F   
 INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --and  F.IsRange = 1   
 INNER JOIN ATFacetRangeDate FRX ON FR.Id = FRX.FacetRangeID  
 INNER JOIN #PSFAVDate PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value) AND ISDATE(PSFAV.Value)=1  
    AND F.AttributeID = PSFAV.AttributeId  
   -- WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)  
      
PRINT 'Insert AFTER INTO #tempCache Date'  
PRINT (CONVERT( VARCHAR(24), GETDATE(), 121))  

Drop table  #PSFAVDate

truncate table dbo.ATFacetRangeProduct_Cache  
  
 --repopulate the real cache  
 --(again, best method for this? this will be a LOT of data - DM)  
 INSERT INTO dbo.ATFacetRangeProduct_Cache   
 (Id,FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder)  
 SELECT newid(),T.FacetId,T.AttributeId,T.FacetValueID,T.DisplayText,T.ProductId,T.SortOrder   
 FROM #tempCache T  
  
TRUNCATE TABLE ATFacetRangeProductTop_Cache  
  
-- Create Distinct Product Count by Facet.  
INSERT INTO ATFacetRangeProductTop_Cache --112,008  
(FacetId, ProductId)
SELECT   
 [FacetID],      
    [ProductID]      
FROM   
 ATFacetRangeProduct_Cache --242,572  
GROUP BY  
 [FacetID]    
  ,[ProductID]  
  
END  
GO

IF EXISTS (SELECT *
          FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'CLImageVariant'
                 AND COLUMN_NAME = 'FileName' )
BEGIN
Alter Table CLImageVariant Drop Column FileName 
Alter Table CLImageVariant Drop Column RelativePath 

ALter Table CLImageVariantCache Drop Column FileName 
ALter Table CLImageVariantCache Drop Column RelativePath 

END
GO

PRINT 'Dropping column Size from table CLImageVariant'
GO
IF EXISTS (SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CLImageVariant' AND COLUMN_NAME = 'Size' )
BEGIN
	ALTER TABLE CLImageVariant DROP COLUMN Size
END
GO

PRINT 'Dropping column Height from table CLImageVariant'
GO
IF EXISTS (SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CLImageVariant' AND COLUMN_NAME = 'Height' )
BEGIN
	ALTER TABLE CLImageVariant DROP COLUMN Height
END
GO

PRINT 'Dropping column Width from table CLImageVariant'
GO
IF EXISTS (SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CLImageVariant' AND COLUMN_NAME = 'Width' )
BEGIN
	ALTER TABLE CLImageVariant DROP COLUMN Width
END
GO

PRINT 'Dropping column ParentImage from table CLImageVariant'
GO
IF EXISTS (SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CLImageVariant' AND COLUMN_NAME = 'ParentImage' )
BEGIN
	ALTER TABLE CLImageVariant DROP COLUMN ParentImage
END
GO

PRINT 'Dropping column Size from table CLImageVariantCache'
GO
IF EXISTS (SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CLImageVariantCache' AND COLUMN_NAME = 'Size' )
BEGIN
	ALTER TABLE CLImageVariantCache DROP COLUMN Size
END
GO

PRINT 'Dropping column Height from table CLImageVariantCache'
GO
IF EXISTS (SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CLImageVariantCache' AND COLUMN_NAME = 'Height' )
BEGIN
	ALTER TABLE CLImageVariantCache DROP COLUMN Height
END
GO

PRINT 'Dropping column Width from table CLImageVariantCache'
GO
IF EXISTS (SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CLImageVariantCache' AND COLUMN_NAME = 'Width' )
BEGIN
	ALTER TABLE CLImageVariantCache DROP COLUMN Width
END
GO

PRINT 'Dropping column ParentImage from table CLImageVariantCache'
GO
IF EXISTS (SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'CLImageVariantCache' AND COLUMN_NAME = 'ParentImage' )
BEGIN
	ALTER TABLE CLImageVariantCache DROP COLUMN ParentImage
END
GO

PRINT 'Create view vwProductTypeImport'
GO
IF (OBJECT_ID('vwProductTypeImport') IS NOT NULL)
	DROP View vwProductTypeImport	
GO
CREATE VIEW [dbo].[vwProductTypeImport]  
AS  
SELECT     Id AS ProductTypeId, Title As ProductTypeName, Description, FriendlyName, CMSPageId, ModifiedBy, ModifiedDate, Status, SiteId
FROM         dbo.PRProductType
UNION ALL 
SELECT     Id AS ProductTypeId, Title As ProductTypeName, Description, FriendlyName, CMSPageId, ModifiedBy, ModifiedDate, Status, SiteId
FROM         dbo.PRProductTypeVariantCache 
GO



IF(COL_LENGTH('USCommerceUserProfile', 'IsTaxExempt') IS NULL)
	ALTER TABLE USCommerceUserProfile ADD IsTaxExempt bit not null default(0)
GO

IF(COL_LENGTH('USCommerceUserProfile', 'TaxId') IS NULL)
	ALTER TABLE USCommerceUserProfile ADD TaxId nvarchar(255)
GO


PRINT 'Creating View vwCustomer'
GO
IF (OBJECT_ID('vwCustomer') IS NOT NULL)
	DROP View vwCustomer
GO
CREATE VIEW [dbo].[vwCustomer] 
AS         
SELECT	CUP.Id, U.FirstName, U.LastName, U.MiddleName, U.UserName, NULL AS [Password], M.Email, U.EmailNotification, U.CompanyName, U.BirthDate, U.Gender, U.LeadScore,
		CUP.IsBadCustomer, CUP.IsActive, M.IsApproved, M.IsLockedOut, CUP.IsOnMailingList, CUP.[Status], CUP.CSRSecurityQuestion, CUP.CSRSecurityPassword, U.HomePhone,
		U.MobilePhone, U.OtherPhone, U.ImageId, CUP.AccountNumber, CUP.IsExpressCustomer, CUP.ExternalId, CUP.ExternalProfileId,
		(
			SELECT	COUNT(*)
			FROM	CSCustomerLogin
			WHERE	CustomerId = CUP.Id
		) + 1 AS NumberOfLoginAccounts,
		CASE WHEN EXISTS
		(
			SELECT	Id
			FROM	PTLineOfCreditInfo
			WHERE	CustomerId =  CUP.Id AND
					[Status] <> dbo.GetDeleteStatus()
		) THEN 1 ELSE 0 END AS HasLOC,
		COALESCE(CO.TotalOrders, 0) AS TotalOrders, COALESCE(CO.TotalSpend, 0) AS TotalSpend, CO.LastPurchase,
		A.Id AS AddressId, A.AddressLine1, A.AddressLine2, A.AddressLine3, A.City, A.StateId, ST.[State], ST.StateCode,
		A.CountryId, C.CountryName, C.CountryCode, A.Zip, A.County, A.Phone,
		U.ExpiryDate AS ExpirationDate, U.LastActivityDate,
		CASE WHEN EXISTS (
			SELECT	*
			FROM	USSiteUser
			WHERE	UserId = U.Id AND
					IsSystemUser = 1
		) THEN 1 ELSE 0 END AS IsSystemUser,
		CUP.IsTaxExempt,
		CUP.Taxid,
		U.CreatedDate,  U.CreatedBy, CFN.UserFullName AS CreatedByFullName,
		ISNULL(U.ModifiedDate, U.CreatedDate) AS ModifiedDate, ISNULL(U.ModifiedBy, U.CreatedBy) AS ModifiedBy, MFN.UserFullName AS ModifiedByFullName
FROM	USCommerceUserProfile AS CUP
        INNER JOIN USUser AS U ON U.Id = CUP.Id
		INNER JOIN USMembership AS M ON M.UserId = U.Id
		LEFT JOIN (
			SELECT		CustomerId, COUNT(*) AS TotalOrders, SUM(GrandTotal) AS TotalSpend, MAX(OrderDate) AS LastPurchase
			FROM		OROrder
			GROUP BY	CustomerId
		) AS CO ON CO.CustomerId = U.Id
		LEFT JOIN USUserShippingAddress AS USA ON USA.UserId = U.Id AND USA.IsPrimary = 1
		LEFT JOIN GLAddress AS A ON A.Id = USA.AddressId
		LEFT JOIN GLState AS ST ON ST.Id = A.StateId
		LEFT JOIN GLCountry AS C ON C.Id = A.CountryId
		LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = U.CreatedBy
		LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = U.ModifiedBy
WHERE	U.[Status] !=3
GO

PRINT 'Modify stored procedure CampaignDto_IsNameExists'
GO
IF(OBJECT_ID('CampaignDto_IsNameExists') IS NOT NULL)
	DROP PROCEDURE CampaignDto_IsNameExists
GO
CREATE PROCEDURE [dbo].[CampaignDto_IsNameExists]
(
	@Id			uniqueidentifier,
	@Title		nvarchar(4000),
	@Type		int,
	@SiteId		uniqueidentifier
)
AS
BEGIN
	DECLARE @Exists bit, @CampaignId uniqueidentifier
	SET @Exists = 0

	IF @Type = 0 SET @Type = 1

	SELECT TOP 1 @CampaignId = C.Id FROM MKCampaign C
		JOIN MKCampaignSend S ON C.Id = S.CampaignId AND S.SiteId = @SiteId
	WHERE C.Id != @Id AND 
		C.Type = @Type AND 
		S.Status != 8 AND 
		C.Title like @Title

	IF @CampaignId IS NOT NULL
		SET @Exists = 1

	SELECT @Exists 'Exists'
END
GO

PRINT 'Modify stored procedure CampaignDto_Get'
GO
IF(OBJECT_ID('CampaignDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignDto_Get
GO
CREATE PROCEDURE [dbo].[CampaignDto_Get]  
(  
	@Id						uniqueidentifier = NULL,  
	@SiteId					uniqueidentifier,
	@Status					int = NULL,
	@Type					int = NULL,
	@CampaignGroupId		uniqueidentifier = NULL,
	@ParentId				uniqueidentifier = NULL,
	@CampaignRunId			uniqueidentifier = NULL,
	@PageSize				int = NULL,   
	@PageNumber				int = NULL,
	@MaxRecords				int = NULL,
	@Keyword				nvarchar(MAX) = NULL,
	@SortBy					nvarchar(100) = NULL,  
	@SortOrder				nvarchar(10) = NULL,
	@IgnoreDetails			bit = NULL,
	@Filter					int = NULL
)  
AS  
BEGIN
	DECLARE @EmptyGuid uniqueidentifier, @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	SET @EmptyGuid = dbo.GetEmptyGUID()

	IF @Id = @EmptyGuid SET @Id = NULL
	IF @CampaignGroupId = @EmptyGuid SET @CampaignGroupId = NULL
	IF @CampaignGroupId IS NULL AND @ParentId != @EmptyGuid SET @CampaignGroupId = @ParentId

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
	IF (@SortBy IS NOT NULL)
		SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

	IF @Keyword IS NOT NULL SET @Keyword = '%' + @Keyword + '%'
	IF @Filter = 0 SET @Filter = NULL
	
	DECLARE @tbIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbQueryIds TABLE (Id uniqueidentifier primary key, RowNumber int)
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)

	IF @Id IS NULL AND @CampaignRunId IS NOT NULL
		SELECT TOP 1 @Id = CampaignId FROM MKCampaignRunHistory WHERE Id = @CampaignRunId

	IF @Id IS NULL	
	BEGIN
		INSERT INTO @tbIds
		SELECT C.Id AS Id, ROW_NUMBER() OVER (ORDER BY 
			CASE WHEN @SortClause = 'Title ASC' THEN C.Title END ASC,
			CASE WHEN @SortClause = 'Title DESC' THEN C.Title END DESC,
			CASE WHEN @SortClause = 'CreatedDate ASC' THEN C.CreatedDate END ASC,
			CASE WHEN @SortClause = 'CreatedDate DESC' THEN C.CreatedDate END DESC,
			CASE WHEN @SortClause = 'ModifiedDate ASC' THEN S.ModifiedDate END ASC,
			CASE WHEN @SortClause = 'ModifiedDate DESC' THEN S.ModifiedDate END DESC,
			CASE WHEN @SortClause IS NULL THEN C.CreatedDate END DESC		
		) AS RowNumber
		FROM MKCampaign AS C
			JOIN vwCampaignSend S ON C.Id = S.CampaignId
			LEFT JOIN MKCampaignGroup G ON G.Id = C.CampaignGroupId
		WHERE (@Id IS NULL OR C.Id = @Id) AND
			(@Type IS NULL OR C.Type = @Type) AND 
			((@CampaignGroupId IS NULL AND S.status != 4) OR G.Id = @CampaignGroupId) AND
			(@Status IS NULL OR S.Status = @Status) AND
			(@Filter IS NULL OR 
				(
					(@Filter = 1 AND (S.Status = 2 OR S.Status = 5)) OR
					(@Filter = 2 AND (S.Status = 1 OR S.Status = 6)) OR
					(@Filter = 3 AND S.Status = 3) OR
					(@Filter = 5 AND (S.Status = 1 OR S.Status = 3 OR S.Status = 6))
				)
			) AND
			(@SiteId IS NULL OR S.SiteId = @SiteId) AND
			(@Keyword IS NULL OR C.Title like @Keyword OR C.Description like @Keyword)
		OPTION (RECOMPILE)

		;WITH CTE AS(
			SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				C.Id AS Id
			FROM MKCampaign C
				JOIN @tbIds T ON T.Id = C.Id
		)
	
		INSERT INTO @tbPagedResults
		SELECT Id, RowNumber, TotalRecords FROM CTE
		WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
			OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
			AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	END
	ELSE
	BEGIN
		INSERT INTO @tbPagedResults
		SELECT @Id, 1, 1
	END

	SELECT C.ApplicationId, 
		G.Id AS CampaignGroupId,
		C.ConfirmationEmail,
		C.Description,
		C.Id,	       
		C.SenderEmail,
		C.SenderName,
		C.ReplyToEmail,
		ISNULL(S.Status, 1) AS Status,
		C.Title,
		C.Type,	
		S.ScheduledTimeZone, 
		A.AuthorId,
		A.AutoUpdateLists,
		A.CampaignId,
		A.CostPerEmail,	      
		A.LastPublishDate,
		S.UseLocalTimeZone AS UseLocalTimeZoneToSend,       
		S.ScheduleId,
		S.SendToNotTriggered,
		S.UniqueRecipientsOnly,
		A.WorkflowId,
		A.WorkflowStatus,
		ISNULL(A.IsEditable, 1) AS IsEditable,
		S.InteractedUsersDateRange,
		S.NonInteractedUsersPercent,
		S.SendCount,
		T.TotalRecords,
		A.LastPublishDate,
		C.CreatedDate,	      
		C.ModifiedDate,
		C.CreatedBy,
		C.ModifiedBy,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName
	FROM MKCampaign C
		JOIN @tbPagedResults T ON T.Id = C.Id
		LEFT JOIN MKCampaignAdditionalInfo A ON A.CampaignId = C.Id
		LEFT JOIN MKCampaignSend S ON S.CampaignId = C.Id AND S.SiteId = @SiteId
		LEFT JOIN MKCampaignGroup G ON G.Id = C.CampaignGroupId AND G.ApplicationId = @SiteId
		LEFT JOIN VW_UserFullName CU on CU.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = S.ModifiedBy
	ORDER BY T.RowNumber
	
	SELECT E.[Id],
		[CampaignId],
		[EmailSubject],
		CASE WHEN @IgnoreDetails = 1 THEN '' ELSE [EmailHtml] END AS EmailHtml,
		[CMSPageId],
		[EmailText],
		[Sequence],
		[TimeValue],
		[TimeMeasurement],
		[EmailContentType],
		[EmailContentTypePriority]
	FROM [MKCampaignEmail] E
		JOIN @tbPagedResults T ON T.Id = E.CampaignId
	WHERE @SiteId IS NULL OR E.SiteId = @SiteId

	--Getting the contact count for each campaign
	DECLARE @tbCampaignListCount TABLE 
	(
		CampaignId			uniqueidentifier,
		ContactListId		uniqueidentifier,
		ContactListTitle	nvarchar(max),
		ContactCount		int
	)

	INSERT INTO @tbCampaignListCount
	SELECT T.Id,
		CDL.DistributionListId,
		D.Title,
		ISNULL(DLS.Count, 0)
	FROM MKCampaignDistributionList CDL
		JOIN @tbPagedResults T ON T.Id = CDL.CampaignId
		JOIN MKCampaignSend S ON S.CampaignId = T.Id AND S.SiteId = @SiteId
		JOIN TADistributionLists D on D.Id = CDL.DistributionListId
		JOIN TADistributionListSite DLS ON DLS.DistributionListId = D.Id AND DLS.SiteId = @SiteId
		JOIN MKCampaign C ON C.Id = S.CampaignId
	WHERE CDL.CampaignSendId = S.Id
		
	SELECT CampaignId, SUM(ContactCount) ContactCount FROM @tbCampaignListCount
	GROUP BY CampaignId
	
	SELECT * FROM @tbCampaignListCount

	SELECT CS.CampaignId AS CampaignId,
		S.[Id],
		S.[Title],
		S.[Description],
		CAST(CONVERT(varchar(12), S.StartTime, 101) AS dateTime) AS StartDate,
		CAST(CONVERT(varchar(12), S.EndDate, 101)  AS dateTime) AS EndDate,
		S.StartTime,
		S.EndTime, 
		S.[MaxOccurrence],
		S.[Type], 
		S.[NextRunTime], 
		S.[LastRunTime],
		S.[ScheduleTypeId],
		S.[Status],
		S.[CreatedBy],
		S.CreatedDate,
		S.[ModifiedBy],
		S.ModifiedDate
	FROM TASchedule S
		JOIN MKCampaignSend CS ON S.Id = CS.ScheduleId
		JOIN @tbPagedResults T ON T.Id = CS.CampaignId AND CS.SiteId = @SiteId
END

GO

Declare @StatusCol bit
SELECT @StatusCol = COLUMNPROPERTY(OBJECT_ID('dbo.CLImageVariant', 'U'), 'Status', 'AllowsNull') 
IF(@StatusCol = 0)
BEGIN 
ALTER TABLE CLImageVariant Alter column Status INT NULL
END 
GO

PRINT 'Create table ObjectLanguageVariants'
GO
IF(OBJECT_ID('ObjectLanguageVariants') IS NULL)
	CREATE TABLE [dbo].[ObjectLanguageVariants] (
		[Id]				UNIQUEIDENTIFIER NOT NULL,
		[ObjectId]			UNIQUEIDENTIFIER NOT NULL,
		[ObjectType]		INT NOT NULL,
		[VariantId]			UNIQUEIDENTIFIER NULL,
		[VariantUrl]		NVARCHAR(256) NULL,
		[Culture]			NVARCHAR(56) NULL
	)
GO

PRINT 'Create view vwObjectLanguageVariants'
GO
IF(OBJECT_ID('vwObjectLanguageVariants') IS NOT NULL)
	DROP VIEW vwObjectLanguageVariants
GO
CREATE VIEW [dbo].[vwObjectLanguageVariants] (MasterObjectId, VariantObjectId, ObjectId, ObjectType, SiteId, Culture, IsAutomatic, [Url])
AS
WITH VariantPages (MasterPageId, SitePageId, SiteId)
AS (
	SELECT	PD.PageDefinitionId, PD.PageDefinitionId, PD.SiteId
	FROM	PageDefinition AS PD
			LEFT JOIN PageDefinition AS PD2 ON PD2.PageDefinitionId = PD.SourcePageDefinitionId
	WHERE	NULLIF(PD.SourcePageDefinitionId, '00000000-0000-0000-0000-000000000000') IS NULL OR
			PD2.PageDefinitionId IS NULL

	UNION ALL

	SELECT	VP.MasterPageId, PD.PageDefinitionId, PD.SiteId
	FROM	PageDefinition AS PD
			INNER JOIN VariantPages AS VP ON VP.SitePageId = PD.SourcePageDefinitionId
)
SELECT		VP1.MasterPageId, PD.PageDefinitionId, VP1.SitePageId, 8, PD.SiteId, NULLIF(S.UICulture, ''), 1,
			CASE WHEN RIGHT(S.PrimarySiteUrl, 1) = '/' THEN LEFT(S.PrimarySiteUrl, LEN(S.PrimarySiteUrl) - 1) ELSE S.PrimarySiteUrl END +
			CASE
				WHEN PMN.TargetId = PD.PageDefinitionId THEN PMN.CompleteFriendlyUrl
				ELSE PMN.CompleteFriendlyUrl + '/' + PD.FriendlyName
			END
FROM		VariantPages AS VP1
			INNER JOIN VariantPages AS VP2 ON VP2.MasterPageId = VP1.MasterPageId
			INNER JOIN PageDefinition AS PD ON PD.PageDefinitionId = VP2.SitePageId
			INNER JOIN PageMapNodePageDef AS PMNPD ON PMNPD.PageDefinitionId = PD.PageDefinitionId
			INNER JOIN PageMapNode AS PMN ON PMN.PageMapNodeId = PMNPD.PageMapNodeId
			INNER JOIN SISite AS S ON S.Id = PD.SiteId
WHERE		S.[Status] = 1 AND
			PD.PageStatus = 8 AND
			PMN.MenuStatus = 0 AND
			NULLIF(PMN.CompleteFriendlyUrl, '') IS NOT NULL AND
			NULLIF(S.UICulture, '') IS NOT NULL

UNION ALL

SELECT		DISTINCT VP1.MasterPageId, OLV.Id, VP1.SitePageId, 8, NULL, OLV.Culture, 0, OLV.VariantUrl
FROM		VariantPages AS VP1
			INNER JOIN VariantPages AS VP2 ON VP2.MasterPageId = VP1.MasterPageId
			INNER JOIN ObjectLanguageVariants AS OLV ON OLV.ObjectType = 8 AND OLV.ObjectId = VP2.SitePageId
WHERE		OLV.VariantUrl IS NOT NULL AND
			OLV.Culture IS NOT NULL

UNION ALL

SELECT		DISTINCT VP1.MasterPageId, OLV.Id, VP1.SitePageId, 8, PD.SiteId, NULLIF(S.UICulture, ''), 0,
			CASE WHEN RIGHT(S.PrimarySiteUrl, 1) = '/' THEN LEFT(S.PrimarySiteUrl, LEN(S.PrimarySiteUrl) - 1) ELSE S.PrimarySiteUrl END +
			CASE
				WHEN PMN.TargetId = PD.PageDefinitionId THEN PMN.CompleteFriendlyUrl
				ELSE PMN.CompleteFriendlyUrl + '/' + PD.FriendlyName
			END
FROM		VariantPages AS VP1
			INNER JOIN VariantPages AS VP2 ON VP2.MasterPageId = VP1.MasterPageId
			INNER JOIN ObjectLanguageVariants AS OLV ON OLV.ObjectType = 8 AND OLV.ObjectId = VP2.SitePageId
			INNER JOIN PageDefinition AS PD ON PD.PageDefinitionId = OLV.VariantId
			INNER JOIN PageMapNodePageDef AS PMNPD ON PMNPD.PageDefinitionId = PD.PageDefinitionId
			INNER JOIN PageMapNode AS PMN ON PMN.PageMapNodeId = PMNPD.PageMapNodeId
			INNER JOIN SISite AS S ON S.Id = PD.SiteId
WHERE		OLV.VariantId IS NOT NULL AND
			NULLIF(S.UICulture, '') IS NOT NULL

UNION ALL

SELECT		DISTINCT P.Id, P.Id, P.Id, 205, S.Id, NULLIF(S.UICulture, '') AS Culture, 1, 
			CASE WHEN RIGHT(S.PrimarySiteUrl, 1) = '/' THEN LEFT(S.PrimarySiteUrl, LEN(S.PrimarySiteUrl) - 1) ELSE S.PrimarySiteUrl END +
			CASE WHEN LEFT(P.ProductUrl, 1) = '/' THEN P.ProductUrl ELSE '/' + P.ProductUrl END
FROM		vwProduct AS P
			INNER JOIN SISite AS S ON S.Id = P.SiteId
WHERE		S.[Status] = 1 AND
			P.IsActive = 1 AND
			NULLIF(P.ProductUrl, '') IS NOT NULL

UNION ALL

SELECT		DISTINCT P.Id, OLV.Id, P.Id, 205, NULL, OLV.Culture, 0, OLV.VariantUrl
FROM		vwProduct AS P
			INNER JOIN SISite AS S ON S.Id = P.SiteId
			INNER JOIN ObjectLanguageVariants AS OLV ON OLV.ObjectType = 205 AND OLV.ObjectId = P.Id
WHERE		S.[Status] = 1 AND
			P.IsActive = 1 AND
			NULLIF(P.ProductUrl, '') IS NOT NULL AND
			OLV.VariantUrl IS NOT NULL AND
			OLV.Culture IS NOT NULL
GO

PRINT 'Modify stored procedure PageMap_SavePageDefinition'
GO
IF(OBJECT_ID('PageMap_SavePageDefinition') IS NOT NULL)
	DROP PROCEDURE PageMap_SavePageDefinition
GO
CREATE PROCEDURE [dbo].[PageMap_SavePageDefinition]
(
	@Id					uniqueidentifier output,
	@SiteId				uniqueidentifier,
	@ParentNodeId		uniqueidentifier,
	@CreatedBy			uniqueidentifier,
	@ModifiedBy			uniqueidentifier,
	@PublishPage		bit,
	@PageDefinitionXml	xml,
	@ContainerXml		xml = null,
	@UnmanagedContentXml xml = null,
	@ZonesXml			xml = null,
	@UpdatePageName bit = null
)
AS
BEGIN
	
	IF(@PublishPage = 1)
		SET @UpdatePageName = 1
		
	IF(@UpdatePageName IS NULL)
		SET @UpdatePageName = 0
	
		
	IF NOT EXISTS(Select 1 from PageMapNode Where PageMapNodeId = @ParentNodeId and SiteId = @SiteId)
	BEGIN
		RAISERROR('NOTEXISTS||ParentId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	DECLARE @pageDefinitionSegmentIds table(Id uniqueidentifier)	
	DECLARE @PublishCount int
	DECLARE @PublishDate datetime
	DECLARE @PageDefinitionSegmentId uniqueidentifier 
	
	IF @PublishPage = 1
		SET @PublishDate = GETUTCDATE()
	ELSE
		SET @PublishDate = @PageDefinitionXml.value('(pageDefinition/@publishDate)[1]','datetime')
		
	DECLARE @ActionType int
	SET @ActionType = 2
	IF @Id IS NULL OR (Select count(*) FROM PageDefinition Where PageDefinitionId=@Id)=0 -- New Page
	BEGIN
		SET @ActionType = 1
		-- get the highest value of the DisplayOrder currently in the DB for this menuId
		DECLARE @CurrentDisplayOrder int
		SELECT @CurrentDisplayOrder = ISNULL(MAX(DisplayOrder),0) FROM PageMapNodePageDef M WHERE M.PageMapNodeId = @ParentNodeId
		IF @CurrentDisplayOrder IS NULL
			SET @CurrentDisplayOrder = 1
		
		IF @PublishPage = 1
			SET @PublishCount = 1
		ELSE
			SET @PublishCount = 0
		IF @Id IS NULL 	
			SET @Id = NEWID()

		IF NOT EXISTS (Select 1 from PageDefinition Where PageDefinitionId =@Id)
			INSERT INTO PageDefinition(PageDefinitionId, SiteId, TemplateId, Title, Description, 
						PublishDate, PageStatus, WorkflowState, PublishCount, --ContainerXml,UnmanagedContentXml,
						FriendlyName, WorkflowId,
						CreatedBy, CreatedDate, ArchivedDate, StatusChangedDate, AuthorId, EnableOutputCache,
						OutputCacheProfileName, ExcludeFromSearch,ExcludeFromExternalSearch, IsDefault, IsTracked, HasForms, TextContentCounter, 
						CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,ScheduledPublishDate,ScheduledArchiveDate,NextVersionToPublish,PageSegmentCount)
			VALUES(@Id,  @SiteId, 
					@PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)'), 
					@PublishDate,
					@PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
					@PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'),
					@PublishCount,
					--@ContainerXml,
					--isnull(@UnmanagedContentXml,'<unmanagedPageContents/>'),
					@PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
					@CreatedBy,
					GETUTCDATE(),
					@PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(1000)'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromExternalSearch)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
					dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
					@PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
					isnull(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
					@PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier'),
					1
				)
				
		IF NOT EXISTS (Select 1 from PageMapNodePageDef Where PageDefinitionId =@Id AND PageMapNodeId=@ParentNodeId)
			INSERT INTO PageMapNodePageDef(PageDefinitionId,PageMapNodeId,DisplayOrder)
			VALUES (@Id,@ParentNodeId,@CurrentDisplayOrder + 1)
			
			
		INSERT INTO PageDefinitionSegment(
			Id
			,PageDefinitionId
			,DeviceId
			,AudienceSegmentId
			,UnmanagedXml
			,ZonesXml
			)
			 output inserted.Id into @pageDefinitionSegmentIds
		VALUES (
			NEWID()
			,@Id
			,dbo.GetDesktopDeviceId(@SiteId)
			,dbo.GetDesktopAudienceSegmentId(@SiteId)
			,isnull(@UnmanagedContentXml,'<unmanagedPageContents/>')
			,@ZonesXml
			)
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			INSERT INTO [PageDefinitionContainer]
				   (
					[PageDefinitionSegmentId]
				   ,[PageDefinitionId]
				   ,[ContainerId]
				   ,[ContentId]
				   ,[InWFContentId]
				   ,[ContentTypeId]
				   ,[IsModified]
				   ,[IsTracked]
				   ,[Visible]
				   ,[InWFVisible]
				   ,[DisplayOrder]
				   ,[InWFDisplayOrder]
				   ,[CustomAttributes]
				   ,ContainerName)
			 SELECT
					S.Id -- as this would be called only for desktop version, we can come in a better way
					,@Id
					,X.value('(@id)[1]','uniqueidentifier') as ContainerId
					,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
					,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
					,X.value('(@contentTypeId)[1]','int') as contentTypeId
					,X.value('(@isModified)[1]','bit') as isModified
					,X.value('(@isTracked)[1]','bit') as isTracked
					,X.value('(@visible)[1]','bit') as Visible
					,X.value('(@inWFVisible)[1]','bit') as InWFVisible
					,X.value('(@displayOrder)[1]','int') as DisplayOrder
					,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
					,dbo.GetCustomAttributeXml(X.query('.'),'container')
					,X.value('(@name)[1]','nvarchar(100)') as ContainerName
			From @ContainerXml.nodes('/Containers/container') temp(X)
			CROSS JOIN @pageDefinitionSegmentIds S
		END
	END
	ELSE
	BEGIN
		SELECT @PublishCount = PublishCount FROM PageDefinition WHERE PageDefinitionId = @Id
		IF @PublishPage = 1
			SELECT @PublishCount = @PublishCount + 1
			
		UPDATE PageDefinition Set 
			Title = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)') ELSE title END,
			TemplateId =@PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
			description = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)') ELSE description END, 
			publishDate = @PublishDate, 
			PageStatus = @PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
			WorkflowState = @PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'), 
			PublishCount = @PublishCount,
			--ContainerXml = @containerXml,
			--UnmanagedContentXml = isnull(@unmanagedContentXml, UnmanagedContentXml),
			FriendlyName = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)') ELSE FriendlyName END,
			WorkflowId = @PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = GETUTCDATE(),
			ArchivedDate = @PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
			StatusChangedDate = @PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
			AuthorId = @PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
			EnableOutputCache = @PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
			OutputCacheProfileName = @PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(100)'),
			ExcludeFromSearch = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
		    ExcludeFromExternalSearch = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromExternalSearch)[1]','bit'),
			IsDefault = @PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
			IsTracked = @PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
			HasForms = @PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
			TextContentCounter = @PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
			SourcePageDefinitionId = @PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
			CustomAttributes = dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
			IsInGroupPublish  =ISNULL(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
			ScheduledPublishDate = @PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
			ScheduledArchiveDate = @PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
			NextVersionToPublish =@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier')
		WHERE PageDefinitionId = @Id AND SiteId = @SiteId
		
		DECLARE @DeskTopDeviceId uniqueidentifier
		DECLARE @DeskTopAudienceId uniqueidentifier
		SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@SiteId)
		SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@SiteId)
		
		SELECT @PageDefinitionSegmentId = Id 
			FROM PageDefinitionSegment WHERE DeviceId = @DeskTopDeviceId AND 
				AudienceSegmentId = @DeskTopAudienceId AND
				PageDefinitionId = @Id
		
		UPDATE PageDefinitionSegment Set 
			UnmanagedXml = isnull(@unmanagedContentXml, UnmanagedXml),
			ZonesXml = isnull(@ZonesXml, ZonesXml)
		WHERE PageDefinitionId = @Id AND Id = @PageDefinitionSegmentId
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			UPDATE [PageDefinitionContainer] SET 
			   [ContentId] = X.value('(@contentId)[1]','uniqueidentifier')
			  ,[InWFContentId] = X.value('(@inWFContentId)[1]','uniqueidentifier')
			  ,[ContentTypeId] = X.value('(@contentTypeId)[1]','int') 
			  ,[IsModified] = X.value('(@isModified)[1]','bit')
			  ,[IsTracked] = X.value('(@isTracked)[1]','bit')
			  ,[Visible] = X.value('(@visible)[1]','bit')
			  ,[InWFVisible] = X.value('(@inWFVisible)[1]','bit')
			  ,[CustomAttributes] = dbo.GetCustomAttributeXml(X.query('.'),'container')
			  ,[DisplayOrder] = X.value('(@displayOrder)[1]','int')
			  ,[InWFDisplayOrder] = X.value('(@inWFDisplayOrder)[1]','int')
			From @ContainerXml.nodes('/Containers/container') temp(X)
			WHERE [PageDefinitionId] = @Id 
			AND PageDefinitionSegmentId = @PageDefinitionSegmentId
			AND [ContainerId] = X.value('(@id)[1]','uniqueidentifier')
			--If any containers added to pagedefinition insert those(ex. marketier template switching)
			INSERT INTO [PageDefinitionContainer]
						   (
							[PageDefinitionSegmentId]
						   ,[PageDefinitionId]
						   ,[ContainerId]
						   ,[ContentId]
						   ,[InWFContentId]
						   ,[ContentTypeId]
						   ,[IsModified]
						   ,[IsTracked]
						   ,[Visible]
						   ,[InWFVisible]
						   ,[DisplayOrder]
						   ,[InWFDisplayOrder]
						   ,[CustomAttributes]
						   ,[ContainerName])
					 SELECT
							@PageDefinitionSegmentId
							,@Id
							,X.value('(@id)[1]','uniqueidentifier') as ContainerId
							,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
							,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
							,X.value('(@contentTypeId)[1]','int') as contentTypeId
							,X.value('(@isModified)[1]','bit') as isModified
							,X.value('(@isTracked)[1]','bit') as isTracked
							,X.value('(@visible)[1]','bit') as Visible
							,X.value('(@inWFVisible)[1]','bit') as InWFVisible
							,X.value('(@displayOrder)[1]','int') as DisplayOrder
							,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
							,dbo.GetCustomAttributeXml(X.query('.'),'container')
							,X.value('(@name)[1]','nvarchar(100)') as ContainerName
					From @ContainerXml.nodes('/Containers/container') temp(X)					
					where X.value('(@id)[1]','uniqueidentifier') not in (select ContainerId from PageDefinitionContainer where PageDefinitionSegmentId=@PageDefinitionSegmentId and PageDefinitionId=@Id)
		
		END
		
	END
	
	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO USSecurityLevelObject 
		SELECT @Id, 8, SL.Id
	FROM dbo.SplitComma(@PageDefinitionXml.value('(pageDefinition/@securityLevels)[1]','nvarchar(max)'), ',') S 
		JOIN USSecurityLevel SL ON S.Value = SL.Id
	
	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO SIPageStyle 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'style')
	FROM @PageDefinitionXml.nodes('/pageDefinition/style') T(C) JOIN SIStyle S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO SIPageScript 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'script')
	FROM @PageDefinitionXml.nodes('/pageDefinition/script') T(C) JOIN SIScript S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	UPDATE D SET D.PageSegmentCount = T.Cnt
	FROM PageDefinition D
	INNER JOIN (SELECT PageDefinitionId, Count(PageDefinitionId) cnt FROM PageDefinitionSegment
	Where PageDefinitionId=@Id
	Group by PageDefinitionId
	) T
	ON T.PageDefinitionId =D.PageDefinitionId 


	EXEC [PageDefinition_AdjustDisplayOrder] @ParentNodeId

	EXEC PageMapBase_UpdateLastModification @SiteId
	EXEC PageMap_UpdateHistory @SiteId, @Id, 8, @ActionType

	EXEC [PageDefinition_UpdateAssetReference] @Id
END
GO

PRINT 'Modify stored procedure Sku_GetEffectivePrice'
GO
IF(OBJECT_ID('Sku_GetEffectivePrice') IS NOT NULL)
	DROP PROCEDURE Sku_GetEffectivePrice
GO
CREATE PROCEDURE [dbo].[Sku_GetEffectivePrice](
			 @CustomerId uniqueidentifier = null,
			 @SKUIdQuantityList XML =null,
			 @ApplicationId uniqueidentifier=null
										)

AS

BEGIN

	Declare @SKUQuantity TYSKUQuantity
	Declare @output table(SkuId uniqueidentifier,ProductId uniqueidentifier,MinQuantity decimal(18,4),MaxQuantity float,PriceSetId uniqueidentifier null,Issale bit,EffectivePrice  decimal(18,4),ListPrice  decimal(18,4))

	INSERT INTO @SKUQuantity(SkuId,Quantity)
		Select tab.col.value('(key/guid/text())[1]','uniqueidentifier'),tab.col.value('(value/double/text())[1]','decimal(18,2)')
		From @SKUIdQuantityList.nodes('dictionary/SerializableDictionary/item') tab(col)
	INSERT INTO @output
	EXEC [dbo].[Sku_GetPrice] @CustomerId=@CustomerId,@SKUQuantity=@SKUQuantity,@ApplicationId=@ApplicationId

	select * from @output where PriceSetId IS NOT NULL

-- in any scenario - return List Price for all the SKUs		
		Select ProductId, SkuId, ListPrice 
			From @output 
			
END


GO

Print 'Updating IsShared Column'
Declare @IsSharedCol bit
SELECT @IsSharedCol = COLUMNPROPERTY(OBJECT_ID('dbo.PRProduct', 'U'), 'IsShared', 'AllowsNull') 
IF(@IsSharedCol = 1)
BEGIN 
	PRINT 'Updating IsShared'
	Update PRProduct Set IsShared = 1 where IsShared IS NULL
	Alter table PRProduct Alter column IsShared bit NOT NULL 
	Alter table PRProduct ADD Constraint DF_ProductIsShared Default 1 For ISShared
END 
GO

PRINT 'Creating OrderShipment_Save'
GO
IF(OBJECT_ID('OrderShipment_Save') IS NOT NULL)
	DROP PROCEDURE  OrderShipment_Save
GO	
CREATE PROCEDURE [dbo].[OrderShipment_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@OrderShippingId	uniqueidentifier,
	@ShippingOptionId	uniqueidentifier,
	@ModifiedBy       	uniqueidentifier, 
	@EstimatedShipmentTotal money,	
	@ShipmentStatus int,
	@WarehouseId uniqueidentifier,
	@ShipmentNumber nvarchar(50)=null,
	@ExternalReferenceNumber nvarchar(50)=null OUT,
	@Random1 varchar(10)=null,
	@Random2 varchar(10)=null
)
AS
BEGIN

-- Delete All Non Shipped Packages.
--Exec OrderShipping_DeleteNonShippedPackages @OrderShippingId

	IF (@Id is null OR @Id =dbo.GetEmptyGUID())
	BEGIN
		Set @Id =newid()
	END
	
	IF Not Exists(SELECT Id FROM FFOrderShipment Where Id = @Id)
	BEGIN	
	IF EXISTS (Select 1 from FFOrderShipment Where ExternalReferenceNumber =@ExternalReferenceNumber)
	BEGIN
	IF @Random1 IS NOT NULL AND NOT EXISTS (Select 1 from FFOrderShipment Where ExternalReferenceNumber =@Random1)
		SET @ExternalReferenceNumber =@Random1
	ELSE IF @Random2 IS NOT NULL AND NOT EXISTS (Select 1 from FFOrderShipment Where ExternalReferenceNumber =@Random2)
		SET @ExternalReferenceNumber =@Random2	
	ELSE
		BEGIN
		DECLARE @Prefix char(4)
		SET @Prefix = SUBSTRING(@ExternalReferenceNumber,0,4)
		select @ExternalReferenceNumber =	@Prefix + CONVERT(char(6), right(newid(),6))
		While(NOT EXISTS (Select 1 from FFOrderShipment Where ExternalReferenceNumber =@ExternalReferenceNumber))
			BEGIN
				select @ExternalReferenceNumber =	@Prefix + CONVERT(char(6), right(newid(),6))
			END
		END	
	END
		INSERT INTO [dbo].[FFOrderShipment]
			   ([Id]
			   ,[OrderShippingId]
			   ,[ShippedDate]
			   ,[ShippingOptionId]
			   ,[CreateDate]
			   ,[CreatedBy]
			   ,[EstimatedShipmentTotal]				   
			   ,ShipmentStatus
			   ,WarehouseId
				,ExternalReferenceNumber
			   )
		 VALUES
			   (@Id
			   ,@OrderShippingId
			   ,GetUTCDate()
			   ,@ShippingOptionId
			   ,GetUTCDate()
			   ,@ModifiedBy
			   ,IsNull(@EstimatedShipmentTotal, 0)
			   ,@ShipmentStatus
			   ,@WarehouseId
				,@ExternalReferenceNumber)
	END
	ELSE
	BEGIN
		UPDATE [dbo].[FFOrderShipment]
		SET
		   [OrderShippingId] = ISNULL(@OrderShippingId,[OrderShippingId])
		   ,[ShippedDate] = GetUTCDate()
		   ,[ShippingOptionId] =  ISNULL(@ShippingOptionId,[ShippingOptionId])
		   ,[ModifiedDate] = GetUTCDate()
		   ,[ModifiedBy] = @ModifiedBy
		   ,[EstimatedShipmentTotal] = IsNull(@EstimatedShipmentTotal, 0)
		   ,[ShipmentStatus] =  ISNULL(@ShipmentStatus,[ShipmentStatus])
		   ,[WarehouseId]= ISNULL(@WarehouseId,[WarehouseId])
			,[ExternalReferenceNumber] =  ISNULL(@ExternalReferenceNumber,[ExternalReferenceNumber])
		WHERE [Id] = @Id			   
	END


	if @@error <> 0
	begin
		raiserror('DBERROR||%s',16,1,'Insert OrderShipment')
		return dbo.GetDataBaseErrorCode()	
	end
			

END


GO
PRINT 'Creating OrderShipment_UpdateInventory'
GO
IF(OBJECT_ID('OrderShipment_UpdateInventory') IS NOT NULL)
	DROP PROCEDURE  OrderShipment_UpdateInventory
GO	
CREATE PROCEDURE [dbo].[OrderShipment_UpdateInventory]
(
	@OrderId			uniqueidentifier,
	@OrderShipmentId	uniqueidentifier,
	@ModifiedBy       	uniqueidentifier
)
AS
BEGIN
	DECLARE @ProductSkuId uniqueidentifier
	DECLARE @OrderStatusId int
	DECLARE @count1 int
	DECLARE @count2 int
	Declare @OrderShippingId uniqueidentifier

	Select @OrderShippingId = OrderShippingId from FFOrderShipment Where Id=@OrderShipmentId
	
	SELECT @OrderStatusId =OrderStatusId 
	from OROrder
	Where Id=@OrderId

	IF @OrderStatusId=1 OR @OrderStatusId=11 OR @OrderStatusId=4
	BEGIN
		Select @count1 = count(*) 
		from OROrderItem
		Where OrderId=@OrderId
			
		Select @count2=  count(*) 
		from OROrderItem
		Where OrderId=@OrderId AND OrderItemStatusId=2

		IF(@count1 = @count2)
		BEGIN
			Update OROrder Set OrderStatusId=2
			Where Id=@OrderId	

		END
		ELSE IF(@count2>0 OR (Select count(*) 
			from OROrderItem
			Where OrderId=@OrderId AND OrderItemStatusId=7) >0)
		BEGIN
			IF(@OrderStatusId<>4)
			BEGIN
				Update OROrder Set OrderStatusId=11
				Where Id=@OrderId
			END
		END
		IF EXISTS (Select 1 FROM OROrderItem Where OrderShippingId=@OrderShippingId AND OrderItemStatusId NOT IN (2,3))
			UPDATE OROrderShipping SET Status=7 Where Id =@OrderShippingId
		ELSE 
			UPDATE OROrderShipping SET Status=2 Where Id =@OrderShippingId
	END

	UPDATE SK  SET SK.PreviousSoldCount = SK.PreviousSoldCount + SI.Quantity
	from OROrderItem OI
	Inner jOin FFOrderShipmentItem SI on SI.OrderItemId= Oi.Id
	Inner Join FFOrderShipment S on S.Id =SI.OrderShipmentId
	Inner Join PRProductSKU SK on SK.Id =OI.ProductSKUId
	Where S.Id =@OrderShipmentId

	DECLARE @tblItems TABLE(inventoryId UNIQUEIDENTIFIER, quantity DECIMAL)

	INSERT INTO @tblItems
	SELECT I.Id, SUM(SI.Quantity)
	FROM FFOrderShipment S
	INNER JOIN FFOrderShipmentItem SI  ON S.Id = SI.OrderShipmentId
	INNER JOIN OROrderItem OI ON OI.Id = SI.OrderItemId
	INNER JOIN PRProductSKU SK on SK.Id = OI.ProductSKUId
	INNER JOIN INInventory I on I.SKUId =SK.Id AND I.WarehouseId = S.WarehouseId
	GROUP BY SK.ID,S.WarehouseId,I.Id,sk.IsUnlimitedQuantity,S.Id
	HAVING S.Id = @OrderShipmentId AND  SK.IsUnlimitedQuantity = 0	

		if (Select count(*) from @tblItems)>0
		begin
			insert into INInventoryLog
			(
				Id,
				SKUId,
				InventoryId,
				OldQuantity,
				NewQuantity,
				ReasonId,
				ReasonDetail,
				ModifiedDate,
				ModifiedBy			
			)
			Select
				newid(),
				I.SKUId,
				I.Id,
				I.Quantity,
				AD.Quantity,
				'9194342A-3AC3-4246-BAD8-C9E6A547B30A',
				'New Shipment - Order Fullfilled',
				GETUTCDATE(),
				@ModifiedBy		
				from INInventory I
				Inner Join @tblItems AD on I.Id = AD.inventoryId
		end

	UPDATE I SET I.Quantity = I.Quantity- AD.Quantity
	FROM dbo.INInventory I INNER JOIN @tblItems AD
	ON I.Id = AD.inventoryId
END
GO

PRINT 'Modify view vwAbandonedCheckout'
GO
IF(OBJECT_ID('vwAbandonedCheckout') IS NOT NULL)
	DROP VIEW vwAbandonedCheckout
GO
CREATE VIEW [dbo].[vwAbandonedCheckout]
AS
SELECT	AC.CustomerId, AC.CartId, AC.CartTotal, AC.FullName, AC.Email, AC.ModifiedDate, AC.ExpirationDate, AC.SiteId
FROM	ORAbandonedCheckout AS AC
		INNER JOIN USCommerceUserProfile AS CUP ON CUP.Id = AC.CustomerId AND CUP.IsExpressCustomer = 0

UNION

SELECT	CUP.Id AS CustomerId, CT.Id AS CartId, O.OrderTotal AS CartTotal, U.LastName + ',' + U.FirstName AS FullName,
		M.Email, ISNULL(CT.ModifiedDate,CT.CreatedDate) AS ModifiedDate, CT.ExpirationDate, CT.SiteId
FROM	CSCart AS CT
		INNER JOIN OROrder AS O ON O.CartId = CT.Id
		INNER JOIN dbo.USUser AS U ON (CT.CustomerId = U.Id OR O.CustomerId = U.Id)
		INNER JOIN dbo.USCommerceUserProfile AS CUP ON CUP.Id = U.Id AND CUP.IsExpressCustomer = 0
		INNER JOIN dbo.USMembership AS M ON M.UserId = U.Id
		INNER JOIN dbo.USSiteUser AS SU ON SU.UserId = U.Id	
WHERE   O.OrderStatusId = 12
GO

PRINT 'Modify view vwShippingOption'
GO
IF(OBJECT_ID('vwShippingOption') IS NOT NULL)
	DROP VIEW vwShippingOption
GO
CREATE VIEW vwShippingOption 
AS
SELECT SO.[Id],
	SO.[ShipperId],
	SO.[Name],
	SO.[Description],
	SO.[CustomId],
	SO.[NumberOfDaysToDeliver],
	SO.[CutOffHour],
	SO.[SaturdayDelivery],
	SO.[AllowShippingCoupon],
	SO.[IsPublic],
	SO.[IsInternational],
	SO.[ServiceCode],
	SO.[BaseRate],
	SO.[SiteId],
	SO.[Sequence],
	SO.[Status],
	SO.CreatedDate,
	SO.CreatedBy AS CreatedById,
	CFN.UserFullName AS CreatedByFullName,    
	SO.ModifiedDate,
	SO.ModifiedBy AS ModifiedById,     
	MFN.UserFullName AS ModifiedByFullName,    
	SO.[ExternalShipCode],
	O.IsShared
FROM [dbo].[ORShippingOption] SO
	INNER JOIN ORShipper O ON O.Id = SO.ShipperId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = SO.CreatedBy    
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = SO.ModifiedBy  
WHERE O.IsShared = 0

UNION ALL

SELECT SO.[Id],
	SO.[ShipperId],
	SO.[Name],
	SO.[Description],
	SO.[CustomId],
	SO.[NumberOfDaysToDeliver],
	SO.[CutOffHour],
	SO.[SaturdayDelivery],
	SO.[AllowShippingCoupon],
	SO.[IsPublic],
	SO.[IsInternational],
	SO.[ServiceCode],
	SO.[BaseRate],
	CS.Id AS [SiteId],
	SO.[Sequence],
	SO.[Status],
	SO.CreatedDate,
	SO.CreatedBy AS CreatedById,
	CFN.UserFullName AS CreatedByFullName,    
	SO.ModifiedDate,
	SO.ModifiedBy AS ModifiedById,     
	MFN.UserFullName AS ModifiedByFullName,    
	SO.[ExternalShipCode],
	O.IsShared
FROM [dbo].[ORShippingOption] SO
	INNER JOIN ORShipper O ON O.Id = SO.ShipperId
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = SO.CreatedBy    
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = SO.ModifiedBy  
	CROSS APPLY SISite CS
	INNER JOIN SISite S ON SO.SiteId = S.Id
WHERE O.IsShared = 1 AND CS.Status = 1
	AND CS.LftValue >= S.LftValue AND CS.RgtValue <= S.RgtValue AND S.MasterSiteId = CS.MasterSiteId
GO

PRINT 'Modify view vwAttributeEnum'
GO
IF(OBJECT_ID('vwAttributeEnum') IS NOT NULL)
	DROP VIEW vwAttributeEnum
GO
CREATE VIEW [dbo].[vwAttributeEnum]
AS
WITH CTE AS
(
	SELECT E.Id, 
		A.SiteId,
		E.AttributeID,
		E.Title,
		ISNULL(E.ModifiedDate, E.CreatedDate) AS ModifiedDate, 
		ISNULL(E.ModifiedBy, E.CreatedBy) AS  ModifiedBy
	FROM ATAttributeEnum AS E
		JOIN ATAttribute A ON A.Id = E.AttributeID

	UNION ALL

	SELECT E.Id, 
		E.SiteId,
		E.AttributeId,
		E.Title,
		E.ModifiedDate, 
		E.ModifiedBy
	FROM ATAttributeEnumVariantCache AS E
)

SELECT E.Id, 
	C.SiteId,
	A.SiteId AS SourceSiteId,
	C.Title,
	E.Code,
	E.Status,
	E.NumericValue,
	E.Sequence,
	E.Value,
	A.Id AS AttributeId,
	E.IsDefault,
	E.CreatedDate, 
	E.CreatedBy, 
	CFN.UserFullName AS CreatedByFullName,
	C.ModifiedDate, 
	C.ModifiedBy, 
	MFN.UserFullName AS ModifiedByFullName
FROM CTE C
	INNER JOIN ATAttributeEnum AS E ON E.Id = C.Id
	INNER JOIN ATAttribute AS A ON A.Id = E.AttributeID
	LEFT JOIN VW_UserFullName AS CFN ON CFN.UserId = E.CreatedBy
	LEFT JOIN VW_UserFullName AS MFN ON MFN.UserId = C.ModifiedBy
WHERE E.Status = 1

 GO

Print 'Executing Migration script to update Attribute variant cache table for variant site. '
IF NOT EXISTS (Select top 1 Id from ATAttributeVariantCache )
BEGIN 
	Declare @Sites table 
	(
		SiteId uniqueidentifier,
		ParentSiteId uniqueidentifier,
		LftValue int,
		IsProcessed bit
	)

	INSERT INTO @Sites 
	select Id, ParentSiteId, LftValue, 0 from SISIte where Id != MasterSiteId and Status = 1
	Order by LftValue

	Declare @SiteId uniqueidentifier
	Declare @ParentSiteId uniqueidentifier

	WHILE EXISTS(Select 1 from @Sites where IsProcessed = 0)
	BEGIN 
		Select top 1  @SiteId = SiteId, @ParentSiteId = ParentSiteId from @Sites where IsProcessed = 0 Order by LftValue
		
		INSERT INTO [dbo].[ATAttributeVariantCache]
			   ([Id]
			   ,[SiteId]
			   ,[Title]
			   ,[Description]
			   ,[ModifiedDate]
			   ,[ModifiedBy])
			SELECT DISTINCT A.Id,
				@SiteId,
				A.Title,
				A.Description,
				A.ModifiedDate,
				A.ModifiedBy
			FROM vwAttribute A WHERE A.SiteId = @ParentSiteId

			INSERT INTO [dbo].[ATAttributeEnumVariantCache]
			([Id]
			,[SiteId]
			,[AttributeId]
			,[Title]
			,[ModifiedDate]
			,[ModifiedBy])
				SELECT A.[Id]
			  ,@SiteId
			  ,A.AttributeId
			  ,A.Title
			  ,ModifiedDate
			  ,ModifiedBy
			FROM vwAttributeEnum A  WHERE A.SiteId = @ParentSiteId
		
		Update @Sites Set IsProcessed = 1 where SiteId = @SiteId		
	
	END
END
GO

PRINT 'Migration for Filter query' --Using new view 
IF EXISTS (Select 1 from NVFilterQuery where ActualQuery like '%VWProductAttribute%')
BEGIN 
	Update NVFilterQuery Set ActualQuery = Replace(ActualQuery, 'VWProductAttribute', 'vwProductAndSKUAttributeValue')
END 
GO


PRINT 'Creating cache_RefreshProductMinEffectivePrice'
GO
IF(OBJECT_ID('cache_RefreshProductMinEffectivePrice') IS NOT NULL)
	DROP PROCEDURE  cache_RefreshProductMinEffectivePrice
GO
CREATE PROCEDURE [dbo].[cache_RefreshProductMinEffectivePrice](@ApplicationId uniqueidentifier = null)
AS
BEGIN

	Declare @DefaultGroupId uniqueidentifier
	SET @DefaultGroupId=  (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
	where STT.Name='CommerceEveryOneGroupId' and --STS.SiteId=IsNull(@ApplicationId,STS.SiteId))
		(@ApplicationId is null or STS.SiteId = @ApplicationId))

	declare @tempPriceTab table (CustomerGroupId uniqueidentifier, ProductId uniqueidentifier,
						  MinPricedSKU uniqueidentifier, MinimumEffectivePrice money)

	declare @defaultPriceTab table (CustomerGroupId uniqueidentifier, ProductId uniqueidentifier,
				MinPricedSKU uniqueidentifier, MinimumEffectivePrice money)
	print 'starting insert to @tempPriceTab'
		Insert into @tempPriceTab
		Select 	EFPS.CustomerGroupId,PSKU.ProductId, PSKU.Id as MinPricedSKUId,
			MinimumEffectivePrice=
			Case WHEN PSKU.ListPrice < IsNull(EFPS.EffectivePrice, PSKU.ListPrice) THEN PSKU.ListPrice
			ELSE IsNull(EFPS.EffectivePrice, PSKU.ListPrice)
			end
		FROM
		(
			Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
				PSC.IsSale, PSC.EffectivePrice,
				PSC.CustomerGroupId,
				(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId ORDER BY PSC.EffectivePrice ASC))  as Sno1,
				(ROW_NUMBER() OVER(PARTITION BY PSC.ProductId ORDER BY PSC.EffectivePrice ASC))  as Sno2
			from dbo.cache_FlatPriceSets PSC 
			Where 
			
				(MinQuantity <= 1 and MaxQuantity >= 1)	
		) EFPS
			Inner join PRProductSKU PSKU on EFPS.SkuId = PSKU.Id
		WHERE EFPS.Sno1=1	and EFPS.Sno2=1
		--Select Distinct ProductId from @tempPriceTab
		print 'starting insert to @defaultPriceTab'
		Insert into @defaultPriceTab
		Select @DefaultGroupId, MPSKU.ProductId, MPSKU.Id as MinPricedSKUId,
		   MinimumEffectivePrice = MPSKU.ListPrice
		FROM 
			(
				Select ProductId, Id, ListPrice,
						(ROW_NUMBER() OVER(PARTITION BY ProductId ORDER BY ListPrice ASC))  as Sno1
				from PRProductSKU 
			 ) MPSKU
		WHERE MPSKU.Sno1 =1 
	
		
	print 'starting truncate cache_ProductMinEffectivePrice '
	truncate table cache_ProductMinEffectivePrice 
	
	print 'starting insert from @defaultPriceTab'
	insert into cache_ProductMinEffectivePrice
	SELECT CustomerGroupId, ProductId, MinPricedSKU, MinimumEffectivePrice
		FROM 
			(
				Select CustomerGroupId, ProductId, MinPricedSKU, MinimumEffectivePrice,
						(ROW_NUMBER() OVER(PARTITION BY ProductId ORDER BY MinimumEffectivePrice ASC))  as Sno1
				FROM
				(select CustomerGroupId, ProductId, MinPricedSKU, MinimumEffectivePrice from @defaultPriceTab
				UNION ALL
				select CustomerGroupId, ProductId, MinPricedSKU, MinimumEffectivePrice from @tempPriceTab)T
			 ) MPSKU 
		WHERE MPSKU.Sno1 =1 

		
END
GO

IF(OBJECT_ID('Commerce_GetURLForSiteMap') IS NOT NULL)
	DROP PROCEDURE Commerce_GetURLForSiteMap
GO
CREATE PROCEDURE [dbo].[Commerce_GetURLForSiteMap]
(
	@SiteId		uniqueidentifier
)
AS
BEGIN
	DECLARE @tbProducts TABLE (Id uniqueidentifier, Url nvarchar(max), ModifiedDate datetime)
	DECLARE @tbProductView TABLE (Id uniqueidentifier, ViewCount int)
	DECLARE @tbProductPriority TABLE(Id uniqueidentifier, Priority int)
	DECLARE @MaxViewCount int

	INSERT INTO @tbProducts
	SELECT P.Id,
		CASE WHEN SEOFriendlyUrl IS NULL 
			THEN LOWER(dbo.GetProductTypePath(T.Id, @SiteId) + N'id-' + P.ProductIDUser + '/' + UrlFriendlyTitle)
			ELSE LOWER(SEOFriendlyUrl)
		END,
		CASE WHEN P.ModifiedDate IS NULL
			THEN P.CreatedDate
			ELSE P.ModifiedDate
		END
	FROM (SELECT DISTINCT ProductId FROM PRProductSKU WHERE IsActive = 1 AND IsOnline = 1) AS S
		INNER JOIN PRProduct AS P ON P.Id = S.ProductId
		INNER JOIN PRProductType AS T ON P.ProductTypeId = T.Id

	INSERT INTO @tbProductView
	SELECT ProductId, COUNT(ProductId)
	FROM TRProductView
	GROUP BY ProductId

	SELECT @MaxViewCount = ISNULL(MAX(ViewCount), 1) FROM @tbProductView

	INSERT INTO @tbProductPriority
	SELECT Id,
		CAST((CASE WHEN ISNULL(ViewCount, 0) = 0
			THEN 0.5
			ELSE ViewCount
		END) / @MaxViewCount AS decimal(2, 1))
	FROM @tbProductView

	SELECT P.Id AS ProductId,
		P.ModifiedDate AS LastModifiedDate,
		CASE CHARINDEX('/', Url) WHEN 1 
			THEN SUBSTRING(Url, 2, LEN(Url) - 1) 
			ELSE Url 
		END AS Url,
		CASE WHEN PP.Priority IS NULL THEN 0.5 
			WHEN PP.Priority < 0.5 THEN 0.5
			ELSE PP.Priority
		END AS Priority,
		CASE WHEN P.ModifiedDate > DATEADD(DD, -1, GETDATE()) THEN 'Daily'  
			WHEN P.ModifiedDate > DATEADD(DD, -7, GETDATE()) THEN 'Weekly' 
			WHEN P.ModifiedDate > DATEADD(MM, -1, GETDATE()) THEN 'Monthly' 
			ELSE 'Weekly'
		END AS ChangeFrequency
	FROM @tbProducts P
		JOIN @tbProductPriority PP ON P.Id = PP.Id
END
GO

IF(OBJECT_ID('GetCommerceURLForSiteMap') IS NOT NULL)
	DROP PROCEDURE GetCommerceURLForSiteMap
GO
CREATE PROCEDURE [dbo].[GetCommerceURLForSiteMap]
(@SiteId uniqueidentifier)
as 

	Declare @Min bigint , @Max bigint , @RangeValue bigint;

			WITH
            ProductsNavigation AS
            (
                  SELECT      p.Id AS ProductId, (case when LOWER(SEOFriendlyUrl) is null then LOWER(dbo.GetProductTypePath(pt.Id, @SiteId) + N'id-' + p.ProductIDUser + '/' + UrlFriendlyTitle)
                              else LOWER(seoFriendlyUrl) end )AS FriendlyUrl, (case when p.ModifiedDate is null then p.CreatedDate else p.ModifiedDate end) as ModifiedDate
                  FROM 
                  (SELECT DISTINCT ProductId FROM PRProductSKU WHERE IsActive=1 And IsOnline=1 ) AS sku
                          INNER JOIN PRProduct AS p ON p.Id = sku.ProductId
                          INNER JOIN PRProductType AS pt ON p.ProductTypeId = pt.Id
            ),

            ProductsViewCount AS
            (
                  SELECT            ProductId, COUNT(ProductId) AS ViewCount
                  FROM        TRProductView
                  GROUP BY    ProductId
            )

		SELECT pn.ProductId as ProductId, FriendlyUrl, ModifiedDate, isnull(ViewCount,0) as ViewCount, 'yearly ' as Frequency,0.5 as Priority  into #tblProduct 
		FROM   ProductsNavigation AS pn LEFT JOIN ProductsViewCount AS pvc ON pvc.ProductId = pn.ProductId


		--hourly 
		update #tblProduct set Frequency='hourly' where ProductId in (
		select ObjectId from (
		select ObjectId,convert(nvarchar(10),CreatedDate,101) as CreatedDate,count(*) as ModifiedCount from VEVersion where convert(nvarchar(10),CreatedDate,101) = convert(nvarchar(10),dateadd(dd,-1,getdate()),101)
		and ObjectId in (Select ProductId from #tblProduct)
		group by ObjectId,convert(nvarchar(10),CreatedDate,101) having count(*)>3) X) and Frequency='yearly'
		
		--weekly 	
		update #tblProduct set Frequency='weekly' where ProductId in (
		select ObjectId from (
		select ObjectId,convert(nvarchar(10),CreatedDate,101) CreatedDate,count(*) ModifiedCount from VEVersion where CreatedDate >= dateadd(dd,-7,getdate())
		and ObjectId in (Select ProductId from #tblProduct  where Frequency='yearly')
		group by ObjectId,convert(nvarchar(10),CreatedDate,101) having count(*)>3) X)and Frequency='yearly'


		--monthly 
		update #tblProduct set Frequency='monthly' where ProductId in (
		select ObjectId from (
		select ObjectId,convert(nvarchar(10),CreatedDate,101) CreatedDate,count(*) ModifiedCount from VEVersion where CreatedDate >= dateadd(mm,-1,getdate())
		and ObjectId in (Select ProductId from #tblProduct  where Frequency='yearly')
		group by ObjectId,convert(nvarchar(10),CreatedDate,101) having count(*)>3) X)and Frequency='yearly'


		Select @max = Max(viewcount) from #tblProduct
		
		if(@max<>0)
		begin
				set @RangeValue = @max/3
				
				print cast(@RangeValue as nvarchar(10))
				update #tblProduct set Priority=0.6 where ViewCount>=@RangeValue and ViewCount<(@RangeValue*2)
				update #tblProduct set Priority=0.8 where ViewCount>=(@RangeValue*2) 
		end

		select ProductId,FriendlyUrl as url,ModifiedDate as LastModifiedDate,Frequency as ChangeFrequency,Priority from #tblProduct
GO

IF(OBJECT_ID('Product_GetProduct') IS NOT NULL)
	DROP PROCEDURE Product_GetProduct
GO
CREATE PROCEDURE [dbo].[Product_GetProduct](
@Id uniqueidentifier = null,
@ApplicationId uniqueidentifier=null,
@skuId uniqueidentifier = null,
@ProductIDUser nvarchar(50) =null
)    
AS    

if(@skuId is not null and @Id is null)
begin 
	select @Id = ProductId from PRProductSku where Id=@skuId
End 

if(@ProductIDUser is not null and @Id is null)
begin 
	select @Id = Id from PRProduct where ProductIDUser=@ProductIDUser
End 


	SELECT     
	 [Prd].[Id],    
	 [Prd].ProductIDUser,    
	 [Prd].[Title],    
	 [Prd].[ShortDescription],    
	 [Prd].[LongDescription],    
	 [Prd].[Description],    
	 [Prd].[Keyword],    
	 [Prd].[IsActive],    
	 [Prd].[ProductTypeID],    
	 vPT.[ProductTypeName] as ProductTypeName,    
	 vPT.[ProductTypePath] +  N'id-' + Prd.ProductIDUser + '/' + Prd.UrlFriendlyTitle as DefaultUrl, Prd.UrlFriendlyTitle,    
	 vPT.[IsDownloadableMedia] ,
	 [Prd].[ProductStyle],    
	 dbo.ConvertTimeFromUtc([Prd].CreatedDate,@ApplicationId)CreatedDate,    
	 [Prd].CreatedBy,    
	 dbo.ConvertTimeFromUtc([Prd].ModifiedDate,@ApplicationId)ModifiedDate,    
	 [Prd].ModifiedBy,    
	 [Prd].Status,    
	 [Prd].IsBundle,    
	 dbo.ConvertTimeFromUtc( [Prd].BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,    
	 [Prd].TaxCategoryID,    
	 [Prd].PromoteAsNewItem,
	 [Prd].PromoteAsTopSeller,
	 [Prd].ExcludeFromDiscounts,
	 [Prd].ExcludeFromShippingPromotions,
	 [Prd].Freight,
	 [Prd].Refundable,
	 [Prd].TaxExempt,
	 [Prd].SEOH1,
	 [Prd].SEODescription,
	 [Prd].SEOKeywords,
	 [Prd].SEOFriendlyUrl,
	 (Select Count(*) From PRProductSKU where ProductId= [Prd].Id) as NumberOfSKUs    
	FROM PRProduct Prd INNER JOIN vwProductTypePath vPT on [Prd].ProductTypeID = vPT.ProductTypeId    
	WHERE [Prd].Id = @Id;    
	    
	    
	select     
	 a.[Id],    
	 a.[AttributeDataType],    
	 a.[Title],    
	 av.[Value],    
	 pav.[ProductId]    
	from PRProductAttributeValue pav join ATAttributeEnum av on av.[Id] = pav.[AttributeEnumId]    
	 join ATAttribute a on av.[AttributeID]=a.[Id]     
	     
	where pav.[ProductId] = @Id    
	order by [ProductId],av.[AttributeID];
GO

IF(OBJECT_ID('Product_GetProductAndSkuByType') IS NOT NULL)
	DROP PROCEDURE Product_GetProductAndSkuByType
GO
CREATE PROCEDURE [dbo].[Product_GetProductAndSkuByType]      
(      
 @ProductTypeID uniqueidentifier
,@ApplicationId uniqueidentifier=null

)      
AS      

--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
   

 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
vPT.[IsDownloadableMedia], S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 S.TaxCategoryId,
 S.PromoteAsNewItem,
 S.PromoteAsTopSeller,
 S.ExcludeFromDiscounts,
 S.ExcludeFromShippingPromotions,
 S.Freight,
 S.Refundable,
 S.TaxExempt,
 S.SEOH1,
 S.SEODescription,
 S.SEOKeywords,
 S.SEOFriendlyUrl
 FROM PRProduct S       
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
  Where S.ProductTypeID =@ProductTypeID
--Order By T.Sno      


Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from PRProduct t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.Id  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  
AND t.ProductTypeID =@ProductTypeID


SELECT S.[Id]    
   ,S.[ProductId]    
   ,S.[SiteId]    
   ,[SKU]    
   ,S.[Title]    
   ,S.[Description]    
   ,S.[Sequence]    
   ,S.[IsActive]    
   ,[IsOnline]    
   ,[ListPrice]    
   ,[UnitCost]    
   ,[PreviousSoldCount]    
   ,S.[CreatedBy]    
   ,S.[CreatedDate]    
   ,S.[ModifiedBy]    
   ,S.[ModifiedDate]    
   ,S.[Status]    
   ,[Measure]    
   ,[OrderMinimum]    
   ,[OrderIncrement]  
   ,[HandlingCharges] 
   ,[Length]
   ,[Height]
   ,[Width]
   ,[Weight]
   ,[SelfShippable]    
   ,[AvailableForBackOrder]
   ,MaximumDiscountPercentage
   ,[BackOrderLimit]
   ,[IsUnlimitedQuantity]
   ,FreeShipping
   ,UserDefinedPrice
  FROM [PRProductSKU] S   
  Inner Join PRProduct t on S.ProductId=t.Id  
  Where t.ProductTypeID =@ProductTypeID



Select   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from PRProduct t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.Id  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On ( PS.Id = SA.SKUId  AND SA.ProductAttributeID = PA.ID)  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
and t.ProductTypeID =@ProductTypeID
--Order By SA.AttributeId  




select  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
from PRProduct t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.Id   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId  
where  A.Status=dbo.GetActiveStatus()     
and t.ProductTypeID =@ProductTypeID

END
GO

IF(OBJECT_ID('Product_GetProductAndSkuByXmlGuids') IS NOT NULL)
	DROP PROCEDURE Product_GetProductAndSkuByXmlGuids
GO
CREATE PROCEDURE [dbo].[Product_GetProductAndSkuByXmlGuids]      
(      
 @Id Xml  
,@ApplicationId uniqueidentifier=null

)      
AS      

--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)      

Insert Into @tempXml(ProductId)      
Select Distinct tab.col.value('text()[1]','uniqueidentifier')      
 FROM @Id.nodes('/ProductCollection/Product')tab(col)      

 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
vPT.[IsDownloadableMedia], S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 SEOFriendlyUrl,
S.TaxCategoryId,
S.PromoteAsNewItem,
S.PromoteAsTopSeller,
S.ExcludeFromDiscounts,
S.ExcludeFromShippingPromotions,
S.Freight,
S.Refundable,
S.TaxExempt,
S.SEOH1,
S.SEODescription,
S.SEOKeywords
  FROM @tempXml T      
  INNER JOIN PRProduct S on S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
--Order By T.Sno      


Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from @tempXml t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.ProductId  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  


SELECT 
	S.[Id]    
   ,S.[ProductId]    
   ,S.[SiteId]    
   ,S.[SKU]    
   ,S.[Title]    
   ,S.[Description]    
   ,S.[Sequence]    
   ,S.[IsActive]    
   ,S.[IsOnline]    
   ,ISNULL(SC.ListPrice,S.ListPrice)  ListPrice    
   ,S.[UnitCost]    
   ,S.[PreviousSoldCount]    
   ,S.[CreatedBy]    
   ,S.[CreatedDate]    
   ,S.[ModifiedBy]    
   ,S.[ModifiedDate]    
   ,S.[Status]    
   ,S.[Measure]    
   ,S.[OrderMinimum]    
   ,S.[OrderIncrement]  
   ,S.[HandlingCharges] 
   ,S.[Length]
   ,S.[Height]
   ,S.[Width]
   ,S.[Weight]
   ,S.[SelfShippable]    
   ,S.[AvailableForBackOrder]
   ,S.MaximumDiscountPercentage
   ,S.[BackOrderLimit]
   ,S.[IsUnlimitedQuantity]
   ,S.FreeShipping
   ,S.UserDefinedPrice
  FROM [PRProductSKU] S   
  Inner Join @tempXml t on S.ProductId=t.ProductId  
  LEFT JOIN PRProductSKUVariantCache SC ON S.Id = SC.Id AND SC.SiteId=@ApplicationId



Select   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On ( PS.Id = SA.SKUId  AND SA.ProductAttributeID = PA.ID)  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  




select  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized,
  AG.CategoryId,
  AG.AttributeGroupId

from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=dbo.GetActiveStatus()     


END
GO

IF(OBJECT_ID('Product_GetProductAndSkuByXmlGuidsForProductIndexer') IS NOT NULL)
	DROP PROCEDURE Product_GetProductAndSkuByXmlGuidsForProductIndexer
GO
CREATE PROCEDURE [dbo].[Product_GetProductAndSkuByXmlGuidsForProductIndexer]      
(      
		@ApplicationId uniqueidentifier=null
)      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)      
      
Insert Into @tempXml(ProductId)      
Select distinct ProductId as Id from PRProductSKU where IsActive =1 and IsOnline=1  
      
 SELECT   
 DISTINCT     
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
 vPT.[IsDownloadableMedia],S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 SEOFriendlyUrl,
 PromoteAsNewItem,
PromoteAsTopSeller,
ExcludeFromDiscounts,
ExcludeFromShippingPromotions,
Freight,
Refundable,
TaxExempt,
SEOH1,
SEODescription,
SEOKeywords,
 [S].TaxCategoryID        
  FROM @tempXml T      
  INNER JOIN PRProduct S on S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
--Order By T.Sno      
  
  
Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from @tempXml t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.ProductId  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  
  
  
SELECT [Id]    
      ,S.[ProductId]    
      ,[SiteId]    
      ,[SKU]    
   ,[Title]    
      ,[Description]    
      ,[Sequence]    
      ,[IsActive]    
      ,[IsOnline]    
      ,[ListPrice]    
      ,[UnitCost]    
      ,[PreviousSoldCount]    
   ,[CreatedBy]    
   ,[CreatedDate]    
   ,[ModifiedBy]    
   ,[ModifiedDate]    
   ,[Status]    
   ,[Measure]    
   ,[OrderMinimum]    
   ,[OrderIncrement]  
   ,[HandlingCharges]  
    ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]    
    ,[AvailableForBackOrder]
	,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
	,FreeShipping
,UserDefinedPrice
  FROM [PRProductSKU] S   
  Inner Join @tempXml t on S.ProductId=t.ProductId  
  
  
Select   
 SA.Id,  
 SA.SKUId,  
 PA.ProductId,
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On (SA.AttributeId=PA.AttributeId AND PS.Id = SA.SKUId )  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  
  
  
  
  
select  
  A.Id,  
  AG.CategoryId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
	A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where  A.Status=dbo.GetActiveStatus()    and  A.IsSystem=0 and A.IsFaceted=1
  

SELECT P.Id AS ProductId,	CASE WHEN Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice) WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
 ELSE Min(ListPrice) END  as Price ,  cast( SUM(PS.PreviousSoldCount) as int) AS soldcount, P.CreatedDate		
			FROM			
				dbo.PRProduct P
				INNER JOIN dbo.PRProductSKU PS ON P.Id = PS.ProductId 		
					LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
									) PP ON P.Id= PP.ProductId 	
					INNER JOIN @tempXml TP  ON P.Id = TP.ProductId 	
			WHERE     
				(PS.IsActive = 1) AND (PS.IsOnline = 1) AND (P.IsActive = 1)     
			GROUP BY     
				P.Id, 
					PP.MinimumEffectivePrice,
				P.CreatedDate,   
				P.ProductIDUser 

  
END
GO

IF(OBJECT_ID('Product_GetProductByType') IS NOT NULL)
	DROP PROCEDURE Product_GetProductByType
GO
CREATE PROCEDURE [dbo].[Product_GetProductByType]    
(    
 @ProductTypeID uniqueidentifier,
 @ApplicationId uniqueidentifier=null
)    
AS    
    
--********************************************************************************    
-- Variable declaration    
--********************************************************************************    
BEGIN    
--********************************************************************************    
-- code    
--********************************************************************************    
 
 SELECT      
 S.[Id],    
 S.[ProductIDUser],    
 S.[Title],    
 S.[ShortDescription],    
 S.[LongDescription],    
 S.[Description],    
 S.[Keyword],    
 S.[IsActive],    
 S.[ProductTypeID],    
 vPT.[ProductTypeName] as ProductTypeName,    
  vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,     
  vPT.[IsDownloadableMedia],
 S.[ProductStyle],    
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,    
 dbo.ConvertTimeFromUtc(S.CreatedDate,@ApplicationId)CreatedDate,    
 S.CreatedBy,    
 dbo.ConvertTimeFromUtc(S.ModifiedDate,@ApplicationId)ModifiedDate,    
 S.ModifiedBy,    
 S.Status,    
 S.IsBundle,    
 dbo.ConvertTimeFromUtc(S.BundleCompositionLastModified,@ApplicationId) BundleCompositionLastModified ,  
 SEOFriendlyUrl,
S.TaxcategoryId,
PromoteAsNewItem,
PromoteAsTopSeller,
ExcludeFromDiscounts,
ExcludeFromShippingPromotions,
Freight,
Refundable,
TaxExempt,
SEOH1,
SEODescription,
SEOKeywords
  FROM  PRProduct S 
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId    
  Where S.ProductTypeID =@ProductTypeID	
END    
    
select     
 a.[Id],    
 a.[AttributeDataType],    
 a.[Title],    
 pav.[Value],    
 pav.[ProductId]    
from PRProductAttributeValue pav     
 join ATAttribute a on pav.[AttributeId]=a.[Id]    
 Inner Join PRProduct P on pav.ProductId = P.Id
where P.ProductTypeID =@ProductTypeID
order by [ProductId],[AttributeId];
GO

IF(OBJECT_ID('Product_GetProductByXmlGuids') IS NOT NULL)
	DROP PROCEDURE Product_GetProductByXmlGuids
GO
CREATE PROCEDURE [dbo].[Product_GetProductByXmlGuids]    
(    
 @Id Xml,
 @ApplicationId uniqueidentifier=null
)    
AS    
    
--********************************************************************************    
-- Variable declaration    
--********************************************************************************    
BEGIN    
--********************************************************************************    
-- code    
--********************************************************************************    
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)    
    
Insert Into @tempXml(ProductId)    
Select tab.col.value('text()[1]','uniqueidentifier')    
 FROM @Id.nodes('/ProductCollection/Product')tab(col)    
    
 SELECT      
 S.[Id],    
 S.[ProductIDUser],    
 S.[Title],    
 S.[ShortDescription],    
 S.[LongDescription],    
 S.[Description],    
 S.[Keyword],    
 S.[IsActive],    
 S.[ProductTypeID],    
 vPT.[ProductTypeName] as ProductTypeName,    
  vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,     
  vPT.[IsDownloadableMedia],
 S.[ProductStyle],    
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,    
 dbo.ConvertTimeFromUtc(S.CreatedDate,@ApplicationId)CreatedDate,    
 S.CreatedBy,    
 dbo.ConvertTimeFromUtc(S.ModifiedDate,@ApplicationId)ModifiedDate,    
 S.ModifiedBy,    
 S.Status,    
 S.IsBundle,    
 dbo.ConvertTimeFromUtc(S.BundleCompositionLastModified,@ApplicationId) BundleCompositionLastModified ,  
 SEOFriendlyUrl,
S.TaxcategoryId,
PromoteAsNewItem,
PromoteAsTopSeller,
ExcludeFromDiscounts,
ExcludeFromShippingPromotions,
Freight,
Refundable,
TaxExempt,
SEOH1,
SEODescription,
SEOKeywords
  FROM @tempXml T    
  INNER JOIN PRProduct S on S.Id=T.ProductId    
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId    
Order By T.Sno    
END    
    
select     
 a.[Id],    
 a.[AttributeDataType],    
 a.[Title],    
 pav.[Value],    
 pav.[ProductId],
  AG.AttributeGroupId
from PRProductAttributeValue pav     
 join ATAttribute a on pav.[AttributeId]=a.[Id]    
 LEFT JOIN (select CI.AttributeId,MIN(CI.CategoryId) CategoryId,MIN(C.Name) Title,MIN(C.GroupId) AttributeGroupId
										from ATAttributeCategory C
										INNER JOIN ATAttributeCategoryItem CI on C.Id=CI.CategoryId
										Group By AttributeId) ag on a.Id=ag.AttributeId 
where pav.[ProductId] in (select ProductId FROM @Id.nodes('/ProductCollection/Product')tab(col)    
  INNER JOIN PRProduct S on S.Id=tab.col.value('text()[1]','uniqueidentifier'))    
order by [ProductId],pav.[AttributeId];
GO

IF(OBJECT_ID('Product_GetProductIdByUrl') IS NOT NULL)
	DROP PROCEDURE Product_GetProductIdByUrl
GO
CREATE PROCEDURE [dbo].[Product_GetProductIdByUrl]
(
	@url nvarchar(max), 
	@productIDUser nvarchar(max) = null, 
	@SiteId UniqueIdentifier
)    
AS 
BEGIN
	SELECT	DISTINCT Id, ProductUrl, SEOFriendlyUrl
	FROM	vwProduct
	WHERE	(ProductUrl = @url OR '/' + SEOFriendlyUrl = @url OR SEOFriendlyUrl =  '/' + @url) AND
			SiteId = @SiteId	
END 
GO

IF(OBJECT_ID('Product_GetProductVariant') IS NOT NULL)
	DROP PROCEDURE Product_GetProductVariant
GO
CREATE PROCEDURE [dbo].[Product_GetProductVariant]
	(
		@ProductIds xml = null,
		@IncludeSku bit = 0,
		@ApplicationId uniqueidentifier
	)
AS
BEGIN
	IF(@ProductIds IS NOT NULL)
	BEGIN

		declare @TabProdIds as table (Id uniqueidentifier)

		insert into @TabProdIds 
		SELECT tab.col.value('text()[1]','uniqueidentifier')    
		FROM @ProductIds.nodes('/GenericCollectionOfGuid/guid') tab(col) 

		 SELECT     
		 [Prd].[Id],    
		 [PrdSeo].ProductIDUser,    
		 [Prd].[Title],    
		 [Prd].[ShortDescription],    
		 [Prd].[LongDescription],    
		 [Prd].[Description],    
		 [Prd].[Keyword],    
		 [Prd].[IsActive],    
		 [Prd].[ProductTypeID],    
		 vPT.[ProductTypeName] AS ProductTypeName,    
		 vPT.[ProductTypePath] +  N'id-' + PrdSeo.ProductIDUser + '/' + Prd.UrlFriendlyTitle AS DefaultUrl, Prd.UrlFriendlyTitle,    
		 vPT.[IsDownloadableMedia] ,
		 [Prd].[ProductStyle],    
		 dbo.ConvertTimeFromUtc([PrdSeo].CreatedDate,@ApplicationId)CreatedDate,    
		 [PrdSeo].CreatedBy,    
		 dbo.ConvertTimeFromUtc([Prd].ModifiedDate,@ApplicationId)ModifiedDate,    
		 [Prd].ModifiedBy,    
		 [PrdSeo].Status,    
		 [PrdSeo].IsBundle,    
		dbo.ConvertTimeFromUtc( [PrdSeo].BundleCompositionLastModified,@ApplicationId) AS BundleCompositionLastModified,    
		 [PrdSeo].SEOFriendlyUrl AS SEOFriendlyUrl,
		 [Prd].TaxCategoryID,    
		 [PrdSeo].PromoteAsNewItem,
		[PrdSeo].PromoteAsTopSeller,
		[PrdSeo].ExcludeFromDiscounts,
		[PrdSeo].ExcludeFromShippingPromotions,
		[PrdSeo].Freight,
		[PrdSeo].Refundable,
		[PrdSeo].TaxExempt,
		[PrdSeo].SEOH1,
		[PrdSeo].SEODescription,
		[PrdSeo].SEOKeywords,
		 (Select Count(*) From PRProductSKU where ProductId= [Prd].Id) as NumberOfSKUs    
		FROM PRProductVariant Prd INNER JOIN vwProductTypePath vPT on [Prd].ProductTypeID = vPT.ProductTypeId    
		INNER JOIN PRProduct PrdSeo ON Prd.Id = PrdSeo.Id    
		WHERE [Prd].Id in (SELECT Id from @TabProdIds)
		and @ApplicationId=Prd.SiteId;


		if(@IncludeSku = 1)
		BEGIN
			
			SELECT S.[Id]    
			  ,PRS.[Id] as ProductId
			  ,S.[SiteId]    
			  ,[SKU]    
				,S.[Title]    
			  ,S.[Description]    
			  ,S.[Sequence]    
			  ,S.[IsActive]    
			  ,S.[IsOnline]    
			  ,S.[ListPrice]    
			  ,S.[UnitCost]    
			  ,S.[PreviousSoldCount]    
			   ,[CreatedBy]    
			   ,[CreatedDate]    
			   ,S.[ModifiedBy]    
			   ,S.[ModifiedDate]    
			   ,[Status]    
			   ,S.[Measure]    
			   ,S.[OrderMinimum]    
			   ,S.[OrderIncrement]  
			   ,S.[HandlingCharges] 
				,[Length]
			  ,[Height]
			  ,[Width]
			  ,[Weight]
			  ,S.[SelfShippable]    
			  ,S.[AvailableForBackOrder]
			  ,S.MaximumDiscountPercentage
			  ,S.[BackOrderLimit]
			,S.[IsUnlimitedQuantity]
			FROM [PRProductSKUVariant] S   
			INNER JOIN PRProductSKU PRS ON PRS.Id = S.Id
			WHERE PRS.Id in (SELECT Id FROM @TabProdIds )
			AND @ApplicationId = S.SiteId
		END	
	END
END
GO

IF(OBJECT_ID('ProductSEO_Get') IS NOT NULL)
	DROP PROCEDURE ProductSEO_Get
GO
CREATE  PROCEDURE [dbo].[ProductSEO_Get](
--********************************************************************************
-- Parameters Passing in ProductSEO_ 
--********************************************************************************
			@Id uniqueidentifier = null OUTPUT,
			@ProductId uniqueidentifier = null ,
			@ApplicationId uniqueidentifier=null
)
AS
--********************************************************************************
-- code
--********************************************************************************
BEGIN
		IF (@Id Is Null) AND (@ProductId Is Null)
		BEGIN
				RAISERROR('Either SEO Term Id or Product Id is required', 16, 1)
				RETURN dbo.GetBusinessRuleErrorCode()
		END

				Select Id, 
						Title, 
						SEOH1 ProductTitleH1,
						SEODescription Description, 
						SEOKeywords Keywords, 
						SEOFriendlyUrl FriendlyUrl, 
						dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId) CreatedDate, 
						dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,
						CreatedBy,
						ModifiedBy
				FROM PRProduct
				WHERE 
						[Status] = dbo.GetActiveStatus()
						AND Id = @Id or Id = @ProductId
END
GO

IF(OBJECT_ID('ProductSEO_Save') IS NOT NULL)
	DROP PROCEDURE ProductSEO_Save
GO

CREATE PROCEDURE [dbo].[ProductSEO_Save](
--********************************************************************************
-- Parameters Passing in ProductSEO_ 
--********************************************************************************
			@Id uniqueidentifier = null OUTPUT,
			@ProductId uniqueidentifier, 
			@Title nvarchar(512) = null,
			@ProductTitleH1 nvarchar(512) = null,
			@Description nvarchar(2048) = null,
			@Keywords  nvarchar(1024)=null,
			@FriendlyUrl nvarchar(1024) = null,
			@ModifiedDate datetime = null,
			@ModifiedBy uniqueidentifier,
			@ApplicationId uniqueidentifier
)
AS

Declare @slash char(1)
select @slash= SUBSTRING(@FriendlyUrl, 1, 1) 
if(@slash<> '/')
	set @FriendlyUrl = '/' + @FriendlyUrl
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Now dateTime
DECLARE @NewGuid uniqueidentifier
DECLARE @RowCount int

--********************************************************************************
-- code
--********************************************************************************
BEGIN

	IF((SELECT Count(Title) FROM  PRProduct
				WHERE (isnull(SEOFriendlyUrl,'1')) = (isnull(@FriendlyUrl,'2')) 
						AND SiteId= @ApplicationId AND Id!=@ProductId)> 0 )
		   BEGIN
				RAISERROR('SEO Friendly Url already exists', 16, 1)
				RETURN dbo.GetBusinessRuleErrorCode()
		   END
		   
	If (@Id is null)   -- Check if the product has a SEO already
	BEGIN
		Set @Id = @ProductId
	END

				Set @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId)

						UPDATE	PRProduct  WITH (ROWLOCK)
					SET			[Title]			=	@Title
							   ,SEOH1 = @ProductTitleH1
							   ,SEODescription	= @Description
							   ,[SEOKeywords]		= @Keywords
							   ,[SEOFriendlyUrl]	= @FriendlyUrl
							   ,[ModifiedBy]	= @ModifiedBy
							   ,[ModifiedDate]	= GetUTCDate()
					WHERE   
						[Id]           		=	@Id 
				

Select @Id
END

GO

PRINT 'Add column SubmitButtonDescription to Forms'
GO

IF(COL_LENGTH('Forms', 'SubmitButtonDescription') IS NULL)
BEGIN
	ALTER TABLE Forms
    ADD SubmitButtonDescription nvarchar(255) NULL
END
GO

PRINT 'Modify stored procedure iAppsForm_GetiAppsForm'
GO
IF(OBJECT_ID('iAppsForm_GetiAppsForm') IS NOT NULL)
	DROP PROCEDURE iAppsForm_GetiAppsForm
GO
CREATE PROCEDURE [dbo].[iAppsForm_GetiAppsForm] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id uniqueidentifier
)
as
BEGIN
	
		SELECT	Id,
				ApplicationId,
				Title,
				Description, 
				FormType,								
				CreatedDate,
				CreatedBy,
				ModifiedBy,
				ModifiedDate, 
				Status,
				XMLString,				
				XSLTString,
				XSLTFileName,
				SendEmailYesNo,
				Email,
				ThankYouURL,
				SubmitButtonText,
				SubmitButtonDescription,
				PollResultType,	
				PollVotingType,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				AddAsContact,
				ContactEmailField,
				WatchId,
				F.EmailSubjectText,
				F.EmailSubjectField,
				F.SourceFormId,
				ParentId,
				ThankYouContent,
				F.CssClass,
				F.Captcha
		FROM Forms F
		LEFT JOIN VW_UserFullName FN on FN.UserId = F.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = F.ModifiedBy
		where Id = @Id 			  
			  and
			  Status <> dbo.GetDeleteStatus()
			  Order by Title 
END
GO

PRINT 'Modify stored procedure iAppsForm_Save'
GO
IF(OBJECT_ID('iAppsForm_Save') IS NOT NULL)
	DROP PROCEDURE iAppsForm_Save
GO
CREATE PROCEDURE [dbo].[iAppsForm_Save] (
	@Id UNIQUEIDENTIFIER out
	,@ApplicationId UNIQUEIDENTIFIER
	,@Title NVARCHAR(1024)
	,@Description NVARCHAR(1024)
	,@XMLString XML
	,@XSLTString TEXT
	,@XSLTFileName VARCHAR(100)
	,@ThankYouURL NVARCHAR(1024)
	,@Email NVARCHAR(max)
	,@SendEmailYesNo INT
	,@Status INT
	,@CreatedBy UNIQUEIDENTIFIER
	,@ModifiedBy UNIQUEIDENTIFIER
	,@SubmitButtonText NVARCHAR(100)
	,@SubmitButtonDescription NVARCHAR(255)
	,@FormType INT
	,@PollResultType INT
	,@PollVotingType INT
	,@AddAsContact INT
	,@ContactEmailField NVARCHAR(256)
	,@WatchId UNIQUEIDENTIFIER
	,@EmailSubjectText NVARCHAR(256)
	,@EmailSubjectField NVARCHAR(256)
	,@SourceFormId UNIQUEIDENTIFIER = NULL
	,@ParentId  uniqueidentifier = null
	,@ThankYouContent nvarchar(max) = null
	,@CssClass NVARCHAR(255) = NULL
	,@Captcha BIT = NULL
	)
AS
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @reccount INT
	,@error INT
	,@Now DATETIME
	,@stmt VARCHAR(256)
	,@rowcount INT

BEGIN
	--********************************************************************************  
	-- code  
	--********************************************************************************  
	IF (@Status = dbo.GetDeleteStatus())
	BEGIN
		RAISERROR (
				'INVALID||Status'
				,16
				,1
				)

		RETURN dbo.GetBusinessRuleErrorCode()
	END

	/* IF @Id specified, ensure exists */
	IF (@Id IS NOT NULL)
		IF (
				(
					SELECT count(*)
					FROM Forms
					WHERE Id = @Id AND STATUS != dbo.GetDeleteStatus() AND ApplicationId = @ApplicationId
					) = 0
				)
		BEGIN
			RAISERROR (
					'NOTEXISTS||Id'
					,16
					,1
					)

			RETURN dbo.GetBusinessRuleErrorCode()
		END

	SET @Now = getutcdate()

	IF (@Status IS NULL OR @Status = 0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	IF (@Id IS NULL) -- New INSERT  
	BEGIN
		SET @stmt = 'INSERT'
		SET @Id = newid();

		INSERT INTO Forms (
			Id
			,ApplicationId
			,Title
			,Description
			,FormType
			,XMLString
			,XSLTString
			,XSLTFileName
			,STATUS
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,ThankYouURL
			,Email
			,SendEmailYesNo
			,SubmitButtonText
			,SubmitButtonDescription
			,PollResultType
			,PollVotingType
			,AddAsContact
			,ContactEmailField
			,WatchId
			,EmailSubjectText
			,EmailSubjectField
			,SourceFormId
			,ParentId
			,ThankYouContent
			,CssClass
			,Captcha
			)
		VALUES (
			@Id
			,@ApplicationId
			,@Title
			,@Description
			,@FormType
			,@XMLString
			,@XSLTString
			,@XSLTFileName
			,@Status
			,@CreatedBy
			,@Now
			,@ModifiedBy
			,@Now
			,@ThankYouURL
			,@Email
			,@SendEmailYesNo
			,@SubmitButtonText
			,@SubmitButtonDescription
			,@PollResultType
			,@PollVotingType
			,@AddAsContact
			,@ContactEmailField
			,@WatchId
			,@EmailSubjectText
			,@EmailSubjectField
			,@SourceFormId
			,@ParentId
			,@ThankYouContent
			,@CssClass
			,@Captcha
			)

		SELECT @error = @@error

		IF @error <> 0
		BEGIN
			RAISERROR (
					'DBERROR||%s'
					,16
					,1
					,@stmt
					)

			RETURN dbo.GetDataBaseErrorCode()
		END
	END
	ELSE -- update  
	BEGIN
		SET @stmt = 'UPDATE'
		IF @WatchId = dbo.GetEmptyGuid()
			SET @WatchId = NULL
		
		UPDATE Forms
		WITH (ROWLOCK)

		SET ApplicationId = @ApplicationId
			,Title = isnull(@Title, Title)
			,Description = isnull(@Description, Description)
			,FormType = @FormType
			,XMLString = @XMLString
			,XSLTString = @XSLTString
			,XSLTFileName = @XSLTFileName
			,ThankYouURL = @ThankYouURL
			,Email = @Email
			,SendEmailYesNo = @SendEmailYesNo
			,STATUS = @Status
			,ModifiedBy = @ModifiedBy
			,ModifiedDate = @Now
			,SubmitButtonText = @SubmitButtonText
			,SubmitButtonDescription = @SubmitButtonDescription
			,PollResultType = @PollResultType
			,PollVotingType = @PollVotingType
			,AddAsContact = @AddAsContact
			,ContactEmailField = @ContactEmailField
			,WatchId = @WatchId
			,EmailSubjectText=@EmailSubjectText
			,EmailSubjectField=@EmailSubjectField
			,SourceFormId=ISNULL(@SourceFormId, SourceFormId)
			,ParentId = @ParentId
			,ThankYouContent = @ThankYouContent
			,CssClass = @CssClass
			,Captcha = @Captcha
		WHERE Id = @Id

		--update title to HSStructure table--  
		--UPDATE HSStructure
		--SET Title = @Title
		--WHERE Id = @Id

		--------------------------------------------------  
		SELECT @error = @@error
			,@rowcount = @@rowcount

		IF @error <> 0
		BEGIN
			RAISERROR (
					'DBERROR||%s'
					,16
					,1
					,@stmt
					)

			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @rowcount = 0
		BEGIN
			RAISERROR (
					'CONCURRENCYERROR'
					,16
					,1
					)

			RETURN dbo.GetDataConcurrencyErrorCode() -- concurrency error  
		END
	END
END
GO
Print 'Creating Procedure Site_CreateMicroSiteData'
GO

IF(OBJECT_ID('Site_CreateMicroSiteData') IS NOT NULL)
	DROP PROCEDURE Site_CreateMicroSiteData
GO

CREATE PROCEDURE [dbo].[Site_CreateMicroSiteData]
(
	@ProductId						uniqueidentifier,
	@MasterSiteId					uniqueidentifier,
	@MicroSiteId					uniqueidentifier,
	@ModifiedBy						uniqueidentifier = null,
	@ImportAllAdminUserAndPermission bit ,
	@ImportAllWebSiteUser			bit,
	@PageImportOptions				int = 2,
	@ImportContentStructure			bit = 0,
	@ImportFileStructure			bit = 0,
	@ImportImageStructure			bit = 0,
	@ImportFormsStructure			bit = 0
)
AS
BEGIN
	-- Import Front end and admin users
	EXEC [dbo].[VariantSite_ImportUser] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllWebSiteUser, @ImportAllAdminUserAndPermission, @ProductId
	-- Import Menu and Pages
	EXEC [dbo].[Site_CopyMenusAndPages] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, @PageImportOptions
	--content structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 7, @ImportContentStructure
	--File structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 9, @ImportFileStructure
	--Image structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 33, @ImportImageStructure
	--Forms structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId, @MicroSiteId, @ModifiedBy, @ImportAllAdminUserAndPermission, 38, @ImportFormsStructure
	
	-- Import Tag, Form, etc. 
	EXEC [VariantSite_CopyMisc] @MicroSiteId, @ModifiedBy
	
	-- Changing the status of the site		
	UPDATE SISite SET Status = 1 WHERE Id = @MicroSiteId		
	

	--CREATE MARKETIER RELATED DEFAULT DATA FOR VARIANT SITES
	EXEC Site_CreateMicrositeDataForMarketier @MicroSiteId

	--CREATE COMMERCE RELATED DEFAULT DATA FOR VARIANT SITES
	EXEC Site_CreateMicrositeDataForCommerce @MasterSiteId, @MicroSiteId
	;WITH CTE AS
	(
		SELECT PageMapNodeId, ROW_NUMBER() OVER(PARTITION BY ParentId ORDER BY LftValue) AS DisplayOrder
		FROM PageMapNode WHERE SiteId = @MicroSiteId
	)

	UPDATE P
	SET P.DisplayOrder = C.DisplayOrder
	FROM PageMapNode P JOIN CTE C ON P.PageMapNodeId = C.PageMapNodeId	

	--Inserting in Country Cache
	INSERT INTO [dbo].[GLCountryVariantCache]
    (
		[Id],
        [SiteId],
        [CountryName],
        [Status],
        [ModifiedBy],
        [ModifiedDate]
	)
    SELECT
		Id,
		@MicroSiteId,
		CountryName,
		Status,
		ModifiedBy,
		ModifiedDate
	FROM vwCountry where SiteId = @MasterSiteId

	--Inserting in State Cache
	INSERT INTO [dbo].[GLStateVariantCache]
    (
		[Id],
        [SiteId],
        [State],
        [Status],
        [ModifiedBy],
        [ModifiedDate]
	)
    SELECT
		Id,
		@MicroSiteId,
		State,
		Status,
		ModifiedBy,
		ModifiedDate
	FROM vwState where SiteId = @MasterSiteId
END
GO

Print 'Creating Procedure Country_Get'
GO

IF(OBJECT_ID('Country_Get') IS NOT NULL)
	DROP PROCEDURE Country_Get
GO
CREATE PROCEDURE [dbo].[Country_Get](@Id uniqueidentifier=null,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	
		IF(@Id IS NULL)
		BEGIN
				

				SELECT [Id]
					  ,[CountryName] AS Title
					  ,[CountryCode] 
					  ,[CreatedBy]
					  ,[CreatedDate]
					  ,[ModifiedDate]
					  ,[ModifiedBy]
					  ,[RegionName] As Region
				  FROM [dbo].[vwCountry] 
				  WHERE Status = 1 AND (@ApplicationId IS NULL OR SiteId = @ApplicationId)
					Order By CountryName

				SELECT S.[Id]
					  ,[State] As Title
					  ,[StateCode]
					  ,[CountryId]
					  ,S.[CreatedBy]
					  ,S.[CreatedDate]
					  ,S.[ModifiedDate]
					  ,S.[ModifiedBy]
					  ,R.Name Region
					  FROM [dbo].[vwState] S
					INNER JOIN GLCountry C ON S.CountryId=C.Id AND (@ApplicationId IS NULL OR S.SiteId = @ApplicationId)
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					Where S.Status = 1	 AND C.Status=1 

		END
	ELSE
		BEGIN
					SELECT	C.Id,
					CountryName Title,
					CountryCode,
					CreatedBy,
					CreatedDate,
					ModifiedDate,
					ModifiedBy,
					R.Name Region
					FROM GLCountry C
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					WHERE C.Id = @Id

						SELECT 
					S.Id,
					S.State Title,
					S.StateCode,
					S.CountryId,
					S.CreatedBy,
					S.CreatedDate,
					S.ModifiedDate,
					S.ModifiedBy,
					R.Name Region 
					FROM GLState S
					INNER JOIN GLCountry C ON S.CountryId=C.Id
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					WHERE (@Id is null or C.Id = @Id) AND S.Status = 1	
		END
END

GO
Print 'Creating Procedure Country_GetState'
GO

IF(OBJECT_ID('Country_GetState') IS NOT NULL)
	DROP PROCEDURE Country_GetState
GO
CREATE PROCEDURE [dbo].[Country_GetState]
(@Id uniqueidentifier=null
,@CountryId uniqueidentifier=null
,@ApplicationId uniqueidentifier=null
)
AS
BEGIN

				SELECT			   
						S.[Id]
					  ,S.[State] As Title
					  ,S.[StateCode]
					  ,S.[CountryId]
					  ,S.[CreatedBy]
					  ,S.[CreatedDate]
					  ,S.[ModifiedDate]
					  ,S.[ModifiedBy]
					  ,R.Name Region
					  FROM [dbo].[vwState] S
					INNER JOIN GLCountry C ON S.CountryId=C.Id AND (@ApplicationId IS NULL OR S.SiteId = @ApplicationId)
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					 WHERE (@Id is null or S.Id = @Id) 
					AND (@CountryId is null or C.Id = @CountryId) 
					AND S.Status = 1	 AND C.Status=1 
					Order by S.State

END

GO

PRINT 'Modify stored procedure Membership_GetUser'
GO
IF(OBJECT_ID('Membership_GetUser') IS NOT NULL)
	DROP PROCEDURE Membership_GetUser
GO


CREATE PROCEDURE [dbo].[Membership_GetUser]
--********************************************************************************
-- PARAMETERS
	@ApplicationId						uniqueidentifier,
    @UserId						uniqueidentifier,
	@UserName					nvarchar(256),
    @UpdateLastActivity			bit = 0
--********************************************************************************
AS
BEGIN
--********************************************************************************
-- Variable declaration
--********************************************************************************
	DECLARE @CurrentTimeUtc     datetime
	SET @CurrentTimeUtc = GetUTCDate()
	DECLARE @DeletedSatus INT
	SET @DeletedSatus = dbo.GetDeleteStatus()
	
	IF (@ApplicationId = dbo.GetEmptyGUID())
		SET @ApplicationId = NULL
			
	IF ( @UserName IS NOT NULL )
	BEGIN

		SET @UserName = LOWER(@UserName)  
		Select @UserId = Id from USUser where LoweredUserName = @UserName

		IF EXISTS (Select 1 from vwCustomer where Id = @UserId) --If its customer , its a global entity across variant site
			SET @ApplicationId = NULL

		IF ( @UpdateLastActivity = 1 )
		BEGIN
			UPDATE   dbo.USUser
			SET      LastActivityDate = @CurrentTimeUtc
			FROM     dbo.USUser u, dbo.USSiteUser s
			WHERE	 u.LoweredUserName	= LOWER(@UserName) AND
					(@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))) AND 
					-- s.SiteId	= ISNULL(@ApplicationId,s.SiteId) AND
					 u.Status <> @DeletedSatus	
			
			IF ( @@ROWCOUNT = 0 ) -- User ID not found
			BEGIN
				RAISERROR ('NOTEXISTS||%s', 16, 1, 'User')
				RETURN dbo.GetBusinessRuleErrorCode()
			END
		
		END
		
		
		SELECT  u.UserName,u.CreatedDate,
				m.Email, m.PasswordQuestion,  m.IsApproved,
				m.LastLoginDate, u.LastActivityDate,
				m.LastPasswordChangedDate, u.Id, m.IsLockedOut,
				m.LastLockoutDate
		FROM    dbo.USUser u, dbo.USMembership m, USSiteUser s
		WHERE   u.LoweredUserName	=@UserName AND 
				u.Id = m.UserId		   AND
				m.UserId = s.UserId    AND
				u.Status <> @DeletedSatus	AND
				--s.SiteId = @ApplicationId   
				(@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
	END
	ELSE
	BEGIN
		IF ( @UpdateLastActivity = 1 )
		BEGIN
			UPDATE   dbo.USUser
			SET      LastActivityDate = @CurrentTimeUtc
			FROM     dbo.USUser
			WHERE    @UserId = Id

			IF ( @@ROWCOUNT = 0 ) -- User ID not found
			BEGIN
				RAISERROR ('NOTEXISTS||%s', 16, 1, 'User')
				RETURN dbo.GetBusinessRuleErrorCode()
			END
		
		END

		SELECT  u.UserName,u.CreatedDate,
				m.Email, m.PasswordQuestion,  m.IsApproved,
				m.LastLoginDate, u.LastActivityDate,
				m.LastPasswordChangedDate, u.Id, m.IsLockedOut,
				m.LastLockoutDate
		FROM    dbo.USUser u, dbo.USMembership m
		WHERE   @UserId = u.Id AND u.Id = m.UserId
	END
    
	IF ( @@ROWCOUNT = 0 ) -- User ID not found
    BEGIN
            RAISERROR ('NOTEXISTS||%s', 16, 1, 'User')
			RETURN dbo.GetBusinessRuleErrorCode()
	END   

    RETURN 0
END
GO

PRINT 'Modify stored procedure Membership_GetPasswordWithFormat'
GO
IF(OBJECT_ID('Membership_GetPasswordWithFormat') IS NOT NULL)
	DROP PROCEDURE Membership_GetPasswordWithFormat
GO

CREATE PROCEDURE [dbo].[Membership_GetPasswordWithFormat]
(
	@ApplicationId					uniqueidentifier,
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit
)
AS
BEGIN
	DECLARE @IsLockedOut bit, @UserId uniqueidentifier, @Password nvarchar(128), @PasswordSalt nvarchar(128),
		@PasswordFormat int, @FailedPasswordAttemptCount int, @FailedPasswordAnswerAttemptCount int, 
		@IsApproved bit, @LastActivityDate datetime, @LastLoginDate datetime, 
		@UtcNow datetime, @DeletedSatus int
	
	   SET @UserName = LOWER(@UserName) 
	   Select @UserId = Id from USUser where LoweredUserName = @UserName

	   IF EXISTS (Select 1 from vwCustomer where Id = @UserId)
			SET @ApplicationId = NULL

	SET @DeletedSatus = dbo.GetDeleteStatus()
	SET @UtcNow = GETUTCDATE()
	SET  @UserId = NULL
	
    SELECT @UserId = U.Id, 
		@IsLockedOut = M.IsLockedOut, 
		@Password = M.Password, 
		@PasswordFormat = M.PasswordFormat,
        @PasswordSalt = M.PasswordSalt, 
		@FailedPasswordAttemptCount = M.FailedPasswordAttemptCount,
		@FailedPasswordAnswerAttemptCount = M.FailedPasswordAnswerAttemptCount, 
		@IsApproved = M.IsApproved,
        @LastActivityDate = U.LastActivityDate, 
		@LastLoginDate = M.LastLoginDate
    FROM dbo.USUser U, dbo.USMembership M, dbo.USSiteUser S
    WHERE U.Id = M.UserId
		AND S.UserId = U.Id
		AND U.LoweredUserName = LOWER(@UserName)
		AND U.Status != @DeletedSatus
		AND (@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))  
		--AND	S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))

    --User not found
	IF (@UserId IS NULL)
	BEGIN
        RAISERROR ('NOTEXISTS||%s', 16, 1, @UserName)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

    --User is locked out
	IF (@IsLockedOut = 1)
	BEGIN
        RAISERROR ('USERLOCKED||%s', 16, 1, @UserName)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	DECLARE @CNT int
	
	SELECT @CNT = COUNT(*) FROM USUser WHERE Id = @UserId AND ISNULL(ExpiryDate, '12/31/9999') < GETUTCDATE()
	IF (@CNT > 0)
	BEGIN
		RAISERROR ('USEREXPIRED||%s', 16, 1, @UserName)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
			
    SELECT @Password Password, 
		@PasswordFormat PasswordFormat, 
		@PasswordSalt PasswordSalt, 
		@FailedPasswordAttemptCount FailedAttemptCount,
        @FailedPasswordAnswerAttemptCount FailedAnswerAttemptCount, 
		@IsApproved IsApproved, 
		@LastLoginDate LastLoginDate, 
		@LastActivityDate LastActivityDate

	IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE dbo.USMembership SET LastLoginDate = @UtcNow WHERE UserId = @UserId        

        UPDATE dbo.USUser SET LastActivityDate = @UtcNow WHERE @UserId = Id
    END

    RETURN 0
END
GO

PRINT 'Creating View VW_UserFullName'
GO
IF (OBJECT_ID('VW_UserFullName') IS NOT NULL)
	DROP View VW_UserFullName	
GO
CREATE VIEW [dbo].[VW_UserFullName]
AS
SELECT 
	Id AS UserId, 
	CASE 
		WHEN LastName IS NULL Then ' , '
		WHEN LEN(LTRIM(LastName)) = 0 THEN '' 
		ELSE LastName + ', '  
	END + ISNULL(FirstName, ' ') AS UserFullName
FROM vw_contacts
GO

PRINT 'Add column Sequence to CSCustomerGroupPriceSet'
GO
IF(COL_LENGTH('CSCustomerGroupPriceSet', 'Sequence') IS NULL)
	ALTER TABLE [dbo].[CSCustomerGroupPriceSet]
    ADD Sequence INT NULL
GO

PRINT 'Modify stored procedure PriceSet_Save'
GO
IF(OBJECT_ID('PriceSet_Save') IS NOT NULL)
	DROP PROCEDURE PriceSet_Save
GO
CREATE PROCEDURE [dbo].[PriceSet_Save]      
(      
 @Id uniqueidentifier = null out,      
 @Title nvarchar(max),      
 @ApplicationId uniqueidentifier,   
 @StartDate datetime,  
 @EndDate datetime ,  
 @IsSale bit ,  
 @IsPendingChange bit,  
 @TypeId  int,  
 @Status int =null,      
 @CreatedBy  uniqueidentifier,      
 @ModifiedBy  uniqueidentifier,      
 @ModifiedDate datetime =null,  
 @CustomerGroupIds xml     
 )      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
DECLARE       
  @Now   dateTime,      
  @NewGuid  uniqueidentifier ,  
  @RowCount int     
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
 
	IF (ltrim(rtrim(@Title)) ='')
	BEGIN
		 RAISERROR('Title should not be empty||Id', 16, 1)      
		 RETURN dbo.GetBusinessRuleErrorCode()      
	END      
      
 /* IF @Id specified, ensure exists */      
 IF (@Id is not null)      
 Begin      
       IF((SELECT Count(Title) FROM  PSPriceSet WHERE Id = @Id And SiteId=@ApplicationId) = 0)      
       BEGIN      
		 RAISERROR('NOTEXISTS||Id', 16, 1)      
		 RETURN dbo.GetBusinessRuleErrorCode()      
       END      
 End      
 /* Checking for Title Duplicate */
 IF (@Id is null)      
 Begin   
       IF((SELECT Count(Title) FROM  PSPriceSet WHERE Title = @Title AND Status =1 AND SiteId =@ApplicationId) > 0)      
       BEGIN      
		 RAISERROR('This name already existing||Id ', 16, 1)      
		 RETURN dbo.GetBusinessRuleErrorCode()      
       END  
 End
 Else
 Begin
       IF((SELECT Count(Title) FROM  PSPriceSet WHERE Title = @Title And Id <> @Id and Status=1 AND SiteId =@ApplicationId) > 0)      
       BEGIN      
		 RAISERROR('This name already existing||Id', 16, 1)      
		 RETURN dbo.GetBusinessRuleErrorCode()      
       END  
 End

/* Check the TypeId is existing or not */  
-- IF (@TypeId is null)      
-- BEGIN  
--    RAISERROR('Type Should not be blank||Id', 16, 1)      
--    RETURN dbo.GetBusinessRuleErrorCode()       
-- END  
-- ELSE  
-- Begin      
--       IF((SELECT Count(Title) FROM  PSPriceSetType WHERE Id = @TypeId ) = 0)      
--       BEGIN      
--     RAISERROR('TYPE NOT EXISTS||Id', 16, 1)      
--     RETURN dbo.GetBusinessRuleErrorCode()      
--       END      
-- End      
  
  
 SET @Now = getUTCDate()      
 IF (@Id is null)   -- New INSERT      
 BEGIN      
 SET @NewGuid = newid()      
 SET @Status= dbo.GetActiveStatus()      
 INSERT INTO PSPriceSet (      
    [Id],      
    [Title],      
    [SiteId],   
    [TypeId],   
    [IsPendingChange],   
    [IsSale],   
    [StartDate],   
    [EndDate],  
    [CreatedBy],      
    [CreatedDate],      
    [Status])           
 VALUES (      
    @NewGuid,      
    @Title,   
    @ApplicationId,     
    @TypeId,   
    @IsPendingChange,   
    @IsSale,   
    dbo.ConvertTimeToUtc(@StartDate,@ApplicationId),   
    dbo.ConvertTimeToUtc(@EndDate,@ApplicationId),  
    @CreatedBy,      
    @Now,      
    isnull(@Status,dbo.GetActiveStatus())      
    )      
  
 IF @@ERROR <> 0      
 BEGIN      
  RETURN dbo.GetDataBaseErrorCode()               
 END       
  
 /* Insert into CSCustomerPriceSet*/  
 INSERT INTO CSCustomerGroupPriceSet   
  (Id, PriceSetId, CustomerGroupId, Sequence)  
 SELECT newId(), @NewGuid As PriceSetId,   
   tab.col.value('text()[1]','uniqueidentifier')   as CustomerGroupId, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) as Sequence  
 FROM @CustomerGroupIds.nodes('/GenericCollectionOfGuid/guid')tab(col)  
  
 IF @@ERROR <> 0      
 BEGIN      
  RETURN dbo.GetDataBaseErrorCode()               
 END       
   
 SELECT @Id = @NewGuid           
 END      
 ELSE      
 BEGIN      
 UPDATE PSPriceSet  WITH (ROWLOCK)      
 SET     [Title]  =  @Title,       
   [SiteId] =  @ApplicationId,      
   [TypeId] =  @TypeId,  
   [IsPendingChange] =  @IsPendingChange,  
   [IsSale] = @IsSale,   
   [StartDate] = dbo.ConvertTimeToUtc(@StartDate,@ApplicationId),   
   [EndDate] = dbo.ConvertTimeToUtc(@EndDate,@ApplicationId),  
   [ModifiedBy]  =  @ModifiedBy,      
   [ModifiedDate] =  @Now,      
   [Status]  = ISNULL(@Status,[Status])      
 WHERE   [Id]            = @Id       
  
 SELECT  @RowCount = @@ROWCOUNT      
 IF @@ERROR <> 0      
 BEGIN      
  RETURN dbo.GetDataBaseErrorCode()      
 END      
 IF @RowCount = 0      
 BEGIN      
  /* concurrency error */      
  RAISERROR('DBCONCURRENCY',16,1)      
  RETURN dbo.GetDataConcurrencyErrorCode()                                         
 END        
   
 /* Delete existing customer set collection */  
 DELETE FROM CSCustomerGroupPriceSet WHERE PriceSetId = @Id  
 IF @@ERROR <> 0      
 BEGIN      
  RETURN dbo.GetDataBaseErrorCode()               
 END   
  
 /* Insert into CSCustomerPriceSet*/  
 INSERT INTO CSCustomerGroupPriceSet   
  (Id, PriceSetId, CustomerGroupId, Sequence)  
 SELECT newId(), @Id As PriceSetId,   
   tab.col.value('text()[1]','uniqueidentifier')   as CustomerGroupId, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) as Sequence  
 FROM @CustomerGroupIds.nodes('/GenericCollectionOfGuid/guid')tab(col)  
  
 IF @@ERROR <> 0      
 BEGIN      
  RETURN dbo.GetDataBaseErrorCode()               
 END   
  
 END 
END
GO

Print 'Migration for Enum Attributes with No Values' -- Few Attributes which is set Boolean having wrong AttributeDataTypeId 
Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateEnumAttributeNoValues'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN
	DECLARE @StringDataTypeId uniqueidentifier, @EnumeratedDataTypeId uniqueidentifier
	SELECT TOP 1 @StringDataTypeId = Id FROM ATAttributeDataType where Title= 'String'
	SELECT TOP 1 @EnumeratedDataTypeId = Id FROM ATAttributeDataType WHERE Title = 'Enumerated'

	UPDATE A SET A.AttributeDataTypeId = @StringDataTypeId, A.IsEnum = 0 
	FROM ATAttribute A
	WHERE A.AttributeDataTypeId = @EnumeratedDataTypeId AND NOT EXISTS (SELECT 1 FROM ATAttributeEnum E WHERE E.AttributeId = A.Id)

	INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(), @ScriptName, @Version, GetDate())
END
GO

Print 'Migration for Boolean Attributes' -- Few Attributes which is set Boolean having wrong AttributeDataTypeId 
Declare @Version varchar(20)
Declare @ScriptName varchar(100)
Set @ScriptName = 'UpdateBooleanAttributesAll'
Select @Version = ProductVersion from iAppsProductSuite where Title = 'Content Manager'

IF NOT EXISTS(Select 1 from SQLExecution where Title = @ScriptName and DBVersion = @Version)
BEGIN

	Declare @EnumValues Table
	(
		Value varchar(20)
	)
	INSERT INTO @EnumValues Values ('true')
	INSERT INTO @EnumValues Values ('false')

	Declare @BooleanDataTypeId uniqueidentifier 
	Select @BooleanDataTypeId = Id from ATAttributeDataType where Type= 'System.Boolean'

	DECLARE @Attribute TABLE
	(
	  ID uniqueidentifier, 
	  Processed bit
	)

	INSERT INTO @Attribute
	Select Id, 0 from ATAttribute where AttributeDataType = 'System.Boolean' 
	AND AttributeDataTypeId != @BooleanDataTypeId

	Declare @AttributeId uniqueidentifier

	WHILE EXISTS(Select 1 from @Attribute where Processed = 0)
	 BEGIN
		Select top 1 @AttributeId = Id from @Attribute where Processed=0

		 IF NOT EXISTS (Select 1 from ATAttributeEnum AE LEFT JOIN @EnumValues A ON A.Value = AE.Value
					where AE.AttributeID =@AttributeId
					AND A.Value IS NULL)
			 BEGIN 
				Print 'Attribute Updating to Boolean' 
				Print @AttributeId
				Update A Set AttributeDataTypeId = @BooleanDataTypeId, IsEnum = 0
				From ATAttribute A Where Id = @AttributeId

			 END 

		Update @Attribute Set Processed = 1 where Id = @AttributeId

	 END

	INSERT INTO [dbo].[SQLExecution] VALUES (NEWID(),@ScriptName,@Version,GetDate())

END
 GO 

PRINT 'Modify stored procedure Version_GetLatest'
GO
IF(OBJECT_ID('Version_GetLatest') IS NOT NULL)
	DROP PROCEDURE Version_GetLatest
GO
CREATE PROCEDURE Version_GetLatest
(
	@ObjectIds varchar(max)
)
AS
BEGIN 

Declare @VersionLatest Table
(
	CreatedById Uniqueidentifier,
	Id Uniqueidentifier,
	ObjectId Uniqueidentifier,
	ObjectTypeId int,
	VersionNumber int,
	RevisionNumber int,
	[Status] int,
	[ObjectStatus] int,
	[VersionStatus] int,
	[Comments] varchar(max),
	[TriggeredObjectId] Uniqueidentifier, 
	[TriggeredObjectTypeId] int, 
	[CreatedDate] DateTime,
	[CreatedByFullName] varchar(500)
)
Declare @ObjectIdTable Table
(
	Id uniqueidentifier,
	Processed bit
)

Insert INTO @ObjectIdTable Select Items,0 from dbo.SplitGUID(@ObjectIds,',')

Declare @ObjectId uniqueidentifier
WHILE EXISTS (Select 1 from @ObjectIdTable where Processed = 0)
BEGIN
	Select top 1 @ObjectId = Id from  @ObjectIdTable where Processed = 0
	
	INSERT INTO @VersionLatest
	Select [CreatedBy] AS [CreatedById], [Id], [ObjectId], [ObjectTypeId],  [VersionNumber], [RevisionNumber], [Status], [ObjectStatus], [VersionStatus], [Comments], [TriggeredObjectId], [TriggeredObjectTypeId], [CreatedDate], [CreatedByFullName]
	FROM vwVersionLatest V 
	where V.ObjectId = @ObjectId
	order by CreatedDate Desc
	option (recompile)

	Update @ObjectIdTable Set Processed = 1 where Id = @ObjectId
END 

Select * from @VersionLatest

END
GO
Print 'ProductType Index Creation'
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_PRProductTypeVariantCache_SiteId_ID' AND object_id = OBJECT_ID('PRProductTypeVariantCache'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_PRProductTypeVariantCache_SiteId_ID]
	ON [dbo].[PRProductTypeVariantCache] ([SiteId])
	INCLUDE ([Id]);

END
GO
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_PRProductType_SiteId_ID' AND object_id = OBJECT_ID('PRProductType'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_PRProductType_SiteId_ID]
	ON [dbo].[PRProductType] ([SiteId])
	INCLUDE ([Id])

END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_PRProductVariantCache_SiteId_Id_Url' AND object_id = OBJECT_ID('PRProductVariantCache'))
BEGIN
	
	CREATE NONCLUSTERED INDEX [IX_PRProductVariantCache_SiteId_Id_Url]
	ON [dbo].[PRProductVariantCache] ([SiteId])
	INCLUDE ([Id],[UrlFriendlyTitle],[SEOFriendlyUrl])


END
GO

UPDATE iAppsProductSuite SET UpdateEndDate = GETUTCDATE()
GO
PRINT 'Updating the version in the productsuite table'
GO

UPDATE iAppsProductSuite SET UpdateStartDate = GETUTCDATE(), ProductVersion='7.0.0609.0300'
GO

PRINT 'Add columns to SITemplate'
GO

IF(COL_LENGTH('SITemplate', 'AmpEnabled') IS NULL)
BEGIN
	ALTER TABLE SITemplate ADD
		[AmpEnabled] BIT NOT NULL DEFAULT ((0)), 
		[AmpView] NVARCHAR(256) NULL
END
GO

PRINT 'Update stored procedure Template_GetAllTemplates'
GO

ALTER PROCEDURE [dbo].[Template_GetAllTemplates]
(
	@Id				uniqueIdentifier = null ,
	@ApplicationId	uniqueidentifier = null,
	@Title			nvarchar(256) = null,
	@FileName		nvarchar(4000) = null,
	@Status			int = null,
	@PageIndex		int = 1,
	@PageSize		int = null,
	@Type			int = null 
)
AS

BEGIN
	DECLARE @EmptyGUID uniqueidentifier, @DeleteStatus int
	SET @EmptyGUID = dbo.GetEmptyGUID()
	SET @DeleteStatus = dbo.GetDeleteStatus()
	
	IF @PageSize IS NULL
	BEGIN
		SELECT S.Id, 
			S.Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,
			FileName,
			ImageURL,
			Type,
			ApplicationId,
			PagePart,
			LayoutTemplateId,
			ZonesXml,
			(SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,
			(SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			[dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, 
			CodeFile, 
			H.ParentId,
			AllowAccessInChildrenSites,
			AmpEnabled,
			AmpView
		FROM SITemplate S 
			LEFT JOIN VW_UserFullName FN ON FN.UserId = S.CreatedBy
			LEFT JOIN VW_UserFullName MN ON MN.UserId = S.ModifiedBy
			INNER JOIN HSStructure H ON S.Id = H.Id
		WHERE S.Id = ISNULL(@Id, S.Id) 
			AND (@Title IS NULL OR S.Title like @Title) 
			AND (@FileName IS NULL OR S.FileName like @FileName)
			AND (@ApplicationId IS NULL 
				OR S.ApplicationId = @ApplicationId 
				OR S.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))
				) 
			AND (@Type IS NULL OR S.Type = @Type)
			AND (@Status IS NULL OR S.Status = @Status)
			AND	(S.Status != @DeleteStatus OR @Status = @DeleteStatus)
			AND ((S.AllowAccessInChildrenSites IS NULL OR S.AllowAccessInChildrenSites = 1) OR (@ApplicationId IS NULL OR ApplicationId = @ApplicationId))  
	END
	ELSE
	BEGIN
		WITH ResultEntries AS 
		( 
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS RowNo, 
				S.Id, 
				S.Title, 
				Description, 
				Status, 
				CreatedBy, 
				CreatedDate, 
				ModifiedBy, 
				ModifiedDate,
				FileName,
				ImageURL,
				Type,
				ApplicationId,
				PagePart,
				LayoutTemplateId,
				ZonesXml,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,
				[dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, 
				CodeFile,
				AllowAccessInChildrenSites,
				AmpEnabled,
				AmpView
			FROM SITemplate S 
			WHERE S.Id = ISNULL(@Id, S.Id) 
				AND (@Title IS NULL OR S.Title like @Title) 
				AND (@FileName IS NULL OR S.FileName like @FileName)
				AND (@ApplicationId IS NULL 
					OR S.ApplicationId = @ApplicationId 
					OR S.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))
					) 
				AND (@Type IS NULL OR S.Type = @Type)
				AND (@Status IS NULL OR S.Status = @Status)
				AND	(S.Status != @DeleteStatus OR @Status = @DeleteStatus)
				AND ((S.AllowAccessInChildrenSites IS NULL OR S.AllowAccessInChildrenSites = 1) OR (@ApplicationId IS NULL OR ApplicationId = @ApplicationId))  
		)

		SELECT S.Id, 
			S.Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,
			FileName,
			ImageURL,
			Type,
			ApplicationId, 
			StyleId,
			ScriptId,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			ParentVirtualPath, 
			CodeFile,
			PagePart,
			LayoutTemplateId,
			ZonesXml, 
			H.ParentId,
			AllowAccessInChildrenSites,
			AmpEnabled,
			AmpView
		FROM ResultEntries S
			INNER JOIN HSStructure H on S.Id = H.Id
			LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
		WHERE RowNo BETWEEN (@PageIndex - 1) * @PageSize + 1 AND @PageIndex * @PageSize
	END

END
GO

PRINT 'Update stored procedure Template_GetTemplate'
GO

ALTER PROCEDURE [dbo].[Template_GetTemplate]    
(      
 @Id    uniqueIdentifier=null ,      
 @ApplicationId uniqueidentifier=null,      
 @Title   nvarchar(256)=null,      
 @FileName  nvarchar(4000)=null,      
 @Status   int=null,      
 @PageIndex  int=1,      
 @PageSize  int=null,      
 @Type   int=null,      
 @ThemeId  UNIQUEIDENTIFIER=null,      
 @ResolveThemeId bit = null       
 )      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
DECLARE @EmptyGUID uniqueidentifier      
      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
IF(@ResolveThemeId IS NULL)      
 SET @ResolveThemeId = 0      

   IF EXISTS( Select 1 from SISIte where Id = @ApplicationId and ThemeId IS NULL ) -- This is changed as Template was not pulled in case of multisite, if template was created in a different site and used in another site
	SET	@ApplicationId = NULL

SET @EmptyGUID = dbo.GetEmptyGUID()      
 IF @PageSize is Null      
 BEGIN      
   IF(@ResolveThemeId = 1)      
    SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 3)      
          
  IF(@ThemeId = dbo.GetEmptyGUID() OR @ThemeId IS NULL)      
  BEGIN      
  --Print 'In ThemeId is null'
	IF(@Id IS NOT NULL)
	BEGIN
		--PRINT 'In ID is not null'
		   SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,      
			FileName,ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView,
		  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,      
		  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,      
			FN.UserFullName CreatedByFullName,      
			MN.UserFullName ModifiedByFullName,      
		  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, CodeFile, H.ParentId      
		  FROM SITemplate S       
		  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
		  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
		  INNER JOIN HSStructure H on S.Id = H.Id      
		  WHERE S.Id = isnull(@Id, S.Id) and      
		   isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and      
		   isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and      
		   (@ApplicationId IS NULL OR S.ApplicationId=@ApplicationId OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and      
		   S.Type=isnull(@Type,S.Type) and      
		   S.Status = isnull(@Status, S.Status)and       
			(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())      
		END	
		ELSE
		BEGIN	
			--PRINT 'In ID is null'
			 SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,      
			FileName,ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView,
		  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,      
		  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,      
			FN.UserFullName CreatedByFullName,      
			MN.UserFullName ModifiedByFullName,      
		  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, CodeFile, H.ParentId      
		  FROM SITemplate S       
		  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
		  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
		  INNER JOIN HSStructure H on S.Id = H.Id      
		  WHERE S.Id = isnull(@Id, S.Id) and      
		   isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and      
		   isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and      
		   (@ApplicationId IS NULL OR  S.ApplicationId=@ApplicationId OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId)) ) and      
		   S.Type=isnull(@Type,S.Type) and      
		   S.Status = isnull(@Status, S.Status)and       
			(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())      
			AND ((S.AllowAccessInChildrenSites IS NULL OR S.AllowAccessInChildrenSites = 1) OR (@ApplicationId IS NULL OR ApplicationId=@ApplicationId))
		END	
  END      
  ELSE      
  BEGIN  
     --Print 'In ThemeId is not null'
    SELECT S.Id,   
    CASE WHEN OT.Title IS NULL THEN S.Title ELSE OT.Title END AS Title,    
    S.Description, S.Status, S.CreatedBy, S.CreatedDate, S.ModifiedBy, S.ModifiedDate,      
    CASE WHEN OT.FileName IS NULL THEN S.FileName ELSE OT.FileName END AS FileName,  
    ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView,
  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,      
  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,      
    FN.UserFullName CreatedByFullName,      
    MN.UserFullName ModifiedByFullName,      
  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, OT.CodeFile, H.ParentId      
  FROM SITemplate S       
  LEFT JOIN SIObjectTheme OT ON OT.ObjectId = S.Id AND OT.ObjectTypeId = 3  AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId=@ThemeId)  
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
  INNER JOIN HSStructure H on S.Id = H.Id      
  WHERE   
  S.Id = isnull(@Id, S.Id) and      
   isnull(upper(OT.Title),'') = isnull(upper(@Title), isnull(upper(OT.Title),'')) and      
   isnull(upper(OT. FileName),'') = isnull(upper(@FileName), isnull(upper(OT.FileName),'')) and      
   (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and      
   S.Type=isnull(@Type,S.Type) and      
   S.Status = isnull(@Status, S.Status)and       
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())   
  END      
        
 END      
       
 ELSE      
 BEGIN      
  WITH ResultEntries AS (       
   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)      
    AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy,       
    ModifiedDate, FileName,ImageURL,Type,ApplicationId,      
 (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO ) StyleId,      
    (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO ) ScriptId,      
    [dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet, AllowAccessInChildrenSites, AmpEnabled, AmpView
   FROM SITemplate S      
   WHERE S.Id = isnull(@Id, S.Id) and      
    isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and      
    isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and      
    isnull(S.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(S.ApplicationId,@EmptyGUID)) and      
    S.Type=isnull(@Type,S.Type) and      
    S.Status = isnull(@Status, S.Status)and       
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))      
      
  SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,      
    FileName,ImageURL,Type,ApplicationId, StyleId,ScriptId,      
    FN.UserFullName CreatedByFullName,      
    MN.UserFullName ModifiedByFullName,      
    ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml, H.ParentId, EditorOptionsStyleSheet, EditorContentStyleSheet, AllowAccessInChildrenSites, AmpEnabled, AmpView
  FROM ResultEntries S      
  INNER JOIN HSStructure H on S.Id = H.Id      
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
   LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
  WHERE Row between       
   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize      
      
 END      
      
END
GO

PRINT 'Update stored procedure Template_GetTemplateByContainer'
GO

ALTER PROCEDURE [dbo].[Template_GetTemplateByContainer]
(
	@ContainerId		uniqueIdentifier ,
	@ApplicationId	uniqueidentifier=null,
	@Status			int=null,
	@Type			int=null
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier

BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,Type,ApplicationId,
				(SELECT TS.ContainerId FROM SITemplateContainer S WHERE S.ContainerId =TS.ContainerId FOR XML AUTO ) ContainerId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CodeFile, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView
		FROM SITemplate T INNER JOIN SITemplateContainer TS ON TS.ContainerId = T.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE	TS.ContainerId = @ContainerId and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
			T.Type=isnull(@Type,T.Type) and
			T.Status = isnull(@Status, T.Status)and 
				(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
END
GO

PRINT 'Update stored procedure Template_GetTemplateByHierarchy'
GO

ALTER PROCEDURE [dbo].[Template_GetTemplateByHierarchy]
(
	
	@Id					uniqueIdentifier=null ,
	@ApplicationId		uniqueidentifier=null,
	@FileName			nvarchar(4000)=null,
	@Level				int=null,
	@PageIndex			int=1,
	@PageSize			int=null,
	@Type				int=null 
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier
BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	IF @PageSize is Null
	BEGIN

		SELECT T.Id, T.Title, T.Description, T.Status, T.CreatedBy, T.CreatedDate, T.ModifiedBy, T.ModifiedDate,
				T.FileName,T.ImageURL,T.Type,T.ApplicationId,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE T.Id = CSS.TemplateId FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE T.Id = SC.TemplateId FOR XML AUTO ) ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				 CodeFile,PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView
		FROM SITemplate T 
		INNER JOIN dbo.GetChildrenByHierarchy(@Id,@Level) H ON T.ID = H.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE 
			T.Type=isnull(@Type,T.Type) and
			T.Status != dbo.GetDeleteStatus() and
			isnull(T.FileName,'') = isnull(@FileName,isnull(T.FileName,'')) and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) 
			
	END
	ELSE
	BEGIN
		WITH ResultEntries AS ( 
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)
				AS Row, T.Id, T.Title, T.Description, T.Status, T.CreatedBy, T.CreatedDate, T.ModifiedBy, 
				T.ModifiedDate, T.FileName,T.ImageURL,T.CodeFile,T.Type,T.ApplicationId,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE T.Id = CSS.TemplateId FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE T.Id = SC.TemplateId FOR XML AUTO ) ScriptId,
				PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView
			FROM SITemplate T, dbo.GetChildrenByHierarchy(@Id,@Level) H
			WHERE T.Id = H.Id and
				T.Type=isnull(@Type,T.Type) and
				 T.Status != dbo.GetDeleteStatus() and
				 isnull(T.FileName,'') = isnull(@FileName,isnull(T.FileName,'')) and
				isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) )

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,CodeFile,Type,ApplicationId,StyleId,ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView
		FROM ResultEntries A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE Row between 
			(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

	END

END
GO

PRINT 'Update stored procedure Template_GetTemplateByRelation'
GO

ALTER PROCEDURE [dbo].[Template_GetTemplateByRelation]
(
	@RelationName		varchar(256)=null,
	@ObjectId			varchar(8000)=null,
	@ReturnObjectType	int ,
	@Id					uniqueIdentifier=null ,
	@ApplicationId		uniqueidentifier=null,
	@Title				nvarchar(256)=null,
	@FileName			nvarchar(4000)=null,
	@Status				int=null,
	@PageIndex			int=1,
	@PageSize			int=null,
	@Type				int=null 
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier
BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	IF @PageSize is Null
	BEGIN

		SELECT T.Id, T.Title, T.Description, T.Status, T.CreatedBy, T.CreatedDate, T.ModifiedBy, T.ModifiedDate,
				T.FileName,T.ImageURL,T.Type,T.ApplicationId,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE T.Id = CSS.TemplateId FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE T.Id = SC.TemplateId FOR XML AUTO ) ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CodeFile,PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView
		FROM SITemplate T
		INNER JOIN dbo.GetObjectByRelationType(@RelationName,@ObjectId,@ReturnObjectType) R
		ON T.Id = R.ObjectId
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE  
			T.Id = isnull(@Id, T.Id) and
			isnull(upper(T.Title),'') = isnull(upper(@Title), isnull(upper(T.Title),'')) and
			isnull(upper(T. FileName),'') = isnull(upper(@FileName), isnull(upper(T.FileName),'')) and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
			T.Type=isnull(@Type,T.Type) and
			T.Status = isnull(@Status, T.Status)and 
			(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
	END
	ELSE
	BEGIN
		WITH ResultEntries AS ( 
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)
				AS Row, T.Id, T.Title, T.Description, T.Status, T.CreatedBy, T.CreatedDate, T.ModifiedBy, 
				T.ModifiedDate, T.FileName,T.ImageURL,T.CodeFile,T.Type,T.ApplicationId,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE T.Id = CSS.TemplateId FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE T.Id = SC.TemplateId FOR XML AUTO ) ScriptId,
				PagePart
			FROM SITemplate T, dbo.GetObjectByRelationType(@RelationName,@ObjectId,@ReturnObjectType) R
			WHERE T.Id = R.ObjectId and
				 T.Id = isnull(@Id, T.Id) and
				isnull(upper(T.Title),'') = isnull(upper(@Title), isnull(upper(T.Title),'')) and
				isnull(upper(T. FileName),'') = isnull(upper(@FileName), isnull(upper(T.FileName),'')) and
				isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
				T.Type=isnull(@Type,T.Type) and
				T.Status = isnull(@Status, T.Status)and 
				(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,CodeFile,Type,ApplicationId,StyleId,ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				PagePart
		FROM ResultEntries A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE Row between 
			(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

	END

END
GO

PRINT 'Update stored procedure Template_GetTemplateByScript'
GO

ALTER PROCEDURE [dbo].[Template_GetTemplateByScript]
(
	@ScriptId		uniqueIdentifier ,
	@ApplicationId	uniqueidentifier=null,
	@Status			int=null,
	@Type			int=null
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier

BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,Type,ApplicationId,
				(SELECT TS.ScriptId FROM SITemplateScript S WHERE S.ScriptId =TS.ScriptId FOR XML AUTO ) ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CodeFile,
				PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView
		FROM SITemplate T 
		INNER JOIN SITemplateScript TS ON TS.ScriptId = T.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE	TS.ScriptId = @ScriptId and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
			T.Type=isnull(@Type,T.Type) and
			T.Status = isnull(@Status, T.Status)and 
				(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
END
GO

PRINT 'Update stored procedure Template_GetTemplateByStyle'
GO

ALTER PROCEDURE [dbo].[Template_GetTemplateByStyle]
(
	@StyleId		uniqueIdentifier ,
	@ApplicationId	uniqueidentifier=null,
	@Status			int=null,
	@Type			int=null
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier

BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,CodeFile,Type,ApplicationId,
				(SELECT TS.StyleId FROM SITemplateStyle S WHERE S.StyleId =TS.StyleId FOR XML AUTO ) StyleId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CodeFile,
				PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,AllowAccessInChildrenSites, AmpEnabled, AmpView
		FROM SITemplate T INNER JOIN SITemplateStyle TS ON TS.StyleId = T.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE	TS.StyleId = @StyleId and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
			T.Type=isnull(@Type,T.Type) and
			T.Status = isnull(@Status, T.Status)and 
				(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
END
GO

PRINT 'Update stored procedure Template_GetTemplateByLayoutTemplate'
GO

ALTER PROCEDURE [dbo].[Template_GetTemplatesByLayoutTemplate]
(
	@LayoutTemplateId uniqueidentifier=null,
	@ApplicationId	uniqueidentifier=null
)

AS
BEGIN
SELECT [Id]
      ,[Title]
      ,[Description]
      ,[FileName]
      ,[ImageURL]
      ,[Type]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[Status]
      ,[CodeFile]
      ,[PagePart]
      ,[LayoutTemplateId]
      ,[ZonesXml]
	  ,[EditorOptionsStyleSheet]
	  ,[EditorContentStyleSheet]
	  ,AllowAccessInChildrenSites
	  ,AmpEnabled
	  ,AmpView
      ,[ApplicationId]
      ,(SELECT StyleId FROM SITemplateStyle CSS WHERE S.Id = CSS.TemplateId FOR XML AUTO ) StyleId
	  ,(SELECT ScriptId FROM SITemplateScript SC WHERE S.Id = SC.TemplateId FOR XML AUTO ) ScriptId
      ,FN.UserFullName CreatedByFullName
	  ,MN.UserFullName ModifiedByFullName
	  ,[dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath
  FROM [SITemplate] S
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
  WHERE Status != dbo.GetDeleteStatus()
		AND (@LayoutTemplateId IS NULL OR LayoutTemplateId = @LayoutTemplateId)
  		AND (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId)))
 
END
GO

PRINT 'Update stored procedure Template_Save'
GO

ALTER PROCEDURE [dbo].[Template_Save]
(
	@Id					uniqueidentifier =null OUT,
	@ApplicationId		uniqueidentifier=null,
	@Title				nvarchar(256)=null,
	@Description        nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier=null,
	@ModifiedDate    	datetime=null,
	@FileName            nvarchar(4000)=null,
	@ImageURL	         nvarchar(4000)=null,
	@CodeFile			 nvarchar(4000)=null,
	@TemplateType        int=null,
	@Status				 int=null,
	@PagePart			 bit=0,
	@LayoutTemplateId		 uniqueidentifier=null,
	@ZonesXml			xml = null,
	@ThemeId			UNIQUEIDENTIFIER = null,
	@EditorOptionsStyleSheet nvarchar(256) = null,
	@EditorContentStyleSheet nvarchar(256) = null,
	@AllowAccessInChildrenSites bit = 1,
	@AmpEnabled bit = 0,
	@AmpView nvarchar(256) = null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int
BEGIN
--********************************************************************************
-- code
--********************************************************************************

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	/* IF @Id specified, ensure exists */
   IF (@Id is not null)
       IF((SELECT count(*) FROM  SITemplate WHERE Id = @Id AND Status != dbo.GetDeleteStatus() 
			AND ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId))) = 0)
       BEGIN
			RAISERROR('NOTEXISTS||Id', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
       END

	SET @Now = getdate()
	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	/*
	Sankar(05/01/2014) : 
	1. Upload a template with DEFAULT Option => Insert a row in the sitemplate with IsThemed = 0
	2. Upload a template with SELECTED Theme => Insert a row in the sitemplate with IsThemed = 1
	
	*/


	--For insert, no need to worry about the theme as we always upload the default template 1st. There can not be a Theme without a default template.
	IF (@Id is null)			-- New INSERT
	BEGIN
		SET @stmt = 'TEMPLATE INSERT'
		SET @Id = newid();
		INSERT INTO SITemplate(
					   Id,	
					   ApplicationId,
					   Title,
                       Description,
					   CreatedBy,
                       CreatedDate,
					   Status,
                       FileName,
						ImageURL,
						CodeFile,	
                       Type,
						PagePart,
						LayoutTemplateId,
						ZonesXml,
						EditorOptionsStyleSheet,
						EditorContentStyleSheet,
						AllowAccessInChildrenSites,
						AmpEnabled,
						AmpView
						) 
					values (
						@Id,
						@ApplicationId,
						@Title,
						@Description,
						@ModifiedBy,
						@Now,
						@Status,
						@FileName,
						@ImageURL,
						@CodeFile,
						@TemplateType,
						@PagePart,
						@LayoutTemplateId,
						@ZonesXml,
						@EditorOptionsStyleSheet,
						@EditorContentStyleSheet,
						@AllowAccessInChildrenSites,
						@AmpEnabled,
						@AmpView
						)

		SELECT @error = @@error
		IF @error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END
    END
    ELSE			-- update
         BEGIN
			
			--We need to update the SIObjectTheme table if we have a theme id greater than 0; Themeid 0 is for default template and it should go to SITemplate table
			SET @stmt = 'TEMPLATE UPDATE'
			IF ( @ThemeId = dbo.GetEmptyGUID() OR @TemplateType = 1)
			BEGIN
				UPDATE	SITemplate  WITH (rowlock)	SET 
					ApplicationId=@ApplicationId,
					Title =@Title,
					Description=@Description,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=@Now,
					ImageURL=@ImageURL,
					CodeFile=@CodeFile,
					FileName=@FileName,
					--Type=@TemplateTyp ,
					Status =@Status,
					PagePart =@PagePart,
					LayoutTemplateId = ISNULL(@LayoutTemplateId, LayoutTemplateId),
					ZonesXml = ISNULL(@ZonesXml, ZonesXml),
					EditorOptionsStyleSheet = ISNULL(@EditorOptionsStyleSheet, EditorOptionsStyleSheet),
					EditorContentStyleSheet = ISNULL(@EditorContentStyleSheet, EditorContentStyleSheet),
					AllowAccessInChildrenSites = ISNULL(@AllowAccessInChildrenSites,AllowAccessInChildrenSites),
					AmpEnabled = ISNULL(@AmpEnabled, AmpEnabled),
					AmpView = ISNULL(@AmpView, AmpView)
 				WHERE 	Id    = @Id 
				--AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)

			END
			ELSE
			BEGIN
				
				--Update the SIObjectTheme table with the properties
				DELETE FROM SIObjectTheme WHERE ObjectId=@Id AND ObjectTypeId=3 AND ThemeId=@ThemeId
				
				INSERT INTO SIObjectTheme(Id,ObjectId,ThemeId,FileName, CodeFile, ObjectTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status,Title)
				VALUES (NEWID(),@Id,@ThemeId,@FileName,@CodeFile,3,@ModifiedBy,@Now,NULL,NULL,1,@Title) -- 3 for template

				UPDATE SITemplate SET IsThemed=1 WHERE Id=@Id
			END
			SELECT @error = @@error, @rowcount = @@rowcount

			IF @error <> 0
			BEGIN
				RAISERROR('DBERROR||%s',16,1,@stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END
END
GO

PRINT 'Add columns to COXMLForm'
GO

IF(COL_LENGTH('COXMLForm', 'AmpEnabled') IS NULL)
BEGIN
	ALTER TABLE COXMLForm ADD
		[AmpEnabled] BIT NOT NULL DEFAULT ((0)), 
		[AmpView] NVARCHAR(256) NULL
END
GO

PRINT 'Update stored procedure XMLForm_GetXMLForm'
GO

ALTER PROCEDURE [dbo].[XMLForm_GetXMLForm] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(  
 @Id uniqueidentifier =null,  
 @ApplicationId uniqueidentifier = null,  
 @PageSize int=null,  
 @PageIndex int=null,  
 @ThemeId uniqueidentifier,  
 @ResolveThemeId bit = null  
)  
as  
Begin  
  
DECLARE @EmptyGUID uniqueidentifier  
SET @EmptyGUID = dbo.GetEmptyGUID()  
  
IF (@ResolveThemeId IS NULL)  
 Set  @ResolveThemeId  = 0    
  
 IF(@Id IS NOT NULL)  --In some case, from code library API all content definition is pulled from a site, we dont have to make applicationid null in that case, its casuing issue if there are multisite and both site is having the same content definition name
  BEGIN  
	  -- This condition is added as XMLForm was not pulled in case of multisite. If contentdefinitoin was created in a different site and used in another site, if we want to use Themeing AND want to use content defintion from a different site then we may have to visit this again.
	  IF NOT EXISTS( Select 1 from SISIte S 
					INNER JOIN SITheme ST ON S.ThemeId = ST.Id
					where S.Id = @ApplicationId) 
		SET	@ApplicationId = NULL
  END

 If (@PageSize is null and @PageIndex is null)   
 Begin  
  
  IF(@ThemeId IS NOT NULL AND @ResolveThemeId = 1)  
   SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 13)   
  If(@Id is NOT NULL)
	BEGIN
		IF NOT EXISTS(Select 1 FROM SIObjectTheme where ObjectId = @Id and ThemeId = @ThemeId)
		BEGIN
			SET @ThemeId = NULL
		END
	  select A.Id,  
		ApplicationId,  
		Name,  
		XMLString,  
		CASE WHEN @ThemeId IS NULL THEN A.XsltString ELSE OT.CodeFile END AS XsltString,  
		A.Status,  
		Description,  
		A.XMLFileName,  
		CASE WHEN @ThemeId IS NULL THEN A.XSLTFileName ELSE OT.FileName END AS XSLTFileName,   
		A.CreatedDate,   
		A.CreatedBy,   
		A.ModifiedDate, A.ModifiedBy,   
		FN.UserFullName CreatedByFullName,  
		MN.UserFullName ModifiedByFullName,  
		@ThemeId,
		A.AllowAccessInChildrenSites,
		ImageUrl,
		A.AmpEnabled,
		A.AmpView
		FROM COXMLForm A  
		LEFT JOIN SIObjectTheme OT ON OT.ObjectId = A.Id  AND OT.ObjectTypeId = 13  
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
	  where A.Id = Isnull(@Id, A.Id)  
		 and  
		 (@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)))   
		 and  
		 A.Status != dbo.GetDeleteStatus() --= dbo.GetActiveStatus()  
		 AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId = @ThemeId)  
	END		 
	ELSE
		BEGIN
			with CTEXmlFormId(Id)
			AS(
			Select Id FROM COXMLForm A where 
			(@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) )  
			
			select  A.Id,  
			ApplicationId,  
			Name,  
			XMLString,  
			A.XsltString AS XsltString,  
			A.Status,  
			Description,  
			A.XMLFileName,  
			A.XSLTFileName AS XSLTFileName,   
			A.CreatedDate,   
			A.CreatedBy,   
			A.ModifiedDate, A.ModifiedBy,   
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName,  
			@ThemeId,
			A.AllowAccessInChildrenSites  ,
			A.ImageUrl,
			A.AmpEnabled,
			A.AmpView
			FROM COXMLForm A  
			INNER JOIN CTEXmlFormId X ON A.Id = X.Id
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
		  where   
			 (@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)))   
			 and  
			 A.Status != dbo.GetDeleteStatus() --= dbo.GetActiveStatus()  
			 AND (A.AllowAccessInChildrenSites IS NULL OR A.AllowAccessInChildrenSites = 1 OR (@ApplicationId IS NULL OR ApplicationId = @ApplicationId))  
		 
		END	 
 end  
 else    
 Begin  
  select  Id,  
    ApplicationId,  
    Name,  
    XMLString,  
    XsltString,  
    Status,  
    Description,  
    XMLFileName,  
    XSLTFileName, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
    CreatedByFullName, ModifiedByFullName,
	ImageUrl,
	AmpEnabled,
	AmpView
  from (select ROW_NUMBER() over (order by Id) as RowId,  
      Id,  
      ApplicationId,  
      Name,  
      XMLString,  
      XsltString,  
      Status,  
      Description,  
      XMLFileName,  
      XSLTFileName, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
      FN.UserFullName CreatedByFullName,  
      MN.UserFullName ModifiedByFullName,
	  A.AllowAccessInChildrenSites,
	  A.ImageUrl,
	  A.AmpEnabled,
	  A.AmpView
    from COXMLForm A  
    LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
    LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
    where Id = Isnull(@Id, Id)  
       and  
       (@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)))   
       and  
       Status != dbo.GetDeleteStatus()) --= dbo.GetActiveStatus()) -- it will change after creating updateStatus() mehod  
   as XMLFormWithRowNumbers  
  where RowId between ((@PageIndex -1) * @PageSize +1) and (@PageIndex * @PageSize)  
    
 end  
end
GO

PRINT 'Update stored procedure XMLForm_GetXMLFormByHierarchy'
GO

ALTER PROCEDURE [dbo].[XMLForm_GetXMLFormByHierarchy]
(
	
	@Id					uniqueIdentifier=null ,
	@ApplicationId		uniqueidentifier=null,
	@Level				int=null,
	@PageIndex			int=1,
	@PageSize			int=null 
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier
BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	IF @PageSize is Null
	BEGIN

		SELECT  T.Id,
		ApplicationId,	Name,
		XMLString,XsltString, 
		Status, Description, XMLFileName, 
		XSLTFileName, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		T.AllowAccessInChildrenSites,
		T.ImageUrl,
		T.AmpEnabled,
		T.AmpView
		FROM COXMLForm T
		INNER JOIN dbo.GetChildrenByHierarchy(@Id,@Level) H ON T.Id = H.Id 
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId =T.ModifiedBy
		WHERE 
			T.Status != dbo.GetDeleteStatus() and
			(@ApplicationId is null or T.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) 
			
	END
	ELSE
	BEGIN
		WITH ResultEntries AS ( 
				SELECT ROW_NUMBER() OVER (ORDER BY Name)
				AS Row, T.Id,
						ApplicationId,
						Name,
						XMLString,
						XsltString,
						Status,
						Description,
						XMLFileName,
						XSLTFileName, 
						CreatedBy, CreatedDate,
						ModifiedBy, ModifiedDate,
						T.AllowAccessInChildrenSites,
						T.ImageUrl,
						T.AmpEnabled,
						T.AmpView
						FROM COXMLForm T
						INNER JOIN dbo.GetChildrenByHierarchy(@Id,@Level) H ON H.Id = T.Id
			WHERE --T.Id = H.Id and
				 T.Status != dbo.GetDeleteStatus() and
				(@ApplicationId is null or T.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) 
				)

		SELECT  Id,
				ApplicationId,
				Name,
				XMLString,
				XsltString,
				Status,
				Description,
				XMLFileName,
				XSLTFileName, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, 
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				A.AllowAccessInChildrenSites,
				A.ImageUrl,
				A.AmpEnabled,
				A.AmpView
				FROM ResultEntries A
				LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE Row between 
			(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

	END

END
GO

PRINT 'Update stored procedure XMLForm_Save'
GO

ALTER PROCEDURE [dbo].[XMLForm_Save] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id uniqueidentifier =null output,
	@ApplicationId uniqueidentifier,
	@Name nvarchar(256),
	@Description nvarchar(1024)=null,
	@XMLString xml =null,
	@XsltString ntext=null,
	@Status int,
	@XMLFileName nvarchar(1024)=null,
	@XSLTFileName nvarchar(1024)=null,
	@CreatedBy uniqueidentifier,
    @ModifiedBy uniqueidentifier = null,
    @ModifiedDate datetime,
	@ThemeId uniqueidentifier = null,
	@AllowAccessInChildrenSites bit = 1,
	@ImageUrl nvarchar(max) = null,
	@AmpEnabled bit = 0,
	@AmpView nvarchar(256) = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
	@NewId		uniqueidentifier,
	@RowCount		INT,
	@Stmt 		VARCHAR(36),	
	@Error 		int,
	@Now datetime

--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@Id IS NOT NULL)
		IF (NOT EXISTS(SELECT 1 FROM   COXMLForm WHERE  Id = @Id))
		BEGIN
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END		
		

	SET @Now = getdate()
	
	IF (@Id IS NULL AND @ThemeId = dbo.GetEmptyGUID())			-- new insert
	BEGIN
		SET @Stmt = 'Create XMLForm'
		Select @NewId =newid()
		set @Status = dbo.GetActiveStatus()
		INSERT INTO COXMLForm  WITH (ROWLOCK)(
			Id,
			ApplicationId,
			Name,
			XMLString,
			XsltString,
			Status,
			Description,
			XMLFileName,
			XSLTFileName,
			CreatedBy,
			CreatedDate,
			IsThemed,
			AllowAccessInChildrenSites,
			ImageURL,
			AmpEnabled,
			AmpView
		)
		values
			(
				@NewId,
				@ApplicationId,
				@Name,
				@XMLString,
				@XsltString,
				@Status,
				@Description,
				@XMLFileName,
				@XSLTFileName,
				@CreatedBy,
				@Now,
				CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGUID() THEN 0 ELSE 1 END ,
				@AllowAccessInChildrenSites,
				@ImageUrl,
				@AmpEnabled,
				@AmpView
			)
		SELECT @Error = @@ERROR
	 	IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		SELECT @Id = @NewId
	END
	ELSE			-- Update
	BEGIN
		SET @Stmt = 'Modify XMLForm'
		IF ( @ThemeId = dbo.GetEmptyGUID())
		BEGIN

		Update COXMLForm WITH (ROWLOCK)
		Set 	
			--ApplicationId = @ApplicationId,
			Name = @Name,
			Status = @Status,
			Description = @Description,
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @Now, 
			XMLString = @XMLString,
			XMLFileName = @XMLFileName,				
			
			XsltString = 
				Case 
				WHEN ApplicationId = @ApplicationId
				THEN @XsltString
				ELSE XsltString
				END,			
			XSLTFileName = 
				Case 
				WHEN ApplicationId = @ApplicationId
				Then @XSLTFileName
				Else XSLTFileName
				End,
				AllowAccessInChildrenSites = @AllowAccessInChildrenSites,
				ImageURL = @ImageUrl,
				AmpEnabled = @AmpEnabled,
				AmpView = @AmpView
		Where Id = @Id --AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)

		END
		ELSE
			BEGIN
				IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@Id AND ThemeId=@ThemeId)
				BEGIN
					UPDATE SIObjectTheme 
					SET ModifiedBy=@ModifiedBy,
				   ModifiedDate=@Now,
				   FileName= @XSLTFileName,
				   CodeFile = @XsltString,
				   Status =@Status
				   WHERE ObjectId=@Id AND ThemeId=@ThemeId
				END
				ELSE
				BEGIN
		
					IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@Id AND ThemeId=@ThemeId)
					BEGIN
						UPDATE SIObjectTheme SET FileName=@XSLTFileName,Title=@Name, CodeFile = @XsltString,ModifiedBy=@ModifiedBy,ModifiedDate=@Now WHERE ObjectId=@Id AND ThemeId=@ThemeId
					END
					ELSE
					BEGIN
						INSERT INTO SIObjectTheme(Id,ObjectId,ThemeId,FileName, CodeFile, ObjectTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status,Title)
							VALUES (NEWID(),@Id,@ThemeId,@XSLTFileName,@XsltString,13,@ModifiedBy,@Now,NULL,NULL,1,@Name) -- 3 for template
					END		
					UPDATE COXMLForm SET IsThemed=1 WHERE Id=@Id
				END	
			END

		SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT

		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @RowCount = 0
		BEGIN
			--concurrency error
			RAISERROR('DBCONCURRENCY',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()
		END	
	END		
END
GO

PRINT 'Update stored procedure Content_GetContentsByDictionary'
GO

ALTER PROCEDURE [dbo].[Content_GetContentsByDictionary] 
--********************************************************************************
--Parameters
--********************************************************************************
(
@xmlGuids xml,
@ThemeId uniqueidentifier = null,
@ApplicationId uniqueidentifier = null,
@ResolveThemeId bit = 0
)
AS
BEGIN

	IF(@ThemeId IS NOT NULL AND @ResolveThemeId = 1)
			SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 13) 

	DECLARE @tbContentIds TABLE (Id uniqueidentifier, ContentTypeId int)
	INSERT INTO @tbContentIds
	SELECT DISTINCT T.c.value('@Id','uniqueidentifier'), T.c.value('@ContentTypeID','int')
		FROM @xmlGuids.nodes('/Contents/Content') T(c)
		
	DECLARE @tbParentFolder TABLE(ContentId uniqueidentifier primary key, ParentId uniqueidentifier, FolderPath nvarchar(1000), ContentTypeID int)
	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COContentStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3

	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COImageStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3


	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COFileStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3
		
	DECLARE @tbContentDefinitionIds TABLE(TemplateId uniqueidentifier, SiteId uniqueidentifier)
	INSERT INTO @tbContentDefinitionIds
		SELECT DISTINCT XMLString.value('(//contentDefinition/@TemplateId)[1]','uniqueidentifier'), C.ApplicationId  
			FROM COContent C INNER JOIN @tbParentFolder t on C.Id = t.ContentId 
		WHERE ObjectTypeId = 13
			
	
	SELECT Id,
		ApplicationId,
		Title,
		Description, 
		[Text], 
		URL,
		URLType, 
		ObjectTypeId, 
		CreatedDate,
		CreatedBy,
		ModifiedBy,
		ModifiedDate, 
		Status,
		RestoreDate, 
		BackupDate, 
		StatusChangedDate,
		PublishDate, 
		ArchiveDate, 
		XMLString,
		BinaryObject, 
		Keywords,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		OrderNo, SourceContentId,
		PA.ParentId ,
		PA.FolderPath  
	FROM COContent CO
		INNER JOIN @tbParentFolder PA ON CO.Id = PA.ContentId AND 
			(pa.ContentTypeID = 7 or pa.ContentTypeID = 13 ) and Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = CO.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = CO.ModifiedBy
	Order by OrderNo

	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ExcludeFromExternalSearch,            
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		C.ParentId,
		PA.VirtualPath  
	FROM COContent C 
		INNER JOIN COFile F ON C.Id = F.ContentId
		INNER JOIN @tbContentIds T ON T.Id = C.Id
		INNER JOIN COFileStructure PA ON C.ParentId = PA.Id	
				AND C.ObjectTypeId = 9 AND C.Status = 1
		LEFT JOIN VW_UserFullName FN ON FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN ON MN.UserId = C.ModifiedBy
	Order by C.OrderNo
		
	IF(@ThemeId IS NOT NULL AND @ThemeId <> dbo.GetEmptyGUID())
	BEGIN
	SELECT  xF.Id,
			xF.ApplicationId,
			xF.Name,
			xF.XMLString,
			CASE WHEN @ThemeId IS NULL THEN xF.XsltString ELSE ISNULL(OT.CodeFile,xF.XsltString) END AS XsltString, 
			--ISNULL(VX.XsltString, xF.XsltString) As XsltString,
			xF.Status,
			xF.Description,
			xF.XMLFileName,
			CASE WHEN @ThemeId IS NULL THEN xF.XSLTFileName ELSE ISNULL(OT.FileName,xF.XSLTFileName) END AS XSLTFileName, 
			CASE WHEN @ThemeId IS NULL THEN xF.CreatedDate ELSE ISNULL(OT.CreatedDate,xF.CreatedDate) END AS CreatedDate, 
			CASE WHEN @ThemeId IS NULL THEN xF.CreatedBy ELSE ISNULL(OT.CreatedBy,xF.CreatedBy) END AS CreatedBy, 
			CASE WHEN @ThemeId IS NULL THEN xF.ModifiedDate ELSE ISNULL(OT.ModifiedDate,xF.ModifiedDate) END AS ModifiedDate, 
			 CASE WHEN @ThemeId IS NULL THEN xF.ModifiedBy ELSE ISNULL(OT.ModifiedBy,xF.ModifiedBy) END AS ModifiedBy,  
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			OT.ThemeId,
			xF.ImageUrl,
			xf.AmpEnabled,
			xf.AmpView
	FROM COXMLForm xF
		INNER JOIN @tbContentDefinitionIds cD  On xF.Id  = cD.templateId
		LEFT JOIN VW_UserFullName FN on FN.UserId = xF.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = xF.ModifiedBy
		LEFT JOIN COXMLFormXslt VX on VX.SiteId = cD.SiteId And VX.XmlFormId = cD.templateId
		LEFT JOIN SIObjectTheme OT ON OT.ObjectId = xF.Id AND OT.ObjectTypeId = 13
		AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId = @ThemeId)
		
	END
	ELSE
	BEGIN
		SELECT  xF.Id,
			xF.ApplicationId,
			xF.Name,
			xF.XMLString,
			ISNULL(VX.XsltString, xF.XsltString) As XsltString,
			xF.Status,
			xF.Description,
			xF.XMLFileName,
			xF.XSLTFileName, xF.CreatedDate, xF.CreatedBy, 
			xF.ModifiedDate, xF.ModifiedBy, 
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			xF.ImageUrl,
			xf.AmpEnabled,
			xf.AmpView
	FROM COXMLForm xF
		INNER JOIN @tbContentDefinitionIds cD  On xF.Id  = cD.templateId
		LEFT JOIN VW_UserFullName FN on FN.UserId = xF.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = xF.ModifiedBy
		LEFT JOIN COXMLFormXslt VX on VX.SiteId = cD.SiteId And VX.XmlFormId = cD.templateId AND VX.Status != 3
	END
	
	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ExcludeFromExternalSearch,
		F.ImageHeight, F.ImageWidth,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo,	C.SourceContentId,
		C.ParentId,
		PA.VirtualPath  
	FROM COContent C 
		INNER JOIN COFile F On C.Id = F.ContentId
		INNER JOIN @tbContentIds T ON T.Id = C.Id
		INNER JOIN COImageStructure PA ON C.ParentId = PA.Id
				AND C.ObjectTypeId = 33 AND C.Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
	ORDER BY C.OrderNo
		
	SELECT C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ExcludeFromExternalSearch,
		F.ImageHeight, F.ImageWidth,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		dbo.GetEmptyGUID() ParentId ,
		'' FolderPath,
		V.BackgroundColor,
		V.DynamicStreaming,
		V.Height,
		V.IsUI,
		V.IsVideo,
		V.PlayerId,
		V.PlayerKey,
		V.VideoId,
		V.PlaylistId,
		V.VideoType,
		V.Width  
	FROM COContent C 
		INNER JOIN COFile F On C.Id = F.ContentId
		INNER JOIN COVideo V on V.ContentId=C.Id
		INNER JOIN @tbContentIds T ON T.Id = C.Id
				AND C.Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
	ORDER BY C.OrderNo
END
GO

PRINT 'Update view vwImage'
GO

ALTER VIEW [dbo].[vwImage]
AS
SELECT C.Id,
	C.ApplicationId,
	C.Title,
	C.Description,
	C.ParentId,
	F.FileName,
	REPLACE(ISNULL(F.Extension, ''), '.', '') AS Extension,
	F.FileSize,
	ISNULL(F.RelativePath, ISNULL(S.VirtualPath, '/' + S.Title)) AS RelativePath,
	F.MimeType,
	F.AltText,
	F.ImageHeight,
	F.ImageWidth,
	C.SourceContentId,
	C.CreatedBy,
	C.CreatedDate,
	C.ModifiedBy,
	C.ModifiedDate,
	C.Status,
	C.OrderNo,
	FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
FROM [dbo].[COContent] C
	JOIN COFile F ON C.Id = F.ContentId AND C.ObjectTypeId = 33
	LEFT JOIN COImageStructure S ON S.Id = C.ParentId
	LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
WHERE C.Status != 3
GO

PRINT 'Updating end date in the iappsproductsuite table'
GO

UPDATE iAppsProductSuite SET UpdateEndDate = GETUTCDATE()
GO
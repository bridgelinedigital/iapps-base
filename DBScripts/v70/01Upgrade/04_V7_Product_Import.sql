GO
PRINT 'Updating the version in the productsuite table'
GO
UPDATE iAppsProductSuite SET UpdateStartDate = GETUTCDATE(), ProductVersion='7.0.0609.0400'
GO

declare @tableName varchar(512)
declare @colName varchar(512)

set @tableName = 'BLD_Attribute'
set @colName = 'SiteId'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_Attribute add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
	print 'adding ['+ @tableName + '].[AttributeId]'
	alter table BLD_Attribute add AttributeId uniqueidentifier null
end

set @tableName = 'BLD_AttributeEnumValue'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_AttributeEnumValue add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
	print 'renaming ['+ @tableName + '].[Key] -> [Id]'
	exec sp_rename 'BLD_AttributeEnumValue.Key', 'Id', 'COLUMN'
	print 'adding ['+ @tableName + '].[Key]'
	alter table	BLD_AttributeEnumValue add [Key] uniqueidentifier
end

set @tableName = 'BLD_PriceSet'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_PriceSet add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
end

set @tableName = 'BLD_PriceSetSku'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_PriceSetSku add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
end

set @tableName = 'BLD_Product'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_Product add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
	print 'adding ['+ @tableName + '].[IsShared]'
	alter table BLD_Product add IsShared bit not null default(0)
end

if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME in ('SEOTitle', 'SEODescription', 'SEOKeywords', 'SEOFriendlyURL')))
begin
	print 'adding ['+ @tableName + '].[SEOTitle]'
	alter table BLD_Product add SEOTitle nvarchar(255)
	print 'adding ['+ @tableName + '].[SEODescription]'
	alter table BLD_Product add SEODescription nvarchar(max)
	print 'adding ['+ @tableName + '].[SEOKeywords]'
	alter table BLD_Product add SEOKeywords nvarchar(max)
	print 'adding ['+ @tableName + '].[SEOFriendlyURL]'
	alter table BLD_Product add SEOFriendlyURL nvarchar(max)
end

set @tableName = 'BLD_ProductAttribute'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_ProductAttribute add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
end

set @tableName = 'BLD_ProductImage'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_ProductImage add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
	print 'dropping SKU primary key constraint'
	alter table BLD_ProductImage drop constraint PK_BLD_ProductImage
	alter table BLD_ProductImage add Id int identity(1, 1)
	alter table BLD_ProductImage add constraint PK_BLD_ProductImage primary key clustered (Id asc, ProductId asc, Sequence asc)	alter table BLD_ProductImage alter column SKU nvarchar(255) null
end

if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME='Title'))
begin
	alter table BLD_ProductImage add Title nvarchar(256)
	alter table BLD_ProductImage add Description nvarchar(4000)
	alter table BLD_ProductImage add AltText nvarchar(1024)
end

IF(COL_LENGTH('BLD_ProductImage', 'IsShared') IS NULL)
BEGIN
	ALTER TABLE BLD_ProductImage ADD IsShared BIT NOT NULL DEFAULT 1
END


set @tableName = 'BLD_ProductRelation'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_ProductRelation add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
end

set @tableName = 'BLD_ProductType'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_ProductType add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
end

set @tableName = 'BLD_Sku'
print 'processing ' + @tableName
if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME=@colName))
begin
	print 'adding ['+ @tableName + '].[' + @colName +']'
	alter table BLD_Sku add SiteId uniqueidentifier not null default('00000000-0000-0000-0000-000000000000')
	print 'altering ['+ @tableName + '].[SKUTitle]'
	alter table BLD_Sku alter column SKUTitle nvarchar(555) not null
end

if (exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName) and
	not exists (select 1 from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' and TABLE_NAME=@tableName AND COLUMN_NAME='SelfShippable'))
begin
	print 'adding ['+ @tableName + '].[SelfShippable]'
	alter table BLD_Sku add SelfShippable bit not null default(0)
end
go

print 'altering spDeleteLoadedProducts'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='spDeleteLoadedProducts')
    drop procedure spDeleteLoadedProducts
go

CREATE PROCEDURE [dbo].[spDeleteLoadedProducts]
(
	@SinceDate DATETIME = NULL     
)
AS 
BEGIN
	begin try
		begin transaction
			IF @SinceDate IS NULL 
				BEGIN
					SET @SinceDate = '4-10-2012'
				END

			DELETE  FROM dbo.PRProductCategory 
                    
			UPDATE  dbo.PRProductSKUAttributeValue
			SET     AttributeEnumId = NULL
  
			UPDATE  dbo.PRProductAttributeValue
			SET     AttributeEnumId = NULL

			DELETE  FROM dbo.CLObjectImage
			WHERE   ObjectId IN ( SELECT    Id
								  FROM      dbo.PRProduct
								  WHERE     CreatedDate > @SinceDate )
            
			DELETE	FROM dbo.CLObjectImage
			WHERE	ObjectId IN ( SELECT	Id
								  FROM		dbo.PRProductSKU
								  WHERE		CreatedDate > @SinceDate )                           
                           
			DELETE  FROM dbo.CLImage
			WHERE   Id NOT IN ( SELECT  ImageID
								FROM    dbo.CLObjectImage )

			Select ID into #DeletedAttributeCategoryIds 
			From ATAttributeCategory
			WHERE (IsSystem=0 and CreatedDate>@SinceDate and CreatedBy is not null or Name in ('Product Type Product Attributes', 'Order Item', 'Product Type SKU Attributes'))

			select AttributeId as ID into #DeletedAttributeIds 
			from dbo.ATAttributeCategoryItem
			where CategoryId in (select ID from #DeletedAttributeCategoryIds)


			Select ID Into #DeletedFacetIds From ATFacet WHERE AttributeId IN (SELECT ID FROM #DeletedAttributeIds)

			Delete From ATFacetRangeDate Where FacetRangeId in (Select Id From ATFacetRange Where FacetId in (Select Id from #DeletedFacetIds))
			Delete From ATFacetRangeDecimal Where FacetRangeId in (Select Id From ATFacetRange Where FacetId in (Select Id from #DeletedFacetIds))
			Delete From ATFacetRangeInt Where FacetRangeId in (Select Id From ATFacetRange Where FacetId in (Select Id from #DeletedFacetIds))
			Delete From ATFacetRangeString Where FacetRangeId in (Select Id From ATFacetRange Where FacetId in (Select Id from #DeletedFacetIds))
			Delete From ATFacetRange Where FacetId in (Select Id from #DeletedFacetIds)

			Delete NVNavNodeFacet Where FacetId in (Select Id from #DeletedFacetIds)

			Delete From ATFacet WHERE Id IN (Select Id from #DeletedFacetIds)
			
			DELETE FROM PRProductTypeAttributeValue where AttributeId in (select Id from #DeletedAttributeIds)
			DELETE  FROM dbo.ATAttributeEnum WHERE AttributeId IN (SELECT ID FROM #DeletedAttributeIds)

			DELETE  FROM dbo.PRProductToProductRel
			WHERE   ProductId IN ( SELECT   Id
								   FROM     dbo.PRProduct
								   WHERE    CreatedDate > @SinceDate )
					OR RelatedProduct IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate )
                    
			DELETE  FROM dbo.CSWishListItem
			WHERE   SKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )

			DELETE  FROM dbo.PSPriceSetManualSKU
			WHERE   SKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )
                 
			DELETE  FROM FFOrderShipmentITem
			WHERE   OrderItemId IN (
					SELECT  ID
					FROM    dbo.OROrderItem
					WHERE   ProductSKUId IN (
							SELECT  ID
							FROM    dbo.PRProductSKU
							WHERE   ProductId IN ( SELECT   Id
												   FROM     dbo.PRProduct
												   WHERE    CreatedDate > @SinceDate ) ) )
            
			DELETE  FROM dbo.OROrderItemAttributeValue
			WHERE   SKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )
                    
			DELETE  FROM dbo.PSPriceSetSKU
			WHERE   SkuId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )
                    

			DELETE  FROM dbo.CSCartItem
			WHERE   ParentCartItemId IS NOT NULL 
                    
			DELETE  FROM dbo.CSCartItem
			WHERE   SkuId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )


			Delete from OROrderItemCouponCode
			Where OrderItemId in
				(
			Select OrderItemId From dbo.OROrderItem
			WHERE   ProductSKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )
				)

			Delete from OROrderItemCouponVersion
			Where OrderItemId in
				(
			Select OrderItemId From dbo.OROrderItem
			WHERE   ProductSKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )
				)

			delete from dbo.ORRefundItem
			Where OrderItemId in
				(
			Select OrderItemId From dbo.OROrderItem
			WHERE   ProductSKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )
				)


			delete from dbo.ORReturnItem
			Where OrderItemId in
				(
			Select OrderItemId From dbo.OROrderItem
			WHERE   ProductSKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )
				)

                          
			DELETE  FROM dbo.OROrderItem
			WHERE   ProductSKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )

			delete from dbo.PRProductMedia
			WHERE   SKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )

			DELETE  FROM dbo.INInventoryLog
			WHERE   InventoryId IN (
					SELECT  ID
					FROM    dbo.INInventory
					WHERE   SKUId IN (
							SELECT  ID
							FROM    dbo.PRProductSKU
							WHERE   ProductId IN ( SELECT   Id
												   FROM     dbo.PRProduct
												   WHERE    CreatedDate > @SinceDate ) ) )
                    
			DELETE  FROM dbo.INInventory
			WHERE   SKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductID IS NULL OR ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )

			DELETE  FROM dbo.PRProductSKUAttributeValue
			WHERE   SKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )
                                     
 

			DELETE  FROM dbo.PRProductAttributeValue
			WHERE   ProductId IN ( SELECT   Id
								   FROM     dbo.PRProduct
								   WHERE    CreatedDate > @SinceDate )

			DELETE  FROM dbo.PRProductAttribute
			WHERE   ProductId IN ( SELECT   Id
								   FROM     dbo.PRProduct
								   WHERE    CreatedDate > @SinceDate )

			DELETE  FROM dbo.MRFeatureOutput
			WHERE   ProductId IN ( SELECT   Id
								   FROM     dbo.PRProduct
								   WHERE    CreatedDate > @SinceDate )

			DELETE  FROM dbo.PRProductToProductRel
			WHERE   RelatedProduct IN ( SELECT  Id
										FROM    dbo.PRProduct
										WHERE   CreatedDate > @SinceDate )

			DELETE  FROM dbo.CPCouponProduct
			WHERE   ProductId IN ( SELECT   Id
								   FROM     dbo.PRProduct
								   WHERE    CreatedDate > @SinceDate )

			DELETE  FROM dbo.ORReturnItem
			WHERE   SKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )   
     
			DELETE  FROM dbo.CPCouponSKU
			WHERE   SKUId IN (
					SELECT  ID
					FROM    dbo.PRProductSKU
					WHERE   ProductId IN ( SELECT   Id
										   FROM     dbo.PRProduct
										   WHERE    CreatedDate > @SinceDate ) )    
                    
			DELETE  FROM dbo.PRProductSKU
			WHERE   ProductId IS NULL OR ProductId IN ( SELECT   Id
								   FROM     dbo.PRProduct
								   WHERE    CreatedDate > @SinceDate)

			DELETE  FROM dbo.MRFeatureManualItems
			WHERE   ProductId IN ( SELECT   Id
								   FROM     dbo.PRProduct
								   WHERE    CreatedDate > @SinceDate )    
						   
								   DELETE  FROM dbo.PRProductCategory     
									WHERE   ProductId IN ( SELECT   Id
								   FROM     dbo.PRProduct
								   WHERE    CreatedDate > @SinceDate )      
								   
			DELETE  FROM dbo.PRProduct
			WHERE   CreatedDate > @SinceDate


			DELETE  FROM dbo.ATAttributeEnum WHERE   AttributeID IN (SELECT ID FROM #DeletedAttributeIds )

			DELETE  FROM dbo.NVNavNodeFacet WHERE   AttributeID IN (SELECT ID FROM #DeletedAttributeIds)

			DELETE FROM dbo.PRProductAttribute WHERE AttributeId IN (SELECT ID FROM #DeletedAttributeIds)
                                  
			-- Only one root record (ParentId = SiteId) allowed by UI, delete all rows and leave just a single root record
			declare @rootTypeId uniqueidentifier
			set @rootTypeId = (select top 1 Id from dbo.PRProductType where ParentId = SiteId order by CreatedDate, Title)

			select Id into #DeletedProductTypeAttribute from PRProductTypeAttribute where AttributeId in (SELECT ID FROM #DeletedAttributeIds)
			delete from PRProductTypeAttribute where Id in (select Id from #DeletedProductTypeAttribute )

			DELETE FROM dbo.ATAttribute WHERE ID in (SELECT ID FROM #DeletedAttributeIds)
                    
			delete from dbo.ATAttributeCategoryItem where AttributeId in (SELECT ID FROM #DeletedAttributeIds)
			DELETE  FROM dbo.ATAttributeCategory WHERE ID IN (SELECT ID FROM #DeletedAttributeCategoryIds) and (IsNull(ProductId, '00000000-0000-0000-0000-000000000000') <> 'CCF96E1D-84DB-46E3-B79E-C7392061219B')


			-- begin ProductType
			delete from dbo.PSPriceSetProductType
	
			delete from dbo.PRProductType where Id != @rootTypeId and Id in (select Id from #DeletedProductTypeAttribute)

			-- Update the single root type 
			update dbo.PRProductType set LftValue = 1, RgtValue = 2 where Id = @rootTypeId
			-- end ProductType
		commit tran
	end try
	begin catch
		if (@@TRANCOUNT	> 0) rollback tran;
		throw;
	end catch
END
GO

print 'altering BLD_ProductImport_Product'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_Product')
    drop procedure BLD_ProductImport_Product
go

CREATE PROCEDURE [dbo].[BLD_ProductImport_Product]
	@ProductIDUser nvarchar(max),
	@ProductType nvarchar(max) = null,
	@UrlFriendlyTitle nvarchar(max) = null,
	@Title nvarchar(max) = null,
	@ShortDescription nvarchar(max) = null,
	@LongDescription nvarchar(max) = null,
	@SEODescription nvarchar(max) = null,
	@SEOKeywords nvarchar(max) = null,
	@SEOTitle nvarchar(max) = null,
	@SEOFriendlyURL nvarchar(max) = null,
	@IsActive bit = null,
	@IsOnline bit = null,
	@IsBundle bit = null,
	@TaxExempt bit = null,
	@OperatorId uniqueidentifier = null,
	@ProductIDUserOld nvarchar(max) = null,
	@ProductIdGuid uniqueidentifier = null,
	@IsShared bit
AS
BEGIN
	SET NOCOUNT ON;

	declare @actionType int = 0

	BEGIN TRY
        BEGIN TRAN

		if @OperatorId is null
		begin
			set @OperatorId = (select top 1 CreatedBy from PRProductType where CreatedBy is not null);
		end
		

		declare @ProductId uniqueidentifier;
		declare @IsNewProduct bit
		declare @ProductTypeId uniqueidentifier;
		declare @TaxCategoryId uniqueidentifier;
		Set @IsNewProduct = 1

		If (@ProductIdGuid IS NOT NULL)
			SET @ProductId = @ProductIdGuid

		-- TODO: fix this since title may not be unique
		select @ProductTypeId = Id from PRProductType where Title like @ProductType

		IF @ProductId IS NULL
		BEGIN 
			if @ProductIDUserOld is null
			begin
		
				select @ProductId = Id FROM PRProduct where ProductIDUser like @ProductIDUser;
			
			end else
			begin
				select @ProductId = Id FROM PRProduct where ProductIDUser like @ProductIDUserOld;
			end
		END

		IF EXISTS(Select 1 From PRProduct where Id = @ProductId)
			SET @IsNewProduct = 0

		--INSERTING THE PRODUCT
		if @IsNewProduct = 1
		begin
		
			if(@TaxExempt=1)
			begin
				select @TaxCategoryId = Id from PRTaxCategory where Title like '%non%';
			end else
			begin
				select @TaxCategoryId = Id from PRTaxCategory where Title not like '%non%';
			end


			if(@ProductTypeId is null)RAISERROR ('Unknown product type!', 16, 1);
			if(@TaxCategoryId is null)RAISERROR ('Unknown tax category!', 16, 1);
		
			IF(@ProductId IS NULL)
				SET @ProductId = newid();

			insert into PRProduct (
				Id, 
				ProductIdUser, 
				Title, 
				UrlFriendlyTitle, 
				ShortDescription, 
				LongDescription, 
				Description, 
				KeyWord, 
				IsActive, 
				ProductTypeId, 
				ProductStyle, 
				SiteId, 
				CreatedDate, 
				CreatedBy, 
				ModifiedDate, 
				ModifiedBy, 
				Status, 
				IsBundle, 
				BundleCompositionLastModified, 
				TaxCategoryId,
				TaxExempt, 
				SEOH1, 
				SEODescription, 
				SEOKeywords, 
				SEOFriendlyUrl,
				IsShared)
			values (
				@ProductId, 
				@ProductIDUser, 
				@Title, 
				@UrlFriendlyTitle, 
				@ShortDescription, 
				@LongDescription, 
				@LongDescription, 
				'', 
				@IsActive, 
				@ProductTypeId, 
				null, 
				'8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', 
				GETDATE(), 
				@OperatorId, 
				null, 
				null, 
				1, 
				@IsBundle, 
				null, 
				@TaxCategoryId, 
				@TaxExempt, 
				@SEOTitle, 
				@SEODescription, 
				@SEOKeywords, 
				@SEOFriendlyURL,
				@IsShared);

			set @actionType = 1

		end else
		begin
		
			update PRProduct set
				ProductIDUser = @ProductIDUser,
				Title = isnull(@Title, Title),
				UrlFriendlyTitle = isnull(@UrlFriendlyTitle, UrlFriendlyTitle),
				ShortDescription = isnull(@ShortDescription, ShortDescription),
				LongDescription = isnull(@LongDescription, LongDescription),
				Description = isnull(@LongDescription, Description),
				-- Keyword = isnull(@Keyword, Keyword),
				IsActive = isnull(@IsActive, IsActive),
				ProductTypeID = isnull(@ProductTypeId, ProductTypeID),
				ModifiedDate = GETDATE(),
				ModifiedBy = isnull(@OperatorId, ModifiedBy),
				IsBundle = isnull(@IsBundle, IsBundle),
				TaxCategoryID = isnull(@TaxCategoryID, TaxCategoryID),
				TaxExempt = isnull(@TaxExempt, TaxExempt),
				SEOH1 = @SEOTitle,
				SEODescription = @SEODescription,
				SEOKeywords = @SEOKeywords,
				SEOFriendlyURL = @SEOFriendlyURL,
				IsShared = @IsShared
			where Id = @ProductId;

			set @actionType = 2
			
		end

		COMMIT TRAN

		select @actionType, @ProductId

    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX)
        SET @ErrorMessage = ERROR_MESSAGE()
        RAISERROR(@ErrorMessage,16, 1)
    END CATCH

END
GO

print 'altering AttributeGroup_Save'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='AttributeGroup_Save')
    drop procedure AttributeGroup_Save
go

CREATE PROCEDURE [dbo].[AttributeGroup_Save]
(  
--********************************************************************************  
-- Parameters Passing in Attribute Group ID  
--********************************************************************************  
   @Id uniqueidentifier = null OUTPUT,
   @Title nvarchar(max),   
   @ApplicationId uniqueidentifier,  
   @IsSystem bit =0,  
   @CreatedBy uniqueidentifier,  
   @ModifiedBy uniqueidentifier,  
   @ModifiedDate datetime =null,  
   @Status int
)  
AS  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @maxId int = 50
DECLARE @Now dateTime  
DECLARE @NewGuid uniqueidentifier
DECLARE @RowCount int 
  
--********************************************************************************  
-- code  
--********************************************************************************  
BEGIN  
  
 IF (@Id is null)  -- New Insert  
  BEGIN  
	set @NewGuid = NEWID()
	set @Id = @NewGuid
	INSERT INTO ATAttributeCategory 
	(
		Id,
		Name,   
		SiteId,   
		IsSystem,  
		CreatedDate,   
		CreatedBy,   
		ModifiedDate,   
		ModifiedBy,   
		[Status],
		GroupId) 
	(select
		max(Id)+1,
		@Title,  
		@ApplicationId,  
		Isnull(@IsSystem,0),  
		getUTCDate(),  
		@CreatedBy,  
		NULL,  
		NULL,  
		@Status,
		@NewGuid
		from ATAttributeCategory)

  END  
 ELSE    -- Update the AttributeGroup  
  Begin  
  
     IF((SELECT Count(Id) FROM  ATAttributeCategory WHERE GroupId = @Id ) = 0)  
     BEGIN  
    RAISERROR('NOTEXISTS||Id', 16, 1)  
    RETURN dbo.GetBusinessRuleErrorCode()  
     END  
  End  
  
  
  BEGIN  
   SET @Now = getUTCDate()  
   IF (@ModifiedDate IS NULL)  
    BEGIN  
     UPDATE ATAttributeCategory WITH (ROWLOCK)  
     SET     [Name]    =  @Title,  
       [SiteId]   = @ApplicationId,  
       [IsSystem]   =   Isnull(@IsSystem,0),  
       [ModifiedDate]  = @Now,  
       [ModifiedBy]  =  @ModifiedBy,   
       [Status]   = @Status  
     WHERE   [GroupId]             = @Id   
      
    END  
   ELSE  
    BEGIN  
    Set @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId)

     UPDATE ATAttributeCategory WITH (ROWLOCK)  
     SET     [Name]    =  @Title,  
       [SiteId]   = @ApplicationId,  
       [IsSystem]   =   @IsSystem,  
       [ModifiedDate]  = @Now,  
       [ModifiedBy]  =  @ModifiedBy,   
       [Status]   = @Status  
     WHERE   [GroupId]             = @Id   
       AND  
       [ModifiedDate]  = @ModifiedDate  
  
  
     -- last modified date when record was read should match with the current Modified Date   
     -- in the table for the record otherwise it means that someone has modified it already.  
  
     SELECT  @RowCount = @@ROWCOUNT  
     IF @@ERROR <> 0  
     BEGIN  
      RETURN dbo.GetDataBaseErrorCode()  
     END  
       PRINT @RowCount  
     IF @RowCount = 0  
     BEGIN  
      /* concurrency error */  
      RAISERROR('DBCONCURRENCY',16,1)  
      RETURN dbo.GetDataConcurrencyErrorCode()                                     
     END    
  
    END  
  
  END  
  
END
GO

print 'altering ProductAttribute_Save'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='ProductAttribute_Save')
    drop procedure ProductAttribute_Save
go

CREATE PROCEDURE [dbo].[ProductAttribute_Save]
(
--********************************************************************************
-- Parameters Passing in Attribute Group ID
--********************************************************************************
			@Id uniqueidentifier = null OUTPUT,
			@AttributeGroupId uniqueidentifier=null,
			@Title nvarchar(max), 
			@ModifiedBy uniqueidentifier,
			@ModifiedDate datetime=null,
			@AttributeDataType nvarchar(30)=null,
			@AttributeDataTypeId uniqueidentifier=null,
			@Description nvarchar(400) =null,
			@Sequence int,
			@Code nvarchar(128) =null,
			@IsFaceted bit,
			@IsSearched bit,
			@IsSystem bit =0,
			@IsDisplayed bit=1,
			@IsEnum bit=0,
			@IsRequired bit=0,
			@Status int,
			@ApplicationId uniqueidentifier,
			@IsPersonalized  bit=0
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Now dateTime
DECLARE @NewGuid uniqueidentifier
DECLARE @RowCount int

--********************************************************************************
-- code
--********************************************************************************
BEGIN

	IF (@Id is null)  -- New Insert
		BEGIN
			SET @NewGuid = newid()
			set @Id= @NewGuid
			
			Declare @currentSequence int
			declare @attributeCategoryId int
			Select @currentSequence = Isnull(max(Sequence),0) from ATAttribute 
			
			IF @AttributeGroupId IS NOT NULL AND @AttributeGroupId != '00000000-0000-0000-0000-000000000000'
			BEGIN
				select @attributeCategoryId=Id from ATAttributeCategory where GroupId=@AttributeGroupId
				if (@attributeCategoryId is null)
				begin
					raiserror('NOTEXISTS||AttributeGroupId', 16, 1)
					return dbo.GetBusinessRuleErrorCode()
				end
			END
			INSERT INTO ATAttribute(
				Id, 
				AttributeDataType,
				AttributeDataTypeId, 
				Title,
				[Description],
				Sequence,
				Code,
				IsFaceted,
				IsSearched,
				IsSystem,
				IsDisplayed,
				IsEnum,
				CreatedDate, 
				CreatedBy, 
				[Status],
				IsPersonalized,
				SiteId)
		   VALUES(
				@NewGuid,
				@AttributeDataType,
				@AttributeDataTypeId,
				@Title,
				@Description,
				@currentSequence+1,
				@Code,
				@IsFaceted,
				@IsSearched,
				@IsSystem,
				@IsDisplayed,
				@IsEnum,
				getUTCDate(),
				NULL,
				@Status,
				@IsPersonalized,
				@ApplicationId
		   )
		   IF(@AttributeGroupId IS NOT NULL AND @AttributeGroupId != '00000000-0000-0000-0000-000000000000')
		   insert ATAttributeCategoryItem (Id, AttributeId, CategoryId, IsRequired) values (newid(), @NewGuid, @attributeCategoryId, @IsRequired)
		END
	ELSE				-- Update the AttributeGroup
		Begin
		   IF((SELECT Count(Title) FROM  ATAttribute WHERE Id = @Id) = 0)
		   BEGIN
				RAISERROR('NOTEXISTS||Id', 16, 1)
				RETURN dbo.GetBusinessRuleErrorCode()
		   END
		End


		BEGIN
			SET @Now = getUTCDate()
			IF (@ModifiedDate IS NULL)
				BEGIN
					UPDATE	ATAttribute  WITH (ROWLOCK)
					SET	AttributeDataType		= @AttributeDataType, 
						AttributeDataTypeId		= @AttributeDataTypeId, 
						Title					= @Title,
						[Description]			= @Description,
						Sequence				= @Sequence,
						Code					= @Code,
						IsFaceted				= @IsFaceted,
						IsSearched				= @IsSearched,
						IsSystem				= @IsSystem,
						IsDisplayed				= @IsDisplayed,
						IsEnum					= @IsEnum,
						[ModifiedDate]			= @Now,
						[ModifiedBy]			= @ModifiedBy,	
						[Status]				= @Status,
						[IsPersonalized]		=@IsPersonalized
					WHERE   [Id]           		=	@Id 
				
				END
			ELSE
				BEGIN
				Set @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId)

					UPDATE	ATAttribute  WITH (ROWLOCK)
					SET	AttributeDataType		= @AttributeDataType, 
						AttributeDataTypeId		= @AttributeDataTypeId, 
						Title					= @Title,
						[Description]			= @Description,
						Sequence				= @Sequence,
						Code					= @Code,
						IsFaceted				= @IsFaceted,
						IsSearched				= @IsSearched,
						IsSystem				= @IsSystem,
						IsDisplayed				= @IsDisplayed,
						IsEnum					= @IsEnum,
						[ModifiedDate]			= @Now,
						[ModifiedBy]			= @ModifiedBy,	
						[Status]				= @Status,
						[IsPersonalized]		=@IsPersonalized
					WHERE   
						[Id]           		=	@Id 
						AND
						[ModifiedDate]		= @ModifiedDate				


					-- last modified date when record was read should match with the current Modified Date 
					-- in the table for the record otherwise it means that someone has modified it already.

					SELECT  @RowCount = @@ROWCOUNT
					IF @@ERROR <> 0
					BEGIN
						RETURN dbo.GetDataBaseErrorCode()
					END
					  PRINT @RowCount
					IF @RowCount = 0
					BEGIN
						/* concurrency error */
						RAISERROR('DBCONCURRENCY',16,1)
						RETURN dbo.GetDataConcurrencyErrorCode()                                   
					END  

				END
				--update attribute name referred in navfilter items table
				update NVFilterItem set LftOpndName= @Title where LftOpnd=@Id
		END

END
GO

print 'altering BLD_ExportProductAttributes'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportProductAttributes')
    drop procedure BLD_ExportProductAttributes
go

CREATE PROCEDURE [dbo].[BLD_ExportProductAttributes]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		Begin
			select
				 '' as 'Key'
				,p.ProductIDUser as ProductId
				,'' as SKU
				,a.Title as AttributeTitle
				,coalesce(pa.IsSKULevel, 0) as IsSKULevel
				,pa.IsRequired
				,0 as IsDefault -- NOTE: the product loader and importer do not currently use this and it should probably be moved to the AttributeEnum tab
				,pav.Value
				,ag.Name as AttributeCategoryName
				,0 as UpdateFlag
			from dbo.PRProductAttributeValue pav
			join dbo.ATAttribute a on (pav.AttributeId = a.Id)
			join dbo.ATAttributeCategoryItem aci on (a.id = aci.AttributeId)
			join dbo.ATAttributeCategory ag on (ag.Id = aci.CategoryId) 
			join dbo.PRProductAttribute pa on (pav.ProductAttributeId = pa.Id)
			join dbo.PRProduct p on (pa.ProductId = p.Id)
			where 
				--ag.IsSystem = 0 and 
				(ag.IsGlobal != 1 or ag.IsGlobal is null)

			union all

			select
				 '' as 'Key'
				,p.ProductIDUser as ProductId
				,s.SKU
				,a.Title as AttributeTitle
				,coalesce(pa.IsSKULevel, 0) as IsSKULevel
				,pa.IsRequired
				,0 as IsDefault -- NOTE: the product loader and importer do not currently use this and it should probably be moved to the AttributeEnum tab
				,psav.Value
				,ag.Name as AttributeCategoryName
				,0 as UpdateFlag
			from dbo.PRProductSKUAttributeValue psav
			join dbo.ATAttribute a on (psav.AttributeId = a.Id)
			join dbo.ATAttributeCategoryItem aci on (a.id = aci.AttributeId)
			join dbo.ATAttributeCategory ag on (ag.Id = aci.CategoryId)
			join dbo.PRProductAttribute pa on (psav.ProductAttributeId = pa.Id)
			join dbo.PRProduct p on (pa.ProductId = p.Id)
			join dbo.PRProductSKU s on (psav.SKUId = s.Id)
			where 
				--ag.IsSystem = 0 and 
				(ag.IsGlobal != 1 or ag.IsGlobal is null)
		END
	ELSE
		Begin
			select
				 '' as 'Key'
				,p.ProductIDUser as ProductId
				,'' as SKU
				,a.Title as AttributeTitle
				,coalesce(pa.IsSKULevel, 0) as IsSKULevel
				,pa.IsRequired
				,0 as IsDefault -- NOTE: the product loader and importer do not currently use this and it should probably be moved to the AttributeEnum tab
				,ISNULL(pavc.Value, pav.Value) as Value
				,ag.Name as AttributeCategoryName
				,0 as UpdateFlag
			from dbo.PRProductAttributeValueVariantCache pavc
			join dbo.PRProductAttributeValue pav on (pav.Id = pavc.Id)
			left join dbo.ATAttribute a on (pav.AttributeId = a.Id)
			join dbo.ATAttributeCategoryItem aci on (a.id = aci.AttributeId)
			left join dbo.ATAttributeCategory ag on (ag.Id = aci.CategoryId) 
			left join dbo.PRProductAttribute pa on (pav.ProductAttributeId = pa.Id)
			left join dbo.PRProduct p on (pa.ProductId = p.Id)
			where 
				--ag.IsSystem = 0 and 
				(ag.IsGlobal != 1 or ag.IsGlobal is null) and
				pavc.SiteId = @siteId

			union all

			select 
				 '' as 'Key'
				,p.ProductIDUser as ProductId
				,s.SKU
				,a.Title as AttributeTitle
				,coalesce(pa.IsSKULevel, 0) as IsSKULevel
				,pa.IsRequired
				,0 as IsDefault -- NOTE: the product loader and importer do not currently use this and it should probably be moved to the AttributeEnum tab
				,ISNULL(psavc.Value, psav.Value) as Value
				,ag.Name as AttributeCategoryName
				,0 as UpdateFlag
			from dbo.PRProductSKUAttributeValueVariantCache psavc
			join dbo.PRProductSKUAttributeValue psav on (psavc.Id = psav.Id)
			left join dbo.ATAttribute a on (psav.AttributeId = a.Id)
			join dbo.ATAttributeCategoryItem aci on (a.id = aci.AttributeId)
			left join dbo.ATAttributeCategory ag on (ag.Id = aci.CategoryId)
			left join dbo.PRProductAttribute pa on (psav.ProductAttributeId = pa.Id)
			left join dbo.PRProduct p on (pa.ProductId = p.Id)
			left join dbo.PRProductSKU s on (psav.SKUId = s.Id)
			where 
				--ag.IsSystem = 0 and 
				(ag.IsGlobal != 1 or ag.IsGlobal is null) and
				psavc.SiteId = @siteId
		END
END

GO

print 'altering BLD_ExportProducts'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportProducts')
    drop procedure BLD_ExportProducts
go

CREATE PROCEDURE [dbo].[BLD_ExportProducts]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		BEGIN
			select 
				 p.ProductIDUser as ProductId
				,p.Title as ProductName
				,cast(p.ProductTypeID as varchar(max)) as ProductTypeId
				,pt.Title as ProductType
				,p.ShortDescription
				,p.LongDescription
				,p.IsActive
				,0 as UpdateFlag
				,coalesce(SEOH1, '') as 'SEO Title'
				,coalesce(SEODescription, '') as 'SEO Description'
				,coalesce(SEOKeywords, '') as 'SEO Keywords'
				,coalesce(SEOFriendlyUrl, '') as 'SEO Friendly URL'
				,p.IsShared
			from dbo.PRProduct p
			join dbo.PRProductType pt on (p.ProductTypeID = pt.Id)
			order by p.Title
		END
	ELSE
		BEGIN
			select
				 p.ProductIDUser as ProductId
				,ISNULL(prvc.Title, p.Title) as ProductName
				,cast(p.ProductTypeID as varchar(max)) as ProductTypeId
				,pt.Title as ProductType
				,ISNULL(prvc.ShortDescription ,p.ShortDescription) as ShortDescription
				,ISNULL(prvc.LongDescription, p.LongDescription) as LongDescription
				,ISNULL(prvc.IsActive, p.IsActive) as IsActive
				,0 as UpdateFlag
				,coalesce(prvc.SEOH1, p.SEOH1) as [SEO Title]
				,coalesce(prvc.SEODescription, p.SEODescription) as 'SEO Description'
				,coalesce(prvc.SEOKeywords, p.SEOKeywords) as 'SEO Keywords'
				,coalesce(prvc.SEOFriendlyUrl, p.SEOFriendlyUrl) as 'SEO Friendly URL'
				,p.IsShared
			from dbo.PRProductVariantCache prvc
			left join dbo.PRProduct p on (prvc.Id = p.Id)
			left join dbo.PRProductType pt on (p.ProductTypeID = pt.Id)
			where prvc.SiteId = @siteId
			order by p.Title
		END
END

GO

print 'altering BLD_ExportProductImages'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportProductImages')
    drop procedure BLD_ExportProductImages
go

CREATE PROCEDURE [dbo].[BLD_ExportProductImages]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	declare @images table
	(
		 ProductTitle varchar(max)
		,ProductId varchar(max)
		,SKU varchar(max)
		,Sequence varchar(max)
		,Title nvarchar(256)
		,Description nvarchar(4000)
		,AltText nvarchar(1024)
		,Path varchar(max)
		,IsShared bit
	)

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		Begin
			insert into @images
			select
				 p.Title 
				,p.ProductIDUser
				,''
				,oi.Sequence
				,i.Title
				,i.Description
				,i.AltText
				,i.FileName
				,isnull(i.IsShared,0) IsShared
			from dbo.PRProduct p
			join dbo.CLObjectImage oi on (p.Id = oi.ObjectId)
			join dbo.CLImage i on (oi.ImageId = i.Id)
			where
				oi.ObjectType = 205 -- product type
			and i.ParentImage is null 

			union all 

			select
				 p.Title 
				,p.ProductIDUser
				,s.SKU
				,oi.Sequence
				,i.Title
				,i.Description
				,i.AltText
				,i.FileName
				,isnull(i.IsShared,0) IsShared
			from dbo.PRProductSKU s
			join dbo.PRProduct p on (s.ProductId = p.Id)
			join dbo.CLObjectImage oi on (s.Id = oi.ObjectId)
			join dbo.CLImage i on (oi.ImageId = i.Id)
			where
				oi.ObjectType = 206 -- sku type
			and i.ParentImage is null 
		END
	ELSE
		Begin
			insert into @images
			select
				 p.Title
				,p.ProductIDUser
				,''
				,oi.Sequence
				,v.Title
				,v.Description
				,v.AltText
				,i.FileName
				,isnull(i.IsShared,0) IsShared
			from dbo.PRProduct p
			join dbo.CLObjectImage oi on (p.Id = oi.ObjectId)
			join CLImage i on oi.ImageId=i.id
			join dbo.CLImageVariantCache v on (oi.ImageId = v.Id)
			where
				oi.ObjectType = 205 -- product type
			and i.ParentImage is null 
			and v.SiteId = @siteId

			union all 

			select
				 p.Title
				,p.ProductIDUser
				,s.SKU
				,oi.Sequence
				,v.Title
				,v.Description
				,v.AltText
				,i.FileName
				,isnull(i.IsShared,0) IsShared
			from dbo.PRProductSKU s
			join dbo.PRProduct p on (s.ProductId = p.Id)
			join dbo.CLObjectImage oi on (s.Id = oi.ObjectId)
			join CLImage i on oi.ImageId=i.id
			join dbo.CLImageVariantCache v on (oi.ImageId = v.Id)
			where
				oi.ObjectType = 206 -- sku type
			and i.ParentImage is null 
			and v.SiteId = @siteId
		END
		select 
			ProductTitle as 'Product Title'
			,ProductId
			,SKU
			,Sequence
			,Title
			,Description
			,AltText
			,Path
			,0 as UpdateFlag
			,IsShared
		from @images
END



GO

print 'altering BLD_ExportAttributes'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportAttributes')
    drop procedure BLD_ExportAttributes
go

CREATE PROCEDURE [dbo].[BLD_ExportAttributes]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

		IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
			BEGIN
				select 
					 a.id as AttributeId
					,adt.Title as AttributeDataType
					,ac.Name as ATtributeCategoryName
					,a.Title
					,coalesce(a.Description, '') as Description
					,coalesce(a.IsFaceted, 0) as IsFaceted
					,coalesce(a.IsSearched, 0) as IsSearched
					,coalesce(a.IsDisplayed, 0) as IsDisplayed
					,coalesce(a.IsEnum, 0) as IsEnum
					,coalesce(a.IsPersonalized, 0) as IsPersonalized
					,coalesce(a.IsMultiValued, 0) as IsMultiValued
					,0 as UpdateFlag
				from dbo.ATAttributeCategoryItem aci
					join dbo.ATAttributeCategory ac on (ac.id = aci.CategoryId)
					join dbo.ATAttribute a on (aci.AttributeId = a.Id)
					join dbo.ATAttributeDataType adt on (a.AttributeDataTypeId = adt.Id)
				where 
					--ac.IsSystem = 0 and 
					(ac.IsGlobal != 1 or ac.IsGlobal is null)
			END
		ELSE
			BEGIN
				select 
					 a.id as AttributeId
					,adt.Title as AttributeDataType
					,ac.Name as ATtributeCategoryName
					,avc.Title
					,coalesce(avc.Description, a.Description) as Description
					,coalesce(a.IsFaceted, 0) as IsFaceted
					,coalesce(a.IsSearched, 0) as IsSearched
					,coalesce(a.IsDisplayed, 0) as IsDisplayed
					,coalesce(a.IsEnum, 0) as IsEnum
					,coalesce(a.IsPersonalized, 0) as IsPersonalized
					,coalesce(a.IsMultiValued, 0) as IsMultiValued
					,0 as UpdateFlag
				from dbo.ATAttributeVariantCache avc
				left join dbo.ATAttribute a on (avc.Id = a.Id)
				join dbo.ATAttributeCategoryItem aci on (a.id = aci.AttributeId)
				join dbo.ATAttributeCategory ac on (ac.id = aci.CategoryId)
				join dbo.ATAttributeDataType adt on (a.AttributeDataTypeId = adt.Id)
				where 
					--ac.IsSystem = 0 and 
					(ac.IsGlobal != 1 or ac.IsGlobal is null)
					and avc.siteid = @siteId
			END
END
GO

print 'altering BLD_ExportAttributeEnums'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportAttributeEnums')
    drop procedure BLD_ExportAttributeEnums
go

CREATE PROCEDURE [dbo].[BLD_ExportAttributeEnums]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		Begin
			select 
				e.Id as 'Key'
				,a.Title as AttributeTitle
				,coalesce(cast(e.Value as varchar(max)), cast(e.NumericValue as varchar(max)), '')
				,ac.Name as AttributeCategoryName
				,0 as UpdateFlag
			from dbo.ATAttributeCategoryItem aci
			join dbo.ATAttributeCategory ac on (ac.id = aci.CategoryId)
			join dbo.ATAttribute a on (aci.AttributeId = a.Id)
			join dbo.ATAttributeEnum e on (a.Id = e.AttributeId)
			where 
				--ac.IsSystem = 0 and 
				(ac.IsGlobal != 1 or ac.IsGlobal is null)
		END
	ELSE
		Begin
			select 
			 e.Id  as 'Key'
            ,a.Title as AttributeTitle
            ,coalesce(cast(e.Value as varchar(max)), cast(e.NumericValue as varchar(max)), '')
            ,ac.Name as AttributeCategoryName
            ,0 as UpdateFlag
            from dbo.ATAttributeEnumVariantCache aevc
            join dbo.ATAttributeEnum e on aevc.id=e.id
            join dbo.ATAttribute a on (e.AttributeID = a.Id)
            join dbo.ATAttributeCategoryItem aci on (a.id = aci.AttributeId)
            join dbo.ATAttributeCategory ac on (aci.CategoryId = ac.Id)
			where 
				--ac.IsSystem = 0 and
				(ac.IsGlobal != 1 or ac.IsGlobal is null) and 
				aevc.SiteId = @siteId
		END
END

GO

print 'altering BLD_ExportPriceSets'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportPriceSets')
    drop procedure BLD_ExportPriceSets
go

CREATE PROCEDURE [dbo].[BLD_ExportPriceSets]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		Begin
			select 
				 ps.Title as PriceSetName
				,'' as CustomerGroupName
				,ps.StartDate
				,ps.EndDate
				,ps.IsSale
				,0 as UpdateFlag
			from dbo.PSPriceSet ps
			where ps.TypeId = 2 -- manual price set
		END
	ELSE
		Begin
			select 
				 ps.Title as PriceSetName
				,'' as CustomerGroupName
				,ps.StartDate
				,ps.EndDate
				,ps.IsSale
				,0 as UpdateFlag
			from dbo.PSPriceSet ps
			where 
			ps.TypeId = 2  and -- manual price set
			ps.SiteId = @siteId
		END
END
GO

print 'altering BLD_ExportPriceSetSKUs'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportPriceSetSKUs')
    drop procedure BLD_ExportPriceSetSKUs
go

CREATE PROCEDURE [dbo].[BLD_ExportPriceSetSKUs]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		Begin
			select 
				 ps.Title as PriceSetName
				,s.SKU
				,pss.MinQuantity as MinQty
				,pss.MaxQuantity as MaxQty
				,pss.Price as PriceSetPRice
				,0 as UpdateFlag
			from dbo.PSPriceSet ps
			join dbo.PSPriceSetManualSKU pss on (ps.Id = pss.PriceSetId)
			join dbo.PRProductSKU s on (pss.SKUId = s.Id)
			where ps.TypeId = 2 -- manual price set
		END
	ELSE
		Begin
			select 
				ps.Title as PriceSetName
				,s.SKU
				,pss.MinQuantity as MinQty
				,pss.MaxQuantity as MaxQty
				,pss.Price as PriceSetPRice
				,0 as UpdateFlag
			from dbo.PSPriceSet ps
			join dbo.PSPriceSetManualSKU pss on (ps.Id = pss.PriceSetId)
			join dbo.PRProductSKU s on (pss.SKUId = s.Id)
			where ps.TypeId = 2 and-- manual price set
			ps.SiteId = @siteId
		END
END
GO

print 'altering BLD_ExportRelatedProducts'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportRelatedProducts')
    drop procedure BLD_ExportRelatedProducts
go

CREATE PROCEDURE [dbo].[BLD_ExportRelatedProducts]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		begin
			select
				 p1.ProductIDUser as ProductId
				,rt.Id as RelationType
				,p2.ProductIDUser as RelatedProductId
				,0 as UpdateFlag
			from dbo.PRProductToProductRel ppr
			join dbo.PRProductRelationType rt on (ppr.ProductRelationTypeId = rt.Id)
			join dbo.PRProduct p1 on (ppr.ProductId = p1.Id)
			join dbo.PRProduct p2 on (ppr.RelatedProduct = p2.Id)
		END
	ELSE
		begin
			select
				 p1.ProductIDUser as ProductId
				,rt.Id as RelationType
				,p2.ProductIDUser as RelatedProductId
				,0 as UpdateFlag
			from dbo.PRProductToProductRel ppr
			left join dbo.PRProductRelationType rt on (ppr.ProductRelationTypeId = rt.Id)
			left join dbo.PRProduct p1 on (ppr.ProductId = p1.Id)
			left join dbo.PRProduct p2 on (ppr.RelatedProduct = p2.Id)
			where
			p1.SiteId = @siteId and p2.SiteId = p1.SiteId
		END
END
GO

print 'altering BLD_ExportSKUs'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ExportSKUs')
    drop procedure BLD_ExportSKUs
go

CREATE PROCEDURE [dbo].[BLD_ExportSKUs]
	@siteId uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (select 1 from SISite where @SiteId=id and MasterSiteId=Id)
		BEGIN
			select
				 p.ProductIDUser as ProductId
				,s.Title as SkuTitle
				,s.SKU 
				,s.IsOnline 
				,s.IsActive
				,s.ListPrice
				,s.UnitCost
				,coalesce(s.Measure, 'each') as UnitOfMeasure
				,coalesce(s.OrderMinimum, 0) as OrderQuantityMin
				,coalesce(s.OrderIncrement, 0) as OrderQuantityIncrement
				,s.HandlingCharges as HandlingCharge
				,coalesce(s.Length, 0) as Length
				,coalesce(s.Width, 0) as Length
				,coalesce(s.Height, 0) as Length
				,coalesce(s.Weight, 0) as Length
				,coalesce(s.SelfShippable, 0) as SelfShippable
				,s.AvailableForBackOrder
				,s.BackOrderLimit
				,s.IsUnlimitedQuantity
				,0 as UpdateFlag
			from dbo.PRProductSKU s
			join dbo.PRProduct p on (s.ProductId = p.Id)
		END
	ELSE
		BEGIN
			select 
				 p.ProductIDUser as ProductId
				,ISNULL(prcv.Title,p.Title) as SkuTitle
				,prs.SKU as SKU
				,prcv.IsOnline as IsOnline
				,ISNULL(prcv.IsActive,p.IsActive) as IsActive
				,prcv.ListPrice as ListPrice
				,prcv.UnitCost as UnitCost
				,coalesce(prcv.Measure, 'each') as UnitOfMeasure
				,coalesce(prcv.OrderMinimum, 0) as OrderQuantityMin
				,coalesce(prcv.OrderIncrement, 0) as OrderQuantityIncrement
				,prcv.HandlingCharges as HandlingCharges
				,coalesce(prs.Length, 0) as Length
				,coalesce(prs.Width, 0) as Length
				,coalesce(prs.Height, 0) as Length
				,coalesce(prs.Weight, 0) as Length
				,coalesce(prcv.SelfShippable, 0) as SelfShippable
				,prcv.AvailableForBackOrder as AvailableForBackOrder
				,prcv.BackOrderLimit as BackOrderLimit
				,prcv.IsUnlimitedQuantity as IsUnlimitedQuantity
				,0 as UpdateFlag
			from dbo.PRProductSKUVariantCache prcv
			left join PRProductSKU prs on (prcv.Id = prs.Id)
			left join dbo.PRProduct p on (prs.ProductId = p.Id)
			where prcv.SiteId = @siteId
		END
END
GO

print 'altering BLD_GetImageId'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_GetImageId')
    drop procedure BLD_GetImageId
go

CREATE PROCEDURE [dbo].[BLD_GetImageId]
(
	 @ProductId uniqueidentifier,
	 @FileName nvarchar(256),
	 @SiteId uniqueidentifier = null
)
AS
BEGIN

	if @SiteId is null
	begin
		select @SiteId=Id from SISite where Id=MasterSiteId
	end

	SELECT Img.Id 
	FROM [CLImage] Img
		JOIN [CLObjectImage] Obj on Obj.ImageId = Img.id
	WHERE Obj.ObjectId = @ProductId
		and Obj.SiteId=@SiteId
		AND lower(Img.FileName) = lower(@FileName)
		AND ParentImage IS NULL
END
go

print 'altering BLD_DeleteImage'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_DeleteImage')
    drop procedure BLD_DeleteImage
go

CREATE PROCEDURE [dbo].[BLD_DeleteImage]
(
	@ImageId uniqueidentifier,
	@SiteId uniqueidentifier = null
)
AS
BEGIN

	if @SiteId is null
	begin
		select @SiteId=Id from SISite where Id=MasterSiteId
		DELETE
		FROM [CLObjectImage]
		WHERE 
			([ImageId] = @ImageId OR [ImageId] IN (SELECT Img.Id FROM [CLImage] Img WHERE Img.ParentImage = @ImageId))
		AND SiteId=@SiteId

		DELETE FROM [CLImage]
		WHERE Id = @ImageId 
		OR ParentImage = @ImageId 
	end
	else
	begin
		DELETE FROM CLImageVariant WHERE SiteId=@SiteId and (Id = @ImageId)

	end
END

go

print 'altering BLD_ImageInsert'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ImageInsert')
    drop procedure BLD_ImageInsert
go

CREATE PROCEDURE [dbo].[BLD_ImageInsert]  
(  
 @SiteId uniqueidentifier = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',  
 @Description [nvarchar](4000),  
 @Title [nvarchar](256),  
 @FileName [nvarchar](256),  
 @AltText [nvarchar](1024),  
 @ObjectId [uniqueidentifier],  
 @ProductId [uniqueidentifier], 
 @ObjectType int,  
 @Sequence int,
 @IsShared bit = 0
)  
AS  
BEGIN  
  
 declare @Now datetime;  
 set @Now = GetUTCDate();  
  
  
 ------------------------------------------  
 -- Main  
 ------------------------------------------  
  
 declare @ParentImageId uniqueidentifier;  
 set @ParentImageId = newid();  
  
 Insert into  CLImage(  
  [Id],   
  [Description],   
  [Title],   
  [FileName],   
  [Size],   
  [Attributes],   
  [RelativePath],   
  [AltText],   
  [Height],   
  [Width],   
  [ParentImage],   
  [ImageType],   
  [Status],   
  [CreatedDate],   
  [CreatedBy],  
  [SiteId],
  [IsShared])  
 Values (  
  @ParentImageId,   
  @Description,   
  @Title,   
  @FileName,   
  0,   
  null,   
  null,   
  @AltText,   
  0,   
  0,   
  NULL,   
  '396B425C-E197-4F2D-A4A3-25448CA366F8',   
  1,   
  @Now,   
  'EA6BB064-3138-4211-9B35-F240AA38C1B2',   
  '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',
  @IsShared)  
  
  IF (@ObjectType = 205)  
 BEGIN  
 INSERT INTO CLObjectImage(Id,ObjectId,ObjectType,ImageId,IsPrimary,Sequence, SiteId)  
 VALUES (  
  newid(),  
  @ProductId,  
  205,  
  @ParentImageId,  
  0,  
  @Sequence,
  @SiteId  
 )  
 END  
  
  
 IF (@ObjectType = 206)  
 BEGIN  
  
 INSERT INTO CLObjectImage(Id,ObjectId,ObjectType,ImageId,IsPrimary,Sequence, SiteId)  
 VALUES (  
  newid(),  
  @ProductId,  
  205,  
  @ParentImageId,  
  0,  
  @Sequence,
  @SiteId  
 )  
  
 INSERT INTO CLObjectImage(Id,ObjectId,ObjectType,ImageId,IsPrimary,Sequence, SiteId)  
 VALUES (  
  newid(),  
  @ObjectId,  
  206,  
  @ParentImageId,  
  0,  
  @Sequence,
  @SiteId  
 )  
  
 END    
  
 ------------------------------------------  
 -- Sys  
 ------------------------------------------  
  
 declare @FileNameSys nvarchar(256)  
 set @FileNameSys = 'systhumb_' + @FileName;  
  
 declare @ImageIdSys uniqueidentifier;  
 set @ImageIdSys = newid();  
  
 Insert into CLImage([Id],  
      [Description],  
      [Title],  
      [FileName],  
      [Size],  
      [Attributes],  
      [RelativePath],  
      [AltText],  
      [Height],  
      [Width],  
      [ParentImage],  
      [ImageType],  
      [Status],  
      [CreatedDate],  
      [CreatedBy],  
      [SiteId],
	  [IsShared])  
 Values    (  
      @ImageIdSys,  
      @Description,  
      @Title,  
      @FileNameSys,  
      0,  
      null,  
      null,  
      @AltText,  
      0,  
      0,  
      @ParentImageId,  
      '63CF2418-684A-4D1F-88B2-34968C6F139F',  
      1,  
      @Now,  
      'EA6BB064-3138-4211-9B35-F240AA38C1B2',  
      '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',
	  @IsShared)  
    
  
 ------------------------------------------  
 -- Prev  
 ------------------------------------------  
  
 declare @FileNamePrev nvarchar(256)  
 set @FileNamePrev = 'prev_' + @FileName;  
  
 declare @ImageIdPrev uniqueidentifier;  
 set @ImageIdPrev = newid();  
  
 Insert into CLImage([Id],  
      [Description],  
      [Title],  
      [FileName],  
      [Size],  
      [Attributes],  
      [RelativePath],  
      [AltText],  
      [Height],  
      [Width],  
      [ParentImage],  
      [ImageType],  
      [Status],  
      [CreatedDate],  
      [CreatedBy],  
      [SiteId],
	  [IsShared])  
 Values    (  
      @ImageIdPrev,  
      @Description,  
      @Title,  
      @FileNamePrev,  
      0,  
      null,  
      null,  
      @AltText,  
      0,  
      0,  
      @ParentImageId,  
      '2770534E-4675-4764-A620-56776398E03E',  
      1,  
      @Now,  
      'EA6BB064-3138-4211-9B35-F240AA38C1B2',  
      '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',
	  @IsShared)   
  
  
 ------------------------------------------  
 -- Mini  
 ------------------------------------------  
  
 declare @FileNameMini nvarchar(256)  
 set @FileNameMini = 'mini_' + @FileName;  
  
 declare @ImageIdMini uniqueidentifier;  
 set @ImageIdMini = newid();  
  
 Insert into CLImage([Id],  
      [Description],  
      [Title],  
      [FileName],  
      [Size],  
      [Attributes],  
      [RelativePath],  
      [AltText],  
      [Height],  
      [Width],  
      [ParentImage],  
      [ImageType],  
      [Status],  
      [CreatedDate],  
      [CreatedBy],  
      [SiteId],
	  [IsShared])  
 Values    (  
      @ImageIdMini,  
      @Description,  
      @Title,  
      @FileNameMini,  
      0,  
      null,  
      null,  
      @AltText,  
      0,  
      0,  
      @ParentImageId,  
      'E02ED0F3-1271-4218-B81E-816E458434A1',  
      1,  
      @Now,  
      'EA6BB064-3138-4211-9B35-F240AA38C1B2',  
      '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',
	  @IsShared)     
   
  
 ------------------------------------------  
 -- Thumb  
 ------------------------------------------  
  
 declare @FileNameThumb nvarchar(256)  
 set @FileNameThumb = 'thumb_' + @FileName;  
  
 declare @ImageIdThumb uniqueidentifier;  
 set @ImageIdThumb = newid();  
  
 Insert into CLImage([Id],  
      [Description],  
      [Title],  
      [FileName],  
      [Size],  
      [Attributes],  
      [RelativePath],  
      [AltText],  
      [Height],  
      [Width],  
      [ParentImage],  
      [ImageType],  
      [Status],  
      [CreatedDate],  
      [CreatedBy],  
      [SiteId],
	  [IsShared])  
 Values    (  
      @ImageIdThumb,  
      @Description,  
      @Title,  
      @FileNameThumb,  
      0,  
      null,  
      null,  
      @AltText,  
      0,  
      0,  
      @ParentImageId,  
      'BA9B3922-4F9F-4B21-A5CF-E03B26B588AD',  
      1,  
      @Now,  
      'EA6BB064-3138-4211-9B35-F240AA38C1B2',  
      '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',
	  @IsShared)  
  
  
 select @ParentImageId;  
  
END  
go

print 'altering Product_GetSKU'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='Product_GetSKU')
    drop procedure Product_GetSKU
go

CREATE PROCEDURE [dbo].[Product_GetSKU]  
(  
 @ProductId uniqueidentifier  = null
 ,@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
IF @ProductId is not null
select  
  Id,  
  ProductId,  
  SiteId,  
  Title,  
  SKU,  
  Description,  
  Sequence,  
  IsActive,  
  IsOnline,  
  ListPrice,  
  UnitCost,  
  Measure,
  OrderMinimum,
  OrderIncrement,	HandlingCharges, 
  PreviousSoldCount,  
  dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,  
  CreatedBy,  
  dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,  
  ModifiedBy,
  Status
    ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable] 
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
	,FreeShipping
	,UserDefinedPrice
  from PRProductSKU 
  where ProductId=@ProductId and Status=dbo.GetActiveStatus()  
  Order by Sequence, Title  
ELSE
 select  
  Id,  
  ProductId,  
  SiteId,  
  Title,  
  SKU,  
  Description,  
  Sequence,  
  IsActive,  
  IsOnline,  
  ListPrice,  
  UnitCost,  
  Measure,
  OrderMinimum,
  OrderIncrement,	HandlingCharges, 
  PreviousSoldCount,  
  dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,  
  CreatedBy,  
  dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,  
  ModifiedBy,
  Status  
,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable] 
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
	,FreeShipping
	,UserDefinedPrice
  from PRProductSKU where 
		--ProductId=Isnull(@ProductId,ProductId) 
		(@ProductId is null or ProductId = @ProductId)
		and Status=dbo.GetActiveStatus()  
  Order by Sequence, Title  
END
go

print 'altering Sku_GetSkuVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='Sku_GetSkuVariant')
    drop procedure Sku_GetSkuVariant
go

CREATE PROCEDURE [dbo].[Sku_GetSkuVariant]
( @SkuIds		xml,@ApplicationId uniqueidentifier=null)
AS

Declare @TabSkuIds AS Table (Id Uniqueidentifier)

insert into @TabSkuIds
select tab.col.value('text()[1]','uniqueidentifier') 
from @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	


SELECT PS.[Id]
      ,PS.[ProductId]
      ,S.[SiteId]
      ,PS.[SKU]
	  ,isnull(S.[Title], PS.Title) as Title
      ,isnull(S.[Description], PS.Description) as Description
      ,isnull(S.[Sequence], PS.Sequence) as Sequence
      ,isnull(S.[IsActive], PS.IsActive) as IsActive
      ,isnull(S.[IsOnline], PS.IsOnline) as IsOnline
      ,isnull(S.[ListPrice], PS.ListPrice) as ListPrice
      ,isnull(S.[UnitCost], PS.UnitCost) as UnitCost
      ,isnull(S.[PreviousSoldCount], PS.PreviousSoldCount) as PreviousSoldCount
	  ,isnull(S.[Measure], PS.Measure) as Measure
	  ,isnull(S.[OrderMinimum], PS.OrderMinimum) as OrderMinimum
	  ,isnull(S.[OrderIncrement], PS.OrderIncrement) as OrderIncrement
	  ,PS.[CreatedBy]
	  ,PS.CreatedDate AS CreatedDate
	  ,isnull(S.[ModifiedBy], PS.ModifiedBy) as ModifiedBy
	  ,isnull(S.ModifiedDate, PS.ModifiedDate) as ModifiedDate
	  ,PS.[Status]
	  ,isnull(S.[HandlingCharges], PS.HandlingCharges) as HandlingCharges
	  ,PS.[Length]
	  ,PS.[Height]
	  ,PS.[Width]
	  ,PS.[Weight]
	  ,isnull(S.[SelfShippable], PS.SelfShippable) as SelfShippable
	  ,isnull(S.[AvailableForBackOrder], PS.AvailableForBackOrder) as AvailableForBackOrder
	  ,isnull(S.MaximumDiscountPercentage, PS.MaximumDiscountPercentage) as MaximumDiscountPercentage
	  ,isnull(S.[BackOrderLimit], PS.BackOrderLimit) as BackOrderLimit
	  ,isnull(S.[IsUnlimitedQuantity], PS.IsUnlimitedQuantity) as IsUnlimitedQuantity
   FROM @TabSkuIds TS	
	JOIN PRProductSKU PS ON TS.Id =PS.Id
	left Join PRProductSKUVariant S on PS.Id =S.Id
	WHERE S.SiteId = @ApplicationId

GO

print 'altering BLD_ProductImport_Attribute'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_Attribute')
    drop procedure BLD_ProductImport_Attribute
go

CREATE PROCEDURE [dbo].[BLD_ProductImport_Attribute]

@ProductIDUser nvarchar(max),
@AttrGroupId uniqueidentifier,
@AttrId uniqueidentifier,
@AttrEnum bit = 0,
@AttrValue nvarchar(max),
@SKU nvarchar(max) = null,
@OperatorId uniqueidentifier = null,
@IsDefault bit = 0

AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
        BEGIN TRAN

		declare @NumericValue int;

		if lower(@AttrValue) like 'true' set @NumericValue = 1;
		if lower(@AttrValue) like 'false' set @NumericValue = 0;

		declare @ProductId uniqueidentifier;
		declare @ProductTypeId uniqueidentifier;
		declare @ProductAttrId uniqueidentifier;
		declare @ProductTypeAttributeId uniqueidentifier;
		declare @ProductAttrValueId uniqueidentifier;
		declare @ProductSKUAttrValueId uniqueidentifier;
		declare @AttrEnumId uniqueidentifier;
		declare @ProductSKUId uniqueidentifier;
		declare @IsSKULevel bit = iif(@SKU is null, 0, 1);
		declare @IsMultiValued bit =0

		select 
			@ProductId = p.Id, 
			@ProductAttrId = pa.Id,
			@ProductTypeId = p.ProductTypeId, 
			@ProductTypeAttributeId = pta.Id,
			@IsMultiValued = a.IsMultiValued
		from PRProduct p
			left join PRProductAttribute pa on p.id=pa.ProductId and pa.AttributeId=@AttrId and IsSKULevel = @IsSKULevel
			left join ATATtribute a on a.Id = pa.AttributeId
			left join ATAttributeCategoryItem ai on pa.AttributeId=ai.AttributeId
			left join ATAttributeCategory c on ai.CategoryId=c.Id and c.GroupId=@AttrGroupId
			left join PRProductTypeAttribute pta on p.ProductTypeID=pta.ProductTypeId and pa.AttributeId=pta.AttributeId
		where p.ProductIDUser=@ProductIDUser

		declare @Output varchar(max)
		set @Output = concat('product not found: ', @ProductIDUser)
		if(@ProductId is null)RAISERROR (@Output, 16, 1);

		-- ** 0 **
		if @ProductTypeAttributeId is null
		begin
			set @ProductTypeAttributeId = newid()
			insert into PRProductTypeAttribute (id, ProductTypeId, AttributeId, Sequence, IsSKULevel, IsRequired)
				values (@ProductTypeAttributeId, @ProductTypeId, @AttrId, 1, @IsSKULevel, 0)
		end

		-- ** 1 **
		select @AttrEnumId = Id from ATAttributeEnum where AttributeID = @AttrId and Title like @AttrValue;

		if @ProductAttrId is null
		begin
			set @ProductAttrId = newid();
			declare @Seq int;
			select @Seq = isnull(max(Sequence)+1,0) from PRProductAttribute where ProductId = @ProductId;
			if @SKU is not null
			begin
				select @Seq = isnull(max(Sequence)+1,0) from PRProductAttribute where ProductId = @ProductId and IsSKULevel = 1;
			end
			insert into PRProductAttribute
				select @ProductAttrId, @ProductId, @AttrId, @Seq, (case when @SKU is null then 0 else 1 end), 0;

		end

		-- ** 2 **  TODO: FIX THIS!!!

		-- IF (enumeration not found AND attribute is flagged for enumeration)
		if (@AttrEnumId is null AND @AttrEnum = 1)
		begin
			-- auto insert new enumeration
			set @AttrEnumId = newid();
			insert into ATAttributeEnum select
			@AttrEnumId, @AttrId, @AttrValue, @AttrValue, null, @AttrValue, @IsDefault, (select isnull(max(Sequence),0)+1 from ATAttributeEnum where AttributeID = @AttrId), GETDATE(), null, null, null, 1;

			insert into PRProductTypeAttributeValue 
				select 
					newid(), 
					@ProductTypeAttributeId, 
					@AttrId, 
					@AttrEnumId, 
					(case when @SKU is null then 0 else 1 end), 
					(select isnull(max(Sequence),0)+1 from PRProductTypeAttributeValue where ProductTypeAttributeId = @ProductTypeAttributeId),
					@ProductTypeId

		end else
		begin

			if (@AttrEnum = 0)
			begin
			set @AttrEnumId = null
			end


		end


		if @SKU is null
		begin

			if @AttrEnumId is not null AND @IsMultiValued =1
			begin

				print '@AttrEnumId EXISTS!'


				select @ProductAttrValueId = Id from PRProductAttributeValue 
				where ProductId = @ProductId 
				and AttributeId = @AttrId 
				and ProductAttributeId = @ProductAttrId
				and AttributeEnumId = @AttrEnumId;

			end
			else begin

				print '@AttrEnumId NULL!'

				select @ProductAttrValueId = Id from PRProductAttributeValue 
				where ProductId = @ProductId 
				and AttributeId = @AttrId 
				and ProductAttributeId = @ProductAttrId;

			end



			if @ProductAttrValueId is null
			begin

				set @ProductAttrValueId = newid();
				insert into PRProductAttributeValue
					select @ProductAttrValueId, @ProductAttrId, @ProductId, @AttrId, @AttrEnumId, @AttrValue, GETDATE(), @OperatorId, null, null, 1;

			end else
			begin
				
				print @AttrValue

				update PRProductAttributeValue set
				Value = @AttrValue,
				AttributeEnumId = @AttrEnumId,
				ModifiedDate = GetDate(),
				ModifiedBy = @OperatorId
				where Id = @ProductAttrValueId;
			end
		end else
		begin

			print 'Sku Exists'

			select @ProductSKUId = Id FROM PRProductSKU where ProductId = @ProductId and SKU = @SKU;
			if @ProductSKUId is not null
			begin
				
				--select @ProductSKUAttrValueId = Id from PRProductSKUAttributeValue where SKUId = @ProductSKUId and AttributeId = @AttrId and ProductAttributeId = @ProductAttrId;

				if @AttrEnumId is not null AND @IsMultiValued=1
				begin

					print '@AttrEnumId EXISTS!'

					select @ProductSKUAttrValueId = Id from PRProductSKUAttributeValue where SKUId = @ProductSKUId 
					and AttributeId = @AttrId 
					and ProductAttributeId = @ProductAttrId
					and AttributeEnumId = @AttrEnumId;

				end
				else begin

					print '@AttrEnumId NULL!'

					select @ProductSKUAttrValueId = Id from PRProductSKUAttributeValue where SKUId = @ProductSKUId 
					and AttributeId = @AttrId 
					and ProductAttributeId = @ProductAttrId

				end


				if @ProductSKUAttrValueId is null
				begin
					set @ProductSKUAttrValueId = newid()
					insert into PRProductSKUAttributeValue
						select @ProductSKUAttrValueId, @ProductAttrId, @ProductSKUId, @AttrId, @AttrEnumId, @AttrValue, getdate(), @OperatorId, null, null, 1;
				end else
				begin
					update PRProductSKUAttributeValue set
					Value = @AttrValue,
					AttributeEnumId = @AttrEnumId,
					ModifiedDate = GETDATE(),
					ModifiedBy = @OperatorId
					where Id = @ProductSKUAttrValueId;
				end
			end
		end

		if @SKU is null select @ProductAttrValueId
		else select @ProductSKUAttrValueId
		COMMIT TRAN
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX)
        SET @ErrorMessage = ERROR_MESSAGE()
        RAISERROR(@ErrorMessage,16, 1)
    END CATCH

END


go

print 'Creating BLD_ProductImport_AttributeEnumVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_AttributeEnumVariant')
    drop procedure BLD_ProductImport_AttributeEnumVariant
go
CREATE PROCEDURE [dbo].[BLD_ProductImport_AttributeEnumVariant]
(
	@SiteId					uniqueidentifier,
	@AttributeEnumId		uniqueidentifier,
	@Value					nvarchar(50),
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	begin try
		
		declare @AttributeEnumVariantId		uniqueidentifier,
				@MasterAttributeEnumId		uniqueidentifier,
				@MasterAttributeDataType	nvarchar(50),
				@MasterAttributeId			uniqueidentifier,
				@MasterTitle				nvarchar(max),
				@MasterCode					nvarchar(200),
				@Code						nvarchar(200),
				@Output						nvarchar(512),
				@IsSkipped					bit = 1

		if @SiteId is null or @AttributeEnumId is null
			raiserror(15600, -1, -1, 'SiteId, ModifiedBy, or AttributeEnumId is null')

		begin tran
		select
			@MasterAttributeEnumId=ae.Id,
			@MasterAttributeId=a.Id,
			@MasterAttributeDataType = a.AttributeDataType,
			@MasterTitle=iif(v.Title is null, ae.Value, null)
		from ATAttributeEnum ae
			left join ATAttributeEnumVariant v on ae.Id=v.Id and v.SiteId=@SiteId
			join ATAttribute a on ae.AttributeID=a.Id
			join ATAttributeCategoryItem i on a.Id = i.AttributeId
			join ATAttributeCategory c on i.CategoryId=c.Id
		where
			ae.Id=@AttributeEnumId

		if @MasterAttributeEnumId is null
		begin
			set @Output = concat('Unable to find attribute enum with id=', @AttributeEnumId)
			raiserror(15600, -1, -1, @Output)
		end

		select 
			@Code = 
				case 
					when charindex('Boolean', @MasterAttributeDataType) > 0 then				
						case when lower(@Value)='true' then '1' else '0' end
					else @MasterCode
				end

		select @AttributeEnumVariantId=Id from ATAttributeEnumVariant where Id=@MasterAttributeEnumId
		select
			@Value = case when @MasterTitle is null or @Value <> @MasterTitle and @Value is not null then @Value else null end,
			@Code = case when @MasterCode is null or @Code <> @MasterCode and @Code is not null then @Code else null end

		if @AttributeEnumVariantId is not null
		begin
			update ATAttributeEnumVariant set 
				Title=@Value
				where Id=@AttributeEnumVariantId and SiteId=@SiteId
			set @Output=concat('updated ATAttributeEnumVariant id=', @AttributeEnumVariantId, ' Value=', @Value, ' code=', @Code)
		end
		else
		begin
			if @Value is not null
			begin
				insert into ATAttributeEnumVariant (Id, SiteId, AttributeId, Title, ModifiedDate, ModifiedBy)
					values (@MasterAttributeEnumId, @SiteId, @MasterAttributeId, @Value, getdate(), @ModifiedBy)
				set @IsSkipped=0
			end

			set @Output = concat('ATAttributeEnumVariant ', iif(@IsSkipped=1, 'skipped', 'inserted'), ' id=', @MasterAttributeEnumId, ' Value=', @Value, ' code=', @Code)
		end

		commit tran
		select @MasterAttributeEnumId, @Output
	end try
	begin catch
		rollback tran
		DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE(),
				@ErrorState int				= ERROR_STATE(),
				@ErrorSeverity int			= ERROR_SEVERITY()
        RAISERROR(@ErrorMessage,@ErrorSeverity, @ErrorState)

	end catch
END
go

print 'Creating BLD_ProductImport_AttributeVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_AttributeVariant')
    drop procedure BLD_ProductImport_AttributeVariant
go

CREATE PROCEDURE [dbo].[BLD_ProductImport_AttributeVariant]
(
	@SiteId					uniqueidentifier,
	@AttributeId			uniqueidentifier,
	@Title					nvarchar(4000),
	@Description			nvarchar(50),
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	BEGIN TRY
		declare @AttributeVariantId			uniqueidentifier
		declare @MasterTitle				nvarchar(4000)
		declare @MasterDescription			nvarchar(2000)
		declare @Output						nvarchar(512)
		declare @IsSkipped					bit = 1

		if @SiteId is null or @AttributeId is null or @Title is null
			raiserror(15600, -1, -1, 'SiteId, AttributeId, or Title is null')

		BEGIN TRAN
		select
			@MasterTitle=iif(v.Title is null, a.Title, null),
			@MasterDescription=iif(v.Description is null, a.Description, null)
		from ATAttributeCategoryItem i
			join ATAttributeCategory c on i.CategoryId=c.Id
			join ATAttribute a	on i.AttributeId=a.Id
			left join ATAttributeVariant v on a.Id=v.Id and v.SiteId=@SiteId
		where a.Id=@AttributeId

		if @MasterTitle is null
		begin
			set @Output = concat('Unable to find master attribute with id=', @AttributeId)
			raiserror(15600, -1, -1, @Output)
		end
		
		select @AttributeVariantId=Id from ATAttributeVariant where Id=@AttributeId
		select
			@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
			@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end

		if @AttributeVariantId is not null
		begin
			update ATAttributeVariant set Description=@Description where Id=@AttributeVariantId and SiteId=@SiteId
			set @Output=concat('updated ATAttributeVariant id=', @AttributeVariantId)
		end
		else
		begin
			if @Title is not null or @Description is not null
			begin
				insert into ATAttributeVariant (Id, SiteId, Title, Description, ModifiedDate, ModifiedBy)
					values (@AttributeId, @SiteId, @Title, @Description, getdate(), @ModifiedBy)
				set @IsSkipped=0
			end

			set @Output = concat(
				'ATAttributeVariant ', 
				iif(@IsSkipped=1, 'skipped', concat('inserted Title=', @Title, ' | Description=', @Description)))
		end
		commit tran
		select @AttributeId, @Output
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE(),
				@ErrorState int				= ERROR_STATE(),
				@ErrorSeverity int			= ERROR_SEVERITY()
        RAISERROR(@ErrorMessage,@ErrorSeverity, @ErrorState)
    END CATCH

END
go

print 'Creating BLD_ProductImport_ImageVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_ImageVariant')
    drop procedure BLD_ProductImport_ImageVariant
go

CREATE PROCEDURE [dbo].[BLD_ProductImport_ImageVariant]
(
	@SiteId					uniqueidentifier,
	@ProductIdUser			nvarchar(100),
	@Title					nvarchar(256),
	@Description			nvarchar(4000),
	@AltText				nvarchar(1024),
	@ObjectId				uniqueidentifier,
	@ObjectType				int,
	@Sequence				int,
	@IsShared				bit,
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	begin try
		declare @MasterSiteid				uniqueidentifier,
				@MasterMainImageId			uniqueidentifier,
				@VariantMainImageId			uniqueidentifier,
				@MainImageTypeId			uniqueidentifier	= '396B425C-E197-4F2D-A4A3-25448CA366F8',
				@MasterPreviewImageId		uniqueidentifier,
				@VariantPreviewImageId		uniqueidentifier,
				@PreviewImageTypeId			uniqueidentifier	= '2770534E-4675-4764-A620-56776398E03E',
				@MasterMiniImageId			uniqueidentifier,
				@VariantMiniImageId			uniqueidentifier,
				@MiniImageTypeId			uniqueidentifier	= 'E02ED0F3-1271-4218-B81E-816E458434A1',
				@MasterSystemThumbImageId	uniqueidentifier,
				@VariantSystemThumbImageId	uniqueidentifier,
				@SystemThumbImageTypeId		uniqueidentifier	= '63CF2418-684A-4D1F-88B2-34968C6F139F',
				@MasterThumbnailImageId		uniqueidentifier,
				@VariantThumbnailImageId	uniqueidentifier,
				@ThumbnailImageTypeId		uniqueidentifier	= 'BA9B3922-4F9F-4B21-A5CF-E03B26B588AD',
				@Output						varchar(512)		= ''

		if @SiteId is null or @ObjectId is null or @ObjectType is null or @Sequence is null
			raiserror(15600, -1, -1, 'SiteId, ObjectId, ObjectType, or Sequence is null')

		begin tran
		select @MasterSiteId=Id from SISite where id=MasterSiteId

		--get master image IDs for each image type
		select
			@MasterMainImageId=main,
			@MasterPreviewImageId=preview,
			@MasterMiniImageId=mini,
			@MasterSystemThumbImageId=system_thumb,
			@MasterThumbnailImageId=thumbnail
		from 
			(select o.SiteId, o.ObjectId, o.Sequence, i.id as ImageId, t.Title as ImageType
			from CLImage i
				join CLObjectImage o on o.ImageId=i.Id and o.siteid=@MasterSiteId and o.Sequence=@Sequence
				join CLImageType t on i.ImageType=t.id
			) as src
			pivot (max(src.ImageId) for ImageType in ([main], [preview], [mini], [system_thumb], [thumbnail])) as piv
		where ObjectId=@ObjectId

		--get variant image IDs for each image type
		select
			@VariantMainImageId=main,
			@VariantPreviewImageId=preview,
			@VariantMiniImageId=mini,
			@VariantSystemThumbImageId=system_thumb,
			@VariantThumbnailImageId=thumbnail
		from 
			(select o.SiteId, o.ObjectId, o.Sequence, i.id as ImageId, t.Title as ImageType
			from CLImageVariant i
				join CLObjectImage o on o.ImageId=i.Id and o.siteid=@MasterSiteId and o.Sequence=@Sequence
				join CLImageType t on i.ImageType=t.id
			) as src
			pivot (max(src.ImageId) for ImageType in ([main], [preview], [mini], [system_thumb], [thumbnail])) as piv
		where ObjectId=@ObjectId


		if @MasterMainImageId is null begin
			set @Output = concat(
				'Could not find master main image for product=', @ProductIdUser,
				' | sequence=', @Sequence)
			raiserror(15600, -1, -1, @Output)
		end

		set @Output = concat(@Output, 'inserting image for product=', @ProductIdUser)
		declare @MasterTitle		nvarchar(256),
				@MasterDescription	nvarchar(4000),
				@MasterAltText		nvarchar(1024)

		select 
			@MasterTitle=iif(v.Title is null, i.Title, null),
			@MasterDescription=iif(v.Description is null, i.Description, null),
			@MasterAltText=iif(v.AltText is null, i.AltText, null)
		from CLImage i
			left join CLImageVariant v on i.Id=v.Id and v.SiteId=@SiteId
		where i.Id=@MasterMainImageId

		select
			@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
			@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end,
			@AltText = case when @MasterAltText is null or @AltText <> @MasterAltText and @AltText is not null then @AltText else null end

		--main
		set @Output = concat(@Output, ' | main=')
		if @VariantMainImageId is not null begin
			set @Output = concat(@Output, 'updated')
			update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantMainImageId and SiteId=@SiteId
		end else begin
			set @Output = concat(@Output, 'inserted')
			insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
				values(@MasterMainImageId, @SiteId, @Title, @Description, @AltText, @MainImageTypeId, NULL, getdate(), @ModifiedBy)
		end

		--preview
		set @Output = concat(@Output, ' | preview=')
		if @MasterPreviewImageId is null begin
			set @Output = concat(@Output, 'skipped')
		end else begin
			if @VariantPreviewImageId is not null begin
				set @Output = concat(@Output, 'updated')
				update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantPreviewImageId and SiteId=@SiteId
			end else begin
				set @Output = concat(@Output, 'inserted')
				insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
					values(@MasterPreviewImageId, @SiteId, @Title, @Description, @AltText, @PreviewImageTypeId, NULL, getdate(), @ModifiedBy)
			end
		end

		--mini
		set @Output = concat(@Output, ' | mini=')
		if @MasterMiniImageId is null begin
			set @Output = concat(@Output, 'skipped')
		end else begin
			if @VariantMiniImageId is not null begin
				set @Output = concat(@Output, 'updated')
				update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantMiniImageId and SiteId=@SiteId
			end else begin
				set @Output = concat(@Output, 'inserted')
				insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
					values(@MasterMiniImageId, @SiteId, @Title, @Description, @AltText, @MiniImageTypeId, NULL, getdate(), @ModifiedBy)
			end
		end

		--system thumb
		set @Output = concat(@Output, ' | systemthumb=')
		if @MasterSystemThumbImageId is null begin
			set @Output = concat(@Output, 'skipped')
		end else begin
			if @VariantSystemThumbImageId is not null begin
				set @Output = concat(@Output, 'updated')
				update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantSystemThumbImageId and SiteId=@SiteId
			end else begin
				set @Output = concat(@Output, 'inserted')
				insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
					values(@MasterSystemThumbImageId, @SiteId, @Title, @Description, @AltText, @SystemThumbImageTypeId, NULL, getdate(), @ModifiedBy)
			end
		end

		--thumbnail
		set @Output = concat(@Output, ' | thumbnail=')
		if @MasterThumbnailImageId is null begin
			set @Output = concat(@Output, 'skipped')
		end else begin
			if @VariantThumbnailImageId is not null begin
				set @Output = concat(@Output, 'updated')
				update CLImageVariant set Title=@Title, Description=@Description, AltText=@AltText where Id=@VariantThumbnailImageId and SiteId=@SiteId
			end else begin
				set @Output = concat(@Output, 'inserted')
				insert into CLImageVariant (Id, SiteId, Title, Description, AltText, ImageType, Status, ModifiedDate, ModifiedBy)
					values(@MasterThumbnailImageId, @SiteId, @Title, @Description, @AltText, @ThumbnailImageTypeId, NULL, getdate(), @ModifiedBy)
			end
		end

		commit tran
		select @MasterMainImageId, @Output
	end try
	begin catch
		rollback tran
		DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE(),
				@ErrorState int				= ERROR_STATE(),
				@ErrorSeverity int			= ERROR_SEVERITY()
        RAISERROR(@ErrorMessage,@ErrorSeverity, @ErrorState)
	end catch
end
go

print 'Creating BLD_ProductImport_ProductAttributeVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_ProductAttributeVariant')
    drop procedure BLD_ProductImport_ProductAttributeVariant
go

CREATE PROCEDURE [dbo].[BLD_ProductImport_ProductAttributeVariant]
(
	@SiteId					uniqueidentifier,
	@ProductIdUser			nvarchar(50),
	@AttributeTitle			nvarchar(4000),
	@AttributeCategoryName	nvarchar(2000),
	@Sku					nvarchar(255),
	@IsSkuLevel				bit,
	@Value					nvarchar(max),
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	begin try
		
		declare @MasterProductId			uniqueidentifier,
				@MasterProductAttributeId	uniqueidentifier,
				@MasterAttributeId			uniqueidentifier,
				@MasterAttributeEnumId		uniqueidentifier,
				@MasterSkuId				uniqueidentifier,
				@MasterValue				nvarchar(max),
				@NumbericValue				int = iif(@Value like 'true', 1, iif(@Value like 'false', 0, null)),
				@IsSkipped					bit = 1,
				@Output						varchar(512),
				@IsEnum bit

		if @SiteId is null or @ProductIdUser is null or @AttributeTitle is null or @AttributeCategoryName is null or (@IsSkuLevel=1 and @Sku is null)
			raiserror(15600, -1, -1, 'SiteId, ProductIdUser, AttributeTitle, AttributeCategoryName, or Sku is null')

		begin tran
		select
			@MasterProductId=p.Id,
			@MasterProductAttributeId=pa.Id,
			@MasterAttributeId=pa.AttributeId,
			@MasterAttributeEnumId = e.Id,
			@MasterSkuId=s.Id,
			@IsEnum = a.IsEnum
		from PRProductAttribute pa
			join PRProduct p on pa.ProductId=p.Id
			left join PRProductSKU s on p.Id=s.ProductId and s.SKU=@Sku
			join ATAttribute a on pa.AttributeId=a.Id
			left join ATAttributeVariant av on a.Id=av.Id and av.SiteId=@SiteId
			join ATAttributeCategoryItem i on a.Id=i.AttributeId
			join ATAttributeCategory c on i.CategoryId=c.Id and c.Name=@AttributeCategoryName
			left join ATAttributeEnum e on a.Id=e.AttributeID
			left join ATAttributeEnumVariant ev on e.Id=ev.Id and ev.SiteId=@SiteId
		where
			p.ProductIDUser like @ProductIdUser and
			pa.IsSKULevel = isnull(@IsSkuLevel, 0) and
			((a.IsEnum=1 and isnull(ev.Title, e.Title) like @Value and e.Status = 1) or
			(a.IsEnum=0 and isnull(av.Title, a.Title) like @AttributeTitle))

		if @MasterProductId is null
		begin
			set @Output = concat(
				'Could not find product attribute for ProductId=', @ProductIdUser, 
				' | sku=', @Sku,
				' | category=', @AttributeCategoryName, 
				' | attribute title=', @AttributeTitle,
				' | value=', @Value)
			raiserror(15600, -1, -1, @Output)
		end

		if @IsSkuLevel=1
		begin
			declare @MasterProductSkuAttributeValueId	uniqueidentifier,
					@ProductSkuAttributeValueVariantId	uniqueidentifier

			select 
				@MasterProductSkuAttributeValueId=m.Id, 
				@MasterValue=iif(v.Value is null, m.Value, null),
				@ProductSkuAttributeValueVariantId=v.Id
			from PRProductSkuAttributeValue m
				left join PRProductSkuAttributeValueVariant v on m.Id=v.Id and v.SiteId=@SiteId
				left join ATAttributeEnum ae on m.attributeenumid = ae.id
				left join ATAttributeEnumVariant aev on m.attributeenumid = aev.id
			where m.SKUId=@MasterSkuId and m.AttributeId=@MasterAttributeId and ProductAttributeId=@MasterProductAttributeId
			      AND ((@IsEnum = 1 and (ae.Title = @Value or aev.Title = @Value)) or @IsEnum=0)

			if @MasterProductSkuAttributeValueId is null
			begin
				set @Output = concat('Unable to find PRProductSkuAttributeValue with sku/attribute/productattribute: ', @MasterSkuId, '|', @MasterAttributeId, '|', @MasterProductAttributeId)
				raiserror(15600, -1, -1, @Output)
			end
			
			select @Value = case when @MasterValue is null or @Value <> @MasterValue and @Value is not null then @Value else null end

			if @ProductSkuAttributeValueVariantId is not null
			begin
				update PRProductSkuAttributeValueVariant set
					Value=@Value,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getdate()
				where Id=@ProductSkuAttributeValueVariantId and SiteId=@SiteId

				set @Output = concat('updated PRProductSkuAttributeValueVariant id=', @MasterProductSkuAttributeValueId, ' value=', @Value)
			end
			else
			begin
				if @Value is not null
				begin
					insert into PRProductSkuAttributeValueVariant (Id, SiteId, SkuId, AttributeId, AttributeEnumId, Value, ModifiedBy, ModifiedDate)
						values (@MasterProductSkuAttributeValueId, @SiteId, @MasterSkuId, @MasterAttributeId, @MasterAttributeEnumId, @Value, @ModifiedBy, getdate())
					set @IsSkipped = 0
				end
				set @Output = concat(
					'Sku level attribute value variant ', 
					iif(@IsSkipped=1, 'skipped', 'inserted '), 
					' id=', @ProductIdUser, 
					' | sku', @Sku,
					' | category', @AttributeCategoryName, 
					' | attribute', @AttributeTitle, 
					' | value', @Value)
			end
		end
		else
		begin
			--Product Level Attribute
			declare @MasterProductAttributeValueId	uniqueidentifier,
					@ProductAttributeValueVariantId	uniqueidentifier

			select 
				@MasterProductAttributeValueId=m.Id, 
				@MasterValue=iif(v.Value is null, m.Value, null), 
				@ProductAttributeValueVariantId=v.Id
			from PRProductAttributeValue m
				left join PRProductAttributeValueVariant v on m.Id=v.Id and v.SiteId=@SiteId
				left join ATAttributeEnum ae on m.attributeenumid = ae.id
				left join ATAttributeEnumVariant aev on m.attributeenumid = aev.id
			where m.ProductId=@MasterProductId and m.AttributeId=@MasterAttributeId and ProductAttributeId=@MasterProductAttributeId
			      AND ((@IsEnum = 1 and (ae.Title = @Value or aev.Title = @Value)) or @IsEnum=0)

			if @MasterProductAttributeValueId is null
			begin
				set @Output = concat('Unable to find PRProductAttributeValue with product/attribute/productattribute/isenum/value: ', @MasterProductId, '|', @MasterAttributeId, '|', @MasterProductAttributeId, '|', @IsEnum, '|', @Value)
				raiserror(15600, -1, -1, @Output)
			end

			select @Value = case when (@MasterValue is null or @Value <> @MasterValue) and @Value is not null then @Value else null end
			if @ProductAttributeValueVariantId is not null
			begin
				update PRProductAttributeValueVariant set 
					Value = @Value, 
					ModifiedBy = @ModifiedBy, 
					ModifiedDate = getdate()
				where Id=@ProductAttributeValueVariantId and SiteId=@SiteId
				set @Output = concat('updated PRProductAttributeValueVariant id=', @ProductAttributeValueVariantId, ' value=', @Value, ' mastervalue=', @MasterValue)
			end
			else
			begin
				if @Value is not null
				begin
					insert into PRProductAttributeValueVariant (Id, SiteId, ProductId, AttributeId, AttributeEnumId, Value, ModifiedBy, ModifiedDate) 
						values (@MasterProductAttributeValueId, @SiteId, @MasterProductId, @MasterAttributeId, @MasterAttributeEnumId, @Value, @ModifiedBy, getdate())
					set @IsSkipped = 0
				end
				
				set @Output = concat(
					'Product level attribute value variant ', 
					iif(@IsSkipped=1, 'skipped', 'inserted '), 
					'id=', @ProductIdUser, 
					' | category=', @AttributeCategoryName, 
					' | attribute', @AttributeTitle, 
					' | value', @Value)
			end
		end

		commit tran
		select @MasterProductAttributeValueId, @MasterProductSkuAttributeValueId, @Output
	end try
	begin catch
		rollback tran
		DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE(),
				@ErrorState int				= ERROR_STATE(),
				@ErrorSeverity int			= ERROR_SEVERITY()
        RAISERROR(@ErrorMessage,@ErrorSeverity, @ErrorState)

	end catch
END
go

print 'Creating BLD_ProductImport_ProductSkuVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_ProductSkuVariant')
    drop procedure BLD_ProductImport_ProductSkuVariant
go

CREATE procedure [dbo].[BLD_ProductImport_ProductSkuVariant]
(
	@SiteId						uniqueidentifier,
	@ProductId					nvarchar(100),
	@Sku						nvarchar(255),
	@Title						nvarchar(555),
	--@Description				nvarchar(max),
	--@Sequence					int,
	@IsActive					bit,
	@IsOnline					bit,
	@ListPrice					money,
	@UnitCost					money,
	--@WholesalePrice				money,
	@Measure					nvarchar(50),
	@OrderMinimum				decimal(18,4),
	@OrderIncrement				decimal(18,4),
	@HandlingCharges			decimal(18,4),
	@SelfShippable				bit,
	@AvailableForBackOrder		bit,
	@BackOrderLimit				decimal(18,4),
	@IsUnlimitedQuantity		bit,
	--@MaximumDiscountPercentage	money,
	--@FreeShipping				bit,
	--@UserDefinedPrice			bit,
	@ModifiedBy					uniqueidentifier
)
as
begin
	begin try

		declare @ProductSkuVariantId				uniqueidentifier
		declare @MasterProductSkuId					uniqueidentifier
		declare @MasterTitle						nvarchar(555)
		declare @MasterSequence						int
		declare @MasterIsActive						bit
		declare @MasterIsOnline						bit
		declare @MasterListPrice					money
		declare @MasterUnitCost						money
		declare @MasterWholesalePrice				money
		declare @MasterMeasure						nvarchar(50)
		declare @MasterOrderMinimum					decimal(18,4)
		declare @MasterOrderIncrement				decimal(18,4)
		declare @MasterHandlingCharges				decimal(18,4)
		declare @MasterSelfShippable				bit
		declare @MasterAvailableForBackOrder		bit
		declare @MasterBackOrderLimit				decimal(18,4)
		declare @MasterIsUnlimitedQuantity			bit
		declare @MasterMaximumDiscountPercentage	money
		declare @MasterFreeShipping					bit
		declare @MasterUserDefinedPrice				bit
		declare @Output								nvarchar(512) = ''
		declare @IsSkipped							bit = 1

		if (@SiteId is null or @ProductId is null or @ModifiedBy is null)
			raiserror(15600, -1, -1, 'BLD_ProductImport_ProductSkuVariant SiteId, ModifiedBy, or ProductId is null')

		begin tran
		select 
			@MasterProductSkuId=ps.Id,
			@MasterTitle=iif(v.Title is null, ps.Title, null),
			@MasterSequence=ps.Sequence,
			@MasterIsActive=iif(v.IsActive is null, ps.IsActive, null),
			@MasterIsOnline=iif(v.IsOnline is null, ps.IsOnline, null),
			@MasterListPrice=iif(v.ListPrice is null, ps.ListPrice, null),
			@MasterUnitCost=iif(v.UnitCost is null, ps.UnitCost, null),
			@MasterWholesalePrice=ps.WholesalePrice,
			@MasterMeasure=iif(v.Measure is null, ps.Measure, null),
			@MasterOrderMinimum=iif(v.OrderMinimum is null, ps.OrderMinimum, null),
			@MasterOrderIncrement=iif(v.OrderIncrement is null, ps.OrderIncrement, null),
			@MasterHandlingCharges=iif(v.HandlingCharges is null, ps.HandlingCharges, null),
			@MasterSelfShippable=iif(v.SelfShippable is null, ps.SelfShippable, null),
			@MasterAvailableForBackOrder=iif(v.AvailableForBackOrder is null, ps.AvailableForBackOrder, null),
			@MasterBackOrderLimit=iif(v.BackOrderLimit is null, ps.BackOrderLimit, null),
			@MasterIsUnlimitedQuantity=iif(v.IsUnlimitedQuantity is null, ps.IsUnlimitedQuantity, null),
			@MasterMaximumDiscountPercentage=ps.MaximumDiscountPercentage,
			@MasterFreeShipping=ps.FreeShipping,
			@MasterUserDefinedPrice=ps.UserDefinedPrice
		from PRProductSku ps
			left join PRProductSKUVariant v on ps.Id=v.Id and v.SiteId=@SiteId
			join PRProduct p on ps.ProductId=p.Id
			join SISite s on ps.SiteId=s.Id
		where p.ProductIDUser=@ProductId and ps.SKU=@Sku and s.Id=s.MasterSiteId

		if @MasterProductSkuId is null
		begin
			set @Output = concat('Product Id [', @ProductId, '] with SKU [', @Sku, 'does not exist on master site')
		end 
		else
		begin
			select @ProductSkuVariantId=Id from PRProductSKUVariant where Id=@MasterProductSkuId and SiteId=@SiteId
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@IsActive = case when @MasterIsActive is null or @IsActive <> @MasterIsActive and @IsActive is not null then @IsActive else null end,
				@IsOnline = case when @MasterIsOnline is null or @IsOnline <> @MasterIsOnline and @IsOnline is not null then @IsOnline else null end,
				@ListPrice = case when @MasterListPrice is null or @ListPrice <> @MasterListPrice and @ListPrice is not null then @ListPrice else null end,
				@UnitCost = case when @MasterUnitCost is null or @UnitCost <> @MasterUnitCost and @UnitCost is not null then @UnitCost else null end,
				@Measure = case when @MasterMeasure is null or @Measure <> @MasterMeasure and @Measure is not null then @Measure else null end,
				@OrderMinimum = case when @MasterOrderMinimum is null or @OrderMinimum <> @MasterOrderMinimum and @OrderMinimum is not null then @OrderMinimum else null end,
				@OrderIncrement = case when @MasterOrderIncrement is null or @OrderIncrement <> @MasterOrderIncrement and @OrderIncrement is not null then @OrderIncrement else null end,
				@HandlingCharges = case when @MasterHandlingCharges is null or @HandlingCharges <> @MasterHandlingCharges and @HandlingCharges is not null then @HandlingCharges else null end,
				@SelfShippable = case when @MasterSelfShippable is null or @SelfShippable <> @MasterSelfShippable and @SelfShippable is not null then @SelfShippable else null end,
				@AvailableForBackOrder = case when @MasterAvailableForBackOrder is null or @AvailableForBackOrder <> @MasterAvailableForBackOrder and @AvailableForBackOrder is not null then @AvailableForBackOrder else null end,
				@BackOrderLimit = case when @MasterBackOrderLimit is null or @BackOrderLimit <> @MasterBackOrderLimit and @BackOrderLimit is not null then @BackOrderLimit else null end,
				@IsUnlimitedQuantity = case when @MasterIsUnlimitedQuantity is null or @IsUnlimitedQuantity <> @MasterIsUnlimitedQuantity and @IsUnlimitedQuantity is not null then @IsUnlimitedQuantity else null end

			if @ProductSkuVariantId is not null
			begin
				update PRProductSKUVariant set
					Title=@Title,
					IsActive=@IsActive,
					IsOnline=@IsOnline,
					ListPrice=@ListPrice,
					UnitCost=@UnitCost,
					Measure=@Measure,
					OrderMinimum=@OrderMinimum,
					OrderIncrement=@OrderIncrement,
					HandlingCharges=@HandlingCharges,
					SelfShippable=@SelfShippable,
					AvailableForBackOrder=@AvailableForBackOrder,
					BackOrderLimit=@BackOrderLimit,
					IsUnlimitedQuantity=@IsUnlimitedQuantity,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getdate()
				where Id=@ProductSkuVariantId and SiteId=@SiteId

				set @Output = concat('updated PRProductSkuVariant Id [', @ProductSkuVariantId, ']')
			end
			else
			begin
				if coalesce(cast(@Title as sql_variant), @IsActive, @IsOnline, @ListPrice, @UnitCost, @Measure, @OrderMinimum, @OrderIncrement, @HandlingCharges, @SelfShippable, @AvailableForBackOrder, @BackOrderLimit, @IsUnlimitedQuantity) is not null
				begin
					insert into PRProductSKUVariant (SiteId, Id, Title, IsActive, IsOnline, ListPrice, UnitCost, Measure, OrderMinimum, OrderIncrement, HandlingCharges, SelfShippable, AvailableForBackOrder, BackOrderLimit, IsUnlimitedQuantity, ModifiedBy, ModifiedDate)
						values (@SiteId, @MasterProductSkuId, @Title, @IsActive, @IsOnline, @ListPrice, @UnitCost, @Measure, @OrderMinimum, @OrderIncrement, @HandlingCharges, @SelfShippable, @AvailableForBackOrder, @BackOrderLimit, @IsUnlimitedQuantity, @ModifiedBy, getdate())
					set @IsSkipped = 0
				end

				set @Output = concat(
					'PRProductSkuVariant ', 
					iif(@IsSkipped=1, 'skipped', concat('inserted Title=', @Title, ' | sku=', @Sku)))
			end
			
		end
		commit tran
		select @Output
	end try
	begin catch
		rollback tran
		declare @ErrorMessage nvarchar(4000) = error_message(),
				@ErrorSeverity int = error_severity(),
				@ErrorState int = error_state()
		print concat('BLD_ProductImport_ProductSkuVariant error: severity= ', @ErrorSeverity, ' state=', @ErrorState, ' message=', @ErrorMessage)
		raiserror(@ErrorMessage, @ErrorSeverity, @ErrorState)
	end catch
end
go

print 'Creating BLD_ProductImport_ProductTypeVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_ProductTypeVariant')
    drop procedure BLD_ProductImport_ProductTypeVariant
go

create procedure [dbo].[BLD_ProductImport_ProductTypeVariant] 
(
	@SiteId					uniqueidentifier,
	@ProductTypeId			uniqueidentifier,
	@Title					nvarchar(50),
	@Description			nvarchar(200),
	@IsDownloadableMedia	bit,
	--@FriendlyName			nvarchar(50),
	--@CMSPageId			uniqueidentifier,
	--@Status				int,
	@ModifiedBy				uniqueidentifier
)
as
begin
	begin try
		declare @MasterProductTypeId		uniqueidentifier
		declare @ProductTypeVariantId		uniqueidentifier
		declare @MasterTitle				nvarchar(50)
		declare @MasterDescription			nvarchar(200)
		declare @MasterFriendlyName			nvarchar(50)
		declare @MasterStatus				int
		declare @MasterIsDownloadableMedia	bit
		declare @Output						nvarchar(512) = ''
		declare @IsSkipped					bit = 1

		if (@SiteId is null or @ModifiedBy is null)
			raiserror(15600, -1, -1, 'BLD_ProductImport_ProductTypeVariant SiteId, ModifiedBy or ProductTypeId is null')
		
		begin tran
		select 
			@MasterProductTypeId=pt.Id, 
			@MasterTitle=iif(v.Title is null, pt.Title,  null),
			@MasterDescription=iif(v.Description is null, pt.Description,  null),
			@MasterFriendlyName=pt.FriendlyName, 
			@MasterIsDownloadableMedia=pt.IsDownloadableMedia,
			@MasterStatus=pt.Status
		from PRProductType pt
			left join PRProductTypeVariant v on pt.Id=v.Id and v.SiteId=@SiteId
			join SISite s on pt.SiteId=s.Id
		where pt.Id=@ProductTypeId and s.Id=s.MasterSiteId

		if @MasterProductTypeId is null
		begin
			set @Output = concat('Product Type id=[', @ProductTypeId, '] does not exist on master site')
		end
		else
		begin
			select @ProductTypeVariantId=Id from PRProductTypeVariant where id=@MasterProductTypeId and SiteId=@SiteId
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@Description = case when @MasterDescription is null or @Description <> @MasterDescription and @Description is not null then @Description else null end,
				@IsDownloadableMedia = case when @MasterIsDownloadableMedia is null or @IsDownloadableMedia <> @MasterIsDownloadableMedia and @IsDownloadableMedia is not null then @IsDownloadableMedia else null end

			if @ProductTypeVariantId is not null 
			begin
				update PRProductTypeVariant set
					Title=@Title,
					Description=@Description,
					FriendlyName=@MasterFriendlyName,
					--CMSPageId=@CMSPageId,
					--Status=@Status
					ModifiedDate=getdate(),
					ModifiedBy=@ModifiedBy
				where Id=@MasterProductTypeId and SiteId=@SiteId

				set @Output = concat('updated PRProductTypeVariant ', @MasterProductTypeId, ' ', @Title, ' ', @Description)
			end
			else
			begin
				if coalesce(@Title, @Description) is not null
				begin
					insert into PRProductTypeVariant (Id, Title, Description, FriendlyName, SiteId, ModifiedDate, ModifiedBy) 
						values (@MasterProductTypeId, @Title, @Description, null, @SiteId, getdate(), @ModifiedBy)
					set @IsSkipped = 0
				end

				set @Output = concat('PRProductTypeVariant ', iif(@IsSkipped=1, 'skipped', concat('inserted. title=', @Title)))
			end

		end
		commit tran
		select @MasterProductTypeId, @Output
	end try
	begin catch
		rollback tran
		declare @ErrorMessage nvarchar(4000) = error_message()
		declare @ErrorSeverity int = error_severity()
		declare @ErrorState int = error_state()
		print concat('BLD_ProductImport_ProductTypeVariant error: severity= ', @ErrorSeverity, ' state=', @ErrorState, ' message=', @ErrorMessage)
		raiserror(@ErrorMessage, @ErrorSeverity, @ErrorState)
	end catch
end
go

print 'Creating BLD_ProductImport_ProductVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_ProductImport_ProductVariant')
    drop procedure BLD_ProductImport_ProductVariant
go

create PROCEDURE [dbo].[BLD_ProductImport_ProductVariant]
	@SiteId				uniqueidentifier,
	@ProductIDUser		nvarchar(100),
	@ProductTypeName	nvarchar(50),
	@ProductTypeId		uniqueidentifier,
	@Title				nvarchar(555),
	@ShortDescription	nvarchar(4000),
	@LongDescription	nvarchar(4000),
	@IsActive			bit,
	@SEOTitle			nvarchar(255),
	@SEODescription		nvarchar(4000),
	@SEOKeywords		nvarchar(4000),
	@SEOFriendlyURL		nvarchar(4000),
	@ModifiedBy			uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	declare @ActionType				int					= 0
	declare @MasterSiteId			uniqueidentifier
	declare @MasterProductId		uniqueidentifier
	declare @MasterProductTypeId	uniqueidentifier
	declare @MasterTitle			nvarchar(555)
	declare @MasterShortDescription nvarchar(4000)
	declare @MasterLongDescription	nvarchar(4000)
	declare @MasterIsActive			bit
	declare @MasterSEOTitle			nvarchar(255)
	declare @MasterSEODescription	nvarchar(4000)
	declare @MasterSEOKeywords		nvarchar(4000)
	declare @MasterSEOFriendlyURL	nvarchar(255)
	declare @IsSkipped				bit = 1
	declare @Output					nvarchar(512)

	if coalesce(cast(@SiteId as sql_variant), @ProductIdUser, @ProductTypeId) is null
		raiserror(15600, -1, -1, 'SiteId, ProductIdUser, or ProductTypeId is null')

	BEGIN TRY
        BEGIN TRAN
		
		select @MasterSiteId=Id from SISite where Id=MasterSiteId

		select 
			@MasterProductId=p.Id, 
			@MasterProductTypeId=p.ProductTypeID,
			@MasterTitle=iif(v.Title is null, p.Title, null),
			@MasterShortDescription=iif(v.ShortDescription is null, p.ShortDescription, null),
			@MasterLongDescription=iif(v.LongDescription is null, p.LongDescription, null),
			@MasterIsActive=iif(v.IsActive is null, p.IsActive, null),
			@MasterSEOTitle=iif(v.SEOH1 is null, p.SEOH1, null),
			@MasterSEODescription=iif(v.SEODescription is null, p.SEODescription, null),
			@MasterSEOKeywords=iif(v.SEOKeywords is null, p.SEOKeywords, null),
			@MasterSEOFriendlyURL=iif(v.SEOFriendlyUrl is null, p.SEOFriendlyUrl, null)
		from PRProduct p
			left join PRProductVariant v on p.Id=v.Id and v.SiteId=@SiteId
		where p.ProductIDUser like @ProductIDUser and p.ProductTypeID like @ProductTypeId and p.SiteId=@MasterSiteId

		if @MasterProductId is null or @MasterProductTypeId is null
		begin
			set @Output = concat('Product Id: [', @ProductIDUser, ']/[', @ProductTypeId, '] does not exist.')
			raiserror(15600, -1, -1, @Output)
		end
		else
		begin
			select
				@Title = case when @MasterTitle is null or @Title <> @MasterTitle and @Title is not null then @Title else null end,
				@ShortDescription = case when @MasterShortDescription is null or @ShortDescription <> @MasterShortDescription and @ShortDescription is not null then @ShortDescription else null end,
				@LongDescription = case when @MasterLongDescription is null or @LongDescription <> @MasterLongDescription and @LongDescription is not null then @LongDescription else null end,
				@IsActive = case when @MasterIsActive is null or @IsActive <> @MasterIsActive and @IsActive is not null then @IsActive else null end,
				@SEOTitle = case when @MasterSEOTitle is null or @SEOTitle <> @MasterSEOTitle and @SEOTitle is not null then @SEOTitle else null end,
				@SEODescription = case when @MasterSEODescription is null or @SEODescription <> @MasterSEODescription and @SEODescription is not null then @SEODescription else null end,
				@SEOKeywords = case when @MasterSEOKeywords is null or @SEOKeywords <> @MasterSEOKeywords and @SEOKeywords is not null then @SEOKeywords else null end,
				@SEOFriendlyURL = case when @MasterSEOFriendlyURL is null or @SEOFriendlyURL <> @MasterSEOFriendlyURL and @SEOFriendlyURL is not null then @SEOFriendlyURL else null end

			if exists (select 1 from PRProductVariant where Id=@MasterProductId and SiteId=@SiteId)
			begin
				update PRProductVariant set
					Title=@Title,
					ShortDescription=@ShortDescription,
					LongDescription=@LongDescription,
					Description=@LongDescription,
					IsActive=@IsActive,
					SEOH1=@SEOTitle,
					SEODescription=@SEODescription,
					SEOKeywords=@SEOKeywords,
					SEOFriendlyURL=@SEOFriendlyURL,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getdate()
				where Id=@MasterProductId and SiteId=@SiteId

				set @Output = concat('updated PRProductVariant id=', @MasterProductId)
				set @actionType = 2
			end
			else
			begin
				if coalesce(cast(@Title as sql_variant), @ShortDescription, @LongDescription, @IsActive, @SEOTitle, @SEODescription, @SEOKeywords, @SEOFriendlyURL) is not null
				begin
					insert into PRProductVariant (SiteId, Id, ProductTypeId, Title, ShortDescription, LongDescription, Description, IsActive, SEOH1, SEODescription, SEOKeywords, SEOFriendlyURL, ModifiedBy, ModifiedDate)
						values (@SiteId, @MasterProductId, @MasterProductTypeId, @Title, @ShortDescription, @LongDescription, @LongDescription, @IsActive, @SEOTitle, @SEODescription, @SEOKeywords, @SEOFriendlyURL, @Modifiedby, getdate())
					set @IsSkipped = 0
				end

				set @Output = concat(
					'PRProductVariant ', 
					iif(
						@IsSkipped=1, 
						'skipped', 
						concat('inserted Name=', @Title, ' | product type=', @ProductTypeName)))
				set @actionType = 2
			end

		end
		COMMIT TRAN
		select @MasterProductid, @ActionType, @Output
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN
        DECLARE @ErrorMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		declare @ErrorState int = ERROR_STATE()
		declare @ErrorSeverity int = ERROR_SEVERITY()
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
    END CATCH

END
go

print 'Creating spDeleteLoadedProductsVariant'
if exists (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='spDeleteLoadedProductsVariant')
    drop procedure spDeleteLoadedProductsVariant
go

CREATE PROCEDURE [dbo].[spDeleteLoadedProductsVariant]
    (
      @VariantSiteId uniqueidentifier null
    )
AS 
BEGIN
	begin try
		begin transaction
			IF @VariantSiteId IS NOT NULL 
				BEGIN
					print 'deleting from CLImageVariant for variant'
					DELETE FROM dbo.CLImageVariant
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from CLObjectImage for variant'
					DELETE FROM dbo.CLObjectImage
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from CLImage for variant'
					DELETE FROM dbo.CLImage
					WHERE SiteId = @VariantSiteId
					
					print 'finding attribute enums for variant'
					SELECT Id INTO #variantAttributeEnumIds FROM dbo.ATAttributeEnumVariant
					WHERE SiteId = @VariantSiteId
					
					print 'finding attributes for variant'
					SELECT Id INTO #variantAttributeIds FROM dbo.ATAttributeVariant
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from dbo.ATAttributeValue for variant enum ids'
					DELETE FROM dbo.ATAttributeValue
					WHERE AttributeEnumId in (SELECT Id from #variantAttributeEnumIds)
					
					print 'deleting from dbo.AtAttributeValue for variant attribute ids'
					DELETE FROM dbo.ATAttributeValue
					WHERE AttributeId in (SELECT Id from #variantAttributeIds)
					
					print 'deleting from dbo.ATAttributeVariant for variant'
					DELETE FROM dbo.ATAttributeVariant
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from dbo.ATAttributeEnumVariant for variant'
					DELETE FROM dbo.ATAttributeEnumVariant
					WHERE SiteId = @VariantSiteId
					
					print 'finding pricesets for variant'
					SELECT Id INTO #variantPriceSetIds from dbo.PSPriceSet
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from dbo.PSPriceSetManualSKU for variant'
					DELETE FROM dbo.PSPriceSetManualSKU
					WHERE PriceSetId in (SELECT Id FROM #variantPriceSetIds)
					
					print 'deleting from dbo.PSPriceSetDiscount for variant'
					DELETE FROM dbo.PSPriceSetDiscount
					WHERE PriceSetId in (SELECT Id FROM #variantPriceSetIds)
					
					print 'deleting from dbo.PSPriceSetProductType for variant'
					DELETE FROM dbo.PSPriceSetProductType
					WHERE PriceSetId in (SELECT Id FROM #variantPriceSetIds)
					
					print 'deleting from dbo.PSPriceSetSKU for variant'
					DELETE FROM dbo.PSPriceSetSKU
					WHERE PriceSetId in (SELECT Id FROM #variantPriceSetIds)
										
					print 'deleting from PSPriceSetType for variant'
					DELETE FROM PSPriceSetType
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from dbo.PRProductMediaVariant for variant'
					DELETE FROM dbo.PRProductMediaVariant 
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from dbo.PRProductSKUAttributeValueVariant for variant'
					DELETE FROM dbo.PRProductSKUAttributeValueVariant
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from dbo.PRProductAttributeValueVariant for variant'
					DELETE FROM dbo.PRProductAttributeValueVariant
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from dbo.PRProductSKUVariant for variant'
					DELETE FROM dbo.PRProductSKUVariant
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from dbo.PRProductVariant for variant'
					DELETE FROM dbo.PRProductVariant
					WHERE SiteId = @VariantSiteId
					
					print 'deleting from dbo.PRProductTypeVariant'
					DELETE FROM dbo.PRProductTypeVariant
					WHERE SiteId = @VariantSiteID
					
					DROP TABLE #variantAttributeIds
					DROP TABLE #variantAttributeEnumIds
					DROP TABLE #variantPriceSetIds
				END
			ELSE
				BEGIN
					print 'no site id has been passed. deleting all variant info'
					
					print 'detecting master site Id'
					declare @masterSiteId uniqueidentifier
					select @masterSiteId = s.Id from SISite s where s.Id=s.MasterSiteId 
					
					print 'deleting from CLImageVariant for all variants'
					DELETE FROM dbo.CLImageVariant
					
					print 'deleting from CLObjectImage for all variants'
					DELETE FROM dbo.CLObjectImage
					Where SiteId <> @masterSiteId
					
					print 'deleting from CLImage for all variants'
					DELETE FROM dbo.CLImage
					WHERE SiteId <> @masterSiteId
					
					print 'finding attribute enums for all variants'
					SELECT Id INTO #allVariantAttributeEnumIds FROM dbo.ATAttributeEnumVariant
					
					print 'finding attributes for all variants'
					SELECT Id INTO #allVariantAttributeIds FROM dbo.ATAttributeVariant
					
					print 'deleting from dbo.ATAttributeValue for all variant enum ids'
					DELETE FROM dbo.ATAttributeValue
					WHERE AttributeEnumId in (SELECT Id from #allVariantAttributeEnumIds)
					
					print 'deleting from dbo.AtAttributeValue for all variant attribute ids'
					DELETE FROM dbo.ATAttributeValue
					WHERE AttributeId in (SELECT Id from #allVariantAttributeIds)
					
					print 'deleting from dbo.ATAttributeVariant for all variants'
					DELETE FROM dbo.ATAttributeVariant
					
					print 'deleting from dbo.ATAttributeEnumVariant for all variants'
					DELETE FROM dbo.ATAttributeEnumVariant
										
					print 'finding pricesets for all variants'
					SELECT Id INTO #allVariantPriceSetIds from dbo.PSPriceSet
					WHERE SiteId <> @masterSiteId
					
					print 'deleting from dbo.PSPriceSetManualSKU for all variants'
					DELETE FROM dbo.PSPriceSetManualSKU
					WHERE PriceSetId in (SELECT Id FROM #allVariantPriceSetIds)
					
					print 'deleting from dbo.PSPriceSetDiscount for all variants'
					DELETE FROM dbo.PSPriceSetDiscount
					WHERE PriceSetId in (SELECT Id FROM #allVariantPriceSetIds)
					
					print 'deleting from dbo.PSPriceSetProductType for all variants'
					DELETE FROM dbo.PSPriceSetProductType
					WHERE PriceSetId in (SELECT Id FROM #allVariantPriceSetIds)
					
					print 'deleting from dbo.PSPriceSetSKU for all variants'
					DELETE FROM dbo.PSPriceSetSKU
					WHERE PriceSetId in (SELECT Id FROM #allVariantPriceSetIds)
										
					print 'deleting from PSPriceSetType for all variants'
					DELETE FROM PSPriceSetType
					WHERE SiteId <> @masterSiteId
					
					print 'deleting from dbo.PRProductMediaVariant for all variants'
					DELETE FROM dbo.PRProductMediaVariant 
					
					print 'deleting from dbo.PRProductSKUAttributeValueVariant for all variants'
					DELETE FROM dbo.PRProductSKUAttributeValueVariant
					
					print 'deleting from dbo.PRProductAttributeValueVariant for all variants'
					DELETE FROM dbo.PRProductAttributeValueVariant
					
					print 'deleting from dbo.PRProductSKUVariant for all variants'
					DELETE FROM dbo.PRProductSKUVariant
					
					print 'deleting from dbo.PRProductVariant for all variants'
					DELETE FROM dbo.PRProductVariant
					
					print 'deleting from dbo.PRProductTypeVariant for all variants'
					DELETE FROM dbo.PRProductTypeVariant
					
					DROP TABLE #allVariantAttributeIds
					DROP TABLE #allVariantAttributeEnumIds
					DROP TABLE #allVariantPriceSetIds
				END
		commit tran
	end try
	begin catch
		if (@@TRANCOUNT	> 0) rollback tran;
		throw;
	end catch
END

GO

PRINT 'Creating BLD_DeleteRelatedProductsByUpdateFlag'
GO
IF EXISTS (select 1 from INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='BLD_DeleteRelatedProductsByUpdateFlag')
    DROP PROCEDURE BLD_DeleteRelatedProductsByUpdateFlag
GO


CREATE PROCEDURE [dbo].[BLD_DeleteRelatedProductsByUpdateFlag]
(@UpdateFlag int)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE from PRProductToProductRel 
			where Id in 
			(select id from PRProductToProductRel pr 
			
						inner join bld_productrelation ldr 
						on  pr.ProductId in (select id from prproduct where productiduser = ldr.ProductId) and 
						pr.ProductRelationTypeId=ldr.RelationshipType and 
						 pr.RelatedProduct in (select id from prproduct where productiduser = ldr.RelatedProductId)
						where ldr.UpdateFlag <> 0) 

	UPDATE bld_productrelation set UpdateFlag = 0 where UpdateFlag = @UpdateFlag
END
GO
PRINT 'Creating procedure BLD_ClearProductTables'
GO
IF (OBJECT_ID('BLD_ClearProductTables') IS NOT NULL)
	DROP PROCEDURE BLD_ClearProductTables
GO
CREATE PROCEDURE [dbo].[BLD_ClearProductTables]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM BLD_PriceSetSku
	DELETE FROM BLD_PriceSet
	DELETE FROM BLD_AttributeEnumValue
	DELETE FROM BLD_ProductRelation
	DELETE FROM BLD_ProductAttribute
	DELETE FROM BLD_ProductImage
	DELETE FROM BLD_Sku
	DELETE FROM BLD_Product
	DELETE FROM BLD_Attribute;
	DELETE FROM BLD_ProductType;

END
GO

PRINT 'Set GroupId field of ATAttributeCategory to GUID if any GroupId is NULL'
GO

UPDATE ATAttributeCategory
SET GroupId = NEWID()
WHERE GroupId IS NULL
GO

GO

UPDATE iAppsProductSuite SET UpdateEndDate = GETUTCDATE()
GO
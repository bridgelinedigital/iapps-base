@SET SOLUTIONDIR=%CD%
@SET Base=%2

ECHO CMS Path: %Base%

@SET ExtensionsAdminPath=%Base%\ExtensionsAdmin

robocopy "%SOLUTIONDIR%\iAppsBase\Working\FrontEnd\CMSFrontEndSite" "%Base%\CMSFrontEnd" /e /a-:R /a+:S /xf "Web.appsettings.config" "Web.config" "Web.connections.config" "Web.exception.config" "Web.iappsbrowsercaps.config" "Web.iappsettings.config" "Web.logging.config" "Web.unity.config" "errorpage.aspx" "iapps_editor\authorStyles.css" "iapps_Editor\EditorContentArea.css" /xd "bin"

IF NOT EXIST %ExtensionsAdminPath% mkdir %ExtensionsAdminPath%
robocopy "%SOLUTIONDIR%\iAppsBase\Working\Admin\ExtensionsAdmin" "%ExtensionsAdminPath%" /e /a-:R /a+:S /xf "Web.config"

@SET APPCMD="%windir%\system32\inetsrv\Appcmd.exe"

@SET CMSPATH=%Base%\CMSFrontEnd
@SET LIBPATH=%Base%\Libraries
@SET APPNAME=%1

ECHO CMS Path:   %CMSPATH%
ECHO Lib Path:   %LIBPATH%
ECHO App Name:   %APPNAME%

IF NOT EXIST "%CMSPATH%" GOTO NOCMS
IF NOT EXIST "%LIBPATH%" GOTO NOLIB

%APPCMD% delete apppool /apppool.name:"%APPNAME%"
%APPCMD% add apppool /name:"%APPNAME%"
%APPCMD% set apppool /apppool.name:"%APPNAME%" /managedRuntimeVersion:v4.0
%APPCMD% set apppool /apppool.name:"%APPNAME%" /enable32BitAppOnWin64:true
%APPCMD% set config /section:applicationPools /[name='%APPNAME%'].processModel.identityType:LocalService

%APPCMD% delete site /site.name:"%APPNAME%"
%APPCMD% add site /name:"%APPNAME%" /bindings:http://%APPNAME%.local.iapps.com:80 /physicalPath:"%CMSPATH%"
%APPCMD% set site /site.name:"%APPNAME%" /+bindings.[protocol='https',bindingInformation='*:443:%APPNAME%.local.iapps.com']
%APPCMD% set app "%APPNAME%/" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/admin" /physicalPath:"%SOLUTIONDIR%\iAppsBase\Working\Admin\CMSAdmin"
%APPCMD% set app "%APPNAME%/admin" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/webservice" /physicalPath:"%SOLUTIONDIR%\iAppsBase\Working\Admin\CMSWebService"
%APPCMD% set app "%APPNAME%/webservice" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/CommerceAdmin" /physicalPath:"%SOLUTIONDIR%\iAppsBase\Working\Admin\CommerceAdmin"
%APPCMD% set app "%APPNAME%/CommerceAdmin" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/CommerceWebService" /physicalPath:"%SOLUTIONDIR%\iAppsBase\Working\Admin\CommerceWebService"
%APPCMD% set app "%APPNAME%/CommerceWebService" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/commonlogin" /physicalPath:"%SOLUTIONDIR%\iAppsBase\Working\Admin\iAPPSCommonLogin"
%APPCMD% set app "%APPNAME%/commonlogin" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/AnalyzerAdmin" /physicalPath:"%SOLUTIONDIR%\iAppsBase\Working\Admin\AnalyzerAdmin"
%APPCMD% set app "%APPNAME%/AnalyzerAdmin" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/MarketierAdmin" /physicalPath:"%SOLUTIONDIR%\iAppsBase\Working\Admin\MarketierAdmin"
%APPCMD% set app "%APPNAME%/MarketierAdmin" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/MarketierWebservice" /physicalPath:"%SOLUTIONDIR%\iAppsBase\Working\Admin\MarketierWebservice"
%APPCMD% set app "%APPNAME%/Marketierwebservice" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/ExtensionsAdmin" /physicalPath:"%ExtensionsAdminPATH%"
%APPCMD% set app "%APPNAME%/ExtensionsAdmin" /applicationPool:"%APPNAME%"

%APPCMD% add app /site.name:"%APPNAME%" /path:"/AMP" /physicalPath:"%Base%\AMP"
%APPCMD% set app "%APPNAME%/AMP" /applicationPool:"%APPNAME%"

%APPCMD% add vdir /app.name:"%APPNAME%/Admin" /path:"/iAPPS_Editor" /physicalPath:"%CMSPATH%\iAPPS_Editor"
%APPCMD% add vdir /app.name:"%APPNAME%/ExtensionsAdmin" /path:"/iAPPS_Editor" /physicalPath:"%CMSPATH%\iAPPS_Editor"
%APPCMD% add vdir /app.name:"%APPNAME%/MarketierAdmin" /path:"/iAPPS_Editor" /physicalPath:"%CMSPATH%\iAPPS_Editor"
%APPCMD% add vdir /app.name:"%APPNAME%/AnalyzerAdmin" /path:"/iAPPS_Editor" /physicalPath:"%CMSPATH%\iAPPS_Editor"
%APPCMD% add vdir /app.name:"%APPNAME%/CommerceAdmin" /path:"/iAPPS_Editor" /physicalPath:"%CMSPATH%\iAPPS_Editor"
%APPCMD% add vdir /app.name:"%APPNAME%/CommonLogin" /path:"/iAPPS_Editor" /physicalPath:"%CMSPATH%\iAPPS_Editor"

%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/CustomXSLT" /physicalPath:"%LIBPATH%\CodeLibrary\CustomXSLT"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/File Library" /physicalPath:"%LIBPATH%\File Library"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/Form Library" /physicalPath:"%LIBPATH%\Form Library"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/Image Library" /physicalPath:"%LIBPATH%\Image Library"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/Page Template Library" /physicalPath:"%LIBPATH%\Page Template Library"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/RssFeeds" /physicalPath:"%LIBPATH%\RssFeeds"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/Script Library" /physicalPath:"%LIBPATH%\Script Library"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/Style Library" /physicalPath:"%LIBPATH%\Style Library"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/VersionedFiles" /physicalPath:"%LIBPATH%\VersionedFiles"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/VersionedImages" /physicalPath:"%LIBPATH%\VersionedImages"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/ProductImages" /physicalPath:"%LIBPATH%\ProductImages"
%APPCMD% add vdir /app.name:"%APPNAME%/" /path:"/AttributeFileUploadFolder" /physicalPath:"%LIBPATH%\AttributeFileUploadFolder"

GOTO Done

:NOCMS
ECHO CMS Path (%CMSPATH%) does not exist, please check settings and try again
GOTO Done

:NOLIB
ECHO LIbrary Path (%LIBPATH%) does not exist, please check settings and try again
GOTO Done

:Done
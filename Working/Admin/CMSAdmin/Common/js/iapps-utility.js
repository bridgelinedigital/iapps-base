﻿$(function () {
    if (typeof editorContext == "undefined" || editorContext != 'SiteEditor') {
        $.ajaxSetup({ cache: false });

        $(".checkbox-list").each(function () {
            $(this).find("input:checkbox").addClass("checkbox");
            $(this).find("input:checkbox:first").addClass("first-checkbox");
            $(this).find("input:checkbox:last").addClass("last-checkbox");
        });

        $(".textBoxes").each(function () {
            if (typeof $(this).attr("title") != "undefined" && $(this).attr("title") != "")
                $(this).attr("placeholder", $(this).attr("title"));
        });

        $(document).on("keypress", ".iapps-client-grid-search", function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                $(this).find("input.button").click();
                return false;
            }
        });

        $("input.read-only").each(function () {
            $(this).attr("readonly", "readonly");
        });

        SetTextboxDefaultValues();
        SetVerticalScrollGrid();

        $(".validation-summary-valid").each(function () {
            $(this).prepend($("<div class='validation-summary-close' />"));
        });

        $(document).on("click", ".validation-summary-close", function () {
            $(this).parents('.validation-summary-errors').addClass("validation-summary-valid");
        });

        $(document).on("click", ".iapps-validation-summary-close", function () {
            $(this).parents('.iapps-validation-summary').hide();
        });

        InitializeToolTip();

        try {
            $(".iapps-phone-field").mask("(999) 999-9999");
        }
        catch (e) { }
    }

    try {
        $(document).on("click", ".show-loading", function () {
            ShowiAppsLoadingPanel();
        });

        setTimeout(function () {
            // To enable CA combobox filtering 
            $(".comboBox input.combo-editable").prop("disabled", false);
            $(".comboBox input.combo-noneditable").prop("disabled", true);
        }, 100);
    }
    catch (e) { }
});

function InitializeToolTip() {
    setTimeout(function () {
        if (typeof iAPPS_Tooltip != "undefined")
            iAPPS_Tooltip.Initialize('rel', 'title', 'bottom');
    }, 100);
}

function CreateToolTip(tipText) {
    return stringformat("<img class='help-icon' rel='tooltip' src='../App_Themes/General/images/icon-tooltip.gif' title='{0}' />", tipText);
}

function SetTextboxDefaultValues() {
    $(".textBoxes").each(function () {
        if ($(this).attr("title") && $(this).attr("title") != "" &&
                ($(this).val() == "" || $(this).val() == $(this).attr("title"))) {
            $(this).blur();
        }
    });
}

function ValidateSearchInput() {
    var inputValue = $(".search-box input.header-search").val();
    if (typeof inputValue == "undefined")
        inputValue = "";

    if (inputValue == "") {
        alert(__JSMessages["PleaseEnterASearchTerm"]);
        return false;
    }

    return true;
}

function GetRelativeUrl(fullUrl) {
    var relativeUrl = fullUrl.replace(new RegExp(jPublicSiteUrl, "gi"), "");
    if (relativeUrl.charAt(0) != "/" && relativeUrl.indexOf("://") == -1)
        relativeUrl = "/" + relativeUrl;

    if (relativeUrl.indexOf("://") == -1)
        relativeUrl = relativeUrl.replace(new RegExp("//", "gi"), "/");

    return relativeUrl;
}

function SetVerticalScrollGrid() {
    setTimeout(function () {
        $(".tabular-grid").each(function () {
            SetVerticalScrollStyles($(this).prop("id"));
        });
    }, 200);
}

function SetVerticalScrollStyles(gridId) {
    var grid = $("#" + gridId);
    var scrollDiv = $("#" + gridId + "_VerticalScrollDiv");
    if (scrollDiv.length > 0 && !scrollDiv.hasClass("vertical-scroll-section")) {
        scrollDiv.addClass("vertical-scroll-section").css({ "height": grid.css("height") });
        grid.addClass("vertical-scroll-grid");
        var dummyCell = grid.find("td").filter(function () {
            return $(this).css("font-size") == "0px" || $(this).css("width") == "0px";
        });
        dummyCell.addClass("iapps-heading-scroll-cell").css({ "border-left": 0, "padding": "1px" });
        dummyCell.prev().css({ "border-right": 0 });
    }
}

function JumpToTemplatePreview(pageUrl) {
    if (pageUrl.indexOf("?") > -1)
        fullUrl = pageUrl + "&IsTemplatePreview=true&PageState=Preview";
    else
        fullUrl = pageUrl + "?IsTemplatePreview=true&PageState=Preview";

    window.open(fullUrl);
}

function fn_ShowSearchBox(link) {
    var parentLi = $("#" + link.id).parent("li");
    var linkLi = parentLi.find(".search-box");

    if (parentLi.hasClass("selected")) {
        linkLi.slideUp("fast");
        parentLi.removeClass("selected");
    }
    else {
        linkLi.slideDown("fast");
        parentLi.addClass("selected");
    }

    $(document).click(function (e) {
        if (!$(e.target).closest(parentLi).length && !$(e.target).closest(linkLi).length) {
            linkLi.slideUp("fast");
            parentLi.removeClass("selected");
        }
    });
}

function ShowiAppsLoadingPanel() {
    Bridgeline.iapps.components.showLoadingPanel();
}

function CloseiAppsLoadingPanel() {
    Bridgeline.iapps.components.hideLoadingPanel();
}

function OpeniAppsMergeTokens(textFieldId, append) {
    var $this = $("#divInsertMergeToken");
    $this.iAppsDialog('open');
    $this.data("TextField", $("#" + textFieldId));
    $this.data("Append", append);

    return false;
}

function CloseiAppsMergeTokens() {
    var $this = $("#divInsertMergeToken");
    $this.iAppsDialog('close');
    $this.data("TextField", "");

    return false;
}

function InsertiAppsMergeToken() {
    var $this = $("#divInsertMergeToken");
    var textField = $this.data("TextField");
    var append = $this.data("Append");
    var tokenField = $this.find(".token-value");

    var tValue = textField.val();
    if (append && tValue != "")
        textField.val($.trim(tValue) + ' ' + tokenField.val());
    else
        textField.val(tokenField.val());

    CloseiAppsMergeTokens();
    return false;
}

function ShowiAppsNotification(message, success, autoHide) {
    try {
        if ($("#iapps-notification").length > 0) {
            if (typeof success == "undefined")
                success = true;
            if (typeof autoHide == "undefined")
                autoHide = false;

            $("#iapps-notification").iAppsNotification({ success: success, autoHide: autoHide });
            $("#iapps-notification").iAppsNotification("show", message, { focus: true });

            $("#iapps-notification").data("CreatedTime", Math.round(+new Date() / 1000));
        }
        else {
            alert(message);
        }
    } catch (e) {
        alert(message);
    }
}

function CloseiAppsNotification() {
    try {
        var createdTime = $("#iapps-notification").data("CreatedTime");
        var timeNow = Math.round(+new Date() / 1000);
        if (createdTime == undefined)
            createdTime = timeNow - 10;

        if ($("#iapps-notification").length > 0 && (timeNow - createdTime) > 5)
            $("#iapps-notification").iAppsNotification("close");
    } catch (e) {
        console.log(e.message);
    }
}


function OpeniAppsShortcut(value) {
    $("<div />").html(value).iAppsDialog("open", { alertBox: true });
}

function DisableLinks(selector) {
    $(selector).on("click", function (e) {
        e.stopPropagation();
        return false;
    });
}

function HtmlEncode(value) {
    if (value != null)
        return $('<div/>').text(value).html();

    return value;
}

function HtmlDecode(value) {
    if (value != null)
        return $('<div/>').html(value).text();

    return value;
}

function SearchClientGrid(txtBoxId, objGrid) {
    var txtValue = $("#" + txtBoxId).val();
    if (txtValue == $("#" + txtBoxId).attr("title"))
        txtValue = "";
    /*if (txtValue == "" || txtValue == $("#" + txtBoxId).attr("title")) {
    alert(EnterValue2Search);
    $("#" + txtBoxId).focus();
    return false;
    }
    else if (!txtValue.match(/^[a-zA-Z0-9. _']+$/)) {
    alert(InValidSearchTerm);
    $("#" + txtBoxId).focus();
    return false;
    }
    else
    {*/
    objGrid.Search(txtValue, false);
    document.getElementById(objGrid.get_id() + "_dom").style.height = "auto";

    return true;
    //}
}


function OpenIAppsModal(url) {
    dhtmlPopupModal = dhtmlmodal.open('', 'iframe', url, '', '');
}

var EmptyGuid = '00000000-0000-0000-0000-000000000000';
function stringformat(str)            //created by Adams to format the string for localization needs
{
    if (arguments.length == 0)
        return null;

    var str = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
        str = str.replace(re, arguments[i]);
    }
    return (str);
}

// Function to trim
function Trim(s) {
    // Remove leading spaces and carriage returns
    while ((s.substring(0, 1) == ' ') || (s.substring(0, 1) == '\n') || (s.substring(0, 1) == '\r')) {
        s = s.substring(1, s.length);
    }
    // Remove trailing spaces and carriage returns
    while ((s.substring(s.length - 1, s.length) == ' ') || (s.substring(s.length - 1, s.length) == '\n') || (s.substring(s.length - 1, s.length) == '\r')) {
        s = s.substring(0, s.length - 1);
    }
    return s;
}

function CheckLicense(productName, showMessage) {
    var hasLicenese = true;
    var hasPermission = true;
    switch (productName.toLowerCase()) {
        case 'analyzer':
            hasLicenese = hasAnalyticsLicense;
            hasPermission = hasAnalyticsPermission;
            break;
        case 'commerce':
            hasLicenese = hasCommerceLicense;
            hasPermission = hasCommercePermission;
            break;
        case 'marketier':
            hasLicenese = hasMarketierLicense;
            hasPermission = hasMarketierPermission;
            break;
        case 'social':
            hasLicenese = hasSocialLicense;
            hasPermission = hasSocialPermission;
            break;
        case "cms":
            hasLicenese = hasCMSLicense;
            hasPermission = hasCMSPermission;
            break;
    }

    if (!hasLicenese || !hasPermission) {
        if (showMessage)
            OpenLicenseWarningPopup(productName, hasLicenese, hasPermission);
        return false;
    }

    return true;
}

function GetCurrentDomainUrl() {
    var currentDomain = jPublicSiteUrl;

    if (typeof editorContext == "undefined" || editorContext != 'SiteEditor') {
        if (typeof extensionsAdmin != "undefined" && extensionsAdmin == true)
            currentDomain = stringformat("{0}/{1}", jPublicSiteUrl, jCurrentVDName);
        else if (jProductId == jCMSProductId)
            currentDomain = jCMSAdminSiteUrl;
        else if (jProductId == jCommerceProductId)
            currentDomain = jCommerceAdminUrl;
        else if (jProductId == jMarketierProductId)
            currentDomain = jmarketierAdminURL;
        else if (jProductId == jSocialProductId)
            currentDomain = jSocialAdminURL;
        else if (jProductId == jAnalyticsProductId)
            currentDomain = jAnalyticAdminSiteUrl;
    }

    return currentDomain;
}

function GetCurrentProductName() {
    var productName = "";
    if (jProductId == jCMSProductId)
        productName = "cms";
    else if (jProductId == jCommerceProductId)
        productName = "commerce";
    else if (jProductId == jMarketierProductId)
        productName = "marketier";
    else if (jProductId == jSocialProductId)
        productName = "social";
    else if (jProductId == jAnalyticsProductId)
        productName = "analyzer";

    return productName;
}

function GetCurrentVDName() {
    var currentUrl = GetCurrentDomainUrl();

    return currentUrl.substring(currentUrl.lastIndexOf("/") + 1);
}

function RedirectWithToken(productName, mUrl, applicationId, topWindow) {
    ShowiAppsLoadingPanel();

    let url = GetAdminUrlWithAuthToken(productName, mUrl, applicationId, true);

    if (topWindow === true)
        window.top.location = url;
    else
        window.location = url;
}

function RedirectBySiteWithToken(productName, applicationId) {
    ShowiAppsLoadingPanel();
    var mUrl = FWCallback.GetAdminSiteUrl(GetjProductId(productName), applicationId);
    mUrl = mUrl + "/default.aspx";

    window.location = GetAdminUrlWithAuthToken(productName, mUrl, applicationId, true);
}

function GetjProductId(productName) {
    var productId = jCMSProductId;
    if (typeof productName != "undefined"){
        productName = productName.toLowerCase();
        if (productName == "commerce") {
            productId = jCommerceProductId;
        }
        else if (productName == "marketier") {
            productId = jMarketierProductId;
        }
        else if (productName == "analyzer") {
            productId = jAnalyticsProductId;
        }
        else if (productName == "social") {
            productId = jSocialProductId;
        }
    }
    return productId;
}

function GetjAdminUrl(productName) {
    var adminUrl = jCMSAdminSiteUrl;
    productName = productName.toLowerCase();
    if (productName == "commerce") {
        adminUrl = jCommerceAdminUrl;
    }
    else if (productName == "marketier") {
        adminUrl = jmarketierAdminURL;
    }
    else if (productName == "analyzer") {
        adminUrl = jAnalyticAdminSiteUrl;
    }
    else if (productName == "social") {
        adminUrl = jSocialAdminURL;
    }

    return adminUrl;
}

function GetAdminUrlWithAuthToken(productName, mUrl, applicationId, isRedirect) {
    var productId = GetjProductId(productName);
    if (typeof applicationId == "undefined" || applicationId.length != 36)
        applicationId = jAppId;

    if (typeof isRedirect == "undefined" || isRedirect == "")
        isRedirect = false;

    if (typeof jProductId == "undefined" || productId != jProductId ||
        typeof jAppId == "undefined" || applicationId != jAppId) {
        var token = FWCallback.GetAuthToken(juserName, jUserId, productId, applicationId, isRedirect);
        if (token != "") {
            if (mUrl.indexOf("?") > -1)
                mUrl += "&Token=" + token;
            else
                mUrl += "?Token=" + token;
        }
    }

    return mUrl;
}

function GetTreeNodeCount(dataItem) {
    var noOfPages = 0;
    if (typeof dataItem.GetProperty("NoOfPages") != "undefined")
        noOfPages = parseInt(dataItem.GetProperty("NoOfPages"));
    if (typeof dataItem.GetProperty("StorageIndex") != "undefined")
        noOfPages = parseInt(dataItem.GetProperty("StorageIndex"));

    if (noOfPages > 0)
        return stringformat('<span class="tree-node-count">({0})</span>', noOfPages);

    return "";
}

function ResolveClientUrl(relativeUrl) {
    relativeUrl = relativeUrl.toLowerCase();
    var resolvedUrl = relativeUrl;
    if (resolvedUrl.charAt(0) == "~")
        resolvedUrl = resolvedUrl.slice(1);

    if (typeof jCurrentVDName != "undefined") {
        jCurrentVDName = jCurrentVDName.toLowerCase();
        if (resolvedUrl.charAt(0) == "/")
            resolvedUrl = resolvedUrl.slice(1);

        if (!resolvedUrl.startsWith(jCurrentVDName))
            relativeUrl = stringformat("/{0}/{1}", jCurrentVDName, resolvedUrl);
    }

    return relativeUrl;
}

function IsHomeNode(node) {
    return typeof node.GetProperty("HomeNode") != "undefined" && node.GetProperty("HomeNode") != "";
}

function GetTreeNodeIcons(dataItem) {
    var icons = "";
    if (IsHomeNode(dataItem))
        icons += stringformat('<img src="{0}" alt="{1}" />',
            ResolveClientUrl("~/App_Themes/General/images/tree/icon-home.png"), dataItem.GetProperty("HomeNode"));
    if (typeof dataItem.GetProperty("SecuredNode") != "undefined" && dataItem.GetProperty("SecuredNode") != "")
        icons += stringformat('<img src="{0}" alt="{1}" />',
            ResolveClientUrl("~/App_Themes/General/images/tree/icon-lock.png"), dataItem.GetProperty("SecuredNode"));
    if (typeof dataItem.GetProperty("LinkedNode") != "undefined" && dataItem.GetProperty("LinkedNode") != "")
        icons += stringformat('<img src="{0}" alt="{1}" />',
            ResolveClientUrl("~/App_Themes/General/images/tree/icon-linked.png"), dataItem.GetProperty("LinkedNode"));

    if (icons != "")
        icons = stringformat('<span class="tree-node-icons">{0}</span>', icons);

    return icons;
}

function GetClientTemplateHtml(dataItem, dataField) {
    var fieldValue = dataItem.getMember(dataField).get_text();

    return stringformat('<span title="{0}">{1}</span>', fieldValue.indexOf("<") == -1 ? fieldValue : "", fieldValue);
}

function GetGridItemRowNumber(objGrid, dataItem) {
    return gridActionsJson.PageSize * (gridActionsJson.PageNumber - 1) + dataItem.Index + 1;
}

function GetGridLoadingPanelContent(objGrid) {
    var height = 50;
    var width = 100;
    if (objGrid != "") {
        var gridDom = document.getElementById(objGrid.get_id() + "_dom");
        if (gridDom) {
            if (gridDom.offsetHeight > 0)
                height = gridDom.offsetHeight;
            if (gridDom.offsetWidth > 0)
                width = gridDom.offsetWidth;
        }
        var scrollBox = $("#" + objGrid.get_id()).parent(".scroll-box");
        if (scrollBox.length > 0)
            height = scrollBox.height();
        else {
            scrollBox = $("#" + objGrid.get_id()).parent(".scroll-section");
            if (scrollBox.length > 0)
                height = scrollBox.height();
        }
    }
    var html = "<table style='width:100%;'><tr><td class='grid-loading' style='height:{0}px;width:{1}px;'><div class='loading-spinner'><img src='{2}' border='0' alt='Loading...' /></div></td></tr></table>";

    return stringformat(html, height, width, jSpinnerUrl);
}

function GetTreeLoadingPanelContent(objTree) {
    var height = 100;
    var width = 100;
    if (objTree != "") {
        var treeDom = document.getElementById(objTree.get_id());
        if (treeDom) {
            if (treeDom.offsetHeight > 0)
                height = treeDom.offsetHeight;
            if (treeDom.offsetWidth > 0)
                width = treeDom.offsetWidth;
        }
    }
    var html = "<table><tr><td class='tree-loading' style='height:{0}px;width:{1}px;'><div class='loading-spinner'><img src='{2}' border='0' alt='Loading...' /></div></td></tr></table>";

    return stringformat(html, height, width, jSpinnerUrl);
}

function IsMarketierDirectory(node) {
    return node != null && typeof node.getProperty("IsMarketierDir") != "undefined" &&
        node.getProperty("IsMarketierDir").toLowerCase() == "true";
}

function GetGridDragTemplate(title) {
    return "<div class='drag-template'>" + title + "</div>";
}

function GetGridDragTemplateById(objGrid, dragItem) {
    var objectId = dragItem.Key;
    var gridId = objGrid.get_id();

    if (typeof objectId != "undefined") {
        var $row = $("#" + gridId + " .grid-item[objectId='" + objectId + "']");

        if (!$row.hasClass("disable-move"))
            return "<div class='drag-template'>" + $row.parent().html() + "</div>";
        else
            return "";
    }
    else
        return "<div class='drag-template'>" + dragItem.getMember("Title").get_value() + "</div>";
}

function FormatGridHtml(gridHtml) {
    if (gridHtml == null)
        return "";

    return gridHtml.replace(new RegExp("#%cLt#%", "g"), "<");
}

function SetTaxonomyForList() {
    $(".view-tags").iAppsClickMenu({
        width: "150px",
        heading: __JSMessages["Tags"],
        enableLoadingPanel: true,
        create: function (e, menu) {
            var taxonomyNames = JSON.parse(GetTaxonomyJson(e.attr("objectId")));
            if (taxonomyNames.length > 0) {
                $.each(taxonomyNames, function () {
                    menu.addListItem(this.toString());
                });
            }
            else {
                menu.addListItem(__JSMessages["NoTagsAttached"]);
            }
        }
    });
}

function GetQueryStringValue(key) {
    var re = new RegExp('(?:\\?|&)' + key + '=(.*?)(?=&|$)', 'gi');
    var r = [], m;
    while ((m = re.exec(document.location.search)) != null) r.push(m[1]);
    return r;
}

function OpenResetCachePopup() {
    OpeniAppsAdminPopup("ResetCachePopup", "", "CloseResetCachePopup");

    return false;
}

function CloseResetCachePopup() {
    window.location = window.location;
}

function OpenCreateEmailPopup(querystring) {
    OpeniAppsMarketierPopup("ChooseTemplate", querystring, "CloseCreateEmailPopup");

    return false;
}

function CloseCreateEmailPopup() {
    JumpToEmailFrontPage(popupActionsJson.CustomAttributes["PageUrl"]);
}

//script taken from http://weblogs.asp.net/cprieto/archive/2010/01/03/handling-timezone-information-in-asp-net.aspx 
function setCookie(cookieName, cookieValue, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = cookieName + "=" + escape(cookieValue) + ((expiredays == null) ? "" : ";expires=" + exdate.toUTCString());
}

function getCookie(cookieName) {
    if (document.cookie.length > 0) {
        cookieStart = document.cookie.indexOf(cookieName + "=");
        if (cookieStart != -1) {
            cookieStart = cookieStart + cookieName.length + 1;
            cookieEnd = document.cookie.indexOf(";", cookieStart);
            if (cookieEnd == -1)
                cookieEnd = document.cookie.length;
            return unescape(document.cookie.substring(cookieStart, cookieEnd));
        }
    }
    return "";
}

function getUtcOffset() {
    return (new Date()).getTimezoneOffset();
}

function checkCookie() {
    var timeOffset = getCookie("TimeZoneOffset");
    if (timeOffset == null || timeOffset == "") {
        setCookie("TimeZoneOffset", getUtcOffset(), 1);
        //window.location.reload(); 
    }
}

function GetCookie(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return getCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return null;
}

function SetCookie(name, value) {
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape(value) +
((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
((path == null) ? "" : ("; path=" + path)) +
((domain == null) ? "" : ("; domain=" + domain)) +
((secure == true) ? "; secure" : "");
}

function getCookieVal(offset) {
    var endstr = document.cookie.indexOf(";", offset);
    if (endstr == -1)
        endstr = document.cookie.length;
    return unescape(document.cookie.substring(offset, endstr));
}

function DeleteCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1); // This cookie is history
    var cval = GetCookie(name);
    document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}

function validKey(e) {
    var key;
    var keychar;
    var reg;

    if (window.event) {
        // for IE, e.keyCode or window.event.keyCode can be used
        key = e.keyCode;
    }
    else if (e.which) {
        // netscape
        key = e.which;
    }
    else {
        // no event, so pass through
        return true;
    }
    if (key > 31 || key == 8 || key == 16) {
        return true;
    }
    else {
        return false;
    }
}

//moved these methods from RadWindow.js
function GetDialogArguments() {
    var clientParameters = getRadWindow().ClientParameters; //return the arguments supplied from the parent page
    return clientParameters;
}
//moved these methods from RadWindow.js
function getRadWindow() {
    if (window.radWindow) {
        return window.radWindow;
    }
    if (window.frameElement && window.frameElement.radWindow) {
        return window.frameElement.radWindow;
    }
    return null;
}

function ChangeSpecialCharacters(str) {
    var replacedString = str;
    if (replacedString != null) {
        replacedString = replacedString.replace(new RegExp("&", "g"), "&amp;");
        replacedString = replacedString.replace(new RegExp("<", "g"), "&lt;");
        replacedString = replacedString.replace(new RegExp(">", "g"), "&gt;");
        replacedString = replacedString.replace(new RegExp("\"", "g"), "&quot;");
        replacedString = replacedString.replace(new RegExp("'", "g"), "\\'");
        replacedString = replacedString.replace(new RegExp("\n", "g"), " ");
    }
    return replacedString;
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
};

function NewGuid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
}

var dtCh = "/";
var minYear = 1900;
var maxYear = 2100;

function isInteger(s) {
    var i;
    for (i = 0; i < s.length; i++) {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag) {
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary(year) {
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
}
function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31
        if (i == 4 || i == 6 || i == 9 || i == 11) { this[i] = 30 }
        if (i == 2) { this[i] = 29 }
    }
    return this
}

function isDate(dtStr) {
    var daysInMonth = DaysArray(12)
    var pos1 = dtStr.indexOf(dtCh)
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
    var strMonth = dtStr.substring(0, pos1)
    var strDay = dtStr.substring(pos1 + 1, pos2)
    var strYear = dtStr.substring(pos2 + 1)
    strYr = strYear
    if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
    if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
    }
    month = parseInt(strMonth)
    day = parseInt(strDay)
    year = parseInt(strYr)
    if (pos1 == -1 || pos2 == -1) {
        alert(__JSMessages["TheDateFormatShouldBemmddyyyy"])
        return false
    }
    if (strMonth.length < 1 || month < 1 || month > 12) {
        alert(__JSMessages["PleaseEnterAValidMonth"])
        return false
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        alert(__JSMessages["PleaseEnterAValidDay"])
        return false
    }
    if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        alert(stringformat(__JSMessages["PleaseEnterAValid4DigitYearBetween0And1"], minYear, maxYear))
        return false
    }
    if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        alert(__JSMessages["PleaseEnterAValidDate"])
        return false
    }
    return true
}
//credit: http://jquery-howto.blogspot.ie/2009/09/get-url-parameters-values-with-jquery.html
function getQueryStrings() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function replacer(key, value) {
    if (key == "Parent" || value == null) return;
    else return value;
}

/**
 * This is required to fix a bug in MicrosoftAjaxWebForms.js
 * in Firefox where if window.event is not initialized, it loops stack
 * via arguments.callee.caller chain and breaks because of the
 * "use strict" mode
 *
 * Hacking window.event property is required because it is
 * not settable in Internet Explorer

 * https://mnaoumov.wordpress.com/2016/02/12/wtf-microsoftajax-js-vs-use-strict-vs-firefox-vs-ie/
 */
function hackEventWithinDoPostBack() {
    if (navigator.userAgent.indexOf('Firefox') > -1) {
        var originalEventDescriptor = Object.getOwnPropertyDescriptor(Window.prototype, "event");
        var hackEventVariable = false;
        var eventPropertyHolder;
        Object.defineProperty(window, "event", {
            configurable: true,
            get: function get() {
                var result = originalEventDescriptor ? originalEventDescriptor.get.apply(this, arguments) : eventPropertyHolder;
                if (result || !hackEventVariable)
                    return result;
                return {};
            },
            set: function set(value) {
                if (originalEventDescriptor)
                    originalEventDescriptor.set.apply(this, arguments);
                else
                    eventPropertyHolder = value;
            }
        });

        var originalDoPostBack = window.__doPostBack;

        window.__doPostBack = function hackedDoPostBack() {
            hackEventVariable = true;
            originalDoPostBack.apply(this, arguments);
            hackEventVariable = false;
        };
    }
}

//generic validation

function checkFields(form, fldName, fldMsg) {
    var fldRef, fldType;

    for (y = 0; y < fldName.length; y++) {
        if (fldName[y] != "") {
            fldRef = form.elements[fldName[y]];

            if (fldRef) {
                //check for text or textarea field
                if (fldRef.type == "text" || fldRef.type == "textarea") {
                    if (fldRef.value == "") {
                        alert(stringformat(__JSMessages["PleaseEnter0"], fldMsg[y]));
                        fldRef.focus();
                        return false;
                    } else if ((fldRef.name == "email") && (!isEmail(fldRef.value))) {
                        alert(__JSMessages["InvalidEmailAddress"]);
                        fldRef.focus();
                        return false;
                    }
                }
                //check for select-one or select-multiple field
                else if ((fldRef.type == "select-one") || (fldRef.type == "select-multiple")) {
                    if (fldRef.options[fldRef.selectedIndex].value == "") {
                        alert(stringformat(__JSMessages["PleaseSelect0"], fldMsg[y]));
                        fldRef.focus();
                        return false;
                    }
                }
                //check for radio buttons
                else if (fldRef[0].type == "radio") {
                    var optChecked = false;
                    for (i = 0; i < fldRef.length; i++) {
                        if (fldRef[i].checked) {
                            optChecked = true;
                        }
                    }
                    if (!optChecked) {
                        alert(stringformat(__JSMessages["PleaseSpecify0"], fldMsg[y]));
                        fldRef[0].focus();
                        return false;
                    }
                }
                //check for password field
                if (fldRef.type == "password") {
                    if (fldRef.value == "") {
                        alert(stringformat(__JSMessages["PleaseEnter0"], fldMsg[y]));
                        fldRef.focus();
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

/////////////////////////////////////////////////////////////////////////
// BEGIN date validation functions
/////////////////////////////////////////////////////////////////////////
function validate_date(date_field, desc) {
    if (!date_field.value)
        return true;
    var in_date = stripCharString(date_field.value, " ");
    in_date = in_date.toUpperCase();
    var date_is_bad = 0;
    // if (!allowInString(in_date,"/0123456789T+-"))
    if (!allowInString(in_date, "/0123456789"))
        date_is_bad = 1; // invalid characters in date
    if (!date_is_bad) {
        var has_rdi = 0;
        if (in_date.indexOf("T") >= 0) {
            has_rdi = 1;
        }
        if (!date_is_bad && has_rdi && (in_date.indexOf("T") != 0)) {
            date_is_bad = 2; // relative date index character is not in first position
        }
        if (!date_is_bad && has_rdi && (in_date.length == 1)) {
            var d = new Date();
            var return_month = parseInt(d.getMonth() + 1).toString();
            //return_month = (return_month.length==1 ? "0" : "") + return_month; 
            var return_date = parseInt(d.getDate()).toString();
            //return_date = (return_date.length==1 ? "0" : "") + return_date; 
            in_date = return_month + "/" + return_date + "/" + get_full_year(d);
            has_rdi = 0; // date doesn't have rdi char anymore (will also cause failure of add'l rdi checks, which is a good thing)
        }
        if (!date_is_bad && has_rdi && (in_date.length > 1) && !(in_date.charAt(1) == "+" || in_date.charAt(1) == "-")) {
            date_is_bad = 3; // length of rdi string is greater than 1 but second char is not "+" or "-"
        }
        if (!date_is_bad && has_rdi && isNaN(parseInt(in_date.substring(2, in_date.length), 10))) {
            date_is_bad = 4; // rdi value is not a number
        }
        if (!date_is_bad && has_rdi && (parseInt(in_date.substring(2, in_date.length), 10) < 0)) {
            date_is_bad = 5; // rdi value is not a positive integer
        }
        if (!date_is_bad && has_rdi) {
            var d = new Date();
            ms = d.getTime();
            offset = parseInt(in_date.substring(2, in_date.length), 10);
            if (in_date.charAt(1) == "+") {
                ms += (86400000 * offset);
            } else {
                ms -= (86400000 * offset);
            }
            d.setTime(ms);
            var return_month = parseInt(d.getMonth() + 1).toString();
            return_month = (return_month.length == 1 ? "0" : "") + return_month;
            var return_date = parseInt(d.getDate()).toString();
            return_date = (return_date.length == 1 ? "0" : "") + return_date;
            in_date = return_month + "/" + return_date + "/" + get_full_year(d);
            has_rdi = 0;
        }
    }
    if (!date_is_bad) {
        var date_pieces = new Array();
        date_pieces = in_date.split("/");
        if (date_pieces.length == 2) {
            var d = new Date();
            in_date = in_date + "/" + get_full_year(d);
            date_pieces = in_date.split("/");
        }
        if (date_pieces.length != 3 || parseInt(date_pieces[0], 10) < 1 || parseInt(date_pieces[0], 10) > 12
            || parseInt(date_pieces[1], 10) < 1 || parseInt(date_pieces[1], 10) > 31
            || (date_pieces[2].length != 2 && date_pieces[2].length != 4)) {
            date_is_bad = 6;  // date is not in format of m[m]/d[d]/yy[yy]
        }
    }
    if (date_is_bad) {
        alert(stringformat(__JSMessages["MustBeInTheFormatOfmmddyyyy"], desc));
        date_field.focus();
        return (false);
    }

    var ms = Date.parse(in_date);
    var d = new Date();
    d.setTime(ms);
    var return_date = d.toLocaleString();
    var return_month = parseInt(d.getMonth() + 1).toString();
    //return_month = (return_month.length==1 ? "0" : "") + return_month; 
    var return_date = parseInt(d.getDate()).toString();
    //return_date = (return_date.length==1 ? "0" : "") + return_date; 
    return_date = return_month + "/" + return_date + "/" + get_full_year(d);
    date_field.value = return_date;
    return true;
}       // normalize the year to yyyy

function get_full_year(d) {
    var y = "";
    var z = "";
    if (d.getFullYear() != null) {
        y = d.getFullYear();
        if (y < 2002) y = 2002;
    } else {
        y = d.getYear();
        if (y > 0 && y < 100) y += 2000;
        if (y < 1000) y += 2000;
    }
    z = y.toString();
    //return z.substring(2);
    return z;
}

function stripCharString(InString, CharString) {
    var OutString = "";
    for (var Count = 0; Count < InString.length; Count++) {
        var TempChar = InString.substring(Count, Count + 1);
        var Strip = false;
        for (var Countx = 0; Countx < CharString.length; Countx++) {
            var StripThis = CharString.substring(Countx, Countx + 1)
            if (TempChar == StripThis) {
                Strip = true;
                break;
            }
        }
        if (!Strip)
            OutString = OutString + TempChar;
    }
    return (OutString);
}

function allowInString(InString, RefString) {
    if (InString.length == 0) return (false);
    for (var Count = 0; Count < InString.length; Count++) {
        var TempChar = InString.substring(Count, Count + 1);
        if (RefString.indexOf(TempChar, 0) == -1)
            return (false);
    }
    return (true);
}
/////////////////////////////////////////////////////////////////////////
// END date validation functions
/////////////////////////////////////////////////////////////////////////

// format date onKeyUp - fills in date chars as user types
function dateFormat(obj, dateFldVal) {
    //base year defaults to current year
    //use current year in default publish date
    var retDate = new Date();
    var retYear = retDate.getYear();

    //for archive date, add 10 years to publish date
    //user may change publish date, so it might not be default date
    if ((obj.name == "archivedate") && (dateFldVal != "")) {
        var pubDate = new Date(dateFldVal);
        var pubYear = pubDate.getYear();
        retYear = pubYear + 10;
    }

    if ((obj.value.length == 1) && (obj.value > 1)) {
        obj.value = obj.value + '/';
    }
    else if ((obj.value.length == 2) && (obj.value.charAt(1) != "/")) {
        obj.value = obj.value + '/';
    }
    else if ((obj.value.length == 3) && (obj.value.charAt(2) > 3)) {
        obj.value = obj.value + '/' + retYear;
    }
    else if (obj.value.length == 4) {
        if ((obj.value.charAt(3) != "/") && (obj.value.charAt(3) > 3)) {
            obj.value = obj.value + '/' + retYear;
        }
        else if ((obj.value.charAt(1) == "/") && (obj.value.charAt(3) != "/")) {
            obj.value = obj.value + '/' + retYear;
        }
        else if ((obj.value.charAt(1) == "/") && (obj.value.charAt(3) == "/")) {
            obj.value = obj.value + retYear;
        }
    }
    else if (obj.value.length == 5) {
        if (obj.value.charAt(4) != "/") {
            obj.value = obj.value + '/' + retYear;
        }
        else {
            obj.value = obj.value + retYear;
        }
    }
}

function isEmail(string) {
    if (string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
        return true;
    else
        return false;
}

function checkEmailFields(form, fldName, fldMsg) {
    var fldRef, fldType;
    for (y = 0; y < fldName.length; y++) {
        if (fldName[y] != "") {
            fldRef = form.elements[fldName[y]];
            if (fldRef.value == "") {
                alert(stringformat(__JSMessages["PleaseEnter0"], fldMsg[y]));
                fldRef.focus();
                return false;
            } else {
                if (fldRef.value.length < 5) {
                    alert(stringformat(__JSMessages["PleaseEnterValid0"], fldMsg[y]));
                    fldRef.focus();
                    return false;
                } else {
                    i = fldRef.value.indexOf('.');
                    j = fldRef.value.indexOf('@');
                    if (i < 0 || j < 0) {
                        alert(stringformat(__JSMessages["PleaseEnterValid0"], fldMsg[y]));
                        fldRef.focus();
                        return false;
                    }
                }
            }
        }
    }
    return true;
}




/////////////////////////////////////////////////////////////////////////
// BEGIN date validation functions without alert message - Maheswari May 22 2007
/////////////////////////////////////////////////////////////////////////
function validate_dateSummary(date_field) {
    if (!date_field.value)
        return true;

    var myDateFormatted = date_field.value;

    var in_date = stripCharString(myDateFormatted, " ");
    in_date = in_date.toUpperCase();
    var date_is_bad = 0;
    // if (!allowInString(in_date,"/0123456789T+-"))
    if (!allowInString(in_date, "/0123456789"))
        date_is_bad = 1; // invalid characters in date
    if (!date_is_bad) {
        var has_rdi = 0;
        if (in_date.indexOf("T") >= 0) {
            has_rdi = 1;
        }
        if (!date_is_bad && has_rdi && (in_date.indexOf("T") != 0)) {
            date_is_bad = 2; // relative date index character is not in first position
        }
        if (!date_is_bad && has_rdi && (in_date.length == 1)) {
            var d = new Date();
            var return_month = parseInt(d.getMonth() + 1).toString();
            //return_month = (return_month.length==1 ? "0" : "") + return_month; 
            var return_date = parseInt(d.getDate()).toString();
            //return_date = (return_date.length==1 ? "0" : "") + return_date; 
            in_date = return_month + "/" + return_date + "/" + get_full_year(d);
            has_rdi = 0; // date doesn't have rdi char anymore (will also cause failure of add'l rdi checks, which is a good thing)
        }
        if (!date_is_bad && has_rdi && (in_date.length > 1) && !(in_date.charAt(1) == "+" || in_date.charAt(1) == "-")) {
            date_is_bad = 3; // length of rdi string is greater than 1 but second char is not "+" or "-"
        }
        if (!date_is_bad && has_rdi && isNaN(parseInt(in_date.substring(2, in_date.length), 10))) {
            date_is_bad = 4; // rdi value is not a number
        }
        if (!date_is_bad && has_rdi && (parseInt(in_date.substring(2, in_date.length), 10) < 0)) {
            date_is_bad = 5; // rdi value is not a positive integer
        }
        if (!date_is_bad && has_rdi) {
            var d = new Date();
            ms = d.getTime();
            offset = parseInt(in_date.substring(2, in_date.length), 10);
            if (in_date.charAt(1) == "+") {
                ms += (86400000 * offset);
            } else {
                ms -= (86400000 * offset);
            }
            d.setTime(ms);
            var return_month = parseInt(d.getMonth() + 1).toString();
            return_month = (return_month.length == 1 ? "0" : "") + return_month;
            var return_date = parseInt(d.getDate()).toString();
            return_date = (return_date.length == 1 ? "0" : "") + return_date;
            in_date = return_month + "/" + return_date + "/" + get_full_year(d);
            has_rdi = 0;
        }
    }
    if (!date_is_bad) {
        var date_pieces = new Array();
        date_pieces = in_date.split("/");
        if (date_pieces.length == 2) {
            var d = new Date();
            in_date = in_date + "/" + get_full_year(d);
            date_pieces = in_date.split("/");
        }
        if (date_pieces.length != 3 || parseInt(date_pieces[0], 10) < 1 || parseInt(date_pieces[0], 10) > 12
            || parseInt(date_pieces[1], 10) < 1 || parseInt(date_pieces[1], 10) > 31
            || (date_pieces[2].length != 2 && date_pieces[2].length != 4)) {
            date_is_bad = 6;  // date is not in format of m[m]/d[d]/yy[yy]
        }
    }
    if (date_is_bad) {
        // date_field.focus();
        return (false);
    }

    var ms = Date.parse(in_date);
    var d = new Date();
    d.setTime(ms);
    var return_date = d.toLocaleString();
    var return_month = parseInt(d.getMonth() + 1).toString();
    //return_month = (return_month.length==1 ? "0" : "") + return_month; 
    var return_date = parseInt(d.getDate()).toString();
    //return_date = (return_date.length==1 ? "0" : "") + return_date; 
    return_date = return_month + "/" + return_date + "/" + get_full_year(d);
    //date_field.value = return_date;
    return true;
}       // normalize the year to yyyy
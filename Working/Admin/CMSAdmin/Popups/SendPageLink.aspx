<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SendPageLink" StylesheetTheme="General"
    CodeBehind="SendPageLink.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master"
    ClientIDMode="Static" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ValidateSelectedList() {
            if (selectedUsersCount == 0) {
                //If nothing is selected please give message
                return false;
            }
            return true;
        }
        //we need one more checkbox for select all
        var selectedUsersCount = 0;
        function AtLeastOneUserIsSelected(sender, args) {
            var boxes = $('#gridview input[type=checkbox]');
            selectedUsersCount = 0;
            boxes.each(function () {
                if ($(this).is(":checked")) {
                    selectedUsersCount++;
                }
            });
            //alert(selectedUsersCount);
            //alert($('#gridview input[type=checkbox]:checked').length); //this will work

            if (selectedUsersCount > 0) {
                args.IsValid = true;
                return;
            }
            args.IsValid = false;

        }
        function updateSelectedUserCount(checkbox) {
            if (!checkbox.checked) {
                $('#chkAllUser').prop('checked', false);

                if (selectedUsersCount > 0)
                    selectedUsersCount--;
                else
                    selectedUsersCount = 0;
            }
            else {
                selectedUsersCount++;
            }
        }
        function SelectAllUser() {
            $('#gridview tbody input[type=checkbox]').prop('checked', $('#chkAllUser').prop('checked'));
        }
        $(document).ready(function () {
            var chkCount = $('#gridview tbody input[type=checkbox]').length;
            if (chkCount == 0)
                $('#chkAllUser').parent().hide();
        });
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ObjectDataSource ID="odsUserList" runat="server" EnablePaging="false" EnableViewState="true"
        EnableCaching="false" TypeName="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SendPageLink" SelectMethod="GetUserList"></asp:ObjectDataSource>
    <asp:ListView ID="lstUserList" runat="server" ItemPlaceholderID="userListContainer"
        DataKeyNames="UserId" DataSourceID="odsUserList">
        <LayoutTemplate>
            <table width="100%" cellpadding="0" cellspacing="0" class="grid" id="gridview">
                <thead>
                    <tr>
                        <th runat="server" id="headerSelect" style="text-align:center;">
                            <asp:CheckBox ID="chkAllUser" runat="server" onclick="SelectAllUser()" />
                        </th>
                        <th id="tdHdFirstName">
                            <asp:Localize ID="lczLastName" runat="server" Text="<%$ Resources:GUIStrings, LastName %>" />
                        </th>
                        <th id="tdHdLastName">
                            <asp:Localize ID="lczFirstName" runat="server" Text="<%$ Resources:GUIStrings, FirstName %>" />
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder ID="userListContainer" runat="server" />
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr class='<%# Container.DataItemIndex % 2 == 0 ? "odd-row" : "even-row" %>'>
                <td runat="server" id="tdCheckbox" style="text-align:center;">
                    <asp:CheckBox ID="chkUser" runat="server" onclick="updateSelectedUserCount(this)" />
                    <asp:HiddenField ID="hdnUserId" runat="server" Value='<%# Eval("UserId")  %>' />
                    <asp:HiddenField ID="hdnEmail" runat="server" Value='<%# Eval("Email")  %>' />
                </td>
                <td>
                    <asp:Literal ID="ltLastName" runat="server" Text='<%# Eval("LastName") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltFirstName" runat="server" Text='<%# Eval("FirstName") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            <div class="grid-emptyText">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, NoUserToDisplay %>" />
            </div>
        </EmptyDataTemplate>
    </asp:ListView>
    <asp:CustomValidator ID="cvAtLeastOneUser" runat="server" Display="None" ErrorMessage="<%$ Resources:GUIStrings, User_AtleastOneUserShouldBeSelected %>"
        ValidationGroup="vgUserSelect" ClientValidationFunction="AtLeastOneUserIsSelected"></asp:CustomValidator>
    <asp:ValidationSummary ID="vUserSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
        ValidationGroup="vgUserSelect" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSendMail" runat="server" Text="<%$ Resources:GUIStrings, SendMail %>"
        ToolTip="<%$ Resources:GUIStrings, SendMail %>" CssClass="primarybutton" ValidationGroup="vgUserSelect" />
</asp:Content>

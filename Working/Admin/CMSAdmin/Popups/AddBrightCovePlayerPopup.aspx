﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master"
    CodeBehind="AddBrightCovePlayerPopup.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.AddBrightCovePlayerPopup"
    StylesheetTheme="General" ClientIDMode="Static"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.PlayerName %></label>
        <div class="form-value">
            <asp:TextBox ID="txtPlayerName" runat="server" CssClass="textBoxes" Width="200" />
            <asp:RequiredFieldValidator ID="reqtxtPlayerName" ControlToValidate="txtPlayerName" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, PlayerNameIsRequired %>" Display="None" SetFocusOnError="true" />
            <asp:RegularExpressionValidator ID="regtxtPlayerName" runat="server" ControlToValidate="txtPlayerName"
                ErrorMessage="<%$ Resources:JSMessages, PlayerNameIsNotValid %>" Display="None" SetFocusOnError="true"
                ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.PlayerId %></label>
        <div class="form-value">
            <asp:TextBox ID="txtPlayerId" runat="server" CssClass="textBoxes" Width="200" />
            <asp:RequiredFieldValidator ID="reqtxtPlayerId" ControlToValidate="txtPlayerId" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, PlayerIdIsRequired %>" Display="None" SetFocusOnError="true" />
            <asp:RegularExpressionValidator ID="regtxtPlayerId" runat="server" ControlToValidate="txtPlayerId"
                ErrorMessage="<%$ Resources:JSMessages, PlayerIdIsNotValid %>" Display="None" SetFocusOnError="true"
                ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.PlayerKey %></label>
        <div class="form-value">
            <asp:TextBox ID="txtPlayerKey" runat="server" CssClass="textBoxes" Width="200" />
            <asp:RequiredFieldValidator ID="reqtxtPlayerKey" ControlToValidate="txtPlayerKey" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, PlayerKeyIsRequired %>" Display="None" SetFocusOnError="true" />
            <asp:RegularExpressionValidator ID="regtxtPlayerKey" runat="server" ControlToValidate="txtPlayerKey"
                ErrorMessage="<%$ Resources:JSMessages, PlayerKeyIsNotValid %>" Display="None" SetFocusOnError="true"
                ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= Bridgeline.iAPPS.Resources.SiteSettings.PlayerMinimumHeight %></label>
        <div class="form-value">
            <asp:TextBox ID="txtPlayerMinimumHeight" runat="server" CssClass="textBoxes" Width="200" />
            <asp:RequiredFieldValidator ID="reqtxtPlayerMinimumHeight" ControlToValidate="txtPlayerMinimumHeight" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, PlayerMinimumHeightIsRequired %>" Display="None" SetFocusOnError="true" />
            <asp:CompareValidator CultureInvariantValues="true" runat="server" ID="regCmptxtPlayerMinimumHeight"
                Display="None" ControlToValidate="txtPlayerMinimumHeight" Text="*" ErrorMessage="<%$ Resources:JSMessages, PlayerMinimumHeightIsNotValid %>"
                Type="Integer" Operator="DataTypeCheck" ></asp:CompareValidator>
            <asp:CompareValidator ID="regCmpValtxtPlayerMinimumHeight" runat="server" ControlToValidate="txtPlayerMinimumHeight"
                ValueToCompare="0" Operator="GreaterThan" Type="Integer" CultureInvariantValues="true"
                ErrorMessage="<%$ Resources:JSMessages, PlayerMinimumHeightShouldBeGreaterThanZero %>"
                Display="None" Text="">
            </asp:CompareValidator>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= Bridgeline.iAPPS.Resources.SiteSettings.PlayerMinimumWidth %></label>
        <div class="form-value">
            <asp:TextBox ID="txtPlayerMinimumWidth" runat="server" CssClass="textBoxes" Width="200" />
            <asp:RequiredFieldValidator ID="reqtxtPlayerMinimumWidth" ControlToValidate="txtPlayerMinimumWidth" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, PlayerMinimumWidthIsRequired %>" Display="None" SetFocusOnError="true" />
            <asp:CompareValidator CultureInvariantValues="true" runat="server" ID="regCmptxtPlayerMinimumWidth"
                Display="None" ControlToValidate="txtPlayerMinimumWidth" Text="*" ErrorMessage="<%$ Resources:JSMessages, PlayerMinimumWidthIsNotValid %>"
                Type="Integer" Operator="DataTypeCheck" ></asp:CompareValidator>
            <asp:CompareValidator ID="regCmpValtxtPlayerMinimumWidth" runat="server" ControlToValidate="txtPlayerMinimumWidth"
                ValueToCompare="0" Operator="GreaterThan" Type="Integer" CultureInvariantValues="true"
                ErrorMessage="<%$ Resources:JSMessages, PlayerMinimumWidthShouldBeGreaterThanZero %>"
                Display="None" Text="">
            </asp:CompareValidator>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            &nbsp;</label>
        <div class="form-value">
            <asp:CheckBox ID="chkIsPlayList" runat="server" Text="<%$ Resources:GUIStrings, IsItPlayListPlayer %>" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        cssclass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        cssclass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>"/>
</asp:Content>

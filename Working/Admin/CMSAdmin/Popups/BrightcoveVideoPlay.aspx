﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="BrightcoveVideoPlay.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.BrightcoveVideoPlay"
    StylesheetTheme="General" ClientIDMode="Static" %>

<%@ Register Src="../UserControls/BrigthcoveVideoPlayer.ascx" TagName="BrigthcoveVideoPlayer"
    TagPrefix="uc1" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
<script type="text/javascript" >
    function CloseVideoPlayPopup() {
        parent.popupPreviewVideoPopup.hide();
    }
</script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div>
        <uc1:BrigthcoveVideoPlayer ID="BrigthcoveVideoPlayer1" runat="server" VideoId="0"
            IsVideo="false" PlayerId="0" PlayerKey="A" Height="400" Width="400" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnClose" value="<%= GUIStrings.Close %>" onclick="CloseVideoPlayPopup();" class="button" />
</asp:Content>

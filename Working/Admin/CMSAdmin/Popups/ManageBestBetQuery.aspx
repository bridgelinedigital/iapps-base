﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" StylesheetTheme="General"
    CodeBehind="ManageBestBetQuery.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageBestBetQuery" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var targetLibrary = '';
        var curTextboxId = '';
        $(function () {
            fn_TogglePopupHandle();
        });

        function fn_TogglePopupHandle() {
            $('.link-type').each(function (index) {
                if ($(this).val() == 'ExternalURL') {
                    $(this).siblings('.popup-handle').hide();
                }
                else {
                    $(this).siblings('.popup-handle').show();
                }
            });
        }

        function ShowLibrary(objDropdownId, objTextBoxId) {
            var popup = $('#' + objDropdownId).val();
            curTextboxId = objTextBoxId;
            if (popup == "PageLibrary") {
                OpeniAppsAdminPopup("SelectPageLibraryPopup", "", "SelectPage");
            }
            else if (popup == "FileLibrary") {
                OpeniAppsAdminPopup("SelectFileLibraryPopup", "", "SelectFile");
            }
            else if (popup = "CommerceProduct") {
                OpeniAppsCommercePopup("SelectProductPopup", "showProducts=true","SelectProductFromPopup");
            }
        }

        function SelectPage() {
            selectedId = popupActionsJson.SelectedItems.first();
            var pageUrl = popupActionsJson.CustomAttributes["SelectedPageUrl"];
            $('#' + curTextboxId).val(pageUrl);
        }

        function SelectFile() {
            selectedId = popupActionsJson.SelectedItems.first();
            var fileUrl = popupActionsJson.CustomAttributes["RelativePath"] + "/" + popupActionsJson.CustomAttributes["FileName"];
            $('#' + curTextboxId).val(fileUrl);
        }

        function Validate() {
            if (Page_ClientValidate('Query')) {
                return true;
            }
            else {
                return false;
            }
        }

        function SelectProductFromPopup(product) {         
            if (product != null && product != '' && product != 'undefined') {                             
                $('#' + curTextboxId).val(jPublicSiteUrl + "/" + product.Url);
                selectedId = product.Id;              
            }
            CloseiAppsCommercePopup();
        }

        function CloseProductPopupFromPopup() {
            CloseiAppsCommercePopup();
        }
    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.Query %></label>
        <div class="form-value">
            <asp:TextBox ID="txtQuery" runat="server" Width="400" ValidationGroup="Query"/>
            <asp:RequiredFieldValidator runat="server" ID="reqtxtQuery" ControlToValidate="txtQuery"
                ValidationGroup="Query" SetFocusOnError="true" ErrorMessage="<%$ Resources:JSMessages, QueryIsRequired %>" Display="None"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regtxtQuery" runat="server" ControlToValidate="txtQuery"
                ErrorMessage="<%$ Resources:JSMessages, BestBetInvalidCharacters %>" Display="None" SetFocusOnError="true"
               ValidationExpression="^[A-Za-z0-9-_,'’.!()/&\s&quot;]+$" ValidationGroup="Query" />
        </div>
    </div>
    <div class="button-row">
        <asp:Button ID="btnAddURL" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, AddURL %>" 
            CommandName="Add" OnClick="btnAddURL_Click" CausesValidation="false" />
    </div>

    <div class="link-list">
        <asp:Repeater ID="rptBestBetsDetails" runat="server" OnItemCommand="rptBestBetsDetails_ItemCommand">
            <HeaderTemplate>
                <table width="100%" class="grid">
                    <thead>
                        <thead>
                            <tr>
                                <th><%= Bridgeline.iAPPS.Resources.GUIStrings.Title %></th>
                                <th><%= Bridgeline.iAPPS.Resources.GUIStrings.URL %></th>
                                <th><%= Bridgeline.iAPPS.Resources.GUIStrings.Description %></th>
                                <th><%= Bridgeline.iAPPS.Resources.GUIStrings.Action %></th>
                            </tr>
                        </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="odd-row">
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server" Width="90%" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' />
                        <asp:RequiredFieldValidator runat="server" ID="reqtxtQuery" ValidationGroup="Query" SetFocusOnError="true" 
                            ErrorMessage="<%$ Resources:GUIStrings, TitleCanNotBeEmpty %>" Display="None" />
                        <asp:RegularExpressionValidator ID="regtxtQuery" runat="server"
                            ErrorMessage="<%$ Resources:JSMessages, BestBetInvalidCharacters %>" Display="None" SetFocusOnError="true"
                            ValidationExpression="^[A-Za-z0-9-_,'’.!()/&\s&quot;]+$" ValidationGroup="Query" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLinkType" runat="server" Width="40%" AutoPostBack="true" CssClass="link-type">
                            <asp:ListItem Text="<%$ Resources:GUIStrings, ExternalURL %>" Value="ExternalURL" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, Page %>" Value="PageLibrary" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, File %>" Value="FileLibrary" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, Product %>" Value="CommerceProduct"/>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtLink" runat="server" Width="40%" Text='<%# DataBinder.Eval(Container.DataItem, "FileName") %>' />
                        <asp:Image ID="imgLink" runat="server" ImageUrl="~/App_Themes/General/images/icon-browse.gif" AlternateText="select link" CssClass="popup-handle" />
                        <asp:RequiredFieldValidator runat="server" ID="reqtxtLink" ValidationGroup="Query" SetFocusOnError="true" 
                            ErrorMessage="<%$ Resources:GUIStrings, URLCannotBeEmpty %>" Display="None" />
                        <asp:RegularExpressionValidator ID="regtxtLink" runat="server"
                            ErrorMessage="<%$ Resources:JSMessages, InvalidCharacters %>" Display="None" SetFocusOnError="true"
                            ValidationExpression="^[^<>]+$" ValidationGroup="Query" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtDescription" runat="server" Width="90%" Text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' />
                        <asp:RequiredFieldValidator runat="server" ID="reqtxtDescription" ValidationGroup="Query" SetFocusOnError="true" 
                            ErrorMessage="<%$ Resources:GUIStrings, DescriptionCannotBeEmpty %>" Display="None" />
                        <asp:RegularExpressionValidator ID="regtxtDescription" runat="server"
                            ErrorMessage="<%$ Resources:JSMessages, BestBetInvalidCharacters %>" Display="None" SetFocusOnError="true"
                            ValidationExpression="^[A-Za-z0-9-_,'’.!()/&\s&quot;]+$" ValidationGroup="Query" />
                    </td>
                    <td>
                        <asp:Button ID="btnDelete" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Delete %>" CommandArgument="<%# Container.ItemIndex %>)" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CancelAddBestBets();" />
    <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
        OnClick="SaveButton_Click" ValidationGroup="Query" CausesValidation="true" OnClientClick="return Validate()" />
    <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true" ValidationGroup="Query" />
</asp:Content>

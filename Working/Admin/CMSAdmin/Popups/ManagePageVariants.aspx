<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManagePageVariants.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManagePageVariants" StylesheetTheme="General" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var webDeviceId = "<%= WebDefaultDeviceId %>";

        function grdVariants_OnLoad(sender, eventArgs) {
            $(".page-variants").gridActions({ objList: grdVariants });
        }

        function grdVariants_OnRenderComplete(sender, eventArgs) {
            $(".page-variants").gridActions("bindControls");

            SetVerticalScrollStyles(grdVariants.get_id());
        }

        function grdVariants_OnCallbackComplete(sender, eventArgs) {
            $(".page-variants").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;

            switch (gridActionsJson.Action) {
                case "Redirect":
                    doCallback = false;
                    gridActionsJson.CustomAttributes["DeviceId"] = gridActionsJson.CustomAttributes["Redirect"];
                    var returnVal = GetDeviceInfo(gridActionsJson, eventArgs, 'true');
                    parent.OpenSegmentationFrame(returnVal);
                    break;
                case "Edit":
                    doCallback = false;
                    gridActionsJson.CustomAttributes["DeviceId"] = webDeviceId;
                    var returnVal = GetDeviceInfo(gridActionsJson, eventArgs, 'false');
                    parent.OpenSegmentationFrame(returnVal, function () {
                        grdVariants.callback();
                    });
                    break;
                case "Remove":
                    doCallback = window.confirm("<%= JSMessages.RemovePageSegmentConfirmation %>");
                break;
        }
        if (doCallback) {
            grdVariants.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdVariants.callback();
        }
    }

    function Grid_ItemSelect(sender, eventArgs) {
        sender.getItemByCommand("Preview").hide();
        sender.getItemByCommand("Remove").hideButton();
        if (eventArgs.selectedItem.getMember("Editable").get_value() == true) {
            sender.getItemByCommand("Preview").show();
            sender.getItemByCommand("Remove").showButton();
        }
    }

    function GetDeviceInfo(gridActionsJson, eventArgs, viewMode) {
        var returnVal = {};
        returnVal.DeviceId = gridActionsJson.CustomAttributes["DeviceId"];
        returnVal.AudienceId = gridActionsJson.SelectedItems.first();
        returnVal.AudienceText = eventArgs.selectedItem.getMember("Title").get_text();

        returnVal.NavigateUrl = "ViewOnly=" + viewMode + "&" + GetSegmentIdentifierQueryString(returnVal.DeviceId, returnVal.AudienceId);

        return returnVal;
    }
    </script>
    <style type="text/css">
        .grid-loading {
            opacity: 0.4;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <p><%= GUIStrings.MangePageSegmentsHelpText %></p>
    <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
    <div class="grid-section">
        <ComponentArt:Grid ID="grdVariants" SkinID="Hierarchical" runat="server" Width="810" PageSize="1000"
            CallbackCachingEnabled="false" CallbackCacheLookAhead="0" RunningMode="Callback"
            AllowVerticalScrolling="true" Height="336" ShowFooter="false" AllowEditing="false"
            EditOnClickSelectedItem="false" LoadingPanelClientTemplateId="grdVariantsLoadingPanelTemplate">
            <ClientEvents>
                <Load EventHandler="grdVariants_OnLoad" />
                <CallbackComplete EventHandler="grdVariants_OnCallbackComplete" />
                <RenderComplete EventHandler="grdVariants_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" DataCellCssClass="iapps-data-cell grid-item" HeadingCellCssClass="iapps-heading-cell"
                    RowCssClass="iapps-row" SelectedRowCssClass="iapps-selected-row" AlternatingRowCssClass="iapps-alternate-row"
                    HoverRowCssClass="iapps-hover-row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" Width="570" />
                        <ComponentArt:GridColumn DataField="Status" DataCellClientTemplateId="StatusTemplate" Width="200" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="true" Visible="false" />
                        <ComponentArt:GridColumn DataField="Editable" IsSearchable="true" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="StatusTemplate">
                    ## DataItem.getMember('Status').get_text() ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdVariantsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdVariants) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>

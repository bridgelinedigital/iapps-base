﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubmitIntoWorkflow.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SubmitIntoWorkflow" MasterPageFile="~/Popups/iAPPSPopup.master"
    StylesheetTheme="General" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".workflow-tabs").iAppsTabs();
        });

        function ValidateFields() {
            if ($('#selectWorkflow').find(":selected").val() == "Please select") {
                alert("Please select a workflow");
                return false;
            }
            return true;
        }

        function publishCalendar_onSelectionChanged(sender, eventArgs) {
            var selectedDate = publishCalendar.getSelectedDate();
            var selectedDateFormat = publishCalendar.formatDate(selectedDate, "MM/dd/yyyy");
            var currentDate1 = new Date();
            var currentDate = new Date(currentDate1.getYear(), currentDate1.getMonth(), currentDate1.getDate());
            if (selectedDate < currentDate) {
                alert(GetMessage('PublisherDateValidation'));
            }
            else {
                document.getElementById("<%=txtPublishDate.ClientID %>").value = publishCalendar.formatDate(selectedDate, "MM/dd/yyyy");
            }
        }

        function PublishPopUpCalendar() {
            obj = document.getElementById('<%=txtPublishDate.ClientID %>').value;

            if (obj != "" && obj != null) {
                if (validate_dateSummary(document.getElementById('<%=txtPublishDate.ClientID %>')) == true) {// available in validation.js
                    var thisDate = new Date(obj);
                    publishCalendar.setSelectedDate(thisDate);
                }
            }

            if (!(publishCalendar.get_popUpShowing()));
            publishCalendar.show();
        }

        function ArchiveCalendar_onSelectionChanged(sender, eventArgs) {
            var selectedDate = ArchiveCalendar.getSelectedDate();
            var selectedDateFormat = ArchiveCalendar.formatDate(selectedDate, "MM/dd/yyyy");
            var currentDate1 = new Date();
            var currentDate = new Date(currentDate1.getYear(), currentDate1.getMonth(), currentDate1.getDate());
            if (selectedDate < currentDate) {
                alert(GetMessage('ArchiveDateValidation'));
            }
            else {
                document.getElementById('<%=submitArchivePicker.ClientID %>').value = ArchiveCalendar.formatDate(selectedDate, "MM/dd/yyyy");
            }
        }

        function ArchivepopUpCalendar() {
            var thisDate = new Date();
            obj = document.getElementById('<%=submitArchivePicker.ClientID %>').value;
            if (obj != "" && obj != null)
                if (validate_dateSummary(document.getElementById('<%=submitArchivePicker.ClientID %>')) == true)// available in validation.js 
                    thisDate = new Date(obj);
            if (!(ArchiveCalendar.get_popUpShowing()));
            ArchiveCalendar.show();
        }

        function DisableEnableValidation(enableCtrl,isPublish) {
            var rfvSelectWorflow = document.getElementById('<%=rfvSelectWorflow.ClientID %>');
            ValidatorEnable(rfvSelectWorflow, enableCtrl);

            if (Page_ClientValidate()) {
                //if (!enableCtrl)
                    ShowiAppsLoadingPanel();

                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="vsSubmitIntoWorkFlow" runat="server" ShowMessageBox="true"
        ShowSummary="false" EnableClientScript="true" />
    <asp:HiddenField ID="hdnWorkFlowId" runat="server" />
    <asp:Panel ID="pnlWorkflow" runat="server">
        <ul class="tab-list">
            <li><span><a href="javascript://"><%= GUIStrings.PublicationDetails %></a></span></li>
            <li><span><a href="javascript://"><%= GUIStrings.TranslationDetails %></a></span></li>
        </ul>
        <div>
            <div class="form-row required-fields">
                <span class="req">*&nbsp;</span><%= GUIStrings.RequiredFields %></div>
            <asp:PlaceHolder ID="phSelectWorkflow" runat="server">
                <div class="form-row">
                    <label for="<%= selectWorkflow.ClientID%>" class="form-label">
                        <%= GUIStrings.SelectWorkFlow %></label>
                    <div class="form-value">
                        <asp:DropDownList ID="selectWorkflow" runat="server" AutoPostBack="true" OnSelectedIndexChanged="selectWorkflow_SelectedIndexChanged"
                            ClientIDMode="Static" Width="500" />
                        <asp:RequiredFieldValidator ID="rfvSelectWorflow" runat="server" ControlToValidate="selectWorkflow"
                            Display="None" InitialValue="<%$ Resources:GUIStrings, PleaseSelect %>" ErrorMessage="<%$ Resources: JSMessages, PleaseSelectWorkFlow %>"
                            EnableClientScript="true" />
                    </div>
                </div>
                <p>
                    <%= GUIStrings.ThisPageExistsInTheFollowingMenuItems %></p>
                <div class="form-row">
                    <div class="form-value text-value">
                        <asp:Repeater ID="rptMenuItems" runat="server" OnItemDataBound="rptMenuItems_ItemDataBound">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <asp:Literal ID="ltlMenuName" runat="server" /></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </asp:PlaceHolder>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.PublishDate %><span class="req">&nbsp;*</span></label>
                <div class="form-value calendar-value">
                    <span>
                        <asp:TextBox CssClass="textBoxes selectDate" ID="txtPublishDate" runat="server" onfocus="PublishPopUpCalendar(this)"
                            onclick="PublishPopUpCalendar(this)" />
                        <img id="calendar_button" alt="" onclick="PublishPopUpCalendar(this);" src="/Admin/App_Themes/General/images/calendar-button.png" />
                    </span>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <asp:DropDownList ID="publishTime" CssClass="textBoxes timeWidth" runat="server"
                        Width="306" />
                    <asp:RangeValidator ID="publishDateValidator" runat="server" ControlToValidate="txtPublishDate"
                        ErrorMessage="<%$ Resources: JSMessages, PublisherDateValidation %>"
                        Display="None" SetFocusOnError="true" MaximumValue="01/13/3000" MinimumValue="01/01/1900"
                        Type="Date" />
                    <asp:RequiredFieldValidator ID="PublishFieldValidator" runat="server" Display="None"
                        ControlToValidate="txtPublishDate" ErrorMessage="<%$ Resources: JSMessages, PublishDateEmpty %>"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="publishTimeValidator" runat="server" ControlToValidate="publishTime"
                        Display="None" ErrorMessage="<%$ Resources: JSMessages, PleaseSelectAPublishTime %>"
                        EnableClientScript="true" />
                    <ComponentArt:Calendar runat="server" SkinID="Default" ID="publishCalendar" ToolTip="Calendar"
                        PopUpExpandControlId="calendar_button">
                        <ClientEvents>
                            <SelectionChanged EventHandler="publishCalendar_onSelectionChanged" />
                        </ClientEvents>
                    </ComponentArt:Calendar>
                </div>
            </div>
            <div class="form-row">
                <label for="<%=submitArchivePicker.ClientID %>" class="form-label">
                    <%= GUIStrings.ArchiveDate %></label>
                <div class="form-value calendar-value">
                    <span>
                        <asp:TextBox CssClass="textBoxes selectDate" ID="submitArchivePicker" runat="server"
                            onfocus="ArchivepopUpCalendar(this)" onclick="ArchivepopUpCalendar(this)" />
                        <img id="Archivecalendar_button" alt="" class="marginLeftTop" onclick="ArchivepopUpCalendar(this);"
                            src="/Admin/App_Themes/General/images/calendar-button.png" />
                    </span>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <asp:DropDownList ID="archiveTime" CssClass="textBoxes timeWidth" runat="server"
                        Width="306" />
                    <asp:RangeValidator ID="ArchivePickerDateValidator" runat="server" ControlToValidate="submitArchivePicker"
                        ErrorMessage="<%$ Resources: JSMessages, ArchiveDateValidation %>"
                        Display="None" SetFocusOnError="true" MaximumValue="01/13/3000" MinimumValue="01/01/1900"
                        Type="Date" />
                    <ComponentArt:Calendar runat="server" SkinID="Default" ToolTip="Calendar" ID="ArchiveCalendar"
                        PopUpExpandControlId="Archivecalendar_button">
                        <ClientEvents>
                            <SelectionChanged EventHandler="ArchiveCalendar_onSelectionChanged" />
                        </ClientEvents>
                    </ComponentArt:Calendar>
                </div>
            </div>
            <div class="form-row">
                <label for="<%=dwnTimezone.ClientID %>" class="form-label">
                    <%= GUIStrings.TimeZone %></label>
                <div class="form-value">
                    <asp:DropDownList ID="dwnTimezone" runat="server" Width="500" />
                </div>
            </div>
            <div class="form-row">
                <div class="form-value" id="divSynchronizePublication" runat="server" style="display:none">
                    <asp:CheckBox ID="chkIsInGroupPublish" runat="server" Text="<%$ Resources:GUIStrings, SynchronizePublication %>" />
                    <br />
                    <em style="font-size: 11px;">
                        <%= GUIStrings.SynchronizePublicationInstructions %></em>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:HiddenField ID="hdnUserCurrentTimezone" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="submitBtn" runat="server" Text="<%$ Resources:GUIStrings, SubmitIntoWorkflow %>"
        ToolTip="<%$ Resources:GUIStrings, SubmitIntoWorkflow %>" OnClick="SubmitBtn_Click" CssClass="primarybutton"
        OnClientClick="return DisableEnableValidation(true);" />
    <asp:Button ID="publishBtn" runat="server" Text="<%$ Resources:GUIStrings, Publish %>"
        ToolTip="<%$ Resources:GUIStrings, Publish %>" OnClick="PublishBtn_Click" OnClientClick="DisableEnableValidation(false,true);"
        CssClass="primarybutton" />
</asp:Content>

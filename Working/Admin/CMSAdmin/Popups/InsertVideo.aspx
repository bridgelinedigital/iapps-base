﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="InsertVideo.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.InsertVideo"
    StylesheetTheme="General" ClientIDMode="Static" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var tag = '';
        var filterType = 1;
        var initialPlaylistId = '';
        var initialTag = '';
        var initialFilterType = 1;
        var initialVideoId = '';
        var initialPlayerId = '';
        var initialPlayerHeight = '';
        var initialPlayerWidth = '';
        var contentId = '';
        var previewPlayerDisplayString = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PreviewPlayer%>'/>";

        function ToggleVideoSection(displaySection) {
            var objBrightcoveSection = $('.brightcove-section');
            var objOtherVideoSection = $('.other-video-section');
            var objBrightcoveLink = $('#linkBrightcove');
            var objOtherVideoLink = $('#linkOtherVideo');
            switch (displaySection) {
                case 'brightcove':
                    $(objBrightcoveSection).show();
                    $(objOtherVideoSection).hide();
                    $(objBrightcoveLink).addClass('active');
                    $(objOtherVideoLink).removeClass('active');
                    if ($('#videoId').html() == '') {
                        $('#btnSave').attr('disabled', true);
                    }
                    else {
                        $('#btnSave').attr('disabled', false);
                    }
                    break;
                case 'others':
                    $(objBrightcoveSection).hide();
                    $(objOtherVideoSection).show();
                    $(objBrightcoveLink).removeClass('active');
                    $(objOtherVideoLink).addClass('active');
                    filterType = 4;
                    $('#btnSave').attr('disabled', false);
                    break;
            }
        }
        var displayErrorMessage = false;
        function ajaxError(ajaxErrorObject, type, errorThrown) {
            $('#linkOtherVideo').click();
            $('#linkBrightcove').attr('style', 'display:none');
            $('#divBrightcove').attr('style', 'display:none');
            if (displayErrorMessage)
                $('#lblErrorMessage').text(errorThrown);
            //alert(ajaxErrorObject.responseText);
            //$('#divPageTopContainer').html(errorThrown).addClass('errorMessage');
            displayErrorMessage = true;
        }

        function ShowBrightcoveVideos(objItemClicked, selectedId, byPlaylistId) {
            $('.video-grouping a').removeClass('selected');
            $(objItemClicked).addClass('selected');
            if (byPlaylistId) {
                FillAllVideosByPlaylistId(selectedId);
            }
            else {
                FillAllVideosByTag(selectedId);
            }
        }
        function FillAllTagsBasedOnCurrentPage() {
            //filterType = 3;
            var pageId = parent.PageId;
            var selectedVideo = '';
            var selectedRowIndex = 0;
            if (initialVideoId != '') {
                selectedVideo = initialVideoId;
            }
            $('.video-grouping a').remove();
            $.ajax({
                type: "POST",
                url: "InsertVideo.aspx/GetAllVideosBasedOnCurrentPageTag",
                data: "{'pageId':'" + pageId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    var divVideolisting = $('#tblVideo tbody');
                    $('#tblVideo .odd-row').remove();
                    $('#tblVideo .even-row').remove();
                    if (results.d != null) {

                        $.each(results.d, function (i, item) {
                            var thumbnailImage = '<img title="' + item.VideoName + '" alt="' + item.VideoName + ' id="videoThumbImg"' + i + ' src="' + item.ThumbnailUrl + '">';
                            var videoName = item.VideoName;
                            var datelastUpdated = item.PublishedDate;

                            var line = '';
                            if ((i % 2) == 1) {
                                line = '<tr class="even-row" onclick="DisplayVideoMetaDataByVideoId(this, ' + item.VideoId + ');">' + '<td>' + thumbnailImage + '</td><td>' + videoName + '</td><td>' + datelastUpdated + '</td></tr>';
                            }
                            else {
                                line = '<tr class="odd-row" onclick="DisplayVideoMetaDataByVideoId(this, ' + item.VideoId + ');">' + '<td>' + thumbnailImage + '</td><td>' + videoName + '</td><td>' + datelastUpdated + '</td></tr>';
                            }
                            if (i == 0 && selectedVideo == '')
                                selectedVideo = item.VideoId;
                            if (selectedVideo != '' && selectedVideo == item.VideoId) {
                                selectedRowIndex = i;
                            }
                            divVideolisting.append(line);
                        });
                        var thisObject = $('#tblVideo').find('tbody tr:eq(' + selectedRowIndex + ')');
                        DisplayVideoMetaDataByVideoId(thisObject, selectedVideo);
                    }
                },
                error: ajaxError
            });
        }
        var brightcoveTagsObject;
        function FillAllBrightcoveTags() {
            filterType = 2;
            $('#divTagSearch').css('display', 'none');
            $('.video-grouping a').remove();
            $.ajax({
                type: "POST",
                url: "InsertVideo.aspx/GetAllTags",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    brightcoveTagsObject = results.d;
                    if (brightcoveTagsObject.length > 0) {
                        $('#divTagSearch').css('display', 'block');
                        $('#txtSearchTag').val('');
                    }
                    RenderTags();
                },
                error: ajaxError
            });
        }
        function FillAllPageTags() {
            filterType = 3;
            $('#divTagSearch').css('display', 'none');
            $('.video-grouping a').remove();
            $.ajax({
                type: "POST",
                url: "InsertVideo.aspx/GetAllTagsFromCurrentPage",
                data: "{'pageId':'" + parent.PageId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    brightcoveTagsObject = results.d;
                    if (brightcoveTagsObject.length > 0) {
                        $('#divTagSearch').css('display', 'block');
                        $('#txtSearchTag').val('');
                    }
                    RenderTags();
                },
                error: ajaxError
            });
        }

        function RenderTags() {
            var divvideogrouping = $('.video-grouping');
            var indexNo = 0;
            var searchTag = $('#txtSearchTag').val();
            var selectedTag = '';
            if (initialTag != '') {
                selectedTag = initialTag;
            }

            $('.video-grouping a').remove();
            $.each(brightcoveTagsObject, function (i, item) {
                var line = '';
                //item == searchTag
                if (searchTag == '' || (searchTag != '' && item.indexOf(searchTag) > -1)) {
                    if (indexNo == 0) {
                        line = "<a href=\"#\" onclick=\"javascript: ShowBrightcoveVideos(this,'" + item + "',false);\" class=\"selected\">" + item + "</a>";
                        if (selectedTag == '') {
                            selectedTag = item;
                        }
                        indexNo++;
                    }
                    else {
                        line = "<a href=\"#\" onclick=\"javascript: ShowBrightcoveVideos(this,'" + item + "',false);\">" + item + "</a>";
                        indexNo++;
                    }
                }
                divvideogrouping.append(line);
            });
            if (selectedTag != '') {
                FillAllVideosByTag(selectedTag);
                $('.video-grouping a').removeClass('selected');
                $('.video-grouping a:contains(' + selectedTag + ')').addClass('selected');
            }
            else {
                ClearVideoListing();
            }
        }

        function FillAllVideosByTag(tagObject) {
            tag = tagObject;
            //filterType = 2;
            var selectedVideo = '';
            var selectedRowIndex = 0;
            if (initialVideoId != '') {
                selectedVideo = initialVideoId;
            }
            $.ajax({
                type: "POST",
                url: "InsertVideo.aspx/GetAllVideosByTag",
                data: "{'tag':'" + tagObject + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    var divVideolisting = $('#tblVideo tbody');
                    ClearVideoListing();
                    if (results.d != null) {
                        $.each(results.d, function (i, item) {
                            var thumbnailImage = '<img title="' + item.VideoName + '" alt="' + item.VideoName + ' id="videoThumbImg"' + i + ' src="' + item.ThumbnailUrl + '">';
                            var videoName = item.VideoName;
                            var datelastUpdated = item.PublishedDate;

                            var line = '';
                            if ((i % 2) == 1) {
                                line = '<tr class="even-row" id="tr_' + item.VideoId + '"  onclick="DisplayVideoMetaDataByVideoId(this, ' + item.VideoId + ');">' + '<td>' + thumbnailImage + '</td><td>' + videoName + '</td><td>' + datelastUpdated + '</td></tr>';
                            }
                            else {
                                line = '<tr class="odd-row" id="tr_' + item.VideoId + '"  onclick="DisplayVideoMetaDataByVideoId(this, ' + item.VideoId + ');">' + '<td>' + thumbnailImage + '</td><td>' + videoName + '</td><td>' + datelastUpdated + '</td></tr>';
                            }
                            if (i == 0 && selectedVideo == '')
                                selectedVideo = item.VideoId;
                            if (selectedVideo != '' && selectedVideo == item.VideoId) {
                                selectedRowIndex = i;
                            }
                            divVideolisting.append(line);
                        });
                        var thisObject = $('#tblVideo').find('tbody tr:eq(' + selectedRowIndex + ')');
                        DisplayVideoMetaDataByVideoId(thisObject, selectedVideo);
                    }
                },
                error: ajaxError
            });
        }

        function ClearVideoListing() {
            $('#tblVideo .odd-row').remove();
            $('#tblVideo .even-row').remove();
            ClearPreviewPanel();
        }

        function FillAllPlaylist() {
            $('.video-grouping a').remove();
            var selectedPlaylist = '';
            if (initialPlaylistId != '') {
                selectedPlaylist = initialPlaylistId;
            }
            $.ajax({
                type: "POST",
                url: "InsertVideo.aspx/GetAllPlaylist",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    var divvideogrouping = $('.video-grouping');
                    if (results.d != null) {
                        $.each(results.d, function (i, item) {
                            var line = '';
                            if (i == 0) {
                                line = '<a href="#" id="' + item.PlaylistId + '" onclick=\"javascript: ShowBrightcoveVideos(this,' + item.PlaylistId + ',true);\" class=\"selected\">' + item.PlaylistName + '<span>(' + item.VideoCount + ')</span></a>';
                                if (selectedPlaylist == '') {
                                    selectedPlaylist = item.PlaylistId;
                                }
                            }
                            else
                                line = '<a href="#" id="' + item.PlaylistId + '" onclick=\"javascript: ShowBrightcoveVideos(this,' + item.PlaylistId + ',true);\">' + item.PlaylistName + '<span>(' + item.VideoCount + ')</span></a>';

                            divvideogrouping.append(line);
                        });
                        FillAllVideosByPlaylistId(selectedPlaylist);
                        $('.video-grouping a').removeClass('selected');
                        $('.video-grouping a[id=' + selectedPlaylist + ']').addClass('selected');
                    }
                    $('#divTagSearch').css('display', 'none');
                },
                error: ajaxError
            });
        }

        function FillAllVideosByPlaylistId(playlistId) {
            filterType = 1;
            returnPlaylistId = playlistId;
            var selectedVideo = '';
            var selectedRowIndex = 0;
            if (initialVideoId != '') {
                selectedVideo = initialVideoId;
            }
            $.ajax({
                type: "POST",
                url: "InsertVideo.aspx/GetAllVideosByPlaylistId",
                data: "{'playlistId':'" + playlistId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    var divVideolisting = $('#tblVideo tbody');
                    $('#tblVideo .odd-row').remove();
                    $('#tblVideo .even-row').remove();
                    if (results.d != null) {
                        $.each(results.d, function (i, item) {
                            var thumbnailImage = '<img title="' + item.ShortDescription + '" alt="' + item.VideoName + ' id="videoThumbImg"' + i + ' src="' + item.ThumbnailUrl + '">';
                            var videoName = item.VideoName;
                            var datelastUpdated = item.PublishedDate;

                            var line = '';
                            if ((i % 2) == 1) {
                                line = '<tr class="even-row" onclick="DisplayVideoMetaDataByVideoId(this, ' + item.VideoId + ');">' + '<td>' + thumbnailImage + '</td><td>' + videoName + '</td><td>' + datelastUpdated + '</td></tr>';
                            }
                            else {
                                line = '<tr class="odd-row" onclick="DisplayVideoMetaDataByVideoId(this, ' + item.VideoId + ');">' + '<td>' + thumbnailImage + '</td><td>' + videoName + '</td><td>' + datelastUpdated + '</td></tr>';
                            }
                            if (i == 0 && selectedVideo == '')
                                selectedVideo = item.VideoId;
                            if (selectedVideo != '' && selectedVideo == item.VideoId) {
                                selectedRowIndex = i;
                            }
                            divVideolisting.append(line);
                        });
                        var thisObject = $('#tblVideo').find('tbody tr:eq(' + selectedRowIndex + ')');
                        DisplayVideoMetaDataByVideoId(thisObject, selectedVideo);
                    }
                },
                error: ajaxError
            });
        }

        function DisplayVideoMetaDataByVideoId(objRowClicked, videoId) {
            $(objRowClicked).siblings().removeClass('selected-row');
            $(objRowClicked).addClass('selected-row');
            if (videoId == '') {
                ClearPreviewPanel();
            }
            else {
                $('#btnSave').attr('disabled', false);
                $.ajax({
                    type: "POST",
                    url: "InsertVideo.aspx/GetVideoById",
                    data: "{'videoId':'" + videoId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (results) {
                        var item = results.d;
                        $('#divVideoThumbnailImage').css('display', 'block');
                        $('#videoName').html(item.Name);
                        $('#videoStatus').html(item.Status);
                        $('#videoDuration').html(item.Duration);
                        $('#videoId').html(item.Id);
                        $('#videoRefId').html(item.ReferenceId);
                        $('#videoTags').html(item.Tags);
                    },
                    error: ajaxError
                });
            }
        }

        function SearchVideos() {
            filterType = 4;
            var selectedVideo = '';
            var selectedRowIndex = 0;
            if (initialVideoId != '') {
                selectedVideo = initialVideoId;
            }
            $.ajax({
                type: "POST",
                url: "InsertVideo.aspx/GetAllVideosBySeachValue",
                data: "{'searchValue':'" + $('#txtVideoSearch').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    var divVideolisting = $('#tblVideo tbody');
                    ClearVideoListing();
                    if (results.d != null) {
                        $.each(results.d, function (i, item) {
                            var thumbnailImage = '<img title="' + item.VideoName + '" alt="' + item.VideoName + ' id="videoThumbImg"' + i + ' src="' + item.ThumbnailUrl + '">';
                            var videoName = item.VideoName;
                            var datelastUpdated = item.PublishedDate;

                            var line = '';
                            if ((i % 2) == 1) {
                                line = '<tr class="even-row" id="tr_' + item.VideoId + '"  onclick="DisplayVideoMetaDataByVideoId(this, ' + item.VideoId + ');">' + '<td>' + thumbnailImage + '</td><td>' + videoName + '</td><td>' + datelastUpdated + '</td></tr>';
                            }
                            else {
                                line = '<tr class="odd-row" id="tr_' + item.VideoId + '"  onclick="DisplayVideoMetaDataByVideoId(this, ' + item.VideoId + ');">' + '<td>' + thumbnailImage + '</td><td>' + videoName + '</td><td>' + datelastUpdated + '</td></tr>';
                            }
                            if (i == 0 && selectedVideo == '')
                                selectedVideo = item.VideoId;
                            if (selectedVideo != '' && selectedVideo == item.VideoId) {
                                selectedRowIndex = i;
                            }
                            divVideolisting.append(line);
                        });
                        var thisObject = $('#tblVideo').find('tbody tr:eq(' + selectedRowIndex + ')');
                        DisplayVideoMetaDataByVideoId(thisObject, selectedVideo);
                    }
                },
                error: ajaxError
            });
        }

        function ClearPreviewPanel() {
            $('#divVideoThumbnailImage').css('display', 'none');
            $('#videoName').html('');
            $('#videoStatus').html('');
            $('#videoDuration').html('');
            $('#videoId').html('');
            $('#videoRefId').html('');
            $('#videoTags').html('');
            $('#btnSave').attr('disabled', true);
        }

        function FlushVideoList(thisObject) {
            initialPlaylistId = '';
            initialVideoId = '';
            initialTag = '';
            initialPlayerHeight = '';
            initialPlayerWidth = '';
            $('#divVideoSearch').css('display', 'none');
            var selectedVideoGroup = $('#ddlVideoGroup').val();
            if (selectedVideoGroup == 1) {
                FillAllPlaylist();
            }
            if (selectedVideoGroup == 2) {
                FillAllBrightcoveTags();
            }
            else if (selectedVideoGroup == 3) {
                FillAllPageTags(); //  FillAllTagsBasedOnCurrentPage();
            }
            else {
                $('#divTagSearch').css('display', 'none');
                $('#divVideoSearch').css('display', 'block');
                $('.video-grouping a').remove();
            }
        }

        function DisplayPlayerDetail(thisObject) {
            var selectedPlayerValue = $('#ddlPlayer').val();
            if (selectedPlayerValue != null) {
                var playerJSONObject = JSON.parse(selectedPlayerValue);
                $('#txtHeight').val(playerJSONObject.Height);
                $('#txtWidth').val(playerJSONObject.Width);
                $('#hdnCanPlayList').val(playerJSONObject.CanPlayList);
            }
        }

        var returnPlayerId;
        var returnPlayerKey;
        var returnVideoIds;
        var returnPlaylistId;
        var returnEmbedCode;
        var returnPlayerType;

        function GetPlayerDetails() {
            var selectedPlayerValue = $('#ddlPlayer').val();
            if (selectedPlayerValue != null) {
                var playerJSONObject = JSON.parse(selectedPlayerValue);
                returnPlayerId = playerJSONObject.Id;
                returnPlayerKey = playerJSONObject.Key;
            }
        }

        function GetSelectedVideoIds() {
            returnVideoIds = '';
            var selectedVideoGroup = $('#ddlVideoGroup').val();
            if (selectedVideoGroup != null && selectedVideoGroup != 1) {
                returnVideoIds = $('#videoId').html();
            }
        }

        function ReturnSelectedVideo() {
            if ($('#divOther').css("display") == "block") {
                returnEmbedCode = $('#txtEmbedCode').val();
                if (returnEmbedCode == '') {
                    alert("Embed code should not be empty.");
                    return false;
                }
            }
            else {
                returnEmbedCode = '';
                GetPlayerDetails();
                GetSelectedVideoIds();
            }

            if (returnVideoIds == '') {
                returnPlayerType = 4;
            }
            else {
                returnPlayerType = 1;
            }
            var returnObject =
			{
			    PlayerId: returnPlayerId,
			    PlayerKey: returnPlayerKey,
			    VideoIds: returnVideoIds,
			    PlaylistId: returnPlaylistId,
			    PlayerType: returnPlayerType,
			    EmbedCode: returnEmbedCode
			};
            GenerateVideoXmlData();
        }

        function ValidateHeightAndWidth(strWidth, strHeight, width, height) {
            if (isNaN(strWidth) || isNaN(strHeight)) {
                alert("The width and height of the video player must be numeric.");
                return false;
            }
            if (strHeight != '' && strHeight != 'undefined') {
                height = parseInt(strHeight);
            }
            if (strWidth != '' && strWidth != 'undefined') {
                width = parseInt(strWidth);
            }
            if (height <= 0 || width <= 0) {
                alert("The width and height of the video player must be greater than 0.");
                return false;
            }
            return true;
        }

        function GenerateVideoXmlData() {
            var strWidth = $('#txtWidth').val().trim();
            var strHeight = $('#txtHeight').val().trim();
            var height = 0;
            var width = 0;

            if (returnEmbedCode == '') {
                if (!ValidateHeightAndWidth(strWidth, strHeight, width, height)) {
                    return;
                }
                else {
                    width = parseInt(strWidth);
                    height = parseInt(strHeight);
                }
            }
            returnVideoIds = $('#videoId').html();
            //if ($('#hdnCanPlayList').val() == 'True') {
            //    returnVideoIds = '';
            //}
            $.ajax({
                type: "POST",
                url: "InsertVideo.aspx/GetVideoXmlData",
                data: "{'playerId':'" + returnPlayerId + "','playerKey':'" + returnPlayerKey + "','videoIds':'" + returnVideoIds + "','playlistId':'" + returnPlaylistId + "','height':'" + height + "','width':'" + width + "','playerType':'" + returnPlayerType + "','embed':'" + returnEmbedCode + "','contentId':'" + contentId + "','filterType':'" + filterType + "','tag':'" + tag + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    var item = results.d;
                    popupActionsJson.CustomAttributes["XmlData"] = item.xmlData;
                    popupActionsJson.CustomAttributes["HtmlData"] = item.htmlData;

                    SelectiAppsAdminPopup();
                },
                error: ajaxError
            });
        }

        var popupPreviewVideoPopup;
        function OpenPreviewPopup() {
            var videoId = $('#videoId').html();

            var strWidth = $('#txtWidth').val().trim();
            var strHeight = $('#txtHeight').val().trim();
            var height = 0;
            var width = 0;
            if (!ValidateHeightAndWidth(strWidth, strHeight, width, height)) {
                return;
            }
            else {
                width = parseInt(strWidth);
                height = parseInt(strHeight);
            }
            //var width = $('#txtWidth').val();
            //var height = $('#txtHeight').val();

            GetPlayerDetails();
            var adminpath = jCMSAdminSiteUrl + "/Popups/BrightcoveVideoPlay.aspx?VideoId=" + videoId + "&PlayerId=" + returnPlayerId + "&PlayerKey=" + returnPlayerKey + "&IsVideo=true"
            if (height != '') {
                adminpath = adminpath + "&Height=" + height;
            }
            if (width != '') {
                adminpath = adminpath + "&Width=" + width;
            }
            popupPreviewVideoPopup = dhtmlmodal.open('', 'iframe', adminpath, '', '');
        }


        $(document).ready(function () {
            var editedVideoXml = parent.popupActionsJson.CustomAttributes["XmlData"];
            if (editedVideoXml != '') {
                parseEditedVideoXml(editedVideoXml);
                $('#ddlVideoGroup').val(initialFilterType);


                $("#ddlPlayer option")
                .filter(function () {
                    var playerValueJson = $(this).val();
                    var playerJSONObject = JSON.parse(playerValueJson);
                    if (playerJSONObject.Id == initialPlayerId) {
                        $('#hdnCanPlayList').val(playerJSONObject.CanPlayList);
                        return $(this);
                    }
                }).attr("selected", true);

                $('#txtHeight').val(initialPlayerHeight);
                $('#txtWidth').val(initialPlayerWidth);


                if (initialFilterType == '2') {
                    FillAllBrightcoveTags();
                }
                else if (initialFilterType == '1') {
                    //                    initialPlaylistId = initialVideoId;
                    //                    initialVideoId = '';
                    if ($('#hdnCanPlayList').val() == 'True') {
                        initialVideoId = '';
                    }
                    FillAllPlaylist();
                }
                else if (initialFilterType == '3') {
                    FillAllPageTags(); // FillAllTagsBasedOnCurrentPage();
                }
                else if (initialFilterType == '4') {
                    $('#divVideoSearch').css('display', 'block');
                    $('.video-grouping a').remove();
                    SearchVideos();
                }
                else
                    FillAllPlaylist();
            }
            else
                FillAllPlaylist();
        });

        function parseEditedVideoXml(editedVideoXml) {
            if (editedVideoXml != '') {
                initialVideoId = $(editedVideoXml).attr('VideoId');
                initialPlaylistId = $(editedVideoXml).attr('PlaylistId');;
                initialFilterType = $(editedVideoXml).attr('URLType');
                initialTag = $(editedVideoXml).attr('Keywords');
                initialPlayerId = $(editedVideoXml).attr('PlayerId');
                initialPlayerHeight = $(editedVideoXml).attr('Height');
                initialPlayerWidth = $(editedVideoXml).attr('Width');
                contentId = $(editedVideoXml).attr('ContentId');
                if ($(editedVideoXml).attr('Text') != '') {
                    //ToggleVideoSection('others');
                    $('#linkOtherVideo').click();
                    $('#divOther').attr('style', 'display:block');
                    $('#txtEmbedCode').val($(editedVideoXml).attr('Text'));
                }
                else
                    ToggleVideoSection('brightcove');
            };
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p><%= GUIStrings.BrightCoveVideoInstruction %></p>
    <hr />
    <div class="navigation-bar clear-fix">
        <div class="navigation-links">
            <a href="javascript: ToggleVideoSection('brightcove');" id="linkBrightcove" runat="server" class="active">
                <%= GUIStrings.InsertVideo_Brightcove %></a>
            <a onclick="javascript: ToggleVideoSection('others');" href="#" id="linkOtherVideo" runat="server">
                <%= GUIStrings.InsertVideo_Embed %></a>
        </div>
    </div>
    <div class="brightcove-section clear-fix" id="divBrightcove" runat="server">
        <div class="video-grouping">
            <h3><%= GUIStrings.FindVideos %></h3>
            <p><%= GUIStrings.BrightCoveFindVideosInstructions %></p>
            <asp:DropDownList ID="ddlVideoGroup" runat="server" onchange="FlushVideoList(this);">
                <asp:ListItem Text="Playlist" Value="1" />
                <asp:ListItem Text="Tags" Value="2" />
                <asp:ListItem Text="Page Tags" Value="3" />
                <asp:ListItem Text="Search Videos" Value="4" />
            </asp:DropDownList>
            <div class="form-row" id="divTagSearch">
                <div class="form-value">
                    <input type="text" id="txtSearchTag" /><input type="button" id="btnSearchTag" value="<%= GUIStrings.Search %>"
                        onclick="RenderTags();" class="small-button" />
                </div>
            </div>
        </div>
        <div class="video-listing">
            <h3><%= GUIStrings.Videos %></h3>
            <div class="form-row" id="divVideoSearch" style="display: none;">
                <div class="form-value">
                    <input type="text" id="txtVideoSearch" /><input type="button" id="Button1" value="<%= GUIStrings.Search %>"
                        onclick="SearchVideos();" class="small-button" />
                </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" width="96%" class="grid" id="tblVideo">
                <thead>
                    <tr>
                        <th>
                            <%= GUIStrings.StillImage %>
                        </th>
                        <th>
                            <%= GUIStrings.VideoName%>
                        </th>
                        <th>
                            <%= GUIStrings.LastUpdated%>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="video-details">
            <h3><%= GUIStrings.SelectedVideoDetails %></h3>
            <div class="form-row" style="display: none">
                <label class="form-label">
                    <%= GUIStrings.Name%></label>
                <div class="form-value text-value" id="videoName">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Status%></label>
                <div class="form-value text-value" id="videoStatus">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Duration%></label>
                <div class="form-value text-value" id="videoDuration">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Id%></label>
                <div class="form-value text-value" id="videoId">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.RefId%></label>
                <div class="form-value text-value" id="videoRefId">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Tags%></label>
                <div class="form-value text-value" id="videoTags">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Player%></label>
                <div class="form-value" id="videoPlayers">
                    <asp:DropDownList ID="ddlPlayer" runat="server" Width="140" onchange="DisplayPlayerDetail(this);"
                        ClientIDMode="Static">
                        <asp:ListItem Text="Player 1" />
                        <asp:ListItem Text="Player 2" />
                        <asp:ListItem Text="Player 3" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">&nbsp;</label>
                <div class="form-value">
                    <div class="image-row" id="divVideoThumbnailImage" style="display: none">
                        <a href="javascript:OpenPreviewPopup()" id="videoThumbnailImageId">
                            <%= GUIStrings.PreviewPlayer %></a>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Height%></label>
                <div class="form-value text-value" id="divHeight">
                    <asp:TextBox ID="txtHeight" runat="server" Style="text-align: right;" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Width%></label>
                <div class="form-value text-value" id="divWidth">
                    <asp:TextBox ID="txtWidth" runat="server" Style="text-align: right;" />
                </div>
            </div>
        </div>
    </div>
    <div class="other-video-section" id="divOther">
        <p>
            <em>
                <%= GUIStrings.InsertVideo_EmbedInstructions %></em>
        </p>
        <asp:TextBox ID="txtEmbedCode" runat="server" TextMode="MultiLine" />
    </div>
    <asp:HiddenField ID="hdnCanPlayList" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnClose" value="<%= GUIStrings.Close %>" onclick="CanceliAppsAdminPopup();"
        class="button" />
    <input type="button" id="btnSave" value="<%= GUIStrings.Select %>" onclick="ReturnSelectedVideo();"
        class="primarybutton" />
</asp:Content>

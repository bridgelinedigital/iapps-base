﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchIndex.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.SearchIndex"
    MasterPageFile="~/Administration/Search/SearchSettings.master" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">

        var Bridgeline = Bridgeline || {};

        Bridgeline.namespace = function (ns_string) {

            var parts = ns_string.split('.'), parent = Bridgeline, i;
            if (parts[0] === "Bridgeline") {
                parts = parts.slice(1);
            }
            for (i = 0; i < parts.length; i += 1) {
                if (typeof parent[parts[i]] === "undefined") {
                    parent[parts[i]] = {};
                }
                parent = parent[parts[i]];
            }
            return parent;
        }

        Bridgeline.namespace('Bridgeline.SearchInjector');
        Bridgeline.namespace('Bridgeline.SearchInjector.Dao');
        Bridgeline.namespace('Bridgeline.SearchInjector.Global');

        Bridgeline.SearchInjector = {

            currentPagep: 0,
            action: "add",
            type: "",
            index: "",
            indexTerms: true,
            Start: function (action, type, currentpage, index, indexTerms, fetchProductHtml) {
                this.action = action;
                this.type = type;
                this.index = index;
                this.indexTerms = indexTerms;
                this.fetchProductHtml = fetchProductHtml;

                Bridgeline.SearchInjector.ClearStatus();
                Bridgeline.SearchInjector.AddStatus("Starting Indexing...");
                Bridgeline.SearchInjector.Inject(currentpage);
            },
            ClearStatus: function (text) {
                $(Bridgeline.SearchInjector.Global.STATUS).html("");
            },
            AddStatus: function (text) {
                $(Bridgeline.SearchInjector.Global.STATUS).html(text);
            },
            Inject: function (page) {

                var req = Bridgeline.SearchInjector.Dao.CreateRequestModel(this.action, this.type, page, this.index, this.indexTerms, this.fetchProductHtml);
                Bridgeline.SearchInjector.Dao.Post(Bridgeline.SearchInjector.HandleResponse, req);
            },

            HandleResponse: function (data) {

                Bridgeline.SearchInjector.AddStatus(data.text);
                if (data.nextpage > 0) {
                    Bridgeline.SearchInjector.Inject(data.nextpage);
                }
            }
        }

        Bridgeline.SearchInjector.Dao = {

            Post: function (callback, request) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/administration/search/injector.ashx',
                    data: { request: request },
                    success: function (data) {
                        callback.call(this, data);
                    },
                    dataType: 'json',
                    timeout: 1800000
                });
            },

            CreateRequestModel: function (action, type, currentPage, index, indexTerms, fetchProductHtml) {
                request = {
                    "action": action,
                    "type": type,
                    "currentpage": currentPage,
                    "index": index,
                    "indexterms": indexTerms,
                    "fetchproducthtml": fetchProductHtml
                };
                return JSON.stringify(request);
            }
        }


        Bridgeline.SearchInjector.Global = {
            STATUS: "#status"
        }


        /* ON LOAD */


        $(document).ready(function () {
            $('.indexable-list ul li input').each(function (i) {

                $(this).bind('click', function () {
                    var rel = $(this).attr('rel');
                    var index = $("#ddlIndexType").val();
                    var indexTerm = $('#chkIndexTerms').is(':checked');
                    var fetchProductHtml = $('#chkProductHtml').is(':checked');
                    Bridgeline.SearchInjector.Start("add", rel, 1, index, indexTerm, fetchProductHtml);

                });
            });
        });



        /*************************MISC*****************************/

        function ShowFrontEndStyles() {
            $(".admin-index").css("display", "none");
            $(".website-index").css("display", "block");
        }

        function cbIndexer_BeforeCallback(sender, eventArgs) {
            grdIndexFiles.dispose();
        }

        function cbIndexer_CallbackComplete(sender, eventArgs) {
            ShowFrontEndStyles();
        }

        function SendData(app, option) {
            cbIndexer.callback(app, option);
        }
    </script>

    <style type="text/css">

       .itemstoindex li div{
        height:35px;
        }

        .itemstoindex li div div {
            text-align:right;
            padding-right:5px;
            float:left;
        }

        .itemstoindex li div div:first-child {
            width:175px;

        }
    </style>

</asp:Content>

<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:PlaceHolder ID="phOldIsys" runat="server">
        <div class="search-index">
            <asp:HiddenField ID="hdnSelectedIndex" runat="server" />
            <div class="grid-utility clear-fix">
                <div class="columns">
                    <asp:Label ID="lblWebSite" Text="<%$ Resources:GUIStrings, WebsiteSearchIndex %>" runat="server"></asp:Label>                   
                </div>
                <div class="columns" style="padding-top: 5px;">
                    <asp:CheckBox ID="chkUpdateIndex" Checked="true" runat="server" Text="<%$ Resources:GUIStrings, UpdateIndex %>" />
                </div>                         
                <div class="right-columns website-index">
                    <asp:Button ID="btnInvokeFrontEnd" OnClick="btnInvoke_OnClick" runat="server" Text="<%$ Resources:GUIStrings, InvokeFrontEnd %>"
                        CommandArgument="FrontEnd" CssClass="button" />
                </div>
            </div>
            <ComponentArt:CallBack ID="cbIndexer" runat="server" OnCallback="cbIndexer_Callback">
                <ClientEvents>
                    <CallbackComplete EventHandler="cbIndexer_CallbackComplete" />
                    <BeforeCallback EventHandler="cbIndexer_BeforeCallback" />
                </ClientEvents>
                <Content>
                    <asp:PlaceHolder ID="phIndexer" runat="server">
                        <ComponentArt:Grid ID="grdIndexFiles" SkinID="default" runat="server" Width="100%"
                            ShowFooter="false" EnableViewState="true" LoadingPanelClientTemplateId="grdIndexFilesLoadingTemplate"
                            EmptyGridText="<%$ Resources:GUIStrings, NoIndexFiles %>" CssClass="fat-grid">
                            <Levels>
                                <ComponentArt:GridLevel DataKeyField="FileName" AllowSorting="false" SelectedRowCssClass="selected"
                                    DataCellCssClass="data-cell" AllowGrouping="false" ShowHeadingCells="false" AlternatingRowCssClass="alternate-row"
                                    RowCssClass="row" HoverRowCssClass="hover-row">
                                    <Columns>
                                        <ComponentArt:GridColumn DataField="FileName" Visible="true" DataCellServerTemplateId="DetailsTemplate" />
                                        <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" />
                                        <ComponentArt:GridColumn DataField="Type" Visible="false" />
                                        <ComponentArt:GridColumn DataField="Size" Visible="false" />
                                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                                    </Columns>
                                </ComponentArt:GridLevel>
                            </Levels>
                            <ServerTemplates>
                                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                                    <Template>
                                        <div class="collection-row grid-item clear-fix">
                                            <div class="first-cell">
                                                <%# GetGridItemRowNumber(Container.DataItem)%>
                                            </div>
                                            <div class="middle-cell">
                                                <h4>
                                                    <%# Container.DataItem["FileName"]%>
                                                </h4>
                                            </div>
                                            <div class="last-cell">
                                                <div class="child-row">
                                                    Modified Date:
                                                <%# Container.DataItem["ModifiedDate"] %>
                                                </div>
                                                <div class="child-row">
                                                    <%# Container.DataItem["Type"] %>
                                                |
                                                <%# Container.DataItem["Size"] %>
                                                KB
                                                </div>
                                            </div>
                                        </div>
                                    </Template>
                                </ComponentArt:GridServerTemplate>
                            </ServerTemplates>
                            <ClientTemplates>
                                <ComponentArt:ClientTemplate ID="grdIndexFilesLoadingTemplate">
                                    ## GetGridLoadingPanelContent(grdIndexFiles) ##
                                </ComponentArt:ClientTemplate>
                            </ClientTemplates>
                        </ComponentArt:Grid>
                    </asp:PlaceHolder>
                </Content>
            </ComponentArt:CallBack>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phNewIsys" runat="server">
        <div class="form-row">
            <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.IndexType %></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlIndexType" ClientIDMode="Static" runat="server" OnSelectedIndexChanged="ddlIndexType_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.IndexPageTags %></label>
            <div class="form-value">
                <asp:CheckBox ID="chkIndexTerms" ClientIDMode="Static" runat="server" AutoPostBack="true" />
            </div>
        </div>
        <asp:PlaceHolder ID="phProduct" runat="server" Visible="false">
            <div class="form-row">
            <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.FetchProductHtml %></label>
            <div class="form-value">
                <asp:CheckBox ID="chkProductHtml" ClientIDMode="Static" runat="server" />
            </div>
        </div>
        </asp:PlaceHolder>
        <asp:MultiView ID="uxMultiviewIndexOptions" runat="server">
            <asp:View ID="uxViewAdminIndexPanel" runat="server">
                <div class="form-row">
                    <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.ItemsToIndex %></label>
                    <div id="status"></div>                    
                    <div class="form-value indexable-list">
                          <ul class="itemstoindex">
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.ContentItems %> (<asp:Literal runat="server" ID="uxContentItemToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.ContentItems %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.ContentDefinitions %> (<asp:Literal runat="server" ID="uxContentDefinitionToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.ContentDefinitions %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Pages %> (<asp:Literal runat="server" ID="uxPageToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Pages %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Templates %> (<asp:Literal runat="server" ID="uxTemplatesToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Templates %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>

                            </li>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Styles %> (<asp:Literal runat="server" ID="uxStylesToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Styles %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>

                            </li>
                            <%--  <li><%= Bridgeline.iAPPS.Resources.GUIStrings.Users %></span> (<asp:Literal runat="server" ID="uxUsersToIndexCount" Text="0"></asp:Literal>)
                            <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Users %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" /></li>--%>
                            <%--   <li><%= Bridgeline.iAPPS.Resources.GUIStrings.Workflows %></span> (<asp:Literal runat="server" ID="uxWorkflowsToIndexCount" Text="0"></asp:Literal>)
                            <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Workflows %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" /></li>--%>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Blogs %> (<asp:Literal runat="server" ID="uxBlogsToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Blogs %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>

                            </li>

                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Images %> (<asp:Literal runat="server" ID="uxImagesToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Images %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>

                            </li>

                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Files %> (<asp:Literal runat="server" ID="uxFilesToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Files %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>

                            </li>
                            <%-- <li><%= Bridgeline.iAPPS.Resources.GUIStrings.Menus %></span> (<asp:Literal runat="server" ID="uxMenusToIndexCount" Text="0"></asp:Literal>)
                            <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Menus %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" /></li>--%>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Metadata %></span>
                                    </div>
                                    <div>
                                        <!--Please do not transfer/change the rel="metadata" to read from a resource file, it will break search reprocess metadata -->
                                        <input type="button" class="button" rel="Metadata" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.ReprocessMetadata %>" />

                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
            </asp:View>
            <asp:View ID="uxViewWebsiteIndexPanel" runat="server">
                <div class="form-row">
                    <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.ItemsToIndex %></label>
                    <div id="status"></div>
                    <div class="form-value indexable-list">
                           <ul class="itemstoindex">
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Pages %> (<asp:Literal runat="server" ID="uxPageWebsiteToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Pages %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Blogs %> (<asp:Literal runat="server" ID="uxBlogWebsiteToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Blogs %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Images %> (<asp:Literal runat="server" ID="uxImageWebsiteToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Images %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />

                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Files %> (<asp:Literal runat="server" ID="uxFileWebsiteToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Files %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Metadata %></span>

                                    </div>
                                    <div>
                                        <!--Please do not transfer/change the rel="metadata" to read from a resource file, it will break search reprocess metadata -->
                                        <input type="button" class="button" rel="Metadata" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.ReprocessMetadata %>" />

                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
            </asp:View>
            <asp:View ID="uxViewProductIndexPanel" runat="server">
                <div class="form-row">
                    <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.ItemsToIndex %></label>
                    <div id="status"></div>
                    <div class="form-value indexable-list">
                        <ul class="itemstoindex">
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Products %> (<asp:Literal runat="server" ID="uxProductsToIndexCount" Text="0"></asp:Literal>)</span>
                                    </div>
                                    <div>
                                        <input type="button" class="button" rel="<%= Bridgeline.iAPPS.Resources.GUIStrings.Products %>" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.UpdateIndex %>" />
                                    </div>

                                </div>
                            </li>
                            <li>
                                <div>
                                    <div>
                                        <span><%= Bridgeline.iAPPS.Resources.GUIStrings.Metadata %></span>
                                    </div>
                                    <div>
                                         <!--Please do not transfer/change the rel="metadata" to read from a resource file, it will break search reprocess metadata -->
                                        <input type="button" class="button" rel="Metadata" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.ReprocessMetadata %>" />
                                    </div>
                                </div>
                            </li>
                    </div>

                </div>
                </ul>

                    </div>
                </div>
            </asp:View>
        </asp:MultiView>


        <%--  <div class="form-row">
            <label class="form-label" id="lblItemsToIndex" runat="server" visible="false"><%= Bridgeline.iAPPS.Resources.GUIStrings.IndexRebuildStatus %></label>
            <div class="form-value">


                <asp:Repeater runat="server" ID="uxRebuildStatus">
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    <ItemTemplate>

                        <li><%# Container.DataItem.ToString() %></li>

                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>


            </div>
        </div>--%>

        <div class="form-row">
            <div class="form-value">
                <asp:Button ID="btnNewIndex" runat="server" Text="<%$ Resources:GUIStrings, BuildNewIndex %>" CssClass="button" Style="margin-right: 10px; display: none;" OnClick="btnNewIndex_Click" />
                <asp:Button ID="btnRebuildIndex" runat="server" Text="<%$ Resources:GUIStrings, RebuildExistingIndex %>" CssClass="button" Style="display: none;" OnClick="btnRebuildIndex_Click" />
            </div>
        </div>
    </asp:PlaceHolder>
</asp:Content>

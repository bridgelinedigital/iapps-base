﻿<%@ Page Language="C#" MasterPageFile="~/Administration/Search/SearchSettings.master" AutoEventWireup="true"
    CodeBehind="SearchSiteSettings.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.SearchSiteSettings" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            setTimeout(function () {
                iAPPS_Tooltip.Initialize('rel', 'title', 'top');
            }, 1000);
        });      
      
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <asp:Button ID="btnReinitialize" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings,ReInitializeIndex %>"
        OnClick="btnReinitialize_Click" OnClientClick="return confirm('Reinitialize would purge the existing indexes. Would you like to go ahead with this option?');" ValidationGroup="SearchSettings" CausesValidation="true" />
    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true" ValidationGroup="SearchSettings" />

</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">    
    <div class="form-row">
        <label class="form-label">
            <%= Bridgeline.iAPPS.Resources.GUIStrings.ISYSVersion %>
        </label>
        <div class="form-value">
            <asp:DropDownList ID="ddlISYSVersion" runat="server" OnSelectedIndexChanged="ddlISYSVersion_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Text="ISYS Version 9.7" />
                <asp:ListItem Text="ISYS Version 10.0" />
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.IndexType %></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlIndexType" runat="server" OnSelectedIndexChanged="ddlIndexType_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Admin %>" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Product %>" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Website %>" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Custom %>" />
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-row">
            <div style="float: left; width: 75%;">
                <label ID="lblServerName" class="form-label" runat="server" visible="false"><span class="req">*</span><%= Bridgeline.iAPPS.Resources.GUIStrings.ServerName %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtServerName" runat="server" Width="600" Visible="false"/>
                    <asp:RequiredFieldValidator runat="server" ID="reqtxtServerName" ControlToValidate="txtServerName"
                        ValidationGroup="SearchSettings" SetFocusOnError="true" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterDetailsForServerName %>" Display="None"/>
                    <%--<asp:RegularExpressionValidator ID="regtxtServerName" runat="server" ControlToValidate="txtServerName"
                        ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterValidServerName %>" Display="None" SetFocusOnError="true"
                        ValidationExpression="([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)" ValidationGroup="SearchSettings" />--%>
                </div>
            </div>
            <div style="float: right; width: 25%;">
                <label ID="lblPortNo" class="form-label" style="min-width: 60px;"  runat="server" visible="false"><span class="req">*</span><%= Bridgeline.iAPPS.Resources.GUIStrings.PortNo %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtPortNo" runat="server" Width="60" Visible="false" />
                    <asp:RequiredFieldValidator runat="server" ID="reqtxtPortNo" ControlToValidate="txtPortNo"
                        ValidationGroup="SearchSettings" SetFocusOnError="true" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterDetailsForPortNumber %>" Display="None"/>
                </div>
            </div>
        </div>
    <div class="form-row">
            <label ID="lblCustomerName" class="form-label"  runat="server" visible="false"><span class="req">*</span><%= Bridgeline.iAPPS.Resources.GUIStrings.CustomerName %></label>
            <div class="form-value">
                <asp:TextBox ID="txtCustomerName" runat="server" Width="600" Visible="false"/>
                <asp:RequiredFieldValidator runat="server" ID="reqtxtCustomerName" ControlToValidate="txtCustomerName"
                    ValidationGroup="SearchSettings" SetFocusOnError="true" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterDetailsForCustomerName %>" Display="None"/>
            </div>
        </div>
    <asp:Panel ID="configPanel" runat="server" Visible="false">        
        <div class="form-row">
            <label class="form-label" id="lblCacheDocuments" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.CacheDocuments %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.CacheDocumentsTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlCacheDocuments" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblCacheMetadata" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.CacheMetaData %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.CacheMetaDataTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlCacheMetaData" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblConcurrent" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.Concurrent %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.ConcurrentTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlConcurrent" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblDateRecognition" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.DateRecognition %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.DateRecognitionTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlDateRecognition" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblDotHandling" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.DotHandling %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.DotHandlingTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlDotHandling" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblEntityRecognition" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.EntityRecognition %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.EntityRecognitionTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlEntityRecognition" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblFuzzyPrecompensation" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.FuzzyPrecompensation %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.FuzzyPrecompensationTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlFuzzyPrecompensation" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblInclude" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.Include %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.IncludeTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtInclude" runat="server" Width="600" Visible="false" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblInsignificant" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.Insignificant %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.InsignificantTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtInsignificant" runat="server" Width="600" Visible="false" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblMetaDataLimit" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.MetaDataLimit %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.MetaDataLimitTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtMetaDataLimit" runat="server" Width="600" Visible="false" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblLanguageEncoding" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.LanguageEncoding %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.LanguageEncodingTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlLanguageEncoding" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, English %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, EnglishIgnoreAccents %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Korean %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, ChineseTraditional %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, ChineseHongKong %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Japanese %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Arabic %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, ChineseSimplified %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Cyrillic %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Greek %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Turkish %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Hebrew %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Vietnamse %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Baltic %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblUnicodeSupport" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.UnicodeSupport %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.UnicodeSupportTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlUnicodeSupport" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblNumberRecognition" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.NumberRecognition %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.NumberRecognitionTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlNumberRecognition" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblSignificant" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.Significant %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.SignificantTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtSignificant" Visible="false" runat="server" Width="600" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" id="lblLanguageDetect" visible="false" runat="server">
                <%= Bridgeline.iAPPS.Resources.GUIStrings.LanguageDetect %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.LanguageDetectTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlLanguageDetect" Visible="false" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
          <asp:PlaceHolder ID="phIndexTerms" runat="server" Visible="false">
            <label class="form-label" id="lblIndexTerms" visible="false" runat="server"><%= GUIStrings.IndexPageTags %></label>
            <div class="form-value text-value">
                <asp:CheckBox ID="chkIndexPageTags" runat="server" AutoPostBack="true" OnCheckedChanged="chkIndexPageTags_CheckedChanged" />
            </div>
          </asp:PlaceHolder>
        </div>
    </asp:Panel>
</asp:Content>

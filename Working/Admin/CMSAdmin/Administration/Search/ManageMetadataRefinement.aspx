﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Search/SearchSettings.master" AutoEventWireup="true"
    CodeBehind="ManageMetadataRefinement.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageMetadataRefinement" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function upMetadataRefinement_OnLoad() {
            $(".metadata-refinements").gridActions({
                objList: $(".metadata-refinements-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upMetadataRefinement_Callback(sender, eventArgs);
                },
                onItemSelect: function (sender, eventArgs) {
                    upMetadataRefinement_ItemSelect(sender, eventArgs);
                }
            });
        }

        function upMetadataRefinement_OnRenderComplete() {
            $(".metadata-refinements").gridActions("bindControls");
        }

        function upMetadataRefinement_OnCallbackComplete() {
            $(".metadata-refinements").gridActions("displayMessage", {
                complete: function () {
                    if (gridActionsJson.ResponseCode == 100)
                        CanceliAppsAdminPopup(true);
                }
            });
        }

        function upMetadataRefinement_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];            
            var selectedTitle = "";
            if (eventArgs && eventArgs.selectedItem)
                selectedTitle = eventArgs.selectedItem.getValue("FileName");
          
            switch (gridActionsJson.Action) {
                case "AddNew":
                    doCallback = false;
                    $("#txtField").val('');
                    $("#ddlType").val('-1');
                    $('#divMultivalue').hide();
                    OpenAddMetadata();
                    break;
                case "Edit":
                    doCallback = false;
                    $("#divMetadata").iAppsDialog("open");
                    selectedField = gridActionsJson.SelectedItems[0];
                    $("#txtField").val(eventArgs.selectedItem.getValue("MetaRefField"));
                    $("#ddlType").val(eventArgs.selectedItem.getValue("MetaRefType"));
                    $("#txtMultivalueDelimiter").val(eventArgs.selectedItem.getValue("MetaRefDelimiter"));
                    OpenAddMetadata();
                    fn_ToggleMultivalue($("#ddlType"));
                    break;
                case "Delete":
                    doCallback = window.confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, DeleteConfirmation %>' />");                    
                    gridActionsJson.CustomAttributes["MetaRefField"] = gridActionsJson.SelectedItems[0];
                    break;
            }


            if (doCallback) {
               
                if (gridActionsJson.SelectedItems.length > 0) {
                    gridActionsJson.CustomAttributes["SelectedItem"] = gridActionsJson.SelectedItems[0];                   
                    gridActionsJson.SelectedItems = [];
                }               
               
                upMetadataRefinement.set_callbackParameter(JSON.stringify(gridActionsJson));                
                upMetadataRefinement.callback();
            }
        }

        function upMetadataRefinement_ItemSelect(sender, eventArgs) {
            if (gridActionsJson.SelectedItems.length > 0) {
                var selectedItemId = gridActionsJson.SelectedItems[0];
            }            
        }

        function OpenAddMetadata() {
            $("#divMetadata").iAppsDialog("open");
            return false;
        }

        function CancelAddMetadata() {
            $("#divMetadata").iAppsDialog("close");
            return false;
        }

        function fn_ToggleMultivalue(objDropdown) {
            var selectedType = $(objDropdown).val();
            if (selectedType.toLowerCase() == 'multivalue') {
                $('#divMultivalue').show();
            }
            else {
                $('#divMultivalue').hide();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="metadata-refinements">
        <iAppsControls:CallbackPanel ID="upMetadataRefinement" runat="server" OnClientCallbackComplete="upMetadataRefinement_OnCallbackComplete"
            OnClientRenderComplete="upMetadataRefinement_OnRenderComplete" OnClientLoad="upMetadataRefinement_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvMetadataRefinement" runat="server" ItemPlaceholderID="phMetadataRefinementList">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            <asp:Localize ID="lc100" runat="server" Text="<%$ Resources:GUIStrings, NoRecordsFound %>" />
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="grid-section">
                            <div class="collection-table fat-grid metadata-refinements-list">
                                <asp:PlaceHolder ID="phMetadataRefinementList" runat="server" />
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                            <div class="grid-item collection-row clear-fix" objectid='<%# Eval("MetaRefField")%>'>
                                <div class="first-cell">
                                    <%--<%# gridActionsDTO.PageSize * (gridActionsDTO.PageNumber - 1) + Container.DisplayIndex + 1 %>--%>
                                </div>
                                <div class="middle-cell">
                                    <h4 datafield="MetaRefField"><%# Eval("MetaRefField") %></h4>
                                </div>
                                <div class="last-cell">
                                    <div class="child-row" datafield="MetaRefType">
                                        <%# Eval("MetaRefType") %>
                                    </div>
                                    <%# Eval("MetaRefType").ToString().ToLower().Equals("multivalue") ? 
                                        "<div class=\"child-row\">" + Bridgeline.iAPPS.Resources.GUIStrings.Delimiter + ": <span datafield=\"MetaRefDelimiter\">" + Eval("MetaRefDelimiter") + "</span></div>" : "" %>
                                    
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
    <div class="iapps-modal add-metadata" id="divMetadata" style="display: none;">
        <div class="modal-header clear-fix">
            <h2><%= Bridgeline.iAPPS.Resources.GUIStrings.MetadataRefinement %></h2>
        </div>
        <div class="modal-content clear-fix">
            <asp:ValidationSummary ID="vsAddMetadata" runat="server" DisplayMode="List" 
                ShowMessageBox="true" ShowSummary="false" ValidationGroup="AddMetaData" />
            <div class="form-row">
                <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.Field %><span class="req">*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtField" runat="server" ClientIDMode="Static" Width="300" Text="" />
                    <asp:RequiredFieldValidator ID="rfvField" runat="server" ControlToValidate="txtField"
                        Display="None" ErrorMessage="<%$ Resources:GUIStrings, PleaseProvideAValueForField %>" ValidationGroup="AddMetaData" />
                    <asp:RegularExpressionValidator ID="revField" runat="server" ControlToValidate="txtField" ValidationGroup="AddMetaData"
                        Display="None" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInField %>" ValidationExpression="^[^<>]+$" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.Type %><span class="req">*</span></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlType" runat="server" ClientIDMode="Static" Width="310" 
                        OnSelectedIndexChanged="ddlType_SelectedIndexChanged" onchange="fn_ToggleMultivalue(this);">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" Value="-1" />
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlType" Display="None"
                        InitialValue="-1" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAValueForType %>" ValidationGroup="AddMetaData" />
                </div>
            </div>
            <div class="form-row" style="display: none;" id="divMultivalue">
                <label ID="lblMultivalue" class="form-label" runat="server" visible="true"><%= Bridgeline.iAPPS.Resources.GUIStrings.Delimiter %></label>
                <div class="form-value">
                   <asp:TextBox ID="txtMultivalueDelimiter" runat="server" ClientIDMode="Static" Width="300" Text=";" Visible="true"/>
                </div>
            </div>        
        </div>
        <div class="modal-footer clear-fix">
            <input type="button" class="button" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.Cancel %>" onclick="CancelAddMetadata()" />
            <asp:Button ID="btnSave" CssClass="primarybutton" runat="server" Text="<%$ Resources:GUIStrings, Save %>" 
                OnClick="btnSave_Click" ValidationGroup="AddMetaData" />
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Administration/Search/SearchSettings.master" AutoEventWireup="true"
    CodeBehind="ManageCommonWords.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageCommonWords" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function upCommonWords_OnLoad() {
            $(".common-words").gridActions({
                objList: $(".common-words-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upCommonWords_Callback(sender, eventArgs);
                },
                onItemSelect: function (sender, eventArgs) {
                    upCommonWords_ItemSelect(sender, eventArgs);
                }
            });
        }

        function upCommonWords_OnRenderComplete() {
            $(".common-words").gridActions("bindControls");
        }

        function upCommonWords_OnCallbackComplete() {
            $(".common-words").gridActions("displayMessage", {
                complete: function () {
                    if (gridActionsJson.ResponseCode == 100)
                        CanceliAppsAdminPopup(true);
                }
            });
        }

        function upCommonWords_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            var selectedTitle = "";
            if (eventArgs && eventArgs.selectedItem)
                selectedTitle = eventArgs.selectedItem.getValue("FileName");

            switch (gridActionsJson.Action) {
                case "AddNew":
                    doCallback = false;
                    OpenCommonWords();
                    break;
                case "Delete":
                    doCallback = true;
                    window.confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, DeleteConfirmation %>' />");
                    break;
            }

            if (doCallback) {
                if (gridActionsJson.SelectedItems.length > 0) {
                    gridActionsJson.CustomAttributes["SelectedItem"] = gridActionsJson.SelectedItems[0];
                    gridActionsJson.SelectedItems = [];
                }

                upCommonWords.set_callbackParameter(JSON.stringify(gridActionsJson));
                upCommonWords.callback();
            }
        }

        function RefreshCommonWords() {
            $(".common-words").gridActions("callback");
        }

        function upCommonWords_ItemSelect(sender, eventArgs) {
            var selectedItemId = gridActionsJson.SelectedItems[0];

        }

        function OpenCommonWords() {
            $("#txtCommonWords").val("");
            $("#divCommonWords").iAppsDialog("open");
            return false;
        }

        function CancelCommonWords() {
            $("#divCommonWords").iAppsDialog("close");
            return false;
        }

        function AddCommonWord() {
            if (Page_ClientValidate("CommonWords")) {
                gridActionsJson.CustomAttributes["NewCommonWord"] = $("#txtCommonWords").val();
                $(".common-words").gridActions("callback", "Add");

                $("#divCommonWords").iAppsDialog("close");
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p style="margin-bottom: 10px;"><%= Bridgeline.iAPPS.Resources.GUIStrings.CommonWordsInstructions %></p>
    <div class="common-words">
        <iAppsControls:CallbackPanel ID="upCommonWords" runat="server" OnClientCallbackComplete="upCommonWords_OnCallbackComplete"
            OnClientRenderComplete="upCommonWords_OnRenderComplete" OnClientLoad="upCommonWords_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvCommonWords" runat="server" ItemPlaceholderID="phCommonWordsList">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            <asp:Localize ID="lc100" runat="server" Text="<%$ Resources:GUIStrings, NoRecordsFound %>" />
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="grid-section">
                            <div class="collection-table fat-grid common-words-list">
                                <asp:PlaceHolder ID="phCommonWordsList" runat="server" />
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                            <div class="grid-item collection-row clear-fix" objectid='<%# Eval("CommonWords")%>'>
                                <div class="first-cell">
                                 <%--   <%# gridActionsDTO.PageSize * (gridActionsDTO.PageNumber - 1) + Container.DisplayIndex + 1 %>--%>
                                </div>
                                <div class="middle-cell">
                                    <p>
                                        <%# Eval("CommonWords") %>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
    <div class="iapps-modal add-common-words" id="divCommonWords" style="display: none;">
        <div class="modal-header clear-fix">
            <h2><%= Bridgeline.iAPPS.Resources.GUIStrings.CommonWords %></h2>
        </div>
        <div class="modal-content clear-fix">
            <div class="form-row">
                <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.CommonWords %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtCommonWords" runat="server" ClientIDMode="Static" Width="300" Text="" TextMode="MultiLine" ValidationGroup="CommonWords" />
                    <asp:RequiredFieldValidator runat="server" ID="reqtxtCommonWords" ControlToValidate="txtCommonWords"
                        ValidationGroup="CommonWords" SetFocusOnError="true" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValueForCommonWords %>" Display="None" />
                    <asp:RegularExpressionValidator ID="regtxtCommonWords" runat="server" ControlToValidate="txtCommonWords"
                        ErrorMessage="<%$ Resources:JSMessages, UseSemiColonsToSeparateWords %>" Display="None" SetFocusOnError="true"
                        ValidationExpression="^\w+(;\w+)*$" ValidationGroup="CommonWords" />
                </div>
            </div>
        </div>
        <div class="modal-footer clear-fix">
            <input type="button" class="button" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.Cancel %>" onclick="CancelCommonWords()" />
            <asp:Button ID="btnSave" CssClass="primarybutton" runat="server" Text="<%$ Resources:GUIStrings, Save %>" OnClientClick="return AddCommonWord();" ValidationGroup="CommonWords" />
            <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true" ValidationGroup="CommonWords" />
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Administration/Search/SearchSettings.master" AutoEventWireup="true"
    CodeBehind="ManageSynonymRings.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageSynonymRings" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function upSynonymRings_OnLoad() {
            $(".synonym-rings").gridActions({
                objList: $(".synonym-rings-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upSynonymRings_Callback(sender, eventArgs);
                },
                onItemSelect: function (sender, eventArgs) {
                    upSynonymRings_ItemSelect(sender, eventArgs);
                }
            });
        }

        function upSynonymRings_OnRenderComplete() {
            $(".synonym-rings").gridActions("bindControls");
        }

        function upSynonymRings_OnCallbackComplete() {
            $(".synonym-rings").gridActions("displayMessage", {
                complete: function () {
                    if (gridActionsJson.ResponseCode == 100)
                        CanceliAppsAdminPopup(true);
                }
            });
        }

        function upSynonymRings_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            var selectedTitle = "";
            if (eventArgs && eventArgs.selectedItem)
                selectedTitle = eventArgs.selectedItem.getValue("FileName");

            switch (gridActionsJson.Action) {
                case "AddNew":
                    doCallback = false;
                    OpenSynonymRings();
                    break;
                case "Edit":
                    $("#divSynonymRings").iAppsDialog("open");
                    selectedItemId = gridActionsJson.SelectedItems[0];                   
                    $("#txtSynonymRings").val(eventArgs.selectedItem.getValue("SynonymRings"));
                    doCallback = true; 
                    break;
                case "Delete":
                    doCallback = window.confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, DeleteConfirmation %>' />");
                    break;
            }

            if (doCallback) {
                if (gridActionsJson.SelectedItems.length > 0) {
                    gridActionsJson.CustomAttributes["SelectedItem"] = gridActionsJson.SelectedItems[0];
                    gridActionsJson.SelectedItems = [];
                }

                upSynonymRings.set_callbackParameter(JSON.stringify(gridActionsJson));
                upSynonymRings.callback();
            }
        }

        function RefreshSynonymRings() {
            $(".synonym-rings").gridActions("callback");
        }

        function upSynonymRings_ItemSelect(sender, eventArgs) {
            var selectedItemId = gridActionsJson.SelectedItems[0];
        }

        function OpenSynonymRings() {
            $("#txtSynonymRings").val("");
            $("#divSynonymRings").iAppsDialog("open");
            return false;
        }

        function CancelSynonymRings() {
            $("#divSynonymRings").iAppsDialog("close");
            return false;
        }

        function AddSynonyms() {
            if (Page_ClientValidate("SynonymRings")) {
                gridActionsJson.CustomAttributes["NewSynonyms"] = $("#txtSynonymRings").val();
                $(".synonym-rings").gridActions("callback", "Add");

                $("#divSynonymRings").iAppsDialog("close");
            }

            return false;
        }
        
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="synonym-rings">
        <iAppsControls:CallbackPanel ID="upSynonymRings" runat="server" OnClientCallbackComplete="upSynonymRings_OnCallbackComplete"
            OnClientRenderComplete="upSynonymRings_OnRenderComplete" OnClientLoad="upSynonymRings_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvSynonymRings" runat="server" ItemPlaceholderID="phSynonymRingsList">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            <asp:Localize ID="lc100" runat="server" Text="<%$ Resources:GUIStrings, NoRecordsFound %>" />
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="grid-section">
                            <div class="collection-table fat-grid synonym-rings-list">
                                <asp:PlaceHolder ID="phSynonymRingsList" runat="server" />
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                            <div class="grid-item collection-row clear-fix" objectid='<%# Eval("SynonymRings")%>'>
                                <div class="first-cell">
                                    <%--<%# gridActionsDTO.PageSize * (gridActionsDTO.PageNumber - 1) + Container.DisplayIndex + 1 %>--%>
                                </div>
                                <div class="middle-cell">
                                    <p>
                                         <h4 datafield="SynonymRings"><%# Eval("SynonymRings") %></h4>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
    <div class="iapps-modal add-synonym-rings" id="divSynonymRings" style="display: none;">
        <div class="modal-header clear-fix">
            <h2><%= Bridgeline.iAPPS.Resources.GUIStrings.SynonymRings %></h2>
        </div>
        <div class="modal-content clear-fix">
            <div class="form-row">
                <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.SynonymRings %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtSynonymRings" runat="server" ClientIDMode="Static" Width="300" Text="" TextMode="MultiLine" ValidationGroup="SynonymRings" />
                    <asp:RequiredFieldValidator runat="server" ID="reqtxtSynonymRings" ControlToValidate="txtSynonymRings"
                        ValidationGroup="SynonymRings" SetFocusOnError="true" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValueForSynonymRings %>" Display="None" />
                    <asp:RegularExpressionValidator ID="regtxtSynonymRings" runat="server" ControlToValidate="txtSynonymRings"
                        ErrorMessage="<%$ Resources:JSMessages, UseSemiColonsToSeparateWords %>" Display="None" SetFocusOnError="true"
                        ValidationExpression="(^\w.*)+(;\w.*)" ValidationGroup="SynonymRings" />
                </div>
            </div>
        </div>
        <div class="modal-footer clear-fix">
            <input type="button" class="button" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.Cancel %>" onclick="CancelSynonymRings()" />
            <asp:Button ID="btnSave" CssClass="primarybutton" runat="server" Text="<%$ Resources:GUIStrings, Save %>"  OnClientClick="return AddSynonyms();" ValidationGroup="SynonymRings" />
            <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true" ValidationGroup="SynonymRings" />
        </div>
    </div>
</asp:Content>

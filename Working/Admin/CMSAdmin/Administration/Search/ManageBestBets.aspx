﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Search/SearchSettings.master" AutoEventWireup="true"
    StylesheetTheme="General" CodeBehind="ManageBestBets.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageBestBets" ValidateRequest="false" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function upBestBets_OnLoad() {
            $(".best-bets").gridActions({
                objList: $(".best-bets-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upBestBets_Callback(sender, eventArgs);
                },
                onItemSelect: function (sender, eventArgs) {
                    upBestBets_ItemSelect(sender, eventArgs);
                }
            });
        }

        function upBestBets_OnRenderComplete() {
            $(".best-bets").gridActions("bindControls");
        }

        function upBestBets_OnCallbackComplete() {
            $(".best-bets").gridActions("displayMessage", {
                complete: function () {
                    if (gridActionsJson.ResponseCode == 100)
                        CanceliAppsAdminPopup(true);
                }
            });
        }

        function upBestBets_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];          
            var selectedTitle = "";
            if (eventArgs && eventArgs.selectedItem)
                selectedTitle = eventArgs.selectedItem.getValue("FileName");

            switch (gridActionsJson.Action) {
                case "AddNew":
                    doCallback = false;
                    var selectedIndexType = gridActionsJson.CustomAttributes["SelectedIndexType"];
                    OpeniAppsAdminPopup("ManageBestBetQuery", "&SelectedIndexType=" + selectedIndexType, "RefreshBestBets");
                    break;
                case "Edit":
                    doCallback = false;
                    var selectedItems = gridActionsJson.CustomAttributes["SelectedItems"];
                    var modifiedItems = selectedItems.replaceAll("&", "%26");                  
                    var selectedIndexType = gridActionsJson.CustomAttributes["SelectedIndexType"];
                    OpeniAppsAdminPopup("ManageBestBetQuery", "Action=Edit&SelectedValues=" + modifiedItems + "&SelectedIndexType=" + selectedIndexType, "RefreshBestBets");
                    break;
                case "Delete":
                    var selectedItems = gridActionsJson.CustomAttributes["SelectedItems"];
                    var selectedIndexType = gridActionsJson.CustomAttributes["SelectedIndexType"];
                    doCallback = window.confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, DeleteConfirmation %>' />");
                    break;
            }

            if (doCallback) {

                if (gridActionsJson.SelectedItems.length > 0) {
                    gridActionsJson.CustomAttributes["SelectedItems"] = gridActionsJson.SelectedItems[0];
                    gridActionsJson.SelectedItems = [];
                }

                upBestBets.set_callbackParameter(JSON.stringify(gridActionsJson));
                upBestBets.callback();
            }
        }

        function RefreshBestBets() {
            $(".best-bets").gridActions("callback");
        }

        function upBestBets_ItemSelect(sender, eventArgs) {
            if (gridActionsJson.SelectedItems.length > 0) {
                var selectedItemId = gridActionsJson.SelectedItems[0];
                gridActionsJson.CustomAttributes["SelectedItems"] = gridActionsJson.SelectedItems[0];
            }

        }
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="best-bets">
        <iAppsControls:CallbackPanel ID="upBestBets" runat="server" OnClientCallbackComplete="upBestBets_OnCallbackComplete"
            OnClientRenderComplete="upBestBets_OnRenderComplete" OnClientLoad="upBestBets_OnLoad" >
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvBestBets" runat="server" ItemPlaceholderID="phBestBetsList">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            <asp:Localize ID="lcEmptyList" runat="server" Text="<%$ Resources:GUIStrings, NoRecordsFound %>" />
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="grid-section">
                            <div class="collection-table fat-grid best-bets-list">
                                <asp:PlaceHolder ID="phBestBetsList" runat="server" />
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                            <div class="grid-item collection-row clear-fix" objectid='<%# System.Web.HttpUtility.HtmlEncode(Eval("Title"))%>|<%# Eval("Query")%>|<%# System.Web.HttpUtility.HtmlEncode(Eval("Description"))%>'>
                                <div class="first-cell">
                                   <%-- <%# gridActionsDTO.PageSize * (gridActionsDTO.PageNumber - 1) + Container.DisplayIndex + 1 %>--%>
                                </div>
                                <div class="middle-cell">
                                    <h4><%# Eval("Query") %></h4>
                                    <p>
                                        <%# Eval("Title") %>&nbsp;|&nbsp;<%# Eval("Description") %>
                                    </p>
                                </div>
                                <div class="last-cell">
                                    <div class="child-row">
                                        <%# Eval("FileName") %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Administration/Search/SearchSettings.master"
    AutoEventWireup="true" ValidateRequest="false" CodeBehind="FrontEndSearchGlobalSettings.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.FrontEndSearchGlobalSettings" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <asp:Button runat="server" ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
        ValidationGroup="callback" OnClick="btnSave_Onclick" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="search-settings">
          <label  id="lblError" runat="server" visible="false"></label>
         <asp:PlaceHolder ID="phIndexStatistics" runat="server" Visible="true">
        <div class="form-row">
            <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.Indexes %></label>
            <div class="form-value">
                <table cellpadding="0" cellspacing="0" width="962" class="grid">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th><%= Bridgeline.iAPPS.Resources.GUIStrings.Website %></th>
                            <th><%= Bridgeline.iAPPS.Resources.GUIStrings.Admin %></th>
                            <th><%= Bridgeline.iAPPS.Resources.GUIStrings.Products %></th>
                            <th><%= Bridgeline.iAPPS.Resources.GUIStrings.Custom %></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="odd-row">
                            <td><%= Bridgeline.iAPPS.Resources.GUIStrings.LastUpdated %></td>
                            <td>
                                <asp:Literal ID="litWebsiteStatistics" runat="server" Visible="false" />
                                <asp:Literal ID="litWebsiteLastUpdated" runat="server" Text="" />
                            </td>
                            <td>
                                <asp:Literal ID="litAdminStatistics" runat="server" Visible="false" />
                                <asp:Literal ID="litAdminLastUpdated" runat="server" Text="" />
                            </td>
                            <td>
                                <asp:Literal ID="litProductStatistics" runat="server" Visible="false" />
                                <asp:Literal ID="litProductsLastUpdated" runat="server" Text="" />
                            </td>
                            <td>
                                <asp:Literal ID="litCustomStatistics" runat="server" Visible="false" />
                                <asp:Literal ID="litCustomLastUpdated" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr class="even-row">
                            <td><%= Bridgeline.iAPPS.Resources.GUIStrings.TotalDocuments %></td>
                            <td>
                                <asp:Literal ID="litWebsiteTotalDocuments" runat="server" /></td>
                            <td>
                                <asp:Literal ID="litAdminTotalDocuments" runat="server" /></td>
                            <td>
                                <asp:Literal ID="litProductsTotalDocuments" runat="server" /></td>
                            <td>
                                <asp:Literal ID="litCustomTotalDocuments" runat="server" /></td>
                        </tr>
                        <tr class="odd-row">
                            <td><%= Bridgeline.iAPPS.Resources.GUIStrings.IndexLanguage %></td>
                            <td>
                                <asp:Literal ID="litWebsiteIndexLanguage" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litAdminIndexLanguage" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litProductsIndexLanguage" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litCustomIndexLanguage" runat="server" Text="" /></td>
                        </tr>
                        <tr class="even-row">
                            <td><%= Bridgeline.iAPPS.Resources.GUIStrings.TotalWords %></td>
                            <td>
                                <asp:Literal ID="litWebsiteTotalWords" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litAdminTotalWords" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litProductsTotalWords" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litCustomTotalWords" runat="server" Text="" /></td>
                        </tr>
                        <tr class="odd-row">
                            <td><%= Bridgeline.iAPPS.Resources.GUIStrings.TotalWordsIndexed %></td>
                            <td>
                                <asp:Literal ID="litWebsiteTotalWordsIndexed" runat="server" /></td>
                            <td>
                                <asp:Literal ID="litAdminTotalWordsIndexed" runat="server" /></td>
                            <td>
                                <asp:Literal ID="litProductsTotalWordsIndexed" runat="server" /></td>
                            <td>
                                <asp:Literal ID="litCustomTotalWordsIndexed" runat="server" /></td>
                        </tr>
                        <tr class="even-row">
                            <td><%= Bridgeline.iAPPS.Resources.GUIStrings.IndexSize %></td>
                            <td>
                                <asp:Literal ID="litWebsiteIndexSize" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litAdminIndexSize" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litProductsIndexSize" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litCustomIndexSize" runat="server" Text="" /></td>
                        </tr>
                        <tr class="odd-row">
                            <td><%= Bridgeline.iAPPS.Resources.GUIStrings.TotalCommonWords %></td>
                            <td>
                                <asp:Literal ID="litWebsiteTotalCommonWords" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litAdminTotalCommonWords" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litProductsTotalCommonWords" runat="server" Text="" /></td>
                            <td>
                                <asp:Literal ID="litCustomTotalCommonWords" runat="server" Text="" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phOldConfig" runat="server" Visible="true">
            <div class="form-row">
                <label class="form-label">
                    <%= Bridgeline.iAPPS.Resources.SiteSettings.IncreaseMetaDataRelevance%><img src="../../App_Themes/General/images/icon-help.gif"
                        rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.IncreaseMetaDataRelevanceTooltip %>"
                        alt="" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlMetaDataRelevance" runat="server">
                        <asp:ListItem Text="0" Value="0" />
                        <asp:ListItem Text="1" Value="1" />
                        <asp:ListItem Text="2" Value="2" />
                        <asp:ListItem Text="3" Value="3" />
                        <asp:ListItem Text="4" Value="4" />
                        <asp:ListItem Text="5" Value="5" />
                        <asp:ListItem Text="6" Value="6" />
                        <asp:ListItem Text="7" Value="7" />
                        <asp:ListItem Text="8" Value="8" />
                        <asp:ListItem Text="9" Value="9" />
                        <asp:ListItem Text="10" Value="10" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= Bridgeline.iAPPS.Resources.SiteSettings.ConflationLevel%><img src="../../App_Themes/General/images/icon-help.gif"
                        rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ConflationLevelTooltip %>" alt="" /></label>
                <div class="form-value">
                    <asp:CheckBox ID="chkConflation" runat="server" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= Bridgeline.iAPPS.Resources.SiteSettings.Synonyms%><img src="../../App_Themes/General/images/icon-help.gif"
                        rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SynonymsTooltip %>" alt="" /></label>
                <div class="form-value">
                    <asp:CheckBox ID="chkSynonyms" runat="server" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= Bridgeline.iAPPS.Resources.SiteSettings.Thesaurus%><img src="../../App_Themes/General/images/icon-help.gif"
                        rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ThesaurusTooltip %>" alt="" /></label>
                <div class="form-value">
                    <asp:CheckBox ID="chkThesaurus" runat="server" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= Bridgeline.iAPPS.Resources.SiteSettings.BestBetsThreshold%><img src="../../App_Themes/General/images/icon-help.gif"
                        rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.BestBetsThresholdToolTip %>"
                        alt="" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlBestBetsThreshold" runat="server">
                        <asp:ListItem Text="0" Value="0" />
                        <asp:ListItem Text="0.1" Value="1" />
                        <asp:ListItem Text="0.2" Value="2" />
                        <asp:ListItem Text="0.3" Value="3" />
                        <asp:ListItem Text="0.4" Value="4" />
                        <asp:ListItem Text="0.5" Value="5" />
                        <asp:ListItem Text="0.6" Value="6" />
                        <asp:ListItem Text="0.7" Value="7" />
                        <asp:ListItem Text="0.8" Value="8" />
                        <asp:ListItem Text="0.9" Value="9" />
                        <asp:ListItem Text="1" Value="10" />
                    </asp:DropDownList>
                </div>
            </div>
        </asp:PlaceHolder>
       
        <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
            ValidationGroup="callback" />
    </div>
</asp:Content>

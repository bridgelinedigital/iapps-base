﻿<%@ Page Language="C#" Theme="General" AutoEventWireup="true" CodeBehind="WorkflowEmailHandler.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.WorkflowEmailHandler" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title></title>
</head>
<body>
    <div style="position: relative; min-height: 100%;">
        <form id="frmMain" runat="server">
        <!-- Header Starts -->
        <div class="header-wrapper" id="headerWrapper">
            <div class="product-nav-wrapper">
                <div class="product-nav clear-fix">
                </div>
            </div>
            <div class="site-utility clear-fix">
            </div>
        </div>
        <!-- Header Ends -->
        <div class="wrapper clear-fix" id="wrapper">
            <div class="breadcrumb clear-fix">
            </div>
            <!-- Content Section Starts -->
            <div class="contentSection clear-fix" runat="server" id="contentSection" style="padding-top: 100px;">
                <h1 style="background: url('../../App_Themes/General/images/icon-workflow.png') no-repeat left top;
                    padding: 100px 0 120px 340px; font-size: 40px;">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Workflow %>" /></h1>
                <div class="clear-fix">
                    <div class="left-col" style="float: left; width: 300px; margin: 0 50px 0 340px;">
                        <p style="margin-bottom: 20px;">
                            <strong><asp:Literal ID="litMessage" runat="server" /></strong></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Starts -->
        <div class="footerWrapper" id="footerWrapper">
            <div class="footer" id="footer">
                <div class="leftFooter">
                </div>
                <div class="rightFooter">
                    <asp:Image ImageUrl="~/App_Themes/General/images/bridgeline-logo.png" ID="companyLogo" runat="server"
                        ToolTip="Bridgeline Digital" AlternateText="Bridgeline Digital" />
                </div>
            </div>
        </div>
        </form>
    </div>
</body>
</html>

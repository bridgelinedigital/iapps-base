﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeBehind="ManageWorkflowDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageWorkflowDetails"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdMembers_RenderComplete() {
            SetVerticalScrollStyles(grdMembers.get_id());
        }

        function workflowSequenceGrid_RenderComplete() {
            SetVerticalScrollStyles(workflowSequenceGrid.get_id());
        }
        function grdMembers_ItemExternalDrop(sender, eventArgs) {
            var newRow;
            var isExist = false;
            var targetItem = eventArgs.get_target();
            var selectedActor = eventArgs.get_item().getMember('ActorId').get_text();
            var selectedRole = eventArgs.get_item().getMember('RoleId').get_text();
            var count = workflowSequenceGrid.get_table().getRowCount();
            for (var i = 0; i < count; i++) {
                var Actorid = workflowSequenceGrid.get_table().getRow(i).get_cells(0)[0].Value;
                var Roleid = workflowSequenceGrid.get_table().getRow(i).get_cells(0)[3].Value;
                if ((Actorid == selectedActor) && (Roleid == selectedRole)) {
                    isExist = true;
                }
            }
            if (isExist == false) {
                if (targetItem != null) {
                    newRow = workflowSequenceGrid.get_table().addEmptyRow(targetItem.get_index());
                }
                else {
                    newRow = workflowSequenceGrid.get_table().addEmptyRow();
                }
                workflowSequenceGrid.beginUpdate();
                newRow.setValue(0, eventArgs.get_item().getMember('ActorId').get_text());
                newRow.setValue(1, eventArgs.get_item().getMember('ActorName').get_text());
                newRow.setValue(2, eventArgs.get_item().getMember('ActorType').get_text());
                newRow.setValue(3, eventArgs.get_item().getMember('RoleId').get_text());
                newRow.setValue(4, eventArgs.get_item().getMember('RoleName').get_text());
                workflowSequenceGrid.endUpdate();
            }
        }

        function BindUsers() {
            $('#btnSearchMember').click();
            grdMembers.set_callbackParameter($("#ddlType").val());
            grdMembers.callback();

            return false;
        }


        function DeleteWorkflowSequence_onclick() {
            workflowSequenceGrid.deleteSelected();
        }
        function AddWorkflowSequence_onclick(sender, eventArgs)//new change added this sender, eventArgs
        {
            var draggedItem;
            if (eventArgs != null)//new 
            {
                draggedItem = eventArgs.get_item();
            }
            else {
                //var targetItem = eventArgs.get_target();
                draggedItem = grdMembers.GetSelectedItems()[0];
            }
            //var draggedItemLength=grdMembers.GetSelectedItems().length;
            var isExist = false;
            if (draggedItem != null)//new        
            {
                var count = workflowSequenceGrid.get_table().getRowCount();
                var actorId = draggedItem.getMember("ActorId").get_text();
                var roleId = draggedItem.getMember("RoleId").get_text();
                for (var i = 0; i < count; i++) {
                    var Actorid = workflowSequenceGrid.get_table().getRow(i).get_cells(0)[0].Value;
                    var Roleid = workflowSequenceGrid.get_table().getRow(i).get_cells(0)[3].Value;
                    if ((Actorid == actorId) && (Roleid == roleId)) {
                        isExist = true;
                    }
                }
                if (isExist == false) {
                    var actorName = draggedItem.getMember("ActorName").get_text();
                    var actorType = draggedItem.getMember("ActorType").get_text();
                    var roleName = draggedItem.getMember("RoleName").get_text();
                    var rowitem;
                    var targetItem;
                    if (eventArgs != null)//new 
                    {
                        targetItem = eventArgs.get_target();
                        if (targetItem != null) {
                            rowitem = workflowSequenceGrid.get_table().addEmptyRow(targetItem.get_index());
                        }
                        else {
                            rowitem = workflowSequenceGrid.get_table().addEmptyRow();
                        }
                    }
                    else {
                        rowitem = workflowSequenceGrid.get_table().addEmptyRow();
                    }
                    workflowSequenceGrid.edit(rowitem);
                    rowitem.setValue(0, draggedItem.getMember("ActorId").get_text());
                    rowitem.setValue(1, draggedItem.getMember("ActorName").get_text());
                    rowitem.setValue(2, draggedItem.getMember("ActorType").get_text());
                    rowitem.setValue(3, draggedItem.getMember("RoleId").get_text());
                    rowitem.setValue(4, draggedItem.getMember("RoleName").get_text());

                    workflowSequenceGrid.editComplete();
                    workflowSequenceGrid.unSelect(rowitem);
                    workflowSequenceGrid.render();
                }
            }
        }

        function workflowSequenceGrid_onItemDelete(sender, eventArgs) {
            if (eventArgs.get_targetControl().get_id() != document.getElementById('<%=workflowSequenceGrid.ClientID%>').id) {
                workflowSequenceGrid.deleteItem(eventArgs.get_item());
            }
            else if (eventArgs.get_targetControl().get_id() == document.getElementById('<%=workflowSequenceGrid.ClientID%>').id) {
                var targetItem = eventArgs.get_target();
                var newRow;
                if (targetItem != null) {
                    workflowSequenceGrid.deleteItem(eventArgs.get_item());
                    newRow = workflowSequenceGrid.get_table().addEmptyRow(targetItem.get_index());
                }
                else {
                    workflowSequenceGrid.deleteItem(eventArgs.get_item());
                    newRow = workflowSequenceGrid.get_table().addEmptyRow();
                }
                workflowSequenceGrid.beginUpdate();
                newRow.setValue(0, eventArgs.get_item().getMember('ActorId').get_text());
                newRow.setValue(1, eventArgs.get_item().getMember('ActorName').get_text());
                newRow.setValue(2, eventArgs.get_item().getMember('ActorType').get_text());
                newRow.setValue(3, eventArgs.get_item().getMember('RoleId').get_text());
                newRow.setValue(4, eventArgs.get_item().getMember('RoleName').get_text());
                workflowSequenceGrid.endUpdate();
            }
        }


        function MoveWFMembersDown_onclick() {

            var recordcount = workflowSequenceGrid.get_recordCount();
            if (recordcount != 0) {
                var draggedItem = workflowSequenceGrid.GetSelectedItems()[0];
                var draggedItemLength = workflowSequenceGrid.GetSelectedItems().length;
                if (draggedItemLength > 0) {
                    var draggedItemIndex = workflowSequenceGrid.GetSelectedItems()[0].get_index();
                    if (draggedItemIndex != recordcount - 1) {
                        var actorId = draggedItem.getMember("ActorId").get_text();
                        var actorName = draggedItem.getMember("ActorName").get_text();
                        var actorType = draggedItem.getMember("ActorType").get_text();
                        var roleId = draggedItem.getMember("RoleId").get_text();
                        var roleName = draggedItem.getMember("RoleName").get_text();
                        var rowItemIndex = draggedItemIndex + 1;
                        workflowSequenceGrid.deleteItem(draggedItem);
                        workflowSequenceGrid.unSelect(draggedItem); //deleting row is unselect and new will be select
                        var rowitem = workflowSequenceGrid.get_table().addEmptyRow(rowItemIndex);
                        workflowSequenceGrid.edit(rowitem);

                        rowitem.setValue(0, draggedItem.getMember("ActorId").get_text());
                        rowitem.setValue(1, draggedItem.getMember("ActorName").get_text());
                        rowitem.setValue(2, draggedItem.getMember("ActorType").get_text());
                        rowitem.setValue(3, draggedItem.getMember("RoleId").get_text());
                        rowitem.setValue(4, draggedItem.getMember("RoleName").get_text());

                        workflowSequenceGrid.editComplete();
                        workflowSequenceGrid.select(rowitem); //                
                        workflowSequenceGrid.render();
                    }
                }
            }
        }
        function MoveWFMembersUp_onclick() {
            var recordcount = workflowSequenceGrid.get_recordCount();
            if (recordcount != 0) {
                var draggedItem; var draggedItemIndex;
                draggedItem = workflowSequenceGrid.GetSelectedItems()[0];
                var draggedItemLength = workflowSequenceGrid.GetSelectedItems().length;
                if (draggedItemLength > 0) {
                    draggedItemIndex = workflowSequenceGrid.GetSelectedItems()[0].get_index();
                    if (draggedItemIndex != 0) {
                        var actorId = draggedItem.getMember("ActorId").get_text();
                        var actorName = draggedItem.getMember("ActorName").get_text();
                        var actorType = draggedItem.getMember("ActorType").get_text();
                        var roleId = draggedItem.getMember("RoleId").get_text();
                        var roleName = draggedItem.getMember("RoleName").get_text();
                        var rowItemIndex = draggedItemIndex - 1;
                        workflowSequenceGrid.deleteItem(draggedItem);
                        workflowSequenceGrid.unSelect(draggedItem); //deleting row is unselect and new will be select
                        var rowitem = workflowSequenceGrid.get_table().addEmptyRow(rowItemIndex);

                        workflowSequenceGrid.edit(rowitem);

                        rowitem.setValue(0, draggedItem.getMember("ActorId").get_text());
                        rowitem.setValue(1, draggedItem.getMember("ActorName").get_text());
                        rowitem.setValue(2, draggedItem.getMember("ActorType").get_text());
                        rowitem.setValue(3, draggedItem.getMember("RoleId").get_text());
                        rowitem.setValue(4, draggedItem.getMember("RoleName").get_text());

                        workflowSequenceGrid.editComplete();
                        workflowSequenceGrid.select(rowitem);
                        workflowSequenceGrid.render();
                    }
                }
            }
        }

        function workflowSelection(objRbt, objRbt1, objDrp, objGrid, objTxt, objChk, objSaveWorkflowTxt) {
            if (objRbt.checked) {
                objDrp.selectedIndex = 0;
                objDrp.disabled = true;
                objTxt.value = "";
                objChk.checked = false;
                var count = objGrid.get_recordCount();
                objGrid.get_table().clearData()
                objGrid.render();
                objSaveWorkflowTxt.value = "";
            }
            if (objRbt1.checked) {
                objDrp.selectedIndex = 0;
                objDrp.disabled = false;
            }

        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" HeaderText=""
        DisplayMode="List" ShowMessageBox="True"></asp:ValidationSummary>
    <div class="dark-section-header clear-fix">
        <h2><%= GUIStrings.WorkflowDetails %></h2>
    </div>
    <div class="form-row">
        <label class="form-label" style="min-width: 20px;">
            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, Title %>" /><span
                class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtTitle" CssClass="textBoxes" runat="server" Width="280" />
            <asp:RequiredFieldValidator ID="WorkflowNameFieldValidator1" runat="server" Display="None"
                ErrorMessage="<%$ Resources: JSMessages, WorkflowNameRequired %>"
                ControlToValidate="txtTitle" EnableClientScript="true"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="WorkflowNameExpressionValidator" runat="server"
                SetFocusOnError="true" ControlToValidate="txtTitle" Display="None" ErrorMessage="<%$ Resources: JSMessages, ValidWorkflowName %>"
                ValidationExpression="^[^<>]+$"></asp:RegularExpressionValidator>
        </div>
    </div>
    <asp:PlaceHolder ID="divRadioButtons" runat="server">
        <div class="form-row">
            <div class="form-value text-value">
                <asp:RadioButton ID="RBNewWF" runat="server" Text="<%$ Resources:GUIStrings, BuildWorkflowFromScratch %>"
                    GroupName="RBWorkflow" AutoPostBack="true" Checked="true" />
            </div>
        </div>
        <div class="form-row">
            <div class="form-value text-value">
                <asp:RadioButton ID="RBExistingWF" runat="server" Text="<%$ Resources:GUIStrings, UseExistingWorkflowAsABaseColon %>"
                    GroupName="RBWorkflow" AutoPostBack="false" />
                <asp:DropDownList ID="WfName" runat="server" CssClass="textBoxes" OnSelectedIndexChanged="OnChanged"
                    AutoPostBack="true" Width="312" Enabled="false" />
                <asp:PlaceHolder ID="phWorkflowWarning" runat="server" Visible="false">
                    <span><em>Workflows having users without permission on this site are not listed.</em></span>
                </asp:PlaceHolder>
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phIsGlobal" runat="server">
        <div class="form-row">
            <div class="form-value">
                <asp:CheckBox runat="server" ID="chkIsGlobal" Text="<%$ Resources:GUIStrings, MakeThisWorkflowGloballyAvailable %>" />
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="form-row">
        <div class="form-value">
            <asp:CheckBox runat="server" ID="chkIsShared" Text="<%$ Resources:GUIStrings, SharedWorkFlow %>" />
        </div>
    </div>
    <div class="form-row">
        <div class="form-value">
            <asp:CheckBox runat="server" ID="skipUsersChk" Text="<%$ Resources:GUIStrings, SkipUserAfterNumberOfDays %>" />
            <asp:TextBox runat="server" ID="skipUsersTxt" Enabled="false" Width="38" MaxLength="2" />
            <asp:RegularExpressionValidator ID="skipuserdays" runat="server" ControlToValidate="skipUsersTxt"
                ErrorMessage="<%$ Resources: JSMessages, ValidSkipDays %>"
                ValidationExpression="\d{1,2}" Display="None"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="row clear-fix">
        <div class="dark-section-header clear-fix" style="margin-top: 50px;">
            <h2><%= GUIStrings.WorkflowSequence %></h2>
        </div>
        <div class="columns">
            <h4>
                <%= GUIStrings.AllUsersGroupsColon %></h4>
            <div class="form-row">
                <select onchange="BindUsers();" id="ddlType" runat="server" clientidmode="Static">
                </select>
                <div class="search-box iapps-client-grid-search">
                    <input id="txtSearch" type="text" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                        style="width: 160px;" />
                    <input type="button" id="btnSearchMember" class="button small-button" value="<%= GUIStrings.Filter %>"
                        onclick="SearchClientGrid('txtSearch', grdMembers);" />
                </div>
            </div>
            <div class="scroll-section">
                <ComponentArt:Grid ID="grdMembers" EditOnClickSelectedItem="false" SkinID="Default"
                    AllowVerticalScrolling="true" RunningMode="Client" Width="545" EnableViewState="true"
                    runat="server" ItemDraggingEnabled="true" AllowMultipleSelect="false" ExternalDropTargets="workflowSequenceGrid"
                    Height="300" ShowFooter="false" LoadingPanelClientTemplateId="grdMembersLoadingTemplate">
                    <ClientEvents>
                        <RenderComplete EventHandler="grdMembers_RenderComplete" />
                        <ItemExternalDrop EventHandler="grdMembers_ItemExternalDrop" />
                    </ClientEvents>
                    <Levels>
                        <ComponentArt:GridLevel AlternatingRowCssClass="alternate-row" ShowSelectorCells="false"
                            SelectorImageUrl="selector.gif" SelectorImageWidth="17" SelectorImageHeight="15"
                            DataCellCssClass="data-cell" RowCssClass="row" SelectedRowCssClass="selected-row"
                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" AllowReordering="true"
                            HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" HoverRowCssClass="hover-row">
                            <Columns>
                                <ComponentArt:GridColumn DataField="ActorId" Visible="false" IsSearchable="false" />
                                <ComponentArt:GridColumn DataCellCssClass="first-cell last-cell" DataField="ActorName"
                                    HeadingText="<%$ Resources:GUIStrings, UserGroupName %>" />
                                <ComponentArt:GridColumn DataField="ActorType" Visible="false" IsSearchable="false" />
                                <ComponentArt:GridColumn DataField="RoleId" Visible="false" IsSearchable="false" />
                                <ComponentArt:GridColumn DataField="RoleName" Visible="false" IsSearchable="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="grdMembersLoadingTemplate">
                            ## GetGridLoadingPanelContent(grdMembers) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
        </div>
        <div class="action-column">
            <img src="~/App_Themes/General/images/action-right-arrow.gif" alt="<%$ Resources:GUIStrings, AddWorkflowSequence %>"
                title="<%$ Resources:GUIStrings, AddWorkflowSequence %>" runat="server" id="IMG2"
                onclick="return AddWorkflowSequence_onclick()" />
            <img src="~/App_Themes/General/images/action-left-arrow.gif" alt="<%$ Resources:GUIStrings, RemoveWorkflowSequence %>"
                title="<%$ Resources:GUIStrings, RemoveWorkflowSequence %>" runat="server" id="IMG1"
                onclick="return DeleteWorkflowSequence_onclick()" />
        </div>
        <div class="columns right-column">
            <p>
                <%= GUIStrings.DragAndDropFromAllUsersGroupsToWorkflowSequenceGrid %>
            </p>
            <ComponentArt:Grid ID="workflowSequenceGrid" EditOnClickSelectedItem="false" SkinID="Default"
                EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" AllowEditing="false"
                RunningMode="Client" ItemDraggingEnabled="true" PageSize="100" ShowFooter="false"
                Width="545" ExternalDropTargets="workflowSequenceGrid,grdMembers" Height="276"
                runat="server" AllowVerticalScrolling="true">
                <ClientEvents>
                    <ItemExternalDrop EventHandler="workflowSequenceGrid_onItemDelete" />
                    <RenderComplete EventHandler="workflowSequenceGrid_RenderComplete" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel AlternatingRowCssClass="alternate-row" ShowSelectorCells="false"
                        SelectorImageUrl="selector.gif" SelectorImageWidth="17" SelectorImageHeight="15"
                        DataCellCssClass="data-cell" RowCssClass="row" SelectedRowCssClass="selected-row"
                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" AllowReordering="true"
                        HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="ActorId" Visible="false" />
                            <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, ActorName %>" DataCellCssClass="first-cell"
                                DataField="ActorName" FixedWidth="true" AllowReordering="False" Width="300" />
                            <ComponentArt:GridColumn DataField="ActorType" Visible="false" />
                            <ComponentArt:GridColumn DataField="RoleId" Visible="false" />
                            <ComponentArt:GridColumn DataCellCssClass="last-cell" HeadingText="<%$ Resources:GUIStrings, Role %>"
                                DataField="RoleName" FixedWidth="true" AllowReordering="False" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
            </ComponentArt:Grid>
        </div>
        <div class="action-column last-column">
            <img src="~/App_Themes/General/images/action-up-arrow.gif" runat="server" alt="<%$ Resources:GUIStrings, MoveUp %>"
                title="<%$ Resources:GUIStrings, MoveUp %>" id="IMG4" onclick="return MoveWFMembersUp_onclick()" />
            <img src="~/App_Themes/General/images/action-down-arrow.gif" runat="server" alt="<%$ Resources:GUIStrings, MoveDown %>"
                title="<%$ Resources:GUIStrings, MoveDown %>" id="IMG3" onclick="return MoveWFMembersDown_onclick()" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="buttons" runat="server" ContentPlaceHolderID="cphHeaderButtons">
    <div class="page-header clear-fix">
        <h1>
            <%= GUIStrings.AddEditWorkflow %></h1>
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
            OnClick="btnSave_Click" ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" />
        <a href="../../workflow-landing" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" /></a>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.FindAndReplace" StylesheetTheme="General"
    MasterPageFile="~/MasterPage.Master" CodeBehind="FindAndReplace.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#previewResult").iAppsDialog({ appendToEnd: false });
        });

        function upFindReplace_OnCallbackComplete(sender, eventArgs) {
            if (gridActionsJson.Action == "Preview") {
                $("#previewResult").iAppsDialog("open");

                gridActionsJson.Action = "LoadData";
            }

            $(".find-replace").gridActions("displayMessage");
        }

        function upFindReplace_OnRenderComplete(sender, eventArgs) {
            $(".find-replace").gridActions("bindControls");
            $(".find-replace").iAppsSplitter();
        }

        function upFindReplace_OnLoad(sender, eventArgs) {
            $(".find-replace").gridActions({
                objList: $(".collection-table"),
                type: "list",
                pageSize: 10,
                onCallback: function (sender, eventArgs) {
                    upFindReplace_Callback(sender, eventArgs);
                },
                onItemSelect: function (sender, eventArgs) {
                    upFindReplace_ItemSelect(sender, eventArgs);
                }
            });
        }

        function upFindReplace_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = "";
            if (eventArgs.selectedItem)
                selectedItemId = eventArgs.selectedItem.getKey();

            switch (gridActionsJson.Action) {
                case "Preview":
                    gridActionsJson.CustomAttributes["SelectedId"] = selectedItemId;
                    gridActionsJson.CustomAttributes["SelectedType"] = eventArgs.selectedItem.getValue("ItemType");
                    break;
                case "ViewPage":
                    doCallback = false;
                    JumpToFrontPage(eventArgs.selectedItem.getValue("CompleteFriendlyURL"));
                    break;
                case "Find":
                    if ($.trim($("#txtFind").val()).length == 0 || /[<>]/.test($("#txtFind").val())) {
                        doCallback = false;
                        alert(__JSMessages["PleaseEnterASearchTermSearchTermsCannotContainTheSpecialCharactersLtOrGt"]);
                    }
                    else if ($(".select-field input:checked").length == 0) {
                        doCallback = false;
                        alert(__JSMessages["PleaseSelectAtLeastOneItemTypeToLookIn"]);
                    }
                    break;
                case "Replace":
                    if ($.trim($("#txtReplace").val()).length == 0 || /[<>]/.test($("#txtReplace").val())) {
                        doCallback = false;
                        alert(__JSMessages["PleaseEnterAReplaceTermReplaceTermsCannotContainTheSpecialCharactersLtOrGt"]);
                    }
                    else if (gridActionsJson.SelectedItems.length == 0) {
                        doCallback = false;
                        alert(__JSMessages["PleaseSelectAtLeastOneItem"]);
                    }
                    else {
                        var message = __JSMessages["AreYouSureYouWantToReplaceAllInstancesOf0With1InThe2SelectedItems"];
                        doCallback = confirm(stringformat(message, $("#txtFind").val(), $("#txtReplace").val(), gridActionsJson.SelectedItems.length));
                    }
                    break;
                case "JumpToLibrary":
                    doCallback = false;
                    var folderId = eventArgs.selectedItem.getValue("FolderId");
                    var itemtypeId = eventArgs.selectedItem.getValue("ItemType");
                    PageMethods.WebMethod_HasDirectoryAccess(itemtypeId, '', folderId, function (result) {
                        if (result == "true") {
                            switch (itemtypeId) {
                                case "1":
                                case "2":
                                    RedirectToAdminPage("ManageContentLibrary", "DirectoryId=" + folderId);
                                    break;
                                case "4":
                                case "8":
                                    RedirectToAdminPage("ManagePageLibrary", "DirectoryId=" + folderId);
                                    break;
                                case "32":
                                    RedirectToAdminPage("ManageFileLibrary", "DirectoryId=" + folderId);
                                    break;
                                case "16":
                                    RedirectToAdminPage("ManageImageLibrary", "DirectoryId=" + folderId);
                                    break;
                            }
                        }
                        else {
                            alert("<%= GUIStrings.NoAccessToLibraryFolder %>");
                        }
                    });

                    break;
            }

            if (doCallback) {
                upFindReplace.set_callbackParameter(JSON.stringify(gridActionsJson));
                upFindReplace.callback();
            }
        }

        function upFindReplace_ItemSelect(sender, eventArgs) {
            if (eventArgs.selectedItem.getValue("ItemType") == "4" || eventArgs.selectedItem.getValue("ItemType") == "8")
                sender.getItemByCommand("ViewPage").show();
            else
                sender.getItemByCommand("ViewPage").hide();
        }

        function FindResults() {
            $(".find-replace").gridActions("callback", "Find");

            return false;
        }

        function ClosePreview() {
            $("#previewResult").iAppsDialog("close");

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <iAppsControls:CallbackPanel ID="upFindReplace" runat="server" OnClientCallbackComplete="upFindReplace_OnCallbackComplete"
        OnClientRenderComplete="upFindReplace_OnRenderComplete" OnClientLoad="upFindReplace_OnLoad">
        <ContentTemplate>
            <asp:Panel ID="pnlLeftControl" runat="server" CssClass="left-control" DefaultButton="btnFind">
                <h2><asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, Filters %>" /></h2>
                <div class="form-section">
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, FindWhatColon %>" /></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtFind" runat="server" CssClass="textBoxes" Width="283" ClientIDMode="Static" />
                        </div>
                    </div>
                    <asp:PlaceHolder ID="phReplaceWith" runat="server">
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ReplaceWithColon %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtReplace" runat="server" CssClass="textBoxes" Width="283" ClientIDMode="Static" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, SearchOptions %>" /></label>
                        <div class="form-value checkbox-list">
                            <asp:CheckBox ID="chkMatchCase" runat="server" Text="<%$ Resources:GUIStrings, MatchCase %>" /><br />
                            <asp:CheckBox ID="chkWholeWords" runat="server" Text="<%$ Resources:GUIStrings, WholeWordsOnly %>" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, LookIn %>" /></label>
                        <div class="form-value checkbox-list">
                            <asp:CheckBoxList ID="chkLookIn" CssClass="select-field" runat="server" RepeatDirection="Vertical" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, ContentOptions %>" /></label>
                        <div class="form-value checkbox-list">
                            <asp:CheckBox ID="chkIncludeAttributes" runat="server" Text="<%$ Resources:GUIStrings, IncludeAttributeValues %>" />
                        </div>
                    </div>
                    <div class="button-row">
                        <asp:Button ID="btnFind" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Find %>"
                            ToolTip="<%$ Resources:GUIStrings, Find %>" OnClientClick="return FindResults();" />
                    </div>
                </div>
            </asp:Panel>
            <div class="right-control">
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvFindReplace" runat="server" ItemPlaceholderID="phFindReplace">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            No Items found!
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="collection-table fat-grid">
                            <asp:PlaceHolder ID="phFindReplace" runat="server" />
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                            <div class="collection-row clear-fix grid-item" id="divRow" runat="server">
                                <div class="first-cell">
                                    <input type="checkbox" class="item-selector" id="chkSelect" runat="server" />
                                    <p><%# gridActionsDTO.PageSize * (gridActionsDTO.PageNumber - 1) + Container.DisplayIndex + 1 %></p>
                                </div>
                                <div class="middle-cell">
                                    <h4>
                                        <asp:Literal ID="ltTitle" runat="server" /></h4>
                                    <p>
                                        <asp:Literal ID="ltPath" runat="server" />
                                    </p>
                                </div>
                                <div class="last-cell">
                                    <div class="child-row">
                                        <asp:Literal ID="ltType" runat="server" />
                                    </div>
                                </div>
                                <input type="hidden" id="hdnUrl" runat="server" datafield="CompleteFriendlyURL" />
                                <input type="hidden" id="hdnFolderId" runat="server" datafield="FolderId" />
                                <input type="hidden" id="hdnType" runat="server" datafield="ItemType" />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <div id="previewResult" class="iapps-modal" style="display: none;">
                <div class="modal-header clear-fix">
                    <h2>
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, FindAndReplace %>" /></h2>
                </div>
                <div class="modal-content clear-fix">
                    <div class="preview-content">
                        <asp:Literal ID="ltPreviewContent" runat="server" />
                    </div>
                    <div class="preview-options clear-fix">
                        <a href="#" class="content-view-button selected">
                            <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, Content %>" /></a>
                        <a href="#" class="html-view-button">
                            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, HTML %>" /></a>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                        CssClass="button" OnClientClick="return ClosePreview();" />
                </div>
            </div>
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/AngularWeb/AngularMasterPage.Master" CodeBehind="AngularPage.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Angular.Web.AngularPage" AutoEventWireup="True" %>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:PlaceHolder ID="phContentManager" runat="server" Visible="false">
        <iapps-cms>
            <div class="iapps-padding iapps-align-center">
	    	    <h3>LOADING...</h3>
            </div>
        </iapps-cms>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phMarketier" runat="server" Visible="false">
        <iapps-marketier>
            <div class="iapps-padding iapps-align-center">
	    	    <h3>LOADING...</h3>
            </div>
        </iapps-marketier>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phCommerce" runat="server" Visible="false">
        <iapps-commerce>
            <div class="iapps-padding iapps-align-center">
	    	    <h3>LOADING...</h3>
            </div>
        </iapps-commerce>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phInsights" runat="server" Visible="false">
        <iapps-insights>
            <div class="iapps-padding iapps-align-center">
	    	    <h3>LOADING...</h3>
            </div>
        </iapps-insights>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phSocial" runat="server" Visible="false">
        <iapps-social>
            <div class="iapps-padding iapps-align-center">
	    	    <h3>LOADING...</h3>
            </div>
        </iapps-social>
    </asp:PlaceHolder>
</asp:Content>



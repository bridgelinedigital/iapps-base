(function ($) {
    var methods = {
        init: function (options) {
            var settings = $.extend({
                objList: null,
                type: "grid",
                listType: "fatGrid",
                fixHeader: true,
                hasBorder: false,
                hasTree: true,
                selectable: true,
                onDropMenu: function (sender, eventArgs) {
                    if (typeof GridItem_ShowMenu == 'function') GridItem_ShowMenu(sender, eventArgs);
                },
                onCallback: function (sender, eventArgs) {
                    if (typeof Grid_Callback == 'function') Grid_Callback(sender, eventArgs);
                },
                onSortMenu: function () { },
                onItemSelect: function (sender, eventArgs) {
                    if (typeof Grid_ItemSelect == 'function') Grid_ItemSelect(sender, eventArgs);
                },
                onItemCheckChange: function (sender, eventArgs) {
                    if (typeof Grid_ItemCheckChange == 'function') Grid_ItemCheckChange(sender, eventArgs);
                }
            }, options);

            return this.each(function () {
                var $this = $(this);
                $this.data("settings", settings);

                if (settings.objList == null) {
                    alert("Grid Actions is not initialized properly.");
                    return;
                }

                gridActionsJson.SelectedItem = null;

                $this.removeClass("selectable");
                $this.removeClass("not-selectable");
                $this.addClass(settings.selectable ? "selectable" : "not-selectable");

                $this.off("click", "input.item-selector");
                $this.on("click", "input.item-selector", function (e) {
                    methods.onSelectItem($this, $(this).val(), $(this).prop("checked"));
                    //if (($(this).prop("checked") && gridActionsJson.SelectedItems.length == 1) || !$(this).prop("checked")) {
                    methods.setSelectedItem($this, settings, true);
                    //}
                    var totalItemSelector = $this.find("input.item-selector").length;
                    if (gridActionsJson.SelectedItems.length == totalItemSelector)
                        $this.find("input.item-select-all").prop("checked", true);
                    else if (gridActionsJson.SelectedItems.length > 0)
                        $this.find("input.item-select-all").prop("checked", false);

                    e.stopPropagation();
                });

                $this.find("input.item-select-all").prop("checked", false);
                $this.off("click", "input.item-select-all");
                $this.on("click", "input.item-select-all", function (e) {
                    $this.find("input.item-selector").prop("checked", $(this).prop("checked"));

                    var firstElement = $this.find("input.item-selector").first();
                    methods.onSelectItem($this, firstElement.val(), firstElement.prop("checked"));
                    methods.setSelectedItem($this, settings, true);
                });

                $this.on("change", "input.item-selector", function (e) {
                    var args = {};
                    args.Selected = $(this).prop("checked");
                    args.Item = methods.getItemByKey($this, settings, $(this).val());

                    settings.onItemCheckChange.call(this, $this, args);
                });

                $this.off("click", ".edit-grid-item");
                $this.on("click", ".edit-grid-item", function (e) {
                    methods.onSelectItem($this, $(this).attr("objectId"), true);

                    gridActionsJson.Action = "Edit";
                    methods.setSelectedItem($this, settings, true);
                    methods.onBeforeCallback($this, settings, e);
                });

                $this.off("click", ".delete-grid-item");
                $this.on("click", ".delete-grid-item", function (e) {
                    methods.onSelectItem($this, $(this).attr("objectId"), true);

                    gridActionsJson.Action = "Delete";
                    methods.setSelectedItem($this, settings, true);
                    methods.onBeforeCallback($this, settings, e);
                });

                $this.off("click", ".grid-actions .command-button");
                $this.on("click", ".grid-actions .command-button", function (evt) {
                    var commandName = $(this).attr("commandName");
                    methods.setCallbackParams($(this), commandName, $(this).attr("commandArgument"));
                    methods.onBeforeCallback($this, settings, evt);
                    $this.find(".grid-actions .hasSubMenu ul").hide();
                });

                $this.off("change", ".grid-actions .command-select");
                $this.on("change", ".grid-actions .command-select", function (evt) {
                    var commandName = $(this).attr("commandName");
                    methods.setCallbackParams($(this), commandName, $(this).val());
                    methods.onBeforeCallback($this, settings, evt);
                    $this.find(".grid-actions .hasSubMenu ul").hide();
                });

                $this.off("click", ".grid-actions .view-selector img");
                $this.on("click", ".grid-actions .view-selector img", function (evt) {
                    gridActionsJson.ViewMode = parseInt($(this).attr("commandArgument"));

                    methods.onBeforeCallback($this, settings, evt);
                });

                $this.off("click", ".grid-actions .prev-page");
                $this.on("click", ".grid-actions .prev-page", function (evt) {
                    if (gridActionsJson.PageNumber > 1) {
                        gridActionsJson.Action = "Paging";
                        gridActionsJson.PageNumber--;
                        //gridActionsJson.SelectedItems.clear();
                        gridActionsJson.SelectedItem = null;

                        if (settings.type != "grid" || settings.objList.getProperty("RunningMode") == "2")
                            methods.onBeforeCallback($this, settings, evt);
                        else {
                            settings.objList.Page(gridActionsJson.PageNumber - 1);
                            $this.data("bindControlsComplete", false);
                            methods.bindControls(null, $this);
                        }
                        $this.find(".grid-actions .grid-item-action").hide();
                    }
                });

                $this.off("click", ".grid-actions .next-page");
                $this.on("click", ".grid-actions .next-page", function (evt) {
                    var pCount = gridActionsJson.PageCount;
                    if (settings.type == "grid") pCount = settings.objList.PageCount;
                    if (gridActionsJson.PageNumber < pCount) {
                        gridActionsJson.Action = "Paging";
                        gridActionsJson.PageNumber++;
                        //gridActionsJson.SelectedItems.clear();
                        gridActionsJson.SelectedItem = null;

                        if (settings.type != "grid" || settings.objList.getProperty("RunningMode") == "2")
                            methods.onBeforeCallback($this, settings, evt);
                        else {
                            settings.objList.Page(gridActionsJson.PageNumber - 1);
                            $this.data("bindControlsComplete", false);
                            methods.bindControls(null, $this);
                        }
                        $this.find(".grid-actions .grid-item-action").hide();
                    }
                });

                $this.off("click", ".grid-actions .pager-size ul li");
                $this.on("click", ".grid-actions .pager-size ul li", function (evt) {
                    var pSize = $(this).attr("commandArgument");
                    if (pSize != "") {
                        //gridActionsJson.SelectedItems.clear();
                        gridActionsJson.SelectedItem = null;

                        if (settings.type == "grid") {
                            if (pSize == -1) pSize = settings.objList.RecordCount;
                            settings.objList.set_pageSize(pSize);
                        }
                        else {
                            if (pSize == -1) pSize = gridActionsJson.RecordCount;

                        }
                        gridActionsJson.PageNumber = 1;
                        gridActionsJson.PageSize = pSize;
                        gridActionsJson.Action = "PageSize";
                        methods.onBeforeCallback($this, settings, evt);
                    }
                    $(".hasSubMenu ul").hide();
                });

                $this.on("keypress", ".grid-actions .grid-search-text", function (event) {
                    var btnSearchId = $(this).attr("id").replace("txtSearch_", "btnSearch_");
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        $("#" + btnSearchId).click();
                        return false;
                    }
                });

                $this.on("keyup", ".grid-actions .grid-search-text", function (event) {
                    var btnSearchId = $(this).attr("id").replace("txtSearch_", "btnSearch_");
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if ((keycode == '8' || keycode == '46') && $(this).val().trim() == '') {
                        $("#" + btnSearchId).click();
                        return false;
                    }
                });

                $this.off("click", ".grid-actions .grid-search-button");
                $this.on("click", ".grid-actions .grid-search-button", function (evt) {
                    var txtSearchId = $(this).attr("id").replace("btnSearch_", "txtSearch_");
                    var txtSearch = $("#" + txtSearchId);
                    var keyword = txtSearch.val();

                    if (keyword == txtSearch.attr("title"))
                        keyword = "";

                    if (keyword.indexOf('%') > -1) {
                        alert("% is a invalid character. Please re-enter.");
                        txtSearch.focus();
                    }
                    else {
                        gridActionsJson.Action = "Search";
                        gridActionsJson.PageNumber = 1;
                        gridActionsJson.Keyword = keyword;
                        //gridActionsJson.SelectedItems.clear();
                        gridActionsJson.SelectedItem = null;

                        methods.onBeforeCallback($this, settings, evt);
                    }
                    return false;
                });

                if (settings.listType == "tableGrid") {
                    var gLevel = settings.objList.get_levels()[0];
                    gLevel.set_rowCssClass(gLevel.get_rowCssClass() + " grid-item");
                    gLevel.set_alternatingRowCssClass(gLevel.get_alternatingRowCssClass() + " grid-item");
                    gLevel.set_selectedRowCssClass(gLevel.get_selectedRowCssClass() + " grid-item");

                    settings.objList.get_table().get_columns()[0].set_headingCellCssClass("first-cell");
                    settings.objList.get_table().get_columns()[settings.objList.get_table().get_columns().length - 1].set_headingCellCssClass("last-cell");

                    settings.objList.get_table().get_columns()[0].set_dataCellCssClass("first-cell");
                    settings.objList.get_table().get_columns()[settings.objList.get_table().get_columns().length - 1].set_dataCellCssClass("last-cell");

                    $("#" + settings.objList.get_id()).width("100%");

                    settings.objList.render();
                }

                if (!$("body").hasClass("popupBody")) {
                    $(window).load(function () {
                        shortcut.add("Ctrl+ALT+G", function () {
                            if (gridActionsJson.SelectedItems.length > 0) {
                                OpeniAppsShortcut(gridActionsJson.SelectedItems.first());
                            }
                        });
                    });
                }

                if (settings.fixHeader && !$("body").hasClass("popupBody") && $this.find(".grid-actions").length > 0) {
                    //adjust fixed header width
                    var scrollToPosition = $this.find(".grid-actions").offset().top + $this.find(".grid-actions").outerHeight(true) - 40;
                    $(".fixed-action-bg").remove();
                    $(".back-to-top").remove();
                    var fixedBg = $("<div />").addClass("fixed-action-bg").hide();
                    var btHtml = stringformat('<img src="{0}" alt="" class="back-to-top" />', ResolveClientUrl("~/App_Themes/General/images/back-to-top.png"));
                    var backToTop = $(btHtml).bind("click", function () {
                        $(document).scrollTop(0);
                    });
                    $this.find(".grid-actions").children().each(function () {
                        var rClass = "row";
                        if ($(this).is(':first-child')) rClass = "top-row";
                        if ($(this).is(':last-child')) rClass = "bottom-row";
                        if ($(this).hasClass('tab-row')) rClass += " tab-row";
                        var fRow = $("<div />").addClass(rClass);
                        fixedBg.append(fRow);
                    });
                    $("body").append(fixedBg);
                    $("body").append(backToTop);

                    if (!$this.find(".grid-actions").hasClass("grid-actions-fixed")) {
                        $(window).on('scroll', function () {
                            var hWidth = 0;

                            var rightSection = $this.find(".right-control");
                            var gridSection = $this.find(".grid-section");
                            if (rightSection.length > 0)
                                hWidth = rightSection.width();
                            else if (gridSection.length > 0)
                                hWidth = gridSection.width();
                            else if (settings.type == "grid")
                                hWidth = $("#" + settings.objList.get_id()).width();
                            else {
                                var oList = $this.getList(settings);
                                hWidth = oList.width();
                            }

                            var scrollTop = $(window).scrollTop();
                            if ($this.find(".grid-actions").hasClass("grid-actions-fixed"))
                                scrollTop += $this.find(".grid-actions").height();
                            if (scrollTop > scrollToPosition) {
                                $this.find(".grid-actions").addClass('grid-actions-fixed');
                                if (hWidth > 0)
                                    $this.find(".grid-actions").width(hWidth);
                                fixedBg.show();
                                backToTop.fadeIn();
                            }
                            else {
                                $this.find(".grid-actions").removeClass('grid-actions-fixed');
                                fixedBg.hide();
                                backToTop.fadeOut();
                            }
                        });
                    }
                }
            });
        },
        setCallbackParams: function ($this, commandName, commandArgument) {
            var clearSelected = true;
            switch (commandName) {
                case "Sort":
                    gridActionsJson.PageNumber = 1;
                    gridActionsJson.Action = "LoadData";
                    gridActionsJson.SortBy = commandArgument;
                    gridActionsJson.SortOrder = "ASC";
                    if ($this.hasClass("selected-asc"))
                        gridActionsJson.SortOrder = "DESC";
                    break;
                case "Status":
                    gridActionsJson.PageNumber = 1;
                    gridActionsJson.Action = "LoadData";
                    gridActionsJson.Status = commandArgument;
                    break;
                case "CustomFilter":
                    gridActionsJson.PageNumber = 1;
                    gridActionsJson.Action = "LoadData";
                    gridActionsJson.CustomFilter = commandArgument;
                    break;
                case "TabIndex":
                    gridActionsJson.PageNumber = 1;
                    gridActionsJson.Action = "LoadData";
                    gridActionsJson.TabIndex = commandArgument;
                    gridActionsJson.SelectedItems.clear();
                    break;
                case "Redirect":
                    gridActionsJson.PageNumber = 1;
                    gridActionsJson.Action = commandName;
                    gridActionsJson.CustomAttributes[commandName] = commandArgument;
                    break;
                default:
                    clearSelected = false;
                    gridActionsJson.Action = commandArgument;
                    break;
            }

            if (clearSelected) {
                //gridActionsJson.SelectedItems.clear();
                gridActionsJson.SelectedItem = null;
                $("input.item-select-all").prop("checked", false);
            }
        },
        callback: function (method, options) {
            var $this = $(this);
            var settings = $this.data("settings");

            var tOptions = options;
            var action = "LoadData";
            if (typeof method === 'object') {
                tOptions = method;
            }
            else if (method) {
                action = method;
            }

            gridActionsJson.Action = action;
            if (settings.type == "list" && tOptions && tOptions.selectedItem)
                tOptions.selectedItem.trigger("click");

            if (tOptions && tOptions.refreshTree == true)
                methods.refreshTree($this);

            methods.onBeforeCallback($this, settings, null);
        },
        onBeforeCallback: function ($this, settings, evt) {
            if (gridActionsJson.StartDate != null && gridActionsJson.StartDate.toString().indexOf("Date") > 0)
                gridActionsJson.StartDate = jQuery.toDate(gridActionsJson.StartDate);
            if (gridActionsJson.EndDate != null && gridActionsJson.EndDate.toString().indexOf("Date") > 0)
                gridActionsJson.EndDate = jQuery.toDate(gridActionsJson.EndDate);


            if (gridActionsJson.SelectedItems.length > 0 && gridActionsJson.SelectedItems.first().length != 36) {
                gridActionsJson.SelectedIds.clear();
                $.each(gridActionsJson.SelectedItems, function () {
                    gridActionsJson.SelectedIds.push(this);
                });

                gridActionsJson.SelectedItems.clear();
            }

            CloseiAppsNotification();

            settings.onCallback.call(this, $this, { selectedItem: $this.data("selectedItem"), event: evt });
        },
        selectNode: function (folderId, options) {
            var $this = $(this);
            var settings = $this.data("settings");
            var thisSettings = $.extend({
                start: function () { }
            }, options);

            return this.each(function () {
                if (settings == null)
                    return;

                $this.find("input.item-select-all").prop("checked", false);
                $this.find(".grid-actions .grid-item-action").hide();
                var hideAction = gridActionsJson.HideActions;
                var customFilter = gridActionsJson.CustomFilter;
                var viewMode = gridActionsJson.ViewMode;

                gridActionsJson = JSON.parse(JSON.stringify(defaultGridActionsJson));
                if (folderId)
                    gridActionsJson.FolderId = folderId;

                gridActionsJson.HideActions = hideAction;
                gridActionsJson.CustomFilter = customFilter;
                gridActionsJson.FolderName = escape(treeActionsJson.Text);
                gridActionsJson.ViewMode = viewMode;

                if (settings.type == "grid") {
                    settings.objList.unSelectAll();
                    settings.objList.set_pageSize(gridActionsJson.PageSize);
                }

                thisSettings.start.call(this);
                methods.onBeforeCallback($this, settings, null);
            });
        },
        bindControls: function (options, $t) {
            var $this = $(this);
            if ($t)
                $this = $t;
            var settings = $this.data("settings");
            var thisSettings = $.extend({
                forceBind: false,
                complete: function () { }
            }, options);

            if (!thisSettings.forceBind) {
                if (settings == null || $this.data("bindControlsComplete"))
                    return;
            }

            $this.find(".grid-actions ul.drop-menu > li a").each(function () {
                var $li = $(this).parent("li");
                var subMenu = $li.find("ul");
                if (subMenu.length > 0) {
                    $li.addClass("hasSubMenu");
                    $li.hover(
                        function () {
                            $(".hasSubMenu ul").hide();
                            if ($(this).parent(".grid-item-action").length > 0)
                                settings.onDropMenu.call(this, subMenu, { menu: subMenu, selectedItem: $this.data("selectedItem") });
                            else if ($(this).parent(".sort-menu").length > 0)
                                settings.onSortMenu.call(this, subMenu, { menu: subMenu, selectedItem: $this.data("selectedItem") });
                            //subMenu.show();
                            clearTimeout($.data(subMenu, 'timer'));
                            subMenu.stop(true, true).show();
                            if ($("body").hasClass("popupBody")) {
                                var sRight = subMenu.offset().left + subMenu.width();
                                var cWidth = $(".modal-content").width();
                                if (sRight > cWidth)
                                    subMenu.css({ "left": cWidth - sRight });
                            }
                        },
                        function () {
                            //$(".hasSubMenu ul").hide();
                            $.data(subMenu, 'timer', setTimeout(function () {
                                subMenu.stop(true, true).hide();
                            }, 200));
                        }
                    );
                }
            });

            if (typeof gridActionsJson != "undefined") {
                $("input.item-selector").attr("checked", false);
                $.each(gridActionsJson.SelectedItems, function () {
                    $("input.item-selector[value='" + this + "']").prop("checked", true);
                });

                $this.find(".grid-actions ul li[commandName='Sort']").removeClass("selected-asc");
                $this.find(".grid-actions ul li[commandName='Sort']").removeClass("selected-desc");
                $this.find(".grid-actions ul li[commandName='Sort'][commandArgument='" + gridActionsJson.SortBy + "']").addClass(gridActionsJson.SortOrder == "DESC" ? "selected-desc" : "selected-asc");

                $this.find(".grid-actions ul li[commandName='Status']").removeClass("selected");
                $this.find(".grid-actions ul li[commandName='Status'][commandArgument='" + gridActionsJson.Status + "']").addClass("selected");
                $this.find(".grid-actions select[commandName='Status']").val(gridActionsJson.Status);

                $this.find(".grid-actions ul.custom-filter li[commandName='CustomFilter']").removeClass("selected");
                $this.find(".grid-actions ul.custom-filter li[commandName='CustomFilter'][commandArgument='" + gridActionsJson.CustomFilter + "']").addClass("selected");
                $this.find(".grid-actions select[commandName='CustomFilter']").val(gridActionsJson.CustomFilter);

                $this.find(".grid-actions [commandName='Redirect']").removeClass("selected");
                $this.find(".grid-actions [commandName='Redirect'][commandArgument='" + gridActionsJson.CustomAttributes["Redirect"] + "']").addClass("selected");

                $this.find(".grid-actions ul.tab-menu li").removeClass("selected").removeClass("selected-tab");
                var tSelected = $this.find(".grid-actions ul.tab-menu li[commandArgument='" + gridActionsJson.TabIndex + "']");
                $(tSelected).addClass("selected").addClass("selected-tab");
                $(tSelected).prev('li').addClass('prev').siblings().removeClass('prev');
                $(tSelected).next('li').addClass('next').siblings().removeClass('next');

                $this.find(".grid-actions select[commandName='TabIndex']").val(gridActionsJson.TabIndex);
                //$this.find(".grid-actions ul.custom-filter li.selected").each(function () {
                //    $(this).parents(".hasSubMenu").find("a").html($(this).html());
                //});
                $this.find(".grid-actions .command-select").each(function () {
                    var cName = $(this).attr("commandName");
                    if (typeof cName != "undefined" && cName != "" && cName != "Status" && cName != "TabIndex" && cName != "CustomFilter")
                        $(this).val(gridActionsJson.CustomAttributes[cName]);
                });

                $this.find(".grid-actions .pager-size ul li").removeClass("selected");
                $this.find(".grid-actions .pager-size ul li[commandArgument='" + gridActionsJson.PageSize + "']").addClass("selected");
                //$this.find(".grid-actions select.pager-size").val(gridActionsJson.PageSize);

                $this.find(".grid-actions .grid-search-button").each(function () {
                    var txtSearchId = $(this).attr("id").replace("btnSearch_", "txtSearch_");
                    if (gridActionsJson.Keyword && gridActionsJson.Keyword != "")
                        $("#" + txtSearchId).val(gridActionsJson.Keyword);
                    else
                        $("#" + txtSearchId).val("");
                });

                $this.find(".grid-actions .view-selector img").removeClass("active");
                $this.find(".grid-actions .view-selector img[commandArgument='" + gridActionsJson.ViewMode + "']").addClass("active");
                var $selectedView = null;
                $this.find(".grid-actions .view-selector img").each(function (evt) {
                    $(this).attr("src", $(this).attr("src").replace("-active.png", ".png"));

                    if ($(this).hasClass("active"))
                        $selectedView = $(this);
                });
                if ($selectedView != null)
                    $selectedView.attr("src", $selectedView.attr("src").replace(".png", "-active.png"));
            }
            if (settings.type == "grid") {
                gridActionsJson.RecordCount = settings.objList.RecordCount;

                $this.off("click", ".grid-item");
                $this.on("click", ".grid-item", function (e) {
                    if (!$(e.target).is(".item-selector")) {
                        var selId = settings.objList.getSelectedKeys().last();
                        methods.onSelectItem($this, selId, true);

                        methods.setSelectedItem($this, settings, false, selId);
                    }
                });

                if (gridActionsJson.Action == "PageSize")
                    settings.objList.Page(0);

                if (settings.objList.PageCount > 1) {
                    $this.find(".pager-buttons, .sort-action").show();
                }
                else {
                    $this.find(".pager-buttons, .sort-action").hide();
                }

                if (settings.objList.RecordCount > 0) {
                    methods.toggleEmptyGridStyle($this, settings, true);

                    var startRow = 1;
                    if (settings.objList.CurrentPageIndex > 0)
                        startRow = (settings.objList.CurrentPageIndex * settings.objList.PageSize) + 1;

                    var lastRow = (settings.objList.CurrentPageIndex + 1) * settings.objList.PageSize;

                    if (lastRow >= settings.objList.RecordCount)
                        lastRow = settings.objList.RecordCount;

                    $this.find(".pager-info").html(stringformat("{0} - {1} of {2}", startRow, lastRow, settings.objList.RecordCount));
                }
                else {
                    methods.toggleEmptyGridStyle($this, settings, false);
                }

                $this.find("#" + settings.objList.get_id() + "_dom").css("height", "auto");
            }
            else {
                $this.off("click", ".grid-item");
                $this.on("click", ".grid-item", function (e) {
                    if (!$(e.target).is(".item-selector")) {
                        var objectId = $(this).attr("objectId");
                        methods.onSelectItem($this, objectId, true);
                        methods.setSelectedItem($this, settings, true, objectId);
                    }
                });

                if (gridActionsJson.PageCount > 1) {
                    $this.find(".pager-buttons, .sort-action").show();
                }
                else {
                    $this.find(".pager-buttons, .sort-action").hide();
                }

                if (gridActionsJson.RecordCount > 0) {
                    methods.toggleEmptyGridStyle($this, settings, true);

                    var startRow = 1;
                    var currentPageIndex = gridActionsJson.PageNumber - 1;
                    if (currentPageIndex > 0)
                        startRow = (currentPageIndex * gridActionsJson.PageSize) + 1;

                    var lastRow = (currentPageIndex + 1) * gridActionsJson.PageSize;

                    if (lastRow >= gridActionsJson.RecordCount)
                        lastRow = gridActionsJson.RecordCount;

                    $this.find(".pager-info").html(stringformat("{0} - {1} of {2}", startRow, lastRow, gridActionsJson.RecordCount));
                }
                else {
                    methods.toggleEmptyGridStyle($this, settings, false);
                }
            }

            if (gridActionsJson.RecordCount > 0)
                $this.find(".view-selector").show();
            else
                $this.find(".view-selector").hide();

            methods.resetGridItemActions($this);
            $this.data("bindControlsComplete", true);

            if (gridActionsJson.SelectedItems.length > 0) {
                setTimeout(function () {
                    methods.setSelectedItem($this, settings, true);
                }, 10);
            }

            if (gridActionsJson.RecordCount > 10000) {
                // Remove show all
                $this.find(".grid-actions .pager-size ul li[commandArgument='-1']").remove();
            }

            /*setTimeout(function () {
            if (typeof Tree_GetBreadcrumb == 'function') {
            var treeBreadcrumb = Tree_GetBreadcrumb(gridActionsJson.FolderId);
            if (treeBreadcrumb != "")
            $("#pageDirectoryPath").html(treeBreadcrumb);
            }
            if (typeof Tree_GetSelectedTitle == 'function') {
            var treeSelectedTitle = Tree_GetSelectedTitle(gridActionsJson.FolderId);
            if (treeSelectedTitle != "")
            $("#selectedNodeTitle").html(treeSelectedTitle);
            }

            if ($this.data("refreshTree") == true) {
            $this.removeData("refreshTree");
            if (typeof RefreshLibraryTree == "function") {
            //$.each(gridActionsJson.SelectedItems, function () {
            //    gridActionsJson.SelectedItems.pop(this);
            //});
            RefreshLibraryTree();
            }
            }

            }, 100);*/
            if ($this.data("refreshTree") == true) {
                $this.removeData("refreshTree");
                if (typeof RefreshLibraryTree == "function") {
                    treeActionsJson.FolderId = gridActionsJson.FolderId;
                    RefreshLibraryTree();
                }
            }

            if (typeof treeActionsJson != "undefined" && treeActionsJson.Text != null && treeActionsJson.Text != "")
                $("#selectedNodeTitle").html(treeActionsJson.Text);

            if (typeof GetRightSectionTitle == "function")
                $("#selectedNodeTitle").html(GetRightSectionTitle(treeActionsJson.Text));

            $this.on("focus", ".grid-actions .grid-search-text", function () {
                if ($(this).val() == $(this).attr("title")) {
                    $(this).removeClass("defaultText");
                    $(this).val("");
                }
            });

            $this.on("blur", ".grid-actions .grid-search-text", function () {
                if ($(this).attr("title") && $(this).attr("title") != "" && ($(this).val() == "" || $(this).val() == $(this).attr("title"))) {
                    if (gridActionsJson.Keyword == "" || gridActionsJson.Keyword == null) {
                        $(this).addClass("defaultText");
                        $(this).val($(this).attr("title"));
                    }
                    else {
                        $(this).removeClass("defaultText");
                        //$(this).val(gridActionsJson.Keyword);
                    }
                }
            });

            if ($this.find("input.item-selector").length == 0)
                $this.find(".select-all-box").hide();
            else
                $this.find(".select-all-box").show();

            thisSettings.complete.call(this);

            if (gridActionsJson.ViewMode == 2)
                $this.removeClass("list-view").addClass("thumb-view");
            else
                $this.removeClass("thumb-view").addClass("list-view");

            setTimeout(function () {
                if (settings.hasBorder)
                    $this.find(".grid-item").last().addClass("last-item").css("border-bottom", "0");

                $this.find(".collapsible-row").each(function () {
                    if ($(this).children(":visible").length > 0)
                        $(this).show();
                    else
                        $(this).hide();
                });
            }, 500);
        },
        toggleEmptyGridStyle: function ($this, settings, show) {
            if (show) {
                $this.find(".pager-info").show();
                $this.find(".pager-size").show();
                $this.find(".sort-menu").show();
                $this.find(".select-all-box").show();
            }
            else {
                $this.find(".pager-info").hide();
                $this.find(".pager-size").hide();
                $this.find(".sort-menu").hide();
                $this.find(".select-all-box").hide();

                if (settings.type == "grid") {
                    setTimeout(function () {
                        $this.setEmptyGridStyle();
                    }, 10);
                }
            }
        },
        displayMessage: function (options, $t) {
            var $this = $(this);
            if ($t)
                $this = $t;
            var thisSettings = $.extend({
                complete: function () { }
            }, options);

            $this.data("bindControlsComplete", false);

            //alert(typeof PageMethods);

            if (typeof GetSessionGridActionJson == 'function') {
                methods.onDisplayMessage($this, thisSettings, GetSessionGridActionJson());
            }
            else if (typeof PageMethods == 'function' && typeof PageMethods.WebMethod_GetSessionGridActionJson == 'function') {

                $this.data("bindControlsComplete", true);
                PageMethods.WebMethod_GetSessionGridActionJson(function (result) {
                    methods.onDisplayMessage($this, thisSettings, result);

                    $this.data("bindControlsComplete", false);
                    methods.bindControls(options, $this);
                });
            }
            else {
                //from MVC - ajax call back to get the DTO from session
                var gridId = $this.find("div.grid-actions").attr("gridId");
                var gridControllerName = $this.find("div.grid-actions").attr("gridControllerName");
                var gridActionsMethodName = $this.find("div.grid-actions").attr("gridActionsMethodName");

                var relativeUrl = "/General/GetSessionGridAction";
                if (typeof gridId != "undefined" && typeof gridControllerName != "undefined" && typeof gridActionsMethodName != "undefined")
                    relativeUrl = stringformat("/{0}/{1}/{2}", gridControllerName, gridActionsMethodName, gridId);

                $.get(GetCurrentDomainUrl() + relativeUrl, function (result) {
                    methods.onDisplayMessage($this, thisSettings, result);
                    $this.data("bindControlsComplete", false);
                    methods.bindControls(options, $this);
                });
            }
        },
        onDisplayMessage: function ($this, settings, result) {
            if (result != "") {
                var folderId = gridActionsJson.FolderId;
                gridActionsJson = JSON.parse(result);
                gridActionsJson.FolderId = folderId;

                if (gridActionsJson.SelectedIds.length > 0 && gridActionsJson.SelectedItems.length == 0) {
                    gridActionsJson.SelectedItems.clear();
                    $.each(gridActionsJson.SelectedIds, function () {
                        gridActionsJson.SelectedItems.push(this);
                    });
                }

                if (gridActionsJson.ResponseMessage) {
                    if (gridActionsJson.ResponseMessage.indexOf("failed.") > 0)
                        gridActionsJson.ResponseCode = 500;

                    ShowiAppsNotification(gridActionsJson.ResponseMessage,
                        gridActionsJson.ResponseCode == 100 || gridActionsJson.ResponseCode == 0 ? true : false, false);
                    //$("<div />").html(gridActionsJson.ResponseMessage).iAppsDialog("open", {alertBox: true});
                }

                if (gridActionsJson.Action == "RefreshTree")
                    methods.refreshTree($this);

                settings.complete.call(this);

                methods.resetGridItemActions($this);

                var mainSettings = $this.data("settings");
                if (typeof mainSettings != "undefined") {
                    if (mainSettings.type == "grid" && gridActionsJson.SelectedItems.length == 0) {
                        mainSettings.objList.unSelectAll();
                    }
                }

                gridActionsJson.ResponseCode = 0;
                gridActionsJson.ResponseMessage = null;
            }
            //$.each(gridActionsJson.CustomAttributes, function () {
            //  gridActionsJson.CustomAttributes.pop();
            //});
            gridActionsJson.Action = "LoadData";
        },
        onSelectItem: function ($this, selectedId, checkSelector) {
            $this.find("input.item-selector[value='" + selectedId + "']").prop("checked", checkSelector)

            //gridActionsJson.SelectedItems.clear();

            $this.find("input.item-selector:checked").each(function () {
                var $val = $(this).val();
                if (!gridActionsJson.SelectedItems.exists($val))
                    gridActionsJson.SelectedItems.push($val);
            });

            $this.find("input.item-selector:not(:checked)").each(function () {
                var $val = $(this).val();
                if (gridActionsJson.SelectedItems.exists($val))
                    gridActionsJson.SelectedItems.remove($val);
            });

            if (checkSelector) {
                if (!gridActionsJson.SelectedItems.exists(selectedId))
                    gridActionsJson.SelectedItems.push(selectedId);
            }

            gridActionsJson.SelectedItem = null;
            if (gridActionsJson.SelectedItems.length > 0) {
                var sItems = JSON.parse(JSON.stringify(gridActionsJson.SelectedItems));
                gridActionsJson.SelectedItems.clear();
                if (typeof selectedId != "undefined" && selectedId) {
                    if (checkSelector) {
                        gridActionsJson.SelectedItem = selectedId;
                        gridActionsJson.SelectedItems.push(selectedId);
                    }
                    else if (sItems.length > 0) {
                        selectedId = sItems[0];
                        gridActionsJson.SelectedItem = selectedId;
                    }
                }
                $.each(sItems, function () {
                    if (!gridActionsJson.SelectedItems.exists(this))
                        gridActionsJson.SelectedItems.push(this);
                });
            }
            methods.resetGridItemActions($this);
        },
        resetGridItemActions: function ($this) {
            var settings = $this.data("settings");

            $this.find(".grid-actions .grid-action").hide();
            $this.find(".grid-actions .grid-item-action").hide();
            $this.find(".grid-actions .grid-bulk-action").hide();

            var checkTreeActions = typeof treeActionsJson != "undefined" &&
                (typeof settings == "undefined" || settings.hasTree);

            if (gridActionsJson.HideActions == false) {
                $this.find(".grid-actions .grid-action").show();
                if (gridActionsJson.SelectedItems.length > 0) {
                    $this.find(".grid-actions .grid-bulk-action").show();
                    if (gridActionsJson.SelectedItem != null)
                        $this.find(".grid-actions .grid-item-action").show();

                    if (checkTreeActions && (treeActionsJson.NodeType == 0 || treeActionsJson.NodeType == 3)) {
                        $this.find(".grid-actions .grid-action").hide();
                        $this.find(".grid-actions .grid-bulk-action").hide();
                    }
                    else {
                        $this.find(".grid-actions .grid-action").show();
                        $this.find(".grid-actions .grid-bulk-action").show();
                    }
                }

                if (checkTreeActions && (treeActionsJson.NodeType == 0 || treeActionsJson.NodeType == 3)) {
                    $this.find(".grid-actions .common-action").hide();
                    $this.find(".grid-actions .grid-action").hide();
                }
                else {
                    $this.find(".grid-actions .common-action").show();
                    $this.find(".grid-actions .grid-action").show();
                }
            }

            if (checkTreeActions) {
                if (gridActionsJson.FolderId == EmptyGuid && treeActionsJson.NodeType == 0) {
                    $this.find(".grid-actions .common-action").hide();
                    $this.find(".grid-actions .grid-action").hide();
                }
            }

            if (gridActionsJson.RecordCount > 0) {
                $this.find(".drop-menu.last").removeClass("last");
                $this.find(".grid-all-action").show();
            }
            else {
                $this.find(".pager-buttons").siblings(".drop-menu:visible:last").addClass("last");
                $this.find(".grid-all-action").hide();
            }
        },
        setSelectedItem: function ($this, settings, selectItem, selectedId) {
            if (gridActionsJson.SelectedItem != null) {
                if (typeof selectedId == "undefined")
                    selectedId = gridActionsJson.SelectedItem;
                if (settings.type == "grid") {
                    var sgItem = settings.objList.getItemFromKey(0, selectedId);
                    if (sgItem == null)
                        sgItem = settings.objList.getItemFromKey(1, selectedId);
                    if (selectItem)
                        settings.objList.select(sgItem, false);

                    sgItem.Key = selectedId;
                    $this.data("selectedItem", sgItem);
                }
                else {
                    $this.find(".selected-list-item").removeClass("selected-list-item");
                    var sItem = $this.getItemFromKey(selectedId);
                    if (settings.selectable)
                        sItem.addClass("selected-list-item");
                    sItem.Key = selectedId;
                    //if (selectItem)
                    $this.data("selectedItem", sItem);
                }

                settings.onItemSelect.call(this, $this, { selectedItem: $this.data("selectedItem") });

                $this.find(".grid-actions ul.drop-menu.grid-item-action, .grid-actions ul.drop-menu.grid-bulk-action").each(function () {
                    var hiddenLis = $(this).find("ul li").filter(function () {
                        return $(this).css('display') == "none";
                    });

                    if ($(this).find("ul li").length == hiddenLis.length)
                        $(this).hide();
                });
            }
            else {
                $this.removeData("selectedItem");
                if (settings.type == "grid") {
                    settings.objList.unSelectAll();
                }
                else {
                    $this.find(".selected-list-item").removeClass("selected-list-item");
                }
            }
        },
        getSelectedItem: function () {
            var $this = $(this);

            return $this.data("selectedItem");
        },
        getItemByKey: function ($this, settings, key) {
            if (settings.type == "grid") {
                var sgItem = settings.objList.getItemFromKey(0, key);
                if (sgItem == null)
                    sgItem = settings.objList.getItemFromKey(1, key);

                return sgItem
            }
            else {
                var item = settings.objList.refresh().getItemFromKey(key);
                item.Key = key;

                return item;
            }
        },
        getItems: function () {
            var $this = $(this);

            return $(this).find(".grid-item");
        },
        refreshTree: function ($this) {
            if (!($this))
                $this = $(this);

            $this.data("refreshTree", true);
        },
        mvcCallback: function (url, itemContainer, options) {
            var $this = $(this);
            $this.iAppsSplitter();

            ShowiAppsLoadingPanel();
            $.get(url, { gridAction: JSON.stringify(gridActionsJson) }, function (result) {
                if (typeof (result.success) != 'undefined' && !result.success) {
                    alert(result.message);
                }
                else {
                    $(itemContainer).html(result);

                    methods.displayMessage({
                        complete: function () {
                            $this.data("bindControlsComplete", false);
                            methods.bindControls(null, $this);
                            $this.iAppsSplitter();

                            options.complete.call(this);
                        }
                    }, $this);
                }
            }).fail(function (message) {
                alert(message.statusText);
            }).always(function () {
                CloseiAppsLoadingPanel();
            });
        }
    };

    $.fn.gridActions = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.gridActions');
        }

    };

})(jQuery);

(function ($) {
    var methods = {
        init: function (options) {
            var settings = $.extend({
                objTree: null,
                objSelectTree: null,
                pageTree: true,
                blogTree: false,
                fixHeader: true,
                maxNodeType: 2,
                expandOnSelect: true,
                kendoTree: false,
                treeSelector: null,
                nameRegex: "[\<\>\"]",
                onCallback: function (sender, eventArgs) {
                    if (typeof Tree_Callback == 'function') Tree_Callback(sender, eventArgs);
                },
                onItemSelect: function (sender, eventArgs) {
                    if (typeof Tree_ItemSelect == 'function') Tree_ItemSelect(sender, eventArgs);
                },
                onBeforeSiteChange: function (sender, eventArgs) {
                    if (typeof Tree_BeforeSiteChange == 'function') return Tree_BeforeSiteChange(sender, eventArgs);

                    return true;
                }
            }, options);

            return this.each(function () {
                var $this = $(this);
                //if ($this.data("settings") != null)
                //    return;

                $this.data("settings", settings);

                if (settings.kendoTree)
                    settings.objTree = settings.treeSelector.data("kendoTreeView");

                if (settings.objTree == null) {
                    alert("Tree Actions is not initialized properly.");
                    return;
                }

                if (!settings.kendoTree) {
                    settings.objTree.set_expandNodeOnSelect(settings.expandOnSelect);
                }

                $this.find(".tree-actions ul.drop-menu > li a").each(function () {
                    var $li = $(this).parent("li");
                    var subMenu = $li.find("ul");
                    if (subMenu.length > 0) {
                        $li.addClass("hasSubMenu");
                        $li.hover(
                            function () {
                                $(".hasSubMenu ul").hide();
                                clearTimeout($.data(subMenu, 'timer'));
                                subMenu.stop(true, true).show();
                            },
                            function () {
                                $.data(subMenu, 'timer', setTimeout(function () {
                                    subMenu.stop(true, true).slideUp();
                                }, 200));
                            }
                        );
                    }
                });

                $this.off("click", ".tree-actions .tree-toggle");
                $this.on("click", ".tree-actions .tree-toggle", function () {
                    if ($(this).hasClass("collapse")) {
                        if (settings.kendoTree) {
                            settings.objTree.expand('.k-item');
                        }
                        else {
                            settings.objTree.expandAll();
                            if (settings.objSelectTree != null)
                                settings.objSelectTree.expandAll();
                        }
                        $(this).removeClass("collapse");
                        $(this).addClass("expand");
                        //$(this).attr("src", ResolveClientUrl("~/App_Themes/General/images/tree/collapse-all.png"));
                        //$(this).attr("alt", "collapse all");
                        $(this).text(__JSMessages["CollapseAll"]);
                    }
                    else {
                        if (settings.kendoTree) {
                            settings.objTree.collapse('.k-item');
                        }
                        else {
                            settings.objTree.collapseAll();
                            if (settings.objTree.get_nodes().get_length() > 0)
                                settings.objTree.get_nodes().getNode(0).expand();
                            if (settings.objSelectTree != null) {
                                settings.objSelectTree.collapseAll();
                                setTimeout(function () {
                                    if (settings.objSelectTree.get_nodes().get_length() > 0)
                                        settings.objSelectTree.get_nodes().getNode(0).expand();
                                }, 300);
                            }
                        }
                        $(this).removeClass("expand");
                        $(this).addClass("collapse");
                        //$(this).attr("src", ResolveClientUrl("~/App_Themes/General/images/tree/expand-all.png"));
                        //$(this).attr("alt", "expand all");
                        $(this).text(__JSMessages["ExpandAll"]);
                    }

                    return false;
                });

                $this.off("change", ".tree-actions .command-select");
                $this.on("change", ".tree-actions .command-select", function (evt) {
                    var commandName = $(this).attr("commandName");
                    switch (commandName) {
                        case "SiteSelector":
                            var proceed = settings.onBeforeSiteChange.call(this, $this, { "SiteId": $(this).val() });
                            if (typeof proceed == "undefined")
                                proceed = true;
                            if (proceed) {
                                treeActionsJson.SiteId = $(this).val();
                                settings.onCallback.call(this, $this, { "Action": commandName });
                            }
                            else {
                                $(this).val(treeActionsJson.SiteId);
                            }
                            break;
                    }
                });

                $this.off("click", ".tree-actions .command-button");
                $this.on("click", ".tree-actions .command-button", function (evt) {
                    var command = $(this).attr("commandName");
                    var commandArgument = $(this).attr("commandArgument");
                    treeActionsJson.Action = commandArgument;
                    var selectedNode = null;
                    if (settings.kendoTree)
                        selectedNode = settings.objTree.findByUid(treeActionsJson.FolderId);
                    else
                        selectedNode = settings.objTree.findNodeById(treeActionsJson.FolderId);

                    switch (command) {
                        case "EditNode":
                            selectedNode.edit();
                            break;
                        case "DeleteNode":
                            settings.onCallback.call(this, $this, { "Action": treeActionsJson.Action });
                            if (treeActionsJson.Success == true) {
                                var nodeId = "";
                                if (selectedNode.get_parentNode().getProperty("NodeType") == 0) {
                                    settings.objTree.findNodeById(treeActionsJson.DefaultFolderId).select();
                                    nodeId = treeActionsJson.DefaultFolderId;
                                }
                                else {
                                    selectedNode.get_parentNode().select();
                                    nodeId = selectedNode.get_parentNode().get_id();
                                }
                                selectedNode.remove();
                                if (typeof RefreshLibraryTree == "function") {
                                    treeActionsJson.FolderId = nodeId;
                                    RefreshLibraryTree();
                                }
                            }
                            break;
                        case "AddNode":
                            settings.objTree.beginUpdate();
                            var newNode = new ComponentArt.Web.UI.TreeViewNode();
                            newNode.set_text("New Node");
                            newNode.set_id(EmptyGuid);
                            newNode.set_value(selectedNode.get_value());
                            var nodeType = 2;
                            if (nodeType > settings.maxNodeType) nodeType = settings.maxNodeType;
                            if (settings.pageTree && parseInt(selectedNode.getProperty("NodeType")) == 0)
                                nodeType = 3;
                            newNode.setProperty("NodeType", nodeType.toString());
                            newNode.setProperty("IsMarketierDir", selectedNode.getProperty("IsMarketierDir"));
                            var isVisible = true;
                            var isGroupNode = false;
                            if (settings.pageTree) {
                                isGroupNode = isVisible = (nodeType == 3);
                            }
                            if (settings.blogTree)
                                isGroupNode = true;
                            if (isGroupNode) {
                                newNode.setProperty("CssClass", "GroupTreeNode");
                                newNode.setProperty("HoverCssClass", "GroupTreeNode");
                                newNode.setProperty("SelectedCssClass", "GroupTreeNode");
                            }
                            else if (!isVisible) {
                                newNode.setProperty("CssClass", "GrayedTreeNode");
                                newNode.setProperty("HoverCssClass", "GrayedTreeNode");
                                newNode.setProperty("SelectedCssClass", "GrayedTreeNode");
                            }
                            newNode.setProperty("IsVisible", isVisible ? "true" : "false");
                            selectedNode.get_nodes().add(newNode);
                            settings.objTree.endUpdate();
                            selectedNode.Expand();
                            newNode.edit();
                            break;
                        case "MakeVisible":
                            settings.onCallback.call(this, $this, { "Action": treeActionsJson.Action });
                            if (treeActionsJson.Success == true) {
                                if (treeActionsJson.Action == "MakeVisible") {
                                    selectedNode.setProperty("CssClass", "TreeNode");
                                    selectedNode.setProperty("HoverCssClass", "HoverTreeNode");
                                    selectedNode.setProperty("SelectedCssClass", "SelectedTreeNode");
                                    selectedNode.setProperty("IsVisible", "true");
                                    treeActionsJson.IsVisible = true;
                                }
                                else {
                                    selectedNode.setProperty("CssClass", "GrayedTreeNode");
                                    selectedNode.setProperty("HoverCssClass", "GrayedTreeNode");
                                    selectedNode.setProperty("SelectedCssClass", "SelectedGrayedTreeNode");
                                    selectedNode.setProperty("IsVisible", "false");
                                    treeActionsJson.IsVisible = false;
                                }
                                methods.setVisibility($this);
                            }
                            break;
                        case "TabIndex":
                            treeActionsJson.TabIndex = commandArgument;
                            settings.onCallback.call(this, $this, { "Action": command });
                            break;
                        default:
                            treeActionsJson.Action = commandArgument;
                            settings.onCallback.call(this, $this, { "Action": treeActionsJson.Action });
                            break;
                    }

                    $this.find(".tree-actions .hasSubMenu ul").hide();
                    methods.bindControls($this);

                    return false;
                });

                methods.bindControls($this);
                if (!settings.kendoTree && settings.objTree.get_selectedNode() != null) {
                    methods.onSelectNode($this, settings.objTree.get_selectedNode());
                    settings.objTree.get_selectedNode().expand();
                    methods.setVisibility($this);
                }

                $(".navigation-links a").each(function () {
                    var linkUrl = $(this).prop("href");
                    if (linkUrl.indexOf("?") > -1)
                        linkUrl = linkUrl.substring(0, linkUrl.indexOf('?'));
                    if (document.URL.indexOf(linkUrl) > -1)
                        $(this).addClass("active");
                });

                if (!$("body").hasClass("popupBody")) {
                    $(window).load(function () {
                        shortcut.add("Ctrl+ALT+T", function () {
                            OpeniAppsShortcut(treeActionsJson.FolderId);
                        });
                    });
                }

                //adjust fixed header width
                if (settings.fixHeader && !$("body").hasClass("popupBody") && $this.find(".tree-header").length > 0) {
                    var scrollToPosition = $this.find(".tree-header").offset().top + $this.find(".tree-header").outerHeight() - 40;
                    if (!$this.find(".tree-header").hasClass("tree-header-fixed")) {
                        $(window).on('scroll', function () {
                            var hWidth = 0;
                            if (settings.kendoTree)
                                hWidth = settings.treeSelector.width();
                            else
                                hWidth = $("#" + settings.objTree.get_id()).width();
                            var scrollTop = $(window).scrollTop();
                            if ($this.find(".tree-header").hasClass("tree-header-fixed"))
                                scrollTop += $this.find(".tree-header").height();
                            if (scrollTop > scrollToPosition) {
                                if (hWidth > 0)
                                    $this.find(".tree-header").addClass('tree-header-fixed').width(hWidth);
                            }
                            else {
                                $this.find(".tree-header").removeClass('tree-header-fixed');
                            }
                        });
                    }
                }

                if (settings.kendoTree && treeActionsJson.FolderId == EmptyGuid) {
                    treeActionsJson.FolderId = $("#treeview").find("li:first").data("uid");
                }
            });
        },
        selectNode: function (eventArgs) {
            var $this = $(this);
            var settings = $this.data("settings");

            return this.each(function () {
                if (settings == null)
                    return;

                if (settings.kendoTree) {
                    gridActionsJson.FolderId = treeActionsJson.FolderId = eventArgs.data("uid");
                    treeActionsJson.Text = settings.objTree.text(eventArgs);

                    settings.objTree.select(eventArgs);

                    settings.treeSelector.find("div.selected").removeClass("selected");
                    eventArgs.children().first("div").addClass("selected");
                    //treeActionsJson.ParentId = selectedNode.get_parentNode().get_id();
                }
                else {
                    //if (settings.expandOnSelect)
                    //    eventArgs.get_node().expand();
                    methods.onSelectNode($this, eventArgs.get_node());
                }

                settings.onItemSelect.call(this, $this, eventArgs);

                methods.setVisibility($this);
            });
        },
        setVisibility: function ($this) {
            if (treeActionsJson.IsVisible) {
                $this.getItemByCommand("MakeVisible").setComandArgument("MakeInVisible");
                $this.getItemByCommand("MakeVisible").attr("title", "Make Invisible");
                $this.getItemByCommand("MakeVisible").attr("src", ResolveClientUrl("~/App_Themes/General/images/tree/node-invisible.png"));
            }
            else {
                $this.getItemByCommand("MakeVisible").setComandArgument("MakeVisible");
                $this.getItemByCommand("MakeVisible").attr("title", "Make Visible");
                $this.getItemByCommand("MakeVisible").attr("src", ResolveClientUrl("~/App_Themes/General/images/tree/node-visible.png"));
            }
        },
        bindControls: function ($t) {
            var $this = $(this);
            if ($t)
                $this = $t;

            $this.find(".tree-actions ul.tab-menu li").removeClass("selected").removeClass("selected-tab");
            var tSelected = $this.find(".tree-actions ul.tab-menu li[commandArgument='" + treeActionsJson.TabIndex + "']");
            $(tSelected).addClass("selected").addClass("selected-tab");
            $(tSelected).prev('li').addClass('prev').siblings().removeClass('prev');
            $(tSelected).next('li').addClass('next').siblings().removeClass('next');

            $this.find(".tree-actions select[commandName='SiteSelector']").val(treeActionsJson.SiteId);

            $this.find(".tree-actions .tree-node-action").hide();
            if (treeActionsJson.HideActions == false) {
                $this.find(".tree-actions .tree-node-action").each(function () {
                    if ($.inArray(treeActionsJson.NodeType, $(this).attr("nodeTypes").split(",")) > -1)
                        $(this).show();
                    else
                        $(this).hide();
                });
            }
        },
        beforeRename: function (eventArgs) {
            var $this = $(this);
            var settings = $this.data("settings");

            return this.each(function () {
                if (settings == null)
                    return;

                treeActionsJson.Success = true;
                if (eventArgs.get_newText() == "") {
                    treeActionsJson.Success = false;
                    alert("Please try entering the title again, and ensure it is not blank.");
                }
                else if (eventArgs.get_newText().match(settings.nameRegex)) {
                    treeActionsJson.Success = false;
                    alert("Please try entering the title again, and ensure it is has valid characters.");
                }
                else {
                    var siblingNodes = eventArgs.get_node().get_parentNode().get_nodes();
                    for (var i = 0; i < siblingNodes.get_length() - 1; i++) {
                        if (eventArgs.get_node().get_id() != siblingNodes.getNode(i).get_id() && eventArgs.get_newText().toLowerCase() == siblingNodes.getNode(i).get_text().toLowerCase()) {
                            treeActionsJson.Success = false;
                            alert("Title you're trying to enter already exists. Please try again with a different name.");
                            break;
                        }
                    }
                }
                if (treeActionsJson.Success) {
                    treeActionsJson.NewText = eventArgs.get_newText();
                    if (treeActionsJson.Action == "AddNode") {
                        treeActionsJson.ParentId = treeActionsJson.FolderId;
                        treeActionsJson.FolderId = eventArgs.get_node().get_id();
                    }
                    settings.onCallback.call(this, $this, { "Action": "ValidateName" });
                }
                if (treeActionsJson.Success) {
                    settings.onCallback.call(this, $this, { "Action": "UpdateName" });
                    if (treeActionsJson.Success) {
                        eventArgs.get_node().set_id(treeActionsJson.FolderId);
                        treeActionsJson.Text = treeActionsJson.NewText;
                        $this.data("NodeRenamed", true);
                        eventArgs.get_node().select();
                        methods.onSelectNode($this, eventArgs.get_node());
                        $this.removeData("NodeRenamed");
                    }
                }
                if (treeActionsJson.Success == false) {
                    if (treeActionsJson.Action == "AddNode") {
                        eventArgs.get_node().get_parentNode().select();
                        eventArgs.get_node().remove();
                    }
                    else {
                        eventArgs.get_node().setProperty("Text", treeActionsJson.Text);
                        //setTimeout(function(){ eventArgs.get_node().edit();}, 10);
                    }
                    eventArgs.set_cancel(true);
                    //settings.objTree.render();
                }
            });
        },
        onSelectNode: function ($this, selectedNode) {
            treeActionsJson.IsVisible = true;
            if (typeof selectedNode.getProperty("IsVisible") != "undefined")
                treeActionsJson.IsVisible = selectedNode.getProperty("IsVisible").toLowerCase() == "true";

            treeActionsJson.NodeType = selectedNode.getProperty("NodeType");
            treeActionsJson.SelectedValue = selectedNode.get_value();
            treeActionsJson.FolderId = selectedNode.get_id();
            if ($this.data("NodeRenamed") != true) {
                treeActionsJson.Text = selectedNode.get_text();
            }
            if (selectedNode.get_parentNode() != null)
                treeActionsJson.ParentId = selectedNode.get_parentNode().get_id();

            treeActionsJson.SelectedPath = selectedNode.get_text();
            while (selectedNode.get_parentNode() != null) {
                treeActionsJson.SelectedPath = selectedNode.get_parentNode().get_text() + "&nbsp;>&nbsp;" + treeActionsJson.SelectedPath;
                selectedNode = selectedNode.get_parentNode();
            }

            if (typeof UpdateHideActions == "function")
                UpdateHideActions();

            methods.bindControls($this);
        },
        getSelectNode: function () {
            var $this = $(this);
            var settings = $this.data("settings");

            if (settings == null)
                return;

            if (settings.kendoTree)
                return settings.objTree.findByUid(treeActionsJson.FolderId);
            else
                return settings.objTree.findNodeById(treeActionsJson.FolderId);
        },
    };

    $.fn.treeActions = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.treeActions');
        }

    };

})(jQuery);

(function ($) {
    var methods = {
        init: function (options) {
            var settings = $.extend({
                fixHeader: true,
                onCallback: function (sender, eventArgs) {
                    if (typeof Group_Callback == 'function') Group_Callback(sender, eventArgs);
                },
                onRebind: function (sender, eventArgs) {
                    if (typeof Group_Rebind == 'function') Group_Rebind(sender, eventArgs);
                },
                onItemSelect: function (sender, eventArgs) {
                    if (typeof Group_ItemSelect == 'function') Group_ItemSelect(sender, eventArgs);
                }
            }, options);

            var $this = $(this);

            $this.data("settings", settings);

            $this.off("click", ".group-actions .command-button");
            $this.on("click", ".group-actions .command-button", function (evt) {
                var command = $(this).attr("commandName");
                var commandArgument = $(this).attr("commandArgument");
                treeActionsJson.Action = commandArgument;

                var selectedItem = methods.getSelectedItem_($this);

                settings.onCallback.call(this, $this, { "Action": commandArgument });

                return false;
            });

            $this.off("click", ".group-item");
            $this.on("click", ".group-item", function () {
                methods.selectItem($(this).attr("objectId"), $this);
            });

            if (!$("body").hasClass("popupBody")) {
                $(window).load(function () {
                    shortcut.add("Ctrl+ALT+T", function () {
                        OpeniAppsShortcut(treeActionsJson.FolderId);
                    });
                });
            }

            //adjust fixed header width
            if (settings.fixHeader && !$("body").hasClass("popupBody") && $this.find(".group-header").length > 0) {
                var scrollToPosition = $this.find(".group-header").offset().top + $this.find(".group-header").outerHeight() - 40;
                if (!$this.find(".group-header").hasClass("group-header-fixed")) {
                    $(window).on('scroll', function () {
                        var hWidth = $this.width();
                        var scrollTop = $(window).scrollTop();
                        if ($this.find(".group-header").hasClass("group-header-fixed"))
                            scrollTop += $this.find(".group-header").height();
                        if (scrollTop > scrollToPosition) {
                            if (hWidth > 0)
                                $this.find(".group-header").addClass('group-header-fixed').width(hWidth);
                        }
                        else {
                            $this.find(".group-header").removeClass('group-header-fixed');
                        }
                    });
                }
            }

            $this.data("Initialized", true);
        },
        selectItem: function (selectedId, $t) {
            var $this = $(this);
            if ($t)
                $this = $t;

            var settings = $this.data("settings");

            var $selected = $this.find(".group-item[objectId='" + selectedId + "']");
            if ($selected.length > 0) {
                var selectedItem = {};
                selectedItem.Id = selectedId;
                selectedItem.Title = $selected.find(".group-item-title").text();
                var itemNodeType = $selected.find(".group-item-node-type").val();
                if (isNaN(itemNodeType))
                    treeActionsJson.NodeType = 0;
                else
                    treeActionsJson.NodeType = parseInt(itemNodeType);

                treeActionsJson.FolderId = selectedItem.Id;
                treeActionsJson.Text = selectedItem.Title;

                $this.data("selectedItem", selectedItem);

                $this.find(".group-item").removeClass("selected");
                $selected.addClass("selected");
            }

            methods.resetGroupItemActions($this);

            settings.onItemSelect.call(this, $this, $(this));
        },
        bindControls: function (options, $t) {
            var settings = $.extend({

            }, options);

            var $this = $(this);
            if ($t)
                $this = $t;

            var settings = $this.data("settings");

            if (treeActionsJson.FolderId == EmptyGuid) {
                var firstItem = $this.find(".group-item").first();
                if (firstItem.length > 0)
                    treeActionsJson.FolderId = firstItem.attr("objectId");
            }

            if (treeActionsJson.FolderId != EmptyGuid)
                methods.selectItem(treeActionsJson.FolderId, $this);

            methods.resetGroupItemActions($this);
        },
        displayMessage: function (options) {
            var $this = $(this);
            var thisSettings = $.extend({
                complete: function () { }
            }, options);

            if (typeof GetSessionTreeActionJson == 'function') {
                methods.onDisplayMessage($this, thisSettings, GetSessionTreeActionJson());
            }
            else if (typeof PageMethods == 'function' && typeof PageMethods.WebMethod_GetSessionTreeActionJson == 'function') {
                PageMethods.WebMethod_GetSessionTreeActionJson(function (result) {
                    methods.onDisplayMessage($this, thisSettings, result);

                    methods.bindControls(options, $this);
                });
            }
        },
        onDisplayMessage: function ($this, settings, result) {
            if (result != "") {
                treeActionsJson = JSON.parse(result);

                if (treeActionsJson.ResponseMessage) {
                    ShowiAppsNotification(treeActionsJson.ResponseMessage,
                        treeActionsJson.ResponseCode == 100 || treeActionsJson.ResponseCode == 0 ? true : false, false);
                    //$("<div />").html(gridActionsJson.ResponseMessage).iAppsDialog("open", {alertBox: true});
                }

                treeActionsJson.ResponseMessage = null;
            }

            treeActionsJson.ResponseCode = 0;
            treeActionsJson.Action = "LoadData";
        },
        getSelectedItem_: function ($this) {
            if ($this.data("selectedItem") == null)
                return null;

            return $this.data("selectedItem");
        },
        getSelectedItem: function () {
            var $this = $(this);

            return methods.getSelectedItem_($this);
        },
        rebind: function () {
            var $this = $(this);

            var settings = $this.data("settings");

            settings.onRebind.call(this, $this);
        },
        resetGroupItemActions: function ($this) {
            $this.find(".group-action").show();
            $this.find(".group-item-action").hide();

            var selectedItem = methods.getSelectedItem_($this);
            if (selectedItem != null && selectedItem.Id != "" && treeActionsJson.NodeType == 1) {
                $this.find(".group-item-action").show();
            }
        }
    };

    $.fn.groupActions = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.groupActions');
        }

    };

})(jQuery);

$(function () {
    $(document).click(function (e) {
        if (!$(e.target).closest(".grid-actions .hasSubMenu").length) {
            $(".grid-actions .hasSubMenu ul").hide();
        }

        if (!$(e.target).closest(".iapps-click-menu").length && !$(e.target).closest(".iapps-click-item").length) {
            $(".iapps-click-menu").remove();
            $(".iapps-click-item").removeClass("iapps-click-item");
        }
    });
});

(function ($) {
    var methods = {
        init: function (options) {
            var settings = $.extend({
                onTabSelect: function () { }
            }, options);

            return this.each(function () {
                var $this = $(this);

                $this.children("ul").addClass("iapps-tabs clear-fix");
                $this.children("div").addClass("iapps-tab-content clear-fix");
                $this.children("div").not(":first").hide();

                setTimeout(function () {
                    $this.children("ul").children("li:visible:first").addClass("selected-tab");
                    $this.children("ul").children("li:visible:first").addClass("first");
                    $this.children("ul").children("li:visible:last").addClass("last");

                    $this.children("ul").find("li").click(function () {
                        var selectedTabIndex = $this.children("ul").find("li").index($(this));
                        $(this).addClass("selected-tab");
                        $(this).prev('li').addClass('prev').siblings().removeClass('prev');
                        $(this).next('li').addClass('next').siblings().removeClass('next');
                        $(this).siblings().removeClass("selected-tab");

                        $this.children("div").hide();
                        $($this.children("div").get(selectedTabIndex)).show();

                        settings.onTabSelect.call(this, { selectedIndex: selectedTabIndex, selectedItem: $this.children("ul").find("li") });
                    });
                }, 500);

            });
        },
        selectTab: function (selectedIndex) {
            return this.each(function () {
                var $this = $(this);
                selectedIndex--;

                var selectedLi = $($this.children("ul").find("li").get(selectedIndex));
                selectedLi.addClass("selected-tab");
                selectedLi.siblings().removeClass("selected-tab");

                $this.children("div").hide();
                $($this.children("div").get(selectedIndex)).show();

                $this.children("ul").children("li").removeClass("first");
                $this.children("ul").children("li:visible:first").addClass("first");

                $this.children("ul").children("li").removeClass("last");
                $this.children("ul").children("li:visible:last").addClass("last");
            });
        },
        getTabByIndex: function (index, $t) {
            var $this = $(this);
            if ($t)
                $this = $t;
            index--;

            var item = {};
            item.Tab = $($this.children("ul").find("li").get(index));
            item.Content = $($this.children("div").get(index));
            item.Index = index + 1;
            item.Id = item.Tab.attr("id");

            return item;
        },
        getTabById: function (id, $t) {
            var $this = $(this);
            if ($t)
                $this = $t;

            var index = $this.children("ul").find("#" + id).index();
            var item = {};
            item.Tab = $($this.children("ul").find("li").get(index));
            item.Content = $($this.children("div").get(index));
            item.Index = index + 1;
            item.Id = id;

            return item;
        },
        setVisibility: function (index, isVisible) {
            var $this = $(this);

            var selectedTab = methods.getTabByIndex(index, $this);
            if (isVisible) {
                selectedTab.Tab.show();
                //selectedTab.Content.show();
            }
            else {
                selectedTab.Tab.hide();
                //selectedTab.Content.hide();
            }

            $this.children("ul").children("li:visible:first").addClass("first");
            $this.children("ul").children("li:visible:last").addClass("last");
        }

    };
    $.fn.iAppsTabs = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.iAppsTabs');
        }

    };
})(jQuery);

(function ($) {
    $.fn.iAppsFileUpload = function (options) {
        var settings = $.extend({
            buttonText: 'Browse',
            textboxWidth: '100px',
            buttonClass: 'button',
            checkFileName: false,
            customLook: false
        }, options);

        return this.each(function () {
            var $this = $(this);
            if (settings.customLook) {
                $this.addClass("iapps-file").css("opacity", 0)
                .wrap('<div class="iapps-file-upload"></div>');

                var textBoxId = $this.attr('id') + '_fakeFile';
                $this.parent().append($('<div class="iapps-fake-file" />')
                .append($('<input type="text" class="textBoxes" />')
                .attr('id', textBoxId)
                .css('width', settings.textboxWidth))
                .append($('<input type="button" />')
                .addClass(settings.buttonClass)
                .val(settings.buttonText))
                .append('<img src="' + ResolveClientUrl("~/App_Themes/General/images/icon-delete-cross.png") + '" alt="Remove" />'));
            }

            $this.bind('change', function (e) {
                var ext = $(this).val().split('.').pop().toLowerCase();
                var oldfileName = $this.siblings(".coaching-text").html();
                var newfileName = $this.val();
                if (newfileName.indexOf("\\") > 0)
                    newfileName = newfileName.substr(newfileName.lastIndexOf("\\") + 1);

                if (settings.allowedExtensions && $.inArray(ext, settings.allowedExtensions.split(',')) == -1) {
                    alert('Invalid extension!\nAllowed extension(s) - ' + settings.allowedExtensions);

                    $this.val("");
                    $this.parent().html($this.parent().html());
                    e.stopPropagation();
                }
                else if (settings.checkFileName && oldfileName != "" && oldfileName != null && oldfileName != newfileName) {
                    alert('Please upload file with same name - ' + oldfileName);

                    $this.val("");
                    $this.parent().html($this.parent().html());
                    e.stopPropagation();
                }
                else {
                    if (settings.customLook) {
                        $('#' + textBoxId).val($(this).val());
                    }
                }
            });
        });
    };
})(jQuery);

(function ($) {
    $.fn.iAppsClickMenu = function (options) {
        var settings = $.extend({
            create: function () { },
            height: 50,
            width: 200,
            maxHeight: 250,
            enableLoadingPanel: false
        }, options);

        return this.each(function () {
            var $this = $(this);

            $this.on("click", function (e) {
                $(".iapps-click-menu").remove();
                $(".iapps-click-item").removeClass("iapps-click-item");

                var jMenuHolder = $("<div />").css({
                    "width": settings.width
                });

                var jMenuArrow = $('<div class="iapps-menu-arrow-up"><!-- Arrow Up --></div>');
                jMenuHolder.append(jMenuArrow);
                var jMenuHeader = stringformat('<div class="iapps-menu-header clear-fix"><h4>{0}</h4><img alt="close" src="{1}" /></div>',
                    settings.heading, ResolveClientUrl("~/App_Themes/General/images/close.gif"));
                var jMenuContainer = $(stringformat('<div class="iapps-menu-container">{0}</div>', jMenuHeader)).bind("click",
                    function () {
                        $(".iapps-click-menu").remove();
                        $(".iapps-click-item").removeClass("iapps-click-item");
                    });
                var jMenu = $("<ul />").css({
                    "max-height": settings.maxHeight
                });
                jMenuHolder.append(jMenuContainer.append(jMenu));
                if (settings.menuId)
                    jMenuHolder = $("#" + settings.menuId);
                else if (settings.enableLoadingPanel)
                    jMenu.addListItem(stringformat('<div><img src="{0}" alt="Loading..." /></div>', jSpinnerUrl), 'iapps_cm_lp');

                $this.addClass("iapps-click-item");
                $("body").append(jMenuHolder.addClass("iapps-click-menu")
                        .css({
                            "top": $(this).offset().top + $this.height(),
                            "min-height": settings.height,
                            "left": ($this.offset().left + $this.outerWidth() - $(jMenuHolder).outerWidth())
                        }));

                settings.create.call(this, $this, jMenu);
                jMenuHolder.removeListItem('iapps_cm_lp');
                jMenuHolder.css("left", ($this.offset().left + $this.outerWidth() - $(jMenuHolder).outerWidth()));
                jMenuArrow.css("margin-left", (jMenuHolder.outerWidth() - 20));
                return false
            });

        });
    };
})(jQuery);

(function ($) {
    var methods = {
        init: function (options) {
            var settings = $.extend({
                appendToEnd: false
            }, options);

            return this.each(function () {
                var $this = $(this);

                methods.onInit($this, settings.appendToEnd, false);
            });
        },
        open: function (options) {
            var settings = $.extend({
                displayAfter: 0,
                alertBox: false
            }, options);

            return this.each(function () {
                var $this = $(this);
                $this.data("closed", false);

                if ($this.data("init") != true)
                    methods.onInit($this, false, settings.alertBox);

                var top = (($(window).height() - $this.height()) / 2);
                if (top < 0) top = 0;
                top += $(window).scrollTop();

                var left = (($(window).width() - $this.width()) / 2);
                if (left < 0) left = 0;
                left += $(window).scrollLeft();
                if (settings.displayAfter < 0)
                    settings.displayAfter = 0;

                setTimeout(function () {
                    if ($this.data("closed") == false) {
                        $this.css({
                            top: top - 50,
                            left: left
                        }).show();
                        setTimeout(function () {
                            if ($this.data("closed") == false) {
                                var overlay = $this.data("overlay");
                                if (overlay)
                                    overlay.show().animate({ opacity: 0.5 }, "fast").css({ zIndex: 19995 });
                            }
                        }, (settings.displayAfter * 1000) + 100);
                    }
                }, settings.displayAfter * 1000);

                $this.off("click", ".close-button");
                $this.on("click", ".close-button", function () {
                    $this.data("closed", true);
                    $this.hide();
                    methods.hideOverlay($this);
                });
            });
        },
        close: function () {
            return this.each(function () {
                var $this = $(this);

                $this.data("closed", true);
                $this.hide();
                methods.hideOverlay($this);
            });
        },
        center: function () {
            return this.each(function () {
                var $this = $(this);

                if ($this.is(":visible")) {
                    var $window = $(window);

                    var top = ($window.height() - $this.height()) / 2;

                    if (top < 0)
                        top = 0;

                    top += $window.scrollTop();

                    var left = ($window.width() - $this.width()) / 2;

                    if (left < 0)
                        left = 0;

                    left += $window.scrollLeft();

                    $this.css({
                        top: top - 50,
                        left: left
                    }).show();
                }
            });
        },
        destroy: function () {
            return this.each(function () {
                var $this = $(this);

                $this.remove();
            });
        },
        onInit: function ($this, appendToEnd, alertBox) {
            $this.css({
                position: "absolute",
                zIndex: 19996
            }).hide();

            if (appendToEnd)
                $this.appendTo("form");

            if (alertBox) {
                $this.wrapInner('<div class="iapps-alert-content"></div>').append($("<div />").addClass("iapps-alert-footer").
                    append($("<input type='button' value='Close' />").
                    bind("click", function () {
                        methods.hideOverlay($this);
                        $this.remove();
                    })));
                $this.wrapInner('<div class="iapps-alert-box"></div>');

                $("body").append($this);
            }
            var overlay = $(".iAPPSPopupOverlay");
            if (overlay.length == 0) {
                overlay = $("<div />").addClass("iAPPSPopupOverlay iapps-modal-overlay").hide().css({ opacity: 0 });
                $("body").append(overlay);
            }
            $this.data("overlay", overlay);
            $this.data("init", true);
        },
        hideOverlay: function ($this) {
            var overlay = $this.data("overlay");
            if (overlay)
                overlay.hide().css({ opacity: 0, zIndex: 9998 });
        }
    };
    $.fn.iAppsDialog = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.iAppsDialog');
        }

    };
})(jQuery);

(function ($) {
    var methods = {
        init: function (options) {
            var settings = $.extend({
                container: ".errorMessage",
                success: false,
                autoHide: false
            }, options);

            return this.each(function () {
                var $this = $(this);

                $this.data("settings", settings);

                var $container = $this.find(settings.container);
                $container.contents().filter(function () {
                    return this.nodeType == 3;
                }).remove();

                $container.closest(".messageContainer").show();

                $container.removeClass("successMessage");

                if (settings.success)
                    $container.addClass("successMessage");
            });
        },
        show: function (message, options) {
            var thisSettings = $.extend({
                focus: false
            }, options);

            return this.each(function () {
                var $this = $(this);

                var settings = $this.data("settings");

                var $container = $this.find(settings.container);
                $container.append(message);

                $this.slideDown();
                if (settings.autoHide)
                    $this.delay(4000).slideUp();

                if (thisSettings.focus) {
                    $("html, body").animate({
                        scrollTop: $this.offset().top - 100
                    }, "slow");
                }
            });
        },
        close: function () {
            return this.each(function () {
                var $this = $(this);

                $this.slideUp();
            });
        }
    };
    $.fn.iAppsNotification = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.iAppsNotification');
        }

    };
})(jQuery);

(function ($) {
    $.fn.iAppsSplitter = function (options) {
        var settings = $.extend({
            leftSection: ".left-control",
            rightSection: ".right-control",
            leftScrollSection: ".TreeView",
            content: "&nbsp;",
            minHeight: 600,
            maxHeight: 0,
            borderIndex: 0,
            leftHeaderHeight: 0
        }, options);

        return this.each(function () {
            var $this = $(this);
            if ($this.hasClass("splitter-loaded") && $("body").hasClass("popupBody"))
                return;

            setTimeout(function () {
                var rightSection = $this.find(settings.rightSection);
                var leftSection = $this.find(settings.leftSection);

                if (leftSection.length == 0 || rightSection.length == 0) {
                    return;
                }

                leftSection.find(settings.leftScrollSection).css({ "width": "auto", "height": "auto" });
                leftSection.css("min-height", 0);
                rightSection.css("min-height", 0);

                var height = rightSection.height() + settings.borderIndex;
                if (leftSection.height() > height)
                    height = leftSection.height();

                if ($("body").hasClass("popupBody"))
                    height = 455;
                else if (settings.minHeight > height)
                    height = settings.minHeight;

                if (settings.maxHeight > 0 && height > settings.maxHeight)
                    height = settings.maxHeight;

                height = height - 1;
                leftSection.css("min-height", height);

                var bBottom = 0;
                if (leftSection.height() + settings.borderIndex > rightSection.height()) {
                    bBottom = 1;
                }

                //rightSection.css("min-height", height);
                rightSection.css({ "min-height": height, "border-bottom-width": bBottom });

                var leftScrollHeight = height;
                if (leftSection.find(".tree-header-fixed").length == 0 && leftSection.find(".tree-header").length > 0)
                    leftScrollHeight = height - leftSection.find(".tree-header").height();
                else if (leftSection.find(".group-header-fixed").length == 0 && leftSection.find(".group-header").length > 0)
                    leftScrollHeight = height - leftSection.find(".group-header").height();

                if ($("body").hasClass("popupBody"))
                    leftScrollHeight = 417;

                leftSection.find(settings.leftScrollSection).css({
                    "width": "auto",
                    "overflow-x": "auto",
                    "overflow-y": "auto",
                    "height": leftScrollHeight
                });

                if ($this.find(".tree-header").length > 0 || $this.find(".group-header").length > 0) {
                    $(window).on('scroll', function () {
                        if ($this.find(".tree-header").length > 0) {
                            if (leftSection.find(".tree-header-fixed").length > 0)
                                leftSection.css("min-height", leftScrollHeight + 1 - settings.leftHeaderHeight);
                            else
                                leftSection.css("min-height", height + 0);
                        }

                        if ($this.find(".group-header").length > 0) {
                            if (leftSection.find(".group-header-fixed").length > 0)
                                leftSection.css("min-height", leftScrollHeight + 1 - settings.leftHeaderHeight);
                            else
                                leftSection.css("min-height", height + 0);
                        }

                        if (rightSection.find(".grid-actions-fixed").length > 0)
                            rightSection.css("min-height", leftScrollHeight + 1 - settings.leftHeaderHeight);
                        else
                            rightSection.css("min-height", height + 0);
                    });
                }

                if (typeof cbTreeRefresh != "undefined") {
                    var html = stringformat("<table><tr><td class='tree-loading' style='height:{0}px;width:{1}px;'><img src='{0}' border='0' alt='Loading...' /></td></tr></table>",
                        jSpinnerUrl);
                    cbTreeRefresh.LoadingPanelClientTemplate = stringformat(html, leftScrollHeight, leftSection.width());
                }

                rightSection.find(".iapps-splitter-bar").each(function () {
                    $(this).remove();
                });

                rightSection.css("position", "relative")
                    .append($('<div class="iapps-splitter-bar">' + settings.content + '</div>')
                            .css({ "top": height / 2 })
                            .bind("click", function () {
                                if (leftSection.css('display') == "none") {
                                    $(this).removeClass('iapps-splitter-expanded');
                                    rightSection.toggleClass("iapps-expand-section");
                                    rightSection.find(".grid-actions").width(rightSection.width());
                                    leftSection.toggle("slow");
                                }
                                else {
                                    $(this).addClass('iapps-splitter-expanded');
                                    leftSection.toggle("slow", function () {
                                        rightSection.toggleClass("iapps-expand-section");
                                        rightSection.find(".grid-actions").width(rightSection.width());
                                    });
                                }
                            })
                            .hover(
                                function () { $(this).addClass("iapps-splitter-hover"); },
                                function () { $(this).removeClass("iapps-splitter-hover"); }
                            )
                        );
            }, 10);

            $this.addClass("splitter-loaded")
        });
    }
})(jQuery);

(function ($) {
    $.fn.iAppsFixHeader = function (options) {
        var settings = $.extend({
            contentSection: ".contentSection"
        }, options);

        return this.each(function () {
            var $this = $(this);

            var scrollToPosition = $this.offset().top + 40;
            $(window).on('scroll', function () {
                if ($(window).scrollTop() > scrollToPosition) {
                    $this.addClass('iapps-header-fixed').width($(settings.contentSection).width());
                }
                else {
                    $this.removeClass('iapps-header-fixed');
                }
            });

        });
    }
})(jQuery);

(function ($) {
    $.fn.iAppsMegaMenu = function (options) {
        var settings = $.extend({
            useToken: false,
            showLoading: false
        }, options);

        return this.each(function () {
            var $this = $(this);

            if (settings.useToken) {
                $this.children("li").each(function () {
                    var productName = "cms";

                    if ($(this).hasClass("iapps-commerce"))
                        productName = "commerce";
                    else if ($(this).hasClass("iapps-marketier"))
                        productName = "marketier";
                    else if ($(this).hasClass("iapps-analyzer") || $(this).hasClass("iapps-insights"))
                        productName = "analyzer";
                    else if ($(this).hasClass("iapps-social"))
                        productName = "social";

                    var isValid = CheckLicense(productName, false);

                    $(this).toggleClass("iapps-no-access", !isValid);
                    $(this).data("productName", productName);
                });

                $this.on("click", "a", function (event) {
                    event.preventDefault();

                    var productName = $(this).parents("li:last").data("productName");

                    if ($(this).closest(".iapps-no-access").size() == 0) {
                        if (settings.showLoading)
                            ShowiAppsLoadingPanel();

                        RedirectWithToken(productName, $(this).prop("href"));
                    }
                    else
                        CheckLicense(productName, true);
                });
            }
        });
    }
})(jQuery);

(function ($) {
    if (!$.setCookie) {
        $.extend({
            setCookie: function (c_name, value, exdays) {
                try {
                    if (!c_name) return false;
                    var exdate = new Date();
                    exdate.setDate(exdate.getDate() + exdays);
                    var c_value = escape(value) + ";path=/" + ((exdays == null) ? "" : ";expires=" + exdate.toUTCString());
                    document.cookie = c_name + "=" + c_value;
                }
                catch (err) {
                    return false;
                };
                return true;
            }
        });
    };
    if (!$.getCookie) {
        $.extend({
            getCookie: function (c_name) {
                try {
                    var i, x, y,
                        ARRcookies = document.cookie.split(";");
                    for (i = 0; i < ARRcookies.length; i++) {
                        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                        x = x.replace(/^\s+|\s+$/g, "");
                        if (x == c_name) return unescape(y);
                    };
                }
                catch (err) {
                    return false;
                };
                return false;
            }
        });
    };
    if (!$.deleteCookie) {
        $.extend({
            deleteCookie: function (c_name) {
                try {
                    if (!c_name) return false;
                    var exdate = new Date();
                    exdate.setDate(exdate.getDate() - 7);
                    var c_value = "DELETED;path=/" + ((exdays == null) ? "" : ";expires=" + exdate.toUTCString());
                    document.cookie = c_name + "=" + c_value;
                }
                catch (err) {
                    return false;
                };
                return true;
            }
        });
    };
    if (!$.getQueryString) {
        $.extend({
            getQueryString: function (queryString) {
                if (typeof queryString == "undefined") {
                    if (window.location != null)
                        queryString = window.location.search.substr(1);
                    else
                        queryString = "";
                }

                var params = queryString.split("&");
                var qsCollection = {};
                for (var i = 0; i < params.length; i++) {
                    var pair = params[i].split("=");
                    if (pair[0] != null && pair[1] != null)
                        qsCollection[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1].replace(/\+/g, " "));
                }

                return qsCollection;
            }
        });
    };

    $.fn.onEnter = function (func) {
        this.bind("keypress", function (e) {
            if (e.keyCode == 13) return func.apply(this, [e]);
        });
        return this;
    };
})(jQuery);

Array.prototype.first = function () {
    if (this.length > 0)
        return this[0];
    else
        return "";
};

Array.prototype.last = function () {
    if (this.length > 0)
        return this[this.length - 1];
    else
        return "";
};

Array.prototype.remove = function (value) {
    var $this = this;
    var items = JSON.parse(JSON.stringify($this));
    $this.clear();
    $.each(items, function () {
        if (this.toString().toLowerCase() != value.toString().toLowerCase())
            $this.push(this);
    });
};

Array.prototype.clear = function (value) {
    this.splice(0, this.length);
};

Array.prototype.exists = function (value) {
    var exist = false;
    $.each(this, function () {
        if (this.toString().toLowerCase() == value.toString().toLowerCase())
            exist = true;
    });

    return exist;
};

Array.prototype.equals = function (array) {
    return $(this).not(array).length == 0 && $(array).not(this).length == 0
};

String.prototype.replaceAll = function (from, to) {
    return this.split(from).join(to);
};

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}

String.prototype.endsWith = function (suffix) {
    return (this.substr(this.length - suffix.length) === suffix);
}

String.prototype.startsWith = function (prefix) {
    return (this.substr(0, prefix.length) === prefix);
}

String.prototype.equals = function (text) {
    if (text == null)
        text = "";

    return this.toString().toLowerCase() == text.toString().toLowerCase();
}

String.prototype.toPlainText = function () {
    return $("<div>").html(this).text();
};

jQuery.fn.refresh = function () {
    return $(this.selector);
};

jQuery.fn.getItemByCommand = function (commandName) {
    return $(this).find(".command-button[commandName='" + commandName + "']");
};

jQuery.fn.getElementByName = function (name) {
    return $(this).find("[name='" + name + "']");
};

jQuery.fn.setText = function (text) {
    if ($(this).is("input"))
        return $(this).val(text);
    else
        return $(this).html(text);
};

jQuery.fn.getText = function (selector) {
    if (selector) {
        return jQuery.trim($(this).find(selector).html());
    }
    else {
        return jQuery.trim($(this).html());
    }
};

jQuery.fn.getValue = function (fieldName) {
    var gVal = jQuery.trim($(this).find("*[dataField = '" + fieldName + "']").html());
    if (gVal == "")
        gVal = jQuery.trim($(this).find("*[dataField = '" + fieldName + "']").val());

    return gVal;
};

jQuery.fn.showButton = function () {
    return $(this).parent().show();
};

jQuery.fn.hideButton = function () {
    return $(this).parent().hide();
};

jQuery.fn.setComandArgument = function (commandArgument) {
    return $(this).attr("commandArgument", commandArgument);
};

jQuery.fn.getKey = function () {
    return $(this).attr("objectId");
};

jQuery.fn.getItemFromKey = function (key) {
    return $(this).find(".grid-item[objectId = '" + key + "']");
};

jQuery.fn.setComandArgument = function (commandArgument) {
    return $(this).attr("commandArgument", commandArgument);
};

jQuery.fn.addListItem = function (listItemText, id) {
    var listItem = $("<li />").html(listItemText);
    if (id)
        listItem.attr("id", id);

    return $(this).append(listItem);
};

jQuery.fn.getList = function (settings) {
    var className = settings.objList.attr("class");

    if (typeof className != "undefined") {
        if (className.indexOf(" "))
            className = className.substring(0, className.indexOf(" "));

        return this.find("." + className);
    }
    else {
        return null;
    }
};

jQuery.fn.addSortItem = function (text, commandArgument) {
    var listItem = $("<li />").html(text).addClass("command-button").attr({ "CommandName": "Sort", "CommandArgument": commandArgument });
    if (commandArgument == gridActionsJson.SortBy) {
        if (gridActionsJson.SortOrder == "ASC")
            listItem.addClass("selected-asc");
        else
            listItem.addClass("selected-desc");
    }
    return $(this).append(listItem);
};

jQuery.fn.removeListItem = function (id) {
    return $(this).find("#" + id).remove();
};

jQuery.fn.outerHtml = function () {
    return $('<div>').append($(this).clone()).html();
};

jQuery.fn.setEmptyGridStyle = function () {
    var emptyGridCell = $(this).find("td").filter(function () {
        return $(this).attr("colspan") == "199" || $(this).attr("height") == "20";
    });
    emptyGridCell.addClass("empty-grid-cell");
};

jQuery.propHooks.checked = {
    set: function (el, value) {
        if (el.checked !== value) {
            el.checked = value;
            $(el).trigger('change');
        }
    }
};

jQuery.fn.removeAttributes = function () {
    return this.each(function () {
        var attributes = $.map(this.attributes, function (item) {
            return item.name;
        });
        var img = $(this);
        $.each(attributes, function (i, item) {
            img.removeAttr(item);
        });
    });
}

jQuery.fn.shortenText = function (length) {
    return this.each(function () {
        var text = $(this).html();

        if (text.length > length) {
            var trimmedText = text.substr(0, length);

            $(this).html(trimmedText.substr(0, Math.min(trimmedText.length, trimmedText.lastIndexOf(" "))));
        }
    });
};

$.extend({
    toDate: function (jsonDateString) {
        return new Date(parseInt(jsonDateString.replace('/Date(', '')));
    }
});

﻿<%@ Page Language="C#" CodeBehind="~/AngularPage.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CommonLogin.Web.AngularPage" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="UTF-8">
    <title>Unbound - Bridging the gaps between marketing automation, web content management, eCommerce, social media & website insights!</title>
    <link rel="manifest" href="~/common/favicon/manifest.json">
    <link rel="shortcut icon" type="image/x-icon" href="~/common/favicon/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link runat="server" rel="stylesheet" type="text/css" href="~/css/iapps-admin.css" />
</head>
<body>
    <div class="iapps-reset">
        <common-login></common-login>
    </div>
    <script type="text/javascript" src="angular/script/iapps-common-login.min.js" defer="defer"></script>
    <form runat="server"></form>
</body>
</html>

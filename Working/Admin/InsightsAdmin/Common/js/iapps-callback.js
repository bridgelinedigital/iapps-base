﻿var Bridgeline = Bridgeline || {};

Bridgeline.addNamespace = function (ns_string) {
    var parts = ns_string.split('.'), parent = Bridgeline, i;
    if (parts[0] === "Bridgeline") {
        parts = parts.slice(1);
    }
    for (i = 0; i < parts.length; i += 1) {
        if (typeof parent[parts[i]] === "undefined") {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
}

Bridgeline.addNamespace("Bridgeline.Framework.Common");
Bridgeline.addNamespace("Bridgeline.Framework.WebApi");
Bridgeline.addNamespace("Bridgeline.iAPPS.Admin.Controller");

Bridgeline.Framework.Common = {
    Get: function (url, request, callback, options) {
        var settings = $.extend({
            dataType: "json",
            async: false,
            cache: false
        }, options);

        $.ajax({
            type: "GET",
            url: url,
            data: request,
            success: function (data) {
                callback.call(this, data);
                //CloseiAppsLoadingPanel();
            },
            error: function (a, status, error) {
                console.error(stringformat("Status : {0} \nError : {1} \nURL : {2}", status, error, url));
                //CloseiAppsLoadingPanel();
            },
            async: settings.async,
            cache: settings.cache
        });
    },
    Post: function (url, request, callback, options) {
        var settings = $.extend({
            async: false,
            cache: false
        }, options);

        $.ajax({
            type: "POST",
            url: url,
            data: request,
            success: function (data) {
                callback.call(this, data);
                //CloseiAppsLoadingPanel();
            },
            error: function (a, status, error) {
                console.error(stringformat("Status : {0} \nError : {1} \nURL : {2}", status, error, url));
                //CloseiAppsLoadingPanel();
            },
            async: settings.async,
            cache: settings.cache
        });
    }
};

Bridgeline.Framework.WebApi = {
    ExecuteGet: function (service, endpoint, request, options) {
        var url = Bridgeline.Framework.WebApi.EndPointUrl(service, endpoint);

        var result = null;
        Bridgeline.Framework.Common.Get(url, request, function (data) {
            result = data;
        }, options);

        return result;
    },
    ExecutePost: function (service, endpoint, request, options) {
        var url = Bridgeline.Framework.WebApi.EndPointUrl(service, endpoint);

        var result = null;
        Bridgeline.Framework.Common.Post(url, request, function (data) {
            result = data;
        }, options);

        return result;
    },
    Get: function (service, endpoint, request, callback, options) {
        var url = Bridgeline.Framework.WebApi.EndPointUrl(service, endpoint);

        Bridgeline.Framework.Common.Get(url, request, callback, options);
    },
    Post: function (service, endpoint, request, callback, options) {
        var url = Bridgeline.Framework.WebApi.EndPointUrl(service, endpoint);

        Bridgeline.Framework.Common.Post(url, request, callback, options);
    },
    GetAsync: function (service, endpoint, request, callback, options) {
        var url = Bridgeline.Framework.WebApi.EndPointUrl(service, endpoint);

        var settings = $.extend({}, options);
        settings.async = true;
        Bridgeline.Framework.Common.Get(url, request, callback, settings);
    },
    PostAsync: function (service, endpoint, request, callback, options) {
        var url = Bridgeline.Framework.WebApi.EndPointUrl(service, endpoint);

        var settings = $.extend({}, options);
        settings.async = true;
        Bridgeline.Framework.Common.Post(url, request, callback, settings);
    },
    EndPointUrl: function (service, endpoint) {
        var url = jPublicSiteUrl;
        if (isFolderBasedSite)
            url = jPublicSiteRootUrl;
        if (typeof jCurrentVDName != "undefined")
            url = stringformat("{0}/{1}", url, jCurrentVDName);

        service = service.toLowerCase().replace("service", "");
        url = stringformat("{0}/{1}/{2}/{3}", url, jWebApiRoutePath, service, endpoint);
        if (isFolderBasedSite)
            url = stringformat("{0}?micro_site_id={1}", url, jAppId);

        return url;
    }
};

Bridgeline.iAPPS.Admin.Controller = {
    ExecuteGet: function (controller, endpoint, request, options) {
        var url = Bridgeline.iAPPS.Admin.Controller.EndPointUrl(controller, endpoint);

        var result = null;
        Bridgeline.Framework.Common.Get(url, request, function (data) {
            result = data;
        });

        return result;
    },
    ExecutePost: function (controller, endpoint, request, options) {
        var url = Bridgeline.iAPPS.Admin.Controller.EndPointUrl(controller, endpoint);

        var result = null;
        Bridgeline.Framework.Common.Post(url, request, function (data) {
            result = data;
        });

        return result;
    },
    Get: function (service, endpoint, request, callback, options) {
        var url = Bridgeline.iAPPS.Admin.Controller.EndPointUrl(service, endpoint);

        Bridgeline.Framework.Common.Get(url, request, callback, options);
    },
    Post: function (service, endpoint, request, callback, options) {
        var url = Bridgeline.iAPPS.Admin.Controller.EndPointUrl(service, endpoint);

        Bridgeline.Framework.Common.Post(url, request, callback, options);
    },
    GetAsync: function (service, endpoint, request, callback, options) {
        var url = Bridgeline.iAPPS.Admin.Controller.EndPointUrl(service, endpoint);

        var settings = $.extend({}, options);
        settings.async = true;
        Bridgeline.Framework.Common.Get(url, request, callback, settings);
    },
    PostAsync: function (service, endpoint, request, callback, options) {
        var url = Bridgeline.iAPPS.Admin.Controller.EndPointUrl(service, endpoint);

        var settings = $.extend({}, options);
        settings.async = true;
        Bridgeline.Framework.Common.Post(url, request, callback, settings);
    },
    EndPointUrl: function (controller, endpoint) {
        var url = jPublicSiteUrl;
        if (isFolderBasedSite)
            url = jPublicSiteRootUrl;
        if (typeof jCurrentVDName != "undefined")
            url = stringformat("{0}/{1}", url, jCurrentVDName);

        controller = controller.toLowerCase().replace("controller", "");
        url = stringformat("{0}/{1}/{2}", url, controller, endpoint);
        if (isFolderBasedSite)
            url = stringformat("{0}?micro_site_id={1}", url, jAppId);

        return url;
    }
};

function iAppsCallbackController(controller, method, mode, paramKeysJson, paramValues) {
    var callbackJson = {};

    if (typeof paramKeysJson != "undefined" && paramKeysJson != "" && paramValues.length > 0) {
        var paramKeys = JSON.parse(paramKeysJson)[paramValues.length];
        if (paramKeys != "" && paramKeys != ",") {
            $.each(paramKeys.split(','), function (i, p) {
                callbackJson[p] = paramValues[i];
            });
        }
    }

    if (mode.toLowerCase() == "post")
        return Bridgeline.Framework.WebApi.ExecutePost(controller, method, callbackJson);
    else
        return Bridgeline.Framework.WebApi.ExecuteGet(controller, method, callbackJson);
}

function iAppsValidator_Validate(sender, args) {
    var validationJson = eval(sender.id + "_ValidateJson");

    var valid = true;
    var message = "";
    if (validationJson.ListControlId != "") {
        valid = false;
        $("#" + validationJson.ListControlId).find(":checkbox").each(function () {
            if ($(this).prop("checked"))
                valid = true;
        });
    }
    else {
        if (validationJson.Required && args.Value == "") {
            valid = false;
            if (validationJson.FieldName)
                message = stringformat("Enter value for '{0}'", validationJson.FieldName);
        }

        if (valid && validationJson.Type && args.Value != "") {
            valid = AdminCallback.ValidateByType(validationJson.Type, args.Value);          

            if (validationJson.FieldName != null)
            {   
                if( validationJson.FieldName.toLowerCase() == "email")
                    message = "Please select the correct delimiter";
                else
                    message = stringformat("Enter a valid value for '{0}'", validationJson.FieldName);
            }
        }
    }

    args.IsValid = valid;
    if (!valid) {
        if (message != '')
            sender.errormessage = message;
    }
}

function CreateBitlyUrl(fullUrl) {
    var loginKey = 'iappsteam';
    var apiKey = 'R_2bf72d87350b9f68c091c7a785858b70';
    var format = 'json';
    var bitlyUrl = fullUrl;
    fullUrl = encodeURIComponent(fullUrl);
    var callbackUrl = stringformat("https://api-ssl.bitly.com/v3/shorten?login={0}&apiKey={1}&longUrl={2}&format={3}",
        loginKey, apiKey, fullUrl, format);

    $.ajax({
        type: "GET",
        url: callbackUrl,
        dataType: "json",
        async: false,
        cache: false,
        success: function (response) {
            if (response.status_code == 200)
                bitlyUrl = response.data.url;
        }
    });

    return bitlyUrl;
}
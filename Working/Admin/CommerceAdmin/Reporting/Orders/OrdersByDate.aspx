﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingOrdersOrdersbyShipDate %>"
    Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true"
    CodeBehind="OrdersByDate.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.OrdersByDate" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer">
        <ComponentArt:Grid SkinID="Default" ID="grdOrdersByDate" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="grdOrdersByDateLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                    HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
                    HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
                    SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                    AllowSorting="true">
                    <Columns>
                        <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, OrderDate %>"
                            DataField="OrderDate" DataCellClientTemplateId="OrderDateHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="d" />
                        <ComponentArt:GridColumn Width="270" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="NoOfOrderItemssHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, NumOfOrderItems %>"
                            DataField="NumberOfOrderItems" Align="Right" DataCellClientTemplateId="NoOfOrderItemsHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="False" FormatString="N0" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, OrderTotal %>"
                            DataField="OrderTotal" Align="Right" DataCellClientTemplateId="OrderTotalHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="OrderDateHoverTemplate">
                    <span title="## DataItem.GetMember('OrderDate').get_text() ##">## DataItem.GetMember('OrderDate').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('OrderDate').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NoOfOrderItemssHoverTemplate">
                    <span title="## DataItem.GetMember('NumberOfOrders').get_text() ##">## DataItem.GetMember('NumberOfOrders').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('NumberOfOrders').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NoOfOrderItemsHoverTemplate">
                    <span title="## DataItem.GetMember('NumberOfOrderItems').get_text() ##">## DataItem.GetMember('NumberOfOrderItems').get_text()
                        == "" ? "0" : DataItem.GetMember('NumberOfOrderItems').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="OrderTotalHoverTemplate">
                    <span title="## DataItem.GetMember('OrderTotal').get_text() ##">## DataItem.GetMember('OrderTotal').get_text()
                        == "" ? "0" : DataItem.GetMember('OrderTotal').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                 <ComponentArt:ClientTemplate ID="grdOrdersByDateLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdOrdersByDate) ##
            </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingOrdersOrderStatistics %>" Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master"
    AutoEventWireup="true" CodeBehind="OrderStatistics.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.OrderStatistics" StylesheetTheme="General" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid-view">
    <tbody>
        <tr class="row">
            <td style="border-top: solid 1px #d2d2d2;"><label class="form-label"><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, TotalNumberOfOrders %>" /></label></td>
            <td style="border-top: solid 1px #d2d2d2;"><asp:Label ID="lblTotalNumberOfOrders" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="alternate-row">
            <td><label class="form-label"><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, TotalItemsSold %>" /></label></td>
            <td><asp:Label ID="lblTotalItemsSold" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="row">
            <td><label class="form-label"><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, TotalSales %>" /></label></td>
            <td><asp:Label ID="lblTotalSales" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="alternate-row">
            <td><label class="form-label"><asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, AverageOrdersPerDay %>" /></label></td>
            <td><asp:Label ID="lblAverageOrdersPerDay" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="row">
            <td><label class="form-label"><asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, AverageItemsPerOrder %>" /></label></td>
            <td><asp:Label ID="lblAverageItemsPerOrder" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="alternate-row">
            <td><label class="form-label"><asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, AverageSalesAmountPerOrder %>" /></label></td>
            <td><asp:Label ID="lblAverageSalesPerOrder" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="row">
            <td><label class="form-label"><asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, MostPopularItemSold %>" /></label></td>
            <td><asp:Label ID="lblMostPopularItemSold" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
    </tbody>
</table>
</asp:Content>

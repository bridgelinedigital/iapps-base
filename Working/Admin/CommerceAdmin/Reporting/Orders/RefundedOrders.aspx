﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RefundedOrders.aspx.cs" 
     MasterPageFile="~/Reporting/ReportMaster.Master" 
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Reporting.Orders.RefundedOrders" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="reports">
   
    </div>
        <div class="gridContainer">
        <ComponentArt:Grid SkinID="Default" ID="grdRefundedOrder" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="grdRefundedOrderLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                    HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
                    HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
                    SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                    AllowSorting="true">
                    <Columns>
                        <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, OrderNumber %>"
                            DataField="PurchaseOrderNumber" DataCellClientTemplateId="PurchaseOrderNumberHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True"  />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, OrderTotal %>"
                            DataField="OrderTotal" Align="Right" DataCellClientTemplateId="OrderTotalHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, Refund %>"
                            DataField="RefundTotal" Align="Right" DataCellClientTemplateId="RefundTotalHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn Width="270" HeadingText="<%$ Resources:GUIStrings, RefundDate %>"
                            DataField="TransactionDate" Align="Right" DataCellClientTemplateId="TransactionDateHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="d" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, IsFullRefund %>"
                            DataField="IsFullRefund" Align="Right" DataCellClientTemplateId="IsFullRefundHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="False" FormatString="N0" />
                        <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="PurchaseOrderNumberHoverTemplate">
                    <span title="## DataItem.GetMember('PurchaseOrderNumber').get_text() ##">## DataItem.GetMember('PurchaseOrderNumber').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('PurchaseOrderNumber').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="OrderTotalHoverTemplate">
                    <span title="## DataItem.GetMember('OrderTotal').get_text() ##">## DataItem.GetMember('OrderTotal').get_text()
                        == "" ? "0" : DataItem.GetMember('OrderTotal').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="RefundTotalHoverTemplate">
                    <span title="## DataItem.GetMember('RefundTotal').get_text() ##">## DataItem.GetMember('RefundTotal').get_text()
                        == "" ? "0" : DataItem.GetMember('RefundTotal').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TransactionDateHoverTemplate">
                    <span title="## DataItem.GetMember('TransactionDate').get_text() ##">## DataItem.GetMember('TransactionDate').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('TransactionDate').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                 <ComponentArt:ClientTemplate ID="IsFullRefundHoverTemplate">
                 <span title="## DataItem.GetMember('IsFullRefund').get_text() ##">## DataItem.GetMember('IsFullRefund').get_text()
                        == "" ? "False" : DataItem.GetMember('IsFullRefund').get_text() ##</span>
            </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdRefundedOrderLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdOrdersByDate) ##
            </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

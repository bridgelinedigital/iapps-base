﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingProductManagementProductMissingImages %>"
    Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true"
    CodeBehind="ProductMissingImages.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductMissingImages"
    StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer">
        <ComponentArt:Grid SkinID="Default" ID="grdSKUsMissingImages" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="grdSKUsMissingImagesLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="RowNumber" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                    ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="375" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                            DataField="ProductTitle" DataCellClientTemplateId="ProductTitleHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" TextWrap="true" />
                        <ComponentArt:GridColumn Width="370" HeadingText="<%$ Resources:GUIStrings, SKUName %>"
                            DataField="SKUTitle" TextWrap="true" DataCellClientTemplateId="SKUTitleHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="False" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, SKU %>"
                            DataField="SKU" DataCellClientTemplateId="SKUHoverTemplate" AllowReordering="false"
                            FixedWidth="true" AllowSorting="False" />
                        <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, IsActive %>"
                            DataField="IsActive" Align="Left" DataCellClientTemplateId="IsActiveHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="False" />
                        <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, IsOnline %>"
                            DataField="IsOnline" Align="Left" DataCellClientTemplateId="IsOnlineHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="False" />
                        <ComponentArt:GridColumn DataField="ProductId" Visible="false" />
                        <ComponentArt:GridColumn DataField="SKUId" Visible="false" />
                        <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="ProductTitleHoverTemplate">
                    <span title="## DataItem.GetMember('ProductTitle').get_text() ##">## DataItem.GetMember('ProductTitle').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('ProductTitle').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="SKUTitleHoverTemplate">
                    <span title="## DataItem.GetMember('SKUTitle').get_text() ##">## DataItem.GetMember('SKUTitle').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('SKUTitle').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="SKUHoverTemplate">
                    <span title="## DataItem.GetMember('SKU').get_text() ##">## DataItem.GetMember('SKU').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('SKU').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="IsOnlineHoverTemplate">
                    <span title="## DataItem.GetMember('IsOnline').get_text() == 'true' ? 'Online' : 'Offline' ##">
                        ## DataItem.GetMember('IsOnline').get_text() == 'true' ? 'Online' : 'Offline' ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="IsActiveHoverTemplate">
                    <span title="## DataItem.GetMember('IsActive').get_text() == 'true' ? 'Active' : 'Inactive' ##">
                        ## DataItem.GetMember('IsActive').get_text() == 'true' ? 'Active' : 'Inactive' ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdSKUsMissingImagesLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdSKUsMissingImages) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

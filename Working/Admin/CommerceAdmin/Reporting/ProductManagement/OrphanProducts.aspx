﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingProductManagementOrphanProducts %>"
    Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true"
    CodeBehind="OrphanProducts.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.OrphanProducts" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
    <script type="text/javascript">
        var activeText = '<%= GUIStrings.Active %>';
        var inActiveText = '<%= GUIStrings.InActive %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer">
        <ComponentArt:Grid SkinID="Default" ID="grdOrphanProducts" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="grdOrphanProductsLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="ProductId" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                    ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="800" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                            DataField="ProductTitle" DataCellClientTemplateId="ProductTitleHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="310" HeadingText="<%$ Resources:GUIStrings, IsActive %>"
                            DataField="IsActive" Align="Left" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                            DataCellClientTemplateId="IsActiveHoverTemplate" />
                        <ComponentArt:GridColumn DataField="ProductId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="ProductTitleHoverTemplate">
                    <span title="## DataItem.GetMember('ProductTitle').get_text() ##">## DataItem.GetMember('ProductTitle').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('ProductTitle').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="IsActiveHoverTemplate">
                    ## DataItem.GetMember('IsActive').get_text() == 'true' ? activeText : inActiveText
                    ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdOrphanProductsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdOrphanProducts) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

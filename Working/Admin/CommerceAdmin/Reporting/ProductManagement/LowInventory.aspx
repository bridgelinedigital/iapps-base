﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingProductManagementLowInventory %>" Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" 
    AutoEventWireup="true" CodeBehind="LowInventory.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.LowInventory" StylesheetTheme="General" %>
<%@ Register TagPrefix="pi" TagName="ProductInventory" Src="~/UserControls/Reporting/Products/ProductInventory.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
<div class="gridContainer">
    <pi:ProductInventory id="ctlProductInventory" runat="server" />
</div>
</asp:Content>

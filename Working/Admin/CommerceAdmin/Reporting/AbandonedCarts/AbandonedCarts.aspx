﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingAbandonedCarts %>"
    Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true"
    CodeBehind="AbandonedCarts.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AbandonedCarts" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer">
        <ComponentArt:Grid SkinID="Default" ID="grdAbandonedCart" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="grdAbandonedCartLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="RowNumber" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                    ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, AbandonedDate %>"
                            DataField="AbandonedDate" Align="Left" DataCellClientTemplateId="AbandonedDateHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="MMM dd, yyyy" />
                        <ComponentArt:GridColumn Width="400" HeadingText="<%$ Resources:GUIStrings, CustomerName %>"
                            DataField="FullName" DataCellClientTemplateId="FullNameHoverTemplate" AllowReordering="false"
                            FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="400" HeadingText="<%$ Resources:GUIStrings, EmailAddress %>"
                            DataField="Email" Align="Left" DataCellClientTemplateId="EmailHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, CartTotal %>"
                            DataField="CartTotal" Align="Right" DataCellClientTemplateId="CartTotalHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn DataField="CartId" Visible="false" />
                        <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="FullNameHoverTemplate">
                    <span title="## DataItem.GetMember('FullName').get_text() ##">## DataItem.GetMember('FullName').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('FullName').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CartTotalHoverTemplate">
                    <span title="## DataItem.GetMember('CartTotal').get_text() ##">## DataItem.GetMember('CartTotal').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('CartTotal').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="EmailHoverTemplate">
                    <span title="## DataItem.GetMember('Email').get_text() ##">## DataItem.GetMember('Email').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('Email').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="AbandonedDateHoverTemplate">
                    <span title="## DataItem.GetMember('AbandonedDate').get_text() ##">## DataItem.GetMember('AbandonedDate').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('AbandonedDate').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdAbandonedCartLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdAbandonedCart) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

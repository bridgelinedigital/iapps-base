﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingProductMediaDownloads %>"
    Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true"
    CodeBehind="MediaDownloads.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.MediaDownloads" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="grid-utility">
        <div class="columns">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, OrderNumber %>" /></label>
            <asp:TextBox type="text" ID="txtOrderNumber" CssClass="textbox" Width="175" runat="server" />
            <asp:RegularExpressionValidator runat="server" ID="revtxtOrderNumber" ControlToValidate="txtOrderNumber"
                ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInSearch %>"
                Text="<%$ Resources:GUIStrings, InvalidCharactersInSearch %>"></asp:RegularExpressionValidator>
        </div>
        <div class="columns">
            <label class="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, CustomerEmail %>" /></label>
            <asp:TextBox type="text" ID="txtEmail" CssClass="textbox" Width="175" runat="server" />
            <asp:RegularExpressionValidator runat="server" ID="revtxtEmail" ControlToValidate="txtEmail"
                ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInSearch %>"
                Text="<%$ Resources:GUIStrings, InvalidCharactersInSearch %>"></asp:RegularExpressionValidator>
        </div>
        <div class="columns">
            <label class="form-label">
                &nbsp;</label>
            <asp:Button ID="btnFilter" runat="server" Text="<%$ Resources:GUIStrings, Go %>"
                ToolTip="<%$ Resources:GUIStrings, FilterCustomers %>" CssClass="small-button" />
        </div>
        <div class="columns">
            <label class="form-label">
                &nbsp;</label>
            <asp:Button ID="btnClear" runat="server" Text="<%$ Resources:GUIStrings, Clear %>"
                ToolTip="<%$ Resources:GUIStrings, ClearFilter %>" CssClass="small-button"
                Visible="false" />
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <ComponentArt:Grid SkinID="Default" ID="grdProductMedia" Width="940" runat="server"
        RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
        AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
        SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="grdProductMediaLoadingPanelTemplate">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="RowNumber" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="60" HeadingText="<%$ Resources:GUIStrings, OrderNumber %>"
                        DataField="OrderNumber" DataCellClientTemplateId="OrderNumberHoverTemplate" IsSearchable="true"
                        AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                    <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, FileTitle %>"
                        DataField="FileTitle" DataCellClientTemplateId="FileTitleHoverTemplate" IsSearchable="true"
                        AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                    <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, CustomerName %>"
                        DataField="UserName" Align="Left" DataCellClientTemplateId="UserNameHoverTemplate"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                    <ComponentArt:GridColumn Width="180" HeadingText="<%$ Resources:GUIStrings, CustomerEmail %>"
                        DataField="Email" Align="Left" DataCellClientTemplateId="EmailHoverTemplate"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                    <ComponentArt:GridColumn Width="125" HeadingText="<%$ Resources:GUIStrings, StartDateTime %>"
                        DataField="StartDateTime" Align="Left" DataCellClientTemplateId="StartDateHoverTemplate"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                    <ComponentArt:GridColumn Width="125" HeadingText="<%$ Resources:GUIStrings, EndDateTime %>"
                        DataField="EndDateTime" Align="Left" DataCellClientTemplateId="EndDateHoverTemplate"
                        IsSearchable="true" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" AllowSorting="True" />
                    <ComponentArt:GridColumn Width="110" HeadingText="<%$ Resources:GUIStrings, BytesTransferred %>"
                        DataField="BytesTransferred" Align="Right" DataCellClientTemplateId="BytesTransferredHoverTemplate"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                    <ComponentArt:GridColumn Width="52" HeadingText="<%$ Resources:GUIStrings, IsDownloaded %>"
                        DataField="IsDownloaded" Align="Left" DataCellClientTemplateId="IsDownloadedHoverTemplate"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="OrderNumberHoverTemplate">
                <span title="## DataItem.GetMember('OrderNumber').get_text() ##">## DataItem.GetMember('OrderNumber').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('OrderNumber').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="FileTitleHoverTemplate">
                <span title="## DataItem.GetMember('FileTitle').get_text() ##">## DataItem.GetMember('FileTitle').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('FileTitle').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="UserNameHoverTemplate">
                <span title="## DataItem.GetMember('UserName').get_text() ##">## DataItem.GetMember('UserName').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('UserName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="EmailHoverTemplate">
                <span title="## DataItem.GetMember('Email').get_text() ##">## DataItem.GetMember('Email').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Email').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StartDateHoverTemplate">
                <span title="## DataItem.GetMember('StartDateTime').get_text() ##">## DataItem.GetMember('StartDateTime').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('StartDateTime').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="EndDateHoverTemplate">
                <span title="## DataItem.GetMember('EndDateTime').get_text() ##">## DataItem.GetMember('EndDateTime').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('EndDateTime').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BytesTransferredHoverTemplate">
                <span title="## DataItem.GetMember('BytesTransferred').get_text() ##">## DataItem.GetMember('BytesTransferred').get_text()
                    == "" ? "0" : DataItem.GetMember('BytesTransferred').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="IsDownloadedHoverTemplate">
                <span title="## DataItem.GetMember('IsDownloaded').get_text() ##">## DataItem.GetMember('IsDownloaded').get_text().toLowerCase()
                    == "true" ? "Success" : "Failed" ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdProductMediaLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdProductMedia) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</asp:Content>

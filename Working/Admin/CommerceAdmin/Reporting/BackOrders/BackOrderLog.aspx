﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceBackOrdersLog %>" Language="C#"
    MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="BackOrderLog.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.BackOrderLog" StylesheetTheme="General" %>

<asp:Content ID="cphHead" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        var versionedFilePathColumn = 4;

        var selectedItem;
        function FileGridContext(sender, eventArgs) {
            selectedItem = eventArgs.get_item();
            FileHistoryGrid.select(selectedItem);
            FileHistoryGridContextMenu.showContextMenuAtEvent(eventArgs.get_event());
        }
        function FileHistoryGridContextMenuClickHandler(sender, eventArgs) {
            if (eventArgs.get_item().get_id() == "ViewFile") {
                var versionedFilePath = selectedItem.getMember(versionedFilePathColumn).get_text();
                if (versionedFilePath != "[object Object]" && versionedFilePath != null && versionedFilePath != '') {
                    ViewFile(versionedFilePath);
                }
                eventArgs.set_cancel(true);
            }
        }
        //Display the file 
        function ViewFile(fullFilePath) {
            var tokenValue = GetToken(jCMSProductId, 'CMS');
            var queryString = '?Token=' + tokenValue
            var fullFilePathWithToken = fullFilePath + queryString
            var fileWindow;
            fileWindow = window.open(fullFilePathWithToken, null, 'height=600, width=500,status= no,resizable= no, scrollbars=yes,toolbar=no,location=no,menubar=no,modal=yes,top=50,left=200');
        }
    </script>
</asp:Content>
<asp:Content ID="cphBody" ContentPlaceHolderID="cphContent" runat="server">
    <h1>
        <%= GUIStrings.BackOrderLog %></h1>
    <ComponentArt:Grid ID="FileHistoryGrid" SkinID="ScrollingGrid" ScrollPopupClientTemplateId="ScrollPopupTemplate"
        Width="970" runat="server" PageSize="10" AllowPaging="true" AutoCallBackOnUpdate="true"
        LoadingPanelClientTemplateId="FileHistoryGridLoadingPanelTemplate">
        <ClientEvents>
            <ContextMenu EventHandler="FileGridContext" />
        </ClientEvents>
        <Levels>
            <ComponentArt:GridLevel DataKeyField="CurrentVersion" ShowTableHeading="false" ShowSelectorCells="false"
                HeadingRowCssClass="sHeadingRow" HeadingCellCssClass="sHeadingCell" HeadingCellHoverCssClass="sHeadingCellHover"
                HeadingCellActiveCssClass="sHeadingCellActive" HeadingTextCssClass="sHeadingCellText"
                DataCellCssClass="sDataCell" RowCssClass="sRow" SelectedRowCssClass="sSelectedRow"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortedDataCellCssClass="SortedDataCell"
                AlternatingRowCssClass="sAlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="100" AllowReordering="false" runat="server" HeadingText="<%$ Resources:GUIStrings, VersionNumber %>"
                        HeadingCellCssClass="sFirstHeadingCell" DataField="CurrentVersion" DataCellCssClass="sFirstDataCell"
                        SortedDataCellCssClass="sSortedDataCell" FixedWidth="true" />
                    <ComponentArt:GridColumn Width="150" AllowReordering="false" runat="server" DataField="FileSize"
                        SortedDataCellCssClass="sSortedDataCell" Visible="false" FixedWidth="true" />
                    <ComponentArt:GridColumn Width="215" AllowReordering="false" runat="server" HeadingText="<%$ Resources:GUIStrings, Date %>"
                        DefaultSortDirection="Descending" DataField="CreatedDate" SortedDataCellCssClass="sSortedDataCell"
                        FixedWidth="true" />
                    <ComponentArt:GridColumn Width="149" AllowReordering="false" runat="server" HeadingText="<%$ Resources:GUIStrings, UploadedBy %>"
                        DataField="CreatedBy" DataCellCssClass="sLastDataCell" SortedDataCellCssClass="sSortedDataCell"
                        FixedWidth="true" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="VersionedFileName"
                        Width="0" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ScrollPopupTemplate">
                <div class="sScrollPopup">
                    <h5>
                        ## DataItem.GetMember('CurrentVersion').Value ##&nbsp;
                        <asp:Localize ID="Localize1" runat="server" Text="Edited By:" /></h5>
                    <p>
                        ## DataItem.GetMember('CreatedBy').Value ##</p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="FileHistoryGridLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(FileHistoryGrid) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="FileHistoryGridContextMenu" AutoPostBackOnSelect="true" SkinID="ContextMenu"
        runat="server">
        <Items>
            <ComponentArt:MenuItem ID="ViewFile" runat="server" Text="View File">
            </ComponentArt:MenuItem>
        </Items>
        <ItemLooks>
            <ComponentArt:ItemLook LookId="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover"
                LabelPaddingLeft="15" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="3" />
            <ComponentArt:ItemLook LookId="BreakItem" ImageUrl="break.gif" CssClass="MenuBreak"
                ImageHeight="1" ImageWidth="100%" />
        </ItemLooks>
        <ClientEvents>
            <ItemSelect EventHandler="FileHistoryGridContextMenuClickHandler" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>

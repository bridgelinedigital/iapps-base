﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BackOrdersBySKU.aspx.cs"
    Title="<%$ Resources:GUIStrings, iAPPSCommerceBackOrders %>" MasterPageFile="~/Reporting/ReportMaster.Master"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.BackOrdersBySKU" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer">
        <asp:ObjectDataSource ID="odsBackOrders" runat="server" EnablePaging="true" EnableViewState="true"
            EnableCaching="false" TypeName="Bridgeline.iAPPS.Admin.Commerce.Web.BackOrdersBySKU" MaximumRowsParameterName="pageSize"
            StartRowIndexParameterName="rowIndex" SortParameterName="sortExpression" SelectCountMethod="GetBackOrdersCount"
            SelectMethod="GetBackOrders" />
        <asp:ListView ID="lvBackOrders" runat="server" ItemPlaceholderID="itemContainer"
            DataSourceID="odsBackOrders" DataKeyNames="RowNumber" OnItemDataBound="lvBackOrders_ItemDataBound">
            <LayoutTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="grid-view"
                    id="gridview">
                    <thead>
                        <tr>
                            <th>
                                <asp:Localize ID="locSKU" runat="server" Text="<%$ Resources:GUIStrings, SKU %>" />
                            </th>
                            <th>
                                <asp:Localize ID="locProductName" runat="server" Text="<%$ Resources:GUIStrings, ProductName %>" />
                            </th>
                            <th>
                                <asp:Localize ID="locQuantityOnHand" runat="server" Text="<%$ Resources:GUIStrings, Quantity %>" />
                            </th>
                            <th>
                                <asp:Localize ID="locBackOrderedQuantity" runat="server" Text="<%$ Resources:GUIStrings, BackOrderedQuantity %>" />
                            </th>
                            <th>
                                <asp:Localize ID="locExpectedAvailabilityDate" runat="server" Text="<%$ Resources:GUIStrings, ExpAvailabilityDate %>" />
                                <%--Expected availability date for BackOrderedQuantity--%>
                            </th>
                            <th>
                                <asp:Localize ID="locRestockingQuantity" runat="server" Text="<%$ Resources:GUIStrings, RestockingQuantity %>" />
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="itemContainer" runat="server" />
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7" align="right">
                                <asp:DataPager ID="dpBackOrders" runat="server" PageSize="10">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="false" ShowLastPageButton="false"
                                            NextPageText="" PreviousPageText="" RenderDisabledButtonsAsLabels="true" NextPageImageUrl="~/App_Themes/General/images/next-page.png"
                                            RenderNonBreakingSpacesBetweenControls="false" PreviousPageImageUrl="~/App_Themes/General/images/previous-page.png" />
                                        <asp:TemplatePagerField>
                                            <PagerTemplate>
                                                <div class="pager">
                                                    <%# string.Format(JSMessages.Page0Of1Items, (Container.StartRowIndex / Container.PageSize) + 1, 
                                                    Math.Ceiling((double)Container.TotalRowCount / Container.PageSize), Container.TotalRowCount)%>
                                                </div>
                                            </PagerTemplate>
                                        </asp:TemplatePagerField>
                                    </Fields>
                                </asp:DataPager>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "alternate-row" %>'>
                    <td>
                        <asp:Literal ID="ltSKU" runat="server" Text='<%# Eval("SKU") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="ltProductName" runat="server" Text='<%# Eval("ProductName") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="ltQuantityOnHand" runat="server" Text='<%# Eval("QuantityOnHand")  %>' />
                    </td>
                    <td>
                        <asp:Literal ID="ltBackOrderedQuantity" runat="server" Text='<%# Eval("BackOrderedQuantity").ToString() %>' />
                    </td>
                    <td>
                        <asp:Literal ID="ltExpectedAvailabilityDate" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltRestockingQuantity" runat="server" Text='<%# (double.Parse(Eval("RestockingQuantity").ToString())) <=0? Eval("RestockingQuantity").ToString() : string.Format(@"<a href=""../../StoreManager/Products/InventoryAndRestocking.aspx?SelectedProductSkuIds={1}&SelectedProductId={2}"" runat=""server"" id=""backOrdersLink"">{0} </a>", Eval("RestockingQuantity").ToString(), Eval("SKUId").ToString(), Eval("ProductId").ToString()) %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <div class="grid-emptyText">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, NoRecordsFound %>" /></div>
            </EmptyDataTemplate>
        </asp:ListView>
    </div>
</asp:Content>

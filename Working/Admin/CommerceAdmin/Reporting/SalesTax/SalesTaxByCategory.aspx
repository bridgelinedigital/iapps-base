﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingSalesTax %>" Language="C#"
    MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true" CodeBehind="SalesTaxByCategory.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SalesTaxByCategory" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer salesTaxReport">
        <ComponentArt:Grid SkinID="Default" ID="grdSalesTaxByCategory" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" PagerInfoClientTemplateId="grdSalesTaxByCategoryPaginationTemplate"
            LoadingPanelClientTemplateId="grdSalesTaxByCategoryLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="CountryId" DataMember="Country" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="true"
                    ShowTableHeading="false" ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="430" HeadingText="<%$ Resources:GUIStrings, CountryName %>"
                            DataField="CountryName" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, CountryCode %>"
                            DataField="CountryCode" Align="Left"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, TaxCategory %>"
                            Align="Left" AllowReordering="false" FixedWidth="true" AllowSorting="False" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, TotalTax %>"
                            DataField="TotalCountryTax" Align="Right" 
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn DataField="CountryId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
                <ComponentArt:GridLevel DataKeyField="StateId" DataMember="State" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="false"
                    ShowTableHeading="false" ShowSelectorCells="false" ShowHeadingCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="378" HeadingText="<%$ Resources:GUIStrings, StateName %>"
                            DataField="StateName" AllowReordering="false"
                            FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, StateCode %>"
                            DataField="StateCode" Align="Left" 
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, TaxCategory %>"
                            DataField="TaxCategory" Align="Left" 
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, TotalTax %>"
                            DataField="TotalTax" Align="Right"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn DataField="StateId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
                 <ComponentArt:GridLevel DataKeyField="Id" DataMember="City" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="false"
                    ShowTableHeading="false" ShowSelectorCells="false" ShowHeadingCells="false">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn Width="328" HeadingText="<%$ Resources:GUIStrings, StateName %>"
                            DataField="City" AllowReordering="false"
                            FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, StateCode %>"
                            DataField="ZipCode" Align="Left" 
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, TaxCategory %>"
                            DataField="TaxCategory" Align="Left" 
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, TotalTax %>"
                            DataField="TotalTax" Align="Right" 
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn DataField="StateId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="TaxCategoryHoverTemplate">
                    <span title="## DataItem.GetMember('TaxCategory').get_text() ##">## DataItem.GetMember('TaxCategory').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('TaxCategory').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CountryNameHoverTemplate">
                    <span title="## DataItem.GetMember('CountryName').get_text() ##">## DataItem.GetMember('CountryName').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('CountryName').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CountryCodeHoverTemplate">
                    <span title="## DataItem.GetMember('CountryCode').get_text() ##">## DataItem.GetMember('CountryCode').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('CountryCode').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TotalCountryTaxHoverTemplate">
                    <span title="## DataItem.GetMember('TotalCountryTax').get_text() ##">## DataItem.GetMember('TotalCountryTax').get_text()
                        == "" ? "0" : DataItem.GetMember('TotalCountryTax').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="StateNameHoverTemplate">
                    <span title="## DataItem.GetMember('StateName').get_text() ##">## DataItem.GetMember('StateName').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('StateName').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="StateCodeHoverTemplate">
                    <span title="## DataItem.GetMember('StateCode').get_text() ##">## DataItem.GetMember('StateCode').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('StateCode').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ZipCodeHoverTemplate">
                    <span title="## DataItem.GetMember('ZipCode').get_text() ##">## DataItem.GetMember('ZipCode').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('ZipCode').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CityHoverTemplate">
                    <span title="## DataItem.GetMember('City').get_text() ##">## DataItem.GetMember('City').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('City').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TotalTaxHoverTemplate">
                    <span title="## DataItem.GetMember('TotalTax').get_text() ##">## DataItem.GetMember('TotalTax').get_text()
                        == "" ? "0" : DataItem.GetMember('TotalTax').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdSalesTaxByCategoryPaginationTemplate">
                    ## stringformat(Page0Of1Items, currentPageIndex(grdSalesTaxByCategory), pageCount(grdSalesTaxByCategory),
                    grdSalesTaxByCategory.RecordCount) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdSalesTaxByCategoryLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdSalesTaxByCategory) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

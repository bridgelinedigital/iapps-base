﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingSalesTax %>" Language="C#"
    MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true" CodeBehind="SalesTax.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SalesTax" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer salesTaxReport">
        <ComponentArt:Grid SkinID="Default" ID="grdSalesTax" Width="100%" runat="server" RunningMode="Callback"
            EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" AutoAdjustPageSize="true"
            AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false" SearchOnKeyPress="false"
            ManualPaging="true" PagerInfoClientTemplateId="grdSalesTaxPaginationTemplate"
            LoadingPanelClientTemplateId="grdSalesTaxLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="CountryId" DataMember="Country" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="true"
                    ShowTableHeading="false" ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="430" HeadingText="<%$ Resources:GUIStrings, CountryName %>"
                            DataField="CountryName" DataCellClientTemplateId="CountryNameHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, CountryCode %>"
                            DataField="CountryCode" Align="Left" DataCellClientTemplateId="CountryCodeHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, TotalTax %>"
                            DataField="TotalCountryTax" Align="Right" DataCellClientTemplateId="TotalCountryTaxHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="NumberOfCountryOrders" Align="Right" DataCellClientTemplateId="NoOfCountryOrderItemsHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="N0" />
                        <ComponentArt:GridColumn DataField="CountryId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
                <ComponentArt:GridLevel DataKeyField="StateId" DataMember="State" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="false"
                    ShowTableHeading="false" ShowSelectorCells="false" ShowHeadingCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="378" HeadingText="<%$ Resources:GUIStrings, StateName %>"
                            DataField="StateName" DataCellClientTemplateId="StateNameHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, StateCode %>"
                            DataField="StateCode" Align="Left" DataCellClientTemplateId="StateCodeHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, TotalTax %>"
                            DataField="TotalTax" Align="Right" DataCellClientTemplateId="TotalTaxHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="NoOfOrderItemsHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="N0" />
                        <ComponentArt:GridColumn DataField="StateId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
                 <ComponentArt:GridLevel DataKeyField="Id" DataMember="City" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="false"
                    ShowTableHeading="false" ShowSelectorCells="false" ShowHeadingCells="false">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn Width="328" HeadingText="<%$ Resources:GUIStrings, StateName %>"
                            DataField="City" DataCellClientTemplateId="CityHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, StateCode %>"
                            DataField="ZipCode" Align="Left" DataCellClientTemplateId="ZipHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, TotalTax %>"
                            DataField="TotalTax" Align="Right" DataCellClientTemplateId="TotalTaxHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn Width="175" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="NoOfOrderItemsHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="N0" />
                        <ComponentArt:GridColumn DataField="StateId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="CountryNameHoverTemplate">
                    <span title="## DataItem.GetMember('CountryName').get_text() ##">## DataItem.GetMember('CountryName').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('CountryName').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CountryCodeHoverTemplate">
                    <span title="## DataItem.GetMember('CountryCode').get_text() ##">## DataItem.GetMember('CountryCode').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('CountryCode').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TotalCountryTaxHoverTemplate">
                    <span title="## DataItem.GetMember('TotalCountryTax').get_text() ##">## DataItem.GetMember('TotalCountryTax').get_text()
                        == "" ? "0" : DataItem.GetMember('TotalCountryTax').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NoOfCountryOrderItemsHoverTemplate">
                    <span title="## DataItem.GetMember('NumberOfCountryOrders').get_text() ##">## DataItem.GetMember('NumberOfCountryOrders').get_text()
                        == "" ? "0" : DataItem.GetMember('NumberOfCountryOrders').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="StateNameHoverTemplate">
                    <span title="## DataItem.GetMember('StateName').get_text() ##">## DataItem.GetMember('StateName').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('StateName').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="StateCodeHoverTemplate">
                    <span title="## DataItem.GetMember('StateCode').get_text() ##">## DataItem.GetMember('StateCode').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('StateCode').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CityHoverTemplate">
                    <span title="## DataItem.GetMember('City').get_text() ##">## DataItem.GetMember('City').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('City').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ZipHoverTemplate">
                    <span title="## DataItem.GetMember('ZipCode').get_text() ##">## DataItem.GetMember('ZipCode').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('ZipCode').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TotalTaxHoverTemplate">
                    <span title="## DataItem.GetMember('TotalTax').get_text() ##">## DataItem.GetMember('TotalTax').get_text()
                        == "" ? "0" : DataItem.GetMember('TotalTax').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NoOfOrderItemsHoverTemplate">
                    <span title="## DataItem.GetMember('NumberOfOrders').get_text() ##">## DataItem.GetMember('NumberOfOrders').get_text()
                        == "" ? "0" : DataItem.GetMember('NumberOfOrders').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdSalesTaxPaginationTemplate">
                    ## stringformat(Page0Of1Items, currentPageIndex(grdSalesTax), pageCount(grdSalesTax),
                    grdSalesTax.RecordCount) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdSalesTaxLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdSalesTax) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

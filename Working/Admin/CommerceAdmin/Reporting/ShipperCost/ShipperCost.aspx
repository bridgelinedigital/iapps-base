﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingShipperCost %>" Language="C#"
    MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true" CodeBehind="ShipperCost.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ShipperCost" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer">
        <ComponentArt:Grid SkinID="Default" ID="grdShipperCost" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" PagerInfoClientTemplateId="grdShipperCostPaginationTemplate"
            LoadingPanelClientTemplateId="grdShipperCostLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="ShipperName" DataMember="Shipper" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="true"
                    ShowTableHeading="false" ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="710" HeadingText="<%$ Resources:GUIStrings, ShipperName %>"
                            DataField="ShipperName" DataCellClientTemplateId="ShipperNameHoverTemplate" AllowReordering="false"
                            FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="NoOfOrderItemsHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="N0" />
                        <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, TotalShipping %>"
                            DataField="TotalShipping" Align="Right" DataCellClientTemplateId="TotalTaxHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn DataField="ShipperName" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
                <ComponentArt:GridLevel DataKeyField="ShippingOptionId" DataMember="ShippingOption"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                    HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
                    HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
                    SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                    AllowSorting="false" ShowTableHeading="false" ShowSelectorCells="false" ShowHeadingCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="668" HeadingText="<%$ Resources:GUIStrings, ShippingMethod %>"
                            DataField="ShippingOptionName" DataCellClientTemplateId="ShippingOptionNameHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="NoOfOrderItemsHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="False" FormatString="N0" />
                        <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, TotalShipping %>"
                            DataField="TotalShipping" Align="Right" DataCellClientTemplateId="TotalTaxHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn DataField="ShippingOptionId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="ShipperNameHoverTemplate">
                    <span title="## DataItem.GetMember('ShipperName').get_text() ##">## DataItem.GetMember('ShipperName').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('ShipperName').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ShippingOptionNameHoverTemplate">
                    <span title="## DataItem.GetMember('ShippingOptionName').get_text() ##">## DataItem.GetMember('ShippingOptionName').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('ShippingOptionName').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NoOfOrderItemsHoverTemplate">
                    <span title="## DataItem.GetMember('NumberOfOrders').get_text() ##">## DataItem.GetMember('NumberOfOrders').get_text()
                        == "" ? "0" : DataItem.GetMember('NumberOfOrders').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TotalTaxHoverTemplate">
                    <span title="## DataItem.GetMember('TotalShipping').get_text() ##">## DataItem.GetMember('TotalShipping').get_text()
                        == "" ? "0" : DataItem.GetMember('TotalShipping').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdShipperCostPaginationTemplate">
                    ## stringformat(Page0Of1Items, currentPageIndex(grdShipperCost), pageCount(grdShipperCost),
                    grdShipperCost.RecordCount) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdShipperCostLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdShipperCost) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

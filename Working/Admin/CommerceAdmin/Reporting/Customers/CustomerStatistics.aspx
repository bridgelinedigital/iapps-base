﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingCustomersCustomerStatistics %>" Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master"
    AutoEventWireup="true" CodeBehind="CustomerStatistics.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CustomerStatistics" StylesheetTheme="General" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
    <script type="text/javascript">
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var totalOrdersText = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, TotalOrdersFallingInTheSelectedDateRange %>' />";
        var numberOfCustomersText = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, TotalCustomersFallingInTheSelectedDateRange %>' />";
        var newCustomersText = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NoOfCustomersWhoCreatedAnOrderForTheFirstTimeInTheSelectedDateRange %>' />";
        var returnCustomersText = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NoOfCustomersHavingOrdersPriorToTheStartDateAndReturnedToTheSiteToPlaceAnotherOrderWithInTheSelectedDateRange %>' />";
        var repeatCustomersText = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NoOfCustomersHavingMoreThanOneOrderInTheSelectedDateRange %>' />";
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid-view">
    <tbody>
        <tr class="row">
            <td style="border-top: solid 1px #d2d2d2;"><label class="form-label"><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, NumberOfOrders %>" /><img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(totalOrdersText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" /></label></td>
            <td style="border-top: solid 1px #d2d2d2;"><asp:Label ID="lblTotalNumberOfOrders" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="alternate-row">
            <td><label class="form-label"><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, NumberOfCustomers %>" /><img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(numberOfCustomersText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" /></label></td>
            <td><asp:Label ID="lblTotalNumberOfCustomers" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="row">
            <td><label class="form-label"><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, NumberOfNewCustomers %>" /><img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(newCustomersText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" /></label></td>
            <td><asp:Label ID="lblTotalNumberOfNewCustomers" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="alternate-row">
            <td><label class="form-label"><asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, NumberOfReturnCustomers %>" /><img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(returnCustomersText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" /></label></td>
            <td><asp:Label ID="lblTotalNumberOfReturnCustomers" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
        <tr class="row">
            <td><label class="form-label"><asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, NumberOfRepeatCustomers %>" /><img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(repeatCustomersText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" /></label></td>
            <td><asp:Label ID="lblTotalNumberOfRepeatCustomers" runat="server" CssClass="formValue"></asp:Label></td>
        </tr>
    <//tbody>
</table>
</asp:Content>
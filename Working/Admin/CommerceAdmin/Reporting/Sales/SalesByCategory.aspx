﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingSalesSalesbyCategory %>" Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" 
    AutoEventWireup="true" CodeBehind="SalesByCategory.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SalesByCategory" StylesheetTheme="General" %>
<%@ Register TagPrefix="tpc" TagName="TopPurchasedCategories" Src="~/UserControls/Reporting/Products/TopSalesCategory.ascx" %>

<asp:Content ID="reportHeadContent" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportContentHolder" runat="server">
<div class="gridContainer">
    <tpc:TopPurchasedCategories ID="ctlTopPurchasedCategories" runat="server" />
</div>
</asp:Content>

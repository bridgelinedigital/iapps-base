﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingSalesSalesbyPromotionCode %>"
    Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true"
    CodeBehind="SalesByPromoCode.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SalesByPromoCode"
    StylesheetTheme="General" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer">
        <ComponentArt:Grid SkinID="Default" ID="grdSaleByPromoCode" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="grdSaleByPromoCodeLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="CouponId" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="true">
                    <Columns>
                        <ComponentArt:GridColumn Width="500" HeadingText="<%$ Resources:GUIStrings, Name %>"
                            DataField="Title" DataCellClientTemplateId="NameHoverTemplate" AllowReordering="false"
                            FixedWidth="true" AllowSorting="False" />
                        <ComponentArt:GridColumn Width="325" HeadingText="<%$ Resources:GUIStrings, Code %>"
                            DataField="Code" DataCellClientTemplateId="CodeHoverTemplate" AllowReordering="false"
                            FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="OrderCount" Align="Right" DataCellClientTemplateId="NoOfOrdersHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="N0" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, TotalDiscount %>"
                            DataField="TotalDiscount" Align="Right" DataCellClientTemplateId="TotalDiscountHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />
                        <ComponentArt:GridColumn DataField="CouponId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                    <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CodeHoverTemplate">
                    <span title="## DataItem.GetMember('Code').get_text() ##">## DataItem.GetMember('Code').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('Code').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NoOfOrdersHoverTemplate">
                    <span title="## DataItem.GetMember('OrderCount').get_text() ##">## DataItem.GetMember('OrderCount').get_text()
                        == "" ? "0" : DataItem.GetMember('OrderCount').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TotalDiscountHoverTemplate">
                    <span title="## DataItem.GetMember('TotalDiscount').get_text() ##">## DataItem.GetMember('TotalDiscount').get_text()
                        == "" ? "0" : DataItem.GetMember('TotalDiscount').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdSaleByPromoCodeLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdSaleByPromoCode) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

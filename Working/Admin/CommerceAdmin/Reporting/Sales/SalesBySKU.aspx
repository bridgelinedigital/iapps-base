﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingSalesSalesbySKU %>"
    Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true"
    CodeBehind="SalesBySKU.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SalesBySKU" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
    <script type="text/javascript">
        var tooltipIconSrc = '../../App_Themes/General/images/icon-tooltip.gif';
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer">
        <ComponentArt:Grid SkinID="Default" ID="grdTopPurchasedSKU" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" Sort="NetTotal desc" LoadingPanelClientTemplateId="grdTopPurchasedSKULoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="SKUId" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                    HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
                    HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
                    SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                    AllowSorting="true">
                    <Columns>
                        <ComponentArt:GridColumn Width="464" HeadingText="<%$ Resources:GUIStrings, SKUTitle %>"
                            DataField="SKUName" DataCellClientTemplateId="NameHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" TextWrap="true" />
                        <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, SKU %>"
                            DataField="SKU" DataCellClientTemplateId="SKUHoverTemplate" IsSearchable="true"
                            TextWrap="true" AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, Quantity %>"
                            DataField="Quantity" Align="Right" DataCellClientTemplateId="QtyViewsHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="True"
                            FormatString="N" />
                        <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, BundleQuantity %>"
                            DataField="BundleQuantity" Align="Right" DataCellClientTemplateId="BundleQtyViewsHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="True"
                            FormatString="N" />
                        <ComponentArt:GridColumn Width="80" HeadingText="<%$ Resources:GUIStrings, TotalQty %>"
                            DataField="PercentOfQuantity" Align="Right" DataCellClientTemplateId="QtyPercentHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                            FormatString="N" />
                        <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, NetTotal %>"
                            DataField="NetTotal" Align="Right" DataCellClientTemplateId="TotlaHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="True"
                            FormatString="c" />
                        <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, PercentTotal %>"
                            DataField="PercentOfSales" Align="Right" DataCellClientTemplateId="TotalPercentHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                            FormatString="N" />
                        <%-- This column is used in Orders by Product --%>
                        <ComponentArt:GridColumn Width="80" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="NoOfOrdersHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                            FormatString="N0" />
                        <ComponentArt:GridColumn DataField="SKUId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                    <span title="## DataItem.GetMember('SKUName').get_text() ##">## DataItem.GetMember('SKUName').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('SKUName').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="SKUHoverTemplate">
                    <span title="## DataItem.GetMember('SKU').get_text() ##">## DataItem.GetMember('SKU').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('SKU').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="QtyViewsHoverTemplate">
                    <span title="## DataItem.GetMember('Quantity').get_text() ##">## DataItem.GetMember('Quantity').get_text()
                        == "" ? "0" : DataItem.GetMember('Quantity').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="BundleQtyViewsHoverTemplate">
                    <span title="## DataItem.GetMember('BundleQuantity').get_text() ##">## DataItem.GetMember('BundleQuantity').get_text()
                        == "" ? "0" : DataItem.GetMember('BundleQuantity').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="QtyPercentHoverTemplate">
                    <span title="## DataItem.GetMember('PercentOfQuantity').get_text() ##">## DataItem.GetMember('PercentOfQuantity').get_text()
                        == "" ? "0" : DataItem.GetMember('PercentOfQuantity').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TotlaHoverTemplate">
                    <span title="## DataItem.GetMember('NetTotal').get_text() ##">## DataItem.GetMember('NetTotal').get_text()
                        == "" ? "0" : DataItem.GetMember('NetTotal').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TotalPercentHoverTemplate">
                    <span title="## DataItem.GetMember('PercentOfSales').get_text() ##">## DataItem.GetMember('PercentOfSales').get_text()
                        == "" ? "0" : DataItem.GetMember('PercentOfSales').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NoOfOrdersHoverTemplate">
                    <span title="## DataItem.GetMember('NumberOfOrders').get_text() ##">## DataItem.GetMember('NumberOfOrders').get_text()
                        == "" ? "0" : DataItem.GetMember('NumberOfOrders').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdTopPurchasedSKULoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdTopPurchasedSKU) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

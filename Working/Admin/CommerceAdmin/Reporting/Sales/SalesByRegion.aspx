﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingSalesSalesByRegion %>"
    Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" AutoEventWireup="true"
    CodeBehind="SalesByRegion.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SalesByRegion" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
    <div class="gridContainer sales-by-region">
        <ComponentArt:Grid SkinID="Default" ID="grdSalesByRegion" Width="100%" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
            SearchOnKeyPress="false" ManualPaging="true" PagerInfoClientTemplateId="grdSalesByRegionPaginationTemplate"
            LoadingPanelClientTemplateId="grdSalesByRegionLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="CountryCode" DataMember="Country" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="true">
                    <Columns>
                        <ComponentArt:GridColumn Width="350" HeadingText="<%$ Resources:GUIStrings, CountryName %>"
                            DataField="Country" DataCellClientTemplateId="CountryHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, CountryCode %>"
                            DataField="CountryCode" DataCellClientTemplateId="CountryCodeHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="140" HeadingText="<%$ Resources:GUIStrings, Total %>"
                            DataField="ItemTotal" Align="Right" DataCellClientTemplateId="OrderTotalHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                            FormatString="c" />

                         <ComponentArt:GridColumn Width="140" HeadingText="<%$ Resources:GUIStrings, DiscountTotal %>"
                            DataField="DiscountTotal" Align="Right" DataCellClientTemplateId="DiscountTotalHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                            FormatString="c" />

                         <ComponentArt:GridColumn Width="140" HeadingText="<%$ Resources:GUIStrings, NetTotal %>"
                            DataField="NetTotal" Align="Right" DataCellClientTemplateId="NetTotalHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                            FormatString="c" />
                        <ComponentArt:GridColumn Width="160" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="NoOfOrdersHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="True"
                            FormatString="N0" />
                        <ComponentArt:GridColumn DataField="CountryCode" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
                <ComponentArt:GridLevel DataKeyField="StateId" DataMember="State" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="false"
                    ShowSelectorCells="false" ShowHeadingCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="310" HeadingText="<%$ Resources:GUIStrings, State %>"
                            DataField="StateName" DataCellClientTemplateId="NameHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" />
                        <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, StateCode %>"
                            DataField="StateCode" DataCellClientTemplateId="StateCodeHoverTemplate" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" AllowSorting="False" />
                        <ComponentArt:GridColumn Width="140" HeadingText="<%$ Resources:GUIStrings, Total %>"
                            DataField="ItemTotal" Align="Right" DataCellClientTemplateId="OrderTotalHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />

                        
                         <ComponentArt:GridColumn Width="140" HeadingText="<%$ Resources:GUIStrings, DiscountTotal %>"
                            DataField="DiscountTotal" Align="Right" DataCellClientTemplateId="DiscountTotalHoverTemplate"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                            FormatString="c" />

                           <ComponentArt:GridColumn Width="140" HeadingText="<%$ Resources:GUIStrings, NetTotal %>"
                            DataField="NetTotal" Align="Right" DataCellClientTemplateId="NetTotalHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" FormatString="c" />

                        <ComponentArt:GridColumn Width="160" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                            DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="NoOfOrdersHoverTemplate"
                            AllowReordering="false" FixedWidth="true" AllowSorting="False" FormatString="N0" />
                        <ComponentArt:GridColumn DataField="StateId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="CountryHoverTemplate">
                    <span title="## DataItem.GetMember('Country').get_text() ##">## DataItem.GetMember('Country').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('Country').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                    <span title="## DataItem.GetMember('StateName').get_text() ##">## DataItem.GetMember('StateName').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('StateName').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CountryCodeHoverTemplate">
                    <span title="## DataItem.GetMember('CountryCode').get_text() ##">## DataItem.GetMember('CountryCode').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('CountryCode').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="StateCodeHoverTemplate">
                    <span title="## DataItem.GetMember('StateCode').get_text() ##">## DataItem.GetMember('StateCode').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('StateCode').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="OrderTotalHoverTemplate">
                    <span title="## DataItem.GetMember('ItemTotal').get_text() ##">## DataItem.GetMember('ItemTotal').get_text()
                        == "" ? "0" : DataItem.GetMember('ItemTotal').get_text() ##</span>
                </ComponentArt:ClientTemplate>

                 <ComponentArt:ClientTemplate ID="DiscountTotalHoverTemplate">
                    <span title="## DataItem.GetMember('DiscountTotal').get_text() ##">## DataItem.GetMember('DiscountTotal').get_text()
                        == "" ? "0" : DataItem.GetMember('DiscountTotal').get_text() ##</span>
                </ComponentArt:ClientTemplate>

                       <ComponentArt:ClientTemplate ID="NetTotalHoverTemplate">
                    <span title="## DataItem.GetMember('NetTotal').get_text() ##">## DataItem.GetMember('NetTotal').get_text()
                        == "" ? "0" : DataItem.GetMember('NetTotal').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NoOfOrdersHoverTemplate">
                    <span title="## DataItem.GetMember('NumberOfOrders').get_text() ##">## DataItem.GetMember('NumberOfOrders').get_text()
                        == "" ? "0" : DataItem.GetMember('NumberOfOrders').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdSalesByRegionPaginationTemplate">
                    ## stringformat(Page0Of1Items, currentPageIndex(grdSalesByRegion), pageCount(grdSalesByRegion),
                    grdSalesByRegion.RecordCount) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdSalesByRegionLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdSalesByRegion) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

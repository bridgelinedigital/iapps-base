﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceReportingSalesSalesbyProduct %>" Language="C#" MasterPageFile="~/Reporting/ReportMaster.Master" 
    AutoEventWireup="true" CodeBehind="SalesByProduct.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SalesByProduct" StylesheetTheme="General" %>
<%@ Register TagPrefix="tpp" TagName="TopPurchasedProducts" Src="~/UserControls/Reporting/Products/TopPurchasedProducts.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ReportsContentHead" runat="server">
    <script type="text/javascript">
        var tooltipIconSrc = '../../App_Themes/General/images/icon-tooltip.gif';
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        //method to split comma separated navigation categories
        function SplitNavCategory(strNavCategories) {
            var arrNavigtaion = strNavCategories.split(',');
            var strHTML = "";
            for (var i = 0; i < arrNavigtaion.length; i++) {
                strHTML += arrNavigtaion[i] + "<br />";
            }
            return strHTML;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ReportContentHolder" runat="server">
<div class="gridContainer">
    <tpp:TopPurchasedProducts id="ctlTopPurchasedProducts" runat="server" />
</div>
</asp:Content>

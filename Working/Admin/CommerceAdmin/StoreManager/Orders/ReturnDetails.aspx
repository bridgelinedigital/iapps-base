﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReturnDetails.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Orders.ReturnDetails" StylesheetTheme="General"
    MasterPageFile="~/MasterPage.master" EnableEventValidation="false" %>

<%@ Register Src="../../UserControls/StoreManager/Orders/OrderHeaderView.ascx" TagName="OrderHeaderView"
    TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/StoreManager/Orders/SelectReturnItems.ascx"
    TagName="SelectReturnItems" TagPrefix="uc3" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
             $('#ddlClaimReasons').hide();
            ddlClaimReasons
            if (!noRefundAllowed) {
                $('#pnlRefund').css('display', 'inline');
                $('#btnSubmitRefund').css('display', 'inline');
                $('#pnlExchange').css('display', 'none');
                $('#btnSubmitExchange').css('display', 'none');

            }
            else {
                $('#pnlRefund').css('display', 'none');
                $('#btnSubmitRefund').css('display', 'none');
                $('#pnlExchange').css('display', 'inline');
                $('#btnSubmitExchange').css('display', 'inline');
                $('#rbRefund').prop('checked', false);
                $('#rbExchange').prop('checked', true);
            }

            var refundChecked = $('#rbRefund').is(':checked');
            if (!refundChecked)
                refundChecked = $('#rbClaim').is(':checked');

            var exchangeChecked = $('#rbExchange').is(':checked');

            if (refundChecked)
                showHideRefundPanel($('#rbRefund')[0]);
            if(exchangeChecked)
                showHideRefundPanel($('#rbExchange')[0]);
        });

        function showHideRefundPanel(radio) {
            var radioId = radio.id;
            var isClaim = false;
            if (radio.checked && radioId == 'rbRefund') {
                $('#pnlRefund').css('display', 'inline');
                $('#btnSubmitRefund').css('display', 'inline');
                $('#pnlExchange').css('display', 'none');
                $('#btnSubmitExchange').css('display', 'none');

            }
            if (radio.checked && radioId == 'rbClaim') {
                $('#pnlRefund').css('display', 'inline');
                $('#btnSubmitRefund').css('display', 'inline');
                $('#pnlExchange').css('display', 'none');
                $('#btnSubmitExchange').css('display', 'none');
                isClaim = true; 
            }
            else if (radio.checked && radioId == 'rbExchange') {
                $('#pnlRefund').css('display', 'none');
                $('#btnSubmitRefund').css('display', 'none');

                $('#pnlExchange').css('display', 'inline');
                $('#btnSubmitExchange').css('display', 'inline');

            }
            else if (radio.checked && radioId == 'rbOther') {
                $('#pnlRefund').css('display', 'none');
                $('#btnSubmitRefund').css('display', 'none');

                $('#pnlExchange').css('display', 'none');
                $('#btnSubmitExchange').css('display', 'inline');


            }
            updateClaimsText(isClaim); 
        }

        function updateClaimsText(isClaim) {
            if (isClaim)
            {
                $('#btnSubmitRefund').prop('value', 'Submit Claim');
                $('#h1ReturnText').text('New Claim');
                $('#ddlClaimReasons').show();
                $('#ddlReasons').hide();
            }
            else
            {
                $('#btnSubmitRefund').prop('value', '<asp:localize runat="server" text="<%$ Resources:GUIStrings, SubmitAndSendPackingSlip %>"/>');
                $('#h1ReturnText').text('New Return');
                $('#ddlClaimReasons').hide();
                $('#ddlReasons').show();
            }
        }

        function toggleRefundButton(checkbox)
        {
            var checked = $(checkbox).find("input").is(':checked');
            if (checked) {
                $('#rbRefund').prop('disabled', false);               
                $('#rbRefund').prop('checked', true);
                showHideRefundPanel($('#rbRefund')[0]);
            } else {
                $('#rbRefund').prop('disabled', true);
                $('#rbExchange').prop('checked', true);
                showHideRefundPanel($('#rbExchange')[0]);
            }
            
        }
        function showHideOtherBox(dropdown) {
            if (dropdown.value == 'Other') {
                $('#dvOtherTextbox').show();
            }
            else
                $('#dvOtherTextbox').hide();
        }

        function toggleCreditCardSelection(checkBox, replaceId, txtBoxId) {

            toggleTextBoxBasedOnCheckbox(checkBox, 'chkPayment', 'txtRefundAmount');
            var rvAmtRange = checkBox.id.replace('chkPayment', 'rvAmtRange');
            var reqAmtRange = checkBox.id.replace('chkPayment', 'reqAmtRange');
            var rval = document.getElementById(rvAmtRange);
            var reqval = document.getElementById(reqAmtRange);
            if (checkBox.checked) {
                ValidatorEnable(rval, true);
                ValidatorEnable(reqval, true);
            }
            else {
                ValidatorEnable(rval, false);
                ValidatorEnable(reqval, false);
            }

        }
    </script>
</asp:Content>
<asp:Content ID="contentItems" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ObjectDataSource ID="odsPayments" runat="server" EnablePaging="false" EnableViewState="true"
        EnableCaching="false" TypeName="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Orders.ReturnDetails"
        MaximumRowsParameterName="pageSize" StartRowIndexParameterName="rowIndex" SelectMethod="GetPaymentsElligibleForRefund"
        OnSelecting="odsPayments_Selecting"></asp:ObjectDataSource>
    <div class="returns">
        <h1 id="h1ReturnText">New Return</h1>
        <asp:Button ID="btnCancel" runat="server" Text="Back"
            CssClass="button cancel-button" CausesValidation="false" Style="float: right; margin-left: 10px;"
            OnClick="btnCancel_Click" />
        <asp:Button ID="btnSubmitRefund" runat="server" Text="<%$ Resources:GUIStrings, SubmitAndSendPackingSlip %>"
            CssClass="primarybutton" ValidationGroup="vgRefund" OnClick="btnSubmit_Click"
            ClientIDMode="Static" Style="float: right; margin-left: 10px;" />
        <asp:Button ID="btnSubmitExchange" runat="server" Text="<%$ Resources:GUIStrings, SubmitAndSendPackingSlip %>"
            CssClass="primarybutton" OnClick="btnSubmit_Click" ClientIDMode="Static"
            Style="float: right; margin-left: 10px;" />
        <%--<asp:Button ID="btnSubmitOther" runat="server" Text="<%$ Resources:GUIStrings, SubmitAndSendPackingSlip %>"
            CssClass="primarybutton" OnClick="btnSubmit_Click" ClientIDMode="Static"
            Style="float: right; margin-left: 10px;" />--%>
        <asp:ValidationSummary ID="vsRefund" ValidationGroup="vgRefund" ShowSummary="false"
            ShowMessageBox="true" runat="server" />
        <%--<asp:ScriptManager ID="smItemsForReturn" runat="server" />--%>
        <div class="clear-fix"></div>
        <hr />
        <h3>Customer Details</h3>
        <div class="details-column">
            <uc1:OrderHeaderView ID="ucOrderHeaderView" runat="server" />
        </div>
        <div class="clear-fix"></div>
        <hr />
        <h3>
            <asp:Localize ID="Localize1" Text="<%$ Resources:GUIStrings, ReturnType %>" runat="server"></asp:Localize></h3>
        <div class="details-column">
            <asp:RadioButton ID="rbRefund" runat="server" ClientIDMode="Static" GroupName="ReturnType"
                Text="<%$ Resources:GUIStrings, Refund %>" onclick='javascript:showHideRefundPanel(this);'
                Checked="true" />
            
            <asp:RadioButton ID="rbExchange" runat="server" ClientIDMode="Static" GroupName="ReturnType"
                Text="<%$ Resources:GUIStrings, Exchange %>" onclick='javascript:showHideRefundPanel(this);' />

            <asp:RadioButton ID="rbClaim" runat="server" ClientIDMode="Static" GroupName="ReturnType"
                Text="<%$ Resources:GUIStrings, Claims %>" onclick='javascript:showHideRefundPanel(this);' />

            <asp:RadioButton ID="rbOther" runat="server" ClientIDMode="Static" GroupName="ReturnType"
                Text="<%$ Resources:GUIStrings, Others %>" onclick='javascript:showHideRefundPanel(this);' />
        </div>
        <div class="clear-fix"></div>
        <h3>&nbsp;</h3>

        <div class="details-column">
            <asp:CheckBox runat="server" ID="cbOverrideRefund" Visible ="false" onchange="toggleRefundButton(this);" Text="This RMA is not elligible for a refund unless all items are returned. Check here to by pass this restriction." />
        </div>
        <div class="clear-fix"></div>
        <hr />
        <asp:Panel ID="pnlRefund" runat="server" ClientIDMode="Static">
            <!-- Add Refund controls here -->
            <asp:UpdatePanel ID="upPanelTotal" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h3>Refund Amount</h3>
                    <div class="details-column">
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="lcItemsTotal" Text="<%$ Resources:GUIStrings, RMA_Items %>" runat="server"></asp:Localize></label>
                            <div class="form-value" style="float: right;">
                                <asp:Label ID="lblItemTotal" runat="server" ClientIDMode="Static"></asp:Label>
                                <asp:HiddenField runat="server" ID="hdnItemTotal" ClientIDMode="Static" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:CheckBox ID="chkTaxAmount" runat="server" Text="Tax:"
                                    AutoPostBack="true" />
                            </label>
                            <div class="form-value" style="float: right;">
                                <asp:Label ID="lblTaxTotal" runat="server" ClientIDMode="Static"></asp:Label>
                                <asp:HiddenField runat="server" ID="hdnTaxTotal" ClientIDMode="Static" />
                            </div>
                        </div>
                        <div class="form-row">                            
                            <label class="form-label">
                                <asp:CheckBox ID="chkShippingAmount" runat="server" Text="<%$ Resources:GUIStrings, RMA_Shipping %>"
                                    AutoPostBack="true" />
                            </label>
                            <div class="form-value" style="float: right;">
                                <asp:Label ID="lblShippingAmount" runat="server" ClientIDMode="Static"></asp:Label>
                                <asp:HiddenField runat="server" ID="hdnShipingAmount" ClientIDMode="Static" />
                                <asp:HiddenField runat="server" ID="hdnTotalRefundAmount" ClientIDMode="Static" />
                            </div>
                        </div>

                        <div class="form-row" style="margin-left: 20px;">
                            <label class="form-label">
                                <asp:CheckBox ID="chkShippingTaxAmount" runat="server" Text="Shipping Tax:"
                                    AutoPostBack="true" />
                            </label>
                            <div class="form-value" style="float: right;">
                                <asp:Label ID="lblShippingTax" runat="server" ClientIDMode="Static"></asp:Label>
                                <asp:HiddenField runat="server" ID="hdnShippingTax" ClientIDMode="Static" />
                            </div>
                        </div>

                        <asp:PlaceHolder runat="server" ID="handlingItems">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:CheckBox ID="chkHandlingAmount" runat="server" Text="Handling:"
                                        AutoPostBack="true" />
                                </label>
                                <div class="form-value" style="float: right;">
                                    <asp:Label ID="lblHandlingAmount" runat="server" ClientIDMode="Static"></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdnHandlingAmount" ClientIDMode="Static" />
                                </div>
                            </div>


                            <div class="form-row" style="margin-left: 20px;">
                                <label class="form-label">
                                    <asp:CheckBox ID="chkHandlingTaxAmount" runat="server" Text="Handling Tax:"
                                        AutoPostBack="true" />
                                </label>
                                <div class="form-value" style="float: right;">
                                    <asp:Label ID="lblHandlingTax" runat="server" ClientIDMode="Static"></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdnHandlingTaxAmount" ClientIDMode="Static" />
                                </div>
                            </div>
                        </asp:PlaceHolder>

                        <div class="form-row">
                            <label class="form-label">
                                <asp:CheckBox ID="chkAdditionalRefund" runat="server" Text="<%$ Resources:GUIStrings, RMA_AdditionalRefund %>"
                                    AutoPostBack="true" CausesValidation="true" ValidationGroup="vgRefund" />
                            </label>
                            <div class="form-value" style="float: right;">
                                <asp:TextBox ID="txtAdditionalRefund" runat="server" ClientIDMode="Static" AutoPostBack="true"
                                    CssClass="textbox" CausesValidation="true" ValidationGroup="vgRefund"></asp:TextBox>
                                <asp:CompareValidator ID="cvAdditionalRefundDataType" runat="server" ControlToValidate="txtAdditionalRefund"
                                    Type="Currency" CultureInvariantValues="true" Operator="DataTypeCheck" Text="*"
                                    Display="None" ValidationGroup="vgRefund" ErrorMessage="<%$ Resources:GUIStrings, RMA_AdditionalRefundDataTypeInCorrect %>"></asp:CompareValidator>
                                <asp:RangeValidator ID="rvAdditionalRefund" runat="server" ControlToValidate="txtAdditionalRefund"
                                    Type="Currency" CultureInvariantValues="true" MinimumValue="-1000000000" MaximumValue="1000000000"
                                    Text="*" Display="None" ValidationGroup="vgRefund" ErrorMessage="<%$ Resources:GUIStrings, RMA_TotalRefundAmountCannotExceedOrderTotal %>"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Label ID="lblTotal" runat="server" Text="<%$ Resources:GUIStrings, RMA_Total %>"></asp:Label></label>
                            <div class="form-value" style="float: right;">
                                <asp:Label ID="lblTotalRefundAmount" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                    <hr />
                    <h3>Refund Payment(s)</h3>
                    <div class="details-column">
                        <asp:ListView ID="lstPaymentsElligibleForRefund" runat="server" ItemPlaceholderID="itemContainer"
                            DataKeyNames="Id" ClientIDMode="AutoID" DataSourceID="odsPayments">
                            <LayoutTemplate>
                                <table style="width: 600px" border="0" cellpadding="10" cellspacing="0">
                                    <asp:PlaceHolder ID="itemContainer" runat="server" />
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td valign="top" style="padding: 5px;">
                                        <asp:CheckBox ID="chkPayment" runat="server" onclick="javascript:toggleCreditCardSelection(this,'chkPayment', 'txtRefundAmount');" />
                                    </td>
                                    <td width="390" valign="top" style="padding: 5px;">
                                        <asp:HiddenField ID="hfOrderPaymentId" runat="server" />
                                        <asp:Label ID="lblNameOnCard" runat="server" />
                                        &nbsp;<br />
                                        <asp:Label ID="lblCardType" runat="server" />
                                        &nbsp;
                                        <asp:Label ID="lblCardNumber" runat="server" />
                                        &nbsp;
                                        <asp:Label ID="lblCardExpiry" runat="server"></asp:Label>
                                        &nbsp;<br />
                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                        <asp:Label ID="lblCapturedAmount" runat="server" />
                                        <br />
                                        <asp:Label ID="lblRefundStatus" runat="server" Text="Existing Refund:"></asp:Label>
                                        <asp:Label ID="lblRefundedAmount" runat="server" />
                                        &nbsp;
                                        <asp:Label ID="lblAuthCode" runat="server" Visible="false" />
                                    </td>
                                    <td valign="top" style="padding: 5px;">
                                        <span style="float: left; display: inline-block; margin-top: 3px;">Amount:</span>
                                        <asp:TextBox ID="txtRefundAmount" runat="server" ValidationGroup="vgRefund"
                                            Width="60" CssClass="textbox" Style="float: left; margin-left: 5px;" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="reqAmtRange" ValidationGroup="vgRefund"
                                            ControlToValidate="txtRefundAmount" Display="None" Text="*" Enabled="false"
                                            ErrorMessage="<%$ Resources:GUIStrings, RefundAmountIsInvalid %>"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator runat="server" ID="cvAmtDataType" Type="Currency" Operator="DataTypeCheck"
                                            ValidationGroup="vgRefund" ControlToValidate="txtRefundAmount" Display="None"
                                            Text="*" ErrorMessage="<%$ Resources:GUIStrings, RefundAmountIsInvalid %>"></asp:CompareValidator>
                                        <asp:RangeValidator runat="server" ID="rvAmtRange" Type="Currency" ControlToValidate="txtRefundAmount"
                                            CultureInvariantValues="true" MinimumValue="0" MaximumValue="1000000000" Display="None"
                                            ValidationGroup="vgRefund" Text="*" ErrorMessage="<%$ Resources:GUIStrings, RefundAmountCanNotBeMoreThanTheCaptureAmount %>"></asp:RangeValidator>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                        <div class="form-row">
                            <label class="form-label">
                                &nbsp;</label>
                            <div class="form-value">
                                <asp:TextBox ID="txtTotalCreditCardPayment" runat="server" ValidationGroup="vgRefund"
                                    Style="display: none" CssClass="textbox"></asp:TextBox>
                                <asp:RangeValidator runat="server" ID="rvTotalCCPayment" Text="*" Display="None"
                                    Type="Currency" ErrorMessage="<%$ Resources:GUIStrings, RMA_RefundAmountOfCreditCardsDoesNotMatchWithTheTotal %>"
                                    ControlToValidate="txtTotalCreditCardPayment" CultureInvariantValues="true" MinimumValue="0.0"
                                    MaximumValue="1" ValidationGroup="vgRefund"></asp:RangeValidator>
                            </div>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="chkShippingAmount" EventName="CheckedChanged" />
                    <asp:AsyncPostBackTrigger ControlID="chkAdditionalRefund" EventName="CheckedChanged" />
                    <asp:AsyncPostBackTrigger ControlID="txtAdditionalRefund" EventName="TextChanged" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="clear-fix"></div>
            <hr />
        </asp:Panel>
        <asp:Panel ID="pnlExchange" runat="server" ClientIDMode="Static">
            <h3>Exchange Type</h3>
            <div class="details-column">
                <asp:RadioButton ID="rbExchangeNow" runat="server" ClientIDMode="Static" GroupName="ExchangeWhen"
                    Text="<%$ Resources:GUIStrings, ExchangeProcessNow %>" Checked="true" ValidationGroup='vgExchange' />
                <asp:RadioButton ID="rbExchangeLater" runat="server" ClientIDMode="Static" GroupName="ExchangeWhen"
                    ValidationGroup='vgExchange' Text="<%$ Resources:GUIStrings, ExchangeProcessWhenItemIsRecieved %>" />
            </div>
            <div class="clear-fix"></div>
            <hr />
        </asp:Panel>
        <h3>
            <asp:Localize ID="lcRMAReason" runat="server" Text="<%$ Resources:GUIStrings, Reason1 %>"></asp:Localize></h3>
        <div class="details-column">
            <div class="form-row">
                <div class="form-value">
                    <asp:DropDownList ID="ddlReasons"  ClientIDMode="Static" runat="server" onchange="javascript:showHideOtherBox(this);"
                        CssClass="selectbox">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlClaimReasons" ClientIDMode="Static" runat="server" onchange="javascript:showHideOtherBox(this);"
                        CssClass="selectbox">
                    </asp:DropDownList>

                </div>
            </div>
            <div class="form-row" id="dvOtherTextbox" style="display: none">
                <asp:TextBox ID="txtReasonOther" runat="server" ClientIDMode="Static" TextMode="MultiLine"
                    CssClass="textbox" Width="440" Rows="5"></asp:TextBox>
            </div>
        </div>
        <div class="clear-fix"></div>
        <hr />
        <h3>
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Comments %>"></asp:Localize></h3>
        <div class="details-column">
            <div class="form-row">
                <div class="form-value">
                    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="5" Width="440"
                        CssClass="textbox"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="clear-fix"></div>
        <hr />
        <h3>Items in this Return</h3>
        <div class="clear-fix"></div>
        <uc3:SelectReturnItems ID="ucSelectItemsForReturn" runat="server" IsListEditable="false" />
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PickTicket.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Orders.PickTicket" %>

<%@ Register TagPrefix="oi" TagName="OrderItems" Src="~/UserControls/StoreManager/Orders/OrderItems.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, iAPPSCommercePickTicket %>" />
    </title>
    <style type="text/css">
        body, table {
            padding: 10px;
            font-size: 12px;
            font-family: Arial;
        }
        .oddRow td, .evenRow td, .headRow th {
            border-bottom: dashed 1px #cccccc;
            padding-bottom: 3px;
        }
        .header {
            font-size: 18px;
            font-weight: bold;
            padding: 5px 0;
            border-bottom: solid 3px #000000;
            text-align: center;
        }
        
        .headerLight {
            font-size: 14px;
            padding: 5px 0;
            border-bottom: solid 3px #000000;
            text-align: Left;
        }
        .break {
            page-break-before: always;
           page-break-inside: avoid;
        }
        #dhtmltooltip {
            position: absolute;
            left: -300px;
            border: solid 1px #5c7aa3;
            padding: 10px;
            background-color: #fff;
            visibility: hidden;
            z-index: 100;
            font-size: 1.0em;
        }
        #dhtmlpointer {
            position: absolute;
            left: -300px;
            z-index: 101;
            visibility: hidden;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
                    <td colspan="2">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="~/images/commerce-logo.png" AlternateText="<%$ Resources:GUIStrings, iAPPSCommerce %>" />
                    </td>
                </tr>
                </table>
    <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_DataBound">
        <ItemTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="2">
                        <strong>
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, OrderPlaced %>" />: &nbsp; </strong> <asp:Literal
                                 ID="ltlDate" runat="server"></asp:Literal><br />
                        <strong>
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, OrderNumber %>" />: &nbsp;</strong><asp:Literal
                                ID="ltlOrderNumber" runat="server"></asp:Literal><br />
                        <strong>
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, OrderStatus1 %>" />: &nbsp;</strong><asp:Literal
                                ID="ltlOrderStatus" runat="server"></asp:Literal><br />
                        <strong>
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, CustomerName1 %>" />: &nbsp;</strong><asp:Literal
                                ID="ltlName" runat="server"></asp:Literal><br />
                        <strong>
                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, CustomerEmail %>" />: &nbsp;</strong><asp:Literal
                                ID="ltlCustomerEmail" runat="server"></asp:Literal><br />
                        <strong>
                            <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, AccountNumber %>" />: &nbsp;</strong><asp:Literal
                                ID="ltlAccountNumber" runat="server"></asp:Literal><br />
                        <asp:PlaceHolder ID="trPlacedBy" runat="server"><strong>
                            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, PlacedBy %>" />: &nbsp;</strong><asp:Literal
                                ID="ltlPlacedBy" runat="server"></asp:Literal><br />
                        </asp:PlaceHolder>
                        <asp:Repeater ID="rptShippings" runat="server" OnItemDataBound="rptShippings_DataBound">
                            <ItemTemplate>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td class="header">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="header">
                                            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, ShippingInformation %>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                            <strong>
                                                <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, AddressType %>" />: &nbsp;</strong><asp:Literal
                                                    ID="lblAddressType" runat="server"></asp:Literal><br />
                                            <strong>
                                                <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, Addressee %>" />: &nbsp;</strong><asp:Literal
                                                    ID="lblAddressee" runat="server"></asp:Literal><br />
                                            <strong>
                                                <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, ShippingMethod %>" />: &nbsp;</strong><asp:Literal
                                                    ID="lblShippingMethod" runat="server"></asp:Literal><br />
                                                    <strong>
                                                <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings, Shipper %>" />: &nbsp;</strong><asp:Literal
                                                    ID="ltlShipper" runat="server"></asp:Literal><br />
                                            <strong>
                                                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, Address %>" />: &nbsp;</strong>
                                            <asp:Literal ID="lblAddressTo" runat="server"></asp:Literal>
                                            <asp:Literal ID="lblAddress1" runat="server"></asp:Literal>
                                            <asp:Literal ID="lblAddress2" runat="server"></asp:Literal>
                                            <br />
                                            <asp:Literal ID="lblCity" runat="server"></asp:Literal>,&nbsp;
                                            <asp:Literal ID="lblState" runat="server"></asp:Literal>,&nbsp;
                                            <asp:Literal ID="lblZip" runat="server"></asp:Literal>, &nbsp;
                                            <asp:Literal ID="ltlCountry" runat="server"></asp:Literal>
                                            <br />    
                                            <strong>
                                                <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Phone %>" /></strong>
                                            <asp:Literal ID="lblPhone" runat="server"></asp:Literal><br />
                                            <table width="90%">
                                                <asp:Repeater ID="rptOrderPackages" runat="server" OnItemDataBound="rptOrderPackages_DataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="headerLight">
                                                                <asp:Localize ID="locShippingTitle" runat="server" Text="<%$ Resources:GUIStrings, ShippingPackage %>" />
                                                                 <asp:Localize ID="locRemainingItemsTitle" runat="server" Text="<%$ Resources:GUIStrings, RemainingItemsToShip %>" Visible="false"  />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%--<strong>Shipping Package</strong>--%><br />
                                                                <strong>
                                                                    <asp:Localize ID="locTrackingNumber" runat="server" Text="<%$ Resources:GUIStrings, TrackingNumber %>" /></strong><asp:Literal
                                                                        ID="ltTrackingNumber" runat="server"></asp:Literal><br />
                                                                <strong>
                                                            </td>
                                                        </tr>
                                                        <%--<asp:Repeater ID="rptPackageItems" runat="server" >
                                                    
                                                </asp:Repeater>    --%>
                                                        <tr>
                                                            <td>
                                                                <oi:OrderItems ID="ctlOrderItems" runat="server"></oi:OrderItems>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
            <div class="break"></div>
        </ItemTemplate>
    </asp:Repeater>
    </form>
</body>
</html>

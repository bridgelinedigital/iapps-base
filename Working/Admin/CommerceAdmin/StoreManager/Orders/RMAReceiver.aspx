﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RMAReceiver.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Orders.RMAReceiver"
    StylesheetTheme="General" MasterPageFile="~/MasterPage.master" EnableEventValidation="false" %>

<%@ Register Src="../../UserControls/StoreManager/Orders/OrderHeaderView.ascx" TagName="OrderHeaderView"
    TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/StoreManager/Orders/SelectReturnItems.ascx"
    TagName="SelectReturnItems" TagPrefix="uc3" %>
    <%@ Register Src="../../UserControls/StoreManager/Orders/ReceivedItems.ascx"
    TagName="ReceivedItems" TagPrefix="uc3" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="server">
    <div class="rma rma-details">
        <h1>
            <asp:Literal ID="ltlRMADetails" runat="server"></asp:Literal>
          </h1>
        <asp:HyperLink  Id="lnkCancel" runat="server" Text="Manage Returns"
            CssClass="button" NavigateUrl="~/returns-landing" Style="float: right;
            margin-left: 10px;" />
        <asp:Button ID="btnUpdate" runat="server" Text="<%$ Resources:GUIStrings, RMAComplete %>"
            CssClass="primarybutton" OnClick="btnUpdate_Click" ValidationGroup="vgReceiver"
            Style="float: right;" />
        <div class="clear-fix">
        </div>
        <hr />
        <div class="first-column">
           <h3><%= GUIStrings.RMA_CustomerDetails %></h3>
        </div>
        <div class="second-column">
            <uc1:OrderHeaderView ID="ucOrderHeaderView" runat="server" ShowOrderDate="false"
                ShowAccountNumber="true" />
        </div>
        <div class="clear-fix"></div>
        <hr />
        <div class="first-column">

         <h3>
             <asp:Literal ID="ltlReturnDetails" runat="server"></asp:Literal>
             </h3>
           <div class="form-row">
            <asp:Button ID="btnResendPackingSlip" runat="server" Text="<%$ Resources:GUIStrings, RMAGenerateReceipt%>"
            CssClass="primarybutton" OnClick="btnResendPackingSlip_Click" 
            Style="float: left;" />
            </div>
            <br />
            <br />
            <div class="form-row">
                 <asp:Button ID="btnCancelRMA" runat="server" Text="<%$ Resources:GUIStrings, RMACancelThisRMA%>"
            CssClass="primarybutton" OnClick="btnCancelRMA_Click" 
            Style="float: left;" />
            </div>
        </div>
        <div class="second-column">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="lcRMAType" runat="server" Text="<%$ Resources:GUIStrings, RMAResolutionType %>"></asp:Localize></label>
                <div class="form-value">
                    <asp:HyperLink ID="hplResolution" runat="server"></asp:HyperLink>
                </div>
            </div>
            <div class="form-row" runat="server" id="divRefundDetails">
                <label class="form-label">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, RMA_RefundAmount %>"></asp:Localize></label>
                <div class="form-value">
                    <asp:Literal ID="litRefundAmount" runat="server"></asp:Literal>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, RMA_RefundStatus %>"></asp:Localize></label>
                    <div class="form-value">
                        <asp:Literal ID="litRefundStatus" runat="server"></asp:Literal>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, RMA_RefundCreditCards %>"></asp:Localize></label>
                        <div class="form-value">
                            <asp:ListView ID="lstRefundCreditCards" runat="server" ItemPlaceholderID="itemContainer"
                                DataKeyNames="Id" ClientIDMode="AutoID" >
                                <LayoutTemplate>
                                    <table style="border: solix 1px #999; width: 600px;">
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <asp:PlaceHolder ID="itemContainer" runat="server" />
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altRow" %>'>
                                        <td>
                                            <asp:HiddenField ID="hfRefundPaymentId" runat="server" />
                                            <asp:Label ID="lblNameOnCard" runat="server" />
                                            &nbsp;
                                            <asp:Label ID="lblCardType" runat="server" />
                                            &nbsp;
                                            <asp:Label ID="lblCardNumber" runat="server" />
                                            &nbsp;&nbsp;
                                            <asp:Label ID="lblCardExpiry" runat="server"></asp:Label>
                                            &nbsp;&nbsp;
                                            <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                            <asp:Label ID="lblRefundAmount" runat="server" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="lcRMAStatus" runat="server" Text="<%$ Resources:GUIStrings, Status %>"></asp:Localize></label>
                <div class="form-value">
                    <asp:Literal ID="litStatus" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="lcRMACode" runat="server" Text="<%$ Resources:GUIStrings, RMACode %>"></asp:Localize></label>
                <div class="form-value">
                    <asp:Literal ID="litRMACode" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="lcRMAReason" runat="server" Text="<%$ Resources:GUIStrings, Reason1 %>"></asp:Localize></label>
                <div class="form-value">
                    <asp:Literal ID="litReason" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, RMA_CreatedOnWithColon %>"></asp:Localize></label>
                <div class="form-value">
                    <asp:Literal ID="litCreatedOn" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, RMA_CreatedByWithColon %>"></asp:Localize></label>
                <div class="form-value">
                    <asp:Literal ID="litCreatedBy" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="lcComments" runat="server" Text="<%$ Resources:GUIStrings, Comments %>"></asp:Localize></label>
                <div class="form-value">
                    <asp:Literal ID="litComments" runat="server"></asp:Literal>
                </div>
            </div>


        </div>

        <hr />
        <div class="clear-fix">
        </div>
        <uc3:SelectReturnItems ID="ucSelectItemsForReturn" runat="server" IsListEditable="false"
            ValidationGroup="vgReceiver" />

    <hr />
        <div class="clear-fix">
        </div>
        <uc3:ReceivedItems ID="ucReceivedItems" runat="server"  />
    </div>
</asp:Content>

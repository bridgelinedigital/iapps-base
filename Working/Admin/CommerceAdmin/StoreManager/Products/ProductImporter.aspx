﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductImporter.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductImporter" 
    Title="Product Import"
    ValidateRequest="false" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true"
    EnableEventValidation="false" 
    StylesheetTheme="General" EnableViewStateMac="False" ViewStateEncryptionMode="Never"
    %>
<%@ Import Namespace="Bridgeline.FW.SiteBlock" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">

    <!-- START: Progress Meter -->
    <style type="text/css">

        p 
        {
            margin: 8px 0 0 0;
        }

        .buttons a 
        {
            padding-right: 15px;
        }

        
        .error 
        {
            color: #990000;
            font-weight: bold;
        }

        #log
        {
            margin-top: 18px;
        }

        #log p 
        {
            overflow: hidden;
            display: block;
        }

            #log p span 
            {
                padding-right: 15px;
                float: left;
            }
             
            #log p.error span.log
            {
                font-weight: normal;
            }

  
        div.complete 
        {
            font-weight: bold;
            margin: 15px 0;
            font-size: 1.5em;
        }

        .progress 
        {
            display: none;
        }

            .progress ul
            {
                line-height: 1.5em;
                list-style: outside none none;
                padding: 0;
            }
    
            .progress ul li 
            {
                display: block;
                margin-bottom: 8px;
                overflow: hidden;
            }

        #progressMsg 
        {
            font-weight: bold;
        }

	    .meter { 
		    height: 20px;  /* Can be anything */
		    position: relative;
		    margin: 10px 0 10px 0; 
		    background: #777;
		    -moz-border-radius: 25px;
		    -webkit-border-radius: 25px;
		    border-radius: 25px;
		    padding: 10px;
		    -webkit-box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);
		    -moz-box-shadow   : inset 0 -1px 1px rgba(255,255,255,0.3);
		    box-shadow        : inset 0 -1px 1px rgba(255,255,255,0.3);
	    }
	    .meter > span {
		    display: block;
		    height: 100%;
			    -webkit-border-top-right-radius: 8px;
		    -webkit-border-bottom-right-radius: 8px;
			        -moz-border-radius-topright: 8px;
			    -moz-border-radius-bottomright: 8px;
			            border-top-right-radius: 8px;
			        border-bottom-right-radius: 8px;
			    -webkit-border-top-left-radius: 20px;
			    -webkit-border-bottom-left-radius: 20px;
			        -moz-border-radius-topleft: 20px;
			        -moz-border-radius-bottomleft: 20px;
			            border-top-left-radius: 20px;
			            border-bottom-left-radius: 20px;
		    background-color: rgb(43,194,83);
		    background-image: -webkit-gradient(
			    linear,
			    left bottom,
			    left top,
			    color-stop(0, rgb(43,194,83)),
			    color-stop(1, rgb(84,240,84))
			    );
		    background-image: -moz-linear-gradient(
			    center bottom,
			    rgb(43,194,83) 37%,
			    rgb(84,240,84) 69%
			    );
		    -webkit-box-shadow: 
			    inset 0 2px 9px  rgba(255,255,255,0.3),
			    inset 0 -2px 6px rgba(0,0,0,0.4);
		    -moz-box-shadow: 
			    inset 0 2px 9px  rgba(255,255,255,0.3),
			    inset 0 -2px 6px rgba(0,0,0,0.4);
		    box-shadow: 
			    inset 0 2px 9px  rgba(255,255,255,0.3),
			    inset 0 -2px 6px rgba(0,0,0,0.4);
		    position: relative;
		    overflow: hidden;
	    }
	    .meter > span:after, .animate > span > span {
		    content: "";
		    position: absolute;
		    top: 0; left: 0; bottom: 0; right: 0;
		    background-image: 
			    -webkit-gradient(linear, 0 0, 100% 100%, 
			        color-stop(.25, rgba(255, 255, 255, .2)), 
			        color-stop(.25, transparent), color-stop(.5, transparent), 
			        color-stop(.5, rgba(255, 255, 255, .2)), 
			        color-stop(.75, rgba(255, 255, 255, .2)), 
			        color-stop(.75, transparent), to(transparent)
			    );
		    background-image: 
			    -moz-linear-gradient(
				    -45deg, 
			        rgba(255, 255, 255, .2) 25%, 
			        transparent 25%, 
			        transparent 50%, 
			        rgba(255, 255, 255, .2) 50%, 
			        rgba(255, 255, 255, .2) 75%, 
			        transparent 75%, 
			        transparent
			    );
		    z-index: 1;
		    -webkit-background-size: 50px 50px;
		    -moz-background-size: 50px 50px;
		    background-size: 50px 50px;
		    -webkit-animation: move 2s linear infinite;
		    -moz-animation: move 2s linear infinite;
			    -webkit-border-top-right-radius: 8px;
		    -webkit-border-bottom-right-radius: 8px;
			        -moz-border-radius-topright: 8px;
			    -moz-border-radius-bottomright: 8px;
			            border-top-right-radius: 8px;
			        border-bottom-right-radius: 8px;
			    -webkit-border-top-left-radius: 20px;
			    -webkit-border-bottom-left-radius: 20px;
			        -moz-border-radius-topleft: 20px;
			        -moz-border-radius-bottomleft: 20px;
			            border-top-left-radius: 20px;
			            border-bottom-left-radius: 20px;
		    overflow: hidden;
	    }
		
	    .animate > span:after {
		    display: none;
	    }
		
	    @-webkit-keyframes move {
		    0% {
		        background-position: 0 0;
		    }
		    100% {
		        background-position: 50px 50px;
		    }
	    }
		
	    @-moz-keyframes move {
		    0% {
		        background-position: 0 0;
		    }
		    100% {
		        background-position: 50px 50px;
		    }
	    }
		
		
	    .orange > span {
		    background-color: #f1a165;
		    background-image: -moz-linear-gradient(top, #f1a165, #f36d0a);
		    background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f1a165),color-stop(1, #f36d0a));
		    background-image: -webkit-linear-gradient(#f1a165, #f36d0a); 
	    }
		
	    .red > span {
		    background-color: #f0a3a3;
		    background-image: -moz-linear-gradient(top, #f0a3a3, #f42323);
		    background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f0a3a3),color-stop(1, #f42323));
		    background-image: -webkit-linear-gradient(#f0a3a3, #f42323);
	    }
		
	    .nostripes > span > span, .nostripes > span:after {
		    -webkit-animation: none;
		    -moz-animation: none;
		    background-image: none;
	    }
        .import-tab-container {
            border: solid 1px #d2d2d2;
            padding: 10px 0;
            background: #fff;
        }
        .import-tab-container .import-tab-content {
            border-top: solid 1px #d2d2d2;
            padding: 20px 0 0 0;
        }
        .import-tab-footer {
            padding-left: 20px;
            padding-bottom: 10px;
            margin-top: 30px;
        }
        .import-tab-row {
            padding-left: 20px;
            padding-bottom: 10px;
            clear: both;
        }
        .import-tab-form-label {
            display: inline-block;
            text-align: left;
            padding: 3px 10px 0 0;
            min-width: 150px;
        }
        .import-tab-form-value {
            display: inline-block;
            text-align: left;
            clear: right;
        }
    </style>
    <!-- END: Progress Meter -->
    

</asp:Content>
<asp:Content ID="AllProductsContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="import-tab-container">
    <ComponentArt:TabStrip ID="tabOptions" runat="server" SkinID="Default" MultiPageId="mltpOptions">
        <Tabs>
            <ComponentArt:TabStripTab ID="tabUploadExcelFile" runat="server" Text="Excel Files" ToolTip="Upload Excel file to server and download existing Excel files from server"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab ID="tabLoadExcelData" runat="server" Text="Load Excel Data" ToolTip="Load data from Excel file into loader tables"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab ID="tabImportToIapps" runat="server" Text="Import To iAPPS" ToolTip="Import loader table data into iAPPS tables"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab ID="tabExportProducts" runat="server" Text="Export to Excel File" ToolTip="Export current products from iAPPS tables to an Excel file"></ComponentArt:TabStripTab>
            <ComponentArt:TabStripTab ID="tabDeleteProducts" runat="server" Text="Delete Current Products" ToolTip="Delete current products in iAPPS tables"></ComponentArt:TabStripTab>
        </Tabs>
    </ComponentArt:TabStrip>
    <ComponentArt:MultiPage ID="mltpOptions" runat="server" CssClass="MultiPage">
        <ComponentArt:PageView ID="pvUpdateExcelFile" runat="server">
            <div class="import-tab-content">
                <asp:Repeater ID="rptrExistingFiles" runat="server" OnItemDataBound="rptrExistingFiles_ItemDataBound">
                    <HeaderTemplate>
                        <div class="import-tab-row"><h3>Current Files</h3></div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="import-tab-row">
                            <asp:Button ID="btnDeleteFile" runat="server" CssClass="button" Text="Delete" OnClientClick="return confirm('Delete file?');" OnCommand="ControlCommand" CommandName="DeleteFile" CausesValidation="false" />
                            <asp:Button ID="btnDownloadFile" runat="server" CssClass="button" Text="Download" OnCommand="ControlCommand" CommandName="DownloadFile" CausesValidation="false" />
                            <span style="padding-left: 20px;"><asp:Literal ID="litExistingFileName" runat="server"></asp:Literal></span>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <div class="import-tab-row"><em><asp:Literal ID="litNoFiles1" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="No files found" /></em></div>
                    </FooterTemplate>
                </asp:Repeater>
                <p></p>
                <div class="import-tab-row"><b>File location: </b><asp:Literal ID="litFileLocation" runat="server"></asp:Literal></div>
                <div class="import-tab-footer">
                    <asp:FileUpload ID="fuExcelFile" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server" CssClass="fileUpload" onchange="uploadFile(this);" /><asp:Button ID="btnUploadFile" runat="server" style="display: none;" OnClick="btnUploadFile_Click" />
                </div>
                <asp:Panel ID="pnlFileError" runat="server" CssClass="import-tab-footer error" Visible="false"><asp:Literal ID="litFileError" runat="server"></asp:Literal></asp:Panel>
            </div>
        </ComponentArt:PageView>
        <ComponentArt:PageView ID="pvLoadExcelData" runat="server">
            <div class="import-tab-content">
                <div class="import-tab-row"><h3>Current Site: <%= SiteManager.GetCurrentSite().Title %></h3></div>
                <asp:Repeater ID="rptrProcessFiles" runat="server" OnItemDataBound="rptrProcessFiles_ItemDataBound">
                    <HeaderTemplate>
                        <div class="import-tab-row"><h3>Select Files to Process</h3></div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="import-tab-row"><input type="checkbox" id="chkProcessFile" runat="server" /> <span style="padding-left: 20px;"><asp:Literal ID="litProcessFileName" runat="server"></asp:Literal></span></div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <div class="import-tab-row"><em><asp:Literal ID="litNoFiles2" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="No files found" /></em></div>
                    </FooterTemplate>
                </asp:Repeater>
                <p></p>
                <div class="import-tab-row"><h3>Select Actions to Perform</h3></div>
                <%--<div class="import-tab-row"><input type="checkbox" name="load" value="refreshTables" id="refreshTables" /><label for="refreshTables"> Refresh Tables (clears existing data)</label></div>--%>
                <div class="import-tab-row"><input type="checkbox" name="load" value="loadAttributes" id="loadAttributes" /><label for="loadAttributes"> Load Attributes</label></div>
                <div class="import-tab-row"><input type="checkbox" name="load" value="loadProductTypes" id="loadProductTypes" /><label for="loadProductTypes"> Load Product Types</label></div>
                <div class="import-tab-row"><input type="checkbox" name="load" value="loadProducts" id="loadProducts" /><label for="loadProducts"> Load Products</label></div>
                <div class="import-tab-row"><input type="checkbox" name="load" value="loadSkus" id="loadSkus" /><label for="loadSkus"> Load SKUs</label></div>
                <div class="import-tab-row"><input type="checkbox" name="load" value="loadProductAttributes" id="loadProductAttributes" /><label for="loadProductAttributes"> Load Product Attributes</label></div>
                <div class="import-tab-row"><input type="checkbox" name="load" value="loadRelatedProducts" id="loadRelatedProducts" /><label for="loadRelatedProducts"> Load Related Products</label></div>
                <div class="import-tab-row"><input type="checkbox" name="load" value="loadImages" id="loadImages" /><label for="loadImages"> Load Images</label></div>
                <div class="import-tab-row"><input type="checkbox" name="load" value="loadPricesets" id="loadPricesets" /><label for="loadPricesets"> Load Pricesets</label></div>
                <div class="import-tab-row"><input type="checkbox" name="load" value="loadPricesetSkus" id="loadPricesetSkus" /><label for="loadPricesetSkus"> Load Priceset SKUs</label></div>
                <div class="import-tab-footer"><asp:HyperLink ID="btnLoad" runat="server" CssClass="button"  NavigateUrl="javascript:void(0);">Load Excel Data</asp:HyperLink></div>
            </div>
        </ComponentArt:PageView>
        <ComponentArt:PageView ID="pvImportToIapps" runat="server">
            <div class="import-tab-content">       
                <div class="import-tab-row"><h3>Current Site: <%= SiteManager.GetCurrentSite().Title %></h3></div>

                <div class="import-tab-row"><h3>Select Actions to Perform</h3></div>
                <div class="import-tab-row"><input type="checkbox" name="import" value="importAttributes" id="importAttributes" /><label for="importAttributes"> Import Attributes</label></div>
                <div class="import-tab-row"><input type="checkbox" name="import" value="importProductTypes" id="importProductTypes" /><label for="importProductTypes"> Import Product Types</label></div>
                <div class="import-tab-row"><input type="checkbox" name="import" value="importProducts" id="importProducts" /><label for="importProducts"> Import Products</label></div>
                <div class="import-tab-row"><input type="checkbox" name="import" value="importRelatedProducts" id="importRelatedProducts" /><label for="importRelatedProducts"> Import Related Products</label></div>
                <div class="import-tab-row"><input type="checkbox" name="import" value="importImages" id="importImages" /><label for="importImages"> Import Images</label></div>
                <div class="import-tab-row"><input type="checkbox" name="import" value="importPricesets" id="importPricesets" /><label for="importPricesets"> Import Pricesets</label></div>
               
                <asp:PlaceHolder ID="phTranslation" runat="server">
                    <div class="import-tab-row"><h3>Translation Options</h3></div>
                    <div class="import-tab-row"><input type="checkbox" name="import" value="translateProductTypes" id="translateProductTypes" /><label for="translateProductTypes"> Translate Product Types</label></div>
                    <div class="import-tab-row"><input type="checkbox" name="import" value="translateProducts" id="translateProducts" /><label for="translateProducts"> Translate Products, SKUs and Images</label></div>
                    <div class="import-tab-row">
                        <asp:Label ID="lblSiteList" runat="server" CssClass="import-tab-form-label">Site List:</asp:Label>
                        <asp:DropDownList ID="ddlSiteList" runat="server" CssClass="import-tab-form-value" Width="300" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                </asp:PlaceHolder>
                <p></p>
                <div class="import-tab-row"><b>Database location: </b><asp:Literal ID="litDatabaseLocation1" runat="server"></asp:Literal></div>
                <div class="import-tab-footer"><asp:HyperLink ID="btnImport" runat="server" CssClass="button"  NavigateUrl="javascript:void(0);">Import to iAPPS</asp:HyperLink></div>
            </div>
        </ComponentArt:PageView>
        <ComponentArt:PageView ID="pvExportProducts" runat="server">
            <asp:Panel ID="pnlExportProducts" runat="server" CssClass="import-tab-content">
                <div class="import-tab-row"><h3>Current Site: <%= SiteManager.GetCurrentSite().Title %></h3></div>

                <div class="import-tab-row"><h3>Export Products</h3></div>
                <div class="import-tab-row">
                    <asp:Label ID="lblExportFileLocation" runat="server" CssClass="import-tab-form-label">File Location:</asp:Label><span class="import-tab-form-value"><asp:Literal ID="litExportFileLocation" runat="server"></asp:Literal></span>
                </div>
                <div class="import-tab-row">
                    <asp:Label ID="lblExportFileName" runat="server" CssClass="import-tab-form-label">File Name:</asp:Label><asp:TextBox ID="txtExportFileName" runat="server" CssClass="import-tab-form-value" MaxLength="100" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvExportFileName" ControlToValidate="txtExportFileName" CssClass="error" EnableClientScript="true" Display="Dynamic" ErrorMessage="File Name Required" runat="server" SetFocusOnError="true" ValidationGroup="ExportProducts"></asp:RequiredFieldValidator>
                </div>
                <p></p>
                <div class="import-tab-footer">
                    <asp:Button ID="btnExportProducts" runat="server" CssClass="button" Text ="Export Products" OnClientClick="return confirm('Export products now?');" OnCommand="ControlCommand" CommandName="ExportProducts" CausesValidation="true" ValidationGroup="ExportProducts" />
                </div>
                <asp:Panel ID="pnlExportProductsMessage" runat="server" CssClass="import-tab-footer error" Visible="false"><asp:Literal ID="litExportProductsMessage" runat="server"></asp:Literal></asp:Panel>
            </asp:Panel>
        </ComponentArt:PageView>
        <ComponentArt:PageView ID="pvDeleteProducts" runat="server">
            <asp:Panel ID="pnlDeleteProducts" runat="server" CssClass="import-tab-content" DefaultButton="btnDeleteProducts"> 
                <div class="import-tab-row"><h3>Current Site: <%= SiteManager.GetCurrentSite().Title %></h3></div>
 
                <div class="import-tab-row"><h3>Delete ALL Products</h3></div>
                <p></p>
                <div class="import-tab-row"><b>Database location: </b><asp:Literal ID="litDatabaseLocation2" runat="server"></asp:Literal></div>                
                <div class="import-tab-footer">                    
                    <asp:Button ID="btnDeleteProducts" runat="server" CssClass="button" Text="Delete ALL Products" OnClientClick="return confirm('Delete ALL products?');" OnCommand="ControlCommand" CommandName="DeleteAllProducts" CausesValidation="true" ValidationGroup="DeleteAllProducts" />
                </div>
                <asp:Panel ID="pnlDeleteProductsMessage" runat="server" CssClass="import-tab-footer error" Visible="false"><asp:Literal ID="litDeleteProductsMessage" runat="server"></asp:Literal></asp:Panel>
            </asp:Panel>
        </ComponentArt:PageView>
    </ComponentArt:MultiPage>
    </div>
    <div>
         <div id="importForm" class="progress" style="display:block">
        </div>


        <div id="importComplete" class="progress">
            <div id="completeMsg" class="complete"></div>
        </div>

         <div id="importProgress" class="progress">
            <ul id="meter">	
                <li>
		            <div class="meter">
	                    <span style="width: 0%" id="exportMeter"></span>
                    </div>
		        </li>
                <li>
		            <span id="progressMsg">Initializing Task...</span>
		        </li>
            </ul>

             <hr />

             <div class="filters">
                 <a href="javascript:void(0)" onclick="FilterErrors()" id="filterErrors">Show Errors Only</a>
                 <a href="javascript:void(0)" onclick="FilterAll()" id="filterAll" style="display:none">Show All</a>
             </div>

             <div id="log"></div>

        </div>

    </div>
    

<script type="text/javascript">

$(document).ready(function () {
        window.setInterval(function () {
            var url = '/commerceadmin/api/login/CreateFormsAuthCookie';
            $.get(url);                
        },900000);
});

    function BeginImport(method, workerPage) {

        // build querystring
        var i = 0;
        var params = [];
        $("input[name='" + method + "']").each(function () {
            // Checkboxes of functions to perform
            if ($(this).prop('checked') == true) {
                params[i++] = $(this).val() + "=1";
            }
        });
        if (method == 'load') {
            var x = 0;
            var files = [];i
            // When loading Excel spreadsheets, pick which file(s) to load
            $("input[name$='chkProcessFile']").each(function () {
                if ($(this).prop('checked') == true) {
                    files[x++] = $(this).val();
                }
            });
            if (files.length > 0) {
                params[i++] = "processFiles=" + files.join("|");
            }
        }

        if (method == 'import') {
            var siteList = $("#ddlSiteList");
             if (siteList && siteList.val())
                params[i++] = 'translateSiteList=' + siteList.val();
        }

        if (method == 'export') {
            // ToDo: get checkbox values
            params[i++] = 'fileName=' + $("input[id$='txtExportFileName']").val();
        }
        if (params.length > 0) {
            workerPage += "?" + params.join("&");
        }

        // create an iframe.
        var iframe = document.createElement("iframe");

        // point the iframe to the location of the long running process.
        iframe.src = workerPage;

        // make the iframe invisible.
        if(method !== 'export')
            iframe.style.display = "none";

        // add the iframe to the DOM.  The process will begin execution at this point.
        document.body.appendChild(iframe);

        $('.import-tab-container').hide(500);
        $("#importComplete").hide();

        $("#meter").show();

        $("#exportMeter").animate({
            width: "0%"
        }, 200);

        $("#progressMsg").html("Initializing Task...");

        $("#importProgress").show();

        $("#log").empty();
    }

    function UpdateProgress(percentComplete, message, log) {

        $("#progressMsg").html(message);

        $("#log").append('<p class="log">' + log + '</p>');

        if (percentComplete > 0) {

            $("#exportMeter").animate({
                width: percentComplete + "%"
            }, 200);
        }
    }

    function UpdateProgressError(percentComplete, message, log) {

        $("#progressMsg").html(message);

        $("#log").append('<p class="error">' + log + '</p>');

        if (percentComplete > 0) {

            $("#exportMeter").animate({
                width: percentComplete + "%"
            }, 200);
        }
    }


    function UpdateLog(log) {

        $("#log").append('<p class="log">' + log + '</p>');
    }

    function UpdateLogError(log) {

        $("#log").append('<p class="error">' + log + '</p>');
    }

    function UpdateComplete(msg) {

        $("#meter").hide();
        $("#importComplete").show(500);
        $('.import-tab-container').show(500);
        $("#completeMsg").html(msg);
    }

    $(function () {
        $(".meter > span").each(function () {
            $(this)
				    .data("origWidth", $(this).width())
				    .width(0)
				    .animate({
				        width: $(this).data("origWidth") + "%"
				    }, 1200);
        });

        $('#translateProductTypes').prop("disabled", true);
        $('#importProductTypes').change(function () {
            $('#translateProductTypes').prop("disabled", !$(this).prop("checked"));
        });

        $('#translateProducts').prop("disabled", true);
        $('#importProducts').change(function () {
            $('#translateProducts').prop("disabled", !$(this).prop("checked"));
        });
    });


    function FilterErrors() {

        $("p.log").hide();
        $("#filterErrors").hide();
        $("#filterAll").show();
    }

    function FilterAll() {

        $("p.log").show();
        $("#filterErrors").show();
        $("#filterAll").hide();
    }

    function uploadFile(uploadControl) {
        if (uploadControl.value != '') {
            document.getElementById("<%= btnUploadFile.ClientID %>").click();
        }
    }


</script>

</asp:Content>




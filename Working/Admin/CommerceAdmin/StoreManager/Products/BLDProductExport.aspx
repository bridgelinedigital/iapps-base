﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BLDProductExport.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.BLDProductExport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        function DownloadExportedFile() {
            document.getElementById('btnDownloadExportedFile').click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="filePathAndName" runat="server" />
        <asp:Button ID="btnDownloadExportedFile" runat="server" style="display:none;" OnClick="btnDownloadExportedFile_Click" ClientIDMode="Static" />
    </div>
    </form>
</body>
</html>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="remove"
                xmlns:resx="iAppsExtUri" exclude-result-prefixes="resx">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:for-each select="/action">
      <xsl:for-each select="row">
        <div class="row clear-fix {@css}">
          <xsl:for-each select="column">
            <div class="columns {@css}">
              <xsl:if test="@id != ''">
                <xsl:attribute name="id">
                  <xsl:value-of select="@id"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:for-each select="item">
                <xsl:choose>
                  <xsl:when test="@type = 'treeBreadcrumb'">
                    <xsl:attribute name="id">
                      <xsl:value-of select="'pageDirectoryPath'"/>
                    </xsl:attribute>
                    <xsl:attribute name="css">
                      <xsl:value-of select="'tree-breadcrumb'"/>
                    </xsl:attribute>
                    <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
                  </xsl:when>
                  <xsl:when test="@type = 'gridHeading'">
                    <xsl:call-template name="GridHeading">
                      <xsl:with-param name="item" select="item" />
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="@type = 'button'">
                    <div class="{@css} {@mode}">
                      <input type="button" class="{@css} command-button" commandArgument="{@commandArgument}" commandName="{@commandName}">
                        <xsl:attribute name="value">
                          <xsl:value-of select="resx:GetResourceText(@text)"/>
                        </xsl:attribute>
                      </input>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'selectAll'">
                    <div class="select-all-box">
                      <label>
                        <xsl:value-of select="resx:GetResourceText('Select All')"/>
                      </label>
                      <input type="checkbox" class="item-select-all" />
                    </div>
                  </xsl:when>
                  <xsl:when test="@type='serverLiteral'">
                    <asp:Literal ID="{@id}" runat="server">
                      <xsl:attribute name="Text">
                        <xsl:value-of select="resx:GetResourceText(@text)"/>
                      </xsl:attribute>
                    </asp:Literal>
                  </xsl:when>
                  <xsl:when test="@type='serverPlaceholder'">
                    <div class="{@css} {@mode}">
                      <asp:Placeholder ID="{@id}" runat="server" />
                    </div>
                  </xsl:when>
                  <xsl:when test="@type='serverButton'">
                    <div class="{@css} {@mode}">
                      <asp:Button ID="{@id}" runat="server" CssClass="{@css} command-button" CommandName="{@commandName}" CommandArgument="{@commandArgument}">
                        <xsl:attribute name="Text">
                          <xsl:value-of select="resx:GetResourceText(@text)"/>
                        </xsl:attribute>
                      </asp:Button>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'serverLink'">
                    <asp:HyperLink ID="{@id}" runat="server" Text="{@text}" ClientIDMode="Static" CssClass="{@css}"></asp:HyperLink>
                  </xsl:when>
                  <xsl:when test="@type = 'textBox'">
                    <input type="textbox" ID="{@id}" class="{@css}">
                      <xsl:if test="@width != ''">
                        <xsl:attribute name="style">
                          <xsl:value-of select="concat('width:', @width, 'px')"/>
                        </xsl:attribute>
                      </xsl:if>
                    </input>
                  </xsl:when>
                  <xsl:when test="@type = 'imageButton'">
                    <input type="image" ID="{@id}" runat="server" src="{@image}" />
                  </xsl:when>
                  <xsl:when test="@type = 'link'">
                    <a class="command-button {@css}" commandArgument="{@commandArgument}" commandName="{@commandName}">
                      <xsl:value-of select="resx:GetResourceText(@text)"/>
                    </a>
                  </xsl:when>
                  <xsl:when test="@type = 'image'">
                    <img class="{@css}" src="{@src}">
                      <xsl:attribute name="alt">
                        <xsl:value-of select="resx:GetResourceText(@altText)"/>
                      </xsl:attribute>
                    </img>
                  </xsl:when>
                  <xsl:when test="@type = 'gridHeading'">
                    <xsl:call-template name="GridHeading">
                      <xsl:with-param name="item" select="item" />
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="@type = 'searchBlock'">
                    <div class="grid-search-box common-action">
                      <input type="textbox" ID="txtSearch_{@id}" class="grid-search-text">
                        <xsl:attribute name="placeholder">
                          <xsl:value-of select="resx:GetResourceText(@textTooltip)"/>
                        </xsl:attribute>
                        <xsl:attribute name="style">
                          <xsl:choose>
                            <xsl:when test="@textWidth != ''">
                              <xsl:value-of select="concat('width:', @textWidth, 'px')"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="'width: 180px'"/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:attribute>
                      </input>
                      <input type="image" ID="btnSearch_{@id}" class="grid-search-button" src="/Admin/App_Themes/General/images/search-button.png" />
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'dropDown'">
                    <div class="{@css} {@mode}">
                      <select class="command-select" commandName="{@commandName}">
                        <xsl:for-each select="item">
                          <option value="{@commandArgument}">
                            <xsl:value-of select="resx:GetResourceText(@text)"/>
                          </option>
                        </xsl:for-each>
                      </select>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'dropMenu'">
                    <xsl:if test="count(item) &gt; 0">
                      <ul class="{@css} {@mode}" name="{@name}">
                        <xsl:if test="@commandName != ''">
                          <xsl:attribute name="commandName">
                            <xsl:value-of select="@commandName"/>
                          </xsl:attribute>
                        </xsl:if>
                        <li>
                          <xsl:value-of select="@label"/>
                          <a>
                            <xsl:value-of select="resx:GetResourceText(@text)"/>
                          </a>
                          <ul>
                            <xsl:for-each select="item">
                              <li class="command-button {@css}" commandArgument="{@commandArgument}" commandName="{@commandName}">
                                <xsl:if test="@id != ''">
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="@id"/>
                                  </xsl:attribute>
                                </xsl:if>
                                <xsl:choose>
                                  <xsl:when test="@type = 'serverLinkButton'">
                                    <asp:LinkButton ID="{@id}" runat="server" CssClass="{@css}" CommandName="{@commandName}" CommandArgument="{@commandArgument}">
                                      <xsl:attribute name="Text">
                                        <xsl:value-of select="resx:GetResourceText(@text)"/>
                                      </xsl:attribute>
                                    </asp:LinkButton>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="resx:GetResourceText(@text)"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </li>
                            </xsl:for-each>
                          </ul>
                        </li>
                      </ul>
                    </xsl:if>
                  </xsl:when>
                  <xsl:when test="@type = 'tabMenu'">
                    <div class="tabs">
                      <ul class="{@css} {@mode} tab-menu">
                        <xsl:if test="@id != ''">
                          <xsl:attribute name="id">
                            <xsl:value-of select="@id"/>
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:for-each select="item">
                          <li class="command-button {@css}" commandName="TabIndex">
                            <xsl:attribute name="commandArgument">
                              <xsl:choose>
                                <xsl:when test="@commandArgument != ''">
                                  <xsl:value-of select="@commandArgument"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="position()"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:attribute>
                            <span>
                              <xsl:choose>
                                <xsl:when test="@type = 'serverLinkButton'">
                                  <asp:LinkButton ID="{@id}" runat="server" CssClass="{@css}" CommandName="{@commandName}" CommandArgument="{@commandArgument}">
                                    <xsl:attribute name="Text">
                                      <xsl:value-of select="resx:GetResourceText(@text)"/>
                                    </xsl:attribute>
                                  </asp:LinkButton>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="resx:GetResourceText(@text)"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </span>
                          </li>
                        </xsl:for-each>
                      </ul>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'sortMenu'">
                    <ul class="drop-menu sort-menu {@mode} sort-action">
                      <li>
                        <a>
                          <xsl:value-of select="resx:GetResourceText(@text)"/>
                        </a>
                        <ul>
                          <xsl:for-each select="item">
                            <li class="command-button" commandArgument="{@commandArgument}" commandName="Sort">
                              <xsl:value-of select="resx:GetResourceText(@text)"/>
                            </li>
                          </xsl:for-each>
                        </ul>
                      </li>
                    </ul>
                  </xsl:when>
                  <xsl:when test="@type = 'list'">
                    <ul class="{@css} {@mode}">
                      <xsl:for-each select="item">
                        <li class="command-button" commandArgument="{@commandArgument}" commandName="{@commandName}">
                          <xsl:value-of select="resx:GetResourceText(@text)"/>
                        </li>
                      </xsl:for-each>
                    </ul>
                  </xsl:when>
                  <xsl:when test="@type = 'viewSelector'">
                    <div class="view-selector">
                      <img class="list-view" commandName="viewSelector" commandArgument="1" src="/Admin/App_Themes/General/images/grid-list-view.png" />
                      <img class="thumb-view" commandName="viewSelector" commandArgument="2" src="/Admin/App_Themes/General/images/grid-thumb-view.png" />
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'pagerInfo'">
                    <div class="pager-info">
                      <xsl:comment>Paging Information</xsl:comment>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'pagerButtons'">
                    <div class="pager-buttons">
                      <img class="prev-page" src="/Admin/App_Themes/General/images/prev.gif">
                        <xsl:attribute name="alt">
                          <xsl:value-of select="resx:GetResourceText('Previous')"/>
                        </xsl:attribute>
                      </img>
                      <img class="next-page" src="/Admin/App_Themes/General/images/next.gif">
                        <xsl:attribute name="alt">
                          <xsl:value-of select="resx:GetResourceText('Next')"/>
                        </xsl:attribute>
                      </img>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'pagerSize'">
                    <!--<div class="pager-size">
                      <label>
                        <xsl:value-of select="@text"/>
                      </label>
                      <select class="pager-size">
                        <xsl:choose>
                          <xsl:when test="count(option)">
                            <xsl:for-each select="option">
                              <option value="{@value}">
                                <xsl:value-of select="@text"/>
                              </option>
                            </xsl:for-each>
                          </xsl:when>
                          <xsl:otherwise>
                            <option value="-1">All</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                          </xsl:otherwise>
                        </xsl:choose>
                      </select>
                    </div>-->
                    <ul class="drop-menu pager-size">
                      <li>
                        <a>
                          <xsl:value-of select="'Show'"/>
                        </a>
                        <ul>
                          <xsl:choose>
                            <xsl:when test="count(item)">
                              <xsl:for-each select="item">
                                <li commandArgument="{@value}">
                                  <xsl:value-of select="resx:GetResourceText(@text)"/>
                                </li>
                              </xsl:for-each>
                            </xsl:when>
                            <xsl:otherwise>
                              <li commandArgument="-1">
                                <xsl:value-of select="resx:GetResourceText('All')"/>
                              </li>
                              <li commandArgument="10">10</li>
                              <li commandArgument="20">20</li>
                              <li commandArgument="50">50</li>
                            </xsl:otherwise>
                          </xsl:choose>
                        </ul>
                      </li>
                    </ul>
                  </xsl:when>
                  <xsl:when test="@type = 'navLinks'">
                    <xsl:call-template name="NavigationLinks">
                      <xsl:with-param name="item" select="item" />
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="@type = 'toggleLinks'">
                    <xsl:call-template name="ToggleLinks">
                      <xsl:with-param name="item" select="item" />
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </div>
          </xsl:for-each>
          <xsl:if test="contains(@css, 'tab-row')">
            <div class="clear-fix">
              <xsl:comment>Clear float</xsl:comment>
            </div>
            <div class="tab-border">
              <xsl:comment>Tab Border</xsl:comment>
            </div>
          </xsl:if>
          <xsl:for-each select="item">
            <xsl:choose>
              <xsl:when test="@type = 'navLinks'">
                <xsl:call-template name="NavigationLinks">
                  <xsl:with-param name="item" select="item" />
                </xsl:call-template>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </div>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="ToggleLinks">
    <xsl:param name="item" />
    <div class="toggle-links">
      <xsl:for-each select="item">
        <a href="{@url}" class="{@css}">
          <xsl:value-of select="resx:GetResourceText(@text)"/>
        </a>
      </xsl:for-each>
    </div>
  </xsl:template>
  <xsl:template name="NavigationLinks">
    <xsl:param name="item" />
    <div class="navigation-bar clear-fix {@css}">
      <div class="navigation-links">
        <xsl:for-each select="item">
          <a href="{@url}">
            <xsl:value-of select="resx:GetResourceText(@text)"/>
          </a>
        </xsl:for-each>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="GridHeading">
    <xsl:param name="item" />
    <h2 class="grid-heading">
      <xsl:attribute name="id">
        <xsl:choose>
          <xsl:when test="@mode and @mode = 'static'">
            <xsl:value-of select="'staticSelectedNodeTitle'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'selectedNodeTitle'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@id and @text">
          <asp:Literal ID="{@id}" runat="server">
            <xsl:attribute name="Text">
              <xsl:value-of select="resx:GetResourceText(@text)"/>
            </xsl:attribute>
          </asp:Literal>
        </xsl:when>
        <xsl:when test="@text">
          <xsl:value-of select="resx:GetResourceText(@text)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </h2>
  </xsl:template>
</xsl:stylesheet>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="remove"
xmlns:resx="iAppsExtUri" exclude-result-prefixes="resx">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:for-each select="/action">
      <xsl:for-each select="row">
        <div class="row clear-fix {@css}">
          <xsl:for-each select="column">
            <div class="columns {@css}">
              <xsl:if test="@id != ''">
                <xsl:attribute name="id">
                  <xsl:value-of select="@id"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:for-each select="item">
                <xsl:choose>
                  <xsl:when test="@type = 'button'">
                    <div class="{@css} {@mode}">
                      <input type="button" class="{@css} {@mode} command-button" commandArgument="{@commandArgument}" commandName="{@commandName}">
                        <xsl:attribute name="Value">
                          <xsl:value-of select="resx:GetResourceText(@text)"/>
                        </xsl:attribute>
                      </input>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'imageButton'">
                    <input type="image" class="{@css} {@mode} command-button" nodeTypes="{@nodeTypes}" commandArgument="{@commandArgument}" commandName="{@commandName}" >
                      <xsl:attribute name="src">
                        <xsl:value-of select="resx:ResolveUrl(concat('~/App_Themes/General/images/tree/', @icon))"/>
                      </xsl:attribute>
                      <xsl:attribute name="Title">
                        <xsl:value-of select="resx:GetResourceText(@text)"/>
                      </xsl:attribute>
                    </input>
                  </xsl:when>
                  <xsl:when test="@type = 'dropMenu'">
                    <ul class="{@css} {@mode}" nodeTypes="{@nodeTypes}">
                      <li>
                        <xsl:value-of select="@label"/>
                        <a>
                          <xsl:value-of select="resx:GetResourceText(@text)"/>
                        </a>
                        <ul>
                          <xsl:for-each select="item">
                            <li class="command-button" commandArgument="{@commandArgument}" commandName="{@commandName}">
                              <xsl:choose>
                                <xsl:when test="@type = 'serverLink'">
                                  <asp:LinkButton ID="{@id}" runat="server" CssClass="{@css}" CommandName="{@commandName}" CommandArgument="{@commandArgument}" >
                                    <xsl:attribute name="Text">
                                      <xsl:value-of select="resx:GetResourceText(@text)"/>
                                    </xsl:attribute>
                                  </asp:LinkButton>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="resx:GetResourceText(@text)"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </li>
                          </xsl:for-each>
                        </ul>
                      </li>
                    </ul>
                  </xsl:when>
                  <xsl:when test="@type = 'dropDown'">
                    <div class="{@css} {@mode}">
                      <select class="command-select" commandName="{@commandName}">
                        <xsl:for-each select="item">
                          <option value="{@commandArgument}">
                            <xsl:value-of select="resx:GetResourceText(@text)"/>
                          </option>
                        </xsl:for-each>
                      </select>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type='serverDropDown'">
                    <asp:DropDownList ID="{@id}" runat="server" CssClass="{@css} command-select" CommandName="{@commandName}">
                      <xsl:if test="@width">
                        <xsl:attribute name="Width">
                          <xsl:value-of select="@width"/>
                        </xsl:attribute>
                      </xsl:if>
                      <xsl:if test="@commandArgument">
                        <xsl:attribute name="CommandArgument">
                          <xsl:value-of select="@commandArgument"/>
                        </xsl:attribute>
                      </xsl:if>
                    </asp:DropDownList>
                  </xsl:when>
                  <xsl:when test="@type = 'tabMenu'">
                    <div class="tabs">
                      <ul class="{@css} {@mode} tab-menu">
                        <xsl:if test="@id != ''">
                          <xsl:attribute name="id">
                            <xsl:value-of select="@id"/>
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:for-each select="item">
                          <li class="command-button" commandName="TabIndex">
                            <xsl:attribute name="commandArgument">
                              <xsl:choose>
                                <xsl:when test="@commandArgument != ''">
                                  <xsl:value-of select="@commandArgument"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="position()"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:attribute>
                            <xsl:if test="@id">
                              <xsl:attribute name="id">
                                <xsl:value-of select="@id"/>
                              </xsl:attribute>
                            </xsl:if>
                            <span>
                              <xsl:value-of select="resx:GetResourceText(@text)"/>
                            </span>
                          </li>
                        </xsl:for-each>
                      </ul>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'toggleExpand'">
                    <img class="tree-toggle collapse">
                      <xsl:attribute name="src">
                        <xsl:value-of select="resx:ResolveUrl('~/App_Themes/General/images/tree/expand-all.png')"/>
                      </xsl:attribute>
                      <xsl:attribute name="alt">
                        <xsl:value-of select="resx:GetResourceText(@text)"/>
                      </xsl:attribute>
                    </img>
                  </xsl:when>
                  <xsl:when test="@type = 'toggleExpandText'">
                    <a href="javascript://" class="tree-toggle collapse">
                      <xsl:value-of select="resx:GetResourceText(@text)"/>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </div>
          </xsl:for-each>
          <xsl:if test="count(column) &gt; 0">
            <div class="clear-fix">
              <xsl:comment>Clear float</xsl:comment>
            </div>
          </xsl:if>
          <xsl:if test="contains(@css, 'tab-row')">
            <div class="tab-border">
              <xsl:comment>Tab Border</xsl:comment>
            </div>
          </xsl:if>
        </div>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>

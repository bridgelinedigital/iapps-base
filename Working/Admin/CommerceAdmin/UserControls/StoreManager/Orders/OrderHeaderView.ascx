﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderHeaderView.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Orders.OrderHeaderView" %>
<div class="form-row">
    <label class="form-label">Name:</label>
    <div class="form-value"><asp:Literal ID="lblCustomer" runat="server"></asp:Literal></div>
</div>
<div class="form-row">
    <label class="form-label">Email:</label>
    <div class="form-value"><asp:HyperLink ID="hplEmail" runat="server"></asp:HyperLink></div>
</div>
<div class="form-row">
    <label class="form-label" id="lblAccountNumberString" runat="server">Account Number:</label>
    <div class="form-value"><asp:Literal ID="lblAccountNumber" runat="server"></asp:Literal></div>
</div>
<div class="form-row">
    <label class="form-label" runat="server" id="lblOrderDateString">Date:</label>
    <div class="form-value"><asp:Literal ID="lblDate" runat="server"></asp:Literal></div>
</div>
<div class="form-row">
    <label class="form-label">Order Number:</label>
    <div class="form-value"><asp:HyperLink ID="hplOrderNumber" runat="server" Text="" NavigateUrl="#"></asp:HyperLink></div>
</div>
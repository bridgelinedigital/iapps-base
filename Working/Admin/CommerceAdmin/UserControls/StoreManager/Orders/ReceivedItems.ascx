﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReceivedItems.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Orders.ReceivedItems" %>
<asp:ListView ID="lstReceivedItems" runat="server" ItemPlaceholderID="itemContainer" Visible='<%# this.IsSynWarehouse %>'
    DataKeyNames="Id" ClientIDMode="AutoID" OnItemDataBound="lstReceivedItems_ItemDataBound">
    <LayoutTemplate>
        <table width="100%" cellpadding="0" cellspacing="0" class="grid-view" id="gridview">
            <thead>
                <tr>
                    <th>
                        Product Name
                    </th>
                    <th>
                        SKU
                    </th>
                    <th>
                        Received Qty
                    </th>
                    <th>
                        Received Date
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder ID="itemContainer" runat="server" />
            </tbody>
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "alternate-row" %>'>
            <td>
                <asp:Literal ID="ltProduct" runat="server" />
            </td>
            <td>
                <asp:Literal ID="ltSKU" runat="server" />
            </td>
            <td>
                <asp:Literal ID="ltShippedQty" runat="server" Text='<%# Eval("Quantity") %>' />
            </td>
            <td>
                <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("CreatedDate") %>' />
            </td>
        </tr>
    </ItemTemplate>
</asp:ListView>

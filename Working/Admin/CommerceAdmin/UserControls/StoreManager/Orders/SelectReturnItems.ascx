﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectReturnItems.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Orders.SelectReturnItems" %>
<script type="text/javascript">
    function Validate_ReceivedVsAcceptedAndRejected(sender, args) {
        var validatorId = sender.id;
        var txtReceivedQtyId = validatorId.replace('custReceivedVsAcceptedAndRejected', "txtReceivedQty");
        var txtAcceptedQtyId = validatorId.replace('custReceivedVsAcceptedAndRejected', "txtAcceptedQty");
        var txtRejectedQtyId = validatorId.replace('custReceivedVsAcceptedAndRejected', "txtRejectedQty");
       
        if (Number($('#' + txtReceivedQtyId).val()) == Number($('#' + txtAcceptedQtyId).val()) + Number($('#' + txtRejectedQtyId).val())) {
            args.IsValid = true;
            return;
        }
        args.IsValid = false;
    }

    function ToggleRequiredFieldValidator(txtbox) {

        var txtboxId = txtbox.id;
        var rfWarehouseId = txtboxId.replace('txtBackToInvQty', 'rfWarehouse');
        var rval=document.getElementById(rfWarehouseId);
        if (Number($('#' + txtboxId).val()) > 0) {
            ValidatorEnable(rval, true);
        }
        else {
            ValidatorEnable(rval, false);
        }
    }

   // var selectedItemsForReturn=0;
    function AtLeastOneItemIsSelected(sender, args) {
        if (selectedItemsForReturn > 0) {
            args.IsValid = true;
            return
        }
        args.IsValid = false;

    }

    function updateSelectedItemCount(checkbox) {

        if (!checkbox.checked) {
            if (selectedItemsForReturn > 0)
                selectedItemsForReturn--;
            else
                selectedItemsForReturn = 0;
        }
        else {
                selectedItemsForReturn++;
        }
    }
</script>

 <asp:TextBox ID="txtSearch" runat="server" CssClass="textBoxes" placeholder="Type here to filter results"></asp:TextBox>
 <asp:Button ID="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click" CssClass="small-button" />
                    
<asp:ListView ID="lstOrderItemsElligibleForReturn" runat="server" ItemPlaceholderID="itemContainer" OnPagePropertiesChanging="lstOrderItemsElligibleForReturn_PagePropertiesChanging"
    DataKeyNames="Id" ClientIDMode="AutoID" >
    <LayoutTemplate>      
        <table width="100%" cellpadding="0" cellspacing="0" class="grid-view" id="gridview">
            <thead>
                <tr>
                    <th runat="server" id="headerSelect">
                    </th>
                    <th>
                        Return Qty
                    </th>
                    <th runat="server" id="tdHdReceivedQty" visible="false">
                        Received Qty
                    </th>
                    <th runat="server" id="tdHdAcceptedQty" visible="false">
                        Accepted Qty
                    </th>
                    <th runat="server" id="tdHdRejectedQty" visible="false">
                        Rejected Qty
                    </th>
                    <th runat="server" id="tdHdBackToInvQty" visible="false">
                        Inventory Qty
                    </th>
                    <th id="thShippedQty" runat="server">
                        Shipped Qty
                    </th>
                    <th>
                        Product Name
                    </th>
                    <th>
                        SKU
                    </th>
                    <th  id="thPrice" runat="server">
                        Price
                    </th>
                    <th id="thItemTotal" runat="server">
                        Item Total
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder ID="itemContainer" runat="server" />
            </tbody>
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "alternate-row" %>'>
            <td runat="server" id="tdCheckbox">
                <asp:CheckBox ID="chkReturnItem"  runat="server" onclick="javascript:toggleTextBoxBasedOnCheckbox(this,'chkReturnItem', 'txtReturnQty'); updateSelectedItemCount(this);" OnCheckedChanged="chkReturnItem_CheckedChanged" />
                <asp:HiddenField ID="hdnOrderItemId" runat="server" Value='<%# Eval("Id")  %>' />
            </td>
            <td runat="server" id="tdEditReturnQty">
                <asp:TextBox ID="txtReturnQty"   runat="server" Enabled="true" Text='<%# Eval("ElligibleQtyForReturn")  %>' />
                <asp:CompareValidator runat="server" ID="qtyDataType" Type="Double" Operator="DataTypeCheck"
                    ValidationGroup="vgInitiation" ControlToValidate="txtReturnQty" Display="None"
                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, ReturnQtyShouldBeDouble %>"></asp:CompareValidator>
                <asp:RangeValidator runat="server" ID="qtyRange" Type="Double" ControlToValidate="txtReturnQty"
                    ValidationGroup="vgInitiation" CultureInvariantValues="true" MinimumValue="1"
                    MaximumValue="100000000" Display="None" Text="*" ErrorMessage="<%$ Resources:GUIStrings, ReturnQtyShouldBeLessThanEqualToShipQty %>"></asp:RangeValidator>
            </td>
            <td runat="server" id="tdViewReturnQty">
                <asp:Literal ID="ltReturnQty" runat="server" />
            </td>
            <td runat="server" id="tdReceivedQty" visible="false">
                <asp:Literal ID="litReceivedQty" runat="server" />
                <asp:TextBox ID="txtReceivedQty" Enabled='<%# this.IsEditable %>' runat="server" Width="50" Text="0" />
                <asp:CompareValidator runat="server" ID="cvReceivedQty" Type="Double" Operator="DataTypeCheck"
                    ValidationGroup="vgReceiver" ControlToValidate="txtReceivedQty" Display="None"
                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, RMA_ReceivedQtyShouldBeDouble %>"></asp:CompareValidator>
                <asp:RangeValidator ID="rvReceivedQty" runat="server" ValidationGroup="vgReceiver" Type="Double"
                    ControlToValidate="txtReceivedQty" MinimumValue="0" MaximumValue="10000000" Display="None"
                    Operator="LessThanEqual" ErrorMessage="<%$ Resources:GUIStrings, RMA_ReceivedQtycannotbemorethantheshippedqty %>"></asp:RangeValidator>
            </td>
            <td runat="server" id="tdAcceptedQty" visible="false">
                <asp:Literal ID="litAcceptedQty" runat="server"></asp:Literal>
                <asp:TextBox ID="txtAcceptedQty" Enabled='<%# this.IsEditable %>' runat="server" Width="50" Text="0" />
                <asp:CompareValidator runat="server" ID="cvAcceptedQty" Type="Double" Operator="DataTypeCheck"
                    ValidationGroup="vgReceiver" ControlToValidate="txtAcceptedQty" Display="None"
                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, RMA_AcceptedQtyShouldBeDouble %>"></asp:CompareValidator>
                <asp:CompareValidator ID="cvAcceptedQtyVsReceivedQty" runat="server" ValidationGroup="vgReceiver"
                    ControlToValidate="txtAcceptedQty" Display="None" ControlToCompare="txtReceivedQty"
                    Operator="LessThanEqual" ErrorMessage="<%$ Resources:GUIStrings, RMA_AcceptedQtycannotbemorethanthereceivedqty %>"></asp:CompareValidator>
            </td>
            <td runat="server" id="tdRejectedQty" visible="false">
                <asp:Literal ID="litRejectedQty" runat="server"></asp:Literal>
                <asp:TextBox ID="txtRejectedQty" Enabled='<%# this.IsEditable %>' runat="server" Width="50" Text="0" />
                <asp:CompareValidator runat="server" ID="cvRejectedQty" Type="Double" Operator="DataTypeCheck"
                    ValidationGroup="vgReceiver" ControlToValidate="txtRejectedQty" Display="None"
                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, RMA_RejectedQtyShouldBeDouble %>"></asp:CompareValidator>
                <asp:CompareValidator ID="cvRejectedQtyVsReceivedQty" runat="server" ValidationGroup="vgReceiver"
                    ControlToValidate="txtRejectedQty" Display="None" ControlToCompare="txtReceivedQty"
                    Operator="LessThanEqual" ErrorMessage="<%$ Resources:GUIStrings, RMA_RejectedQtycannotbemorethanthereceivedqty %>"></asp:CompareValidator>
            </td>
            <td runat="server" id="tdBackToInvQty" visible="false" width="200">
                <asp:Literal ID="litBackToInvQty" runat="server"></asp:Literal>
                <asp:TextBox ID="txtBackToInvQty" Enabled='<%# this.IsEditable %>' runat="server" Width="50" Text="0" onblur="javascript:ToggleRequiredFieldValidator(this);" /> &nbsp;
                <asp:DropDownList ID="ddlWarehouse"  runat="server" AppendDataBoundItems="true" Width="125" CssClass="selectbox">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, RMA_SelectWarehouse %>" Value="-1"></asp:ListItem>
                </asp:DropDownList>

                <asp:RequiredFieldValidator ID="rfWarehouse" runat="server" ControlToValidate="ddlWarehouse" InitialValue="-1"
                    ValidationGroup="vgReceiver" ErrorMessage="<%$ Resources:GUIStrings, RMA_PleaseSelectAWarehouse %>"
                    Text="*" Display="None"></asp:RequiredFieldValidator>

                <asp:CompareValidator runat="server" ID="cvBackToInvQty" Type="Double" Operator="DataTypeCheck"
                    ValidationGroup="vgReceiver" ControlToValidate="txtBackToInvQty" Display="None"
                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, RMA_BackToInventoryQtyShouldBeDouble %>"></asp:CompareValidator>
                <asp:CompareValidator ID="cvBackToInvQtyVsAcceptedQty" runat="server" ValidationGroup="vgReceiver"
                    ControlToValidate="txtBackToInvQty" Display="None" ControlToCompare="txtAcceptedQty"
                    Operator="LessThanEqual" ErrorMessage="<%$ Resources:GUIStrings, RMA_BackToInvQtycannotbemorethantheacceptedqty %>"></asp:CompareValidator>
                <asp:CustomValidator ID="custReceivedVsAcceptedAndRejected" runat="server" ControlToValidate="txtReceivedQty"
                    ValidationGroup="vgReceiver" ErrorMessage="<%$ Resources:GUIStrings, RMA_ReceivedQtyCanNotBeLessThanSumOfAcceptedAndRejected %>"
                    ClientValidationFunction="Validate_ReceivedVsAcceptedAndRejected" Text="*" Display="None"></asp:CustomValidator>
            </td>
            <td id="tdShippedQty" runat="server">
                <asp:Literal ID="ltShippedQty" runat="server" Text='<%# Eval("ShippedQuantity") %>' />
            </td>
            <td>
                <asp:Literal ID="ltProduct" runat="server" Text='<%# Eval("SKU.ParentProduct.Title") %>' />
            </td>
            <td>
                <asp:Literal ID="ltSKU" runat="server" Text='<%# Eval("SKU.SKU") %>' />
            </td>
            <td id="tdPrice" runat="server">
                <asp:Literal ID="Literal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ItemTotal","{0:c}") %>' />
            </td>
            <td id="tdItemTotal" runat="server">
                <asp:Literal ID="ltItemTotal" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Price","{0:c}") %>' />
            </td>
        </tr>
    </ItemTemplate>
</asp:ListView>
 <asp:DataPager ID="orderItemPager" PagedControlID="lstOrderItemsElligibleForReturn" PageSize="25" runat="server">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" PreviousPageText="<" ShowNextPageButton="False" ShowPreviousPageButton="True" />
                            <asp:NumericPagerField ButtonCount="10" />
                            <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" NextPageText=">" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                        </Fields>
</asp:DataPager>

<asp:CustomValidator Id="cvAtLeastOneItem" runat="server" Display="None" ErrorMessage="<%$ Resources:GUIStrings, RMA_AtleastOneItemShouldBeSelected %>" ValidationGroup="vgInitiation" 
    ClientValidationFunction="AtLeastOneItemIsSelected" ></asp:CustomValidator>
<asp:ValidationSummary ID="vInitSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
    ValidationGroup="vgInitiation" />
<asp:ValidationSummary ID="vCompSummary" runat="server" ShowMessageBox="true" ShowSummary="false"
    ValidationGroup="vgReceiver" />

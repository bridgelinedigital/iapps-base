﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MultiSiteDropDown.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.MultiSiteDropDown" %>
<asp:PlaceHolder ID="phMultiSites" runat="server">
    <div class="form-row multi-site">
        <label class="form-label"><%= GUIStrings.SharedSites %></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlSiteIds" OnSelectedIndexChanged="ddlSiteIds_OnSelectedIndexChanged"
                ClientIDMode="Static" Width="130" runat="server" EnableViewState="true" />
        </div>
    </div>
</asp:PlaceHolder>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportDateRange.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.Reporting.ReportDateRange" %>
<script type="text/javascript">
    var iframeObject = null;
    function CalendarFrom_OnChange(sender, eventArgs) {

        var selectedDate = <%=CalendarFrom.ClientID%>.getSelectedDate();
        if (selectedDate > iAppsCurrentLocalDate)
        {
            alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, DateShouldNotExceedTodaysDate %>' />");
            selectedDate = iAppsCurrentLocalDate;       
        }
        else
        {
            document.getElementById("<%=txtFromDate.ClientID%>").value = <%=CalendarFrom.ClientID%>.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
            document.getElementById("<%=hdnFromDate.ClientID%>").value = <%=CalendarFrom.ClientID%>.formatDate(selectedDate, calendarDateFormat);  
        }
    }
    function CalendarTo_OnChange(sender, eventArgs) {
   
        var selectedDate = <%=CalendarTo.ClientID%>.getSelectedDate();
        if (selectedDate > iAppsCurrentLocalDate)
         {
             alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, EndDateShouldNotExceedTodaysDate %>' />");
             selectedDate = iAppsCurrentLocalDate;       
         }
        document.getElementById("<%=txtToDate.ClientID%>").value = <%=CalendarTo.ClientID%>.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
        document.getElementById("<%=hdnToDate.ClientID%>").value = <%=CalendarTo.ClientID%>.formatDate(selectedDate, calendarDateFormat);  
    }
    function popUpFromCalendar(textBoxId, hiddenFieldId) {
        if(document.getElementById("<%=txtFromDate.ClientID%>").disabled == false)
        {
            var thisDate = iAppsCurrentLocalDate;
            var textBoxObject = document.getElementById(textBoxId);
            var hiddenFieldObject = document.getElementById(hiddenFieldId);
            if(Trim(textBoxObject.value) == "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, FromDate %>' />" || Trim(textBoxObject.value) == "")
            {
                textBoxObject.value = "";
            }
            if(textBoxObject.value != "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, FromDate %>' />" && Trim(textBoxObject.value) != "" )
            {
                 ValidatorEnable(document.getElementById("<%= valFromDateFormat.ClientID %>"), true);
                if(document.getElementById("<%= valFromDateFormat.ClientID %>").isvalid)// available in validation.js 
                {
                    thisDate = new Date(hiddenFieldObject.value);  
                    <%=CalendarFrom.ClientID%>.setSelectedDate(thisDate);  
                }
            }
            else
            {
               //<%=CalendarFrom.ClientID%>.setSelectedDate(thisDate);
            }
            if(!(<%=CalendarFrom.ClientID%>.get_popUpShowing())); 
                <%=CalendarFrom.ClientID%>.show();
        }
    }
    function popUpToCalendar(textBoxId, hiddenFieldId)
    {
        if(document.getElementById("<%=txtToDate.ClientID%>").disabled == false)
        {
            var thisDate = iAppsCurrentLocalDate;
            var textBoxObject = document.getElementById(textBoxId);
            var hiddenFieldObject = document.getElementById(hiddenFieldId);
            if(Trim(textBoxObject.value) == "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ToDate %>' />" || Trim(textBoxObject.value) == "")
            {
                textBoxObject.value = "";
            }
            if(textBoxObject.value != "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ToDate %>' />"  && Trim(textBoxObject.value) != "")
            {
                 ValidatorEnable(document.getElementById("<%= valToDateFormat.ClientID %>"), true);

                if(document.getElementById("<%= valToDateFormat.ClientID %>").isvalid)// available in validation.js 
                {
                    thisDate = new Date(hiddenFieldObject.value);    
                    <%=CalendarTo.ClientID%>.setSelectedDate(thisDate);
                }
            }
            else
            {
                //<%=CalendarTo.ClientID%>.setSelectedDate(thisDate);
            }
            if(!(<%=CalendarTo.ClientID%>.get_popUpShowing())); 
                <%=CalendarTo.ClientID%>.show();
        }
    }
    function ShowDateRange()
    {
        var destinationObject = document.getElementById("dateRangeSelector");
        var sourceObject = document.getElementById("lblRangerSelector");
        if(destinationObject.style.display == "none" || destinationObject.style.display == "")
        {
            destinationObject.style.display = "block";
            setPosition(destinationObject, sourceObject);
            if(document.getElementById('<%=rbSelectRange.ClientID%>').checked == true)
            {
                disableFields('custom');
            }
            else
            {
                disableFields('predefined');
            }
        }
        else
        {
            destinationObject.style.display = "none";
        }
    }
    function setPosition(destObj, srcObj)
    {
        var popupWidth = $(destObj).outerWidth();
        var selectorOffset = $(srcObj).parent().offset();
        var selectorHeight = $(srcObj).parent().outerHeight();
        var selectorWidth = $(srcObj).parent().outerWidth();
        $(destObj).css('top', (selectorOffset.top + selectorHeight));
        $(destObj).css('left', (selectorOffset.left + selectorWidth - popupWidth));
    } 
    function disableFields(fieldText) 
    {
        switch(fieldText)
        {
            case 'predefined':
                document.getElementById("<%=txtFromDate.ClientID%>").disabled = true;
                document.getElementById("<%=txtToDate.ClientID%>").disabled = true;
                document.getElementById("<%=txtFromDate.ClientID%>").value = "From Date";
                document.getElementById("<%=txtToDate.ClientID%>").value = "To Date";
                document.getElementById("<%=ddlPredefindedRange.ClientID%>").disabled = false;

                 ValidatorEnable(document.getElementById("<%= valFromDateRequired.ClientID %>"), false);
                  ValidatorEnable(document.getElementById("<%= valToDateRequired.ClientID %>"), false);

                   ValidatorEnable(document.getElementById("<%= valFromDateFormat.ClientID %>"), false);
                  ValidatorEnable(document.getElementById("<%= valToDateFormat.ClientID %>"), false);

                  
                   ValidatorEnable(document.getElementById("<%= valFromDateLaterThanToday.ClientID %>"), false);
                  ValidatorEnable(document.getElementById("<%= valToDateLaterThanToday.ClientID %>"), false);
                   ValidatorEnable(document.getElementById("<%= valFromDateLaterThanToDate.ClientID %>"), false);
                break;
            case 'custom':
                document.getElementById("<%=txtFromDate.ClientID%>").disabled = false;
                document.getElementById("<%=txtToDate.ClientID%>").disabled = false;
                if (document.getElementById("<%=txtFromDate.ClientID%>").value == "From Date") {
                    document.getElementById("<%=txtFromDate.ClientID%>").value = CalendarFrom.formatDate(iAppsCurrentLocalDate, shortCultureDateFormat);
                    document.getElementById("<%=hdnFromDate.ClientID%>").value = <%=CalendarFrom.ClientID%>.formatDate(iAppsCurrentLocalDate, calendarDateFormat);  
                    document.getElementById("<%=txtToDate.ClientID%>").value = CalendarTo.formatDate(iAppsCurrentLocalDate, shortCultureDateFormat);
                    document.getElementById("<%=hdnToDate.ClientID%>").value = <%=CalendarTo.ClientID%>.formatDate(iAppsCurrentLocalDate, calendarDateFormat);  
                }
                document.getElementById("<%=ddlPredefindedRange.ClientID%>").disabled = true;
                 ValidatorEnable(document.getElementById("<%= valFromDateRequired.ClientID %>"), true);
                  ValidatorEnable(document.getElementById("<%= valToDateRequired.ClientID %>"), true);

                   ValidatorEnable(document.getElementById("<%= valFromDateFormat.ClientID %>"), true);
                  ValidatorEnable(document.getElementById("<%= valToDateFormat.ClientID %>"), true);

                   ValidatorEnable(document.getElementById("<%= valFromDateLaterThanToday.ClientID %>"), true);
                  ValidatorEnable(document.getElementById("<%= valToDateLaterThanToday.ClientID %>"), true);

                   document.getElementById("<%= valFromDateLaterThanToday.ClientID %>").valuetocompare = iAppsCurrentLocalDate;
                    document.getElementById("<%= valToDateLaterThanToday.ClientID %>").valuetocompare = iAppsCurrentLocalDate;

                   ValidatorEnable(document.getElementById("<%= valFromDateLaterThanToDate.ClientID %>"), true);
                 break;
        }
    }
</script>
<div class="reporting-range-container">
    <div class="reporting-range">
        <asp:Label ID="lblReportingHeading" runat="server" Text="<%$ Resources:GUIStrings, ReportingPeriod %>" />
        <asp:Label ID="startPeriod" runat="server" />&nbsp;-&nbsp;<asp:Label ID="endPeriod"
            runat="server" />
    </div>
    <asp:Label ID="lblRangerSelector" runat="server" onclick="ShowDateRange();" Text="<%$ Resources:GUIStrings, Change %>" ClientIDMode="Static" CssClass="reporting-range-selector" />
</div>
<div id="dateRangeContainer">
    <div class="date-range-popup" id="dateRangeSelector">
        <asp:ValidationSummary runat="server" ID="valSummary" EnableClientScript="true" ShowMessageBox="true"
            ValidationGroup="grpDateValidate" ShowSummary="false" />
        <div class="form-row">
            <asp:RadioButton ID="rbPredefindedRange" runat="server" Checked="true" GroupName="DateRange"
                onclick="disableFields('predefined');" Text="<%$ Resources:GUIStrings, PredefinedRanges %>" />
        </div>
        <div class="form-row">
            <asp:DropDownList ID="ddlPredefindedRange" runat="server" Width="205">
                <asp:ListItem Text="<%$ Resources:GUIStrings, CurrentDay %>" Value="CurrentDay"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:GUIStrings, CurrentWeek %>" Value="CurrentWeek"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:GUIStrings, MonthToDate %>" Value="MonthToDate"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:GUIStrings, QuarterToDate %>" Value="QuarterToDate"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:GUIStrings, YearToDate %>" Value="YearToDate"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:GUIStrings, Last30Days %>" Value="Last30days"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:GUIStrings, Last60Days %>" Value="Last60days"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:GUIStrings, Last180Days %>" Value="Last180days"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:GUIStrings, Last365Days %>" Value="Last365days"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-row">
            <asp:RadioButton ID="rbSelectRange" runat="server" GroupName="DateRange" onclick="disableFields('custom');" Text="<%$ Resources:GUIStrings, SelectDateRange %>" />
        </div>
        <div class="form-row">
            <div class="form-value calendar-value">
                <span>
                <asp:TextBox ID="txtFromDate" runat="server" Text="<%$ Resources:GUIStrings, FromDate %>" CssClass="textbox" Width="183" Enabled="false"></asp:TextBox>
                <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, FromDate %>"
                    ToolTip="<%$ Resources:GUIStrings, SelectFromDate %>" ID="imgFromDate" runat="server" />
                <ComponentArt:Calendar runat="server" ID="CalendarFrom" SkinID="Default">
                    <ClientEvents>
                        <SelectionChanged EventHandler="CalendarFrom_OnChange" />
                    </ClientEvents>
                </ComponentArt:Calendar>
                </span>
                <asp:HiddenField ID="hdnFromDate" runat="server" />
                <asp:RequiredFieldValidator ID="valFromDateRequired" runat="server" ControlToValidate="txtFromDate" Display="None"
                    ValidationGroup="grpDateValidate" ErrorMessage="<%$ Resources:GUIStrings, FromDateIsRequired %>"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="valFromDateFormat" runat="server" ControlToValidate="txtFromDate"
                    Operator="DataTypeCheck" Type="Date" CultureInvariantValues="true" ValidationGroup="grpDateValidate"
                    ErrorMessage="<%$ Resources:GUIStrings, FromDateFormatIsNotCorrect %>" Display="None"
                    Text="">
                </asp:CompareValidator>
                <asp:CompareValidator ID="valFromDateLaterThanToday" runat="server" ControlToValidate="txtFromDate"
                    Operator="GreaterThan" Type="String" CultureInvariantValues="true" ValidationGroup="grpDateValidate"
                    ErrorMessage="<%$ Resources:GUIStrings, FromDateCanNotBeLaterThanToday %>"
                    Display="None" Text="">
                </asp:CompareValidator>
            </div>
        </div>
        <div class="form-row">
            <div class="form-value calendar-value">
                <span>
                    <asp:TextBox ID="txtToDate" runat="server" Text="<%$ Resources:GUIStrings, ToDate %>"
                        CssClass="textbox" Width="183" Enabled="false"></asp:TextBox>
                    <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, ToDate %>"
                        ToolTip="<%$ Resources:GUIStrings, SelectToDate %>" ID="imgToDate" runat="server" />
                    <ComponentArt:Calendar runat="server" ID="CalendarTo" SkinID="Default">
                        <ClientEvents>
                            <SelectionChanged EventHandler="CalendarTo_OnChange" />
                        </ClientEvents>
                    </ComponentArt:Calendar>
                </span>
                <asp:HiddenField ID="hdnToDate" runat="server" />
                <asp:RequiredFieldValidator ID="valToDateRequired" runat="server" ControlToValidate="txtFromDate" Display="None"
                    ValidationGroup="grpDateValidate" ErrorMessage="<%$ Resources:GUIStrings, ToDateIsRequired %>"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="valToDateFormat" runat="server" ControlToValidate="txtToDate"
                    Operator="DataTypeCheck" Type="Date" CultureInvariantValues="true" ValidationGroup="grpDateValidate"
                    ErrorMessage="<%$ Resources:GUIStrings, ToDateFormatIsNotCorrect %>" Display="None"
                    Text="">
                </asp:CompareValidator>
                <asp:CompareValidator ID="valToDateLaterThanToday" runat="server" ControlToValidate="txtToDate"
                    Operator="GreaterThan" Type="String" CultureInvariantValues="true" ValidationGroup="grpDateValidate"
                    ErrorMessage="<%$ Resources:GUIStrings, ToDateCanNotBeLaterThanToday %>" Display="None"
                    Text="">
                </asp:CompareValidator>
                <asp:CompareValidator ID="valFromDateLaterThanToDate" runat="server" ControlToValidate="txtToDate"
                    ControlToCompare="txtFromDate" Operator="GreaterThanEqual" Type="Date" CultureInvariantValues="true"
                    ValidationGroup="grpDateValidate" ErrorMessage="<%$ Resources:GUIStrings, ToDateCanNotEarlierThanFromDate %>"
                    Display="None" Text="">
                </asp:CompareValidator>
            </div>
        </div>
        <asp:CheckBox ID="chkIncludeChildrenSitesReport" runat="server" Text="Include Children Sites"  />
        <div class="form-row" style="text-align: right;">
            
            
            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Submit %>"
                ToolTip="<%$ Resources:GUIStrings, Submit %>" OnClick="btnSubmit_Click" ValidationGroup="grpDateValidate" />
        </div>
        <asp:HiddenField ID="hdTodaysDate" runat="server" />
    </div>
</div>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopSalesCategory.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.TopSalesCategory" %>
<ComponentArt:Grid SkinID="Default" ID="grdTopSalesCategory" Width="100%" runat="server"
    RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
    AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" Sort="SaleTotal desc"
    CallbackCachingEnabled="false" SearchOnKeyPress="false" ManualPaging="true" 
    LoadingPanelClientTemplateId="grdTopSalesCategoryLoadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="NavId" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
            HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
            HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
            SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
            AllowSorting="true">
            <Columns>
                <ComponentArt:GridColumn Width="500" HeadingText="<%$ Resources:GUIStrings, NavigationCategory %>"
                    DataField="NavigationCategory" DataCellServerTemplateId="NavigationHoverTemplate"
                    IsSearchable="true" TextWrap="true" AllowReordering="false" FixedWidth="true"
                    AllowSorting="Inherit" />
                <ComponentArt:GridColumn Width="80" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                    DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="OrdersHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                    FormatString="N0" />
                <ComponentArt:GridColumn Width="80" HeadingText="<%$ Resources:GUIStrings, Quantity %>"
                    DataField="Quantity" Align="Right" DataCellClientTemplateId="QtyViewsHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="Inherit"
                    FormatString="N" />
                <ComponentArt:GridColumn Width="90" HeadingText="<%$ Resources:GUIStrings, PTotalQty %>"
                    DataField="PercentOfQuantity" Align="Right" DataCellClientTemplateId="QtyPercentHoverTemplate"
                    IsSearchable="true" Visible="false" AllowReordering="false" FixedWidth="true"
                    AllowSorting="False" FormatString="N" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, NetTotal %>"
                    DataField="SaleTotal" Align="Right" DataCellClientTemplateId="TotlaHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="Inherit"
                    FormatString="c" />
                <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, PTotalM %>"
                    DataField="PercentOfSales" Align="Right" DataCellClientTemplateId="TotalPercentHoverTemplate"
                    IsSearchable="true" Visible="false" AllowReordering="false" FixedWidth="true"
                    AllowSorting="False" FormatString="N" />
                <ComponentArt:GridColumn DataField="NavId" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ServerTemplates>
        <ComponentArt:GridServerTemplate ID="NavigationHoverTemplate">
            <Template>
                <a href="~/Reporting/Sales/SalesByProduct.aspx" runat="server" id="catLink">
                    <%# Container.DataItem["NavigationCategory"] %>
                </a>
            </Template>
        </ComponentArt:GridServerTemplate>
    </ServerTemplates>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="OrdersHoverTemplate">
            <span title="## DataItem.GetMember('NumberOfOrders').get_text() ##">## DataItem.GetMember('NumberOfOrders').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('NumberOfOrders').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="QtyViewsHoverTemplate">
            <span title="## DataItem.GetMember('Quantity').get_text() ##">## DataItem.GetMember('Quantity').get_text()
                == "" ? "0" : DataItem.GetMember('Quantity').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="QtyPercentHoverTemplate">
            <span title="## DataItem.GetMember('PercentOfQuantity').get_text() ##">## DataItem.GetMember('PercentOfQuantity').get_text()
                == "" ? "0" : DataItem.GetMember('PercentOfQuantity').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TotlaHoverTemplate">
            <span title="## DataItem.GetMember('SaleTotal').get_text() ##">## DataItem.GetMember('SaleTotal').get_text()
                == "" ? "0" : DataItem.GetMember('SaleTotal').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TotalPercentHoverTemplate">
            <span title="## DataItem.GetMember('PercentOfSales').get_text() ##">## DataItem.GetMember('PercentOfSales').get_text()
                == "" ? "0" : DataItem.GetMember('PercentOfSales').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdTopSalesCategoryLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdTopSalesCategory) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>

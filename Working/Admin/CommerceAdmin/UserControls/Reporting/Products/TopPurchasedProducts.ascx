﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopPurchasedProducts.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.TopPurchasedProducts" %>
<script type="text/javascript">
    function CreateTopPurchasedProductTooltip(dataItemObject) {
        var navigationHierarchy = dataItemObject.GetMember('NavigationCategories').Text;
        var strHTML = SplitNavCategory(navigationHierarchy);
        strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'');positiontip(event);\" onmouseout='hideddrivetip();' src='" + tooltipIconSrc + "' />";
        return strHTML;
    }
    //method to return the first category
    function FirstCategory(strNavCategories) {
        var arrNavigtaion = strNavCategories.split(',');
        var strHTML = "";
        if (arrNavigtaion.length > 0)
            strHTML = arrNavigtaion[0];
        return strHTML;
    }
</script>
<ComponentArt:Grid SkinID="Default" ID="grdTopPurchasedProducts" runat="server" RunningMode="Callback"
    Width="100%" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" AutoAdjustPageSize="true"
    AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false" SearchOnKeyPress="false"
    ManualPaging="true" Sort="NetTotal desc" LoadingPanelClientTemplateId="grdTopPurchasedProductsLoadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="ProductId" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
            HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
            HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
            SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
            AllowSorting="true">
            <Columns>
                <ComponentArt:GridColumn Width="250" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                    DataField="ProductSKUName" DataCellClientTemplateId="NameHoverTemplate" IsSearchable="true"
                    TextWrap="true" AllowReordering="false" FixedWidth="true" AllowSorting="Inherit" />
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, NavigationCategory %>"
                    DataField="NavigationCategories" DataCellClientTemplateId="NavigationHoverTemplate"
                    IsSearchable="true" TextWrap="true" AllowReordering="false" FixedWidth="true"
                    AllowSorting="False" />
                <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, Quantity %>"
                    DataField="Quantity" Align="Right" DataCellClientTemplateId="QtyViewsHoverTemplate"
                    IsSearchable="true" Visible="false" AllowReordering="false" FixedWidth="true"
                    AllowSorting="Inherit" FormatString="N" />
                <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, BundleQuantity %>"
                    DataField="BundleQuantity" Align="Right" DataCellClientTemplateId="BundleQtyViewsHoverTemplate"
                    IsSearchable="true" Visible="false" AllowReordering="false" FixedWidth="true"
                    AllowSorting="False" FormatString="N" />
                <ComponentArt:GridColumn Width="75" HeadingText="<%$ Resources:GUIStrings, PTotalQty %>"
                    DataField="PercentOfQuantity" Align="Right" DataCellClientTemplateId="QtyPercentHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False" Visible="true"
                    FormatString="N" />
             
                 <ComponentArt:GridColumn  Width="50"   HeadingText="<%$ Resources:GUIStrings, ItemTotal %>"   Visible="false"
                    DataField="ItemTotal" Align="Right" DataCellClientTemplateId="ItemTotalHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="Inherit"
                    FormatString="c" />

                     

                   <ComponentArt:GridColumn  Width="75"  HeadingText="<%$ Resources:GUIStrings, NetTotal %>" Visible="true"
                    DataField="NetTotal" Align="Right" DataCellClientTemplateId="NetTotalHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="Inherit"
                    FormatString="c" />


                <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, PTotalM %>"
                    DataField="PercentOfSales" Align="Right" DataCellClientTemplateId="TotalPercentHoverTemplate"
                    IsSearchable="true" Visible="false" AllowReordering="false" FixedWidth="true"
                    AllowSorting="False" FormatString="N" />
                <%-- This column is used in Orders by Product --%>
                <ComponentArt:GridColumn Width="80" HeadingText="<%$ Resources:GUIStrings, NumOfOrders %>"
                    DataField="NumberOfOrders" Align="Right" DataCellClientTemplateId="NoOfOrdersHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                    FormatString="N" Visible="false" />
                <ComponentArt:GridColumn Width="45" HeadingText="<%$ Resources:GUIStrings, Details %>"
                    DataField="NavigationCategories" Align="Center" DataCellClientTemplateId="DetailsHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False" />
                <ComponentArt:GridColumn DataField="ProductId" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="NameHoverTemplate">
            <span title="## DataItem.GetMember('ProductSKUName').get_text() ##">## DataItem.GetMember('ProductSKUName').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ProductSKUName').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="NavigationHoverTemplate">
            <span title="## FirstCategory(DataItem.GetMember('NavigationCategories').get_text()) ##">
                ## DataItem.GetMember('NavigationCategories').get_text() == "" ? "&nbsp;" : FirstCategory(DataItem.GetMember('NavigationCategories').get_text())
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="QtyViewsHoverTemplate">
            <span title="## DataItem.GetMember('Quantity').get_text() ##">## DataItem.GetMember('Quantity').get_text()
                == "" ? "0" : DataItem.GetMember('Quantity').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BundleQtyViewsHoverTemplate">
            <span title="## DataItem.GetMember('BundleQuantity').get_text() ##">## DataItem.GetMember('BundleQuantity').get_text()
                == "" ? "0" : DataItem.GetMember('BundleQuantity').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="QtyPercentHoverTemplate">
            <span title="## DataItem.GetMember('PercentOfQuantity').get_text() ##">## DataItem.GetMember('PercentOfQuantity').get_text()
                == "" ? "0" : DataItem.GetMember('PercentOfQuantity').get_text() ##</span>
        </ComponentArt:ClientTemplate>

        <ComponentArt:ClientTemplate ID="ItemTotalHoverTemplate">
            <span title="## DataItem.GetMember('ItemTotal').get_text() ##">## DataItem.GetMember('ItemTotal').get_text()
                == "" ? "0" : DataItem.GetMember('ItemTotal').get_text() ##</span>
        </ComponentArt:ClientTemplate>

          <ComponentArt:ClientTemplate ID="NetTotalHoverTemplate">
            <span title="## DataItem.GetMember('NetTotal').get_text() ##">## DataItem.GetMember('NetTotal').get_text()
                == "" ? "0" : DataItem.GetMember('NetTotal').get_text() ##</span>
        </ComponentArt:ClientTemplate>

         <ComponentArt:ClientTemplate ID="DiscountTotalHoverTemplate">
            <span title="## DataItem.GetMember('DiscountTotal').get_text() ##">## DataItem.GetMember('DiscountTotal').get_text()
                == "" ? "0" : DataItem.GetMember('DiscountTotal').get_text() ##</span>
        </ComponentArt:ClientTemplate>

       

        <ComponentArt:ClientTemplate ID="TotalPercentHoverTemplate">
            <span title="## DataItem.GetMember('PercentOfSales').get_text() ##">## DataItem.GetMember('PercentOfSales').get_text()
                == "" ? "0" : DataItem.GetMember('PercentOfSales').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="NoOfOrdersHoverTemplate">
            <span title="## DataItem.GetMember('NumberOfOrders').get_text() ##">## DataItem.GetMember('NumberOfOrders').get_text()
                == "" ? "0" : DataItem.GetMember('NumberOfOrders').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="DetailsHoverTemplate">
            ## CreateTopPurchasedProductTooltip(DataItem) ##
        </ComponentArt:ClientTemplate>
         <ComponentArt:ClientTemplate ID="grdTopPurchasedProductsLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdTopPurchasedProducts) ##
            </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
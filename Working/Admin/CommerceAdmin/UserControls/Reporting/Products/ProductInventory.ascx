﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductInventory.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductInventory" %>
<ComponentArt:Grid SkinID="Default" ID="grdProductInventory" Width="100%" runat="server"
    RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings,  NoItemsFound  %>"
    AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
    SearchOnKeyPress="false" ManualPaging="true" DataAreaCssClass="data-area"
    LoadingPanelClientTemplateId="grdProductInventoryLoadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="ProductId" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
            HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
            HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
            SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
            AllowSorting="true">
            <Columns>
                <ComponentArt:GridColumn Width="145" HeadingText="<%$ Resources:GUIStrings,  ProductName  %>"
                    DataField="ProductTitle" TextWrap="true" AllowReordering="false" FixedWidth="true"
                    AllowSorting="True" DataCellClientTemplateId="ProductTitleCT" />
                <ComponentArt:GridColumn Width="63" HeadingText="<%$ Resources:GUIStrings,  Qty  %>"
                    DataField="Quantity" Align="Right" AllowReordering="false" FixedWidth="true"
                    AllowSorting="False" />
                <ComponentArt:GridColumn DataField="SKU" Visible="false" />
                <ComponentArt:GridColumn DataField="ProductId" Visible="false" />
                <ComponentArt:GridColumn DataField="SKUId" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="ProductTitleCT">
            <a href="## jCommerceAdminSiteUrl + '/product-detail/' + DataItem.getMember('ProductId').get_text() ##">
                ## DataItem.getMember('ProductTitle').get_text() ##</a>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdProductInventoryLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdProductInventory) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>

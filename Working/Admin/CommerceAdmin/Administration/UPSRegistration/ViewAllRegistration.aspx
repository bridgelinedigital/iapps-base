﻿<%@ Page Title="iAPPS Commerce: Administration: UPS: Registration List" Language="C#"
    MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="ViewAllRegistration.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ViewAllRegistration" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="warehouse ups-registration">
        <div class="page-header clear-fix">
            <h1><%= SiteSettings.Registration %></h1>
            <asp:HyperLink ID="hplAddNewRegistration" runat="server" CssClass="primarybutton"
                Text="<%$ Resources:SiteSettings, AddNew %>"
                NavigateUrl="~/Administration/UPSRegistration/EditRegistration.aspx?Type=UserProfile" />
        </div>
        <asp:Repeater runat="server" ID="rptRegistration" OnItemDataBound="rptRegistration_ItemDataBound">
            <HeaderTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="registration-list warehouse-list">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td valign="top" width="70%">
                        <asp:Image ImageUrl="../../App_Themes/General/images/warehouse-icon.png" alt="" runat="server" ID="imgDefault" />
                        <div class="form-row">
                            <h2>
                                <asp:Literal ID="ltCompanyName" runat="server" /></h2>
                            <asp:Literal ID="ltHostingProvider" runat="server" />
                        </div>
                        <div class="form-row">
                            <asp:Literal ID="ltTitle" runat="server" />
                        </div>
                        <div class="form-row">
                            <asp:Literal ID="ltContactName" runat="server" />
                        </div>
                        <div class="form-row">
                            <asp:Literal ID="ltAddress" runat="server" />
                        </div>
                        <div class="form-row">
                            <asp:HyperLink ID="hplEditRegistration" runat="server" CssClass="small-button"
                                Text="<%$ Resources:SiteSettings, EditRegistration %>" />
                            <asp:HyperLink ID="hplAddAccount" runat="server" CssClass="small-button" Text="<%$ Resources:SiteSettings, AddNewShippingNumber1 %>" />
                            <asp:HyperLink ID="hplAuthentication" runat="server" CssClass="small-button"
                                Text="<%$ Resources:SiteSettings, AuthenticateShippingNumber %>" />
                            <asp:LinkButton ID="lbtnUpdateStatus" runat="server" CssClass="small-button"
                                Visible="false" Text="<%$ Resources:SiteSettings, Disable %>" OnClick="lbtnUpdateStatus_Click" />
                        </div>
                    </td>
                    <td valign="top" width="30%">
                        <div class="form-row">
                            <asp:Literal ID="ltShippingNumber" runat="server" />
                        </div>
                    </td>
                    <tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <asp:PlaceHolder ID="pnlNoItems" runat="server" Visible="false">
            <p><%= SiteSettings.NoItemsFound %></p>
        </asp:PlaceHolder>
        <div class="page-header clear-fix">
            <h1>Grant Access to your UPS Account</h1>
            <asp:HyperLink ID="hlOAuth" runat="server" CssClass="primarybutton" Text="Authorize" />
            <asp:PlaceHolder ID="phoAuth" runat="server" Visible="false">
                <div class="form-row">
                 <p>  Refresh Token Expiration: <asp:Literal ID="ltRefreshTokenExp" runat="server" /></p>
                 <p>   Access Token Expiration: <asp:Literal ID="ltAccessTokenExp" runat="server" /></p>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="iAPPS Commerce: Administration: UPS: Registration" Language="C#" 
    MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="EditRegistration.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.EditRegistration" StylesheetTheme="General" ClientIDMode="Static" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function ValidateZip(src, args) {
            args.IsValid = true;
            var countryDDL = document.getElementById('<%=ddlCountry.ClientID %>');

            if (countryDDL.options[countryDDL.selectedIndex].value == usId) {
                if (!args.Value.match(/^\d{5}$/)) args.IsValid = false;
            }
        }
        function ShowFirstScreen() {
            $('#divAgreement').hide();
            $('#divProvider').show();
            $('#btnBack2').hide();
            $('#btnPrint').hide();
            return false;
        }
        function ValidateAgreement() {
            if ($('#divProvider').is(":visible")) {
                if ($('#rdEndUser').prop('checked') || $('#rdHostingProvider').prop('checked')) {
                    if ($('#rdHostingProvider').prop('checked')) {
                        $('#hdnIsHostingProvider').val('True');
                        //$('#divIsDefaultProfile').hide();
                        return true;
                    }
                    else {
                        $('#btnBack2').show();    
                        $('#btnPrint').show();
                        $('#divAgreement').show();
                        $('#divProvider').hide();
                        $('#hdnIsHostingProvider').val('False');
                        //$('#divIsDefaultProfile').show();
                    }
                    return false;
                }
                else {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:SiteSettings, PleaseHostingProviderOrEndUser%>' />");
                    return false;
                }
            }
            else {
                if ($('#rdYes').prop('checked')) {
                    //$('#btnPrint').hide();
                    //$('#divAgreement').show();
                    //$('#divProvider').hide();
                    return true;
                }
                else {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:SiteSettings, PleaseApproveTheAgreement %>' />");
                    return false;
                }
            }
        }
        function HostingValidateAgreement() {
            if (Page_ClientValidate('vGroup') == true) {
                if ($('#hdnIsHostingProvider').val() == 'True') {
                    if ($('#rdHostingYes').prop('checked')) {
                        return true;
                    }
                    else {
                        alert("<asp:Localize runat='server' Text='<%$ Resources:SiteSettings, PleaseApproveTheAgreement %>' />");
                        return false;
                    }
                }
            }
            else
                return false;
        }
        function PrintHostingLicenseAgreement() {
            PopupPrint($('#divHostingLicense').text());
        }
        function PrintLicenseAgreement() {
            PopupPrint($('#txtAgreement').val());
        }
        function PopupPrint(printContent) {
            var printWindow = window.open('', 'Print', 'height=400,width=600');
            printWindow.document.write('<html><head><title>Print License</title>');
            printWindow.document.write('</head><body>');
            printWindow.document.write(printContent);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.focus();
            printWindow.window.print();
            return false;
        }
        function DisplayIsDefault() {
            if ($('#txtShippingNumber').val() == '') {
                $('#chkIsDefault').attr('disabled', 'disabled');
                $('#chkIsDefault').prop('checked',false);
            }
            else
                $('#chkIsDefault').removeAttr('disabled');
        }
        $(document).ready(function () {
            if ($('#hdnIsHostingProvider').val() == 'True') {
                $('#divIsDefaultProfile').hide();
            }
            if ($('#hdnStepId').val() == '1.back') {
                ValidateAgreement();
            }
            if ($('#hdnStepId').val() == '1.OnlyEndUser') {
                ValidateAgreement();
                //$('#btnBack2').hide(); //already done in code behind
            }
            $('#txtAgreement').scroll(function () {
                var textarea_height = $(this)[0].scrollHeight;
                var scroll_height = textarea_height - $(this).innerHeight();
                var scroll_top = $(this).scrollTop();
                if (scroll_top >= scroll_height) {
                    $('#divLicenseAgreement').removeAttr('disabled');
                    $('#rdYes').removeAttr('disabled');
                    $('#rdNo').removeAttr('disabled');
                }
            });
            if ($('#rdHostingYes').prop('checked') == false) {
                $('#divHostingProviderLicenseAgreement').prop('disabled', true);
                $('#rdHostingYes').prop('disabled', true);
                $('#rdHostingNo').prop('disabled', true);
                $('#txtHostingProviderAgreement').scroll(function () {
                    var textarea_height = $(this)[0].scrollHeight;
                    var scroll_height = textarea_height - $(this).innerHeight();
                    var scroll_top = $(this).scrollTop();
                    if (scroll_top >= scroll_height) {
                        $('#divHostingProviderLicenseAgreement').removeAttr('disabled');
                        $('#rdHostingYes').removeAttr('disabled');
                        $('#rdHostingNo').removeAttr('disabled');
                    }
                });
            }
        });
        //Create date calendar methods
        function popUpInvoiceDateCalendar(ctl) {
            var thisDate = iAppsCurrentLocalDate;
            if ($('#txtInvoiceDate').val()=='') {
                calInvoiceDate.setSelectedDate(thisDate)
            }
            if (!(calInvoiceDate.get_popUpShowing()));
            calInvoiceDate.show(ctl);
        }
        function calInvoiceDate_onSelectionChanged(sender, eventArgs) {
            var selectedDate = calInvoiceDate.getSelectedDate();
            document.getElementById("<%=txtInvoiceDate.ClientID%>").value = calInvoiceDate.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
        }
        function openPopup(url) {
            window.open(url, "popup_id", "scrollbars,resizable,width=800,height=800");
        }
        function gE(id) {
            return document.getElementById(id);
        }
        function ResetValidationFlag() {
            if (gE('<%=hfIsAddressValidated.ClientID %>') != null) {
                gE('<%=hfIsAddressValidated.ClientID %>').value = 0;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
    <div class="ups-registration">
        <div class="page-header clear-fix">
            <h1><asp:Literal ID="litHeading" runat="server" /></h1>
        </div>
        <asp:PlaceHolder ID="phStep1" runat="server">
            <div class="button-row clear-fix">
                <asp:Button ID="btnNext1" runat="server" OnClick="btnNext1_Click" Text="<%$ Resources:SiteSettings, Next %>"
                    Style="float: right;" ToolTip="<%$ Resources:SiteSettings, Next %>" CssClass="primarybutton" />
                <asp:HyperLink ID="hplCancel1" runat="server" CssClass="button" Style="float: right;"
                    Text="<%$ Resources:SiteSettings, Cancel %>" ToolTip="<%$ Resources:SiteSettings, Cancel %>"
                    NavigateUrl="ViewAllRegistration.aspx" />
            </div>
            <div class="clear-fix">
                <div class="left-column">
                    <img src="images/UPS_LOGO_L.gif" alt="UPS" />
                </div>
                <div class="right-column">
                    <p>
                        This wizard will assist you in completing the necessary licensing and registration
                        requirements to activate and use the UPS Developer Kit from this application. If
                        you do not wish to use any of the functions that utilize the UPS Developer Kit,
                        click the Cancel button and those functions will not be enabled. If, at a later
                        time, you wish to use the UPS Developer Kit, return to this section and complete
                        the UPS Developer Kit licensing and registration process.</p>
                    <p class="ups-notice"><%= GUIStrings.UPSTrademarkNotice %></p>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phStep2" runat="server">
            <div class="form-row button-row clear-fix">
                <asp:Button ID="btnNext2" runat="server" OnClick="btnNext2_Click" Text="<%$ Resources:SiteSettings, Next %>"
                    Style="float: right;" ToolTip="<%$ Resources:SiteSettings, Next %>" CssClass="primarybutton"
                    OnClientClick="return ValidateAgreement();" />
                <asp:Button ID="btnBack2" runat="server" Text="<%$ Resources:SiteSettings, Back %>"
                    Style="float: right; display:none;" ToolTip="<%$ Resources:SiteSettings, Back %>" CssClass="button" OnClientClick="return ShowFirstScreen();" />
                <asp:HyperLink ID="hplCancel2" runat="server" CssClass="button" Style="float: right;"
                    Text="<%$ Resources:SiteSettings, Cancel %>" ToolTip="<%$ Resources:SiteSettings, Cancel %>"
                    NavigateUrl="ViewAllRegistration.aspx" />
                <asp:Button ID="btnPrint" runat="server" Text="<%$ Resources:SiteSettings, Print %>"
                    Style="float: right; display:none;" ToolTip="<%$ Resources:SiteSettings, Print %>" CssClass="button" OnClientClick="return PrintLicenseAgreement();"/>
            </div>
            <div class="clear-fix">
                <div class="left-column">
                    <img src="images/UPS_LOGO_L.gif" alt="UPS" />
                </div>
                <div class="right-column">
                    <asp:PlaceHolder ID="phProvider" runat="server">
                    <div id="divProvider">
                        <p><%= SiteSettings.ThankyouforchoosingourapplicationPleaseselecttheoptionbelowthatapplies %></p>
                        <p><%=  GUIStrings.EndUserHelp %></p>
                        <p><%=  GUIStrings.HostingProviderHelp %></p>
                        <div class="form-row">
                            <div class="form-value">
                                <asp:RadioButton ID="rdEndUser" runat="server" Text="<%$ Resources:SiteSettings, EndUser %>" GroupName="HostingProvider" />
                                <asp:RadioButton ID="rdHostingProvider" runat="server" Text="<%$ Resources:SiteSettings, HostingProvider %>" GroupName="HostingProvider"/>
                            </div>
                        </div>
                    </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phAgreement" runat="server">
                    <div id="divAgreement" style="display: none;">
                        <asp:TextBox Width="790" Height="200" ID="txtAgreement" runat="server" ReadOnly="true" TextMode="MultiLine"
                            Text="" style="resize: none; padding: 0;" />
                        <div style="margin: 20px 0;">
                            DO YOU AGREE TO ACCESS THE UPS SYSTEMS IN ACCORDANCE WITH AND BE BOUND BY EACH OF
                            THE TERMS AND CONDITIONS SET FORTH ABOVE?
                        </div>
                        <div class="form-row">
                            <div class="form-value" id="divLicenseAgreement">
                                <asp:RadioButton ID="rdYes" runat="server" Text="Yes I Do" GroupName="LicenseAgreement" Enabled="false" />
                                <asp:RadioButton ID="rdNo" runat="server" Text="No I Do Not Agree" GroupName="LicenseAgreement" Enabled="false" />
                            </div>
                        </div>
                    </div>
                    </asp:PlaceHolder>
                    <p class="ups-notice"><%= GUIStrings.UPSTrademarkNotice %></p>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phStep3" runat="server">
            <div class="button-row clear-fix">
                <asp:Button ID="btnAuthenticate" runat="server" OnClick="btnAuthenticate_Click" Visible="false"
                    Text="<%$ Resources:SiteSettings, Authenticate %>" ValidationGroup="vAIAGroup"
                    Style="float: right;" ToolTip="<%$ Resources:SiteSettings, Authenticate %>"
                    CssClass="primarybutton" />
                <asp:Button ID="btnNext3" runat="server" OnClick="btnNext3_Click" Text="<%$ Resources:SiteSettings, Finish %>"
                    ValidationGroup="vGroup" Style="float: right;" ToolTip="<%$ Resources:SiteSettings, Finish %>"
                    CssClass="primarybutton" OnClientClick="return HostingValidateAgreement();" />
                <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="<%$ Resources:SiteSettings, Back %>"
                    Style="float: right;" ToolTip="<%$ Resources:SiteSettings, Back %>" CssClass="button" Visible="false"/>
                <asp:HyperLink ID="hplCancel3" runat="server" CssClass="button" Style="float: right;"
                    Text="<%$ Resources:SiteSettings, Cancel %>" ToolTip="<%$ Resources:SiteSettings, Cancel %>"
                    NavigateUrl="ViewAllRegistration.aspx" />
                <asp:Button ID="btnPrintHostingProvider" runat="server" Text="<%$ Resources:SiteSettings, Print %>"
                    Style="float: right;" ToolTip="<%$ Resources:SiteSettings, Print %>" CssClass="button"
                    Visible="false" OnClientClick="return PrintHostingLicenseAgreement();"/>
            </div>
            <div class="clear-fix">
                <div class="left-column">
                    <img src="images/UPS_LOGO_L.gif" alt="UPS" />
                </div>
                <asp:PlaceHolder runat="server" ID="phRegistration">
                    <asp:UpdatePanel ID="updStep3" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="right-column">
                                <asp:PlaceHolder runat="server" ID="phHostingProviderLicense" Visible="false">
                                    <div id="divHostingLicense">
                                        <asp:TextBox Width="790" Height="200" ID="txtHostingProviderAgreement" runat="server" ReadOnly="true" TextMode="MultiLine"
                                            Text="" />
                                    </div>
                                    <p><%= SiteSettings.DoYouAgreeStatement %></p>
                                    <p id="divHostingProviderLicenseAgreement">
                                        <asp:RadioButton ID="rdHostingYes" runat="server" Text="Yes I Do" GroupName="HostingLicenseAgreement"  />
                                        <asp:RadioButton ID="rdHostingNo" runat="server" Text="No I Do Not Agree" GroupName="HostingLicenseAgreement"  />
                                    </p>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="phAddRegistration">
                                    <asp:PlaceHolder runat="server" ID="phContactDetails">
                                        <div class="form-row">
                                            <label class="form-label">
                                                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:SiteSettings, ContactName %>" /><span
                                                    class="req">*</label>
                                            <div class="form-value">
                                                <asp:TextBox ID="txtContactName" runat="server" CssClass="textbox" Width="274" MaxLength="255"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqContactName" ControlToValidate="txtContactName"
                                                    runat="server" ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterContactName %>"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator runat="server" ID="regExContactName" ControlToValidate="txtContactName"
                                                    ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:SiteSettings, InvalidCharactersInContactName %>"
                                                    Text=""></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <label class="form-label">
                                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:SiteSettings, Title %>" /><span
                                                    class="req">*</label>
                                            <div class="form-value">
                                                <asp:TextBox ID="txtTitle" runat="server" CssClass="textbox" Width="274" MaxLength="255"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqTitle" ControlToValidate="txtTitle" runat="server"
                                                    ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterTitle %>"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator runat="server" ID="regExTitle" ControlToValidate="txtTitle"
                                                    ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:SiteSettings, InvalidCharactersInTitle %>"
                                                    Text=""></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                    <div class="form-row">
                                        <label class="form-label">
                                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:SiteSettings, CompanyName %>" /><span
                                                class="req">*</label>
                                        <div class="form-value">
                                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="textbox" Width="274" MaxLength="255"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqCompanyName" ControlToValidate="txtCompanyName"
                                                runat="server" ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterCompanyName %>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator runat="server" ID="regExCompanyName" ControlToValidate="txtCompanyName"
                                                ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:SiteSettings, InvalidCharactersInCompanyName %>"
                                                Text=""></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <label class="form-label">
                                            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:SiteSettings, StreetAddress %>" /><span
                                                class="req">*</label>
                                        <div class="form-value">
                                            <asp:TextBox ID="txtStreetAddress" runat="server" CssClass="textbox" OnChange="ResetValidationFlag()" Width="274"
                                                MaxLength="255"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqStreetAddress" ControlToValidate="txtStreetAddress"
                                                runat="server" ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterStreetAddress %>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator runat="server" ID="regExAddress" ControlToValidate="txtStreetAddress"
                                                ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:SiteSettings, InvalidCharactersInSteetAddress %>"
                                                Text=""></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <label class="form-label">
                                            <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:SiteSettings, City %>" /><span
                                                class="req">*</span></label>
                                        <div class="form-value">
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="textbox" Width="274" OnChange="ResetValidationFlag()" MaxLength="255"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtCity" ControlToValidate="txtCity" runat="server"
                                                ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheCity %>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator runat="server" ID="revtxtCity" ControlToValidate="txtCity"
                                                ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:SiteSettings, InvalidCharactersInCity %>"
                                                Text=""></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                   <asp:HiddenField ID="hfIsAddressValidated" runat="server" Value="0" />
                                </asp:PlaceHolder>
                                <div class="form-row">
                                    <label class="form-label">
                                        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:SiteSettings, Country %>" /><span
                                            class="req">*</span></label>
                                    <div class="form-value">
                                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="selectbox" Width="282"
                                            EnableViewState="true" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="reqddlCountry" ControlToValidate="ddlCountry" runat="server"
                                            ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectACountry %>"
                                            InitialValue="00000000-0000-0000-0000-000000000000"></asp:RequiredFieldValidator>
                                    </div>
                                    <div id="divCountryHref" runat="server" visible="false" >
                                        <%=SiteSettings.UPSCountryText %>
                                        <a id="hrefCountry" runat="server"  href="#" onclick="openPopup(this.href); return false;"><%=SiteSettings.UPSCountryLink %></a> 
                                    </div>
                                </div>
                                <asp:PlaceHolder ID="phCountryRelated" runat="server">
                                    <div class="form-row">
                                        <label class="form-label">
                                            <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:SiteSettings, State %>" /><span
                                                class="req">*</span></label>
                                        <div class="form-value">
                                            <asp:DropDownList ID="ddlState" OnChange="ResetValidationFlag()" runat="server" CssClass="selectbox" Width="282" EnableViewState="true"
                                                AutoPostBack="false" />
                                            <asp:TextBox runat="server" ID="txtState" CssClass="textbox" Width="274" MaxLength="255"
                                                Visible="false"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvddlState" ControlToValidate="ddlState" runat="server"
                                                ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAState %>"
                                                InitialValue="00000000-0000-0000-0000-000000000000"></asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="rvStateText" ControlToValidate="txtState" runat="server"
                                                Text="" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAState %>"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <label class="form-label">
                                            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:SiteSettings, PostalCode %>" /><span
                                                class="req">*</span></label>
                                        <div class="form-value">
                                            <asp:TextBox ID="txtZip" runat="server" Width="274" OnChange="ResetValidationFlag()" CssClass="textbox RegistrationZip" MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtZip" ControlToValidate="txtZip" runat="server"
                                                ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAZipCode %>"></asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvtxtZip" runat="server" ControlToValidate="txtZip" Text=""
                                                ValidationGroup="vGroup" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidPostalCode %>"
                                                ClientValidationFunction="ValidateZip"></asp:CustomValidator>
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="phPhone">
                                    <div class="form-row">
                                        <label class="form-label">
                                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:SiteSettings, PhoneNumber %>" /><span
                                                class="req">*</span></label>
                                        <div class="form-value">
                                            <asp:TextBox ID="txtPhoneNumber" runat="server" Width="274" CssClass="textbox" MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqPhoneNumber" ControlToValidate="txtPhoneNumber"
                                                runat="server" ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAPhoneNumber %>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator runat="server" ID="regExPhoneNumber" ControlToValidate="txtPhoneNumber"
                                                ValidationGroup="vGroup" ValidationExpression="^[a-zA-Z0-9() +-.]+$" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidPhoneNumber %>"
                                                Text=""></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <label class="form-label">
                                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:SiteSettings, EmailAddress %>" /><span
                                                class="req">*</span></label>
                                        <div class="form-value">
                                            <asp:TextBox ID="txtEmailAddress" runat="server" Width="274" CssClass="textbox" MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqEmailAddress" ControlToValidate="txtEmailAddress"
                                                runat="server" ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAEmail %>"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="phShippingNumber">
                                    <div class="form-row">
                                        <label class="form-label">
                                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:SiteSettings, ShippingNumber %>" /><span id="spReqShippingNumber" runat="server" visible="false"
                                                class="req">*</span></label>
                                        <div class="form-value">
                                            <asp:TextBox ID="txtShippingNumber" runat="server" Width="274" CssClass="textbox" onkeyup="DisplayIsDefault();"
                                                MaxLength="6"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqShippingNumber" ControlToValidate="txtShippingNumber"
                                                Enabled="false" runat="server" ValidationGroup="vGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAShippingNumber %>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator runat="server" ID="regShippingNumber" ControlToValidate="txtShippingNumber"
                                                ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:SiteSettings, InvalidCharactersInShippingNumber %>"
                                                Text=""></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="regExShippingNumber" runat="server" ErrorMessage="<%$ Resources:SiteSettings, NoOfCharactersInShippingNumber %>"
                                                ControlToValidate="txtShippingNumber" ValidationGroup="vGroup" ValidationExpression="^[a-zA-Z0-9\s]{6,}$" />
                                        </div>
                                    </div>
                                    <div class="form-row" style="display:none;">
                                        <label class="form-label">&nbsp;</label>
                                        <div class="form-value">
                                            <asp:CheckBox runat="server" ID="chkIsDefault" Text="<%$ Resources:SiteSettings, SetShippingNumberAsDefault %>" Visible="false"/>
                                        </div>
                                     </div>
                                </asp:PlaceHolder>
                                <div class="form-row" id="divIsDefaultProfile" runat="server">
                                    <label class="form-label">&nbsp;</label>
                                    <div class="form-value">
                                        <asp:CheckBox runat="server" ID="chkIsDefaultProfile" Text="<%$ Resources:SiteSettings, SetThisProfileAsDefault %>"/>
                                    </div>
                                 </div>
                                <p class="ups-notice"><%= GUIStrings.UPSTrademarkNotice %></p>
                            </div>
                            <asp:ValidationSummary runat="server" ID="vsSummary" EnableClientScript="true" ShowMessageBox="true"
                                ShowSummary="false" DisplayMode="List" ValidationGroup="vGroup" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="phAuthentication" Visible="false">
                    <div class="right-column">
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:SiteSettings, ShippingNumber %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlShippingNumber" runat="server" CssClass="selectbox" Width="282"
                                    EnableViewState="true" AutoPostBack="false" />
                                <asp:RequiredFieldValidator ID="reqSelectShippingNumber" ControlToValidate="ddlShippingNumber"
                                    runat="server" ValidationGroup="vAIAGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseSelectAShippingNumber %>"
                                    InitialValue=""></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-row" style="display:none;">
                            <label class="form-label">&nbsp;</label>
                            <div class="form-value">
                                <asp:CheckBox runat="server" ID="chkIsDefaultShippingNumber" Text="<%$ Resources:SiteSettings, SetShippingNumberAsDefault %>" Visible="false"/>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:SiteSettings, InvoiceDate %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value calendar-value">
                                <asp:TextBox ID="txtInvoiceDate" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
                                <span>
                                    <img id="InvoiceDate_button" class="buttonCalendar" alt="<%= GUIStrings.OpenCalendar %>"
                                        src="../../App_Themes/General/images/calendar-button.png" onclick="popUpInvoiceDateCalendar(this);" />
                                </span>
                                <ComponentArt:Calendar runat="server" SkinID="Default" PickerFormat="Custom" ID="calInvoiceDate"
                                    MaxDate="9998-12-31" PopUpExpandControlId="InvoiceDate_button">
                                    <ClientEvents>
                                        <SelectionChanged EventHandler="calInvoiceDate_onSelectionChanged" />
                                    </ClientEvents>
                                </ComponentArt:Calendar>
                                <asp:RequiredFieldValidator ID="valInvoiceDateRequired" runat="server" ControlToValidate="txtInvoiceDate"
                                    ValidationGroup="vAIAGroup" ErrorMessage="<%$ Resources:SiteSettings, InvoiceDateShouldNotBeEmpty %>"
                                    Display="None" Text="*"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="valInvoiceDateFormat" runat="server" ControlToValidate="txtInvoiceDate"
                                    Operator="DataTypeCheck" Type="Date" CultureInvariantValues="true" ValidationGroup="vAIAGroup"
                                    ErrorMessage="<%$ Resources:SiteSettings, InvoiceDateIsNotInCorrectFormat %>" Display="None"
                                    Text="*">
                                </asp:CompareValidator>
                                <asp:CompareValidator ID="valInvoiceDateLaterThanToday" runat="server" ControlToValidate="txtInvoiceDate"
                                    Operator="LessThanEqual" Type="Date" CultureInvariantValues="true" ValidationGroup="vAIAGroup"
                                    ErrorMessage="<%$ Resources:SiteSettings, InvoiceDateShouldBeLessThanToday %>"
                                    Display="None" Text="*">
                                </asp:CompareValidator>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:SiteSettings, InvoiceNumber %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtInvoiceNumber" runat="server" Width="274" CssClass="textbox"
                                    MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqInvoiceNumber" ControlToValidate="txtInvoiceNumber"
                                    runat="server" ValidationGroup="vAIAGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterInvoiceNumber %>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator runat="server" ID="reqExInvoiceNumber" ControlToValidate="txtInvoiceNumber"
                                    ValidationGroup="vAIAGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:SiteSettings, InvalidCharactersInInvoiceNumber %>"
                                    Text=""></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:SiteSettings, InvoiceAmount %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtInvoiceAmount" runat="server" Width="274" CssClass="textbox"
                                    MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqInvoiceAmount" ControlToValidate="txtInvoiceAmount"
                                    runat="server" ValidationGroup="vAIAGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterInvoiceAmount %>"></asp:RequiredFieldValidator>
                                <asp:CompareValidator CultureInvariantValues="false" runat="server" ID="rgtxtInvoiceAmount"
                                    Display="None" ControlToValidate="txtInvoiceAmount" Text="*" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterTheValidInvoiceAmount %>"
                                    Type="Currency" Operator="DataTypeCheck" ValidationGroup="vAIAGroup" ></asp:CompareValidator>
                                <asp:CompareValidator ID="valInvoiceAmount" runat="server" ControlToValidate="txtInvoiceAmount"
                                    ValueToCompare="0" Operator="GreaterThan" Type="Double" CultureInvariantValues="false"
                                    ValidationGroup="vAIAGroup" ErrorMessage="<%$ Resources:SiteSettings, InvoiceAmountShouldBeGreaterThanZero %>"
                                    Display="None" Text="">
                                </asp:CompareValidator>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:SiteSettings, InvoiceControlID %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtInvoiceControlID" runat="server" Width="274" CssClass="textbox"
                                    MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqInvoiceControlID" ControlToValidate="txtInvoiceControlID" Enabled="false"
                                    runat="server" ValidationGroup="vAIAGroup" Text="" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterInvoiceControlID %>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator runat="server" ID="regExInvoiceControlID" ControlToValidate="txtInvoiceControlID"
                                    ValidationGroup="vAIAGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:SiteSettings, InvalidCharactersInInvoiceControlID %>"
                                    Text=""></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <p class="ups-notice"><%= GUIStrings.UPSTrademarkNotice %></p>
                    </div>
                    <asp:ValidationSummary runat="server" ID="vAIAGroupSummary" EnableClientScript="true"
                        ShowMessageBox="true" ShowSummary="false" DisplayMode="List" ValidationGroup="vAIAGroup" />
                </asp:PlaceHolder>
            </div>
        </asp:PlaceHolder>
    </div>
    <asp:HiddenField ID="hdnStepId" runat="server" Value="1" />
    <asp:HiddenField ID="hdnUserId" runat="server" />
    <asp:HiddenField ID="hdnShippingAccountId" runat="server" />
    <asp:HiddenField ID="hdnIsHostingProvider" runat="server" />
    <asp:HiddenField ID="hdnHostingProviderId" runat="server" />
</asp:Content>

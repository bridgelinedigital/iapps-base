
var emptyGuid = '00000000-0000-0000-0000-000000000000';
//Special Character Validation
function ValidationSpecailCharacterFunction(stringToValidate,validationKey,controlName)
{        
    var regularEval = new RegExp("[\<\>!\"#$%&'()*+,-./:;?@[\\\]_`{|}~]","i");
    var blnReportValidation = regularEval.test(stringToValidate);
    if (blnReportValidation)
    {
        message=new String(GetMessage(validationKey)); 
        message=message.replace("{0}",controlName);       
        alert(message);
        return false;         
    }  
    else  
    {
        return true;
    }
    
}
//Required Field Validation
function ValidationRequiredFunction(stringToValidate,validationKey)
{
    if(Trim(stringToValidate)=="" || stringToValidate==null)
    {
        alert(GetMessage(validationKey));
        return false;
    }
    else
    {
        return true;
    }
    
}

//Method for validating an email address
function ValidateEmailFunction(eMail) {
    //return /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/.test(eMail);
    return /^(\w+\.)*([\w-]+)@([\w-]+\.)+([a-zA-Z]{2,4})$/.test(eMail);

}

// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert(__JSMessages["TheDateFormatShouldBemmddyyyy"])
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert(__JSMessages["PleaseEnterAValidMonth"])
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert(__JSMessages["PleaseEnterAValidDay"])
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert(stringformat(__JSMessages["PleaseEnterAValid4DigitYearBetween0And1"], minYear, maxYear))
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert(__JSMessages["PleaseEnterAValidDate"])
		return false
	}
return true
}

function isUnicodeCharacter(text, msgTitle) {

    if (text.indexOf('&#0;') >= 0) {
        alert(stringformat(__JSMessages["IsTheUnicodeCombinationForANewLineCharacterItIsNotAllowedIn0"], msgTitle) + " \n");
        return true;
    }
    return false;
}
//  check for valid numeric strings	
function IsNumeric(strString) {
    var strValidChars = "0123456789.";
    var strChar;
    var blnResult = true;

    if (strString.length == 0) return false;

    //  test strString consists of valid characters listed above
    for (i = 0; i < strString.length && blnResult == true; i++) {
        strChar = strString.charAt(i);
        if (strValidChars.indexOf(strChar) == -1) {
            blnResult = false;
        }
    }
    return blnResult;
}
//generic validation

function checkFields(form,fldName,fldMsg)
{
	var fldRef,fldType;
	
	for (y=0; y < fldName.length; y++)
	{
		if (fldName[y] != "")
		{
			fldRef = form.elements[fldName[y]];
			
			if (fldRef)
			{
				//check for text or textarea field
				if (fldRef.type == "text" || fldRef.type == "textarea")
				{
					if (fldRef.value == "")
					{
						alert("Please enter " + fldMsg[y] + ".");
						fldRef.focus();
						return false;
					} else if ((fldRef.name == "email") && (! isEmail(fldRef.value))) {
						alert("Invalid Email Address");
						fldRef.focus();
						return false;
					}
				}
				//check for select-one or select-multiple field
				else if ((fldRef.type == "select-one") || (fldRef.type == "select-multiple"))
				{
					if (fldRef.options[fldRef.selectedIndex].value == "")
					{
						alert("Please select " + fldMsg[y] + ".");
						fldRef.focus();
						return false;
					}
				}
				//check for radio buttons
				else if (fldRef[0].type == "radio")
				{
					var optChecked = false;
					for (i=0; i<fldRef.length;i++)
					{
						if (fldRef[i].checked)
						{
							optChecked = true;
						}
					}
					if (!optChecked)
					{
						alert("Please specify " + fldMsg[y] + ".");
						fldRef[0].focus();
						return false;
					}
				}
				//check for password field
				if (fldRef.type == "password")
				{
					if (fldRef.value == "")
					{
						alert("Please enter " + fldMsg[y] + ".");
						fldRef.focus();
						return false;
					}
				}
			}
		}
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////
// BEGIN date validation functions
/////////////////////////////////////////////////////////////////////////
function validate_date(date_field, desc) {
        if (!date_field.value)  
                return true;
        var in_date = stripCharString(date_field.value," ");
        in_date = in_date.toUpperCase();
        var date_is_bad = 0;  
       // if (!allowInString(in_date,"/0123456789T+-"))
		if (!allowInString(in_date,"/0123456789"))
                date_is_bad = 1; // invalid characters in date
        if (!date_is_bad) { 
                var has_rdi = 0;
                if (in_date.indexOf("T") >= 0){ 
                        has_rdi = 1;
                }
                if (!date_is_bad && has_rdi && (in_date.indexOf("T") != 0)) { 
                        date_is_bad = 2; // relative date index character is not in first position
                }
                if (!date_is_bad && has_rdi && (in_date.length == 1)) { 
                        var d = new Date();
						var return_month = parseInt(d.getMonth() + 1).toString();
						//return_month = (return_month.length==1 ? "0" : "") + return_month; 
						var return_date =  parseInt(d.getDate()).toString();
						//return_date = (return_date.length==1 ? "0" : "") + return_date; 
				        in_date = return_month + "/" + return_date + "/" + get_full_year(d);		
                        has_rdi = 0; // date doesn't have rdi char anymore (will also cause failure of add'l rdi checks, which is a good thing)
                }
                if (!date_is_bad && has_rdi && (in_date.length > 1) && !(in_date.charAt(1) == "+" || in_date.charAt(1) == "-")) {
                        date_is_bad = 3; // length of rdi string is greater than 1 but second char is not "+" or "-"
                }
                if (!date_is_bad && has_rdi && isNaN(parseInt(in_date.substring(2,in_date.length),10))) {
                        date_is_bad = 4; // rdi value is not a number
                }
                if (!date_is_bad && has_rdi && (parseInt(in_date.substring(2,in_date.length),10) < 0)) {
                        date_is_bad = 5; // rdi value is not a positive integer
                }
                if (!date_is_bad && has_rdi) {
                        var d = new Date();
                        ms = d.getTime();
                        offset = parseInt(in_date.substring(2,in_date.length),10);
                        if(in_date.charAt(1) == "+") {
                                ms += (86400000 * offset);
                        } else {
                                ms -= (86400000 * offset);
                        }
                        d.setTime(ms);
						var return_month = parseInt(d.getMonth() + 1).toString();
						return_month = (return_month.length==1 ? "0" : "") + return_month; 
						var return_date =  parseInt(d.getDate()).toString();
						return_date = (return_date.length==1 ? "0" : "") + return_date; 
				        in_date = return_month + "/" + return_date + "/" + get_full_year(d);	
                        has_rdi = 0;
                }
        } 
        if (!date_is_bad) {
                var date_pieces = new Array();
                date_pieces = in_date.split("/");
                if (date_pieces.length == 2) {
                        var d = new Date();
                        in_date = in_date + "/" + get_full_year(d);
                        date_pieces = in_date.split("/");
                }
                if (date_pieces.length != 3 || parseInt(date_pieces[0],10) < 1 || parseInt(date_pieces[0],10) > 12 
                                || parseInt(date_pieces[1],10) < 1 || parseInt(date_pieces[1],10) > 31 
                                || (date_pieces[2].length != 2 && date_pieces[2].length != 4)) {
                        date_is_bad = 6;  // date is not in format of m[m]/d[d]/yy[yy]
                }
        }
        if (date_is_bad) {
                alert(desc + " must be in the format of [m]m/[d]d/yyyy.");
                date_field.focus();
                return (false);
        }
        
        var ms = Date.parse(in_date);
        var d = new Date();
        d.setTime(ms);
		var return_date = d.toLocaleString();
		var return_month = parseInt(d.getMonth() + 1).toString();
		//return_month = (return_month.length==1 ? "0" : "") + return_month; 
		var return_date =  parseInt(d.getDate()).toString();
		//return_date = (return_date.length==1 ? "0" : "") + return_date; 
        return_date = return_month + "/" + return_date + "/" + get_full_year(d);
        date_field.value = return_date;
        return true;
}       // normalize the year to yyyy

function get_full_year(d) {
		var y = "";
		var z = "";
		if (d.getFullYear() != null)
		{
			y = d.getFullYear();
			if (y < 2002) y=2002;		
		} else
		{	
	        y = d.getYear();
	        if (y > 0  && y < 100) y += 2000;
	        if (y < 1000) y += 2000;
		}
		z = y.toString();
		//return z.substring(2);
		return z;
}

function stripCharString (InString, CharString)  {
        var OutString="";
   for (var Count=0; Count < InString.length; Count++)  {
        var TempChar=InString.substring (Count, Count+1);
      var Strip = false;
      for (var Countx = 0; Countx < CharString.length; Countx++) {
        var StripThis = CharString.substring(Countx, Countx+1)
         if (TempChar == StripThis) {
                Strip = true;
            break;
         }
      }
      if (!Strip)
        OutString=OutString+TempChar;
   }
        return (OutString);
}

function allowInString (InString, RefString)  {
        if(InString.length==0) return (false);
        for (var Count=0; Count < InString.length; Count++)  {
        var TempChar= InString.substring (Count, Count+1);
      if (RefString.indexOf (TempChar, 0)==-1)  
        return (false);
   }
   return (true);
}
/////////////////////////////////////////////////////////////////////////
// END date validation functions
/////////////////////////////////////////////////////////////////////////

// format date onKeyUp - fills in date chars as user types
function dateFormat(obj,dateFldVal) 
{	
	//base year defaults to current year
	//use current year in default publish date
	var retDate = new Date();
	var retYear = retDate.getYear();
	
	//for archive date, add 10 years to publish date
	//user may change publish date, so it might not be default date
	if ((obj.name == "archivedate") && (dateFldVal != ""))
	{
		var pubDate = new Date(dateFldVal);
		var pubYear = pubDate.getYear();
		retYear = pubYear + 10;
	}
	
	if ((obj.value.length == 1)&&(obj.value > 1)) 
	{
		obj.value = obj.value + '/';
	}
	else if ((obj.value.length == 2) && (obj.value.charAt(1) != "/"))
	{
		obj.value=obj.value + '/';
	}
	else if ((obj.value.length == 3) && (obj.value.charAt(2) > 3))
	{
		obj.value=obj.value + '/' + retYear;
	}
	else if (obj.value.length == 4)
	{
		if ((obj.value.charAt(3) != "/") && (obj.value.charAt(3) > 3))
		{
			obj.value=obj.value + '/' + retYear;
		}
		else if ((obj.value.charAt(1) == "/") && (obj.value.charAt(3) != "/"))
		{
			obj.value=obj.value + '/' + retYear;
		}
		else if((obj.value.charAt(1) == "/") && (obj.value.charAt(3) == "/"))
		{
			obj.value=obj.value + retYear;
		}
	}
	else if (obj.value.length == 5)
	{
		if (obj.value.charAt(4) != "/")
		{
			obj.value=obj.value + '/' + retYear;
		}
		else
		{
			 obj.value=obj.value + retYear;
		}
	}
}

function isEmail(string) {
    if (string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
        return true;
    else
        return false;
}

function checkEmailFields(form,fldName,fldMsg){
	var fldRef,fldType;
	for (y=0; y < fldName.length; y++)
	{
		if (fldName[y] != "")
		{
			fldRef = form.elements[fldName[y]];
			if (fldRef.value=="")
			{
				alert("Please enter " + fldMsg[y] + ".");
				fldRef.focus();
				return false;
			}else{
				if (fldRef.value.length < 5)	{
					alert("Please enter valid " + fldMsg[y] + ".");
					fldRef.focus();
					return false;
				}else {
					i = fldRef.value.indexOf('.');
					j = fldRef.value.indexOf('@');
					if (i < 0 || j < 0){
						alert("Please enter valid " + fldMsg[y] + ".");
						fldRef.focus();
						return false;
					}
				}
			}
		}
	}	
	return true;
}		




/////////////////////////////////////////////////////////////////////////
// BEGIN date validation functions without alert message - Maheswari May 22 2007
/////////////////////////////////////////////////////////////////////////
function validate_dateSummary(date_field) {
        if (!date_field.value)         
                return true;
  
   var myDateFormatted = date_field.value;

        var in_date = stripCharString(myDateFormatted," ");
        in_date = in_date.toUpperCase();
        var date_is_bad = 0;  
       // if (!allowInString(in_date,"/0123456789T+-"))
		if (!allowInString(in_date,"/0123456789"))
                date_is_bad = 1; // invalid characters in date
        if (!date_is_bad) { 
                var has_rdi = 0;
                if (in_date.indexOf("T") >= 0){ 
                        has_rdi = 1;
                }
                if (!date_is_bad && has_rdi && (in_date.indexOf("T") != 0)) { 
                        date_is_bad = 2; // relative date index character is not in first position
                }
                if (!date_is_bad && has_rdi && (in_date.length == 1)) { 
                        var d = new Date();
						var return_month = parseInt(d.getMonth() + 1).toString();
						//return_month = (return_month.length==1 ? "0" : "") + return_month; 
						var return_date =  parseInt(d.getDate()).toString();
						//return_date = (return_date.length==1 ? "0" : "") + return_date; 
				        in_date = return_month + "/" + return_date + "/" + get_full_year(d);		
                        has_rdi = 0; // date doesn't have rdi char anymore (will also cause failure of add'l rdi checks, which is a good thing)
                }
                if (!date_is_bad && has_rdi && (in_date.length > 1) && !(in_date.charAt(1) == "+" || in_date.charAt(1) == "-")) {
                        date_is_bad = 3; // length of rdi string is greater than 1 but second char is not "+" or "-"
                }
                if (!date_is_bad && has_rdi && isNaN(parseInt(in_date.substring(2,in_date.length),10))) {
                        date_is_bad = 4; // rdi value is not a number
                }
                if (!date_is_bad && has_rdi && (parseInt(in_date.substring(2,in_date.length),10) < 0)) {
                        date_is_bad = 5; // rdi value is not a positive integer
                }
                if (!date_is_bad && has_rdi) {
                        var d = new Date();
                        ms = d.getTime();
                        offset = parseInt(in_date.substring(2,in_date.length),10);
                        if(in_date.charAt(1) == "+") {
                                ms += (86400000 * offset);
                        } else {
                                ms -= (86400000 * offset);
                        }
                        d.setTime(ms);
						var return_month = parseInt(d.getMonth() + 1).toString();
						return_month = (return_month.length==1 ? "0" : "") + return_month; 
						var return_date =  parseInt(d.getDate()).toString();
						return_date = (return_date.length==1 ? "0" : "") + return_date; 
				        in_date = return_month + "/" + return_date + "/" + get_full_year(d);	
                        has_rdi = 0;
                }
        } 
        if (!date_is_bad) {
                var date_pieces = new Array();
                date_pieces = in_date.split("/");
                if (date_pieces.length == 2) {
                        var d = new Date();
                        in_date = in_date + "/" + get_full_year(d);
                        date_pieces = in_date.split("/");
                }
                if (date_pieces.length != 3 || parseInt(date_pieces[0],10) < 1 || parseInt(date_pieces[0],10) > 12 
                                || parseInt(date_pieces[1],10) < 1 || parseInt(date_pieces[1],10) > 31 
                                || (date_pieces[2].length != 2 && date_pieces[2].length != 4)) {
                        date_is_bad = 6;  // date is not in format of m[m]/d[d]/yy[yy]
                }
        }
        if (date_is_bad) {
               // date_field.focus();
                return (false);
        }
        
        var ms = Date.parse(in_date);
        var d = new Date();
        d.setTime(ms);
		var return_date = d.toLocaleString();
		var return_month = parseInt(d.getMonth() + 1).toString();
		//return_month = (return_month.length==1 ? "0" : "") + return_month; 
		var return_date =  parseInt(d.getDate()).toString();
		//return_date = (return_date.length==1 ? "0" : "") + return_date; 
        return_date = return_month + "/" + return_date + "/" + get_full_year(d);
        //date_field.value = return_date;
        return true;
}       // normalize the year to yyyy
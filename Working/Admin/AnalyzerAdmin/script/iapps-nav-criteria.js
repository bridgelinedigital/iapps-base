﻿(function ($) {
    var methods = {
        init: function (options) {
            var settings = $.extend({
                onSelectValue: function (sender, eventArgs) {
                    if (typeof Criteria_SelectValue == 'function') Criteria_SelectValue(sender, eventArgs);
                }
            }, options);

            return this.each(function () {
                var $this = $(this);
                $this.data("settings", settings);

                $this.on("change", "select.criteria-options", function () {
                    methods.toggleCriteriaValues($this);
                });

                methods.toggleCriteriaValues($this);

                $this.on("click", "a.criteria-page", function () {
                    OpeniAppsAdminPopup("SelectPageLibraryPopup",
                        "PageStatus=-1", 'var selectPage = function() { $("#' + $this.attr("id") + '").navCriteria("selectPage"); }');
                });

                $this.on("click", "a.criteria-pages", function () {
                    var customAttributes = {};
                    var selectedValue = $this.find(".criteria-pages-id").val();
                    if (selectedValue != "") {
                        var listItemsJson = [];
                        var arrValue = selectedValue.split(",");
                        var arrText = $this.find(".criteria-pages-title").text().split(",");
                        for (var i = 0; i < arrValue.length; i++) {
                            listItemsJson.push({ "Id": arrValue[i], "Title": arrText[i], "ObjectTypeId": 8 });
                        }
                        customAttributes["ListItemsJson"] = JSON.stringify(listItemsJson);
                    }

                    OpeniAppsAdminPopup("SelectManualListPopup",
                        "PageStatus=-1&ObjectTypeId=8&ForceEnableSelection=true",
                        'var selectPages = function() { $("#' + $this.attr("id") + '").navCriteria("selectPages"); }',
                        customAttributes);
                });

                $this.on("click", "a.criteria-product", function () {
                    $("body").data("nav-criteria-obj", $this);
                    var popupPath = jCommerceAdminSiteUrl + '/Popups/StoreManager/SelectProductSkuPopup.aspx?showProducts=true';
                    SendTokenRequestByURLWithMethodNameForCommerce(popupPath, "OpenProductPopup()");
                });

                methods.setSelectedCriteria($this);
            });
        },
        selectPage: function () {
            var $this = $(this);
            var settings = $this.data("settings");

            var selectedValue = popupActionsJson.SelectedItems.first();
            var selectedText = popupActionsJson.CustomAttributes["SelectedTitle"];

            $this.find(".criteria-page-id").val(selectedValue);
            $this.find(".criteria-page-title").text(selectedText);

            methods.saveSelectedCriteria($this);
            settings.onSelectValue.call(this, $this, { selectedValue: selectedValue, selectedText: selectedText });
        },
        selectPages: function () {
            var $this = $(this);
            var settings = $this.data("settings");

            var listItemsJson = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);
            var selectedText = [], selectedId = [];
            $.each(listItemsJson, function () {
                selectedText.push(this.Title);
                selectedId.push(this.Id);
            });

            $this.find(".criteria-pages-id").val(selectedId.join(","));
            $this.find(".criteria-pages-title").text(selectedText.join(", "));
        },
        selectProduct: function (id, title) {
            var $this = $(this);
            var settings = $this.data("settings");

            $this.find(".criteria-product-id").val(id);
            $this.find(".criteria-product-title").text(title);

            methods.saveSelectedCriteria($this);
            settings.onSelectValue.call(this, $this, { selectedValue: id, selectedText: title });
        },
        toggleCriteriaValues: function ($this) {
            var $criteria = $this.find("select.criteria-options");
            var $criteriaValue = $this.find(".criteria-value");

            $criteriaValue.children().hide();
            $criteriaValue.children("." + $criteria.val().toLowerCase() + "-value").show();
        },
        setSelectedCriteria: function ($this) {
            if (typeof $this == "undefined")
                $this = $(this);

            var selectedJson = $this.find(".nav-criteria-json > input").val();
            if (selectedJson != "") {
                var result = JSON.parse(selectedJson);

                var selectedCriteria = result.SelectedOption;
                $this.find(".criteria-options").val(result.SelectedOption);
                $this.find(".criteria-id").val(result.Id);
                $this.find(".criteria-title").text(result.Title);
                if (selectedCriteria == "PageView") {
                    $this.find(".criteria-page-id").val(result.SelectedValue);
                    $this.find(".criteria-page-title").text(result.SelectedText);
                    $this.find(".criteria-page-label").text(result.SelectedLabel);
                }
                else if (selectedCriteria == "PageGroup") {
                    $this.find(".criteria-pages-id").val(result.SelectedValue);
                    var sValues = result.SelectedText.split("|");
                    $this.find(".criteria-pages-name").val(sValues[0]);
                    $this.find(".criteria-pages-title").text(sValues[1])
                }
                else if (selectedCriteria == "Product") {
                    $this.find(".criteria-product-id").val(result.SelectedValue);
                    $this.find(".criteria-product-title").text(result.SelectedText);
                }
                else if (selectedCriteria == "Watch")
                    $this.find(".criteria-watch-id").val(result.SelectedValue);
                else if (selectedCriteria == "MaxClick")
                    $this.find(".criteria-maxclick").val(result.SelectedValue);
            }
        },
        getSelectedCriteria: function ($this) {
            if (typeof $this == "undefined")
                $this = $(this);

            var result = {};

            var selectedCriteria = $this.find(".criteria-options").val();
            result.Id = $this.find(".criteria-id").val();
            result.Title = $this.find(".criteria-title").text();
            result.SelectedOption = selectedCriteria;
            result.SelectedValue = "";
            if (selectedCriteria == "PageView") {
                result.SelectedValue = $this.find(".criteria-page-id").val();
                result.SelectedText = $this.find(".criteria-page-title").text();
                result.SelectedLabel = $this.find(".criteria-page-label").text();
            }
            else if (selectedCriteria == "PageGroup") {
                result.SelectedValue = $this.find(".criteria-pages-id").val();
                result.SelectedText = stringformat("{0}|{1}",
                    $this.find(".criteria-pages-name").val(), $this.find(".criteria-pages-title").text());
            }
            else if (selectedCriteria == "Product") {
                result.SelectedValue = $this.find(".criteria-product-id").val();
                result.SelectedText = $this.find(".criteria-product-title").text();
            }
            else if (selectedCriteria == "Watch")
                result.SelectedValue = $this.find(".criteria-watch-id").val();
            else if (selectedCriteria == "MaxClick")
                result.SelectedValue = $this.find(".criteria-maxclick").val();

            $this.find(".nav-criteria-json > input").val(JSON.stringify(result));

            return result;
        },
        saveSelectedCriteria: function ($this) {
            if (typeof $this == "undefined")
                $this = $(this);

            var result = methods.getSelectedCriteria($this);

            return result.SelectedValue != "";
        }
    };
    $.fn.navCriteria = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.navCriteria');
        }

    };

})(jQuery);

function OpenProductPopup() {
    productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', destinationUrl, '', '');
}

function SelectProductFromPopup(product) {
    if (product != null && product != '' && product != 'undefined') {
        var $navCriteria = $("body").data("nav-criteria-obj");

        $navCriteria.navCriteria("selectProduct", product.Id, product.Title);
    }

    productSearchPopup.hide();
}

function CloseProductPopupFromPopup() {
    productSearchPopup.hide();
}

function ValidateNavigationCriteria(sender, args) {
    var containerId = $("#" + sender.controltovalidate).parents(".nav-criteria").attr("id");

    var result = $("#" + containerId).navCriteria("getSelectedCriteria");

    sender.errormessage = "Please select a value for " + result.SelectedLabel;
    args.IsValid = result.SelectedValue != "";
}
        
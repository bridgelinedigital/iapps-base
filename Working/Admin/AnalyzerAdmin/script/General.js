﻿if (!window.jQuery) {
    var documentProtocol = parent.location.protocol == "https:" ? "https://" : "http://";
    documentProtocol = documentProtocol + "ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js";
    var jsJquery = document.createElement("script");
    jsJquery.setAttribute("type", "text/javascript");
    jsJquery.setAttribute("src", documentProtocol);

    if (document.getElementsByTagName("head")) {
        document.getElementsByTagName("head").item(0).appendChild(jsJquery);
    }
    else {
        document.appendChild(jsJquery);
    }
}
//variable for default tooltip arrow
var tooltipArrowPath = '/AnalyzerAdmin/App_Themes/General/images/tooltip-arrow.gif';
/** Browser Detection Code **/
var detect = navigator.userAgent.toLowerCase();
var OS, browser, version, total, thestring;
var browserPlatform = navigator.platform.toLowerCase();
var macBrowser = navigator.userAgent.toLowerCase();

if (checkIt('safari')) browser = "safari";
else if (checkIt('msie')) browser = "ie";
else if (!checkIt('compatible')) {
    browser = "mozilla";
}
function checkIt(string) {
    place = detect.indexOf(string) + 1;
    thestring = string;
    return place;
}
function OScheckIt(string) {
    place = browserPlatform.indexOf(string) + 1;
    thestring = string;
    return place;
}
if (OScheckIt('mac')) OS = "mac";
else if (OScheckIt('win')) OS = "windows";
/** Browser Detection Code **/
// Function to trim
function Trim(s) {
    if (s != null) {
        // Remove leading spaces and carriage returns
        while ((s.substring(0, 1) == ' ') || (s.substring(0, 1) == '\n') || (s.substring(0, 1) == '\r')) {
            s = s.substring(1, s.length);
        }
        // Remove trailing spaces and carriage returns
        while ((s.substring(s.length - 1, s.length) == ' ') || (s.substring(s.length - 1, s.length) == '\n') || (s.substring(s.length - 1, s.length) == '\r')) {
            s = s.substring(0, s.length - 1);
        }
    }
    return s;
}
/** Methods for Global search **/
function clearSearch(objText) {
    if (objText.value == __JSMessages["Search"])
        objText.value = "";
}
function setSearch(objText) {
    if (Trim(objText.value) == "") {
        objText.value = __JSMessages["Search"];
    }
}
/** Methods for Global Search **/
/** Methods for Grid search **/
function setText(objText) {
    if (Trim(objText.value) == "") {
        objText.value = __JSMessages["Typeheretofilterresults"];
    }
}
function clearText(objText) {
    if (objText.value == __JSMessages["Typeheretofilterresults"]) {
        objText.value = "";
    }
}
/** Methods for Grid search **/

/** Fixing the Footer to the bottom of the page **/
function validKey(e) {
    var key;
    var keychar;
    var reg;

    if (window.event) {
        // for IE, e.keyCode or window.event.keyCode can be used
        key = e.keyCode;
    }
    else if (e.which) {
        // netscape
        key = e.which;
    }
    else {
        // no event, so pass through
        return true;
    }
    if (key > 31 || key == 8 || key == 16) {
        return true;
    }
    else {
        return false;
    }
}



/*** Methods added by AV ***/
// "Internal" function to return the decoded value of a cookie
//
function getCookieVal(offset) {
    var endstr = document.cookie.indexOf(";", offset);
    if (endstr == -1)
        endstr = document.cookie.length;
    return unescape(document.cookie.substring(offset, endstr));
}

//
// Function to return the value of the cookie specified by "name".
// name - String object containing the cookie name.
// returns - String object containing the cookie value, or null if
// the cookie does not exist.
//
function GetCookie(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return getCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return null;
}

//
// Function to create or update a cookie.
// name - String object object containing the cookie name.
// value - String object containing the cookie value. May contain
// any valid string characters.
// [expires] - Date object containing the expiration data of the cookie. If
// omitted or null, expires the cookie at the end of the current session.
// [path] - String object indicating the path for which the cookie is valid.
// If omitted or null, uses the path of the calling document.
// [domain] - String object indicating the domain for which the cookie is
// valid. If omitted or null, uses the domain of the calling document.
// [secure] - Boolean (true/false) value indicating whether cookie transmission
// requires a secure channel (HTTPS). 
//
// The first two parameters are required. The others, if supplied, must
// be passed in the order listed above. To omit an unused optional field,
// use null as a place holder. For example, to call SetCookie using name,
// value and path, you would code:
//
// SetCookie ("myCookieName", "myCookieValue", null, "/");
//
// Note that trailing omitted parameters do not require a placeholder.
//
// To set a secure cookie for path "/myPath", that expires after the
// current session, you might code:
//
// SetCookie (myCookieVar, cookieValueVar, null, "/myPath", null, true);
//
function SetCookie(name, value) {
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape(value) +
((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
((path == null) ? "" : ("; path=" + path)) +
((domain == null) ? "" : ("; domain=" + domain)) +
((secure == true) ? "; secure" : "");
}

// Function to delete a cookie. (Sets expiration date to current date/time)
// name - String object containing the cookie name
//
function DeleteCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1); // This cookie is history
    var cval = GetCookie(name);
    document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}


function UpdateSessionStatus() {
    // debugger;
    // get session timeout value in minutes (it comes from server and is set to this window value in MasterPage.master
    var sessionTimeout = window["SessionTimeoutValue"]; // 1 minute less than the actual server session timeout
    if (window["IsSessionValid"] != false) {
        if (sessionTimeoutCounter >= sessionTimeout) // session has already expired
        {
            window["IsSessionValid"] = false;
        }
        else {
            var shouldResetSessionCounter = GetCookie('ResetSessionCounter');
            if (shouldResetSessionCounter == "yes") {
                sessionTimeoutCounter = 2;
                SetCookie('ResetSessionCounter', 'no');
            }
            else
                sessionTimeoutCounter = sessionTimeoutCounter + 1;

            window["IsSessionValid"] = true;
        }
    }
}

function IsThisSessionValid() {
    if (window["IsSessionValid"]) {
        DeleteCookie('ResetSessionCounter');
        return true;
    }
    return false;
}

var timerObj;
var sessionTimeoutCounter = 0;
window["IsSessionValid"] = true;
function SetTimer() {

    var dblMinutes = 1;       // every minute check call this function
    //set timer to call function to confirm update 
    timerObj = setInterval("UpdateSessionStatus()", 1000 * 60 * dblMinutes);
}

function CheckSession() {
    if (!IsThisSessionValid()) {
        window.location.reload();
        return false;
    }
    return true;
}

//Event Handler 

function CheckSessionEventHandler(sender, eventArgs) {
    if (!IsThisSessionValid()) {
        eventArgs.set_cancel(true);
        window.location.reload();
    }
}


//function for the Calendar and Picker on Modify Users Page
function expirationCalendar_onSelectionChanged(sender, eventArgs) {
    var cal = window[expirydateIdHolder];
    var selectedDate = cal.getSelectedDate();
    if (selectedDate <= new Date()) {
        alert(GetMessage('UserInvalidExpiryDate'));
    }
    else {
        document.getElementById(expirydateTxtIdHolder).value = cal.formatDate(selectedDate, "MM/dd/yyyy");
    }
}

function popUpCalendar() {

    var thisDate = new Date();
    obj = document.getElementById(expirydateTxtIdHolder).value;

    var cal = window[expirydateIdHolder];

    if (obj != "" && obj != null) {
        if (validate_dateSummary(document.getElementById(expirydateTxtIdHolder)) == true) {// available in validation.js 
            thisDate = new Date(obj);
            cal.setSelectedDate(thisDate);
        }
    }
    if (!(cal.get_popUpShowing()));
    cal.show();


    //    expirationCalendar.setSelectedDate(thisDate);
    //    if(!(expirationCalendar.get_popUpShowing())); 
    //        expirationCalendar.show();
}

//Method for validating an email address
function ValidateEmail(eMail) {
    return /^(\w+\.)*([\w-]+)@([\w-]+\.)+([a-zA-Z]{2,4})$/.test(eMail);
}

//Method for converting Integer value to Decimal value
function GetContentDecimal(items) {
    if (items != null) {

        var aInt = items;
        var aintDecimal = (parseFloat(aInt) - 0.00).toFixed(2);
        return aintDecimal;
    }

}

function GetContentAfterSlash(items) {
    if (items != null && items != "") {
        var strMenu = items;
        var index = strMenu.lastIndexOf('/');
        if (index < 0) {
            return items;
        }
        else {
            return strMenu.substring(0, strMenu.lastIndexOf('/'));
        }

    }

}

/*
* Accepts a date, a mask, or a date and a mask.
* Returns a formatted version of the given date.
* The date defaults to the current date/time.
* The mask defaults to dateFormat.masks.default.
*/
var FormatDate = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
		    val = String(val);
		    len = len || 2;
		    while (val.length < len) val = "0" + val;
		    return val;
		};

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = FormatDate;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && (typeof date == "string" || date instanceof String) && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date();
        if (isNaN(date)) throw new SyntaxError(__JSMessages["invaliddate"]);

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
			    d: d,
			    dd: pad(d),
			    ddd: dF.i18n.dayNames[D],
			    dddd: dF.i18n.dayNames[D + 7],
			    m: m + 1,
			    mm: pad(m + 1),
			    mmm: dF.i18n.monthNames[m],
			    mmmm: dF.i18n.monthNames[m + 12],
			    yy: String(y).slice(2),
			    yyyy: y,
			    h: H % 12 || 12,
			    hh: pad(H % 12 || 12),
			    H: H,
			    HH: pad(H),
			    M: M,
			    MM: pad(M),
			    s: s,
			    ss: pad(s),
			    l: pad(L, 3),
			    L: pad(L > 99 ? Math.round(L / 10) : L),
			    t: H < 12 ? "a" : "p",
			    tt: H < 12 ? "am" : "pm",
			    T: H < 12 ? "A" : "P",
			    TT: H < 12 ? "AM" : "PM",
			    Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
			    o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
			    S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
} ();

// Some common format strings
FormatDate.masks = {
    "default": "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
FormatDate.i18n = {
    dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
    monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return FormatDate(this, mask, utc);
};

function cover(coverObject) {
    var ieMat = null; //for IE6 iframe shim
    // Cover <select> elements with <iframe> elements only in IE < 7
    if (navigator.appVersion.substr(22, 2) == "6." && browser == "ie") {
        if (ieMat == null) {
            ieMat = document.createElement('iframe');
            if (document.location.protocol == "https:")
                ieMat.src = "//0";
            else if (window.opera != "undefined")
                ieMat.src = "";
            else
                ieMat.src = "javascript:false";
            ieMat.scrolling = "no";
            ieMat.frameBorder = "0";
            ieMat.style.position = "absolute";
            ieMat.style.top = coverObject.offsetTop + "px";
            ieMat.style.left = coverObject.offsetLeft + "px";
            ieMat.style.width = "232px";
            ieMat.style.height = "185px";
            ieMat.style.filter = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)";
            ieMat.style.zIndex = "1";
            document.getElementById('dateRangeContainer').insertBefore(ieMat, document.getElementById('dateRangeSelector'));
        }
    }
    return ieMat;
}

//method to wrap text after 40 characters for the tooltip
function WrapTooltipText(objString) {
    var newString = "";
    var i = 0;
    if (objString != "") {
        while (i < objString.length) {
            newString += objString.substring(i, i + 40) + "<br />";
            i += 40;
        }
        return newString;
    }
    else {
        return objString;
    }
}
function OpenLicenseWarningPopup(productName, hasLicenese, hasPermission) {
    var url = "..";
    if (jAnalyticAdminSiteUrl != undefined && jAnalyticAdminSiteUrl != null && jAnalyticAdminSiteUrl != 'undefined' && jAnalyticAdminSiteUrl != '')
        url = jAnalyticAdminSiteUrl;
    url += "/Popups/LicenseWarning.aspx?ProductName=" + productName + "&HasLicense=" + hasLicenese + "&HasPermission=" + hasPermission;
    licenseWarningWindow = dhtmlmodal.open('LicenseWarning', 'iframe', url, __JSMessages["LicenseWarning"], 'width=415px,height=350px,center=1,resize=0,scrolling=1');
    licenseWarningWindow.onclose = function () {
        var a = document.getElementById('LicenseWarning');
        var ifr = a.getElementsByTagName("iframe");
        window.frames[ifr[0].name].location.replace("/blank.html");
        return true;
    }
}
function OpenAnalyticsVersionCheckPopup() {
    var currentSite = GetCurrentSection();
    var url = jAnalyticAdminSiteUrl + "/Popups/iAPPSVersionCheck.aspx";
    versionCheckWindow = dhtmlmodal.open('iAPPSVersionCheck', 'iframe', url, __JSMessages["iAPPSVersionCheck"], 'width=425px,height=200px,center=1,resize=0,scrolling=1');
    versionCheckWindow.onclose = function () {
        var a = document.getElementById('iAPPSVersionCheck');
        var ifr = a.getElementsByTagName("iframe");
        window.frames[ifr[0].name].location.replace("/blank.html");
        return true;
    }
}

/** Methods to set the pagination details **/
function GetGridPaginationInfo(grid) {
    return stringformat(Page0of12items, Grid_CurrentPageIndex(grid), Grid_PageCount(grid), grid.RecordCount);
}

function Grid_CurrentPageIndex(grid) {
    return grid.RecordCount == 0 ? 1 : grid.CurrentPageIndex + 1;
}

function Grid_PageCount(grid) {
    return grid.RecordCount == 0 ? 1 : grid.PageCount;
}
/** Methods to set the pagination details **/

/** i18n code generated by Adams **/
function stringformat(str)            //created by Adams to format the string for localization needs
{
    if (arguments.length == 0)
        return null;

    var str = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
        str = str.replace(re, arguments[i]);
    }
    return (str);
}

function ChangeSpecialCharacters(str) {
    var replacedString = str;
    if (replacedString != null) {
        replacedString = replacedString.replace(new RegExp("&", "g"), "&amp;");
        replacedString = replacedString.replace(new RegExp("<", "g"), "&lt;");
        replacedString = replacedString.replace(new RegExp(">", "g"), "&gt;");
        replacedString = replacedString.replace(new RegExp("\"", "g"), "&quot;");
        replacedString = replacedString.replace(new RegExp("'", "g"), "\\'");
        replacedString = replacedString.replace(new RegExp("\n", "g"), " ");
    }
    return replacedString;
}

function gridCurrentPageIndex(objGrid) {
    return objGrid.RecordCount == 0 ? 1 : objGrid.CurrentPageIndex + 1;
}

function gridPageCount(objGrid) {
    return objGrid.RecordCount == 0 ? 1 : objGrid.PageCount;
}

function SetVerticalScroll(sender, eventArgs) {
    $("#" + sender.get_id() + "_VerticalScrollDiv").addClass("vertical-scroll");
}

function SetGridStyles(sender, eventArgs) {
    FormatDataGrid(sender, true);
}

function FormatDataGrid(objGrid, formatLastColumn) {
    document.getElementById(objGrid.get_id() + "_dom").style.height = "auto";

    var pCount = objGrid.RecordCount == 0 ? 1 : objGrid.PageCount;
    HideGridFooter(objGrid, pCount);

    if (formatLastColumn) {
        $("#" + objGrid.get_id() + " tr td.HeadingCell:last").addClass("LastHeadingCell");
        $("#" + objGrid.get_id() + " tr").each(function () {
            $(this).find("td.DataCell:last").addClass("LastDataCell");
        });
    }

    if (objGrid.RecordCount == 0) {
        $("#" + objGrid.get_id() + " table:first").find("tr:last td").addClass("empty-grid-row");
    }
}

function HideGridFooter(objGrid, pCount) {
    var gridFooter = document.getElementById(objGrid.get_id() + "_footer");
    if (gridFooter) {
        if (objGrid.RecordCount == 0) {
            gridFooter.style.display = "none";
            $("#" + objGrid.get_id()).addClass("empty-grid");
        }
        else {
            gridFooter.style.display = "";
            $("#" + objGrid.get_id()).removeClass("empty-grid");

            if (pCount <= 1)
                $("#" + objGrid.get_id() + "_footer table").find("td").first().hide();
        }
    }
}

function GetGridLoadingPanelContent(objGrid) {
    var height = 50;
    var width = 100;
    if (objGrid != "") {
        var gridDom = document.getElementById(objGrid.get_id() + "_dom");
        if (gridDom) {
            height = gridDom.offsetHeight;
            width = gridDom.offsetWidth;
        }
    }
    var html = "<table><tr><td class='LoadingPopup' style='height:{0}px;width:{1}px;'>Loading...<br /><img src='/iapps_images/spinner.gif' border='0' alt='' /></td></tr></table>";

    return stringformat(html, height, width);
}

function onClickClearGrid(txtBoxId, objGrid) {
    document.getElementById(txtBoxId).value = "";
    objGrid.Search("", false);
}

function onClickSearchonGrid(txtBoxId, objGrid) {
    var txtValue = $("#" + txtBoxId).val();
    if (txtValue == $("#" + txtBoxId).attr("title"))
        txtValue = "";
    /*if (txtValue == "" || txtValue == $("#" + txtBoxId).attr("title")) {
    alert(EnterValue2Search);
    $("#" + txtBoxId).focus();
    return false;
    }
    else if (!txtValue.match(/^[a-zA-Z0-9. _']+$/)) {
    alert(InValidSearchTerm);
    $("#" + txtBoxId).focus();
    return false;
    }
    else*/
    {
        objGrid.Search(txtValue, true);
        document.getElementById(objGrid.get_id() + "_dom").style.height = "auto";
        return true;
    }
}

function FormatStatusColumn(txtStatus) {
    if (txtStatus.toLowerCase() == "true")
        return __JSMessages["InActiveStatus"];
    else
        return __JSMessages["ActiveStatus"];
}

function SetTextboxDefaultValues() {
    $(".textBoxes").each(function () {
        if ($(this).attr("title") && $(this).attr("title") != "" &&
                ($(this).val() == "" || $(this).val() == $(this).attr("title"))) {
            $(this).focus(function () {
                if ($(this).val() == $(this).attr("title")) {
                    $(this).removeClass("defaultText");
                    $(this).val("");
                }
            });
            $(this).blur(function () {
                if ($(this).val() == "" || $(this).val() == $(this).attr("title")) {
                    $(this).addClass("defaultText");
                    $(this).val($(this).attr("title"));
                }
            });

            $(this).blur();
        }
    });
}

$(function () {
    SetTextboxDefaultValues();
});

/*
function to move the selected site and groups to the permissions Listbox.
site    : siteName#SiteId
groups  : array of groupName#groupId
permissions : object of permissions list box.
*/
function MoveSiteGroupsToPermission(site, groups, permissions) {

    if (groups != null && site != null) {
        var siteNameWithValue = new Array();
        if (site != null && site.length > 0) {
            siteNameWithValue = site[0].split('#');
            for (i = 0; i < groups.length; i++) {
                var groupNameWithValue = new Array();
                groupNameWithValue = groups[i].split("#");
                var newText = siteNameWithValue[0] + ":" + groupNameWithValue[0];
                var newValue = siteNameWithValue[1] + ":" + groupNameWithValue[1];

                if (!containsElement(permissions, newValue)) {
                    permissions.options[permissions.options.length] = new Option(newText, newValue);
                    siteGroupListItems[siteGroupListItems.length] = newValue;
                }
            }
        }
    }
}

function RemoveSelectedItemsFromLstBox(lstBox) {

    for (var i = lstBox.options.length - 1; i >= 0; i--) {
        if (lstBox.options[i].selected)
            lstBox.remove(i);
    }
}

function GetSelectedTextWithValueOfLstBox(lstBox) {
    if (lstBox != null) {
        var textWithValue = new Array();
        var lstOptions = lstBox.options;

        for (i = 0; i < lstOptions.length; i++) {
            if (lstOptions[i].selected) {
                var option = lstOptions[i];
                var text;
                if (option.innerText) {
                    text = lstOptions[i].innerText;
                }
                else {
                    text = lstOptions[i].text;
                }
                textWithValue[textWithValue.length] = text + "#" + lstOptions[i].value;
            }
        }

        return textWithValue;
    }
    else {
        return null;
    }

}

function containsElement(lstToBeSearched, valueToBeSearched) {
    for (j = 0; j < lstToBeSearched.options.length; j++) {
        if (lstToBeSearched.options[j].value == valueToBeSearched) {
            return true;
        }
    }

    if (browser == "ie") {
        lstToBeSearched.focus();
        event.returnValue = false;
    }
    else {
        lstToBeSearched.focus();
        return false;
    }
}

function fn_ShowSearchBox() {
    var objSearchBox = $('li#divSearchBox')
    $(objSearchBox).slideToggle('fast', function () {
        $(objSearchBox).parents('li').toggleClass('selected');
    });
}
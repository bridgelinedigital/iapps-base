﻿function sendRequest(url, callback, postData) {
    var req = createXMLHTTPObject();
    if (!req) return;
    var method = (postData) ? "POST" : "GET";
    req.open(method, url, true);
    req.setRequestHeader('User-Agent', 'XMLHTTP/1.0');
    if (postData)
        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function () {
        if (req.readyState != 4) return;
        if (req.status != 200 && req.status != 304) {
            //			alert('HTTP error ' + req.status);
            return;
        }
        callback(req);
    }
    req.send(postData);

    if (req.readyState == 4) return;
}

var XMLHttpFactories = [
	function () { return new XMLHttpRequest() },
	function () { return new ActiveXObject("Msxml2.XMLHTTP") },
	function () { return new ActiveXObject("Msxml3.XMLHTTP") },
	function () { return new ActiveXObject("Microsoft.XMLHTTP") }
];

function createXMLHTTPObject() {
    var xmlhttp = false;
    for (var i = 0; i < XMLHttpFactories.length; i++) {
        try {
            xmlhttp = XMLHttpFactories[i]();
        }
        catch (e) {
            continue;
        }
        break;
    }
    return xmlhttp;
}

function handleResponse(req) {
    var callResponse = req.responseText;
    var emptyGuid = '00000000-0000-0000-0000-000000000000';
    if (callResponse.length == emptyGuid.length) {
        if (callResponse != 'Invalid') {
            var destinationUrl = "";
            var currentUrl = document.URL;
            if (currentUrl == null || currentUrl == '') {
                currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
            }
            if (currentUrl.indexOf('?') > 0) {
                currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
            }
            if (itemContext == 'JumpToHome') {
                destinationUrl = jPublicSiteUrl + '/Default.aspx?LastVisitedAnalyticsAdminPageUrl=' + currentUrl + '&Token=' + callResponse + '&PageState=Overlay';
            }
            else if (itemContext == 'JumpToLastVisitedPage') {
                if (jLastVisitedFrontEndPage == null || jLastVisitedFrontEndPage == '') {
                    jLastVisitedFrontEndPage = "Default.aspx";
                }
                destinationUrl = jPublicSiteUrl + '/' + jLastVisitedFrontEndPage + '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl + '&Token=' + callResponse + '&PageState=Overlay';
            }
            if (itemContext =='Editor' || itemContext == 'JumpToHome' || itemContext == 'JumpToLastVisitedPage') {
                window.location = destinationUrl;
            }
        }
    }
    else {
        window.location = jAnalyticAdminSiteUrl;
    }
}

var itemContext;
function SendTokenRequest(itemId) {
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jCMSProductId + "&userName=" + juserName;
    var urlToCall = jAnalyticAdminSiteUrl + '/' + tokenPageUrl;
    itemContext = itemId;
    switch (itemId) {
        case 'CheckForUpdates':
            OpenAnalyticsVersionCheckPopup();
            break;
        case 'Editor':
        case 'JumpToHome':
        case 'JumpToLastVisitedPage':
            sendRequest(urlToCall, handleResponse, "GET");
            break;
    }
}

function SendTokenRequestByURLWithMethodNameForCommerce(redirectURL, methodName) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jCommerceProductId + "&userName=" + juserName;
    var urlToCall = jAnalyticAdminSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    functionToCallAfterTokenCallback = methodName;
    sendRequest(urlToCall, handleResponseInSite, "GET");
}

function handleResponseInSite(req) {
    var callResponse = req.responseText;
    var emptyGuid = '00000000-0000-0000-0000-000000000000';
    if (callResponse.length == emptyGuid.length) {
        if (callResponse != 'Invalid') {
            //check '?' we have to add here
            if (navigateURL.indexOf('?') > -1) {
                destinationUrl = navigateURL + '&Token=' + callResponse;
            }
            else {
                destinationUrl = navigateURL + '?Token=' + callResponse;
            }
            //Modified by AV
            setTimeout(functionToCallAfterTokenCallback, 1000);
            // ENd Modified
        }
    }
}

var functionToCallAfterTokenCallback;
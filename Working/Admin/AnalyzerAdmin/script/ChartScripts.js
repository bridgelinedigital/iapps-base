function onDataPointHover(who, what) {
    var point = what.get_dataPoint();
    var series = point.get_parentSeries();
    //create the HTML for the pop-up
    var popupHTML = point.get_x().split(" ")[0];
    if (series.get_name() != "seriesPublishDateMarker") {
        if (point.get_y() != 0) {
            popupHTML += "<br/><b>" + point.get_y() + "</b>";
        }
    }
    if (series.get_name() == "seriesPublishDateMarker") {
        popupHTML += "<br/><b>" + pageAuthor + "</b>";
    }
    ddrivetip(popupHTML, '100', '', imgSrc, revImgSrc, downImgSrc);
}

function onDataPointExit(who, what) {
    hideddrivetip();
}

function registerMouse() {
    window.mouseX = 0;
    window.mouseY = 0;
    document.onmousemove = watchMouse;
}
function watchMouse(e) {
    var posx = 0;
    var posy = 0;
    if (!e)
        var e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft;
        posy = e.clientY + document.body.scrollTop;
    }
    window.mouseX = posx;
    window.mouseY = posy;
}
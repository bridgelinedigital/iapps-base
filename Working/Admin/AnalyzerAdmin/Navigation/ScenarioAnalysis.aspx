<%@ Page Language="C#" MasterPageFile="~/RangeMaster.master" AutoEventWireup="true" StylesheetTheme="General" runat="server" 
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.ScenarioAnalysis" CodeBehind="ScenarioAnalysis.aspx.cs"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerNavigationScenarioAnalysis %>"%>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<asp:Content ID="head" ContentPlaceHolderID="rangeHeaderHolder" runat="server">
    <script type="text/javascript">
        function cmScenario_onContextMenu(sender, eventArgs) {
            gridItem = eventArgs.get_item();

            var evt = eventArgs.get_event();
            cmScenario.showContextMenuAtEvent(evt);
        }

        function cmScenario_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();

            var selectedId = gridItem.getMember("Id").get_text();

            switch (selectedMenuId) {
                case "RunScenario":
                    window.location = "ScenarioResults.aspx?Id=" + selectedId
                    break;
                case "EditScenario":
                    window.location = "ScenarioDetails.aspx?Id=" + selectedId
                    break;
                case "DeleteScenario":
                    grdScenarios.set_callbackParameter(selectedId);
                    grdScenarios.callback();
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="scenarioHeaderButtons" ContentPlaceHolderID="headerButtons" runat="Server">
    <asp:HyperLink ID="hplAddScenario" runat="server" Text="<%$ Resources:GUIStrings, AddNewScenario %>"
        CssClass="primarybutton" NavigateUrl="ScenarioDetails.aspx" />
</asp:Content>
<asp:Content ID="contentScenarioAnallysis" ContentPlaceHolderID="rangeContentHolder"
    runat="Server">
    <asp:Panel ID="pnlResults" runat="server" CssClass="scenario-analysis">
        <p>
            Right click a scenario below for options.</p>
        <ComponentArt:Grid ID="grdScenarios" EditOnClickSelectedItem="false" SkinID="Default"
            Width="100%" ShowHeader="false" ShowFooter="false" PageSize="100" ShowSearchBox="false"
            runat="server" RunningMode="Client" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>">
            <Levels>
                <ComponentArt:GridLevel AlternatingRowCssClass="AlternateDataRow" AllowSorting="true"
                    DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false" SelectorCellCssClass="SelectorCell"
                    SelectorCellWidth="18" SelectorImageWidth="17" SelectorImageHeight="15" HeadingSelectorCellCssClass="SelectorCell"
                    HeadingRowCssClass="HeadingRow" DataCellCssClass="DataCell" RowCssClass="Row"
                    SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    HeadingCellCssClass="HeadingCell" HeadingTextCssClass="HeadingCellText">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Name" IsSearchable="true" AllowEditing="false"
                            Align="left" Visible="true" runat="server" HeadingText="<%$ Resources:GUIStrings, ScenarioName %>"
                            FixedWidth="true" DataCellClientTemplateId="NameCT" />
                        <ComponentArt:GridColumn DataField="ViewType" IsSearchable="true" AllowEditing="false"
                            Align="left" Visible="true" runat="server" HeadingText="<%$ Resources:GUIStrings, View %>"
                            FixedWidth="true" DataCellClientTemplateId="ViewTypeCT" />
                        <ComponentArt:GridColumn DataField="Segment_Id" IsSearchable="true" AllowEditing="false"
                            Align="left" Visible="true" runat="server" HeadingText="<%$ Resources:GUIStrings, Segments %>"
                            FixedWidth="true" DataCellClientTemplateId="SegmentsCT" />
                        <ComponentArt:GridColumn DataField="CreatedByFullName" IsSearchable="true" AllowEditing="false"
                            Align="left" Visible="true" runat="server" HeadingText="<%$ Resources:GUIStrings, CreatedBy %>"
                            FixedWidth="true" DataCellClientTemplateId="CreatedByFullNameCT" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CreatedBy" IsSearchable="false" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="NameCT">
                    <div title="## DataItem.GetMember('Name').Value ##">
                        ## DataItem.GetMember("Name").Value ##
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ViewTypeCT">
                    <div title="## DataItem.GetMember('ViewType').Value ##">
                        ## DataItem.GetMember("ViewType").Value != 0 ? "Only to Me" : "Everyone" ##
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="SegmentsCT">
                    <div title="## DataItem.GetMember('Segment_Id').Value ##">
                        ## DataItem.GetMember("Segment_Id").Value != "" ? DataItem.GetMember("Segment_Id").Value
                        : " - " ##
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CreatedByFullNameCT">
                    <div title="## DataItem.GetMember('CreatedByFullName').Value ##">
                        ## DataItem.GetMember("CreatedByFullName").Value ##
                    </div>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ContextMenu EventHandler="cmScenario_onContextMenu" />
            </ClientEvents>
        </ComponentArt:Grid>
        <ComponentArt:Menu SkinID="ContextMenu" ID="cmScenario" runat="server">
            <Items>
                <ComponentArt:MenuItem ID="RunScenario" runat="server" Text="<%$ Resources:GUIStrings, RunScenario %>"
                    AutoPostBackOnSelect="true" />
                <ComponentArt:MenuItem LookId="BreakItem" />
                <ComponentArt:MenuItem LookId="BreakItem" />
                <ComponentArt:MenuItem ID="EditScenario" runat="server" Text="<%$ Resources:GUIStrings, EditScenario %>"
                    Look-LeftIconUrl="cm-icon-edit.png" AutoPostBackOnSelect="false" />
                <ComponentArt:MenuItem LookId="BreakItem" />
                <ComponentArt:MenuItem ID="DeleteScenario" runat="server" Text="<%$ Resources:GUIStrings, DeleteScenario %>"
                    Look-LeftIconUrl="cm-icon-delete.png" AutoPostBackOnSelect="false" />
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="cmScenario_onItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
    </asp:Panel>
</asp:Content>

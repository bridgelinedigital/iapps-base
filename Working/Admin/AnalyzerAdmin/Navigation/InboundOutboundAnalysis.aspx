<%@ Page Language="C#" MasterPageFile="~/RangeMaster.master" AutoEventWireup="true" Theme="General" 
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.InboundOutboundAnalysis" CodeBehind="InboundOutboundAnalysis.aspx.cs"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerNavigationInboundOutboundAnalysis %>"%>

<%@ Register TagName="Criteria" TagPrefix="UC" Src="~/UserControls/Navigation/NavigationCriteria.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="rangeHeaderHolder" runat="server">
    <script type="text/javascript">
        function Criteria_SelectValue(sender, eventArgs) {
            LoadPageClicks(eventArgs.selectedValue, eventArgs.selectedText);
        }

        function LoadPageClicks(id, title) {
            $("#hdnSelectedPage").val(stringformat("{0}|{1}", id, title));
            LoadPageClickValues();
        }

        function LoadPageClickValues(type) {
            var defaultInfo = $("#hdnSelectedPage").val();
            if (typeof defaultInfo != "undefined" && defaultInfo != "") {
                var defaultArr = defaultInfo.split("|");
                var id = defaultArr[0];
                var title = defaultArr[1];
                if (typeof (type) == "undefined" || type == "from") {
                    $("#PageHeadingFrom").html(title);

                    var rowItem = grdTopClicksFrom.get_table().addEmptyRow(0);
                    rowItem.setValue(5, id);
                    grdTopClicksFrom.editComplete();
                    grdTopClicksFrom.Callback();
                }
                if (typeof (type) == "undefined" || type == "to") {
                    $("#PageHeadingTo").html(title);

                    var item = grdTopClicksTo.get_table().addEmptyRow(0);
                    item.setValue(5, id);
                    grdTopClicksTo.editComplete();
                    grdTopClicksTo.Callback();
                }
                $(".result-section").show();
            }
        }

        function grdTopClicksFrom_onLoad(sender, eventArgs) {
            LoadPageClickValues("from");
        }

        function grdTopClicksTo_onLoad(sender, eventArgs) {
            LoadPageClickValues("to");
        }

        var pageId = "", fromItem, toItem;
        function grdTopClicksFrom_onContextMenu(sender, eventArgs) {
            fromItem = eventArgs.get_item();
            grdTopClicksFrom.select(fromItem);

            if (fromItem.getMember("Author_Email").Text != "<asp:localize runat='server' text='<%$ Resources:JSMessages, ReferrerSite %>'/>") {
                if (fromItem.getMember("Title").Text != "<asp:localize runat='server' text='<%$ Resources:JSMessages, Unknown %>'/>") {
                    var evt = eventArgs.get_event();
                    cmTopClicksFrom.showContextMenuAtEvent(evt, eventArgs.get_item());
                }
            }
        }

        function cmTopClicksFrom_onItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            pageId = fromItem.getMember("OriginalId").get_text();
            pageId = pageId.replace("{", "");
            pageId = pageId.replace("}", "");
            switch (selectedMenu) {
                case "ViewStatistics":
                    var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + fromItem.GetMember('OriginalId').Text;
                    viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', '');
                    break;
                case "ViewAnalysis":
                    window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + pageId;
                    break;
                case "ViewNewWindow":
                    window.open(analyticsPublicSiteUrl + "/" + fromItem.GetMember('Menu').Text);
                    break;
                case "ViewSiteEditor":
                    JumpToFrontPage(fromItem.GetMember('Menu').Text);
                    break;
                case "ViewOverlay":
                    JumpToFrontPage(fromItem.GetMember('Menu').Text, "PageState=Overlay");
                    break;
                case "PathStart":
                    window.location.href = analyticsUrl + "/Navigation/PathAnalysis.aspx?startPageId=" + pageId;
                    break;
                case "PathEnd":
                    window.location.href = analyticsUrl + "/Navigation/PathAnalysis.aspx?endPageId=" + pageId;
                    break;
                case "AssignIndexTerms":
                    OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                    break;
                case "EmailAuthor":
                    location.href = "mailto:" + fromItem.getMember("Author_Email").Text;
                    break;
            }
        }
        function grdTopClicksTo_onContextMenu(sender, eventArgs) {
            toItem = eventArgs.get_item();
            grdTopClicksTo.select(toItem);
            if (toItem.getMember("Title").Text != "<asp:localize runat='server' text='<%$ Resources:JSMessages, ExitedTheSite %>'/>") {
                var evt = eventArgs.get_event();
                cmTopClicksTo.showContextMenuAtEvent(evt);
            }
        }
        function cmTopClicksTo_onItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            pageId = toItem.getMember("OriginalId").get_text();
            pageId = pageId.replace("{", "");
            pageId = pageId.replace("}", "");
            switch (selectedMenu) {
                case "ViewStatisticsTo":
                    var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + toItem.GetMember('OriginalId').Text;
                    viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', '');
                    break;
                case "ViewAnalysisTo":
                    window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + pageId;
                    break;
                case "ViewNewWindowTo":
                    window.open(analyticsPublicSiteUrl + "/" + toItem.GetMember('Menu').Text);
                    break;
                case "ViewSiteEditor":
                    JumpToFrontPage(toItem.GetMember('Menu').Text);
                    break;
                case "ViewOverlay":
                    JumpToFrontPage(toItem.GetMember('Menu').Text, "PageState=Overlay");
                    break;
                case "StartPathTo":
                    window.location.href = analyticsUrl + "/Navigation/PathAnalysis.aspx?startPageId=" + pageId;
                    break;
                case "EndPathTo":
                    window.location.href = analyticsUrl + "/Navigation/PathAnalysis.aspx?endPageId=" + pageId;
                    break;
                case "AssignIndexTermsTo":
                    OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                    break;
                case "EmailAuthorTo":
                    location.href = "mailto:" + toItem.getMember("Author_Email").Text;
                    break;
            }
        }

        function ShowEmailReport() {
            exportData();
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', '../popups/ExportReportAsEmail.aspx', '', '');

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="contentTrafficAnalysis" ContentPlaceHolderID="rangeContentHolder"
    runat="Server">
    <div class="inbound-outbound">
        <div class="filter-section clear-fix">
            <h3>
                Select a criteria to start analysis.</h3>
            <UC:Criteria ID="selectedCriteria" runat="server" />
            <asp:HiddenField ID="hdnSelectedPage" runat="server" ClientIDMode="Static" />
        </div>
        <div class="result-section" style="display: none;">
            <div class="export-section">
                <asp:LinkButton ID="lbtnEmail" Text="<%$ Resources:GUIStrings, Email1 %>" runat="server"
                    CssClass="email-icon" ToolTip="<%$ Resources:GUIStrings, Email1 %>" OnClientClick="return ShowEmailReport();" />
                <asp:LinkButton ID="lbtnExcel" Text="<%$ Resources:GUIStrings, ExportToExcel %>"
                    runat="server" CssClass="excel-icon" ToolTip="<%$ Resources:GUIStrings, ExportToExcel %>" />
                <asp:LinkButton ID="lbtnPdf" Text="<%$ Resources:GUIStrings, ExporttoPDF %>" runat="server"
                    CssClass="pdf-icon" ToolTip="<%$ Resources:GUIStrings, ExporttoPDF %>" />
            </div>
            <div class="page-sub-header clear-fix">
                <%= GUIStrings.Top10ClicksTo %>
                <h4 id="PageHeadingFrom" runat="server" clientidmode="Static" class="selected-title">
                </h4>
            </div>
            <ComponentArt:Grid SkinID="Default" ID="grdTopClicksFrom" runat="server" RunningMode="Client"
                EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
                Width="100%" PageSize="10" ShowFooter="false">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="UniqueId" ShowTableHeading="false" ShowSelectorCells="false"
                        RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                        HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                        HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                        HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                        SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                        <Columns>
                            <ComponentArt:GridColumn Width="250" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                                HeadingCellCssClass="FirstHeadingCell" DataField="Title" DataCellCssClass="FirstDataCell"
                                SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                                FixedWidth="true" TextWrap="true" />
                            <ComponentArt:GridColumn Width="275" runat="server" HeadingText="<%$ Resources:GUIStrings, Menu %>"
                                DataField="Menu" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                                AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="MenuFromHT" />
                            <ComponentArt:GridColumn Width="75" runat="server" HeadingText="<%$ Resources:GUIStrings, Clicks %>"
                                DataField="Click" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                                FixedWidth="true" Align="Right" DataType="System.Int64" HeadingCellClientTemplateId="ClicksHT" />
                            <ComponentArt:GridColumn Width="70" runat="server" HeadingText="<%$ Resources:GUIStrings, Click %>"
                                DataField="Click_Percentage" DataCellClientTemplateId="ClickFromCT" FixedWidth="true"
                                DataType="System.Double" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                AllowReordering="false" Align="Right" HeadingCellClientTemplateId="ClicksFromHT" />
                            <ComponentArt:GridColumn DataField="UniqueId" Visible="false" />
                            <ComponentArt:GridColumn HeadingText="Action" Visible="false" />
                            <ComponentArt:GridColumn DataField="OriginalId" Visible="false" />
                            <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientEvents>
                    <Load EventHandler="grdTopClicksFrom_onLoad" />
                    <ContextMenu EventHandler="grdTopClicksFrom_onContextMenu" />
                    <RenderComplete EventHandler="InitializeToolTip" />
                </ClientEvents>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="ClickFromCT">
                        ## GetContentDecimal(DataItem.GetMember('Click_Percentage').Value) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="MenuFromHT">
                        <div class="custom-header">
                            <span class="header-text">Menu</span> ## CreateToolTip(iMenu) ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ClicksHT">
                        <div class="custom-header">
                            <span class="header-text">Clicks</span> ## CreateToolTip(iClick)##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ClicksFromHT">
                        <div class="custom-header">
                            <span class="header-text">Click %</span> ## CreateToolTip(iClickFrom) ##
                        </div>
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
            <ComponentArt:Menu ID="cmTopClicksFrom" runat="server" SkinID="ContextMenu">
                <Items>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>"
                        ID="ViewStatistics" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>"
                        ID="ViewNewWindow" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, PathAnalysisStart %>"
                        ID="PathStart" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, PathAnalysisEnd %>"
                        ID="PathEnd" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
                        ID="ViewSiteEditor" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinOverlayMode %>"
                        ID="ViewOverlay" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>"
                        ID="AssignIndexTerms" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>"
                        ID="EmailAuthor" ToolTip="<%$ Resources:GUIStrings, EmailAuthor %>" />
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="cmTopClicksFrom_onItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
            <div class="page-sub-header clear-fix">
                <%= GUIStrings.Top10ClicksFrom %>
                <h4 id="PageHeadingTo" runat="server" clientidmode="Static" class="selected-title">
                </h4>
            </div>
            <ComponentArt:Grid SkinID="Default" ID="grdTopClicksTo" runat="server" RunningMode="Client"
                EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
                Width="100%" PageSize="10" ShowFooter="false">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="UniqueId" ShowTableHeading="false" ShowSelectorCells="false"
                        RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                        HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                        HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                        HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                        SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                        <Columns>
                            <ComponentArt:GridColumn Width="250" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                                DataField="Title" IsSearchable="true" AllowReordering="false" FixedWidth="true"
                                TextWrap="true" />
                            <ComponentArt:GridColumn Width="275" runat="server" HeadingText="<%$ Resources:GUIStrings, Menu %>"
                                DataField="Menu" IsSearchable="true" TextWrap="true" AllowReordering="false"
                                FixedWidth="true" HeadingCellClientTemplateId="MenuToHT" />
                            <ComponentArt:GridColumn Width="75" runat="server" HeadingText="<%$ Resources:GUIStrings, Clicks %>"
                                DataField="Click" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                                FixedWidth="true" Align="Right" DataType="System.Int64" HeadingCellClientTemplateId="ClickHT" />
                            <ComponentArt:GridColumn Width="70" runat="server" HeadingText="<%$ Resources:GUIStrings, Clicks1 %>"
                                DataField="Click_Percentage" DataCellClientTemplateId="ClickToCT" FixedWidth="true"
                                Align="Right" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                AllowReordering="false" DataType="System.Double" HeadingCellClientTemplateId="ClickToHT" />
                            <ComponentArt:GridColumn DataField="UniqueId" Visible="false" />
                            <ComponentArt:GridColumn HeadingText="Action" Visible="false" />
                            <ComponentArt:GridColumn DataField="OriginalId" Visible="false" />
                            <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="ClickToCT">
                        ## GetContentDecimal(DataItem.GetMember('Click_Percentage').Value) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="MenuToHT">
                        <div class="custom-header">
                            <span class="header-text">Menu</span> ## CreateToolTip(iMenu) ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ClickHT">
                        <div class="custom-header">
                            <span class="header-text">Clicks</span> ## CreateToolTip(iClick)##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ClickToHT">
                        <div class="custom-header">
                            <span class="header-text">Click %</span> ## CreateToolTip(iClickFrom) ##
                        </div>
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
                <ClientEvents>
                    <Load EventHandler="grdTopClicksTo_onLoad" />
                    <ContextMenu EventHandler="grdTopClicksTo_onContextMenu" />
                    <RenderComplete EventHandler="InitializeToolTip" />
                </ClientEvents>
            </ComponentArt:Grid>
            <ComponentArt:Menu ID="cmTopClicksTo" runat="server" SkinID="ContextMenu">
                <Items>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>"
                        ID="ViewStatisticsTo" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>"
                        ID="ViewNewWindowTo" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, PathAnalysisStart %>"
                        ID="StartPathTo" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, PathAnalysisEnd %>"
                        ID="EndPathTo" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
                        ID="ViewSiteEditorTo" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinOverlayMode %>"
                        ID="ViewOverlayTo" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>"
                        ID="AssignIndexTermsTo" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>"
                        ToolTip="<%$ Resources:GUIStrings, EmailAuthor %>" ID="EmailAuthorTo" />
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="cmTopClicksTo_onItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
        </div>
    </div>
</asp:Content>

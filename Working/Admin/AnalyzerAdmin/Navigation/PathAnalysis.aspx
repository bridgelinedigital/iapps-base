<%@ Page Language="C#" MasterPageFile="~/RangeMaster.master" AutoEventWireup="true" runat="server" 
    Theme="General" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.PathAnalysis" CodeBehind="PathAnalysis.aspx.cs"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerNavigationPathAnalysis %>"%>

<%@ Register TagName="Criteria" TagPrefix="UC" Src="~/UserControls/Navigation/NavigationCriteria.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="rangeHeaderHolder" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".filter-section").on("click", ".toggle-link", function () {
                var container = $("#" + $(this).attr("container-id"));
                var display = $(this).attr("container-display");
                if (display == "hide") {
                    container.show();
                    $(this).attr("container-display", "show").text("Hide Options");
                }
                else {
                    container.hide();
                    $(this).attr("container-display", "hide").text("More Options");
                }
            });
        });

        function GetSortHtml(dataItem) {
            var sortHtml = "";
            if (grdPathAnalysisResults.Levels[0].IndicatedSortColumn == dataItem.ColumnNumber) {
                if (grdPathAnalysisResults.Levels[0].IndicatedSortDirection == 1)
                    sortHtml = '<img class="sort-image" src="../App_Themes/General/images/desc.gif" alt="<%= JSMessages.descending %>" />';
                else
                    sortHtml = '<img class="sort-image" src="../App_Themes/General/images/asc.gif" alt="<%= JSMessages.ascending %>" />';
            }
            return sortHtml;
        }

        function GetHeadingText(dataItem, toolTip) {
            var html = '<div class="custom-header"><span class="header-text">{0}</span>{1}{2}</div>';

            return stringformat(html, dataItem.HeadingText, CreateToolTip(toolTip), GetSortHtml(dataItem));
        }

        var item, pageId = "";
        function ShowPageIcons(dataItem) {
            var numberOfPages = parseInt(dataItem.GetMember('PagePathLength').get_text());
            if (numberOfPages > 0)
                numberOfPages = numberOfPages + 1;

            var $container = $("<div class='page-icons' />");
            for (var i = 0; i < numberOfPages; i++) {
                if (dataItem.get_childTable() != null) {
                    var currentRow = dataItem.get_childTable().getRow(i);
                    if (currentRow != null) {
                        var $element = $("<img src='../App_Themes/General/images/page-icon.gif' rel='tooltip' />");
                        $element.attr("title", stringformat("Page Title: {0}<br />Time on Page: {1}<br />% Time on Page for Complete Path: {2}",
                             currentRow.GetMember('PageTitle').Text,
                             currentRow.GetMember('AvgTimePerPage').Text,
                             GetContentDecimal(currentRow.GetMember('PercentTimeOnPage').Text)));

                        $container.append($element);
                    }
                }
            }

            return $('<div />').append($container.clone()).html();
        }

        function UpdateResults(validate) {
            var result = {};
            var startCriteria = $("#starting_point").navCriteria("getSelectedCriteria");
            result.StartCriteria = startCriteria.SelectedOption;
            result.StartValue = startCriteria.SelectedValue;

            var endCriteria = $("#ending_point").navCriteria("getSelectedCriteria");
            result.EndCriteria = endCriteria.SelectedOption;
            result.EndValue = endCriteria.SelectedValue;

            result.NoOfItems = parseInt($("#ddlNoOfItems").val());

            var isValid = true;
            if (result.StartValue == "") {
                if (validate)
                    alert("Please select or enter a value for starting point.");
                isValid = false;
            }
            else if (result.EndCriteria != "LeaveSite" && result.EndValue == "") {
                if (validate)
                    alert("Please select or enter a value for ending point.");
                isValid = false;
            }

            if (isValid) {
                $(".result-section").show();
                grdPathAnalysisResults.set_callbackParameter(JSON.stringify(result));
                grdPathAnalysisResults.callback();
            }

            return false;
        }

        function grdPathAnalysisResults_onLoad(sender, eventArgs) {
            UpdateResults(false);
        }

        function grdPathAnalysisResults_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            if (item.get_childTable() == null) {
                grdPathAnalysisResults.select(item);
                cmPathAnalysis.showContextMenuAtEvent(eventArgs.get_event(), eventArgs.get_item());
            }
            else {
                cmPathAnalysisParent.showContextMenuAtEvent(eventArgs.get_event(), eventArgs.get_item());
            }
        }

        function cmPathAnalysisParent_onItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            var pagePath = "dsPathScenarios";
            var pageId = item.getMember("Path").get_text();
            switch (selectedMenu) {
                case "CreateScenario":
                    window.location.href = analyticsUrl + "/Navigation/ScenarioAnalysis.aspx?pagePathScenario=" + pagePath + "&pathPageId=" + pageId;
                    break;
            }
        }

        function cmPathAnalysis_onItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            pageId = item.getMember("OriginalId").get_text();
            pageId = pageId.replace("{", "");
            pageId = pageId.replace("}", "");
            switch (selectedMenu) {
                case "PathStart":
                    window.location.href = analyticsUrl + "/Navigation/PathAnalysis.aspx?startPageId=" + pageId;
                    break;
                case "PathEnd":
                    window.location.href = analyticsUrl + "/Navigation/PathAnalysis.aspx?endPageId=" + pageId;
                    break;
                case "ViewStatistics":
                    var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + pageId;
                    viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', '');
                    break;
                case "ViewAnalysis":
                    window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + pageId;
                    break;
                case "ViewNewWindow":
                    window.open(analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text);
                    break;
                case "ViewSiteEditor":
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetToken(cmsProductId, siteId, siteName);
                    break;
                case "ViewOverlay":
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?PageState=Overlay&LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetToken(cmsProductId, siteId, siteName);
                    break;
                case "AssignIndexTerms":
                    OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                    break;
                case "EmailAuthor":
                    location.href = "mailto:" + item.getMember("AuthorEmail").Text;
                    break;
            }
        }

        function ShowEmailReport() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', '../popups/ExportReportAsEmail.aspx', '', '');

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="contentPathAnalysis" ContentPlaceHolderID="rangeContentHolder" runat="Server">
    <div class="path-analysis">
        <div class="filter-section">
            <div class="form-row clear-fix">
                <div class="columns" id="starting_point">
                    <h3>
                        Starting point for analysis
                    </h3>
                    <UC:Criteria ID="startCriteria" runat="server" />
                </div>
                <div class="columns" id="ending_point">
                    <h3>
                        Ending point for analysis
                    </h3>
                    <UC:Criteria ID="endCriteria" runat="server" />
                </div>
                <div class="columns button-column">
                    <div class="form-row">
                        <asp:Button ID="btnUpdate" runat="server" Text="Find Results" CssClass="primarybutton"
                            OnClientClick="return UpdateResults(true);" />
                    </div>
                    <div class="form-row">
                        <a class="toggle-link" container-id="more_options" container-display="hide">More Options</a>
                    </div>
                </div>
            </div>
            <div class="form-row clear-fix">
                <div class="more-options" id="more_options" style="display: none;">
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.Show %></label>
                        <div class="form-value">
                            <asp:DropDownList ID="ddlNoOfItems" runat="server" Width="240" ClientIDMode="Static">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Top5Paths %>" Value="5" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Top10Paths %>" Value="10" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Top15Paths %>" Value="15" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.AudienceSegments1%></label>
                        <div class="form-value scrolling-div">
                            <asp:CheckBoxList ID="chklAudienceSegments" runat="server" RepeatDirection="Vertical"
                                CssClass="checkbox-list" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="result-section" style="display: none;">
            <div class="form-row clear-fix">
                <div class="export-section">
                    <asp:LinkButton ID="lbtnEmail" runat="server" Text="<%$ Resources:GUIStrings, Email1 %>"
                        CssClass="email-icon" ToolTip="<%$ Resources:GUIStrings, Email1 %>" OnClientClick="return ShowEmailReport();" />
                    <asp:LinkButton ID="lbtnExcel" runat="server" Text="<%$ Resources:GUIStrings, ExportToExcel %>"
                        CssClass="excel-icon" ToolTip="<%$ Resources:GUIStrings, ExportToExcel %>" />
                    <asp:LinkButton ID="lbtnPdf" runat="server" Text="<%$ Resources:GUIStrings, ExporttoPDF %>"
                        CssClass="pdf-icon" ToolTip="<%$ Resources:GUIStrings, ExporttoPDF %>" />
                </div>
            </div>
            <ComponentArt:Grid SkinID="Hierarchical" ID="grdPathAnalysisResults" runat="server"
                RunningMode="Client" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
                Width="100%" ShowFooter="false" TreeLineImageWidth="19" TreeLineImageHeight="20"
                IndentCellCssClass="IndentCell" IndentCellWidth="18" DataAreaCssClass="hgDataAreaTable"
                EditOnClickSelectedItem="false" EnableViewState="true" Sort="PathCount desc">
                <Levels>
                    <ComponentArt:GridLevel DataMember="Table1" DataKeyField="Id" HeadingCellCssClass="HeadingCell"
                        HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell"
                        RowCssClass="Row" SelectorCellWidth="18" SelectorImageWidth="18" SelectorImageUrl="last.gif"
                        ShowTableHeading="false" HeadingCellActiveCssClass="HeadingCellActive" SelectedRowCssClass="SelectedRow"
                        GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                        ShowSelectorCells="false" HeadingCellHoverCssClass="HeadingCellHover" ShowFooterRow="false">
                        <Columns>
                            <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Frequency %>"
                                HeadingCellCssClass="FirstHeadingCell" DataField="PathCount" DataCellCssClass="FirstDataCell"
                                SortedDataCellCssClass="SortedDataCell" IsSearchable="true" Align="Right" AllowReordering="false"
                                FixedWidth="true" HeadingCellClientTemplateId="PathFrequencyHT" />
                            <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, ofClicks %>"
                                DataField="PagePathLength" IsSearchable="true" Align="Right" SortedDataCellCssClass="SortedDataCell"
                                AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="ClickHT" />
                            <ComponentArt:GridColumn Width="300" runat="server" HeadingText="<%$ Resources:GUIStrings, UniquePaths %>"
                                DataField="PagePathLength" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                                FixedWidth="true" DataCellClientTemplateId="PageWatches" TextWrap="true" HeadingCellClientTemplateId="UniquePathsHT" />
                            <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, AveragePathTime %>"
                                DataField="Duration" FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                HeadingCellCssClass="LastHeadingCell" HeadingCellClientTemplateId="TimeonpathHT"
                                AllowReordering="false" Align="Right" />
                            <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, AverageTimePerPage %>"
                                DataField="TimePerPage" FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                HeadingCellCssClass="LastHeadingCell" HeadingCellClientTemplateId="TimeonpageHT"
                                AllowReordering="false" Align="Right" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Path" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                    <ComponentArt:GridLevel DataKeyField="Id" DataMember="PageDetails" HeadingCellCssClass="HeadingCell"
                        HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" RowCssClass="Row"
                        DataCellCssClass="DataCell" SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif"
                        ShowTableHeading="false" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
                        SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                        AlternatingRowCssClass="AlternateDataRow" ShowSelectorCells="false" ShowFooterRow="false">
                        <Columns>
                            <ComponentArt:GridColumn DataCellCssClass="BlankDataCell" FixedWidth="true" Width="76"
                                HeadingText="" IsSearchable="false" AllowReordering="false" HeadingCellCssClass="FirstHeadingCell"
                                AllowSorting="False" />
                            <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                                DataField="PageTitle" Width="296" DataCellCssClass="FirstDataCell" IsSearchable="true"
                                AllowReordering="false" FixedWidth="true" TextWrap="true" />
                            <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, TimeonPage %>"
                                DataField="AvgTimePerPage" IsSearchable="true" TextWrap="true" AllowReordering="false"
                                FixedWidth="true" Align="Left" Width="80" DataCellCssClass="DataCell" />
                            <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, FullPath %>"
                                DataField="PercentTimeOnPage" FixedWidth="true" DataCellClientTemplateId="PercentTimeOnPageCT"
                                DataCellCssClass="hgLastDataCell1" SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell hgLastHeadingCellCss"
                                AllowReordering="false" Align="Left" TextWrap="true" Width="70" />
                            <ComponentArt:GridColumn DataCellCssClass="BlankDataCell" FixedWidth="true" Width="139"
                                HeadingText="" HeadingCellCssClass="hgHeadingCell1" DataCellClientTemplateId="BlankCT1"
                                IsSearchable="false" AllowReordering="false" AllowSorting="False" />
                            <ComponentArt:GridColumn DataField="AuthorEmail" Visible="false" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="OriginalId" Visible="false" />
                            <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="BlankCT">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="BlankCT1">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="PercentTimeOnPageCT">
                        <div class="customHeader">
                            ## GetContentDecimal(DataItem.GetMember('PercentTimeOnPage').Value) ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="PageWatches">
                        <div class="customHeader">
                            ## ShowPageIcons(DataItem) ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="PathFrequencyHT">
                        ## GetHeadingText(DataItem, pFrequency) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ClickHT">
                        ## GetHeadingText(DataItem, pClicks) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="UniquePathsHT">
                        ## GetHeadingText(DataItem, pUniquepath) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="TimeonpageHT">
                        ## GetHeadingText(DataItem, pTimeonpage) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="TimeonpathHT">
                        ## GetHeadingText(DataItem, pTimeonpath) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
                <ClientEvents>
                    <ContextMenu EventHandler="grdPathAnalysisResults_onContextMenu" />
                    <Load EventHandler="grdPathAnalysisResults_onLoad" />
                    <CallbackComplete EventHandler="InitializeToolTip" />
                </ClientEvents>
            </ComponentArt:Grid>
            <ComponentArt:Menu ID="cmPathAnalysis" runat="server" SkinID="ContextMenu">
                <Items>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, PathAnalysisStart %>"
                        ID="PathStart" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, PathAnalysisEnd %>"
                        ID="PathEnd" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>"
                        ID="ViewStatistics" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewInboundOutboundAnalysis %>"
                        ID="ViewAnalysis" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
                        ID="ViewSiteEditor" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinOverlayMode %>"
                        ID="ViewOverlay" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>"
                        ID="AssignIndexTerms" />
                    <ComponentArt:MenuItem LookId="BreakItem" />
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>"
                        ID="EmailAuthor" ToolTip="<%$ Resources:GUIStrings, EmailAuthor %>" />
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="cmPathAnalysis_onItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
            <ComponentArt:Menu ID="cmPathAnalysisParent" runat="server" SkinID="ContextMenu">
                <Items>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, CreateScenariofromthispath %>"
                        ID="CreateScenario" ToolTip="<%$ Resources:GUIStrings, CreateScenariofromthispath %>">
                    </ComponentArt:MenuItem>
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="cmPathAnalysisParent_onItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/RangeMaster.master" AutoEventWireup="true" StylesheetTheme="General" runat="server"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.ScenarioResults" CodeBehind="ScenarioResults.aspx.cs"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerNavigationScenarioAnalysis %>" %>

<asp:Content ID="head" ContentPlaceHolderID="rangeHeaderHolder" runat="server">
    <script type="text/javascript">
        function grdStrictOrder_onRenderComplete(sender, args) {
            for (var i = 0; i < grdStrictOrder.get_table().getRowCount() ; i++) {
                var childTable = grdStrictOrder.get_table().getRow(i).get_childTable();
                if (childTable != null) {
                    var toolTip = "";
                    for (var j = 0; j < childTable.getRowCount() ; j++) {
                        toolTip += "<li>" + childTable.getRow(j).GetMember("PageTitle").Text + "</li>";
                    }
                    toolTip = CreateToolTip("<ul>" + toolTip + "</ul>");
                    $("#graphTitle_" + i).html($("#graphTitle_" + i).html() + toolTip);
                }
            }
        }

        function grdAnalysisGraph_onRenderComplete(sender, args) {
            var baseWidth = 280;
            var color = '#8e6da2';
            $(".graph-row-container").each(function () {
                var $parentRow = $(this).parents("tr.Row");
                var rowIndex = $parentRow.prevAll().length;

                var width = baseWidth - (rowIndex * 20);
                var rgbaCol = 'rgba(' + parseInt(color.slice(-6, -4), 16)
                    + ',' + parseInt(color.slice(-4, -2), 16)
                    + ',' + (parseInt(color.slice(-2), 16) + (rowIndex * 2))
                    + ',1)';

                $(this).width(width);
                $(this).find(".graph-row").css(
                {
                    "background-color": rgbaCol,
                    "width": width - 20
                });
            });
        }

        function grdStrictOrder_onItemSelect(sender, eventArgs) {
            var gridItem = eventArgs.get_item();
            var selectedItems = grdStrictOrder.getSelectedItems();
            if (selectedItems.length > 2) {
                alert("<%= JSMessages.Selectmaximumof2pagestorunPathAnalysis %>");
                grdStrictOrder.unSelect(gridItem);
            }
        }

        function grdStrictOrder_onContextMenu(sender, eventArgs) {
            var selectedItems = grdStrictOrder.getSelectedItems();
            var menuItems = cmStrictOrder.get_items();
            var showContextMenu = true;
            if (selectedItems.length == 2) {
                if (selectedItems[0].GetMember("Type").Text == "PG" || selectedItems[1].GetMember("Type").Text == "PG") {
                    alert("<%= JSMessages.Pleaseselectapageorawatchtorunpathanalysis %>");
                    showContextMenu = false;
                }
                menuItems.getItem(0).set_visible(true);
                menuItems.getItem(1).set_visible(false);
                menuItems.getItem(2).set_visible(false);
            }
            else {
                strictItem = eventArgs.get_item();
                grdStrictOrder.select(strictItem);
                if (strictItem.GetMember("Type").Text == "PG" || strictItem.GetMember("Type").Text == "W" || strictItem.GetMember("PageTitle").Text == '<%=GUIStrings.ConversionRate %>')
                    showContextMenu = false;
                menuItems.getItem(0).set_visible(false);
                menuItems.getItem(1).set_visible(true);
                menuItems.getItem(2).set_visible(true);
            }
            var evt = eventArgs.get_event();
            cmStrictOrder.showContextMenuAtEvent(evt, eventArgs.get_item());
            if (!showContextMenu) {
                cmStrictOrder.hide();
            }
        }

        function cmStrictOrder_onItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            var selectedItems = grdStrictOrder.getSelectedItems();
            var startVar = "";
            var endVar = "";
            if (selectedItems.length == 2) {
                var startpageWatchId = selectedItems[0].GetMember("PageId").Text;
                var endpageWatchId = selectedItems[1].GetMember("PageId").Text;
            }
            else {
                selectedItems = eventArgs.get_item();
            }
            switch (selectedMenu) {
                case "StrictPath":
                    startVar = selectedItems[0].GetMember("Type").Text == "P" || selectedItems[0].GetMember("Type").Text == "GM" ? "startPageId" : "startWatchId";
                    endVar = selectedItems[1].GetMember("Type").Text == "P" || selectedItems[1].GetMember("Type").Text == "GM" ? "endPageId" : "endWatchId";
                    window.location.href = stringformat("{0}/Navigation/PathAnalysis.aspx?{1}={2}&{3}={4}&selectedSegments=All", jAnalyticAdminSiteUrl, startVar, startpageWatchId, endVar, endpageWatchId);
                    break;
                case "EmailAuthor":
                    location.href = "mailto:" + strictItem.getMember("AuthorEmail").Text;
                    break;
                case "AbandonmentPath":
                    var isPage = strictItem.getMember('PageId').get_value();
                    viewPopupWindow = dhtmlmodal.open('Content', 'iframe', '../popups/AbandonmentPath.aspx?isPage=' + isPage, '', '');
                    break;
                case "ViewSiteEditor":
                    var pageId = strictItem.getMember('PageId').get_value();
                    JumpToFrontPage(GetTokenWithReferrerURL(cmsProductId, siteId, siteName, pageId, "?"));
                    break;
            }
        }

        function grdAnyOrder_onItemSelect(sender, eventArgs) {
            var gridItem = eventArgs.get_item();
            var selectedItems = grdAnyOrder.getSelectedItems();
            if (selectedItems.length > 2) {
                alert("<%= JSMessages.Selectmaximumof2pagestorunPathAnalysis %>");
                grdAnyOrder.unSelect(gridItem);
            }
        }

        function grdAnyOrder_onContextMenu(sender, eventArgs) {
            var selectedItems = grdAnyOrder.getSelectedItems();
            var showContextMenu = true;
            var menuItems = cmAnyOrder.get_items();
            if (selectedItems.length == 2) {
                if (selectedItems[0].GetMember("Type").Text == "PG" || selectedItems[1].GetMember("Type").Text == "PG") {
                    alert("<%= JSMessages.Pleaseselectapageorawatchtorunpathanalysis %>");
                    showContextMenu = false;
                }
                menuItems.getItem(0).set_visible(true);
                menuItems.getItem(1).set_visible(false);
                menuItems.getItem(2).set_visible(false);
            }
            else {
                anyItem = eventArgs.get_item();
                grdAnyOrder.select(anyItem);
                if (anyItem.GetMember("Type").Text == "PG" || anyItem.GetMember("Type").Text == "W")
                    showContextMenu = false;
                menuItems.getItem(0).set_visible(false);
                menuItems.getItem(1).set_visible(true);
                menuItems.getItem(2).set_visible(true);
            }

            var evt = eventArgs.get_event();
            cmAnyOrder.showContextMenuAtEvent(evt, eventArgs.get_item());
            if (!showContextMenu) {
                cmAnyOrder.hide();
            }
        }

        function cmAnyOrder_onItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            var selectedItems = grdAnyOrder.getSelectedItems();
            var startVar = "";
            var endVar = "";
            if (selectedItems.length == 2) {
                var startpageWatchId = selectedItems[0].GetMember("Type").Text == "P" || selectedItems[0].GetMember("Type").Text == "GM" ? selectedItems[0].GetMember("PageID").Text : selectedItems[0].GetMember("WatchID").Text;
                var endpageWatchId = selectedItems[1].GetMember("Type").Text == "P" || selectedItems[1].GetMember("Type").Text == "GM" ? selectedItems[1].GetMember("PageID").Text : selectedItems[1].GetMember("WatchID").Text;
            }
            else {
                selectedItems = eventArgs.get_item();
            }
            switch (selectedMenu) {
                case "AnyOrderPath":
                    startVar = selectedItems[0].GetMember("Type").Text == "P" || selectedItems[0].GetMember("Type").Text == "GM" ? "startPageId" : "startWatchId";
                    endVar = selectedItems[1].GetMember("Type").Text == "P" || selectedItems[1].GetMember("Type").Text == "GM" ? "endPageId" : "endWatchId";
                    window.location.href = stringformat("{0}/Navigation/PathAnalysis.aspx?{1}={2}&{3}={4}&selectedSegments=All", jAnalyticAdminSiteUrl, startVar, startpageWatchId, endVar, endpageWatchId);
                    break;
                case "AOEmailAuthor":
                    location.href = "mailto:" + anyItem.getMember("AuthorEmail").Text;
                    break;
                case "AbandonmentPathOrder":
                    var isPageOrder = anyItem.getMember('PageID').get_value();
                    viewPopupWindow = dhtmlmodal.open('Content', 'iframe', '../popups/AbandonmentPath.aspx?isPage=' + isPageOrder, '', '');
                    break;
                case "AOViewSiteEditor":
                    var pageId = anyItem.getMember('PageID').get_value();
                    JumpToFrontPage(GetTokenWithReferrerURL(cmsProductId, siteId, siteName, pageId, "?"));
                    break;
            }
        }

        function ShowEmailReport() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', '../popups/ExportReportAsEmail.aspx', '', '');

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="contentScenarioAnallysis" ContentPlaceHolderID="rangeContentHolder"
    runat="server">
    <div class="scenario-analysis">
        <div class="form-row clear-fix">
            <div class="export-section">
                <asp:LinkButton ID="lbtnEmail" runat="server" Text="<%$ Resources:GUIStrings, Email1 %>"
                    CssClass="email-icon" ToolTip="<%$ Resources:GUIStrings, Email1 %>" OnClientClick="return ShowEmailReport();" />
                <asp:LinkButton ID="lbtnExcel" runat="server" Text="<%$ Resources:GUIStrings, ExportToExcel %>"
                    CssClass="excel-icon" ToolTip="<%$ Resources:GUIStrings, ExportToExcel %>" />
                <asp:LinkButton ID="lbtnPdf" runat="server" Text="<%$ Resources:GUIStrings, ExporttoPDF %>"
                    CssClass="pdf-icon" ToolTip="<%$ Resources:GUIStrings, ExporttoPDF %>" />
            </div>
        </div>
        <div class="result-section">
            <ComponentArt:Grid ID="grdAnalysisGraph" runat="server" RunningMode="Client" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
                AutoPostBackOnSelect="false" PageSize="20" AllowEditing="false" AutoCallBackOnInsert="false"
                AutoCallBackOnUpdate="false" AutoCallBackOnDelete="false" EditOnClickSelectedItem="false"
                CssClass="Grid SAGrid" ShowFooter="false" Width="100%">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="pageWatchId" ShowTableHeading="false" ShowSelectorCells="false"
                        RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                        HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                        HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                        HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="Row" GroupHeadingCssClass="GroupHeading"
                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                        SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="Row">
                        <Columns>
                            <ComponentArt:GridColumn Width="300" HeadingText=" " DataField="PageTitle" AllowReordering="false"
                                FixedWidth="true" AllowSorting="False" DataCellClientTemplateId="DataCT" DataCellCssClass="FirstDataCell" />
                            <ComponentArt:GridColumn Width="300" runat="server" HeadingText="<%$ Resources:GUIStrings, StrictOrderFunnel %>"
                                DataField="hitCount" AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="StrictOrderGraph"
                                Align="Center" AllowSorting="False" />
                            <ComponentArt:GridColumn Width="300" runat="server" HeadingText="<%$ Resources:GUIStrings, AnyOrder %>"
                                DataField="AnyOrder" AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="AnyOrderGraph"
                                Align="Center" AllowSorting="False" />
                            <ComponentArt:GridColumn DataField="pageWatchId" Visible="false" />
                            <ComponentArt:GridColumn DataField="isGroup" Visible="false" />
                            <ComponentArt:GridColumn DataField="gropuId" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="DataCT">
                        <div title="##DataItem.GetMember('PageTitle').Text##" class="graphTitle" id="graphTitle_##DataItem.get_index()##">
                            ## DataItem.GetMember('PageTitle').Text ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="StrictOrderGraph">
                        <div class="graph-row-container">
                            <div class="## DataItem.get_index() == 0 ? 'graph-row top-graph-row' : 'graph-row' ##">
                                ## DataItem.GetMember('hitCount').Text ##
                            </div>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="AnyOrderGraph">
                        <div class="graph-row-container">
                            <div class="## DataItem.get_index() == 0 ? 'graph-row top-graph-row' : 'graph-row' ##">
                                ## DataItem.GetMember('AnyOrder').Text ##
                            </div>
                        </div>
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
                <ClientEvents>
                    <RenderComplete EventHandler="grdAnalysisGraph_onRenderComplete" />
                </ClientEvents>
            </ComponentArt:Grid>
            <div class="bottom-section clear-fix">
                <div class="left-column">
                    <h3>
                        <%= GUIStrings.StrictOrderFunnel %></h3>
                    <ComponentArt:Grid ID="grdStrictOrder" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
                        SkinID="Hierarchical" RunningMode="Client" runat="server" IndentCellWidth="10"
                        DataAreaCssClass="hgDataAreaTable" Width="100%" EditOnClickSelectedItem="false"
                        AllowEditing="false" AllowMultipleSelect="true" ShowFooter="false">
                        <Levels>
                            <ComponentArt:GridLevel DataKeyField="gropuId" DataMember="StrictOrderParent" HeadingCellCssClass="HeadingCell"
                                HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell"
                                RowCssClass="Row" SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif"
                                ShowTableHeading="false" HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
                                SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                                SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                                AlternatingRowCssClass="AlternateDataRow" ShowSelectorCells="false">
                                <Columns>
                                    <ComponentArt:GridColumn DataField="PageTitle" AllowEditing="False" Align="left"
                                        AllowSorting="false" Width="150" AllowReordering="false" runat="server" HeadingText="<%$ Resources:GUIStrings, LevelofPages %>"
                                        FixedWidth="true" DataCellClientTemplateId="PageNameStrictCT" />
                                    <ComponentArt:GridColumn DataField="hitCount" AllowEditing="True" HeadingCellClientTemplateId="ViewsTMP"
                                        Align="right" Width="70" AllowReordering="false" FixedWidth="true" AllowSorting="false" />
                                    <ComponentArt:GridColumn DataField="Previous" AllowEditing="True" HeadingCellClientTemplateId="DropTMP"
                                        Align="right" Width="75" AllowReordering="false" FixedWidth="true" AllowSorting="false"
                                        DataCellClientTemplateId="DropoutSOCT" />
                                    <ComponentArt:GridColumn DataField="DropOut" Visible="false" />
                                    <ComponentArt:GridColumn DataField="pageWatchId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="PageId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="Type" Visible="false" />
                                    <ComponentArt:GridColumn DataField="gropuId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="WatchID" Visible="false" />
                                    <ComponentArt:GridColumn DataField="AuthorEmail" Visible="false" />
                                </Columns>
                                <ConditionalFormats>
                                    <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('PageTitle').Value=='<%$ Resources:GUIStrings, ConversionRate %>'"
                                        RowCssClass="BoldRow" SelectedRowCssClass="BoldSelectedRow" />
                                </ConditionalFormats>
                            </ComponentArt:GridLevel>
                            <ComponentArt:GridLevel DataMember="StrictOrderChild" DataKeyField="UniqueId" HeadingCellCssClass="HeadingCell"
                                HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell"
                                RowCssClass="Row" SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif"
                                ShowTableHeading="false" HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
                                SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                                SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                                AlternatingRowCssClass="AlternateDataRow" ShowSelectorCells="false" ShowHeadingCells="false">
                                <Columns>
                                    <ComponentArt:GridColumn DataField="PageTitle" AllowEditing="True" Align="Left" Width="139"
                                        AllowReordering="false" />
                                    <ComponentArt:GridColumn DataField="hitCount" AllowEditing="True" HeadingCellClientTemplateId="ViewsTMP"
                                        Align="right" Width="70" AllowReordering="false" FixedWidth="true" AllowSorting="false" />
                                    <ComponentArt:GridColumn DataField="Previous" DataCellClientTemplateId="DropoutSOCT"
                                        AllowEditing="True" HeadingCellClientTemplateId="DropTMP" Align="right" Width="75"
                                        AllowReordering="false" FixedWidth="true" AllowSorting="false" />
                                    <ComponentArt:GridColumn DataField="DropOut" Visible="false" />
                                    <ComponentArt:GridColumn DataField="pageWatchId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="PageId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="gropuId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="UniqueId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="Type" Visible="false" />
                                    <ComponentArt:GridColumn DataField="WatchID" Visible="false" />
                                    <ComponentArt:GridColumn DataField="AuthorEmail" Visible="false" />
                                </Columns>
                            </ComponentArt:GridLevel>
                        </Levels>
                        <ClientTemplates>
                            <ComponentArt:ClientTemplate ID="PreviousSOCT">
                                ## DataItem.GetMember('DropOut').Text == "" ? "-" : GetContentDecimal(DataItem.GetMember('DropOut').Value)
                                ##
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="DropoutSOCT">
                                ## DataItem.GetMember('Previous').Text == "" ? "-" : GetContentDecimal(DataItem.GetMember('Previous').Value)
                                ##
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="PageNameStrictCT">
                                <div title="## DataItem.GetMember('PageTitle').Text ##">
                                    ## DataItem.GetMember('PageTitle').Text ##
                                </div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="ViewsTMP">
                                <div class="custom-header">
                                    <span class="header-text">Views</span>
                                    ## CreateToolTip(__JSMessages["snViews"]) ##
                                </div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="DropTMP">
                                <div class="custom-header">
                                    <span class="header-text">Drop %</span>
                                    ## CreateToolTip(__JSMessages["snDrop"]) ##
                                </div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="PrevTMP">
                                <div class="custom-header">
                                    <span class="header-text">% Prev Step</span>
                                    ## CreateToolTip(__JSMessages["snPrevStep"]) ##
                                </div>
                            </ComponentArt:ClientTemplate>
                        </ClientTemplates>
                        <ClientEvents>
                            <ContextMenu EventHandler="grdStrictOrder_onContextMenu" />
                            <ItemSelect EventHandler="grdStrictOrder_onItemSelect" />
                            <RenderComplete EventHandler="grdStrictOrder_onRenderComplete" />
                        </ClientEvents>
                    </ComponentArt:Grid>
                    <ComponentArt:Menu ID="cmStrictOrder" runat="server" SkinID="ContextMenu">
                        <Items>
                            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPathAnalysis %>"
                                ID="StrictPath" />
                            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewAbandonmentPath %>"
                                ID="AbandonmentPath" />
                            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>"
                                ID="EmailAuthor" />
                            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
                                ID="ViewSiteEditor" />
                        </Items>
                        <ClientEvents>
                            <ItemSelect EventHandler="cmStrictOrder_onItemSelect" />
                        </ClientEvents>
                    </ComponentArt:Menu>
                </div>
                <div class="right-column">
                    <h3>
                        <%= GUIStrings.AnyOrder %></h3>
                    <ComponentArt:Grid ID="grdAnyOrder" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
                        SkinID="Hierarchical" RunningMode="Client" runat="server" TreeLineImageWidth="0"
                        IndentCellWidth="10" DataAreaCssClass="DataAreaTable" EditOnClickSelectedItem="false"
                        AllowEditing="false" Width="100%" ShowFooter="false">
                        <Levels>
                            <ComponentArt:GridLevel DataKeyField="gropuId" DataMember="AnyOrderParent" HeadingCellCssClass="HeadingCell"
                                HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell"
                                RowCssClass="Row" SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif"
                                ShowTableHeading="false" HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
                                SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                                SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                                AlternatingRowCssClass="AlternateDataRow" ShowSelectorCells="false">
                                <Columns>
                                    <ComponentArt:GridColumn DataField="PageTitle" AllowEditing="False" FixedWidth="true"
                                        Align="left" AllowSorting="false" Width="230" AllowReordering="false" runat="server"
                                        HeadingText="<%$ Resources:GUIStrings, LevelofPages %>" DataCellClientTemplateId="PageNameCT" />
                                    <ComponentArt:GridColumn DataField="hitCount" AllowEditing="True" FixedWidth="true"
                                        AllowSorting="false" Align="right" Width="70" AllowReordering="false" HeadingCellClientTemplateId="ViewsAOTMP" />
                                    <ComponentArt:GridColumn DataField="Previous" Visible="false" />
                                    <ComponentArt:GridColumn DataField="DropOut" Visible="false" />
                                    <ComponentArt:GridColumn DataField="pageWatchId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="PageID" Visible="false" />
                                    <ComponentArt:GridColumn DataField="Type" Visible="false" />
                                    <ComponentArt:GridColumn DataField="gropuId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="WatchID" Visible="false" />
                                    <ComponentArt:GridColumn DataField="AuthorEmail" Visible="false" />
                                </Columns>
                            </ComponentArt:GridLevel>
                            <ComponentArt:GridLevel DataKeyField="UniqueId" DataMember="AnyOrderChild" HeadingCellCssClass="HeadingCell"
                                HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell"
                                RowCssClass="Row" SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif"
                                ShowTableHeading="false" HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
                                SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                                SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                                AlternatingRowCssClass="AlternateDataRow" ShowSelectorCells="false" ShowHeadingCells="false">
                                <Columns>
                                    <ComponentArt:GridColumn DataField="PageTitle" AllowEditing="True" Align="Left" Width="218"
                                        AllowReordering="false" />
                                    <ComponentArt:GridColumn DataField="hitCount" AllowEditing="True" Align="Right" Width="70"
                                        AllowReordering="false" />
                                    <ComponentArt:GridColumn DataField="Previous" DataCellClientTemplateId="DropOutCT"
                                        AllowEditing="True" Align="Right" Width="55" AllowReordering="false" Visible="false" />
                                    <ComponentArt:GridColumn DataField="DropOut" Visible="false" />
                                    <ComponentArt:GridColumn DataField="pageWatchId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="gropuId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="PageID" Visible="false" />
                                    <ComponentArt:GridColumn DataField="Type" Visible="false" />
                                    <ComponentArt:GridColumn DataField="UniqueId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="WatchID" Visible="false" />
                                    <ComponentArt:GridColumn DataField="AuthorEmail" Visible="false" />
                                </Columns>
                            </ComponentArt:GridLevel>
                        </Levels>
                        <ClientEvents>
                            <ItemSelect EventHandler="grdAnyOrder_onItemSelect" />
                            <ContextMenu EventHandler="grdAnyOrder_onContextMenu" />
                        </ClientEvents>
                        <ClientTemplates>
                            <ComponentArt:ClientTemplate ID="PreviousCT">
                                ## DataItem.GetMember('DropOut').Text == "" ? "-" : GetContentDecimal(DataItem.GetMember('DropOut').Value)
                                ##
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="DropOutCT">
                                ## DataItem.GetMember('Previous').Text == "" ? "-" : GetContentDecimal(DataItem.GetMember('Previous').Value)
                                ##
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="PageNameCT">
                                <div title="## DataItem.GetMember('PageTitle').Text ##">
                                    ## DataItem.GetMember('PageTitle').Text ##
                                </div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="ViewsAOTMP">
                                <div class="custom-header">
                                    <span class="header-text">Views</span>
                                    ## CreateToolTip(__JSMessages["snViews"]) ##
                                </div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="DropAOTMP">
                                <div class="custom-header">
                                    <span class="header-text">Drop %</span>
                                    ## CreateToolTip(__JSMessages["snDrop"]) ##
                                </div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="PrevAOTMP">
                                <div class="custom-header">
                                    <span class="header-text">% Prev Step</span>
                                    ## CreateToolTip(__JSMessages["snPrevStep"]) ##
                                </div>
                            </ComponentArt:ClientTemplate>
                        </ClientTemplates>
                    </ComponentArt:Grid>
                    <ComponentArt:Menu ID="cmAnyOrder" runat="server" SkinID="ContextMenu">
                        <Items>
                            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPathAnalysis %>"
                                ID="AnyOrderPath" />
                            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewAbandonmentPath %>"
                                ID="AbandonmentPathOrder" />
                            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>"
                                ID="AOEmailAuthor" />
                            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
                                ID="AOViewSiteEditor" />
                        </Items>
                        <ClientEvents>
                            <ItemSelect EventHandler="cmAnyOrder_onItemSelect" />
                        </ClientEvents>
                    </ComponentArt:Menu>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/RangeMaster.master" AutoEventWireup="true" StylesheetTheme="General" runat="server" 
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.ScenarioDetails" CodeBehind="ScenarioDetails.aspx.cs"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerNavigationScenarioAnalysis %>"%>

<asp:Content ID="head" ContentPlaceHolderID="rangeHeaderHolder" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".scenario-analysis").on("click", "#hplAddNewStep", function () {
                cbScenarioSteps.callback("Add", GetStepsJson());
            });
        });

        function cbScenarioSteps_Load(sender, eventArgs) {
            $(".nav-criteria").each(function () {
                $("#" + $(this).attr("id")).navCriteria();
            });
        }

        function GetStepsJson() {
            var results = [];
            $(".nav-criteria").each(function () {
                var result = $("#" + $(this).attr("id")).navCriteria("getSelectedCriteria");
                results.push(result);
            });

            return JSON.stringify(results);
        }

        function OnSaveScenario() {
            if ($("#txtScenarioName").val() == "") {
                alert("<%= JSMessages.ScenarioNameRequired %>");
                return false;
            }

            var resultString = GetStepsJson();
            var result = JSON.parse(resultString);
            var isValid = true;
            $.each(result, function () {
                if (this.SelectedValue == "") {
                    isValid = false;
                    alert(stringformat("Please select or enter a value for {0}.", this.Title));
                    return false;
                }
            });

            $("#hdnStepsJson").val(resultString);

            return isValid;
        }
    </script>
</asp:Content>
<asp:Content ID="scenarioHeaderButtons" ContentPlaceHolderID="headerButtons" runat="server">
    <asp:HyperLink ID="hplCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" NavigateUrl="~/Navigation/ScenarioAnalysis.aspx" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        CssClass="button primarybutton" OnClientClick="return OnSaveScenario();" />
    <asp:HyperLink ID="hplRunScenario" runat="server" Text="<%$ Resources:GUIStrings, RunScenario %>"
        CssClass="button primarybutton" />
    <asp:HiddenField ID="hdnScenarioId" runat="server" />
</asp:Content>
<asp:Content ID="contentScenarioAnallysis" ContentPlaceHolderID="rangeContentHolder"
    runat="server">
    <div class="scenario-analysis">
        <asp:Panel ID="pnlResultSection" runat="server" CssClass="result-section clear-fix">
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.ScenarioName1 %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtScenarioName" runat="server" Width="290" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Showscenarioto %></label>
                <div class="form-value">
                    <asp:RadioButtonList ID="rbtnShowScenario" runat="server" CssClass="radio-list" RepeatDirection="Horizontal">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, ShowScenarioToMe %>" Value="0" Selected="True" />
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Everyone %>" Value="1" />
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.AudienceSegments1 %></label>
                <div class="form-value scrolling-div">
                    <asp:CheckBoxList runat="server" ID="chklAudienceSegments" RepeatDirection="Vertical"
                        CssClass="checkbox-list" />
                </div>
            </div>
            <div class="page-sub-header clear-fix">
                <h3>
                    Steps</h3>
                <asp:HyperLink ID="hplAddNewStep" runat="server" Text="Add New Step" CssClass="action-link"
                    ClientIDMode="Static" />
            </div>
            <ComponentArt:CallBack ID="cbScenarioSteps" runat="server">
                <Content>
                </Content>
                <ClientEvents>
                    <Load EventHandler="cbScenarioSteps_Load" />
                    <CallbackComplete EventHandler="cbScenarioSteps_Load" />
                </ClientEvents>
            </ComponentArt:CallBack>
            <asp:HiddenField ID="hdnStepsJson" runat="server" ClientIDMode="Static" />
        </asp:Panel>
    </div>
</asp:Content>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopOpportunities.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.TopOpportunities" %>
<script type="text/javascript">
    var itemTopOpportunities;
    function ShowSeverity(objDataItem) {
        var itemSeverity = objDataItem.getMember('Severity').get_text();
        switch (itemSeverity) {
            case '1':
                return '<img src="../App_Themes/General/images/big-green-bullet.png" alt="<%= JSMessages.Easy %>" />';
                break;
            case '2':
                return '<img src="../App_Themes/General/images/big-yellow-bullet.png" alt="<%= JSMessages.Moderate %>" />';
                break;
            case '3':
                return '<img src="../App_Themes/General/images/big-red-bullet.png" alt="<%= JSMessages.Difficult %>" />';
                break;
        }
    }

    function ShowLevelOfEffort(objDataItem) {
        var itemSeverity = objDataItem.getMember('levelofeffort').get_text();
        switch (itemSeverity) {
            case '3':
                return '<%= JSMessages.Easy %>';
                break;
            case '2':
                return '<%= JSMessages.Moderate %>';
                break;
            case '1':
                return '<%= JSMessages.Difficult %>';
                break;
        }
    }

    function grdTopOpportunities_onContextMenu(sender, eventArgs) {
        var evt = eventArgs.get_event();
        itemTopOpportunities = eventArgs.get_item();
        grdTopOpportunities.select(itemTopOpportunities);
        cmTopOpportunities.showContextMenuAtEvent(evt);
    }
    function cmTopOpportunities_onItemSelect(sender, eventArgs) {
        var itemSelected = eventArgs.get_item().get_id();
        var recomId = itemTopOpportunities.getMember('ALRecommendationOriginalID').get_text();
        var goalId = itemTopOpportunities.getMember('GoalOriginalID').get_text();
        var landingPageId = itemTopOpportunities.getMember('LandingPageId').get_text();
        switch (itemSelected) {
            case 'cmAction':
                OpenTakeActionPopup(recomId, goalId, landingPageId);
                break;
        }
    }
    function OpenTakeActionPopup(id, goalId, landingPageId) {
        var url = jAnalyticAdminSiteUrl + "/Popups/TakeAction.aspx?RecommendationId=" + id + "&GoalId=" + goalId + "&LandingPageId=" + landingPageId;
        takeActionWindow = dhtmlmodal.open('TakeAction', 'iframe', url, '<asp:localize ID="Localize1" runat="server" text="<%$ Resources:JSMessages, TakeAction %>"/>', 'width=415px,height=350px,center=1,resize=0,scrolling=1');
        takeActionWindow.onclose = function () {
            var a = document.getElementById('TakeAction');
            var ifr = a.getElementsByTagName("iframe");
            window.frames[ifr[0].name].location.replace("/blank.html");
            return true;
        }
    }
    function CloseTakeActionAndRedirect(url, productId, site, productString, userId, userName) {
        var token = GetTokenWithUserName(productId, site, productString, userId, userName);
        url = url.replace('[Token]', token);
        takeActionWindow.hide();
        window.location = url;
        return false;
    }
    var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
</script>
<ComponentArt:Menu ID="cmTopOpportunities" runat="server" SkinID="ContextMenu">
    <Items>
        <ComponentArt:MenuItem ID="cmAction" runat="server" Text="<%$ Resources:GUIStrings, TakeAction %>"></ComponentArt:MenuItem>
    </Items>
    <ClientEvents>
        <ItemSelect EventHandler="cmTopOpportunities_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
<ComponentArt:Grid ID="grdTopOpportunities" runat="server" SkinID="Default" RunningMode="Client" FooterCssClass="GridFooter FooterBorder"
    EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" PageSize="5" Width="100%"
    EditOnClickSelectedItem="false" PagerInfoClientTemplateId="CustomPageTemplate" Sort="Severity desc,levelofeffort desc">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="UniqueId" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow" SortedHeadingCellCssClass="HeadingRow"
            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
            <Columns>
                <ComponentArt:GridColumn DataField="UniqueId" Visible="false" />
                <ComponentArt:GridColumn DataField="ALRecommendationOriginalID" Visible="false" />
                <ComponentArt:GridColumn DataField="Severity" Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Severity %>" FixedWidth="true"
                    AllowReordering="False" DataCellClientTemplateId="SeverityCT" Align="Center" DataType="System.Int32" DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell" />
                <ComponentArt:GridColumn DataField="levelofeffort" Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, LevelofEffort %>" FixedWidth="true"
                    AllowReordering="False" Align="Center" DataCellClientTemplateId="levelofeffortCT" />
                <ComponentArt:GridColumn DataField="description" Width="945" runat="server" HeadingText="<%$ Resources:GUIStrings, IssueDescription1 %>" FixedWidth="true"
                    AllowReordering="False" DataCellClientTemplateId="IssueDescCT" DataType="System.String" AllowSorting="False"
                    TextWrap="true" />
                <ComponentArt:GridColumn DataField="GoalOriginalID" Visible="false" />
                <ComponentArt:GridColumn DataField="LandingPageId" Visible="false" />
                <ComponentArt:GridColumn DataField="GoalName" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdTopOpportunities_onContextMenu" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="SeverityCT">
            ## ShowSeverity(DataItem) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="levelofeffortCT">
            ## ShowLevelOfEffort(DataItem) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="IssueDescCT">
            <span title="## DataItem.getMember('description').get_text() ##">## (DataItem.getMember('GoalName').get_text()!='' ? DataItem.getMember('GoalName').get_text() + ' : ' : '') + DataItem.getMember('description').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CustomPageTemplate">
            ## GetGridPaginationInfo(grdTopOpportunities) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>

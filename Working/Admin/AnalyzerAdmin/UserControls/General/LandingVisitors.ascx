<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_General_LandingVisitors" Codebehind="LandingVisitors.ascx.cs" %>
<%--<%@ outputcache duration="200" varybycontrol="ReportStartDate,ReportEndDate" %>--%>
<script type="text/javascript">
    var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
    var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
    var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams

/** Methods to set the pagination details **/
function currentPageIndexV()
{
    var currentPage;
    (grdVisitorsLanding.RecordCount == 0) ? (currentPage = 1) : (currentPage = grdVisitorsLanding.CurrentPageIndex + 1);
    return currentPage;
}
function pageCountV()
{
    var pageCount;
    (grdVisitorsLanding.RecordCount == 0) ? (pageCount = 1) : (pageCount = grdVisitorsLanding.PageCount);
    return pageCount;
}
function grdVisitorsLanding_onLoad(sender, eventArgs)
{
  grdVisitorsLanding.sort(1,true);
}

/** Methods to set the pagination details **/
</script>
<ComponentArt:Snap ID="snapVisitors" runat="server" DockingContainers="rightContentSection"
    CurrentDockingContainer="rightContentSection" DockingStyle="TransparentRectangle" MustBeDocked="true"
    DraggingMode="Vertical" DraggingStyle="SolidOutline">
    <Header>
        <div class="topShadowBox">
            <div class="headerContainer">
                <div class="dragBox" onmousedown="snapVisitors.startDragging(event);">
                    <h3><asp:localize runat="server" text="<%$ Resources:GUIStrings, Visitors %>"/></h3><span>[&nbsp;<asp:HyperLink ID="linkVisitors" runat="server" Text="<%$ Resources:GUIStrings, ViewFullReport %>" NavigateUrl="~/Statistics/Visitors/Visitors.aspx"></asp:HyperLink>&nbsp;]</span>
                </div>
                <div class="toggleBox">
                    <img src="../../images/snap-minimize.gif" runat="server" alt="<%$ Resources:GUIStrings, MinimizeSnap %>" onclick="snapVisitors.toggleExpand();" />
                </div>
                <div class="tabContainer">
                    <ComponentArt:TabStrip ID="tabstripVisitors" SkinID="Vista" runat="server"
                        MultiPageId="">
                        <Tabs>
                            <ComponentArt:TabStripTab ID="tabVisitors" runat="server" Text="<%$ Resources:GUIStrings, Visitors %>" Tooltip="<%$ Resources:GUIStrings, Visitors %>"></ComponentArt:TabStripTab>
                        </Tabs>
                    </ComponentArt:TabStrip>
                </div>
            </div>
        </div>
    </Header>
    <CollapsedHeader>
        <div class="topShadowBox">
            <div class="headerContainer">
                <div class="dragBox" onmousedown="snapVisitors.startDragging(event);">
                    <h3><asp:localize runat="server" text="<%$ Resources:GUIStrings, Visitors %>"/></h3><span>[&nbsp;<asp:HyperLink ID="linkVisitorsCollapsed" runat="server" Text="<%$ Resources:GUIStrings, ViewFullReport %>" NavigateUrl="~/Statistics/Visitors/Visitors.aspx"></asp:HyperLink>&nbsp;]</span>
                </div>
                <div class="toggleBox">
                    <img src="../../images/snap-maximize.gif" runat="server" alt="<%$ Resources:GUIStrings, MaximizeSnap %>" onclick="snapVisitors.toggleExpand();" />
                </div>
            </div>
        </div>
    </CollapsedHeader>
    <Content>
        <div class="contentShadowBox contentShadowBoxNoGap">
            <div class="contentBox">
                <div class="gridContainer">
                    <ComponentArt:Grid SkinId="AutoScrollingGrid" ID="grdVisitorsLanding" runat="server" RunningMode="Client" 
                        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoAdjustPageSize="true"  AutoPostBackOnSelect="false" Width="263" Height="120"
                        SliderPopupClientTemplateId="grdVisitorsLandingSlider" PagerInfoClientTemplateId="grdVisitorsLandingPagination">
                        <Levels>
                            <ComponentArt:GridLevel DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false"
                                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow" SortedHeadingCellCssClass="HeadingRow"
                                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                                <Columns>
                                    <ComponentArt:GridColumn Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, VisitorType %>" HeadingCellCssClass="FirstHeadingCell" DataField="VisitorType" 
                                        DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" IsSearchable="true"
                                        AllowReordering="false" FixedWidth="true" />
                                    <ComponentArt:GridColumn Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>" DataField="Visits" IsSearchable="true" DataType="System.Int64"  
                                        SortedDataCellCssClass="SortedDataCell"  AllowReordering="false" FixedWidth="true" Align="Right" />
                                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                                    <ComponentArt:GridColumn DataField="OrderBy"  Visible="false"  />
                                </Columns>
                            </ComponentArt:GridLevel>
                        </Levels>
                        <ClientTemplates>
                            <ComponentArt:ClientTemplate ID="grdVisitorsLandingSlider">
                                <div class="SliderPopup">
                                    <h5>## DataItem.GetMember(0).Value ##</h5>
                                    <p>## DataItem.GetMember(1).Value ##</p>
                                    <p>## DataItem.GetMember(2).Value ##</p>
                                    <p>## DataItem.GetMember(3).Value ##</p>
                                    <p>## DataItem.GetMember(4).Value ##</p>
                                    <p class="paging">
                                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdVisitorsLanding.PageCount) ##</span>
                                        <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdVisitorsLanding.RecordCount) ##</span>
                                    </p>
                                </div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="grdVisitorsLandingPagination">
                                ## stringformat(Page0of12items, currentPageIndexV(), pageCountV(), grdVisitorsLanding.RecordCount) ##
                            </ComponentArt:ClientTemplate>
                        </ClientTemplates>
                              <ClientEvents>
                        <Load EventHandler="grdVisitorsLanding_onLoad" />
                        
                        </ClientEvents>
                    </ComponentArt:Grid>
                </div>
            </div>
        </div>
    </Content>
    <Footer>                
        <div class="bottomShadowBox recentlyPublishedFooter">
            <div class="gridFooterContainer"></div>
        </div>
    </Footer>
    <CollapsedFooter>
        <div class="bottomShadowBox">
            <div class="gridFooterContainer"></div>
        </div>
    </CollapsedFooter>
</ComponentArt:Snap>
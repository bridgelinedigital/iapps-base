<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LandingTopPagesGrid.ascx.cs"
    Inherits="UserControls_General_LandingTopPagesGrid" %>

<script type="text/javascript">
    var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
    var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
    var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
    var offsetfromcursorX = 12; //Customize x offset of tooltip
    var offsetfromcursorY = 10; //Customize y offset of tooltip

    var typeOfGraph = "2";

    var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
    var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

    function grdTopPages_onContextMenu(sender, eventArgs) {
        item = eventArgs.get_item();
        grdLandingTopPages.select(item);
        var evt = eventArgs.get_event();
        cmTopPages.showContextMenuAtEvent(evt, eventArgs.get_item());
    }
    function cmTopPages_ItemSelect(sender, eventArgs) {
        var selectedMenu = eventArgs.get_item().get_id();
        switch (selectedMenu) {
            case "ViewStatistics":
                var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', 'width=750px,height=635px,center=1,resize=0,scrolling=1');
                viewPopupWindow.onclose = function() {
                    var a = document.getElementById('Content');
                    var ifr = a.getElementsByTagName("iframe");
                    window.frames[ifr[0].name].location.replace("about:blank");
                    return true;
                }
                break;
            case "ViewAnalysis":
                window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                break;
            case "ViewNewWindow":
                window.open(analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text);
                break;
            case "ViewSiteEditor":
                window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + "?Token=" + GetTokenBySiteAndProduct(cmsProductId, siteId, siteName);
                break;
            case "AssignIndexTerms":
                var pageId = item.getMember("Original_Id").get_text().replace("{", "").replace("}", "");
                OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                break;
            case "MenuEmailAuthor":
                var email = item.GetMember('Author_Email').Text;
                //location.href = "mailto:"+email+"";
                location.href = "mailto:" + email + "";
                break;
        }
    }
    function grdTopPages_onItemSelect(sender, eventArgs) {
        var selectedRow = eventArgs.get_item();
        pageId = selectedRow.getMember("Original_Id").get_text();
        pageAuthor = selectedRow.getMember("Author").get_text();
        pageName = selectedRow.getMember("Title").get_text();
        var callbackArgs = new Array(pageId, pageName, typeOfGraph);
        TopPagesCallback.Callback(callbackArgs);
        document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pageviews4 %>'/>", pageName); ;
    }
    function grdTopPages_onLoad(sender, eventArgs) {
        if (grdLandingTopPages.get_table().getRowCount() >= 1 && grdLandingTopPages.PageCount != 0) {
            var firstItem = grdLandingTopPages.get_table().getRow(0);
            pageId = firstItem.getMember("Original_Id").get_text();
            pageAuthor = firstItem.getMember("Author").get_text();
            pageName = firstItem.getMember("Title").get_text();
            grdLandingTopPages.select(firstItem, false);
            document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pageviews4 %>'/>", pageName); ;
        }
    }
    function CreateTip(dataItemObject) {
        var authorName = dataItemObject.GetMember('Author').Text;
        authorName = authorName.replace(new RegExp("'", "g"), "\'").replace(new RegExp('"', "g"), '');

        var publisherName = dataItemObject.GetMember('Published_By').Text;
        publisherName = publisherName.replace(new RegExp("'", "g"), "\'").replace(new RegExp('"', "g"), '');

        var dateTimePublished = dataItemObject.GetMember('Published_Date').Text;
        dateTimePublished = dateTimePublished.replace(new RegExp("'", "g"), "\'").replace(new RegExp('"', "g"), '');

        var navigationHierarchy = dataItemObject.GetMember('Menu').Text;
        navigationHierarchy = navigationHierarchy.replace(new RegExp("'", "g"), "\'").replace(new RegExp('"', "g"), '');


        var strHTML = "";
        var imgTag = "";

        strHTML += "<div class=tooltip>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, Author %>'/></h5>";
        strHTML += "<p>" + authorName + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, PublishedBy %>'/></h5>";
        strHTML += "<p>" + publisherName + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, DateTimePublished %>'/></h5>";
        strHTML += "<p>" + dateTimePublished + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, NavigationHierarchy %>'/></h5>";
        strHTML += "<p>" + WrapTooltipText(navigationHierarchy) + "</p>";
        strHTML += "</div>";
        strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'');positiontip(event);\" onmouseout='hideddrivetip();' src='../App_Themes/General/images/icon-tooltip.gif' />";
        return strHTML;
    }
</script>

<div class="subSectionContent">
    <ComponentArt:Grid SkinID="Default" ID="grdLandingTopPages" runat="server" RunningMode="Client"
        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" PageSize="5" SliderPopupClientTemplateId="grdTopPagesSlider"
        Width="665">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Original_Id" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>" HeadingCellCssClass="FirstHeadingCell"
                        DataField="Title" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" />
                    <ComponentArt:GridColumn Width="68" runat="server" HeadingText="<%$ Resources:GUIStrings, Pageviews1 %>" DataField="Page_Visits"
                        IsSearchable="true" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" Align="Right" DataType="System.Int64" />
                    <ComponentArt:GridColumn Width="115" runat="server" HeadingText="<%$ Resources:GUIStrings, UniquePageviews %>" DataField="Unique_Page_Visits"
                        SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                        Align="Right" DataType="System.Int64" />
                    <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, TimeonPage %>" DataField="Page_Dwell_Time"
                        DefaultSortDirection="Descending" SortedDataCellCssClass="SortedDataCell" FixedWidth="true"
                        AllowReordering="false" Align="Right" Visible="true" />
                    <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>" DataField="BounceRate"
                        FixedWidth="true" SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="HeadingCell"
                        DataType="System.Double" DataCellClientTemplateId="BounceRateCT" AllowReordering="false"
                        Align="Right" />
                    <ComponentArt:GridColumn Width="55" runat="server" HeadingText="<%$ Resources:GUIStrings, Exit %>" DataField="ExitPercentage"
                        FixedWidth="true" SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="HeadingCell"
                        DataType="System.Double" DataCellClientTemplateId="ExitPercentageCT" AllowReordering="false"
                        Align="Right" />
                    <ComponentArt:GridColumn Width="55" runat="server" HeadingText="<%$ Resources:GUIStrings, CMDetails %>" DataField="Original_Id"
                        FixedWidth="true" AllowSorting="false" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                        HeadingCellCssClass="LastHeadingCell" AllowReordering="false" Align="Center"
                        DataCellClientTemplateId="CMHoverTP" Visible="true" />
                    <ComponentArt:GridColumn DataField="Author" Visible="false" />
                    <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                    <ComponentArt:GridColumn DataField="Published_Date" Visible="false" />
                    <ComponentArt:GridColumn DataField="Published_By" Visible="false" />
                    <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="BounceRateCT">
                ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ExitPercentageCT">
                ## GetContentDecimal(DataItem.GetMember('ExitPercentage').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="pageviewsHeaderTP">
                <div class="customHeader rightAligned">
                    ## getSortImageHtml(DataItem,'leftAligned') ## <span><asp:localize runat="server" text="<%$ Resources:GUIStrings, Pageviews1 %>"/></span>
                    <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(tpPageviewText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="uniqueHeaderTP">
                <div class="customHeader rightAligned">
                    ## getSortImageHtml(DataItem,'leftAligned') ## <span><asp:localize runat="server" text="<%$ Resources:GUIStrings, UniquePageviews %>"/></span>
                    <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(tpUniquePageviewText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="timeHeaderTP">
                <div class="customHeader rightAligned">
                    ## getSortImageHtml(DataItem,'leftAligned') ## <span><asp:localize runat="server" text="<%$ Resources:GUIStrings, TimeonPage %>"/></span>
                    <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(tpTimeOnPageText, 300, '', ');positiontip(event);" onmouseout="hideddrivetip();" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="bounceHeaderTP">
                <div class="customHeader rightAligned">
                    ## getSortImageHtml(DataItem,'leftAligned') ## <span><asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/></span>
                    <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(tpBounceRateText, 300, '', ');positiontip(event);" onmouseout="hideddrivetip();" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="exitHeaderTP">
                <div class="customHeader rightAligned">
                    ## getSortImageHtml(DataItem,'leftAligned') ## <span><asp:localize runat="server" text="<%$ Resources:GUIStrings, Exit %>"/></span>
                    <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(tpExitText, 300, '', ');positiontip(event);" onmouseout="hideddrivetip();" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderTP">
                <div class="customHeader">
                    <span><asp:localize runat="server" text="<%$ Resources:GUIStrings, CMDetails %>"/></span>
                    <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(tpCMDetailsText, 300, '', ');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CMHoverTP">
                ## CreateTip(DataItem) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdTopPagesSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdLandingTopPages.PageCount) ##</span> 
                        <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdLandingTopPages.RecordCount) ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdTopPagesPagination">
                ## GetGridPaginationInfo(grdLandingTopPages) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ClientEvents>
            <ItemSelect EventHandler="grdTopPages_onItemSelect" />
            <Load EventHandler="grdTopPages_onLoad" />
            <ContextMenu EventHandler="grdTopPages_onContextMenu" />
        </ClientEvents>
    </ComponentArt:Grid>
    <ComponentArt:Menu SkinID="ContextMenu" ID="cmTopPages" runat="server">
        <Items>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>" ID="ViewStatistics">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewInboundOutboundAnalysis %>" ID="ViewAnalysis">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>" ID="ViewNewWindow">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>" ID="ViewSiteEditor">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>" ID="AssignIndexTerms">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="MenuEmailAuthor" runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="cmTopPages_ItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</div>

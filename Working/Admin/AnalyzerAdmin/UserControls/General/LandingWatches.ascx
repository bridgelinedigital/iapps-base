<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_General_LandingWatches" Codebehind="LandingWatches.ascx.cs" %>
<%--<%@ outputcache duration="200" varybycontrol="ReportStartDate,ReportEndDate" %>--%>

<script type="text/javascript">
    var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
    var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
    var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams

    /** Method for Key Press Search **/
function searchWatchGrid(evt)
{
    if(validKey(evt))
    {
        var str = document.getElementById("txtSearchWatches").value;
        grdLandingWatches.Search(str, false);
    }
}
function grdLandingWatches_onPageIndexChange(sender, eventArgs)
{
    var startRecord = (eventArgs.get_index() * sender.get_pageSize()) + 1;
    var endRecord = (eventArgs.get_index() * sender.get_pageSize()) + sender.get_pageSize();
    if(sender.get_recordCount() == 0)
    {
        startRecord = 0;
    }
    //if record count is less than the calculated end record number then make end record equal to record count
    if(endRecord > sender.get_recordCount() || sender.get_recordCount() < sender.get_pageSize())
    {
        endRecord = sender.get_recordCount();
    }
    document.getElementById("<%=DisplayingRecords.ClientID%>").innerHTML = stringformat("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, Displaying01of2 %>' />", startRecord, endRecord, sender.get_recordCount());

    //document.getElementById("<%=startRecord.ClientID%>").innerHTML = startRecord;
    //document.getElementById("<%=endRecord.ClientID%>").innerHTML = endRecord;
    //document.getElementById("<%=totalRecords.ClientID%>").innerHTML = sender.get_recordCount();
}
function grdLandingWatches_onLoad(sender, eventArgs)
{
  grdLandingWatches.sort(1,true);
}
</script>
<ComponentArt:Snap ID="snapWatches" runat="server" Width="100%" DockingContainers="leftContentSection"
    CurrentDockingContainer="leftContentSection" DockingStyle="TransparentRectangle" MustBeDocked="true"
    DraggingMode="FreeStyle" DraggingStyle="SolidOutline">
    <Header>
        <div class="topShadowBox">
            <div class="headerContainer">
                <div class="dragBox" onmousedown="snapWatches.startDragging(event);">
                    <h3><asp:localize runat="server" text="<%$ Resources:GUIStrings, Watches %>"/></h3><span>[&nbsp;<asp:HyperLink ID="linkWatches" runat="server" Text="<%$ Resources:GUIStrings, ViewFullReport %>" NavigateUrl="~/Statistics/Watches/Watches.aspx"></asp:HyperLink>&nbsp;]</span>
                </div>
                <div class="toggleBox">
                    <img src="../../images/snap-minimize.gif" runat="server" alt="<%$ Resources:GUIStrings, MinimizeSnap %>" onclick="snapWatches.toggleExpand();" />
                </div>
                <div class="tabContainer">
                    <ComponentArt:TabStrip ID="tabWatches" SkinID="Vista" runat="server">
                        <Tabs>
                            <ComponentArt:TabStripTab runat="server" Text="<%$ Resources:GUIStrings, Watches %>" ToolTip="<%$ Resources:GUIStrings, Watches %>"></ComponentArt:TabStripTab>
                        </Tabs>
                    </ComponentArt:TabStrip>
                </div>
            </div>
        </div>
    </Header>
    <CollapsedHeader>
        <div class="topShadowBox">
            <div class="headerContainer">
                <div class="dragBox" onmousedown="snapWatches.startDragging(event);">
                    <h3><asp:localize runat="server" text="<%$ Resources:GUIStrings, Watches %>"/></h3><span>[&nbsp;<asp:HyperLink ID="linkWatchesCollapsed" runat="server" Text="<%$ Resources:GUIStrings, ViewFullReport %>" NavigateUrl="~/Statistics/Watches/Watches.aspx"></asp:HyperLink>&nbsp;]</span>
                </div>
                <div class="toggleBox">
                    <img src="../../images/snap-maximize.gif" runat="server" alt="<%$ Resources:GUIStrings, MaximizeSnap %>" onclick="snapWatches.toggleExpand();" />
                </div>
            </div>
        </div>
    </CollapsedHeader>
    <Content>        
        <div class="contentShadowBox">
            <div class="contentBox">
                <div class="gridContainer">
                    <div class="searchContainer">
                        <input id="txtSearchWatches" class="text textBoxes" value="<%= GUIStrings.TypeHereToFilterResults %>"
                            onfocus="clearDefaultText(this);" onblur="setDefaultText(this);" onkeyup="searchWatchGrid(event);" />
                        <div class="pagingInfo" id="pagingInfo" runat="server">
                        <asp:Label runat="server" ID="DisplayingRecords" />
                            <%--<span>Displaying&nbsp;</span>--%>
                            <asp:Label runat="server" ID="startRecord" Visible="false"></asp:Label>
                            <%--<span>&nbsp;-&nbsp;</span>--%>
                            <asp:Label runat="server" ID="endRecord" Visible="false"></asp:Label>
                            <%--<span>&nbsp;of&nbsp;</span>--%>
                            <asp:Label runat="server" ID="totalRecords" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <ComponentArt:Grid SkinId="Default" ID="grdLandingWatches" runat="server" RunningMode="Client" 
                        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" PageSize="10" Width="666"
                        SliderPopupClientTemplateId="grdLandingWatchesSlider" PagerInfoClientTemplateId="grdLandingWatchesPagination">
                        <Levels>
                            <ComponentArt:GridLevel DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false"
                                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow" SortedHeadingCellCssClass="HeadingRow"
                                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                                <Columns>
                                    <ComponentArt:GridColumn Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, WatchName %>" HeadingCellCssClass="FirstHeadingCell" DataField="Name" 
                                        DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" IsSearchable="true"
                                        AllowReordering="false" FixedWidth="true" />
                                    <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, TotalVisitors %>" DataField="TotalVisitors" IsSearchable="false" DataType="System.Int64"  
                                        SortedDataCellCssClass="SortedDataCell"  AllowReordering="false" FixedWidth="true" Align="Right" />
                                    <ComponentArt:GridColumn Width="80" runat="server" HeadingText="<%$ Resources:GUIStrings, TotalCount %>" DataField="TotalCount" SortedDataCellCssClass="SortedDataCell"
                                        AllowReordering="false" FixedWidth="true" DataType="System.Int64" Align="Right" IsSearchable="false" />
                                    <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, Conversion %>" DataField="TotalConversion" SortedDataCellCssClass="SortedDataCell"
                                        AllowReordering="false" FixedWidth="true" DataType="System.Double" Align="Right" IsSearchable="false" DataCellClientTemplateId="TotalConversionCT" />
                                    <ComponentArt:GridColumn Width="108" runat="server" HeadingText="<%$ Resources:GUIStrings, UniqueVisitors %>" DataField="UniqueVisitors" SortedDataCellCssClass="SortedDataCell"
                                        AllowReordering="false" FixedWidth="true" DataType="System.Int64" Align="Right" IsSearchable="false" />
                                    <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, UniqueCount %>" DataField="UniqueCount" SortedDataCellCssClass="SortedDataCell"
                                        AllowReordering="false" FixedWidth="true" DataType="System.Int64" Align="Right" IsSearchable="false" />
                                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" IsSearchable="false" />
                                </Columns>
                            </ComponentArt:GridLevel>
                        </Levels>
                        <ClientEvents>
                            <PageIndexChange EventHandler="grdLandingWatches_onPageIndexChange" />
                        </ClientEvents>
                        <ClientTemplates>
                                 <ComponentArt:ClientTemplate ID="TotalConversionCT">
                                ## GetContentDecimal(DataItem.GetMember('TotalConversion').Value) ##
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="grdLandingWatchesSlider">
                                <div class="SliderPopup">
                                    <h5>## DataItem.GetMember(0).Value ##</h5>
                                    <p>## DataItem.GetMember(1).Value ##</p>
                                    <p>## DataItem.GetMember(2).Value ##</p>
                                    <p>## DataItem.GetMember(3).Value ##</p>
                                    <p>## DataItem.GetMember(4).Value ##</p>
                                    <p class="paging">
                                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdLandingWatches.PageCount) ##</span>
                                        <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdLandingWatches.RecordCount) ##</span>
                                    </p>
                                </div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="grdLandingWatchesPagination">
                                ## GetGridPaginationInfo(grdLandingWatches) ##
                            </ComponentArt:ClientTemplate>
                        </ClientTemplates>
                        <ClientEvents>
                        <Load EventHandler="grdLandingWatches_onLoad" />
                        
                        </ClientEvents>
                    </ComponentArt:Grid>
                </div>
            </div>
        </div>
    </Content>
    <Footer>
        <div class="bottomShadowBox">
            <div class="gridFooterContainer"></div>
        </div>
    </Footer>
    <CollapsedFooter>
        <div class="bottomShadowBox bottomShadowBoxGap">
            <div class="gridFooterContainer"></div>
        </div>
    </CollapsedFooter>
</ComponentArt:Snap>
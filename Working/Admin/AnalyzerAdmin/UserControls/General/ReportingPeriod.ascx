﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportingPeriod.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.ReportingPeriod" %>
<h3 class="traffic"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Traffic %>"/></h3>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td><asp:HyperLink ID="HyperLink12" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/></asp:HyperLink></td>
        <td><asp:Literal ID="ltVisits" Text="" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:HyperLink ID="HyperLink1" NavigateUrl="~/Statistics/Traffic/TopPagesTimeSeries.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Pageviews1 %>"/></asp:HyperLink></td>
        <td><asp:Literal ID="ltPageviews" Text="" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:HyperLink ID="HyperLink2" NavigateUrl="~/Statistics/Traffic/PageviewsPerVisitTimeSeries.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, PageVisit %>"/></asp:HyperLink></td>
        <td><asp:Literal ID="ltPagePerVisits" Text="" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:HyperLink ID="HyperLink3" NavigateUrl="~/Statistics/Traffic/SinglePageVisits.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate %>"/></asp:HyperLink></td>
        <td><asp:Literal ID="ltBounceRate" Text="" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:HyperLink ID="HyperLink4" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, AverageTimeonSite %>"/></asp:HyperLink></td>
        <td><asp:Literal ID="ltAvgTimeOnSite" Text="" runat="server"></asp:Literal></td>
    </tr>
</table>
<hr />
<h3 class="referring"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Referrers %>"/></h3>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td><asp:HyperLink ID="HyperLink5" NavigateUrl="~/Statistics/Visitors/Direct.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Direct %>"/></asp:HyperLink></td>
        <td><asp:Literal ID="ltDirectTraffic" Text="100,000 (50%)" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:HyperLink ID="HyperLink6" NavigateUrl="~/Statistics/Referrers/SearchReferrers.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Search %>"/></asp:HyperLink></td>
        <td><asp:Literal ID="ltSearch" Text="300,000 (50%)" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:HyperLink ID="HyperLink7" NavigateUrl="~/Statistics/Referrers/ExternalReferrers.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, ReferringSites %>"/></asp:HyperLink></td>
        <td><asp:Literal ID="ltReferringSites" Text="5,000 (30%)" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:HyperLink ID="HyperLink8" NavigateUrl="~/Campaigns/CampaignListing.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Campaigns %>"/></asp:HyperLink></td>
        <td><asp:Literal ID="ltCampaigns" Text="5,000 (30%)" runat="server"></asp:Literal></td>
    </tr>
</table>
<hr />
<asp:PlaceHolder ID="pldCommerce" runat="server">
<h3 class="commerce"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Commerce %>"/></h3>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td><asp:LinkButton ID="lbConversionRate" runat="server" Text="<%$ Resources:GUIStrings, ConversionRate1 %>" OnClick="lbConversionRate_Click"> </asp:LinkButton></td>
        <td><asp:Literal ID="ltConversionRate" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:LinkButton ID="lbRevenue" runat="server" Text="<%$ Resources:GUIStrings, Revenue %>" OnClick="lbRevenue_Click" ></asp:LinkButton></td>
        <td><asp:Literal ID="ltRevenue" runat="server"></asp:Literal></td>
    </tr>
    <tr>
        <td><asp:LinkButton ID="lbTransactions" runat="server" Text="<%$ Resources:GUIStrings, Transactions %>" OnClick="lbTransactions_Click" ></asp:LinkButton></td>
        <td><asp:Literal ID="ltTransaction" runat="server"></asp:Literal></td>
    </tr>
</table>
</asp:PlaceHolder>
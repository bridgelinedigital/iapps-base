<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Controls.AssignTagsWithListbox" 
    CodeBehind="AssignTagsWithListbox.ascx.cs" %>
<script type="text/javascript">

    function OpenAssignIndexTermsPopup() {
        var selectedTaxonomyIds = "";
        var listBox = document.getElementById("<%=newIndexTerms.ClientID%>");

        for (i = 0; i < listBox.options.length; i++) {
            if (selectedTaxonomyIds != "")
                selectedTaxonomyIds = selectedTaxonomyIds + ',' + listBox.options[i].value;
            else
                selectedTaxonomyIds = listBox.options[i].value;
        }

        var customAttributes = {}
        customAttributes["SelectedTaxonomyIds"] = selectedTaxonomyIds;
        OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&ObjectId=" + objectIdVar, "SetIndexTerm", customAttributes);

        return false;
    }

    function SetIndexTerm() {
        var listBox = $("#<%=newIndexTerms.ClientID%>");
        listBox.empty();
        $("#<%=hdnSelectedIndexTerms.ClientID%>").val("");

        if (typeof popupActionsJson.CustomAttributes["SelectedTaxonomyJson"] != "undefined" &&
            popupActionsJson.CustomAttributes["SelectedTaxonomyJson"] != "") {
            var taxonomyJson = JSON.parse(popupActionsJson.CustomAttributes["SelectedTaxonomyJson"]);

            $.each(taxonomyJson, function () {
                listBox.append($("<option />").val(this.Id).html(this.Title));
            });

            $("#<%=hdnSelectedIndexTerms.ClientID%>").val(JSON.stringify(taxonomyJson));
        }

    }
</script>
<div class="form-row form-edit-row">
    <label class="form-label">
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Tags %>" /><br />
        <a href="#" onclick="return OpenAssignIndexTermsPopup();" class="button small-button" id="hplAssignTags" runat="server">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, AssignTags %>" /></a></label>
    <div class="form-value">
        <asp:ListBox ID="newIndexTerms" CssClass="textBoxes textBoxesWidth" runat="server"
            Width="315" Enabled="false"></asp:ListBox>
        <asp:HiddenField ID="hdnSelectedIndexTerms" runat="server" />
    </div>
</div>

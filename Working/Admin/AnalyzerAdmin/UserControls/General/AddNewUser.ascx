<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewUser.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.AddNewUser" %>
<script type="text/javascript">
    function validateUsername(oSrc, args) {
        var Emailpattern = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*$/
        var UserPattern = /^[a-zA-Z0-9_]+[ ]*$/

        if (args.Value.indexOf("@") != -1 || args.Value.indexOf(".") != -1) // then Email validation
        {
            if (args.Value.search(Emailpattern) == -1 || (args.Value.length < 5 || args.Value.length > 256)) // If match failed
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        else {
            if (args.Value.search(UserPattern) == -1 || (args.Value.length < 5 || args.Value.length > 256)) // If match failed
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function popUpExpiryDateCalendar(ctl) {
        var thisDate = new Date();
        if (document.getElementById("<%=hdnDate.ClientID%>").value != "") {
            thisDate = new Date(document.getElementById("<%=hdnDate.ClientID%>").value);
            expirationCalendar.setSelectedDate(thisDate)
        }

        if (!(expirationCalendar.get_popUpShowing()));
        expirationCalendar.show(ctl);
    }

    function calExpiryDate_onSelectionChanged(sender, eventArgs) {
        var selectedDate = expirationCalendar.getSelectedDate();
        if (selectedDate <= new Date()) {
            alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, ExpiryDateShouldbeGreaterThanToday %>'/>");
        }
        else {
            document.getElementById("<%=expiryDate.ClientID%>").value = expirationCalendar.formatDate(selectedDate, shortCultureDateFormat);
            document.getElementById("<%=hdnDate.ClientID%>").value = expirationCalendar.formatDate(selectedDate, calendarDateFormat);
        }
    }

    function OnArrowClick(lstSite, lstGroup, lstPermission, clickMode, addOrModifyUserPage) {
        lstSite = document.getElementById("<%= sites.ClientID %>");
        lstGroup = document.getElementById("<%= groups.ClientID %>");
        lstPermission = document.getElementById("<%= permissions.ClientID %>");

        //This if for adding permission for both addnew user & modify/delete user page.
        if (clickMode == 1) {
            //Move to Permission listbox
            var siteOptions = GetSelectedTextWithValueOfLstBox(lstSite);
            var groupOptions = GetSelectedTextWithValueOfLstBox(lstGroup);

            MoveSiteGroupsToPermission(siteOptions, groupOptions, lstPermission);
        }
        else if (clickMode == 0) {
            //Remove from Pemrisision listbox
            RemoveSelectedItemsFromLstBox(lstPermission);
        }
    }
    function GroupListCallback(selectedValue) {
        GroupCallBack.callback(selectedValue);
    }

    function ValidateEditUser() {
        if (Page_ClientValidate("valAddUser") == true) {
            var lstPermission = document.getElementById("<%= permissions.ClientID %>");
            if (lstPermission.options.length == 0) {
                alert(__JSMessages["PleaseSelectAtLeastOnePermissionForThisUser"]);
                return false;
            }

            listcount = lstPermission.options.length;
            var valuetohiddenfield = "";
            for (i = 0; i < listcount; i = i + 1) {
                valuetohiddenfield += lstPermission.options[i].value + "," + lstPermission.options[i].text + ",";
            }
            document.getElementById("<%= PermissionList.ClientID %>").value = valuetohiddenfield;

            var hdnPermission = document.getElementById("<%= HdnSelectedPermissions.ClientID %>");
            hdnPermission.value = "";
            for (var i = 0; i < lstPermission.options.length; i++) {
                hdnPermission.value += lstPermission.options[i].value + ",";
            }

            return true;
        }
        else {
            return false;
        }
    }

    function ShowImagePopup() {
        OpeniAppsAdminPopup("SelectImageLibraryPopup", "ForceEnableSelection=true", "SelectImage");
        return false;
    }

    function SelectImage() {
        imageUrl = popupActionsJson.CustomAttributes.ImageUrl;
        imageFileName = popupActionsJson.CustomAttributes.ImageFileName;
        imageId = popupActionsJson.SelectedItems[0];

        imageUrl = imageUrl.replace(new RegExp(jPublicSiteUrl, "gi"), "");
        imageUrl = imageUrl.replace(new RegExp(imageFileName, "gi"), "thumbs/thumb_" + imageFileName);
        $("#imgImageURL").prop("src", imageUrl);
        $("#hdnImageId").val(imageId);
    }

    function ClearImageUrlValue() {
        $("#imgImageURL").prop("src", "/iapps_images/no_photo.jpg");
        $("#hdnImageId").val("");

        return false;
    }

    function CustomizeAnalystPermission() {
        analystPermissionPopup = dhtmlmodal.open('AnalystPermission', 'iframe', "../popups/AnalystPermissions.aspx?id=" + siteId + "&mid=<%= UserId.ToString() %>", '', '');
    }

    function PermissionItem_OnClientClick(lstBox) {
        var customPermissionText = "<%= JSMessages.Analyst %>";
        var selectedSiteID = null;
        var selectedText = null;
        if (browser == "ie") {
            if (lstBox != null && lstBox.selectedIndex >= 0 && lstBox.options[lstBox.selectedIndex].innerText) {
                //Consider trimming the site and text values as these things may have spaces near.
                selectedText = lstBox.options[lstBox.selectedIndex].innerText.split(':')[1];
                selectedSiteID = lstBox.options[lstBox.selectedIndex].value.split(':')[0];
            }
        }
        else {
            if (lstBox != null && lstBox.selectedIndex >= 0) {
                selectedText = lstBox.options[lstBox.selectedIndex].text.split(':')[1];
                selectedSiteID = lstBox.options[lstBox.selectedIndex].value.split(':')[0];
            }
        }

        $("#lnkAnalystPerm").hide();
        if (selectedText == customPermissionText && isAdmin == 1 && selectedSiteID.toLowerCase() == siteId.toLowerCase()) {
            //$("#lnkAnalystPerm").show().css("float", "right");
        }
    }

</script>
<asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
    ValidationGroup="valAddUser" />
<div class="add-user clear-fix">
    <div class="clear-fix">
    <div class="columns">
        <div class="form-row profile-picture">
            <label class="form-label">
                <asp:Image runat="server" ID="imgImageURL" ImageUrl="/iapps_images/no_photo.jpg"
                    ClientIDMode="Static" /></label>
            <div class="form-value">
                <a href="#" class="edit-image" onclick="return ShowImagePopup();">
                    <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, EditProfilePicture %>" /></a>
                <a href="#" class="remove-image" onclick="return ClearImageUrlValue();">
                    <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, Remove %>" /></a>
                <asp:HiddenField ID="hdnImageId" runat="server" ClientIDMode="Static" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, FirstName %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="firstName" runat="server" CssClass="textBoxes" Width="360" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="firstName"
                    ErrorMessage="<%$ Resources:JSMessages, FirstNameRequired %>" Display="None"
                    SetFocusOnError="true" runat="server" ValidationGroup="valAddUser" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="firstName"
                    ErrorMessage="<%$ Resources:JSMessages, FirstNameNotValid %>" Display="None"
                    SetFocusOnError="true" runat="server" ValidationExpression="[a-zA-Z0-9 ']+[ ]*"
                    ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, LastName %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="lastName" runat="server" CssClass="textBoxes" Width="360" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="LastName"
                    ErrorMessage="<%$ Resources:JSMessages, LastNameRequired %>" Display="None"
                    runat="server" ValidationGroup="valAddUser" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="lastName"
                    ErrorMessage="<%$ Resources:JSMessages, LastNameNotValid %>" Display="None"
                    SetFocusOnError="true" runat="server" ValidationExpression="[a-zA-Z0-9 ']+[ ]*"
                    ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Email %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="email" runat="server" CssClass="textBoxes" Width="360" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="email"
                    ErrorMessage="<%$ Resources:JSMessages, EmailRequired %>" Display="None"
                    runat="server" ValidationGroup="valAddUser" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="email"
                    ErrorMessage="<%$ Resources:JSMessages, EmailNotValid %>" Display="None"
                    SetFocusOnError="true" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*"
                    ValidationGroup="valAddUser" />
            </div>
        </div>
    </div>
    <div class="columns right-column">
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, UserName %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="userName" runat="server" CssClass="textBoxes" Width="370" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="userName"
                    ErrorMessage="<%$ Resources:GUIStrings, UserNameRequired %>" Display="None"
                    runat="server" ValidationGroup="valAddUser" />
                <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="userName"
                    Display="None" ErrorMessage="<%$ Resources:JSMessages, UserNameNotValid %>"
                    ClientValidationFunction="validateUsername" ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, ExternalPassword %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="externalPassword" runat="server" CssClass="textBoxes" TextMode="Password"
                    Width="370" />
                <asp:RequiredFieldValidator ID="externalPasswordValidator" ControlToValidate="externalPassword"
                    ErrorMessage="<%$ Resources:JSMessages, PasswordRequired %>" Display="None"
                    runat="server" ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, ConfirmPassword %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="confirmPassword" runat="server" CssClass="textBoxes" TextMode="Password"
                    Width="370" />
                <asp:CompareValidator ID="externalPasswordCompareValidator" ControlToValidate="externalPassword"
                    ControlToCompare="confirmPassword" Display="None" ErrorMessage="<%$ Resources:JSMessages, ConfirmPasswordRequired %>"
                    runat="server" ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, EmailNotification %>" /></label>
            <div class="form-value">
                <asp:RadioButton ID="doNotify" Checked="true" Text="<%$ Resources:GUIStrings, On %>"
                    GroupName="EmailNotification" runat="server" />
                <asp:RadioButton ID="noNotify" Text="<%$ Resources:GUIStrings, Off %>" GroupName="EmailNotification"
                    runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, ExpirationDate %>" /></label>
            <div class="form-value calendar-value">
                <asp:HiddenField ID="hdnDate" runat="server" />
                <asp:TextBox ID="expiryDate" runat="server" CssClass="textBoxes" Width="356" AutoCompleteType="none"></asp:TextBox>
                <span>
                    <img id="calendar_button" class="buttonCalendar" alt="<%= GUIStrings.ShowCalendar %>"
                        src="/iapps_images/calendar-button.png" onclick="popUpExpiryDateCalendar(this);" />
                </span>
                <ComponentArt:Calendar runat="server" SkinID="Default" PickerFormat="Custom" ID="expirationCalendar"
                    MaxDate="9998-12-31" PopUpExpandControlId="calendar_button">
                    <ClientEvents>
                        <SelectionChanged EventHandler="calExpiryDate_onSelectionChanged" />
                    </ClientEvents>
                </ComponentArt:Calendar>
                <asp:CompareValidator ID="cvexpiryDate" runat="server" ControlToValidate="expiryDate"
                    Type="Date" Operator="DataTypeCheck" ErrorMessage="<%$ Resources:JSMessages, DateNotValid %>"
                    Display="None" ValidationGroup="valAddUser" />
                <asp:RangeValidator ID="expiryDateValidator" runat="server" ControlToValidate="expiryDate"
                    ErrorMessage="<%$ Resources:JSMessages, DateNotValid %>" Display="None"
                    SetFocusOnError="true" MaximumValue="3000-1-1" MinimumValue="1900-1-1" Type="Date"
                    ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings, LeadScore %>" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtLeadScore" runat="server" CssClass="textBoxes" Width="280" Text="0" />
                <asp:CompareValidator ID="cvLeadScore" runat="server" ControlToValidate="txtLeadScore" ValidationGroup="valAddUser" CssClass="validation-error" Operator="DataTypeCheck" Type="Integer" Display="None" ErrorMessage="<%$ Resources:JSMessages, LeadScoreMustBeAnInteger %>" />
            </div>
        </div>
    </div>
    </div>
    <div class="bottom-section clear-fix">
        <h4>
            <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, AddUserPermissions %>" /></h4>
        <div class="columns">
            <label class="form-label">
                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, Sites %>" /></label>
            <div class="form-value">
                <asp:ListBox ID="sites" onchange="GroupListCallback(this.value);" runat="server"
                    Rows="6" SelectionMode="Single" Width="390" />
            </div>
        </div>
        <div class="action-column">
            <img src="~/App_Themes/General/images/icon-plus.gif" alt="Plus" title="Plus" runat="server" id="imgPlus" />
        </div>
        <div class="columns">
            <label class="form-label">
                <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Groups %>" /></label>
            <div class="form-value">
                <ComponentArt:CallBack OnCallback="GroupCallBack_OnCallback" ID="GroupCallBack" runat="server">
                    <Content>
                        <asp:ListBox SelectionMode="multiple" ID="groups" runat="server" Rows="6" Width="390" />
                    </Content>
                    <ClientEvents>
                        <BeforeCallback EventHandler="CheckSessionEventHandler" />
                    </ClientEvents>
                </ComponentArt:CallBack>
            </div>
        </div>
        <div class="action-column"> 
            <img src="~/App_Themes/General/images/action-right-arrow.gif" id="LftArrow" runat="server"
                alt="<%$ Resources:GUIStrings, AddPermission %>" class="arrowImage actionImage"
                title="<%$ Resources:GUIStrings, AddPermission %>" /><br />
            <img src="~/App_Themes/General/images/action-left-arrow.gif" id="RhtArrow" runat="server"
                alt="<%$ Resources:GUIStrings, DeletePermission %>" class="actionImage" title="<%$ Resources:GUIStrings, DeletePermission %>" />
        </div>
        <div class="columns">
            <label class="form-label">
                <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings, CurrentPermissions %>" />
                <a href="#" onclick="return CustomizeAnalystPermission();" id="lnkAnalystPerm" style="display: none">
                    <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, Customize %>" /></a>
            </label>
            <div class="form-value">
                <asp:ListBox ID="permissions" runat="server" Rows="6" SelectionMode="multiple" EnableViewState="true"
                    Width="390" />
            </div>
        </div>
    </div>
    <input type="hidden" runat="server" value="" id="PermissionList" />
    <asp:HiddenField ID="HdnSelectedPermissions" runat="server" Value="" />
    <asp:HiddenField ID="hdnUserId" runat="server" Value="" />
</div>

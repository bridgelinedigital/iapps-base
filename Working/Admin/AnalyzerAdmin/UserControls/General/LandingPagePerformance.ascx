﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LandingPagePerformance.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.UserControls.General.LandingPagePerformance" %>
<script type="text/javascript">
    /** Methods to get set the tooltip **/
    function CreateImageHovers(dataItemObject) {
        var arrTitle = dataItemObject.GetMember('UnitPrice').Text.split(',');
        var arrTime = dataItemObject.GetMember('UnitsInStock').Text.split(',');
        var arrPathTime = dataItemObject.GetMember('Discontinued').Text.split(',');
        var strHTML = "";
        var hoverText = "";
        for (var i = 0; i < arrTitle.length; i++) {
            hoverText = "<strong><%= JSMessages.PageTitle1 %></strong>" + arrTitle[i] + "<br />";
            hoverText += "<strong><%= JSMessages.TimeonPage %></strong>" + arrTime[i] + "<br />";
            hoverText += "<strong><%= JSMessages.TimeonPageforCompletePath %></strong>" + arrPathTime[i] + "<br />";
            strHTML += "<img style=\"float:left;margin-left:5px;\" onmouseover=\"ddrivetip('" + hoverText + "', 300,'','../App_Themes/General/Images/tooltip-arrow.gif','../App_Themes/General/Images/rev-tooltip-arrow.gif','../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);\" onmouseout=\"hideddrivetip();\" src=\"../App_Themes/General/images/page-icon.gif\" />";
        }
        return strHTML;
    }
    function ShowPath(dataItemObject) {
        var currentRow = "";
        var arrTitle = "";
        var arrTime = "";
        var arrPathTime = "";
        var numberOfPages = parseInt(dataItemObject.GetMember('PagePathLength').get_text());
        if (numberOfPages > 0) {
            numberOfPages = numberOfPages + 1;
        }
        var strHTML = "<table width='100%' border='0'><tr><td>";
        var hoverText = "";
        var replacedString = "";
        for (var i = 0; i < numberOfPages; i++) {
            if (dataItemObject.get_childTable() != null) {
                currentRow = dataItemObject.get_childTable().getRow(i);
                if (currentRow != null) {
                    var arrTitle = currentRow.GetMember('PageTitle').Text;
                    replacedString = arrTitle;
                    replacedString = replacedString.replace(new RegExp("&", "g"), "&amp;");
                    replacedString = replacedString.replace(new RegExp("<", "g"), "&lt;");
                    replacedString = replacedString.replace(new RegExp(">", "g"), "&gt;");
                    replacedString = replacedString.replace(new RegExp("\"", "g"), "&quot;");
                    replacedString = replacedString.replace(new RegExp("'", "g"), "\\'");
                    replacedString = replacedString.replace(new RegExp("\n", "g"), " ");
                    var arrTime = currentRow.GetMember('AvgTimePerPage').Text;
                    var arrPathTime = GetContentDecimal(currentRow.GetMember('PercentTimeOnPage').Text);
                    hoverText = "<strong><%= JSMessages.PageTitle1 %></strong>" + replacedString + "<br />";
                    hoverText += "<strong><%= JSMessages.TimeonPage %></strong>" + arrTime + "<br />";
                    hoverText += "<strong><%= JSMessages.TimeonPageforCompletePath %></strong>" + arrPathTime + "<br />";
                    strHTML += "<img style=\"display: inline-block;margin-left:5px;\" src=\"../App_Themes/General/images/page-icon.gif\" onmouseover=\"ddrivetip('" + hoverText + "', 300,'','../App_Themes/General/Images/tooltip-arrow.gif','../App_Themes/General/Images/rev-tooltip-arrow.gif','../App_Themes/General/Images/down-tooltip-arrow.gif');\" onmouseout=\"hideddrivetip();\" />";
                    if ((i + 1) % 13 == 0) {
                        strHTML += "</td></tr><tr><td>";
                    }
                }
            }
        }
        strHTML += "</td></tr></table>";
        return strHTML;
    }
    function getSortImageHtml(column) {
        // is the grid sorted by this column?
        if (grdPathAnalysisResults.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
            if (grdPathAnalysisResults.Levels[0].IndicatedSortDirection == 1) {
                return '<img src="../App_Themes/General/images/desc.gif" style="margin-left:5px;" alt="<%= JSMessages.descending %>" />';
            }
            else {
                return '<img src="../App_Themes/General/images/asc.gif" style="margin-left:5px;" alt="<%= JSMessages.ascending %>" />';
            }
        }
        // it isn't sorted by this column, no image
        return '';
    }

    /** Methods for the dynamic tooltip **/
    var offsetfromcursorX = 12; //Customize x offset of tooltip
    var offsetfromcursorY = 10; //Customize y offset of tooltip
    var reverseTooltipImage;
    var downTooltipImage;
    var tooltipImage;

    var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
    var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

    document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
    document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

    var ie = document.all;
    var ns6 = document.getElementById && !document.all;
    var enabletip = false;
    if (ie || ns6) {
        var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
    }
    var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
    function ietruebody() {
        return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
    }

    function ddrivetip(thetext, thewidth, thecolor, imgSrc, revImgSrc, downImgSrc) {
        //document.getElementById("dhtmlpointer").src = imgSrc;
        tooltipImage = imgSrc;
        reverseTooltipImage = revImgSrc;
        downTooltipImage = downImgSrc;
        if (ns6 || ie) {
            if (typeof thewidth != "undefined") {
                tipobj.style.width = thewidth + "px";
            }
            if (typeof thecolor != "undefined" && thecolor != "") {
                tipobj.style.backgroundColor = thecolor;
            }
            tipobj.innerHTML = thetext;
            enabletip = true;
            return false;
        }
    }

    function positiontip(e) {
        if (enabletip) {
            pointerobj.src = tooltipImage;
            var nondefaultpos = false;
            var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
            var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
            //Find out how close the mouse is to the corner of the window
            var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
            var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
            var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
            var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
            var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
            //if the horizontal distance isn't enough to accomodate the width of the context menu
            if (rightedge < tipobj.offsetWidth) {
                //move the horizontal position of the menu to the left by it's width
                pointerobj.src = reverseTooltipImage;
                tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                nondefaultpos = true;
            }
            else if (curX < leftedge) {
                tipobj.style.left = "5px";
            }
            else {
                //position the horizontal position of the menu where the mouse is positioned
                tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                pointerobj.style.left = (curX + offsetfromcursorX) + "px";
            }
            //same concept with the vertical position
            if (bottomedge < tipobj.offsetHeight) {
                pointerobj.src = downTooltipImage;
                tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                nondefaultpos = true;
            }
            else {
                tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                pointerobj.style.top = (curY + offsetfromcursorY) + "px";
            }
            tipobj.style.visibility = "visible";
            if (!nondefaultpos)
                pointerobj.style.visibility = "visible";
            else
                pointerobj.style.visibility = "visible";
        }
    }
    function hideddrivetip() {
        if (ns6 || ie) {
            enabletip = false;
            tipobj.style.visibility = "hidden";
            pointerobj.style.visibility = "hidden";
            tipobj.style.left = "-1000px";
            tipobj.style.backgroundColor = '';
            tipobj.style.width = '';
        }
    }
    document.onmousemove = positiontip;
    /** Methods for the dynamic tooltip **/
</script>
<ComponentArt:Grid SkinID="SliderWithHierarchy" ID="grdPathAnalysisResults" runat="server"
    RunningMode="Client" EmptyGridText="No records found." Width="100%" ShowFooter="false"
    DataAreaCssClass="DataAreaTable" EditOnClickSelectedItem="false" EnableViewState="true" Sort="visits desc">
    <Levels>
        <ComponentArt:GridLevel DataMember="TopLandingPages" DataKeyField="PageID" HeadingCellCssClass="HeadingCell"
            HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell"
            RowCssClass="Row" SelectorCellWidth="18" SelectorImageWidth="18" SelectorImageUrl="last.gif"
            ShowTableHeading="false" HeadingCellActiveCssClass="HeadingCellActive" SelectedRowCssClass="SelectedRow"
            GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
            SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
            ShowSelectorCells="false" HeadingCellHoverCssClass="HeadingCellHover">
            <Columns>
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, PageTitle %>" DataField="landingpage" Width="645" AllowReordering="false"
                    FixedWidth="true" TextWrap="true" DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell" />
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Visits %>" DataField="visits" TextWrap="true"
                    AllowReordering="false" FixedWidth="true" Align="Right" Width="100" />
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, BounceRate %>" DataField="Bounce Rate" FixedWidth="true"
                    DataCellServerTemplateId="svtBounceRate" AllowReordering="false" Align="Right"
                    TextWrap="true" Width="150" />
                    <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Goal %>" DataField="Completed Goal" TextWrap="true"
                    AllowReordering="false" FixedWidth="true" Align="Right" Width="100" />
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Conversion %>" DataField="Conversion %" TextWrap="true"
                    AllowReordering="false" FixedWidth="true" Align="Right" Width="150" DataCellServerTemplateId="svtConversion" />
                <ComponentArt:GridColumn DataField="PageID" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
        <ComponentArt:GridLevel DataKeyField="Path" DataMember="Path" HeadingCellCssClass="HeadingCell"
            HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" RowCssClass="Row"
            DataCellCssClass="DataCell" SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif"
            ShowTableHeading="false" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
            SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
            AlternatingRowCssClass="AlternateDataRow" ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn Width="110" HeadingText="<%$ Resources:GUIStrings, Frequency %>" DataField="PathCount"
                    Align="Right" AllowReordering="false" FixedWidth="true" DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell" />
                <ComponentArt:GridColumn Width="110" HeadingText="<%$ Resources:GUIStrings, ofClicks %>" DataField="PagePathLength"
                    IsSearchable="true" Align="Right" AllowReordering="false" FixedWidth="true" />
                <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, UniquePaths %>" DataField="PagePathLength"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="PageWatches"
                    TextWrap="true" />
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, AveragePathTime %>" DataField="Duration"
                    FixedWidth="true" AllowReordering="false" Align="Left" />
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, AverageTimePerPage %>" DataField="TimePerPage"
                    FixedWidth="true" AllowReordering="false" Align="Left" />
                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                <ComponentArt:GridColumn DataField="Original_Id" Visible="false" />
                <ComponentArt:GridColumn DataField="Path" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
        <ComponentArt:GridLevel DataKeyField="Id" DataMember="PageDetails" HeadingCellCssClass="HeadingCell"
            HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" RowCssClass="Row"
            DataCellCssClass="DataCell" SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif"
            ShowTableHeading="false" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
            SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
            AlternatingRowCssClass="AlternateDataRow" ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, PageTitle %>" DataField="PageTitle" Width="511"
                    AllowReordering="false" FixedWidth="true" TextWrap="true" />
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, TimeonPage %>" DataField="AvgTimePerPage" TextWrap="true"
                    AllowReordering="false" FixedWidth="true" Align="Left" Width="150" />
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, FullPath %>" DataField="PercentTimeOnPage"
                    FixedWidth="true" DataCellClientTemplateId="PercentTimeOnPageCT" AllowReordering="false"
                    Align="Left" TextWrap="true" Width="150" />
                <ComponentArt:GridColumn DataField="AuthorEmail" Visible="false" />
                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                <ComponentArt:GridColumn DataField="OriginalId" Visible="false" />
                <ComponentArt:GridColumn DataField="Menu" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="PercentTimeOnPageCT">
            ## GetContentDecimal(DataItem.GetMember('PercentTimeOnPage').Value) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BounceRateOnPageCT">
            ## GetContentDecimal(DataItem.GetMember('Bounce Rate').Value) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PageWatches">
            ## ShowPath(DataItem) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
    <ServerTemplates>
        <ComponentArt:GridServerTemplate ID="svtConversion">
            <Template>
                <span title="<%# Container.DataItem["Conversion %"]!=DBNull.Value? (Convert.ToDecimal(Container.DataItem["Conversion %"]) * 100).ToString("0.##"):"0"  %>">
                    <%# Container.DataItem["Conversion %"]!=DBNull.Value ?(Convert.ToDecimal(Container.DataItem["Conversion %"]) * 100).ToString("0.##"): "0" %> 
                </span>
            </Template>
        </ComponentArt:GridServerTemplate>
        <ComponentArt:GridServerTemplate ID="svtBounceRate">
            <Template>
                <span title="<%# (Convert.ToDecimal( (Container.DataItem["Bounce Rate"]==DBNull.Value?0:Container.DataItem["Bounce Rate"]) ) * 100).ToString("0.##")  %>">
                    <%# (Convert.ToDecimal((Container.DataItem["Bounce Rate"]==DBNull.Value?0:Container.DataItem["Bounce Rate"]) ) * 100).ToString("0.##")%> 
                </span>
            </Template>
        </ComponentArt:GridServerTemplate>
    </ServerTemplates>
</ComponentArt:Grid>
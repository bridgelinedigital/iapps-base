<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_General_LandingFileDownloads"
    CodeBehind="LandingFileDownloads.ascx.cs" %>
<%--<%@ outputcache duration="200" varybycontrol="ReportStartDate,ReportEndDate" %>--%>

<script type="text/javascript">
    var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
    var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
    var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
    var item;
    var folderIdColumn = 5;
    function cmFileDownloadsAC_onItemSelect(sender, eventArgs) {
        var selectedMenu = eventArgs.get_item().get_id();
        var folderId = item.getMember(folderIdColumn).Value;
        switch (selectedMenu) {
            case 'fileLibrary':
                var currentUrl = document.URL;
                if (currentUrl == null || currentUrl == '') {
                    currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                }
                if (currentUrl.indexOf('?') > 0) {
                    currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                }
                currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                window.location.href = analyticsPublicSiteUrl + "/Admin/Default.aspx" + currentUrl + "&Token=" + GetTokenBySiteAndProduct(cmsProductId, siteId, siteName) + "&RequestPageURL=Libraries/Data/ManageFileLibrary.aspx" + "&FolderId=" + folderId;
                break;
        }
    }
    function grdFileDownloadsLanding_onContextMenu(sender, eventArgs) {
        item = eventArgs.get_item();
        grdFileDownloadsLanding.select(item);
        var evt = eventArgs.get_event();
        cmFileDownloadsAC.showContextMenuAtEvent(evt, eventArgs.get_item());
    }

    /** Methods to set the pagination details **/
    function currentPageIndexFD() {
        var currentPage;
        (grdFileDownloadsLanding.RecordCount == 0) ? (currentPage = 1) : (currentPage = grdFileDownloadsLanding.CurrentPageIndex + 1);
        return currentPage;
    }
    function pageCountFD() {
        var pageCount;
        (grdFileDownloadsLanding.RecordCount == 0) ? (pageCount = 1) : (pageCount = grdFileDownloadsLanding.PageCount);
        return pageCount;
    }
    function grdFileDownloadsLanding_onLoad(sender, eventArgs) {
        grdFileDownloadsLanding.sort(1, true);
        // event handler logic goes here
        // eventArgs is an empty EventArgs instance
    }
    /** Methods to set the pagination details **/


</script>

<ComponentArt:Snap ID="snapFileDownloads" runat="server" DockingContainers="rightContentSection"
    CurrentDockingContainer="rightContentSection" DockingStyle="TransparentRectangle"
    MustBeDocked="true" DraggingMode="Vertical" DraggingStyle="SolidOutline">
    <Header>
        <div class="topShadowBox">
            <div class="headerContainer">
                <div class="dragBox" onmousedown="snapFileDownloads.startDragging(event);">
                    <h3>
                        <asp:localize runat="server" text="<%$ Resources:GUIStrings, FileDownloads %>"/></h3>
                    <span>[&nbsp;<asp:HyperLink ID="linkFileDownloads" runat="server" Text="<%$ Resources:GUIStrings, ViewFullReport %>"
                        NavigateUrl="~/Statistics/Content/FileDownloads.aspx"></asp:HyperLink>&nbsp;]</span>
                </div>
                <div class="toggleBox">
                    <img src="../../images/snap-minimize.gif" runat="server" alt="<%$ Resources:GUIStrings, MinimizeSnap %>" onclick="snapFileDownloads.toggleExpand();" />
                </div>
                <div class="tabContainer">
                    <ComponentArt:TabStrip ID="tabstripFileDownloads" SkinID="Vista" runat="server" MultiPageId="">
                        <Tabs>
                            <ComponentArt:TabStripTab ID="tabFileDownloads" runat="server" Text="<%$ Resources:GUIStrings, FileDownloads %>" ToolTip="<%$ Resources:GUIStrings, FileDownloads %>">
                            </ComponentArt:TabStripTab>
                        </Tabs>
                    </ComponentArt:TabStrip>
                </div>
            </div>
        </div>
    </Header>
    <CollapsedHeader>
        <div class="topShadowBox">
            <div class="headerContainer">
                <div class="dragBox" onmousedown="snapFileDownloads.startDragging(event);">
                    <h3>
                        <asp:localize runat="server" text="<%$ Resources:GUIStrings, FileDownloads %>"/></h3>
                    <span>[&nbsp;<asp:HyperLink ID="HyperLink1" runat="server" Text="<%$ Resources:GUIStrings, ViewFullReport %>"
                        NavigateUrl="~/Statistics/Content/FileDownloads.aspx"></asp:HyperLink>&nbsp;]</span>
                </div>
                <div class="toggleBox">
                    <img src="../../images/snap-maximize.gif" runat="server" alt="<%$ Resources:GUIStrings, MaximizeSnap %>" onclick="snapFileDownloads.toggleExpand();" />
                </div>
            </div>
        </div>
    </CollapsedHeader>
    <Content>
        <div class="contentShadowBox contentShadowBoxNoGap">
            <div class="contentBox">
                <div class="gridContainer">
                    <ComponentArt:Grid SkinID="ScrollingGrid" ID="grdFileDownloadsLanding" runat="server"
                        RunningMode="Client" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
                        PageSize="10" Width="261" Height="200" SliderPopupClientTemplateId="grdFileDownloadsLandingSlider"
                        PagerInfoClientTemplateId="grdFileDownloadsLandingPagination">
                        <Levels>
                            <ComponentArt:GridLevel DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false"
                                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                                SortedHeadingCellCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText"
                                SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                                SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                                AlternatingRowCssClass="AlternateDataRow">
                                <Columns>
                                    <ComponentArt:GridColumn Width="111" runat="server" HeadingText="<%$ Resources:GUIStrings, AssetFileName %>" HeadingCellCssClass="FirstHeadingCell"
                                        DataField="AssetFileName" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                                        IsSearchable="true" AllowReordering="false" FixedWidth="true" />
                                    <ComponentArt:GridColumn Width="112" runat="server" HeadingText="<%$ Resources:GUIStrings, NoofDownloads %>" DataField="NoOfDownLoads"
                                        IsSearchable="true" DataType="System.Int64" SortedDataCellCssClass="SortedDataCell"
                                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="FileDownloadsCT" />
                                    <ComponentArt:GridColumn DataField="TotalVisits" Visible="false" />
                                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                                    <ComponentArt:GridColumn DataField="ContentId" Visible="false" />
                                    <ComponentArt:GridColumn DataField="DirectoryId" Visible="false" />
                                </Columns>
                            </ComponentArt:GridLevel>
                        </Levels>
                        <ClientEvents>
                            <ContextMenu EventHandler="grdFileDownloadsLanding_onContextMenu" />
                        </ClientEvents>
                        <ClientTemplates>
                            <ComponentArt:ClientTemplate ID="FileDownloadsCT">
                                <div style="text-align: right;">
                                    ## DataItem.GetMember("NoOfDownLoads").Value ##</div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="grdFileDownloadsLandingSlider">
                                <div class="SliderPopup">
                                    <h5>
                                        ## DataItem.GetMember(0).Value ##</h5>
                                    <p>
                                        ## DataItem.GetMember(1).Value ##</p>
                                    <p>
                                        ## DataItem.GetMember(2).Value ##</p>
                                    <p>
                                        ## DataItem.GetMember(3).Value ##</p>
                                    <p>
                                        ## DataItem.GetMember(4).Value ##</p>
                                    <p class="paging">
                                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdFileDownloadsLanding.PageCount) ##</span>
                                        <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdFileDownloadsLanding.RecordCount) ##</span>
                                    </p>
                                </div>
                            </ComponentArt:ClientTemplate>
                            <ComponentArt:ClientTemplate ID="grdFileDownloadsLandingPagination">
                               ## stringformat(Page0of12items, currentPageIndexFD(), pageCountFD(), grdFileDownloadsLanding.RecordCount) ##
                            </ComponentArt:ClientTemplate>
                        </ClientTemplates>
                        <ClientEvents>
                            <Load EventHandler="grdFileDownloadsLanding_onLoad" />
                        </ClientEvents>
                    </ComponentArt:Grid>
                    <ComponentArt:Menu ID="cmFileDownloadsAC" runat="server" SkinID="ContextMenu">
                        <Items>
                            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, JumptoFileLibrary %>" ID="fileLibrary">
                            </ComponentArt:MenuItem>
                        </Items>
                        <ClientEvents>
                            <ItemSelect EventHandler="cmFileDownloadsAC_onItemSelect" />
                        </ClientEvents>
                    </ComponentArt:Menu>
                </div>
            </div>
        </div>
    </Content>
    <Footer>
        <div class="bottomShadowBox recentlyPublishedFooter">
            <div class="gridFooterContainer">
            </div>
        </div>
    </Footer>
    <CollapsedFooter>
        <div class="bottomShadowBox">
            <div class="gridFooterContainer">
            </div>
        </div>
    </CollapsedFooter>
</ComponentArt:Snap>

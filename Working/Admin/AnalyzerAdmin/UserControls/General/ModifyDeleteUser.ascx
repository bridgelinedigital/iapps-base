<%@ Control Language="C#" AutoEventWireup="True" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ModifyDeleteUser" %>
<script type="text/javascript"s>
    function grdUsers_onContextMenu(sender, eventArgs) {
        gridItem = eventArgs.get_item();
        var evt = eventArgs.get_event();
        menuItems = mnuUsers.get_items();

        var statusMnuItem = menuItems.getItemById("DeactivateMenuItem");
        var deleteMnuItem = menuItems.getItemById("DeleteUserMenuItem");
        if (gridItem.getMember("UserId").get_text().toLowerCase() == jUserId.toLowerCase()) {
            statusMnuItem.set_visible(false);
            deleteMnuItem.set_visible(false);
        }
        else {
            statusMnuItem.set_visible(true);
            deleteMnuItem.set_visible(true);

            //the user is deactivated. so the context menu should show the activate menu
            if (gridItem.getMember('IsActivated').get_text() == "true")
                statusMnuItem.set_text(__JSMessages["ActivateUser"]);
            else
                statusMnuItem.set_text(__JSMessages["DeactivateUser"]);
        }

        mnuUsers.showContextMenuAtEvent(evt);
    }

    function mnuUsers_onItemSelect(sender, eventArgs) {
        var menuItem = eventArgs.get_item();
        var selectedMenuId = menuItem.get_id();

        var selectedUserId = gridItem.getMember("UserId").get_text();
        var selectedUserName = gridItem.getMember("UserName").get_text();
        var currentPermission = AdminCallback.GetUserPermission(selectedUserId);
        //var currentPermission = gridItem.getMember("AllPermissions").get_text();
        var memberType = 1;

        if (selectedMenuId != null) {
            var isConfirm = false;
            switch (selectedMenuId) {
                case 'DeleteUserMenuItem':
                case 'DeactivateMenuItem':
                    var deleteUser = selectedMenuId == "DeleteUserMenuItem" ? true : false;
                    var activateUser = FormatStatusColumn(gridItem.getMember("IsActivated").get_text()) == __JSMessages["ActiveStatus"] ? false : true;
                    if (deleteUser)
                        isConfirm = window.confirm(GetMessage("DeleteConfirmation"));
                    else if (activateUser)
                        isConfirm = window.confirm(GetMessage("ActivateConfirmation"));
                    else
                        isConfirm = window.confirm(GetMessage("DeactivateConfirmation"));

                    if (isConfirm) {
                        var count = 0;
                        if (deleteUser || !activateUser)
                            count = CheckUserWorklows(selectedUserId, memberType, selectedUserName, deleteUser ? currentPermission : null);

                        if (count == -1) {
                            var sitesString = GetSiteLists(currentPermission);
                            alert(deleteUser ? GetMessage('DeleteUserDeniedCrossSite') : GetMessage('DeactivateUserDeniedCrossSite') + " " + sitesString);
                        }
                        else if (count == 1) {
                            alert(deleteUser ? GetMessage('DeleteUserDeniedWorkflow') : GetMessage('DeactivateUserDeniedWorkflow'));
                        }
                        else if (count == 2) {
                            //If any siteadmins are trying to delete the install admins, then it is not allowed.
                            alert(deleteUser ? GetMessage('DeleteUserDeniedPermission') : GetMessage('DeleteUserDeniedPermission'));
                        }
                        else {
                            var statusOfOperation = DeleteDeactivate(selectedUserId, selectedUserName, deleteUser ? 1 : (activateUser ? 4 : 2), null);
                            if (statusOfOperation == "True")
                                grdUsers.callback();
                            else
                                alert(statusOfOperation);
                        }
                    }
                    break;
                case 'EditUserMenuItem':
                    window.location = jAnalyticAdminSiteUrl + "/Administration/AddNewUser.aspx?UserId=" + selectedUserId;
                    break;
            }
        }
    }

    function SearchUsers() {
        grdUsers.set_callbackParameter(stringformat("{0}^{1}", $("#<%= txtSearchUser.ClientID %>").val(), $("#<%=ddlRole.ClientID %>").val()));
        grdUsers.callback();

        return false;
    }

    function SetColumnVisibility() {
        var gridc = grdUsers.get_table().get_columns();
        var selectedRoleId = $("#<%= ddlRole.ClientID %>").val();
        
        grdUsers.render();
    }

    function grdUsers_onRenderComplete(sender, eventArgs) {
        FormatDataGrid(sender, false);
    }

    function grdUsers_onCallbackComplete(sender, eventArgs) {
        SetColumnVisibility();
    }

    function grdUsers_onLoad(sender, eventArgs) {
        SetColumnVisibility();
    }

</script>
<div class="content-area">
    <div class="grid-utility">
        <asp:TextBox runat="server" ID="txtSearchUser" Width="240" CssClass="textBoxes" ToolTip="<%$ Resources:GUIStrings, TypeHereToFilterResults %>"
            ClientIDMode="Static" />
        <asp:DropDownList runat="server" ID="ddlRole" Width="210" ClientIDMode="Static">
            <asp:ListItem Text="<%$ Resources:GUIStrings, AllAnalyticsUsers %>" Value="0" />
            <asp:ListItem Text="<%$ Resources:GUIStrings, Administrators %>" Value="16" />
            <asp:ListItem Text="<%$ Resources:GUIStrings, Analysts %>" Value="17" />
            <asp:ListItem Text="<%$ Resources:GUIStrings, iAPPSUsers %>" Value="-1" />
        </asp:DropDownList>
        <input type="button" class="small-button" value="<%= GUIStrings.Search %>" onclick="return SearchUsers();" />
    </div>
    <ComponentArt:Grid AllowMultipleSelect="false" EmptyGridText="<%$ Resources:GUIStrings, NoUsersFound %>"
        RunningMode="Callback" SkinID="Default" ID="grdUsers" SliderPopupClientTemplateId="grdUsers_sliderTemplate"
        SliderPopupCachedClientTemplateId="grdUsers_cachedSliderTemplate" Width="100%"
        runat="server" PageSize="10" CallbackCachingEnabled="true" PagerInfoClientTemplateId="grdUsers_pageInfoTemplate"
        AutoAdjustPageSize="true" LoadingPanelClientTemplateId="grdUsers_loadingPanelTemplate">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="UserId" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="true" Width="150" DataField="UserName"
                        runat="server" HeadingText="<%$ Resources:GUIStrings, UserName %>" HeadingCellCssClass="FirstHeadingCell"
                        DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        DataCellClientTemplateId="UserNameTemplate" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="true" Width="140" DataField="LastName"
                        runat="server" HeadingText="<%$ Resources:GUIStrings, LastName %>" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" DataCellClientTemplateId="LastNameTemplate" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="true" Width="140" DataField="FirstName"
                        runat="server" HeadingText="<%$ Resources:GUIStrings, FirstName %>" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" DataCellClientTemplateId="FirstNameTemplate" />
<%--                    <ComponentArt:GridColumn FixedWidth="true" Width="185" DataField="GroupName" runat="server"
                        HeadingText="<%$ Resources:GUIStrings, GroupName %>" DefaultSortDirection="Descending"
                        SortedDataCellCssClass="SortedDataCell" DataCellClientTemplateId="GroupNameTemplate" />
                    <ComponentArt:GridColumn FixedWidth="true" Width="300" DataField="Permissions" runat="server"
                        HeadingText="<%$ Resources:GUIStrings, CurrentPermissions %>" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" DataCellClientTemplateId="PermissionsTemplate" />--%>
                    <ComponentArt:GridColumn FixedWidth="true" DataField="UserId" Visible="false" IsSearchable="false" />
<%--                    <ComponentArt:GridColumn FixedWidth="true" DataField="AllPermissions" Visible="false"
                        IsSearchable="false" />--%>
                    <ComponentArt:GridColumn FixedWidth="true" DataField="Email" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="ExpiryDate" Visible="false"
                        IsSearchable="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="EmailNotification" Visible="false"
                        IsSearchable="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="IsActivated" Visible="false"
                        IsSearchable="false" DataCellClientTemplateId="IsActivatedTemplate" HeadingText="Status" />
                   <%-- <ComponentArt:GridColumn FixedWidth="true" DataField="ProductSuite" HeadingText="<%$ Resources:GUIStrings, ProductSuites %>"
                        IsSearchable="false" AllowReordering="false" Width="200" DataCellClientTemplateId="ProductSuiteTemplate"
                        Visible="false" />--%>
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <Load EventHandler="grdUsers_onLoad" />
            <CallbackComplete EventHandler="grdUsers_onCallbackComplete" />
            <BeforeCallback EventHandler="CheckSessionEventHandler" />
            <ContextMenu EventHandler="grdUsers_onContextMenu" />
            <RenderComplete EventHandler="grdUsers_onRenderComplete" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="grdUsers_sliderTemplate">
                <div class="nSliderPopup">
                    <p>
                        ##_datanotload##</p>
                    <p class="npaging">
                        <span class="npages">## stringformat(pageof, DataItem.PageIndex + 1, grdUsers.PageCount)
                            ##</span> <span class="nitems">## stringformat(itemof, DataItem.Index + 1, grdUsers.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdUsers_cachedSliderTemplate">
                <div class="nSliderPopup">
                    <h5>
                        ## DataItem.GetMember('UserName').Value ##</h5>
                    <p>
                        ## DataItem.GetMember('LastName').Value + ', ' + DataItem.GetMember('FirstName').Value
                        ##</p>
                    <p>
                        ## DataItem.GetMember('GroupName').Value ##</p>
                    <p>
                        ## DataItem.GetMember('Email').Value ##</p>
                    <p>
                        ## DataItem.GetMember('Permissions').Value ##</p>
                    <p class="npaging">
                        <span class="npages">## stringformat(pageof, DataItem.PageIndex + 1, grdUsers.PageCount)
                            ##</span> <span class="nitems">## stringformat(itemof, DataItem.Index + 1, grdUsers.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdUsers_pageInfoTemplate">
                ## stringformat(Page0of12items, gridCurrentPageIndex(grdUsers), gridPageCount(grdUsers),
                grdUsers.RecordCount) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdUsers_loadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdUsers) ##
            </ComponentArt:ClientTemplate>
            <%-- Tool Tip for Datagrid Start --%>
            <ComponentArt:ClientTemplate ID="UserNameTemplate">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('UserName').get_text()) ##">
                    ## DataItem.getMember('UserName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="LastNameTemplate">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('LastName').get_text()) ##">
                    ## DataItem.getMember('LastName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="FirstNameTemplate">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('FirstName').get_text()) ##">
                    ## DataItem.getMember('FirstName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
<%--            <ComponentArt:ClientTemplate ID="GroupNameTemplate">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('GroupName').get_text()) ##">
                    ## DataItem.getMember('GroupName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="PermissionsTemplate">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('Permissions').get_text()) ##">
                    ## DataItem.getMember('Permissions').get_text() ##</span>
            </ComponentArt:ClientTemplate>--%>
            <ComponentArt:ClientTemplate ID="ProductSuiteTemplate">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('ProductSuite').get_text()) ##">
                    ## DataItem.getMember('ProductSuite').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="IsActivatedTemplate">
                <span title="## FormatStatusColumn(DataItem.getMember('IsActivated').get_text()) ##">
                    ## FormatStatusColumn(DataItem.getMember('IsActivated').get_text()) ##</span>
            </ComponentArt:ClientTemplate>
            <%-- Tool Tip for Datagrid End --%>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="mnuUsers" SkinID="ContextMenu" runat="server">
        <ClientEvents>
            <ItemSelect EventHandler="mnuUsers_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</div>

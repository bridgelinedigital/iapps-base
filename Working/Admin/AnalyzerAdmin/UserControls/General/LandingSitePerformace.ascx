<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LandingSitePerformace.ascx.cs" Inherits="UserControls_General_LandingSitePerformace" %>
<script type="text/javascript">
var iframeObject = null;
function CalendarFrom_OnChange(sender, eventArgs)
{
    var selectedDate = CalendarFrom.getSelectedDate();
    var date = new Date();
    if(selectedDate >= date)
    {
        alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, DateShouldNotExceedTodaysDate %>'/>");
        selectedDate = date;       
    }
    else
    {
        document.getElementById("<%=txtFromDate.ClientID%>").value = CalendarFrom.formatDate(selectedDate, "MM/dd/yyyy");
    }
}
function CalendarTo_OnChange(sender, eventArgs)
{
    var selectedDate = CalendarTo.getSelectedDate();
    var date = new Date();
       
     if(selectedDate >= date)
     {
         alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, DateShouldNotExceedTodaysDate %>'/>");
        selectedDate = date;       
     }
    document.getElementById("<%=txtToDate.ClientID%>").value = CalendarTo.formatDate(selectedDate, "MM/dd/yyyy");
}
function popUpFromCalendar(textBoxId)
{
    if(document.getElementById("<%=txtFromDate.ClientID%>").disabled == false)
    {
        var thisDate = new Date();
        var textBoxObject = document.getElementById(textBoxId);
        if (Trim(textBoxObject.value) == "<asp:localize runat='server' text='<%$ Resources:JSMessages, FromDate %>'/>" || Trim(textBoxObject.value) == "")
        {
            textBoxObject.value = "";
        }
        if (textBoxObject.value != "<asp:localize runat='server' text='<%$ Resources:JSMessages, FromDate %>'/>" && Trim(textBoxObject.value) != "")
        {
            if(validate_dateSummary(textBoxObject)==true)// available in validation.js 
            {
                thisDate = new Date(textBoxObject.value);
                CalendarFrom.setSelectedDate(thisDate);
            }
        }
        else
        {
            //CalendarFrom.setSelectedDate(thisDate);
        }
        if(!(CalendarFrom.get_popUpShowing())); 
            CalendarFrom.show();
    }
}
function popUpToCalendar(textBoxId)
{
    if(document.getElementById("<%=txtToDate.ClientID%>").disabled == false)
    {
        var thisDate = new Date();
        var textBoxObject = document.getElementById(textBoxId);
        if (Trim(textBoxObject.value) == "<asp:localize runat='server' text='<%$ Resources:JSMessages, ToDate %>'/>" || Trim(textBoxObject.value) == "")
        {
            textBoxObject.value = "";
        }
        if (textBoxObject.value != "<asp:localize runat='server' text='<%$ Resources:JSMessages, ToDate %>'/>" && Trim(textBoxObject.value) != "")
        {
            if(validate_dateSummary(textBoxObject)==true)// available in validation.js 
            {
                thisDate = new Date(textBoxObject.value);
                CalendarTo.setSelectedDate(thisDate);
            }
        }
        else
        {
            //CalendarTo.setSelectedDate(thisDate);
        }
        if(!(CalendarTo.get_popUpShowing())); 
            CalendarTo.show();
    }
}
function ShowDateRange()
{
    var destinationObject = document.getElementById("dateRangeSelector");
    var sourceObject = document.getElementById("rangeSelector");
    if(destinationObject.style.display == "none" || destinationObject.style.display == "")
    {
        destinationObject.style.display = "block";
        setPosition(destinationObject, sourceObject);
        if(document.getElementById('<%=rbSelectRange.ClientID%>').checked == true)
        {
            disableFields('custom');
        }
        else
        {
            disableFields('predefined');
        }
        UpdateChangeRange();
        iframeObject = cover(destinationObject);
    }
    else
    {
        destinationObject.style.display = "none";
        if(iframeObject != null)
        {
            iframeObject.style.display = "none";
        }
        UpdateChangeRange();
    }
}
function setPosition(destObj, srcObj)
{
    var objLeft   = srcObj.offsetLeft;
    var objTop    = srcObj.offsetTop;
    var objParent = srcObj.offsetParent;
    while( objParent.tagName.toUpperCase() != "BODY" && objParent.offsetParent != null )
    {
        objLeft  += objParent.offsetLeft;
        objTop   += objParent.offsetTop;
        objParent = objParent.offsetParent;
    }
    destObj.style.top = (objTop + 22) + "px";
    destObj.style.left = (objLeft - 180) + "px";
}
function UpdateChangeRange()
{
    var sourceObject = document.getElementById("rangeSelector");
    var buttonClass = sourceObject.className
    if(buttonClass.indexOf("Hover") > -1)
    {
        newButtonClass = "rangeSelector";
        oldButtonClass = "rangeSelector" + "Hover";
        sourceObject.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
    else
    {
        oldButtonClass = "rangeSelector";
        newButtonClass = "rangeSelector" + "Hover";
        sourceObject.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
}
function ValidateRange()
{
    var retValue = DateChecking();
    if (retValue)
    {
        ShowDateRange();
    }
    else
    {
        return retValue;
    }
}

function DateChecking()
{
 var retValue = true;
 var errorMessage='';

 if (document.getElementById('<%=rbSelectRange.ClientID%>').checked == true)
   {
           if (document.getElementById('<%=txtFromDate.ClientID %>').value != '')
            {
                if(validate_dateSummary(document.getElementById('<%=txtFromDate.ClientID %>'))==false)
                {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseFormatTheSelectDateRangeStartDateAsMmddyyyy %>'/>";
                    retValue = false;
                }
            }
            else
            {
                errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, ForSelectDateRangepleaseenterthestartdate %>'/>";
                    retValue = false;
            }
            
            if (document.getElementById('<%=txtToDate.ClientID %>').value != '')
            {
                if(validate_dateSummary(document.getElementById('<%=txtToDate.ClientID %>'))==false)
                {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseformattheSelectDateRangeenddateasmmddyyyy %>'/>";
                    retValue = false;
                }
            }
            else
            {
                errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, ForSelectDateRangepleaseentertheenddate %>'/>";
                    retValue = false;
            }
        if(document.getElementById('<%=txtFromDate.ClientID %>').value !="" && document.getElementById('<%=txtToDate.ClientID %>').value !="")
        {
            if(validate_dateSummary(document.getElementById('<%=txtToDate.ClientID %>'))==true  &&  validate_dateSummary(document.getElementById('<%=txtFromDate.ClientID %>'))==true)
            {
                fromdate= new Date(document.getElementById('<%=txtFromDate.ClientID %>').value);
                todate= new Date(document.getElementById('<%=txtToDate.ClientID %>').value);
                if(fromdate>todate)
                {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, ForSelectDateRangetheenddatemustbelaterthanthestartdaten %>'/>";
                      retValue = false;
                }
            }
        }
        if(document.getElementById('<%=txtFromDate.ClientID %>').value !="" && document.getElementById('<%=txtToDate.ClientID %>').value !="")
        {
            if(validate_dateSummary(document.getElementById('<%=txtToDate.ClientID %>'))==true  &&  validate_dateSummary(document.getElementById('<%=txtFromDate.ClientID %>'))==true)
            {
                fromdate= new Date(document.getElementById('<%=txtFromDate.ClientID %>').value);
                todate= new Date(document.getElementById('<%=txtToDate.ClientID %>').value);
                var currentDate = new Date();
                if(fromdate>currentDate)
                {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, StartDateShouldNotExceedTodaysDaten %>'/>";
                      retValue = false;
                }
                if(todate>currentDate)
                {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, EndDateShouldNotExceedTodaysDaten %>'/>";
                      retValue = false;
                }
            }
        }
        
 } 
if(!retValue)
    alert(errorMessage);
 return retValue;
}
function disableFields(fieldText)
{
    switch(fieldText)
    {
        case 'predefined':
            document.getElementById("<%=txtFromDate.ClientID%>").disabled = true;
            document.getElementById("<%=txtToDate.ClientID%>").disabled = true;
            document.getElementById("<%=txtFromDate.ClientID%>").value = "<asp:localize runat='server' text='<%$ Resources:JSMessages, FromDate %>'/>";
            document.getElementById("<%=txtToDate.ClientID%>").value = "<asp:localize runat='server' text='<%$ Resources:JSMessages, ToDate %>'/>";
            document.getElementById("<%=ddlPredefindedRange.ClientID%>").disabled = false;
            break;
        case 'custom':
            document.getElementById("<%=txtFromDate.ClientID%>").disabled = false;
            document.getElementById("<%=txtToDate.ClientID%>").disabled = false;
            var date = new Date();
            if (document.getElementById("<%=txtFromDate.ClientID%>").value == "<asp:localize runat='server' text='<%$ Resources:JSMessages, FromDate %>'/>")
            {
                document.getElementById("<%=txtFromDate.ClientID%>").value = CalendarFrom.formatDate(date, "MM/dd/yyyy");
                document.getElementById("<%=txtToDate.ClientID%>").value = CalendarTo.formatDate(date, "MM/dd/yyyy");
            }
            document.getElementById("<%=ddlPredefindedRange.ClientID%>").disabled = true;
            break;
    }
}
</script>

<div id="dateRangeContainer">
    <div class="dateRangeSelector" id="dateRangeSelector">
        <span class="formRow">
            <asp:RadioButton ID="rbPredefindedRange" runat="server" Text="<%$ Resources:GUIStrings, PredefinedRanges %>" Checked="true" GroupName="DateRange" onclick="disableFields('predefined');" />
        </span>
        <span class="formRow">
            <asp:DropDownList ID="ddlPredefindedRange" runat="server" Width="210">
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, CurrentWeek %>" Value="CurrentWeek"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, MonthToDate %>" Value="MonthToDate"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, QuarterToDate %>" Value="QuarterToDate"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, YearToDate %>" Value="YearToDate"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Last30Days %>" Value="Last30days"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Last60Days %>" Value="Last60days"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Last180Days %>" Value="Last180days"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Last365Days %>" Value="Last365days"></asp:ListItem>
            </asp:DropDownList>
        </span>
        <span class="formRow">
            <asp:RadioButton ID="rbSelectRange" runat="server" Text="<%$ Resources:GUIStrings, SelectDateRange %>" GroupName="DateRange" onclick="disableFields('custom');" />
        </span>
        <span class="formRow">
            <asp:TextBox ID="txtFromDate" runat="server" Text="<%$ Resources:GUIStrings, FromDate %>" CssClass="textBoxes" Width="168" Enabled="false"></asp:TextBox>
            <asp:Image ImageUrl="~/App_Themes/General/images/Calender-btn.gif" AlternateText="<%$ Resources:GUIStrings, FromDate %>" ToolTip="<%$ Resources:GUIStrings, SelectFromDate %>"
                ID="imgFromDate" runat="server" />
            <ComponentArt:Calendar runat="server" id="CalendarFrom" SkinID="Default">
                <ClientEvents>
                    <SelectionChanged EventHandler="CalendarFrom_OnChange" />
                </ClientEvents>
            </ComponentArt:Calendar>
        </span>
        <span class="formRow">
            <asp:TextBox ID="txtToDate" runat="server" Text="<%$ Resources:GUIStrings, ToDate %>" CssClass="textBoxes" Width="168" Enabled="false"></asp:TextBox>
            <asp:Image ImageUrl="~/App_Themes/General/images/Calender-btn.gif" AlternateText="<%$ Resources:GUIStrings, ToDate %>" ToolTip="<%$ Resources:GUIStrings, SelectToDate %>"
                ID="imgToDate" runat="server" />
            <ComponentArt:Calendar runat="server" id="CalendarTo" SkinID="Default">
                <ClientEvents>
                    <SelectionChanged EventHandler="CalendarTo_OnChange" />
                </ClientEvents>
            </ComponentArt:Calendar>
        </span>
        <span class="formRow">
            <asp:Button ID="btnSubmit" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Submit %>" 
                 ToolTip="<%$ Resources:GUIStrings, Submit %>" OnClientClick="return ValidateRange();" />
        </span>
    </div>
</div>
<ComponentArt:Snap ID="snapSitePerformance" runat="server" Width="100%" DockingContainers="HorizontalContainer"
    CurrentDockingContainer="HorizontalContainer" DockingStyle="TransparentRectangle" MustBeDocked="true"
    DraggingMode="FreeStyle" DraggingStyle="SolidOutline">
    <Header>
        <div class="topShadowBox">
            <div class="headerContainer">
                <div class="dragBox" onmousedown="snapSitePerformance.startDragging(event);">
                    <h3><asp:localize runat="server" text="<%$ Resources:GUIStrings, SitePerformance %>"/></h3><span>[&nbsp;<asp:HyperLink ID="HyperLink1" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, ViewFullReport %>"/></asp:HyperLink>&nbsp;]</span>
                </div>
                <div class="rightHeaderSection">
                    <div class="rangeContainer">
                        <h6><asp:localize runat="server" text="<%$ Resources:GUIStrings, ReportingPeriod %>"/></h6>
                        <asp:Label ID="startPeriod" runat="server" Text="MTD Aug 1, 2007" CssClass="selectedDate"></asp:Label>
                        <span>-</span>
                        <asp:Label ID="endPeriod" runat="server" Text="Aug 27, 2007" CssClass="selectedDate"></asp:Label>
                        <label>|</label>
                        <label id="rangeSelector" class="rangeSelector" onclick="ShowDateRange();"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Change %>"/></label>
                    </div>
                    <div class="toggleBox">
                        <img src="../images/snap-minimize.gif" alt="Minimize Snap" onclick="snapSitePerformance.toggleExpand();" />
                    </div>
                </div>
            </div>
        </div>
    </Header>
    <CollapsedHeader>
        <div class="topShadowBox">
            <div class="headerContainer">
                <div class="dragBox" onmousedown="snapSitePerformance.startDragging(event);">
                    <h3><asp:localize runat="server" text="<%$ Resources:GUIStrings, SitePerformance %>"/></h3><span>[&nbsp;<asp:HyperLink ID="HyperLink2" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, ViewFullReport %>"/></asp:HyperLink>&nbsp;]</span>
                </div>
                <div class="rightHeaderSection">
                    <div class="rangeContainer">
                        <h6><asp:localize runat="server" text="<%$ Resources:GUIStrings, ReportingPeriod %>"/></h6>
                        <asp:Label ID="startPeriodCollapsed" runat="server" Text="MTD Aug 1, 2007" CssClass="selectedDate"></asp:Label>
                        <span>-</span>
                        <asp:Label ID="endPeriodCollapsed" runat="server" Text="Aug 27, 2007" CssClass="selectedDate"></asp:Label>
                        <label>|</label>
                        <label id="endRangeSelector" class="rangeSelector" onclick="ShowDateRange();"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Change %>"/></label>
                    </div>
                    <div class="toggleBox">
                        <img src="../images/snap-maximize.gif" alt="<%= GUIStrings.MaximizeSnap %>" onclick="snapSitePerformance.toggleExpand();" />
                    </div>
                </div>
            </div>
        </div>
    </CollapsedHeader>
    <Content>
        <div class="contentShadowBox">
            <div class="contentBox">
            <div class="clearFix">&nbsp;</div>
                <div class="gridContainer">
                    <table border="0" cellpadding="2" cellspacing="0" width="700">
                        <tr>
                            <td valign="top" width="200"><strong><asp:localize runat="server" text="<%$ Resources:GUIStrings, Pageviews4 %>"/> </strong><asp:Label runat="server" id="lblPageViews"></asp:Label></td>
                            <td valign="top" width="300"><strong><asp:localize runat="server" text="<%$ Resources:GUIStrings, AverageTimeOnSite1 %>"/> </strong><asp:Label runat="server" id="lblAvgTimeOnSite"></asp:Label></td>
                            <td valign="top" width="200"><strong><asp:localize runat="server" text="<%$ Resources:GUIStrings, SinglePageVisits2 %>"/> </strong><asp:Label runat="server" id="lblSinglePageVisits"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" width="200"><strong><asp:localize runat="server" text="<%$ Resources:GUIStrings, PagesVisit1 %>"/> </strong><asp:Label runat="server" id="lblPagesperVisit"></asp:Label></td>
                            <td valign="top" width="300"><strong><asp:localize runat="server" text="<%$ Resources:GUIStrings, TotalVisits2 %>"/> </strong><asp:Label runat="server" id="lblTotalVisits"></asp:Label></td>
                            <td valign="top" width="200">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </Content>
        <Footer>
        <div class="bottomShadowBox">
            <div class="gridFooterContainer"></div>
        </div>
    </Footer>
    <CollapsedFooter>
        <div class="bottomShadowBox bottomShadowBoxGap">
            <div class="gridFooterContainer"></div>
        </div>
    </CollapsedFooter>
</ComponentArt:Snap>

<%@ Control Language="C#" AutoEventWireup="true" Inherits="LandingTopPages" CodeBehind="LandingTopPages.ascx.cs" %>
<script type="text/javascript">
    var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
    var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
    var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
    var item = "";
    var pageId = "";
    function grdLandingTopPages_onContextMenu(sender, eventArgs) {
        var evt = eventArgs.get_event();
        item = eventArgs.get_item();
        grdLandingTopPages.select(item);
        var x = (browser == 'ie' ? evt.clientX : evt.clientX + document.documentElement.scrollLeft);
        var y = (browser == 'ie' ? evt.clientY : evt.clientY + document.documentElement.scrollTop);
        cmTopPagesAC.showContextMenuAtPoint(x, y, eventArgs.get_item());
    }
    function cmTopPagesAC_onItemSelect(sender, eventArgs) {
        var selectedMenu = eventArgs.get_item().get_id();
        switch (selectedMenu) {
            case "ViewStatistics":
                var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', 'width=750px,height=635px,center=1,resize=0,scrolling=1');
                viewPopupWindow.onclose = function () {
                    var a = document.getElementById('Content');
                    var ifr = a.getElementsByTagName("iframe");
                    window.frames[ifr[0].name].location.replace("about:blank");
                    return true;
                }
                break;
            case "ViewAnalysis":
                window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                break;
            case "ViewNewWindow":
                window.open(analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text);
                break;
            case "ViewSiteEditor":
                var currentUrl = document.URL;
                if (currentUrl == null || currentUrl == '') {
                    currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                }
                if (currentUrl.indexOf('?') > 0) {
                    currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                }
                currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenBySiteAndProduct(cmsProductId, siteId, siteName);
                break;
            case "AssignIndexTerms":
                var pageId = item.getMember("Original_Id").get_text().replace("{", "").replace("}", "");
                OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                break;
            case "MenuEmailAuthor":
                var email = item.GetMember('Author_Email').Text;
                location.href = "mailto:" + email + "";
                break;
        }
    }

    /** Methods to set the pagination details **/
    function currentPageIndexTopPages() {
        var currentPage;
        (grdLandingTopPages.RecordCount == 0) ? (currentPage = 1) : (currentPage = grdLandingTopPages.CurrentPageIndex + 1);
        return currentPage;
    }
    function pageCountTopPages() {
        var pageCount;
        (grdLandingTopPages.RecordCount == 0) ? (pageCount = 1) : (pageCount = grdLandingTopPages.PageCount);
        return pageCount;
    }
    /** Methods to set the pagination details **/
    function clearDefaultText(textboxObj) {
        if (textboxObj.value == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Typeheretofilterresults %>'/>") {
            textboxObj.value = "";
        }
    }
    function setDefaultText(textboxObj) {
        if (textboxObj.value == "") {
            textboxObj.value = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Typeheretofilterresults %>'/>";
        }
    }
    /** Method for Key Press Search **/
    function searchTopPagesGrid(evt) {
        if (validKey(evt)) {
            var str = document.getElementById("txtSearchTopPages").value;
            grdLandingTopPages.Search(str, false);
        }
    }
    function grdLandingTopPages_onPageIndexChange(sender, eventArgs) {
        var startRecord = (eventArgs.get_index() * sender.get_pageSize()) + 1;
        var endRecord = (eventArgs.get_index() * sender.get_pageSize()) + sender.get_pageSize();
        if (sender.get_recordCount() == 0) {
            startRecord = 0;
        }
        //if record count is less than the calculated end record number then make end record equal to record count
        if (endRecord > sender.get_recordCount() || sender.get_recordCount() < sender.get_pageSize()) {
            endRecord = sender.get_recordCount();
        }
        document.getElementById("<%=DisplayingRecordsAR.ClientID%>").innerHTML = stringformat("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, Displaying01of2 %>' />", startRecord, endRecord, sender.get_recordCount());

        //document.getElementById("<%=startRecordAR.ClientID%>").innerHTML = startRecord;
        //document.getElementById("<%=endRecordAR.ClientID%>").innerHTML = endRecord;
        //document.getElementById("<%=totalRecordsAR.ClientID%>").innerHTML = sender.get_recordCount();
    }
    function grdLandingTopPages_onLoad(sender, eventArgs) {
        grdLandingTopPages.sort(1, true);
    }

</script>
<div style="width: 240px">
    &nbsp;
    <ComponentArt:Snap ID="snapTopPages" runat="server" Width="100%" DockingContainers="leftContentSection"
        CurrentDockingContainer="leftContentSection" DockingStyle="TransparentRectangle"
        MustBeDocked="true" DraggingMode="FreeStyle" DraggingStyle="SolidOutline">
        <Header>
            <div class="topShadowBox">
                <div class="headerContainer">
                    <div class="dragBox" onmousedown="snapTopPages.startDragging(event);">
                        <h3>
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TopPages %>" /></h3>
                        <span>[&nbsp;<asp:HyperLink ID="linkTopPages" NavigateUrl="~/Statistics/Traffic/TopPages.aspx"
                            runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, ViewFullReport %>"/></asp:HyperLink>&nbsp;]</span>
                    </div>
                    <div class="toggleBox">
                        <img src="~/images/snap-minimize.gif" runat="server" alt="<%$ Resources:GUIStrings, MinimizeSnap %>"
                            onclick="snapTopPages.toggleExpand();" />
                    </div>
                    <div class="tabContainer">
                        <ComponentArt:TabStrip ID="tabPages" SkinID="Vista" runat="server">
                            <Tabs>
                                <ComponentArt:TabStripTab runat="server" Text="<%$ Resources:GUIStrings, TopPages %>"
                                    ToolTip="<%$ Resources:GUIStrings, TopPages %>">
                                </ComponentArt:TabStripTab>
                            </Tabs>
                        </ComponentArt:TabStrip>
                    </div>
                </div>
            </div>
        </Header>
        <CollapsedHeader>
            <div class="topShadowBox">
                <div class="headerContainer">
                    <div class="dragBox" onmousedown="snapTopPages.startDragging(event);">
                        <h3>
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TopPages %>" /></h3>
                        <span>[&nbsp;<asp:HyperLink ID="linkTopPagesCollapsed" NavigateUrl="~/Statistics/Traffic/TopPages.aspx"
                            runat="server"><asp:localize runat="server" text="<%$ Resources:GUIStrings, ViewFullReport %>"/></asp:HyperLink>&nbsp;]</span>
                    </div>
                    <div class="toggleBox">
                        <img src="~/images/snap-maximize.gif" runat="server" alt="<%$ Resources:GUIStrings, MaximizeSnap %>"
                            onclick="snapTopPages.toggleExpand();" />
                    </div>
                </div>
            </div>
        </CollapsedHeader>
        <Content>
            <div class="contentShadowBox">
                <div class="contentBox">
                    <div class="gridContainer">
                        <div class="searchContainer">
                            <input id="txtSearchTopPages" class="text textBoxes" value="<%= GUIStrings.TypeHereToFilterResults %>"
                                onfocus="clearDefaultText(this);" onblur="setDefaultText(this);" onkeyup="searchTopPagesGrid(event);" />
                            <div class="pagingInfo" id="pagingInfoAR" runat="server">
                                <asp:Label runat="server" ID="DisplayingRecordsAR" />
                                <%--<span>Displaying&nbsp;</span>--%>
                                <asp:Label runat="server" ID="startRecordAR" Visible="false"></asp:Label>
                                <%--<span>&nbsp;-&nbsp;</span>--%>
                                <asp:Label runat="server" ID="endRecordAR" Visible="false"></asp:Label>
                                <%--<span>&nbsp;of&nbsp;</span>--%>
                                <asp:Label runat="server" ID="totalRecordsAR" Visible="false"></asp:Label>
                            </div>
                        </div>
                        <ComponentArt:Grid SkinID="Default" ID="grdLandingTopPages" runat="server" RunningMode="Client"
                            EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
                            PageSize="10" SliderPopupClientTemplateId="grdLandingTopPagesSlider" PagerInfoClientTemplateId="grdLandingTopPagesPagination"
                            Width="666">
                            <ClientEvents>
                                <Load EventHandler="grdLandingTopPages_onLoad" />
                            </ClientEvents>
                            <Levels>
                                <ComponentArt:GridLevel DataKeyField="Original_Id" ShowTableHeading="false" ShowSelectorCells="false"
                                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                                    SortedHeadingCellCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText"
                                    SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                                    SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                                    AlternatingRowCssClass="AlternateDataRow">
                                    <Columns>
                                        <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                                            HeadingCellCssClass="FirstHeadingCell" DataField="Title" DataCellCssClass="FirstDataCell"
                                            SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                                            FixedWidth="true" />
                                        <ComponentArt:GridColumn Width="75" runat="server" HeadingText="<%$ Resources:GUIStrings, Pageviews1 %>"
                                            DataField="Page_Visits" DefaultSortDirection="Descending" IsSearchable="false"
                                            DataType="System.Int64" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                                            FixedWidth="true" Align="Right" />
                                        <ComponentArt:GridColumn Width="125" runat="server" HeadingText="<%$ Resources:GUIStrings, UniquePageviews %>"
                                            DataField="Unique_Page_Visits" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                                            FixedWidth="true" Align="Right" DataType="System.Int64" IsSearchable="false" />
                                        <ComponentArt:GridColumn Width="93" runat="server" HeadingText="<%$ Resources:GUIStrings, TimeonPage %>"
                                            DataField="Page_Dwell_Time" DefaultSortDirection="Descending" SortedDataCellCssClass="SortedDataCell"
                                            AllowReordering="false" Align="Right" IsSearchable="false" FixedWidth="true" />
                                        <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>"
                                            DataField="BounceRate" FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                            HeadingCellCssClass="LastHeadingCell" AllowReordering="false" DataType="System.Double"
                                            Align="Right" IsSearchable="false" DataCellClientTemplateId="BounceRateCT" />
                                        <ComponentArt:GridColumn Width="55" runat="server" HeadingText="<%$ Resources:GUIStrings, Exit %>"
                                            DataField="ExitPercentage" FixedWidth="true" DataCellCssClass="LastDataCell"
                                            SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                                            DataCellClientTemplateId="ExitPercentageCT" AllowReordering="false" Align="Right"
                                            DataType="System.Double" IsSearchable="false" />
                                        <ComponentArt:GridColumn DataField="Original_Id" Visible="false" IsSearchable="false" />
                                        <ComponentArt:GridColumn DataField="Menu" Visible="false" IsSearchable="false" />
                                        <ComponentArt:GridColumn DataField="Published_Date" Visible="false" IsSearchable="false" />
                                        <ComponentArt:GridColumn DataField="Published_By" Visible="false" IsSearchable="false" />
                                        <ComponentArt:GridColumn DataField="Author_Email" Visible="false" IsSearchable="false" />
                                    </Columns>
                                </ComponentArt:GridLevel>
                            </Levels>
                            <ClientEvents>
                                <ContextMenu EventHandler="grdLandingTopPages_onContextMenu" />
                                <PageIndexChange EventHandler="grdLandingTopPages_onPageIndexChange" />
                            </ClientEvents>
                            <ClientTemplates>
                                <ComponentArt:ClientTemplate ID="BounceRateCT">
                                    ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="ExitPercentageCT">
                                    ## GetContentDecimal(DataItem.GetMember('ExitPercentage').Value) ##
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="grdLandingTopPagesSlider">
                                    <div class="SliderPopup">
                                        <h5>
                                            ## DataItem.GetMember(0).Value ##</h5>
                                        <p>
                                            ## DataItem.GetMember(1).Value ##</p>
                                        <p>
                                            ## DataItem.GetMember(2).Value ##</p>
                                        <p>
                                            ## DataItem.GetMember(3).Value ##</p>
                                        <p>
                                            ## DataItem.GetMember(4).Value ##</p>
                                        <p class="paging">
                                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdLandingTopPages.PageCount)
                                                ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdLandingTopPages.RecordCount)
                                                    ##</span>
                                        </p>
                                    </div>
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="grdLandingTopPagesPagination">
                                    ## GetGridPaginationInfo(grdLandingTopPages) ##
                                </ComponentArt:ClientTemplate>
                            </ClientTemplates>
                        </ComponentArt:Grid>
                        <ComponentArt:Menu ID="cmTopPagesAC" runat="server" SkinID="ContextMenu">
                            <Items>
                                <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>"
                                    ID="ViewStatistics">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem LookId="BreakItem">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewInboundOutboundAnalysis %>"
                                    ID="ViewAnalysis">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem LookId="BreakItem">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>"
                                    ID="ViewNewWindow">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem LookId="BreakItem">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
                                    ID="ViewSiteEditor">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem LookId="BreakItem">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>"
                                    ID="AssignIndexTerms">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem LookId="BreakItem">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem ID="MenuEmailAuthor" runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>">
                                </ComponentArt:MenuItem>
                                <ComponentArt:MenuItem LookId="BreakItem">
                                </ComponentArt:MenuItem>
                            </Items>
                            <ClientEvents>
                                <ItemSelect EventHandler="cmTopPagesAC_onItemSelect" />
                            </ClientEvents>
                        </ComponentArt:Menu>
                    </div>
                </div>
            </div>
        </Content>
        <Footer>
            <div class="bottomShadowBox">
                <div class="gridFooterContainer">
                </div>
            </div>
        </Footer>
        <CollapsedFooter>
            <div class="bottomShadowBox bottomShadowBoxGap">
                <div class="gridFooterContainer">
                </div>
            </div>
        </CollapsedFooter>
    </ComponentArt:Snap>
</div>

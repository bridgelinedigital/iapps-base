﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DirectReferrerStatistics.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.UserControls.Dashboard.DirectReferrerStatistics" %>

<p class="links">
    <asp:HyperLink ID="HyperLink5" NavigateUrl="~/Statistics/Visitors/Direct.aspx" runat="server">
        <asp:Literal ID="ltDirectTraffic" runat="server"></asp:Literal>
    </asp:HyperLink>
    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Direct %>" />
</p>
<p class="links">
    <asp:HyperLink ID="HyperLink6" NavigateUrl="~/Statistics/Referrers/SearchReferrers.aspx" runat="server">
        <asp:Literal ID="ltSearch" runat="server"></asp:Literal>
    </asp:HyperLink>
    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Search %>" />
</p>
<p class="links">
    <asp:HyperLink ID="HyperLink7" NavigateUrl="~/Statistics/Referrers/ExternalReferrers.aspx" runat="server">
        <asp:Literal ID="ltReferringSites" runat="server"></asp:Literal>
    </asp:HyperLink>
    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, ReferringSites %>" />
</p>
<p class="links">
    <asp:HyperLink ID="HyperLink8" NavigateUrl="~/Campaigns/CampaignListing.aspx" runat="server">
        <asp:Literal ID="ltCampaigns" runat="server"></asp:Literal>
    </asp:HyperLink>
    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Campaigns %>" />
</p>

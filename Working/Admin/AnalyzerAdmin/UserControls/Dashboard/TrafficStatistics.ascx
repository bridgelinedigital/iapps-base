﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrafficStatistics.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.UserControls.Dashboard.TrafficStatistics" %>
<div class="traffic-stats">
    <p class="links">
        <asp:HyperLink ID="hplVisits" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server" />
        <asp:Literal ID="ltVisits" Text="<%$ Resources:GUIStrings, Visits %>" runat="server" />
    </p>
    <p class="links">
        <asp:HyperLink ID="hplPageviews" NavigateUrl="~/Statistics/Traffic/TopPagesTimeSeries.aspx" runat="server" />
        <asp:Literal ID="ltPageviews" Text="<%$ Resources:GUIStrings, Pageviews1 %>" runat="server" />
    </p>
    <p class="links">
        <asp:HyperLink ID="hplPagePerVisits" NavigateUrl="~/Statistics/Traffic/PageviewsPerVisitTimeSeries.aspx" runat="server" />
        <asp:Literal ID="ltPagePerVisits" Text="<%$ Resources:GUIStrings, PageVisit %>" runat="server" />
    </p>
    <p class="links">
        <asp:HyperLink ID="hplBounceRate" NavigateUrl="~/Statistics/Traffic/SinglePageVisits.aspx" runat="server" />
        <asp:Literal ID="ltBounceRate" Text="<%$ Resources:GUIStrings, BounceRate %>" runat="server" />
    </p>
    <p class="links">
        <asp:HyperLink ID="hplAvgTimeOnSite" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server" />
        <asp:Literal ID="ltAvgTimeOnSite" Text="<%$ Resources:GUIStrings, AverageTimeonSite %>" runat="server" />
    </p>
    <p style="margin-top: 10px;">
        <asp:HyperLink ID="hplFullReport" runat="server" NavigateUrl="~/Statistics/Traffic/Overview.aspx" />
    </p>
</div>

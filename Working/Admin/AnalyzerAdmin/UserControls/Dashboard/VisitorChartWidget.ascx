﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VisitorChartWidget.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.UserControls.Dashboard.VisitorChartWidget" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<p class="links clear-fix">
    <asp:HyperLink ID="hplNewVisitors" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server" Text="0" />
    <asp:Localize ID="Localize5" runat="server" Text="New Visitors" />
</p>
<p class="links clear-fix">
    <asp:HyperLink ID="hplRepeatVisitors" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server" Text="0" />
    <asp:Localize ID="Localize6" runat="server" Text="Repeat Visitors" />
</p>
<p class="links clear-fix">
    <asp:HyperLink ID="hplReturnVisitors" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server" Text="0" />
    <asp:Localize ID="Localize7" runat="server" Text="Return Visitors" />
</p>
<p class="links clear-fix">
    <asp:HyperLink ID="hplTotal" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" runat="server" Text="0" />
    <asp:Localize ID="Localize8" runat="server" Text="Total" />
</p>
<p style="margin-top: 10px;">
    <asp:HyperLink ID="hplFullReport" runat="server" NavigateUrl="~/Statistics/Visitors/Visitors.aspx" />
</p>

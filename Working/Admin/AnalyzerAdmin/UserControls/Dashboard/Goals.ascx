﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Goals.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.UserControls.Dashboard.Goals" %>
<asp:Repeater runat="server" ID="rptTopGoals">
    <ItemTemplate>
        <p class="links clear-fix">
            <asp:Literal ID="ltGoalData" runat="server" />
        </p>
    </ItemTemplate>
</asp:Repeater>
<p style="margin-top: 10px;">
    <asp:HyperLink ID="hplFullReport" runat="server" NavigateUrl="~/Goals/GoalListing.aspx" />
</p>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommerceStatistics.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.UserControls.Dashboard.CommerceStatistics" %>
<asp:PlaceHolder ID="pldCommerce" runat="server">
    <p class="links">
        <asp:LinkButton ID="lbConversionRate" runat="server" OnClick="lbConversionRate_Click"> </asp:LinkButton>
        <asp:Localize ID="ltConversionRate" runat="server" Text="<%$ Resources:GUIStrings, ConversionRate1 %>" />
    </p>
    <p class="links">
        <asp:LinkButton ID="lbRevenue" runat="server" OnClick="lbRevenue_Click"></asp:LinkButton>
        <asp:Localize ID="ltRevenue" runat="server" Text="<%$ Resources:GUIStrings, Revenue %>" />
    </p>
    <p class="links">
        <asp:LinkButton ID="lbTransactions" runat="server" OnClick="lbTransactions_Click"></asp:LinkButton>
        <asp:Localize ID="ltTransactions" runat="server" Text="<%$ Resources:GUIStrings, Transactions %>" />
    </p>
</asp:PlaceHolder>

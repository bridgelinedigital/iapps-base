﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopOpportunities.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.UserControls.Dashboard.TopOpportunities" %>
<script type="text/javascript">
    function OpenTakeActionPopup(id, goalId, landingPageId) {
        var url = jAnalyticAdminSiteUrl + "/Popups/TakeAction.aspx?RecommendationId=" + id + "&GoalId=" + goalId + "&LandingPageId=" + landingPageId;
        takeActionWindow = dhtmlmodal.open('TakeAction', 'iframe', url, '<asp:localize ID="Localize1" runat="server" text="<%$ Resources:JSMessages, TakeAction %>"/>', 'width=415px,height=350px,center=1,resize=0,scrolling=1');
        takeActionWindow.onclose = function () {
            var a = document.getElementById('TakeAction');
            var ifr = a.getElementsByTagName("iframe");
            window.frames[ifr[0].name].location.replace("/blank.html");
            return true;
        }
    }
    function CloseTakeActionAndRedirect(url, productId, site, productString, userId, userName) {
        var token = GetTokenWithUserName(productId, site, productString, userId, userName);
        url = url.replace('[Token]', token);
        takeActionWindow.hide();
        window.location = url;
        return false;
    }
</script>
<asp:Repeater runat="server" ID="rptTopOpportunities">
    <ItemTemplate>
        <p class="links clear-fix">
            <asp:Literal ID="ltTopOpportunities" runat="server" />
        </p>
    </ItemTemplate>
</asp:Repeater>
<p style="margin-top: 10px;">
    <asp:HyperLink ID="hplFullReport" runat="server" NavigateUrl="~/Goals/GoalListing.aspx" />
</p>

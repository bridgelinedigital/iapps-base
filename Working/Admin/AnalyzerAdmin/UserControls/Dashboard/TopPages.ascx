﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopPages.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.UserControls.Dashboard.TopPages" %>
<asp:Repeater runat="server" ID="rptTopPages">
    <ItemTemplate>
        <p class="links clear-fix">
            <asp:Literal ID="ltPageVisits" runat="server" />
        </p>
    </ItemTemplate>
</asp:Repeater>
<p style="margin-top: 10px;">
    <asp:HyperLink ID="hplFullReport" runat="server" NavigateUrl="~/Statistics/Traffic/TopPagesTimeSeries.aspx" />
</p>

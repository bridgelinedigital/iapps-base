<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.ErrorMessageBox"
    CodeBehind="ErrorMessageBox.ascx.cs" %>
<asp:Panel ID="pnlErrorMessage" runat="server" CssClass="errorMessage">
    <div class="msg-close-handle" onclick="$(this).parents('.messageContainer').slideUp();"></div>
    <asp:Literal runat="server" ID="ltErrorMessage" />
</asp:Panel>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportingRange.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.ReportingRange" %>
<script type="text/javascript">
    var iframeObject = null;
    function CalendarFrom_OnChange(sender, eventArgs) {
        var selectedDate = CalendarFrom.getSelectedDate();
        var date = new Date();
        if (selectedDate >= date) {
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, DateShouldNotExceedTodaysDate %>'/>");
            selectedDate = date;
        }
        else {
            document.getElementById("<%=txtFromDate.ClientID%>").value = CalendarFrom.formatDate(selectedDate, "MM/dd/yyyy");
        }
    }
    function CalendarTo_OnChange(sender, eventArgs) {
        var selectedDate = CalendarTo.getSelectedDate();
        var date = new Date();

        if (selectedDate >= date) {
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, DateShouldNotExceedTodaysDate %>'/>");
            selectedDate = date;
        }
        document.getElementById("<%=txtToDate.ClientID%>").value = CalendarTo.formatDate(selectedDate, "MM/dd/yyyy");
    }
    function popUpFromCalendar(textBoxId) {
        if (document.getElementById("<%=txtFromDate.ClientID%>").disabled == false) {
            var thisDate = new Date();
            var textBoxObject = document.getElementById(textBoxId);
            if (Trim(textBoxObject.value) == "<asp:localize runat='server' text='<%$ Resources:JSMessages, FromDate %>'/>" || Trim(textBoxObject.value) == "") {
                textBoxObject.value = "";
            }
            if (textBoxObject.value != "<asp:localize runat='server' text='<%$ Resources:JSMessages, FromDate %>'/>" && Trim(textBoxObject.value) != "") {
                if (validate_dateSummary(textBoxObject) == true)// available in validation.js 
                {
                    thisDate = new Date(textBoxObject.value);
                    CalendarFrom.setSelectedDate(thisDate);
                }
            }
            else {

            }
            if (!(CalendarFrom.get_popUpShowing()));
            CalendarFrom.show();
        }
    }
    function popUpToCalendar(textBoxId) {
        if (document.getElementById("<%=txtToDate.ClientID%>").disabled == false) {
            var thisDate = new Date();
            var textBoxObject = document.getElementById(textBoxId);
            if (Trim(textBoxObject.value) == "<asp:localize runat='server' text='<%$ Resources:JSMessages, ToDate %>'/>" || Trim(textBoxObject.value) == "") {
                textBoxObject.value = "";
            }
            if (textBoxObject.value != "<asp:localize runat='server' text='<%$ Resources:JSMessages, ToDate %>'/>" && Trim(textBoxObject.value) != "") {
                if (validate_dateSummary(textBoxObject) == true) {
                    thisDate = new Date(textBoxObject.value);
                    CalendarTo.setSelectedDate(thisDate);
                }
            }
            else { }
            if (!(CalendarTo.get_popUpShowing()));
            CalendarTo.show();
        }
    }
    function ShowDateRange() {
        var destinationObject = document.getElementById("dateRangeSelector");
        var sourceObject = document.getElementById("rangeSelector");
        if (destinationObject.style.display == "none" || destinationObject.style.display == "") {
            destinationObject.style.display = "block";
            setPosition(destinationObject, sourceObject);
            if (document.getElementById('<%=rbSelectRange.ClientID%>').checked == true) {
                disableFields('custom');
            }
            else {
                disableFields('predefined');
            }
            UpdateChangeRange();
            iframeObject = cover(destinationObject);
        }
        else {
            destinationObject.style.display = "none";
            if (iframeObject != null) {
                iframeObject.style.display = "none";
            }
            UpdateChangeRange();
        }
    }
    function setPosition(destObj, srcObj) {
        var srcObjOffset = $(srcObj).offset();
        var destObjOffset = $(destObj).offset();
        $(destObj).css('top', (srcObjOffset.top + $(srcObj).outerHeight()) + 'px');
        $(destObj).css('left', (srcObjOffset.left - $(destObj).outerWidth() + $(srcObj).outerWidth()) + 'px');
    }
    function UpdateChangeRange() {
        var sourceObject = document.getElementById("rangeSelector");
        var buttonClass = sourceObject.className
        if (buttonClass.indexOf("Hover") > -1) {
            newButtonClass = "rangeSelector";
            oldButtonClass = "rangeSelector" + "Hover";
            sourceObject.className = buttonClass.replace(oldButtonClass, newButtonClass);
        }
        else {
            oldButtonClass = "rangeSelector";
            newButtonClass = "rangeSelector" + "Hover";
            sourceObject.className = buttonClass.replace(oldButtonClass, newButtonClass);
        }
    }
    function ValidateRange() {
        var retValue = DateChecking();
        if (retValue) {
            ShowDateRange();
        }
        else {
            return retValue;
        }
    }

    function DateChecking() {
        var retValue = true;
        var errorMessage = '';

        if (document.getElementById('<%=rbSelectRange.ClientID%>').checked == true) {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value != '') {
                if (validate_dateSummary(document.getElementById('<%=txtFromDate.ClientID %>')) == false) {
                    errorMessage = errorMessage + "<%= JSMessages.PleaseFormatTheSelectDateRangeStartDateAsMmDdYyyy %>";
                    retValue = false;
                }
            }
            else {
                errorMessage = errorMessage + "<%= JSMessages.ForSelectDateRangePleaseEnterTheStartDate %>";
                retValue = false;
            }

            if (document.getElementById('<%=txtToDate.ClientID %>').value != '') {
                if (validate_dateSummary(document.getElementById('<%=txtToDate.ClientID %>')) == false) {
                    errorMessage = errorMessage + "<%= JSMessages.PleaseFormatTheSelectDateRangeEndDateAsMmDdYyyy %>";
                    retValue = false;
                }
            }
            else {
                errorMessage = errorMessage + "<%= JSMessages.ForSelectDateRangePleaseEnterTheEndDate %>";
                retValue = false;
            }
            if (document.getElementById('<%=txtFromDate.ClientID %>').value != "" && document.getElementById('<%=txtToDate.ClientID %>').value != "") {
                if (validate_dateSummary(document.getElementById('<%=txtToDate.ClientID %>')) == true && validate_dateSummary(document.getElementById('<%=txtFromDate.ClientID %>')) == true) {
                    fromdate = new Date(document.getElementById('<%=txtFromDate.ClientID %>').value);
                    todate = new Date(document.getElementById('<%=txtToDate.ClientID %>').value);
                    if (fromdate > todate) {
                        errorMessage = errorMessage + "<%= JSMessages.ForSelectDateRangetheenddatemustbelaterthanthestartdaten %>";
                        retValue = false;
                    }
                }
            }
            if (document.getElementById('<%=txtFromDate.ClientID %>').value != "" && document.getElementById('<%=txtToDate.ClientID %>').value != "") {
                if (validate_dateSummary(document.getElementById('<%=txtToDate.ClientID %>')) == true && validate_dateSummary(document.getElementById('<%=txtFromDate.ClientID %>')) == true) {
                    fromdate = new Date(document.getElementById('<%=txtFromDate.ClientID %>').value);
                    todate = new Date(document.getElementById('<%=txtToDate.ClientID %>').value);
                    var currentDate = new Date();
                    if (fromdate > currentDate) {
                        errorMessage = errorMessage + "<%= JSMessages.StartDateshouldnotexceedtodaysDaten %>";
                        retValue = false;
                    }
                    if (todate > currentDate) {
                        errorMessage = errorMessage + "<%= JSMessages.EndDateshouldnotexceedtodaysDaten %>";
                        retValue = false;
                    }
                }
            }

        }
        if (!retValue)
            alert(errorMessage);
        return retValue;
    }
    function disableFields(fieldText) {
        switch (fieldText) {
            case 'predefined':
                document.getElementById("<%=txtFromDate.ClientID%>").disabled = true;
                document.getElementById("<%=txtToDate.ClientID%>").disabled = true;
                document.getElementById("<%=txtFromDate.ClientID%>").value = "";
                document.getElementById("<%=txtToDate.ClientID%>").value = "";
                document.getElementById("<%=ddlPredefindedRange.ClientID%>").disabled = false;
                break;
            case 'custom':
                document.getElementById("<%=txtFromDate.ClientID%>").disabled = false;
                document.getElementById("<%=txtToDate.ClientID%>").disabled = false;
                var date = new Date();
                if (document.getElementById("<%=txtFromDate.ClientID%>").value == "<asp:localize runat='server' text='<%$ Resources:JSMessages, FromDate %>'/>") {
                    document.getElementById("<%=txtFromDate.ClientID%>").value = CalendarFrom.formatDate(date, "MM/dd/yyyy");
                    document.getElementById("<%=txtToDate.ClientID%>").value = CalendarTo.formatDate(date, "MM/dd/yyyy");
                }
                document.getElementById("<%=ddlPredefindedRange.ClientID%>").disabled = true;
                break;
        }
    }
</script>
<div id="dateRangeContainer">
    <div class="date-range-popup" id="dateRangeSelector">
        <div class="form-row">
            <div class="form-value">
                <asp:RadioButton ID="rbPredefindedRange" runat="server" Text="<%$ Resources:GUIStrings, PredefinedRanges %>"
                    Checked="true" GroupName="DateRange" onclick="disableFields('predefined');" />
            </div>
        </div>
        <div class="form-row">
            <div class="form-value">
                <asp:DropDownList ID="ddlPredefindedRange" runat="server" Width="210">
                    <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, CurrentWeek %>" Value="CurrentWeek"></asp:ListItem>
                    <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, MonthToDate %>" Value="MonthToDate"></asp:ListItem>
                    <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, QuarterToDate %>" Value="QuarterToDate"></asp:ListItem>
                    <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, YearToDate %>" Value="YearToDate"></asp:ListItem>
                    <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Last30Days %>" Value="Last30days"></asp:ListItem>
                    <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Last60Days %>" Value="Last60days"></asp:ListItem>
                    <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Last180Days %>" Value="Last180days"></asp:ListItem>
                    <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Last365Days %>" Value="Last365days"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <div class="form-value">
                <asp:RadioButton ID="rbSelectRange" runat="server" Text="<%$ Resources:GUIStrings, SelectDateRange %>" GroupName="DateRange"
                    onclick="disableFields('custom');" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.FromDate %></label>
            <div class="form-value calendar-value">
                <asp:TextBox ID="txtFromDate" runat="server" Text="" CssClass="textBoxes" Width="140" Enabled="false" />
                <span>
                    <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, FromDate %>"
                        ToolTip="<%$ Resources:GUIStrings, SelectFromDate %>" ID="imgFromDate" runat="server" />
                </span>
                <ComponentArt:Calendar runat="server" ID="CalendarFrom" SkinID="Default">
                    <ClientEvents>
                        <SelectionChanged EventHandler="CalendarFrom_OnChange" />
                    </ClientEvents>
                </ComponentArt:Calendar>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.ToDate %></label>
            <div class="form-value calendar-value">
                <asp:TextBox ID="txtToDate" runat="server" Text="" CssClass="textBoxes" Width="140" Enabled="false" />
                <span>
                    <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, ToDate %>"
                        ToolTip="<%$ Resources:GUIStrings, SelectToDate %>" ID="imgToDate" runat="server" />
                </span>
                <ComponentArt:Calendar runat="server" ID="CalendarTo" SkinID="Default">
                    <ClientEvents>
                        <SelectionChanged EventHandler="CalendarTo_OnChange" />
                    </ClientEvents>
                </ComponentArt:Calendar>
            </div>
        </div>
        <div class="form-button-row right-align">
            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Submit %>"
                OnClick="btnSubmit_Click" ToolTip="<%$ Resources:GUIStrings, Submit %>" OnClientClick="return ValidateRange();" />
        </div>
    </div>
</div>
<div class="reporting-range-container">
    <div class="reporting-range">
        <label><asp:localize runat="server" text="<%$ Resources:GUIStrings, ReportingPeriod %>"/></label>
        <asp:Label ID="startPeriod" runat="server" />&nbsp;-&nbsp;
        <asp:Label ID="endPeriod" runat="server" />
    </div>
    <label id="rangeSelector" class="reporting-range-selector" onclick="ShowDateRange();"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Change %>"/></label>
    <div class="clearFix"></div>
</div>

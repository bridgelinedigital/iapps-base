<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Statistics_Content_TopPages"
    CodeBehind="TopPagesReport.ascx.cs" %>
<%--<%@ outputcache duration="200" varybycontrol="ReportStartDate,ReportEndDate" %>--%>
<script type="text/javascript">
    var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
    var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
    var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
    var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';
    var _Exit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Exit %>"/>';
    var _CMDetails = '<asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, CMDetails %>"/>';
    var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
    var _SiteExit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, SiteExit %>"/>';
    var _Pageviews1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Pageviews1 %>"/>';
    var _TimeonPage = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, TimeonPage %>"/>';
    var _UniquePageviews = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, UniquePageviews %>"/>';
    var item = "";
    var pageId = "";
    var newPage = "0";
    var orderByCol = "Page_Visits";
    var orderBy = "DESC";
    function grdTopPages_onPageIndexChange(sender, eventArgs) {
        if (isOverview != null && isOverview != true) {
            newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            ChartCallback.Callback(callbackArgs);
        }
    }
    function grdTopPages_onSortChange(sender, eventArgs) {
        grdTopPages.unSelectAll();
        if (isOverview != null && isOverview != true) {
            orderByCol = eventArgs.get_column().get_dataField();
            if (eventArgs.get_descending()) {
                orderBy = "DESC";
            }
            else {
                orderBy = "ASC";
            }
            newPage = "0";
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            if (grdTopPages.RecordCount != 0) {
                ChartCallback.Callback(callbackArgs);
            }
        }
    }
    function grdTopPages_onContextMenu(sender, eventArgs) {
        item = eventArgs.get_item();
        grdTopPages.select(item);
        var evt = eventArgs.get_event();
        cmTopPages.showContextMenuAtEvent(evt, eventArgs.get_item());
    }
    function cmTopPages_onItemSelect(sender, eventArgs) {
        var analyticsUrl = jAnalyticAdminSiteUrl;
        var selectedMenu = eventArgs.get_item().get_id();
        var currentSiteId = $('#ddlChildSites').val();
        
        if (currentSiteId == undefined)
            currentSiteId = siteId;
        
        var selectedSitePublicSiteUrl = GetSiteUrl(currentSiteId);
        switch (selectedMenu) {
            case "ViewStatistics":
                var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', 'width=750px,height=635px,center=1,resize=0,scrolling=1');
                viewPopupWindow.onclose = function () {
                    var a = document.getElementById('Content');
                    var ifr = a.getElementsByTagName("iframe");
                    window.frames[ifr[0].name].location.replace("about:blank");
                    return true;
                }
                break;
            case "ViewAnalysis":
                window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                break;
            case "ViewNewWindow":
                window.open(selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text);
                break;
            case "ViewSiteEditor":
                var currentUrl = document.URL;
                if (currentUrl == null || currentUrl == '') {
                    currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                }
                if (currentUrl.indexOf('?') > 0) {
                    currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                }
                currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;


                var url = selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenbyProductId(cmsProductId, currentSiteId, siteName);

                window.location.href = url;
                break;
            case 'ViewOverlay':
                var currentUrl = document.URL;
                if (currentUrl == null || currentUrl == '') {
                    currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                }
                if (currentUrl.indexOf('?') > 0) {
                    currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                }
                currentUrl = '?PageState=Overlay&LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                var sUrl = selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenbyProductId(cmsProductId, currentSiteId, siteName);

                alert(sUrl);
                //window.location.href = selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenbyProductId(cmsProductId, currentSiteId, siteName);
                break;
            case "AssignIndexTerms":
                var pageId = item.getMember("Original_Id").get_text().replace("{", "").replace("}", "");
                OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                break;
            case "MenuEmailAuthor":
                var email = item.GetMember('Author_Email').Text;
                location.href = "mailto:" + email + "";
                break;
        }
    }

    /** Methods to get set the tooltip **/
    function CreateTip(dataItemObject) {
        var authorName = dataItemObject.GetMember('Author').Text;
        authorName = authorName.replace(new RegExp("'", "g"), "\'");

        var publisherName = dataItemObject.GetMember('Published_By').Text;
        publisherName = publisherName.replace(new RegExp("'", "g"), "\'");

        var dateTimePublished = dataItemObject.GetMember('Published_Date').Text;
        dateTimePublished = dateTimePublished.replace(new RegExp("'", "g"), "\'");

        var navigationHierarchy = dataItemObject.GetMember('Menu').Text;
        navigationHierarchy = navigationHierarchy.replace(new RegExp("'", "g"), "\'");

        var strHTML = "";
        var imgTag = "";
        strHTML += "<div class=tooltip>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, Author %>'/></h5>";
        strHTML += "<p>" + authorName + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, PublishedBy %>'/></h5>";
        strHTML += "<p>" + publisherName + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, DateTimePublished %>'/></h5>";
        strHTML += "<p>" + dateTimePublished + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, NavigationHierarchy %>'/></h5>";
        strHTML += "<p>" + navigationHierarchy + "</p>";
        strHTML += "</div>";
        strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'','../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);\" onmouseout='hideddrivetip();' src='../../App_Themes/General/images/icon-tooltip.gif' />";
        return strHTML;
    }
    /** Methods to get set the tooltip **/
    function getSortImageHtml(column, cssClass) {
        // is the grid sorted by this column?
        if (grdTopPages.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
            if (grdTopPages.Levels[0].IndicatedSortDirection == 1) {
                return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
            }
            else {
                return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
            }
        }
        // it isn't sorted by this column, no image
        return '';
    }
</script>
<div class="chartContainer" runat="server" id="chartContainer">
    <ComponentArt:CallBack ID="ChartCallback" runat="server" Height="100" Width="350"
        OnCallback="ChartCallback_onCallback">
        <Content>
            <ComponentArtChart:Chart ID="targetChart" RenderingPrecision="0.1" Width="1200px"
                Height="328px" runat="server" SelectedPaletteName="Earth">
                <View Kind="TwoDimensional">
                </View>
                <GradientStyles>
                    <ComponentArtChart:GradientStyle StartColor="#ffc932" EndColor="#ffe9a8" GradientKind="Vertical" />
                </GradientStyles>
                <Palettes>
                    <ComponentArtChart:Palette AxisLineColor="#b0b0b0" BackgroundColor="#ffffff" BackgroundEndingColor="#ffffff"
                        CoodinateLabelFontColor="#848484" CoordinateLineColor="#dedede" CoordinatePlaneColor="#ffffff"
                        CoordinatePlaneSecondaryColor="#ffffff" DataLabelFontColor="#848484" FrameColor="#ffffff"
                        FrameFontColor="#848484" FrameSecondaryColor="#ffffff" LegendBackgroundColor="#ffffff"
                        LegendBorderColor="#dedede" LegendFontColor="#848484" Name="Earth" TitleFontColor="#848484"
                        TwoDObjectBorderColor="#dedede" PrimaryColors="ffc932" SecondaryColors="ffe9a8" />
                </Palettes>
            </ComponentArtChart:Chart>
        </Content>
        <LoadingPanelClientTemplate>
        </LoadingPanelClientTemplate>
    </ComponentArt:CallBack>
</div>
<ComponentArt:Grid SkinID="Default" ID="grdTopPages" ShowFooter="false" runat="server"
    RunningMode="Client" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
    AutoPostBackOnSelect="false" PageSize="5" SliderPopupClientTemplateId="grdTopPagesSlider"
    PagerInfoClientTemplateId="grdTopPagesPagination" Width="100%">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Original_Id" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
            <Columns>
                <ComponentArt:GridColumn Width="295" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                    HeadingCellCssClass="FirstHeadingCell" DataField="Title" DataCellCssClass="FirstDataCell"
                    SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                    FixedWidth="true" />
                <ComponentArt:GridColumn Width="115" runat="server" HeadingText="<%$ Resources:GUIStrings, Pageviews1 %>"
                    DataField="Page_Visits" IsSearchable="true" HeadingCellClientTemplateId="pageviewsHeaderTP"
                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                    Align="Right" DataType="System.Int64" />
                <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, UniquePageviews %>"
                    DataField="Unique_Page_Visits" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                    FixedWidth="true" HeadingCellClientTemplateId="uniqueHeaderTP" Align="Right"
                    DataType="System.Int64" />
                <ComponentArt:GridColumn Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, TimeonPage %>"
                    DataField="Page_Dwell_Time" DefaultSortDirection="Descending" SortedDataCellCssClass="SortedDataCell"
                    FixedWidth="true" AllowReordering="false" HeadingCellClientTemplateId="timeHeaderTP"
                    Align="Right" />
                <ComponentArt:GridColumn Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>"
                    DataField="BounceRate" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                    HeadingCellCssClass="HeadingCell" DataType="System.Double" DataCellClientTemplateId="BounceRateCT"
                    AllowReordering="false" HeadingCellClientTemplateId="bounceHeaderTP" Align="Right" />
                <ComponentArt:GridColumn Width="75" runat="server" HeadingText="<%$ Resources:GUIStrings, Exit %>"
                    DataField="ExitPercentage" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                    HeadingCellCssClass="HeadingCell" DataType="System.Double" DataCellClientTemplateId="ExitPercentageCT"
                    AllowReordering="false" HeadingCellClientTemplateId="exitHeaderTP" Align="Right" />
                <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, CMDetails %>"
                    DataField="Original_Id" FixedWidth="true" AllowSorting="false" DataCellCssClass="LastDataCell"
                    SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                    AllowReordering="false" HeadingCellClientTemplateId="cmHeaderTP" Align="Center"
                    DataCellClientTemplateId="CMHoverTP" />
                <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, Site %>" DataField="SiteTitle" Visible="true" Width="150"/>
                <ComponentArt:GridColumn DataField="Author" Visible="false" />
                <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                <ComponentArt:GridColumn DataField="Published_Date" Visible="false" />
                <ComponentArt:GridColumn DataField="Published_By" Visible="false" />
                <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="BounceRateCT">
            ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ExitPercentageCT">
            ## GetContentDecimal(DataItem.GetMember('ExitPercentage').Value) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="pageviewsHeaderTP">
                <div class="custom-header">
                    <span class="header-text">##_Pageviews1##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(tpPageviewText, 300, '', '../../App_Themes/General/images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                    onmouseout="hideddrivetip();" />
                ## getSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="uniqueHeaderTP">
                <div class="custom-header">
                    <span class="header-text">##_UniquePageviews##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(tpUniquePageviewText, 300, '', '../../App_Themes/General/images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                    onmouseout="hideddrivetip();" />
                ## getSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="timeHeaderTP">
                <div class="custom-header">
                    <span class="header-text">##_TimeonPage##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(tpTimeOnPageText, 300, '', '../../App_Themes/General/images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                    onmouseout="hideddrivetip();" />
                ## getSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="bounceHeaderTP">
            <div class="customHeader rightAligned">
                <div class="custom-header">
                    <span class="header-text">##_BounceRate1##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(tpBounceRateText, 300, '', '../../App_Themes/General/images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                    onmouseout="hideddrivetip();" />
                ## getSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="exitHeaderTP">
                <div class="custom-header">
                    <span class="header-text">##_Exit##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(tpExitText, 300, '', '../../App_Themes/General/images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                    onmouseout="hideddrivetip();" />
                ## getSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="cmHeaderTP">
                <div class="custom-header">
                    <span class="header-text">##_CMDetails##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(tpCMDetailsText, 300, '', '../../App_Themes/General/images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                    onmouseout="hideddrivetip();" />
                ## getSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CMHoverTP">
            ## CreateTip(DataItem) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdTopPagesSlider">
            <div class="SliderPopup">
                <h5>
                    ## DataItem.GetMember(0).Value ##</h5>
                <p>
                    ## DataItem.GetMember(1).Value ##</p>
                <p>
                    ## DataItem.GetMember(2).Value ##</p>
                <p>
                    ## DataItem.GetMember(3).Value ##</p>
                <p>
                    ## DataItem.GetMember(4).Value ##</p>
                <p class="paging">
                    <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdTopPages.PageCount)
                        ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdTopPages.RecordCount)
                            ##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdTopPagesPagination">
            ## GetGridPaginationInfo(grdTopPages) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
    <ClientEvents>
        <ContextMenu EventHandler="grdTopPages_onContextMenu" />
        <PageIndexChange EventHandler="grdTopPages_onPageIndexChange" />
        <SortChange EventHandler="grdTopPages_onSortChange" />
    </ClientEvents>
</ComponentArt:Grid>
<ComponentArt:Menu ID="cmTopPages" runat="server" SkinID="ContextMenu">
    <Items>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>"
            ID="ViewStatistics">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewInboundOutboundAnalysis %>"
            ID="ViewAnalysis">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>"
            ID="ViewNewWindow">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
            ID="ViewSiteEditor">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinOverlayMode %>"
            ID="ViewOverlay">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>"
            ID="AssignIndexTerms">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem ID="MenuEmailAuthor" runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>">
        </ComponentArt:MenuItem>
    </Items>
    <ClientEvents>
        <ItemSelect EventHandler="cmTopPages_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>


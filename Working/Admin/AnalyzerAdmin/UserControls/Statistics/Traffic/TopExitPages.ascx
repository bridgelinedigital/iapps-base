<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Statistics_Content_TopExitPages"
    CodeBehind="TopExitPages.ascx.cs" %>
<%--<%@ outputcache duration="200" varybycontrol="ReportStartDate,ReportEndDate" %>--%>
<script type="text/javascript">
    var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
    var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
    var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
    var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';
    var _Exit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Exit %>"/>';
    var _Exits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Exits %>"/>';
    var _CMDetails = '<asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, CMDetails %>"/>';
    var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
    var _SiteExit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, SiteExit %>"/>';
    var _Pageviews1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Pageviews1 %>"/>';
    var item = "";
    var pageId = "";
    var pageName = "";
    var newPage = "0";
    var orderByCol = "Page_Visits";
    var orderBy = "DESC";
    var typeOfGraph = "2";
    //var isOverview = false;
    /** Methods for the dynamic tooltip **/
    function grdTopExitPages_onContextMenu(sender, eventArgs) {
        item = eventArgs.get_item();
        grdTopExitPages.select(item);
        var evt = eventArgs.get_event();
        cmTopExitPages.showContextMenuAtEvent(evt, eventArgs.get_item());
    }
    function cmTopExitPages_onItemSelect(sender, eventArgs) {
        var selectedMenu = eventArgs.get_item().get_id();
        var currentSiteId = $('#ddlChildSites').val();
        if (currentSiteId == undefined)
            currentSiteId = siteId;
        var selectedSitePublicSiteUrl = GetSiteUrl(currentSiteId);
        switch (selectedMenu) {
            case "ViewStatistics":
                var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', 'width=750px,height=635px,center=1,resize=0,scrolling=1');
                viewPopupWindow.onclose = function () {
                    var a = document.getElementById('Content');
                    var ifr = a.getElementsByTagName("iframe");
                    window.frames[ifr[0].name].location.replace("about:blank");
                    return true;
                }
                break;
            case "ViewAnalysis":
                window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                break;
            case "ViewNewWindow":
                window.open(selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text);
                break;
            case "ViewSiteEditor":
                var currentUrl = document.URL;
                if (currentUrl == null || currentUrl == '') {
                    currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                }
                if (currentUrl.indexOf('?') > 0) {
                    currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                }
                currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                window.location.href = selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenbyProductId(cmsProductId, currentSiteId, siteName);
                break;
            case "ViewOverlay":
                var currentUrl = document.URL;
                if (currentUrl == null || currentUrl == '') {
                    currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                }
                if (currentUrl.indexOf('?') > 0) {
                    currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                }
                currentUrl = '?PageState=Overlay&LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                window.location.href = selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenbyProductId(cmsProductId, currentSiteId, siteName);
                break;
            case "AssignIndexTerms":
                var pageId = item.getMember("Original_Id").get_text().replace("{", "").replace("}", "");
                OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                break;
            case "MenuEmailAuthor":
                var email = item.GetMember('Author_Email').Text;
                location.href = "mailto:" + email + "";
                break;
        }
    }

    /** Methods to get set the tooltip **/
    function CreateTip(dataItemObject) {
        var authorName = dataItemObject.GetMember('Author').Text;
        authorName = authorName.replace(new RegExp("'", "g"), "\'");

        var publisherName = dataItemObject.GetMember('Published_By').Text;
        publisherName = publisherName.replace(new RegExp("'", "g"), "\'");

        var dateTimePublished = dataItemObject.GetMember('Published_Date').Text;
        dateTimePublished = dateTimePublished.replace(new RegExp("'", "g"), "\'");

        var navigationHierarchy = dataItemObject.GetMember('Menu').Text;
        navigationHierarchy = navigationHierarchy.replace(new RegExp("'", "g"), "\'");

        var strHTML = "";
        var imgTag = "";
        strHTML += "<div class=tooltip>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, Author %>'/></h5>";
        strHTML += "<p>" + authorName + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, PublishedBy %>'/></h5>";
        strHTML += "<p>" + publisherName + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, DateTimePublished %>'/></h5>";
        strHTML += "<p>" + dateTimePublished + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, NavigationHierarchy %>'/></h5>";
        strHTML += "<p>" + WrapTooltipText(navigationHierarchy) + "</p>";
        strHTML += "</div>";
        strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'');positiontip(event);\" onmouseout='hideddrivetip();' src='../../App_Themes/General/images/icon-tooltip.gif' />";
        return strHTML;
    }
    /** Methods to get set the tooltip **/
    function getExitSortImageHtml(column, cssClass) {
        // is the grid sorted by this column?
        if (grdTopExitPages.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
            if (grdTopExitPages.Levels[0].IndicatedSortDirection == 1) {
                return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
            }
            else {
                return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
            }
        }
        // it isn't sorted by this column, no image
        return '';
    }
    function grdTopExitPages_onItemSelect(sender, eventArgs) {
        if (grdTopExitPages.get_table().getRowCount() >= 1 && grdTopExitPages.PageCount != 0) {
            var selectedRow = eventArgs.get_item();
            pageId = selectedRow.getMember("Original_Id").get_text();
            pageAuthor = selectedRow.getMember("Author").get_text();
            pageName = selectedRow.getMember("Title").get_text();
            var ep_VisitType = "";
            if (typeof (ExitPages_GetVisitType) == 'function') {
                ep_VisitType = ExitPages_GetVisitType();
            }

            var callbackArgs = new Array(pageId, pageName, typeOfGraph, ep_VisitType);
            if (isOverview != null && isOverview != true) {
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, ExitPages %>'/>", pageName);
                ExitPagesCallback.Callback(callbackArgs);
            }
        }
    }
    function grdTopExitPages_onLoad(sender, eventArgs) {
        grdTopExitPages.unSelectAll();
        if (isOverview != null && isOverview != true) {
            if (grdTopExitPages.get_table().getRowCount() >= 1 && grdTopExitPages.PageCount != 0) {
                var firstItem = grdTopExitPages.get_table().getRow(0);
                pageId = firstItem.getMember("Original_Id").get_text();
                pageAuthor = firstItem.getMember("Author").get_text();
                pageName = firstItem.getMember("Title").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, ExitPages %>'/>", pageName);
                var callbackArgs = new Array(pageId, pageName, typeOfGraph);
            }
            if (grdTopExitPages.get_table().getRowCount() == 0) {
                document.getElementById("legendBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("legendBox").style.visibility = "visible";
            }
        }
    }
</script>
<ComponentArt:Grid SkinID="Default" ID="grdTopExitPages" ShowFooter="false" runat="server"
    RunningMode="Client" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
    AutoPostBackOnSelect="false" PagerInfoClientTemplateId="grdTopExitPagesPagination"
    SliderPopupClientTemplateId="grdTopExitPagesSlider" Width="100%" PageSize="5">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Original_Id" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
            <Columns>
                <ComponentArt:GridColumn Width="480" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                    HeadingCellCssClass="FirstHeadingCell" DataField="Title" DataCellCssClass="FirstDataCell"
                    SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                    FixedWidth="true" />
                <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Exits %>"
                    DataField="Exits" IsSearchable="true" Align="Right" DataType="System.Int64" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="exitsHeaderTE" />
                <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, Pageviews1 %>"
                    DataField="Page_Visits" SortedDataCellCssClass="SortedDataCell" DataType="System.Int64"
                    AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="pageviewsHeaderTE"
                    Align="Right" />
                <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Exit %>"
                    DataField="ExitPercentage" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                    HeadingCellCssClass="LastHeadingCell" Align="Right" DataType="System.Double"
                    DataCellClientTemplateId="ExitPercentageCT" AllowReordering="false" HeadingCellClientTemplateId="percentExitsHeaderTE" />
                <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, SiteExit %>"
                    DataField="Site_Exit" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                    HeadingCellCssClass="LastHeadingCell" Align="Right" DataType="System.Double"
                    DataCellClientTemplateId="SiteExitPercentageCT" AllowReordering="false" HeadingCellClientTemplateId="percentageSiteExitTE" />
                <ComponentArt:GridColumn Width="98" runat="server" HeadingText="<%$ Resources:GUIStrings, CMDetails %>"
                    DataField="Author" FixedWidth="true" AllowSorting="false" DataCellCssClass="LastDataCell"
                    SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                    AllowReordering="false" HeadingCellClientTemplateId="cmHeaderTE" DataCellClientTemplateId="CMHoverTE"
                    Align="Center" />
                <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, Site %>" DataField="SiteTitle" Visible="true" Width="150"/>
                <ComponentArt:GridColumn DataField="Original_Id" Visible="false" />
                <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                <ComponentArt:GridColumn DataField="Published_Date" Visible="false" />
                <ComponentArt:GridColumn DataField="Published_By" Visible="false" />
                <ComponentArt:GridColumn DataField="Author" Visible="false" />
                <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdTopExitPages_onContextMenu" />
        <ItemSelect EventHandler="grdTopExitPages_onItemSelect" />
        <Load EventHandler="grdTopExitPages_onLoad" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="SiteExitPercentageCT">
            ## GetContentDecimal(DataItem.GetMember('Site_Exit').Value) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ExitPercentageCT">
            ## GetContentDecimal(DataItem.GetMember('ExitPercentage').Value) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="exitsHeaderTE">
                <div class="custom-header">
                    <span class="header-text">##_Exits##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(epExitsText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                ## getExitSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="pageviewsHeaderTE">
                <div class="custom-header">
                    <span class="header-text">##_Pageviews1##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(epPageviewsText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                ## getExitSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="percentExitsHeaderTE">
                <div class="custom-header">
                    <span class="header-text">##_Exit##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(epPercentExitsText, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                ## getExitSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="percentageSiteExitTE">
                <div class="custom-header">
                    <span class="header-text">##_SiteExit##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(epPercentSiteExitsText, 300, '');positiontip(event);"
                    onmouseout="hideddrivetip();" />
                ## getExitSortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="cmHeaderTE">
                <div class="custom-header">
                    <span class="header-text">##_CMDetails##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(epCMDetailsText, 300, '');positiontip(event);"
                    onmouseout="hideddrivetip();" />
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CMHoverTE">
            ## CreateTip(DataItem) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdTopExitPagesSlider">
            <div class="SliderPopup">
                <h5>
                    ## DataItem.GetMember(0).Value ##</h5>
                <p>
                    ## DataItem.GetMember(1).Value ##</p>
                <p>
                    ## DataItem.GetMember(2).Value ##</p>
                <p>
                    ## DataItem.GetMember(3).Value ##</p>
                <p>
                    ## DataItem.GetMember(4).Value ##</p>
                <p class="paging">
                    <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdTopExitPages.PageCount)
                        ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdTopExitPages.RecordCount)
                            ##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdTopExitPagesPagination">
            ## GetGridPaginationInfo(grdTopExitPages) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Menu ID="cmTopExitPages" runat="server" SkinID="ContextMenu">
    <Items>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>"
            ID="ViewStatistics">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewInboundOutboundAnalysis %>"
            ID="ViewAnalysis">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>"
            ID="ViewNewWindow">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
            ID="ViewSiteEditor">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinOverlayMode %>"
            ID="ViewOverlay">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>"
            ID="AssignIndexTerms">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem ID="MenuEmailAuthor" runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
    </Items>
    <ClientEvents>
        <ItemSelect EventHandler="cmTopExitPages_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>

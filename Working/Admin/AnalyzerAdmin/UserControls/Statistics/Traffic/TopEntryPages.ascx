<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Statistics_Traffic_TopEntryPages"
    CodeBehind="TopEntryPages.ascx.cs" %>
<%--<%@ outputcache duration="200" varybycontrol="ReportStartDate,ReportEndDate" %>--%>
<script type="text/javascript">
    var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
    var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
    var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
    var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';
    var _Exit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Exit %>"/>';
    var _CMDetails = '<asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, CMDetails %>"/>';
    var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
    var item = "";
    var pageId = "";
    var pageName = "";
    var newPage = "0";
    var orderByCol = "Entry";
    var orderBy = "DESC";
    var typeOfGraph = "2";
    //var isOverview = false;
    /** Methods for the dynamic tooltip **/
    function grdEntryPages_onContextMenu(sender, eventArgs) {
        item = eventArgs.get_item();
        grdEntryPages.select(item);
        var evt = eventArgs.get_event();
        cmEntryPages.showContextMenuAtEvent(evt, eventArgs.get_item());
    }
    function cmEntryPages_onItemSelect(sender, eventArgs) {
        var selectedMenu = eventArgs.get_item().get_id();
        var currentSiteId = $('#ddlChildSites').val();
        if (currentSiteId == undefined)
            currentSiteId = siteId;
        var selectedSitePublicSiteUrl = GetSiteUrl(currentSiteId);
        switch (selectedMenu) {
            case "ViewStatistics":
                var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', 'width=750px,height=635px,center=1,resize=0,scrolling=1');
                viewPopupWindow.onclose = function () {
                    var a = document.getElementById('Content');
                    var ifr = a.getElementsByTagName("iframe");
                    window.frames[ifr[0].name].location.replace("about:blank");
                    return true;
                }
                break;
            case "ViewAnalysis":
                window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                break;
            case "ViewNewWindow":
                window.open(selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text);
                break;
            case "ViewSiteEditor":
                var currentUrl = document.URL;
                if (currentUrl == null || currentUrl == '') {
                    currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                }
                if (currentUrl.indexOf('?') > 0) {
                    currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                }
                currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                window.location.href = selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenbyProductId(cmsProductId, currentSiteId, siteName);
                break;
            case "ViewOverlay":
                var currentUrl = document.URL;
                if (currentUrl == null || currentUrl == '') {
                    currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                }
                if (currentUrl.indexOf('?') > 0) {
                    currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                }
                currentUrl = '?PageState=Overlay&LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                window.location.href = selectedSitePublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenbyProductId(cmsProductId, currentSiteId, siteName);
                break;
            case "AssignIndexTerms":
                var pageId = item.getMember("Original_Id").get_text().replace("{", "").replace("}", "");
                OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                break;
            case "MenuEmailAuthor":
                var email = item.GetMember('Author_Email').Text;
                //location.href = "mailto:"+email+"";
                location.href = "mailto:" + email + "";
                break;
        }
    }

    /** Methods to get set the tooltip **/
    function CreateTip(dataItemObject) {
        var authorName = dataItemObject.GetMember('Author').Text;
        authorName = authorName.replace(new RegExp("'", "g"), "\'");

        var publisherName = dataItemObject.GetMember('Published_By').Text;
        publisherName = publisherName.replace(new RegExp("'", "g"), "\'");

        var dateTimePublished = dataItemObject.GetMember('Published_Date').Text;
        dateTimePublished = dateTimePublished.replace(new RegExp("'", "g"), "\'");

        var navigationHierarchy = dataItemObject.GetMember('Menu').Text;
        navigationHierarchy = navigationHierarchy.replace(new RegExp("'", "g"), "\'");

        var strHTML = "";
        var imgTag = "";

        strHTML += "<div class=tooltip>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, Author %>'/></h5>";
        strHTML += "<p>" + authorName + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, PublishedBy %>'/></h5>";
        strHTML += "<p>" + publisherName + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, DateTimePublished %>'/></h5>";
        strHTML += "<p>" + dateTimePublished + "</p>";
        strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, NavigationHierarchy %>'/></h5>";
        strHTML += "<p>" + WrapTooltipText(navigationHierarchy) + "</p>";
        strHTML += "</div>";
        strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'');positiontip(event);\" onmouseout='hideddrivetip();' src='../../App_Themes/General/images/icon-tooltip.gif' />";
        return strHTML;
    }
    /** Methods to get set the tooltip **/
    function getEntrySortImageHtml(column, cssClass) {
        // is the grid sorted by this column?
        if (grdEntryPages.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
            if (grdEntryPages.Levels[0].IndicatedSortDirection == 1) {
                return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
            }
            else {
                return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
            }
        }
        // it isn't sorted by this column, no image
        return '';

    }
    function grdEntryPages_onItemSelect(sender, eventArgs) {
        if (grdEntryPages.get_table().getRowCount() >= 1 && grdEntryPages.PageCount != 0) {
            var selectedRow = eventArgs.get_item();
            pageId = selectedRow.getMember("Original_Id").get_text();
            pageAuthor = selectedRow.getMember("Author").get_text();
            pageName = selectedRow.getMember("Title").get_text();
            var ep_VisitType = "";
            if (typeof (EntryPages_GetVisitType) == 'function') {
                ep_VisitType = EntryPages_GetVisitType();
            }

            var callbackArgs = new Array(pageId, pageName, typeOfGraph, ep_VisitType);
            if (isOverview != null && isOverview != true) {
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, EntryPages1 %>'/>", pageName);
                EntryPagesCallback.Callback(callbackArgs);
            }
        }
    }
    function grdEntryPages_onLoad(sender, eventArgs) {
        grdEntryPages.unSelectAll();
        if (isOverview != null && isOverview != true) {
            if (grdEntryPages.get_table().getRowCount() >= 1 && grdEntryPages.PageCount != 0) {
                var firstItem = grdEntryPages.get_table().getRow(0);
                pageId = firstItem.getMember("Original_Id").get_text();
                pageAuthor = firstItem.getMember("Author").get_text();
                pageName = firstItem.getMember("Title").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, EntryPages1 %>'/>", pageName);
                var callbackArgs = new Array(pageId, pageName, typeOfGraph);
            }
            if (grdEntryPages.get_table().getRowCount() == 0) {
                document.getElementById("legendBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("legendBox").style.visibility = "visible";
            }
        }
    }
</script>
<ComponentArt:Grid SkinID="Default" ID="grdEntryPages" runat="server" RunningMode="Client"
    EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
    PagerInfoClientTemplateId="grdEntryPagesPagination" SliderPopupClientTemplateId="grdEntryPagesSlider"
    Width="100%" PageSize="10">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Original_Id" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
            <Columns>
                <ComponentArt:GridColumn Width="490" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                    HeadingCellCssClass="FirstHeadingCell" DataField="Title" DataCellCssClass="FirstDataCell"
                    SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                    FixedWidth="true" />
                <ComponentArt:GridColumn Width="115" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                    DataField="Entry" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="EntryCTHeader" />
                <ComponentArt:GridColumn Width="180" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>"
                    DataField="BounceRate" DataCellClientTemplateId="BounceRateCT" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="BounceRateCTHeader" />
                <ComponentArt:GridColumn Width="135" runat="server" HeadingText="<%$ Resources:GUIStrings, Exit %>"
                    DataField="ExitPercentage" FixedWidth="true" DataCellClientTemplateId="ExitPercentageCT"
                    SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                    AllowReordering="false" Align="Right" HeadingCellClientTemplateId="ExitPercentageCTHeader" />
                <ComponentArt:GridColumn Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, CMDetails %>"
                    DataField="Author" FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                    HeadingCellCssClass="LastHeadingCell" AllowReordering="false" DataCellClientTemplateId="CMHoverEP"
                    HeadingCellClientTemplateId="cmHeaderEP" Align="Center" AllowSorting="false" />
                <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, Site %>" DataField="SiteTitle" Visible="true" Width="150"/>
                <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                <ComponentArt:GridColumn DataField="Published_Date" Visible="false" />
                <ComponentArt:GridColumn DataField="Published_By" Visible="false" />
                <ComponentArt:GridColumn DataField="Author" Visible="false" />
                <ComponentArt:GridColumn DataField="Original_Id" Visible="false" />
                <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdEntryPages_onContextMenu" />
        <ItemSelect EventHandler="grdEntryPages_onItemSelect" />
        <Load EventHandler="grdEntryPages_onLoad" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="BounceRateCT">
            ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ExitPercentageCT">
            ## GetContentDecimal(DataItem.GetMember('ExitPercentage').Value) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="EntryCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_Visits##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(epVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                ## getEntrySortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BounceRateCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_BounceRate1##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(epBounceRate, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                ## getEntrySortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ExitPercentageCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_Exit##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                    onmouseover="ddrivetip(epExitPercentage, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                ## getEntrySortImageHtml(DataItem,'sort-image') ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="cmHeaderEP">
                <div class="custom-header">
                    <span class="header-text">##_CMDetails##</span>
                <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(epCMDetailsText, 300, '');positiontip(event);"
                    onmouseout="hideddrivetip();" />
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CMHoverEP">
            ## CreateTip(DataItem) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdEntryPagesSlider">
            <div class="SliderPopup">
                <h5>
                    ## DataItem.GetMember(0).Value ##</h5>
                <p>
                    ## DataItem.GetMember(1).Value ##</p>
                <p>
                    ## DataItem.GetMember(2).Value ##</p>
                <p>
                    ## DataItem.GetMember(3).Value ##</p>
                <p>
                    ## DataItem.GetMember(4).Value ##</p>
                <p class="paging">
                    <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdEntryPages.PageCount)
                        ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdEntryPages.RecordCount)
                            ##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdEntryPagesPagination">
            ## GetGridPaginationInfo(grdEntryPages) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Menu ID="cmEntryPages" runat="server" SkinID="ContextMenu">
    <Items>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>"
            ID="ViewStatistics">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewInboundOutboundAnalysis %>"
            ID="ViewAnalysis">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>"
            ID="ViewNewWindow">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
            ID="ViewSiteEditor">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinOverlayMode %>"
            ID="ViewOverlay">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>"
            ID="AssignIndexTerms">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem ID="MenuEmailAuthor" runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
    </Items>
    <ClientEvents>
        <ItemSelect EventHandler="cmEntryPages_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>

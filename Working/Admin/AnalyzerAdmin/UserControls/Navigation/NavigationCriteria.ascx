﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationCriteria.ascx.cs"
    Inherits="NavigationCriteria" %>
<asp:Panel ID="pnlCriteria" runat="server" CssClass="nav-criteria clear-fix">
    <asp:Literal ID="ltHeading" runat="server" />
    <input type="hidden" runat="server" id="hdnId" class="criteria-id" />
    <asp:Panel ID="pnlOptions" runat="server" CssClass="form-row">
        <label class="form-label">
            Criteria:
        </label>
        <div class="form-value">
            <asp:DropDownList ID="ddlNavCriteria" runat="server" Width="240" CssClass="criteria-options" />
        </div>
    </asp:Panel>
    <div class="criteria-value">
        <asp:PlaceHolder ID="phPageView" runat="server" Visible="false">
            <div class="pageview-value">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Label ID="lblPage" runat="server" Text="Selected Page:" CssClass="criteria-page-label" />
                        <br />
                        <asp:HyperLink ID="hplPage" runat="server" Text="Select Page" CssClass="criteria-page" />
                    </label>
                    <div class="form-value text-value">
                        <asp:Label ID="lblPageName" runat="server" Text="-" CssClass="criteria-page-title" />
                        <input type="hidden" id="hdnPageId" runat="server" class="criteria-page-id" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phPageGroup" runat="server" Visible="false">
            <div class="pagegroup-value" style="display: none;">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, PageGroupName %>" />
                        <br />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtPageGroupName" runat="server" Width="230" CssClass="criteria-pages-name" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        Selected Pages:
                        <br />
                        <asp:HyperLink ID="hplPages" runat="server" Text="Select Pages" CssClass="criteria-pages" />
                    </label>
                    <div class="form-value text-value">
                        <asp:Label ID="lblPageNames" runat="server" Text="-" CssClass="criteria-pages-title" />
                        <input type="hidden" id="hdnPageIds" runat="server" class="criteria-pages-id" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phWatch" runat="server" Visible="false">
            <div class="watch-value" style="display: none;">
                <div class="form-row">
                    <label class="form-label">
                        Selected Watch:
                    </label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlWatch" runat="server" Width="240" CssClass="criteria-watch-id" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phProduct" runat="server" Visible="false">
            <div class="product-value" style="display: none;">
                <div class="form-row">
                    <label class="form-label">
                        Selected Product:
                        <br />
                        <asp:HyperLink ID="hplProduct" runat="server" Text="Select Product" CssClass="criteria-product" />
                    </label>
                    <div class="form-value text-value">
                        <asp:Label ID="lblProduct" runat="server" Text="-" CssClass="criteria-product-title" />
                        <input type="hidden" id="hdnProduct" runat="server" class="criteria-product-id" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phMaxClick" runat="server" Visible="false">
            <div class="maxclick-value" style="display: none;">
                <div class="form-row">
                    <label class="form-label">
                        Maximum click:
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtMaxClick" runat="server" CssClass="criteria-maxclick" Width="40"
                            MaxLength="4" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
    <span style="display: none;" class="nav-criteria-json">
        <asp:HiddenField ID="hdnNavCriteriaJson" runat="server" />
    </span>
    <asp:CustomValidator ID="cvNavigationCriteria" runat="server" ControlToValidate="ddlNavCriteria" Enabled="false"
        Display="None" ClientValidationFunction="ValidateNavigationCriteria" />
</asp:Panel>

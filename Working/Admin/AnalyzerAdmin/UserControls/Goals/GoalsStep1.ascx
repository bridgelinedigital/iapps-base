﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoalsStep1.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.GoalsStep1" %>
<script type="text/javascript">
    function ToggleGoalRange(objDropDown) {
        var objRange = document.getElementById("thresholdRange");
        var objFrom = document.getElementById("thresholdFrom");
        var objTo = document.getElementById("thresholdTo");
        var selValue = objDropDown.options[objDropDown.selectedIndex].value;

        switch (selValue) {
            case '1': // Greatethan
                objRange.style.display = 'none';
                objFrom.style.display = 'inline';
                objTo.style.display = 'none';
                ValidatorEnable(document.getElementById('<%=rqFvThresholdFromValue.ClientID%>'), true);
                ValidatorEnable(document.getElementById('<%=rqFvThresholdToValue.ClientID%>'), false);
                ValidatorEnable(document.getElementById('<%=cpvGreaterThanEqual.ClientID %>'), false);
                break;
            case '2': //Between
                objRange.style.display = 'inline';
                objFrom.style.display = 'inline';
                objTo.style.display = 'inline';
                ValidatorEnable(document.getElementById('<%=rqFvThresholdFromValue.ClientID%>'), true);
                ValidatorEnable(document.getElementById('<%=rqFvThresholdToValue.ClientID%>'), true);
                ValidatorEnable(document.getElementById('<%=cpvGreaterThanEqual.ClientID %>'), true);
                break;
            case '3': //Lessthan
                objRange.style.display = 'none';
                objFrom.style.display = 'none';
                objTo.style.display = 'inline';
                ValidatorEnable(document.getElementById('<%=rqFvThresholdFromValue.ClientID%>'), false);
                ValidatorEnable(document.getElementById('<%=rqFvThresholdToValue.ClientID%>'), true);
                ValidatorEnable(document.getElementById('<%=cpvGreaterThanEqual.ClientID %>'), false);
                break;
        }
    }

    function Calendar_OnChange(sender, eventArgs) {
        var selectedDate = CalendarDate.getSelectedDate();
        var date = new Date();
        date.setHours(0, 0, 0, 0)
        if (selectedDate < date) {
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, DateshouldnotbebeforetodaysDate %>'/>");
            selectedDate = date;
        }
        else {
            document.getElementById("<%=txtDate.ClientID%>").value = CalendarDate.formatDate(selectedDate, "MM/dd/yyyy");
        }
    }

    function PopupGoalCalendar() {
        var textBoxObject = document.getElementById("<%=txtDate.ClientID%>");
        if (textBoxObject.disabled == false) {
            var thisDate = new Date();
            if (Trim(textBoxObject.value) == "<%= JSMessages.ToDate %>" || Trim(textBoxObject.value) == "") {
                textBoxObject.value = "";
            }
            if (textBoxObject.value != "<%= JSMessages.ToDate %>" && Trim(textBoxObject.value) != "") {
                if (validate_dateSummary(textBoxObject) == true) {
                    thisDate = new Date(textBoxObject.value);
                    CalendarDate.setSelectedDate(thisDate);
                }
            }
            if (!(CalendarDate.get_popUpShowing()))
                CalendarDate.show();
        }
    }

    function ToggleGoalDuration(optionText) {
        switch (optionText) {
            case 'timespan':
                document.getElementById("<%=txtTime.ClientID%>").disabled = false;
                document.getElementById("<%=ddlTime.ClientID%>").disabled = false;
                document.getElementById("<%=ddlDuration.ClientID%>").disabled = true;
                document.getElementById("<%=txtDate.ClientID%>").disabled = true;
                document.getElementById("<%=txtDate.ClientID%>").value = "";

                ValidatorEnable(document.getElementById('<%=rqFvTimeValue.ClientID%>'), true);
                ValidatorEnable(document.getElementById('<%=rqFvDateValue.ClientID%>'), false);
                break;
            case 'recurrence':
                document.getElementById("<%=txtTime.ClientID%>").disabled = true;
                document.getElementById("<%=txtTime.ClientID%>").value = "";
                document.getElementById("<%=ddlTime.ClientID%>").disabled = true;
                document.getElementById("<%=ddlDuration.ClientID%>").disabled = false;
                document.getElementById("<%=txtDate.ClientID%>").disabled = true;
                document.getElementById("<%=txtDate.ClientID%>").value = "";

                ValidatorEnable(document.getElementById('<%=rqFvTimeValue.ClientID%>'), false);
                ValidatorEnable(document.getElementById('<%=rqFvDateValue.ClientID%>'), false);
                break;
            case 'specificdate':
                document.getElementById("<%=txtTime.ClientID%>").disabled = true;
                document.getElementById("<%=txtTime.ClientID%>").value = "";
                document.getElementById("<%=ddlTime.ClientID%>").disabled = true;
                document.getElementById("<%=ddlDuration.ClientID%>").disabled = true;
                document.getElementById("<%=txtDate.ClientID%>").disabled = false;

                ValidatorEnable(document.getElementById('<%=rqFvTimeValue.ClientID%>'), false);
                ValidatorEnable(document.getElementById('<%=rqFvDateValue.ClientID%>'), true);
                break;
        }
    }

    function OpenFileDirectoryPopup() {
        OpeniAppsAdminPopup("SelectFileLibraryPopup", "SelectDirectory=true", "SelectFileDirectory");
    }

    function SelectFileDirectory() {
        var fileUrl = popupActionsJson.CustomAttributes["RelativePath"];
        SetObjectIdName(popupActionsJson.FolderId, fileUrl);
    }

    function OpenFileLibraryPopup() {
        OpeniAppsAdminPopup("SelectFileLibraryPopup", "", "SelectFile");
    }

    function SelectFile() {
        SetObjectIdName(popupActionsJson.SelectedItems.first(), popupActionsJson.CustomAttributes["FileName"]);
    }

    function OpenFormLibraryPopup() {
        OpeniAppsAdminPopup("SelectFormLibraryPopup", "", "SelectForm");
    }

    function SelectForm() {
        SetObjectIdName(popupActionsJson.SelectedItems.first(), popupActionsJson.CustomAttributes["FormName"]);
    }

    function OpenPageLibraryPopup() {
        OpeniAppsAdminPopup("SelectPageLibraryPopup", "", "SelectPage");
    }

    function SelectPage() {
        SetObjectIdName(popupActionsJson.SelectedItems.first(), popupActionsJson.CustomAttributes["SelectedPageUrl"]);
    }

    function OpenWatchPopup() {
        OpeniAppsAnalyzerPopup("SelectWatch", "", "SelectWatch");
    }

    function SelectWatch() {
        SetObjectIdName(popupActionsJson.SelectedItems.first(), popupActionsJson.CustomAttributes["WatchName"]);
    }

    function OpenProductTypePopup() {
        var url = jAnalyticAdminSiteUrl + '/Popups/SelectProductType.aspx'
        viewPopupWindow = dhtmlmodal.open('Content', 'iframe', url, '');
    }

    function selectedProductType(Id, Name) {
        SetObjectIdName(Id, Name);
    }

    function OpenProductCategoryPopup() {
        var url = jAnalyticAdminSiteUrl + '/Popups/SelectProductCategory.aspx'
        viewPopupWindow = dhtmlmodal.open('Content', 'iframe', url, '');
    }

    function selectedProductCategory(Id, Name) {
        SetObjectIdName(Id, Name);
    }

    var productSearchPopup;
    function SearchProduct() {
        if (hasCommerceLicense && hasCommercePermission)
            productSearchPopup = OpeniAppsCommercePopup("SelectProductPopup", "ShowProducts=true");
        else
            OpenLicenseWarningPopup("Commerce", hasCommerceLicense, hasCommercePermission);
    }

    function SelectProductFromPopup(product) {
        productSearchPopup.hide();
        SetObjectIdName(product.Id, product.Title);
    }

    function CloseProductPopupFromPopup() {
        productSearchPopup.hide();
    }

    function SetObjectIdName(Id, Name) {
        document.getElementById("<%=hdnObjectId.ClientID%>").value = Id;
        document.getElementById("<%=ltSelectedObject.ClientID%>").innerHTML = Name;
        document.getElementById("<%=hdnObjectName.ClientID%>").value = Name;
    }

    function cmbMetric_onLoad(sender, eventArgs) {
        var comboBox_inputCtl = document.getElementById(sender.get_id() + "_Input");
        comboBox_inputCtl.disabled = true;
    }

</script>
<div class="step1">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <asp:Label ID="lblGoalID" runat="server" Text="" Style="visibility: hidden;"></asp:Label>
    <div class="form-row">
        <label class="form-label"><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, GoalName1 %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtGoalName" runat="server" CssClass="textBoxes" Width="400" MaxLength="255"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfTxtLastName" runat="server" ControlToValidate="txtGoalName"
                ErrorMessage="<%$ Resources:GUIStrings, Pleaseentergoalname %>" Display="None"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="revTxtGoal" ControlToValidate="txtGoalName"
                ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInGoalName %>"
                Display="None"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, DesiredGoal %>" /></label>
        <div class="form-value">
            <ComponentArt:ComboBox ID="cmbMetric" runat="server" SkinID="Default" DropDownHeight="297"
                DropDownWidth="400" AutoHighlight="false" AutoComplete="false" Width="410" AdjustPositioning="true">
                <DropDownContent>
                    <ComponentArt:TreeView ID="trvGoalType" ExpandSinglePath="true" SkinID="Default"
                        OnNodeSelected="trvGoalType_OnNodeSelected" AutoPostBackOnSelect="true" Height="300"
                        Width="400" runat="server">
                    </ComponentArt:TreeView>
                </DropDownContent>
                <ClientEvents>
                    <Load EventHandler="cmbMetric_onLoad" />
                </ClientEvents>
            </ComponentArt:ComboBox>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">&nbsp;</label>
        <div class="form-value">
            <asp:Button type="button" ID="btnObject" CssClass="button" Text="<%$ Resources:GUIStrings, SelectObject %>"
                ToolTip="<%$ Resources:GUIStrings, SelectObject %>" runat="server" CausesValidation="false" />
            <asp:HiddenField runat="server" ID="hdnObjectName" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="hdnObjectId" ID="rqFvHiddenObjectId"
                ErrorMessage="<%$ Resources: JSMessages, PleaseSelectAnObject %>"
                Display="None"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" Operator="NotEqual" Type="String" ControlToValidate="hdnObjectId"
                ValueToCompare="00000000-0000-0000-0000-000000000000" ID="cmpHiddenObjectId"
                ErrorMessage="<%$ Resources: JSMessages, PleaseSelectAnObject %>"
                Display="None" />
            <asp:TextBox ID="hdnObjectId" runat="server" Style="visibility: hidden;" Width="10"></asp:TextBox>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, SelectedObjectColon %>" /></label>
        <div class="form-value text-value">
            <asp:Label ID="ltSelectedObject" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, ThresholdGoal %>" /></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlThresholdCriteria" runat="server" onchange="ToggleGoalRange(this);" Width="160">
                <asp:ListItem Value="1" runat="server" Text="<%$ Resources:GUIStrings, GreaterThan %>"></asp:ListItem>
                <asp:ListItem Value="2" runat="server" Text="<%$ Resources:GUIStrings, Between %>"></asp:ListItem>
                <asp:ListItem Value="3" runat="server" Text="<%$ Resources:GUIStrings, LessThan %>"></asp:ListItem>
            </asp:DropDownList>
            <asp:Label runat="server" ID="lblNumberFormat" Style="display: inline-block; padding: 0 8px;" />
            <div id="thresholdFrom" style="display: inline;">
                <asp:TextBox ID="txtThresholdFrom" runat="server" CssClass="textBoxes" Width="80"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtThresholdFrom" ID="rqFvThresholdFromValue"
                    ErrorMessage="<%$ Resources: JSMessages, PleaseEnterAValueForThreshold %>"
                    Display="None"></asp:RequiredFieldValidator>
                <asp:CompareValidator Operator="DataTypeCheck" Type="Double" runat="server" ControlToValidate="txtThresholdFrom"
                    ID="cmpThresholdFromValue" ErrorMessage="<%$ Resources: JSMessages, pleaseenteravalidnumberforthreshold %>"
                    Display="None" />
                <asp:RangeValidator runat="server" Type="Double" MinimumValue="0" ID="rvThresholdFromValue"
                    MaximumValue="9223372036854775807" ControlToValidate="txtThresholdFrom" ErrorMessage="<%$ Resources: JSMessages, pleaseenteravalidnumberforthreshold %>"
                    Display="None" />
            </div>
            <div id="thresholdRange" style="display: none; padding: 0 10px;">
                <label>to</label>
            </div>
            <div id="thresholdTo" style="display: none;">
                <asp:TextBox ID="txtThresoldTo" runat="server" CssClass="textBoxes" Width="80"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtThresoldTo" ID="rqFvThresholdToValue"
                    ErrorMessage="<%$ Resources: JSMessages, PleaseEnterAMaximumValueForThreshold %>"
                    Display="None"></asp:RequiredFieldValidator>
                <asp:CompareValidator Operator="DataTypeCheck" Type="Double" runat="server" ControlToValidate="txtThresoldTo"
                    ID="cmpThresholdToValue" ErrorMessage="<%$ Resources: JSMessages, pleaseenteravalidnumberformaximumthresholdvalue %>"
                    Display="None" />
                <asp:RangeValidator Type="Double" runat="server" ControlToValidate="txtThresoldTo"
                    MinimumValue="0" MaximumValue="9223372036854775807" ID="CompareValidator1" ErrorMessage="<%$ Resources: JSMessages, pleaseenteravalidnumberformaximumthresholdvalue %>"
                    Display="None" />
                <asp:CompareValidator ID="cpvGreaterThanEqual" Operator="GreaterThanEqual" runat="server" Type="Double" ControlToValidate="txtThresoldTo"
                    ControlToCompare="txtThresholdFrom" ErrorMessage="<%$ Resources: JSMessages, ThresholdFromToCheck %>"
                    Display="None" />
            </div>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, DurationtoComplete %>" /></label>
        <div class="form-value">
            <asp:RadioButton ID="rbTime" runat="server" GroupName="Timeframe" Checked="true"
                onclick="ToggleGoalDuration('timespan');" />
            <asp:TextBox ID="txtTime" runat="server" CssClass="textBoxes" Width="50"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtTime" ID="rqFvTimeValue"
                ErrorMessage="<%$ Resources: JSMessages, PleaseEnterAValidNumberForTime %>"
                Display="None"></asp:RequiredFieldValidator>
            <asp:RangeValidator Type="Integer" runat="server" ControlToValidate="txtTime" MinimumValue="0"
                MaximumValue="2147483647" ID="CompareValidator2" ErrorMessage="<%$ Resources: JSMessages, PleaseEnterAValidNumberForTime %>"
                Display="None" />
            <asp:CompareValidator Operator="DataTypeCheck" Type="Integer" runat="server" ControlToValidate="txtTime"
                ID="cmpTimeValue" ErrorMessage="<%$ Resources: JSMessages, PleaseEnterAValidNumberForTime %>"
                Display="None" />
            &nbsp;&nbsp;
            <asp:DropDownList ID="ddlTime" runat="server">
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Days %>" Value="1"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Weeks %>" Value="2"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, months %>" Value="3"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Years %>" Value="4"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">&nbsp;</label>
        <div class="form-value">
            <asp:RadioButton ID="rbDuration" runat="server" GroupName="Timeframe" onclick="ToggleGoalDuration('recurrence');" />
            <asp:DropDownList ID="ddlDuration" runat="server" Enabled="false" Width="164">
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Daily %>" Value="1"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Weekly %>" Value="2"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Monthly %>" Value="3"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Yearly %>" Value="4"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">&nbsp;</label>
        <div class="form-value">
            <asp:RadioButton ID="rbRange" runat="server" GroupName="Timeframe" onclick="ToggleGoalDuration('specificdate');" style="float: left; padding-right: 6px;" />
            <div class="form-value calendar-value">
                <asp:TextBox ID="txtDate" runat="server" Width="140" Enabled="false"></asp:TextBox>
                <span>
                    <img id="imgFromDate" src="../App_Themes/General/images/calendar-button.png" alt="<%= GUIStrings.SelectFromDate %>"
                        onclick="PopupGoalCalendar();" />
                </span>
            </div>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDate" ID="rqFvDateValue"
                ErrorMessage="<%$ Resources: JSMessages, PleaseEnterAValidDate %>"
                Display="None"></asp:RequiredFieldValidator>
            <asp:CompareValidator Operator="DataTypeCheck" Type="Date" runat="server" ControlToValidate="txtDate"
                ID="cmpFvDateValue" ErrorMessage="<%$ Resources: JSMessages, PleaseEnterAValidDate %>"
                Display="None" />
            <ComponentArt:Calendar runat="server" ID="CalendarDate" SkinID="Default" PopUpExpandControlId="imgFromDate"
                ControlType="Calendar" PopUp="Custom">
                <ClientEvents>
                    <SelectionChanged EventHandler="Calendar_OnChange" />
                </ClientEvents>
            </ComponentArt:Calendar>
        </div>
    </div>
</div>
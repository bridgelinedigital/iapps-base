﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoalsStep3.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.GoalsStep3" %>
<div class="step3">
    <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, SelectAnExistingCampaignToAttachThisGoalTo %>"/></p>
    <div class="scrolling-container">
        <asp:CheckBoxList ID="chkCampaigns" runat="server" RepeatColumns="3" CssClass="campaign-checklist" Width="100%">
        </asp:CheckBoxList>
    </div>
</div>
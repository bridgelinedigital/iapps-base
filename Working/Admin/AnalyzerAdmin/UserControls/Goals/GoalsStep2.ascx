﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoalsStep2.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.GoalsStep2" %>
<div class="step2">
    <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, EnterthegoalvaluesforthisGoalSuggestedvalueshavebeenpopulatedforyouhowevershouldyouwishtochangetheseyoumaydosointhetextboxbelow %>" /></p>
    <asp:ValidationSummary ID="vsSummary" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <componentart:grid id="grdGoalMetric" allowtextselection="true" enableviewstate="true" Width="100%"
        editonclickselecteditem="false" allowediting="true" showheader="False" cssclass="Grid"
        keyboardenabled="false" showfooter="false" skinid="Default" footercssclass="GridFooter"
        runningmode="Client" runat="server" pagesize="1000" emptygridtext="<%$ Resources:GUIStrings, NoItemsFound %>">
        <Levels>
            <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" DataKeyField="Id"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                AllowSorting="false" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate"
                InsertCommandClientTemplateId="InsertCommandTemplate">
                <Columns>
                    <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="250"
                        HeadingText="<%$ Resources:GUIStrings, SuccessCategory %>" DataCellClientTemplateId="CategoryHoverTemplate"
                        DataField="Category" DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell" />
                    <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="650"
                        HeadingText="<%$ Resources:GUIStrings, TrackingMetric %>" DataCellClientTemplateId="TrackingMetricHoverTemplate"
                        DataField="TrackingMetric" />
                    <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, GoalValue %>" DataField="GoalValue" DataCellServerTemplateId="GoalValueTemplate"
                        AllowReordering="False" FixedWidth="true" Align="Left" AllowSorting="False" Width="185" />
                    <ComponentArt:GridColumn Align="Center" AllowReordering="False" FixedWidth="true" Width="120"
                        HeadingText="<%$ Resources:GUIStrings, NumberFormat %>" DataCellClientTemplateId="NumberFormatHoverTemplate"
                        DataField="NumberFormat" />
                    <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                    <ComponentArt:GridColumn DataField="Reason" IsSearchable="false" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="CategoryHoverTemplate">
                <div title="## DataItem.GetMember('Category').get_text() ##">
                    ## DataItem.GetMember('Category').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Category').get_text() ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TrackingMetricHoverTemplate">
                <div title="## DataItem.GetMember('Reason').get_text() ##">
                    ## DataItem.GetMember('TrackingMetric').get_text() == "" ? "&nbsp;" : DataItem.GetMember('TrackingMetric').get_text() ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="NumberFormatHoverTemplate">
                <div title="## DataItem.GetMember('NumberFormat').get_text() ##">
                    ## DataItem.GetMember('NumberFormat').get_text() == "" ? "&nbsp;" : DataItem.GetMember('NumberFormat').get_text() ##</div>
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="GoalValueTemplate">
                <Template>
                    <asp:TextBox runat="server" ID="txtGoalValue" Text='<%# Convert.ToDecimal(Container.DataItem["NumberFormat"].ToString() == "%" ? Convert.ToDecimal(Container.DataItem["GoalValue"]) * 100 : Container.DataItem["GoalValue"]).ToString("0.00##")  %>'
                        Width="175" CssClass="textBoxes"  Visible='<%# !string.IsNullOrEmpty(Container.DataItem["NumberFormat"].ToString().Trim()) %>' ></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfvTxtGoalValue"  ControlToValidate="txtGoalValue"
                        ErrorMessage="<%$ Resources: JSMessages, Pleaseenteragoalvalue %>" Enabled='<%# !string.IsNullOrEmpty(Container.DataItem["NumberFormat"].ToString().Trim()) %>'
                        Text="*"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" Type="Double" ControlToValidate="txtGoalValue" Enabled='<%# !string.IsNullOrEmpty(Container.DataItem["NumberFormat"].ToString().Trim()) %>'
                            ValueToCompare="0" Operator="GreaterThanEqual" ErrorMessage="<%$ Resources: JSMessages, Goalvalueshouldbegreaterthanorequaltozero %>"
                        Text="*"></asp:CompareValidator>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" Type="Double" ControlToValidate="txtGoalValue" Enabled='<%# !string.IsNullOrEmpty(Container.DataItem["NumberFormat"].ToString().Trim()) %>'
                        Operator="DataTypeCheck" ErrorMessage="<%$ Resources: JSMessages, Goalvalueshouldbeanumber %>" Text="*"></asp:CompareValidator>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
    </componentart:grid>
</div>
﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DirectoryTree.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Controls.DirectoryTree" %>
<script type="text/javascript">
    function RefreshLibraryTree() {
        if (tvDirectory != null)
            tvDirectory.dispose();
        cbTreeRefresh.callback(JSON.stringify(treeActionsJson));
    }

    function cbTreeRefresh_onCallbackComplete(sender, eventArgs) {
        treeActionsJson.Text = tvDirectory.get_selectedNode().get_text();
        $("#selectedNodeTitle").html(treeActionsJson.Text);
    }

    function CheckAndDisableSettings(selectedNode) {
        if (GetCurrentProductName() == "marketier" && treeActionsJson.ObjectTypeId == 7 && !IsMarketierDirectory(selectedNode)) {
            gridActionsJson.HideActions = true;
            treeActionsJson.HideActions = true;
        }
    }

    function tvDirectory_onLoad(sender, eventArgs) {
        $(".tree-container").treeActions({
            objTree: tvDirectory,
            pageTree: false,
            nameRegex: (treeActionsJson.ObjectTypeId == 9 || treeActionsJson.ObjectTypeId == 33) ? "[\<\>\".]" : "[\<\>\"]"
        });
       
        CheckAndDisableSettings(tvDirectory.get_selectedNode());
    }

    function tvDirectory_onSelect(sender, eventArgs) {
        gridActionsJson.HideActions = true;
        treeActionsJson.HideActions = true;

        if (eventArgs.get_node().Value > 0) {
            gridActionsJson.HideActions = false;
            treeActionsJson.HideActions = false;
        }
        else if (gridActionsJson.ForceDisplayActions) {
            gridActionsJson.HideActions = false;
        }

        CheckAndDisableSettings(eventArgs.get_node());

        var node = eventArgs.get_node();
        if (typeof node.GetProperty("Role") != "undefined" && node.GetProperty("Role") != "" && node.GetProperty("Role") == "Viewer")
            gridActionsJson.HideActions = true;

        $(".tree-container").treeActions("selectNode", eventArgs);
    }

    function UpdateHideActions() {
        if (!gridActionsJson.HideActions && treeActionsJson.NodeType == 4) //IsSystem
            gridActionsJson.HideActions = true;
    }

    function tvDirectory_onBeforeRename(sender, eventArgs) {
        $(".tree-container").treeActions("beforeRename", eventArgs);
    }

    function Tree_Callback(sender, eventArgs) {
        treeActionsJson.Success = true;

        switch (eventArgs.Action) {
            case "DeleteNode":
                treeActionsJson.Success = window.confirm(__JSMessages["AnyFilesItemsInThisDirectoryWillBeMovedToTheUnassignedDirectoryAndThisDirectoryWillBeDeleted"]);
                if (treeActionsJson.Success) {
                    ShowiAppsLoadingPanel();
                    var result = '';
                    if (typeof CreateNode == 'function') {
                        result = DeleteDirectoryNode(treeActionsJson.FolderId, treeActionsJson.ObjectTypeId);
                    }
                    else if (typeof AdminCallback.WebMethod_DeleteDirectoryNode == 'function') {
                        result = AdminCallback.WebMethod_DeleteDirectoryNode(treeActionsJson.FolderId, treeActionsJson.ObjectTypeId);
                    }

                    if (result.toLowerCase() != "true") {
                        alert(result);
                        treeActionsJson.Success = false;
                    }
                    CloseiAppsLoadingPanel();
                }
                break;
            case "EditSettings":
                OpeniAppsAdminPopup("ManageAssetStructureSettings", "NodeId=" + treeActionsJson.FolderId + "&ParentId=" + treeActionsJson.ParentId + "&ObjectType=" + treeActionsJson.ObjectTypeId, "RefreshLibraryTree");
                break;
            case "UpdateName":
                updateNodeName();
                break;
            case "EditPermissions":
                OpeniAppsAdminPopup("ManagePermissionsByTarget", 'ObjectTypeId=' + treeActionsJson.ObjectTypeId + '&ObjectId=' + treeActionsJson.FolderId);
                break;
            case "ImportContent":
                OpeniAppsAdminPopup("SelectContentLibraryPopup", "LoadMasterSiteData=true&ImportDirectoryId=" + treeActionsJson.FolderId, "RefreshContentLibrary");
                break;
            case "ImportForm":
                OpeniAppsAdminPopup("SelectFormLibraryPopup", "LoadMasterSiteData=true&ImportDirectoryId=" + treeActionsJson.FolderId, "RefreshFormLibrary");
                break;
            case "ImportAsset":
                if (treeActionsJson.ObjectTypeId == '9')
                    OpeniAppsAdminPopup("SelectFileLibraryPopup", "LoadMasterSiteData=true&ImportDirectoryId=" + gridActionsJson.FolderId, "RefreshFileLibrary");
                else if (treeActionsJson.ObjectTypeId == '33')
                    OpeniAppsAdminPopup("SelectImageLibraryPopup", "LoadMasterSiteData=true&ImportDirectoryId=" + gridActionsJson.FolderId, "RefreshImageLibrary");
                break;
            case "EditSequence":
                if (treeActionsJson.ObjectTypeId == '4')
                    OpeniAppsAdminPopup("ManageDisplayOrder", "ObjectTypeId=4", "RefreshStyleLibrary");
                else if (treeActionsJson.ObjectTypeId == '35')
                    OpeniAppsAdminPopup("ManageDisplayOrder", "ObjectTypeId=35", "RefreshScriptLibrary");
                break;
            default:
                break;
        }
    }

    function Tree_ItemSelect(sender, eventArgs) {
        if (typeof LoadLibraryGrid == "function")
            LoadLibraryGrid();
    }

    function updateNodeName() {
        if (treeActionsJson.Action == "AddNode") {
            var result = '';
            if (typeof CreateNode == 'function') {
                result = CreateNode(treeActionsJson.NewText, treeActionsJson.ParentId);
            }
            else if (typeof AdminCallback.WebMethod_CreateNode == 'function') {
                result = AdminCallback.WebMethod_CreateNode(treeActionsJson.NewText, treeActionsJson.ParentId);
            }
            if (result.length == 36)
                treeActionsJson.FolderId = result;
            else {
                alert(result);
                treeActionsJson.Success = false;
            }
        }
        else {
            if (typeof EditNode == 'function') {
                EditNode(treeActionsJson.FolderId, treeActionsJson.NewText);
            }
            else if (typeof AdminCallback.WebMethod_EditNode == 'function') {
                AdminCallback.WebMethod_EditNode(treeActionsJson.FolderId, treeActionsJson.NewText);
            }
        }
    }

</script>
<div class="tree-container">
    <div class="tree-header">
        <asp:Literal ID="ltTreeHeading" runat="server" />
        <iAppsControls:TreeActions ID="treeActions" runat="server" TreeName="tvDirectory" />
    </div>
    <ComponentArt:CallBack ID="cbTreeRefresh" runat="server" SkinID="TreeCallback" CssClass="tree-content">
        <Content>
            <ComponentArt:TreeView AutoPostBackOnSelect="false" SkinID="Default" ID="tvDirectory"
                runat="server" DragAndDropEnabled="false" MultipleSelectEnabled="false" OutputCustomAttributes="true"
                DropRootEnabled="false" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
                <ClientEvents>
                    <Load EventHandler="tvDirectory_onLoad" />
                    <NodeSelect EventHandler="tvDirectory_onSelect" />
                    <NodeBeforeRename EventHandler="tvDirectory_onBeforeRename" />
                </ClientEvents>
                <CustomAttributeMappings>
                    <ComponentArt:CustomAttributeMapping From="Title" To="Text" />
                    <ComponentArt:CustomAttributeMapping From="AccessRoles" To="Value" />
                    <ComponentArt:CustomAttributeMapping From="NumberOfFiles" To="StorageIndex" />
                </CustomAttributeMappings>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="TreeNodeTemplate">
                        <div>
                            ## DataItem.GetProperty('Text') ## 
                            ## GetTreeNodeCount(DataItem) ## 
                            ## GetTreeNodeIcons(DataItem) ## 
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="PopupTemplate">
                        <div>
                            ## DataItem.GetProperty('Text') ## 
                            ## GetTreeNodeIcons(DataItem) ## 
                        </div>
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:TreeView>
        </Content>
        <ClientEvents>
            <CallbackComplete EventHandler="cbTreeRefresh_onCallbackComplete" />
        </ClientEvents>
    </ComponentArt:CallBack>
</div>
<asp:XmlDataSource ID="xmlDataSource" runat="server" />

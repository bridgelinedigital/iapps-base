﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Controls.ManagePageLibrary"
    CodeBehind="ManagePageLibrary.ascx.cs" %>
<%@ Register TagPrefix="PT" TagName="PageTree" Src="~/UserControls/Libraries/Data/PageMapTree.ascx" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<script type="text/javascript">
    function grdPages_OnLoad(sender, eventArgs) {
        $("#page_library").gridActions({ objList: grdPages });
    }

    function grdPages_OnCallbackComplete() {
        $("#page_library").gridActions("displayMessage");
    }

    function grdPages_OnRenderComplete(sender, eventArgs) {
        $("#page_library").gridActions("bindControls");
    }

    function LoadPageGrid() {
        $("#page_library").gridActions("selectNode", treeActionsJson.FolderId);
    }

    function Grid_Callback(sender, eventArgs) {
        grdPages.set_callbackParameter(JSON.stringify(gridActionsJson));
        grdPages.callback();
    }

    function SelectPageFromLibrary() {
        var selectedItem = $("#page_library").gridActions("getSelectedItem");
        if (selectedItem) {
            popupActionsJson.CustomAttributes["SelectedPageUrl"] = selectedItem.getMember("CompleteFriendlyURL").get_text();
            popupActionsJson.CustomAttributes["SelectedPageTitle"] = selectedItem.getMember("Title").get_text();
            popupActionsJson.CustomAttributes["SelectedTitle"] = selectedItem.getMember("Title").get_text();
            popupActionsJson.CustomAttributes["SelectedUrl"] = selectedItem.getMember("CompleteFriendlyURL").get_text();
            popupActionsJson.CustomAttributes["AbsolutePageUrl"] = selectedItem.getMember("AbsolutePageUrl").get_text();
            SelectiAppsAdminPopup();
        }

        return false;
    }

    function SelectMenuFromLibrary() {
        popupActionsJson.CustomAttributes["SelectedFolderName"] = treeActionsJson.Text;
        popupActionsJson.CustomAttributes["SelectedFolderId"] = treeActionsJson.FolderId;
        popupActionsJson.CustomAttributes["DirHierarchypath"] = treeActionsJson.SelectedPath;

        SelectiAppsAdminPopup();

        return false;
    }

</script>
<div id="page_library" class="clear-fix">
    <asp:PlaceHolder ID="phPageTree" runat="server">
        <div class="left-control">
            <asp:HiddenField ID="hdnSelectedPagesForImportProcessing" ClientIDMode="Static" runat="server" />
            <PT:PageTree ID="pageTree" runat="server" />
        </div>
    </asp:PlaceHolder>
    <div class="right-control">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <div class="grid-section">
            <ComponentArt:Grid ID="grdPages" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
                ShowFooter="false" LoadingPanelClientTemplateId="grdPagesLoadingPanelTemplate"
                ManualPaging="true" CssClass="fat-grid">
                <ClientEvents>
                    <Load EventHandler="grdPages_OnLoad" />
                    <CallbackComplete EventHandler="grdPages_OnCallbackComplete" />
                    <RenderComplete EventHandler="grdPages_OnRenderComplete" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <ConditionalFormats>
                            <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('IsConnectedPage').Value == true"
                                RowCssClass="connected" SelectedRowCssClass="connected" />
                            <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('PageStatus').Value==7"
                                RowCssClass="archive-row" SelectedRowCssClass="archive-row" />
                        </ConditionalFormats>
                        <Columns>
                            <ComponentArt:GridColumn DataField="DisplayOrder" DataCellServerTemplateId="DetailsTemplate" />
                            <ComponentArt:GridColumn DataField="Title" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="TemplateName" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="PublishDate" Visible="false" />
                            <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" FormatString="d" />
                            <ComponentArt:GridColumn DataField="AuthorName" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="CompleteFriendlyURL" Visible="false" />
                            <ComponentArt:GridColumn DataField="PageStatus" Visible="false" />
                            <ComponentArt:GridColumn DataField="PageStatusId" Visible="false" />
                            <ComponentArt:GridColumn DataField="WorkflowStateId" Visible="false" />
                            <ComponentArt:GridColumn DataField="IsConnectedPage" Visible="false" />
                            <ComponentArt:GridColumn DataField="IsInGroupPublish" Visible="false" />
                            <ComponentArt:GridColumn DataField="SourcePageDefinitionId" Visible="false" />
                            <ComponentArt:GridColumn DataField="AbsolutePageUrl" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                        <Template>
                            <div class="collection-row grid-item clear-fix movable-row" objectid='<%# Container.DataItem["Id"]%>'>
                                <%-- <div class="first-cell">
                                   <%# Container.DataItem["DisplayOrder"] %>
                                </div>--%>
                                <div class="large-cell">
                                    <h4>
                                        <%# Container.DataItem["Title"]%>
                                        <%# string.Format(Convert.ToBoolean(Container.DataItem["IsConnectedPage"]) == true?
                                    "<img src='/Admin/App_Themes/General/images/linked-object.png' alt='Connected to Menu' title='Connected to Menu' />" : "&nbsp")%></h4>
                                    <p><a onclick="ViewLivePage('<%# Container.DataItem["CompleteFriendlyURL"]%>'); return false;" href="#" target="_blank"><%# Container.DataItem["CompleteFriendlyURL"]%></a></p>
                                    <p><%# Container.DataItem["Description"]%></p>
                                </div>
                                <div class="small-cell">
                                    <div class="child-row">
                                        <%# Container.DataItem["TemplateName"].Equals(string.Empty) ? string.Empty : string.Format("<strong>{0}</strong>: {1}", GUIStrings.Template, Container.DataItem["TemplateName"]) %>
                                    </div>
                                    <div class="child-row">
                                        <%# (Convert.ToDateTime(Container.DataItem["PublishDate"]).Equals(new DateTime(1900, 1, 1)) ?
                                            string.Format("<strong>{0}</strong>: {1} {2} {3}", GUIStrings.Created, string.Format("{0:d}", Container.DataItem["ModifiedDate"]), GUIStrings.By, Container.DataItem["AuthorName"]) :
                                            string.Format("<strong>{0}</strong>: {1} {2} {3}", GUIStrings.Published, string.Format("{0:d}", Container.DataItem["PublishDate"]), GUIStrings.By, Container.DataItem["AuthorName"])) %>
                                    </div>
                                    <div class="child-row">
                                        <%# string.Format("<strong>{0}</strong>: {1} | <a class='view-tags' objectid='{2}'>{3}</a>", 
                                    GUIStrings.Status, Container.DataItem["PageStatus"], Container.DataItem["Id"],  GUIStrings.Tags) %>
                                    </div>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                    <ComponentArt:GridServerTemplate ID="PopupTemplate">
                        <Template>
                            <div class="modal-grid-row grid-item clear-fix">
                                <div class="first-cell">
                                    <input type="checkbox" id="chkSelectedForImport" class="item-selector" runat="server"
                                        visible="false" value='<%# Container.DataItem["Id"]%>' />
                                    <h4>
                                        <%# Container.DataItem["Title"]%></h4>
                                </div>
                                <div class="last-cell">
                                    <%# string.Format("<strong>{0}</strong>: {1}", GUIStrings.Template, Container.DataItem["TemplateName"])%>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdPagesDragTemplate">
                        ## GetGridDragTemplateById(grdPages, DataItem) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdPagesLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdPages) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Controls.ManageAssetImage"
    CodeBehind="ManageAssetImage.ascx.cs" %>
<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<script type="text/javascript">
    window["UploadFileType"] = "AssetImage";

    function upImages_OnLoad() {
        $("#image_library").gridActions({
            objList: $(".asset-list"),
            type: "list",
            onCallback: function (sender, eventArgs) {
                upImages_Callback(sender, eventArgs);
            }
        });
    }

    function LoadLibraryGrid() {
        $("#image_library").gridActions("selectNode", treeActionsJson.FolderId);
        fn_InitializeTooltip();
    }

    function upImages_OnRenderComplete() {
        $("#image_library").gridActions("bindControls");
        $("#image_library").iAppsSplitter({ borderIndex: 2 });
        SetTaxonomyForList();

        $(".asset-thumbnail-holder").hover(
        function () {
            $(this).find(".float-icon").show();
            if (gridActionsJson.HideActions)
                $("#image_library").find(".delete-image").hide();
        },
        function () {
            $(this).find(".float-icon").hide();
        });

        $(".float-icon").unbind("click").bind("click", function () {
            $("#image_library").gridActions("callback", $(this).attr("action"), { selectedItem: $(this).parents(".asset-list-item") });
            return false;
        });
    }

    function upImages_OnCallbackComplete() {
        $("#image_library").gridActions("displayMessage", {
            complete: function () {
                if (gridActionsJson.ResponseCode == 100)
                    CanceliAppsAdminPopup(true);
            }
        });
    }

    function upImages_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        var selectedTitle = "";
        if (eventArgs && eventArgs.selectedItem)
            selectedTitle = eventArgs.selectedItem.getValue("FileName");

        switch (gridActionsJson.Action) {
            case "EditImage":
                doCallback = false;
                OpeniAppsAdminPopup("ImageEditor", "ImageId=" + selectedItemId, "RefreshImageLibrary");
                break;
            case "Add":
                doCallback = false;
                var editQS = 'NodeId=' + gridActionsJson.FolderId;
                OpeniAppsAdminPopup("ManageAssetImageDetails", editQS, "RefreshImageLibrary");
                break;
            case "AddImage":
                doCallback = false;
                OpeniAppsAdminPopup("ImageQuickUpload", "", "RefreshImageLibrary");
                break;
            case "EditProperties":
                doCallback = false;
                OpeniAppsAdminPopup("ManageAssetImageDetails", "NodeId=" + gridActionsJson.FolderId + "&ImageId=" + selectedItemId, "RefreshImageLibrary");
                break;
            case "DeleteImage":
                doCallback = false;
                var qString = stringformat("Mode=Warning&ObjectTypeId=33&Id={0}&Name={1}&ButtonText={2}&Url={3}", selectedItemId, selectedTitle, "<%= GUIStrings.DeleteImage %>", eventArgs.selectedItem.getValue("Path"));
                OpeniAppsAdminPopup("ViewPagesUsingObject", qString, "CloseDeleteImage");
                break;
            case "ViewHistory":
                doCallback = false;
                OpeniAppsAdminPopup("FileHistoryPopup", "ObjectType=33&FileId=" + selectedItemId, "RefreshImageLibrary");
                break;
            case "ViewFile":
                doCallback = false;
                OpeniAppsFile(eventArgs.selectedItem.getValue("Path"));
                break;
            case "ViewPages":
                doCallback = false;
                OpeniAppsAdminPopup("ViewPagesUsingObject", stringformat("ObjectTypeId=33&Id={0}&Name={1}&Url={2}", selectedItemId, selectedTitle, eventArgs.selectedItem.getValue("Path")));
                break;
            case "Preview":
                doCallback = false;
                OpeniAppsAdminPopup("PreviewImagePopup", "Id=" + selectedItemId, "");
                break;
            case "ReImport":
                doCallback = false;
                OpeniAppsAdminPopup("ReimportFromMaster", "ObjectTypeId=33", "SetReImportMasterPage");
                break;
            case "AddToList":
                doCallback = false;
                AddManualImageItem($(".asset-list"));
                break;
            case "AssignTags":
                doCallback = false;
                var qString = 'NodeId=' + gridActionsJson.FolderId;
                if (selectedItemId) {
                    qString += '&ImageId=' + selectedItemId + "&AssignTags=true";
                    OpeniAppsAdminPopup("ManageAssetImageDetails", qString, "RefreshImageLibrary");
                }
                break;
        }

        if (doCallback) {
            upImages.set_callbackParameter(JSON.stringify(gridActionsJson));
            upImages.callback();
        }
    }

    function CloseDeleteImage() {
        $("#image_library").gridActions("callback", "Delete", { refreshTree: true });
    }

    function CloseQuickUploadPopup() {
        RefreshImageLibrary();
    }

    function RefreshImageLibrary() {
        $("#image_library").gridActions("callback", { refreshTree: true });
    }

    function SetReImportMasterPage() {
        $("#image_library").gridActions("callback", "ReImportFromMaster", { refreshTree: false });
    }

    function ImportFromMaster() {
        if (gridActionsJson.SelectedItems.length > 0) {
            var result = CAWebMethod_HasManagerAccess('9', gridActionsJson.FolderId);
            if (result == "true") {
                var imageVariantStatus = IsImageVariantExist(JSON.stringify(gridActionsJson.SelectedItems));
                if (imageVariantStatus == "true") {
                    if (window.confirm("<%= JSMessages.ImageReimportAlertMessage %>")) {
                        $("#image_library").gridActions("callback", "ImportFromMaster");
                    }
                    else
                        doCallback = false;
                }
                else {
                    $("#image_library").gridActions("callback", "ImportFromMaster");
                }
            }
            else {
                alert("<%= JSMessages.NoImportAccessonDirectory %>");
            }
        }
        else
            alert("<%= JSMessages.PleaseSelectAImageToImport %>");

        return false;
    }

    function SelectMenuFromLibrary() {
        popupActionsJson.CustomAttributes["SelectedFolderName"] = treeActionsJson.Text;
        popupActionsJson.CustomAttributes["SelectedFolderId"] = treeActionsJson.FolderId;
        popupActionsJson.CustomAttributes["DirHierarchypath"] = treeActionsJson.SelectedPath;

        SelectiAppsAdminPopup();
        return false;
    }

    function SelectImageFromLibrary() {
        var selectedItem = $("#image_library").gridActions("getSelectedItem");
        if (selectedItem) {
            popupActionsJson.CustomAttributes.ImageUrl = selectedItem.getValue("Path");
            popupActionsJson.CustomAttributes.PhysicalPath = selectedItem.getValue("PhysicalPath");
            popupActionsJson.CustomAttributes.ThumbUrl = selectedItem.getValue("ThumbPath");
            popupActionsJson.CustomAttributes.ImageTitle = selectedItem.getValue("Title");
            popupActionsJson.CustomAttributes.ImageFileName = selectedItem.getValue("FileName");
            popupActionsJson.CustomAttributes.AltText = selectedItem.getValue("AltText");

            popupActionsJson.CustomAttributes.SelectedTitle = selectedItem.getValue("Title");
            popupActionsJson.CustomAttributes.SelectedUrl = selectedItem.getValue("Path");
            popupActionsJson.CustomAttributes.ImageSiteId = selectedItem.getValue("imgSiteId");
            popupActionsJson.CustomAttributes.ImageSize = selectedItem.getValue("imgSize");

            SelectiAppsAdminPopup();
        }
        else {
            alert("<%= JSMessages.PleaseSelectAImage %>");
        }

        return false;
    }
    function GridItem_ShowMenu(menu) {

        if (typeof hideAssignTags !== 'undefined') {
            if (hideAssignTags == 'True') {
                menu.getItemByCommand("AssignTags").show();
            }
            else {
                menu.getItemByCommand("AssignTags").hide();
            }
        }
    }
    $(function () {
        fn_InitializeTooltip();
    });

    function fn_InitializeTooltip() {
        setTimeout(function () {
            iAPPS_Tooltip.Initialize('rel', 'title', 'top');
        }, 1000);
    }
</script>
<div id="image_library">
    <div class="left-control">
        <UC:LibraryTree ID="libraryTree" runat="server" />
    </div>
    <div class="right-control">
        <iAppsControls:CallbackPanel ID="upImages" runat="server" OnClientCallbackComplete="upImages_OnCallbackComplete"
            OnClientRenderComplete="upImages_OnRenderComplete" OnClientLoad="upImages_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <div class="grid-section">
                    <asp:ListView ID="lvImages" runat="server" ItemPlaceholderID="phImageList">
                        <EmptyDataTemplate>
                            <div class="empty-collection-table empty-list">
                                <asp:Localize ID="lc100" runat="server" Text="<%$ Resources:GUIStrings, NoImagesFound %>" />
                            </div>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <div class="collection-table asset-list clear-fix fat-grid">
                                <asp:PlaceHolder ID="phImageList" runat="server" />
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <asp:PlaceHolder ID="DetailsTemplate" runat="server">
                                <div class='<%# Convert.ToBoolean(Container.DataItemIndex % 2) ? "row" : "alternate-row" %>'>
                                    <div class="collection-row grid-item clear-fix" objectid='<%# Eval("Id") %>'>
                                        <div class="first-cell">
                                            <div class="thumbnail-container">
                                                <asp:Image ID="imgThumbnail" runat="server" ImageUrl="~/App_Themes/General/images/no-thumbnail.png" />
                                            </div>
                                        </div>
                                        <div class="middle-cell">
                                            <p>
                                                <span datafield="FileName">
                                                    <asp:Literal ID="ltFileName" runat="server" /></span>
                                                <asp:Image ID="imgSizeAlert" runat="server" ImageUrl="~/App_Themes/General/images/icon-exclamation-border.png" CssClass="image-size-alert" rel="tooltip"
                                                    ToolTip="<%$ Resources:GUIStrings, ImageRequiresOptimizationForWeb %>" />
                                            </p>
                                        </div>
                                        <div class="last-cell">
                                            <div class="child-row">
                                                <%= GUIStrings.FileType %>:
                                                <asp:Literal ID="ltType" runat="server" />
                                            </div>
                                            <div class="child-row">
                                                <%= GUIStrings.FileSize %>:
                                                <asp:Literal ID="ltSize" runat="server" />
                                            </div>
                                            <div class="child-row">
                                                <a class="view-tags" objectid='<%# Eval("Id") %>'>Tags</a>
                                                <input type="hidden" datafield="Path" id="hdnImageUrl" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="ThumbnailDetailsTemplate" runat="server">
                                <div class="asset-list-item grid-item" objectid='<%# Eval("Id") %>'>
                                    <div class="asset-thumbnail-holder">
                                        <label>
                                            <asp:Literal ID="t_ltSerialNo" runat="server" /></label>
                                        <asp:Image ID="t_imgSizeAlert" runat="server" ImageUrl="~/App_Themes/General/images/icon-exclamation-border.png" CssClass="image-size-alert" rel="tooltip"
                                            ToolTip="<%$ Resources:GUIStrings, ImageRequiresOptimizationForWeb %>" />
                                        <asp:Image ID="t_imgThumbnail" runat="server" ImageUrl="~/App_Themes/General/images/no-thumbnail.png" />
                                        <asp:Image ID="t_imgPreview" runat="server" ImageUrl="~/App_Themes/General/images/icon-view.png"
                                            CssClass="float-icon preview-image" action="Preview" />
                                        <asp:Image ID="t_imgDelete" runat="server" ImageUrl="~/App_Themes/General/images/trash-icon.png"
                                            CssClass="float-icon delete-image" action="DeleteImage" />
                                    </div>
                                    <div class="asset-title">
                                        <h5 datafield="FileName">
                                            <asp:Literal ID="t_ltFileName" runat="server" /></h5>
                                        <asp:Literal ID="t_ltTitle" runat="server" />
                                    </div>
                                    <div class="asset-info">
                                        Type:
                                        <asp:Literal ID="t_ltType" runat="server" />
                                        <a class="view-tags" objectid='<%# Eval("Id") %>'>Tags</a>
                                        <input type="hidden" datafield="Path" id="t_hdnImageUrl" runat="server" />
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="PopupTemplate" runat="server"> 
                                <div class='<%# Convert.ToBoolean(Container.DataItemIndex % 2) ? "row" : "alternate-row" %>'>
                                    <div class="collection-row grid-item clear-fix" objectid='<%# Eval("Id") %>'>
                                        <div class="first-cell">
                                            <input type="checkbox" id="chkSelectedForImport" runat="server" class="item-selector"
                                                visible="false" value='<%# Eval("Id") %>' />
                                            <div class="thumbnail-container">
                                                <asp:Image ID="p_imgThumbnail" runat="server" ImageUrl="~/App_Themes/General/images/no-thumbnail.png" />
                                            </div>
                                        </div>
                                        <div class="middle-cell">
                                            <p>
                                                <span datafield="FileName"><asp:Literal ID="p_ltFileName" runat="server" /></span>
                                                <asp:Image ID="p_imgSizeAlert" runat="server" ImageUrl="~/App_Themes/General/images/icon-exclamation-border.png" CssClass="image-size-alert" rel="tooltip"
                                                    ToolTip="<%$ Resources:GUIStrings, ImageRequiresOptimizationForWeb %>" />
                                            </p>
                                            <p><asp:Literal ID="p_ltImageURL" runat="server" /></p>
                                        </div>
                                        <div class="last-cell">
                                            <div class="child-row">
                                                <%= GUIStrings.FileType %>:
                                                <asp:Literal ID="p_ltType" runat="server" />
                                            </div>
                                            <div class="child-row">
                                                <%= GUIStrings.FileSize %>:
                                                <asp:Literal ID="p_ltSize" runat="server" />
                                            </div>
                                        </div>
                                        <input type="hidden" datafield="Path" id="p_hdnImageUrl" runat="server" />
                                        <input type="hidden" datafield="PhysicalPath" id="p_hdnPhysicalPath" runat="server" />
                                        <input type="hidden" datafield="ThumbPath" id="p_hdnThumbUrl" runat="server" />
                                        <input type="hidden" datafield="Id" id="hdnId" runat="server" />
                                        <input type="hidden" datafield="Description" id="hdnDescription" runat="server" />
                                        <input type="hidden" datafield="Title" id="hdnTitle" runat="server" />
                                        <input type="hidden" datafield="ImageHeight" id="hdnImageHeight" runat="server" />
                                        <input type="hidden" datafield="ImageWidth" id="hdnImageWidth" runat="server" />
                                        <input type="hidden" datafield="AltText" id="hdnAltText" runat="server" />
                                        <input type="hidden" datafield="ModifiedDate" id="hdnModifiedDate" runat="server" />
                                        <input type="hidden" datafield="imgSiteId" id="hdnimgSiteId" runat="server" />
                                        <input type="hidden" datafield="imgSize" id="hdnImgSize" runat="server" />
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="ThumbnailPopupTemplate" runat="server">
                                <div class="asset-list-item grid-item" objectid='<%# Eval("Id") %>'>
                                    <div class="asset-thumbnail-holder">
                                        <label><asp:Literal ID="pt_ltSerialNo" runat="server" /></label>
                                        <asp:Image ID="pt_imgSizeAlert" runat="server" ImageUrl="~/App_Themes/General/images/icon-exclamation-border.png" CssClass="image-size-alert" rel="tooltip"
                                            ToolTip="<%$ Resources:GUIStrings, ImageRequiresOptimizationForWeb %>" />
                                        <asp:Image ID="pt_imgThumbnail" runat="server" ImageUrl="~/App_Themes/General/images/no-thumbnail.png" />
                                        <input type="checkbox" id="pt_chkSelectedForImport" runat="server" class="item-selector"
                                            visible="false" value='<%# Eval("Id") %>' />
                                    </div>
                                    <div class="asset-title">
                                        <h5 datafield="FileName">
                                            <asp:Literal ID="pt_ltFileName" runat="server" /></h5>
                                    </div>
                                    <input type="hidden" datafield="Path" id="pt_hdnImageUrl" runat="server" />
                                    <input type="hidden" datafield="PhysicalPath" id="pt_hdnPhysicalPath" runat="server" />
                                    <input type="hidden" datafield="ThumbPath" id="pt_hdnThumbUrl" runat="server" />
                                    <input type="hidden" datafield="Id" id="pt_hdnId" runat="server" />
                                    <input type="hidden" datafield="Description" id="pt_hdnDescription" runat="server" />
                                    <input type="hidden" datafield="Title" id="pt_hdnTitle" runat="server" />
                                    <input type="hidden" datafield="ImageHeight" id="pt_hdnImageHeight" runat="server" />
                                    <input type="hidden" datafield="ImageWidth" id="pt_hdnImageWidth" runat="server" />
                                    <input type="hidden" datafield="AltText" id="pt_hdnAltText" runat="server" />
                                    <input type="hidden" datafield="ModifiedDate" id="pt_hdnModifiedDate" runat="server" />
                                    <input type="hidden" datafield="imgSiteId" id="pt_hdnimgSiteId" runat="server" />
                                </div>
                            </asp:PlaceHolder>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</div>

﻿<%@ Control Language="C#" AutoEventWireup="True" Codebehind="ManageAssetFile.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Controls.ManageAssetFile" %>
<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<script type="text/javascript">
    window["UploadFileType"] = "AssetFile";

    var objectType = 9; // AssetFile
    function grdFileLib_OnLoad(sender, eventArgs) {
        $("#file_library").gridActions({ objList: grdFileLib });
    }

    function LoadLibraryGrid() {
        $("#file_library").gridActions("selectNode", treeActionsJson.FolderId);
    }

    function grdFileLib_OnRenderComplete(sender, eventArgs) {
        $("#file_library").gridActions("bindControls");
        $("#file_library").iAppsSplitter({ minHeight: 592 });
    }

    function grdFileLib_OnCallbackComplete(sender, eventArgs) {
        $("#file_library").gridActions("displayMessage", { complete: function () {
            if (gridActionsJson.ResponseCode == 100)
                CanceliAppsAdminPopup(true);
        }
        });
    }

    function Grid_Callback(sender, eventArgs) {
        doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        if (eventArgs)
            selectedItem = eventArgs.selectedItem;

        var selectedTitle = "";
        var relativePath = "";
        if (selectedItem) {
            relativePath = selectedItem.getMember("RelativePath").get_value().replace('~', '');
            selectedTitle = selectedItem.getMember("FileName").get_value();
        }

        switch (gridActionsJson.Action) {
            case "View":
                doCallback = false;
                var fileUrl = relativePath + "/" + selectedItem.getMember("FileName").get_value();
                if (!isMasterSite)
                    fileUrl = fileUrl + "?micro_site_id=" + jAppId;
                OpeniAppsFile(fileUrl);
                break;
            case "ViewHistory":
                doCallback = false;
                OpeniAppsAdminPopup("FileHistoryPopup", "ObjectType=9&FileId=" + selectedItemId, "RefreshFileLibrary");
                break;
            case "Edit":
                doCallback = false;
                OpenAssetFileDetails(selectedItem, selectedItemId);
                break;
            case "Add":
                doCallback = false;
                OpenAssetFileDetails();
                break;
            case "DeleteFile":
                doCallback = false;
                var qString = stringformat("Mode=Warning&ObjectTypeId=9&Id={0}&Name={1}&ButtonText={2}&Url={3}", selectedItemId, selectedTitle, "<%= GUIStrings.DeleteFile %>", relativePath + "/" + selectedTitle);
                OpeniAppsAdminPopup("ViewPagesUsingObject", qString, "CloseDeletePopup");
                break;
            case "ViewPagesUsingFile":
                doCallback = false;
                OpeniAppsAdminPopup("ViewPagesUsingObject", stringformat("ObjectTypeId=9&Id={0}&Name={1}&Url={2}", selectedItemId, selectedTitle, relativePath + "/" + selectedTitle));
                break;
            case "ReImport":
                doCallback = false;
                OpeniAppsAdminPopup("ReimportFromMaster", "ObjectTypeId=9", "SetReImportMasterPage");
                break;
            case "AddToList":
                doCallback = false;
                AddManualListItem(grdFileLib);
                break;
            case "AddFiles":
                doCallback = false;
                OpeniAppsAdminPopup("ImageQuickUpload", "", "RefreshFileLibrary");
                break;
        }

        if (doCallback && typeof grdFileLib != "undefined") {
            grdFileLib.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdFileLib.callback();
        }
    }

    function CloseQuickUploadPopup() {
        RefreshFileLibrary();
    }

    function SetReImportMasterPage() {
        $("#file_library").gridActions("callback", "ReImportFromMaster", { refreshTree: false });
    }

    function ImportFromMaster() {
        if (gridActionsJson.SelectedItems.length > 0) {
            var result = CAWebMethod_HasManagerAccess('9', gridActionsJson.FolderId);
                if (result == "true") {
                    var fileVariantStatus = IsFileVariantExist(JSON.stringify(gridActionsJson.SelectedItems));
                    if (fileVariantStatus == "true") {
                        if (window.confirm("<%= JSMessages.FileReimportAlertMessage %>")) {
                            $("#file_library").gridActions("callback", "ImportFromMaster");
                        }
                        else
                            doCallback = false;
                    }
                    else
                        $("#file_library").gridActions("callback", "ImportFromMaster");
                }
                else {
                    alert("<%= JSMessages.NoImportAccessonDirectory %>");
                }
        }
        else
            alert("<%= JSMessages.PleaseSelectAFileToImport %>");
        return false;
    }

    function RefreshFileLibrary() {
        $("#file_library").gridActions("callback", { refreshTree: true });
    }

    function CloseDeletePopup() {
        //Cancel does not call this method, so there is no need to check the operation.
        $("#file_library").gridActions("callback", "Delete", { refreshTree: true });
    }

    function OpenAssetFileDetails(selectedItem, selectedItemId) {
        var editQS = 'NodeId=' + gridActionsJson.FolderId;
        if (selectedItem)
            editQS += '&FileId=' + selectedItemId;
        editAssetFilePopup = OpeniAppsAdminPopup("ManageAssetFileDetails", editQS, "RefreshFileLibrary");
    }

    function grdFileLib_OnExternalDrop(sender, eventArgs) {
        var replacedString = /['\\"]/g;

        var dragFileId = eventArgs.get_item().getMember("Id").get_text();
        var dragFileTitle = eventArgs.get_item().getMember("Title").get_text().replace(replacedString, '\\$&');

        var targetControl = eventArgs.get_targetControl();
        var targetControlId;
        if (targetControl) {
            targetControlId = targetControl.GlobalAlias;
        }

        if (targetControlId == 'grdFileLib') {
            if (gridActionsJson.SortBy == null || gridActionsJson.SortBy == "OrderNo") {
                var targetItem = eventArgs.get_target();
                var targetItemId = null;
                if (targetItem != null) {
                    targetItemId = targetItem.getMember("Id").get_text();
                }
                if (targetItemId != null && targetItemId != dragFileId) {
                    gridActionsJson.CustomAttributes.FromFileId = dragFileId;
                    gridActionsJson.CustomAttributes.ToFileId = targetItemId;
                    $("#file_library").gridActions("callback", "ChangeDisplayOrder");
                }
            }
            else {
                alert("<%= JSMessages.ChangeSortToDisplayOrderBeforeReorder %>");
            }
        }
        else {
            var toMenuId = eventArgs.get_target().get_id();
            var fromMenuId = gridActionsJson.FolderId;
            gridActionsJson.CustomAttributes["MoveNodeId"] = toMenuId;

            if (dragFileId != null && dragFileId.length == 36 && gridActionsJson.FolderId != toMenuId) {
                targetNode = tvDirectory.findNodeById(toMenuId);
                var targetDepth = targetNode.get_depth();
                if (targetDepth == 0) {
                    alert("<%= JSMessages.MovingAFileToTheRootMenuIsNotAllowed %>");
                }
                else {
                    var queryString = 'FileId=' + dragFileId + "&FromMenuId=" + fromMenuId + "&ToMenuId=" + toMenuId;
                    OpeniAppsAdminPopup("MoveAssetFile", queryString, "CloseMoveFileDialog");
                }
            }
        }

    }

    function CloseMoveFileDialog() {
        gridActionsJson.FolderId = gridActionsJson.CustomAttributes["MoveNodeId"];
        gridActionsJson.PageNumber = 1;
        RefreshFileLibrary();
    }

    function SelectFileFromLibrary() {
        var selectedItem = $("#file_library").gridActions("getSelectedItem");
        if (selectedItem) {
            popupActionsJson.CustomAttributes["RelativePath"] = selectedItem.getMember("RelativePath").get_text();
            popupActionsJson.CustomAttributes["FileName"] = selectedItem.getMember("FileName").get_text();
            popupActionsJson.CustomAttributes["SelectedFileTitle"] = selectedItem.getMember("Title").get_text();
            popupActionsJson.CustomAttributes["SelectedTitle"] = selectedItem.getMember("Title").get_text();
            SelectiAppsAdminPopup();
        }
        else {
            alert("<%= JSMessages.PleaseSelectAFile %>");
        }

        return false;
    }

    function SelectMenuFromLibrary() {
        popupActionsJson.CustomAttributes["SelectedFolderName"] = treeActionsJson.Text;
        popupActionsJson.CustomAttributes["SelectedFolderId"] = treeActionsJson.FolderId;
        popupActionsJson.CustomAttributes["DirHierarchypath"] = treeActionsJson.SelectedPath;

        SelectiAppsAdminPopup();
        return false;
    }
  
</script>
<div id="file_library">
    <div class="left-control">
        <UC:LibraryTree ID="libraryTree" runat="server" />
    </div>
    <div class="right-control">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" Position="Top" />
        <div class="grid-section">
            <ComponentArt:Grid ID="grdFileLib" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
                ShowFooter="false" LoadingPanelClientTemplateId="grdFileLibLoadingPanelTemplate"
                CssClass="fat-grid" ItemDraggingEnabled="true" ItemDraggingClientTemplateId="grdFileDragTemplate">
                <ClientEvents>
                    <Load EventHandler="grdFileLib_OnLoad" />
                    <CallbackComplete EventHandler="grdFileLib_OnCallbackComplete" />
                    <RenderComplete EventHandler="grdFileLib_OnRenderComplete" />
                    <ItemExternalDrop EventHandler="grdFileLib_OnExternalDrop" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate"
                                IsSearchable="true" Visible="true" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="OrderNo" Visible="false" />
                            <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="FileName" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="Extension" Visible="false" />
                            <ComponentArt:GridColumn DataField="FileSizeString" Visible="false" />
                            <ComponentArt:GridColumn DataField="RelativePath" Visible="false" />
                            <ComponentArt:GridColumn DataField="FullPath" Visible="false" />
                            <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" FormatString="d" />
                            <ComponentArt:GridColumn DataField="ModifiedByName" Visible="false" />
                            <ComponentArt:GridColumn DataField="DirectoryId" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                        <Template>
                            <div class="collection-row grid-item clear-fix movable-row" objectid='<%# Container.DataItem["Id"]%>'>
                                <%--<div class="first-cell">
                                    <p>
                                        <%# Container.DataItem["OrderNo"]%></p>
                                </div>--%>
                                <div class="large-cell">
                                    <h4>
                                        <%# Container.DataItem["Title"]%>
                                        <%# Container.DataItem["Title"].ToString() != Container.DataItem["FileName"].ToString() ? String.Format("| {0}",Container.DataItem["FileName"]) : ""%>
                                    </h4>
                                    <p>
                                        <%# Container.DataItem["Title"].ToString() != Container.DataItem["Description"].ToString() ? Container.DataItem["Description"] : ""%>
                                    </p>
                                </div>
                                <div class="small-cell">
                                    <div class="child-row">
                                        <%# String.Format("<strong>{0}</strong>: {1} | <strong>{2}</strong>: {3}", GUIStrings.Type, Container.DataItem["Extension"].ToString().Trim('.').ToUpper(),
                                            GUIStrings.Size, Container.DataItem["FileSizeString"])%>
                                    </div>
                                    <div class="child-row">
                                        <%# String.Format("<strong>{0}</strong>: {1} {2} {3}", GUIStrings.Updated, String.Format("{0:d}",Container.DataItem["ModifiedDate"]),
                                            GUIStrings.By, Container.DataItem["ModifiedByName"])%>
                                    </div>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                    <ComponentArt:GridServerTemplate ID="PopupTemplate" runat="server">
                        <Template>
                            <div class="modal-grid-row grid-item clear-fix">
                                <div class="first-cell">
                                    <input type="checkbox" id="chkSelectedForImport" class="item-selector" runat="server"
                                        visible="false" value='<%# Container.DataItem["Id"]%>' />
                                    <h4>
                                        <%# Container.DataItem["Title"]%>
                                    </h4>
                                </div>
                                <div class="last-cell">
                                    <%# String.Format("<strong>{0}</strong> : {1} ", GUIStrings.Type, Container.DataItem["Extension"].ToString().Trim('.').ToUpper())%>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdFileLibLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdFileLib) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdFileDragTemplate">
                        ## GetGridDragTemplateById(grdFileLib, DataItem) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</div>

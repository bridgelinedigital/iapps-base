﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageMapTree.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Controls.PageMapTree" %>
<script type="text/javascript">
    function RefreshLibraryTree() {
        if (tvPageLibrary != null)
            tvPageLibrary.dispose();
        cbTreeRefresh.callback(JSON.stringify(treeActionsJson));
    }

    function cbTreeRefresh_onCallbackComplete(sender, eventArgs) {
        treeActionsJson.Text = tvPageLibrary.get_selectedNode().get_text();
        $("#selectedNodeTitle").html(treeActionsJson.Text);
    }

    function tvPageLibrary_onLoad(sender, eventArgs) {
        $(".tree-container").treeActions({
            objTree: tvPageLibrary,
            fixHeader: $(".assign-workflow").length > 0 ? false : true
        });
    }

    function tvPageLibrary_onSelect(sender, eventArgs) {
        if (typeof HideGridActions == "function")
            gridActionsJson.HideActions = HideGridActions(eventArgs.get_node().get_value()) == "true";
        if (typeof HideTreeActions == "function")
            treeActionsJson.HideActions = HideTreeActions(eventArgs.get_node().getProperty("NodeType"), eventArgs.get_node().get_value()) == "true";

        $(".tree-container").treeActions("selectNode", eventArgs);
    }

    function tvPageLibrary_onBeforeRename(sender, eventArgs) {
        $(".tree-container").treeActions("beforeRename", eventArgs);
    }

    function Tree_Callback(sender, eventArgs) {
        treeActionsJson.Success = true;

        switch (eventArgs.Action) {
            case "DeleteNode":
                treeActionsJson.Success = window.confirm("<%= JSMessages.AreYouSureYouWantToDelete %>");
                if (treeActionsJson.Success) {
                    var result = DeleteMenuNode(treeActionsJson.FolderId);
                    if (result.toLowerCase() != "true") {
                        alert(result);
                        treeActionsJson.Success = false;
                    }
                }
                break;
            case "ValidateName":
                validateNodeName();
                break;
            case "UpdateName":
                updateNodeName();
                break;
            case "EditProperties":
                OpeniAppsAdminPopup("ManagePageMapNodeDetails", 'NodeId=' + treeActionsJson.FolderId, "RefreshTreeAndLoadGrid");
                break;
            case "MakeVisible":
                treeActionsJson.Success = MakeMenuVisible(treeActionsJson.FolderId) == "True";
                break;
            case "MakeInVisible":
                treeActionsJson.Success = MakeMenuInVisible(treeActionsJson.FolderId) == "True";
                break;
            case "EditPermissions":
                OpeniAppsAdminPopup("ManagePermissionsByTarget", 'ObjectTypeId=2&ObjectId=' + treeActionsJson.FolderId);
                break;
            case "ImportPages":
                OpeniAppsAdminPopup("SelectPageLibraryPopup", "LoadMasterSiteData=true&CheckCustomPermission=false", "SetImportMasterPages");
                break;
            default:
                break;
        }
    }
    function RefreshTreeAndLoadGrid() {
        RefreshLibraryTree();
        LoadPageGrid();

        if(popupActionsJson.Action == "SubmitForDistribution")
            OpeniAppsAdminPopup("SubmitForDistribution", "ObjectTypeId=2&ObjectId=" + popupActionsJson.FolderId);
    }

    function Tree_ItemSelect(sender, eventArgs) {
        if (treeActionsJson.SelectedValue == 21)
            sender.getItemByCommand("EditPermission").show();
        else
            sender.getItemByCommand("EditPermission").hide();

        if (IsHomeNode(eventArgs.get_node()))
            sender.getItemByCommand("MakeVisible").hide();

        if (typeof LoadPageGrid == "function")
            LoadPageGrid();

        if (typeof LoadMenuWorkflows == "function")
            LoadMenuWorkflows();
    }

    function updateNodeName() {
        ShowiAppsLoadingPanel();
        if (treeActionsJson.Action == "AddNode") {
            treeActionsJson.FolderId = CreateMenuNode(treeActionsJson.NewText, treeActionsJson.ParentId);
        }
        else {
            EditMenuNode(treeActionsJson.FolderId, treeActionsJson.NewText);
        }
        CloseiAppsLoadingPanel();
    }

    function validateNodeName() {
        if (treeActionsJson.Success) {
            var bExists = DoesMenuNameOrUrlExist(treeActionsJson.FolderId, treeActionsJson.ParentId, treeActionsJson.NewText, treeActionsJson.NewText)
            if (bExists == "1" || bExists == 1) {
                alert("<%= JSMessages.MenuItemWithTheSameFriendlyUrlAlreadyExistsPleaseChooseADifferentName %>")
                treeActionsJson.Success = false;
            }
            else if (bExists == "2" || bExists == 2) {
                alert("<%= JSMessages.MenuItemWithTheSameNameAlreadyExistsInThisLevelPleaseChooseADifferentName %>")
                treeActionsJson.Success = false;
            }
            else if (bExists == "3" || bExists == 3) {
                alert("<%= JSMessages.MenuItemWithTheSameFriendlyUrlAndNameAlreadyExistPleaseChooseADifferentName %>");
                treeActionsJson.Success = false;
            }
        }
    }

    function tvPageLibrary_onBeforeMove(sender, eventArgs) {
        var movedNode = eventArgs.get_node();
        var parentNode = eventArgs.get_node().get_parentNode();
        var newParentNode = eventArgs.get_newParentNode();

        if (movedNode.get_depth() <= 1) {
            alert("<%= JSMessages.MenuGroupItemCantBeMoved %>");
            eventArgs.set_cancel(true);
        }
        else if (parentNode.get_id() != newParentNode.get_id()) {
            alert("<%= JSMessages.MenuItemsCanOnlyBeSortedWithinTheSameLevel %>");
            eventArgs.set_cancel(true);
        }
        else {
            result = ReorderNode(movedNode.get_id(), eventArgs.get_index());

            if (result.toLowerCase() == "false") {
                alert("<%= JSMessages.ErrorMenuAndPagesNotReorderedSuccessfully %>");
                eventArgs.set_cancel(true);
            }
        }
    }

    function tvPageLibrary_onMove(sender, eventArgs) {
        alert("<%= JSMessages.MenuAndPagesMovedSuccessfully %>");
    }
</script>
<div class="tree-container">
    <div class="tree-header clear-fix">
        <asp:Literal ID="ltTreeHeading" runat="server" />
        <iAppsControls:TreeActions ID="treeActions" runat="server" TreeName="tvPageLibrary" />
    </div>
    <ComponentArt:CallBack ID="cbTreeRefresh" runat="server" SkinID="TreeCallback" CssClass="tree-content">
        <Content>
            <ComponentArt:TreeView AutoPostBackOnSelect="false" SkinID="Default" ID="tvPageLibrary"
                runat="server" DragAndDropEnabled="false" MultipleSelectEnabled="false" OutputCustomAttributes="true"
                DropRootEnabled="false" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
                <ClientEvents>
                    <Load EventHandler="tvPageLibrary_onLoad" />
                    <NodeSelect EventHandler="tvPageLibrary_onSelect" />
                    <NodeBeforeRename EventHandler="tvPageLibrary_onBeforeRename" />
                    <NodeBeforeMove EventHandler="tvPageLibrary_onBeforeMove" />
                    <NodeMove EventHandler="tvPageLibrary_onMove" />
                </ClientEvents>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="TreeNodeTemplate">
                        <div>
                            ## DataItem.GetProperty('Text') ## 
                            ## GetTreeNodeCount(DataItem) ## 
                            ## GetTreeNodeIcons(DataItem) ## 
                        </div>
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:TreeView>
        </Content>
        <ClientEvents>
            <CallbackComplete EventHandler="cbTreeRefresh_onCallbackComplete" />
        </ClientEvents>
    </ComponentArt:CallBack>
</div>
<asp:SiteMapDataSource ID="SiteMapDSMenu" runat="server" SiteMapProvider="FolderWebSiteMapProvider"
    ShowStartingNode="True" />

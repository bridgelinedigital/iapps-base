<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    StylesheetTheme="General" Inherits="General_SearchResult" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerSearchResult %>"
    CodeBehind="SearchResult.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams

        var item = "";

        /** Methods to Expand/Collapse Tree **/
        function expandTree(objTag) {
            if (objTag.className == "expanded") {
                objTag.className = "collapsed";
                objTag.innerHTML = "<asp:localize runat='server' text='<%$ Resources:JSMessages, CollapseTree %>'/>";
                searchResultTree.expandAll();
            }
            else {
                objTag.className = "expanded";
                objTag.innerHTML = "<asp:localize runat='server' text='<%$ Resources:JSMessages, ExpandTree %>'/>";
                searchResultTree.collapseAll();
            }
            return false;
        }
        /** Function used to filter data in search result grid based on limit type selected **/
        function FilterGrid(node) {
            var table = searchResultGrd.get_table(0);
            var Column1 = table.get_columns()[0];
            var Column2 = table.get_columns()[1];

            var searchFordisplay = document.getElementById("<%=searchedFor.ClientID%>");
            searchFordisplay.innerHTML = '<%= JSMessages.For %> ' + node.Value;

            if (node.Value == '<asp:localize runat="server" text="<%$ Resources:JSMessages, ExpandAll %>"/>') {

                searchResultGrd.Search('', false);
                searchResultGrd.Filter('');
                searchResultGrd.SearchString = "";
                Column1.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, Title %>'/>");
                Column2.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, Description %>'/>");
                searchResultGrd.Render();
            }
            else {
                //alert(node.Value);
                searchResultGrd.Filter("DataItem.GetMember('IndexType').Value == '" + node.Value + "'");
                searchResultGrd.Page(0);


                Column2.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, FileName %>'/>");

                switch (node.Value) {
                    case "Users":
                        Column1.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, FirstName %>'/>");
                        Column2.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, LastName %>'/>");
                        //Column2.set_headingText("User Name");
                        break;
                    case "Segments":
                        Column1.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, SegmentName %>'/>");
                        Column2.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, Description %>'/>");
                        break;
                    case "Watches":
                        //Column1 = table.get_columns()["Name"]// [14];
                        //Column2 = table.get_columns()["Description"];
                        Column1.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, WatchName %>'/>");
                        Column2.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, Description %>'/>");
                        break;
                    case "Alerts":
                        Column1.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, AlertName %>'/>");
                        Column2.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, Description %>'/>");
                        break;
                    case "Pages":
                        Column1.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, PageTitle %>'/>");
                        Column2.set_headingText("<asp:localize runat='server' text='<%$ Resources:JSMessages, Description %>'/>");
                        break;
                }

                //            if(node.Value == "Images" || node.Value == "Files")
                //            {
                //                Column2.set_headingText("File Name");
                //            }
                //            
                //            else if(node.Value == "Pages")
                //            {
                //              Column1.set_headingText("Title");
                //              Column2.set_headingText("Menu Hierarchy");
                //            }
                //            else if(node.Value == "Content Definitions")
                //            {
                //              Column1.set_headingText("Name");
                //              Column2.set_headingText("FileName");
                //            }
                //            else if(node.Value == "Users")
                //            {
                //              Column1.set_headingText("First name");
                //              Column2.set_headingText("Last name");
                //            }
                window.onerror = errorsuppress;

                searchResultGrd.Render();
            }

            SetPageSetting();
        }


        function errorsuppress() {
            return true;
        }

        function SetPageSetting() {
            var startRecord = (searchResultGrd.get_currentPageIndex() * searchResultGrd.get_pageSize()) + 1;
            var endRecord = (searchResultGrd.get_currentPageIndex() * searchResultGrd.get_pageSize()) + searchResultGrd.get_pageSize();
            //if record count is less than the calculated end record number then make end record equal to record count
            if (endRecord > searchResultGrd.get_recordCount()) {
                endRecord = searchResultGrd.get_recordCount();
            }
        }
        function searchResultGrid_onCallbackComplete(sender, eventArgs) {
            var startRecord = (sender.get_currentPageIndex() * sender.get_pageSize()) + 1;
            if (sender.get_recordCount() == 0)
                startRecord = 0;
            var endRecord = (sender.get_currentPageIndex() * sender.get_pageSize()) + sender.get_pageSize();
            //if record count is less than the calculated end record number then make end record equal to record count
            if (endRecord > sender.get_recordCount()) {
                endRecord = sender.get_recordCount();
            }
        }
        function searchResultGrid_onPageIndexChanged(sender, eventArgs) {
            var startRecord = (eventArgs.get_index() * sender.get_pageSize()) + 1;
            if (sender.get_recordCount() == 0)
                startRecord = 0;
            var endRecord = (eventArgs.get_index() * sender.get_pageSize()) + sender.get_pageSize();
            //if record count is less than the calculated end record number then make end record equal to record count
            if (endRecord > sender.get_recordCount()) {
                endRecord = sender.get_recordCount();
            }
        }
        function searchResults_OnContextMenu(sender, eventArgs) {
            selectedItem = eventArgs.get_item();
            menuItems = cmSearchResult.get_items();
            if (selectedItem) {
                switch (selectedItem.GetMemberAt(3).Value) {
                    case "Watches":
                        menuItems.getItem(0).set_visible(true);
                        menuItems.getItem(1).set_visible(false);
                        menuItems.getItem(2).set_visible(false);
                        menuItems.getItem(3).set_visible(false);
                        menuItems.getItem(4).set_visible(false);
                        break;
                    case "Segments":
                        menuItems.getItem(1).set_visible(true);
                        menuItems.getItem(0).set_visible(false);
                        menuItems.getItem(2).set_visible(false);
                        menuItems.getItem(3).set_visible(false);
                        menuItems.getItem(4).set_visible(false);
                        break;
                    case "Users":
                        menuItems.getItem(2).set_visible(true);
                        menuItems.getItem(0).set_visible(false);
                        menuItems.getItem(1).set_visible(false);
                        menuItems.getItem(3).set_visible(false);
                        menuItems.getItem(4).set_visible(false);
                        break;
                    case "Alerts":
                        menuItems.getItem(3).set_visible(true);
                        menuItems.getItem(0).set_visible(false);
                        menuItems.getItem(1).set_visible(false);
                        menuItems.getItem(2).set_visible(false);
                        menuItems.getItem(4).set_visible(false);
                        break;
                    case "Pages":
                        menuItems.getItem(4).set_visible(true);
                        menuItems.getItem(0).set_visible(false);
                        menuItems.getItem(1).set_visible(false);
                        menuItems.getItem(2).set_visible(false);
                        menuItems.getItem(3).set_visible(false);
                        break;

                }
            }
            var evt = eventArgs.get_event();
            cmSearchResult.set_contextData(eventArgs.get_item());
            cmSearchResult.showContextMenuAtEvent(evt);
        }
        function cmSearchResult_ContextMenuClickHandler(sender, eventArgs) {
            var selectedAssetImageId = null;
            var pageFriendlyUrl = "";
            //selectedImage = cmSearchResult.get_contextData();
            //selectedAssetImageId =selectedImage.getMember('Id').get_value();
            CheckSession();
            if (eventArgs.get_item()) {
                switch (eventArgs.get_item().get_id()) {
                    case "JumpToWatches":
                        if (selectedItem.GetMember('WatchType').Text == "AssetWatch") {
                            pageFriendlyUrl = "Administration/WatchedEvents/AssetWatches.aspx";
                        }
                        else if (selectedItem.GetMember('WatchType').Text == "ClientWatch") {
                            pageFriendlyUrl = "Administration/WatchedEvents/BrowserWatches.aspx";
                        }
                        else if (selectedItem.GetMember('WatchType').Text == "TopViewWatch") {
                            pageFriendlyUrl = "Administration/WatchedEvents/TopViewedWatches.aspx";
                        }
                        // selectedImage.getMember("CompleteFriendlyUrl").get_text();
                        // get frontend website complete url from callback                    
                        var fullUrl = GetEditorUrlWithToken(pageFriendlyUrl);
                        window.location = fullUrl;
                        break;
                    case "JumpToSegments":

                        var pageFriendlyUrl = "Administration/AudienceSegmentList.aspx";
                        // get frontend website complete url from callback                    
                        var fullUrl = GetEditorUrlWithToken(pageFriendlyUrl);
                        window.location = fullUrl;
                        break;
                    case "JumpToAlerts":
                        if (selectedItem.GetMember('AlertType').Text == "WatchEvent") {
                            pageFriendlyUrl = "Reporting/Alerts/WatchedEvents.aspx";
                        }
                        else if (selectedItem.GetMember('AlertType').Text == "PageView") {
                            pageFriendlyUrl = "Reporting/Alerts/PageViews.aspx";
                        }
                        else {
                            pageFriendlyUrl = "Reporting/Alerts/VisitorCount.aspx";
                        }
                        // get frontend website complete url from callback                    
                        var fullUrl = GetEditorUrlWithToken(pageFriendlyUrl);
                        window.location = fullUrl;
                        break;
                    case "JumpToUsers":
                        var pageFriendlyUrl = "Administration/ManageUser.aspx";
                        // get frontend website complete url from callback                    
                        var fullUrl = GetEditorUrlWithToken(pageFriendlyUrl);
                        window.location = fullUrl;
                        break;
                    case "JumpToEditor":

                        var currentUrl = document.URL;
                        if (currentUrl == null || currentUrl == '') {
                            currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                        }
                        if (currentUrl.indexOf('?') > 0) {
                            currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                        }
                        currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                        window.location.href = selectedItem.GetMember("CompleteFriendlyUrl").get_text() + currentUrl + "&Token=" + GetToken(cmsProductId, siteId, siteName);

                        break;

                }
            }
        }
        /** Methods to Expand/Collapse Tree **/
    </script>
</asp:Content>
<asp:Content ID="contentSearchResult" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
<div class="search-result clear-fix">
    <h1><%= GUIStrings.SearchResults %></h1>
    <p>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Yousearchedfor %>" />&nbsp;
        <asp:Label ID="searchTerms" runat="server" Text="<%$ Resources:GUIStrings, searchstatement %>"></asp:Label>
    </p>
    <div class="left-section">
        <h2><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SearchResultsbyArea %>" /></h2>
        <div class="treeViewContainer">
            <ComponentArt:TreeView SkinID="Default" ID="searchResultTree" EnableViewState="true"
                KeyboardEnabled="true" MultipleSelectEnabled="true" KeyboardCutCopyPasteEnabled="true"
                DisplayMargin="false" ClientSideOnNodeSelect="FilterGrid" runat="server"
                DropSiblingEnabled="true">
            </ComponentArt:TreeView>
        </div>
    </div>
    <div class="right-section">
        <h2>
            <%= GUIStrings.SearchResults %>&nbsp;<asp:Label ID="searchedFor" runat="Server"
                Text="<%$ Resources:GUIStrings, forExpandAll %>"></asp:Label></h2>
        <ComponentArt:Grid ID="searchResultGrd" runat="server" SkinID="Default" Width="694"
            PageSize="10" SliderPopupClientTemplateId="searchResultSliderTemplate" PagerInfoClientTemplateId="MyPagerInfo"
            EmptyGridText="<%$ Resources:GUIStrings, Norecordfound %>">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="IndexId" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                    <Columns>
                        <ComponentArt:GridColumn Width="316" runat="server" HeadingText="<%$ Resources:GUIStrings, Title %>"
                            AllowReordering="false" HeadingCellCssClass="FirstHeadingCell" DataField="DirectLink"
                            DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" />
                        <ComponentArt:GridColumn Width="216" runat="server" HeadingText="<%$ Resources:GUIStrings, Description %>"
                            DataField="DisplayName" SortedDataCellCssClass="SortedDataCell" AllowReordering="false" />
                        <ComponentArt:GridColumn DataField="IndexId" Visible="false" />
                        <ComponentArt:GridColumn DataField="IndexType" Visible="false" runat="server" HeadingText="IndexType" />
                        <ComponentArt:GridColumn DataField="UserId" Visible="false" runat="server" HeadingText="UserId" />
                        <ComponentArt:GridColumn DataField="SegmentId" Visible="false" runat="server" HeadingText="SegmentId" />
                        <ComponentArt:GridColumn DataField="WatchId" Visible="false" runat="server" HeadingText="WatchId" />
                        <ComponentArt:GridColumn DataField="AlertId" Visible="false" runat="server" HeadingText="AlertId" />
                        <ComponentArt:GridColumn DataField="PageId" Visible="false" runat="server" HeadingText="PageId" />
                        <ComponentArt:GridColumn DataField="CompleteFriendlyUrl" Visible="false" runat="server"
                            HeadingText="friendlyUrl" />
                        <ComponentArt:GridColumn DataField="FirstName" Visible="false" runat="server" HeadingText="12FirstName" />
                        <ComponentArt:GridColumn DataField="LastName" Visible="false" runat="server" HeadingText="LastName" />
                        <ComponentArt:GridColumn DataField="SegmentName" Visible="false" runat="server" HeadingText="SegmentName" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" runat="server" HeadingText="Segment Description" />
                        <ComponentArt:GridColumn DataField="Name" Visible="false" runat="server" HeadingText="Watch Name" />
                        <ComponentArt:GridColumn DataField="AlertType" Visible="false" runat="server" HeadingText="AlertTypeName" />
                        <ComponentArt:GridColumn DataField="WatchType" Visible="false" runat="server" HeadingText="WatchTypeName" />
                        <%--<ComponentArt:GridColumn DataField="Description"   Visible="false"  HeadingText="Watch Descripton"/>--%>
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <PageIndexChange EventHandler="searchResultGrid_onPageIndexChanged" />
                <CallbackComplete EventHandler="searchResultGrid_onCallbackComplete" />
                <ContextMenu EventHandler="searchResults_OnContextMenu" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="MyPagerInfo">
                    ## GetGridPaginationInfo(searchResultGrd) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="PreviewTemplateIcon">
                    <%--<img src="## DataItem.GetMember('PreviewIcon').Value ##"   style="## DataItem.GetMember('StyleIcon').Value ##"  onclick="javascript:OpenPreviewURL('## DataItem.GetMember('PreviewURL').Value ##','## DataItem.GetMember('DisplayName').Text ##','## DataItem.GetMember('IndexType').Value ##')" height="16" width="16" class="previewImage" alt="Preview" title="Preview" />--%>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="searchResultSliderTemplate">
                    <div class="SliderPopup">
                        <h5>
                            ## DataItem.GetMember('DisplayName').Value ##</h5>
                        <p>
                            ## DataItem.GetMember('DirectLink').Value ##</p>
                        <p>
                            ## DataItem.GetMember('IndexType').Value ##</p>
                        <p>
                            ## DataItem.GetMember('Date').Value ##</p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, searchResultGrd.PageCount)
                                ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, searchResultGrd.RecordCount)
                                    ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
    <ComponentArt:Menu SkinID="ContextMenu" ID="cmSearchResult" SiteMapXmlFile="<%$ Resources:GUIStrings, XmlSearchResultsXml %>"
        Orientation="Vertical" Width="158" ContextMenu="Custom" runat="server">
        <ClientEvents>
            <ItemSelect EventHandler="cmSearchResult_ContextMenuClickHandler" />
        </ClientEvents>
    </ComponentArt:Menu>
</div>
</asp:Content>

﻿<%@ Page runat="serveR" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerControlCenter %>" ValidateRequest="false" EnableEventValidation="false"
    Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Dashboard" StylesheetTheme="General" %>
<asp:Content ID="headContent" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">


        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var reverseTooltipImage;
        var downTooltipImage;
        var openCodePopup = 0;

        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor, imgSrc, revImgSrc, downImgSrc) {
            document.getElementById("dhtmlpointer").src = imgSrc;
            reverseTooltipImage = revImgSrc;
            downTooltipImage = downImgSrc;
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }
        function positiontip(e) {
            if (enabletip) {
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/


        function BindScripts() {
            $(".manage-widgets").each(function () {
                var $this = $(this);
                $this.off("click", ".edit-widget");
                $this.on("click", ".edit-widget", function () {
                    if ($(this).hasClass("expanded")) {
                        $(".widget-list").hide();
                        $(".edit-widget").removeClass("expanded");
                    }
                    else {
                        $(".widget-list").hide();
                        $(".edit-widget").removeClass("expanded");
                        $this.find(".widget-list").show();
                        $(this).addClass("expanded");
                    }
                });
                $this.off("click", "input[type='checkbox']");
                $this.on("click", "input[type='checkbox']", function (e) {
                    var selCount = $this.find("input[type='checkbox']:checked").length;
                    var maxLength = $this.attr("type") == "links" ? 8 : 4;
                    if (selCount == 0) {
                        alert("You should have atleast one item selected.");
                        $(this).prop("checked", true);
                    }
                    else if (selCount > maxLength) {
                        alert("You cannot have more than " + maxLength + " items selected.");
                        $(this).prop("checked", false);
                    }
                    else {
                        if ($this.attr("type") == "links")
                            cbLinks.callback();
                        else
                            cbWidgets.callback();
                    }
                });

                $this.off("click", ".iapps-menu-close");
                $this.on("click", ".iapps-menu-close", function () {
                    $(this).parent().hide();
                    $(".edit-widget").removeClass("expanded");
                });
            });

            if ($(".manage-widgets").length > 0) {
                $(".cc-buttons, .site-activity").sortable({
                    forcePlaceHolderSize: true,
                    revert: true
                });

                $(document).click(function (e) {
                    if (!$(e.target).closest(".manage-widgets").length) {
                        $(".widget-list").hide();
                    }
                });
            }
        }

        function OnSaveClick() {
            dashboardDataJson.Links = [];
            dashboardDataJson.Widgets = [];

            $(".cc-buttons > a").each(function () {
                dashboardDataJson.Links.push($(this).attr("linkUrl"));
            });
            $(".site-activity > .boxes").each(function () {
                dashboardDataJson.Widgets.push($(this).attr("widgetUrl"));
            });

            $("#hdnDashboardData").val(JSON.stringify(dashboardDataJson));
            return true;
        }

        $(function () {
            BindScripts();
        });

        function cbLinks_OnCallbackComplete() {
            BindScripts();
            $(".cc-buttons .widget-list").show();
        }

        function cbWidgets_OnCallbackComplete() {
            BindScripts();
            $(".site-activity .widget-list").show();
        }
    </script>
</asp:Content>
<asp:Content ID="headerbuttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1>
            <asp:Literal ID="ltPageHeading" runat="server" /></h1>
        <asp:HyperLink ID="hplCustomize" runat="server" Text="<%$ Resources:GUIStrings, Customize %>"
            CssClass="button" NavigateUrl="~/General/Dashboard.aspx?Customize=true" />
        <asp:PlaceHolder ID="phCustomizeButtons" runat="server" Visible="false">
            <asp:HiddenField ID="hdnDashboardData" runat="server" ClientIDMode="Static" />
            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return OnSaveClick();"
                OnClick="btnSave_Click" />
            <asp:HyperLink ID="hplCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                NavigateUrl="~/General/Dashboard.aspx" CssClass="button" />
        </asp:PlaceHolder>
    </div>
</asp:Content>
<asp:Content ID="contentPlaceHolder" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <iAppsControls:CallbackPanel ID="cbLinks" runat="server" CssClass="cc-buttons clear-fix"
        OnClientCallbackComplete="cbLinks_OnCallbackComplete">
        <ContentTemplate>
            <asp:PlaceHolder ID="phEditLinks" runat="server" Visible="false">
                <div class="manage-widgets" type="links">
                    <a class="edit-widget">
                        <%= GUIStrings.AddRemove %></a>
                    <div class="widget-list" style="display: none;">
                        <div class="iapps-menu-close">
                            <img alt="close" src="/Admin/App_Themes/General/images/menu-close.png" />
                        </div>
                        <h4>
                            <asp:Literal ID="ltLinks" runat="server" Text="<%$ Resources:GUIStrings, DashboardLinksHelp %>" /></h4>
                        <asp:CheckBoxList ID="chkLinks" runat="server" />
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:Literal ID="ltCurrentLinks" runat="server" />
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
    <iAppsControls:CallbackPanel ID="cbWidgets" runat="server" CssClass="site-activity clear-fix"
        OnClientCallbackComplete="cbWidgets_OnCallbackComplete">
        <ContentTemplate>
            <div class="section-header clear-fix">
                <h2><asp:Localize ID="lc2" runat="server" Text="<%$ Resources:GUIStrings, SiteActivity %>" /></h2><p>(<em><asp:Localize ID="lc1" runat="server" Text="<%$ Resources:GUIStrings, AnalyzerDashboardDateRangeNode %>" /></em>)</p>
            </div>
            <asp:PlaceHolder ID="phEditWidgets" runat="server" Visible="false">
                <div class="manage-widgets" type="widgets">
                    <a class="edit-widget">
                        <%= GUIStrings.AddRemove %></a>
                    <div class="widget-list" style="display: none;">
                        <div class="iapps-menu-close">
                            <img alt="close" src="/Admin/App_Themes/General/images/menu-close.png" />
                        </div>
                        <h4>
                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:GUIStrings, DashboardWidgetsHelp %>" /></h4>
                        <asp:CheckBoxList ID="chkWidgets" runat="server" />
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phWidgets" runat="server" />
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master"
    StylesheetTheme="General" AutoEventWireup="true" Inherits="Administration_AudienceSegmentsList"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerAdministrationAudienceSegments %>"
    CodeBehind="AudienceSegmentList.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1%>'/>"; //added by adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1%>'/>"; //added by adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items%>'/>"; //added by adams

        var itemAudSeg;
        var emptyGuid = '00000000-0000-0000-0000-000000000000';

        function grdAudienceSegmentList_onContextMenu(sender, eventArgs) {
            var evt = eventArgs.get_event();
            var menuItems = cmAudienceSegMenu.get_items();
            itemAudSeg = eventArgs.get_item();
            id = itemAudSeg.getMember('Id').get_text();

            grdAudienceSegmentList.select(itemAudSeg);
            cmAudienceSegMenu.showContextMenuAtEvent(evt);
        }

        function cmAudienceSegMenu_onItemSelect(sender, eventArgs) {
            var itemSelected = eventArgs.get_item().get_id();
            var id = itemAudSeg.getMember('Id').get_text();

            switch (itemSelected) {
                case "addSegment":
                    window.location = jAnalyticAdminSiteUrl + "/Administration/AudienceSegments.aspx";
                    break;
                case "editSegment":
                    window.location = jAnalyticAdminSiteUrl + "/Administration/AudienceSegments.aspx?SegmentId=" + id;
                    break;
                case "deleteSegment":
                    grdAudienceSegmentList.set_callbackParameter(stringformat("delete^{0}", id));
                    grdAudienceSegmentList.callback();
                    break;
            }
        }

        function grdAudienceSegmentList_OnExternalDrop(sender, eventArgs) {
            grdAudienceSegmentList.set_callbackParameter(stringformat("ChangeDisplayOrder^{0}^{1}^{2}", eventArgs.get_item().getMember('Id').get_value(), eventArgs.get_target().getMember('Id').get_value()), eventArgs.get_target().get_index());
            grdAudienceSegmentList.callback();
        }
    </script>
</asp:Content>
<asp:Content ID="contentClientIPExclusion" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
    <div class="client-ip">
        <div class="page-header clear-fix">
            <h1><%= GUIStrings.AudienceSegments %></h1>
        </div>
        <ComponentArt:Menu ID="cmAudienceSegMenu" runat="server" SkinID="ContextMenu">
            <Items>
                <ComponentArt:MenuItem ID="addSegment" runat="server" Text="<%$ Resources:GUIStrings, AddSegment %>"
                    Look-LeftIconUrl="cm-icon-add.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="editSegment" runat="server" Text="<%$ Resources:GUIStrings, EditSegment %>"
                    Look-LeftIconUrl="cm-icon-edit.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="deleteSegment" runat="server" Text="<%$ Resources:GUIStrings, DeleteSegment %>"
                    Look-LeftIconUrl="cm-icon-delete.png">
                </ComponentArt:MenuItem>
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="cmAudienceSegMenu_onItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
        <ComponentArt:Grid ID="grdAudienceSegmentList" runat="server" SkinID="Default"
            RunningMode="Callback" ItemDraggingEnabled="true" ItemDraggingClientTemplateId="dragTemplate"
            ExternalDropTargets="grdAudienceSegmentList" Height="290" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
            AutoPostBackOnSelect="false" PageSize="100" Width="100%" EditOnClickSelectedItem="false"
            SliderPopupClientTemplateId="grdAudienceSegmentListSlider" SliderPopupCachedClientTemplateId="grdAudienceSegmentListSliderCached"
            PagerInfoClientTemplateId="grdAudienceSegmentListPagination">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    SortedHeadingCellCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText"
                    SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                    SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                    AlternatingRowCssClass="AlternateDataRow">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="SegmentName" Width="550" runat="server" HeadingText="<%$ Resources:GUIStrings, AudienceSegmentName %>"
                            FixedWidth="true" DefaultSortDirection="Ascending" AllowReordering="False" DataCellClientTemplateId="audSegmentNameCT"
                            Align="Left" DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell" />
                        <ComponentArt:GridColumn DataField="Description" Width="550" runat="server" HeadingText="<%$ Resources:GUIStrings, AudienceSegmentDesc %>"
                            FixedWidth="true" AllowReordering="False" DataCellClientTemplateId="audSegmentDescCT" />
                        <ComponentArt:GridColumn DataField="Sequence" Width="115" runat="server" HeadingText="<%$ Resources:GUIStrings, AudienceSegmentSequence %>"
                            FixedWidth="true" AllowReordering="False" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <ContextMenu EventHandler="grdAudienceSegmentList_onContextMenu" />
                <ItemExternalDrop EventHandler="grdAudienceSegmentList_OnExternalDrop" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="audSegmentNameCT">
                    <span title="## DataItem.getMember('SegmentName').get_text() ##">## DataItem.getMember('SegmentName').get_text()
                        ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="audSegmentDescCT">
                    <span title="## DataItem.getMember('Description').get_text() ##">## DataItem.getMember('Description').get_text()
                        ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdAudienceSegmentListSliderCached">
                    <div class="SliderPopup">
                        <h5>
                            ## DataItem.GetMember(0).Value ##</h5>
                        <p>
                            ## DataItem.GetMember(1).Value ##</p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdAudienceSegmentList.PageCount)
                                ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdAudienceSegmentList.RecordCount)
                                    ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdAudienceSegmentListSlider">
                    <div class="SliderPopup">
                        <p>
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdAudienceSegmentList.PageCount)
                                ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdAudienceSegmentList.RecordCount)
                                    ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdAudienceSegmentListPagination">
                    ## GetGridPaginationInfo(grdAudienceSegmentList) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="dragTemplate">
                    <div class="drag-template">
                        ## DataItem.getMember('SegmentName').get_text() ##</div>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MainMaster.master"
    AutoEventWireup="true" StylesheetTheme="General" Inherits="Administration_ClientIPExclusion"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerAdministrationClientIPExclusions %>"
    CodeBehind="ClientIPExclusion.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var fireEvent = 1;
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1%>'/>"; //added by adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1%>'/>"; //added by adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items%>'/>"; //added by adams


        var item = "";
        var selectedAction = "";
        var removeEmptyRow = false;
        var retFlag = true;
        var chkHideIpactive = "";
        var searchStr = "";
        var clientStatus = "";
        var Make = "";


        function grdIPExclusions_onContextMenu(sender, eventArgs) {
            grdIPExclusions.select(eventArgs.get_item());
            if (grdIPExclusions.EditingDirty == true) {
                cmIPExclusion.Items(0).visible = false;
                cmIPExclusion.Items(1).Visible = false;
                cmIPExclusion.Items(2).Visible = false;
                cmIPExclusion.Items(3).Visible = false;
                cmIPExclusion.Items(4).Visible = false;
                cmIPExclusion.Items(5).Visible = false;
                cmIPExclusion.Items(6).Visible = false;
            }
            else {
                cmIPExclusion.Items(0).Visible = true;
                cmIPExclusion.Items(1).Visible = true;
                cmIPExclusion.Items(2).Visible = true;
                cmIPExclusion.Items(3).Visible = true;
                cmIPExclusion.Items(4).Visible = true;
                cmIPExclusion.Items(5).Visible = true;

                clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                if (clientStatus == "Active") {
                    cmIPExclusion.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                }
                else {
                    cmIPExclusion.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                }
                cmIPExclusion.Items(6).Visible = true;
                cmIPExclusion.Items(6).Visible = true;
                item = eventArgs.get_item();
                if (grdIPExclusions.get_table().getRowCount() == 1) {
                    if (eventArgs.get_item()) {
                        if (eventArgs.get_item().GetMemberAt(6).Text == "") {
                            cmIPExclusion.Items(0).Visible = true;
                            cmIPExclusion.Items(1).Visible = false;
                            cmIPExclusion.Items(2).Visible = false;
                            cmIPExclusion.Items(3).Visible = false;
                            cmIPExclusion.Items(4).Visible = false;
                            cmIPExclusion.Items(5).Visible = false;
                            cmIPExclusion.Items(6).Visible = false;
                        }
                        else {
                            cmIPExclusion.Items(0).Visible = true;
                            cmIPExclusion.Items(1).Visible = true;
                            cmIPExclusion.Items(2).Visible = true;
                            cmIPExclusion.Items(3).Visible = true;
                            cmIPExclusion.Items(4).Visible = true;
                            cmIPExclusion.Items(5).Visible = true;
                            var clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                            //var Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                            if (clientStatus == "Active") {
                                cmIPExclusion.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                            }
                            else {
                                cmIPExclusion.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                            }
                            cmIPExclusion.Items(6).Visible = true;
                        }
                    }
                }
                var evt = eventArgs.get_event();
                cmIPExclusion.showContextMenuAtEvent(evt);
            }
        }

        function cmIPExclusion_ItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            if (grdIPExclusions.get_table().getRowCount() == 1) {
                tempItem = grdIPExclusions.get_table().getRow(0);
                if (tempItem.getMemberAt(6).Text == "" || tempItem.getMemberAt(6).Text == null) {
                    removeEmptyRow = true;
                }
                else {
                    removeEmptyRow = false;
                }
            }
            else {
                removeEmptyRow = false;
            }
            switch (selectedMenu) {
                case "addNewExclusion":
                    if (removeEmptyRow == true) {
                        grdIPExclusions.get_table().ClearData();
                    }
                    item = null;
                    grdIPExclusions.get_table().addEmptyRow(0);
                    item = grdIPExclusions.get_table().getRow(0);
                    grdIPExclusions.select(item, false);
                    item.Selected = true;
                    grdIPExclusions.edit(item);
                    selectedAction = "Insert";
                    break;
                case "editExclusion":
                    if (removeEmptyRow == false) {
                        grdIPExclusions.edit(item);
                        selectedAction = "edit";
                    }
                    break;
                case "deleteExclusion":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, AreYouSureYouWantToDelete %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "delete";
                            grdIPExclusions.edit(item);
                            item.SetValue(5, selectedAction);
                            grdIPExclusions.editComplete();
                            grdIPExclusions.callback();
                        }
                    }
                    else
                        return false;
                    break;
                case "changeExclusionStatus":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, Areyousureyouwanttochangestatus %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "status";
                            grdIPExclusions.edit(item);
                            item.SetValue(5, selectedAction);
                            grdIPExclusions.editComplete();
                            grdIPExclusions.callback();
                        }
                    }
                    else
                        return false;
                    break;
            }
        }


        function CancelClicked() {
            if (selectedAction == '<asp:localize runat="server" text="<%$ Resources:JSMessages, Insert %>"/>') {
                if (grdIPExclusions.Data.length == 1 && (item.getMember(0).get_value() == null || item.getMember(0).get_value() == '')) {
                    grdIPExclusions.get_table().ClearData();
                    grdIPExclusions.editCancel();
                    item = null;
                }
                else {

                    grdIPExclusions.deleteItem(item);
                    grdIPExclusions.editCancel();
                    item = null;
                }
            }
            else {
                grdIPExclusions.editCancel();
            }
            if (removeEmptyRow == true) {

                grdIPExclusions.get_table().addEmptyRow(0);
            }
            selectedAction = "";
        }

        //---------------------------------------textbox-----------------------------
        function setPageLevelValue(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
        }

        function getPageLevelValue(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }

        //----------------------action-----------------------------
        function getPageOperation(control) {
            return [selectedAction, selectedAction];
        }

        //------------------------save-------------------------------
        function SaveRecord() {
            CheckSession();
            if (ValidateInput() == true) {
                var clientName = document.getElementById(txtTitle).value;
                var clientStartingIP = document.getElementById(txtStartingIP).value;
                var clientEndingIP = document.getElementById(txtEndingIP).value;
                var clientDescription = document.getElementById(txtDescription).value;
                var clientId = item.getMember(6).get_value();
                var clientStatus = item.getMember(4).get_value();
                var clientMessage = grdIPExclusions_Callback(clientName, clientStartingIP, clientEndingIP, clientDescription, clientId, clientStatus, selectedAction);
                if (clientMessage == "") {
                    grdIPExclusions.editComplete();
                } else {
                    alert(clientMessage);
                }
            }
        }


        //-------------------------------validation---------------------------
        function ValidateInput() {
            var retValue = true;
            var errorMessage = '';

            //---------------Vaildation for Title--------------------------//
            var ObjIPTitle = document.getElementById(txtTitle);
            var ObjIPTitlebln = true;
            if (Trim(ObjIPTitle.value) != '') {
                if (ObjIPTitle.value.match(/^[a-zA-Z0-9. _]+$/))
                { }
                else {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseuseonlyalphanumericcharactersinthetitle %>'/>" + "\n";
                    ObjIPTitlebln = false;
                    ObjIPTitle.focus();
                    retValue = false;
                }
            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseEnterTheTitle %>"/>' + '\n';
                ObjIPTitlebln = false;
                retValue = false;
            }

            //---------------Vaildation for StartingIP--------------------------//
            var ObjIPStartingIP = document.getElementById(txtStartingIP);
            var ObjIPStartingIPbln = true;
            var theName = "IPaddress";
            if (Trim(ObjIPStartingIP.value) != '') {
                var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
                var ipArray = ObjIPStartingIP.value.match(ipPattern);
                if (ipArray == null) {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidstartingIPaddress %>"/>' + '\n';
                    ObjIPStartingIPbln = false;
                    retValue = false;
                }
                else if (ObjIPStartingIP.value == "0.0.0.0") {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidstartingIPaddress %>"/>' + '\n';
                    ObjIPStartingIPbln = false;
                    retValue = false;
                }
                else {
                    for (i = 0; i < 5; i++) {
                        thisSegment = ipArray[i];
                        if (thisSegment > 255) {
                            errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidstartingIPaddress %>"/>' + '\n';
                            ObjIPStartingIPbln = false;
                            retValue = false;
                            i = 5;
                        }
                        if ((i == 0) && (thisSegment > 255)) {
                            errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidstartingIPaddress %>"/>' + '\n';
                            ObjIPStartingIPbln = false;
                            retValue = false;
                            i = 5;
                        }
                        if ((i == 4) && (thisSegment > 254)) {
                            errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidstartingIPaddress %>"/>' + '\n';
                            ObjIPStartingIPbln = false;
                            retValue = false;
                            i = 5;
                        }
                    }

                    extensionLength = 3;
                    if (errorMessage == "") {
                        retValue = true;
                    }
                    else {
                        retValue = false;
                    }
                }
            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidstartingIPaddress %>"/>' + '\n';
                ObjIPStartingIPbln = false;
                retValue = false;
            }


            //---------------Vaildation for EndingIP--------------------------//

            var ObjIPEndingIP = document.getElementById(txtEndingIP);
            var ObjIPEndingIPbln = true;
            var theName = "IPaddress";
            if (Trim(ObjIPEndingIP.value) != '') {
                var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
                var ipArray = ObjIPEndingIP.value.match(ipPattern);
                if (ipArray == null) {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidendingIPaddress %>"/>' + '\n';
                    ObjIPEndingIPbln = false;
                    retValue = false;
                }
                else if (ObjIPEndingIP.value == "0.0.0.0") {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidendingIPaddress %>"/>' + '\n';
                    ObjIPEndingIPbln = false;
                    retValue = false;
                }
                else {
                    for (i = 0; i < 5; i++) {
                        thisSegment = ipArray[i];
                        if (thisSegment > 255) {
                            errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidendingIPaddress %>"/>' + '\n';
                            ObjIPEndingIPbln = false;
                            i = 5;
                        }
                        if ((i == 0) && (thisSegment > 255)) {
                            errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidendingIPaddress %>"/>' + '\n';
                            ObjIPEndingIPbln = false;
                            retValue = false;
                            i = 5;
                        }
                        if ((i == 4) && (thisSegment > 254)) {
                            errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidendingIPaddress %>"/>' + '\n';
                            ObjIPEndingIPbln = false;
                            retValue = false;
                            i = 5;
                        }
                    }
                    extensionLength = 3;
                    if (errorMessage == "") {
                        retValue = true;
                    }
                    else {
                        retValue = false;
                    }
                }
            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenteravalidendingIPaddress %>"/>' + '\n';
                ObjIPEndingIPbln = false;
                retValue = false;
            }

            //---------------Vaildation for StartingIP greater then EndingIP--------------------------//
            var ObjIPEndingIP = document.getElementById(txtEndingIP);
            var ObjIPStartingIP = document.getElementById(txtStartingIP);
            var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
            var ipArrayStart = ObjIPStartingIP.value.match(ipPattern);
            var ipArrayEnd = ObjIPEndingIP.value.match(ipPattern);
            if (ipArrayEnd != null && ipArrayStart != null) {
                if (ObjIPEndingIP.value != "") {
                    var strIPStartingIP = ObjIPStartingIP.value;
                    var strIPEndingIP = ObjIPEndingIP.value;
                    var strStartIp = '';
                    var strEndIp = '';
                    for (i = 1; i < 5; i++) {
                        theEndIp = ipArrayEnd[i];
                        theStartIP = ipArrayStart[i];
                        if (theStartIP.length < 3 && theStartIP.length >= 2) {
                            theStartIP = '0' + theStartIP;
                        }
                        if (theStartIP.length < 2) {
                            theStartIP = '00' + theStartIP;
                        }
                        strStartIp = strStartIp + theStartIP;
                        if (theEndIp.length < 3 && theEndIp.length >= 2) {
                            theEndIp = '0' + theEndIp;
                        }
                        if (theEndIp.length < 2) {
                            theEndIp = '00' + theEndIp;
                        }
                        strEndIp = strEndIp + theEndIp;
                    }
                    if (parseInt(strEndIp) < parseInt(strStartIp)) {
                        errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, EndingIPaddressmustbegreaterthantheStartingIPaddress %>"/>' + '\n';
                        ObjIPEndingIPbln = false;
                        retValue = false;
                    }
                }
            }

            //---------------Vaildation for Description--------------------------//
            var descriptionStr = document.getElementById(txtDescription);
            var descriptionbln = true;
            if (Trim(descriptionStr.value) != '') {
                var regEx = new RegExp("[\<\>]", "i");
                blnResult = regEx.test(descriptionStr.value)
                if (blnResult == false) {
                }
                else {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromThePageDescription %>"/>' + '\n';
                    descriptionbln = false;
                    retValue = false;
                }
            }
            //                 else
            //                 {
            //                    errorMessage = errorMessage + 'Please enter the description.\n';     
            //                    descriptionbln=false;  
            //                    retValue =  false;  
            //                 }
            if (!retValue) {
                alert(errorMessage);
                if (ObjIPTitlebln == false) {
                    ObjIPTitle.focus();
                    return retValue;
                }
                if (ObjIPStartingIPbln == false) {
                    ObjIPStartingIP.focus();
                    return retValue;
                }
                if (ObjIPEndingIPbln == false) {
                    ObjIPEndingIP.focus();
                    return retValue;
                }
                if (descriptionbln == false) {
                    descriptionStr.focus();
                    return retValue;
                }
            }
            return retValue;
        }

        function hidecheckbox(sender, eventArgs) {
            searchStr = document.getElementById('<%=txtSearchIP.ClientID %>').value;
            chkHideIpactive = document.getElementById('<%=chkHideIp.ClientID %>');

            if (chkHideIpactive.checked == true) {
                if (Trim(searchStr) == "<asp:localize runat='server' text='<%$ Resources:GUIStrings, TypeHereToFilterResults %>'/>") {
                    grdIPExclusions.get_table().Grid.filter("Status = 'Active'")
                }
                else {
                    grdIPExclusions.get_table().Grid.filter("Status = 'Active' AND  Title like  '%" + searchStr + "%'")
                }
                grdIPExclusions.Page(0);
                if (grdIPExclusions.EditingDirty == true) {
                    grdIPExclusions.editCancel();
                    selectedAction = "";
                }
                grdIPExclusions.Render();
            }
            else {
                if (Trim(searchStr) == "<asp:localize runat='server' text='<%$ Resources:GUIStrings, TypeHereToFilterResults %>'/>") {
                    grdIPExclusions.filter("Status = 'Active'  or  Status = 'Inactive'");
                } else {
                    grdIPExclusions.filter("Status = 'Active'  or  Status = 'Inactive' AND  Title like  '%" + searchStr + "%' ");
                }
                grdIPExclusions.Page(0);
                if (grdIPExclusions.EditingDirty == true) {
                    grdIPExclusions.editCancel();
                    selectedAction = "";
                }
                grdIPExclusions.Render();
            }
        }


        /** Method for Key Press Search **/
        function searchIPExclusionsGrid(evt) {
            var retFlag = true;
            var searchStr = document.getElementById('<%=txtSearchIP.ClientID %>').value;
            if (searchStr != '') {
                if (searchStr.match(/^[a-zA-Z0-9. _]+$/)) {
                    if (validKey(evt)) {
                        // grdIPExclusions.Search(searchStr, false);
                        if (chkHideIpactive.checked == true) {
                            grdIPExclusions.filter("Title like '%" + searchStr + "%'  AND  Status = 'Active' ");
                        } else {
                            grdIPExclusions.Search(searchStr, false);
                        }
                        //grdIPExclusions.Filter = "(Status like '%Inactive%') AND (Title like '%searchStr%')"; 
                        if (grdIPExclusions.EditingDirty == true) {
                            grdIPExclusions.editCancel();
                            selectedAction = "";
                        }
                        retFlag = true;
                    }
                }
                else {
                    alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseUseOnlyAlphanumericCharactersInTheSearchTerm       %>'/>");
                    document.getElementById('<%=txtSearchIP.ClientID %>').value = '';
                    retFlag = false;
                }
            }
            else {
                if (chkHideIpactive.checked == true) {
                    grdIPExclusions.get_table().Grid.filter("Status = 'Active'")
                } else {
                    grdIPExclusions.Search("", false);
                }
                grdIPExclusions.Search("", false);
                grdIPExclusions.Page(0);
                if (grdIPExclusions.EditingDirty == true) {
                    grdIPExclusions.editCancel();
                    selectedAction = "";
                }
                grdIPExclusions.Render();
                retFlag = false;
            }
            return retFlag;
        }


        function clearText(objTextbox) {
            if (objTextbox.value == "<asp:localize runat='server' text='<%$ Resources:GUIStrings, TypeHereToFilterResults %>'/>")
                objTextbox.value = "";
        }

        function setText(objTextbox) {
            if (Trim(objTextbox.value) == "")
                objTextbox.value = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, TypeHereToFilterResults %>'/>";
        }



        function SearchHide() {
            var searchStr = document.getElementById('<%=txtSearchIP.ClientID %>').value;
            var chkHideIpactive = document.getElementById('<%=chkHideIp.ClientID %>');
            if (grdIPExclusions.Data.length == 0 && searchStr != null) {
                if (Trim(searchStr) == "" && chkHideIpactive.checked == false) {
                    grdIPExclusions.get_table().addEmptyRow(0);
                }
            }
            if (grdIPExclusions.Data.length == 0 && chkHideIpactive.checked == false && searchStr != null) {
                if (Trim(searchStr) == "<asp:localize runat='server' text='<%$ Resources:GUIStrings, TypeHereToFilterResults %>'/>" && chkHideIpactive.checked == false) {
                    grdIPExclusions.get_table().addEmptyRow(0);
                }
            }
        }


        function grdIPExclusions_onCallbackError(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            alert(eventArgs.get_errorMessage());
            // grdIPExclusions.Callback();
        }

        function grdIPExclusions_onCallbackComplete(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                if (grdIPExclusions.EditingDirty == true) {
                    grdIPExclusions.editComplete();
                    selectedAction = "";
                    alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, ThenewClientIPExclusionwasaddedsuccessfully %>'/>");
                }
            }
            else if (selectedAction == "edit") {
                grdIPExclusions.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, TheClientIPExclusionwasupdatedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Delete %>'/>") {
                item = null;
                if (grdIPExclusions.get_table().getRowCount() <= 0 && grdIPExclusions.PageCount == 0) {
                    grdIPExclusions.get_table().addEmptyRow(0);
                }
                else if (grdIPExclusions.get_table().getRowCount() <= 0) {
                    grdIPExclusions.previousPage();
                }
                grdIPExclusions.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Theselectedsetupwasdeletedsuccessfully %>'/>");
            }
            else if (selectedAction == "status") {
                grdIPExclusions.editComplete();
                selectedAction = "";
            }
            SearchHide();
            grdIPExclusions.editComplete();
        }

        function grdIPExclusions_onSortChange(sender, eventArgs) {
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, edit %>'/>") {
                grdIPExclusions.editCancel();
            }
            selectedAction = "";
        }

        var save = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Save %>"/>';

        var cancel = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Cancel %>"/>';  

    </script>
</asp:Content>
<asp:Content ID="contentClientIPExclusion" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
    <div class="client-ip">
        <div class="page-header clear-fix">
            <h1><%= GUIStrings.ClientIPExclusions %></h1>
        </div>
        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, EditthelistofIPrangestoexcludefromyourreporting %>" /></p>
        <div class="grid-search clear-fix">
            <div class="columns">
            <input type="text" onfocus="clearText(this);" onblur="setText(this);" id="txtSearchIP"
                name="txtSearchIP" value="<%$ Resources:GUIStrings, TypeHereToFilterResults %>"
                onkeyup="searchIPExclusionsGrid(event);" runat="server" />
            </div>
            <div class="columns">
                <input type="checkbox" onclick="hidecheckbox(this)" id="chkHideIp" name="chkHideIp"
                    value="<%$ Resources:GUIStrings, HideInactive %>" runat="server" /><label for="chkHideIp"><%= GUIStrings.HideInactive %></label>
            </div>
        </div>
        <ComponentArt:Grid SkinID="Default" ID="grdIPExclusions" ShowSearchBox="true" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoClientIPExclusions %>"
            AutoPostBackOnSelect="false" Width="100%" PageSize="10" SliderPopupClientTemplateId="grdIPExclusionsSlider"
            SliderPopupCachedClientTemplateId="grdIPExclusionsSliderCached" PagerInfoClientTemplateId="grdIPExclusionsPagination"
            AllowEditing="true" AutoCallBackOnInsert="false" AutoCallBackOnUpdate="true"
            CallbackCachingEnabled="true" CallbackCacheSize="3" AutoCallBackOnDelete="false"
            EditOnClickSelectedItem="false" AutoFocusSearchBox="true" CallbackReloadTemplateScripts="true"
            CallbackReloadTemplates="true">
            <ClientEvents>
                <SortChange EventHandler="grdIPExclusions_onSortChange" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    EditCommandClientTemplateId="EditCommandTemplate" EditFieldCssClass="EditDataField"
                    EditCellCssClass="EditDataCell" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                    <Columns>
                        <ComponentArt:GridColumn Width="353" runat="server" HeadingText="<%$ Resources:GUIStrings, Title %>"
                            HeadingCellCssClass="FirstHeadingCell" DataField="Title" DataCellCssClass="FirstDataCell"
                            SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                            FixedWidth="true" EditCellServerTemplateId="svtTitle" EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, StartingIP %>"
                            DataField="StartingIP" IsSearchable="false" SortedDataCellCssClass="SortedDataCell"
                            AllowReordering="false" FixedWidth="true" EditCellServerTemplateId="svtStartingIP"
                            EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, EndingIP %>"
                            DataField="EndingIP" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" IsSearchable="false" EditCellServerTemplateId="svtEndingIP"
                            EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="310" runat="server" HeadingText="<%$ Resources:GUIStrings, Description %>"
                            DataField="Description" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            IsSearchable="false" FixedWidth="true" EditCellServerTemplateId="svtDescription"
                            EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="122" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>"
                            DataField="Status" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" IsSearchable="false" AllowEditing="False" />
                        <ComponentArt:GridColumn IsSearchable="false" FixedWidth="true" AllowSorting="False"
                            Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                            EditControlType="EditCommand" Align="Center" AllowReordering="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="svtTitle">
                    <Template>
                        <asp:TextBox ID="txtTitle" runat="server" Width="140" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtStartingIP">
                    <Template>
                        <asp:TextBox ID="txtStartingIP" runat="server" Width="80" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtEndingIP">
                    <Template>
                        <asp:TextBox ID="txtEndingIP" runat="server" Width="112" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtDescription">
                    <Template>
                        <asp:TextBox ID="txtDescription" runat="server" Width="300" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtStatus">
                    <Template>
                        <asp:TextBox ID="txtStatus" runat="server" CssClass="textBoxes" Width="208"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="EditCommandTemplate">
                    <a href="javascript:SaveRecord();">
                        <img src="/iapps_images/cm-icon-add.png" border="0" alt="save" title="save" /></a>
                    | <a href="javascript:CancelClicked();">
                        <img src="/iapps_images/cm-icon-delete.png" border="0" alt="cancel" title="cancel" /></a>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdIPExclusionsSliderCached">
                    <div class="SliderPopup">
                        <h5>
                            ## DataItem.GetMember(0).Value ##</h5>
                        <p>
                            ## DataItem.GetMember(1).Value ##</p>
                        <p>
                            ## DataItem.GetMember(2).Value ##</p>
                        <p>
                            ## DataItem.GetMember(3).Value ##</p>
                        <p>
                            ## DataItem.GetMember(4).Value ##</p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdIPExclusions.PageCount)
                                ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdIPExclusions.RecordCount)
                                    ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdIPExclusionsSlider">
                    <div class="SliderPopup">
                        <p>
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdIPExclusions.PageCount)
                                ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdIPExclusions.RecordCount)
                                    ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdIPExclusionsPagination">
                    ## GetGridPaginationInfo(grdIPExclusions) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ContextMenu EventHandler="grdIPExclusions_onContextMenu" />
                <CallbackError EventHandler="grdIPExclusions_onCallbackError" />
                <CallbackComplete EventHandler="grdIPExclusions_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:Grid>
        <ComponentArt:Menu SkinID="ContextMenu" ID="cmIPExclusion" runat="server">
            <Items>
                <ComponentArt:MenuItem ID="addNewExclusion" runat="server" Text="<%$ Resources:GUIStrings, AddNewExclusion %>"
                    Look-LeftIconUrl="cm-icon-add.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="editExclusion" runat="server" Text="<%$ Resources:GUIStrings, EditExclusion %>"
                    Look-LeftIconUrl="cm-icon-edit.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="deleteExclusion" runat="server" Text="<%$ Resources:GUIStrings, DeleteExclusion %>"
                    Look-LeftIconUrl="cm-icon-delete.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="changeExclusionStatus" runat="server" Text="<%$ Resources:GUIStrings, MakeActiveInactive %>">
                </ComponentArt:MenuItem>
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="cmIPExclusion_ItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
    </div>
    <script type="text/javascript">
        window.cart_menu_zindexbase = 99999;
    </script>
</asp:Content>

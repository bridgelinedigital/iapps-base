<%@ Page Language="C#" MasterPageFile="~/Administration/UserMaster.master"
    Theme="General" AutoEventWireup="true" Inherits="AddNewUser" EnableEventValidation="false"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerAdministrationAddNewUser %>"
    CodeBehind="AddNewUser.aspx.cs" %>
    
<%@ Register TagPrefix="UC" TagName="AddNewUser" Src="~/UserControls/General/AddNewUser.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="userContentHolder" runat="server">
    <UC:AddNewUser runat="server" ID="addNewUser" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="userHeaderButtons">
    <asp:Button ID="btnSaveUser" runat="server" OnClick="btnSaveUser_Click" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClientClick="return ValidateEditUser();" />
    <a href="ManageUser.aspx" class="button">
        <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" /></a>
</asp:Content>

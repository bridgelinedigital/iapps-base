<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" StylesheetTheme="General"
    AutoEventWireup="true" Inherits="Administration_AudienceSegments" runat="server"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerAdministrationAudienceSegments %>"
    CodeBehind="AudienceSegments.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="IndexTerms" Src="~/UserControls/General/AssignTagsWithListbox.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var item = null;
        var strSearch = "";

        $(function () {
            $(document).on("click", ".audience-segments .navigation-links a", function () {
                var selectedTabIndex = $(".audience-segments .navigation-links a").index($(this));
                $(this).addClass("active");
                $(this).siblings().removeClass("active");

                $(".audience-segments .tab-contents").children("div").hide();
                $($(".audience-segments .tab-contents").children("div").get(selectedTabIndex)).show();
            });

            toggleVisibility();
        });

        function searchWatchesGrid() {
            //This will check the session whether it is expired or not.
            CheckSession();

            var str = document.getElementById("<%=txtSearchWatches.ClientID%>").value;
            //grdWatches.Search(str, false);
            var strFunction = "grdWatches.Search('" + str + "', false)";
            setTimeout(strFunction, 1000);

            return false;
        }

        var SelectedColumnIndex = null;
        function BeforeCheckChanged(sender, eventArgs) {
            SelectedColumnIndex = eventArgs.get_columnIndex();
        }

        function CheckedChanged(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            var column1 = "";
            var column2 = "";
            var currentItem = eventArgs.get_item();
            var SelectedRowIndex = currentItem.Index;
            if (currentItem) {
                var isChecked = currentItem.Data[SelectedColumnIndex];
                grdWatches.edit(currentItem);
                switch (SelectedColumnIndex) {
                    case 1:
                        if (isChecked) {
                            currentItem.SetValue(1, true, true);
                            currentItem.SetValue(2, false, true);
                            currentItem.SetValue(3, false, true);
                            currentItem.SetValue(4, "0", true);
                        }
                        break;
                    case 2:
                        if (isChecked) {
                            currentItem.SetValue(1, false, true);
                            currentItem.SetValue(2, true, true);
                            currentItem.SetValue(3, false, true);
                            currentItem.SetValue(4, "1", true);
                        }
                        break;
                    case 3:
                        if (isChecked) {
                            currentItem.SetValue(1, false, true);
                            currentItem.SetValue(2, false, true);
                            currentItem.SetValue(3, true, true);
                            currentItem.SetValue(4, "2", true);
                        }
                        break;
                }
                grdWatches.editComplete();
            }
        }

        function changeStatus(ChkList, count, flag) {
            for (i = 0; i < count; i++) {
                ChkList.rows[i].cells[0].childNodes[0].checked = flag;
            }
        }

        function chkAllBrowsers_onclick(sender, eventArgs) {
            var chkAllBrowsers = document.getElementById("<%=chkAllBrowsers.ClientID%>");
            var chkListBrowsers = document.getElementById("<%=chkListBrowsers.ClientID%>");
            checkAllItems(chkAllBrowsers, chkListBrowsers);
        }

        function chkAllOS_onclick(sender, eventArgs) {
            var chkAllOS = document.getElementById("<%=chkAllOS.ClientID%>");
            var chkListOS = document.getElementById("<%=chkListOS.ClientID%>");
            checkAllItems(chkAllOS, chkListOS);
        }

        function chkAllResolution_onclick(sender, eventArgs) {
            var chkAllResolution = document.getElementById("<%=chkAllResolution.ClientID%>");
        var chkListResolution = document.getElementById("<%=chkListResolution.ClientID%>");
        checkAllItems(chkAllResolution, chkListResolution);
    }

    function chkAllUSState_onclick(sender, eventArgs) {
        var chkAllUSState = document.getElementById("<%=chkAllUSState.ClientID%>");
        var chkListUSState = document.getElementById("<%=chkListUSState.ClientID%>");
        checkAllItems(chkAllUSState, chkListUSState);
    }

    function chkAllGeography_onclick(sender, eventArgs) {
        var chkAllGeography = document.getElementById("<%=chkAllGeography.ClientID%>");
        var chkListGeography = document.getElementById("<%=chkListGeography.ClientID%>");
        checkAllItems(chkAllGeography, chkListGeography);
    }

    function checkAllItems(chkGeography, chkAllGeography) {
        if (chkGeography.checked == true) {
            if (browser == "ie") {
                if (chkAllGeography != null) {
                    for (i = 0; i < chkAllGeography.children[0].children.length; i++) {
                        chkAllGeography.children[0].children[i].children[0].children[0].checked = true;
                    }
                }
            }
            else {
                if (chkAllGeography != null) {
                    changeStatus(chkAllGeography, chkAllGeography.rows.length, true);
                }
            }
        }
        else {
            if (browser == "ie") {
                if (chkAllGeography != null) {
                    for (i = 0; i < chkAllGeography.children[0].children.length; i++) {
                        chkAllGeography.children[0].children[i].children[0].children[0].checked = false;
                    }
                }
            }
            else {
                if (chkAllGeography != null) {
                    changeStatus(chkAllGeography, chkAllGeography.rows.length, false);
                }
            }
        }
    }

    function segmentValidation(sender, eventArgs) {
        //This will check the session whether it is expired or not.
        CheckSession();

        var segmentName = document.getElementById("<%=txtSegmentName.ClientID%>");
        if (ValidationRequiredFunction(segmentName.value, "SegmentNameRequired") == false) {
            return false;
        }
        var segmentDescription = document.getElementById("<%=txtSegmentDescription.ClientID%>");
        if (ValidationRequiredFunction(segmentDescription.value, "SegmentDescriptionRequired") == false) {
            return false;
        }
        if (ValidationSpecailCharacterFunction(segmentName.value, "SpecialCharacterValidation", "<asp:localize runat='server' text='<%$ Resources:JSMessages, SegmentName %>'/>") == false) {
            return false;
        }
        if (ValidationSpecailCharacterFunction(segmentDescription.value, "SpecialCharacterValidation", "<asp:localize runat='server' text='<%$ Resources:JSMessages, SegmentDescription %>'/>") == false) {
            return false;
        }

    }
    function SegmentValidatorScript(sender, eventArgs) {
        var segmentName = document.getElementById("<%=txtSegmentName.ClientID%>");
        var regularEval = new RegExp("[\<\>!\"#$%&'()*+,-./:;?@[\\\]_`{|}~]", "i");
        var blnReportValidation = regularEval.test(segmentName.value);
        if (blnReportValidation) {
            eventArgs.IsValid = false;
        }
        else {
            eventArgs.IsValid = true;
        }
    }

    function descriptionValidatorScript(sender, eventArgs) {
        var segmentDescription = document.getElementById("<%=txtSegmentDescription.ClientID%>");
        // = new RegExp("[\<\>!\"#$%&'()*+,-./:;?@[\\\]_`{|}~]","i");
        var regularEval = new RegExp("[\<\>]", "i");
        var blnReportValidation = regularEval.test(segmentDescription.value);
        if (blnReportValidation) {
            eventArgs.IsValid = false;
        }
        else {
            eventArgs.IsValid = true;
        }
    }

    function NoOfDaysValidatorScript(sender, eventArgs) {
        var visitDays = document.getElementById("<%=txtLastVisitedDays.ClientID%>");

        var len = '1000';
        if (Trim(visitDays.value) != '') {
            if (visitDays.value.match(/^[0-9]+$/)) {
                if (visitDays.value > parseInt(len)) {
                    eventArgs.IsValid = false;
                    return;
                }
            }
            else {
                eventArgs.IsValid = false;
                return;
            }
        }

        eventArgs.IsValid = true;
    }


    function OnSegmentSave() {
        if (1 == 2) {
            var txtPagesVisited = $("input[id$='txtPagesVisited']").val();
            var lstPagesVisited = $("#lstVisitedPages").children().length;
            var ddlPagesVisited = $("#ddlTotalPagesVisitedOperator").val();
            if (lstPagesVisited > 0) {
                if (txtPagesVisited == "") {
                    alert("<%= JSMessages.PleaseEnterNoOfPagesVisited %>");
                    return false;
                }
                else if (ddlPagesVisited == "-1") {
                    alert("<%= JSMessages.PleaseEnterNoOfPagesVisitedCondition %>");
                        return false;
                    }
            }
            else if (txtPagesVisited != "") {
                if (ddlPagesVisited == "-1") {
                    alert("<%= JSMessages.PleaseEnterNoOfPagesVisitedCondition %>");
                    return false;
                }
                else if (lstPagesVisited == 0) {
                    alert("<%= GUIStrings.ASPageVisitedValidationMsg %>");
                            return false;
                        }
                }
        }
        if (typeof (Page_ClientValidate) == 'function')
            Page_ClientValidate('SegmentSave');

        if (referrerURLValidation() == false)
            return false;

        return (Page_IsValid);
    }

    function referrerURLValidation() {
        var input = document.getElementById("<%=txtReferrerUrls.ClientID%>");
        var lines = input.value.split('\n');
        for (var i = 0; i < lines.length; i++) {
            var line = lines[i];
            if (line != '') {
                var result = is_valid_url(line);
                if (result == false) {
                    alert("<%= JSMessages.ASPleaseEnterValidRefURL %>" + line + ".");
                    return false;
                }
            }
        }
        return true;
    }

    function is_valid_url(URL) {
        return /^(ftp|https?):\/\/+(www\.)?[a-z0-9\-\.]{3,}\.[a-z]{3}$/.test(URL);
    }

    function grdWatch_OnSort(sender, eventArgs) {
        //This will check the session whether it is expired or not.
        CheckSession();
    }

    function searchData() {
        if (strSearch != "" && strSearch != "<asp:localize runat='server' text='<%$ Resources:JSMessages, TypeHereToFilterResults %>'/>") {

            var strFunction = "grdWatches.Search('" + strSearch + "', false)";
            setTimeout(strFunction, 1000);
            grdWatches.Page(0);
        }
    }

    function grdWatch_CallbackCompleted(sender, eventArgs) {
        searchData();
        if (grdWatches.Data.length == 0) {
            grdWatches.EmptyGridText;
        }
        grdWatches.editComplete();
        grdWatches.render();
        strSearch = "";
    }


    function ListClicked(sender, eventArgs) {
        if (browser == "ie") {
            if (eventArgs.srcElement) {
                if (eventArgs.srcElement.checked == false) {
                    switch (sender.id) {
                        case '<%=chkListBrowsers.ClientID%>':
                            document.getElementById('<%=chkAllBrowsers.ClientID%>').checked = false;
                            break;
                        case '<%=chkListOS.ClientID%>':
                            document.getElementById('<%=chkAllOS.ClientID%>').checked = false;
                            break;
                        case '<%=chkListResolution.ClientID%>':
                            document.getElementById('<%=chkAllResolution.ClientID%>').checked = false;
                                break;
                            case '<%=chkListGeography.ClientID%>':
                            document.getElementById('<%=chkAllGeography.ClientID%>').checked = false;

                                break;
                        }
                    }
                    else {
                        if (sender.id == '<%=chkListGeography.ClientID%>') {
                        // if selected region not US then uncheck all and hide the StatesList box
                    }
                }
            }
        }
        else {
            if (eventArgs.target) {
                if (eventArgs.target.checked == false) {
                    switch (sender.id) {
                        case '<%=chkListBrowsers.ClientID%>':
                            document.getElementById('<%=chkAllBrowsers.ClientID%>').checked = false;
                            break;
                        case '<%=chkListOS.ClientID%>':
                            document.getElementById('<%=chkAllOS.ClientID%>').checked = false;
                            break;
                        case '<%=chkListResolution.ClientID%>':
                            document.getElementById('<%=chkAllResolution.ClientID%>').checked = false;
                                break;
                            case '<%=chkListGeography.ClientID%>':
                            document.getElementById('<%=chkAllGeography.ClientID%>').checked = false;
                                break;
                        }
                    }
                }
            }
        }
        //attach click event for all the country checkboxes
        $(document).ready(function () {
            AttachClickHandler();
        });
        function AttachClickHandler() {
            $('#<%=chkListGeography.ClientID%> :checkbox').click(function () {
                if ($(this).next().text() == 'United States') {
                    if ($(this).is(':checked') == true) {
                        $('#<%=chkListUSState.ClientID%> :checkbox').each(function () {
                            $(this).attr('disabled', false);
                        });
                    }
                    else {
                        $('#<%=chkListUSState.ClientID%> :checkbox').each(function () {
                            $(this).attr('checked', false);
                            $(this).attr('disabled', true);
                        });
                    }
                }
            });
        }
        function DisableStatesList() {
            if ($('#<%=chkListGeography.ClientID%> :checkbox').filter(function () {
                return $(this).next().text() == 'United States';
            }).is(':checked') == false) {
                $('#<%=chkListUSState.ClientID%> :checkbox').each(function () {
                    $(this).attr('checked', false);
                    $(this).attr('disabled', true);
                });
            }
        }

        function toggleVisibility() {

            var control = document.getElementById('DesktopSetting');

            if (document.getElementById("<%=ddlDeviceTypes.ClientID %>").value == "0")
            control.style.visibility = "visible";
        else
            control.style.visibility = "hidden";

    }
   
    var entryPagesJson = [];
    var visitedPagesJson = [];
    function OpenSelectPages(type) {
        var customAttributes = {};

        if (type == 1) {
            entryPagesJson = [];
            $("#lstEntryPages").children("option").each(function () {
                entryPagesJson.push({ "Id": $(this).val(), "Title": $(this).text(), "ObjectTypeId": 8 });
            });
            customAttributes["ListItemsJson"] = JSON.stringify(entryPagesJson);
        }
        else {
            visitedPagesJson = [];
            $("#lstVisitedPages").children("option").each(function () {
                visitedPagesJson.push({ "Id": $(this).val(), "Title": $(this).text(), "ObjectTypeId": 8 });
            });
            customAttributes["ListItemsJson"] = JSON.stringify(visitedPagesJson);
        }

        OpeniAppsAdminPopup("SelectManualListPopup", "ObjectTypeId=8", type == 1 ? "UpdateEntryPages" : "UpdatePagesVisited", customAttributes);

        return false;
    }

    function UpdateEntryPages() {
        entryPagesJson = [];
        if (popupActionsJson.CustomAttributes["ListItemsJson"] != "")
            entryPagesJson = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);

        $("#lstEntryPages").children().remove();
        $.each(entryPagesJson, function () {
            $("#lstEntryPages").append(stringformat("<option value='{0}'>{1}</option>", this.Id.toString(), this.Title));
        });

        $("#hdnEntryPages").val(JSON.stringify(entryPagesJson));
    }

    function UpdatePagesVisited() {
        visitedPagesJson = [];
        if (popupActionsJson.CustomAttributes["ListItemsJson"] != "")
            visitedPagesJson = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);

        $("#lstVisitedPages").children().remove();
        $.each(visitedPagesJson, function () {
            $("#lstVisitedPages").append(stringformat("<option value='{0}'>{1}</option>", this.Id.toString(), this.Title));
        });

        $("#hdnVisitedPages").val(JSON.stringify(visitedPagesJson));
    }
    </script>
    <style type="text/css">
        #<%=grdWatches.ClientID%>_VerticalScrollDiv
        {
        	height: 400px !important;
            overflow-x: hidden;
            overflow-y: scroll;
        }
    </style>
</asp:Content>
<asp:Content ID="contentAudienceSegments" ContentPlaceHolderID="mainPlaceHolder"
    runat="Server">
    <div class="audience-segments">
        <div class="page-header clear-fix">
            <h2>
                <%= GUIStrings.AddEditAudienceSegments %></h2>
            <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
                Tooltop="<%$ Resources:GUIStrings, Save %>" OnClientClick="return OnSegmentSave();" ValidationGroup="SegmentSave" />
            <asp:HyperLink ID="hplCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                CssClass="button" NavigateUrl="~/Administration/AudienceSegmentList.aspx" />
        </div>
        <div class="navigation-bar clear-fix">
            <div class="navigation-links clear-fix">
                <asp:HyperLink ID="hplGeneralInformation" runat="server" Text="<%$ Resources:GUIStrings, GeneralInformation %>"
                    ToolTip="<%$ Resources:GUIStrings, GeneralInformation %>" NavigateUrl="#" CssClass="active" />
                <asp:HyperLink ID="hplVisitDetails" runat="server" Text="<%$ Resources:GUIStrings, VisitDetails %>"
                    ToolTip="<%$ Resources:GUIStrings, VisitDetails %>" NavigateUrl="#" />
                <asp:HyperLink ID="hplReferringSites" runat="server" Text="<%$ Resources:GUIStrings, ReferringSites %>"
                    ToolTip="<%$ Resources:GUIStrings, ReferringSites %>" NavigateUrl="#" />
                <asp:HyperLink ID="hplWatchedEvents" runat="server" Text="<%$ Resources:GUIStrings, WatchedEvents %>"
                    ToolTip="<%$ Resources:GUIStrings, WatchedEvents %>" NavigateUrl="#" />
            </div>
        </div>
        <div class="tab-contents">
            <div class="tab1">
                <div class="form-section clear-fix">
                    <div class="columns">
                        <div class="form-row">
                            <label class="form-label">
                                <%= GUIStrings.SegmentName %><span class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtSegmentName" runat="server" CssClass="textBoxes" Width="300" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <%= GUIStrings.Description %><span class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtSegmentDescription" runat="server" CssClass="textBoxes" Width="300"
                                    TextMode="Multiline" Height="73" />
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                        <UC:IndexTerms ID="indexs" runat="server" ListBoxWidth="307" />
                    </div>
                </div>
                <div class="form-section clear-fix">
                    <h3>
                        <%= GUIStrings.Technology%></h3>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.JavaScriptEnabled %></label>
                        <div class="form-value">
                            <asp:RadioButtonList RepeatDirection="Horizontal" ID="rbListJSEnabled" runat="server">
                                <asp:ListItem id="JS1" runat="server" Text="<%$ Resources:GUIStrings, Yes %>" Value="1"></asp:ListItem>
                                <asp:ListItem id="JS2" runat="server" Text="<%$ Resources:GUIStrings, No %>" Value="0"></asp:ListItem>
                                <asp:ListItem id="JS3" runat="server" Text="<%$ Resources:GUIStrings, NA %>" Value="2"
                                    Selected="true"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.FlashEnabled %></label>
                        <div class="form-value">
                            <asp:RadioButtonList RepeatDirection="Horizontal" ID="rbListFlashEnabled" runat="server">
                                <asp:ListItem id="Flash1" runat="server" Text="<%$ Resources:GUIStrings, Yes %>"
                                    Value="1"></asp:ListItem>
                                <asp:ListItem id="Flash2" runat="server" Text="<%$ Resources:GUIStrings, No %>" Value="0"></asp:ListItem>
                                <asp:ListItem id="Flash3" runat="server" Text="<%$ Resources:GUIStrings, NA %>" Value="2"
                                    Selected="true"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.DeviceType %></label>
                        <div class="form-value">
                            <asp:DropDownList ID="ddlDeviceTypes" onChange="toggleVisibility()" runat="server"
                                Width="200">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Desktop %>" Value="0"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Smartphone %>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Tablet %>" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="DesktopSetting" class="form-row">
                        <div class="three-columns">
                            <div class="form-row">
                                <label class="form-label">
                                    <span>
                                        <%= GUIStrings.Browsers %></span>
                                    <asp:CheckBox ID="chkAllBrowsers" runat="server" Text="<%$ Resources:GUIStrings, All %>"
                                        class="checkbox" onclick="chkAllBrowsers_onclick()" />
                                </label>
                                <div class="form-value scrolling-div">
                                    <asp:CheckBoxList RepeatDirection="Vertical" ID="chkListBrowsers" onclick="ListClicked(this,event)"
                                        runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="three-columns">
                            <div class="form-row">
                                <label class="form-label">
                                    <span>
                                        <%= GUIStrings.OperatingSystems %></span>
                                    <asp:CheckBox ID="chkAllOS" runat="server" Text="<%$ Resources:GUIStrings, All %>"
                                        class="checkbox" onclick="chkAllOS_onclick();" />
                                </label>
                                <div class="form-value scrolling-div">
                                    <asp:CheckBoxList RepeatDirection="Vertical" ID="chkListOS" runat="server" onclick="ListClicked(this,event)" />
                                </div>
                            </div>
                        </div>
                        <div class="three-columns">
                            <div class="form-row">
                                <label class="form-label">
                                    <span>
                                        <%= GUIStrings.Resolution %></span>
                                    <asp:CheckBox ID="chkAllResolution" runat="server" Text="<%$ Resources:GUIStrings, All %>"
                                        class="checkbox" onclick="chkAllResolution_onclick();" />
                                </label>
                                <div class="form-value scrolling-div">
                                    <asp:CheckBoxList RepeatDirection="Vertical" ID="chkListResolution" runat="server"
                                        onclick="ListClicked(this,event)" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab2" style="display: none;">
                <div class="form-section">
                    <h3>
                        <%= GUIStrings.Visitors%></h3>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.ofdayssincelastvisited%></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtLastVisitedDays" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="three-columns">
                            <div class="form-row">
                                <label class="form-label">
                                    <span>
                                        <%= GUIStrings.Geography %></span>
                                    <asp:CheckBox ID="chkAllGeography" runat="server" Text="<%$ Resources:GUIStrings, All %>"
                                        class="checkbox" onclick="chkAllGeography_onclick();" />
                                </label>
                                <div class="form-value scrolling-div">
                                    <asp:CheckBoxList RepeatDirection="Vertical" ID="chkListGeography" runat="server"
                                        onclick="ListClicked(this,event)" />
                                </div>
                            </div>
                        </div>
                        <div class="three-columns">
                            <div class="form-row">
                                <label class="form-label">
                                    <span>
                                        <%= GUIStrings.States %></span>
                                    <asp:CheckBox ID="chkAllUSState" runat="server" Text="<%$ Resources:GUIStrings, All %>"
                                        class="checkbox" onclick="chkAllUSState_onclick();" />
                                </label>
                                <div class="form-value scrolling-div">
                                    <asp:CheckBoxList RepeatDirection="Vertical" ID="chkListUSState" runat="server" onclick="ListClicked(this,event)" />
                                </div>
                            </div>
                        </div>
                        <div class="three-columns">
                            <div class="form-row">
                                <label class="form-label clear-fix" style="margin-bottom: 17px;">
                                    <%= GUIStrings.VisitorType %>
                                </label>
                                <div class="form-value scrolling-div">
                                    <asp:RadioButtonList group="visitor" RepeatDirection="Vertical" ID="rbListVisitorType"
                                        runat="server">
                                        <asp:ListItem group="visitor" id="visitor1" runat="server" Text="<%$ Resources:GUIStrings, NotApplicable %>"
                                            Value="0" Selected="true"></asp:ListItem>
                                        <asp:ListItem group="visitor" id="visitor2" runat="server" Text="<%$ Resources:GUIStrings, NewVisitors %>"
                                            Value="1"></asp:ListItem>
                                        <asp:ListItem group="visitor" id="visitor3" runat="server" Text="<%$ Resources:GUIStrings, RepeatVisitors %>"
                                            Value="2"></asp:ListItem>
                                        <asp:ListItem group="visitor" id="visitor4" runat="server" Text="<%$ Resources:GUIStrings, Both %>"
                                            Value="3"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-section">
                    <h3>
                        <%= GUIStrings.PagesVisited%></h3>
                    <div class="box-content clear-fix">
                        <div class="form-row">
                            <label class="form-label">
                                <%= GUIStrings.Pages%><br />
                                <br />
                                <a href="#" onclick="return OpenSelectPages(2);">
                                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SelectPages %>" /></a>
                            </label>
                            <div class="form-value multi-column-value">
                                <asp:HiddenField ID="hdnVisitedPages" runat="server" ClientIDMode="Static" />
                                <asp:ListBox ID="lstVisitedPages" runat="server" ClientIDMode="Static" Width="260" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                &nbsp;</label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlTotalPagesVisitedOperator" runat="server" ClientIDMode="Static" Visible="false">
                                    <asp:ListItem Text="(Select)" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="is < than" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="is > than" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="is = to" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtPagesVisited" runat="server" Width="80" Visible="false" />
                                <asp:RadioButtonList ID="rbtnPagesVisited" runat="server">
                                    <asp:ListItem Value="1" Text="All the above pages should be visited atleast once." Selected="True" />
                                    <asp:ListItem Value="0" Text="Any of the above page should be visited atleast once" />
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="box-content clear-fix">
                            <label class="form-label">
                                <%= GUIStrings.EntryPageIs%><br />
                                <br />
                                <a href="#" onclick="return OpenSelectPages(1);">
                                    <asp:Localize ID="lcl1" runat="server" Text="<%$ Resources:GUIStrings, SelectPages %>" />
                                </a>
                            </label>
                            <div class="form-value multi-column-value">
                                <asp:HiddenField ID="hdnEntryPages" runat="server" ClientIDMode="Static" />
                                <asp:ListBox ID="lstEntryPages" runat="server" ClientIDMode="Static" Width="260" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab3" style="display: none;">
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.ReferrerIs %></label>
                    <div class="form-value">
                        <asp:RadioButtonList ID="rbtnReferrer" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="All" Value="0" Selected="True" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, Paid %>" Value="1" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, NonPaid %>" Value="2" />
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.EnterURLS %><br />
                        <em>
                            <%= GUIStrings.EnterURLSHelp %></em></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtReferrerUrls" runat="server" CssClass="textBoxes" TextMode="MultiLine"
                            Width="300" Height="100" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.lblSearchKeyword%><br />
                        <em>
                            <%= GUIStrings.KeywordsHelp %></em></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtSearchKeyword" TextMode="MultiLine" Height="100" Width="300"
                            runat="server" />
                    </div>
                </div>
            </div>
            <div class="tab4" style="display: none;">
                <div class="grid-utility">
                    <asp:TextBox ID="txtSearchWatches" runat="server" ToolTip="<%$ Resources:GUIStrings, TypeHereToFilterResults %>"
                        CssClass="textBoxes" Width="240" />
                    <input type="button" class="button" value="<%= GUIStrings.Search %>" onclick="return searchWatchesGrid();" />
                </div>
                <ComponentArt:Grid SkinID="Default" ID="grdWatches" EditOnClickSelectedItem="false"
                    ShowHeader="False" ShowFooter="false" Width="100%" ShowSearchBox="false" runat="server"
                    RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoAudiencesegmentwatchestodisplay %>"
                    AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true" AllowMultipleSelect="false"
                    CallbackCachingEnabled="true" AllowVerticalScrolling="true">
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="WatchId" ShowTableHeading="false" ShowSelectorCells="false"
                            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                            AllowSorting="true">
                            <Columns>
                                <ComponentArt:GridColumn Width="588" runat="server" HeadingText="<%$ Resources:GUIStrings, WatchTitle %>"
                                    HeadingCellCssClass="FirstHeadingCell" DataField="Name" DataCellCssClass="FirstDataCell"
                                    SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                                    FixedWidth="true" AllowSorting="true" />
                                <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, NotApplicable %>"
                                    AllowEditing="True" IsSearchable="false" SortedDataCellCssClass="SortedDataCell"
                                    AllowReordering="false" FixedWidth="true" Align="Center" ColumnType="CheckBox"
                                    DataField="NotApplicable" AllowSorting="False" DataCellCssClass="chkWatch" />
                                <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, TriggeredWatch %>"
                                    AllowEditing="True" IsSearchable="false" SortedDataCellCssClass="SortedDataCell"
                                    AllowReordering="false" FixedWidth="true" Align="Center" ColumnType="CheckBox"
                                    DataField="TriggeredWatch" AllowSorting="False" DataCellCssClass="chkWatch" />
                                <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, DidNotTriggerWatch %>"
                                    AllowEditing="True" IsSearchable="false" SortedDataCellCssClass="SortedDataCell"
                                    AllowReordering="false" FixedWidth="true" Align="Center" ColumnType="CheckBox"
                                    DataField="NotTriggeredWatch" AllowSorting="False" DataCellCssClass="chkWatch"
                                    HeadingCellCssClass="LastHeadingCell" />
                                <ComponentArt:GridColumn Width="30" runat="server" HeadingText="<%$ Resources:GUIStrings, WatchType %>"
                                    HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                                    IsSearchable="false" AllowReordering="false" FixedWidth="true" Visible="false"
                                    DataField="WatchType" AllowSorting="False" />
                                <ComponentArt:GridColumn DataField="WatchId" Visible="false" AllowSorting="False"
                                    IsSearchable="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="NAWatchTemplate">
                            <input type="radio" id="chkNA" runat="server" />
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="TWWatchTemplate">
                            <input type="radio" id="chkTW" runat="server" />
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="DNWatchTemplate">
                            <input type="radio" id="chkDN" runat="server" />
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                    <ClientEvents>
                        <ItemCheckChange EventHandler="CheckedChanged"></ItemCheckChange>
                        <ItemBeforeCheckChange EventHandler="BeforeCheckChanged" />
                        <SortChange EventHandler="grdWatch_OnSort" />
                        <CallbackComplete EventHandler="grdWatch_CallbackCompleted" />
                    </ClientEvents>
                </ComponentArt:Grid>
            </div>
        </div>
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true" ValidationGroup="SegmentSave" />
    <asp:RequiredFieldValidator ID="segmentNameValidator" ControlToValidate="txtSegmentName" ValidationGroup="SegmentSave"
        ErrorMessage="<%$ Resources: JSMessages, SegmentNameRequired %>"
        Display="None" runat="server"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="descriptionValidator" ControlToValidate="txtSegmentDescription" ValidationGroup="SegmentSave"
        ErrorMessage="<%$ Resources: JSMessages, SegmentDescriptionRequired %>"
        Display="None" runat="server"></asp:RequiredFieldValidator>
    <asp:CustomValidator ID="segmentCustomValidator" ClientValidationFunction="SegmentValidatorScript" ValidationGroup="SegmentSave"
        runat="server" ControlToValidate="txtSegmentName" ErrorMessage="<%$ Resources: JSMessages, SpecialcharactersarenotallowedinSegmentNamePleaseremovethem %>"
        Display="None"></asp:CustomValidator>
    <asp:CustomValidator ID="descriptionCustomValidator" ClientValidationFunction="descriptionValidatorScript" ValidationGroup="SegmentSave"
        runat="server" ControlToValidate="txtSegmentDescription" ErrorMessage="<%$ Resources: JSMessages, SpecialcharactersandarenotallowedinSegmentDescriptionPleaseremovethem %>"
        Display="None"></asp:CustomValidator>
    <asp:CustomValidator ID="NoOfDaysCValidator" ClientValidationFunction="NoOfDaysValidatorScript" ValidationGroup="SegmentSave"
        runat="server" ControlToValidate="txtLastVisitedDays" ErrorMessage="<%$ Resources: JSMessages, Pleaseuseonlynumericcharactersandbelow1000intheofdayssincelastvisited %>"
        Display="None"></asp:CustomValidator>
</asp:Content>

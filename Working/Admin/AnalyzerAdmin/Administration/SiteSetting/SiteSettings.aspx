﻿<%@ Page Language="C#" MasterPageFile="~/Administration/SiteSetting/SiteSettingsMaster.master" AutoEventWireup="true"
    ValidateRequest="false" CodeBehind="SiteSettings.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.SiteSettings"
    StylesheetTheme="General" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerDeveloperConfigurations %>" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="cphHead">
</asp:Content>
<asp:Content ID="body" runat="server" ContentPlaceHolderID="cphContent">
    <div class="site-settings">
        <div class="top-button-row">
            <asp:Button runat="server" ID="btnSave" Text="Save" ToolTip="Save" CssClass="primarybutton"
                ValidationGroup="callback" OnClick="btnSave_Onclick" />
            <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
                ValidationGroup="callback" />
        </div>
        <p style="text-align: right; margin-bottom: 10px;">
            <em>
                <asp:Literal ID="ltCacheTime" runat="server" /></em>
        </p>
        <div class="form-row">
            <label class="form-label">
                Admin Email:</label><div class="form-value">
                    <asp:TextBox ID="txtAdminEmail" runat="server" CssClass="textBoxes"
                        Width="860"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                Analytics Product Id:</label><div class="form-value">
                    <asp:TextBox ID="txtAnalyticsProductId" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                AnalyticsAdminVDName:</label><div class="form-value">
                    <asp:TextBox ID="txtAnalyticsAdminVDName" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                AnalyticsProduct.Key:</label><div class="form-value">
                    <asp:TextBox ID="txtAnalyticsProductKey" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                Cache.Email:</label><div class="form-value">
                    <asp:TextBox ID="txtcacheemail" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                CMS Product Id:</label><div class="form-value">
                    <asp:TextBox ID="txtCMSProductId" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                CMS Product.Key:</label><div class="form-value">
                    <asp:TextBox ID="txtCMSProductKey" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                CMSAdminVDName:</label><div class="form-value">
                    <asp:TextBox ID="txtCMSAdminVDName" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                CommonLoginVD:</label><div class="form-value">
                    <asp:TextBox ID="txtCommonLoginVD" runat="server" CssClass="textBoxes" Width="860"
                        ReadOnly="false"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                CommerceAdminVDName:</label><div class="form-value">
                    <asp:TextBox ID="txteCommerceAdminVDName" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                CommerceProduct.Key:</label><div class="form-value">
                    <asp:TextBox ID="txteCommerceProductKey" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                Commerce Product Id:</label><div class="form-value">
                    <asp:TextBox ID="txtCommerceProductId" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                ComponentArtScriptControls:</label><div class="form-value">
                    <asp:TextBox ID="txtComponentArtScriptControls" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                Context MenuXml:</label><div class="form-value">
                    <asp:TextBox ID="txtContextMenuXml" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                EventLog Application Key:</label><div class="form-value">
                    <asp:TextBox ID="txtFrontEndEventLogApplicationKey" runat="server" CssClass="textBoxes"
                        Width="860" ReadOnly="false"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                FilesToOpenInBrowser:</label><div class="form-value">
                    <asp:TextBox ID="txtFilesToOpenInBrowser" runat="server" CssClass="textBoxes" Width="860"
                        ReadOnly="false"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                iAPPS_Version:</label><div class="form-value">
                    <asp:TextBox ID="txtiAPPS_Version" runat="server" CssClass="textBoxes  disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                iApps.DateFormat:</label><div class="form-value">
                    <asp:TextBox ID="txtiAppsDateFormat" runat="server" CssClass="textBoxes" Width="860"
                        ReadOnly="false"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                IAppsSystemUser Id:</label><div class="form-value">
                    <asp:TextBox ID="txtiAPPSUser" runat="server" CssClass="textBoxes" Width="860" ReadOnly="false"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="retxtiAPPSUser" runat="server" ControlToValidate="txtiAPPSUser"
                        ValidationExpression="^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                        ValidationGroup="callback" ErrorMessage="Please enter a valid Guid in 'iAPPS System User' field"
                        Text="*" />
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                iAppsTimeoutInMinutes:</label><div class="form-value">
                    <asp:TextBox ID="txtiAppsTimeoutInMinutes" runat="server" CssClass="textBoxes" Width="860"
                        ReadOnly="false"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                Invisible TreeViewNode CssClass:</label><div class="form-value">
                    <asp:TextBox ID="txtInvisibleTreeViewNodeCssClass" runat="server" CssClass="textBoxes"
                        Width="860" ReadOnly="false"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                Invisible Selected NodeCssClass:</label><div class="form-value">
                    <asp:TextBox ID="txtInvisibleSelectedNodeCssClass" runat="server" CssClass="textBoxes"
                        Width="860" ReadOnly="false"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                ISYSBinPath:</label><div class="form-value">
                    <asp:TextBox ID="txtISYSBinPath" runat="server" CssClass="textBoxes  disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                ISYSLicenseKey:</label><div class="form-value">
                    <asp:TextBox ID="txtISYSLicenseKey" runat="server" CssClass="textBoxes  disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                ISYSProductSearchIndex:</label><div class="form-value">
                    <asp:TextBox ID="txtISYSProductSearchIndex" runat="server" CssClass="textBoxes" Width="860"
                        ReadOnly="false"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                JQueryGoogleCDN for Admin:</label><div class="form-value">
                    <asp:TextBox ID="txtjQueryGoogleCDN" runat="server" CssClass="textBoxes  disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                MarketierAdminVD:</label><div class="form-value">
                    <asp:TextBox ID="txtMarketierAdminVD" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                Marketier Product Id:</label><div class="form-value">
                    <asp:TextBox ID="txtMarketierProductId" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row" style="display: none;">
            <label class="form-label">
                MarketierProduct.Key:</label><div class="form-value">
                    <asp:TextBox ID="txtMarketierProductKey" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                PageAccessXml:</label><div class="form-value">
                    <asp:TextBox ID="txtPageAccessXml" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                PublicSite.URL:</label><div class="form-value">
                    <asp:TextBox ID="txtPublicSiteURL" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                Render Controls Attributes In Live:</label><div class="form-value">
                    <asp:DropDownList ID="ddlRenderControlsAttributesInLive" runat="server">
                        <asp:ListItem Text="true" Value="true" />
                        <asp:ListItem Text="false" Value="false" />
                    </asp:DropDownList>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                Render iAPPS SEO Description:</label><div class="form-value">
                    <asp:DropDownList ID="ddlRenderiAPPSSEODescription" runat="server">
                        <asp:ListItem Text="true" Value="true" />
                        <asp:ListItem Text="false" Value="false" />
                    </asp:DropDownList>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                Site Id:</label><div class="form-value">
                    <asp:TextBox ID="txtSiteId" runat="server" CssClass="textBoxes disabledText disabled"
                        Width="860" ReadOnly="true"></asp:TextBox>
                </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                Use Cache:</label><div class="form-value">
                    <asp:DropDownList ID="ddlUseCache" runat="server">
                        <asp:ListItem Text="true" Value="true" />
                        <asp:ListItem Text="false" Value="false" />
                    </asp:DropDownList>
                    <asp:LinkButton ID="lbRefreshCache" Text="Refresh Analyzer Cache" runat="server" OnClick="lbRefreshCache_Click" />
                </div>
        </div>
    </div>
</asp:Content>

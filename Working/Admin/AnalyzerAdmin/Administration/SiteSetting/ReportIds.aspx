﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSAnalyzerDeveloperConfigurations %>" Language="C#"
    MasterPageFile="~/Administration/SiteSetting/SiteSettingsMaster.master" AutoEventWireup="true"
    CodeBehind="ReportIds.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.ReportIds"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
<div class="site-settings">
    <div class="form-row">
        <label class="form-label">AbondanmentReportId:</label>
        <div class="form-value">
            <asp:TextBox ID="txtAbondanmentReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            BrowserReportId:</label><div class="form-value">
                <asp:TextBox ID="txtBrowserReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            DirectReferrerReportId:</label><div class="form-value">
                <asp:TextBox ID="txtDirectReferrerReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            EntryPagesReportId:</label><div class="form-value">
                <asp:TextBox ID="txtEntryPagesReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            ExitPagesReportId:</label><div class="form-value">
                <asp:TextBox ID="txtExitPagesReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            ExternalReferrerDetailReports:</label><div class="form-value">
                <asp:TextBox ID="txtExternalReferrerDetailReports" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            ExternalReferrerReportId:</label><div class="form-value">
                <asp:TextBox ID="txtExternalReferrerReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            FlashCapableReportId:</label><div class="form-value">
                <asp:TextBox ID="txtFlashCapableReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            FileDownloadsReportId:</label><div class="form-value">
                <asp:TextBox ID="txtFileDownloadsReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            GetPathAnalysisStartLengthReportId:</label><div class="form-value">
                <asp:TextBox ID="txtGetPathAnalysisStartLengthReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            ImagesServedReportId:</label><div class="form-value">
                <asp:TextBox ID="txtImagesServedReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            InboundReportId:</label><div class="form-value">
                <asp:TextBox ID="txtInboundReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            JavaScriptEnabledReportId:</label><div class="form-value">
                <asp:TextBox ID="txtJavaScriptEnabledReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            LineChartReportId:</label><div class="form-value">
                <asp:TextBox ID="txtLineChartReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            LineChartHourReportId:</label><div class="form-value">
                <asp:TextBox ID="txtLineChartHourReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            MonitorResolutionReportId:</label><div class="form-value">
                <asp:TextBox ID="txtMonitorResolutionReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            OperatingSystemsReportId:</label><div class="form-value">
                <asp:TextBox ID="txtOperatingSystemsReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            OutboundReportId:</label><div class="form-value">
                <asp:TextBox ID="txtOutboundReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            OutboundReportBookMarkId:</label><div class="form-value">
                <asp:TextBox ID="txtOutboundReportBookMarkId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            OutboundReportExitId:</label><div class="form-value">
                <asp:TextBox ID="txtOutboundReportExitId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            OverviewTopPagesReportId:</label><div class="form-value">
                <asp:TextBox ID="txtOverviewTopPagesReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            OverviewEntryPagesReportId:</label><div class="form-value">
                <asp:TextBox ID="txtOverviewEntryPagesReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            OverviewExitPagesReportId:</label><div class="form-value">
                <asp:TextBox ID="txtOverviewExitPagesReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PageDetailStatisticsReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPageDetailStatisticsReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PageDetailStatisticsReportIdPageView:</label><div class="form-value">
                <asp:TextBox ID="txtPageDetailStatisticsReportIdPageView" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PageDetailStatisticsReportIdMenu:</label><div class="form-value">
                <asp:TextBox ID="txtPageDetailStatisticsReportIdMenu" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PageViewsPerVisitId:</label><div class="form-value">
                <asp:TextBox ID="txtPageViewsPerVisitId" runat="server" CssClass="textBoxes disabledText disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PageViewsPerVisitReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPageViewsPerVisitReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisDetailId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisDetailId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisLengthEndPageReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisLengthEndPageReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisLengthLeaveSiteReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisLengthLeaveSiteReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisStartPageEndWatchReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisStartPageEndWatchReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisLengthEndWatchReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisLengthEndWatchReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisStartWatchEndPageReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisStartWatchEndPageReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisStartWatchEndWatchReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisStartWatchEndWatchReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisStartWatchLeaveSiteReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisStartWatchLeaveSiteReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisDataStartWatchLengthReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisDataStartWatchLengthReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisStartPageEndPageReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisStartPageEndPageReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisDataStartPageLeaveSiteReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisDataStartPageLeaveSiteReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisStartPageEndPageAudienceSegmentReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisStartPageEndPageAudienceSegmentReportId" runat="server"
                    CssClass="disabled" Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisStartPageEndPageAudienceWatchReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisStartPageEndPageAudienceWatchReportId" runat="server"
                    CssClass="disabled" Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisStartPageEndPageAudienceNotWatchReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisStartPageEndPageAudienceNotWatchReportId" runat="server"
                    CssClass="disabled" Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisDataAudienceSegmentWatchLeaveSiteReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisDataAudienceSegmentWatchLeaveSiteReportId" runat="server"
                    CssClass="disabled" Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            PathAnalysisDataAudienceSegmentInWatchLeaveSiteReportId:</label><div class="form-value">
                <asp:TextBox ID="txtPathAnalysisDataAudienceSegmentInWatchLeaveSiteReportId" runat="server"
                    CssClass="disabled" Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            RegionReportId:</label><div class="form-value">
                <asp:TextBox ID="txtRegionReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            SearchReferrerDetailReports:</label><div class="form-value">
                <asp:TextBox ID="txtSearchReferrerDetailReports" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            SearchReferrerReports:</label><div class="form-value">
                <asp:TextBox ID="txtSearchReferrerReports" runat="server" CssClass="textBoxes disabledText disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            SinglePageVisitReportId:</label><div class="form-value">
                <asp:TextBox ID="txtSinglePageVisitReportId" runat="server" CssClass="textBoxes disabledText disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            SitePerformanceDataId:</label><div class="form-value">
                <asp:TextBox ID="txtSitePerformanceDataId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            TopLandingPagesReportId:</label><div class="form-value">
                <asp:TextBox ID="txtTopLandingPagesReportId" runat="server" CssClass="textBoxes disabledText disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            TopPagesReportId:</label><div class="form-value">
                <asp:TextBox ID="txtTopPagesReportId" runat="server" CssClass="textBoxes disabledText disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            TopViewedWatchPageExcludeSubFoldersReportId:</label><div class="form-value">
                <asp:TextBox ID="txtTopViewedWatchPageExcludeSubFoldersReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            TopViewedWatchPageIncludeSubFoldersReportId:</label><div class="form-value">
                <asp:TextBox ID="txtTopViewedWatchPageIncludeSubFoldersReportId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            TopViewedWatchContentExcludeSubFoldersReportId:</label><div class="form-value">
                <asp:TextBox ID="txtTopViewedWatchContentExcludeSubFoldersReportId" runat="server"
                    CssClass="disabled" Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            TopViewedWatchContentIncludeSubFoldersReportId:</label><div class="form-value">
                <asp:TextBox ID="txtTopViewedWatchContentIncludeSubFoldersReportId" runat="server"
                    CssClass="disabled" Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            VisitorCountId:</label><div class="form-value">
                <asp:TextBox ID="txtVisitorCountId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            VisitorLoyaltyId:</label><div class="form-value">
                <asp:TextBox ID="txtVisitorLoyaltyId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            VisitorsReportId:</label><div class="form-value">
                <asp:TextBox ID="txtVisitorsReportId" runat="server" CssClass="disabled" Width="610"
                    ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            WatchReportId:</label><div class="form-value">
                <asp:TextBox ID="txtWatchReportId" runat="server" CssClass="textBoxes disabledText disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
    <div class="form-row">
        <label class="form-label">
            WatchReportTotalVisitorId:</label><div class="form-value">
                <asp:TextBox ID="txtWatchReportTotalVisitorId" runat="server" CssClass="disabled"
                    Width="610" ReadOnly="true"></asp:TextBox></div>
    </div>
</div>
</asp:Content>

﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSAnalyzerDeveloperConfigurations %>" Language="C#" StylesheetTheme="General"
    MasterPageFile="~/Administration/SiteSetting/SiteSettingsMaster.master" AutoEventWireup="true"
    CodeBehind="GeneralSettings.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.GeneralSettings" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
<div class="site-settings"> 
    <div class="top-button-row">
        <asp:Button runat="server" ID="btnSave" Text="Save" ToolTip="Save" CssClass="primarybutton" 
            ValidationGroup="callback" OnClick="btnSave_Onclick" />
        <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
            ValidationGroup="callback" />
    </div>
    <div class="form-row"><label class="form-label">AdhocReportPath:</label><div class="form-value"><asp:TextBox ID="txtAdhocReportPath" runat="server" CssClass="textBoxes" Width="860" ReadOnly="false"></asp:TextBox></div></div>                                
    <div class="form-row"><label class="form-label">Analytics:</label><div class="form-value"><asp:TextBox ID="txtAnalytics" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">AnalyticsAdminURL:</label><div class="form-value"><asp:TextBox ID="txtAnalyticsAdminURL" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="false"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">AnalyticsPageAccessXml:</label><div class="form-value"><asp:TextBox ID="txtAnalyticsPageAccessXml" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">AnalyticsWebServiceURL:</label><div class="form-value"><asp:TextBox ID="txtiAppsAnalyticsWebServiceURL" runat="server" CssClass="textBoxes" Width="860" ReadOnly="false"></asp:TextBox></div></div> 
    <div class="form-row"><label class="form-label">Chart.XAxisTitleLength:</label><div class="form-value"><asp:TextBox ID="txtChartXAxisTitleLength" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">ClientWatchJSFunction:</label><div class="form-value"><asp:TextBox ID="txtClientWatchJSFunction" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">CompanyText:</label><div class="form-value"><asp:TextBox ID="txtCompanyText" runat="server" CssClass="textBoxes" Width="860" ReadOnly="false"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">DateFormatString:</label><div class="form-value"><asp:TextBox ID="txtDateFormatString" runat="server" CssClass="textBoxes" Width="860" ReadOnly="true"></asp:TextBox></div></div>  
    <div class="form-row"><label class="form-label">DateToolTip:</label><div class="form-value"><asp:TextBox ID="txtDateToolTip" runat="server" CssClass="textBoxes" Width="860" ReadOnly="true"></asp:TextBox></div></div> 
    <div class="form-row"><label class="form-label">ExportReportFrom:</label><div class="form-value"><asp:TextBox ID="txtExportReportFrom" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">Enable Overlays:</label><div class="form-value">
        <asp:DropDownList ID="ddlEnableOverlays" runat="server">
            <asp:ListItem Text="true" Value="true"></asp:ListItem>
            <asp:ListItem Text="false" Value="false"></asp:ListItem>
        </asp:DropDownList>
    </div></div>
    <div class="form-row"><label class="form-label">iAPPSAnalytics_Version:</label><div class="form-value"><asp:TextBox ID="txtiAPPSAnalytics_Version" runat="server" CssClass="textBoxes" Width="860" ReadOnly="true"></asp:TextBox></div></div> 
    <div class="form-row"><label class="form-label">PageLegendImagePath:</label><div class="form-value"><asp:TextBox ID="txtPageLegendImagePath" runat="server" CssClass="textBoxes" Width="860" ReadOnly="false"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">PublishLegendImagePath:</label><div class="form-value"><asp:TextBox ID="txtPublishLegendImagePath" runat="server" CssClass="textBoxes" Width="860" ReadOnly="false"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">PageViewsPerVisitEmailBody:</label><div class="form-value"><asp:TextBox ID="txtPageViewsPerVisitEmailBody" runat="server" CssClass="textBoxes" Width="860" ReadOnly="true" TextMode="MultiLine"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">RDLCFolder:</label><div class="form-value"><asp:TextBox ID="txtRDLCFolder" runat="server" CssClass="textBoxes" Width="860" ReadOnly="true"></asp:TextBox></div></div> 
    <div class="form-row"><label class="form-label">ReportsPageSize:</label><div class="form-value"><asp:TextBox ID="txtReportsPageSize" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">ReportServerUrl:</label><div class="form-value"><asp:TextBox ID="txtReportServerUrl" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">reportbuilder.application:</label><div class="form-value"><asp:TextBox ID="txtReportbuilderApplication" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">ReportingServerUserName:</label><div class="form-value"><asp:TextBox ID="txtReportingServerUserName" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">ReportingServerPassword:</label><div class="form-value"><asp:TextBox ID="txtReportingServerPassword" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">ReportingWebServiceUrl:</label><div class="form-value"><asp:TextBox ID="txtReportingWebServiceUrl" runat="server" CssClass="textBoxes disabled" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">StringFormat:</label><div class="form-value"><asp:TextBox ID="txtStringFormat" runat="server" CssClass="textBoxes" Width="860" ReadOnly="true"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">TotalLegendImagePath:</label><div class="form-value"><asp:TextBox ID="txtTotalLegendImagePath" runat="server" CssClass="textBoxes" Width="860" ReadOnly="false"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">TimeSeriesChart:</label><div class="form-value"><asp:TextBox ID="txtTimeSeriesChart" runat="server" CssClass="textBoxes" Width="860" ReadOnly="false"></asp:TextBox></div></div>
    <div class="form-row"><label class="form-label">TimeSeriesChartURL:</label><div class="form-value"><asp:TextBox ID="txtTimeSeriesChartURL" runat="server" CssClass="textBoxes" Width="860" ReadOnly="true"></asp:TextBox></div></div>
</div>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Administration/SiteSetting/SiteSettingsMaster.master" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerDeveloperConfigurations %>"
    AutoEventWireup="true" CodeBehind="BotFilters.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.BotFilters" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p><asp:Literal ID="ltHelpNote" runat="server" /></p>
    <div class="button-row">
        <asp:Button runat="server" ID="btnDownload" Text="<%$ Resources:GUIStrings, Download %>" ToolTip="<%$ Resources:GUIStrings, Download %>" CssClass="button" />
    </div>
</asp:Content>

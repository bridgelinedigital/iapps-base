<%@ Page Language="C#" MasterPageFile="~/Administration/UserMaster.master" EnableEventValidation="false"
    Theme="General" AutoEventWireup="true" Inherits="ManageUser" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerAdministrationManageUser %>"
    CodeBehind="ManageUser.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="UserListing" Src="~/UserControls/General/ModifyDeleteUser.ascx" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="userHeaderButtons">
    <asp:HyperLink runat="server" ID="btnAddNew" Text="<%$ Resources:GUIStrings, AddUser %>"
        CssClass="primarybutton" NavigateUrl="AddNewUser.aspx" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="userContentHolder" runat="Server">
    <UC:UserListing ID="userListing" runat="server" />
</asp:Content>

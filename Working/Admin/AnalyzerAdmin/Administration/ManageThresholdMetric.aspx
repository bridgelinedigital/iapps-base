﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="ManageThresholdMetric.aspx.cs" 
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.ManageThresholdMetric" StylesheetTheme="General" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerAdministrationManageThresholdMetric %>" %>
<asp:Content ID="contentHeader" runat="server" ContentPlaceHolderID="headPlaceHolder">
    <script type="text/javascript">
        function OpenImportDialog() {
            var url = jAnalyticAdminSiteUrl + "/Popups/ImportMetric.aspx";
            importMetricWindow = dhtmlmodal.open('ImportMetric', 'iframe', url, 'ImportMetric', 'width=315px,height=180px,center=1,resize=0,scrolling=1');
            importMetricWindow.onclose = function () {
                var a = document.getElementById('ImportMetric');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("/blank.html");
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="contentSection" runat="server" ContentPlaceHolderID="mainPlaceHolder">
    <div class="threshold-metric">
        <div class="page-header clear-fix">
            <h1><asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, ManageThresholdMetric %>"/></h1>
            <div class="button-row">
                <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" CssClass="primarybutton"
                     />
                <asp:Button runat="server" ID="btnCancel" Text="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="document.forms[0].reset();return false;"
                    UseSubmitBehavior="false" CssClass="button" />
            </div>
        </div>
        <div class="export-section clear-fix">
            <asp:LinkButton runat="server" ID="lBtnExport" Text="<%$ Resources:GUIStrings, ExportMetrictoExcel %>" OnClick="iBtnExport_Click" CssClass="excel-icon" CausesValidation="false" ></asp:LinkButton>
            <a href="javascript:OpenImportDialog();" class="csv-icon"><asp:localize ID="Localize2" runat="server" text="<%$ Resources:GUIStrings, ImportMetricfromCSV %>"/></a>
        </div>
        <asp:ValidationSummary ID="vsSummary" runat="server" ShowMessageBox="true" ShowSummary="false" />
        <ComponentArt:Grid ID="grdThresholdMetric" AllowTextSelection="true" EnableViewState="true" Width="100%"
            EditOnClickSelectedItem="false" AllowEditing="true" ShowHeader="False" CssClass="Grid"
            KeyboardEnabled="false" ShowFooter="false" SkinID="Default" FooterCssClass="GridFooter"
            RunningMode="Client" runat="server" PageSize="1000" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>">
            <Levels>
                <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" DataKeyField="Id"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    AllowSorting="false" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                    EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField" EditCommandClientTemplateId="EditCommandTemplate"
                    InsertCommandClientTemplateId="InsertCommandTemplate">
                    <Columns>
                        <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="140"
                            runat="server" HeadingText="<%$ Resources:GUIStrings, SuccessCategory %>" DataCellClientTemplateId="CategoryHoverTemplate"
                            DataField="Category" DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell" />
                            <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="350"
                            HeadingText="Goal Type" DataCellClientTemplateId="GoalTypeHoverTemplate"
                            DataField="GoalType" />
                        <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="400" DataField="TrackingMetric" TextWrap="true"  
                            runat="server" HeadingText="<%$ Resources:GUIStrings, TrackingMetric %>" DataCellClientTemplateId="TrackingMetricHoverTemplate" />
                        <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, GoalValue %>" DataField="GoalValue" 
                            DataCellServerTemplateId="GoalValueTemplate" AllowReordering="False" FixedWidth="true" Align="Left" AllowSorting="False" Width="150" />
                        <ComponentArt:GridColumn Align="Center" AllowReordering="False" FixedWidth="true" Width="150"
                            runat="server" HeadingText="<%$ Resources:GUIStrings, NumberFormat %>" DataCellClientTemplateId="NumberFormatHoverTemplate"
                            DataField="NumberFormat" TextWrap="true" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="CategoryHoverTemplate">
                    <div title="## DataItem.GetMember('Category').get_text() ##">
                        ## DataItem.GetMember('Category').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Category').get_text() ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="GoalTypeHoverTemplate">
                    <div title="## DataItem.GetMember('GoalType').get_text() ##">
                        ## DataItem.GetMember('GoalType').get_text() == "" ? "&nbsp;" : DataItem.GetMember('GoalType').get_text() ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TrackingMetricHoverTemplate">
                    <div title="## DataItem.GetMember('TrackingMetric').get_text() ##">
                        ## DataItem.GetMember('TrackingMetric').get_text() == "" ? "&nbsp;" : DataItem.GetMember('TrackingMetric').get_text() ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NumberFormatHoverTemplate">
                    <div title="## DataItem.GetMember('NumberFormat').get_text() ##">
                        ## DataItem.GetMember('NumberFormat').get_text() == "" ? "&nbsp;" : DataItem.GetMember('NumberFormat').get_text() ##</div>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="GoalValueTemplate">
                    <Template>
                        <asp:TextBox runat="server" ID="txtGoalValue" Text='<%# Convert.ToDecimal(Container.DataItem["NumberFormat"].ToString() == "%" ? Convert.ToDecimal(Container.DataItem["GoalValue"]) * 100 : Container.DataItem["GoalValue"]).ToString("0.00##")  %>'
                            Width="70%" CssClass="textBoxes" Visible='<%# !string.IsNullOrEmpty(Container.DataItem["NumberFormat"].ToString().Trim()) %>' 
                            MaxLength="18"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="rfvTxtGoalValue"  ControlToValidate="txtGoalValue"
                            ErrorMessage="<%$ Resources: JSMessages, Pleaseenteragoalvalue %>" Enabled='<%# !string.IsNullOrEmpty(Container.DataItem["NumberFormat"].ToString().Trim()) %>' 
                            Text="*"></asp:RequiredFieldValidator >
                        <asp:CompareValidator ID="CompareValidator1" runat="server" Type="Double" ControlToValidate="txtGoalValue"
                            ValueToCompare="0" Operator="GreaterThanEqual" ErrorMessage="<%$ Resources: JSMessages, Goalvalueshouldbegreaterthanorequaltozero %>"
                            Text="*" Enabled='<%# !string.IsNullOrEmpty(Container.DataItem["NumberFormat"].ToString().Trim()) %>' ></asp:CompareValidator>
                            <asp:CompareValidator ID="CompareValidator3" runat="server" Type="Double" ControlToValidate="txtGoalValue"
                            ValueToCompare="100" Operator="LessThanEqual" ErrorMessage="<%$ Resources: JSMessages, Goalvalueshouldbelessthanorequalto100 %>"
                            Text="*" Enabled='<%# !string.IsNullOrEmpty(Container.DataItem["NumberFormat"].ToString().Trim()) && Container.DataItem["NumberFormat"].ToString().Trim()=="%" %>' ></asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" Type="Double" ControlToValidate="txtGoalValue" Enabled='<%# !string.IsNullOrEmpty(Container.DataItem["NumberFormat"].ToString().Trim()) %>' 
                            Operator="DataTypeCheck" ErrorMessage="<%$ Resources: JSMessages, Goalvalueshouldbeanumber %>" Text="*"></asp:CompareValidator>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" Theme="General" Inherits="ChangeUserPassword" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerChangePassword %>" Codebehind="ChangePassword.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="contentChangePassword" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
<div class="change-password">
    <h1><%= GUIStrings.ChangePassword %></h1>
    <div class="container clear-fix">
        <asp:Image AlternateText="iAPPS" ToolTip="iAPPS" ImageUrl="~/images/iApps-logo.gif" ID="iappsLogo" runat="server" CssClass="logo" />
        <div class="form-container">
            <asp:ChangePassword  ID="changePassword" runat="server" ChangePasswordFailureText="<%$ Resources: JSMessages, ChangePasswordError %>">
                <ChangePasswordTemplate>
                    <div class="form-row" runat="server" id="divRequiredFields">
                        <label class="form-label"><span class="req">*</span><%= GUIStrings.RequiredFields %></label>
                    </div>
                    <div class="form-row">
                        <asp:Label ID="lblPassword" runat="server" CssClass="form-label" AssociatedControlID="CurrentPassword" Width="135"><span class="req">*</span><asp:localize runat="server" text="<%$ Resources:GUIStrings, CurrentPassword %>"/></asp:Label>
                        <div class="form-value">
                            <asp:TextBox ID="CurrentPassword" runat="server" CssClass="textBoxes" TextMode="Password" Width="170"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <asp:Label ID="lblNewPassword"  runat="server" CssClass="form-label" AssociatedControlID="NewPassword" Width="135"><span class="req">*</span><asp:localize runat="server" text="<%$ Resources:GUIStrings, NewPassword %>"/></asp:Label>
                        <div class="form-value">
                            <asp:TextBox ID="NewPassword" runat="server" CssClass="textBoxes" TextMode="Password" Width="170"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <asp:Label ID="lblConfirm" runat="server"  CssClass="form-label" AssociatedControlID="ConfirmNewPassword" Width="135"><span class="req">*</span><asp:localize runat="server" text="<%$ Resources:GUIStrings, ConfirmNewPassword %>"/></asp:Label>
                        <div class="form-value">
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="textBoxes" TextMode="Password" Width="170"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </label>
                    </div>
                    <div class="button-row">
                        <asp:Button ID="ChangePasswordPushButton"  CausesValidation="true" runat="server" CommandName="ChangePassword"
                            Text="<%$ Resources:GUIStrings, ChangePassword %>" ValidationGroup="changePassword" ToolTip="<%$ Resources:GUIStrings, ChangePassword %>"  />
                        <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" PostBackUrl="~/Default.aspx?q=out" 
                            CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
                    </div>
                    <div class="validationControl">
                        <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" Display="None"
                            ErrorMessage="<%$ Resources: JSMessages, CurrentPasswordRequired %>" ToolTip="<%$ Resources:GUIStrings, Passwordisrequired %>"
                            ValidationGroup="changePassword" Text=" "></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" Display="None"
                            Text=" " ErrorMessage="<%$ Resources: JSMessages, NewPasswordRequired %>" ToolTip="<%$ Resources:GUIStrings, NewPasswordisrequired %>"
                            ValidationGroup="changePassword"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" Display="None"
                            ErrorMessage="<%$ Resources: JSMessages, ConfirmNewPasswordRequired %>" ToolTip="<%$ Resources:GUIStrings, ConfirmNewPasswordisrequired %>"
                            Text=" " ValidationGroup="changePassword"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="InvalidNewPasswordChars" runat="server" ControlToValidate="NewPassword" Display="None"
                            ErrorMessage="<%$ Resources: JSMessages, InvalidInput %>" SetFocusOnError="True"
                            ValidationExpression="^[^<>]+$" ValidationGroup="changePassword">&nbsp;</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ID="InvalidConfirmNewPasswordChars" runat="server" Display="None"
                            ControlToValidate="ConfirmNewPassword" ErrorMessage="<%$ Resources: JSMessages,  InvalidInput %>"
                            SetFocusOnError="True" ValidationExpression="^[^<>]+$" ValidationGroup="changePassword">&nbsp;</asp:RegularExpressionValidator>
                        <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" Display="None"
                            Text=" " ControlToValidate="ConfirmNewPassword"  ErrorMessage="<%$ Resources: JSMessages, PasswordMismatch %>"
                            ValidationGroup="changePassword"></asp:CompareValidator>
                        <asp:ValidationSummary ID="summaryValidation" ValidationGroup="changePassword"  runat="Server" EnableClientScript="true"
                                ShowMessageBox="true" ShowSummary="false"    DisplayMode="List" />
                    </div>
                </ChangePasswordTemplate>
                <SuccessTemplate>
                    <div class="changePasswordSuccess">
                        <div class="form-row">
                            <label class="form-label"><%= GUIStrings.Yourpasswordhasbeenchangedsuccessfully %></label>
                        </div>
                        <span class="button-row">
                            <asp:Button ID="button" Text="<%$ Resources:GUIStrings, Continue %>" ToolTip="<%$ Resources:GUIStrings, Continue %>" 
                                CssClass="primarybutton" PostBackUrl="~/Default.aspx?q=out" runat="server" />
                        </span>
                    </div>
                </SuccessTemplate>
            </asp:ChangePassword>
        </div>
    </div>
</div>
</asp:Content>

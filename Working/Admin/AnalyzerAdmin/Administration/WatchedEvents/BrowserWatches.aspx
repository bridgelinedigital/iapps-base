<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" StylesheetTheme="General" AutoEventWireup="true" Inherits="Administration_WatchedEvents_BrowserWatches"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerAdministrationWatchedEventsClientBrowserWatches %>"
    CodeBehind="BrowserWatches.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1%>'/>"; //added by adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1%>'/>"; //added by adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items%>'/>"; //added by adams

        var item = "";
        var selectedAction = "";
        var openCodePopup = 0;
        var removeEmptyRow = false;
        var clientStatus = "";
        var Make = "";

        function grdBrowserWatches_onContextMenu(sender, eventArgs) {

            grdBrowserWatches.select(eventArgs.get_item());

            if (grdBrowserWatches.EditingDirty == true) {
                cmBrowserWatches.Items(0).visible = false;
                cmBrowserWatches.Items(1).Visible = false;
                cmBrowserWatches.Items(2).Visible = false;
                cmBrowserWatches.Items(3).Visible = false;
                cmBrowserWatches.Items(4).Visible = false;
                cmBrowserWatches.Items(5).Visible = false;
                cmBrowserWatches.Items(6).Visible = false;

            } else {
                cmBrowserWatches.Items(0).Visible = true;
                cmBrowserWatches.Items(1).Visible = true;
                cmBrowserWatches.Items(2).Visible = true;
                cmBrowserWatches.Items(3).Visible = true;
                cmBrowserWatches.Items(4).Visible = true;
                cmBrowserWatches.Items(5).Visible = true;
                cmBrowserWatches.Items(6).Visible = true;

                clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                if (clientStatus == "Active") {
                    cmBrowserWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                }
                else {
                    cmBrowserWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                }

                item = eventArgs.get_item();

                if (grdBrowserWatches.get_table().getRowCount() == 1) {
                    if (eventArgs.get_item()) {
                        if (eventArgs.get_item().GetMemberAt(7).Text == "") {
                            cmBrowserWatches.Items(0).Visible = true;
                            cmBrowserWatches.Items(1).Visible = false;
                            cmBrowserWatches.Items(2).Visible = false;
                            cmBrowserWatches.Items(3).Visible = false;
                            cmBrowserWatches.Items(4).Visible = false;
                            cmBrowserWatches.Items(5).Visible = false;
                            cmBrowserWatches.Items(6).Visible = false;
                        }
                        else {
                            cmBrowserWatches.Items(0).Visible = true;
                            cmBrowserWatches.Items(1).Visible = true;
                            cmBrowserWatches.Items(2).Visible = true;
                            cmBrowserWatches.Items(3).Visible = true;
                            cmBrowserWatches.Items(4).Visible = true;
                            cmBrowserWatches.Items(5).Visible = true;
                            cmBrowserWatches.Items(6).Visible = true;
                            clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                            //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                            if (clientStatus == "Active") {
                                cmBrowserWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                            }
                            else {
                                cmBrowserWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                            }
                        }
                    }
                }
                var evt = eventArgs.get_event();
                cmBrowserWatches.showContextMenuAtEvent(evt);
            }
        }

        function cmBrowserWatches_ItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            var tempItem = null;
            if (grdBrowserWatches.get_table().getRowCount() == 1) {
                tempItem = grdBrowserWatches.get_table().getRow(0);
                if (tempItem.getMemberAt(7).Text == "" || tempItem.getMemberAt(7).Text == null) {
                    removeEmptyRow = true;
                }
                else {
                    removeEmptyRow = false;
                }
            }
            else {
                removeEmptyRow = false;
            }


            switch (selectedMenu) {
                case "addNewBrowserWatch":
                    if (removeEmptyRow == true) {
                        grdBrowserWatches.get_table().ClearData();
                    }

                    grdBrowserWatches.get_table().addEmptyRow(0);
                    item = grdBrowserWatches.get_table().getRow(0);

                    grdBrowserWatches.edit(item);
                    grdBrowserWatches.select(item, false);
                    selectedAction = "Insert";

                    break;
                case "editBrowserWatch":
                    if (removeEmptyRow == false) {
                        grdBrowserWatches.edit(item);
                        selectedAction = "edit";
                    }

                    break;
                case "deleteBrowserWatch":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, AreYouSureYouWantToDelete %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "delete";
                            grdBrowserWatches.edit(item);
                            item.SetValue(5, selectedAction);
                            selectedAction = "delete";
                            grdBrowserWatches.editComplete();
                            grdBrowserWatches.callback();
                        }
                    }
                    else
                        return false;
                    break;
                case "changeBrowserWatchStatus":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, Areyousureyouwanttochangestatus %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "status";
                            grdBrowserWatches.edit(item);
                            item.SetValue(5, selectedAction);
                            grdBrowserWatches.editComplete();
                            grdBrowserWatches.callback();
                        }
                    }
                    else
                        return false;
                    break;
            }
        }


        function CancelClicked() {
            if (selectedAction == '<asp:localize runat="server" text="<%$ Resources:JSMessages, Insert %>"/>') {
                if (grdBrowserWatches.Data.length == 1 && (item.getMember(0).get_value() == null || item.getMember(0).get_value() == '')) {
                    grdBrowserWatches.get_table().ClearData();
                    grdBrowserWatches.editCancel();
                    item = null;
                }
                else {
                    grdBrowserWatches.editCancel();
                    grdBrowserWatches.deleteItem(item);
                    item = null;
                }
            }
            else {
                grdBrowserWatches.editCancel();
            }
            if (removeEmptyRow == true) {
                grdBrowserWatches.get_table().addEmptyRow(0);
            }
        }

        //-----------------------------Popup------------------------------------------//

        function grBrowserWatch_onItemSelect(sender, eventArgs) {
            if (openCodePopup == 1) {
                openCodePopupWindow(eventArgs.get_item().getMember('Id').get_value());
                openCodePopup = 0;
            }
        }

        function openWin2(obj) {

            openCodePopup = 1;
        }

        function openCodePopupWindow(browserId) {
            if (browserId != null && browserId != '') {
                viewPopupWindow = dhtmlmodal.open('Content', 'iframe', '../../popups/CodetoEmbed.aspx?Id=' + browserId, '',
                                                        'width=310px,height=220px,center=1,resize=0,scrolling=1');
                viewPopupWindow.onclose = function () {
                    var a = document.getElementById('Content');
                    var ifr = a.getElementsByTagName("iframe");
                    window.frames[ifr[0].name].location.replace("about:blank");
                    return true;
                }
            }
        }

        //---------------------------save --------------------------------//
        function SaveRecord() {
            if (CheckSession()) {
                if (ValidateInput() == true) {
                    var BrowserWatchesName = document.getElementById(txtName).value;
                    var BrowserWatchesDescription = document.getElementById(txtDescription).value;
                    var BrowserWatchesIncludeOverlay = document.getElementById(chkIncludeOverlay).checked;
                    var BrowserWatchesId = item.getMember(7).get_value();
                    var BrowserWatchesStatus = item.getMember(4).get_value();
                    var BrowserWatchURL = item.getMember(3).get_value();
                    var BrowserWatchesIsShared = document.getElementById(chkIsShared).checked;
                    var BrowserWatchesMessage = grdBrowserWatches_Callback(BrowserWatchesName, BrowserWatchesDescription, BrowserWatchesIncludeOverlay, BrowserWatchesId, BrowserWatchesStatus, BrowserWatchURL, BrowserWatchesIsShared, selectedAction);

                    if (BrowserWatchesMessage == "") {
                        grdBrowserWatches.editComplete();
                    } else {
                        alert(BrowserWatchesMessage);
                    }
                }
            }
        }

        //-----------------------------validation------------------------------//
        function ValidateInput() {
            var retValue = true;
            var errorMessage = '';
            var ObjBrowserWatchesName = document.getElementById(txtName);
            BrowserWatchesNameBln = true;
            if (Trim(ObjBrowserWatchesName.value) != '') {
                if (ObjBrowserWatchesName.value.match(/^[a-zA-Z0-9. _]+$/))
                { }
                else {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseuseonlyalphanumericcharactersinthename %>'/>" + "\n";
                    ObjBrowserWatchesName.focus();
                    retValue = false;
                    BrowserWatchesNameBln = false;
                }
            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenterthename %>"/>' + '\n';
                ObjBrowserWatchesName.focus();
                retValue = false;
                BrowserWatchesNameBln = false;
            }



            var ObjBrowserWatchesDescription = document.getElementById(txtDescription);
            BrowserWatchesDescriptionBln = true;
            if (Trim(ObjBrowserWatchesDescription.value) != '') {
                var regEx = new RegExp("[\<\>]", "i");
                blnResult = regEx.test(ObjBrowserWatchesDescription.value)
                if (blnResult == false) {
                    //retValue =  true;
                }
                else {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromThePageDescription %>"/>' + '\n';
                    ObjBrowserWatchesDescription.focus();
                    retValue = false;
                    BrowserWatchesDescriptionBln = false;
                }
            }
            else {
                        errorMessage = errorMessage + 'Please enter the description.\n';     
                        ObjBrowserWatchesDescription.focus();      
                retValue = false;
                BrowserWatchesDescriptionBln = false;
                    }

            if (!retValue)
                alert(errorMessage);
            if (BrowserWatchesNameBln == false) {
                ObjBrowserWatchesName.focus();
                return retValue
            }
            if (BrowserWatchesDescriptionBln == false) {
                ObjBrowserWatchesDescription.focus();
                return retValue
            }
            else
                return retValue;
        }


        //---------------------------------------textbox-----------------------------
        function setPageLevelValue(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
        }

        function getPageLevelValue(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }


        function setPageLevelDescription(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
        }

        function getPageLevelDescription(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }

        function setPageLevelStatus(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
        }

        function getPageLevelStatus(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }

        //----------------------action-----------------------------
        function getPageOperation(control) {
            return [selectedAction, selectedAction];
        }

        //----------checkbox--------------------------------
        function getValueCheckbox(control, DataField, DataItem) {
            var txtControl = document.getElementById(control);
            var Status = txtControl.checked;
            return [Status, Status];
        }

        function setValueCheckbox(control, DataField, DataItem) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            if (value == true)
                txtControl.checked = true;
            else
                txtControl.checked = false;
        }


        function grdBrowserWatches_onCallbackError(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, callbackerror %>'/>");
        }

        function grdBrowserWatches_onCallbackComplete(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                grdBrowserWatches.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Thenewclientbrowserwatchwasaddedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, edit %>'/>") {
                grdBrowserWatches.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Theclientbrowserwatchwasupdatedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Delete %>'/>") {
                item = null;
                if (grdBrowserWatches.get_table().getRowCount() <= 0 && grdBrowserWatches.PageCount == 0) {
                    grdBrowserWatches.get_table().addEmptyRow(0);
                }
                else {
                    grdBrowserWatches.previousPage();
                }
                grdBrowserWatches.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Theselectedclientbrowserwatcheswasdeletedsuccessfully %>'/>");
            }
            else if (selectedAction == "changestatus") {
                if (item.GetMemberAt(4).Value == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Active %>'/>") {
                    item.SetValue(4, '<asp:localize runat="server" text="<%$ Resources:JSMessages, InActive %>"/>', false);
                }
                else {
                    item.SetValue(4, '<asp:localize runat="server" text="<%$ Resources:JSMessages, Active %>"/>', false)
                }
                item.SetValue(5, '', false);
                grdBrowserWatches.editComplete();
                selectedAction = "";
            }
            grdBrowserWatches.editComplete();
        }

        function grdBrowserWatches_onSortChange(sender, eventArgs) {
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, edit %>'/>") {
                grdBrowserWatches.editCancel();
            }
            selectedAction = "";
        }
    </script>
</asp:Content>
<asp:Content ID="contentBrowserWatches" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
    <div class="tagged-watched-events">
        <div class="page-header clear-fix">
            <h1><%= GUIStrings.TaggedWatches %></h1>
        </div>
        <p>
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Setthesystemtolookforaparticularclientsideeventintheusersbrowser %>" />
        </p>
        <ComponentArt:Grid SkinID="Default" ID="grdBrowserWatches" runat="server" RunningMode="Callback"
            EmptyGridText="<%$ Resources:GUIStrings, NoBrowserWatches %>" AutoPostBackOnSelect="false"
            Width="100%" PageSize="10" PagerInfoClientTemplateId="grdBrowserWatchesPagination" AllowEditing="true" AutoCallBackOnInsert="false"
            AutoCallBackOnUpdate="true" AutoCallBackOnDelete="false" EditOnClickSelectedItem="false"
            CallbackReloadTemplates="true" CallbackReloadTemplateScripts="true">
            <ClientEvents>
                <SortChange EventHandler="grdBrowserWatches_onSortChange" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    EditFieldCssClass="EditDataField" EditCellCssClass="EditDataCell" SortImageHeight="7"
                    AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                    <Columns>
                        <ComponentArt:GridColumn Width="280" runat="server" HeadingText="<%$ Resources:GUIStrings, Name %>"
                            HeadingCellCssClass="FirstHeadingCell" DataField="Name" DataCellCssClass="FirstDataCell"
                            SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                            FixedWidth="true" EditControlType="Custom" EditCellServerTemplateId="svtName" />
                        <ComponentArt:GridColumn Width="280" runat="server" HeadingText="<%$ Resources:GUIStrings, Description %>"
                            DataField="Description" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                            AllowReordering="false" FixedWidth="true" EditControlType="Custom" EditCellServerTemplateId="svtDescription" />
                        <ComponentArt:GridColumn Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, IncludeinOverlay %>"
                            DataField="IncludesinOverlay" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" Align="Center" EditControlType="Custom" EditCellServerTemplateId="svtOverlay"
                            DataCellClientTemplateId="OverlayTemplate" />
                        <ComponentArt:GridColumn Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, Code %>"
                            DataField="CodetoEmbed" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" DataCellClientTemplateId="CodeTL" AllowEditing="false" AllowSorting="False"
                            Align="Center" />
                        <ComponentArt:GridColumn Width="70" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>"
                            DataField="Status" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" AllowEditing="False" />
                        <ComponentArt:GridColumn Width="200" IsSearchable="true" runat="server" HeadingText="<%$ Resources:GUIStrings, AllowAccessInChildrenSites %>"
                            DataField="IsShared" SortedDataCellCssClass="SortedDataCell" AllowReordering="false" TextWrap="true"
                            FixedWidth="false" Align="Left" EditControlType="Custom" DataCellClientTemplateId="IsSharedTemplate"
                            EditCellServerTemplateId="svtIsShared" />       
                        <ComponentArt:GridColumn IsSearchable="false" FixedWidth="true" AllowSorting="False"
                            Width="80" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                            EditControlType="Custom" EditCellServerTemplateId="EditCommandTemplate" Align="Center"
                            AllowReordering="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="svtName">
                    <Template>
                        <asp:TextBox ID="txtName" runat="server" Width="80%" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtDescription">
                    <Template>
                        <asp:TextBox ID="txtDescription" runat="server" Width="80%" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtOverlay">
                    <Template>
                        <div style="text-align: center;">
                            <asp:CheckBox ID="chkOverlay" runat="server"></asp:CheckBox>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtStatus">
                    <Template>
                        <asp:TextBox ID="txtStatus" runat="server" CssClass="textBoxes" Width="208"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                  <ComponentArt:GridServerTemplate ID="svtIsShared">
                    <Template>
                        <div style="text-align: center;">
                            <asp:CheckBox ID="chkIsShared" runat="server"></asp:CheckBox>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="EditCommandTemplate">
                    <Template>
                        <a href="javascript:SaveRecord();">
                            <img id="Img1" src="/iapps_images/cm-icon-add.png" border="0" runat="server" alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>" /></a>
                        <a href="javascript:CancelClicked();">
                            <img id="Img2" src="/iapps_images/cm-icon-delete.png" border="0" runat="server" alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>" /></a>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="OverlayTemplate">
                    ##if( DataItem.getMember("IncludesinOverlay").get_value()==true ){"<span class='GlobalCssClass'>&nbsp;&nbsp;&nbsp;&nbsp;</span>"}##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="IsSharedTemplate">
                    <div style="text-align: center;">
                        ##if( DataItem.getMember("IsShared").get_value()==true ){"<span class='GlobalCssClass'>&nbsp;&nbsp;&nbsp;&nbsp;</span>"}##                       
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="CodeTL">
                    <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onclick="openWin2(this)" />
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdBrowserWatchesPagination">
                    ## GetGridPaginationInfo(grdBrowserWatches) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ContextMenu EventHandler="grdBrowserWatches_onContextMenu" />
                <ItemSelect EventHandler="grBrowserWatch_onItemSelect" />
                <CallbackError EventHandler="grdBrowserWatches_onCallbackError" />
                <CallbackComplete EventHandler="grdBrowserWatches_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:Grid>
        <ComponentArt:Menu SkinID="ContextMenu" ID="cmBrowserWatches" runat="server">
            <Items>
                <ComponentArt:MenuItem ID="addNewBrowserWatch" runat="server" Text="<%$ Resources:GUIStrings, AddNewWatch %>"
                    Look-LeftIconUrl="cm-icon-add.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="editBrowserWatch" runat="server" Text="<%$ Resources:GUIStrings, EditWatch %>"
                    Look-LeftIconUrl="cm-icon-edit.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="deleteBrowserWatch" runat="server" Text="<%$ Resources:GUIStrings, DeleteWatch %>"
                    Look-LeftIconUrl="cm-icon-delete.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="changeBrowserWatchStatus" runat="server" Text="<%$ Resources:GUIStrings, MakeActiveInactive %>">
                </ComponentArt:MenuItem>
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="cmBrowserWatches_ItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
    </div>
</asp:Content>

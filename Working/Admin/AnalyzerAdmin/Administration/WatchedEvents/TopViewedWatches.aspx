<%@ Page Language="C#" MasterPageFile="~/MainMaster.master"
    StylesheetTheme="General" AutoEventWireup="true" Inherits="Administration_WatchedEvents_TopViewedWatches"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerAdministrationWatchedEventsTopViewedWatches %>"
    CodeBehind="TopViewedWatches.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1%>'/>"; //added by adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1%>'/>"; //added by adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items%>'/>"; //added by adams

        var itemselect = "";
        var item = "";
        var selectedAction = "";
        var openCodePopup = 0;
        var IdDir = '';
        var relativeurl
        var relativeUrlId;
        var relativeId = '';
        var removeEmptyRow = false;
        var clientStatus = "";
        var Make = "";
        var nodeId='';
        $(function () {
            $(".top-viewed-watches").on("click", ".select-page", function (e) {
                //alert(nodeId);
                OpeniAppsAnalyzerPopup("SelectPageLibraryPopup", "SelectDirectory=true&NodeId=" + nodeId, "CloseSelectPageDirectory");

                e.stopPropagation();
            });

            $(".top-viewed-watches").on("click", ".select-file", function (e) {
                OpeniAppsAnalyzerPopup("SelectFileLibraryPopup", "SelectDirectory=true&NodeId=" + nodeId, "CloseSelectFileDirectory");

                e.stopPropagation();
            });

            $(".top-viewed-watches").on("click", ".select-image", function (e) {
                OpeniAppsAnalyzerPopup("SelectImageLibraryPopup", "SelectDirectory=true&NodeId=" + nodeId, "CloseSelectImageDirectory");

                e.stopPropagation();
            });
        });

        function CloseSelectPageDirectory() {
            $(".select-page").text(popupActionsJson.CustomAttributes["SelectedFolderName"]);
            $(".select-image").text("<%= GUIStrings.SelectImageDirectory %>");
            $(".select-file").text("<%= GUIStrings.SelectFileDirectory %>");

            CloseSelectDirectory();
            Text = 1;
            item.SetValue(9, Text, true);
        }

        function CloseSelectFileDirectory() {
            $(".select-file").text(popupActionsJson.CustomAttributes["SelectedFolderName"]);
            $(".select-image").text("<%= GUIStrings.SelectImageDirectory %>");
            $(".select-page").text("<%= GUIStrings.SelectPageDirectory %>");

            CloseSelectDirectory();
            Text = 0;
            item.SetValue(9, Text, true);
        }

        function CloseSelectImageDirectory() {
            $(".select-image").text(popupActionsJson.CustomAttributes["SelectedFolderName"]);
            $(".select-file").text("<%= GUIStrings.SelectFileDirectory %>");
            $(".select-page").text("<%= GUIStrings.SelectPageDirectory %>");

            CloseSelectDirectory();
            Text = 0;
            item.SetValue(9, Text, true);
        }

        function CloseSelectDirectory() {
           
            IdDir = popupActionsJson.CustomAttributes["SelectedFolderId"];
            item.SetValue(8, IdDir, true);
            PageTreeHierarchyPath = popupActionsJson.CustomAttributes["DirHierarchypath"];
            //alert(IdDir);
        }

        function grdTopViewedWatches_onContextMenu(sender, eventArgs) {

            grdTopViewedWatches.select(eventArgs.get_item());
            if (grdTopViewedWatches.EditingDirty == true) {
                cmTopViewedWatches.Items(0).visible = false;
                cmTopViewedWatches.Items(1).Visible = false;
                cmTopViewedWatches.Items(2).Visible = false;
                cmTopViewedWatches.Items(3).Visible = false;
                cmTopViewedWatches.Items(4).Visible = false;
                cmTopViewedWatches.Items(5).Visible = false;
                cmTopViewedWatches.Items(6).Visible = false;

            } else {
                cmTopViewedWatches.Items(0).Visible = true;
                cmTopViewedWatches.Items(1).Visible = true;
                cmTopViewedWatches.Items(2).Visible = true;
                cmTopViewedWatches.Items(3).Visible = true;
                cmTopViewedWatches.Items(4).Visible = true;
                cmTopViewedWatches.Items(5).Visible = true;
                cmTopViewedWatches.Items(6).Visible = true;

                clientStatus = eventArgs.get_item().GetMemberAt(5).Text
                //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                if (clientStatus == "Active") {
                    cmTopViewedWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                }
                else {
                    cmTopViewedWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                }

                item = eventArgs.get_item();

                if (grdTopViewedWatches.get_table().getRowCount() == 1) {
                    if (eventArgs.get_item()) {
                        if (eventArgs.get_item().GetMemberAt(7).Text == "") {
                            cmTopViewedWatches.Items(0).Visible = true;
                            cmTopViewedWatches.Items(1).Visible = false;
                            cmTopViewedWatches.Items(2).Visible = false;
                            cmTopViewedWatches.Items(3).Visible = false;
                            cmTopViewedWatches.Items(4).Visible = false;
                            cmTopViewedWatches.Items(5).Visible = false;
                            cmTopViewedWatches.Items(6).Visible = false;
                        }
                        else {
                            cmTopViewedWatches.Items(0).Visible = true;
                            cmTopViewedWatches.Items(1).Visible = true;
                            cmTopViewedWatches.Items(2).Visible = true;
                            cmTopViewedWatches.Items(3).Visible = true;
                            cmTopViewedWatches.Items(4).Visible = true;
                            cmTopViewedWatches.Items(5).Visible = true;
                            cmTopViewedWatches.Items(6).Visible = true;
                            clientStatus = eventArgs.get_item().GetMemberAt(5).Text
                            //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                            if (clientStatus == "Active") {
                                cmTopViewedWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                            }
                            else {
                                cmTopViewedWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                            }
                        }
                    }
                }
                var evt = eventArgs.get_event();
                cmTopViewedWatches.showContextMenuAtEvent(evt);
            }
        }

        function cmTopViewedWatches_ItemSelect(sender, eventArgs) {

            var selectedMenu = eventArgs.get_item().get_id();
            if (grdTopViewedWatches.get_table().getRowCount() == 1) {
                tempItem = grdTopViewedWatches.get_table().getRow(0);
                if (tempItem.getMemberAt(7).Text == "" || tempItem.getMemberAt(7).Text == null) {
                    removeEmptyRow = true;
                }
                else {
                    removeEmptyRow = false;
                }
            }
            else {
                removeEmptyRow = false;
            }


            switch (selectedMenu) {
                case "addNewTopViewedWatch":
                    if (removeEmptyRow == true) {
                        grdTopViewedWatches.get_table().ClearData();
                    }
                    item.Selected = true;
                    item = null;
                    grdTopViewedWatches.get_table().addEmptyRow(0);
                    item = grdTopViewedWatches.get_table().getRow(0);
                    grdTopViewedWatches.edit(item);
                    grdTopViewedWatches.select(item, false);
                    item.Selected = true;
                    selectedAction = "Insert";
                    break;
                case "editTopViewedWatch":
                    if (removeEmptyRow == false) {
                        grdTopViewedWatches.edit(item);
                        selectedAction = "edit";
                        PageTreeHierarchyPath = item.getMemberAt(2).Text;
                        nodeId = item.getMemberAt(8).Text;
                    }
                    break;
                case "deleteTopViewedWatch":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, AreYouSureYouWantToDelete %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            grdTopViewedWatches.edit(item);
                            selectedAction = "delete";
                            item.SetValue(6, selectedAction);
                            grdTopViewedWatches.editComplete();
                            grdTopViewedWatches.callback();
                        }
                    }
                    else
                        return false;
                    break;
                case "changeTopViewedWatchStatus":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, Areyousureyouwanttochangestatus %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "status";
                            grdTopViewedWatches.edit(item);
                            item.SetValue(6, selectedAction);
                            grdTopViewedWatches.editComplete();
                            grdTopViewedWatches.callback();
                        }
                    }
                    else
                        return false;
                    break;
            }
        }


        function CancelClicked() {
            IdDir = '';
            relativeId = '';
            if (selectedAction == '<asp:localize runat="server" text="<%$ Resources:JSMessages, Insert %>"/>') {
                if (grdTopViewedWatches.Data.length == 1 && (item.getMember(0).get_value() == null || item.getMember(0).get_value() == '')) {
                    grdTopViewedWatches.get_table().ClearData();
                    grdTopViewedWatches.editCancel();
                    item = null;
                }
                else {
                    grdTopViewedWatches.deleteItem(item);
                    grdTopViewedWatches.editCancel();
                    item = null;
                }
            }
            else {

                grdTopViewedWatches.editCancel();
            }
            if (removeEmptyRow == true) {
                grdTopViewedWatches.get_table().addEmptyRow(0);
            }

        }


        function grTopViewedWatches_onItemSelect(sender, eventArgs) {
            itemselect = eventArgs.get_item();
            if (openCodePopup == 1) {
                openCodePopupWindow();
                openCodePopup = 0;
            }
        }


        function openWin7(obj) {
            openCodePopup = 1;
        }


        function openCodePopupWindow() {
            var isPage = itemselect.getMember('DirectoryType').get_value();
            var isIncludeSubFolder = itemselect.getMember('IncludesSubFolder').get_value();
            var Url = itemselect.getMember('ObjectURL').get_value();
            if (isPage != null && Url != null) {
                viewPopupWindow = dhtmlmodal.open('Content', 'iframe', '../../popups/TopViewedItems.aspx?isPage=' + isPage + '&includeSubFolders=' + isIncludeSubFolder + '&Url=' + Url.replace("&nbsp;", ""), '',
                                                    'width=310px,height=220px,center=1,resize=0,scrolling=1');
                viewPopupWindow.onclose = function () {
                    var a = document.getElementById('Content');
                    var ifr = a.getElementsByTagName("iframe");
                    window.frames[ifr[0].name].location.replace("about:blank");
                    return true;
                }
            }
        }


        function openWin6() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', '../../popups/SelectDirectory.aspx', '',
                                                    'width=855px,height=545px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function () {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }


        var Text;
        var listType = 1;
        var PageTreeHierarchyPath;
        var imagehide = 1;
        function SetSelectedNodeId(Id) {
            IdDir = Id;
            item.SetValue(8, Id, true);
            item.SetValue(9, Text, true);
            Text = "";
        }


        function SaveRecord() {
            if (CheckSession()) {
                if (ValidateInput() == true) {
                    item.SetValue(2, PageTreeHierarchyPath, true);

                    var TopViewedWatchesName = document.getElementById(txtName).value;
                    var TopViewedWatchesDescription = document.getElementById(txtDescription).value;
                    var TopViewedWatchesDirectory = PageTreeHierarchyPath;
                    var TopViewedWatchesIncludeSubFolder = document.getElementById(chkIncludeSubfolder).checked;
                    var TopViewedWatchesId = item.getMember(7).get_value();
                    var TopViewedWatchesStatus = item.getMember(5).get_value();
                    var TopViewedWatchesObjectId = item.getMember(8).get_value();
                    var TopViewedWatchesDirectoryType = item.getMember(9).get_value();
                    var TopViewedWatchesMessage = grdTopViewedWatches_Callback(TopViewedWatchesName, TopViewedWatchesDescription, TopViewedWatchesDirectory, TopViewedWatchesIncludeSubFolder, TopViewedWatchesId, TopViewedWatchesStatus, selectedAction, TopViewedWatchesObjectId, TopViewedWatchesDirectoryType);
                    if (TopViewedWatchesMessage == "") {
                        grdTopViewedWatches.editComplete();
                    } else {
                        alert(TopViewedWatchesMessage);
                    }
                }
            }

        }

        function ValidateInput() {
            var retValue = true;
            var errorMessage = '';

            var ObjTopViewedName = document.getElementById(txtName);
            TopViewedNameBln = true;
            if (Trim(ObjTopViewedName.value) != '') {
                if (ObjTopViewedName.value.match(/^[a-zA-Z0-9. _]+$/))
                { }
                else {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseuseonlyalphanumericcharactersinthename %>'/>" + "\n";
                    ObjTopViewedName.focus();
                    retValue = false;
                    TopViewedNameBln = false;
                }
            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenterthename %>"/>' + '\n';
                ObjTopViewedName.focus();
                retValue = false;
                TopViewedNameBln = false;
            }


            var ObjTopViewedDescription = document.getElementById(txtDescription);
            TopViewedDescriptionBln = true;
            if (Trim(ObjTopViewedDescription.value) != '') {
                var regEx = new RegExp("[\<\>]", "i");

                blnResult = regEx.test(ObjTopViewedDescription.value)

                if (blnResult == false) {
                    // retValue =  true;
                }
                else {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheDescription %>"/>' + '\n';

                    ObjTopViewedDescription.focus();
                    retValue = false;
                    TopViewedDescriptionBln = false;
                }
            }
                        else
                        {
                                errorMessage = errorMessage + 'Please enter the description.\n';     
                                ObjTopViewedDescription.focus();      
                                retValue =  false;  
                                TopViewedDescriptionBln =false;
                        }


            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                if (IdDir != '') {
                }
                else {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseSelectTheDirectory %>"/>' + '\n';
                    retValue = false;
                }

            } else {
            }


            if (!retValue)
                alert(errorMessage);
            if (TopViewedNameBln == false) {
                ObjTopViewedName.focus();
                return retValue
            }
            if (TopViewedDescriptionBln == false) {
                ObjTopViewedDescription.focus();
                return retValue;
            }
            else
            // IdDir='';   
                return retValue;
        }

        //---------------------------------------textbox-----------------------------

        function setPageLevelValue(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
        }


        function getPageLevelValue(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }

        //---------------------------------------textbox-----------------------------

        function setdirectoryPageLevelValue(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';

        }


        function getdirectoryPageLevelValue(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }


        //----------------------action-----------------------------

        function getPageOperation(control) {
            return [selectedAction, selectedAction];
        }


        //----------checkbox--------------------------------

        function getValueCheckbox(control, DataField, DataItem) {
            var txtControl = document.getElementById(control);
            var Status = txtControl.checked;
            return [Status, Status];
        }


        function setValueCheckbox(control, DataField, DataItem) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            if (value == true)
                txtControl.checked = true;

            else
                txtControl.checked = false;
        }


        function grTopViewedWatches_onCallbackError(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, callbackerror %>'/>");
        }

        function grTopViewedWatches_onCallbackComplete(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                grdTopViewedWatches.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Thenewtopviewedwatchwasaddedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, edit %>'/>") {
                grdTopViewedWatches.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Thetopviewedwatchwasupdatedsuccessfully %>'/>");

            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Delete %>'/>") {
                item = null;
                if (grdTopViewedWatches.get_table().getRowCount() <= 0 && grdTopViewedWatches.PageCount == 0) {
                    grdTopViewedWatches.get_table().addEmptyRow(0);
                }
                else {
                    grdTopViewedWatches.previousPage();
                }
                grdTopViewedWatches.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Theselectedtopviewedwatcheswasdeletedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, status %>'/>") {
                item.SetValue(6, '', false);
                grdTopViewedWatches.editComplete();
                selectedAction = "";
            }
            grdTopViewedWatches.editComplete();
        }

        function grdTopViewedWatches_onSortChange(sender, eventArgs) {
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, edit %>'/>") {
                grdTopViewedWatches.editCancel();
            }
            selectedAction = "";
        }

    </script>
</asp:Content>
<asp:Content ID="contentTopViewedWatches" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
    <div class="top-viewed-watches">
        <div class="page-header clear-fix">
            <h1><%= GUIStrings.TopViewedWatches %></h1>
        </div>
        <p>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Setthesystemtolookforandtracktherankingstounderstandthetopviewedpagesorassets %>" /></p>
        <ComponentArt:Grid SkinID="Default" ID="grdTopViewedWatches" runat="server" RunningMode="Callback"
            EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
            Width="100%" PageSize="10" SliderPopupClientTemplateId="grdTopViewedWatchesSlider"
            PagerInfoClientTemplateId="grdTopViewedWatchesPagination" AllowEditing="true"
            AutoCallBackOnInsert="false" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="false"
            EditOnClickSelectedItem="false" CallbackReloadTemplates="true" CallbackReloadTemplateScripts="true">
            <ClientEvents>
                <SortChange EventHandler="grdTopViewedWatches_onSortChange" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    EditFieldCssClass="EditDataField" EditCellCssClass="EditDataCell" SortImageHeight="7"
                    AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                    <ConditionalFormats>
                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('ObjectURL').Value==''"
                            RowCssClass="DefaultPageRow" SelectedRowCssClass="SelectedRow" />
                    </ConditionalFormats>
                    <Columns>
                        <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, Name %>"
                            HeadingCellCssClass="FirstHeadingCell" DataField="Name" DataCellCssClass="FirstDataCell"
                            SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                            FixedWidth="true" EditCellServerTemplateId="svtName" EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, Description %>"
                            DataField="Description" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                            AllowReordering="false" FixedWidth="true" EditCellServerTemplateId="svtDescription"
                            EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="400" runat="server" HeadingText="<%$ Resources:GUIStrings, Directory %>"
                            DataField="ObjectURL" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            Align="Left" FixedWidth="true" EditCellServerTemplateId="svtDirectory" EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, IncludeSubfolders %>"
                            DataField="IncludesSubFolder" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" Align="Center" EditControlType="Custom" EditCellServerTemplateId="svtSubfolder"
                            DataCellClientTemplateId="FolderTemplate" />
                        <ComponentArt:GridColumn Width="60" runat="server" HeadingText="<%$ Resources:GUIStrings, View %>"
                            SortedDataCellCssClass="SortedDataCell" AllowReordering="false" AllowEditing="false"
                            AllowSorting="false" FixedWidth="true" DataCellClientTemplateId="ViewTVW" Align="Center" />
                        <ComponentArt:GridColumn Width="80" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>"
                            DataField="Status" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" AllowEditing="False" />
                        <ComponentArt:GridColumn IsSearchable="false" FixedWidth="true" AllowSorting="False"
                            Width="80" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                            EditControlType="Custom" Align="Center" EditCellServerTemplateId="EditCommandTemplate"
                            AllowReordering="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="objectId" Visible="false" />
                        <ComponentArt:GridColumn DataField="DirectoryType" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="svtName">
                    <Template>
                        <asp:TextBox ID="txtName" runat="server" Width="80%" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtDescription">
                    <Template>
                        <asp:TextBox ID="txtDescription" runat="server" Width="80%" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtDirectory">
                    <Template>

                        <div style="text-align: center;">
                            <asp:HyperLink ID="hplSelectPageDirectory" runat="server" CssClass="select-page" Text="<%$ Resources:GUIStrings, SelectPageDirectory %>" />
                            &nbsp;Or&nbsp;
                            <asp:HyperLink ID="hplSelectFileDirectory" runat="server" CssClass="select-file" Text="<%$ Resources:GUIStrings, SelectFileDirectory %>" />
                            &nbsp;Or&nbsp; <br />
                        <asp:HyperLink ID="hplSelectImageDirectory" runat="server" CssClass="select-image" Text="<%$ Resources:GUIStrings, SelectImageDirectory %>" />
                            

                        </div>

<%--                        <div style="text-align: center;">
                            <img src="../../App_Themes/General/images/icon-browse.gif" runat="server" alt="<%$ Resources:GUIStrings, Browse %>"
                                atomicselection="true" align="absmiddle" title="<%$ Resources:GUIStrings, Browse %>"
                                onclick=" openWin6()" />
                        </div>--%>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtSubfolder">
                    <Template>
                        <div style="text-align: center;">
                            <asp:CheckBox ID="chkSubfolder" runat="server"></asp:CheckBox>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtStatus">
                    <Template>
                        <asp:TextBox ID="txtStatus" runat="server" CssClass="textBoxes" Width="208"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="EditCommandTemplate">
                    <Template>
                        <a href="javascript:SaveRecord();">
                            <img id="Img1" src="/iapps_images/cm-icon-add.png" border="0" runat="server"
                                alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>" /></a>
                        | <a href="javascript:CancelClicked();">
                            <img id="Img2" src="/iapps_images/cm-icon-delete.png" border="0" runat="server"
                                alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>" /></a>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="FolderTemplate">
                    ##if( DataItem.getMember("IncludesSubFolder").get_value()==true ){"<span class='GlobalCssClass'>&nbsp;&nbsp;&nbsp;&nbsp;</span>"}##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ViewTVW">
                    <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onclick="openWin7(this)" />
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdTopViewedWatchesSlider">
                    <div class="SliderPopup">
                        <h5>
                            ## DataItem.GetMember(0).Value ##</h5>
                        <p>
                            ## DataItem.GetMember(1).Value ##</p>
                        <p>
                            ## DataItem.GetMember(2).Value ##</p>
                        <p>
                            ## DataItem.GetMember(3).Value ##</p>
                        <p>
                            ## DataItem.GetMember(4).Value ##</p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdTopViewedWatches.PageCount)
                                ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdTopViewedWatches.RecordCount)
                                    ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdTopViewedWatchesPagination">
                    ## GetGridPaginationInfo(grdTopViewedWatches) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ContextMenu EventHandler="grdTopViewedWatches_onContextMenu" />
                <ItemSelect EventHandler="grTopViewedWatches_onItemSelect" />
                <CallbackError EventHandler="grTopViewedWatches_onCallbackError" />
                <CallbackComplete EventHandler="grTopViewedWatches_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:Grid>
        <ComponentArt:Menu SkinID="ContextMenu" ID="cmTopViewedWatches" runat="server">
            <Items>
                <ComponentArt:MenuItem ID="addNewTopViewedWatch" runat="server" Text="<%$ Resources:GUIStrings, AddNewWatch %>"
                    Look-LeftIconUrl="cm-icon-add.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="editTopViewedWatch" runat="server" Text="<%$ Resources:GUIStrings, EditWatch %>"
                    Look-LeftIconUrl="cm-icon-edit.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="deleteTopViewedWatch" runat="server" Text="<%$ Resources:GUIStrings, DeleteWatch %>"
                    Look-LeftIconUrl="cm-icon-delete.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="changeTopViewedWatchStatus" runat="server" Text="<%$ Resources:GUIStrings, MakeActiveInactive %>">
                </ComponentArt:MenuItem>
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="cmTopViewedWatches_ItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
        <asp:HiddenField ID="temp" runat="server" />
    </div>
</asp:Content>

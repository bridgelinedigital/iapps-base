﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" StylesheetTheme="General" AutoEventWireup="true" Inherits="Administration_WatchedEvents_AssetWatches"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerAdministrationWatchedEventsAssetWatches %>" CodeBehind="AssetWatches.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1%>'/>"; //added by adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1%>'/>"; //added by adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items%>'/>"; //added by adams

        var item = "";
        var selectedAction = "";
        var relativeurl
        var relativeUrlId;
        var relativeId = '';
        var removeEmptyRow = false;
        var clientStatus = "";
        var Make = "";
        var selectedFile;
        var listType = 1;

        $(function () {
            $(".asset-watches").on("click", ".select-file", function (e) {
                OpeniAppsAnalyzerPopup("SelectFileLibraryPopup", "", "SelectFile");

                e.stopPropagation();
            });

            $(".asset-watches").on("click", ".select-image", function (e) {
                OpeniAppsAnalyzerPopup("SelectImageLibraryPopup", "", "SelectImage");

                e.stopPropagation();
            });
        });

        function SelectFile() {
            $(".select-file").text(popupActionsJson.CustomAttributes["FileName"]);
            $(".select-image").text("<%= GUIStrings.SelectImage %>");

            relativeurl = popupActionsJson.CustomAttributes["RelativePath"] + "/" + popupActionsJson.CustomAttributes["FileName"];
            relativeId = popupActionsJson.SelectedItems.first();
            item.SetValue(7, relativeId, true);
        }

        function SelectImage() {
            $(".select-image").text(popupActionsJson.CustomAttributes["ImageFileName"]);
            $(".select-file").text("<%= GUIStrings.SelectFile %>");

            relativeId = popupActionsJson.SelectedItems.first();
            relativeurl = popupActionsJson.CustomAttributes.ImageUrl;
            item.SetValue(7, relativeId, true);
        }

        function grdAssetWatches_onContextMenu(sender, eventArgs) {

            grdAssetWatches.select(eventArgs.get_item());

            if (grdAssetWatches.EditingDirty == true) {
                cmAssetWatches.Items(0).visible = false;
                cmAssetWatches.Items(1).Visible = false;
                cmAssetWatches.Items(2).Visible = false;
                cmAssetWatches.Items(3).Visible = false;
                cmAssetWatches.Items(4).Visible = false;
                cmAssetWatches.Items(5).Visible = false;
                cmAssetWatches.Items(6).Visible = false;

            } else {
                cmAssetWatches.Items(0).Visible = true;
                cmAssetWatches.Items(1).Visible = true;
                cmAssetWatches.Items(2).Visible = true;
                cmAssetWatches.Items(3).Visible = true;
                cmAssetWatches.Items(4).Visible = true;
                cmAssetWatches.Items(5).Visible = true;
                cmAssetWatches.Items(6).Visible = true;

                clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                if (clientStatus == "Active") {
                    cmAssetWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                }
                else {
                    cmAssetWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                }
                cmAssetWatches.Items(6).Visible = true;
                cmAssetWatches.Items(6).Visible = true;

                item = eventArgs.get_item();

                if (grdAssetWatches.get_table().getRowCount() == 1) {
                    if (eventArgs.get_item()) {
                        if (eventArgs.get_item().GetMemberAt(7).Text == "") {
                            cmAssetWatches.Items(0).Visible = true;
                            cmAssetWatches.Items(1).Visible = false;
                            cmAssetWatches.Items(2).Visible = false;
                            cmAssetWatches.Items(3).Visible = false;
                            cmAssetWatches.Items(4).Visible = false;
                            cmAssetWatches.Items(5).Visible = false;
                            cmAssetWatches.Items(6).Visible = false;
                        }
                        else {
                            cmAssetWatches.Items(0).Visible = true;
                            cmAssetWatches.Items(1).Visible = true;
                            cmAssetWatches.Items(2).Visible = true;
                            cmAssetWatches.Items(3).Visible = true;
                            cmAssetWatches.Items(4).Visible = true;
                            cmAssetWatches.Items(5).Visible = true;
                            cmAssetWatches.Items(6).Visible = true;
                            clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                            //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                            if (clientStatus == "Active") {
                                cmAssetWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                            }
                            else {
                                cmAssetWatches.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                            }
                        }
                    }
                }
                var evt = eventArgs.get_event();
                cmAssetWatches.showContextMenuAtEvent(evt);
            }
        }

        function cmAssetWatches_ItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            //Checking for empty row.
            var tempItem = null;
            if (grdAssetWatches.get_table().getRowCount() == 1) {

                tempItem = grdAssetWatches.get_table().getRow(0);
                if (tempItem.getMemberAt(7).Text == "" || tempItem.getMemberAt(7).Text == null) {
                    removeEmptyRow = true;
                }
                else {
                    removeEmptyRow = false;
                }
            }
            else {
                removeEmptyRow = false;
            }


            switch (selectedMenu) {
                case "addNewAssetWatch":
                    if (removeEmptyRow == true) {
                        grdAssetWatches.get_table().ClearData();
                    }

                    item.Selected = true;
                    item = null;
                    grdAssetWatches.get_table().addEmptyRow(0);
                    item = grdAssetWatches.get_table().getRow(0);

                    grdAssetWatches.edit(item);
                    grdAssetWatches.select(item, false);
                    item.Selected = true;
                    selectedAction = "Insert";
                    break;
                case "editAssetWatch":
                    if (removeEmptyRow == false) {
                        grdAssetWatches.edit(item);
                        selectedAction = "edit";
                        relativeurl = item.getMemberAt(2).Text;
                    }
                    break;
                case "deleteAssetWatch":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, AreYouSureYouWantToDeleteTheSelectedRecord %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "delete";
                            grdAssetWatches.edit(item);
                            item.SetValue(5, selectedAction);
                            grdAssetWatches.editComplete();
                            grdAssetWatches.callback();
                        }

                    }
                    else
                        return false;
                    break;
                case "changeAssetWatchStatus":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, Areyousureyouwanttochangestatus %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "status";
                            grdAssetWatches.edit(item);
                            item.SetValue(5, selectedAction);
                            grdAssetWatches.editComplete();
                            grdAssetWatches.callback();
                        }

                    }
                    else
                        return false;
                    break;

            }
        }

        function CancelClicked() {
            relativeId = '';
            relativeurl = '';
            relativeurl = '';
            imageUrl = '';
            imageId = '';
            if (selectedAction == '<asp:localize runat="server" text="<%$ Resources:JSMessages, Insert %>"/>') {
                if (grdAssetWatches.Data.length == 1 && (item.getMember(0).get_value() == null || item.getMember(0).get_value() == '')) {
                    grdAssetWatches.get_table().ClearData();
                    grdAssetWatches.editCancel();
                    item = null;
                }
                else {

                    grdAssetWatches.deleteItem(item);
                    grdAssetWatches.editCancel();
                    item = null;
                }
            }
            else {
                grdAssetWatches.editCancel();
            }

            if (removeEmptyRow == true) {
                grdAssetWatches.get_table().addEmptyRow(0);
            }
        }


        function SaveRecord() {
            if (CheckSession()) {
                if (ValidateInput() == true) {
                    item.SetValue(2, relativeurl, true);
                    var AssetName = document.getElementById(txtName).value;
                    var AssetDescription = document.getElementById(txtDescription).value;
                    var assetChkOverlay = document.getElementById(chkOverlay).checked;
                    var assetId = item.getMember(7).get_value();
                    var assetStatus = item.getMember(4).get_value();
                    var assetObjectId = item.getMember(8).get_value();
                    var assetChkIsShared = document.getElementById(chkIsShared).checked;
                    var assetMessage = grdAssetWatches_Callback(AssetName, AssetDescription, relativeurl, assetChkOverlay, assetId, assetStatus, assetId, assetChkIsShared, selectedAction);

                    if (assetMessage == "") {
                        grdAssetWatches.editComplete();
                    } else {
                        alert(assetMessage);
                    }
                }
            }
        }

        function ValidateInput() {

            var retValue = true;
            var errorMessage = '';
            var ObjAssetWatchesName = document.getElementById(txtName);
            AssetWatchesNameBln = true;
            if (Trim(ObjAssetWatchesName.value) != '') {
                if (ObjAssetWatchesName.value.match(/^[a-zA-Z0-9. _]+$/))
                { }
                else {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseuseonlyalphanumericcharactersinthename %>'/>";
                    ObjAssetWatchesName.focus();
                    retValue = false;
                    AssetWatchesNameBln = false;
                }
            }
            else {
                errorMessage = errorMessage + '\n' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenteraname %>"/>';
                ObjAssetWatchesName.focus();
                retValue = false;
                AssetWatchesNameBln = false;
            }


            var ObjAssetWatchesDescription = document.getElementById(txtDescription);
            AssetWatchesDescription = true;
            if (Trim(ObjAssetWatchesDescription.value) != '') {
                var regEx = new RegExp("[\<\>]", "i");
                blnResult = regEx.test(ObjAssetWatchesDescription.value)
                if (blnResult == false) {

                }
                else {

                    errorMessage = errorMessage + '\n' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromThePageDescription %>"/>';
                    ObjAssetWatchesDescription.focus();
                    retValue = false;
                    AssetWatchesDescription = false;
                }
            }
            //        else
            //        {
            //            errorMessage = errorMessage + '\nPlease enter the description.';     
            //            ObjAssetWatchesDescription.focus();      
            //            retValue =  false;  
            //            AssetWatchesDescription=false;
            //        }

            //--------------for asstWatch------------------

            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                if (relativeId != '') {

                }
                else {
                    errorMessage = errorMessage + '\n' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseselectanassettowatch %>"/>';
                    retValue = false;
                }
            } else {
            }

            if (!retValue)
                alert(errorMessage);
            if (AssetWatchesNameBln == false) {
                ObjAssetWatchesName.focus();
                return retValue
            }
            if (AssetWatchesDescription == false) {
                ObjAssetWatchesDescription.focus();
                return retValue
            }
            else
                // relativeId='';
                return retValue;
        }

        //---------------------------------------textbox-----------------------------
        function setPageLevelValue(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
        }

        function getPageLevelValue(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }


        //----------------------action-----------------------------
        function getPageOperation(control) {
            return [selectedAction, selectedAction];
        }

        //----------checkbox--------------------------------
        function getValueCheckbox(control, DataField, DataItem) {
            var txtControl = document.getElementById(control);
            var Status = txtControl.checked;
            return [Status, Status];
        }

        function setValueCheckbox(control, DataField, DataItem) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            if (value == true)
                txtControl.checked = true;
            else
                txtControl.checked = false;
        }


        function grdAssetWatches_onCallbackError(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, callbackerror %>'/>");
        }

        function grdAssetWatches_onCallbackComplete(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                grdAssetWatches.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Thenewassetwatchwasaddedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, edit %>'/>") {
                grdAssetWatches.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Theassetwatchwasupdatedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Delete %>'/>") {
                item = null;
                if (grdAssetWatches.get_table().getRowCount() <= 0 && grdAssetWatches.PageCount == 0) {
                    grdAssetWatches.get_table().addEmptyRow(0);
                }
                else {
                    grdAssetWatches.previousPage();
                }
                grdAssetWatches.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Theselectedsetupwasdeletedsuccessfully %>'/>");
            }
            else if (selectedAction == "changestatus") {
                if (item.GetMemberAt(4).Value == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Active %>'/>") {
                    item.SetValue(4, "<asp:localize runat='server' text='<%$ Resources:JSMessages, InActive %>'/>", false);
                }
                else {
                    item.SetValue(4, "<asp:localize runat='server' text='<%$ Resources:JSMessages, Active %>'/>", false)
                }
                item.SetValue(5, '', false);

                grdAssetWatches.editComplete();
                selectedAction = "";
            }
    grdAssetWatches.editComplete();
}

function grdAssetWatches_onSortChange(sender, eventArgs) {
    if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, edit %>'/>") {
        grdAssetWatches.editCancel();
    }
    selectedAction = "";
}
    </script>
</asp:Content>
<asp:Content ID="contentAssetWatches" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
    <div class="asset-watches">
        <div class="page-header clear-fix">
            <h1><%= GUIStrings.AssetWatches %></h1>
        </div>
        <p>
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Setthesystemtolookforaparticulardownloadservingafileorimage %>" />
        </p>
        <ComponentArt:Grid SkinID="Default" ID="grdAssetWatches" runat="server" RunningMode="Callback"
            EmptyGridText="<%$ Resources:GUIStrings, NoAssetWatches %>" AutoPostBackOnSelect="false" Width="100%" PageSize="10"
            SliderPopupClientTemplateId="grdAssetWatchesSlider" PagerInfoClientTemplateId="grdAssetWatchesPagination"
            AllowEditing="true" AutoCallBackOnInsert="false" AutoCallBackOnUpdate="true"
            AutoCallBackOnDelete="false" EditOnClickSelectedItem="false" CallbackReloadTemplates="true"
            CallbackReloadTemplateScripts="true">
            <ClientEvents>
                <SortChange EventHandler="grdAssetWatches_onSortChange" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    EditFieldCssClass="EditDataField"
                    EditCellCssClass="EditDataCell" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                    <ConditionalFormats>
                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('ObjectURL').Value==''"
                            RowCssClass="DefaultPageRow" SelectedRowCssClass="SelectedRow" />
                    </ConditionalFormats>
                    <Columns>
                        <ComponentArt:GridColumn Width="210" runat="server" HeadingText="<%$ Resources:GUIStrings, Name %>" HeadingCellCssClass="FirstHeadingCell"
                            DataField="Name" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" EditControlType="Custom"
                            EditCellServerTemplateId="svtName" />
                        <ComponentArt:GridColumn Width="210" runat="server" HeadingText="<%$ Resources:GUIStrings, Description %>" DataField="Description"
                            IsSearchable="true" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" EditControlType="Custom" EditCellServerTemplateId="svtDescription" />
                        <ComponentArt:GridColumn Width="280" runat="server" HeadingText="<%$ Resources:GUIStrings, Asset %>" DataField="ObjectURL" SortedDataCellCssClass="SortedDataCell"
                            AllowReordering="false" Align="Left" FixedWidth="true" EditControlType="Custom"
                            EditCellServerTemplateId="svtAsset" />
                        <ComponentArt:GridColumn Width="140" IsSearchable="true" runat="server" HeadingText="<%$ Resources:GUIStrings, IncludeinOverlay %>"
                            DataField="IncludesinOverlay" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" Align="Left" EditControlType="Custom" DataCellClientTemplateId="IncludeTemplate"
                            EditCellServerTemplateId="svtOverlay" />
                        <ComponentArt:GridColumn Width="65" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>" DataField="Status" SortedDataCellCssClass="SortedDataCell"
                            AllowReordering="false" FixedWidth="true" AllowEditing="False" />
                        <ComponentArt:GridColumn Width="190" IsSearchable="true" runat="server" HeadingText="<%$ Resources:GUIStrings, AllowAccessInChildrenSites %>"
                            DataField="IsShared" SortedDataCellCssClass="SortedDataCell" AllowReordering="false" TextWrap="true"
                            FixedWidth="false" Align="Left" EditControlType="Custom" DataCellClientTemplateId="IsSharedTemplate"
                            EditCellServerTemplateId="svtIsShared" />                     
                        <ComponentArt:GridColumn IsSearchable="false" FixedWidth="true" AllowSorting="False"
                            Width="80" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>" EditControlType="Custom" Align="Center"
                            AllowReordering="false" EditCellServerTemplateId="EditCommandTemplate" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="ObjectId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="svtName">
                    <Template>
                        <asp:TextBox ID="txtName" runat="server" Width="90%" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtDescription">
                    <Template>
                        <asp:TextBox ID="txtDescription" runat="server" Width="90%" CssClass="textBoxes"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtAsset">
                    <Template>
                        <div style="text-align: center;">
                            <asp:HyperLink ID="hplSelectFile" runat="server" CssClass="select-file" Text="<%$ Resources:GUIStrings, SelectFile %>" />
                            &nbsp;Or&nbsp;
                        <asp:HyperLink ID="hplSelectImage" runat="server" CssClass="select-image" Text="<%$ Resources:GUIStrings, SelectImage %>" />
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtOverlay">
                    <Template>
                        <div style="text-align: center;">
                            <asp:CheckBox ID="chkOverlay" runat="server"></asp:CheckBox>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtStatus">
                    <Template>
                        <asp:TextBox ID="txtStatus" runat="server" CssClass="textBoxes" Width="208"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                 <ComponentArt:GridServerTemplate ID="svtIsShared">
                    <Template>
                        <div style="text-align: center;">
                            <asp:CheckBox ID="chkIsShared" runat="server"></asp:CheckBox>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="EditCommandTemplate">
                    <Template>
                        <a href="javascript:SaveRecord();">
                            <img id="Img1" src="/iapps_images/cm-icon-add.png" border="0" runat="server" alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>" /></a>
                        | <a href="javascript:CancelClicked();">
                            <img id="Img2" src="/iapps_images/cm-icon-delete.png" border="0" runat="server" alt="<%$ Resources:GUIStrings, Cancel %>"
                                title="<%$ Resources:GUIStrings, Cancel %>" /></a>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="IncludeTemplate">
                    <div style="text-align: center;">
                        ##if( DataItem.getMember("IncludesinOverlay").get_value()==true ){"<span class='GlobalCssClass'>&nbsp;&nbsp;&nbsp;&nbsp;</span>"}##
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="IsSharedTemplate">
                    <div style="text-align: center;">
                        ##if( DataItem.getMember("IsShared").get_value()==true ){"<span class='GlobalCssClass'>&nbsp;&nbsp;&nbsp;&nbsp;</span>"}##                       
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdAssetWatchesSlider">
                    <div class="SliderPopup">
                        <h5>## DataItem.GetMember(0).Value ##</h5>
                        <p>
                            ## DataItem.GetMember(1).Value ##
                        </p>
                        <p>
                            ## DataItem.GetMember(2).Value ##
                        </p>
                        <p>
                            ## DataItem.GetMember(3).Value ##
                        </p>
                        <p>
                            ## DataItem.GetMember(4).Value ##
                        </p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdAssetWatches.PageCount) ##</span>
                            <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdAssetWatches.RecordCount) ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdAssetWatchesPagination">
                    ## GetGridPaginationInfo(grdAssetWatches) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ContextMenu EventHandler="grdAssetWatches_onContextMenu" />
                <CallbackError EventHandler="grdAssetWatches_onCallbackError" />
                <CallbackComplete EventHandler="grdAssetWatches_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:Grid>
        <ComponentArt:Menu SkinID="ContextMenu" ID="cmAssetWatches" runat="server">
            <Items>
                <ComponentArt:MenuItem ID="addNewAssetWatch" runat="server" Text="<%$ Resources:GUIStrings, AddNewWatch %>" Look-LeftIconUrl="cm-icon-add.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="editAssetWatch" runat="server" Text="<%$ Resources:GUIStrings, EditWatch %>" Look-LeftIconUrl="cm-icon-edit.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="deleteAssetWatch" runat="server" Text="<%$ Resources:GUIStrings, DeleteWatch %>" Look-LeftIconUrl="cm-icon-delete.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="changeAssetWatchStatus" runat="server" Text="<%$ Resources:GUIStrings, MakeActiveInactive %>">
                </ComponentArt:MenuItem>
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="cmAssetWatches_ItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
    </div>
</asp:Content>

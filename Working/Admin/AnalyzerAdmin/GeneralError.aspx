﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="GeneralError.aspx.cs" 
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.GeneralError" StylesheetTheme="General" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerError %>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainPlaceHolder" runat="server">
<div class="error">
    <div class="boxShadow">
        <div class="boxContainer">
            <div class="boxHeader">
                <h5><asp:localize runat="server" text="<%$ Resources:GUIStrings, Error %>"/></h5>
            </div>
            <div class="paddingContainer">
                <div class="boxContent">
                    <p>
                       <asp:localize runat="server" text="<%$ Resources:GUIStrings, WeresorrybutwewereunabletoserviceyourrequestYoumaycontacttheadministratororgototheControlCenterandtryagain %>"/>
                        <asp:HyperLink ID="gotoControlCenter" runat="server" Text="<%$ Resources:GUIStrings, ControlCenter %>" NavigateUrl="~/General/AnalyticsControlCenter.aspx"></asp:HyperLink>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/Statistics/Referrers/ReferrersMaster.master"
    AutoEventWireup="true" StylesheetTheme="General" Inherits="Statistics_Traffic_SearchReferrers"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsTrafficSearchReferrers %>" CodeBehind="SearchReferrers.aspx.cs" %>

<asp:Content ID="contentSearchReferrers" ContentPlaceHolderID="contentReferrersPlaceHolder"
    runat="Server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _Keywords = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Keywords %>"/>';
        var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
        var _PagesVisit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, PagesVisit %>"/>';
        var _ATOS = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, ATOS %>"/>';
        var _NewVisit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, NewVisit %>"/>';
        var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';
        var _Source = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Source %>"/>';
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var typeOfGraph = "2";
        var item = "";
        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
                    pointerobj.src = reversedownTooltipImage;
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/

        function grdSearchReferrers_onItemSelect(sender, eventArgs) {
            item = eventArgs.get_item();
            if (grdSearchReferrers.get_table().getRowCount() >= 1 && grdSearchReferrers.PageCount != 0) {
                if (item.Table.get_level() == 0) {
                    var selectedRow = eventArgs.get_item();
                    pageId = selectedRow.getMember("Search_Engine_Keyword").get_text();
                    pageAuthor = selectedRow.getMember("Search_Engine_Keyword").get_text();
                    pageName = selectedRow.getMember("Search_Engine_Keyword").get_text();
                    document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pageviews4 %>'/>", pageName);
                    var callbackArgs = new Array(pageId, pageName, typeOfGraph);
                    SearchReferrersCallback.Callback(callbackArgs);
                }
            }
        }
        function grdSearchReferrers_onLoad(sender, eventArgs) {
            //   if(grdSearchReferrers.get_table().getRowCount()>=1 && grdSearchReferrers.PageCount!=0)
            //    {
            //    var firstItem = grdSearchReferrers.get_table().getRow(0);
            //    grdSearchReferrers.select(firstItem, false);
            //    }
            grdSearchReferrers.unSelectAll();
            if (grdSearchReferrers.get_table().getRowCount() >= 1 && grdSearchReferrers.PageCount != 0) {
                var firstItem = grdSearchReferrers.get_table().getRow(0);
                pageId = firstItem.getMember("Search_Engine_Keyword").get_text();
                pageAuthor = firstItem.getMember("Search_Engine_Keyword").get_text();
                pageName = firstItem.getMember("Search_Engine_Keyword").get_text();
                //externalRefferrers = firstItem.getMember("Referring_URL").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pageviews4 %>'/>", pageName);
            }
            if (grdSearchReferrers.get_table().getRowCount() == 0) {
                document.getElementById("legendBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("legendBox").style.visibility = "visible";
            }
        }
        function ChangeGraph() {
            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 1;

            if (grdSearchReferrers.get_table().getRowCount() >= 1 && grdSearchReferrers.PageCount != 0) {
                var callbackArgs = new Array(pageId, pageName, typeOfGraph);
                SearchReferrersCallback.Callback(callbackArgs);
            }
            return false;
        }
        //For registering the mouse event for CA Chart
        function SearchReferrersCallback_onCallbackComplete(sender, eventArgs) {
            document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
        }

        function grdSearchReferrers_onItemCollapse(sender, eventArgs) {
            grdSearchReferrers.render();
        }
        function grdSearchReferrers_onItemExpand(sender, eventArgs) {
            grdSearchReferrers.render();
        }
        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up
            var popupHTML;
            if (typeOfGraph != 1)
                popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            else
                popupHTML = 'Hour :' + FormatDate(point.get_x(), 'H');
            //var popupHTML = point.get_x().split(" ")[0]; 
            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + " ";
                    popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits %>'/>";
                    popupHTML += "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML += "<br/><b>" + pageAuthor + "</b>";
            }
            ddrivetip(popupHTML, '150', '');
        }

        function onDataPointExit(who, what) {
            hideddrivetip();
        }
    </script>

    <script type="text/javascript">
        function grdSearchReferrers_onPageIndexChange(sender, eventArgs) {
            var newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage);
            grdSearchReferrers.Callback(callbackArgs);
        }
        function grdSearchReferrers_onSortChange(sender, eventArgs) {
            grdSearchReferrers.unSelectAll();
        }
        
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdSearchReferrers.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdSearchReferrers.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }
        function grdSearchReferrers_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            if (item.get_childTable() == null) {
                grdSearchReferrers.select(item);
                var evt = eventArgs.get_event();
                cmSearchReferrer.showContextMenuAtEvent(evt, eventArgs.get_item());
            }
            else {
            }
        }
        function cmSearchReferrer_onItemSelect(sender, eventArgs) { //debugger
            var selectedMenu = eventArgs.get_item().get_id();
            switch (selectedMenu) {
                case "ViewStatistics":
                    var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                    viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', 'width=750px,height=635px,center=1,resize=0,scrolling=1');
                    viewPopupWindow.onclose = function() {
                        var a = document.getElementById('Content');
                        var ifr = a.getElementsByTagName("iframe");
                        window.frames[ifr[0].name].location.replace("about:blank");
                        return true;
                    }
                    break;
                case "ViewAnalysis":
                    window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                    break;
                case "ViewNewWindow":
                    window.open(analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text);
                    break;
                case "ViewSiteEditor":
                    //debugger
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenBySiteAndProduct(cmsProductId, siteId, siteName);
                    //window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text +  "?Token=" + GetToken(cmsProductId, siteId, siteName);
                    break;
                case "AssignIndexTerms":
                    var pageId = item.getMember("Original_Id").get_text().replace("{", "").replace("}", "");
                    OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);                    
                    break;
                case "MenuEmailAuthor":
                    var email = item.GetMember('Author_Email').Text;
                    location.href = "mailto:" + email + "";
                    break;
            }
        }
    </script>

    <div class="report-page">
        <div class="clear-fix">
            <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel" /></h2>
            <div class="graph-type clear-fix">
                <div style="float: right;" class="clear-fix">
                    <div class="form-row">
                        <label class="form-label" style="padding-left: 20px;"><asp:localize runat="server" text="<%$ Resources:GUIStrings, ViewBy %>"/></label>
                        <div class="form-value">
                            <select id="graphOption" onchange="ChangeGraph()">
                                <option value="hour"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Hour %>"/></option>
                                <option value="day" selected="selected"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Day %>"/></option>
                                <option value="week"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Week %>"/></option>
                                <option value="month"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Month %>"/></option>
                            </select>
                        </div>
                    </div>
                </div>
                 <div style="float: right;">
                    <div class="form-row">
                        <label class="form-label"><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ReportType %>" /></label>
                        <div class="form-value">
                            <asp:DropDownList ID="ddlreportTypeOption" runat="server" AutoPostBack="true"  >
                                <asp:ListItem Text="<%$ Resources:GUIStrings, ReportTypeAll %>" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:GUIStrings, ReportPaid %>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:GUIStrings, ReportNonPaid %>" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="graph-info clear-fix">
            <div class="left-section">
                <p><strong><asp:Label ID="lblTotalVisits" runat="server" CssClass="headingLabel" /></strong></p>
                <p><strong><asp:Label ID="lblTotal" runat="server" CssClass="headingLabel" /></strong></p>
            </div>
            <div class="right-section">
                <ul class="legendBox" id="legendBox">
                    <li class="firstLegend" id="firstLegend"><asp:localize runat="server" text="<%$ Resources:GUIStrings, TotalPageviews %>"/></li>
                    <li class="secondLegend" id="secondLegend"></li>
                </ul>
            </div>
        </div>
            <div class="chartContainer" id="chartContainer" runat="server">
                <ComponentArt:CallBack ID="SearchReferrersCallback" runat="server" Height="100" Width="920"
                    OnCallback="SearchReferrersCallback_onCallback">
                    <Content>
                        <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart">
                            <ClientEvents>
                                <DataPointHover EventHandler="onDataPointHover" />
                                <DataPointExit EventHandler="onDataPointExit" />
                            </ClientEvents>
                        </ComponentArtChart:Chart>
                        <asp:HiddenField ID="hdnPageHeading" runat="server" />
                        <asp:HiddenField ID="hdnAverage" runat="server" />
                    </Content>
                    <LoadingPanelClientTemplate>
                    </LoadingPanelClientTemplate>
                    <ClientEvents>
                        <CallbackComplete EventHandler="SearchReferrersCallback_onCallbackComplete" />
                    </ClientEvents>
                </ComponentArt:CallBack>
            </div>
            <ComponentArt:Grid SkinID="Referrer" ManualPaging="true" ID="grdSearchReferrers"
                runat="server" RunningMode="Callback" AllowEditing="false" EditOnClickSelectedItem="false"
                EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" Width="100%" TreeLineImageWidth="19"
                IndentCellWidth="18" PageSize="10" PagerStyle="Slider" ImagesBaseUrl="/iapps_images/"
                PagerImagesFolderUrl="/iapps_images/" TreeLineImageHeight="20">
                 <Levels>
                    <ComponentArt:GridLevel DataMember="SearchReferrer" DataKeyField="Id" HeadingCellCssClass="HeadingCell"
                        HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell"
                        RowCssClass="Row" SelectorCellWidth="18" SelectorImageWidth="18" SelectorImageUrl="last.gif"
                        ShowTableHeading="false" HeadingCellActiveCssClass="HeadingCellActive" SelectedRowCssClass="SelectedRow"
                        GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                        ShowSelectorCells="false" HeadingCellHoverCssClass="hgHeadingCellHover">
                        <Columns>

                            <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, Keywords %>" DataField="Search_Engine_Keyword"
                                IsSearchable="true" AllowReordering="false" DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell"
                                FixedWidth="true" HeadingCellClientTemplateId="keywordsHeader" />

                            <ComponentArt:GridColumn Width="200" runat="server" HeadingText="Source" HeadingCellCssClass="HeadingCell"
                                 DataCellCssClass="DataCell" SortedDataCellCssClass="SortedDataCell"
                                IsSearchable="true" AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="sourceHeader" />

                            <ComponentArt:GridColumn Width="133" runat="server" HeadingText="<%$ Resources:GUIStrings, EntryPage %>" 
                                FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                HeadingCellCssClass="LastHeadingCell" AllowReordering="false" Align="Right" />

                            <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>" DataField="Page_Visits"
                                SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                                Align="Right" HeadingCellClientTemplateId="Page_VisitsCTHeader" />

                            <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, PagesVisit %>" DataField="No_Of_Page_Visits"
                                FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                HeadingCellCssClass="LastHeadingCell" AllowReordering="false" Align="Right" HeadingCellClientTemplateId="No_Of_Page_VisitsCTHeader" />

                            <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, AverageTimeonSite %>" DataField="Average_Time_On_Site"
                                FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                HeadingCellCssClass="LastHeadingCell" AllowReordering="false" Align="Right" HeadingCellClientTemplateId="Average_Time_On_SiteCTHeader" />

                            <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, NewVisit %>" DataField="New_Visits"
                                FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                HeadingCellCssClass="LastHeadingCell" DataCellClientTemplateId="New_VisitsCT"
                                AllowReordering="false" Align="Right" HeadingCellClientTemplateId="New_VisitsCTHeader" />

                            <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>" DataField="BounceRate"
                                FixedWidth="true" DataCellClientTemplateId="BounceRateCT" DataCellCssClass="LastDataCell"
                                SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                                AllowReordering="false" Align="Right" HeadingCellClientTemplateId="BounceRateCTHeader" />

                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                    <ComponentArt:GridLevel DataMember="SearchReferrerDetail" DataKeyField="Id" HeadingCellCssClass="HeadingCell"
                        HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" RowCssClass="Row"
                        SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif" ShowTableHeading="false"
                        SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                        ShowSelectorCells="false" ShowHeadingCells="false" DataCellCssClass="DataCell" >
                        <Columns>

                            <ComponentArt:GridColumn Width="172" runat="server" HeadingText="<%$ Resources:GUIStrings, Keywords %>" 
                                IsSearchable="true" AllowReordering="false" DataCellCssClass="FirstDataCell"
                                FixedWidth="true" HeadingCellClientTemplateId="keywordsHeader" />

                            <ComponentArt:GridColumn Width="200" runat="server" HeadingText="Source" DataField="Referring_URL"
                                IsSearchable="true" AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="sourceHeader" />

                            <ComponentArt:GridColumn Width="133" runat="server" HeadingText="<%$ Resources:GUIStrings, EntryPage %>" 
                                FixedWidth="true" AllowReordering="false" Align="Right" />

                            <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>" DataField="Page_Visits"
                                AllowReordering="false" FixedWidth="true"
                                Align="Right" HeadingCellClientTemplateId="Page_VisitsCTHeader" />

                            <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, PagesVisit %>" DataField="No_Of_Page_Visits"
                                FixedWidth="true" DataCellClientTemplateId="New_VisitsCTEmpty"
                                AllowReordering="false" Align="Right" HeadingCellClientTemplateId="No_Of_Page_VisitsCTHeader" />

                            <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, AverageTimeonSite %>" DataField="Average_Time_On_Site"
                                FixedWidth="true" DataCellClientTemplateId="New_VisitsCTEmpty"
                                AllowReordering="false" Align="Right" HeadingCellClientTemplateId="Average_Time_On_SiteCTHeader" />

                            <ComponentArt:GridColumn DataField="New_Visits" FixedWidth="true" Width="150"  DataCellClientTemplateId="New_VisitsCTEmpty"
                                AllowReordering="false" Align="Right" />

                            <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>" DataField="BounceRate"
                                FixedWidth="true" DataCellCssClass="LastDataCell"  DataCellClientTemplateId="New_VisitsCTEmpty"
                                AllowReordering="false" Align="Right" HeadingCellClientTemplateId="BounceRateCTHeader" />
                                
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                    <ComponentArt:GridLevel DataMember="SearchReferrerThirdLevel" DataKeyField="Id" HeadingCellCssClass="HeadingCell"
                        HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" RowCssClass="Row"
                        SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif" ShowTableHeading="false"
                        SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                        ShowSelectorCells="false" ShowHeadingCells="false" DataCellCssClass="DataCell">
                        <Columns>
                            <ComponentArt:GridColumn DataCellCssClass="FirstDataCell" FixedWidth="true" Width="355"
                                HeadingText="" DataCellClientTemplateId="BlankCT" IsSearchable="false" AllowReordering="false"
                                AllowSorting="False" />
                            <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>" DataField="Title"
                                IsSearchable="true" AllowReordering="false" FixedWidth="true" TextWrap="true" Width="133" />
                            <ComponentArt:GridColumn DataField="Page_Visits" FixedWidth="true" Width="100" AllowReordering="false" Align="Right" />
                            <ComponentArt:GridColumn DataField="No_Of_Page_Visits" FixedWidth="true" Width="100"
                                AllowReordering="false" Align="Right" />
                            <ComponentArt:GridColumn DataField="Average_Time_On_Site" FixedWidth="true" Width="100"
                                AllowReordering="false" Align="Right" />
                            <ComponentArt:GridColumn DataField="New_Visits" FixedWidth="true" Width="150" DataCellClientTemplateId="New_VisitsCT"
                                AllowReordering="false" Align="Right" />
                            <ComponentArt:GridColumn DataField="BounceRate" FixedWidth="true" DataCellClientTemplateId="BounceRateCT"
                                AllowReordering="false" Align="Right" Width="150" DataCellCssClass="LastDataCell" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Original_Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
                            <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientEvents>
                    <%-- <PageIndexChange EventHandler="grdSearchReferrers_onPageIndexChange" />--%>
                    <SortChange EventHandler="grdSearchReferrers_onSortChange" />
                    <ContextMenu EventHandler="grdSearchReferrers_onContextMenu" />
                    <ItemExpand EventHandler="grdSearchReferrers_onItemExpand" />
                    <ItemCollapse EventHandler="grdSearchReferrers_onItemCollapse" />
                    <Load EventHandler="grdSearchReferrers_onLoad" />
                    <ItemBeforeSelect EventHandler="grdSearchReferrers_onItemSelect" />
                </ClientEvents>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="BlankCT">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="BounceRateCT">
                        ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="New_VisitsCT">
                        ## GetContentDecimal(DataItem.GetMember('New_Visits').Value) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="New_VisitsCTEmpty">
                        ## '' ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="sourceHeader">
                        <div class="custom-header">
                            <span class="header-text">##_Source##</span>
                            <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(srSource, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                            ## getSortImageHtml(DataItem,'sort-image') ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="keywordsHeader">
                        <div class="custom-header">
                            <span class="header-text">##_Keywords##</span>
                            <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(srKeywords, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                            ## getSortImageHtml(DataItem,'sort-image') ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="Page_VisitsCTHeader">
                        <div class="custom-header">
                            <span class="header-text">##_Visits##</span>
                            <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(srVisits, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                            ## getSortImageHtml(DataItem,'sort-image') ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="No_Of_Page_VisitsCTHeader">
                        <div class="custom-header">
                            <span class="header-text">##_PagesVisit##</span>
                            <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(srPageVisits, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                            ## getSortImageHtml(DataItem,'sort-image') ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="Average_Time_On_SiteCTHeader">
                        <div class="custom-header">
                            <span class="header-text">##_ATOS##</span>
                            <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(srAverageTime, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                            ## getSortImageHtml(DataItem,'sort-image') ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="New_VisitsCTHeader">
                        <div class="custom-header">
                            <span class="header-text">##_NewVisit##</span>
                            <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(srNewVisit, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                            ## getSortImageHtml(DataItem,'sort-image') ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="BounceRateCTHeader">
                        <div class="custom-header">
                            <span class="header-text">##_BounceRate1##</span>
                            <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(srBounceRate, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                            ## getSortImageHtml(DataItem,'sort-image') ##
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdSearchReferrersSlider">
                        <div class="SliderPopup">
                            <h5>
                                ## DataItem.GetMember(0).Value ##</h5>
                            <p>
                                ## DataItem.GetMember(1).Value ##</p>
                            <p>
                                ## DataItem.GetMember(2).Value ##</p>
                            <p>
                                ## DataItem.GetMember(3).Value ##</p>
                            <p>
                                ## DataItem.GetMember(4).Value ##</p>
                            <p class="paging">
                                <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdSearchReferrers.PageCount) ##</span>
                                <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdSearchReferrers.RecordCount) ##</span>
                            </p>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdSearchReferrersPagination">
                        ## GetGridPaginationInfo(grdSearchReferrers) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
            <ComponentArt:Menu ID="cmSearchReferrer" runat="server" SkinID="ContextMenu">
                <Items>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>" ID="ViewStatistics">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewInboundOutboundAnalysis %>" ID="ViewAnalysis">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>" ID="ViewNewWindow">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>" ID="ViewSiteEditor">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>" ID="AssignIndexTerms" Visible = "false">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem ID="MenuEmailAuthor" runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="cmSearchReferrer_onItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
        </div>

    <script type="text/javascript">
        <%=SearchReferrersCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>

</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/Statistics/Referrers/ReferrersMaster.master"
    AutoEventWireup="true" StylesheetTheme="General" Inherits="Statistics_Traffic_ExternalReferrers"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsTrafficExternalReferrers %>"
    CodeBehind="ExternalReferrers.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <style type="text/css">
        .indent
        {
            /*border:solid 1px red !important;*/
            width: 20px;
        }
    </style>
    <!-- Tooltip Script -->
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _domain = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Domain %>"/>';
        var _visit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
        var _PagesVisit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, PagesVisit %>"/>';
        var _ATOS = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, ATOS %>"/>';
        var _NewVisit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, NewVisit %>"/>';
        var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';

        var item = "";
        var newPage = "0";
        var orderByCol = "Page_Visits";
        var orderBy = "DESC";
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var typeOfGraph = "2";
        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }



        function ddrivetip(thetext, thewidth, thecolor) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
                    pointerobj.src = reversedownTooltipImage;
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
    </script>
    <script type="text/javascript">
        function grdExternalReferrers_onItemExpand(sender, eventArgs) {
            grdExternalReferrers.render();
        }
        function grdExternalReferrers_onCallbackComplete(sender, eventArgs) {
            newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            //ExternalReferrersCallback.Callback(callbackArgs);
        }
        var GridItem = null;
        function grdExternalReferrers_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            GridItem = eventArgs.get_item();
            if (item.get_childTable() == null) {
                grdExternalReferrers.select(item);
                var evt = eventArgs.get_event();
                cmExternalReferrers.showContextMenuAtEvent(evt, eventArgs.get_item());
            }
            else {
                if (item.Table.get_level() == 1) {
                    var evt = eventArgs.get_event();
                    cmExternalReferrersDetails.showContextMenuAtEvent(evt, eventArgs.get_item());
                }
            }
        }
        function cmExternalReferrersDetails_onItemSelect(sender, eventArgs) {

            //alert(GridItem.get_table().get_columns().length);
            var selectedMenu = eventArgs.get_item().get_id();
            switch (selectedMenu) {
                case "VisitReferringPage":
                    var url = GridItem.getMember("Referring_URL").get_text();
                    url = url.replace(" ", "");
                    window.open(url);
                    break;
            }
        }

        function cmExternalReferrers_onItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            pageId = item.getMember("Original_Id").get_text();
            pageId = pageId.replace("{", "");
            pageId = pageId.replace("}", "");
            switch (selectedMenu) {
                case "ViewStatistics":
                    var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                    viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', 'width=750px,height=660px,center=1,resize=0,scrolling=1');
                    viewPopupWindow.onclose = function () {
                        var a = document.getElementById('Content');
                        var ifr = a.getElementsByTagName("iframe");
                        window.frames[ifr[0].name].location.replace("about:blank");
                        return true;
                    }
                    break;
                case "ViewNewWindow":
                    window.open(analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text) + "/" + pageId;
                    break;
                case "ViewSiteEditor":
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenBySiteAndProduct(cmsProductId, siteId, siteName);
                    break;
                case "ViewOverlay":
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?PageState=Overlay&LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenBySiteAndProduct(cmsProductId, siteId, siteName);
                    break;
                case "PathStart":
                    window.location.href = analyticsUrl + "/Navigation/PathAnalysis.aspx?startPageId=" + pageId;
                    break;
                case "PathEnd":
                    window.location.href = analyticsUrl + "/Navigation/PathAnalysis.aspx?endPageId=" + pageId;
                    break;
                case "AssignIndexTerms":
                    OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                    break;
                case "EmailAuthor":
                    location.href = "mailto:" + item.getMember("Author_Email").Text;
                    break;
            }
        }

        function grdExternalReferrers_onSortChange(sender, eventArgs) {
            grdExternalReferrers.unSelectAll();
            orderByCol = eventArgs.get_column().get_dataField();
            if (eventArgs.get_descending()) {
                orderBy = "DESC";
            }
            else {
                orderBy = "ASC";
            }
            newPage = "0";
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            //ExternalReferrersCallback.Callback(callbackArgs);
        }
        
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdExternalReferrers.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdExternalReferrers.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }

        function grdExternalReferrers_onItemSelect(sender, eventArgs) {
            item = eventArgs.get_item();
            if (grdExternalReferrers.get_table().getRowCount() >= 1 && grdExternalReferrers.PageCount != 0) {
                if (item.Table.get_level() == 0) {
                    var selectedRow = eventArgs.get_item();
                    var childetabledata = item.get_childTable();
                    var originalurl = childetabledata.getRow(0);
                    pageId = originalurl.getMember("Original_URL").get_text();
                    pageId = pageId.substring(0, pageId.lastIndexOf('/'));
                    pageAuthor = selectedRow.getMember("Referring_URL").get_text();
                    pageName = selectedRow.getMember("Referring_URL").get_text();
                    document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pageviews4 %>'/>", pageName);
                    var callbackArgs = new Array(pageId, pageName, typeOfGraph);
                    ExternalReferrersCallback.Callback(callbackArgs);
                }
            }
        }
        function grdExternalReferrers_onLoad(sender, eventArgs) {
            grdExternalReferrers.unSelectAll();
            if (grdExternalReferrers.get_table().getRowCount() >= 1 && grdExternalReferrers.PageCount != 0) {
                var firstItem = grdExternalReferrers.get_table().getRow(0);
                pageId = "http://" + firstItem.getMember("Referring_URL").get_text();
                pageAuthor = firstItem.getMember("Referring_URL").get_text();
                pageName = firstItem.getMember("Referring_URL").get_text();

                externalRefferrers = firstItem.getMember("Referring_URL").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pageviews4 %>'/>", externalRefferrers);
            }
            if (grdExternalReferrers.get_table().getRowCount() == 0) {
                document.getElementById("legendBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("legendBox").style.visibility = "visible";
            }

        }
        function ChangeGraph() {
            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 1;

            if (grdExternalReferrers.get_table().getRowCount() >= 1 && grdExternalReferrers.PageCount != 0) {
                var callbackArgs = new Array(pageId, pageName, typeOfGraph);
                ExternalReferrersCallback.Callback(callbackArgs);
            }
            return false;
        }
        //For registering the mouse event for CA Chart
        function ExternalReferrersCallback_onCallbackComplete(sender, eventArgs) {
            //    document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            //    document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
        }
        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up
            var popupHTML;
            if (typeOfGraph != 1)
                popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            else
                popupHTML = 'Hour :' + FormatDate(point.get_x(), 'H');
            //var popupHTML = point.get_x().split(" ")[0]; 
            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + " ";
                    popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits %>'/>";
                    popupHTML += "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML += "<br/><b>" + pageAuthor + "</b>";
            }
            ddrivetip(popupHTML, '150', '');
        }

        function onDataPointExit(who, what) {
            hideddrivetip();
        }
       
    </script>
</asp:Content>
<asp:Content ID="contentExternalReferrers" ContentPlaceHolderID="contentReferrersPlaceHolder" runat="Server">
<div class="report-page">
    <div class="clear-fix">
        <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h2>
        <div class="graph-type clear-fix">
            <div style="float: right;" class="clear-fix">
                <div class="form-row">
                    <label class="form-label" style="padding-left: 20px;"><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ViewBy %>" /></label>
                    <div class="form-value">
                        <select id="graphOption" onchange="ChangeGraph()">
                            <option value="hour">
                                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Hour %>" /></option>
                            <option value="day" selected="selected">
                                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Day %>" /></option>
                            <option value="week">
                                <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Week %>" /></option>
                            <option value="month">
                                <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, Month %>" /></option>
                        </select>
                    </div>
                </div>
            </div>
            <div style="float: right;">
                <div class="form-row">
                    <label class="form-label"><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ReportType %>" /></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlreportTypeOption" runat="server" AutoPostBack="true">
                            <asp:ListItem Text="<%$ Resources:GUIStrings, ReportTypeAll %>" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:GUIStrings, ReportPaid %>" Value="1"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:GUIStrings, ReportNonPaid %>" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="graph-info clear-fix">
        <div class="left-section">
            <p><strong><asp:Label ID="lblTotalVisits" runat="server" CssClass="headingLabel"/></strong></p>
            <p><strong><asp:Label ID="lblTotal" runat="server" CssClass="headingLabel"/></strong></p>
        </div>
        <div class="right-section">
            <ul class="legendBox" id="legendBox">
                <li class="firstLegend" id="firstLegend">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TotalPageviews %>" /></li>
                <li class="secondLegend" id="secondLegend"></li>
        </ul>
    </div>
</div>
    <div class="chart-container" id="chartContainer" runat="server">
        <ComponentArt:CallBack ID="ExternalReferrersCallback" runat="server" Height="100"
            Width="920" OnCallback="ExternalReferrersCallback_onCallback">
            <Content>
                <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart" SaveImageOnDisk="true">
                    <ClientEvents>
                        <DataPointHover EventHandler="onDataPointHover" />
                        <DataPointExit EventHandler="onDataPointExit" />
                    </ClientEvents>
                </ComponentArtChart:Chart>
                <asp:HiddenField ID="hdnPageHeading" runat="server" />
                <asp:HiddenField ID="hdnAverage" runat="server" />
            </Content>
            <LoadingPanelClientTemplate>
            </LoadingPanelClientTemplate>
            <ClientEvents>
                <CallbackComplete EventHandler="ExternalReferrersCallback_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <ComponentArt:Grid SkinID="Referrer" ID="grdExternalReferrers" runat="server" RunningMode="CallBack"
        EditOnClickSelectedItem="false" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
        AutoPostBackOnSelect="false" PagerInfoClientTemplateId="grdExternalReferrersPagination"
        SliderPopupClientTemplateId="grdExternalReferrersSlider" Width="100%" IndentCellWidth="19"
        TreeLineImageWidth="19" TreeLineImageHeight="20" IndentCellCssClass="IndentCell"
        ManualPaging="true" PagerStyle="Slider" ImagesBaseUrl="~/App_Themes/General/images/"
        PagerImagesFolderUrl="~/App_Themes/General/images/">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Sno" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="318" runat="server" HeadingText="<%$ Resources:GUIStrings, Domain %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="Referring_URL" DataCellCssClass="FirstDataCell"
                        IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" HeadingCellClientTemplateId="sourceHeader" />
                    <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                        IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" TextWrap="true" Width="200" />
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                        DataField="Page_Visits" IsSearchable="true" DataType="System.Int64"
                        AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="Page_VisitsCTHeader" />
                    <ComponentArt:GridColumn Width="125" runat="server" HeadingText="<%$ Resources:GUIStrings, PagesVisit %>"
                        DataField="No_Of_Page_Visits" AllowReordering="false"
                        FixedWidth="true" Align="Right" DataType="System.Int64" HeadingCellClientTemplateId="No_Of_Page_VisitsCTHeader"
                        DataCellClientTemplateId="No_Of_Page_VisitsCT" />
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, AverageTimeonSite %>"
                        DataField="Average_Time_On_Site" FixedWidth="true"
                        AllowReordering="false" Align="Right" HeadingCellClientTemplateId="Average_Time_On_SiteCTHeader" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, NewVisit %>"
                        DataField="New_Visits" FixedWidth="true" DataCellClientTemplateId="New_VisitsCT"
                        AllowReordering="false" DataType="System.Double" Align="Right" HeadingCellClientTemplateId="New_VisitsCTHeader" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>"
                        DataField="BounceRate" FixedWidth="true" DataCellClientTemplateId="BounceRateCT"
                        DataCellCssClass="LastDataCell" HeadingCellCssClass="LastHeadingCell"
                        AllowReordering="false" DataType="System.Double" Align="Right" HeadingCellClientTemplateId="BounceRateCTHeader" />
                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn DataField="Object_URL" Visible="false" />
                    <ComponentArt:GridColumn DataField="Sno" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
            <ComponentArt:GridLevel DataMember="ExternalReferrerDetail" DataKeyField="Sno" ShowTableHeading="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell" ShowHeadingCells="false"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover" ShowSelectorCells="false"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn DataField="Object_URL" FixedWidth="true" AllowReordering="false" Width="481"
                        Align="Left" DataCellCssClass="FirstDataCell" DataCellClientTemplateId="ObjectURlTemplate" />
                    <ComponentArt:GridColumn DataField="Page_Visits" FixedWidth="true" AllowReordering="false"
                        Align="Right" Width="98" />
                    <ComponentArt:GridColumn FixedWidth="true" AllowReordering="false" AllowSorting="False"
                        DataCellCssClass="LastDataCell" Width="539" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn DataField="Referring_URL" Visible="false" />
                    <ComponentArt:GridColumn DataField="Original_URL" Visible="false" />
                    <ComponentArt:GridColumn DataField="Sno" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
            <ComponentArt:GridLevel DataMember="ExternalReferrerDetail" DataKeyField="Sno" ShowTableHeading="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell" ShowHeadingCells="false"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover" ShowSelectorCells="false"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn FixedWidth="true" AllowReordering="false" AllowSorting="False"
                        DataCellCssClass="FirstDataCell" Width="257" />
                    <ComponentArt:GridColumn DataField="Title" IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" TextWrap="true" Width="200" />
                    <ComponentArt:GridColumn DataField="Page_Visits" FixedWidth="true" AllowReordering="false"
                        Align="Right" Width="101" />
                    <ComponentArt:GridColumn DataField="No_Of_Page_Visits" FixedWidth="true" AllowReordering="false"
                        Align="Right" Width="122" />
                    <ComponentArt:GridColumn DataField="ATOSDetail" FixedWidth="true" AllowReordering="false"
                        Align="Right" Width="100" />
                    <ComponentArt:GridColumn DataField="New_Visits" FixedWidth="true" AllowReordering="false"
                        Align="Right" DataCellClientTemplateId="New_VisitsCT"
                        Width="150" />
                    <ComponentArt:GridColumn DataField="BounceRate" FixedWidth="true" AllowReordering="false"
                        Align="Right" DataCellCssClass="LastDataCell" DataCellClientTemplateId="BounceRateCT"
                        Width="150" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn DataField="Original_Id" Visible="false" />
                    <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                    <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
                    <ComponentArt:GridColumn DataField="Referring_URL" Visible="false" />
                    <ComponentArt:GridColumn DataField="Sno" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ItemExpand EventHandler="grdExternalReferrers_onItemExpand" />
            <ItemCollapse EventHandler="grdExternalReferrers_onItemExpand" />
            <ItemSelect EventHandler="grdExternalReferrers_onItemSelect" />
            <Load EventHandler="grdExternalReferrers_onLoad" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ObjectURlTemplate">
                <div title="## DataItem.GetMember('Object_URL').Value ##">
                    ## DataItem.GetMember('Object_URL').Value ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BlankCT">
                <table width="287" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="New_VisitsCT">
                ## GetContentDecimal(DataItem.GetMember('New_Visits').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BounceRateCT">
                ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="No_Of_Page_VisitsCT">
                ## GetContentDecimal(DataItem.GetMember('No_Of_Page_Visits').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="sourceHeader">
                <div class="custom-header">
                    <span class="header-text">##_domain##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(erSource, 300, '');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="Page_VisitsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_visit##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="No_Of_Page_VisitsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_PagesVisit##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erPagesVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="Average_Time_On_SiteCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_ATOS##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erAverageTime, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="New_VisitsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_NewVisit##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erNewVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BounceRateCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_BounceRate1##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erBounceRate, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdExternalReferrersSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdExternalReferrers.PageCount)
                            ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdExternalReferrers.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdExternalReferrersPagination">
                ## GetGridPaginationInfo(grdExternalReferrers) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ClientEvents>
            <ContextMenu EventHandler="grdExternalReferrers_onContextMenu" />
        </ClientEvents>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="cmExternalReferrers" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>"
                ID="ViewStatistics">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>"
                ID="ViewNewWindow">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, PathAnalysisStart %>"
                ID="PathStart">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, PathAnalysisEnd %>"
                ID="PathEnd">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>"
                ID="ViewSiteEditor">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinOverlayMode %>"
                ID="ViewOverlay">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>"
                ID="AssignIndexTerms">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>"
                ID="EmailAuthor" ToolTip="<%$ Resources:GUIStrings, EmailAuthor %>">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="cmExternalReferrers_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Menu ID="cmExternalReferrersDetails" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, VisitReferringPage %>"
                ID="VisitReferringPage">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="cmExternalReferrersDetails_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <script type="text/javascript">
        <%=ExternalReferrersCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>
</div>
</asp:Content>

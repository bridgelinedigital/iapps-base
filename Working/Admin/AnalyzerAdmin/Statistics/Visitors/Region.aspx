<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Statistics/Visitors/VisitorMaster.master"
    StylesheetTheme="General" Inherits="Statistics_Visitors_Region" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsVisitorsRegion %>"
    CodeBehind="Region.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _Region = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Region %>"/>';
        var _Country = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, CountryStates %>"/>';
        var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
        var _PagesVisits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, PagesVisits %>"/>';
        var _ATOS = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, ATOS %>"/>';
        var _NewVisit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, NewVisit %>"/>';
        var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';
        //** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var newPage = "0";
        var orderByCol = "Visits";
        var orderBy = "DESC";
        var typeOfGraph = "2";
        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
                    pointerobj.src = reversedownTooltipImage;
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
        
        function grdRegion_onPageIndexChange(sender, eventArgs) {
            newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            LanguageChartCallback.Callback(callbackArgs);
        }
        function grdRegion_onSortChange(sender, eventArgs) {
            grdRegion.unSelectAll();
            orderByCol = eventArgs.get_column().get_dataField();
            if (eventArgs.get_descending()) {
                orderBy = "DESC";
            }
            else {
                orderBy = "ASC";
            }
            newPage = "0";
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            if (grdRegion.RecordCount != 0) {
                LanguageChartCallback.Callback(callbackArgs);
            }
        }
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdRegion.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdRegion.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }


        function grdRegion_onItemSelect(sender, eventArgs) {
            if (grdRegion.get_table().getRowCount() >= 1 && grdRegion.PageCount != 0) {
                var selectedRow = eventArgs.get_item();
                region = selectedRow.getMember("Region").get_text();
                country = selectedRow.getMember("Country").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits1 %>'/>", country);
                var callbackArgs = new Array(region, country, typeOfGraph);
                RegionCallback.Callback(callbackArgs);
            }
        }
        function grdRegion_onLoad(sender, eventArgs) {
            grdRegion.unSelectAll();
            if (grdRegion.get_table().getRowCount() >= 1 && grdRegion.PageCount != 0) {
                var firstItem = grdRegion.get_table().getRow(0);
                region = firstItem.getMember("Region").get_text();
                country = firstItem.getMember("Country").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits1 %>'/>", country);
            }
            if (grdRegion.get_table().getRowCount() == 0) {
                document.getElementById("legendBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("legendBox").style.visibility = "visible";
            }
        }
        function ChangeGraph() {
            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 1;
            if (grdRegion.get_table().getRowCount() >= 1 && grdRegion.PageCount != 0) {
                var callbackArgs = new Array(region, country, typeOfGraph);
                RegionCallback.Callback(callbackArgs);
            }
            return false;
        }
        //For registering the mouse event for CA Chart
        function RegionCallback_onCallbackComplete(sender, eventArgs) {
            document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
        }
        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up
            var popupHTML;
            if (typeOfGraph != 1)
                popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            else
                popupHTML = '<asp:localize runat="server" text="<%$ Resources:JSMessages, Hour %>"/>' + FormatDate(point.get_x(), 'H');
            //var popupHTML = point.get_x().split(" ")[0]; 
            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + " ";
                    popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits %>'/>";
                    popupHTML += "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML += "<br/><b>" + pageAuthor + "</b>";
            }
            ddrivetip(popupHTML, '150', '');
        }

        function onDataPointExit(who, what) {
            hideddrivetip();
        }

    </script>
</asp:Content>
<asp:Content ID="contentLanguage" ContentPlaceHolderID="visitorContentPlaceHolder" runat="Server">
<div class="report-page">
    <div class="clear-fix">
        <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h2>
        <div class="graph-type clear-fix">
            <div class="form-row">
                <label class="form-label"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ViewBy %>" /></label>
                <div class="form-value">
                    <select id="graphOption" onchange="ChangeGraph()">
                        <option value="hour">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Hour %>" /></option>
                        <option value="day" selected="selected">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Day %>" /></option>
                        <option value="week">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Week %>" /></option>
                        <option value="month">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Month %>" /></option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="graph-info clear-fix">
        <div class="left-section">
            <p><strong><asp:Label ID="lblTotalVisits" runat="server" CssClass="headingLabel"/><//strong></p>
            <p><strong><asp:Label ID="lblTotal" runat="server" CssClass="headingLabel"/></h5></strong></p>
        </div>
        <div class="right-section">
            <ul class="legendBox" id="legendBox">
                <li class="firstLegend" id="firstLegend">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TotalVisits %>" /></li>
                <li class="secondLegend" id="secondLegend"></li>
            </ul>
        </div>
    </div>
    <div class="chart-container" id="chartContainer" runat="server">
        <ComponentArt:CallBack ID="RegionCallback" runat="server" Height="100" Width="920"
            OnCallback="RegionCallback_onCallback">
            <Content>
                <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart">
                    <ClientEvents>
                        <DataPointHover EventHandler="onDataPointHover" />
                        <DataPointExit EventHandler="onDataPointExit" />
                    </ClientEvents>
                </ComponentArtChart:Chart>
                <asp:HiddenField ID="hdnPageHeading" runat="server" />
                <asp:HiddenField ID="hdnAverage" runat="server" />
            </Content>
            <LoadingPanelClientTemplate>
            </LoadingPanelClientTemplate>
            <ClientEvents>
                <CallbackComplete EventHandler="RegionCallback_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <ComponentArt:Grid SkinID="Referrer" ID="grdRegion" runat="server" AllowEditing="false"
        EditOnClickSelectedItem="false" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
        AutoPostBackOnSelect="false" Width="100%" TreeLineImageWidth="19" IndentCellWidth="19"
        PageSize="10" PagerStyle="Buttons" ImagesBaseUrl="/iapps_images/"
        PagerImagesFolderUrl="/iapps_images/" TreeLineImageHeight="20">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="RowNumber" DataMember="Country" ShowTableHeading="false"
                ShowSelectorCells="false" RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="180" runat="server" HeadingText="<%$ Resources:GUIStrings, Region %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="Region" DataCellCssClass="FirstDataCell"
                        SortedDataCellCssClass="SortedDataCell" IsSearchable="true" HeadingCellClientTemplateId="cmHeaderRegion"
                        AllowReordering="false" FixedWidth="true" Visible="false" />
                    <ComponentArt:GridColumn Width="450" runat="server" HeadingText="<%$ Resources:GUIStrings, CountryStates %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="Country" DataCellCssClass="FirstDataCell"
                        SortedDataCellCssClass="SortedDataCell" IsSearchable="true" HeadingCellClientTemplateId="cmHeaderCountry"
                        AllowReordering="false" FixedWidth="true" />
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                        DataField="Visits" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="cmHeaderVisits"
                        Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, PagesVisits %>"
                        DataField="PagesPerVisit" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" DataType="System.Double" HeadingCellClientTemplateId="cmHeaderPagesVisits"
                        Align="Right" DataCellClientTemplateId="PagesPerVisitCT" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, AverageTimeonSite %>"
                        DataField="AvgTime" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" HeadingCellClientTemplateId="cmHeaderAverage" Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, NewVisit %>"
                        DataField="NewVisit" SortedDataCellCssClass="SortedDataCell" DataCellClientTemplateId="NewVisitCT"
                        AllowReordering="false" FixedWidth="true" DataType="System.Double" HeadingCellClientTemplateId="cmHeaderNewVisit"
                        Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>"
                        DataField="BounceRate" SortedDataCellCssClass="SortedDataCell" HeadingCellClientTemplateId="cmHeaderBouncerate"
                        AllowReordering="false" FixedWidth="true" DataType="System.Double" Align="Right"
                        DataCellClientTemplateId="BounceRateCT" />
                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
            <ComponentArt:GridLevel DataMember="USState" DataKeyField="RowNumber" ShowHeadingCells="false"
                ShowSelectorCells="false" RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="321" runat="server" HeadingText="<%$ Resources:GUIStrings, States %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="State" DataCellCssClass="FirstDataCell"
                        SortedDataCellCssClass="SortedDataCell" IsSearchable="true" HeadingCellClientTemplateId="cmHeaderCountry"
                        AllowReordering="false" FixedWidth="true" />
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                        DataField="Visits" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="cmHeaderVisits"
                        Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, PagesVisits %>"
                        DataField="PagesPerVisit" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" DataType="System.Double" HeadingCellClientTemplateId="cmHeaderPagesVisits"
                        Align="Right" DataCellClientTemplateId="PagesPerVisitCT" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, AverageTimeonSite %>"
                        DataField="AvgTime" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" HeadingCellClientTemplateId="cmHeaderAverage" Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, NewVisit %>"
                        DataField="NewVisit" SortedDataCellCssClass="SortedDataCell" DataCellClientTemplateId="NewVisitCT"
                        AllowReordering="false" FixedWidth="true" DataType="System.Double" HeadingCellClientTemplateId="cmHeaderNewVisit"
                        Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>"
                        DataField="BounceRate" SortedDataCellCssClass="SortedDataCell" HeadingCellClientTemplateId="cmHeaderBouncerate"
                        AllowReordering="false" FixedWidth="true" DataType="System.Double" Align="Right"
                        DataCellClientTemplateId="BounceRateCT" />
                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <Load EventHandler="grdRegion_onLoad" />
            <ItemSelect EventHandler="grdRegion_onItemSelect" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NewVisitCT">
                ## GetContentDecimal(DataItem.GetMember('NewVisit').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BounceRateCT">
                ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="PagesPerVisitCT">
                ## GetContentDecimal(DataItem.GetMember('PagesPerVisit').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderRegion">
                <div class="custom-header">
                    <span class="header-text">##_Region##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(rgRegion, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderCountry">
                <div class="custom-header">
                    <span class="header-text">##_Country##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(rgCountry, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderVisits">
                <div class="custom-header">
                    <span class="header-text">##_Visits##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(rgVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderPagesVisits">
                <div class="custom-header">
                    <span class="header-text">##_PagesVisits##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(rgPagesVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderAverage">
                <div class="custom-header">
                    <span class="header-text">##_ATOS##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(rgAverage, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderNewVisit">
                <div class="custom-header">
                    <span class="header-text">##_NewVisit##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(rgNewVisit, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderBouncerate">
                <div class="custom-header">
                    <span class="header-text">##_BounceRate1##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(Bouncerate, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdRegionSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdRegion.PageCount)
                            ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdRegion.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdRegionPagination">
                ## GetGridPaginationInfo(grdRegion) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <script type="text/javascript">
        <%=RegionCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>
</div>
</asp:Content>

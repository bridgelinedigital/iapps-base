<%@ Page Language="C#" MasterPageFile="~/Statistics/Visitors/VisitorMaster.master"
    StylesheetTheme="General" AutoEventWireup="true" Inherits="Statistics_Visitors_Visitors"
    Title="iAPPS Analyzer: Statistics: Visitors" CodeBehind="Visitors.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _Visitors = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visitors %>"/>';
        var _Average = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, VisitorATOP %>"/>';
        var _AvgPageViewsperVisit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, AvgPageViewsperVisit %>"/>';
        var _Duration = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Average %>"/>';
        //** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var reverseTooltipImage;
        var downTooltipImage;
        var tooltipImage;
        var newPage = "0";
        var orderByCol = "Visits";
        var orderBy = "DESC";

        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor, imgSrc, revImgSrc, downImgSrc) {
            tooltipImage = imgSrc;
            reverseTooltipImage = revImgSrc;
            downTooltipImage = downImgSrc;
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
        function grdVisitors_onPageIndexChange(sender, eventArgs) {
            newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            VisitorsChartCallback.Callback(callbackArgs);
        }

        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up
            var popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + " ";
                    popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits %>'/>";
                    popupHTML += "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML += "<br/><b>" + pageAuthor + "</b>";
            }
            ddrivetip(popupHTML, '150', '', '../../App_Themes/General/images/tooltip-arrow.gif', '../../App_Themes/General/images/rev-tooltip-arrow.gif', '../../App_Themes/General/images/down-tooltip-arrow.gif')
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        function onDataPointExit(who, what) {
            hideddrivetip();
        }


        function ChangeGraph() {

            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 2;


            if (grdVisitors.get_table().getRowCount() >= 1 && grdVisitors.PageCount != 0) {
                var callbackArgs = new Array(version, typeOfGraph);
                VisitorsChartCallback.Callback(callbackArgs);
            }
            return false;
        }

        function grdVisitors_onSortChange(sender, eventArgs) {
        }
        
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdVisitors.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdVisitors.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }

        //For registering the mouse event for CA Chart
        function VisitorsChartCallback_onCallbackComplete(sender, eventArgs) {
            document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            document.getElementById("<%=lblAverage.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
            document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnTotal.ClientID%>").value;
        }

    </script>
</asp:Content>
<asp:Content ID="contentVisitors" ContentPlaceHolderID="visitorContentPlaceHolder" runat="Server">
<div class="report-page">
    <div class="clear-fix">
        <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h2>
        <div class="graph-type clear-fix">
            <div class="form-row">
                <label class="form-label"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ViewBy %>" /></label>
                <div class="form-value">
                    <select id="graphOption" onchange="ChangeGraph()">
                        <option value="day" selected="selected">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Day %>" /></option>
                        <option value="week">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Week %>" /></option>
                        <option value="month">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Month %>" /></option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="graph-info clear-fix">
        <div class="left-section">
            <p><strong><asp:Label ID="lblTotal" runat="server" CssClass="headingLabel"/></strong></p>
            <p><strong><asp:Label ID="lblAverage" runat="server" CssClass="headingLabel"/></strong></p>
        </div>
        <div class="right-section">
            <ul class="legendBox" id="legendBox">
                <li class="firstLegend" id="firstLegend">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, TotalVisits %>" /></li>
            </ul>
        </div>
    </div>
    <div class="chart-container" runat="server" id="chartContainer">
        <ComponentArt:CallBack ID="VisitorsChartCallback" runat="server" OnCallback="VisitorsCallback_onCallback">
            <Content>
                <ComponentArtChart:Chart ID="VisitorsChart" runat="server" SkinID="LineChartSingle">
                    <ClientEvents>
                        <DataPointHover EventHandler="onDataPointHover" />
                        <DataPointExit EventHandler="onDataPointExit" />
                    </ClientEvents>
                </ComponentArtChart:Chart>
                <asp:HiddenField ID="hdnPageHeading" runat="server" />
                <asp:HiddenField ID="hdnAverage" runat="server" />
                <asp:HiddenField ID="hdnTotal" runat="Server" />
            </Content>
            <LoadingPanelClientTemplate>
            </LoadingPanelClientTemplate>
            <ClientEvents>
                <CallbackComplete EventHandler="VisitorsChartCallback_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <ComponentArt:Grid SkinID="Default" ID="grdVisitors" runat="server" RunningMode="Client"
        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
        Width="100%" PageSize="10" SliderPopupClientTemplateId="grdVisitorsSlider" PagerInfoClientTemplateId="grdVisitorsPagination">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="OrderBy" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="400" runat="server" HeadingText="<%$ Resources:GUIStrings, VisitorType %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="VisitorType" DataCellCssClass="FirstDataCell"
                        SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, Visitors %>"
                        DataField="Visits" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" Align="Right" DataType="System.Int64"
                        HeadingCellClientTemplateId="cmHeaderEP" />
                    <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, VisitorATOP %>"
                        DataField="Average" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" Align="Right" DataType="System.String"
                        DataCellClientTemplateId="AvgPageDurationCT" HeadingCellClientTemplateId="cmHeaderAverage" />
                    <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, Average %>"
                        DataField="duration" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="cmHeaderAvgsession" />
                    <ComponentArt:GridColumn Width="250" runat="server" HeadingText="<%$ Resources:GUIStrings, AvgPageViewsperVisit %>"
                        DataField="AVGPageViews" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" Align="Right" DataCellClientTemplateId="No_Of_Page_VisitsCT"
                        DataType="System.Double" HeadingCellClientTemplateId="cmHeaderpagevist" />
                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                    <ComponentArt:GridColumn DataField="OrderBy" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <PageIndexChange EventHandler="grdVisitors_onPageIndexChange" />
            <SortChange EventHandler="grdVisitors_onSortChange" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="No_Of_Page_VisitsCT">
                ## GetContentDecimal(DataItem.GetMember('AVGPageViews').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="AvgPageDurationCT">
                ## DataItem.GetMember('Average').Value ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DurationCT">
                ## GetContentDecimal(DataItem.GetMember('duration').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderEP">
                <div class="custom-header">
                    <span class="header-text">##_Visitors##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(vpVisitsText, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderAverage">
                <div class="custom-header">
                    <span class="header-text">##_Average##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(VisitorATOPHelpText, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderpagevist">
                <div class="custom-header">
                    <span class="header-text">##_AvgPageViewsperVisit##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(vpAvgPageviews, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderAvgsession">
                <div class="custom-header">
                    <span class="header-text">##_Duration##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(VisitorDurationHelpText, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdVisitorsSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdVisitors.PageCount)
                            ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdVisitors.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdVisitorsPagination">
                ## GetGridPaginationInfo(grdVisitors) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <script type="text/javascript">
        if(document.getElementById("<%=VisitorsChartCallback.ClientID%>") != null && document.getElementById("<%=VisitorsChartCallback.ClientID%>") != undefined) {
            <%=VisitorsChartCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
        }
    </script>
</div>
</asp:Content>

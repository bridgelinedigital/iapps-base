<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Direct.aspx.cs" Inherits="Direct"
    MasterPageFile="~/Statistics/Visitors/VisitorMaster.master" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsReferrersDirect %>"
    Theme="General" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _ReferredPage = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, ReferredPage %>"/>';
        var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
        var _PagesVisit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, PagesVisit %>"/>';
        var _ATOS = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, ATOS %>"/>';
        var _NewVisit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, NewVisit %>"/>';
        var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';

        var item = "";
        var newPage = "0";
        var orderByCol = "Page_Visits";
        var orderBy = "DESC";

        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var typeOfGraph = "2";

        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
                    pointerobj.src = reversedownTooltipImage;
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
    </script>
    <script type="text/javascript">
        function grdDirect_onCallbackComplete(sender, eventArgs) {
            newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            DirectCallback.Callback(callbackArgs);
        }
        function grdDirect_onSortChange(sender, eventArgs) {
            grdDirect.unSelectAll();
            orderByCol = eventArgs.get_column().get_dataField();
            if (eventArgs.get_descending()) {
                orderBy = "DESC";
            }
            else {
                orderBy = "ASC";
            }
            newPage = "0";
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            if (grdDirect.RecordCount != 0) {
                DirectCallback.Callback(callbackArgs);
            }
        }
        
        /** Methods to set the pagination details **/
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdDirect.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdDirect.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }

        function grdDirect_onItemSelect(sender, eventArgs) {
            if (grdDirect.get_table().getRowCount() >= 1 && grdDirect.PageCount != 0) {
                var selectedRow = eventArgs.get_item();
                referrerName = selectedRow.getMember("Referring_URL").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pageviews4 %>'/>", referrerName);
                var callbackArgs = new Array(referrerName, typeOfGraph);
                DirectReferrersCallback.Callback(callbackArgs);
            }
        }
        function grdDirect_onLoad(sender, eventArgs) {
            grdDirect.UnSelectAll();
            if (grdDirect.get_table().getRowCount() >= 1 && grdDirect.PageCount != 0) {
                var firstItem = grdDirect.get_table().getRow(0);
                referrerName = firstItem.getMember("Referring_URL").get_text();
                internalRefferrers = firstItem.getMember("Referring_URL").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pageviews4 %>'/>", internalRefferrers);
            }
            if (grdDirect.get_table().getRowCount() == 0) {
                document.getElementById("legendBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("legendBox").style.visibility = "visible";
            }
        }
        function ChangeGraph(graphType) {
            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 1;
            if (grdDirect.get_table().getRowCount() >= 1 && grdDirect.PageCount != 0) {
                var callbackArgs = new Array(referrerName, typeOfGraph);
                DirectReferrersCallback.Callback(callbackArgs);
            }
            return false;
        }
        //For registering the mouse event for CA Chart
        function DirectReferrersCallback_onCallbackComplete(sender, eventArgs) {
            document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
        }
        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up
            var popupHTML;
            if (typeOfGraph != 1)
                popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            else
                popupHTML = '<asp:localize runat="server" text="<%$ Resources:JSMessages, Hour %>"/>' + FormatDate(point.get_x(), 'H');
            //var popupHTML = point.get_x().split(" ")[0]; 
            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + " ";
                    popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits %>'/>";
                    popupHTML += "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML += "<br/><b>" + pageAuthor + "</b>";
            }
            ddrivetip(popupHTML, '150', '');
        }

        function onDataPointExit(who, what) {
            hideddrivetip();
        }

    </script>
</asp:Content>
<asp:Content ID="contentDirect" ContentPlaceHolderID="visitorContentPlaceHolder" runat="Server">
<div class="report-page">
    <div class="clear-fix">
        <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h2>
        <div class="graph-type clear-fix">
            <div class="form-row">
                <label class="form-label"><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ViewBy %>" /></label>
                <div class="form-value">
                    <select id="graphOption" onchange="ChangeGraph()">
                        <option value="hour">
                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, Hour %>" /></option>
                        <option value="day" selected="selected">
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Day %>" /></option>
                        <option value="week">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Week %>" /></option>
                        <option value="month">
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Month %>" /></option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="graph-info clear-fix">
        <div class="left-section">
            <p><strong><asp:Label ID="lblTotalVisits" runat="server" CssClass="headingLabel"/></strong></p>
            <p><strong><asp:Label ID="lblTotal" runat="server" CssClass="headingLabel"/></strong></p>
        </div>
        <div class="right-section">
            <ul class="legendBox" id="legendBox">
                <li class="firstLegend" id="firstLegend">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, TotalPageviews %>" /></li>
                <li class="secondLegend" id="secondLegend"></li>
            </ul>
        </div>
    </div>
    <div class="chart-container" id="chartContainer" runat="server">
        <ComponentArt:CallBack ID="DirectReferrersCallback" runat="server" OnCallback="DirectReferrersCallback_onCallback">
            <Content>
                <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart">
                    <ClientEvents>
                        <DataPointHover EventHandler="onDataPointHover" />
                        <DataPointExit EventHandler="onDataPointExit" />
                    </ClientEvents>
                </ComponentArtChart:Chart>
                <asp:HiddenField ID="hdnPageHeading" runat="server" />
                <asp:HiddenField ID="hdnAverage" runat="server" />
            </Content>
            <LoadingPanelClientTemplate>
            </LoadingPanelClientTemplate>
            <ClientEvents>
                <CallbackComplete EventHandler="DirectReferrersCallback_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <ComponentArt:Grid SkinID="Default" ID="grdDirect" runat="server" RunningMode="Client"
        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
        PagerInfoClientTemplateId="grdDirectPagination" SliderPopupClientTemplateId="grdDirectSlider"
        Width="100%" PageSize="10">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="500" runat="server" HeadingText="<%$ Resources:GUIStrings, ReferredPage %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="Referring_URL" DataCellCssClass="FirstDataCell"
                        SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" HeadingCellClientTemplateId="sourceHeader" />
                    <ComponentArt:GridColumn Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                        DataField="Page_Visits" IsSearchable="true" DataType="System.Int32" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="Page_VisitsCTHeader" />
                    <ComponentArt:GridColumn Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, PagesVisit %>"
                        DataField="No_Of_Page_Visits" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" Align="Right" DataType="System.Int32" HeadingCellClientTemplateId="No_Of_Page_VisitsCTHeader" />
                    <ComponentArt:GridColumn Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, AverageTimeonSite %>"
                        DataField="Average_Time_On_Site" FixedWidth="true" DataCellCssClass="LastDataCell"
                        SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                        AllowReordering="false" Align="Right" HeadingCellClientTemplateId="Average_Time_On_SiteCTHeader" />
                    <ComponentArt:GridColumn Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, NewVisit %>"
                        DataField="New_Visits" FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                        HeadingCellCssClass="LastHeadingCell" DataCellClientTemplateId="New_VisitsCT"
                        AllowReordering="false" DataType="System.Double" Align="Right" HeadingCellClientTemplateId="New_VisitsCTHeader" />
                    <ComponentArt:GridColumn Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>"
                        DataField="BounceRate" FixedWidth="true" DataCellClientTemplateId="BounceRateCT"
                        DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                        AllowReordering="false" DataType="System.Double" Align="Right" HeadingCellClientTemplateId="BounceRateCTHeader" />
                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ItemSelect EventHandler="grdDirect_onItemSelect" />
            <Load EventHandler="grdDirect_onLoad" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="New_VisitsCT">
                ## GetContentDecimal(DataItem.GetMember('New_Visits').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BounceRateCT">
                ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="sourceHeader">
                <div class="custom-header">
                    <span class="header-text">##_ReferredPage##</span>
                    <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(drSource, 300, '');positiontip(event);"
                        onmouseout="hideddrivetip();" class="help-icon" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="Page_VisitsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_Visits##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="No_Of_Page_VisitsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_PagesVisit##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erPagesVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="Average_Time_On_SiteCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_ATOS##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erAverageTime, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="New_VisitsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_NewVisit##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erNewVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BounceRateCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_BounceRate1##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(erBounceRate, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdDirectSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdDirect.PageCount)
                            ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdDirect.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdDirectPagination">
                ## GetGridPaginationInfo(grdDirect) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <script type="text/javascript">
        <%=DirectReferrersCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>
</div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/Statistics/Visitors/VisitorMaster.master"
    AutoEventWireup="true" Theme="General" Inherits="Statistics_Visitors_OperatingSystems"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsVisitorsOperatingSystems %>"
    CodeBehind="OperatingSystems.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _OperatingSystem = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, OperatingSystem %>"/>';
        var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
        var _PagesVisits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, PagesVisits %>"/>';
        var _ATOS = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, ATOS %>"/>';
        var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';
        var _NewVisit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, NewVisit %>"/>';
        //** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var newPage = "0";
        var orderByCol = "Visits";
        var orderBy = "DESC";

        var typeOfGraph = "2";

        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
                    pointerobj.src = reversedownTooltipImage;
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/

        function grdOperatingSystems_onPageIndexChange(sender, eventArgs) {
            newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            OSChartCallback.Callback(callbackArgs);
        }
        function grdOperatingSystems_onSortChange(sender, eventArgs) {
            grdOperatingSystems.unSelectAll();
            orderByCol = eventArgs.get_column().get_dataField();
            if (eventArgs.get_descending()) {
                orderBy = "DESC";
            }
            else {
                orderBy = "ASC";
            }
            newPage = "0";
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            if (grdOperatingSystems.RecordCount != 0) {
                OSChartCallback.Callback(callbackArgs);
            }
        }
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdOperatingSystems.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdOperatingSystems.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '"  src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '"  src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }

        function grdOperatingSystems_onItemSelect(sender, eventArgs) {
            if (grdOperatingSystems.get_table().getRowCount() >= 1 && grdOperatingSystems.PageCount != 0) {
                var selectedRow = eventArgs.get_item();
                pageId = selectedRow.getMember("OS").get_text();
                pageAuthor = selectedRow.getMember("OS").get_text();
                pageName = selectedRow.getMember("OS").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits1 %>'/>", pageName);
                var callbackArgs = new Array(pageId, pageName, typeOfGraph);
                OSCallback.Callback(callbackArgs);
            }
        }
        function grdOperatingSystems_onLoad(sender, eventArgs) {
            //if(grdOperatingSystems.get_table().getRowCount()>=1 && grdOperatingSystems.PageCount!=0)
            //    {
            //    var firstItem = grdOperatingSystems.get_table().getRow(0);
            //    grdOperatingSystems.select(firstItem, false);
            //    }
            grdOperatingSystems.unSelectAll();
            if (grdOperatingSystems.get_table().getRowCount() >= 1 && grdOperatingSystems.PageCount != 0) {
                var firstItem = grdOperatingSystems.get_table().getRow(0);
                operatingSystem = firstItem.getMember("OS").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits1 %>'/>", operatingSystem);
            }
            if (grdOperatingSystems.get_table().getRowCount() == 0) {
                document.getElementById("legendBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("legendBox").style.visibility = "visible";
            }

        }
        function ChangeGraph() {
            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 1;

            if (grdOperatingSystems.get_table().getRowCount() >= 1 && grdOperatingSystems.PageCount != 0) {
                var callbackArgs = new Array(pageId, pageName, typeOfGraph);
                OSCallback.Callback(callbackArgs);
            }
            return false;
        }
        //For registering the mouse event for CA Chart
        function OSCallback_onCallbackComplete(sender, eventArgs) {
            document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
        }
        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up
            var popupHTML;
            if (typeOfGraph != 1)
                popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            else
                popupHTML = '<asp:localize runat="server" text="<%$ Resources:JSMessages, Hour %>"/>' + FormatDate(point.get_x(), 'H');
            //var popupHTML = point.get_x().split(" ")[0]; 
            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + " ";
                    popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, Visits %>'/>";
                    popupHTML += "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML += "<br/><b>" + pageAuthor + "</b>";
            }
            ddrivetip(popupHTML, '150', '');
        }

        function onDataPointExit(who, what) {
            hideddrivetip();
        }

    </script>
</asp:Content>
<asp:Content ID="contentOS" ContentPlaceHolderID="visitorContentPlaceHolder" runat="Server">
<div class="report-page">
    <div class="clear-fix">
        <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h2>
        <div class="graph-type clear-fix">
            <div class="form-row">
                <label class="form-label"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ViewBy %>" /></label>
                <div class="form-value">
                    <select id="graphOption" onchange="ChangeGraph()">
                        <option value="hour">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Hour %>" /></option>
                        <option value="day" selected="selected">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Day %>" /></option>
                        <option value="week">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Week %>" /></option>
                        <option value="month">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Month %>" /></option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="graph-info clear-fix">
        <div class="left-section">
            <p><strong><asp:Label ID="lblTotalVisits" runat="server" CssClass="headingLabel"/></strong></p>
            <p><strong><asp:Label ID="lblTotal" runat="server" CssClass="headingLabel"/></strong></p>
        </div>
        <div class="right-section">
            <ul class="legendBox" id="legendBox">
                <li class="firstLegend" id="firstLegend">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TotalVisits %>" /></li>
                <li class="secondLegend" id="secondLegend"></li>
            </ul>
        </div>
    </div>
    <div class="chart-container" id="chartContainer" runat="server">
        <ComponentArt:CallBack ID="OSCallback" runat="server" Height="100" Width="920" OnCallback="OSCallback_onCallback">
            <Content>
                <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart">
                    <ClientEvents>
                        <DataPointHover EventHandler="onDataPointHover" />
                        <DataPointExit EventHandler="onDataPointExit" />
                    </ClientEvents>
                </ComponentArtChart:Chart>
                <asp:HiddenField ID="hdnPageHeading" runat="server" />
                <asp:HiddenField ID="hdnAverage" runat="server" />
            </Content>
            <LoadingPanelClientTemplate>
            </LoadingPanelClientTemplate>
            <ClientEvents>
                <CallbackComplete EventHandler="OSCallback_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <ComponentArt:Grid SkinID="Default" ID="grdOperatingSystems" runat="server" RunningMode="Client"
        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
        Width="100%" PageSize="10" SliderPopupClientTemplateId="grdOperatingSystemsSlider"
        PagerInfoClientTemplateId="grdOperatingSystemsPagination">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="435" runat="server" HeadingText="<%$ Resources:GUIStrings, OperatingSystem %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="OS" DataCellCssClass="FirstDataCell"
                        SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" HeadingCellClientTemplateId="cmHeaderOS" Align="Left" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                        DataField="Visits" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="cmHeaderVisit"
                        DataType="System.Int64" Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, PagesVisits %>"
                        DataField="PagesPerVisits" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" DataType="System.Double" HeadingCellClientTemplateId="cmHeaderPagesVisits"
                        Align="Right" DataCellClientTemplateId="PagesPerVisitsCT" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, AverageTimeonSite %>"
                        DataField="AvgTime" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" HeadingCellClientTemplateId="cmHeaderAverageTimeonSite" Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, NewVisit %>"
                        DataField="NewVisits" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" DataType="System.Double" HeadingCellClientTemplateId="cmHeaderNewVisit"
                        DataCellClientTemplateId="NewVisitsCT" Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>"
                        DataField="BounceRate" SortedDataCellCssClass="SortedDataCell" HeadingCellClientTemplateId="cmheaderBouncerate"
                        AllowReordering="false" FixedWidth="true" DataType="System.Double" DataCellClientTemplateId="BounceRateCT"
                        Align="Right" />
                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <%--<PageIndexChange EventHandler="grdOperatingSystems_onPageIndexChange" />--%>
            <%--<SortChange EventHandler="grdOperatingSystems_onSortChange" />--%>
            <ItemSelect EventHandler="grdOperatingSystems_onItemSelect" />
            <Load EventHandler="grdOperatingSystems_onLoad" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NewVisitsCT">
                ## GetContentDecimal(DataItem.GetMember('NewVisits').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BounceRateCT">
                ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="PagesPerVisitsCT">
                ## GetContentDecimal(DataItem.GetMember('PagesPerVisits').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderOS">
                <div class="custom-header">
                    <span class="header-text">##_OperatingSystem##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(osOS, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem, 'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderVisit">
                <div class="custom-header">
                    <span class="header-text">##_Visits##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(osVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem, 'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderPagesVisits">
                <div class="custom-header">
                    <span class="header-text">##_PagesVisits##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(osPagesVisits, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem, 'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderAverageTimeonSite">
                <div class="custom-header">
                    <span class="header-text">##_ATOS##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(osAverageTimeonSite, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem, 'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmheaderBouncerate">
                <div class="custom-header">
                    <span class="header-text">##_BounceRate1##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(osBouncerate, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem, 'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderNewVisit">
                <div class="custom-header">
                    <span class="header-text">##_NewVisit##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(osNewVisit, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem, 'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdOperatingSystemsSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdOperatingSystems.PageCount)
                            ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdOperatingSystems.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdOperatingSystemsPagination">
                ## GetGridPaginationInfo(grdOperatingSystems) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <script type="text/javascript">
        <%=OSCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>
</div>
</asp:Content>

<%@ Page Language="C#" MasterPageFile="~/Statistics/Traffic/TrafficMaster.master"
    AutoEventWireup="true" Theme="General" Inherits="Statistics_Traffic_SinglePageVisits"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsTrafficSinglePageVisits %>" CodeBehind="SinglePageVisits.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';
        var _Exit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Exit %>"/>';
        var _CMDetails = '<asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, CMDetails %>"/>';
        var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
        var _PageVisits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, PageVisits %>"/>';
        var _UniquePageviews = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, UniquePageviews %>"/>';
        var _ofTotal = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, ofTotal %>"/>';
        var _Exits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Exits %>"/>';
        var _ATOS = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, ATOS %>"/>';
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var typeOfGraph = "2";
        var item = "";
        var newPage = "0";
        var orderByCol = "Page_Visits";
        var orderBy = "DESC";
        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
                    pointerobj.src = reversedownTooltipImage;
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
    </script>
    <script type="text/javascript">

        function grdSinglePageVisits_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            grdSinglePageVisits.select
            grdSinglePageVisits.select(eventArgs.get_item());

            var evt = eventArgs.get_event();
            cmSinglePageVisits.showContextMenuAtEvent(evt, eventArgs.get_item());

        }
        function cmSinglePageVisits_ItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            switch (selectedMenu) {
                case "ViewStatistics":
                    var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                    viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', 'width=750px,height=635px,center=1,resize=0,scrolling=1');
                    viewPopupWindow.onclose = function () {
                        var a = document.getElementById('Content');
                        var ifr = a.getElementsByTagName("iframe");
                        window.frames[ifr[0].name].location.replace("about:blank");
                        return true;
                    }
                    break;
                case "ViewAnalysis":
                    window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                    break;
                case "ViewNewWindow":
                    window.open(analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text);
                    break;
                case "ViewOverlay":
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?PageState=Overlay&LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenBySiteAndProduct(cmsProductId, siteId, siteName);
                    break;
                case "ViewSiteEditor":
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenBySiteAndProduct(cmsProductId, siteId, siteName);
                    break;
                case "AssignIndexTerms":
                    var pageId = item.getMember("Original_Id").get_text().replace("{", "").replace("}", "");
                    OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                    break;
                case "MenuEmailAuthor":
                    var email = item.GetMember('Author_Email').Text;
                    location.href = "mailto:" + email + "";
                    break;
            }
        }

        
        /** Methods to get set the tooltip **/
        function CreateTip(dataItemObject) {
            var authorName = dataItemObject.GetMember('Author').Text;
            authorName = authorName.replace(new RegExp("'", "g"), "\'");

            var publisherName = dataItemObject.GetMember('Published_By').Text;
            publisherName = publisherName.replace(new RegExp("'", "g"), "\'");

            var dateTimePublished = dataItemObject.GetMember('Published_Date').Text;
            dateTimePublished = dateTimePublished.replace(new RegExp("'", "g"), "\'");

            var navigationHierarchy = dataItemObject.GetMember('Menu').Text;
            navigationHierarchy = navigationHierarchy.replace(new RegExp("'", "g"), "\'");

            var strHTML = "";
            var imgTag = "";
            strHTML += "<div>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, Author %>'/></h5>";
            strHTML += "<p>" + authorName + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, PublishedBy %>'/></h5>";
            strHTML += "<p>" + publisherName + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, DateTimePublished %>'/></h5>";
            strHTML += "<p>" + dateTimePublished + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, NavigationHierarchy %>'/></h5>";
            strHTML += "<p>" + WrapTooltipText(navigationHierarchy) + "</p>";
            strHTML += "</div>";
            strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'');\" onmouseout='hideddrivetip();' src='../../App_Themes/General/images/icon-tooltip.gif' />";
            return strHTML;
        }
        /** Methods to get set the tooltip **/
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdSinglePageVisits.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdSinglePageVisits.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }
        function grdSinglePageVisits_onLoad(sender, eventArgs) {
            //grdSinglePageVisits.sort(1,true);    
            //    var firstItem = grdTopPages.get_table().getRow(0);
            //    pageName=firstItem.getMember("Title").get_text();    
            //    document.getElementById("firstLegend").innerHTML = pageName + " Single Page Visits";
            grdSinglePageVisits.unSelectAll();
            if (grdSinglePageVisits.get_table().getRowCount() >= 1 && grdSinglePageVisits.PageCount != 0) {
                var firstItem = grdSinglePageVisits.get_table().getRow(0);
                pageId = firstItem.getMember("Original_Id").get_text();
                pageAuthor = firstItem.getMember("Author").get_text();
                pageName = firstItem.getMember("Title").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, SinglePageVisits %>'/>", pageName);
            }
            if (grdSinglePageVisits.get_table().getRowCount() == 0) {
                document.getElementById("legendBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("legendBox").style.visibility = "visible";
            }

        }
        function grdSinglePageVisits_onItemSelect(sender, eventArgs) {
            var selectedRow = eventArgs.get_item();
            pageId = selectedRow.getMember("Original_Id").get_text();
            pageAuthor = selectedRow.getMember("Author").get_text();
            pageName = selectedRow.getMember("Title").get_text();
            document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, SinglePageVisits %>'/>", pageName);
            var callbackArgs = new Array(pageId, pageName, typeOfGraph, $("select.visit-type").val());
            SinglePageVisitsCallback.Callback(callbackArgs);
        }
        function ChangeGraph() {
            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 1;
            if (grdSinglePageVisits.get_table().getRowCount() >= 1 && grdSinglePageVisits.PageCount != 0) {
                var callbackArgs = new Array(pageId, pageName, typeOfGraph, $("select.visit-type").val());
                SinglePageVisitsCallback.Callback(callbackArgs);
            }
            return false;
        }
        function SinglePageVisitsCallback_onCallbackComplete(sender, eventArgs) {
            document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
        }
        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up
            var popupHTML;
            if (typeOfGraph != 1)
                popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            else
                popupHTML = '<asp:localize runat="server" text="<%$ Resources:JSMessages, Hour %>"/>' + FormatDate(point.get_x(), 'H');
            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + " ";
                    popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, SinglePageVisits1 %>'/>";
                    popupHTML += "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML = "<asp:localize runat='server' text='<%$ Resources:JSMessages, PublishedOn %>'/>";
                popupHTML += " " + FormatDate(point.get_x().split(" ")[0], DateFormat);
                popupHTML += "<br/><b>";
                popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, By %>'/>";
                popupHTML += " " + pageAuthor + "</b>";
            }
            ddrivetip(popupHTML, '150', '');
        }

        function onDataPointExit(who, what) {
            hideddrivetip();
        }
    </script>
</asp:Content>
<asp:Content ID="contentSingePageVisits" ContentPlaceHolderID="trafficContentPlaceHolder" runat="Server">
<div class="report-page">
    <div class="clear-fix">
        <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h2>
        <div class="graph-type clear-fix">
            <div class="columns">
                <div class="form-row">
                    <label class="form-label"><%= GUIStrings.Traffic %></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlVisitType" AutoPostBack="true"  runat="server" CssClass="visit-type">
                            <asp:ListItem Text="<%$ Resources:GUIStrings, All %>" Value="-1" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, Paid %>" Value="0" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, NonPaid %>" Value="1" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="form-row">
                    <label class="form-label"><asp:localize runat="server" text="<%$ Resources:GUIStrings, ViewBy %>"/></label>
                    <div class="form-value">
                        <select id="graphOption" onchange="ChangeGraph()">
                            <option value="hour"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Hour %>"/></option>
                            <option value="day" selected="selected"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Day %>"/></option>
                            <option value="week"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Week %>"/></option>
                            <option value="month"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Month %>"/></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="graph-info clear-fix">
        <div class="left-section">
            <p><strong><asp:Label ID="lblPagesTotal" runat="server" CssClass="headingLabel"/></strong></p>
            <p><strong><asp:Label ID="lblTotal" runat="server" CssClass="headingLabel"/></strong></p>
        </div>
        <div class="right-section">
            <ul class="legendBox" id="legendBox">
                <li class="firstLegend" id="firstLegend"><asp:localize runat="server" text="<%$ Resources:GUIStrings, TotalSinglePageVisits %>"/></li>
                <li class="secondLegend" id="secondLegend"><asp:localize runat="server" text="<%$ Resources:GUIStrings, SinglePageVisits %>"/></li>
                <li class="thirdLegend" id="thirdLegend"><asp:localize runat="server" text="<%$ Resources:GUIStrings, PublishDate %>"/></li>
            </ul>
        </div>
    </div>
    <div class="chart-container" id="chartContainer" runat="server" style="height: 370px;">
        <ComponentArt:CallBack ID="SinglePageVisitsCallback" runat="server" Height="100"
            Width="920" OnCallback="SinglePageVisitsCallback_onCallback">
            <Content>
                <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart">
                    <ClientEvents>
                        <DataPointHover EventHandler="onDataPointHover" />
                        <DataPointExit EventHandler="onDataPointExit" />
                    </ClientEvents>
                </ComponentArtChart:Chart>
                <asp:HiddenField ID="hdnPageHeading" runat="server" />
                <asp:HiddenField ID="hdnAverage" runat="server" />
            </Content>
            <LoadingPanelClientTemplate>
            </LoadingPanelClientTemplate>
            <ClientEvents>
                <CallbackComplete EventHandler="SinglePageVisitsCallback_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <ComponentArt:Grid SkinID="Default" ID="grdSinglePageVisits" runat="server" RunningMode="Client"
        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" Width="100%" PageSize="10"
        SliderPopupClientTemplateId="grdSinglePageVisitsSlider" PagerInfoClientTemplateId="grdSinglePageVisitsPagination">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Original_Id" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="600" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>" HeadingCellCssClass="FirstHeadingCell"
                        DataField="Title" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" />
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Exits %>" DataField="Bounce" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="Page_VisitsCTHeader" />
                    <ComponentArt:GridColumn Width="135" runat="server" HeadingText="<%$ Resources:GUIStrings, ofTotal %>" DataField="TotalBounce"
                        FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                        HeadingCellClientTemplateId="VisitCTHeader" HeadingCellCssClass="LastHeadingCell"
                        AllowReordering="false" Align="Right" DataCellClientTemplateId="TotalBounceCT" />
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, AverageTimeonSite %>" DataField="Page_Dwell_Time"
                        FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                        HeadingCellCssClass="LastHeadingCell" AllowReordering="false" Align="Right" HeadingCellClientTemplateId="Page_Dwell_TimeCTHeader" />
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, CMDetails %>" DataField="Author"
                        FixedWidth="true" AllowSorting="false" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                        HeadingCellCssClass="LastHeadingCell" AllowReordering="false" DataCellClientTemplateId="CMHoverSPV"
                        HeadingCellClientTemplateId="cmHeaderSPV" Align="Center" />
                    <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, Site %>" DataField="SiteTitle" Visible="true" Width="150"/>
                    <ComponentArt:GridColumn DataField="Published_By" Visible="false" />
                    <ComponentArt:GridColumn DataField="Published_Date" Visible="false" />
                    <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                    <ComponentArt:GridColumn DataField="Original_Id" Visible="false" />
                    <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ItemSelect EventHandler="grdSinglePageVisits_onItemSelect" />
            <Load EventHandler="grdSinglePageVisits_onLoad" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="TotalBounceCT">
                ## GetContentDecimal(DataItem.GetMember('TotalBounce').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="Page_VisitsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_Exits##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(spvPageViews, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="VisitCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_ofTotal##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(svBounceRate, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="Unique_Page_VisitsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_UniquePageviews##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(spvUniquePageViews, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="Page_Dwell_TimeCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_ATOS##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(spvTimeOnPage, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cmHeaderSPV">
                <div class="custom-header">
                    <span class="header-text">##_CMDetails##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(svCMDetailsText, 300, '');"
                        onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CMHoverSPV">
                ## CreateTip(DataItem) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdSinglePageVisitsSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdSinglePageVisits.PageCount) ##</span> 
                        <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdSinglePageVisits.RecordCount) ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdSinglePageVisitsPagination">
                ## GetGridPaginationInfo(grdSinglePageVisits) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ClientEvents>
            <ContextMenu EventHandler="grdSinglePageVisits_onContextMenu" />
        </ClientEvents>
    </ComponentArt:Grid>
    <ComponentArt:Menu SkinID="ContextMenu" ID="cmSinglePageVisits" runat="server">
        <Items>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>" ID="ViewStatistics">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewInboundOutboundAnalysis %>" ID="ViewAnalysis">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>" ID="ViewNewWindow">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>" ID="ViewSiteEditor">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinOverlayMode %>" ID="ViewOverlay">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>" ID="AssignIndexTerms">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="MenuEmailAuthor" runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="cmSinglePageVisits_ItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <script type="text/javascript">
        <%=SinglePageVisitsCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>
</div>
</asp:Content>

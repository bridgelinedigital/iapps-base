<%@ Page Language="C#" MasterPageFile="~/Statistics/Traffic/TrafficMaster.master"
    AutoEventWireup="true" Theme="General" Inherits="Statistics_Content_Overview"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsTrafficOverview %>"
    CodeBehind="Overview.aspx.cs" %>

<%@ Register TagPrefix="ctlTopPages" TagName="TopPages" Src="~/UserControls/Statistics/Traffic/TopPagesReport.ascx" %>
<%@ Register TagPrefix="ctlTopEntryPages" TagName="TopEntryPages" Src="~/UserControls/Statistics/Traffic/TopEntryPages.ascx" %>
<%@ Register TagPrefix="ctlTopExitPages" TagName="TopExitPages" Src="~/UserControls/Statistics/Traffic/TopExitPages.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <!-- Tooltip Script -->
    <script type="text/javascript">
        var isOverview = true;
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";

        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor, imgSrc, revImgSrc, downImgSrc) {
            //document.getElementById("dhtmlpointer").src = imgSrc;
            //tooltipImage = imgSrc;
            //reverseTooltipImage = revImgSrc;
            //downTooltipImage = downImgSrc;
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                var nondefaultpos = false;
                pointerobj.src = tooltipImage;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
    </script>
</asp:Content>
<asp:Content ID="overviewContent" ContentPlaceHolderID="trafficContentPlaceHolder" runat="Server">
<div class="page-sub-header clear-fix" style="margin-top: 0;">
    <h2><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TopPages %>" /></h2>
    <span>[&nbsp;<asp:HyperLink ID="viewTopPagesHpl" runat="server" NavigateUrl="~/Statistics/Traffic/TopPagesTimeSeries.aspx"
        Text="<%$ Resources:GUIStrings, ViewFullReport %>"></asp:HyperLink>&nbsp;]</span>
</div>
<ctlTopPages:TopPages ID="cntlTopPages" runat="server" />
<div class="page-sub-header clear-fix">
    <h2><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TopEntryPages %>" /></h2>
    <span>[&nbsp;<asp:HyperLink ID="viewTopEntryPagesHpl" runat="server" NavigateUrl="~/Statistics/Traffic/EntryPages.aspx"
        Text="<%$ Resources:GUIStrings, ViewFullReport %>"></asp:HyperLink>&nbsp;]</span>
</div>
<ctlTopEntryPages:TopEntryPages ID="cntlTopEntryPages" runat="server" />
<div class="page-sub-header clear-fix">
    <h2><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TopExitPages %>" /></h2>
    <span>[&nbsp;<asp:HyperLink ID="viewTopExitPagesHpl" runat="server" NavigateUrl="~/Statistics/Traffic/TopExitPages.aspx"
        Text="View Full Report"></asp:HyperLink>&nbsp;]</span>
</div>
<ctlTopExitPages:TopExitPages ID="cntlTopExitPages" runat="server" />
</asp:Content>

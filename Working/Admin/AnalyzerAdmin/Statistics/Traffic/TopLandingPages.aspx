<%@ Page Language="C#" MasterPageFile="~/Statistics/Traffic/TrafficMaster.master"
    AutoEventWireup="true" Theme="General" Inherits="Statistics_Content_TopLandingPages"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsTrafficTopLandingPages %>" CodeBehind="TopLandingPages.aspx.cs" %>

<asp:Content ID="contentTopLandingPages" ContentPlaceHolderID="trafficContentPlaceHolder"
    runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';
        var _Exit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Exit %>"/>';
        var _CMDetails = '<asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, CMDetails %>"/>';
        var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
        var _PageVisits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, PageVisits %>"/>';
        var _UniquePageviews = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, UniquePageviews %>"/>';
        var _ofTotal = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, ofTotal %>"/>';
        var _TimeonPage = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, TimeonPage %>"/>';
        var _Pageviews1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Pageviews1 %>"/>';
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var reverseTooltipImage;
        var downTooltipImage;
        var tooltipImage;
        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).
        var authorName;
        var item;
        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor, imgSrc, revImgSrc, downImgSrc) {
            //document.getElementById("dhtmlpointer").src = imgSrc;
            tooltipImage = imgSrc;
            reverseTooltipImage = revImgSrc;
            downTooltipImage = downImgSrc;
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
    </script>

    <script type="text/javascript">
        function grdTopLandingPages_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            grdTopLandingPages.select(eventArgs.get_item());
            var evt = eventArgs.get_event();
            cmTopLandingPages.showContextMenuAtEvent(evt, eventArgs.get_item());

        }
        function cmTopLandingPages_ItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            switch (selectedMenu) {
                case "ViewStatistics":
                    var PopupUrl = analyticsUrl + "/popups/PageDetailStatistics.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                    viewPopupWindow = dhtmlmodal.open('Content', 'iframe', PopupUrl, '', 'width=750px,height=660px,center=1,resize=0,scrolling=1');
                    viewPopupWindow.onclose = function() {
                        var a = document.getElementById('Content');
                        var ifr = a.getElementsByTagName("iframe");
                        window.frames[ifr[0].name].location.replace("about:blank");
                        return true;
                    }
                    break;
                case "ViewAnalysis":
                    window.location.href = analyticsUrl + "/Navigation/InboundOutboundAnalysis.aspx?reqPageId=" + item.GetMember('Original_Id').Text;
                    break;
                case "ViewNewWindow":
                    window.open(analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text);
                    break;
                case "ViewSiteEditor":
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    window.location.href = analyticsPublicSiteUrl + "/" + item.GetMember('Menu').Text + currentUrl + "&Token=" + GetTokenBySiteAndProduct(cmsProductId, siteId, siteName);
                    break;
                case "AssignIndexTerms":
                    var pageId = item.getMember("Original_Id").get_text().replace("{", "").replace("}", "");
                    OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=8&ObjectId=" + pageId);
                    break;
                case "MenuEmailAuthor":
                    var email = item.GetMember('Author_Email').Text;
                    location.href = "mailto:" + email + "";
                    break;
            }
        }
       
        /** Methods to get set the tooltip **/
        function CreateTip(dataItemObject) {
            authorName = dataItemObject.GetMember('Author').Text;
            authorName = authorName.replace(new RegExp("'", "g"), "\'");

            var publisherName = dataItemObject.GetMember('Published_By').Text;
            publisherName = publisherName.replace(new RegExp("'", "g"), "\'");

            var dateTimePublished = dataItemObject.GetMember('Published_Date').Text;
            dateTimePublished = dateTimePublished.replace(new RegExp("'", "g"), "\'");

            var navigationHierarchy = dataItemObject.GetMember('Menu').Text;
            navigationHierarchy = navigationHierarchy.replace(new RegExp("'", "g"), "\'");

            var strHTML = "";
            var imgTag = "";
            strHTML += "<div class=tooltip>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, Author %>'/></h5>";
            strHTML += "<p>" + authorName + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, PublishedBy %>'/></h5>";
            strHTML += "<p>" + publisherName + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, DateTimePublished %>'/></h5>";
            strHTML += "<p>" + dateTimePublished + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, NavigationHierarchy %>'/></h5>";
            strHTML += "<p class=lastRow>" + WrapTooltipText(navigationHierarchy) + "</p>";
            strHTML += "</div>";
            strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'','../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);\" onmouseout='hideddrivetip();' src='../../App_Themes/General/images/icon-tooltip.gif' />";
            return strHTML;
        }
        /** Methods to get set the tooltip **/
        function grdTopLandingPages_onPageIndexChange(sender, eventArgs) {
            var newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage);
            TopLandingPagesChartCallback.Callback(callbackArgs);
        }
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdTopLandingPages.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdTopLandingPages.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" style="float:left;margin:5px;" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" style="float:left;margin:5px;" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }
        function grdTopLandingPages_onLoad(sender, eventArgs) {
            grdTopLandingPages.sort(1, true);
        }
    </script>

    <div class="dataAreaContent">
        <div class="subSectionContent">
            <h5>
                <asp:Label runat="server" ID="lblHeading"></asp:Label></h5>
            <div class="clearFix">
                &nbsp;</div>
            <div class="chartContainer" runat="server" id="chartContainer">
                <ComponentArt:CallBack ID="TopLandingPagesChartCallback" runat="server" Height="100"
                    Width="350" OnCallback="TopLandingPagesChartCallback_onCallback">
                    <Content>
                        <ComponentArtChart:Chart ID="TopLandingPagesChart" RenderingPrecision="0.1" Width="920px"
                            Height="328px" runat="server" SelectedPaletteName="Earth">
                            <View Kind="TwoDimensional">
                            </View>
                            <GradientStyles>
                                <ComponentArtChart:GradientStyle StartColor="#ffc932" EndColor="#ffe9a8" GradientKind="Vertical" />
                            </GradientStyles>
                            <Palettes>
                                <ComponentArtChart:Palette AxisLineColor="#b0b0b0" BackgroundColor="#ffffff" BackgroundEndingColor="#ffffff"
                                    CoodinateLabelFontColor="#848484" CoordinateLineColor="#dedede" CoordinatePlaneColor="#ffffff"
                                    CoordinatePlaneSecondaryColor="#ffffff" DataLabelFontColor="#848484" FrameColor="#ffffff"
                                    FrameFontColor="#848484" FrameSecondaryColor="#ffffff" LegendBackgroundColor="#ffffff"
                                    LegendBorderColor="#dedede" LegendFontColor="#848484" Name="Earth" TitleFontColor="#848484"
                                    TwoDObjectBorderColor="#dedede" PrimaryColors="ffc932" SecondaryColors="ffe9a8" />
                            </Palettes>
                        </ComponentArtChart:Chart>
                    </Content>
                    <LoadingPanelClientTemplate>
                    </LoadingPanelClientTemplate>
                </ComponentArt:CallBack>
            </div>
            <ComponentArt:Grid SkinID="Default" ID="grdTopLandingPages" runat="server" RunningMode="Client"
                EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" PagerInfoClientTemplateId="grdTopLandingPagesPagination"
                SliderPopupClientTemplateId="grdTopLandingPagesSlider" Width="935" PageSize="10">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Original_Id" ShowTableHeading="false" ShowSelectorCells="false"
                        RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                        HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                        HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                        HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                        SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                        <Columns>
                            <ComponentArt:GridColumn Width="380" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>" HeadingCellCssClass="FirstHeadingCell"
                                DataField="Title" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                                IsSearchable="true" AllowReordering="false" FixedWidth="true" />
                            <ComponentArt:GridColumn Width="118" runat="server" HeadingText="<%$ Resources:GUIStrings, Pageviews1 %>" DataField="PageViews"
                                IsSearchable="true" HeadingCellClientTemplateId="entrancesHeaderTL" SortedDataCellCssClass="SortedDataCell"
                                AllowReordering="false" FixedWidth="true" Align="Right" DataType="System.Int64" />
                            <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, UniquePageviews %>" DataField="UniquePageViews"
                                SortedDataCellCssClass="SortedDataCell" DataType="System.Int64" AllowReordering="false"
                                FixedWidth="true" HeadingCellClientTemplateId="bouncesHeaderTL" Align="Right" />
                            <ComponentArt:GridColumn Width="130" runat="server" HeadingText="<%$ Resources:GUIStrings, TimeonPage %>" DataField="TimeOnPage"
                                FixedWidth="true" SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                                Align="Right" AllowReordering="false" HeadingCellClientTemplateId="bounceRateHeaderTL" />
                            <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, CMDetails %>" DataField="Author"
                                FixedWidth="true" AllowSorting="false" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                HeadingCellCssClass="LastHeadingCell" AllowReordering="false" HeadingCellClientTemplateId="cmHeaderTL"
                                DataCellClientTemplateId="CMHoverTL" Align="Center" />
                            <ComponentArt:GridColumn DataField="Published_By" Visible="false" />
                            <ComponentArt:GridColumn DataField="Published_Date" Visible="false" />
                            <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                            <ComponentArt:GridColumn DataField="Original_Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientEvents>
                    <PageIndexChange EventHandler="grdTopLandingPages_onPageIndexChange" />
                </ClientEvents>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="entrancesHeaderTL">
                        <div class="customHeader rightAligned">
                            ## getSortImageHtml(DataItem,'leftAligned') ## <span>##_Pageviews1##</span>
                            <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(lpPageviewsText, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="bouncesHeaderTL">
                        <div class="customHeader rightAligned">
                            ## getSortImageHtml(DataItem,'leftAligned') ## <span>##_UniquePageviews##</span>
                            <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(lpUniquePageviewsText, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="bounceRateHeaderTL">
                        <div class="customHeader rightAligned">
                            ## getSortImageHtml(DataItem,'leftAligned') ## <span>##_TimeonPage##</span>
                            <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(lpTimeOnPageText, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="cmHeaderTL">
                        <div class="customHeader">
                            <span>##_CMDetails##</span>
                            <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(lpCMDetailsText, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="CMHoverTL">
                        ## CreateTip(DataItem) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdTopLandingPagesSlider">
                        <div class="SliderPopup">
                            <h5>
                                ## DataItem.GetMember(0).Value ##</h5>
                            <p>
                                ## DataItem.GetMember(1).Value ##</p>
                            <p>
                                ## DataItem.GetMember(2).Value ##</p>
                            <p>
                                ## DataItem.GetMember(3).Value ##</p>
                            <p>
                                ## DataItem.GetMember(4).Value ##</p>
                            <p class="paging">
                                <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdTopLandingPages.PageCount) ##</span>
                                <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdTopLandingPages.RecordCount) ##</span>
                            </p>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdTopLandingPagesPagination">
                        ## GetGridPaginationInfo(grdTopLandingPages) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
                <ClientEvents>
                    <ContextMenu EventHandler="grdTopLandingPages_onContextMenu" />
                    <Load EventHandler="grdTopLandingPages_onLoad" />
                </ClientEvents>
            </ComponentArt:Grid>
            <ComponentArt:Menu SkinID="ContextMenu" ID="cmTopLandingPages" runat="server">
                <Items>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageDetailStatistics %>" ID="ViewStatistics">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewInboundOutboundAnalysis %>" ID="ViewAnalysis">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinNewWindow %>" ID="ViewNewWindow">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>" ID="ViewSiteEditor">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, AssignIndexTerms %>" ID="AssignIndexTerms">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem ID="MenuEmailAuthor" runat="server" Text="<%$ Resources:GUIStrings, EmailAuthor %>">
                    </ComponentArt:MenuItem>
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="cmTopLandingPages_ItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
        </div>
    </div>

    <script type="text/javascript">
        window.cart_menu_zindexbase = 99999;
        <%=TopLandingPagesChartCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>

</asp:Content>

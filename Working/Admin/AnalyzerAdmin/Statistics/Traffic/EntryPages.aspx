<%@ Page Language="C#" MasterPageFile="~/Statistics/Traffic/TrafficMaster.master"
    Theme="General" AutoEventWireup="true" Inherits="Statistics_Traffic_EntryPage"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsTrafficEntryPages %>"
    CodeBehind="EntryPages.aspx.cs" %>
<%@ Register TagPrefix="ctlTopEntryPages" TagName="TopEntryPages" Src="~/UserControls/Statistics/Traffic/TopEntryPages.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <!-- Tooltip Script -->
    <script type="text/javascript">
        var isOverview = false;
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";

        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                else {
                    tipobj.style.width = "auto";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
                    pointerobj.src = reversedownTooltipImage;
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }

        function EntryPages_GetVisitType() {
            var control = document.getElementById('<%= ddlVisitType.ClientID %>');
            var selectedvalue = control.options[control.selectedIndex].value;
            return selectedvalue;
        }

        function ChangeGraph() {
            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 1;

            //    typeOfGraph = graphType;
            if (grdEntryPages.get_table().getRowCount() >= 1 && grdEntryPages.PageCount != 0) {
                var callbackArgs = new Array(pageId, pageName, typeOfGraph, EntryPages_GetVisitType());
                if (isOverview != null && isOverview != true) {
                    EntryPagesCallback.Callback(callbackArgs);
                }
            }
            return false;
        }
        function EntryPagesCallback_onCallbackComplete(sender, eventArgs) {
            document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up DateToolTip.replace('{0}', FormatDate(date,DateFormat)); 
            var popupHTML;
            if (typeOfGraph != 1)
                popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            else
                popupHTML = '<asp:localize runat="server" text="<%$ Resources:JSMessages, Hour %>"/>' + FormatDate(point.get_x(), 'H');

            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + " ";
                    popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, EntryPages %>'/>";
                    popupHTML += "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Published %>'/>";
                popupHTML += " " + FormatDate(point.get_x().split(" ")[0], DateFormat);
                popupHTML += "<br/>";
                popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, By %>'/>";
                popupHTML += " " + pageAuthor;
            }
            ddrivetip(popupHTML, '150', '');
        }

        function onDataPointExit(who, what) {
            hideddrivetip();
        }
    </script>
</asp:Content>
<asp:Content ID="contentEntryPage" ContentPlaceHolderID="trafficContentPlaceHolder" runat="Server">
<div class="report-page">
    <div class="clear-fix">
        <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h2>
        <div class="graph-type clear-fix">
            <div class="columns">
                <div class="form-row">
                    <label class="form-label"><%= GUIStrings.Traffic %></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlVisitType" AutoPostBack="true"  runat="server">
                            <asp:ListItem Text="<%$ Resources:GUIStrings, All %>" Value="-1" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, Paid %>" Value="0" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, NonPaid %>" Value="1" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="form-row">
                    <label class="form-label"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ViewBy %>" /></label>
                    <div class="form-value">
                        <select id="graphOption" onchange="ChangeGraph()">
                            <option value="hour">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Hour %>" /></option>
                            <option value="day" selected="selected">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Day %>" /></option>
                            <option value="week">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Week %>" /></option>
                            <option value="month">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Month %>" /></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="graph-info clear-fix">
        <div class="left-section">
            <p><strong><asp:Label ID="lblPagesTotal" runat="server" CssClass="headingLabel"/></strong></p>
            <p><strong><asp:Label ID="lblTotal" runat="server" CssClass="headingLabel"/></strong></p>
        </div>
        <div class="right-section">
            <ul class="legendBox" id="legendBox">
                <li class="firstLegend" id="firstLegend">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TotalEntryPages %>" /></li>
                <li class="secondLegend" id="secondLegend"></li>
                <li class="thirdLegend" id="thirdLegend">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PublishDate %>" /></li>
            </ul>
        </div>
    </div>
    <div class="chart-container" runat="server" id="chartContainer">
        <ComponentArt:CallBack ID="EntryPagesCallback" runat="server" Height="100" Width="920"
            OnCallback="EntryPagesCallback_onCallback">
            <Content>
                <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart">
                    <ClientEvents>
                        <DataPointHover EventHandler="onDataPointHover" />
                        <DataPointExit EventHandler="onDataPointExit" />
                    </ClientEvents>
                </ComponentArtChart:Chart>
                <asp:HiddenField ID="hdnPageHeading" runat="server" />
                <asp:HiddenField ID="hdnAverage" runat="server" />
            </Content>
            <LoadingPanelClientTemplate>
            </LoadingPanelClientTemplate>
            <ClientEvents>
                <CallbackComplete EventHandler="EntryPagesCallback_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <ctlTopEntryPages:TopEntryPages ID="cntlTopEntryPages" runat="server"></ctlTopEntryPages:TopEntryPages>
    <script type="text/javascript">
        <%=EntryPagesCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>
</div>
</asp:Content>
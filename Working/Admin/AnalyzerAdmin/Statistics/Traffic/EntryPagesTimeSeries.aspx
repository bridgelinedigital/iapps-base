<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EntryPagesTimeSeries.aspx.cs"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsTrafficPages %>"
    Inherits="EntryPagesTimeSeries" MasterPageFile="~/Statistics/Traffic/TrafficMaster.master"
    Theme="General" %>

<asp:Content ID="contentEntryPages" ContentPlaceHolderID="trafficContentPlaceHolder"
    runat="Server">
    <!-- Tooltip Script -->
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _BounceRate1 = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, BounceRate1 %>"/>';
        var _Exit = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Exit %>"/>';
        var _CMDetails = '<asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, CMDetails %>"/>';
        var _Visits = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Visits %>"/>';
        var isOverview = false;
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var reverseTooltipImage;
        var downTooltipImage;
        var tooltipImage;
        var typeOfGraph = "2";
        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor, imgSrc, revImgSrc, downImgSrc) {
            //document.getElementById("dhtmlpointer").src = imgSrc;
            tooltipImage = imgSrc;
            reverseTooltipImage = revImgSrc;
            downTooltipImage = downImgSrc;
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
        /** Methods to get set the tooltip **/
        function CreateTip(dataItemObject) {
            var authorName = dataItemObject.GetMember('Author').Text;
            authorName = authorName.replace(new RegExp("'", "g"), "\'");

            var publisherName = dataItemObject.GetMember('Published_By').Text;
            publisherName = publisherName.replace(new RegExp("'", "g"), "\'");

            var dateTimePublished = dataItemObject.GetMember('Published_Date').Text;
            dateTimePublished = dateTimePublished.replace(new RegExp("'", "g"), "\'");

            var navigationHierarchy = dataItemObject.GetMember('Menu').Text;
            navigationHierarchy = navigationHierarchy.replace(new RegExp("'", "g"), "\'");

            var strHTML = "";
            var imgTag = "";
            strHTML += "<div class=tooltip>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, Author %>'/></h5>";
            strHTML += "<p>" + authorName + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, PublishedBy %>'/></h5>";
            strHTML += "<p>" + publisherName + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, DateTimePublished %>'/></h5>";
            strHTML += "<p>" + dateTimePublished + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, NavigationHierarchy %>'/></h5>";
            strHTML += "<p>" + navigationHierarchy + "</p>";
            strHTML += "</div>";
            strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'','../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);\" onmouseout='hideddrivetip();' src='../../App_Themes/General/images/icon-tooltip.gif' />";
            return strHTML;
        }
        /** Methods to get set the tooltip **/
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdEntryPages.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdEntryPages.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" style="float:left;margin:5px;" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" style="float:left;margin:5px;" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }
        function grdEntryPages_onItemSelect(sender, eventArgs) {
            var selectedRow = eventArgs.get_item();
            pageId = selectedRow.getMember("Original_Id").get_text();
            pageAuthor = selectedRow.getMember("Author").get_text();
            var callbackArgs = new Array(pageId, typeOfGraph);
            EntryPagesCallback.Callback(callbackArgs);
        }
        function ChangeGraph(graphType) {
            typeOfGraph = graphType;
            var callbackArgs = new Array(pageId, typeOfGraph);
            EntryPagesCallback.Callback(callbackArgs);
            return false;
        }
        //For registering the mouse event for CA Chart
        registerMouse();
        function EntryPagesCallback_onCallbackComplete(sender, eventArgs) {
            document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
        }
    </script>
    <div class="dataAreaContent">
        <div class="subSectionContent">
            <h5>
                <asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h5>
            <h5 style="clear: both;">
                <asp:Label ID="lblTotal" runat="server" CssClass="headingLabel"></asp:Label></h5>
            <div style="padding-left: 7px; clear: both;">
                <label>
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, By %>" />&nbsp;</label>
                <asp:LinkButton ID="lbHour" runat="server" Text="<%$ Resources:GUIStrings, Hour %>"
                    OnClientClick="return ChangeGraph('1');"></asp:LinkButton>
                &nbsp;&nbsp;<label>|</label>&nbsp;&nbsp;
                <asp:LinkButton ID="lbDay" runat="server" Text="<%$ Resources:GUIStrings, Day %>"
                    OnClientClick="return ChangeGraph('2');"></asp:LinkButton>
                &nbsp;&nbsp;<label>|</label>&nbsp;&nbsp;
                <asp:LinkButton ID="lbWeek" runat="server" Text="<%$ Resources:GUIStrings, Week %>"
                    OnClientClick="return ChangeGraph('3');"></asp:LinkButton>
                &nbsp;&nbsp;<label>|</label>&nbsp;&nbsp;
                <asp:LinkButton ID="lbMonth" runat="server" Text="<%$ Resources:GUIStrings, Month %>"
                    OnClientClick="return ChangeGraph('4');"></asp:LinkButton>
            </div>
            <div class="clearFix">
                &nbsp;</div>
            <div class="chartContainer" id="chartContainer" runat="server">
                <ComponentArt:CallBack ID="EntryPagesCallback" runat="server" Height="100" Width="940"
                    OnCallback="EntryPagesCallback_onCallback">
                    <Content>
                        <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart">
                            <ClientEvents>
                                <DataPointHover EventHandler="onDataPointHover" />
                                <DataPointExit EventHandler="onDataPointExit" />
                            </ClientEvents>
                        </ComponentArtChart:Chart>
                        <asp:HiddenField ID="hdnPageHeading" runat="server" />
                        <asp:HiddenField ID="hdnAverage" runat="server" />
                    </Content>
                    <LoadingPanelClientTemplate>
                    </LoadingPanelClientTemplate>
                    <ClientEvents>
                        <CallbackComplete EventHandler="EntryPagesCallback_onCallbackComplete" />
                    </ClientEvents>
                </ComponentArt:CallBack>
            </div>
            <%--<div style="height:200px;margin-top:30px;clear:both;" id="errorContainer" runat="server">
            <asp:Label id="lblError" runat="server" Text="Please select a valid date range." cssClass="errorMessage"></asp:Label>
        </div>--%>
            <ComponentArt:Grid SkinID="Default" ID="grdEntryPages" runat="server" RunningMode="Client"
                EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
                PagerInfoClientTemplateId="grdEntryPagesPagination" SliderPopupClientTemplateId="grdEntryPagesSlider"
                Width="935" PageSize="10">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Original_Id" ShowTableHeading="false" ShowSelectorCells="false"
                        RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                        HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                        HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                        HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                        SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                        <Columns>
                            <ComponentArt:GridColumn Width="410" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                                HeadingCellCssClass="FirstHeadingCell" DataField="Title" DataCellCssClass="FirstDataCell"
                                SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                                FixedWidth="true" />
                            <ComponentArt:GridColumn Width="115" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                                DataField="Entry" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                                AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="EntryCTHeader" />
                            <ComponentArt:GridColumn Width="130" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate1 %>"
                                DataField="BounceRate" DataCellClientTemplateId="BounceRateCT" SortedDataCellCssClass="SortedDataCell"
                                AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="BounceRateCTHeader" />
                            <ComponentArt:GridColumn Width="135" runat="server" HeadingText="<%$ Resources:GUIStrings, Exit %>"
                                DataField="ExitPercentage" FixedWidth="true" DataCellClientTemplateId="ExitPercentageCT"
                                SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                                AllowReordering="false" Align="Right" HeadingCellClientTemplateId="ExitPercentageCTHeader" />
                            <ComponentArt:GridColumn Width="85" runat="server" HeadingText="<%$ Resources:GUIStrings, CMDetails %>"
                                DataField="Author" FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                HeadingCellCssClass="LastHeadingCell" AllowReordering="false" DataCellClientTemplateId="CMHoverEP"
                                HeadingCellClientTemplateId="cmHeaderEP" Align="Center" AllowSorting="false" />
                            <ComponentArt:GridColumn DataField="Menu" Visible="false" />
                            <ComponentArt:GridColumn DataField="Published_Date" Visible="false" />
                            <ComponentArt:GridColumn DataField="Published_By" Visible="false" />
                            <ComponentArt:GridColumn DataField="Author" Visible="false" />
                            <ComponentArt:GridColumn DataField="Original_Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Author_Email" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientEvents>
                    <ContextMenu EventHandler="grdEntryPages_onContextMenu" />
                    <PageIndexChange EventHandler="grdEntryPages_onPageIndexChange" />
                    <SortChange EventHandler="grdEntryPages_onSortChange" />
                </ClientEvents>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="BounceRateCT">
                        ## GetContentDecimal(DataItem.GetMember('BounceRate').Value) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ExitPercentageCT">
                        ## GetContentDecimal(DataItem.GetMember('ExitPercentage').Value) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="EntryCTHeader">
                        <div class="customHeader rightAligned">
                            ## getEntrySortImageHtml(DataItem,'leftAligned') ## <span>##_Visits##</span>
                            <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(epVisits, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="BounceRateCTHeader">
                        <div class="customHeader rightAligned">
                            ## getEntrySortImageHtml(DataItem,'leftAligned') ## <span>##_BounceRate1##</span>
                            <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(epBounceRate, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ExitPercentageCTHeader">
                        <div class="customHeader rightAligned">
                            ## getEntrySortImageHtml(DataItem,'leftAligned') ## <span>##_Exit##</span>
                            <img class="helpIcon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                                onmouseover="ddrivetip(epExitPercentage, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="cmHeaderEP">
                        <div class="customHeader">
                            <span>##_CMDetails##</span>
                            <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(epCMDetailsText, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                                onmouseout="hideddrivetip();" />
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="CMHoverEP">
                        ## CreateTip(DataItem) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdEntryPagesSlider">
                        <div class="SliderPopup">
                            <h5>
                                ## DataItem.GetMember(0).Value ##</h5>
                            <p>
                                ## DataItem.GetMember(1).Value ##</p>
                            <p>
                                ## DataItem.GetMember(2).Value ##</p>
                            <p>
                                ## DataItem.GetMember(3).Value ##</p>
                            <p>
                                ## DataItem.GetMember(4).Value ##</p>
                            <p class="paging">
                                <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdEntryPages.PageCount)
                                    ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdEntryPages.RecordCount)
                                        ##</span>
                            </p>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdEntryPagesPagination">
                        ## GetGridPaginationInfo(grdEntryPages) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
    <script type="text/javascript">
    window.cart_menu_zindexbase = 99999;
    <%=EntryPagesCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>
</asp:Content>

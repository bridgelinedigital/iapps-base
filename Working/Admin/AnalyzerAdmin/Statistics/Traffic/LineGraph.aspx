<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LineGraph.aspx.cs" Inherits="LineGraph" Theme="General" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><asp:localize runat="server" text="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsTrafficLineGraph %>"/></title>
    <script type="text/javascript">
    var newPage = "0";
    var orderByCol = "Visits";
    var orderBy = "DESC";
    function grdLineChart_onPageIndexChange(sender, eventArgs)
    {
        newPage = "" + eventArgs.get_index();
        var callbackArgs = new Array(newPage, orderByCol, orderBy);
        LineChartReferrersCallback.Callback(callbackArgs);
    }
    function grdLineChart_onSortChange(sender, eventArgs)
    {
        grdLineChart.unSelectAll();
        orderByCol = eventArgs.get_column().get_dataField();
        if(eventArgs.get_descending())
        {
            orderBy = "DESC";
        }
        else
        {
            orderBy = "ASC";
        }
        newPage = "0";
        var callbackArgs = new Array(newPage, orderByCol, orderBy);
        LineChartReferrersCallback.Callback(callbackArgs);
    }
    function hidePopup()
    {
        parent.viewGraphPopupWindow.hide();
    }
    </script>
</head>
<body class="popupBody">
    <form id="form1" runat="server">
        <div id="dialog" class="lineGraph">
            <div class="popupLibraryBoxShadow">
                <div class="popupLibraryContainer">
                    <div class="boxHeader">
                        <h3><asp:Label runat="server" id="lblHeading"></asp:Label></h3>
                    </div>
                    <div class="popupBoxContent">
                        <div class="dataAreaContent">
                            <div class="subSectionContent">
                                <div style="padding:20px 0 20px 20px;">
                                    <asp:LinkButton id="lbHour" runat="server" Text="<%$ Resources:GUIStrings, Hour %>"></asp:LinkButton>
                                    &nbsp;&nbsp;<label>|</label>&nbsp;&nbsp;
                                    <asp:LinkButton id="lbDay" runat="server" Text="<%$ Resources:GUIStrings, Day %>"></asp:LinkButton>
                                    &nbsp;&nbsp;<label>|</label>&nbsp;&nbsp;
                                    <asp:LinkButton id="lbWeek" runat="server" Text="<%$ Resources:GUIStrings, Week %>"></asp:LinkButton>
                                    &nbsp;&nbsp;<label>|</label>&nbsp;&nbsp;
                                    <asp:LinkButton id="lbMonth" runat="server" Text="<%$ Resources:GUIStrings, Month %>"></asp:LinkButton>
                                </div>
                                <div class="clearFix">&nbsp;</div>
                                <div class="chartContainer" id="chartContainer" runat="server">
                                    <ComponentArt:CallBack ID="LineChartReferrersCallback" runat="server" Height="100" Width="350" OnCallback="LineChartReferrersCallback_onCallback">
                                        <Content>
                                            <ComponentArtChart:Chart id="LineChart2d" RenderingPrecision="0.1" width="920px" height="328px" runat="server"
                                                SelectedPaletteName="Earth" MainStyle="Line">
                                                <SeriesStyles>
                                                    <ComponentArtChart:SeriesStyle ChartKind="line2D" LineStyleName="StripLine" />
                                                </SeriesStyles>
                                                <View Kind="TwoDimensional"></View>
                                                <GradientStyles>
                                                    <ComponentArtChart:GradientStyle StartColor="#ffc932" EndColor="#ffe9a8" GradientKind="Vertical" />
                                                </GradientStyles>
                                                <Palettes>
                                                    <ComponentArtChart:Palette AxisLineColor="#b0b0b0" BackgroundColor="#ffffff" BackgroundEndingColor="#ffffff"
                                                        CoodinateLabelFontColor="#848484" CoordinateLineColor="#dedede" CoordinatePlaneColor="#ffffff"
                                                        CoordinatePlaneSecondaryColor="#ffffff" DataLabelFontColor="#848484"
                                                        FrameColor="#ffffff" FrameFontColor="#848484" FrameSecondaryColor="#ffffff"
                                                        LegendBackgroundColor="#ffffff" LegendBorderColor="#dedede" LegendFontColor="#848484"
                                                        Name="Earth" TitleFontColor="#848484" TwoDObjectBorderColor="#dedede" PrimaryColors="ffc932" SecondaryColors="ffe9a8" />
                                                </Palettes>
                                            </ComponentArtChart:Chart>
                                        </Content>
                                        <LoadingPanelClientTemplate>
                                        </LoadingPanelClientTemplate>
                                    </ComponentArt:CallBack>
                                </div>
                                <ComponentArt:Grid SkinId="Default" ID="grdLineChart" runat="server" RunningMode="Client" Height="275"
                                    EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" Width="915" PageSize="10">
                                    <Levels>
                                        <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false"
                                            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                                            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                                            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                                            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                                            <Columns>
                                                <ComponentArt:GridColumn Width="680" runat="server" HeadingText="<%$ Resources:GUIStrings, Date %>" HeadingCellCssClass="FirstHeadingCell" DataField="Title" 
                                                    DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" IsSearchable="true"
                                                    AllowReordering="false" FixedWidth="true" />
                                                <ComponentArt:GridColumn Width="215" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>" DataField="Visits" IsSearchable="true"
                                                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true" Align="Right" />
                                            </Columns>
                                        </ComponentArt:GridLevel>
                                    </Levels>
                                    <ClientEvents>
                                        <PageIndexChange EventHandler="grdLineChart_onPageIndexChange" />
                                        <SortChange EventHandler="grdLineChart_onSortChange" />
                                    </ClientEvents>
                                </ComponentArt:Grid>
                            </div>
                        </div>
                        <div class="popupFooter">
                            <asp:button id="btnClose" runat="server" CssClass="button" OnClientClick="hidePopup()" Text="<%$ Resources:GUIStrings, Close %>" Tooltip="<%$ Resources:GUIStrings, Close %>"
                                />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
<script>
    <%=LineChartReferrersCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
</script>
</html>

<%@ Page Language="C#" MasterPageFile="~/Statistics/Content/ContentMaster.master"
    AutoEventWireup="true" Theme="General" Inherits="UserControls_Statistics_Content_FileDownloads"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsContentFilesDownloaded %>" CodeBehind="FileDownloads.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _NoofDownloads = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, NoofDownloads %>"/>';
        var _AssetFilename = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, AssetFileName %>"/>';

        var item;
        var newPage = "0";
        var orderByCol = "NoOfDownLoads";
        var orderBy = "DESC";
        function grdFileDownloads_onSortChange(sender, eventArgs) {
            orderByCol = eventArgs.get_column().get_dataField();
            if (eventArgs.get_descending()) {
                orderBy = "DESC";
            }
            else {
                orderBy = "ASC";
            }
            grdFileDownloads.unSelectAll();
        }

        function cmFileDownloads_onItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            var folderId = item.getMember("DirectoryId").get_text();
            switch (selectedMenu) {
                case 'fileLibrary':
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    var completeUrl = analyticsPublicSiteUrl + "/Admin/Default.aspx" + currentUrl + "&Token=" + GetTokenbyProductId(cmsProductId, siteId, siteName) + "&RequestPageURL=Libraries/Data/ManageFileLibrary.aspx" + "&FolderId=" + folderId;
                    window.location.href = completeUrl;                   
                    break;
            }
        }
        function grdFileDownloads_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            grdFileDownloads.select(item);
            var evt = eventArgs.get_event();
            cmFileDownloads.showContextMenuAtEvent(evt, eventArgs.get_item());
        }
    </script>

    <!-- Tooltip Script -->

    <script type="text/javascript">
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var reverseTooltipImage;
        var downTooltipImage;
        var tooltipImage;
        var item = "";

        var spvPageViews = "<asp:localize runat='server' text='<%$ Resources:JSMessages, ThetotalnumberofpagesviewedRepeatedviewsofasinglepagearecounted %>'/>";
        var spvUniquePageViews = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Thenumberofvisitsduringwhichoneormoreofthesepageswasviewed %>'/>";
        var spvTimeOnPage = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Theaverageamountoftimevisitorsspentviewingthissetofpagesorpage %>'/>";

        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor, imgSrc, revImgSrc, downImgSrc) {
            //document.getElementById("dhtmlpointer").src = imgSrc;
            tooltipImage = imgSrc;
            reverseTooltipImage = revImgSrc;
            downTooltipImage = downImgSrc;
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdFileDownloads.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdFileDownloads.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }
    </script>
</asp:Content>
<asp:Content ID="contentFileDownloads" ContentPlaceHolderID="contentContentPlaceHolder" runat="Server">
<div class="report-page">
    <div class="clear-fix">
        <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h2>
    </div>
    <div class="graph-info clear-fix">
        <div class="left-section">
            <p><strong><asp:Label ID="lblTotal" runat="server" Text="<%$ Resources:GUIStrings, Total100 %>" CssClass="headingLabel"/></strong></p>
        </div>
    </div>
    <ComponentArt:Menu ID="cmFileDownloads" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, JumptoFileLibrary %>" ID="fileLibrary">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="cmFileDownloads_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Grid SkinID="Default" ID="grdFileDownloads" runat="server" RunningMode="Client"
        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" Width="100%" PageSize="10"
        SliderPopupClientTemplateId="grdFileDownloadsSlider" PagerInfoClientTemplateId="grdFileDownloadsPagination">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="900" runat="server" HeadingText="<%$ Resources:GUIStrings, AssetFileName %>" HeadingCellCssClass="FirstHeadingCell"
                        DataField="AssetFileName" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="AssetFileNameHeader" />
                    <ComponentArt:GridColumn Width="328" runat="server" HeadingText="<%$ Resources:GUIStrings, NoofDownloads %>" DataField="NoofDownloads"
                        IsSearchable="true" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" DataCellClientTemplateId="NoofDownloadsCT" HeadingCellClientTemplateId="NoofDownloadsCTHeader" />
                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                    <ComponentArt:GridColumn DataField="ContentId" Visible="false" />
                    <ComponentArt:GridColumn DataField="DirectoryId" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <%--<PageIndexChange EventHandler="grdFileDownloads_onPageIndexChange" />--%>
            <ContextMenu EventHandler="grdFileDownloads_onContextMenu" />
            <SortChange EventHandler="grdFileDownloads_onSortChange" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="AssetFileNameHeader">
                <div class="custom-header">
                    <span class="header-text">##_AssetFilename##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(fdAssetFileName, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="NoofDownloadsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_NoofDownloads##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(fdNoOfDownloads, 300, '', '../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="NoofDownloadsCT">
                <div style="text-align: right;">
                    ## DataItem.GetMember("NoofDownloads").Value ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdFileDownloadsSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdFileDownloads.PageCount) ##</span>
                        <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdFileDownloads.RecordCount) ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdFileDownloadsPagination">
                ## GetGridPaginationInfo(grdFileDownloads) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</div>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/ReportingRangeMaster.master" AutoEventWireup="true"
    CodeBehind="InternalSearch.aspx.cs" Inherits="InternalSearch" StylesheetTheme="General"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsSearchInternalSearch %>" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <!-- Tooltip Script -->
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var isOverview = false;
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";

        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                else {
                    tipobj.style.width = "auto";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
                    pointerobj.src = reversedownTooltipImage;
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }

        function ChangeGraph() {
            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 1;

            //    typeOfGraph = graphType;
            if (grdInternalSearch.get_table().getRowCount() >= 1 && grdInternalSearch.PageCount != 0) {
                var callbackArgs = new Array(pageId, pageName, typeOfGraph);
                if (isOverview != null && isOverview != true) {
                    EntryPagesCallback.Callback(callbackArgs);
                }
            }
            return false;
        }

        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up DateToolTip.replace('{0}', FormatDate(date,DateFormat)); 
            var popupHTML;
            if (typeOfGraph != 1)
                popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            else
                popupHTML = 'Hour :' + FormatDate(point.get_x(), 'H');

            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + " ";
                    popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, EntryPages %>'/>";
                    popupHTML += "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Published %>'/>";
                popupHTML += " " + FormatDate(point.get_x().split(" ")[0], DateFormat);
                popupHTML += "<br/>";
                popupHTML += "<asp:localize runat='server' text='<%$ Resources:JSMessages, By %>'/>";
                popupHTML += " " + pageAuthor;
            }
            ddrivetip(popupHTML, '150', '');
        }

        function onDataPointExit(who, what) {
            hideddrivetip();
        }

        function getInternalSearchSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdInternalSearch.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdInternalSearch.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }
        var numSearched = '<%= GUIStrings.numSearched %>';
        var numSearchAgain = '<%= GUIStrings.numSearchAgain %>';
        var perSearchAgain = '<%= GUIStrings.perSearchAgain %>';
        var numZeroResults = '<%= GUIStrings.numZeroResults %>';
        var perZeroResults = '<%= GUIStrings.perZeroResults %>';
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="reportingContentHolder" runat="server">
    <h3 style="margin-bottom: 15px;"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, InternalSearch %>" /></h3>
    <ComponentArt:Grid SkinID="Default" ID="grdInternalSearch" runat="server" RunningMode="Client"
        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
        Width="100%" PageSize="10" SliderPopupClientTemplateId="grdInternalSearchSlider"
        PagerInfoClientTemplateId="grdInternalSearchPagination">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="[Dim Internal Search Term].[SK Search Term Id].[SK Search Term Id].[MEMBER_CAPTION]"
                ShowTableHeading="false" ShowSelectorCells="false" RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="454" runat="server" HeadingText="<%$ Resources:GUIStrings, SearchTerm %>"
                        DataField="[Dim Internal Search Term].[Search Term].[Search Term].[MEMBER_CAPTION]"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" Visible="true" />
                    <ComponentArt:GridColumn Width="130" runat="server" HeadingText="<%$ Resources:GUIStrings, numSearched %>"
                        DataField="[Measures].[TimesSearched]" IsSearchable="true"
                        AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="NumSearchedHT" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, numSearchAgain %>"
                        DataField="[Measures].[TimeCausedResearch]" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" Align="Right" HeadingCellClientTemplateId="NumSearchAgainHT" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, perSearchAgain %>"
                        HeadingCellClientTemplateId="SearchAgainHT" DataField="[Measures].[ResearchPct]"
                        AllowReordering="false" FixedWidth="true" Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, numZeroResults %>"
                        DataField="[Measures].[TimesNoResultsFound]" HeadingCellClientTemplateId="NumZeroResultsHT"
                        AllowReordering="false" FixedWidth="true" Align="Right" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, perZeroResults %>"
                        DataField="[Measures].[ZeroResultPct]" HeadingCellClientTemplateId="PerZeroResultsHT"
                        AllowReordering="false" FixedWidth="true" Align="Right" />
                    <ComponentArt:GridColumn DataField="[Dim Internal Search Term].[SK Search Term Id].[SK Search Term Id].[MEMBER_CAPTION]"
                        Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NumSearchedHT">
                <div class="custom-header">
                    <span class="header-text">## numSearched ##</span>
                    ## getInternalSearchSortImageHtml(DataItem,'sort-image') ## 
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="NumSearchAgainHT">
                <div class="custom-header">
                    <span class="header-text">## numSearchAgain ##</span>
                    ## getInternalSearchSortImageHtml(DataItem,'sort-image') ## 
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SearchAgainHT">
                <div class="custom-header">
                    <span class="header-text">## perSearchAgain ##</span>
                    ## getInternalSearchSortImageHtml(DataItem,'sort-image') ## 
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="NumZeroResultsHT">
                <div class="custom-header">
                    <span class="header-text">## numZeroResults ##</span>
                    ## getInternalSearchSortImageHtml(DataItem,'sort-image') ## 
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="PerZeroResultsHT">
                <div class="custom-header">
                    <span class="header-text">## perZeroResults ##</span>
                    ## getInternalSearchSortImageHtml(DataItem,'sort-image') ## 
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdInternalSearchSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdInternalSearch.PageCount)
                            ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdInternalSearch.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdInternalSearchPagination">
                ## GetGridPaginationInfo(grdInternalSearch) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</asp:Content>

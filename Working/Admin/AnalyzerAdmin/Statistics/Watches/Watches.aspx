<%@ Page Language="C#" MasterPageFile="~/ReportingRangeMaster.master" AutoEventWireup="true"
    Theme="General" Inherits="Statistics_Watches_Watches" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerStatisticsWatches %>"
    CodeBehind="Watches.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _WatchName = '<asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, WatchName %>"/>';
        var _TotalVisitors = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, TotalVisitors %>"/>';
        var _TotalCount = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, TotalCount %>"/>';
        var _Conversion = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Conversion %>"/>';
        var _UniqueVisitors = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, UniqueVisitors %>"/>';
        var _UniqueCount = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, UniqueCount %>"/>';
        var newPage = "0";
        var orderByCol = "TotalCount";
        var orderBy = "DESC";
        var typeOfGraph = "2";
        
        function grdWatches_onPageIndexChange(sender, eventArgs) {
            newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            WatchesChartCallback.Callback(callbackArgs);
        }
        function grdWatches_onSortChange(sender, eventArgs) {
            grdWatches.unSelectAll();
            orderByCol = eventArgs.get_column().get_dataField();
            if (eventArgs.get_descending()) {
                orderBy = "DESC";
            }
            else {
                orderBy = "ASC";
            }
            newPage = "0";
            var callbackArgs = new Array(newPage, orderByCol, orderBy);
            if (grdWatches.RecordCount != 0) {
                WatchesChartCallback.Callback(callbackArgs);
            }
        }
    </script>
    <!-- Tooltip Script -->
    <script type="text/javascript">
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var item = "";
        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                pointerobj.src = tooltipImage;
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
                    pointerobj.src = reversedownTooltipImage;
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
        function getSortImageHtml(column, cssClass) {
            // is the grid sorted by this column?
            if (grdWatches.Levels[0].IndicatedSortColumn == column.ColumnNumber) {
                if (grdWatches.Levels[0].IndicatedSortDirection == 1) {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/desc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, descending %>"/>' + '" />';
                }
                else {
                    return '<img class="' + cssClass + '" src="../../App_Themes/General/images/asc.gif" alt="' + '<asp:localize runat="server" text="<%$ Resources:JSMessages, ascending %>"/>' + '" />';
                }
            }
            // it isn't sorted by this column, no image
            return '';
        }
        function grdWatches_onItemSelect(sender, eventArgs) {
            if (grdWatches.get_table().getRowCount() >= 1 && grdWatches.PageCount != 0) {
                var selectedRow = eventArgs.get_item();
                watchName = selectedRow.getMember("Name").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, WatchTriggered %>'/>", watchName);
                //pageAuthor = selectedRow.getMember("Name").get_text();
                //pageName = selectedRow.getMember("Name").get_text();
                var callbackArgs = new Array(watchName, typeOfGraph); //, pageName, typeOfGraph);
                WatchesCallback.Callback(callbackArgs);
            }
        }
        function grdWatches_onLoad(sender, eventArgs) {
            grdWatches.unSelectAll();
            if (grdWatches.get_table().getRowCount() >= 1 && grdWatches.PageCount != 0) {
                var firstItem = grdWatches.get_table().getRow(0);
                watchName = firstItem.getMember("Name").get_text();
                document.getElementById("secondLegend").innerHTML = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, WatchTriggered %>'/>", watchName);
            }
            if (grdWatches.get_table().getRowCount() == 0) {
                document.getElementById("legendBox").style.visibility = "hidden";
            }
            else {
                document.getElementById("legendBox").style.visibility = "visible";
            }
        }
        function ChangeGraph() {
            var checkOption = document.getElementById('graphOption').selectedIndex;
            typeOfGraph = checkOption + 1;

            if (grdWatches.get_table().getRowCount() >= 1 && grdWatches.PageCount != 0) {
                var callbackArgs = new Array(watchName, typeOfGraph); //, pageName, typeOfGraph);
                WatchesCallback.Callback(callbackArgs);
            }
            return false;
        }
        //For registering the mouse event for CA Chart
        function WatchesCallback_onCallbackComplete(sender, eventArgs) {
            document.getElementById("<%=lblHeading.ClientID%>").innerHTML = document.getElementById("<%=hdnPageHeading.ClientID%>").value;
            document.getElementById("<%=lblTotal.ClientID%>").innerHTML = document.getElementById("<%=hdnAverage.ClientID%>").value;
        }
        function onDataPointHover(who, what) {
            var point = what.get_dataPoint();
            var series = point.get_parentSeries();
            //create the HTML for the pop-up
            var popupHTML;
            if (typeOfGraph != 1)
                popupHTML = DateToolTip.replace('{0}', FormatDate(point.get_x().split(" ")[0], DateFormat));
            else
                popupHTML = '<asp:localize runat="server" text="<%$ Resources:JSMessages, Hour %>"/>' + point.get_x().split(" ")[0];
            //var popupHTML = point.get_x().split(" ")[0]; 
            if (series.get_name() != "seriesPublishDateMarker") {
                if (point.get_y() != 0) {
                    popupHTML += "<br/><b>" + point.get_y() + "</b>";
                }
            }
            if (series.get_name() == "seriesPublishDateMarker") {
                popupHTML += "<br/><b>" + pageAuthor + "</b>";
            }
            ddrivetip(popupHTML, '150', '');
        }

        function onDataPointExit(who, what) {
            hideddrivetip();
        }
    </script>
</asp:Content>
<asp:Content ID="contentWatches" ContentPlaceHolderID="reportingContentHolder" runat="Server">
<div class="report-page">
    <div class="clear-fix">
        <h2><asp:Label runat="server" ID="lblHeading" CssClass="headingLabel"></asp:Label></h2>
        <div class="graph-type clear-fix">
            <div class="form-row">
                <label class="form-label"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ViewBy %>" /></label>
                <div class="form-value">
                    <select id="graphOption" onchange="ChangeGraph()">
                        <option value="hour">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Hour %>" /></option>
                        <option value="day" selected="selected">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Day %>" /></option>
                        <option value="week">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Week %>" /></option>
                        <option value="month">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Month %>" /></option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="graph-info clear-fix">
        <div class="left-section">
            <p><strong><asp:Label ID="lblTotalVisits" runat="server" CssClass="headingLabel"/></strong></p>
            <p><strong><asp:Label ID="lblTotal" runat="server" CssClass="headingLabel"/></strong></p>
        </div>
        <div class="right-section">
            <ul class="legendBox" id="legendBox">
                    <li class="firstLegend" id="firstLegend">
                        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, TotalWatchTriggered %>" /></li>
                    <li class="secondLegend" id="secondLegend"></li>
            </ul>
        </div>
    </div>
    <div class="chart-container" id="chartContainer" runat="server">
        <ComponentArt:CallBack ID="WatchesCallback" runat="server" Height="100" Width="920"
            OnCallback="WatchesCallback_onCallback">
            <Content>
                <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart">
                    <ClientEvents>
                        <DataPointHover EventHandler="onDataPointHover" />
                        <DataPointExit EventHandler="onDataPointExit" />
                    </ClientEvents>
                </ComponentArtChart:Chart>
                <asp:HiddenField ID="hdnPageHeading" runat="server" />
                <asp:HiddenField ID="hdnAverage" runat="server" />
            </Content>
            <LoadingPanelClientTemplate>
            </LoadingPanelClientTemplate>
            <ClientEvents>
                <CallbackComplete EventHandler="WatchesCallback_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <ComponentArt:Grid SkinID="Default" ID="grdWatches" runat="server" RunningMode="Client"
        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
        Width="100%" PageSize="10" SliderPopupClientTemplateId="grdWatchesSlider" PagerInfoClientTemplateId="grdWatchesPagination">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="434" runat="server" HeadingText="<%$ Resources:GUIStrings, WatchName %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="Name" DataCellCssClass="FirstDataCell"
                        SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" HeadingCellClientTemplateId="watchNameHeader" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, TotalVisitors %>"
                        DataField="TotalVisitors" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" Align="Right" DataType="System.Int64"
                        HeadingCellClientTemplateId="TotalVisitorsCTHeader" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, TotalCount %>"
                        DataField="TotalCount" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" Align="Right" DataType="System.Int64" HeadingCellClientTemplateId="TotalCountCTHeader" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, Conversion %>"
                        DataField="TotalConversion" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" DataType="System.Double" Align="Right" DataCellClientTemplateId="TotalVisitorsCT"
                        HeadingCellClientTemplateId="TotalConversionCTHeader" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, UniqueVisitors %>"
                        DataField="UniqueVisitors" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" Align="Right" DataType="System.Int64" HeadingCellClientTemplateId="UniqueVisitorsCTHeader" />
                    <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, UniqueCount %>"
                        DataField="UniqueCount" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" Align="Right" DataType="System.Int64" HeadingCellClientTemplateId="UniqueCountCTHeader" />
                    <ComponentArt:GridColumn DataField="RowNumber" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ItemSelect EventHandler="grdWatches_onItemSelect" />
            <Load EventHandler="grdWatches_onLoad" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="TotalVisitorsCT">
                ## GetContentDecimal(DataItem.GetMember('TotalConversion').Value) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="watchNameHeader">
                <div class="custom-header">
                    <span class="header-text">##_WatchName##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(wWatchName, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TotalVisitorsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_TotalVisitors##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(wTotalVisitor, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TotalCountCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_TotalCount##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(wTotalCount, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TotalConversionCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_Conversion##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(wConversion, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="UniqueVisitorsCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_UniqueVisitors##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(wUniqueVisitors, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="UniqueCountCTHeader">
                <div class="custom-header">
                    <span class="header-text">##_UniqueCount##</span>
                    <img class="help-icon" src="../../App_Themes/General/images/icon-tooltip.gif" alt=""
                        onmouseover="ddrivetip(wUniqueCount, 300, '');positiontip(event);" onmouseout="hideddrivetip();" />
                    ## getSortImageHtml(DataItem,'sort-image') ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdWatchesSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdWatches.PageCount)
                            ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdWatches.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdWatchesPagination">
                ## GetGridPaginationInfo(grdWatches) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <script type="text/javascript">
        <%=WatchesCallback.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td><%= GUIStrings.Loading1 %></td></tr></table>';
    </script>
</div>
</asp:Content>

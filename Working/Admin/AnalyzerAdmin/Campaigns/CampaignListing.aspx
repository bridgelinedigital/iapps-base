﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" StylesheetTheme="General" runat="server"
    CodeBehind="CampaignListing.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.CampaignListing"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerCampaignsViewallCampaigns %>" %>

<%@ Register TagPrefix="rr" TagName="ReportingRange" Src="~/UserControls/ReportingRange.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var selectedItem;
        var isInsert = false;
        function grdCampaign_toggleItem(sender, eventArgs) {
            grdCampaign.render();
        }

        function grdCampaign_onLoad(sender, eventArgs) {
            isInsert = true;
            grdCampaign.beginUpdate();
            grdCampaign.get_table().addEmptyRow(0);
            grdCampaign.editComplete();
            selectedItem = grdCampaign.get_table().getRow(0);
            grdCampaign.edit(selectedItem);
        }

        function grdCampaign_onContextMenu(sender, eventArgs) {
            var evt = eventArgs.get_event();
            selectedItem = eventArgs.get_item();
            var selectedId = selectedItem.getMember('Id').get_text();
            var menuItems = mnuCampaigns.get_items();
            grdCampaign.select(selectedItem);
            var itemLevel = selectedItem.get_table().get_level();
            if (itemLevel == 0) {
                for (i = 0; i < menuItems.get_length() ; i++) {

                    if (menuItems.getItem(i).get_id() != 'cmAddCampaign' && selectedId == '') {
                        menuItems.getItem(i).set_visible(false);
                    }
                    else if (menuItems.getItem(i).get_id() == 'cmInactive') {
                        if (selectedItem.getMember('Status').get_text() == '1') {
                            menuItems.getItem(i).set_text('<%= JSMessages.MakeInactive %>');
                        }
                        else {
                            menuItems.getItem(i).set_text('<%= JSMessages.MakeActive %>');
                        }
                    }
                    if (menuItems.getItem(i).get_id() == 'cmEmail') {
                        if (!hasMarketierPermission || !hasMarketierLicense) {
                            menuItems.getItem(i).set_visible(false);
                        }
                    }

                }
                mnuCampaigns.showContextMenuAtEvent(evt);
            }
            else {
                var campaignType = selectedItem.getMember('Type').get_text();
                if (campaignType == 1) {
                    mnuCampaignLinks.showContextMenuAtEvent(evt);
                }
                else {
                    if (hasMarketierPermission && hasMarketierLicense) {
                        mnuCampaignEmails.showContextMenuAtEvent(evt);
                    }
                }
            }
        }
        function mnuCampaigns_onItemSelect(sender, eventArgs) {
            var selectedMenuId = eventArgs.get_item().get_id();
            var selectedId = selectedItem.getMember('Id').get_text();
            switch (selectedMenuId) {
                case 'cmAddCampaign':
                    isInsert = true;
                    if (selectedId != '') {
                        grdCampaign.beginUpdate();
                        grdCampaign.get_table().addEmptyRow(0);
                        grdCampaign.editComplete();
                        selectedItem = grdCampaign.get_table().getRow(0);
                    }
                    grdCampaign.edit(selectedItem);
                    break;
                case 'cmEmail':
                    if (hasMarketierPermission && hasMarketierLicense)
                    {
                        window.location = jmarketierAdminURL + '/Emails/ManageEmails.aspx?RedirectFrom=ALCampaign&GroupId=' + selectedId + "&Token=" + GetTokenBySiteAndProduct(jMarketierProductId, siteId, siteName);
                    }
                    else
                    {
                        OpenLicenseWarningPopup('Marketier', hasMarketierLicense, hasMarketierPermission);
                    }
                    break;
                case 'cmLink':
                OpenLinkBuilder(selectedId);
                    break;
                case 'cmEditCampaign':
                    isInsert = false;
                    grdCampaign.edit(selectedItem);
                    break;
                case 'cmEditGoals':
                    var url = jAnalyticAdminSiteUrl + "/Popups/AssignGoalsForCampaign.aspx?CampaignId=" + selectedId;
                campaignLinksWindow = dhtmlmodal.open('AssignGoalsForCampaign', 'iframe', url, '');
                    break;
                case 'cmInactive':
                    if (selectedItem.getMember('Status').get_text() == '1')
                        MakeCampaignInactive(selectedId);
                    else
                        MakeCampaignActive(selectedId);
                    grdCampaign.callback();
                    break;
                case 'cmDeleteCampaign':
                    var nCount = CheckBeforeDeleteCampaign(selectedId);
                    if (nCount == 0) {
                        DeleteCampaign(selectedId);
                        grdCampaign.callback();
                    }
                    else if (window.confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, GoalIsAlreadyAttachedWithThisGoal %>' />")) {
                        DeleteCampaign(selectedId);
                        grdCampaign.callback();
                    }                    
                    break;
            }
        }

        function mnuCampaignLinks_onItemSelect(sender, eventArgs) {
            var selectedMenuId = eventArgs.get_item().get_id();
            var selectedId = selectedItem.getMember('Id').get_text();
            var selectedCampaignId = selectedItem.getMember('CampaignId').get_text();
            switch (selectedMenuId) {
                case 'cmGetLinks':
                    OpenGetLinks(selectedId);
                    break;
                case 'cmEditLink':
                OpenLinkBuilder(selectedCampaignId, selectedId);
                break;
                    }
            }

        function OpenGetLinks(linkId) {
        OpeniAppsAnalyzerPopup("CampaignLinks", "LinkId=" + linkId);
    }

    function OpenLinkBuilder(campaignId, linkId) {
        var qString = "CampaignId=" + campaignId;
        if (typeof linkId != "undefined")
            qString += "&LinkId=" + linkId;

        OpeniAppsAnalyzerPopup("LinkBuilder", qString, "CloseLinkBuilder");
    }

    function CloseLinkBuilder(refreshGrid, openLinks, linkId) {
        grdCampaign.callback();
        if (typeof popupActionsJson.CustomAttributes["LinkId"] != "undefined")
            OpenGetLinks(popupActionsJson.CustomAttributes["LinkId"])
        }

        function mnuCampaignEmails_onItemSelect(sender, eventArgs) {
            var selectedMenuId = eventArgs.get_item().get_id();
            var selectedId = selectedItem.getMember('Id').get_text();
            var selectedCampaignId = selectedItem.getMember('CampaignId').get_text();
            switch (selectedMenuId) {
                case 'cmEditEmail':

                    if (hasMarketierPermission && hasMarketierLicense) {
                        window.location = jmarketierAdminURL + '/Campaigns/DefineCampaign.aspx?RedirectFrom=ALCampaign&Id=' + selectedId + '&GroupId=' + selectedCampaignId + '&Token=' + GetTokenBySiteAndProduct(jMarketierProductId, siteId, siteName);
                    }
                    else {
                        OpenLicenseWarningPopup('Marketier', hasMarketierLicense, hasMarketierPermission);
                    }
                    break;
            }
        }

        function SaveCampaign() {
            var newCampaignName = document.getElementById(txtTitleClientID).value;
            var re = new RegExp('^[^&<>]+$');
            if (newCampaignName == '') {
                alert('<%= JSMessages.EnterCampaignName %>');
            }
            else {
                if (re.exec(newCampaignName) == null) {
                    alert('<%= JSMessages.InvalidCharCampaignName  %>');
                }
                else {
                    if (CheckCampaignExists(newCampaignName) == 'False') {
                        alert('<%= JSMessages.CampaignNameExists  %>');
                    }
                    else {
                        grdCampaign.editComplete();
                    }
                }
            }
        }

        function CancelEditing() {
            if (isInsert) {
                selectedItem = grdCampaign.get_table().getRow(0);
                grdCampaign.deleteItem(selectedItem);
            }
            grdCampaign.editCancel();
        }

        function setCampaignValue(control, DataField) {
            var txtTitle = document.getElementById(control);
            var txtTitlevalue = selectedItem.GetMember(DataField).Value;
            if (txtTitlevalue == null || txtTitlevalue == 'null')
                txtTitle.value = '';
            else
                txtTitle.value = txtTitlevalue;
        }

        function getCampaignValue(control) {
            var txtTitle = document.getElementById(control);
            return [txtTitle.value, txtTitle.value];
        }

        function GetInactiveCampaign(chk) {
            SetShowInactive(chk.checked);
            grdCampaign.callback();
        }

        function GetStatusText(objDataItem) {
            var type = objDataItem.getMember('Type').get_text();
            var statusCode = objDataItem.getMember('Status').get_text();
            if (statusCode != '') {
                if (type == "1") {
                    if (statusCode == "1")
                        return 'Active';
                    else if (statusCode == "3")
                        return 'Deleted';
                    else if (statusCode == "2")
                        return 'Inactive';
                    else
                        return 'Draft';
                }
                else {
                    if (statusCode == "1")
                        return 'Draft';
                    else if (statusCode == "2")
                        return 'Active';
                    else if (statusCode == "3")
                        return 'Completed';
                    else if (statusCode == "4")
                        return 'Archived';
                    else if (statusCode == "5")
                        return 'Running';
                    else if (statusCode == "6")
                        return 'Cancelled';
                    else
                        return '';
                }
                }

                 return '';
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="mainPlaceHolder" runat="server">
<div class="campaigns clear-fix">
    <div class="page-header clear-fix">
        <h1><%= GUIStrings.ViewAllCampaigns %></h1>
        <rr:ReportingRange ID="ctlReportingRange" runat="server" />
    </div>
    <ComponentArt:Menu ID="mnuCampaigns" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem ID="cmAddCampaign" runat="server" Text="<%$ Resources:GUIStrings, AddNewCampaign %>"
                Look-LeftIconUrl="cm-icon-add.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmEmail" runat="server" Text="<%$ Resources:GUIStrings, AddNewEmail %>">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmLink" runat="server" Text="<%$ Resources:GUIStrings, AddNewLink %>">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmEditCampaign" runat="server" Text="<%$ Resources:GUIStrings, EditCampaign %>"
                Look-LeftIconUrl="cm-icon-edit.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmEditGoals" runat="server" Text="<%$ Resources:GUIStrings, ViewAttachedGoals %>">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmInactive" Text="<%$ Resources:GUIStrings, MakeInactive %>">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmDeleteCampaign" runat="server" Text="<%$ Resources:GUIStrings, DeleteCampaign %>"
                Look-LeftIconUrl="cm-icon-delete.png">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="mnuCampaigns_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Menu ID="mnuCampaignLinks" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem ID="cmGetLinks" runat="server" Text="<%$ Resources:GUIStrings, GetLinks %>">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmEditLink" runat="server" Text="<%$ Resources:GUIStrings, EditLink %>"
                Look-LeftIconUrl="cm-icon-edit.png">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="mnuCampaignLinks_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Menu ID="mnuCampaignEmails" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem ID="cmEditEmail" runat="server" Text="<%$ Resources:GUIStrings, EditEmail %>"
                Look-LeftIconUrl="cm-icon-edit.png">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="mnuCampaignEmails_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <div style="text-align: right; padding: 0 0 10px 0;">
        <asp:CheckBox runat="server" ID="chkShowInactive" Text="<%$ Resources:GUIStrings, ShowInactiveCampaigns%>"
                onClick="GetInactiveCampaign(this);" />
            <asp:HiddenField ID="hdnSave" runat="server" />
    </div>
    <ComponentArt:Grid SkinID="SliderWithHierarchy" ID="grdCampaign" runat="server" RunningMode="Callback"
        EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" Width="100%" PageSize="10" PagerStyle="Buttons"
        EnableViewState="true" AutoCallBackOnUpdate="true" TreeLineImageWidth="15" TreeLineImageHeight="15">
        <Levels>
            <ComponentArt:GridLevel DataMember="Campaign" DataKeyField="Id" HeadingCellCssClass="HeadingCell"
                HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell"
                RowCssClass="Row" SelectorCellWidth="19" SelectorImageWidth="19" SelectorImageUrl="last.gif"
                ShowTableHeading="false" HeadingCellActiveCssClass="HeadingCellActive" SelectedRowCssClass="SelectedRow"
                GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                ShowSelectorCells="false" HeadingCellHoverCssClass="HeadingCellHover">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn Width="404" runat="server" HeadingText="<%$ Resources:GUIStrings, AnalyzerCampaigns %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="Name" DataCellCssClass="FirstDataCell"
                        SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                        EditControlType="Custom" EditCellServerTemplateId="svtCampaign" DataCellClientTemplateId="GroupNameCT" />
                    <ComponentArt:GridColumn Width="110" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>"
                        DataField="Status" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        DataCellClientTemplateId="StatusCT" FixedWidth="true" AllowEditing="False" />
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                        DataField="Visits" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" TextWrap="true" AllowEditing="False" Align="Right" />
                    <ComponentArt:GridColumn Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate %>"
                            DataField="BounceRate" FixedWidth="true" DataCellClientTemplateId="BounceRateCT"
                        SortedDataCellCssClass="SortedDataCell" AllowEditing="False" HeadingCellCssClass="LastHeadingCell"
                        AllowReordering="false" Align="Right" />
                    <ComponentArt:GridColumn Width="110" runat="server" HeadingText="<%$ Resources:GUIStrings, Sends %>"
                        DataField="Sends" FixedWidth="true" SortedDataCellCssClass="SortedDataCell" AllowEditing="False"
                        HeadingCellCssClass="LastHeadingCell" AllowReordering="false" Align="Right" />
                    <ComponentArt:GridColumn Width="110" runat="server" HeadingText="<%$ Resources:GUIStrings, Deliveries %>"
                        DataField="Deliveries" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                        Align="Right" AllowEditing="False" HeadingCellCssClass="LastHeadingCell" AllowReordering="false" />
                    <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, Clicks %>"
                        DataField="Clicks" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                        Align="Right" AllowEditing="False" HeadingCellCssClass="LastHeadingCell" AllowReordering="false" />
                    <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                        DataField="" FixedWidth="true" SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="LastHeadingCell"
                        AllowReordering="false" EditControlType="Custom" EditCellServerTemplateId="svtActions"
                        Align="Center" />
                </Columns>
            </ComponentArt:GridLevel>
            <ComponentArt:GridLevel DataKeyField="Id" DataMember="CampaignLinks" HeadingCellCssClass="HeadingCell"
                HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" RowCssClass="Row SubRow"
                DataCellCssClass="DataCell" SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif"
                ShowTableHeading="false" SelectedRowCssClass="SelectedRow" SortAscendingImageUrl="asc.gif"
                SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                AlternatingRowCssClass="AlternateDataRow SubRow" ShowSelectorCells="false" ShowHeadingCells="false">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn DataField="CampaignId" Visible="false" />
                    <ComponentArt:GridColumn Width="376" runat="server" HeadingText="<%$ Resources:GUIStrings, Title %>"
                        HeadingCellCssClass="FirstHeadingCell" DataField="Name" DataCellCssClass="FirstDataCell"
                        SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="CampaignNameCT" />
                    <ComponentArt:GridColumn Width="110" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>"
                        DataField="Status" SortedDataCellCssClass="SortedDataCell" FixedWidth="true"
                        DataCellClientTemplateId="StatusChildCT" />
                    <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                        DataField="Visits" SortedDataCellCssClass="SortedDataCell" FixedWidth="true"
                        TextWrap="true" Align="Right" />
                    <ComponentArt:GridColumn Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, BounceRate %>"
                        DataField="BounceRate" DataCellClientTemplateId="BounceRateCT" FixedWidth="true"
                        SortedDataCellCssClass="SortedDataCell" Align="Right" />
                    <ComponentArt:GridColumn Width="110" runat="server" HeadingText="<%$ Resources:GUIStrings, Sends %>"
                        DataField="Sends" FixedWidth="true" SortedDataCellCssClass="SortedDataCell" Align="Right" />
                    <ComponentArt:GridColumn Width="110" runat="server" HeadingText="<%$ Resources:GUIStrings, Deliveries %>"
                        DataField="Deliveries" FixedWidth="true" SortedDataCellCssClass="SortedDataCell" Align="Right" />
                    <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, Clicks %>"
                        DataField="Clicks" FixedWidth="true" SortedDataCellCssClass="SortedDataCell" Align="Right" />
                    <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, Action %>"
                        DataField="" FixedWidth="true" SortedDataCellCssClass="SortedDataCell" Align="Center" />
                    <ComponentArt:GridColumn DataField="Type" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ItemCollapse EventHandler="grdCampaign_toggleItem" />
            <ItemExpand EventHandler="grdCampaign_toggleItem" />
            <ContextMenu EventHandler="grdCampaign_onContextMenu" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="GroupNameCT">
                <span title="## DataItem.getMember('Name').get_text() ##">## DataItem.getMember('Name').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CampaignNameCT">
                <span title="## DataItem.getMember('Name').get_text() ##">## DataItem.getMember('Name').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusCT">
                <span title="## DataItem.getMember('Status')!=null
                    ? DataItem.getMember('Status').get_text() ==1 ? 'Active' : DataItem.getMember('Status').get_text()
                    ==3 ? 'Deleted' : DataItem.getMember('Status').get_text() ==2 ? 'Inactive' :'' :
                    '' ##">## DataItem.getMember('Status')!=null
                    ? DataItem.getMember('Status').get_text() ==1 ? 'Active' : DataItem.getMember('Status').get_text()
                    ==3 ? 'Deleted' : DataItem.getMember('Status').get_text() ==2 ? 'Inactive' :'' :
                    '' ##
                </span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusChildCT">
                    <span title="## GetStatusText(DataItem) ##">## GetStatusText(DataItem) ##
                                    
                </span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BounceRateCT">
                    <span title="## DataItem.getMember('BounceRate').get_text()## ">## DataItem.getMember('BounceRate').get_text() ## 
            </span>
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="svtActions">
                <Template>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center">
                                <asp:Image ID="imgSave" runat="server" ImageUrl="/iapps_images/cm-icon-add.png"
                                    AlternateText="<%$ Resources:GUIStrings, SaveCampaign %>" onclick="SaveCampaign();" />
                            </td>
                            <td align="center">
                                <asp:Image ID="imgCancel" runat="server" ImageUrl="/iapps_images/cm-icon-delete.png"
                                    AlternateText="<%$ Resources:GUIStrings, CancelEditing %>" onclick="CancelEditing();" />
                            </td>
                        </tr>
                    </table>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtCampaign">
                <Template>
                    <asp:TextBox ID="txtCampaign" runat="server" CssClass="textBoxes" Width="300" MaxLength="255"></asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtBounceRate">
                <Template>
                    <span title="<%# Container.DataItem["BounceRate"]!=DBNull.Value? (((decimal)Container.DataItem["BounceRate"]) * 100).ToString("0.##"): string.Empty %>  ">
                        <%# Container.DataItem["BounceRate"] != DBNull.Value ? ((decimal)Container.DataItem["BounceRate"] * 100).ToString("0.##") : string.Empty%>
                    </span>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
    </ComponentArt:Grid>
</div>
</asp:Content>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignGoalsForCampaign.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Popups.AssignGoalsForCampaign" StylesheetTheme="General" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerCampaignGoal %>" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePopup() {
            parent.campaignLinksWindow.hide();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, Selectagoalwhichneedstobeattachedtothiscampaign %>"/></p>
    <asp:ListBox ID="rdlGoals" Width="300" Height="250" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnClose" title="<%= GUIStrings.Close %>" value="<%= GUIStrings.Close %>" class="button" onclick="ClosePopup();" />
</asp:Content>
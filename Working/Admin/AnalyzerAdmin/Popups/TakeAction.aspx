﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TakeAction.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.TakeAction"
    StylesheetTheme="General" MasterPageFile="~/Popups/iAPPSPopup.Master" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerTakeAction %>" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePopup() {
            parent.takeActionWindow.hide();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p style="margin-bottom: 10px;">
        <strong>
            <asp:Literal ID="ltIssue" runat="server" Text="<%$ Resources:GUIStrings, GOALNAMEREASON %>"></asp:Literal></strong></p>
    <p style="margin-bottom: 10px;">
        <asp:Literal ID="ltIssueDesc" runat="server" Text="<%$ Resources:GUIStrings, ISSUEDESCRIPTION %>"></asp:Literal></p>
    <asp:Panel runat="server" ID="pnlSiteEditor">
        <p style="margin-top: 10px;">
            <strong>
                <asp:Literal ID="ltTitle" runat="server" Text="" /></strong></p>
        <p>
            <label>
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, LastEdited1 %>" />
            </label>
            <strong>
                <asp:Literal ID="ltLastEdited" runat="server" Text="<%$ Resources:GUIStrings, Date1 %>"></asp:Literal></strong></p>
        <p>
            <label>
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, LastEditedBy %>" />
            </label>
            <strong>
                <asp:Literal ID="ltLastEditedBy" runat="server" Text="<%$ Resources:GUIStrings, Name1 %>"></asp:Literal></strong></p>
        <asp:Button ID="btnEditor" runat="server" Text="<%$ Resources:GUIStrings, EditinSiteEditor %>"
            ToolTip="<%$ Resources:GUIStrings, EditinSiteEditor %>" CssClass="primarybutton"
            UseSubmitBehavior="false" />
    </asp:Panel>
    <p>
        <label id="lblDSGReference" runat="server" visible="false">
        </label>
    </p>
    <asp:Panel runat="server" ID="pnlList">
        <asp:Repeater runat="server" ID="rptList">
            <HeaderTemplate>
                <table border="0" width="100%" cellpadding="0" cellspacing="0" class="Grid">
                    <tr class="HeadingRow">
                        <td class="HeadingCell">
                            <strong>
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, PageName1 %>" /></strong>
                        </td>
                        <td class="HeadingCell">
                            <strong>
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, PageViews3 %>" /></strong>
                        </td>
                        <td class="HeadingCell">
                            <strong>
                                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, PublishedBy1 %>" /></strong>
                        </td>
                        <td class="HeadingCell">
                            <strong>
                                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, PublishedDate1 %>" /></strong>
                        </td>
                        <td class="HeadingCell" align="center">
                            <strong>
                                <%= GUIStrings.Action %></strong>
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="Row">
                    <td class="DataCell" title='<%# DataBinder.Eval(Container.DataItem, "Title") %>'>
                        <%# DataBinder.Eval(Container.DataItem, "Title") %>
                    </td>
                    <td class="DataCell" align="right">
                        <%# DataBinder.Eval(Container.DataItem, "PageViews") %>
                    </td>
                    <td class="DataCell">
                        <%# DataBinder.Eval(Container.DataItem, "Published_By")%>
                    </td>
                    <td class="DataCell">
                        <%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "Published_Date")).ToString("MM/dd/yyyy")%>
                    </td>
                    <td class="DataCell">
                        <asp:Button ID="btnListEditor" runat="server" Text="<%$ Resources:GUIStrings, EditinSiteEditor %>"
                            ToolTip="<%$ Resources:GUIStrings, EditinSiteEditor %>" CssClass="button" />
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="AlternateDataRow">
                    <td class="DataCell" title='<%# DataBinder.Eval(Container.DataItem, "Title") %>'>
                        <%# DataBinder.Eval(Container.DataItem, "Title") %>
                    </td>
                    <td class="DataCell" align="right">
                        <%# DataBinder.Eval(Container.DataItem, "PageViews") %>
                    </td>
                    <td class="DataCell">
                        <%# DataBinder.Eval(Container.DataItem, "Published_By")%>
                    </td>
                    <td class="DataCell">
                        <%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "Published_Date")).ToString("MM/dd/yyyy")%>
                    </td>
                    <td class="DataCell">
                        <asp:Button ID="btnListEditor" runat="server" Text="<%$ Resources:GUIStrings, EditinSiteEditor %>"
                            ToolTip="<%$ Resources:GUIStrings, EditinSiteEditor %>" CssClass="button" />
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnClose" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
        class="button" onclick="return ClosePopup();" />
</asp:Content>
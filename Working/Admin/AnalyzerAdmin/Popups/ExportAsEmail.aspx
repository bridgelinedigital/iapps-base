<%@ Page Language="C#" AutoEventWireup="true" Inherits="Popups_ExportAsEmail" Theme="General" CodeBehind="ExportAsEmail.aspx.cs" Title="<%$ Resources:GUIStrings, ExportAsEmail %>"
    MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">

        function hidePopup() {
            parent.viewPopupWindow.hide();
        }

        function ValidateExportEmails() {
            var ctlEmail = document.getElementById("<%=tbCodeEmbed.ClientID%>");
            var strEmail = ctlEmail.value.replace(/\s+/g, '');
            ctlEmail.value = strEmail.toString();
            var str = strEmail;
            var strEmailId = str.split(",");

            if (Trim(ctlEmail.value) != "") {
                for (var i = 0; i < strEmailId.length; i++) {
                    if (ValidateEmailFunction(strEmailId[i]) == true) {
                        document.getElementById("<%=hdnPopup.ClientID%>").value = true;
                    }
                    else {
                        var x = $.inArray(";", strEmailId.toString());
                        if (x > 1) {
                            alert("One or more emails have been seperated by semicolon delimiter. Use comma delimiter");
                        }
                        else {
                            alert(GetMessage("EmailNotValid"));
                        }
                        ctlEmail.focus();
                        return false;
                    }
                } return true;
            }
            else {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseprovideatleastoneemailaddress %>'/>");
                return false;
            }
            return false;
        }

    $(document).ready(function () {
        if (typeof document.getElementById("<%=hdnPopup.ClientID%>") != "undefined")
            if (document.getElementById("<%=hdnPopup.ClientID%>").value == 'true')
                parent.viewPopupWindow.hide();
    });

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.Separateeachaddresswithacomma %></label>
        <div class="form-value">
            <asp:TextBox ID="tbCodeEmbed" runat="server" TextMode="MultiLine" Text="" Width="280" 
                Height="75" />
            <div style="visibility: hidden;">
                <ComponentArt:Grid SkinID="Default" ID="GridEmailExport" runat="server" RunningMode="Client"
                    ClientTarget="Downlevel" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
                    AutoPostBackOnSelect="false" PageSize="10">
                    <Levels>
                        <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" RowCssClass="Row"
                            ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell"
                            HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
                            HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow"
                            GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                            SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="DecimalTemplate">
                            ## GetContentDecimal(DataItem.GetMember(DataItem.GetCurrentMember().Value)) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="hidePopup()" />
    <asp:Button ID="btnExport" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, SendEmail %>"
        ToolTip="<%$ Resources:GUIStrings, SendEmail %>" OnClientClick="return ValidateExportEmails();" />
    <asp:HiddenField ID="hdnPopup" runat="server" ClientIDMode="Static" />
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" StylesheetTheme="General" MasterPageFile="~/Popups/iAPPSPopup.Master"
    CodeBehind="LinkBuilder.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.LinkBuilder"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerLinkBuilder %>" %>

<%@ Register TagName="Criteria" TagPrefix="UC" Src="~/UserControls/Navigation/NavigationCriteria.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".link-builder").on("change", "#ddlMedium", function () {
                ToggleMedium();
            });

            ToggleMedium();
        });

        function ToggleMedium() {
            $("#other_detail").hide();
            if ($("#ddlMedium").val() == "3")
                $("#other_detail").show();
        }

        function OnSaveLink() {
            var isValid = $(".nav-criteria").navCriteria("saveSelectedCriteria");
            if (!isValid)
                alert("<%= JSMessages.Pleaseselectapageorproductfordestination %>");
            else if (cmbSource.get_text() == "") {
                isValid = false;
                alert("<%= GUIStrings.SourceNameShouldNotBeEmpty %>");
            }
            else if ($("#ddlMedium").val() == 3 && $("txtOtherText").val() == "") {
                isValid = false;
                alert("<%= JSMessages.PleaseEnterAValueInOtherColumn %>");
            }

    return isValid;
}
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="page-sub-header clear-fix">
        <h2>
            <%= GUIStrings.SelectDestination %></h2>
    </div>
    <UC:Criteria ID="destination" runat="server" />
    <div class="page-sub-header clear-fix">
        <h2>
            <%= GUIStrings.SourceMedium %></h2>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.Source1 %></label>
        <div class="form-value">
            <ComponentArt:ComboBox ID="cmbSource" runat="server" RunningMode="Client" Width="300"
                AutoTheming="true" AutoHighlight="false" AutoComplete="true" AutoFilter="true"
                KeyboardEnabled="true" DropDownPageSize="10" DataTextField="Name" DataValueField="Name"
                SkinID="Default">
            </ComponentArt:ComboBox>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.Medium1 %></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlMedium" runat="server" CssClass="link-medium" Width="300"
                ClientIDMode="Static">
                <asp:ListItem Value="0" Text="<%$ Resources:GUIStrings, PPC %>" />
                <asp:ListItem Value="1" Text="<%$ Resources:GUIStrings, Banner %>" />
                <asp:ListItem Value="2" Text="<%$ Resources:GUIStrings, TextLink %>" />
                <asp:ListItem Value="3" Text="<%$ Resources:GUIStrings, Other %>" />
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-row" id="other_detail" style="display: none;">
        <label class="form-label">
            &nbsp;</label>
        <div class="form-value">
            <asp:TextBox ID="txtOtherText" runat="server" CssClass="textBoxes" Width="290" MaxLength="255" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnClose" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
        class="button cancel-button" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClick="btnSave_Click"
        CausesValidation="true" OnClientClick="return OnSaveLink();" />
</asp:Content>

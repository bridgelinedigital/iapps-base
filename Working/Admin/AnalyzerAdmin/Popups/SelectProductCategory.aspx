﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectProductCategory.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master"
    StylesheetTheme="General" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Popups.SelectProductCategory" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        //<![CDATA[

        var id;
        var text;
        function tree_onNodeSelect(sender, eventArgs) {
            var text = eventArgs.get_node().get_text();
            var id = eventArgs.get_node().get_id();
            window.parent.selectedProductType(id, text);
        }

        function CloseDialog() {
            parent.viewPopupWindow.hide();
        }

        //Context menu for Tree.. disable or enable
        function tree_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            selectedTreeNode = eventArgs.get_node();

            TreeContextMenu.showContextMenuAtEvent(e, selectedTreeNode);

            id = eventArgs.get_node().get_id()
            text = eventArgs.get_node().get_text(); ;
        }

        function Menu_OnClick(sender, eventArgs) {
            window.parent.selectedProductType(id, text);
            CloseDialog();
        }

        //]]>
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:TreeView ID="treeNavigation" runat="server" SkinID="Default" AutoScroll="true"
        Visible="true" FillContainer="true" AutoPostBackOnSelect="false" MultipleSelectEnabled="false"
        Height="293" Width="367">
        <ClientEvents>
            <ContextMenu EventHandler="tree_onContextMenu" />
            <NodeSelect EventHandler="tree_onNodeSelect" />
        </ClientEvents>
    </ComponentArt:TreeView>
    <asp:SiteMapDataSource ID="SiteMapDSNav" runat="server" SiteMapProvider="folderNavProvider"
        ShowStartingNode="True" />
    <ComponentArt:Menu ID="TreeContextMenu" SkinID="ContextMenu" Orientation="Vertical"
        Width="220" ContextMenu="ControlSpecific" ContextControlId="gridForm" runat="server"
        ExpandOnClick="true">
        <ItemLooks>
            <ComponentArt:ItemLook LookId="DefaultItemLook" CssClass="msMenuItem" HoverCssClass="msMenuItemHover"
                LabelPaddingLeft="15" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="3" />
            <ComponentArt:ItemLook LookId="BreakItem" ImageUrl="break.gif" CssClass="msMenuBreak"
                ImageHeight="1" ImageWidth="100%" />
        </ItemLooks>
        <Items>
            <ComponentArt:MenuItem ID="selectContent" runat="server" Text="<%$ Resources:GUIStrings, Select %>">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="Menu_OnClick" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Close %>" class="button" onclick="CloseDialog()" />
</asp:Content>
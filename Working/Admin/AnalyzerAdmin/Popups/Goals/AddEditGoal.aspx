﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEditGoal.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.AddEditGoal"
    StylesheetTheme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:localize runat="server" text="<%$ Resources:GUIStrings, iAPPSAnalyzerAddEditGoal %>"/></title>
    <script type="text/javascript">
        function ClosePopup() {
            parent.AddEditGoalWindow.hide();
        }
    </script>
</head>
<body class="popupBody">
    <form id="form1" runat="server">
    <div class="AddEditGoal">
        <div class="popupBoxShadow">
            <div class="popupboxContainer">
                <div class="dialogBox">
                <asp:ValidationSummary runat="server" ShowMessageBox="true" ShowSummary="false" ID="vssummary" />
                    <h3><asp:localize runat="server" text="<%$ Resources:GUIStrings, GoalName1 %>"/>&nbsp;<asp:Literal ID="ltGoalName" runat="server" Text="<%$ Resources:GUIStrings, JerseyBlowout %>"></asp:Literal></h3>
                    <div class="leftContent" style="height: 216px;">
                        <span class="formRow">
                            <label class="formLabel"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Threshold1 %>"/></label>
                        </span>
                        <span class="formRow">
                            <asp:Label ID="lblThresholdOperator" runat="server"  Visible="true"></asp:Label>
                            <asp:TextBox ID="txtThresholdMinValue" runat="server" CssClass="textBoxes" Width="70"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtThresholdMinValue" ID="rfvtxtThresholdMinValue" ErrorMessage="<%$ Resources: JSMessages, pleaseenterminimumvalue %>" Text="*"></asp:RequiredFieldValidator>
                            <asp:CompareValidator Operator="DataTypeCheck" Type="Double"  runat="server" ControlToValidate="txtThresholdMinValue" ID="RequiredFieldValidator1" ErrorMessage="<%$ Resources: JSMessages, pleaseenteravalidnumberforminimumvalue %>" Text="*" />
                            <asp:Label ID="lblThresholdAnd" runat="server" Text=" <%$ Resources:GUIStrings, and %> " Visible="false" style="margin-right: 7px; display: inline-block;"></asp:Label>
                            <asp:TextBox ID="txtThresholdMaxValue" runat="server" CssClass="textBoxes" Width="70"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtThresholdMaxValue" ID="rfvtxtThresholdMaxValue" ErrorMessage="<%$ Resources: JSMessages, pleaseentermaximumvalue %>" Text="*"></asp:RequiredFieldValidator>
                            <asp:CompareValidator Operator="DataTypeCheck" Type="Double"  runat="server" ControlToValidate="txtThresholdMaxValue" ID="CompareValidator1" ErrorMessage="<%$ Resources: JSMessages, pleaseenteravalidnumberformaximumvalue %>" Text="*" />
                            <asp:Label ID="lblNumberFormat" runat="server" Text="<%$ Resources:GUIStrings, and %>" Visible="false"></asp:Label>
                        </span>
                        <span class="formRow">
                            <label class="formLabel"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Status2 %>"/></label>
                            <asp:RadioButtonList ID="rbStatus" runat="server" RepeatDirection="Vertical">
                                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Active %>" Value="1"></asp:ListItem>
                                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Inactive %>" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </span>
                    </div>
                    <div class="rightContent">
                        <span class="formRow">
                            <label class="formLabel"><asp:localize runat="server" text="<%$ Resources:GUIStrings, ExistingCampaigns %>"/></label>
                            <div class="scrollingContainer">
                                <asp:CheckBoxList ID="chkCampaigns" runat="server" RepeatDirection="Vertical">
                                </asp:CheckBoxList>
                            </div>
                        </span>
                    </div>
                    <div class="clearFix">&nbsp;</div>
                    <div class="footerContent">
                        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
                            onclick="btnSave_Click" />
                        <input type="button" id="btnClose" title="<%= GUIStrings.Close %>" value="<%= GUIStrings.Close %>" class="button" onclick="ClosePopup();"
                             />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>

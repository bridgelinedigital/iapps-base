﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GoalAlerts.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.GoalAlerts"
    StylesheetTheme="General" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerGoalAlets %>" MasterPageFile="~/Popups/iAPPSPopup.Master" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePopup() {
            parent.GoalAlertsWindow.hide();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p><%= GUIStrings.Selectemailaddressesandgoalsyouwishtosetalertfor %></p>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Frequency %></label>
        <div class="form-value">
            <asp:DropDownList runat="server" ID="ddlFrequency">
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Daily %>" Value="1"
                    Selected="True"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Weekly %>" Value="2"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Monthly %>" Value="3"></asp:ListItem>
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Yearly %>" Value="4"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.EmailAddresses %></label>
        <div class="form-value scrolling-div">
            <asp:CheckBoxList ID="chkEmails" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Goals1 %></label>
        <div class="form-value scrolling-div">
            <asp:CheckBoxList ID="chkGoals" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnClose" title="<%= GUIStrings.Close %>" value="<%= GUIStrings.Close %>"
        class="button" onclick="ClosePopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClick="btnSave_Click" />
</asp:Content>
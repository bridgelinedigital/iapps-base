﻿<%@ Page Language="C#" AutoEventWireup="true" StylesheetTheme="General" MasterPageFile="~/Popups/iAPPSPopup.Master"
    CodeBehind="SelectWatch.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.SelectWatch"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerSelectWatch %>" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
   
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Watch1 %></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlWatch" runat="server" Width="240" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Cancel %>" class="button cancel-button" />
    <asp:Button ID="btnSelect" runat="server" CssClass="primarybutton"
        Text="<%$ Resources:GUIStrings, Select %>" ToolTip="<%$ Resources:GUIStrings, Select %>" />
</asp:Content>

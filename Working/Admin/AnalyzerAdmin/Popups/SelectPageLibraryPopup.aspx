﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Popups.SelectPageLibraryPopup" StylesheetTheme="General"
    CodeBehind="SelectPageLibraryPopup.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="UC" TagName="PageLibrary" Src="~/UserControls/Libraries/Data/ManagePageLibrary.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            setTimeout(function () {
                $("#page_library").iAppsSplitter();
            }, 200);
        });

        function GetPageVariationsValue() {
            if ($("#<%=chkPageVariations.ClientID%>").attr("Checked") == "checked")
                return "true";
            else
                return "false";
        }
    </script>
    <link runat="server" rel="stylesheet" type="text/css" href="../css/LibraryPopup/LibraryPopupStyle.css" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:PlaceHolder ID="phPageVariations" runat="server" Visible="false">
        <div class="form-row">
            <asp:CheckBox ID="chkPageVariations" runat="server" Text="<%$ Resources:GUIStrings,IncludePageVariations %>"
                Checked="true" />
        </div>
    </asp:PlaceHolder>
    <UC:PageLibrary ID="pageLibrary" runat="server" HideTreeContextMenu="true" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, SelectPage%>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SelectPage %>" OnClientClick="return SelectPageFromLibrary();" />
</asp:Content>

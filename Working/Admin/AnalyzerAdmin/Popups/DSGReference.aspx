﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DSGReference.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.DSGReference" StylesheetTheme="General" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>iAPPS Analyzer: Digital Strategy Group</title>
    <script type="text/javascript">
        function ClosePopup() {
            parent.DSGWindow.hide();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="campaignLinks">
            <div class="popupBoxShadow">
                <div class="popupboxContainer">
                    <div class="dialogBox">
                        <h3>Digital Strategy Group</h3>
                        <div class="paddingContainer">
                            <p>Digital Strategy Group</p>
                        </div>
                        <div class="clearFix">&nbsp;</div>
                        <div class="footerContent">
                            <input type="button" id="btnClose" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>" class="button"
                                onclick="return ClosePopup();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
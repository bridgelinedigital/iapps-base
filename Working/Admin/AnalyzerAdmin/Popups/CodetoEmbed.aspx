﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Popups_CodetoEmbed" Theme="General"
    Title="<%$ Resources:GUIStrings, CodetoEmbed %>" CodeBehind="CodetoEmbed.aspx.cs"
    MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript">
        function hidePopup() {
            parent.viewPopupWindow.hide();
        }

        function CopyToClipboard() {

            document.form1.lbCodeEmbed.focus();
            document.form1.lbCodeEmbed.select();
            var CopiedTxt = document.selection.createRange();
            CopiedTxt.execCommand("Copy");
        }
        function copy(text2copy) {
            if (window.clipboardData) {
                window.clipboardData.setData("Text", text2copy);
            }
            else {
                var flashcopier = 'flashcopier';
                if (!document.getElementById(flashcopier)) {
                    var divholder = document.createElement('div');
                    divholder.id = flashcopier;
                    document.body.appendChild(divholder);
                }
                document.getElementById(flashcopier).innerHTML = '';
                var divinfo = '<embed src="_clipboard.swf" FlashVars="clipboard=' + escape(text2copy) + '" width="0" height="0" type="application/x-shockwave-flash"></embed>';
                document.getElementById(flashcopier).innerHTML = divinfo;
            }
        }
        function do_copy() {
            copy(gid("lbCodeEmbed").value);
            if (document.selection) {
            }
            else {
            }
            return false;
        }
        function gid(id) {

            return document.getElementById(id)
        }
        function setClipboard(text) {
            var url = [
		        'data:text/html;charset=utf-8;base64,PGJvZHk+PC9ib2',
		        'R5PjxzY3JpcHQgdHlwZT0idGV4dC9qYXZhc2NyaXB0Ij4KKGZ1',
		        'bmN0aW9uKGVuY29kZWQpe3ZhciBzd2ZfZGF0YSA9IFsKICdkYX',
		        'RhOmFwcGxpY2F0aW9uL3gtc2hvY2t3YXZlLWZsYXNoO2Jhc2U2',
		        'NCxRMWRUQjJ3JywKICdBQUFCNG5EUGdZbGpBd01qSTRNejAlMk',
		        'YlMkY5JTJGZTJaZkJnYUdhV3dNRE1uNUthJywKICdrTU10TjRH',
		        'ZGdaZ1NJTXdaWEZKYW01UUFFJTJCQm9iaTFCTG5uTXlDcFB6RW',
		        '9oU0dJJywKICdQRnAlMkZBeHNEREJRa3BGWkRGUUZGQ2d1eVM4',
		        'QXlqSTRBRVVCaXkwVndBJTNEJTNEJwpdLmpvaW4oIiIpOwpkb2',
		        'N1bWVudC5ib2R5LmlubmVySFRNTCA9IFsKICc8ZW1iZWQgc3Jj',
		        'PSInLHN3Zl9kYXRhLCciICcsCiAnRmxhc2hWYXJzPSJjb2RlPS',
		        'csZW5jb2RlZCwnIj4nLAogJzwvZW1iZWQ+JwpdLmpvaW4oIiIp',
		        'Owp9KSgi',
		        base64encode(encodeURIComponent(text) + '")</' + 'script>')
	        ].join("");
                    var tmp = document.createElement("div");
                    tmp.innerHTML = [
		         '<iframe src="', url, '"'
		        , ' width="0" height="0">'
		        , '</iframe>'
	        ].join("");
            with (tmp.style) {
                position = "absolute";
                left = "-10px";
                top = "-10px";
                visibility = "hidden";
            };
            document.body.appendChild(tmp);
            setTimeout(function () { document.body.removeChild(tmp) }, 1000);
            function base64encode(str) {
                var Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".split("");
                var c1, c2, c3;
                var buf = [];
                var len = str.length;
                var i = 0;
                while (i < len) {
                    c1 = str.charCodeAt(i) & 0xff;
                    c2 = str.charCodeAt(i + 1);
                    c3 = str.charCodeAt(i + 2);
                    buf.push(Chars[(c1 >> 2)]);
                    if (i + 1 == len) {
                        buf.push(Chars[(c1 & 0x3) << 4], "==");
                        break;
                    }
                    buf.push(Chars[((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4)]);
                    if (i + 2 == len) {
                        buf.push(Chars[(c2 & 0xF) << 2], "=");
                        break;
                    }
                    buf.push(
				        Chars[((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6)],
				        Chars[(c3 & 0x3F)]
			        );
                    i += 3;
                }
                return buf.join("");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.Cutandpastethefollowingcode %></label>
        <div class="form-value">
            <asp:TextBox ID="lbCodeEmbed" runat="server" TextMode="MultiLine" Width="280" Height="78" ClientIDMode="Static" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Close %>"
        ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="hidePopup()" />
</asp:Content>

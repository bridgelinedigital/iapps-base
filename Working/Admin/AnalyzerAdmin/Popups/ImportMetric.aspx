﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportMetric.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.ImportMetric"
    StylesheetTheme="General" MasterPageFile="~/Popups/iAPPSPopup.Master" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerImportMetric %>" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <!--[if lt IE 9]>
    <style type="text/css">
        div.fakeFile { left: 0 !important; }
        input.button, input.primarybutton { padding: 3px 5px; }
    </style>
    <![endif]-->
    <script type="text/javascript">
        function ValidateUpload() {
            var upload = document.getElementById("<%=fileUploads.ClientID %>").value;

            if (upload == "") {
                alert(__JSMessages["PleaseSelectAFileToUpload"]);
                return false;
            }
            if (upload.substring(upload.length - 3, upload.length) != "csv") {
                alert(__JSMessages["TheUploadedFileMustBeACSVFile"]);
                return false;
            }
            return true;
        }

        function ClosePopup() {
            parent.importMetricWindow.hide();
            return false;
        }
        function setFileOnPath(objFile) {
            var txtVal = document.getElementById("hdnName").value;
            document.getElementById(txtVal).value = objFile.value;
        }
        function ChangeFileButton(browseButtonId) {
            var btnBrowse = document.getElementById(browseButtonId);
            if (btnBrowse != null) {
                changeButton(btnBrowse);
            }
        }
        function RefreshAndClosePopup() {
            parent.grdThresholdMetric.callback();
            parent.importMetricWindow.hide();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p>
        <asp:Label ID="ErrorLabel" runat="server" ForeColor="red" /></p>
    <asp:FileUpload ID="fileUploads" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnClose" value="<%= GUIStrings.Cancel %>" title="<%= GUIStrings.Cancel %>"
        class="button" onclick="return ClosePopup();" />
    <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="primarybutton"
        OnClientClick="return ValidateUpload();" OnClick="btnUpload_Click" />
</asp:Content>

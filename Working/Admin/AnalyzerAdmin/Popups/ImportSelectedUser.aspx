<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Popups_ImportSelectedUser" Theme="General" Codebehind="ImportSelectedUser.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:localize runat="server" text="<%$ Resources:GUIStrings, ImportSelectedUser %>"/></title>
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0Of1Items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0Of1Items %>'/>"; //added by Adams

    var userToImport;
    function grdImportSelected_OnItemSelect(sender, eventArgs)
    {
        userToImport = eventArgs.get_item();
        parent.isImportedUser = "false";
    }
    function getSelectUser()
    {   
        var grdSelectedItems = grdImportSelectedUser.getSelectedItems();
        if(grdSelectedItems.length > 0)
        {
            parent.selectedUserRow = userToImport;
            parent.isImportedUser = "true";
            parent.PreFillImportedUser();
            parent.importPopupWindow.hide();
        }
        else
        {
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseselectausertoimport %>'/>");
            return false;
        }
    }
    function clearDefaultText(objTextbox)
    {
        if (objTextbox.value == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Search %>'/>")
            objTextbox.value = "";
    }
    function setDefaultText(objTextbox)
    {
        if(Trim(objTextbox.value) == "")
            objTextbox.value = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Search %>'/>";
    }
    function hidePopup()
    {
        parent.importPopupWindow.hide();
    }
    
    function searchUsersGrid(evt)
    {
        if(validKey(evt))
        {
            var str = document.getElementById("txtSearchUsers").value;
            grdImportSelectedUser.Search(str, false);
        }
    }
    </script>

</head>
<body class="popupBody">
    <form id="form1" runat="server">
        <div id="dialog" class="selectedUser">
            <div class="popupLibraryBoxShadow">
                <div class="popupLibraryContainer">
                    <div class="boxHeader">
                        <h3>
                            <asp:localize runat="server" text="<%$ Resources:GUIStrings, ImportSelectedUser %>"/></h3>
                        <div class="searchDiv">
                            <label for="txtSearch">
                                <asp:localize runat="server" text="<%$ Resources:GUIStrings, SearchTable %>"/></label>
                            <input type="text" class="textBoxes" onfocus="clearText(this);" onblur="setText(this);"
                                id="txtSearchUsers" name="txtSearchUsers" value="<%= GUIStrings.TypeHereToFilterResults %>"
                                onkeyup="searchUsersGrid(event);" />
                            <%--<asp:TextBox ID="txtSearch" cssClass="textBoxes" Width="200" runat="server" Text="Search"
                            onfocus="clearDefaultText(this);" onblur="setDefaultText(this);"></asp:TextBox>--%>
                        </div>
                    </div>
                    <div class="popupBoxContent">
                        <div class="popupGrid">
                            <p>
                                <asp:localize runat="server" text="<%$ Resources:GUIStrings, SelecttheuseryouwishtoimportfromtheiAPPSFramework %>"/></p>
                            <div class="rightContent importContainer">
                                <div class="fileGridTopGrid">
                                    <h4>
                                    </h4>
                                    <div id="TopGridHeader" class="gridCombo">
                                        <asp:Label ID="DisplayingRecords" runat="server" />
                                        <%--<span>Displaying&nbsp;</span>--%>
                                        <asp:Label runat="server" ID="startRecord" Text="1" Visible="false"></asp:Label>
                                        <%--<span>&nbsp;-&nbsp;</span>--%>
                                        <asp:Label runat="server" ID="endRecord" Text="10" Visible="false"></asp:Label>
                                        <%--<span>&nbsp;of&nbsp;</span>--%>
                                        <asp:Label runat="server" ID="totalRecords" Text="100" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <ComponentArt:Grid SkinID="Default" ID="grdImportSelectedUser" runat="server" RunningMode="Client"
                                    EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" PagerInfoClientTemplateId="grdImportSelectedUserPagination"
                                    SliderPopupClientTemplateId="grdImportSelectedUserSlider" Width="560" PageSize="10" Height="275">
                                    <Levels>
                                        <ComponentArt:GridLevel DataKeyField="UserId" ShowTableHeading="false" ShowSelectorCells="false"
                                            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                                            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                                            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                                            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                                            <Columns>
                                                <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, FirstName1 %>" HeadingCellCssClass="FirstHeadingCell"
                                                    DataField="FirstName" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                                                    IsSearchable="true" AllowReordering="false" FixedWidth="true" />
                                                <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, LastName1 %>" DataField="LastName"
                                                    IsSearchable="true" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                                                    FixedWidth="true" />
                                                <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, UserName1 %>" DataField="UserName"
                                                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true" IsSearchable="true"/>
                                                <ComponentArt:GridColumn Width="180" runat="server" HeadingText="<%$ Resources:GUIStrings, ProductSuites %>" DataField="ProductSuite"
                                                    FixedWidth="true" DataCellCssClass="LastDataCell" SortedDataCellCssClass="SortedDataCell"
                                                    AllowReordering="false" IsSearchable="false"/>
                                                <ComponentArt:GridColumn Visible="False" DataField="UserId" IsSearchable="false"/>
                                                <ComponentArt:GridColumn Visible="False" DataField="ExpiryDate" IsSearchable="false"/>
                                                <ComponentArt:GridColumn Visible="False" DataField="Email" IsSearchable="false"/>
                                                <ComponentArt:GridColumn Visible="False" DataField="EmailNotification" IsSearchable="false"/>
                                            </Columns>
                                        </ComponentArt:GridLevel>
                                    </Levels>
                                    <ClientEvents>
                                        <ItemSelect EventHandler="grdImportSelected_OnItemSelect" />
                                    </ClientEvents>
                                    <ClientTemplates>
                                        <ComponentArt:ClientTemplate ID="grdImportSelectedUserSlider">
                                            <div class="SliderPopup">
                                                <h5>
                                                    ## DataItem.GetMember(0).Value ##</h5>
                                                <p>
                                                    ## DataItem.GetMember(1).Value ##</p>
                                                <p>
                                                    ## DataItem.GetMember(2).Value ##</p>
                                                <p>
                                                    ## DataItem.GetMember(3).Value ##</p>
                                                <p>
                                                    ## DataItem.GetMember(4).Value ##</p>
                                                <p class="paging">
                                                    <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdImportSelectedUser.PageCount) ##</span>
                                                    <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdImportSelectedUser.RecordCount) ##</span>
                                                </p>
                                            </div>
                                        </ComponentArt:ClientTemplate>
                                        <ComponentArt:ClientTemplate ID="grdImportSelectedUserPagination">
                                            ## GetGridPaginationInfo(grdImportSelectedUser) ##
                                        </ComponentArt:ClientTemplate>
                                    </ClientTemplates>
                                </ComponentArt:Grid>
                            </div>
                        </div>
                        <div class="clearFix">
                        </div>
                        <div class="popupFooter">
                            <asp:Button ID="btnImportSelectedUser" runat="server" CssClass="primarybutton" 
                                Text="<%$ Resources:GUIStrings, ImportSelectedUser %>" ToolTip="<%$ Resources:GUIStrings, ImportSelectedUser %>"
                                OnClientClick="return getSelectUser();" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button buttonSpace"
                                Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="hidePopup()" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

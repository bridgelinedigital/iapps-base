<%@ Page Language="C#" AutoEventWireup="true" Inherits="Popups_TopViewedItems" StylesheetTheme="General"
    MasterPageFile="~/Popups/iAPPSPopup.Master" CodeBehind="TopViewedItems.aspx.cs"
    Title="<%$ Resources:GUIStrings, CodetoEmbed %>" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var item = "";
        function grdTopViewedItems_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            grdTopViewedItems.select(item);
            var evt = eventArgs.get_event();
            menuTopViewed.showContextMenuAtEvent(evt, eventArgs.get_item());
        }
        function hidePopup() {
            parent.viewPopupWindow.hide();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:Grid SkinID="Default" ID="grdTopViewedItems" runat="server" RunningMode="Client"
        ShowFooter="false" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
        AutoPostBackOnSelect="false" PagerInfoClientTemplateId="grdTopClicksFromPagination"
        Width="280" Height="105" PageSize="5" ShowHeader="false">
        <Levels>
            <ComponentArt:GridLevel AlternatingRowCssClass="AlternateDataRow" AllowSorting="true"
                DataKeyField="RowNumber" ShowTableHeading="false" ShowSelectorCells="false" SelectorCellCssClass="SelectorCell"
                SelectorCellWidth="18" SelectorImageWidth="17" SelectorImageHeight="15" HeadingSelectorCellCssClass="SelectorCell"
                DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" ShowHeadingCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="140" DataField="Title" SortedDataCellCssClass="SortedDataCell"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" />
                    <ComponentArt:GridColumn Width="70" DataField="Count" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="true" FixedWidth="true" />
                    <ComponentArt:GridColumn DataField="RowNumber" IsSearchable="false" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdTopViewedItems_onContextMenu" />
        </ClientEvents>
    </ComponentArt:Grid>
    <ComponentArt:Menu SkinID="ContextMenu" ID="menuTopViewed" runat="server">
        <Items>
            <ComponentArt:MenuItem ID="jumpToEditor" runat="server" Text="<%$ Resources:GUIStrings, JumpToPageInEditor %>"
                Look-LeftIconUrl="cm-icon-edit.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="previewFile" runat="server" Text="<%$ Resources:GUIStrings, PreviewFileImage %>">
            </ComponentArt:MenuItem>
        </Items>
    </ComponentArt:Menu>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Close %>"
        ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="hidePopup()" />
</asp:Content>

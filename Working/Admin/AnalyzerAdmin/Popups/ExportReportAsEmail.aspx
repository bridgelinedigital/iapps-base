<%@ Page Language="C#" AutoEventWireup="True" Inherits="Popups_ExportReportAsEmail"
    StylesheetTheme="General" CodeBehind="ExportReportAsEmail.aspx.cs" Title="<%$ Resources:GUIStrings, ExportAsEmail %>"
    MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function hidePopup() {
            parent.viewPopupWindow.hide();
        }
        function ValidateExportEmails() {
            var expression = /^(([a-zA-Z0-9\-\._]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3})(,(?!$))?)+$/;
            var textareaObject = document.getElementById("<%=tbCodeEmbed.ClientID%>");
            if (Trim(textareaObject.value) != "") {
                if (!expression.test(textareaObject.value)) {
                    alert('<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseentervalidcharactersfortheemailMultipleemailscanbeseparatedusingcomma %>"/>');
                    return false;
                }
                else {
                    alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Reportemailedsuccessfully %>'/>");
                    document.getElementById("<%=hdnPopup.ClientID%>").value = 'true';
                    return true;
                }
            }
            else {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseprovideatleastoneemailaddress %>'/>");
                return false;
            }
        }
        $(document).ready(function () {
            if (typeof document.getElementById("<%=hdnPopup.ClientID%>") != "undefined")
                if (document.getElementById("<%=hdnPopup.ClientID%>").value == 'true')
                    parent.viewPopupWindow.hide();
        });
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p>
        <%= GUIStrings.Separateeachaddresswithacomma %></p>
    <asp:TextBox ID="tbCodeEmbed" runat="server" TextMode="MultiLine" Text="" Width="280"
        Height="78" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="hidePopup()" />
    <asp:Button ID="btnExport" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, SendEmail %>"
        ToolTip="<%$ Resources:GUIStrings, SendEmail %>" OnClientClick="return ValidateExportEmails();" />
    <asp:HiddenField ID="hdnPopup" runat="server" ClientIDMode="Static" />
</asp:Content>

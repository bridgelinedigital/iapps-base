<%@ Page Language="C#" AutoEventWireup="true" Inherits="Popups_PageDetailStatistics"
    Theme="General" CodeBehind="PageDetailStatistics.aspx.cs" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerPageDetailStatistics %>" MasterPageFile="~/Popups/iAPPSPopup.Master" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <!-- Tooltip Script -->
    <script type="text/javascript">
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams

        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var reverseTooltipImage;
        var downTooltipImage;

        var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
        var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

        document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
        document.write('<img id="dhtmlpointer" src="' + tooltipArrowPath + '">'); //write out pointer image

        var ie = document.all;
        var ns6 = document.getElementById && !document.all;
        var enabletip = false;
        if (ie || ns6) {
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
        }
        var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
        }

        function ddrivetip(thetext, thewidth, thecolor, imgSrc, revImgSrc, downImgSrc) {
            document.getElementById("dhtmlpointer").src = imgSrc;
            reverseTooltipImage = revImgSrc;
            downTooltipImage = downImgSrc;
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") {
                    tipobj.style.width = thewidth + "px";
                }
                if (typeof thecolor != "undefined" && thecolor != "") {
                    tipobj.style.backgroundColor = thecolor;
                }
                tipobj.innerHTML = thetext;
                enabletip = true;
                return false;
            }
        }

        function positiontip(e) {
            if (enabletip) {
                var nondefaultpos = false;
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
                var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
                var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
                var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
                var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth) {
                    //move the horizontal position of the menu to the left by it's width
                    pointerobj.src = reverseTooltipImage;
                    tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
                    pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
                    nondefaultpos = true;
                }
                else if (curX < leftedge) {
                    tipobj.style.left = "5px";
                }
                else {
                    //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
                    pointerobj.style.left = (curX + offsetfromcursorX) + "px";
                }
                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight) {
                    pointerobj.src = downTooltipImage;
                    tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
                    pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
                    nondefaultpos = true;
                }
                else {
                    tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
                    pointerobj.style.top = (curY + offsetfromcursorY) + "px";
                }
                tipobj.style.visibility = "visible";
                if (!nondefaultpos)
                    pointerobj.style.visibility = "visible";
                else
                    pointerobj.style.visibility = "visible";
            }
        }
        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false;
                tipobj.style.visibility = "hidden";
                pointerobj.style.visibility = "hidden";
                tipobj.style.left = "-1000px";
                tipobj.style.backgroundColor = '';
                tipobj.style.width = '';
            }
        }
        document.onmousemove = positiontip;
        /** Methods for the dynamic tooltip **/
    </script>

    <script type="text/javascript">
        /**Export**/
        var GroupBy = 0;
        function openExportEmail() {
            setGroupingColumn();
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', '../popups/ExportAsEmail.aspx?DatatableId=' + DatatableId + '&ReportName=' + ReportName + '&GroupBy=' + GroupBy, '', 'width=310px,height=220px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function() {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        /** Methods to get set the tooltip **/
        function CreateTip(dataItemObject) {
            var authorName = dataItemObject.GetMember('Replies').Text;
            authorName = authorName.replace(new RegExp("'", "g"), "\'");

            var publisherName = dataItemObject.GetMember('TotalViews').Text;
            publisherName = publisherName.replace(new RegExp("'", "g"), "\'");

            //var dateTimePublished = dataItemObject.GetMember('StartedBy').Text;
            var dateTimePublished = dataItemObject.GetMember('SqlDateTimestamp').Text;
            dateTimePublished = dateTimePublished.replace(new RegExp("'", "g"), "\'");

            var navigationHierarchy = dataItemObject.GetMember('Subject').Text;
            navigationHierarchy = navigationHierarchy.replace(new RegExp("'", "g"), "\'");

            var strHTML = "";
            var imgTag = "";
            strHTML += "<div>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, Author %>'/></h5>";
            strHTML += "<p>" + authorName + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, PublishedBy %>'/></h5>";
            strHTML += "<p>" + publisherName + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, DateTimePublished %>'/></h5>";
            strHTML += "<p>" + dateTimePublished + "</p>";
            strHTML += "<h5><asp:localize runat='server' text='<%$ Resources:JSMessages, NavigationHierarchy %>'/></h5>";
            strHTML += "<p>" + navigationHierarchy + "</p>";
            strHTML += "</div>";
            strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'','../../App_Themes/General/Images/tooltip-arrow.gif','../../App_Themes/General/Images/rev-tooltip-arrow.gif','../../App_Themes/General/Images/down-tooltip-arrow.gif');\" onmouseout='hideddrivetip();' src='../../App_Themes/General/images/icon-tooltip.gif' />";
            return strHTML;
        }
        /** Methods to get set the tooltip **/
        function hidePopup() {
            parent.viewPopupWindow.hide();
        }
        function grdPageDetails_onIndexChange(sender, eventArgs) {
            var newPage = "" + eventArgs.get_index();
            var callbackArgs = new Array(newPage);
        }

        function grdPageDetails_onGroupChange(sender, eventArgs) {
        }
        function grdPageDetails_onExpandCollapse(sender, eventArgs) {
            grdPageDetails.render();
        }

    </script>

    <script type="text/javascript">
        function setGroupingColumn() {
            var arGroups = grdPageDetails.get_table().get_groups();
            document.getElementById("<%=hdnGridGrouping.ClientID%>").value = arGroups[0].get_column();
            GroupBy = arGroups[0].get_column();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="left-column">
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.PageTitle1 %></label>
            <div class="form-value text-value">
                <asp:Literal ID="litPageTitle" runat="server" />
            </div>
        </div>
                <div class="form-row">
            <label class="form-label"><%= GUIStrings.Site1 %></label>
            <div class="form-value text-value">
                <asp:Literal ID="litSiteTitle" runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.Author1 %></label>
            <div class="form-value text-value">
                <asp:Literal ID="litAuthor" runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.PublishedBy1 %></label>
            <div class="form-value text-value">
                <asp:Literal ID="litPublishedBy" runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.DatePublished %></label>
            <div class="form-value text-value">
                <asp:Literal ID="litDatePublished" runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.TimePublished %></label>
            <div class="form-value text-value">
                <asp:Literal ID="litTimePublished" runat="server" />
            </div>
        </div>
    </div>
    <div class="right-column">
        <div class="form-row export-section">
            <asp:LinkButton ID="lbtnEmail" runat="server" Text="<%$ Resources:GUIStrings, Email %>" runat="server" CssClass="email-icon" ToolTip="<%$ Resources:GUIStrings, Email %>"
                OnClientClick="openExportEmail(); return false;"></asp:LinkButton>
            <asp:LinkButton ID="lbtnExcel" runat="server" Text="<%$ Resources:GUIStrings, ExportToExcel %>" runat="server" CssClass="excel-icon"
                ToolTip="<%$ Resources:GUIStrings, ExportToExcel %>" OnClick="lbtnExcel_Click" OnClientClick="setGroupingColumn();"></asp:LinkButton>
            <asp:LinkButton ID="lbtnPdf" runat="server" Text="<%$ Resources:GUIStrings, ExporttoPDF %>" runat="server" CssClass="pdf-icon"
                ToolTip="<%$ Resources:GUIStrings, ExporttoPDF %>" OnClick="lbtnPdf_Click" OnClientClick="setGroupingColumn();"></asp:LinkButton>
        </div>
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.Thispageisfoundinthefollowingmenuitems %></label>
            <div class="form-value">
                <asp:ListBox ID="lstMenus" runat="server" Width="368px" />
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>
    <p><%= GUIStrings.Draganddropacolumnheadertogroupbythatcategory %></p>
    <div style="overflow: auto; width: 100%; height: 385px;">
    <ComponentArt:Grid SkinID="GroupedGrid" ID="grdPageDetails" runat="server" SliderPopupGroupedClientTemplateId="grdPageDetailsSlider"
        RunningMode="Client" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
        PagerInfoClientTemplateId="grdPageDetailsPagination" SliderPopupClientTemplateId="grdPageDetailsSlider"
        Width="800" GroupBy="SqlDateTimestamp ASC" GroupingPageSize="10" PageSize="10">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="SqlDateTimestamp" ShowTableHeading="false"
                ShowSelectorCells="false" RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn Width="135" runat="server" HeadingText="<%$ Resources:GUIStrings, Date %>" HeadingCellCssClass="FirstHeadingCell"
                        DataField="SqlDateTimestamp" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowGrouping="true"
                        FormatString="MM/dd/yy" />
                    <ComponentArt:GridColumn Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, TotalPageViews1 %>" DataField="Page_Visits"
                        IsSearchable="true" AllowGrouping="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="pageviewsHeaderTE" />
                    <ComponentArt:GridColumn Width="292" runat="server" HeadingText="<%$ Resources:GUIStrings, MenuItem %>" DataField="Menu" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="menuItemHeaderTE"
                        AllowGrouping="true" />
                    <ComponentArt:GridColumn DataField="Title" Visible="false" />
                    <ComponentArt:GridColumn DataField="Author" Visible="false" />
                    <ComponentArt:GridColumn DataField="Published_By" Visible="false" />
                    <ComponentArt:GridColumn DataField="Published_Date" Visible="false" />
                    <ComponentArt:GridColumn DataField="SqlDateTimestamp" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <PageIndexChange EventHandler="grdPageDetails_onIndexChange" />
            <GroupingChange EventHandler="grdPageDetails_onGroupChange" />
            <GroupExpand EventHandler="grdPageDetails_onExpandCollapse" />
            <GroupCollapse EventHandler="grdPageDetails_onExpandCollapse" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="pageviewsHeaderTE">
                <div class="gridHeader">
                    <span><asp:localize runat="server" text="<%$ Resources:GUIStrings, TotalPageViews1 %>"/></span>
                    <img src="../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip('<%= GUIStrings.ThetotalnumberofpagesviewedRepeatedviewsofasinglepagearecounted %>', 300, '', '../App_Themes/General/Images/tooltip-arrow.gif','../App_Themes/General/Images/rev-tooltip-arrow.gif','../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="menuItemHeaderTE">
                <div class="gridHeader">
                    <span><asp:localize runat="server" text="<%$ Resources:GUIStrings, MenuItem %>"/></span>
                    <img src="../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip('<%= GUIStrings.Thepageisavailableinthismenuitem %>', 300, '', '../App_Themes/General/Images/tooltip-arrow.gif','../App_Themes/General/Images/rev-tooltip-arrow.gif','../App_Themes/General/Images/down-tooltip-arrow.gif');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdPageDetailsSlider">
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdPageDetailsPagination">
                ## GetGridPaginationInfo(grdPageDetails) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" CssClass="button" 
        Text="<%$ Resources:GUIStrings, Close %>" ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="hidePopup()" />
    <asp:HiddenField ID="hdnGridGrouping" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSiteName" runat="server" />
</asp:Content>
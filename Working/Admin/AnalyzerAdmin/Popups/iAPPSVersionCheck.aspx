﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iAPPSVersionCheck.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.iAPPSVersionCheck" StylesheetTheme="General"
    MasterPageFile="~/Popups/iAPPSPopup.Master" Title="<%$ Resources:GUIStrings, iAPPSProductSuiteVersionCheck %>" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePopup() {
            parent.versionCheckWindow.hide();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:PlaceHolder ID="phNoUpdates" runat="server" Visible="false">
        <p>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YouareusingthelatestversionofiAPPSTherearenonewupdatesavailableatthemoment %>" />
        </p>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phUpdatesAvailable" runat="server">
        <p>
            <asp:Literal ID="ltCurrentVersion" runat="server" Visible="false"></asp:Literal>
            <asp:Literal ID="ltNewVersion" runat="server" Visible="false"></asp:Literal>
            <asp:Label runat="server"><%= string.Format(Resources.GUIStrings.ThereisanewversionofiAPPSavailable, ltNewVersion.Text, ltCurrentVersion.Text)%></asp:Label>
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, PleaseContactYourAccountManagerToFindOutHowYouCanUpdateToTheLatestVersion %>" />
        </p>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return ClosePopup();" />
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" StylesheetTheme="General" MasterPageFile="~/Popups/iAPPSPopup.Master"
    CodeBehind="PageViewDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.PageViewDetails" 
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerReportingAlertsPageviews %>" %>

<%@ Register TagName="Criteria" TagPrefix="UC" Src="~/UserControls/Navigation/NavigationCriteria.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true"
        ValidationGroup="valPageview" />
    <UC:Criteria ID="pageSelector" runat="server" />
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Pageviews1 %>" />
        </label>
        <div class="form-value text-value">
            <asp:TextBox ID="txtPageview" runat="server" Width="240" />
            <asp:RequiredFieldValidator ID="reqtxtPageview" ControlToValidate="txtPageview" ErrorMessage="<%$ Resources:JSMessages, Pleaseenterthenumberofpageviews %>"
                Display="None" runat="server" ValidationGroup="valPageview" />
            <asp:CompareValidator ID="cvtxtPageview" runat="server" ControlToValidate="txtPageview"
                Type="Integer" Operator="DataTypeCheck" ErrorMessage="<%$ Resources:JSMessages, Pleaseuseonlynumericcharactersinthepageviews %>"
                Display="None" SetFocusOnError="true" ValidationGroup="valPageview" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ResetEvery %>" />
        </label>
        <div class="form-value text-value">
            <asp:DropDownList ID="ddlReset" runat="server" Width="250" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Email1 %>" />
        </label>
        <div class="form-value text-value">
            <asp:TextBox ID="txtEmail" runat="server" Width="240" />
            <asp:RequiredFieldValidator ID="reqtxtEmail" ControlToValidate="txtEmail" ErrorMessage="<%$ Resources:JSMessages, EmailRequired %>"
                Display="None" runat="server" ValidationGroup="valPageview" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnClose" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
        class="button cancel-button" onclick="CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ValidationGroup="valPageview" ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
        CausesValidation="true" />
</asp:Content>

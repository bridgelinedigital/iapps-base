<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AnalystPermissions.aspx.cs"
    MasterPageFile="~/Popups/iAPPSPopup.Master" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Popups.AnalystPermissions"
    StylesheetTheme="General" Title="<%$ Resources:GUIStrings, AnalystPermissions %>" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function expandPageTree(objTag) {
            if (objTag.className == "expanded") {
                objTag.className = "collapsed";
                objTag.innerHTML = "<asp:localize runat='server' text='<%$ Resources:JSMessages, CollapseTree %>'/>";
                AnalystPermissionTree.expandAll();
            }
            else {
                objTag.className = "expanded";
                objTag.innerHTML = "<asp:localize runat='server' text='<%$ Resources:JSMessages, ExpandTree %>'/>";
                AnalystPermissionTree.collapseAll();
            }
            return false;
        }

        function hidePopup() {
            parent.analystPermissionPopup.hide();
        }

        function TreeSelect_Parent(node) {
            if (node != null) {
                node.set_checked(true);
                TreeSelect_Parent(node.GetParentNode());
            }
        }

        function TreeViewNode_NodeCheckChange(sender, eventArgs) {
            var currentNode = eventArgs.get_node();
            if (currentNode != null) {
                if (eventArgs.get_node().get_checked() == true) {
                    if (eventArgs.get_node().get_nodes().get_length() > 0) {
                        var result = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, Doyouwanttoselectallsubterms %>'/>")
                        if (result == true) eventArgs.get_node().checkAll();
                    }
                    var curChil = currentNode.Nodes();

                    if (curChil != null && curChil.length == 0) {
                        TreeSelect_Parent(currentNode.GetParentNode());
                    }

                }
                else if (eventArgs.get_node().get_showCheckBox() != null || eventArgs.get_node().get_showCheckBox() == 1) {
                    if (eventArgs.get_node().get_nodes().get_length() > 0) {
                        var result = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, Doyouwanttodeselectallsubterms %>'/>")
                        if (result == true) eventArgs.get_node().unCheckAll();
                    }
                }
                sender.render();
            }
        }
        function UnCheckParents(currentNode) {

            var node = currentNode.GetParentNode();
            if (node != null) {
                node.set_checked(false);
                UnCheckParents(node);
            }
            //************return 
            return;
        }

        //This is to populate the parent page's hidden field with the siteid and permission combination
        function Permission_OnClientClick(sender, e) {

            ValidChildSelection(window["AnalystPermissionTree"]);

            if (strErrMsg != "") {
                alert(strErrMsg);
                // clear the error messagae
                strErrMsg = "";
                return false;
            }

            if (ele = parent.document.getElementById(parent.isPermissionModifiedIdHolder)) {
                ele.value = 1;
            }
            BuildPermissionList();
            hidePopup();
        }

        function BuildPermissionList() {
            var testvar = window["AnalystPermissionTree"];

            var tx = GetCheckNode(testvar);
            var currentSiteID = querySt("id");
            if (currentSiteID != null) {
                UpdatePermissionArray(currentSiteID, tx);
            }
        }

        /*
        Function to update the permission Array present in the parent page. Inserts if it is new siteid else replaces the siteId value.
        */
        function UpdatePermissionArray(currentSiteID, permissionString) {
            var index = parent.permissionsArr.length;

            for (var k = 0; k < parent.permissionsArr.length; k++) {
                var permissions = parent.permissionsArr[k];
                var sitePermission = permissions.split(":");
                if (sitePermission[0] == currentSiteID) {
                    index = k;
                    break;
                }
            }
            parent.permissionsArr[index] = currentSiteID + ":" + permissionString;
        }

        /*
        Function to get the query string value for the given key.
        */
        function querySt(ji) {
            hu = window.location.search.substring(1);
            gy = hu.split("&");
            for (i = 0; i < gy.length; i++) {
                ft = gy[i].split("=");
                if (ft[0] == ji) {
                    return ft[1];
                }
            }
        }

        // return immedidate parent for leaf node
        function getImmediateParent(node) {
            idColle = "";
            ValidateID(node);

            gy = idColle.split(",");

            for (i = 0; i < gy.length - 1; i++) {
                if (strList.indexOf(gy[i]) == -1) {
                    strList += gy[i] + ",";
                }
            }
        }

        var idColle

        function ValidateID(node) {
            if (node != null) {
                idColle = idColle + node.Value + ",";
                ValidateID(node.GetParentNode());
            }
        }

        function ValidChildSelection(ParentNode) {
            var UnchkCount = 0;
            var ChildNode = ParentNode.Nodes();

            if (ChildNode.length > 0) {
                for (var i = 0; i < ChildNode.length; i++) {
                    if (!ChildNode[i].Checked) {
                        UnchkCount++;
                    }

                    if (strErrMsg == "")
                        ValidChildSelection(ChildNode[i]);
                }

                if (ParentNode.Checked && ChildNode.length == UnchkCount) {
                    strErrMsg = stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, Selectthechildnodefor0menu %>'/>", ParentNode.Text);
                }


            }
        }



        var strList = "";
        var strErrMsg = "";

        function GetChildNodeText(ChildNode) {
            if (ChildNode.length > 0) {
                for (var i = 0; i < ChildNode.length; i++) {
                    if (ChildNode[i].Checked) {


                        getImmediateParent(ChildNode[i]);
                        //  strList1 = strList1+"|"+ChildNode[i].Text; 

                        if (strList.indexOf(ChildNode[i].Value) == -1)
                            strList = strList + ChildNode[i].Value + ",";
                    }
                    //*****************find child Node until not ChildNode 
                    GetChildNodeText(ChildNode[i].Nodes());
                }
            }
            //************return 
            return strList;
        }
        function GetCheckNode(TreeView1) {
            var result = "";
            var childNodeArray = TreeView1.Nodes();

            result = GetChildNodeText(childNodeArray);
            return result;
        }

        function CheckStoredPermissions() {
            //Checks all the permissions that are saved in the permissionArr.
            for (var i = 0; i < parent.permissionsArr.length; i++) {
                var sitePermissions = parent.permissionsArr[i].split(':');
                var siteId = sitePermissions[0];
                //TODO: Check this
                var permissions = sitePermissions.splice(1, sitePermissions.length - 1);
            }
        }

        function TreeViewNode_OnLoad(sender, e) {
            for (var i = 0; i < parent.permissionsArr.length; i++) {
                //Get the site id
                var currentSiteID = querySt("id");
                var siteIdPermissions = parent.permissionsArr[i].split(":");

                if (siteIdPermissions != null) {
                    var siteId = siteIdPermissions[0];
                    var permissionsId = null;

                    permissionsId = siteIdPermissions[1];
                    //check whether it is the one which is being edited.
                    if (siteId == currentSiteID) {
                        //if it is then traverse the tree and update the treeview.
                        var permissions = permissionsId.split(',');

                        UpdateTreeView(permissions);
                        break;
                    }
                }
            }
        }

        function UpdateTreeView(permissions) {
            //if the permission node is checked and it is present in the array then no need to change
            //if the permission node is checked and it is not present in the array then uncheck the node in the treeview
            //if the permission node is not checked and it is present in the array then check the node in the treeview
            var treeView = window["AnalystPermissionTree"];

            GetCheckNodeWithId(treeView, permissions);

        }


        function GetChildNodeId(ChildNode, permissions) {
            if (ChildNode.length > 0) {
                for (var i = 0; i < ChildNode.length; i++) {
                    var isPresent = IsNodePresentInArray(ChildNode[i].Value, permissions);
                    if (ChildNode[i].Checked && !isPresent) {
                        window.status = ChildNode[i].Checked;
                        ChildNode[i].set_checked(false);
                        //strList = strList+"|"+ChildNode[i].Text; 
                        //strList = ChildNode[i].Value;
                    }
                    else if (ChildNode[i].Checked != true && isPresent) {
                        //window.status = ChildNode[i].Checked;
                        ChildNode[i].set_checked(true);
                    }
                    //*****************find child Node until not ChildNode 
                    GetChildNodeId(ChildNode[i].Nodes(), permissions);
                }
            }
            //************return 
            return;
        }

        function IsNodePresentInArray(value, permissions) {
            if (permissions) {
                for (var p = 0; p < permissions.length; p++) {
                    if (permissions[p] == value) {

                        return true;
                    }
                }
            }
            return false;
        }

        function GetCheckNodeWithId(TreeView1, permissions) {
            var result = "";
            var childNodeArray = TreeView1.Nodes();
            GetChildNodeId(childNodeArray, permissions);
            return;
        }
    
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row right-align">
        <a href="#" onclick="return expandPageTree(this);" class="expanded">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ExpandTree %>" /></a>
    </div>
    <ComponentArt:TreeView SkinID="Default" Height="320" Width="310" ID="AnalystPermissionTree"
        EnableViewState="true" runat="server">
        <CustomAttributeMappings>
            <ComponentArt:CustomAttributeMapping From="Id" To="Value" />
            <ComponentArt:CustomAttributeMapping From="Id" To="Id" />
            <ComponentArt:CustomAttributeMapping From="Title" To="Text" />
        </CustomAttributeMappings>
        <ClientEvents>
            <Load EventHandler="TreeViewNode_OnLoad" />
            <NodeCheckChange EventHandler="TreeViewNode_NodeCheckChange" />
        </ClientEvents>
    </ComponentArt:TreeView>
    <asp:XmlDataSource ID="xmlDataSource" runat="server"></asp:XmlDataSource>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnSavePermissions" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, SaveCustomPermissions %>"
        ToolTip="<%$ Resources:GUIStrings, SaveCustomPermissions %>" OnClientClick="javascript:return Permission_OnClientClick(this,event)" />
    <asp:Button ID="btnCancel" runat="server" CssClass="button buttonSpace" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="hidePopup()" />
</asp:Content>

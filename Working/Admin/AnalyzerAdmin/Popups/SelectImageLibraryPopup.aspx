﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Popups.SelectImageLibraryPopup" Theme="General"
    CodeBehind="SelectImageLibraryPopup.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" ValidateRequest="false" %>

<%@ Register TagPrefix="UC" TagName="ImageLibrary" Src="~/UserControls/Libraries/Data/ManageAssetImage.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            setTimeout(function () {
                $("#image_library").iAppsSplitter();
            }, 200);
        });
    </script>
    <link runat="server" rel="stylesheet" type="text/css" href="../css/LibraryPopup/LibraryPopupStyle.css" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:ImageLibrary ID="imageLibrary" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, SelectImage%>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SelectImage %>" OnClientClick="return SelectImageFromLibrary();" />
</asp:Content>

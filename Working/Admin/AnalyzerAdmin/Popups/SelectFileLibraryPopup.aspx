﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Popups.SelectFileLibraryPopup"
    StylesheetTheme="General" CodeBehind="SelectFileLibraryPopup.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="UC" TagName="FileLibrary" Src="~/UserControls/Libraries/Data/ManageAssetFile.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
<script type="text/javascript">
    $(function () {
        setTimeout(function () {
            $("#file_library").iAppsSplitter();
        }, 200);
    });
    </script>
    <link runat="server" rel="stylesheet" type="text/css" href="../css/LibraryPopup/LibraryPopupStyle.css" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:FileLibrary ID="fileLibrary" runat="server" AllowReorder="false" AllowMove="false" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, SelectFile%>" OnClientClick="return SelectFileFromLibrary();"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SelectFile %>"  />
</asp:Content>

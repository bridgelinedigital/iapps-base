<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AbandonmentPath.aspx.cs" Title="<%$ Resources:GUIStrings, AbandonmentPath %>"
    Inherits="AbandonmentPath" StylesheetTheme="General" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var item = "";
        function grdAbandonmentPath_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            grdAbandonmentPath.select(item);

            var evt = eventArgs.get_event();
            cmAbandonmentPath.showContextMenuAtEvent(evt, eventArgs.get_item());
        }
        function hidePopup() {
            parent.viewPopupWindow.hide();
        }

        function cmAbandonmentPath_ItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            var selectedItems = grdAbandonmentPath.getSelectedItems();

            switch (selectedMenu) {
                case "ViewSiteEditor":
                    var currentUrl = document.URL;
                    if (currentUrl == null || currentUrl == '') {
                        currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
                    }
                    if (currentUrl.indexOf('?') > 0) {
                        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
                    }
                    currentUrl = '?LastVisitedAnalyticsAdminPageUrl=' + currentUrl;
                    var pageId = item.GetMember('OriginalId').Text;
                    window.location.href = analyticsPublicSiteUrl + "/" + GetTokenWithReferrerURL(cmsProductId, siteId, siteName, pageId, currentUrl);
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:Grid SkinID="Default" ID="grdAbandonmentPath" runat="server" RunningMode="Client"
        ShowFooter="false" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
        Width="280" Height="105" PageSize="10" ShowHeader="false">
        <Levels>
            <ComponentArt:GridLevel AlternatingRowCssClass="sAlternateDataRow" AllowSorting="true"
                DataKeyField="OriginalId" ShowTableHeading="false" ShowSelectorCells="false"
                SelectorCellCssClass="sSelectorCell" SelectorCellWidth="18" SelectorImageWidth="17"
                SelectorImageHeight="15" HeadingSelectorCellCssClass="SelectorCell" DataCellCssClass="sDataCell"
                RowCssClass="sRow" SelectedRowCssClass="sSelectedRow" SortAscendingImageUrl="asc.gif"
                SortDescendingImageUrl="desc.gif" ShowHeadingCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="250" DataField="Title" SortedDataCellCssClass="SortedDataCell"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" DataCellCssClass="FirstDataCell" />
                    <ComponentArt:GridColumn Width="70" DataField="Click" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" />
                    <ComponentArt:GridColumn DataField="OriginalId" IsSearchable="false" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdAbandonmentPath_onContextMenu" />
        </ClientEvents>
    </ComponentArt:Grid>
    <ComponentArt:Menu SkinID="ContextMenu" ID="cmAbandonmentPath" runat="server">
        <Items>
            <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, ViewPageinSiteEditor %>" ID="ViewSiteEditor" />
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="cmAbandonmentPath_ItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>  
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="hidePopup()" />
</asp:Content>
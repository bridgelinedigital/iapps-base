﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectProductType.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master"
    StylesheetTheme="General" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Popups.SelectProductType" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        //<![CDATA[
        function trvProductTypes_onNodeSelect(sender, eventArgs) {
            var text = eventArgs.get_node().get_text();
            var id = eventArgs.get_node().get_id();
            window.parent.selectedProductType(id, text);
            CloseDialog();
        }

        function CloseDialog() {
            parent.viewPopupWindow.hide();

        }
        //]]>
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:TreeView ID="trvProductTypes" ExpandSinglePath="true" SkinID="Default"
        AutoPostBackOnSelect="true" Height="293" Width="367" CausesValidation="false"
        runat="server">
        <ClientEvents>
            <NodeSelect EventHandler="trvProductTypes_onNodeSelect" />
        </ClientEvents>
    </ComponentArt:TreeView>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Close %>" class="button" onclick="CloseDialog()" />
</asp:Content>
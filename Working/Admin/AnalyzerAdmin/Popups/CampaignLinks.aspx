﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.Master" StylesheetTheme="General" 
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.CampaignLinks"CodeBehind="CampaignLinks.aspx.cs"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerCampaignLinks %>" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
   
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Source1%></label>
        <div class="form-value text-value">
            <asp:Literal ID="litSource" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Medium1%></label>
        <div class="form-value text-value">
            <asp:Literal ID="litMedium" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Medium1%></label>
        <div class="form-value text-value">
            <asp:Literal ID="ltLinkUrl" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnClose" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
        class="button cancel-button"  />
</asp:Content>
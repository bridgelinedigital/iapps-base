<%@ Page Language="C#" Theme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, UntitledPage %>" />
    </title>
    <script type="text/javascript">
        function openWin1() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', 'popups/AnalystPermissions.aspx', '',
                                            'width=263px,height=460px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function () {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function openWin2() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', 'popups/CodetoEmbed.aspx', '',
                                            'width=275px,height=220px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function () {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function openWin3() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', 'popups/ImportSelectedUser.aspx', '',
                                            'width=590px,height=302px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function () {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function openWin4() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', 'popups/PageDetailStatistics.aspx', '',
                                            'width=750px,height=595px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function () {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function openWin5() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', 'popups/SelectAssettoWatch.aspx', '',
                                            'width=855px,height=545px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function () {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function openWin6() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', 'popups/SelectDirectory.aspx', '',
                                            'width=855px,height=545px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function () {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function openWin7() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', 'popups/TopViewedItems.aspx', '',
                                            'width=275px,height=220px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function () {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function openWin8() {
            viewPopupWindow = dhtmlmodal.open('Content', 'iframe', 'popups/ExportAsEmail.aspx', '', 'width=275px,height=220px,center=1,resize=0,scrolling=1');
            viewPopupWindow.onclose = function () {
                var a = document.getElementById('Content');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input type="button" value="<%= GUIStrings.AnalystPermissions %>" onclick="openWin1(); return false" />
    <input type="button" value="<%= GUIStrings.CodetoEmbed %>" onclick="openWin2(); return false" />
    <input type="button" value="<%= GUIStrings.ImportSelectedUser %>" onclick="openWin3(); return false" />
    <input type="button" value="<%= GUIStrings.PagedetailStatistics2 %>" onclick="openWin4(); return false" />
    <input type="button" value="<%= GUIStrings.SelectAssettoWatch %>" onclick="openWin5(); return false" />
    <input type="button" value="<%= GUIStrings.SelectDirectory %>" onclick="openWin6(); return false" />
    <input type="button" value="<%= GUIStrings.TopViewedItems %>" onclick="openWin7(); return false" />
    <input type="button" value="<%= GUIStrings.ExportAsEmail %>" onclick="openWin8(); return false" />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:iAppsWarehouseConnectionString %>"
        SelectCommand="GetTopTenInboundPages" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="DestPageId" Type="Object" />
            <asp:Parameter Name="SiteId" Type="Object" />
            <asp:Parameter Name="StartDate" Type="DateTime" />
            <asp:Parameter Name="EndDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="GoalListing.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.GoalListing"
    StylesheetTheme="General" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerGoalsViewallGoals %>" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams

        var itemGoals;
        var emptyGuid = '00000000-0000-0000-0000-000000000000';
        function grdGoals_onContextMenu(sender, eventArgs) {
            var evt = eventArgs.get_event();
            var menuItems = cmGoals.get_items();
            itemGoals = eventArgs.get_item();
            id = itemGoals.getMember('Id').get_text();
            for (i = 0; i < menuItems.get_length(); i++) {
                if (menuItems.getItem(i).get_id() != 'cmAdd') {
                    if (id == '' || id == undefined)
                        menuItems.getItem(i).set_visible(false);
                }
                if (menuItems.getItem(i).get_id() == 'cmInactive') {
                    if (itemGoals.getMember('Status').get_text() == '1')
                        menuItems.getItem(i).set_text('<%= JSMessages.MakeInactive %>');
                    else {

                        menuItems.getItem(i).set_text('<%= JSMessages.MakeActive %>');
                    }
                }
            }
            grdGoals.select(itemGoals);
            cmGoals.showContextMenuAtEvent(evt);
        }
        function cmGoals_onItemSelect(sender, eventArgs) {
            var itemSelected = eventArgs.get_item().get_id();
            var id = itemGoals.getMember('Id').get_text();
            switch (itemSelected) {
                case 'cmAdd':
                    window.location = jAnalyticAdminSiteUrl + "/Goals/DefineGoals.aspx";
                    break;
                case 'cmEdit':
                    var url = jAnalyticAdminSiteUrl + "/Goals/DefineGoals.aspx";
                    if (id != "") {
                        url += "?GoalId=" + id;
                    }
                    window.location = url;
                    break;
                case 'cmEditAlert':
                    OpenGoalAlertsPopup(id);
                    break;
                case 'cmInactive':
                    if (itemGoals.getMember('Status').get_text() == '1')
                        MakeGoalInactive(id);
                    else
                        MakeGoalActive(id);
                    grdGoals.callback();
                    break;
                case 'cmViewStats':
                    window.location = jAnalyticAdminSiteUrl + "/Goals/GoalStatistics.aspx?GoalId=" + id;
                    break;
                case 'cmDeleteGoal':
                    var nCount = CheckBeforeDeleteGoal(id);
                    if (nCount == 0) {
                        var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, AreYouSureYouWantToDelete %>'/>");
                        if (agree) {
                            DeleteGoal(id);
                            grdGoals.callback();
                        }
                    }
                    else if (window.confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ThisManyCampaignExistInThisGoal %>' />")) {
                        DeleteGoal(id);
                        grdGoals.callback();
                    }

                    break;
            }
        }
        function OpenGoalPopup(GoalId) {
            var url = jAnalyticAdminSiteUrl + "/Popups/Goals/AddEditGoal.aspx";
            if (GoalId != "") {
                url += "?Id=" + GoalId;
            }
            AddEditGoalWindow = dhtmlmodal.open('AddEditGoal', 'iframe', url, '<asp:localize runat="server" text="<%$ Resources:JSMessages, AddEditGoal %>"/>', 'width=415px,height=350px,center=1,resize=0,scrolling=1');
            AddEditGoalWindow.onclose = function () {
                var a = document.getElementById('AddEditGoal');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("/blank.html");
                return true;
            }
        }
        function OpenGoalAlertsPopup(GoalId) {
            var url = jAnalyticAdminSiteUrl + "/Popups/Goals/GoalAlerts.aspx";
            if (GoalId != "") {
                url += "?Id=" + GoalId;
            }
            GoalAlertsWindow = dhtmlmodal.open('GoalAlerts', 'iframe', url, '<asp:localize runat="server" text="<%$ Resources:JSMessages, GoalAlerts %>"/>', 'width=415px,height=350px,center=1,resize=0,scrolling=1');
            GoalAlertsWindow.onclose = function () {
                var a = document.getElementById('GoalAlerts');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("/blank.html");
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="mainPlaceHolder" runat="server">
<div class="goals">
        <div class="page-header clear-fix">
            <h1><%= GUIStrings.ViewAllGoals %></h1>
        </div>
    <ComponentArt:Menu ID="cmGoals" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem ID="cmAdd" runat="server" Text="<%$ Resources:GUIStrings, AddGoal %>"
                Look-LeftIconUrl="cm-icon-add.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmEdit" runat="server" Text="<%$ Resources:GUIStrings, EditGoals %>"
                Look-LeftIconUrl="cm-icon-edit.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmViewStats" runat="server" Text="<%$ Resources:GUIStrings, ViewGoalStats %>">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmEditAlert" runat="server" Text="<%$ Resources:GUIStrings, EditAlerts %>">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmInactive" Text="<%$ Resources:GUIStrings, MakeInactive %>">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmDeleteGoal" Text="<%$ Resources:GUIStrings, DeleteGoal %>"
                Look-LeftIconUrl="cm-icon-delete.png">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="cmGoals_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Grid ID="grdGoals" runat="server" SkinID="Default" RunningMode="Callback"
        Height="290" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false"
        PageSize="10" Width="100%" EditOnClickSelectedItem="false" PagerInfoClientTemplateId="CustomPageTemplate">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                SortedHeadingCellCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText"
                SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn DataField="Name" Width="407" runat="server" HeadingText="<%$ Resources:GUIStrings, GoalName %>"
                        FixedWidth="true" DefaultSortDirection="Ascending" AllowReordering="False" DataCellClientTemplateId="GoalNameCT"
                        Align="Left" DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell" />
                    <ComponentArt:GridColumn DataField="GoalType" Width="407" runat="server" HeadingText="<%$ Resources:GUIStrings, GroupName %>"
                        FixedWidth="true" AllowReordering="False" DataCellClientTemplateId="GroupNameCT" />
                    <ComponentArt:GridColumn DataField="MinValue" Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, Threshold %>"
                        FixedWidth="true" AllowReordering="False" DataCellClientTemplateId="ThresholdCT"
                        Align="Center" />
                    <ComponentArt:GridColumn DataField="NumberFormat" Width="140" runat="server" HeadingText="<%$ Resources:GUIStrings, NumberFormat %>"
                        FixedWidth="true" AllowReordering="False" Align="Center" />
                    <ComponentArt:GridColumn DataField="Status" Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>"
                        FixedWidth="true" AllowReordering="False" DataCellClientTemplateId="StatusCT"
                        Align="Center" />
                    <ComponentArt:GridColumn DataField="" Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                        FixedWidth="true" AllowReordering="False" DataCellClientTemplateId="ActionsCT"
                        Visible="false" />
                    <ComponentArt:GridColumn DataField="MaxValue" Visible="false" />
                    <ComponentArt:GridColumn DataField="ThresholdOpertor" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdGoals_onContextMenu" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="GoalNameCT">
                <span title="## DataItem.getMember('Name').get_text() ##">## DataItem.getMember('Name').get_text()
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="GroupNameCT">
                <span title="## DataItem.getMember('GoalType').get_text() ##">## DataItem.getMember('GoalType').get_text()
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ActionsCT">
                <a href="DefineGoals.aspx?GoalId=## DataItem.getMember('Id').get_text() ##">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Edit1 %>" /></a>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ThresholdCT">
                <span title="## DataItem.getMember('MinValue')!=null
                    ? DataItem.getMember('ThresholdOpertor').get_text()==2 ? ' > ' + DataItem.getMember('MinValue').get_text()
                    + ' & < ' + DataItem.getMember('MaxValue').get_text() : DataItem.getMember('ThresholdOpertor').get_text()==1
                    ?' > ' + DataItem.getMember('MinValue').get_text() :' < ' + DataItem.getMember('MaxValue').get_text()
                    : '' ##">
                    ## DataItem.getMember('MinValue')!=null
                    ? DataItem.getMember('ThresholdOpertor').get_text()==2 ? ' > ' + DataItem.getMember('MinValue').get_text()
                    + ' & < ' + DataItem.getMember('MaxValue').get_text() : DataItem.getMember('ThresholdOpertor').get_text()==1
                    ?' > ' + DataItem.getMember('MinValue').get_text() :' < ' + DataItem.getMember('MaxValue').get_text()
                    : '' ##
                </span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusCT">
                <span title="## DataItem.getMember('Status')!=null
                    ? DataItem.getMember('Status').get_text() ==1 ? 'Active' : DataItem.getMember('Status').get_text()
                    ==3 ? 'Deleted' : DataItem.getMember('Status').get_text() ==2 ? 'Inactive' :'' :
                    '' ##">
                    ## DataItem.getMember('Status')!=null
                    ? DataItem.getMember('Status').get_text() ==1 ? 'Active' : DataItem.getMember('Status').get_text()
                    ==3 ? 'Deleted' : DataItem.getMember('Status').get_text() ==2 ? 'Inactive' :'' :
                    '' ##
                </span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomPageTemplate">
                ## GetGridPaginationInfo(grdGoals) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</div>
</asp:Content>

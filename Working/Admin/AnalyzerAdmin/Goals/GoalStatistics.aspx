﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GoalStatistics.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.Goals.GoalStatistics" MasterPageFile="~/MainMaster.master"
    StylesheetTheme="General" Title="<%$ Resources:GUIStrings, GoalStatisticsPageTitle %>" %>
<%@ Register Src="~/UserControls/General/LandingPagePerformance.ascx" TagName="LandingPagePerformance" TagPrefix="lpp" %>
<%@ Register TagPrefix="rr" TagName="ReportingRange" Src="~/UserControls/ReportingRange.ascx" %>
<%@ Register TagPrefix="to" TagName="TopOpportunities" Src="~/UserControls/General/TopOpportunities.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            //hide the all of the element with class msg_body
            //$(".collapsed").hide();
            ////toggle the componenet with class msg_body
            //$(".snap-toggle").click(function () {
            //    $(this).parent().next(".snap-content").slideToggle('normal', function () {
            //        if ($(this).is(':hidden')) {
            //            $(this).prev().children('.snap-toggle').attr('src', '../App_Themes/General/images/snap-expand.png');
            //        }
            //        else {
            //            $(this).prev().children('.snap-toggle').attr('src', '../App_Themes/General/images/snap-collapse.png');
            //        }
            //    });
            //});
        });

        function GetCurrentValue(dataItem) {
            var value = parseFloat(dataItem.getMember('Goal').get_text()).toFixed(2);
            if (dataItem.getMember('NumberFormat').get_text() == '$') {
                return '$' + value;
            }
            if (dataItem.getMember('NumberFormat').get_text() == '%') {
                return value + '%';
            }
            return dataItem.getMember('Goal').get_text();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="mainPlaceHolder" runat="server">
<div class="goal-statistics">
    <div class="page-header clear-fix">
        <h1>
            <asp:Localize ID="lcGoalNameLabel" runat="server" Text="<%$ Resources:GUIStrings, GoalLabel %>" />
            <asp:Literal ID="ltGoalName" runat="server"></asp:Literal>
        </h1>
        <rr:ReportingRange ID="ctlReportingRange" runat="server" />
    </div>
    <div id="goalStatistics" class="clear-fix">
        <!-- Top Opportunities STARTS -->
        <div class="snap-header clear-fix">
            <img id="Img3" src="~/App_Themes/General/images/snap-collapse.png" runat="server" alt="<%$ Resources:GUIStrings, MinimizeSnap %>"
                class="snap-toggle" />
            <h2><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, TopOpportunities %>" /></h2>
        </div>
        <div class="snap-content">
            <to:TopOpportunities ID="ctlTopOpportunities" runat="server" />
        </div>
        <!-- Top Opportunities ENDS -->
        <!-- Supplemental Data Metrics STARTS -->
        <div class="snap-header no-top-border clear-fix">
            <img id="Img1" src="~/App_Themes/General/images/snap-expand.png" runat="server" alt="<%$ Resources:GUIStrings, MinimizeSnap %>"
                class="snap-toggle" />
            <h2><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SupplementalDataMetrics %>" /></h2>
        </div>
        <div class="snap-content collapsed">
            <ComponentArt:Grid ID="grdCampaigns" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
                SkinID="SliderWithHierarchy" RunningMode="Client" runat="server"  DataAreaCssClass="DataAreaTable" 
                AllowEditing="true" AutoCallBackOnInsert="false" Width="100%"
                AutoCallBackOnUpdate="true" AutoCallBackOnDelete="false" EditOnClickSelectedItem="false">
                <Levels>
                    <ComponentArt:GridLevel HeadingCellCssClass="HeadingCell" HeadingRowCssClass="HeadingRow"
                        HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell" RowCssClass="Row"
                        SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif" ShowTableHeading="false"
                        HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
                        SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                        SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                        AlternatingRowCssClass="AlternateDataRow" ShowSelectorCells="false" DataKeyField="Id">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Name" HeadingText=" " DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell"
                                Width="105" AllowReordering="false" DataCellClientTemplateId="GroupNameCT" TextWrap="true" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="CampaignName" runat="server" DataCellClientTemplateId="CampaignNameCT"
                                HeadingText="<%$ Resources:GUIStrings, Campaigns1 %>" Width="275" AllowReordering="false" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Source" runat="server" HeadingText="<%$ Resources:GUIStrings, Source %>"
                                Width="275" AllowReordering="false" DataCellClientTemplateId="SourceCT" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Medium" runat="server" HeadingText="<%$ Resources:GUIStrings, Medium %>"
                                Width="105" AllowReordering="false" DataCellClientTemplateId="MediumCT" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Visits" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                                Width="60" AllowReordering="false" Align="Right" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Goal" runat="server" HeadingText="<%$ Resources:GUIStrings, Goal %>"
                                Width="65" AllowReordering="false" Align="Right"  DataCellClientTemplateId="CurrentGoalCT" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="ConversionPer" runat="server"
                                HeadingText="<%$ Resources:GUIStrings, Conversion %>" Width="100" AllowReordering="false"
                                Align="Right" DataCellServerTemplateId="svtConversion" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="BounceRate" runat="server"
                                HeadingText="<%$ Resources:GUIStrings, BounceRate %>" Width="125" AllowReordering="false"
                                Align="Right" DataCellServerTemplateId="svtBounceRate" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Sends" runat="server" HeadingText="<%$ Resources:GUIStrings, Sends %>"
                                Width="46" AllowReordering="false" Align="Right" Visible="false" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Deliveries" runat="server" Visible="false"
                                HeadingText="<%$ Resources:GUIStrings, Deliveries %>" Width="70" AllowReordering="false"
                                Align="Right" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Clicks" runat="server" HeadingText="<%$ Resources:GUIStrings, Clicks %>"
                                Width="46" AllowReordering="false" Align="Right" Visible="false" />
                            <ComponentArt:GridColumn DataField="NumberFormat" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                    <ComponentArt:GridLevel DataMember="Distribution" HeadingCellCssClass="HeadingCell"
                        HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" DataCellCssClass="DataCell"
                        RowCssClass="Row" SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif"
                        ShowTableHeading="false" HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
                        SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                        SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                        AlternatingRowCssClass="AlternateDataRow" ShowSelectorCells="false" ShowHeadingCells="false" DataKeyField="Id">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn FixedWidth="true" HeadingText=" " Width="77" AllowReordering="False" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="CampaignName" HeadingText="<%$ Resources:GUIStrings, Campaign %>"
                                Width="275" AllowReordering="false" DataCellClientTemplateId="CampaignName2CT" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Source" runat="server" HeadingText="<%$ Resources:GUIStrings, Source %>"
                                Width="275" AllowReordering="false" DataCellClientTemplateId="Source2CT" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Medium" runat="server" HeadingText="<%$ Resources:GUIStrings, Medium %>"
                                Width="105" AllowReordering="false" DataCellClientTemplateId="Medium2CT" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Visits" runat="server" HeadingText="<%$ Resources:GUIStrings, Visits %>"
                                Width="60" AllowReordering="false" Align="Right" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Goal" runat="server" HeadingText="<%$ Resources:GUIStrings, Goal %>"
                                Width="65" AllowReordering="false" Align="Right"  DataCellClientTemplateId="CurrentGoalCT" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="ConversionPer" runat="server"
                                HeadingText="<%$ Resources:GUIStrings, Conversion %>" Width="100" AllowReordering="false"
                                Align="Right" DataCellServerTemplateId="svtConversion" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="BounceRate" runat="server"
                                HeadingText="<%$ Resources:GUIStrings, BounceRate %>" Width="125" AllowReordering="false"
                                Align="Right"  DataCellServerTemplateId="svtBounceRate" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Sends" runat="server" HeadingText="<%$ Resources:GUIStrings, Sends %>"
                                Width="46" AllowReordering="false" Align="Right" Visible="false" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Deliveries" runat="server" Visible="false"
                                HeadingText="<%$ Resources:GUIStrings, Deliveries %>" Width="70" AllowReordering="false"
                                Align="Right" />
                            <ComponentArt:GridColumn FixedWidth="true" DataField="Clicks" runat="server" HeadingText="<%$ Resources:GUIStrings, Clicks %>"
                                Width="46" AllowReordering="false" Align="Right" Visible="false" />
                            <ComponentArt:GridColumn DataField="NumberFormat" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="CurrentGoalCT">
                            <span title="## DataItem.getMember('Goal').get_text() ##">
                            ## GetCurrentValue(DataItem) ##</span>
                        </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="CampaignName2CT">
                        <span title="## DataItem.getMember('CampaignName').get_text() ##">## DataItem.getMember('CampaignName').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="Source2CT">
                        <span title="## DataItem.getMember('Source').get_text() ##">## DataItem.getMember('Source').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="Medium2CT">
                        <span title="## DataItem.getMember('Medium').get_text() ##">## DataItem.getMember('Medium').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="GroupNameCT">
                        <span title="## DataItem.getMember('Name').get_text() ##">## DataItem.getMember('Name').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="CampaignNameCT">
                        <span title="## DataItem.getMember('CampaignName').get_text() ##">## DataItem.getMember('CampaignName').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="SourceCT">
                        <span title="## DataItem.getMember('Source').get_text() ##">## DataItem.getMember('Source').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="MediumCT">
                        <span title="## DataItem.getMember('Medium').get_text() ##">## DataItem.getMember('Medium').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="svtConversion">
                        <Template>
                            <span title="<%# (Convert.ToDecimal(Container.DataItem["Conversionper"]) * 100).ToString("0.##")  %>">
                                <%# (Convert.ToDecimal(Container.DataItem["ConversionPer"]) * 100).ToString("0.##")  %> 
                            </span>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                    <ComponentArt:GridServerTemplate ID="svtBounceRate">
                        <Template>
                            <span title="<%# (Convert.ToDecimal(Container.DataItem["BounceRate"]) * 100).ToString("0.##")  %>">
                                <%# (Convert.ToDecimal(Container.DataItem["BounceRate"]) * 100).ToString("0.##") %> 
                            </span>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
            </ComponentArt:Grid>
        </div>
        <!-- Supplemental Data Metrics ENDS -->
        <!-- Landing Page Performance STARTS -->
        <div class="snap-header no-top-border clear-fix">
            <img id="Img5" src="~/App_Themes/General/images/snap-expand.png" runat="server" alt="<%$ Resources:GUIStrings, MinimizeSnap %>"
                class="snap-toggle" />
            <h2><asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, LandingPagePerformance %>" /></h2>
        </div>
        <div class="snap-content collapsed">
            <lpp:LandingPagePerformance ID="LandingPagePerformance1" runat="server" />
        </div>
        <!-- Landing Page Performance STARTS -->
    </div>
</div>
</asp:Content>

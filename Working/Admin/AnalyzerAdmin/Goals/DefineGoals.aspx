﻿<%@ Page Title="iAPPS Analyzer: Goals: Define a Goal" Language="C#" MasterPageFile="~/MainMaster.master"
    AutoEventWireup="true" CodeBehind="DefineGoals.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.DefineGoals"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="gs" TagName="GoalStep1" Src="~/UserControls/Goals/GoalsStep1.ascx" %>
<%@ Register TagPrefix="gs" TagName="GoalStep2" Src="~/UserControls/Goals/GoalsStep2.ascx" %>
<%@ Register TagPrefix="gs" TagName="GoalStep3" Src="~/UserControls/Goals/GoalsStep3.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <div class="goal-details">
        <div class="page-header clear-fix">
            <h1>
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DefineaGoal %>" /></h1>
        </div>
        <asp:Wizard ID="goalWizard" runat="server" EnableViewState="true" ActiveStepIndex="0" DisplaySideBar="false"
            Width="100%">
            <LayoutTemplate>
                <div class="wizard-header clear-fix">
                    <div class="header-container">
                        <asp:PlaceHolder ID="headerPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                    <div class="navigation-container">
                        <asp:PlaceHolder ID="navigationPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
                <div class="step-container">
                    <asp:PlaceHolder ID="wizardStepPlaceHolder" runat="server"></asp:PlaceHolder>
                </div>
            </LayoutTemplate>
            <HeaderTemplate>
                <div class="import-steps">
                    <span class="step1<%=goalWizard.ActiveStepIndex%>">
                        <asp:LinkButton ID="lbtnThresholds" runat="server" Text="<%$ Resources:GUIStrings, GoalDetails %>"
                            OnClick="lbtnThresholds_Click" />
                    </span>
                    <span class="step2<%=goalWizard.ActiveStepIndex%>">
                        <asp:LinkButton ID="lbtnBenchmark" runat="server" Text="<%$ Resources:GUIStrings, SetBenchmarkParameters %>"
                            OnClick="lbtnBenchmark_Click" />
                    </span>
                    <span class="step3<%=goalWizard.ActiveStepIndex%>">
                        <asp:LinkButton ID="lbtnCampaigns" runat="server" Text="<%$ Resources:GUIStrings, DefineCampaigns %>"
                            OnClick="lbtnCampaigns_Click" />
                    </span>
                </div>
            </HeaderTemplate>
            <WizardSteps>
                <asp:WizardStep ID="WizardStep1" runat="server" Title="<%$ Resources:GUIStrings, GoalDetails %>">
                    <gs:GoalStep1 ID="ctlGoalStep1" runat="server" />
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep2" runat="server" Title="<%$ Resources:GUIStrings, SetBenchmarkParameters %>">
                    <gs:GoalStep2 ID="ctlGoalStep2" runat="server" />
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep3" runat="server" Title="<%$ Resources:GUIStrings, DefineCampaigns %>">
                    <gs:GoalStep3 ID="ctlGoalStep3" runat="server" />
                </asp:WizardStep>
            </WizardSteps>
            <StartNavigationTemplate>
                <div class="button-row">
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClick="btnCancel_Click" CausesValidation="false" />
                    <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:GUIStrings, Continue %>"
                        ToolTip="<%$ Resources:GUIStrings, Continue %>" CssClass="primarybutton" OnClick="btnContinue_Click" />
                </div>
            </StartNavigationTemplate>
            <StepNavigationTemplate>
                <div class="button-row">
                    <asp:Button ID="btnStepback" runat="server" Text="<%$ Resources:GUIStrings, Back %>"
                        ToolTip="<%$ Resources:GUIStrings, Back %>" CssClass="button" OnClick="btnStepback_Click" />
                    <asp:Button ID="btnStepCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClick="btnStepCancel_Click" />
                    <asp:Button ID="btnStepContinue" runat="server" Text="<%$ Resources:GUIStrings, Continue %>"
                        ToolTip="<%$ Resources:GUIStrings, Continue %>" CssClass="primarybutton" OnClick="btnStepContinue_Click" />
                </div>
            </StepNavigationTemplate>
            <FinishNavigationTemplate>
                <div class="button-row">
                    <asp:Button ID="btnFinishback" runat="server" Text="<%$ Resources:GUIStrings, Back %>"
                        ToolTip="<%$ Resources:GUIStrings, Back %>" CssClass="button" OnClick="btnFinishback_Click" />
                    <asp:Button ID="btnFinishCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClick="btnFinishCancel_Click" />
                    <asp:Button ID="BtnSaveCreateCampaign" runat="server" Text="<%$ Resources:GUIStrings, SaveCreateCampaign %>"
                        ToolTip="<%$ Resources:GUIStrings, SaveCreateCampaign %>" CssClass="primarybutton"
                        OnClick="BtnSaveCreateCampaign_Click" />
                    <asp:Button ID="btnFinishSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClick="btnFinishSave_Click" />
                </div>
            </FinishNavigationTemplate>
        </asp:Wizard>
    </div>
</asp:Content>

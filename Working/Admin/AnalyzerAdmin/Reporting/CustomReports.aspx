<%@ Page Language="C#" MasterPageFile="~/Reporting/ReportingMaster.master" AutoEventWireup="true"
    Theme="General" Inherits="CustomReports" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerReportingCustomReports %>"
    CodeBehind="CustomReports.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var item;
        function ContextReportMenu_onItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            alert(selectedMenu);
        }
        function grdReport_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            grdReport.select(item);
            var evt = eventArgs.get_event();
            ContextReportMenu.showContextMenuAtEvent(evt, eventArgs.get_item());
        }
    </script>
    <style type="text/css">
        .right-column table td {
            vertical-align: top !important;
        }
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="reportingContentHolder" runat="Server">
<div class="custom-reports clear-fix">
    <p>Right click the grid for more options. Use the buttons below the grid to create a new custom report or refresh the list of available custom reports.</p>
    <div class="left-column clear-fix">
        <ComponentArt:Menu ID="ContextReportMenu" runat="server" AutoPostBackOnSelect="true"
            OnItemSelected="ContextReportMenu_ItemSelected" SkinID="ContextMenu">
            <Items>
                <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, RunReport %>" ID="MnuRun">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem runat="server" Text="<%$ Resources:GUIStrings, DeleteReport %>" ID="MnuDelete">
                </ComponentArt:MenuItem>
            </Items>
        </ComponentArt:Menu>
        <ComponentArt:Grid ID="grdReport" EditOnClickSelectedItem="false" ScrollPopupClientTemplateId="grdAudienceSegmentsScrollTemplate"
            SkinID="ScrollingGrid" ShowHeader="False" PageSize="13" ShowFooter="false" ImagesBaseUrl="images/"
            ShowSearchBox="false" runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>"
            Height="230" Width="100%" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="true"
            AllowMultipleSelect="false">
            <Levels>
                <ComponentArt:GridLevel AlternatingRowCssClass="AlternateDataRow" AllowSorting="true"
                    DataKeyField="Sno" ShowTableHeading="false" ShowSelectorCells="false" SelectorCellCssClass="SelectorCell"
                    SelectorCellWidth="18" SelectorImageWidth="17" SelectorImageHeight="15" HeadingSelectorCellCssClass="SelectorCell"
                    DataCellCssClass="DataCell" RowCssClass="Row" SelectedRowCssClass="SelectedRow"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" ShowHeadingCells="false">
                    <Columns>
                        <ComponentArt:GridColumn DataField="ReportName" IsSearchable="true" AllowEditing="false"
                            Align="left" Visible="true" TextWrap="true" />
                        <ComponentArt:GridColumn DataField="Sno" IsSearchable="false" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <ContextMenu EventHandler="grdReport_onContextMenu" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdAudienceSegmentsScrollTemplate">
                    <div class="sScrollPopup smallScrollPopup">
                        <h5>
                            ## DataItem.GetMember("0").Value ##</h5>
                    </div>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
        <div class="button-row">
            <asp:Button ID="btnNew" runat="server" Text="<%$ Resources:GUIStrings, CreateNew %>" CssClass="button" ToolTip="<%$ Resources:GUIStrings, CreateNew %>" />
            <asp:Button ID="btnRefresh" runat="server" Text="<%$ Resources:GUIStrings, RefreshList %>" CssClass="button" ToolTip="<%$ Resources:GUIStrings, RefreshList %>" />
        </div>
    </div>
    <div class="right-column clear-fix">
        <rsweb:ReportViewer ID="rptCustomReportViewer" Width="100%" runat="server">
            <LocalReport>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:HiddenField Id="hdnError" runat="server"/>
    </div>
</div>
</asp:Content>

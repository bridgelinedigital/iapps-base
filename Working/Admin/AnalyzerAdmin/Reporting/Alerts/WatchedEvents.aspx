<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    StylesheetTheme="General" EnableEventValidation="false" Inherits="Reporting_Alerts_WatchedEvents"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerReportingAlertsWatchedEvents %>" CodeBehind="WatchedEvents.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams
        var _Count = '<%= GUIStrings.Count %>';
        var item = "";
        var selectedAction;
        var selectedNodeText;
        var removeEmptyRow = false;
        var clientStatus = "";
        var Make = "";
        function grdWatchedEventsPageviews_onContextMenu(sender, eventArgs) {
            grdWatchedEventsPageviews.select(eventArgs.get_item());
            if (grdWatchedEventsPageviews.EditingDirty == true) {
                cmWatchedEventsPageviews.Items(0).visible = false;
                cmWatchedEventsPageviews.Items(1).Visible = false;
                cmWatchedEventsPageviews.Items(2).Visible = false;
                cmWatchedEventsPageviews.Items(3).Visible = false;
                cmWatchedEventsPageviews.Items(4).Visible = false;
                cmWatchedEventsPageviews.Items(5).Visible = false;
                cmWatchedEventsPageviews.Items(6).Visible = false;

            } else {
                cmWatchedEventsPageviews.Items(0).Visible = true;
                cmWatchedEventsPageviews.Items(1).Visible = true;
                cmWatchedEventsPageviews.Items(2).Visible = true;
                cmWatchedEventsPageviews.Items(3).Visible = true;
                cmWatchedEventsPageviews.Items(4).Visible = true;
                cmWatchedEventsPageviews.Items(5).Visible = true;
                cmWatchedEventsPageviews.Items(6).Visible = true;

                clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                if (clientStatus == "Active") {
                    cmWatchedEventsPageviews.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                }
                else {
                    cmWatchedEventsPageviews.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                }

                item = eventArgs.get_item();

                if (grdWatchedEventsPageviews.get_table().getRowCount() == 1) {
                    if (eventArgs.get_item()) {
                        if (eventArgs.get_item().GetMemberAt(6).Text == "") {
                            cmWatchedEventsPageviews.Items(1).Visible = true;
                            cmWatchedEventsPageviews.Items(1).Visible = false;
                            cmWatchedEventsPageviews.Items(2).Visible = false;
                            cmWatchedEventsPageviews.Items(3).Visible = false;
                            cmWatchedEventsPageviews.Items(4).Visible = false;
                            cmWatchedEventsPageviews.Items(5).Visible = false;
                            cmWatchedEventsPageviews.Items(6).Visible = false;
                        }
                        else {
                            cmWatchedEventsPageviews.Items(1).Visible = true;
                            cmWatchedEventsPageviews.Items(2).Visible = true;
                            cmWatchedEventsPageviews.Items(3).Visible = true;
                            cmWatchedEventsPageviews.Items(4).Visible = true;
                            cmWatchedEventsPageviews.Items(5).Visible = true;
                            cmWatchedEventsPageviews.Items(6).Visible = true;
                            clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                            //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                            if (clientStatus == "Active") {
                                cmWatchedEventsPageviews.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                            }
                            else {
                                cmWatchedEventsPageviews.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                            }
                        }
                    }
                }
                var evt = eventArgs.get_event();
                cmWatchedEventsPageviews.showContextMenuAtEvent(evt, eventArgs.get_item());
            }
        }

        function cmWatchedEventsPageviews_ItemSelect(sender, eventArgs) {
            var selectedMenu = eventArgs.get_item().get_id();
            var tempItem = null;
            if (grdWatchedEventsPageviews.get_table().getRowCount() == 1) {
                tempItem = grdWatchedEventsPageviews.get_table().getRow(0);
                if (tempItem.getMemberAt(6).Text == "" || tempItem.getMemberAt(6).Text == null) {
                    removeEmptyRow = true;
                }
                else {
                    removeEmptyRow = false;
                }
            }
            else {
                removeEmptyRow = false;
            }
            switch (selectedMenu) {
                case "addNewAlert":
                    if (removeEmptyRow == true) {
                        grdWatchedEventsPageviews.get_table().ClearData();
                    }
                    item = null;
                    grdWatchedEventsPageviews.get_table().addEmptyRow(0);
                    item = grdWatchedEventsPageviews.get_table().getRow(0);
                    grdWatchedEventsPageviews.edit(item);
                    selectedAction = "Insert";
                    break;
                case "editAlert":
                    if (removeEmptyRow == false) {
                        grdWatchedEventsPageviews.edit(item);
                        selectedAction = "edit";
                    }
                    break;
                case "deleteAlert":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, AreYouSureYouWantToDeleteTheSelectedRecord %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "delete";
                            grdWatchedEventsPageviews.edit(item);
                            item.SetValue(5, selectedAction);
                            selectedAction = "delete";
                            grdWatchedEventsPageviews.editComplete();
                            grdWatchedEventsPageviews.callback();
                        }
                    }
                    else
                        return false;
                    break;
                case "changeAlertStatus":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, Areyousureyouwanttochangestatus %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "status";
                            grdWatchedEventsPageviews.edit(item);
                            item.SetValue(5, selectedAction);
                            // grdWatchedEventsPageviews.editComplete();
                            grdWatchedEventsPageviews.callback();
                        }
                    }
                    else
                        return false;
                    break;
            }
        }


        function CancelClicked() {
            if (selectedAction == '<asp:localize runat="server" text="<%$ Resources:JSMessages, Insert %>"/>') {
                if (grdWatchedEventsPageviews.Data.length == 1 && (item.getMember(0).get_value() == null || item.getMember(0).get_value() == '')) {
                    grdWatchedEventsPageviews.get_table().ClearData();
                    grdWatchedEventsPageviews.editCancel();
                    item = null;
                }
                else {
                    grdWatchedEventsPageviews.deleteItem(item);
                    grdWatchedEventsPageviews.editCancel();
                    item = null;
                }
            }
            else {
                grdWatchedEventsPageviews.editCancel();
            }
            if (removeEmptyRow == true) {
                grdWatchedEventsPageviews.get_table().addEmptyRow(0);
            }
        }


        function setPageLevelValue(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
        }


        function getPageLevelValue(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }



        function setPageLevelCountValue(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
        }


        function getPageLevelCountValue(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }


        function getPageOperation(control) {
            return [selectedAction, selectedAction];
        }


        function SaveRecord() {
            if (CheckSession()) {
                if (ValidateInput() == true) {
                    var watchName = document.getElementById(cmbwatch).innerText;
                    var watchResetEvery = document.getElementById(dropdownlist).value;
                    var watchView = document.getElementById(txtCount).value;
                    var watchEmail = document.getElementById(txtEmail).value;
                    var watchAlertId = item.getMember(6).get_value();
                    var watchstatus = document.getElementById(txtStatus).value;
                    var WatchReachCount = item.getMember(10).get_value();
                    var WatchId = item.getMember(7).get_value();
                    var AlertMessage = grdWatchedEventsPageviews_Callback(watchName, watchResetEvery, watchView, watchEmail, watchAlertId, watchstatus, selectedAction, WatchReachCount, WatchId);
                    if (AlertMessage == "") {
                        grdWatchedEventsPageviews.editComplete();
                    } else {
                        var alertWatch = cmbPages.get_text();
                        alert(stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, The0pageisalreadyreflected %>'/>", alertWatch));
                        //alert(AlertMessage);
                    }

                }
            }
        }


        function getPageResetValue(control, DataField, DataItem) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }

        //-------------------combobox for watch name -----------

        function grdWatchedEventsPageviews_onBeforeCallback(sender, eventArgs) {
            if (cmbPages != null) {
                cmbPages.dispose();
            }
        }

        function cmbPages_Change(sender, eventArgs) {
            if (cmbPages.getSelectedItem() != null) {
                cmbPages.text = cmbPages.getSelectedItem().Text;
                cmbPages.value = cmbPages.getSelectedItem().Value;
                item.SetValue(7, cmbPages.getSelectedItem().Value, true);
                cmbPages.set_text(cmbPages.getSelectedItem().Text);
                cmbPages.collapse();
            }
        }


        function getCategory(DataField, DataItem) {
            var retValue = '';
            if (cmbPages != null) {
                if (cmbPages.getSelectedItem() != null) {
                    retValue = cmbPages.getSelectedItem().Text;
                }
            }
            return [retValue, retValue];
        }


        function setCategory(DataItem) {
            if (cmbPages != null) {
                cmbPages.beginUpdate();
                for (var i = 0; i < cmbPages.get_itemCount(); i++) {
                    var item = cmbPages.getItem(i);
                    cmbPages.unSelect();
                    if (item.Value == DataItem.getMember('WatchId').get_text()) {
                        cmbPages.selectItem(item);
                        if (cmbPages.getSelectedItem() != null) {
                            cmbPages.text = cmbPages.getSelectedItem().Text
                        }
                        break;
                    }
                }
                cmbPages.endUpdate();
            }
        }

        //--------------------------dropdown fro resetevery-----------------------


        function getPageResetValue(control, DataField, DataItem) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }

        function setPageResetValue(control, DataField, DataItem) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            for (i = 0; i < txtControl.length; i++) {
                if (txtControl[i].value == value) {
                    txtControl[i].selected = true;
                }
            }
        }


        function ValidateInput() {
            var retValue = true;
            var errorMessage = '';
            var combocmbwatch = document.getElementById(cmbPages);
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                if (cmbPages.SelectedIndex == -1) {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseselectthewatchname %>"/>' + '\n';
                    retValue = false;
                }
                else {
                }
            }
            else {
            }

            var ObjPageClientId = document.getElementById(txtCount);
            watchviewbln = true;
            var len = '2147483648';
            if (Trim(ObjPageClientId.value) != '') {
                if (ObjPageClientId.value.match(/^[0-9]+$/) && ObjPageClientId.value != 0) {
                    if (ObjPageClientId.value < parseInt(len)) {

                    } else {
                        errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, YouhaveenteredabovethemaximumlimitPleaseenterbelow2147483648 %>'/>" + '\n';
                        retValue = false;
                        watchviewbln = false;
                    }

                }
                else {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseuseonlynumericcharactersinthecount %>"/>' + '\n';
                    retValue = false;
                    watchviewbln = false;
                }

            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenterthecount %>"/>' + '\n';
                ObjPageClientId.focus();
                retValue = false;
                watchviewbln = false;
            }

            var Reset = document.getElementById(dropdownlist);
            if (Reset.value == "" || Reset.length == 0) {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseselectresetevery %>"/>' + '\n'; //alert("Please select ResetEvery");            
                Reset.focus();
                retValue = false;
            }


            var email = document.getElementById(txtEmail);
            emailbln = true;
            if (Trim(email.value) != '') {
                if (!AdminCallback.ValidateByType('Emails', email.value)) {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenteravalidemailaddress %>"/>' + '\n';
                    retValue = false;
                    emailbln = false;
                }
            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenteranemailaddress %>"/>' + '\n';
                retValue = false;
                emailbln = false;
            }

            if (!retValue)
                alert(errorMessage);
            if (watchviewbln == false) {
                ObjPageClientId.focus();
                return retValue;
            }
            if (emailbln == false) {
                email.focus();
                return retValue;
            }
            return retValue;
        }



        function grdWatchedEventsPageviews_onCallbackError(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            grdWatchedEventsPageviews.page(0);
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, callbackerror %>'/>");
        }

        function grdWatchedEventsPageviews_onCallbackComplete(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            if (selectedAction == "Insert") {
                grdWatchedEventsPageviews.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Thenewwatcheventwasaddedsuccessfully %>'/>");
            }
            else if (selectedAction == "edit") {
                grdWatchedEventsPageviews.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Thewatcheventwasupdatedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Delete %>'/>") {

                item = null;
                if (grdWatchedEventsPageviews.get_table().getRowCount() <= 0 && grdWatchedEventsPageviews.PageCount == 0) {
                    grdWatchedEventsPageviews.get_table().addEmptyRow(0);
                }
                else if (grdWatchedEventsPageviews.get_table().getRowCount() <= 0) {
                    grdWatchedEventsPageviews.previousPage();
                }
                grdWatchedEventsPageviews.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Theselectedwatchedeventswasdeletedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, status %>'/>") {
                grdWatchedEventsPageviews.editComplete();
                selectedAction = "";
            }
            grdWatchedEventsPageviews.editComplete();
        }

        function grdWatchedEventsPageviews_onSortChange(sender, eventArgs) {
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, edit %>'/>") {
                grdWatchedEventsPageviews.editCancel();
            }
            selectedAction = "";
        }

        function grdWatchedEventsPageviews_onItemBeforeUpdate(sender, eventArgs) {
            var SelectedReachCount = item.getMember(2).get_text();
            item.SetValue(10, SelectedReachCount.value, true);
        }

        var saveAlt = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Save %>"/>';
        var saveTitle = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Save %>"/>';

        var cancelAlt = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Cancel %>"/>';
        var cancelTitle = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Cancel %>"/>';  
    </script>
</asp:Content>
<asp:Content ID="contentWatchedEvents" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
    <div class="watched-events">
        <div class="page-header clear-fix">
            <h1><%= GUIStrings.CurrentWatchActivity %></h1>
        </div>
        <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, Setanalerttobenotifiedwhenawatchedeventistriggeredacertainnumberoftimeswithinagivenperiod %>"/></p>
        <ComponentArt:Grid SkinID="Default" ID="grdWatchedEventsPageviews" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoalertsforWatchedEventsPageviews %>"
            AutoPostBackOnSelect="false" Width="100%" CallbackReloadTemplateScripts="true"
            PageSize="10" SliderPopupClientTemplateId="grdWatchedEventsPageviewsSlider" PagerInfoClientTemplateId="grdWatchedEventsPageviewsPagination"
            AllowEditing="true" AutoCallBackOnInsert="false" AutoCallBackOnUpdate="true"
            AutoCallBackOnDelete="false" EditOnClickSelectedItem="false" CallbackReloadTemplates="true">
            <ClientEvents>
                <CallbackError EventHandler="grdWatchedEventsPageviews_onCallbackError" />
                <BeforeCallback EventHandler="grdWatchedEventsPageviews_onBeforeCallback" />
                <SortChange EventHandler="grdWatchedEventsPageviews_onSortChange" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    EditCommandClientTemplateId="EditCommandTemplate" EditFieldCssClass="EditDataField"
                    EditCellCssClass="EditDataCell" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                    <ConditionalFormats>
                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('Name').Value==''"
                            RowCssClass="DefaultPageRow" SelectedRowCssClass="SelectedRow" />
                    </ConditionalFormats>
                    <Columns>
                        <ComponentArt:GridColumn Width="400" runat="server" HeadingText="<%$ Resources:GUIStrings, WatchName %>" HeadingCellCssClass="FirstHeadingCell"
                            DataField="Name" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" EditCellServerTemplateId="svtWatchName"
                            EditControlType="Custom" CustomEditSetExpression="setCategory(DataItem)" CustomEditGetExpression="getCategory()" />
                        <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, Count %>" DataField="AlertCount" IsSearchable="true"
                            SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="CountHT"
                            DataType="System.Int32" EditCellServerTemplateId="svtCount" EditControlType="Custom"
                            Align="Right" />
                        <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, ResetEvery %>" DataField="Reset"
                            SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                            EditCellServerTemplateId="svtResetWatch" EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="340" runat="server" HeadingText="<%$ Resources:GUIStrings, Email %>" DataField="ExternalEmailId"
                            SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                            EditControlType="Custom" EditCellServerTemplateId="svtEmail" />
                        <ComponentArt:GridColumn Width="120" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>" DataField="Status" SortedDataCellCssClass="SortedDataCell"
                            AllowReordering="false" FixedWidth="true" AllowEditing="False" EditControlType="Custom"
                            EditCellServerTemplateId="svtStatus" />
                        <ComponentArt:GridColumn IsSearchable="false" FixedWidth="true" AllowSorting="False"
                            Width="80" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>" EditControlType="EditCommand" Align="Center"
                            AllowReordering="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn Width="228" HeadingText="WatchId" HeadingCellCssClass="FirstHeadingCell"
                            DataField="WatchId" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                            IsSearchable="false" AllowReordering="false" Visible="false" FixedWidth="true"
                            EditControlType="Custom" />
                        <ComponentArt:GridColumn DataField="SubscriptionId" Visible="false" />
                        <ComponentArt:GridColumn DataField="ScheduleId" Visible="false" />
                        <ComponentArt:GridColumn DataField="Reset" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="svtWatchName">
                    <Template>
                        <ComponentArt:ComboBox ID="cmbPages" TextBoxEnabled="false" SkinID="Default" runat="server"
                            Width="220" DropDownHeight="297" DropDownWidth="220" AutoHighlight="false" AutoComplete="true"
                            AutoFilter="true">
                            <ClientEvents>
                                <Change EventHandler="cmbPages_Change" />
                            </ClientEvents>
                        </ComponentArt:ComboBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtCount">
                    <Template>
                        <asp:TextBox ID="txtCount" runat="server" CssClass="textBoxes" Width="80%"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtResetWatch">
                    <Template>
                        <asp:DropDownList ID="ddlReset" runat="server" Width="80%">
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, CountReached %>" Value="CountReached"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Daily %>" Value="Daily"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Weekly %>" Value="Weekly"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Monthly %>" Value="Monthly"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Quarterly %>" Value="Quarterly"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Yearly %>" Value="Yearly"></asp:ListItem>
                        </asp:DropDownList>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtEmail">
                    <Template>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxes" Width="80%"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtStatus">
                    <Template>
                        <asp:TextBox ID="txtStatus" runat="server" CssClass="textBoxes" Width="195"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="CountHT">
                    <div class="custom-header">
                        <span class="header-text">##_Count##</span>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="EditCommandTemplate">
                    <a href="javascript:SaveRecord();">
                        <img src="/iapps_images/cm-icon-add.png" border="0"  alt=saveAlt title=saveTitle /></a>
                    <a href="javascript:CancelClicked();">
                        <img src="/iapps_images/cm-icon-delete.png" border="0"  alt=cancelAlt
                            title=cancelTitle /></a>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="EditTemplate">
                    <a href="javascript:editGrid('## DataItem.ClientId ##');"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Edit1 %>"/></a>
                    <a href="javascript:deleteRow('## DataItem.ClientId ##')">
                        <asp:localize runat="server" text="<%$ Resources:GUIStrings, Delete1 %>"/></a>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdWatchedEventsPageviewsSlider">
                    <div class="SliderPopup">
                        <h5>
                            ## DataItem.GetMember(0).Value ##</h5>
                        <p>
                            ## DataItem.GetMember(1).Value ##</p>
                        <p>
                            ## DataItem.GetMember(2).Value ##</p>
                        <p>
                            ## DataItem.GetMember(3).Value ##</p>
                        <p>
                            ## DataItem.GetMember(4).Value ##</p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdWatchedEventsPageviews.PageCount) ##</span> 
                            <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdWatchedEventsPageviews.RecordCount) ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdWatchedEventsPageviewsPagination">
                    ## GetGridPaginationInfo(grdWatchedEventsPageviews) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ItemBeforeUpdate EventHandler="grdWatchedEventsPageviews_onItemBeforeUpdate" />
                <ContextMenu EventHandler="grdWatchedEventsPageviews_onContextMenu" />
                <CallbackError EventHandler="grdWatchedEventsPageviews_onCallbackError" />
                <CallbackComplete EventHandler="grdWatchedEventsPageviews_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:Grid>
        <ComponentArt:Menu SkinID="ContextMenu" ID="cmWatchedEventsPageviews" runat="server">
            <Items>
                <ComponentArt:MenuItem ID="addNewAlert" runat="server" Text="<%$ Resources:GUIStrings, AddNewAlert %>" Look-LeftIconUrl="cm-icon-add.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="editAlert" runat="server" Text="<%$ Resources:GUIStrings, EditAlert %>" Look-LeftIconUrl="cm-icon-edit.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="deleteAlert" runat="server" Text="<%$ Resources:GUIStrings, DeleteAlert %>" Look-LeftIconUrl="cm-icon-delete.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="changeAlertStatus" runat="server" Text="<%$ Resources:GUIStrings, MakeActiveInactive %>">
                </ComponentArt:MenuItem>
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="cmWatchedEventsPageviews_ItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
    </div>
</asp:Content>

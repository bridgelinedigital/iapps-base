<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" Theme="General" runat="server"
    Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.PageViews" CodeBehind="PageViews.aspx.cs"
    Title="<%$ Resources:GUIStrings, iAPPSAnalyzerReportingAlertsPageviews %>" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".pageviews").on("click", "#hplAddPageview", function (e) {
                OpenPageviewDetails("");
                e.stopPropagation();
            });
        });

        function OpenPageviewDetails(id) {
            OpeniAppsAnalyzerPopup("PageViewDetails", "Id=" + id, "ClosePageviewDetails");
        }

        function ClosePageviewDetails() {
            grdPageviews_callback();
        }

        function grdPageviews_callback(action) {
            var callbackJson = {};
            callbackJson.Action = "Load";
            if (typeof action != "undefined")
                callbackJson.Action = action;

            callbackJson.SelectedItems = [];
            if (typeof gridItem != "undefined")
                callbackJson.SelectedItems.push(gridItem.getMember('Id').get_value());

            grdPageviews.set_callbackParameter(JSON.stringify(callbackJson));
            grdPageviews.callback();
        }

        function grdPageviews_onRenderComplete(sender, eventArgs) {
            FormatDataGrid(sender, false);
        }

        function grdPageviews_onContextMenu(sender, eventArgs) {
            grdPageviews.select(eventArgs.get_item());
            gridItem = eventArgs.get_item();

            var evt = eventArgs.get_event();
            menuItems = cmPageviews.get_items();

            var statusMnuItem = menuItems.getItemById("changePageviewStatus");
            if (gridItem.getMember('Status').get_value() == "Active")
                statusMnuItem.set_text("<%= JSMessages.MakeInactive %>");
            else
                statusMnuItem.set_text("<%= JSMessages.MakeActive %>");

            cmPageviews.showContextMenuAtEvent(evt);
        }

        function cmPageviews_ItemSelect(sender, eventArgs) {
            var selectedMenuId = eventArgs.get_item().get_id();
            var selectedId = gridItem.getMember('Id').get_text();
            switch (selectedMenuId) {
                case "editPageview":
                    OpenPageviewDetails(selectedId);
                    break;
                case "deletePageview":
                    var agree = confirm("<%= JSMessages.AreYouSureYouWantToDeleteTheSelectedRecord  %>");
                    if (agree)
                        grdPageviews_callback("Delete");
                    break;
                case "changePageviewStatus":
                    var agree = confirm("<%= JSMessages.Areyousureyouwanttochangestatus %>");
                    if (agree) {
                        if (eventArgs.get_item().get_text() == "<%= JSMessages.MakeActive %>")
                            grdPageviews_callback("MakeActive");
                        else
                            grdPageviews_callback("MakeInActive");
                    }
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="alertsContent" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
    <div class="pageviews">
        <div class="page-header clear-fix">
            <h1>
                <%= GUIStrings.PageViews %></h1>
            <asp:HyperLink ID="hplAddPageview" runat="server" Text="<%$ Resources:GUIStrings, AddNewAlert %>"
                CssClass="primarybutton" ClientIDMode="Static" />
        </div>
        <p>
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Setanalerttobenotifiedwhenapageisviewedacertainnumberoftimeswithinagivenperiod %>" />
        </p>
        <ComponentArt:Grid SkinID="Default" ID="grdPageviews" runat="server" RunningMode="Callback"
            EmptyGridText="<%$ Resources:GUIStrings, Noalertsforpageviews %>" AutoPostBackOnSelect="false"
            Width="100%" PageSize="10" PagerInfoClientTemplateId="grdPageviews_pageInfoTemplate"
            LoadingPanelClientTemplateId="grdPageviews_loadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                    <ConditionalFormats>
                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('PageTitle').Value=='#Page Not Found#'"
                            RowCssClass="DefaultPageRow" SelectedRowCssClass="SelectedRow" />
                    </ConditionalFormats>
                    <Columns>
                        <ComponentArt:GridColumn Width="270" runat="server" HeadingText="<%$ Resources:GUIStrings, PageTitle %>"
                            HeadingCellCssClass="FirstHeadingCell" DataCellCssClass="FirstDataCell" DataField="PageTitle"
                            SortedDataCellCssClass="SortedDataCell" IsSearchable="true" AllowReordering="false" />
                        <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, Pageviews1 %>"
                            DataField="AlertCount" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                            AllowReordering="false" FixedWidth="true" DataType="System.Int32" Align="Right" />
                        <ComponentArt:GridColumn Width="122" runat="server" HeadingText="<%$ Resources:GUIStrings, ResetEvery %>"
                            DataField="Reset" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" EditCellServerTemplateId="svtReset" EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="200" runat="server" HeadingText="<%$ Resources:GUIStrings, Email1 %>"
                            DataField="ExternalEmailID" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" />
                        <ComponentArt:GridColumn Width="122" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>"
                            DataField="Status" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="ObjectURL" Visible="false" />
                        <ComponentArt:GridColumn DataField="ObjectId" Visible="false" />
                        <ComponentArt:GridColumn DataField="Reset" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdPageviews_loadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdPageviews) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdPageviews_pageInfoTemplate">
                    ## stringformat(Page0of12items, gridCurrentPageIndex(grdPageviews), gridPageCount(grdPageviews),
                    grdPageviews.RecordCount) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ContextMenu EventHandler="grdPageviews_onContextMenu" />
                <RenderComplete EventHandler="grdPageviews_onRenderComplete" />
            </ClientEvents>
        </ComponentArt:Grid>
        <ComponentArt:Menu SkinID="ContextMenu" ID="cmPageviews" runat="server">
            <Items>
                <ComponentArt:MenuItem ID="editPageview" runat="server" Text="<%$ Resources:GUIStrings, EditAlert %>"
                    Look-LeftIconUrl="cm-icon-edit.png" />
                <ComponentArt:MenuItem LookId="BreakItem" />
                <ComponentArt:MenuItem ID="deletePageview" runat="server" Text="<%$ Resources:GUIStrings, DeleteAlert %>"
                    Look-LeftIconUrl="cm-icon-delete.png" />
                <ComponentArt:MenuItem LookId="BreakItem" />
                <ComponentArt:MenuItem ID="changePageviewStatus" runat="server" Text="<%$ Resources:GUIStrings, MakeActiveInactive %>" />
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="cmPageviews_ItemSelect" />
            </ClientEvents>
            <ClientEvents>
            </ClientEvents>
        </ComponentArt:Menu>
    </div>
</asp:Content>

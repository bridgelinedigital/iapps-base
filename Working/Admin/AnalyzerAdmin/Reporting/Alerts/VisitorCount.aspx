<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    Theme="General" Inherits="Reporting_Alerts_VisitorCount" runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerReportingAlertsVisitorCount %>"
    CodeBehind="VisitorCount.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>"; //added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>"; //added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by Adams

        var item = "";
        var selectedAction = "";
        var removeEmptyRow = false;
        var errorMessage = false;
        var aletVisitorStatus = "";
        var Make = "";
        function grdVisitorPageviews_onContextMenu(sender, eventArgs) {
            grdVisitorPageviews.select(eventArgs.get_item());
            if (grdVisitorPageviews.EditingDirty == true) {
                cmVisitorCountPageviews.Items(0).visible = false;
                cmVisitorCountPageviews.Items(1).Visible = false;
                cmVisitorCountPageviews.Items(2).Visible = false;
                cmVisitorCountPageviews.Items(3).Visible = false;
                cmVisitorCountPageviews.Items(4).Visible = false;
                cmVisitorCountPageviews.Items(5).Visible = false;
                cmVisitorCountPageviews.Items(6).Visible = false;

            } else {
                cmVisitorCountPageviews.Items(0).Visible = true;
                cmVisitorCountPageviews.Items(1).Visible = true;
                cmVisitorCountPageviews.Items(2).Visible = true;
                cmVisitorCountPageviews.Items(3).Visible = true;
                cmVisitorCountPageviews.Items(4).Visible = true;
                cmVisitorCountPageviews.Items(5).Visible = true;
                aletVisitorStatus = eventArgs.get_item().GetMemberAt(5).Text
                //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                if (aletVisitorStatus == "Active") {
                    cmVisitorCountPageviews.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                }
                else {
                    cmVisitorCountPageviews.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive%>'/>";
                }
                cmVisitorCountPageviews.Items(6).Visible = true;

                item = eventArgs.get_item();
                if (grdVisitorPageviews.get_table().getRowCount() == 1) {
                    if (eventArgs.get_item()) {
                        if (eventArgs.get_item().GetMemberAt(7).Text == "") {
                            cmVisitorCountPageviews.Items(0).Visible = true;
                            cmVisitorCountPageviews.Items(1).Visible = false;
                            cmVisitorCountPageviews.Items(2).Visible = false;
                            cmVisitorCountPageviews.Items(3).Visible = false;
                            cmVisitorCountPageviews.Items(4).Visible = false;
                            cmVisitorCountPageviews.Items(5).Visible = false;
                            cmVisitorCountPageviews.Items(6).Visible = false;
                        }
                        else {
                            cmVisitorCountPageviews.Items(0).Visible = true;
                            cmVisitorCountPageviews.Items(1).Visible = true;
                            cmVisitorCountPageviews.Items(2).Visible = true;
                            cmVisitorCountPageviews.Items(3).Visible = true;
                            cmVisitorCountPageviews.Items(4).Visible = true;
                            cmVisitorCountPageviews.Items(5).Visible = true;
                            aletVisitorStatus = eventArgs.get_item().GetMemberAt(5).Text
                            //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make%>'/>";
                            if (aletVisitorStatus == "Active") {
                                cmVisitorCountPageviews.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                            }
                            else {
                                cmVisitorCountPageviews.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive%>'/>";
                            }
                            cmVisitorCountPageviews.Items(6).Visible = true;
                        }
                    }
                }
                var evt = eventArgs.get_event();
                cmVisitorCountPageviews.showContextMenuAtEvent(evt, eventArgs.get_item());
            }
        }

        function grdVisitorPageviews_setMenuItem() {
            var menuItems = cmVisitorCountPageviews.get_items();
            if (grdVisitorPageviews.RecordCount == 1 && item.getMember("Name").get_text() == "") {
                //alert(menuItems.getItem(0));
                menuItems.getItem(0).set_visible(true);
                menuItems.getItem(1).set_visible(false);
                menuItems.getItem(2).set_visible(false);
                menuItems.getItem(3).set_visible(false);
                menuItems.getItem(4).set_visible(false);
                menuItems.getItem(5).set_visible(false);
                menuItems.getItem(6).set_visible(false);
            }
            else {
                menuItems.getItem(0).set_visible(true);
                menuItems.getItem(1).set_visible(true);
                menuItems.getItem(2).set_visible(true);
                menuItems.getItem(3).set_visible(true);
                menuItems.getItem(4).set_visible(true);
                menuItems.getItem(5).set_visible(true);
                menuItems.getItem(6).set_visible(true);
            }
        }


        function cmAlertVisitorCount_ItemSelect(sender, eventArgs) { //debugger;
            var selectedMenu = eventArgs.get_item().get_id();
            if (grdVisitorPageviews.get_table().getRowCount() == 1) {
                tempItem = grdVisitorPageviews.get_table().getRow(0);
                if (tempItem.getMemberAt(7).Text == "" || tempItem.getMemberAt(7).Text == null) {
                    removeEmptyRow = true;
                }
                else {
                    removeEmptyRow = false;
                }
            }
            else {
                removeEmptyRow = false;
            }
            switch (selectedMenu) {
                case "addNewVisitorCountAlert":
                    if (removeEmptyRow == true) {
                        grdVisitorPageviews.get_table().ClearData();
                    }
                    item = null;
                    grdVisitorPageviews.get_table().addEmptyRow(0);
                    item = grdVisitorPageviews.get_table().getRow(0);
                    grdVisitorPageviews.edit(item);
                    selectedAction = "Insert";
                    break;
                case "editVisitorCountAlert":
                    if (removeEmptyRow == false) {
                        grdVisitorPageviews.edit(item);
                        selectedAction = "edit";
                    }
                    break;
                case "deleteVisitorCountAlert":
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, AreYouSureYouWantToDeleteTheSelectedRecord %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "delete";
                            grdVisitorPageviews.edit(item);
                            item.SetValue(6, selectedAction);
                            selectedAction = "delete";
                            grdVisitorPageviews.editComplete();
                            grdVisitorPageviews.callback();
                        }
                    }
                    else
                        return false;
                    break;
                case "changeVisitorCountAlertStatus":
                    // Are you sure you want to activate the selected Visitor Count?
                    var agree = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, Areyousureyouwanttochangestatus %>'/>");
                    if (agree) {
                        if (removeEmptyRow == false) {
                            selectedAction = "status";
                            grdVisitorPageviews.edit(item);
                            item.SetValue(6, selectedAction);
                            grdVisitorPageviews.editComplete();
                            grdVisitorPageviews.callback();
                        }
                    }
                    else
                        return false;
                    break;
            }
        }


        function CancelClicked() {
            if (selectedAction == '<asp:localize runat="server" text="<%$ Resources:JSMessages, Insert %>"/>') {
                relativeId = '';
                if (grdVisitorPageviews.Data.length == 1 && (item.getMember(0).get_value() == null || item.getMember(0).get_value() == '')) {
                    grdVisitorPageviews.get_table().ClearData();
                    grdVisitorPageviews.editCancel();
                    item = null;
                }
                else {
                    grdVisitorPageviews.editCancel();
                    grdVisitorPageviews.deleteItem(item);
                    item = null;
                }
            }
            else {
                grdVisitorPageviews.editCancel();
            }
            if (removeEmptyRow == true) {
                grdVisitorPageviews.get_table().addEmptyRow(0);
            }
        }

        //----------------------------save--------------------------------
        function SaveRecord() {
            if (CheckSession()) {
                if (ValidateInput() == true) {
                    var vistorName = document.getElementById(txtName).value;
                    var visitResetEvery = document.getElementById(dropdownlist).value;
                    var visitView = document.getElementById(txtCount).value;
                    var visitEmail = document.getElementById(txtEmail).value;
                    var visitAlertId = item.getMember(7).get_value();
                    var visitEditCount = item.getMember(10).get_value();
                    var visitorstatus = document.getElementById(txtStatus).value;
                    var vistorUnique = document.getElementById(chkUnique).checked;
                    var AlertMessage = grdVisitorPageviews_Callback(vistorName, visitResetEvery, visitView, visitEmail, visitAlertId, visitorstatus, vistorUnique, selectedAction, visitEditCount);
                    if (AlertMessage == "") {
                        grdVisitorPageviews.editComplete();
                    } else {
                        alert(AlertMessage);
                    }
                }
            }
        }

        //--------------------------validation-----------------------------
        function ValidateInput() {
            var retValue = true;
            var errorMessage = '';
            var ObjPageName = document.getElementById(txtName);
            visitNamebln = true;
            if (Trim(ObjPageName.value) != '') {

                if (ObjPageName.value.match(/^[a-zA-Z0-9. _]+$/)) {
                    //alert(ObjPageName.value);
                }
                else {
                    errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseuseonlyalphanumericcharactersinthename %>'/>" + "\n";
                    ObjPageName.focus();
                    retValue = false;
                    visitNamebln = false;
                }
            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenteraname %>"/>' + '\n';
                ObjPageName.focus();
                retValue = false;
                visitNamebln = false;
            }

            var ObjPageCount = document.getElementById(txtCount);
            visitCountbln = true;
            var len = '2147483648';
            if (Trim(ObjPageCount.value) != '') {
                if (ObjPageCount.value.match(/^[0-9]+$/) && ObjPageCount.value != 0) {
                    if (ObjPageCount.value < parseInt(len)) {
                    } else {
                        errorMessage = errorMessage + "<asp:localize runat='server' text='<%$ Resources:JSMessages, YouhaveenteredabovethemaximumlimitPleaseenterbelow2147483648 %>'/>" + '\n';
                        retValue = false;
                        visitCountbln = false;
                    }
                }
                else {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseuseonlynumericcharactersinthecount %>"/>' + '\n';
                    retValue = false;
                    visitCountbln = false;
                }
            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenteracount %>"/>' + '\n';
                ObjPageCount.focus();
                retValue = false;
                visitCountbln = false;
            }

            var email = document.getElementById(txtEmail);
            emailBln = true;
            if (Trim(email.value) != '') {
                if (!AdminCallback.ValidateByType('Emails', email.value)) {
                    errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenteravalidemailaddress %>"/>' + '\n';
                    retValue = false;
                    emailBln = false;
                }
            }
            else {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseenteranemailaddress %>"/>' + '\n';
                retValue = false;
                emailBln = false;
            }

            var Reset = document.getElementById(dropdownlist);
            if (Reset.value == "" || Reset.length == 0) {
                errorMessage = errorMessage + '<asp:localize runat="server" text="<%$ Resources:JSMessages, Pleaseselectaresetevery %>"/>' + '\n';
                ObjPageCount.focus();
                retValue = false;
            }
            if (!retValue)
                alert(errorMessage);
            if (visitNamebln == false) {
                ObjPageName.focus();
                return retValue;
            }
            if (visitCountbln == false) {
                ObjPageCount.focus();
                return retValue;
            }
            if (emailBln == false) {
                email.focus();
                return retValue;
            }
            return retValue;
        }


        //---------------------------------------textbox-----------------------------
        function setPageLevelValue(control, DataField) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
        }

        function getPageLevelValue(control, DataField) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }


        //----------------------action-----------------------------
        function getPageOperation(control) {
            return [selectedAction, selectedAction];
        }


        //----------------------dropdown-------------------------------
        function getPageResetValue(control, DataField, DataItem) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.value;
            return [PageName, PageName];
        }


        function setPageResetValue(control, DataField, DataItem) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            for (i = 0; i < txtControl.length; i++) {
                if (txtControl[i].value == value) {
                    txtControl[i].selected = true;
                }
            }
        }
        //----------checkbox--------------------------------
        function getValueCheckbox(control, DataField, DataItem) {
            var txtControl = document.getElementById(control);
            var Status = txtControl.checked;
            return [Status, Status];
        }

        function setValueCheckbox(control, DataField, DataItem) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            if (value == "true")
                txtControl.checked = true;
            else
                txtControl.checked = false;
        }


        function getValueUniqueCheckbox(control, DataField, DataItem) {
            var txtControl = document.getElementById(control);
            var PageName = txtControl.checked;
            return [PageName, PageName];
        }


        function setValueUniqueCheckbox(control, DataField, DataItem) {
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            if (value == true)
                txtControl.checked = true;
            else
                txtControl.checked = false;
        }


        function grdVisitorPageviews_onCallbackError(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            var error = eventArgs.get_errorMessage();
            alert(error);
            grdVisitorPageviews.Callback();
            return false;
        }

        function grdVisitorPageviews_onCallbackComplete(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                grdVisitorPageviews.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Thenewvisitorcountwasaddedsuccessfully %>'/>");
            }
            else if (selectedAction == "edit") {
                grdVisitorPageviews.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Thevisitorcountwasupdatedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Delete %>'/>") {
                item = null;
                if (grdVisitorPageviews.get_table().getRowCount() <= 0 && grdVisitorPageviews.PageCount == 0) {
                    grdVisitorPageviews.get_table().addEmptyRow(0);
                }
                else if (grdVisitorPageviews.get_table().getRowCount() <= 0) {
                    grdVisitorPageviews.previousPage();
                }
                grdVisitorPageviews.editComplete();
                selectedAction = "";
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Theselectedvisitorcountwasdeletedsuccessfully %>'/>");
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, status %>'/>") {

                grdVisitorPageviews.editComplete();
                selectedAction = "";
            }
            grdVisitorPageviews.editComplete();
        }

        function grdVisitorPageviews_onSortChange(sender, eventArgs) {
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, edit %>'/>") {
                grdVisitorPageviews.editCancel();
            }
            selectedAction = "";
        }

        function grdVisitorPageviews_onItemBeforeUpdate(sender, eventArgs) {
            var SelectedReachCount = item.getMember(3).get_text();
            item.SetValue(10, SelectedReachCount.value, true);
        }

        var saveAlt = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Save %>"/>';
        var saveTitle = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Save %>"/>';

        var cancelAlt = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Cancel %>"/>';
        var cancelTitle = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, Cancel %>"/>';  
    </script>
</asp:Content>
<asp:Content ID="contentVisitorCount" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
    <div class="visitor-count">
        <div class="page-header clear-fix">
            <h1><%= GUIStrings.VisitorCount %></h1>
        </div>
        <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, Setanalerttobenotifiedwhenacertainnumberofvisitorscometothesitewithinagivenperiod %>"/></p>
        <ComponentArt:Grid SkinID="Default" ID="grdVisitorPageviews" runat="server" RunningMode="Callback"
            AllowEditing="true" EmptyGridText="<%$ Resources:GUIStrings, NoalertsforVisitorPageviews %>" AutoPostBackOnSelect="false"
            Width="100%" PageSize="10" EditOnClickSelectedItem="false" SliderPopupClientTemplateId="grdVisitorPageviewsSlider"
            PagerInfoClientTemplateId="grdVisitorPageviewsPagination" AutoCallBackOnInsert="false"
            AutoCallBackOnUpdate="true" CallbackCachingEnabled="true" CallbackCacheLookAhead="25"
            AutoCallBackOnDelete="false" CallbackReloadTemplateScripts="true" CallbackReloadTemplates="true">
            <ClientEvents>
                <SortChange EventHandler="grdVisitorPageviews_onSortChange" />
                <ItemBeforeUpdate EventHandler="grdVisitorPageviews_onItemBeforeUpdate" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                    HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                    HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    EditCommandClientTemplateId="EditCommandTemplate" EditFieldCssClass="EditDataField"
                    EditCellCssClass="EditDataCell" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                    <Columns>
                        <ComponentArt:GridColumn Width="350" runat="server" HeadingText="<%$ Resources:GUIStrings, VisitorName %>" HeadingCellCssClass="FirstHeadingCell"
                            DataField="Name" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                            IsSearchable="true" AllowReordering="false" FixedWidth="true" EditCellServerTemplateId="svtName"
                            EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="80" runat="server" HeadingText="<%$ Resources:GUIStrings, Unique %>" DataField="UniqueVisitor"
                            IsSearchable="true" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                            FixedWidth="true" Align="Center" DataCellClientTemplateId="UniqueTemplate" EditControlType="Custom"
                            EditCellServerTemplateId="svtUnique" />
                        <ComponentArt:GridColumn Width="77" runat="server" HeadingText="<%$ Resources:GUIStrings, Count %>" DataField="AlertCount" IsSearchable="true"
                            SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                            EditCellServerTemplateId="svtCount" EditControlType="Custom" Align="Right" DataType="System.Int32" />
                        <ComponentArt:GridColumn Width="150" runat="server" HeadingText="<%$ Resources:GUIStrings, ResetEvery %>" DataField="Reset"
                            SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                            EditCellServerTemplateId="svtReset" EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="300" runat="server" HeadingText="<%$ Resources:GUIStrings, Email1 %>" DataField="ExternalEmailID"
                            SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                            EditCellServerTemplateId="svtEmail" EditControlType="Custom" />
                        <ComponentArt:GridColumn Width="100" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>" DataField="Status" SortedDataCellCssClass="SortedDataCell"
                            AllowReordering="false" FixedWidth="true" AllowEditing="False" EditControlType="Custom"
                            EditCellServerTemplateId="svtStatus" />
                        <ComponentArt:GridColumn IsSearchable="false" FixedWidth="true" AllowSorting="False"
                            Width="80" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>" EditControlType="EditCommand" Align="Center"
                            AllowReordering="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="SubscriptionId" Visible="false" />
                        <ComponentArt:GridColumn DataField="ScheduleId" Visible="false" />
                        <ComponentArt:GridColumn DataField="Reset" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="UniqueSvt">
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtName">
                    <Template>
                        <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" Width="208"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtUnique">
                    <Template>
                        <div style="text-align: center;">
                            <asp:CheckBox ID="chkUnique" runat="server"></asp:CheckBox>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtCount">
                    <Template>
                        <asp:TextBox ID="txtCount" runat="server" CssClass="textBoxes" Width="70"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtReset">
                    <Template>
                        <asp:DropDownList ID="ddlReset" runat="server" Width="101">
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, CountReached %>" Value="CountReached"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Daily %>" Value="Daily"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Weekly %>" Value="Weekly"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Monthly %>" Value="Monthly"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Quarterly %>" Value="Quarterly"></asp:ListItem>
                            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Yearly %>" Value="Yearly"></asp:ListItem>
                        </asp:DropDownList>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtEmail">
                    <Template>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxes" Width="205"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtStatus">
                    <Template>
                        <asp:TextBox ID="txtStatus" runat="server" CssClass="textBoxes" Width="208"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="UniqueTemplate">
                    ##if( DataItem.getMember("UniqueVisitor").get_value()==true ){"<span class='GlobalCssClass'>&nbsp;&nbsp;&nbsp;&nbsp;</span>"}
                    ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="EditCommandTemplate">
                    <a href="javascript:SaveRecord();">
                        <img src="/iapps_images/cm-icon-add.png" border="0" alt=saveAlt title=saveTitle /></a>
                    | <a href="javascript:CancelClicked();">
                        <img src="/iapps_images/cm-icon-delete.png" border="0" alt=cancelAlt
                            title=cancelTitle /></a>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdVisitorPageviewsSlider">
                    <div class="SliderPopup">
                        <h5>
                            ## DataItem.GetMember(0).Value ##</h5>
                        <p>
                            ## DataItem.GetMember(1).Value ##</p>
                        <p>
                            ## DataItem.GetMember(2).Value ##</p>
                        <p>
                            ## DataItem.GetMember(3).Value ##</p>
                        <p>
                            ## DataItem.GetMember(4).Value ##</p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdVisitorPageviews.PageCount) ##</span>
                            <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdVisitorPageviews.RecordCount) ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdVisitorPageviewsPagination">
                    ## GetGridPaginationInfo(grdVisitorPageviews) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ContextMenu EventHandler="grdVisitorPageviews_onContextMenu" />
                <ItemBeforeUpdate EventHandler="grdVisitorPageviews_onItemBeforeUpdate" />
                <CallbackError EventHandler="grdVisitorPageviews_onCallbackError" />
                <CallbackComplete EventHandler="grdVisitorPageviews_onCallbackComplete" />
            </ClientEvents>
        </ComponentArt:Grid>
        <ComponentArt:Menu SkinID="ContextMenu" ID="cmVisitorCountPageviews" runat="server">
            <Items>
                <ComponentArt:MenuItem ID="addNewVisitorCountAlert" runat="server" Text="<%$ Resources:GUIStrings, AddNewAlert %>" Look-LeftIconUrl="cm-icon-add.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="editVisitorCountAlert" runat="server" Text="<%$ Resources:GUIStrings, EditAlert %>" Look-LeftIconUrl="cm-icon-edit.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="deleteVisitorCountAlert" runat="server" Text="<%$ Resources:GUIStrings, DeleteAlert %>" Look-LeftIconUrl="cm-icon-delete.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="changeVisitorCountAlertStatus" runat="server" Text="<%$ Resources:GUIStrings, MakeActiveInactive %>">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="cmAlertVisitorCount_ItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
    </div>
</asp:Content>

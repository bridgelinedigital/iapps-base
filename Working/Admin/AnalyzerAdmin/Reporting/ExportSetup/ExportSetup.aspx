<%@ Page Language="C#" MasterPageFile="~/Reporting/ReportingMaster.master" EnableEventValidation="false"
    AutoEventWireup="true" StylesheetTheme="General" Inherits="Reporting_ExportSetup_ExportSetup"
    runat="server" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerReportingExportSetup %>"
    CodeBehind="ExportSetup.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var Page0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of1 %>'/>";//added by Adams
        var Item0of1 = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Item0of1 %>'/>";//added by Adams
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>";//added by Adams

        var item = "";
        var selectedAction;
        var removeEmptyRow = false;
        var isCancelled = false;
        var clientStatus = "";
        var Make = "";
        function grdExportSetup_onContextMenu(sender, eventArgs) {
            //debugger
            //This will check the session whether it is expired or not.
            CheckSession();
            item = eventArgs.get_item();
            grdExportSetup.select(item);
            if (grdExportSetup.EditingDirty == true) {
                cmExportSetup.Items(0).Visible = false;
                cmExportSetup.Items(1).Visible = false;
                cmExportSetup.Items(2).Visible = false;
                cmExportSetup.Items(3).Visible = false;
                cmExportSetup.Items(4).Visible = false;
                cmExportSetup.Items(5).Visible = false;
                cmExportSetup.Items(6).Visible = false;
            }
            else {
                cmExportSetup.Items(0).Visible = true;
                cmExportSetup.Items(1).Visible = true;
                cmExportSetup.Items(2).Visible = true;
                cmExportSetup.Items(3).Visible = true;
                cmExportSetup.Items(4).Visible = true;
                cmExportSetup.Items(5).Visible = true;
                cmExportSetup.Items(6).Visible = true;

                clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                if (clientStatus == "Active") {
                    cmExportSetup.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                }
                else {
                    cmExportSetup.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                }

                if (grdExportSetup.get_table().getRowCount() == 1) {
                    if (eventArgs.get_item()) {
                        if (eventArgs.get_item().GetMemberAt(8).Text == "") {
                            cmExportSetup.Items(1).Visible = false;
                            cmExportSetup.Items(2).Visible = false;
                            cmExportSetup.Items(3).Visible = false;
                            cmExportSetup.Items(4).Visible = false;
                            cmExportSetup.Items(5).Visible = false;
                            cmExportSetup.Items(6).Visible = false;
                        }
                        else {
                            cmExportSetup.Items(1).Visible = true;
                            cmExportSetup.Items(2).Visible = true;
                            cmExportSetup.Items(3).Visible = true;
                            cmExportSetup.Items(4).Visible = true;
                            cmExportSetup.Items(5).Visible = true;
                            cmExportSetup.Items(6).Visible = true;
                            clientStatus = eventArgs.get_item().GetMemberAt(4).Text
                            //Make = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Make %>'/>";
                            if (clientStatus == "Active") {
                                cmExportSetup.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeInactive %>'/>";
                            }
                            else {
                                cmExportSetup.Items(6).Text = "<asp:localize runat='server' text='<%$ Resources:JSMessages, MakeActive %>'/>";
                            }
                        }
                    }

                }
            }
            var evt = eventArgs.get_event();
            cmExportSetup.showContextMenuAtEvent(evt, eventArgs.get_item());
        }

        //function AsyncPageSubmitToWorkflow(sender, eventArgs)
        //{
        //    setTimeout("cmExportSetup_ItemSelect('"+sender+"','"+eventArgs+"');" ,0);
        //}

        /** Methods to set the pagination details **/
        function cmExportSetup_ItemSelect(sender, eventArgs) {
            //debugger
            //This will check the session whether it is expired or not.
            CheckSession();
            var selectedMenu = eventArgs.get_item().get_id();
            //Checking for empty row.
            var tempItem = null;
            if (grdExportSetup.get_table().getRowCount() == 1) {
                tempItem = grdExportSetup.get_table().getRow(0);
                if (tempItem.getMemberAt(8).Text == "" || tempItem.getMemberAt(8).Text == null) {
                    removeEmptyRow = true;
                }
                else {
                    removeEmptyRow = false;
                }
            }
            else {
                removeEmptyRow = false;
            }
            //end  

            switch (selectedMenu) {
                case "addNewExport":
                    if (removeEmptyRow == true) {
                        grdExportSetup.get_table().ClearData();
                    }
                    grdExportSetup.get_table().addEmptyRow(0);
                    //grdExportSetup.get_table().getRow(0).SetValue(5,'insert');            
                    item = grdExportSetup.get_table().getRow(0);
                    item.SetValue(5, 'insert', true);
                    grdExportSetup.edit(item);
                    selectedAction = "Insert";
                    break;
                case "editExport":
                    if (removeEmptyRow == false) {
                        item.SetValue(5, 'update', true);
                        grdExportSetup.edit(item);
                        selectedAction = "Edit";
                    }
                    break;
                case "deleteExport":

                    if (removeEmptyRow == false) {
                        var yesOrNo = window.confirm(GetMessage("DeleteConfirmation"));
                        if (yesOrNo) {

                            if (item) {
                                selectedAction = "delete";

                                grdExportSetup.edit(item);
                                item.SetValue(5, 'delete', false);
                                grdExportSetup.callback();
                            }
                        }
                    }
                    break;
                case "changeExportStatus":

                    if (removeEmptyRow == false) {
                        var yesOrNo = window.confirm(GetMessage("ExportStatusChangeConfirmation"));
                        if (yesOrNo) {
                            selectedAction = "changestatus";
                            grdExportSetup.edit(item);
                            item.SetValue(5, 'changestatus', false);
                            grdExportSetup.callback();
                        }
                    }
                    break;
            }
        }
        function CancelClicked() {
            //debugger
            //This will check the session whether it is expired or not.
            CheckSession();

            if (selectedAction == '<asp:localize runat="server" text="<%$ Resources:JSMessages, Insert %>"/>') {
                if (grdExportSetup.Data.length == 1 && (item.getMember(0).get_value() == null || item.getMember(0).get_value() == '')) {
                    grdExportSetup.get_table().ClearData();
                    grdExportSetup.editCancel();
                    item = null;
                }
                else {
                    grdExportSetup.deleteItem(item);
                    grdExportSetup.editCancel();
                    item = null;
                }
            }
            else {
                grdExportSetup.editCancel();
                item.SetValue(5, 'cancel', false);
                grdExportSetup.callback();
            }

            if (removeEmptyRow == true) {
                grdExportSetup.get_table().addEmptyRow(0);
            }
            isCancelled = true;
        }

        function report_OnSelect(sender, eventArgs) {

            var currentNode = eventArgs.get_node();
            if (currentNode) {
                var reportControl = document.getElementById(cmbReport);
                //note temp change made
                reportControl.text = currentNode.get_text();
                //reportControl.text=currentNode.get_value();
                //grdExportSetup.get_table().getRow(0).SetValue(6,currentNode.get_id())  
                item.SetValue(0, currentNode.get_text(), true);
                item.SetValue(6, currentNode.get_id(), true);
                item.SetValue(9, currentNode.get_value(), true);
                reportControl.value = currentNode.get_id();
                cmbReportTree.set_text(currentNode.get_text());
                cmbReportTree.value = currentNode.get_id();
                //debugger
                if (trvReport) {
                    //trvReport.CollapseAll();
                    //document.getElementById(treeReport).visible=false;
                    cmbReportTree.collapse();
                }

            }

        }

        function report_OnCheckChange(sender, eventArgs) {

            var currentNode = eventArgs.get_node();
            if (currentNode) {
                var reportControl = document.getElementById(cmbReport);
                //note temp change made
                reportControl.text = currentNode.get_text(); 
                item.SetValue(0, currentNode.get_text(), true);
                item.SetValue(6, currentNode.get_id(), true);
                item.SetValue(9, currentNode.get_value(), true);
                reportControl.value = currentNode.get_id();
            }

        }
        function cmbReportTree_OnExpand(sender, eventArgs) {
            trvReport.collapseAll();
            var comboControl = document.getElementById(cmbReport);
            if (comboControl.value != null || comboControl.value != "") {
            }
            var pickerTextbox2 = document.getElementById(cmbReport + "_DropDownContent");
            pickerTextbox2.onmouseleave = function() {
                cmbReportTree.collapse();
            }
        }
        function Type_OnChange(typeId) {
            var aList = document.getElementById(typeId);
            for (i = 0; i < aList.options.length; i++) {
                if (aList.options[i].selected)// = true;
                {

                    break;
                }
            }
        }


        function AddItemsToTypeDropDown(dropdownlist, TypeField, DataItem) {
            // debugger
            // to b done later
            var ddlControl = document.getElementById(dropdownlist)
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                //FillType(dropdownlist);
                ddlControl.disabled = false;
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Edit1 %>'/>") {
                //FillType(dropdownlist);

                //var dropDownListId = item.GetMember(TypeId).Value;

                for (i = 0; i < ddlControl.length; i++) {
                    if (ddlControl[i].text == DataItem.getMemberAt(TypeField).Value) {
                        ddlControl[i].selected = true;
                    }
                }
                ddlControl.disabled = false;
            }
        }

        function AddItemsToResetDropDown(dropdownlist, TypeField, DataItem) {

            // to b done later
            var ddlControl = document.getElementById(dropdownlist)
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                //FillType(dropdownlist);
                ddlControl.disabled = false;
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Edit1 %>'/>") {
                //FillType(dropdownlist);

                //var dropDownListId = item.GetMember(TypeId).Value;

                for (i = 0; i < ddlControl.length; i++) {
                    if (ddlControl[i].text == DataItem.getMemberAt(TypeField).Value) {
                        ddlControl[i].selected = true;
                    }
                }
                ddlControl.disabled = false;
            }
        }

        function FillType(dropdownlist) {
            var ddlControl = document.getElementById(dropdownlist);
            var myOption;
            var row, col;
            col = 0;

            if (ddlControl.length > 0) {
                ddlControl.innerHTML = "";
            }
            for (row = 0; row < templateArray.length; row++) {
                col = 0;
                myOption = new Option(templateArray[row][col + 1], templateArray[row][col]) ///document.createElement("Option");
                ddlControl.options[ddlControl.length] = myOption;
            }
        }
        function getTypeValue(control) {


            var ddlTypeControl = document.getElementById(control);
            var selectedType = "";
            if (ddlTypeControl) {
                //if(ddlTypeControl.selectedIndex)
                //{
                selectedType = ddlTypeControl[ddlTypeControl.selectedIndex].text
                //}    
            }

            return [selectedType, selectedType];
        }
        function getValue(control, DataField, DataItem) {
            //alert("get value textbox called");  
            var txtControl = document.getElementById(control);
            //alert(txtControl.Value);

            var email = txtControl.value;
            return [email, email];
        }
        function setValue(control, DataField, DataItem) {
            // alert("set value textbox called");  
            var value = item.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            txtControl.value = value;
            if (txtControl.value == "null") {
                txtControl.value = "";
            }
        }

        function FillTypeListBox() {
            //debugger
            var ddlControl = document.getElementById(dropdownlist);
            var myOption;
            var row;
            var myindex = ddlControl.selectedIndex;
            var SelTypeId = ddlControl.options[myindex].value;
            getTemplateValue(dropdownlist);
        }

        function getReportValue(control, control1, DataField, DataItem) {
            //debugger        
            var report = "";
            if (cmbReportTree != null) {
                if (cmbReportTree.get_text) {
                    report = cmbReportTree.get_text();
                }
                if (control != null) {
                    if (trvReport != null) {
                        trvReport.dispose();
                    }
                }
                if (control1 != null) {
                    if (cmbReportTree.dispose) {

                        cmbReportTree.dispose();
                    }
                }

            }
            return [report, report];
        }
        function setReportValue(control, control1, ReportNameField, ReportIdField, DataItem) {
            var treeControl = document.getElementById(control);
            var reportName = item.GetMember(ReportNameField).Value;
            var reportId = item.GetMember(ReportIdField).Value;
            var comboControl = document.getElementById(cmbReport);
            //comboControl.value = value;   
            if (reportName == null) {
                reportName = "";
            }
            if (reportId == null) {
                reportId = "";
            }
            //Setting report path
            var reportNode = trvReport.findNodeById(reportId)
            item.SetValue(0, reportName, true);
            if (reportNode != null) {
                item.SetValue(9, reportNode.get_value(), true);
            }

            if (cmbReportTree != null) {
                cmbReportTree.set_text(reportName);
            }
            comboControl.value = reportId;

            if (comboControl.value == null) {
                comboControl.value = "";
            }
        }

        //check and change
        function ValidationEmailFunction(cntrl, validationKey) {
            if (!AdminCallback.ValidateByType('Email', cntrl.value)) {
                alert(GetMessage(validationKey));
                return false;
            }
            else {
                return true;
            }

        }


        function grdExportSetup_itemBeforeUpdate(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();

            //alert("update called"); 
            var ctlReport = document.getElementById(cmbReport);
            var ctlEmail = document.getElementById(txtEmail);
            var ctlType = document.getElementById(dropdownTypelist);
            var ctlReset = document.getElementById(dropdownResetlist);
            var exportSetupReportId = item.getMember(8).get_value();
            var ReportId = item.getMember(6).get_value();
            var pageStatus = item.getMember(4).get_value();
            //if(ValidationEmailFunction(ctlEmail,"EmailNotValid")==false)
            //{
            //    return false;
            //}

            if (ValidationRequiredFunction(ctlReport.value, "ReportNameRequired") == false ||
        ValidationRequiredFunction(ctlType.value, "TypeNameRequired") == false ||
        ValidationRequiredFunction(ctlReset.value, "SendEveryRequired") == false) {
                return false;
            }
            if (ValidationRequiredFunction(ctlEmail.value, "EmailRequired") == false) {
                ctlEmail.focus();
                return false;
            }
            //    debugger
            //    if(ValidationSpecailCharacterFunction(ctlReport.Text,"SpecialCharacterValidation","Report Name")==false)
            //    {
            //        return false;
            //    }    
            //debugger

            var strEmail = ctlEmail.value.replace(/\s+/g, '');
            ctlEmail.value = strEmail;
            var strEmailId = new Array(50);
            strEmailId = strEmail.split(",");

            $(strEmailId).each(function (index, email) {
                if (ValidateEmailFunction(strEmailId[index]) == false) {
                    alert(GetMessage("EmailNotValid"));
                    ctlEmail.focus();
                    return false;
                }
            });

            var AlertMessage = setTimeout("AsyncgrdExportSetup_Callback('" + ReportId + "','" + exportSetupReportId + "','" + ctlReset.value + "','" + ctlEmail.value + "','" + ctlType.value + "','" + pageStatus + "','" + selectedAction + "');", 0);
            if (AlertMessage == "") {
                grdExportSetup.editComplete();
            }
            else {
                var ctlReportName = cmbReportTree.get_text();
                //alert("This matching for "+ ctlReportName +" is already reflected");
                isCancelled = false;
                //alert(AlertMessage);
            }

            //grdExportSetup_Callback
            //    if(selectedAction == "Insert")
            //    {       
            //        grdExportSetup.editComplete();        
            //    }
            //    else if(selectedAction == "Edit")
            //    {
            //        grdExportSetup.editComplete();        
            //    }
        }

        function AsyncgrdExportSetup_Callback(ReportId, exportSetupReportId, ctlReset, ctlEmail, ctlType, pageStatus, selectedAction) {
            var alertMessage = grdExportSetup_Callback(ReportId, exportSetupReportId, ctlReset, ctlEmail, ctlType, pageStatus, selectedAction);
            if (alertMessage == "") {
                grdExportSetup.editComplete();
            }
            else {
                var ctlReportName = cmbReportTree.get_text();
                isCancelled = false;
            }
        }
        function ExportSetup_Save(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();

            //alert("called");
            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                grdExportSetup.editComplete();
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Edit1 %>'/>") {
                grdExportSetup.editComplete();
                //eventArgs.set_cancel=true;
            }
        }

        function grdExportSetup_onCallbackError(sender, eventArgs) {
            //This will check the session whether it is expired or not.
            CheckSession();
            alert("callback error");
        }
        function grdExportSetup_onCallbackComplete(sender, eventArgs) {

            //debugger
            //This will check the session whether it is expired or not.
            CheckSession();

            if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Insert %>'/>") {
                grdExportSetup.editComplete();
                selectedAction = "";
                if (isCancelled != true) {
                    alert(GetMessage('ExportSetupSaved'));
                }
                else {
                    isCancelled = false;
                }
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Edit1 %>'/>") {
                if (grdExportSetup.EditingDirty == "false" || grdExportSetup.EditingDirty == "False" || grdExportSetup.EditingDirty == false) {
                    grdExportSetup.editComplete();
                    selectedAction = "";
                    if (isCancelled != true) {
                        alert(GetMessage('ExportSetupSaved'));
                    }
                    else {
                        isCancelled = false;
                    }
                }
            }
            else if (selectedAction == "<asp:localize runat='server' text='<%$ Resources:JSMessages, Delete %>'/>") {
                //        if(item)
                //        {
                //            grdExportSetup.deleteItem(item);            
                //            item = null;
                //        }
                if (grdExportSetup.get_table().getRowCount() <= 0 && grdExportSetup.PageCount == 0) {
                    grdExportSetup.get_table().addEmptyRow(0);
                }
                else if (grdExportSetup.get_table().getRowCount() <= 0 && grdExportSetup.PageCount > 0) {
                    grdExportSetup.previousPage();
                }
                grdExportSetup.editComplete();
                selectedAction = "";
                alert(GetMessage("ExportDeleteCompleted"));
            }
            else if (selectedAction == "changestatus") {
                //if(item.GetMemberAt(4).Value == "Active")
                // {             
                //    item.SetValue(4,'Inactive',true);
                //  }
                //  else 
                //  {              
                //    item.SetValue(4,'Active',true)
                // }
                item.SetValue(5, '', false);
                grdExportSetup.editComplete();
                selectedAction = "";
            }
            getReportValue(null, null, null, null);
            //alert("completed");
        }
        function grdExportSetup_beforeCallback(sender, eventArgs) {
            //debugger
            if (cmbReportTree) {
                //debugger

                if (trvReport != null) {
                    trvReport.dispose();
                }
                if (cmbReportTree.dispose) {
                    cmbReportTree.dispose();
                }

            }
        }
        function grdExportSetup_onSortChange(sender, eventArgs) {
            //debugger
            if (grdExportSetup.EditingDirty == true) {
                grdExportSetup.editCancel();
                isCancelled = true;
            }

        }

    </script>
</asp:Content>
<asp:Content ID="contentExportSetup" ContentPlaceHolderID="reportingContentHolder"
    runat="Server">
<div class="export-setup">
    <p style="margin-bottom: 5px">
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, EditExcelandPDFreportsyouhavesetupforrecurringexport %>" /></p>
    <ComponentArt:Grid SkinID="Default" ID="grdExportSetup" runat="server" RunningMode="Callback"
        EmptyGridText="<%$ Resources:GUIStrings, Noexportsetup %>" AutoPostBackOnSelect="false"
        Width="100%" PageSize="10" SliderPopupClientTemplateId="grdExportSetupSlider"
        PagerInfoClientTemplateId="grdExportSetupPagination" AllowEditing="true" AutoCallBackOnInsert="false"
        AutoCallBackOnCheckChanged="false" AutoCallBackOnUpdate="true" AutoCallBackOnDelete="false"
        EditOnClickSelectedItem="false" CallbackReloadTemplateScripts="true" CallbackReloadTemplates="true">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                EditCellCssClass="EditDataCell" HeadingCellActiveCssClass="HeadingCellActive"
                HeadingRowCssClass="HeadingRow" EditFieldCssClass="EditDataField" HeadingTextCssClass="HeadingCellText"
                SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
                AlternatingRowCssClass="AlternateDataRow" AllowSorting="true">
                <Columns>
                    <ComponentArt:GridColumn Width="300" runat="server" HeadingText="<%$ Resources:GUIStrings, Report %>"
                        DataField="ReportName" AllowSorting="True" SortedDataCellCssClass="SortedDataCell"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" EditCellServerTemplateId="svtReport"
                        EditControlType="Custom" DataCellCssClass="FirstDataCell" HeadingCellCssClass="FirstHeadingCell" />
                    <ComponentArt:GridColumn Width="150" AllowSorting="True" runat="server" HeadingText="<%$ Resources:GUIStrings, Type %>"
                        DataField="FormatType" IsSearchable="true" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" EditCellServerTemplateId="svtType"
                        EditControlType="Custom" />
                    <ComponentArt:GridColumn Width="150" AllowSorting="True" runat="server" HeadingText="<%$ Resources:GUIStrings, SendEvery %>"
                        DataField="sendEvery" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" EditCellServerTemplateId="svtSendEvery" EditControlType="Custom" />
                    <ComponentArt:GridColumn Width="410" AllowSorting="True" runat="server" HeadingText="<%$ Resources:GUIStrings, Email %>"
                        DataField="ExternalEmailID" SortedDataCellCssClass="SortedDataCell" AllowReordering="false" TextWrap="true"
                        EditCellServerTemplateId="svtEmail" FixedWidth="true" EditControlType="Custom" />
                    <ComponentArt:GridColumn Width="100" AllowSorting="True" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>"
                        DataField="Status" SortedDataCellCssClass="SortedDataCell" AllowReordering="false"
                        FixedWidth="true" AllowEditing="False" />
                    <ComponentArt:GridColumn Width="240" runat="server" HeadingText="<%$ Resources:GUIStrings, ActionColumn %>"
                        AllowEditing="True" IsSearchable="true" AllowReordering="false" FixedWidth="true"
                        Visible="false" AllowSorting="False" />
                    <ComponentArt:GridColumn Width="240" runat="server" HeadingText="<%$ Resources:GUIStrings, ReportId %>"
                        AllowEditing="True" HeadingCellCssClass="FirstHeadingCell" DataField="ReportId"
                        DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" IsSearchable="true"
                        AllowReordering="false" FixedWidth="true" Visible="false" AllowSorting="False" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        AllowSorting="False" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                        Align="left" EditCellServerTemplateId="svtActions" EditControlType="Custom" Width="75" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" AllowSorting="False" />
                    <ComponentArt:GridColumn HeadingText="ReportPath" Visible="false" AllowSorting="False" />
                    <ComponentArt:GridColumn DataField="SubscriptionId" Visible="false" AllowSorting="False" />
                    <ComponentArt:GridColumn DataField="ScheduleId" Visible="false" AllowSorting="False" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="svtReport">
                <Template>
                    <ComponentArt:ComboBox ID="cmbReportTree" SkinID="Default" runat="server" Width="190"
                        DropDownHeight="297" DropDownWidth="300" AutoComplete="false" AutoHighlight="false">
                        <DropDownContent>
                            <ComponentArt:TreeView ID="trvReport" SkinID="Default" Height="293" Width="298" runat="server"
                                AutoPostBackOnSelect="false">
                                <ClientEvents>
                                    <NodeCheckChange EventHandler="report_OnCheckChange" />
                                    <NodeSelect EventHandler="report_OnSelect" />
                                </ClientEvents>
                                <CustomAttributeMappings>
                                    <ComponentArt:CustomAttributeMapping From="Title" To="Text" />
                                    <ComponentArt:CustomAttributeMapping From="Title" To="Value" />
                                    <ComponentArt:CustomAttributeMapping From="NumberOfFiles" To="StorageIndex" />
                                </CustomAttributeMappings>
                            </ComponentArt:TreeView>
                        </DropDownContent>
                        <ClientEvents>
                            <Expand EventHandler="cmbReportTree_OnExpand" />
                        </ClientEvents>
                    </ComponentArt:ComboBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtType">
                <Template>
                    <asp:DropDownList ID="ddlType" runat="server" Width="80">
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Excel %>" Value="1"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, PDF %>" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtSendEvery">
                <Template>
                    <asp:DropDownList ID="ddlReset" runat="server" Width="110">
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Daily %>" Value="Daily"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, EverySunday %>" Value="EverySunday"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, EveryMonday %>" Value="EveryMonday"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, EveryTuesday %>" Value="EveryTuesday"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, EveryWednesday %>" Value="EveryWednesday"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, EveryThursday %>" Value="EveryThursday"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, EveryFriday %>" Value="EveryFriday"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, EverySaturday %>" Value="EverySaturday"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, stofMonth %>" Value="FirstofMonth"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, thofMonth %>" Value="FifteenofMonth"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, LastDayofMonth %>" Value="LastDayOfMonth"></asp:ListItem>
                        <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, stDayofQuarter %>" Value="firstDayOfQuarter"></asp:ListItem>
                    </asp:DropDownList>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtEmail">
                <Template>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxes" Width="190"></asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtActions">
                <Template>
                    <table>
                        <tr>
                            <td>
                                <asp:Image ID="addPageviewAlert" runat="server" AlternateText="<%$ Resources:GUIStrings, Add %>"
                                    ToolTip="<%$ Resources:GUIStrings, Add %>" onclick="grdExportSetup_itemBeforeUpdate();"
                                    ImageUrl="/iapps_images/cm-icon-add.png"></asp:Image>
                            </td>
                            <td>
                                <asp:Image ID="cancelPageviewAlert" runat="server" AlternateText="<%$ Resources:GUIStrings, Cancel %>"
                                    ToolTip="<%$ Resources:GUIStrings, Cancel %>" onclick="CancelClicked();" ImageUrl="/iapps_images/cm-icon-delete.png">
                                </asp:Image>
                            </td>
                        </tr>
                    </table>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="grdExportSetupSlider">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMember(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMember(1).Value ##</p>
                    <p>
                        ## DataItem.GetMember(2).Value ##</p>
                    <p>
                        ## DataItem.GetMember(3).Value ##</p>
                    <p>
                        ## DataItem.GetMember(4).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0of1, DataItem.PageIndex + 1, grdExportSetup.PageCount)
                            ##</span> <span class="items">## stringformat(Item0of1, DataItem.Index + 1, grdExportSetup.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdExportSetupPagination">
                ## GetGridPaginationInfo(grdExportSetup) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ClientEvents>
            <ContextMenu EventHandler="grdExportSetup_onContextMenu" />
            <CallbackError EventHandler="grdExportSetup_onCallbackError" />
            <CallbackComplete EventHandler="grdExportSetup_onCallbackComplete" />
            <BeforeCallback EventHandler="grdExportSetup_beforeCallback" />
            <SortChange EventHandler="grdExportSetup_onSortChange" />
        </ClientEvents>
    </ComponentArt:Grid>
    <asp:XmlDataSource ID="xmlDataSource" runat="server"></asp:XmlDataSource>
    <ComponentArt:Menu SkinID="ContextMenu" Width="160" ID="cmExportSetup" runat="server">
        <Items>
            <ComponentArt:MenuItem ID="addNewExport" runat="server" Text="<%$ Resources:GUIStrings, AddNewExport %>"
                Look-LeftIconUrl="cm-icon-add.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="editExport" runat="server" Text="<%$ Resources:GUIStrings, EditExport %>"
                Look-LeftIconUrl="cm-icon-edit.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="deleteExport" runat="server" Text="<%$ Resources:GUIStrings, DeleteExport %>"
                Look-LeftIconUrl="cm-icon-delete.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="changeExportStatus" runat="server" Text="<%$ Resources:GUIStrings, MakeActiveInactive %>">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="cmExportSetup_ItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</div>
</asp:Content>

﻿iAPPS_Tooltip = {
    Initialize: function (attributeForTooltip, attributeForTooltipText, tooltipPosition) {
        var tooltipTopHtml = '<div id="iappsTooltip" class="iapps-tooltip">';
        var tooltipBottomHtml = '</div>';
        //Select all anchor tag with rel set to tooltip
        $('*[' + attributeForTooltip + '=tooltip]').mouseover(function (e) {

            //Grab the title attribute's value and assign it to a variable
            var tip = $(this).attr(attributeForTooltipText);
            if (typeof tip != "undefined" && tip != "") {
                tip = escape(tip);
                //Remove the title attribute's to avoid the native tooltip from the browser
                $(this).attr(attributeForTooltipText, '');

                //Append the tooltip template and its value
                $('body').append(tooltipTopHtml + unescape(tip) + tooltipBottomHtml);

                //position the tooltip
                iAPPS_Tooltip.Position($(this), tooltipPosition);

                //Show the tooltip with faceIn effect
                $('#iappsTooltip').fadeIn('500');
                $('#iappsTooltip').fadeTo('10', 1.0);
            }

        }).mouseout(function () {

            //Put back the title attribute's value
            $(this).attr(attributeForTooltipText, $('#iappsTooltip').html());

            //Remove the appended tooltip template
            $('body').children('div#iappsTooltip').remove();

        });
    },


    TitleReplacement: function () {
        var tooltipTopHtml = '<div id="iappsTooltip" class="iapps-tooltip">';
        var tooltipBottomHtml = '</div>';
        $('*[title]').each(function (index) {
            var isFormElement = $(this).is('input') || $(this).is('select') || $(this).is('textarea') ? true : false;
            if (isFormElement) {
                $(this).focusin(function (e) {
                    //Grab the title attribute's value and assign it to a variable
                    var tip = $(this).attr('title');
                    tip = escape(tip);
                    //Remove the title attribute's to avoid the native tooltip from the browser
                    $(this).attr('title', '');

                    //Append the tooltip template and its value
                    $('body').append(tooltipTopHtml + unescape(tip) + tooltipBottomHtml);

                    //position the tooltip
                    iAPPS_Tooltip.Position($(this), 'top');

                    //Show the tooltip with faceIn effect
                    $('#iappsTooltip').fadeIn('500');
                    $('#iappsTooltip').fadeTo('10', 1.0);
                }).focusout(function () {

                    //Put back the title attribute's value
                    $(this).attr('title', $('#iappsTooltip').html());

                    //Remove the appended tooltip template
                    $('body').children('div#iappsTooltip').remove();
                });
            }
            else {
                //Select all anchor tag with rel set to tooltip
                $(this).mouseover(function (e) {
                    //Grab the title attribute's value and assign it to a variable
                    var tip = $(this).attr('title');
                    tip = escape(tip);
                    //Remove the title attribute's to avoid the native tooltip from the browser
                    $(this).attr('title', '');

                    //Append the tooltip template and its value
                    $(this).append(tooltipTopHtml + unescape(tip) + tooltipBottomHtml);

                    //position the tooltip
                    iAPPS_Tooltip.Position($(this), 'top');

                    //Show the tooltip with faceIn effect
                    $('#iappsTooltip').fadeIn('500');
                    $('#iappsTooltip').fadeTo('10', 1.0);

                }).mouseout(function () {

                    //Put back the title attribute's value
                    $(this).attr('title', $('#iappsTooltip').html());

                    //Remove the appended tooltip template
                    $(this).children('div#iappsTooltip').remove();

                }).css({
                    'text-decoration': 'none',
                    'border-bottom': 'dashed 1px green',
                    'cursor': 'pointer'
                });
            }
        });
    },

    Position: function (element, tooltipPosition) {
        //tooltip object
        var objTooltip = $('#iappsTooltip');
        //width of tooltip
        var tooltipWidth = $(objTooltip).outerWidth();
        //height of tooltip
        var tooltipHeight = $(objTooltip).outerHeight();
        var elementPosition = $(element).offset();
        //width of the element for which tooltip needs to be displayed
        var elementWidth = $(element).outerWidth();
        //height of the element for which tooltip needs to be displayed
        var elementHeight = $(element).outerHeight();
        var screenWidth = screen.width;
        var tooltipLeft = 0;
        var tooltipTop = 0;
        var isFormElement = $(element).is('input') || $(element).is('select') || $(element).is('textarea') ? true : false;
        if (isFormElement) {
            tooltipLeft = elementPosition.left;
            tooltipTop = elementPosition.top - tooltipHeight;
            tooltipWidth = ($(objTooltip).outerWidth() < $(element).outerWidth() ? ($(element).outerWidth() - ($(objTooltip).outerWidth() - $(objTooltip).width())) : $(objTooltip).width());
            $(objTooltip).css('width', tooltipWidth + 'px');
        }
        else {
            switch (tooltipPosition.toLowerCase()) {
                case 'top':
                    tooltipLeft = (elementPosition.left + (elementWidth / 2)) - (tooltipWidth / 2);
                    tooltipLeft = tooltipLeft < 0 ? 0 : tooltipLeft;
                    tooltipTop = (elementPosition.top - tooltipHeight - 10);
                    tooltipTop = tooltipTop < 0 ? (elementPosition.top + elementHeight + 10) : tooltipTop;
                    break;
                case 'right':
                    tooltipLeft = (elementPosition.left + elementWidth + tooltipWidth) > $(window).width() ? elementPosition.left - tooltipWidth - 10 : elementPosition.left + elementWidth + 10;
                    tooltipTop = tooltipHeight < elementHeight ? elementPosition.top + (elementHeight / 2) : elementPosition.top;
                    break;
                case 'bottom':
                    tooltipLeft = (elementPosition.left + (elementWidth / 2)) - (tooltipWidth / 2);
                    tooltipTop = elementPosition.top + elementHeight + 10;
                    tooltipTop = (tooltipTop + tooltipHeight) > $(window).height() ? (elementPosition.top - tooltipHeight - 10) : tooltipTop;
                    break;
                case 'left':
                    tooltipLeft = elementPosition.left - tooltipWidth < 0 ? elementPosition.left + elementWidth + 10 : elementPosition.left - tooltipWidth - 10;
                    tooltipTop = tooltipHeight < elementHeight ? elementPosition.top + (elementHeight / 2) : elementPosition.top;
                    break;
            }
        }
        $(objTooltip).css({
            'left': tooltipLeft + 'px',
            'top': tooltipTop + 'px'
        });
    }
};
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentMethodListPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.PaymentMethodListPart" %>

<asp:LinkButton ID="lbtnUpdatePrimary" runat="server" Visible="false" OnClick="lbtnUpdatePrimary_Click" />
<asp:Repeater runat="server" ID="paymentMethods">
    <HeaderTemplate>
        <div class="infoAction">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="infoAction-item">
            <div class="infoAction-check">
                <input type="radio" id="ra0<%#Container.ItemIndex.ToString() %>" name="radPaymentMethodId" 
                    onchange="<%# Page.ClientScript.GetPostBackEventReference(lbtnUpdatePrimary, ((Bridgeline.iAPPS.Pro.API.Commerce.Payment.CreditCardHelper)Container.DataItem).SavedCardId.ToString()) %>"
                    <%# ((Bridgeline.iAPPS.Pro.API.Commerce.Payment.CreditCardHelper)Container.DataItem).IsPrimary ? "checked" : ""%> />
                <label for="ra0<%#Container.ItemIndex.ToString() %>">Default</label>
            </div>
            <div class="infoAction-title"> <%# ((Bridgeline.iAPPS.Pro.API.Commerce.Payment.CreditCardHelper)Container.DataItem).Nickname %> </div>
            <asp:PlaceHolder runat="server" Visible="true" ID="phNonStripe" >
                <div class="infoAction-info"><%# ((Bridgeline.iAPPS.Pro.API.Commerce.Payment.CreditCardHelper)Container.DataItem).GetCardType() %> ending in <%# ((Bridgeline.iAPPS.Pro.API.Commerce.Payment.CreditCardHelper)Container.DataItem).Last4Digits %></div>
            </asp:PlaceHolder>
            <asp:LinkButton CssClass="infoAction-action infoAction-action--edit" runat="server" CausesValidation="false" CommandArgument="<%#((Bridgeline.iAPPS.Pro.API.Commerce.Payment.CreditCardHelper)Container.DataItem).SavedCardId%>"
                ID="editCustomerPaymentMethod" OnClick="editPaymentMethod_Click"> Edit </asp:LinkButton>
            <asp:LinkButton runat="server" CausesValidation="false" CssClass="infoAction-action infoAction-action--remove" ID="delete" CommandArgument="<%#((Bridgeline.iAPPS.Pro.API.Commerce.Payment.CreditCardHelper)Container.DataItem).SavedCardId %>" OnClick="delete_Click" OnClientClick="return confirm('Are you sure you want to delete this payment method?');"> Remove </asp:LinkButton>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>


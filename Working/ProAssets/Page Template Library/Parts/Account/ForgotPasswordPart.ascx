﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPasswordPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.ForgotPasswordPart" %>

<asp:Panel runat="server" ID="passwordPopup" CssClass="popup mfp-hide" DefaultButton="btnResetPassword" ClientIDMode="Static">
    <asp:UpdatePanel runat="server" ID="upForgotPassword">
        <ContentTemplate>
            <asp:PlaceHolder ID="error" Visible="false" runat="server">
                <p class="alert icon-attention">Email address not found</p>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="success" Visible="false" runat="server">
                <div class="alert icon-check green">Reset complete, your new password has been sent to your email</div>
            </asp:PlaceHolder>

            <p>If you have forgotten your password, enter the email address you signed up with below and your login information will be emailed to you immediately.</p>

            <div class="inlineLabel">
                <asp:Label runat="server" ID="lblEmail" AssociatedControlID="txtEmail">Email Address</asp:Label>
                <input runat="server" id="txtEmail" type="email" required />
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Email is required" CssClass="form-error"></asp:RequiredFieldValidator>
            </div>
            <p class="h-textRight h-overline"><asp:LinkButton runat="server" ID="btnResetPassword" class="btn">Reset Password</asp:LinkButton></p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderListingPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.OrderListingPart" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>

<asp:PlaceHolder ID="phNoOrders" runat="server" Visible="false">
    <p>You currently have no orders</p>
</asp:PlaceHolder>

<asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_ItemDataBound">
    <HeaderTemplate>
        <div class="infoAction">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="infoAction-item">
            <div id="orderNumber<%#Container.ItemIndex %>" class="infoAction-title">#<%# Eval("OrderNumber") %></div>
            <div class="infoAction-info"><%# Eval("OrderDate") %></div> <%--M/d/yyyy h:mm tt--%>
            <div class="infoAction-info"><%# Eval("GrandTotal", "{0:c}") %></div>
            <div class="infoAction-info">Status: <%# Eval("OrderStatus") %></div>
            <asp:LinkButton runat="server" CssClass="infoAction-action infoAction-action--details" id="lbtnOrderDetail" OnClick="orderDetail_Click" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "ID") %>'> Details </asp:LinkButton>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileNav.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.ProfileNav" %>

<h1>My Profile</h1>
<nav class="navHorizontal">
    <CLControls:CLHierarchicalNav ID="clnavProfile" runat="server" SiteMapProvider="ProfileNav"></CLControls:CLHierarchicalNav>
</nav>

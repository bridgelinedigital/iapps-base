﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WishlistListPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.WishlistListPart" %>

<asp:Repeater runat="server" ID="rptWishlists">
    <HeaderTemplate>
        <div class="infoAction">
    </HeaderTemplate>

    <ItemTemplate>
        <div class="infoAction-item">
            <div class="infoAction-title"><%#  ((Bridgeline.FW.Commerce.Orders.WishList)Container.DataItem).Title%></div>
            <div class="infoAction-info"><%# ((Bridgeline.FW.Commerce.Orders.WishList)Container.DataItem).Description%></div>
            <div class="infoAction-info">Updated: <%# ((Bridgeline.FW.Commerce.Orders.WishList)Container.DataItem).CreatedDate.ToShortDateString()%></div>
            <div class="infoAction-info"><%# ((Bridgeline.FW.Commerce.Orders.WishList)Container.DataItem).Items.Count.ToString()%> item<%# ((Bridgeline.FW.Commerce.Orders.WishList)Container.DataItem).Items.Count > 1 ? "s" : "" %></div>

            <asp:LinkButton CssClass="infoAction-action infoAction-action--edit" runat="server" CausesValidation="false" CommandArgument="<%#((Bridgeline.FW.Commerce.Orders.WishList)Container.DataItem).Id%>"
                ID="lbtnEditWishlist" OnClick="lbtnEditWishlist_Click"> Edit </asp:LinkButton>
            <asp:LinkButton runat="server" CausesValidation="false" CssClass="infoAction-action infoAction-action--remove" ID="lbtnDeleteWishlist" CommandArgument="<%#((Bridgeline.FW.Commerce.Orders.WishList)Container.DataItem).Id %>" OnClick="lbtnDeleteWishlist_Click"> Remove </asp:LinkButton>
        </div>
    </ItemTemplate>

    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>

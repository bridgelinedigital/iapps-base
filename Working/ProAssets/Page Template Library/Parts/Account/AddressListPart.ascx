﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressListPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.AddressListPart" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>

<asp:LinkButton ID="lbtnUpdatePrimary" runat="server" Visible="false" OnClick="lbtnUpdatePrimary_Click" />

<asp:Repeater runat="server" ID="addresses">
    <HeaderTemplate>
        <div class="infoAction">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="infoAction-item">
            <div class="infoAction-check">
                <input type="radio" id="ra0<%#Container.ItemIndex.ToString() %>" name="radAddressId" 
                    onchange="<%# Page.ClientScript.GetPostBackEventReference(lbtnUpdatePrimary, ((CustomerAddressModel)Container.DataItem).Id.ToString()) %>"
                    <%# ((CustomerAddressModel)Container.DataItem).IsPrimary ? "checked" : ""%> />
                <label for="ra0<%#Container.ItemIndex.ToString() %>">Default</label>
            </div>
            <!--/.infoAction-check-->
            <div class="infoAction-title"> <%# ((CustomerAddressModel)Container.DataItem).NickName%> </div>
            <!--/.infoAction-title-->
            <div class="infoAction-info"><%# ((CustomerAddressModel)Container.DataItem).Address.FirstName%> <%# ((CustomerAddressModel)Container.DataItem).Address.LastName%></div>
            <div class="infoAction-info"><%# ((CustomerAddressModel)Container.DataItem).Address.Phone%></div>
            <div class="infoAction-info">
                <%# ((CustomerAddressModel)Container.DataItem).Address.AddressLine1%><br />
                <%# !string.IsNullOrWhiteSpace(((CustomerAddressModel)Container.DataItem).Address.AddressLine2) ? ((CustomerAddressModel)Container.DataItem).Address.AddressLine2 + "<BR>" : string.Empty %>
                <%# ((CustomerAddressModel)Container.DataItem).Address.City%> <%# ((CustomerAddressModel)Container.DataItem).Address.State != null ? ", " + ((CustomerAddressModel)Container.DataItem).Address.State.StateCode : "" %> <%# ((CustomerAddressModel)Container.DataItem).Address.Zip%> <%# ((CustomerAddressModel)Container.DataItem).Address.Country.CountryCode%>
            </div>
            <!--/.infoAction-info-->
            <asp:LinkButton CssClass="infoAction-action infoAction-action--edit" runat="server" CausesValidation="false" CommandArgument="<%#((CustomerAddressModel)Container.DataItem).Id%>"
                ID="editAddress" OnClick="editAddress_Click"> Edit </asp:LinkButton>
            <!--/.infoAction-action-->
            <asp:LinkButton runat="server" CausesValidation="false" CssClass="infoAction-action infoAction-action--remove" ID="delete" CommandArgument="<%#((CustomerAddressModel)Container.DataItem).Id %>" OnClick="delete_Click" OnClientClick="return confirm('Are you sure you want to delete this address?');"> Remove </asp:LinkButton>
            <!--/.infoAction-action-->
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>

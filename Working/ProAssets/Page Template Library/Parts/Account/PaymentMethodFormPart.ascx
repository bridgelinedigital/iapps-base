﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentMethodFormPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.PaymentMethodFormPart" %>

<div class="row row--XTight">
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblNickname" AssociatedControlID="txtNickname">Payment Method Name</asp:Label>
        <input runat="server" id="txtNickname" type="text" maxlength="25" required />
        <p class="formNote">e.g. "My Bank Card" or "My Mastercard"</p>
        <asp:RequiredFieldValidator ID="rfvNickName" runat="server" ControlToValidate="txtNickname" Display="Dynamic" ErrorMessage="Payment Method Name is required" CssClass="form-error"></asp:RequiredFieldValidator>
        <asp:customvalidator ID="cvalNickname" Display="Dynamic" ErrorMessage="A unique Payment Method Name is required" CssClass="form-error" OnServerValidate="cvalNickname_ServerValidate" runat="server" />
    </div>
    <!--/.column-->
    <div class="column">
        <div class="sideToSideMed h-pushBottom">
            <h2 class="h-textLg">Credit card Info</h2>
            <!--/.populate this list dynamically based on which credit cards are accepted by this client-->
            <ul class="list--horizontal h-textXLg">
                <li id="liVisaIcon" class="icon-visa" runat="server"></li>
                <li id="liMasterCardIcon" class="icon-mastercard" runat="server"></li>
                <li id="liDiscoverIcon" class="icon-discover" runat="server"></li>
                <li id="liAmexIcon" class="icon-amex" runat="server"></li>
            </ul>
        </div>
        <!--/.h-stacksToSidesMed-->
    </div>
    <!--/.column-->
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblCardNumber" AssociatedControlID="txtCardNumber">Card Number</asp:Label>
        <input runat="server" id="txtCardNumber" maxlength="25" type="text" required ClientIDMode="Static" />
        <asp:RegularExpressionValidator ID="regCardNumber" runat="server" ControlToValidate="txtCardNumber" ValidationExpression="(\d{6}[-\s]?\d{12})|(\d{4}[-\s]?\d{4}[-\s]?\d{4}[-\s]?\d{4})" ErrorMessage="Please enter a valid Card Number" Display="Dynamic" CssClass="form-error"/>
        <asp:RequiredFieldValidator ID="rfvCardNumber" runat="server" ControlToValidate="txtCardNumber" Display="Dynamic" ErrorMessage="Card Number is required" CssClass="form-error"></asp:RequiredFieldValidator>
        <asp:customvalidator runat="server" ID="cvalLuhnValidation" Display="Dynamic" ErrorMessage="Please enter a valid Card Number" CssClass="form-error" ClientValidationFunction="ccNumberValidation" OnServerValidate="cvalLuhnValidation_ServerValidate"></asp:customvalidator>
        <asp:customvalidator runat="server" ID="cvalCreditCardTypeValidation" Display="Dynamic" ErrorMessage="" CssClass="form-error" ClientValidationFunction="ccTypeValidation" OnServerValidate="cvalCreditCardTypeValidation_ServerValidate" ControlToValidate="txtCardNumber" />
    </div>
    <!--/.column-->
    <div class="column inlineLabel med-12">
        <asp:Label runat="server" ID="lblExpMonth" AssociatedControlID="ddlExpMonth">Expiration Month</asp:Label>
        <asp:DropDownList runat="server" ID="ddlExpMonth" required AppendDataBoundItems="true" ClientIDMode="Static">
            <asp:ListItem Value="Expiration Month">Expiration Month</asp:ListItem>
            <asp:ListItem Text="01" Value="01" />
            <asp:ListItem Text="02" Value="02" />
            <asp:ListItem Text="03" Value="03" />
            <asp:ListItem Text="04" Value="04" />
            <asp:ListItem Text="05" Value="05" />
            <asp:ListItem Text="06" Value="06" />
            <asp:ListItem Text="07" Value="07" />
            <asp:ListItem Text="08" Value="08" />
            <asp:ListItem Text="09" Value="09" />
            <asp:ListItem Text="10" Value="10" />
            <asp:ListItem Text="11" Value="11" />
            <asp:ListItem Text="12" Value="12" />
        </asp:DropDownList>
        <asp:CustomValidator runat="server" ID="cvalExpireMonth" ControlToValidate="ddlExpMonth" ClientValidationFunction="expireDateValidation" Display="Dynamic" ErrorMessage="You have entered an invalid expiration date" CssClass="form-error"/>
        <asp:RequiredFieldValidator ID="rfvExpMonth" runat="server" ControlToValidate="ddlExpMonth" ErrorMessage="Expiration Month is required" InitialValue="Expiration Month" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <!--/.column-->
    <div class="column inlineLabel med-12">
        <asp:Label runat="server" ID="lblExpYear" AssociatedControlID="ddlExpYear">Expiration Year</asp:Label>
        <asp:DropDownList ID="ddlExpYear" runat="server" required CssClass="watermark" AppendDataBoundItems="true" ClientIDMode="Static">
            <asp:ListItem Value="Expiration Year">Expiration Year</asp:ListItem>
        </asp:DropDownList>
        <asp:CustomValidator runat="server" ID="cvalExpireDate" ControlToValidate="ddlExpYear" ClientValidationFunction="expireDateValidation" Display="Dynamic" ErrorMessage="You have entered an invalid expiration date" CssClass="form-error"/>
        <asp:RequiredFieldValidator ID="rfvExpYear" runat="server" ControlToValidate="ddlExpYear" ErrorMessage="Expiration Year is required" InitialValue="Expiration Year" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <!--/.column-->
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblNameOnCard" AssociatedControlID="txtNameOnCard">Name on Card</asp:Label>
        <input runat="server" id="txtNameOnCard" type="text" required />
        <asp:RequiredFieldValidator ID="rfvNameOnCard" runat="server" ControlToValidate="txtNameOnCard" Display="Dynamic" ErrorMessage="Name On Card is required" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <!--/.column-->
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblSecurityCode" AssociatedControlID="txtSecurityCode">Security Code</asp:Label>
        <input runat="server" id="txtSecurityCode" type="text" required ClientIDMode="Static" />
        <asp:RegularExpressionValidator ID="regSecurityCode" runat="server" ControlToValidate="txtSecurityCode" ValidationExpression="\d+" ErrorMessage="Only numeric values allowed" Display="Dynamic" CssClass="form-error"/>
        <asp:CustomValidator runat="server" ID="cvalSecurityCode" ControlToValidate="txtSecurityCode" ClientValidationFunction="securityCodeValidation" OnServerValidate="cvalSecurityCodeValidation_ServerValidate" Display="Dynamic" ErrorMessage="Valid Security Code is required" CssClass="form-error"/>
        <asp:RequiredFieldValidator ID="rfvSecurityCode" runat="server" ControlToValidate="txtSecurityCode" Display="Dynamic" ErrorMessage="Security Code is required" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <!--/.column-->
    <div class="column">
        <h2 class="h-textLg">Billing Address</h2>
    </div>
    <!--/.column-->
    <div class="column inlineLabel lg-16">
        <asp:Label runat="server" ID="lblStreetAddress" AssociatedControlID="txtStreetAddress">Street Address</asp:Label>
        <input runat="server" id="txtStreetAddress" type="text" maxlength="255" required />
        <asp:RequiredFieldValidator ID="rfvStreetAddress" runat="server" ControlToValidate="txtStreetAddress" ErrorMessage="Street address is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <!--/.column-->
    <div class="column inlineLabel lg-8">
        <asp:Label runat="server" ID="lblBldgAptNum" AssociatedControlID="txtBldgAptNum">Bldg/Apt # (optional)</asp:Label>
        <input runat="server" id="txtBldgAptNum" type="text" maxlength="255" />
    </div>
    <!--/.column-->
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblZip" AssociatedControlID="txtZip">Zip/Postal Code</asp:Label>
        <input runat="server" id="txtZip" type="text" maxlength="50" required />
        <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="Postal code is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="regZip" runat="server" ControlToValidate="txtZip" ValidationExpression="(^\d{5}(-\d{4})?$)|(^[AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$)" ErrorMessage="Please enter a valid postal code" Display="Dynamic" CssClass="form-error"></asp:RegularExpressionValidator>
    </div>
    <!--/.column-->
    <asp:UpdatePanel runat="server" ID="upCountryState" ChildrenAsTriggers="true">
        <ContentTemplate>
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblCountry" AssociatedControlID="ddlCountry">Country</asp:Label>
            <asp:DropDownList ID="ddlCountry" runat="server" required AutoPostBack="False"/>
            <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry" ErrorMessage="Country is required" InitialValue="" Display="Dynamic" CssClass="form-error"/>
        </div>
        <!--/.column-->
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblState" AssociatedControlID="ddlState">State</asp:Label>
            <asp:DropDownList ID="ddlState" runat="server" required CssClass="watermark" AppendDataBoundItems="true">
                <asp:ListItem Value="State">State</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="ddlState" ErrorMessage="State is required" InitialValue="State" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblCity" AssociatedControlID="txtCity">City</asp:Label>
        <input runat="server" id="txtCity" type="text" maxlength="255" required />
        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <!--/.column-->
    <!--/.column-->
</div>
<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        RegisterCountryDropdownEvents();
    });

    $(function () {
        RegisterCountryDropdownEvents();
    });

    function RegisterCountryDropdownEvents() {
        $("#<%=ddlCountry.ClientID%>").focus(function (e) {
            this.oldValue = this.value;
        });

        $("#<%=ddlCountry.ClientID%>").blur(function (e) {
            if (this.oldValue != this.value) setTimeout(__doPostBack(e.target.name,''), 0);    
        });
    }

    function securityCodeValidation(source, arguments) {
        if (arguments.IsValid)
        {
            var cardNumber = $('#txtCardNumber').val();

            if (cardNumber.startsWith("3"))//american express
                arguments.IsValid = arguments.Value.length === 4;
            if (cardNumber.startsWith("4"))//visa
                arguments.IsValid = arguments.Value.length === 3;
            if (cardNumber.startsWith("2") || cardNumber.startsWith("5"))//mastercard
                arguments.IsValid = arguments.Value.length === 3;
            if (cardNumber.startsWith("6"))//discover
                arguments.IsValid = arguments.Value.length === 3;
        }
    }

    function expireDateValidation(source, args) {
        var day = "02";
        var month = $('#ddlExpMonth').val();
        var year = $('#ddlExpYear').val();
        var currentDate = new Date();
        var selectedDate = new Date(year + '-' + month + "-" + day);
        if (month != "Expiration Month" && year != "Expiration Year")
            args.IsValid = selectedDate.getTime() > currentDate.getTime();
    }

    function ccTypeValidation(source, arguments) {
        var cardNumber = $('#txtCardNumber').val();
        arguments.IsValid = (validCCtypes.indexOf(cardNumber.charAt(0)) > -1);
    }

    function ccNumberValidation(source, arguments) {
        var cardNumber = $('#txtCardNumber').val();
        arguments.IsValid = !isNaN(cardNumber);
    }
</script>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExpressLoginPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.ExpressLoginPart" %>

<asp:Panel ID="pnlCustomerForm" DefaultButton="lbtnExpressCheckout" runat="server" CssClass="islet fill-greyLightest">
    <h1>Express Checkout</h1>
    <div class="inlineLabel">
        <asp:Label runat="server" ID="lblEmail" AssociatedControlID="txtEmail">Email Address</asp:Label>
        <input runat="server" id="txtEmail" type="email" />
        <asp:RequiredFieldValidator ID="revUserName" runat="server" ControlToValidate="txtEmail" ErrorMessage="*" Text="Enter your email address" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        <span runat="server" id="spanAlreadyRegistered" class="form-error" visible="false">Email address already registered.  Please sign in to use this email address.</span>
    </div>
    <div class="formFooter"><asp:LinkButton runat="server" ID="lbtnExpressCheckout" CssClass="btn" Text="Continue to checkout"></asp:LinkButton></div>
    <p class="h-textSm icon-info-alt">Select this option if you wish to make your purchase without storing your account payment information. This email address will be used to send you status on your order and to track previous purchases.</p>
</asp:Panel>

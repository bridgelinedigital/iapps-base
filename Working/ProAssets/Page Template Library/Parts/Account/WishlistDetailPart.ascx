﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WishlistDetailPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.WishlistDetailPart" %>

<%@ Import Namespace="Bridgeline.FW.Commerce.Orders" %>

<div class="sideToSideMed h-pushBottom">
    <h1 runat="server" id="hWishlistTitle">Edit - My Birthday List</h1>
    <div class="navOptions navOptions--sm navOptions--medHorizontal"><span class="icon-cog"></span>
        <asp:LinkButton runat="server" ID="lbtnEmail" data-mfp-src="#email-popup" CssClass="icon-mail openPopup">Email</asp:LinkButton>
        <asp:LinkButton runat="server" ID="lbtnDelete" CssClass="icon-trash" CausesValidation="false">Delete</asp:LinkButton>
        <asp:LinkButton runat="server" ID="lbtnCancel" CssClass="icon-cancel" CausesValidation="false">Cancel</asp:LinkButton>
    </div>
</div>
<div class="row">
    <div class="column lg-6">
        <div class="inlineLabel">
            <asp:Label runat="server" ID="lblwishlistName" AssociatedControlID="txtWishlistName">Wishlist Name</asp:Label>
            <input runat="server" id="txtWishlistName" type="text" required />
            <asp:RequiredFieldValidator ID="rfvWishlistName" runat="server" ControlToValidate="txtWishlistName" ErrorMessage="*" Display="Dynamic" ValidationGroup="SaveList"></asp:RequiredFieldValidator>
        </div>
        <div class="inlineLabel">
            <asp:Label runat="server" ID="lblWishlistDescription" AssociatedControlID="txtWishlistDescription">Description</asp:Label>
            <textarea runat="server" id="txtWishlistDescription" required />
        </div>
        <p class="h-textRight">
            <asp:LinkButton runat="server" ID="lbtnSave" CssClass="btn btn--sm icon-check" ValidationGroup="SaveList">Save</asp:LinkButton>
        </p>
    </div>

    <div class="column lg-18">
        <asp:Repeater runat="server" ID="rptWishlists" OnItemDataBound="rptWishlists_ItemDataBound" OnItemCommand="rptWishlists_ItemCommand">
            <HeaderTemplate>
                <div class="cartItemContainer">
            </HeaderTemplate>

            <ItemTemplate>
                <div class="cartItem">
                    <figure class="cartItem-image"><img runat="server" id="imgProduct" title="product" alt="product" /></figure>
                    <div class="cartItem-firstSection">
                        <h3 class="cartItem-name">
                            <a href="<%# ((Bridgeline.FW.Commerce.Orders.WishListItem)Container.DataItem).ProductSku.ParentProduct.DefaultProductUrl %>"><%# ((Bridgeline.FW.Commerce.Orders.WishListItem)Container.DataItem).ProductSku.ParentProduct.Title %> - <%#((Bridgeline.FW.Commerce.Orders.WishListItem)Container.DataItem).ProductSku.SKU %></a>
                        </h3>
                        <asp:Repeater runat="server" ID="rptItemAttributes">
                            <HeaderTemplate><ul class="cartItem-infoList"></HeaderTemplate>
                            <ItemTemplate>
                                <li><%#DataBinder.Eval(Container.DataItem, "Name") %>: <strong><%#DataBinder.Eval(Container.DataItem, "Value") %></strong></li>
                            </ItemTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="cartItem-secondSection">
                        <div class="cartItem-actions">
                            <div class="cartItem-actions-counter">
                                <div class="cartItem-actions-counter-control cartItem-actions-counter-control--subtract"></div>
                                <input runat="server" id="txtQty" type="text" class="cartItem-actions-counter-display" value='<%#DataBinder.Eval(Container.DataItem, "Quantity") %>' data-increment="<%#((WishListItem)Container.DataItem).ProductSku.OrderIncrement.ToString() %>" />
                                <div class="cartItem-actions-counter-control cartItem-actions-counter-control--add"></div>
                            </div>
                            <asp:RequiredFieldValidator runat="server" ID="rvalQty" ControlToValidate="txtQty" Display="Dynamic" ErrorMessage="Quantity is required" CssClass="form-error"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvQuantity" runat="server" Display="Dynamic"
                                ControlToValidate="txtQty" ClientValidationFunction="validateQuantity"
                                ErrorMessage='<%# "Quantity must be ordered in increments of "+((WishListItem)Container.DataItem).ProductSku.OrderIncrement.ToString()%>' CssClass="form-error">
                            </asp:CustomValidator>
                            <asp:LinkButton runat="server" ID="lbtnUpdate" CssClass="cartInfo-actions-update" CommandName="UpdateItem" CommandArgument='<%# Eval("Id") %>'>update</asp:LinkButton>
                            <asp:LinkButton runat="server" ID="lbtnAddToCart" CssClass="btn btn--XSm btn--full h-pushSmTop icon-cart" CausesValidation="false" CommandName="AddToCart" CommandArgument='<%# Eval("Id") %>'>Add to Cart</asp:LinkButton>
                        </div>
                    </div>
                    <div class="cartItem-cap">
                        <span class="cartItem-price"><asp:literal runat="server" ID="litTotal">$200.00</asp:literal><span><asp:Literal runat="server" ID="litPricePer">$100.00ea</asp:Literal></span></span>
                        <asp:LinkButton runat="server" ID="lbtnRemove" CssClass="cartItem-removeLink" CommandName="RemoveItem" CommandArgument='<%# Eval("Id") %>' CausesValidation="false">remove</asp:LinkButton>
                    </div>
                </div>
            </ItemTemplate>

            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:Repeater>

        <asp:PlaceHolder ID="phBottomControlPanel" runat="server">
            <div class="navOptions navOptions--sm navOptions--medHorizontal"><span class="icon-cog"></span>
            <asp:LinkButton runat="server" ID="lbtnEmailBottom" data-mfp-src="#email-popup" CssClass="icon-mail openPopup">Email</asp:LinkButton>
            <asp:LinkButton runat="server" ID="lbtnDeleteBottom" CssClass="icon-trash" CausesValidation="false">Delete</asp:LinkButton>
            <asp:LinkButton runat="server" ID="lbtnCancelBottom" CssClass="icon-cancel" CausesValidation="false">Cancel</asp:LinkButton>
            </div>
        </asp:PlaceHolder>
    </div>
</div>

<div id="email-popup" class="popup mfp-hide">
    <h2 class="h-underline">Email <%=WishListTitle %></h2>
    <div class="inlineLabel">
        <label for="<%=txtEmail.ClientID %>">Email Address</label>
        <input runat="server" id="txtEmail" type="email" required />
        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator runat="server" ID="revEmail" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Invalid Email"
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="form-error"></asp:RegularExpressionValidator>
    </div>
    <!--/.inlineLabel-->
    <p>
        <asp:LinkButton runat="server" ID="lbtnSendEmail" CssClass="btn btn--full icon-mail">Send</asp:LinkButton>
    </p>
</div>
<script>
    function validateQuantity(oSrc, args) {
        var increment = ($("#" + oSrc.controltovalidate).attr("data-increment"));
        args.IsValid = (args.Value % increment == 0);
    }
</script>

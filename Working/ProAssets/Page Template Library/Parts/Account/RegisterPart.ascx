﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisterPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.RegisterPart" %>


    <div class="row row--XTight">
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblFirstName" AssociatedControlID="txtFirstName">First Name</asp:Label>
            <input runat="server" id="txtFirstName" type="text" required />
            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="First Name is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>
        <!--/.column-->
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblLastName" AssociatedControlID="txtLastName">Last Name</asp:Label>
            <input runat="server" id="txtLastName" type="text" required />
            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Last Name is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>
        <!--/.column-->
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblPhone" AssociatedControlID="txtPhone">Phone</asp:Label>
            <input runat="server" id="txtPhone" type="tel" required />
            <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ControlToValidate="txtPhone" runat="server" ID="revPhone" Display="Dynamic" ErrorMessage="Invalid Phone"
                ValidationExpression="^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$" CssClass="form-error"></asp:RegularExpressionValidator>
        </div>
        <!--/.column-->
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblEmail" AssociatedControlID="txtEmail">Email</asp:Label>
            <input runat="server" id="txtEmail" type="email" required />
            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="revEmail" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Invalid Email"
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="form-error"></asp:RegularExpressionValidator>
        </div>
        <!--/.column-->
        <% if(!Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.IsUserNameAsEmail) { %>
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblUsername" AssociatedControlID="txtUsername">Username</asp:Label>
            <input runat="server" id="txtUsername" type="text" required />
            <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtUsername" ErrorMessage="Username is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>
        <%} %>
        <!--/.column-->
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblPassword" AssociatedControlID="txtPassword">Password</asp:Label>
            <input runat="server" id="txtPassword" type="password" required />
            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>
        <!--/.column--> 
        <!--/.column-->
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblConfirmPassword" AssociatedControlID="txtConfirmPassword">Confirm Password</asp:Label>
            <input runat="server" id="txtConfirmPassword" type="password" required />
            <asp:CompareValidator runat="server" ID="cvRegisterPassword" ControlToCompare="txtPassword" Display="Dynamic" ControlToValidate="txtConfirmPassword" ErrorMessage="Passwords do not match" CssClass="form-error"></asp:CompareValidator>
            <asp:CustomValidator runat="server" ID="cvalPasswordComplexity" ErrorMessage="Password must be at least 7 characters, contain 1 number and at least 1 special character." OnServerValidate="cvalPasswordComplexity_ServerValidate" ClientValidationFunction="ValidateRegisterPassword" Display="Dynamic" CssClass="form-error" />
        </div>
        <!--/.column-->
        <div class="column">
            <div class="formCheckBox">
                <span>
                    <input runat="server" id="chkRemember" type="checkbox" />
                    <asp:Label runat="server" ID="lblRemember" AssociatedControlID="chkRemember">Remember me on this computer</asp:Label>
                </span>
            </div>
        </div>
    </div>
<script type="text/javascript">
    //ASP.NET blocks pages from sending html tags in GET/POST arguments and cookies to prevent script injection attacks for HTML characters < / >
    function ValidateRegisterPassword(sender, args) {
        var txtPwd = document.getElementById("<%= txtConfirmPassword.ClientID %>");        
        var bFlag = true;
        var strRestrict = ['<', '>', '/'];
        if (txtPwd.value != "") {
            for (var i = 0; i != strRestrict.length; i++) {                
                if (txtPwd.value.indexOf(strRestrict[i]) != -1) {
                    bFlag = false;
                    break;
                }
            }            
        }
        args.IsValid = bFlag;
    }
</script>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StandardLoginPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Account.StandardLoginPart" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>

<asp:PlaceHolder ID="error" Visible="false" runat="server">
    <p class="alert icon-attention">
        <asp:Literal ID="errorMessage" runat="server" Text="Username or password invalid"></asp:Literal>
    </p>
</asp:PlaceHolder>

<asp:Panel runat="server" ID="pnlCustomerForm" DefaultButton="lbtnLogin" CssClass="islet fill-greyLightest">
    <h1>Sign In</h1>
    <div class="row row--tight">
        <div class="column med-12 inlineLabel">
            <asp:Label runat="server" ID="lblUserName" AssociatedControlID="txtUserName"><%=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.IsUserNameAsEmail ? "Email Address" : "User Name" %></asp:Label>
            <input runat="server" id="txtUserName" type="text" required />
            <asp:RequiredFieldValidator ID="revUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="Username is required" Text="Enter your user name" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>
        <div class="column med-12 inlineLabel">
            <asp:Label runat="server" ID="lblPassword" AssociatedControlID="txtPassword">Password</asp:Label>
            <input runat="server" id="txtPassword" type="password" required />
            <asp:RequiredFieldValidator ID="revPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password is required" Text="Enter your password" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="formCheckBox h-pushSmBottom">
        <span>
            <input runat="server" id="chkRemember" type="checkbox" />
            <asp:Label runat="server" ID="lblRemember" AssociatedControlID="chkRemember">Remember me on this computer</asp:Label>
        </span>
    </div>
    <!--/.formCheckBox-->
            
    <div class="formFooter"><asp:LinkButton runat="server" ID="lbtnLogin" class="btn" >Sign In</asp:LinkButton></div>
    <div class="navOptions navOptions--sm navOptions--lgHorizontal">
        <a data-mfp-src="#passwordPopup" class="openPopup icon-help-circled">Forgot your password?</a>
        <a href="<%= Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.GetRegisterPageUrl() %>" class="icon-plus">Create an account</a>
        <a data-mfp-src="#trouble-popup" class="icon-question openPopup">Trouble signing in?</a>
    </div>

    <div id="trouble-popup" class="popup mfp-hide">
        <FWControls:FWTextContainer runat="server" ID="fwtxtLoginTrouble">
            <FwText></FwText>
        </FWControls:FWTextContainer>
    </div>
</asp:Panel>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogArchiveList.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Global.BlogArchiveList" %>
<%@Import Namespace="System.Data"%>

<h3 class="filters-subHeading">Archive</h3>
<asp:Repeater ID="rptArchiveList" runat="server">
    <HeaderTemplate>
        <ul class="filters-list truncateList">
    </HeaderTemplate>
    <ItemTemplate>
        <li><a href="<%# ((DataRowView)Container.DataItem)["PostUrl"] %>"><%# ((DataRowView)Container.DataItem)["PostMonth"] %> (<%# ((DataRowView)Container.DataItem)["PostCount"] %>)</a></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
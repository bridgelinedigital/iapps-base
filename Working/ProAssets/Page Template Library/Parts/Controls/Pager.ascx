﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Pager.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.Pager" %>

<div class="pagination">
    <div class="pageNumbers">
        <asp:LinkButton runat="server" ID="lbtnPrevPage" OnClick="lbtnPrevNextPage_Click" aria-label="Previous Page"/>

        <asp:Repeater runat="server" ID="rptPageNumbers" OnItemCommand="rptPageNumbers_ItemCommand" OnItemDataBound="rptPageNumbers_ItemDataBound">
            <ItemTemplate>
                <asp:LinkButton runat="server" ID="lbtnPageNumber" CommandArgument="<%#(int)Container.DataItem %>" CommandName="ChangePage"><%# Container.DataItem %></asp:LinkButton>
            </ItemTemplate>
        </asp:Repeater>

        <asp:LinkButton runat="server" ID="lbtnNextPage" OnClick="lbtnPrevNextPage_Click" aria-label="Next Page"/>
    </div>
</div>

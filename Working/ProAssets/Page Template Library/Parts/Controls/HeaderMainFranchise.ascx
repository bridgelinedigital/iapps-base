﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderMainFranchise.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.HeaderMainFranchise" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<%@ Import Namespace="Bridgeline.FW.Commerce.Base.SiteSettings" %>

<header class="headerMain">
    <div class="headerMain-util">
        <div class="headerMain-utilInner">
            <div class="headerMain-utilZone01">
                <iAppsPro:NavUtil ID="ctlUtilityNav" runat="server" />
            </div>

            <div class="headerMain-utilZone02">
                <iAppsPro:NavCallout ID="CalloutNav1" runat="server" />
            </div>

            <div class="headerMain-utilZone03">
                <iAppsPro:NavUser ID="UserNav1" runat="server" />
            </div>
        </div>
    </div>

    <div class="headerMain-main">
        <div class="headerMain-mainInner">
            <div class="headerMain-mainZone01">
                <div class="logoMain">
                    <a href="/"><img src="/Image Library/content-images/logo.png" alt="<%=CurrentSite != null ? CurrentSite.Title : "logo" %>"></a>
                </div>
            </div>

            <div class="headerMain-mainZone02">
                <iAppsPro:NavMain runat="server" ID="ctlNavMain" />
            </div>

            <div class="headerMain-mainZone03">
                <iAppsPro:CartStatus ID="ctlMiniCart" runat="server" />

                <iAppsPro:SearchToggle ID="ctlSearchBox" runat="server" />

                <iAppsPro:MobileDrawerToggle ID="ctlMobileDrawer" runat="server" />
            </div>
        </div>
    </div>
</header>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderMainStripped.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.HeaderMainStripped" %>

<header class="headerMain">
  <div class="headerMain-main">
    <div class="headerMain-mainInner">
      <div class="headerMain-mainZone01">
        <div class="logoMain">
            <a href="/"><img src="/Image Library/content-images/logo.png" alt="<%=CurrentSite != null ? CurrentSite.Title : "logo" %>"></a>
        </div>
      </div>
    </div>
  </div>
</header>

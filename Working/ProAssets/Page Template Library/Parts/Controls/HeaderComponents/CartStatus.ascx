﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CartStatus.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.HeaderComponents.CartStatus" %>

<a class="cartStatus" href="<%=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.CartURL %>" >
    <span class="bug cartStatus-bug"><%=CurrentCart.CartItems.Count() %></span>
</a>
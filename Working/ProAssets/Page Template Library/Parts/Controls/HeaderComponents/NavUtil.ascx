﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavUtil.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.HeaderComponents.NavUtil" %>

<nav class="navUtil">
    <CLControls:CLHierarchicalNav ID="navUtility" runat="server" SiteMapProvider="UtilityNav" />
</nav>
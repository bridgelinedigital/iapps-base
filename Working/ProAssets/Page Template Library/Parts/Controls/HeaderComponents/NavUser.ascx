﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavUser.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.HeaderComponents.NavUser" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<%@ Import Namespace="Bridgeline.FW.Commerce.Base.SiteSettings" %>

<%if (!IsUserAuthenticated) { %>
<nav class="navSignin"> 
    <ul>
        <li><a href="<%=SiteSettings.Instance.CustomersLoginPageURL %>">Login</a></li>
        <li><a href="<%=SiteSettings.Instance.GetRegisterPageUrl() %>">Register</a></li>
    </ul>
</nav>
<% } else { %>

<nav class="navUser">
    <ul>
        <li>
            <div class="navUser-message">Hello <%=CurrentCustomer.FirstName %><span class="navUser-icon"></span></div>
            <ul>
                <li><a href="<%=SiteSettings.Instance.MyAccountURL %>">My profile</a></li>
                <li><asp:LinkButton runat="server" ID="lbtnLogOut" CausesValidation="false">Log Out</asp:LinkButton></li>
            </ul>
        </li>
    </ul>
</nav>
<% } %>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavCallout.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.HeaderComponents.NavCallout" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<%@ Import Namespace="Bridgeline.FW.Commerce.Base.SiteSettings" %>

<nav class="navCallout">
    <ul>
        <li><a href="mailto:<%=SiteSettings.Instance.StoreCustomerServiceEmail %>" class="icon-mail">Email Us</a></li>
        <li><a href='tel:<%=SiteSettings.Instance.GetCustomerServiceTelephoneNumberDigits() %>' class="icon-phone"><%=SiteSettings.Instance.GetCustomerServiceTelephoneNumber() %></a></li>
    </ul>
</nav>

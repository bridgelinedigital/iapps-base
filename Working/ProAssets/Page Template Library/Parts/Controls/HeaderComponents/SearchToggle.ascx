﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchToggle.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.HeaderComponents.SearchToggle" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<div data-mfp-focus="#searchMain" data-mfp-src="#search-popup" class="searchToggle openPopup"></div>

<div class="popup mfp-hide popup--search" id="search-popup">
  <div class="searchBox searchBox--main">
    <!--label for="searchMain">Search</label-->
    <input id="searchMain" type="text" placeholder="Search" name="" class="searchBox-textField" aria-label="Search" />
    <a class="searchBox-submit" id="searchBox-submit" href="javascript:void(0)" aria-label="Search Submit"></a>
  </div>
</div>

<script>
    $(document).ready(function () {
        $('#searchMain').keydown(function (event) {
            if (event.keyCode == 13)
            {
                event.preventDefault();
                submitSiteSearch($('#searchMain').val());
            }
        });

        $('#searchBox-submit').click(function () {
            submitSiteSearch($('#searchMain').val());
        });
       
    });
    function submitSiteSearch(searchVal) {
        if (searchVal != "") {
            //document.location = '/search/productsearch?q=' + escape(searchVal);
            //document.location = '/search/sitesearch?q=' + escape(searchVal);
            document.location = '<%=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.GetProductSearchPageUrl() %>?q=' + encodeURI(searchVal);
        }
    }
</script>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterMain.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.FooterMain" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Enums" %>

<footer class="footerMain">
    <div class="footermain-top">
        <div class="footermain-topInner">
            <div class="row">
                <iAppsPro:MegaNav ID="ctlNavFooter" runat="server" XsltFileName="FooterNav.xslt" SiteMapProviderName="FooterNav" />
      
                <div class="column med-6">
                    <span class="footerMain-heading">Connect With Us</span>
                    <iAppsPro:MegaNav ID="wseppMegaNav" runat="server" XsltFileName="FooterSocialNav.xsl" SiteMapProviderName="FooterSocialNav" />
                </div>
            </div>
        </div>
    </div>

    <div class="footermain-bottom">
      <div class="footermain-bottomInner">
          <span class="footerMain-bottomInfo">
            <%=AttributeModel.GetAttributeValue(CurrentSite, SiteAttributeNames.FooterCopyright) %>
              <%if (DisplayDeliveryRelease) { %>
                (build: <%=DeliveryRelease %>)
              <%} %>
            <%if(CurrentSitePrimaryAddress != null) { %>
            <%--<address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="streetAddress"><%=CurrentSitePrimaryAddress.AddressLine1 + (!String.IsNullOrEmpty(CurrentSitePrimaryAddress.AddressLine2) ? ", " + CurrentSitePrimaryAddress.AddressLine2 : "") %></span>, 
                <span itemprop="addressLocality"><%=CurrentSitePrimaryAddress.City %></span>, 
                <span itemprop="addressRegion"><%=CurrentSitePrimaryAddress.State.Title %></span> 
                <span itemprop="postalCode"><%=CurrentSitePrimaryAddress.Zip %></span>
                <span itemprop="addressCountry"><%=CurrentSitePrimaryAddress.Country.CountryCode %></span>
            </address> --%>
            <%} %>
          </span>
        <nav class="footerMain-bottomNav">
            <CLControls:CLHierarchicalNav ID="navFooterUtility" runat="server" SiteMapProvider="FooterUtilityNav" />
        </nav>
      </div>
    </div>
</footer>

<div class="drawer">
    <div class="drawer-close">Close</div>
    <div class="drawer-content">

    </div>
</div>

<div class="globals-mobile drawerItem" data-id="globals-mobile">
    <nav class="navMain-mobile">
        <ul>
        </ul>
    </nav>
</div>

<div class="filters-mobile drawerItem" data-id="filters-mobile">
</div>

<script type="text/javascript">

    $(document).ready(function () {
        var requestQuote = getParameterByName("requestQuote");

        if (requestQuote == "true") {
            var sku = getParameterByName("sku");
            var name = getParameterByName("name");

            $(".txtQuoteProdSku").val(sku);
            $(".txtQuoteProdName").val(name);
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

</script>

<script>
    $('.galleryMainContainer .galleryMain').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.galleryNav'
        //adaptiveHeight: true
    });
    $('.galleryMainContainer .galleryNav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.galleryMain',
        dots: false,
        //centerMode: true,
        //centerPadding: '0',
        focusOnSelect: true,
        vertical: true,
        responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
            //vertical: false,
        }
    },
    {
        breakpoint: 641,
        settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
            vertical: false,
        }
    },
    {
        breakpoint: 450,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            vertical: false,
        }
    },
        ]
    });
</script> 


<script>
    $('.imageGallery .galleryMain').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.galleryNav',
        adaptiveHeight: true
    });
    $('.imageGallery .galleryNav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.galleryMain',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        variableWidth: true,
        responsive: [
          {
              breakpoint: 1022,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3
              }
          },
          {
              breakpoint: 500,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
              }
          }
        ]
    });
</script> 



<script>
    $('.lightbox').each(function () {
        var image = $(this).children('img').attr('src');
        var text = $(this).find('.galleryMain-caption').text();
        $(this).magnificPopup({
            mainClass: 'mfp-fade',
            items:
            [{
                src: image,
                title: text
            }],
            gallery: { enabled: true },
            type: 'image'
        });
    });
</script>

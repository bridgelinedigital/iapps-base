﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageTitle.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.PageTitle" %>

<div class='section <%=ColorSchemeCssClass %> pageTitle <%=PositionCssClass %> <%=FixedBackground ? "pageTitle--fixedBackground" : "" %>' style="<%=BackgroundImageCss%>">
    <div class="contained">
        <div class="pageTitleCopy">
            <CLControls:PageTitle runat="server" ID="cltitle" CssClass="pageTitleCopy-heading" ControlTag="h1" />
            <%if(OptionalSubHeading != "") { %>
            <h2 class="pageTitleCopy-SubHeading"><%=OptionalSubHeading %></h2>
            <%} %>
        </div>
    </div>
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadcrumbNav.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.HeaderComponents.BreadcrumbNav" %>

<div class="pageMeta">
    <div class="pageMeta-inner">
        <nav role="navigation" class="navSecondary">
            <CLControls:Breadcrumb ID="Breadcrumb" runat="server" EnableContextMenu="true" SiteMapProvider="GlobalNavigation" XsltFileName="Breadcrumb.xslt"/>

            <!-- TODO: Add nav for sibling links -->
            <!--<ul class="navSecondary-links">
                <li><a href="#">Interior Page</a></li>
                <li><a href="#">Interior Page</a></li>
                <li><a href="#">Interior Page</a></li>
            </ul>-->
        </nav>
    </div>
</div>

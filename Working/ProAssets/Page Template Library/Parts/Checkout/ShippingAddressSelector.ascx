﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ShippingAddressSelector.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Checkout.ShippingAddressSelector" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<%@ Import Namespace="Bridgeline.FW.Commerce.Base.SiteSettings" %>

<asp:MultiView runat="server" ID="mvAddressBook" ActiveViewIndex="0">
    <asp:View runat="server" ID="vwSavedAddresses">
        <div class="infoAction">
            <asp:Repeater runat="server" id="rptAddresses">
                <ItemTemplate>
                    <div class="infoAction-item">
                        <div class="infoAction-check">
                            <input type="radio" id="ra0<%#Container.ItemIndex.ToString() %>" name="radAddressId" onchange="<%# Page.ClientScript.GetPostBackEventReference(lbtnUpdatePrimary, ((CustomerAddressModel)Container.DataItem).Id.ToString()) %>" />
                            <label for="ra0<%#Container.ItemIndex.ToString() %>">Select</label>
                        </div>
                        <!--/.infoAction-check-->
                        <div class="infoAction-title"><%#((CustomerAddressModel)Container.DataItem).NickName %></div>
                        <!--/.infoAction-title-->
                        <div class="infoAction-info"><%#((CustomerAddressModel)Container.DataItem).Address.FirstName %> <%#((CustomerAddressModel)Container.DataItem).Address.LastName %></div>
                        <!--/.infoAction-info-->
                        <div class="infoAction-info"><%#((CustomerAddressModel)Container.DataItem).Address.Phone %></div>
                        <!--/.infoAction-info-->
                        <div class="infoAction-info">
                            <%#((CustomerAddressModel)Container.DataItem).Address.AddressLine1 %><br>
                            <%#string.Format("{0}, {1}  {2}", ((CustomerAddressModel)Container.DataItem).Address.City, ((CustomerAddressModel)Container.DataItem).Address.State.StateCode, ((CustomerAddressModel)Container.DataItem).Address.Zip) %>
                        </div>
                        <!--/.infoAction-info-->
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <asp:LinkButton runat="server" id="lbtnUpdatePrimary" Visible="false" OnClick="lbtnUpdatePrimary_Click"></asp:LinkButton>
        </div>
    </asp:View>
    <asp:View runat="server" ID="vwNoAddresses">
        <p>No saved addresses found in your address book.</p>
    </asp:View>
</asp:MultiView>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingAddressFormPart.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Checkout.ShippingAddressFormPart" %>

    <div id="confirmShipping" class="popup mfp-hide">
        <h2>Please Confirm Your Address</h2>
        <p>By selecting the correct item from the list below</p>
        <div class="infoAction">
            <asp:Repeater runat="server" ID="rptAVSSuggestedAddresses">
                <ItemTemplate>
                    <div class="infoAction-item">
                        <div class="infoAction-check">
                            <input type="radio" id="ra0<%#Container.ItemIndex.ToString() %>" name="radAddressId" />
                            <label for="ra0<%#Container.ItemIndex.ToString() %>">Select</label>
                        </div>
                        <!--/.infoAction-check-->
                        <div class="infoAction-title">
                            UPS Validated Address <%#(Container.ItemIndex+1).ToString() %>
                        </div>
                        <div class="infoAction-info">
                            <%# ((Bridgeline.FW.Commerce.Base.Address)Container.DataItem).AddressLine1 %><br />
                            <%# string.IsNullOrEmpty(((Bridgeline.FW.Commerce.Base.Address)Container.DataItem).AddressLine2) ? string.Empty : ((Bridgeline.FW.Commerce.Base.Address)Container.DataItem).AddressLine2 + "<br />" %>
                            <%# string.Format("{0}, {1} {2}", ((Bridgeline.FW.Commerce.Base.Address)Container.DataItem).City, ((Bridgeline.FW.Commerce.Base.Address)Container.DataItem).State.StateCode, ((Bridgeline.FW.Commerce.Base.Address)Container.DataItem).Zip) %>
                        </div>
                        <!--/.infoAction-info-->
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <asp:LinkButton runat="server" id="lbtnConfirmAddress" CssClass="btn btn--full" OnClick="lbtnConfirmAddress_Click" CausesValidation="false">Confirm</asp:LinkButton>
    </div>
<asp:HiddenField runat="server" ID="hfSelectedAddress" ClientIDMode="Static" Value="0"/>
<script>
    $('#confirmShipping').on('change', function() {
        $('#hfSelectedAddress').val($('input[name=radAddressId]:checked', '#confirmShipping').attr('index'));
    });
</script>

    <!--/.h-stacksToSidesMed-->
    <div class="column">
        <fieldset>
            <legend class="legend--labelStyle">Address Type</legend>
            <div class="formRadioButton">
                <span>
                    <asp:RadioButton GroupName="AddressType" runat="server" ID="rbHome" Checked="true" /><asp:Label ID="lblResidential" runat="server" AssociatedControlID="rbHome">Residential</asp:Label>
                </span>
                <span>
                    <asp:RadioButton GroupName="AddressType" runat="server" ID="rbBusiness" /><asp:Label ID="lblBusiness" runat="server" AssociatedControlID="rbBusiness">Business</asp:Label>
                </span>
            </div>
        </fieldset>
    </div>
    <!--/.column-->
    <div class="column med-12 inlineLabel">
        <asp:Label runat="server" ID="lblFirstName" AssociatedControlID="txtFirstName">First Name</asp:Label>
        <input runat="server" id="txtFirstName" type="text" maxlength="255" required />
        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="First name is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <!--/.column-->
    <div class="column med-12 inlineLabel">
        <asp:Label runat="server" ID="lblLastName" AssociatedControlID="txtLastName">Last Name</asp:Label>
        <input runat="server" id="txtLastName" type="text" maxlength="255" required />
        <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Last name is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <!--/.column-->
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblPhone" AssociatedControlID="txtPhone">Phone</asp:Label>
        <input runat="server" id="txtPhone" type="tel" maxlength="50" required />
        <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone number is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ControlToValidate="txtPhone" runat="server" ID="revPhone" Display="Dynamic" ErrorMessage="Invalid Phone"
                ValidationExpression="^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$" CssClass="form-error"></asp:RegularExpressionValidator>
    </div>
    <!--/.column-->
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblStreet1" AssociatedControlID="txtStreet1">Street Address</asp:Label>
        <input runat="server" id="txtStreet1" type="text" maxlength="255" required />
        <asp:RequiredFieldValidator ID="rfvStreet1" runat="server" ControlToValidate="txtStreet1" ErrorMessage="Street address is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        <asp:CustomValidator ID="AVS_ValidateAddress" runat="server" Display="Dynamic" ErrorMessage="This is not a valid address for delivery" CssClass="form-error" OnServerValidate="avs_ValidateAddress_ServerValidate"></asp:CustomValidator>
    </div>
    <!--/.column-->
    <div class="column inlineLabel lg-12">
        <asp:Label runat="server" ID="lblStreet2" AssociatedControlID="txtStreet2">Bldg/Apt # (optional)</asp:Label>
        <input runat="server" id="txtStreet2" type="text" maxlength="255" />
    </div>
    <!--/.column-->
    <div class="column inlineLabel lg-12">
        <asp:Label runat="server" ID="lblZip" AssociatedControlID="txtZip">Zip/Postal Code</asp:Label>
        <input runat="server" id="txtZip" type="text" maxlength="50" required />
        <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="Postal code is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="regZip" runat="server" ControlToValidate="txtZip" ValidationExpression="(^\d{5}(-\d{4})?$)|(^[AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$)" ErrorMessage="Please enter a valid postal code" Display="Dynamic" CssClass="form-error"></asp:RegularExpressionValidator>
    </div>
    <!--/.column-->
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblCity" AssociatedControlID="txtCity">City</asp:Label>
        <input runat="server" id="txtCity" type="text" maxlength="255" required />
        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <!--/.column-->
    <asp:UpdatePanel runat="server" ID="upCountryState" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="column inlineLabel">
                <asp:Label runat="server" ID="lblcountry" AssociatedControlID="ddlCountry">Country</asp:Label>
                <asp:DropDownList runat="server" ID="ddlCountry" required AutoPostBack="False"/>
                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry" ErrorMessage="Country is required" InitialValue="Country" Display="Dynamic" CssClass="form-error"/>
            </div>
            <!--/.column-->
            <div class="column inlineLabel">
                <asp:Label runat="server" ID="lblState" AssociatedControlID="ddlState">State</asp:Label>
                <asp:DropDownList runat="server" ID="ddlState"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="ddlState" ErrorMessage="State is required" InitialValue="" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ID="upSaveAddress" ChildrenAsTriggers="true">
        <ContentTemplate>

            <asp:PlaceHolder runat="server" ID="phSaveAddress">
                <div class="column">
                    <div class="formCheckBox">
                        <span>
                            <asp:CheckBox runat="server" id="chkSaveAddress" AutoPostBack="true" OnCheckedChanged="chkSaveAddress_CheckedChanged" />
                            <asp:Label runat="server" ID="lblSaveAddress" AssociatedControlID="chkSaveAddress">Save this address to your Address Book?</asp:Label>
                        </span>
                    </div>
                </div>
                <div runat="server" id="addressName" class="column inlineLabel">
                    <asp:Label runat="server" ID="lblNickName" AssociatedControlID="txtNickName">Address Name</asp:Label>
                    <input runat="server" id="txtNickName" type="text" maxlength="255" required />
                    <asp:RequiredFieldValidator ID="rfvNickName" runat="server" ControlToValidate="txtNickName" Display="Dynamic" ErrorMessage="Name is required" CssClass="form-error"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvalNicknameUnique" runat="server" Display="Dynamic" ErrorMessage="An address already exists with that name" CssClass="form-error" OnServerValidate="cvalNicknameUnique_ServerValidate"></asp:CustomValidator>
                    <p class="formNote">e.g. "Home" or "Office"</p>
                </div>
            </asp:PlaceHolder>

        </ContentTemplate>
    </asp:UpdatePanel>
<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        RegisterCountryDropdownEvents();
    });

    $(function () {
        RegisterCountryDropdownEvents();
    });

    function RegisterCountryDropdownEvents() {
        $("#<%=ddlCountry.ClientID%>").focus(function (e) {
            this.oldValue = this.value;
        });

        $("#<%=ddlCountry.ClientID%>").blur(function (e) {
            if (this.oldValue != this.value) setTimeout(__doPostBack(e.target.name,''), 0);    
        });
    }

    function showSuggestedAddress() {
        if ($('#confirmShipping').length) {
            $.magnificPopup.open({
                items: {
                    src: '#confirmShipping'
                },
                type: 'inline'
            });
        }
    }
</script>
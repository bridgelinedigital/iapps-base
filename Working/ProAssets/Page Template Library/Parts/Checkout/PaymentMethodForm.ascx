﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentMethodForm.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Checkout.PaymentMethodForm" %>
<meta charset="utf-8">
<asp:PlaceHolder runat="server" ID="cphStripe" Visible="false">
    <script src="https://js.stripe.com/v3/"></script>
    <script> //Stripe
        $().ready(function () {
            var cardChanged = false;
            var options;
            // Create an instance of Elements
            if ('<%= (ControlContext.ToString() == "Checkout") %>' == 'True') {
                options = {
                    mode: 'payment',
                    amount: <%= ((int)Math.Round(CurrentOrder==null?0:CurrentOrder.GrandTotal * Math.Pow(10.0, 2), 0)).ToString() %> ,
                    currency: '<%= System.Globalization.RegionInfo.CurrentRegion.ISOCurrencySymbol.ToLower() %>',
                    paymentMethodCreation: 'manual',
                    capture_method: 'manual',
                    // Fully customizable with appearance API.
                    appearance: {/*...*/ },
                };
                var elements = stripe.elements(options);
            }
            if ('<%= (ControlContext.ToString() == "CustomerProfile") %>' == 'True') {
                options = null;
                var elements = stripe.elements({clientSecret: stripeClientSecret });
            }
            // Set up Stripe.js and Elements to use in checkout form
            

            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    lineHeight: '18px',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };
            // Create and mount the Payment Element
            const paymentElement = elements.create('payment');
            paymentElement.mount('#payment-element');


            // const form = document.getElementById('payment-form');
            const handleError = (error) => {
                const messageContainer = document.querySelector('#error-message');
                messageContainer.textContent = error.message;
                $("[id$='lbtnNext']").attr("disabled", true);
                //("[id$='lbtnNext']").prop('disabled')
            }

            // Handle real-time validation errors from the card Element.
            paymentElement.addEventListener('change', function (event) {
                cardChanged = true;
                var displayError = document.getElementById('error-message');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                    $("[id$='lbtnNext']").removeAttr('disabled');
                }

            });

            $("[id$='lbtnNext']").click(async (e) => {

                e.preventDefault();

                var nameOnCard = $('#<%=txtNameOnCard.ClientID%>').val();
                var streetAddress = $('#<%=txtStreetAddress.ClientID%>').val();
                var bldgAptNum = $('#<%=txtBldgAptNum.ClientID%>').val();
                var zip = $('#<%=txtZip.ClientID%>').val();
                var city = $('#<%=txtCity.ClientID%>').val();
                var country = $('#<%= ddlCountry.ClientID %>').find("option:selected").attr("code");
                var state = $('#<%= ddlState.ClientID %>').find("option:selected").text();

                if (cardChanged == true) {

                    if ($("[id$='lbtnNext']").attr('disabled'))
                        return false;

                    $("[id$='lbtnNext']").attr("disabled", true);

                    const { error: submitError } = await elements.submit();
                    if (submitError) {
                        handleError(submitError);
                        return false;
                    }
                    // Create the ConfirmationToken using the details collected by the Payment Element
                    if ('<%= (ControlContext.ToString() == "Checkout") %>' == 'True')
                    var { error, confirmationToken } = await stripe.createConfirmationToken({
                        elements,
                        params: {
                            payment_method_data: {
                                billing_details: {
                                    name: nameOnCard,
                                    "address": {
                                        "city": city,
                                        "country": country,
                                        "line1": streetAddress,
                                        "line2": bldgAptNum,
                                        "postal_code": zip,
                                        "state": state
                                    }
                                }
                            }
                        }
                    });

                    if ('<%= (ControlContext.ToString() == "CustomerProfile") %>' == 'True') {
                        var result = await stripe.confirmSetup({
                            elements,
                            confirmParams: {
                                return_url: '<%= Context.Items[Bridgeline.FW.UI.UIConstants.PUBLIC_SITE_URL].ToString() %>',
                            },
                            billing_details: {
                                name: nameOnCard,
                                "address": {
                                    "city": city,
                                    "country": country,
                                    "line1": streetAddress,
                                    "line2": bldgAptNum,
                                    "postal_code": zip,
                                    "state": state
                                }
                            },
                            redirect: 'if_required'
                        });
                        var error = result.error;
                        if(!error)
                            var confirmationToken = {
                            id: result.setupIntent.payment_method, payment_method_preview: { card: { exp_month: 0, exp_year: 0, last4: "0000", brand: result.setupIntent.payment_method_types[0] } } };
                    }
                    if (error) {
                        // This point is only reached if there's an immediate error when
                        // creating the ConfirmationToken. Show the error to your customer (for example, payment details incomplete)
                        handleError(error);
                        return false;
                    }
                    $("[id$='hfCardToken']").val(confirmationToken.id);
                    if (confirmationToken.payment_method_preview.card) {
                        $("[id$='hfExpMonth']").val(confirmationToken.payment_method_preview.card.exp_month);
                        $("[id$='hfExpYear']").val(confirmationToken.payment_method_preview.card.exp_year);
                        $("[id$='hfCardBrand']").val(confirmationToken.payment_method_preview.card.brand);
                        $("[id$='hfCardLast4']").val(confirmationToken.payment_method_preview.card.last4);

                    }
                    theForm.__EVENTTARGET.value = 'ctl01$lbtnNext';
                    theForm.submit();
                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl01$lbtnNext",
                        "",
                        true,
                        "",
                        "",
                        false,
                        true));
                    return true;

                    cardChanged = false;
                } else {
                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl01$lbtnNext",
                        "",
                        true,
                        "",
                        "",
                        false,
                        true));
                }
                return true;

            });
        });
    </script>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="cphAutorizeNet" Visible="false">
    <script>//Authorize.net

        $().ready(function () {
            var cardChanged = false;
            $("[id$='lbtnNext']").click(function (e) {
                e.preventDefault();
                var cardType = "001";
                var cardBrand = "Visa";
                var cardNumber = $('#txtCardNumber').val();
                if (isNaN(cardNumber)) {
                    alert('Please re-enter your full credit card number.');
                    return;
                }
                var firstNumber = cardNumber.charAt(0);
                GetCardType(firstNumber);
                $("[id$='hfCardLast4']").val(cardNumber.substr(-4, 4));
                var expMonth = $('#<%=ddlExpMonth.ClientID%>').val();
                var expYear = $('#<%=ddlExpYear.ClientID%>').val();
                var cvv = $('#txtSecurityCode').val();

              
                var cardData = {};
                cardData.cardNumber = cardNumber;
                cardData.month = expMonth;
                cardData.year = expYear;
                cardData.cardCode = cvv;

                var secureData = {};
                secureData.authData = authData;
                secureData.cardData = cardData;
                Accept.dispatchData(secureData, responseHandler);
                return false;
            });
        });

        function responseHandler(response) {
            if (response.messages.resultCode === "Error") {
                var i = 0;
                var errMsg = "";
                while (i < response.messages.message.length) {
                    errMsg += response.messages.message[i].code + ": " +
                        response.messages.message[i].text;
                    console.log(errMsg);

                    i = i + 1;
                }
                alert(errMsg);
            } else {
                paymentFormUpdate(response.opaqueData);
            }
        }

        function paymentFormUpdate(opaqueData) {
            var cardNumber = $('#txtCardNumber').val();
            $('#<%=hfCardToken.ClientID%>').val(opaqueData.dataValue + '|' + opaqueData.dataDescriptor);
            //document.getElementById("dataDescriptor").value = opaqueData.dataDescriptor;
            $('#txtCardNumber').val(cardNumber.substr(0, 4) + '11111111' + cardNumber.substr(-4, 4));
            $('#txtSecurityCode').val('000');
            WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl01$lbtnNext",
                "",
                true,
                "",
                "",
                false,
                true));
            return true;

        }
    </script>
</asp:PlaceHolder>
<script>
    function CreateFlexToken(e) {
        e.preventDefault();
        var cardNumber = $('#txtCardNumber').val();
        if (isNaN(cardNumber)) {
            alert('Please re-enter your full credit card number.');
            return;
        }
        var firstNumber = cardNumber.charAt(0);
        GetCardType(firstNumber);
        $("[id$='hfCardLast4']").val(cardNumber.substr(-4, 4));
        var expMonth = $('#<%=ddlExpMonth.ClientID%>').val();
        var expYear = $('#<%=ddlExpYear.ClientID%>').val();
        var code = $('#txtSecurityCode').val();
        var options = {
            kid: jwk.kid,
            keystore: jwk,
            cardInfo: {
                cardNumber: cardNumber,
                cardType: cardType,
                cardExpirationMonth: expMonth,
                cardExpirationYear: expYear,
                code: code
            }

            , production: isProduction
            , encryptionType: 'rsaoaep'
            //, production: true // without specifying this tokens are created in test env
        };

        FLEX.createToken(options, (response) => {
            if (response.error) {
                alert('Error processing credit card');
                console.log(response);
                return;
            }
            else {
                $("[id$='hfCardToken']").val(response.token);
                $("[id$='hfCardLast4']").val(response.maskedPan);
                $('#txtCardNumber').val(cardNumber.substr(0, 6) + '11111111' + cardNumber.substr(-4, 4));
                $('#txtSecurityCode').val('000');
                WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl01$lbtnNext",
                    "",
                    true,
                    "",
                    "",
                    false,
                    true));
                return true;
            }
        });
    }
</script>
<asp:PlaceHolder runat="server" ID="cphCybersource" Visible="false">
    <script src="/Script%20Library/front-end/flex-sdk-web.js"></script>
    <script> //Cybersource
        //import flex from '@cybersource/flex-sdk-web';
        $().ready(function () {
            var cardChanged = false;
            $("[id$='lbtnNext']").click(CreateFlexToken);
        });

    </script>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="cphPayerAuth" Visible="false">
    <script src="/Script%20Library/front-end/flex-sdk-web.js"></script>
    <script> //Cordinal Commerce Payer Authentication
        $().ready(function () {
            Cardinal.configure({
                logging: {
                    debug: 'verbose'
                }
            });
            Cardinal.setup("init", {
                jwt: $("[id$='hfJWTContainer']").val()
            });

            Cardinal.on('payments.setupComplete', function (setupCompleteData) {
                $("[id$='hfSessionId']").val(setupCompleteData.sessionId);
            });



            var cardChanged = false;
            $("[id$='lbtnNext']").click(StartPayerAuth);
        });
        function StartPayerAuth(e) {
            e.preventDefault();
            var cardNumber = $('#txtCardNumber').val();
            Cardinal.trigger("accountNumber.update", cardNumber);
            CreateFlexToken(e);

        }
    </script>
</asp:PlaceHolder>
<div class="column">
    <ul class="list--horizontal h-textXLg">
        <li id="liVisaIcon" class="icon-visa" runat="server"></li>
        <li id="liMasterCardIcon" class="icon-mastercard" runat="server"></li>
        <li id="liDiscoverIcon" class="icon-discover" runat="server"></li>
        <li id="liAmexIcon" class="icon-amex" runat="server"></li>
    </ul>
</div>
<asp:PlaceHolder runat="server" ID="phStripeCard" Visible="false">
    <div class="column inlineLabel">
        <div id="payment-element">
            <!-- Elements will create form elements here -->
        </div>

        <div id="error-message">
            <!-- Display error message to your customers here -->
        </div>
    </div>
</asp:PlaceHolder>
<asp:HiddenField ID="hfNameOnCard" runat="server" />
<asp:HiddenField ID="hfCardToken" runat="server" />
<asp:HiddenField ID="hfExpMonth" runat="server" />
<asp:HiddenField ID="hfExpYear" runat="server" />
<asp:HiddenField ID="hfCardBrand" runat="server" />
<asp:HiddenField ID="hfCardLast4" runat="server" />
<asp:HiddenField ID="hfJWTContainer" runat="server" />
<asp:HiddenField ID="hfSessionId" runat="server" />
<asp:PlaceHolder runat="server" ID="phDefaultCardNumber">
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblCardNumber" AssociatedControlID="txtCardNumber" Text="Card Number"></asp:Label>
        <input runat="server" data-cardinal-field="AccountNumber" id="txtCardNumber" type="text" required maxlength="25" clientidmode="Static" />
        <asp:RegularExpressionValidator ID="regCardNumber" runat="server" ControlToValidate="txtCardNumber" ValidationExpression="(^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$)" ErrorMessage="Please enter a valid Card Number" Display="Dynamic" CssClass="form-error" />
        <asp:RequiredFieldValidator ID="rfvCardNumber" runat="server" ControlToValidate="txtCardNumber" Display="Dynamic" ErrorMessage="Card Number is required" CssClass="form-error"></asp:RequiredFieldValidator>
        <asp:CustomValidator runat="server" ID="cvalLuhnValidation" Display="Dynamic" ErrorMessage="Please enter a valid Card Number" CssClass="form-error" ClientValidationFunction="ccNumberValidation" OnServerValidate="cvalLuhnValidation_ServerValidate"></asp:CustomValidator>
        <asp:CustomValidator runat="server" ID="cvalCreditCardTypeValidation" Display="Dynamic" ErrorMessage="" CssClass="form-error" ClientValidationFunction="ccTypeValidation" OnServerValidate="cvalCreditCardTypeValidation_ServerValidate" ControlToValidate="txtCardNumber" />
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="phExpiration">
    <div class="column inlineLabel med-12">
        <asp:Label runat="server" ID="lblExpMonth" AssociatedControlID="ddlExpMonth" Text="Expiration Month"></asp:Label>
        <asp:DropDownList runat="server" ID="ddlExpMonth" required AppendDataBoundItems="true" ClientIDMode="Static">
            <asp:ListItem Value="Expiration Month" Text="Expiration Month"></asp:ListItem>
            <asp:ListItem Text="01" Value="01" />
            <asp:ListItem Text="02" Value="02" />
            <asp:ListItem Text="03" Value="03" />
            <asp:ListItem Text="04" Value="04" />
            <asp:ListItem Text="05" Value="05" />
            <asp:ListItem Text="06" Value="06" />
            <asp:ListItem Text="07" Value="07" />
            <asp:ListItem Text="08" Value="08" />
            <asp:ListItem Text="09" Value="09" />
            <asp:ListItem Text="10" Value="10" />
            <asp:ListItem Text="11" Value="11" />
            <asp:ListItem Text="12" Value="12" />
        </asp:DropDownList>
        <asp:CustomValidator runat="server" ID="cvalExpireMonth" ControlToValidate="ddlExpMonth" ClientValidationFunction="expireDateValidation" Display="Dynamic" ErrorMessage="You have entered an invalid expiration date" CssClass="form-error" />
        <asp:RequiredFieldValidator ID="rfvExpMonth" runat="server" ControlToValidate="ddlExpMonth" ErrorMessage="Expiration Month is required" InitialValue="Expiration Month" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
    <div class="column inlineLabel med-12">
        <asp:Label runat="server" ID="lblExpYear" AssociatedControlID="ddlExpYear" Text="Expiration Year"></asp:Label>
        <asp:DropDownList runat="server" ID="ddlExpYear" required AppendDataBoundItems="true" ClientIDMode="Static">
            <asp:ListItem Value="Expiration Year" Text="Expiration Year"></asp:ListItem>
        </asp:DropDownList>
        <asp:CustomValidator runat="server" ID="cvalExpireDate" ControlToValidate="ddlExpYear" ClientValidationFunction="expireDateValidation" Display="Dynamic" ErrorMessage="You have entered an invalid expiration date" CssClass="form-error" />
        <asp:RequiredFieldValidator ID="rfvExpYear" runat="server" ControlToValidate="ddlExpYear" ErrorMessage="Expiration Year is required" InitialValue="Expiration Year" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="phNameOnCard">
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblNameOnCard" AssociatedControlID="txtNameOnCard" Text="Name on Card"></asp:Label>
        <input runat="server" id="txtNameOnCard" type="text" required />
        <asp:RequiredFieldValidator ID="rfvNameOnCard" runat="server" ControlToValidate="txtNameOnCard" Display="Dynamic" ErrorMessage="Name On Card is required" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="phSecurityCode">
    <div class="column inlineLabel">
        <asp:Label runat="server" ID="lblSecurityCode" AssociatedControlID="txtSecurityCode" Text="Security Code"></asp:Label>
        <input runat="server" id="txtSecurityCode" type="text" required clientidmode="Static" />
        <asp:RegularExpressionValidator ID="regSecurityCode" runat="server" ControlToValidate="txtSecurityCode" ValidationExpression="\d+" ErrorMessage="Only numeric values allowed" Display="Dynamic" CssClass="form-error" />
        <asp:CustomValidator runat="server" ID="cvalSecurityCode" ControlToValidate="txtSecurityCode" ClientValidationFunction="securityCodeValidation" OnServerValidate="cvalSecurityCodeValidation_ServerValidate" Display="Dynamic" ErrorMessage="Valid Security Code is required" CssClass="form-error" />
        <asp:RequiredFieldValidator ID="rfvSecurityCode" runat="server" ControlToValidate="txtSecurityCode" Display="Dynamic" ErrorMessage="Security Code is required" CssClass="form-error"></asp:RequiredFieldValidator>
    </div>
</asp:PlaceHolder>
<div class="column">
    <h1 class="h-textLg">
        <asp:Localize runat="server" Text="Billing Address"></asp:Localize>
    </h1>
</div>
<asp:UpdatePanel runat="server" ID="upBillingAddress" ChildrenAsTriggers="true">
    <ContentTemplate>
        <asp:PlaceHolder ID="phSameAsShipping" runat="server">
            <div class="column">
                <div class="formCheckBox">
                    <span>
                        <asp:CheckBox runat="server" ID="chkUseShippingAddress" OnCheckedChanged="chkShippingAddress_CheckedChanged" AutoPostBack="true" />
                        <asp:Label runat="server" ID="lblSameAsShipping" AssociatedControlID="chkUseShippingAddress" Text="Same as Shipping Address"></asp:Label>
                    </span>
                </div>
            </div>
        </asp:PlaceHolder>
        <div class="column inlineLabel lg-16">
            <asp:Label runat="server" ID="lblStreetAddress" AssociatedControlID="txtStreetAddress" Text="Street Address"></asp:Label>
            <input runat="server" id="txtStreetAddress" type="text" maxlength="255" required />
            <asp:RequiredFieldValidator runat="server" ID="rfvStreetAddress" ControlToValidate="txtStreetAddress" ErrorMessage="Street address is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>
        <div class="column inlineLabel lg-8">
            <asp:Label runat="server" ID="lblBldgAptNum" AssociatedControlID="txtBldgAptNum" Text="Bldg/Apt # (optional)"></asp:Label>
            <input runat="server" id="txtBldgAptNum" type="text" maxlength="255" />
        </div>
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblZip" AssociatedControlID="txtZip" Text="Zip/Postal Code"></asp:Label>
            <input runat="server" id="txtZip" type="text" maxlength="50" required />
            <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" ErrorMessage="Postal code is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regZip" runat="server" ControlToValidate="txtZip" ValidationExpression="(^\d{5}(-\d{4})?$)|(^[AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$)" ErrorMessage="Please enter a valid postal code" Display="Dynamic" CssClass="form-error"></asp:RegularExpressionValidator>
        </div>
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblCountry" AssociatedControlID="ddlCountry" Text="Country"></asp:Label>
            <asp:DropDownList runat="server" ID="ddlCountry" required AutoPostBack="False" />
            <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry" ErrorMessage="Country is required" InitialValue="" Display="Dynamic" CssClass="form-error" />
        </div>
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblState" AssociatedControlID="ddlState" Text="State"></asp:Label>
            <asp:DropDownList runat="server" ID="ddlState" required>
                <asp:ListItem Value="State" Text="State"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="ddlState" ErrorMessage="State is required" InitialValue="State" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>
        <div class="column inlineLabel">
            <asp:Label runat="server" ID="lblcity" AssociatedControlID="txtCity" Text="City"></asp:Label>
            <input runat="server" id="txtCity" type="text" maxlength="255" required />
            <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is required" Display="Dynamic" CssClass="form-error"></asp:RequiredFieldValidator>
        </div>

        <asp:PlaceHolder runat="server" ID="phSaveCard">
            <div class="column">
                <div class="formCheckBox">
                    <span>
                        <asp:CheckBox runat="server" ID="chkSaveCard" OnCheckedChanged="chkSaveCard_CheckedChanged" AutoPostBack="true" />
                        <asp:Label runat="server" ID="lblSaveCard" AssociatedControlID="chkSaveCard" Text="Save this card to your Payment Methods?"></asp:Label>
                    </span>
                </div>
            </div>
        </asp:PlaceHolder>
        <div runat="server" id="cardName" class="column inlineLabel">
            <asp:Label runat="server" ID="lblNickName" AssociatedControlID="txtNickName" Text="Payment Method Name"></asp:Label>
            <input runat="server" id="txtNickName" type="text" maxlength="255" required />
            <p class="formNote">
                <asp:Localize runat="server" Text='e.g. "My Bank Card" or "My Mastercard"'></asp:Localize>
            </p>
            <asp:RequiredFieldValidator ID="rfvNickName" runat="server" ControlToValidate="txtNickName" Display="Dynamic" ErrorMessage="Payment Method Name is required" CssClass="form-error"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="cvalNickname" Display="Dynamic" ErrorMessage="A unique Payment Method Name is required" CssClass="form-error" OnServerValidate="cvalNickname_ServerValidate" runat="server" />
        </div>

    </ContentTemplate>
</asp:UpdatePanel>
<asp:PlaceHolder runat="server" ID="cphDefaultScript">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            RegisterCountryDropdownEvents();
        });

        $(function () {
            RegisterCountryDropdownEvents();
        });

        function RegisterCountryDropdownEvents() {
            $("#<%=ddlCountry.ClientID%>").focus(function (e) {
                this.oldValue = this.value;
            });

            $("#<%=ddlCountry.ClientID%>").blur(function (e) {
                if (this.oldValue != this.value) setTimeout(__doPostBack(e.target.name, ''), 0);
            });
        }

        function securityCodeValidation(source, arguments) {
            if (arguments.IsValid) {
                var cardNumber = $('#txtCardNumber').val();

                if (cardNumber.startsWith("3"))//american express
                    arguments.IsValid = arguments.Value.length === 4;
                if (cardNumber.startsWith("4"))//visa
                    arguments.IsValid = arguments.Value.length === 3;
                if (cardNumber.startsWith("2") || cardNumber.startsWith("5"))//mastercard
                    arguments.IsValid = arguments.Value.length === 3;
                if (cardNumber.startsWith("6"))//discover
                    arguments.IsValid = arguments.Value.length === 3;
            }
        }

        function ccTypeValidation(source, arguments) {
            var cardNumber = $('#txtCardNumber').val();
            arguments.IsValid = (validCCtypes.indexOf(cardNumber.charAt(0)) > -1);
        }
    </script>
</asp:PlaceHolder>
<script>
    function expireDateValidation(source, args) {
        var day = "02";
        var month = $('#ddlExpMonth').val();
        var year = $('#ddlExpYear').val();
        var currentDate = new Date();
        var selectedDate = new Date(year + '-' + month + "-" + day);
        if (month != "Expiration Month" && year != "Expiration Year")
            args.IsValid = selectedDate.getTime() > currentDate.getTime();
    }
    var cardType = "001";
    var cardBrand = "Visa";
    function GetCardType(firstNumber) {
        switch (firstNumber) {
            case "3":
                cardBrand = "American Express";
                cardType = "003";
                break;
            case "4":
                cardBrand = "Visa";
                cardType = "001";
                break;
            case "2":
            case "5":
                cardBrand = "Mastercard";
                cardType = "002";
                break;
            case "6":
                cardBrand = "Discover";
                cardType = "004";
                break;
        }
        $("[id$='hfCardBrand']").val(cardBrand);
    }

    function ccNumberValidation(source, arguments) {
        var cardNumber = $('#txtCardNumber').val();
        arguments.IsValid = !isNaN(cardNumber);
    }
</script>

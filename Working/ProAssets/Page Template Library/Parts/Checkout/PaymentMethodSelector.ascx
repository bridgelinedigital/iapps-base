﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentMethodSelector.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Checkout.PaymentMethodSelector" %>

<%@ Import namespace="Bridgeline.iAPPS.Pro.API.Commerce.Payment" %>

<asp:Repeater runat="server" ID="rptPaymentMethods">
    <HeaderTemplate>
        <div class="infoAction">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="infoAction-item">
            <div class="infoAction-check">
                <input type="radio" id="ra0<%#Container.ItemIndex.ToString() %>" name="radPaymentMethodId" onchange="<%# Page.ClientScript.GetPostBackEventReference(lbtnSelect, ((CreditCardHelper)Container.DataItem).SavedCardId.ToString()) %>" />
                <label for="ra0<%#Container.ItemIndex.ToString() %>">Select</label>
            </div>
            <!--/.infoAction-check-->
            <div class="infoAction-title"> <%# ((CreditCardHelper)Container.DataItem).Nickname %> </div>
            <asp:PlaceHolder runat="server" Visible="true" ID="phNonStripe" >
                <div class="infoAction-info"><%# ((CreditCardHelper)Container.DataItem).GetCardType() %> ending in <%# ((CreditCardHelper)Container.DataItem).Last4Digits %></div>
            </asp:PlaceHolder>

            <!--/.infoAction-info-->
            <asp:LinkButton CssClass="infoAction-action infoAction-action--edit" runat="server" CausesValidation="false" CommandArgument="<%#((CreditCardHelper)Container.DataItem).SavedCardId%>"
                ID="editCustomerPaymentMethod" OnClick="editCustomerPaymentMethod_Click" Visible="false"> Edit </asp:LinkButton>
            <!--/.infoAction-action-->
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>

<p class="alert alert--yellow icon-attention" runat="server" id="noCardsAlert" visible="false">You currently have no cards in your account, please select a payment option above.</p>

<asp:LinkButton runat="server" ID="lbtnSelect" OnClick="lbtnSelect_Click" Visible="false">Select</asp:LinkButton>
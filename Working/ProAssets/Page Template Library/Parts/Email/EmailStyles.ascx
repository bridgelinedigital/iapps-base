﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailStyles.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Email.EmailStyles" %>

<meta name="viewport" content="width=device-width" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<style type="text/css">
    /* COLORS
    ========================================================================== */
    /*
     * Declare brand colors here as they may be used in some of the variables below.
     *
     * The default secondary and accent colors are populated with SASS color functions to
     * generate a 3 color analogous palette (hue values 30 degrees to either side on the color wheel).
     * If the client has a full color palette those hex values should be entered instead

     * 
     Just in case you want to mess with different color rules:
        Monochromatic color harmony maintains a constant value in the H and varies the S and V values.

        Complementary color harmony uses H values that are about 200 degrees apart (with variations
        in S and V as desired).

        Triadic color harmony uses 3 H values roughly 130 degrees apart (vary S and V to taste).

        Analogous color harmony uses 3 or more H values in a range of about 30 degrees plus and
        minus from your key color.

     */
    /*
     * Standard colors to convey messaging
    *
    * !!! These colors need to be dark enough to work as a background for white text
    */
    /*
     * Monochrome colors
     */
    /*
     * Text elements
     */
    /*
     * Generic border color for consistency
     */
    /*
      * Web Font / @font-face : BEGIN
      */
 
      <!--[if mso]>

    * {
      font-family: sans-serif !important;
    }

 
      <![endif]-->


    <!--[if !mso]>

 
     <![endif]-->

    /* CSS Reset */
    /* What it does: Remove spaces around the email design added by some email clients. */
    /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
    html {
      width: 100% !important;
      height: 100% !important;
      margin: 0 auto !important;
      padding: 0 !important;
    }

    body {
      width: 100% !important;
      height: 100% !important;
      margin: 0 !important;
      padding: 0 !important;
      background-color: #9ab4cb;
      /*premailer html attributes.
        Make sure these match up to any of the other styles above but don't delete them*/
      -premailer-bgcolor: #9ab4cb;
    }

    /* What it does: Stops email clients resizing small text. */
    * {
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
    }

    /* What it does: Centers email on Android 4.4 */
    div[style*='margin: 16px 0'] {
      margin: 0 !important;
    }

    /* What it does: Stops Outlook from adding extra spacing to tables. */
    table,
    td {
      mso-table-lspace: 0 !important;
      mso-table-rspace: 0 !important;
    }

    /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
    table {
      margin: 0 auto !important;
      table-layout: fixed !important;
      border-spacing: 0 !important;
      border-collapse: collapse !important;
    }

    table table table {
      table-layout: auto;
    }

    /* What it does: Uses a better rendering method when resizing images in IE. */
    img {
      -ms-interpolation-mode: bicubic;
    }

    /* What it does: Overrides styles added when Yahoo's auto-senses a link. */
    .yshortcuts a {
      border-bottom: none !important;
    }

    /* What it does: A work-around for iOS meddling in triggered links. */
    .mobile-link--footer a,
    a[x-apple-data-detectors] {
      text-decoration: underline !important;
      color: inherit !important;
    }

    /* Media Queries */
    @media screen and (max-width: 480px) {
      /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
      .fluid,
      .fluid-centered {
        width: 100% !important;
        max-width: 100% !important;
        height: auto !important;
        margin-right: auto !important;
        margin-left: auto !important;
      }
      /* And center justify these ones. */
      .fluid-centered {
        margin-right: auto !important;
        margin-left: auto !important;
      }
      /* What it does: Forces table cells into full-width rows. */
      .stack-column,
      .stack-column-center {
        display: block !important;
        width: 100% !important;
        max-width: 100% !important;
        direction: ltr !important;
      }
      /* And center justify these ones. */
      .stack-column-center {
        text-align: center !important;
      }
      /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
      .center-on-narrow {
        display: block !important;
        float: none !important;
        margin-right: auto !important;
        margin-left: auto !important;
        text-align: center !important;
      }
      table.center-on-narrow {
        display: inline-block !important;
      }
    }

    /* Page elements */
    /*center element that surrounds all content*/
    .center {
      width: 100%;
      background: #9ab4cb;
    }

    /*div element that surrounds all content*/
    .wrappingDiv {
      /*be careful about changing this width as sub-elemnt widths are based on it*/
      max-width: 680px;
      margin: auto;
    }

    /*table element that surrounds all content*/
    .wrappingTable {
      /*be careful about changing this width as sub-elemnt widths are based on it*/
      max-width: 680px;
      background-color: #fff;
      /*premailer html attributes.
        Make sure these match up to any of the other styles above but don't delete them*/
      -premailer-bgcolor: #fff;
    }

    .header {
      max-width: 680px;
      background-color: #fff;
      /*premailer html attributes.
        Make sure these match up to any of the other styles above but don't delete them*/
      -premailer-bgcolor: #fff;
      -premailer-align: center;
    }

    .header-content {
      padding: 20px 0;
      text-align: center;
      border-bottom: 1px solid #eaeaea;
    }

    .footer {
      max-width: 680px;
    }

    .footer-content {
      width: 100%;
      padding: 40px 10px;
      text-align: center;
      font-family: sans-serif;
      font-size: 12px;
      line-height: 18px;
      color: #fff;
      mso-height-rule: exactly;
    }

    .footer-link {
      text-decoration: underline;
      color: #fff;
    }

    /*button*/
    /* The cell that wraps the button link*/
    .buttonTable-td {
      text-align: center;
      border-radius: 3px;
      background: #9ab4cb;
    }

    /* The actual link itself*/
    .buttonTable-link {
      display: block;
      text-align: center;
      text-decoration: none;
      font-family: sans-serif;
      font-size: 13px;
      font-weight: bold;
      line-height: 1.1;
      border: 15px solid #9ab4cb;
      border-radius: 3px;
      background: #9ab4cb;
    }

    /*span that wraps the link content and ensures correct color*/
    .buttonTable-linkInner {
      color: #fff;
    }

    /* Progressive Enhancements */
    /* What it does: Hover styles for buttons */
    .buttonTable-td,
    .buttonTable-link {
      transition: all 100ms ease-in;
    }

    .buttonTable-td:hover,
    .buttonTable-link:hover {
      border-color: #748798 !important;
      background: #748798 !important;
    }

    /* Single, full-width hero image*/
    .hero-img {
      width: 100%;
      max-width: 680px;
    }

    /* Text and image with image aligned to the left */
    /* first inner container */
    .imgLeft-inner,
    .imgRight-inner {
      padding: 10px 0;
    }

    /* The inner table that wraps the image and text */
    .imgLeft-body,
    .imgRight-body {
      max-width: 660px;
    }

    /* the innermost td  that wraps the image and text */
    .imgLeft-bodyInner,
    .imgRight-bodyInner {
      padding: 10px 0;
      font-size: 0;
    }

    /* the div that wraps the image */
    .imgLeft-imgDiv,
    .imgRight-imgDiv {
      display: inline-block;
      width: 100%;
      min-width: 160px;
      max-width: 200px;
      margin: 0 -2px;
      vertical-align: top;
    }

    /* The final wrapping element of the image */
    .imgLeft-imgCell,
    .imgRight-imgCell {
      padding: 0 10px 10px 10px;
    }

    /* The image itself */
    .imgLeft-img,
    .imgRight-img {
      width: 100%;
      max-width: 200px;
      border: 0;
    }

    /* The div that holds the text */
    .imgLeft-textDiv,
    .imgRight-textDiv {
      display: inline-block;
      min-width: 320px;
      max-width: 66.66%;
      margin: 0 -2px;
      vertical-align: top;
    }

    /* The final wrapping element of the text */
    .imgLeft-textCell,
    .imgRight-textCell {
      padding: 10px 10px 0;
      text-align: left;
      font-family: sans-serif;
      font-size: 15px;
      line-height: 20px;
      color: #434343;
      mso-height-rule: exactly;
    }

    /*float the buttonTable left*/
    .imgLeft-textCell .buttonTable,
    .imgRight-textCell .buttonTable {
      float: left;
    }

    .imgLeft-heading,
    .imgRight-heading {
      color: #434343;
    }

    /*Single column text with CTA*/
    /*The td that contains the content*/
    .oneColText {
      padding: 40px;
      text-align: center;
      font-family: sans-serif;
      font-size: 15px;
      line-height: 20px;
      color: #434343;
      mso-height-rule: exactly;
    }

    /*Style the button*/
    .oneColText .buttonTable {
      margin: auto;
      /*premailer html attributes.
        Make sure these match up to any of the other styles above but don't delete them*/
      -premailer-align: center;
    }

    /* Use as an HR */
    .separator {
      width: 100%;
      height: 1px;
      margin: 0 0 0 0;
      padding-top: 10px;
      padding-bottom: 10px;
      border-bottom: 1px solid #eaeaea;
      background: none;
    }

    /*The short summary text that follows the subject line when an email is viewed in the inbox, but does not appear in the email body*/
    .preheaderText {
      display: none;
      overflow: hidden;
      max-width: 0;
      max-height: 0;
      font-family: sans-serif;
      font-size: 1px;
      line-height: 1px;
      opacity: 0;
      mso-hide: all;
    }

    /*Three column image and text*/
    /*The outer TD that wraps all the columns*/
    .threeCol-wrapper01 {
      padding: 10px 0;
      /*premailer html attributes.
        Make sure these match up to any of the other styles above but don't delete them*/
      -premailer-align: center;
    }

    /*The table that wraps all the columns*/
    .threeCol-wrapper02 {
      max-width: 660px;
    }

    /*The td that wraps all the columns*/
    .threeCol-wrapper03 {
      font-size: 0;
    }

    /*The div that wraps each column*/
    .threeCol-colWrap01 {
      display: inline-block;
      width: 100%;
      min-width: 220px;
      max-width: 33.33%;
      vertical-align: top;
      Margin: 0 -2px;
    }

    /*The td that wraps each column*/
    .threeCol-colWrap02 {
      padding: 10px 10px;
    }

    /*The table that makes the actual column*/
    .threeCol-column {
      text-align: left;
      font-size: 14px;
    }

    /*The img in each column*/
    .threeCol-img {
      width: 100%;
      max-width: 200px;
      border: 0;
    }

    /*The td that wraps the text in each column*/
    .threeCol-text {
      padding-top: 10px;
      font-family: sans-serif;
      font-size: 15px;
      line-height: 20px;
      color: #434343;
      mso-height-rule: exactly;
    }

    /*Two column image and text*/
    /*Table wrapping the columns*/
    .twoCol-wrapper01 {
      max-width: 660px;
    }

    /*td wrapping the columns*/
    .twoCol-wrapper02 {
      padding: 10px 0;
      font-size: 0;
    }

    /*div wrapping each column*/
    .twoCol-wrapper03 {
      display: inline-block;
      width: 100%;
      min-width: 200px;
      max-width: 330px;
      vertical-align: top;
      Margin: 0 -2px;
    }

    /*td wrapping each columns*/
    .twoCol-wrapper04 {
      padding: 10px 10px;
    }

    /*The table that makes the actual column*/
    .twoCol-column {
      text-align: left;
      font-size: 14px;
    }

    /*The img in each column*/
    .twoCol-img {
      width: 100%;
      max-width: 310px;
      border: 0;
    }

    /*The td that wraps the text in each column*/
    .twoCol-text {
      padding-top: 10px;
      font-family: sans-serif;
      font-size: 15px;
      line-height: 20px;
      color: #434343;
      mso-height-rule: exactly;
    }
</style>
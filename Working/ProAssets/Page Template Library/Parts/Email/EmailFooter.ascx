﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailFooter.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Email.EmailFooter" %>

<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" class="footer">
  <tr>
    <td class="footer-content">
      <a href="[ViewInBrowserLink]" class="footer-link">View as a Web Page</a>
      <br>
      <br>[SiteProperty:Title]
      <br><span class="mobile-link--footer">[SiteProperty:AddressLine1], [SiteProperty:City], [SiteProperty:StateCode], [SiteProperty:ZipCode]</span>
      <br><span class="mobile-link--footer">[SiteProperty:Phone]</span>
      <br>
      <br>
      <a class="footer-link" href="[Unsubscribe]">unsubscribe</a>
    </td>
  </tr>
</table>
﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="SkuSelector.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Catalog.SkuSelector" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.UI.PageParts.Catalog" %>
<asp:Repeater runat="server" ID="rptSkuOptions" OnItemDataBound="rptSkuOptions_ItemDataBound">
    <ItemTemplate>
        <div class="inlineLabel">
            <asp:Label runat="server" id="lblAttribute" AssociatedControlId="ddlAttributeValues"><%#((SkuSelectionAttribute)Container.DataItem).Attribute.Title %></asp:Label>
            <asp:DropDownList ID="ddlAttributeValues" runat="server" CssClass="sku-selector productTools-optionSelect" DataTextField="Value" DataValueField="Value">
            </asp:DropDownList>
            <asp:HiddenField ID="hfAttributeName" runat="server" Value='<%#((SkuSelectionAttribute)Container.DataItem).Attribute.Title %>'></asp:HiddenField>
        </div>
    </ItemTemplate>
</asp:Repeater>
<script>
    $(function() {
        $('.sku-selector').blur(function(e) {
            setTimeout(__doPostBack(e.target.name, ''), 0);
        });
    });
</script>

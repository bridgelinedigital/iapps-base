﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedProductsList.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Catalog.FeaturedProductsList" %>

<%@ Import Namespace="SiteSettings=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings" %>

<h2><%= FeatureName %></h2>
<div class="row row--tight productGrid">
    <asp:Repeater ID="rptProducts" runat="server" OnItemDataBound="rptProducts_ItemDataBound">
        <ItemTemplate>
            <div class="column sm-12 lg-6">
                <div class="productTile">
                    <div itemscope itemtype="http://schema.org/Product" class="productTile-wrapper">
                        <asp:PlaceHolder id="phProductFlag" runat="server" Visible="false">
                            <div class="productTile-message"><asp:Literal ID="ltlProductFlag" runat="server" /></div>
                        </asp:PlaceHolder>
                        <div class="productTile-image">
                            <img runat="server" id="imgProduct" itemprop="image" src="<%# ProductImageNotFoundMediumPath %>" />
                        </div>
                        <div class="productTile-info">
                            <h3 class="productTile-name"><a href='<%# Eval("URL") %>'><span itemprop="name"><%# Eval("Title") %></span></a></h3>
                            <p itemprop="description" class="productTile-description"><%# Eval("Description") %></p>
                            <asp:Placeholder runat="server" ID="phPriceInfo">
                                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="productTile-priceInfo <%#Boolean.Parse(Eval("PriceInfo.IsSale").ToString()) ? "productTile-priceInfo--hasAlt" : "" %>">
                                    <span class="productTile-price">
                                        <span itemprop="priceCurrency" content="<%= SiteSettings.Instance.CurrencyISOCode %>"><%= SiteSettings.Instance.CurrencySymbol %></span><span itemprop="price" content="<%# double.Parse(Eval("PriceInfo.ListPrice").ToString()).ToString("F") %>"><%# double.Parse(Eval("PriceInfo.ListPrice").ToString()).ToString("F") %></span>
                                    </span>
                                    <span class="productTile-altPrice">
                                        <span><%= SiteSettings.Instance.CurrencySymbol %></span><span><%# double.Parse(Eval("PriceInfo.CurrentPrice").ToString()).ToString("F") %></span>
                                    </span> 
                                </div>
                            </asp:Placeholder>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacetGroup.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Catalog.FacetGroup" %>

<h3 class="filters-subHeading"><%= this.categoryFacet.Title %></h3>
<asp:HiddenField runat="server" ID="hidCategoryFacetId" Value="<%#this.categoryFacet.FacetGuid.ToString() %>" />

<asp:Repeater runat="server" ID="rptFacetItems" OnItemCommand="rptFacetItems_ItemCommand" OnItemDataBound="rptFacetItems_ItemDataBound">
    <HeaderTemplate>
        <ul class="filters-list truncateList">
    </HeaderTemplate>

    <ItemTemplate>
        <li>
            <asp:LinkButton runat="server" ID="lbtnFacetItem" CommandName="AddFacet" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ValueId") %>'>
                <%#DataBinder.Eval(Container.DataItem,"Value") %> (<%#DataBinder.Eval(Container.DataItem,"Count") %>)
            </asp:LinkButton>
        </li>
    </ItemTemplate>

    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
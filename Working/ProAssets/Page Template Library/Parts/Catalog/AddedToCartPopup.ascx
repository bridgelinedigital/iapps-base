﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddedToCartPopup.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Catalog.AddedToCartPopup" %>

<div id="cart-popup" class="popup mfp-hide">
    <div class="addedToCart">
        <h2 class="addedToCart-heading"><%=AddedQty %> item<%=AddedQty > 1 ? "s" : "" %> added to cart</h2>
        <% if (AddedQty == 0)
            {  %>
        <asp:Label ID="lblAddedQtyZero" runat="server" CssClass="form-error" Text="Available stock is reserved in someone's cart."></asp:Label>
        <%  }  else if (SelectedQty != AddedQty)
            {  %>
        <asp:Label ID="lblQtyMismatch" runat="server" CssClass="form-error">We are not able to add the selected quantity <%=SelectedQty %> as we have only <%=AddedQty %> available for sale.</asp:Label>
        <%  }  %>

        <div class="cartItem">
            <figure class="cartItem-image">
                <img src='<%=ProductImages.Count == 0 ? ProductImageNotFoundSmallPath : ProductImages[0].URL %>' alt="">
            </figure>
            <div class="cartItem-firstSection">
                <% if (AddedSku != null)
                   {  %>
                <h3 class="cartItem-name"><%=AddedSku.Product.Title%></h3>
                <%  }  %>
                <ul id="Ul1" class="cartItem-infoList" runat="server" visible="false">
                    <li>Color: <strong>Red</strong></li>
                    <li>Size: <strong>XX-Large</strong></li>
                </ul>
            </div>
        </div>

        <div class="addedToCart-content">
            <dl class="addedToCart-cartList">
                <dt>Cart Total</dt>
                <dd><%=CurrentCart.Total.ToString("c")%></dd>
            </dl>
            <p>
                <a href="<%=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.CartURL %>" class="btn btn--full h-pushXSmBottom">Go to cart/checkout</a>
            </p>
        </div>
    </div>
</div>

<script>
    function showAddToCartPopup() {
        if ($('#cart-popup').length) {
            $.magnificPopup.open({
                items: {
                    src: '#cart-popup'
                },
                type: 'inline'
            });
        }
    }
</script>

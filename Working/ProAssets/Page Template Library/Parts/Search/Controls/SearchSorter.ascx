﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchSorter.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.SearchSorter" %>

<div class="fakeSelectWrap fakeSelectWrap--sm">
    <asp:Label runat="server" ID="lblSortBy" AssociatedControlID="ddlSortBy" CssClass="fakeSelectMask">Sort by</asp:Label>
    <asp:DropDownList runat="server" ID="ddlSortBy" CssClass="fakeSelect">
        <asp:ListItem Text="Price: Low to High" Value="Price Ascending"></asp:ListItem>
        <asp:ListItem Text="Price: High to Low" Value="Price Descending"></asp:ListItem>
        <asp:ListItem Text="Relevance" Value="Relevance" Selected="True"></asp:ListItem>
        <asp:ListItem Text="Newest" Value="CreatedDate Descending"></asp:ListItem>

    </asp:DropDownList>
</div>

<div class="fakeSelectWrap fakeSelectWrap--sm">
    <asp:Label runat="server" ID="lblPageSize" AssociatedControlID="ddlPageSize" CssClass="fakeSelectMask">Show</asp:Label>
    <asp:DropDownList runat="server" ID="ddlPageSize" CssClass="fakeSelect">
        <asp:ListItem Text="16" Value="16" Selected="True"></asp:ListItem>
        <asp:ListItem Text="24" Value="24"></asp:ListItem>
        <asp:ListItem Text="32" Value="32"></asp:ListItem>
    </asp:DropDownList>
</div>

<div class="fakeSelectWrap fakeSelectWrap--sm">
    <asp:LinkButton runat="server" ID="lbtnFilter" CssClass="btn btn--sm" OnClick="lbtnFilter_OnClick">Filter</asp:LinkButton>
</div>
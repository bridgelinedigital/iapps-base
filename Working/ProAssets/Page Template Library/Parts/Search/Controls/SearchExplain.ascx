﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchExplain.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.Page_Template_Library.Parts.Controls.SearchExplain" %>


<asp:Literal ID="ltlExplain" runat="server"><h4>Explain Releveancy</h4></asp:Literal>

<br /> <br />
<table style="margin-top: 20px;">
    <asp:Repeater ID="rptExplain" runat="server">
        <HeaderTemplate>
            <tr>
                <td>Id</td>
                <td>Description</td>
            </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Literal ID="ltlId" runat="server"></asp:Literal>
                </td>
                <td>
                    <asp:Literal ID="ltlDescription" runat="server"></asp:Literal>
                </td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
</table>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchRefinments.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.Page_Template_Library.Parts.Controls.SearchRefinments" %>



<asp:Repeater ID="rptRefinmentsHeader" runat="server">
    <ItemTemplate>
        <strong> <asp:Literal ID="ltlHeading" runat="server"></asp:Literal> </strong>
        <ul class="list-group">
            <asp:Repeater ID="rptRefinments" runat="server" >
                <ItemTemplate>

                    <li id="liValue" runat="server" class="list-group-item bg-success">
                        <asp:CheckBox ID="chkRefinment" runat="server"  OnCheckedChanged="CheckRefinment_Changed" AutoPostBack="true"/>
                        <asp:Literal ID="ltlRefin" runat="server"></asp:Literal>
                        <span class="badge" id="spBadge" runat="server"></span>

                    </li>
                    
                </ItemTemplate>

            </asp:Repeater>
        </ul>
    </ItemTemplate>
</asp:Repeater>
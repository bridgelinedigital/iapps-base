﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Refinments.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.Page_Template_Library.Parts.Controls.Refinments" %>



<asp:Repeater ID="rptRefinmentsHeader" runat="server">
    <ItemTemplate>
        <h3 id="h3Heading" runat="server" class="filters-subHeading"></h3>
        <ul class="filters-list truncateList">
            <asp:Repeater ID="rptRefinments" runat="server">
                <ItemTemplate>
                    <li>
                        <asp:LinkButton ID="lbFilter" runat="server" OnCommand="lbFilter_Command"></asp:LinkButton>
                        
                    </li>
                </ItemTemplate>

            </asp:Repeater>
        </ul>
    </ItemTemplate>
</asp:Repeater>
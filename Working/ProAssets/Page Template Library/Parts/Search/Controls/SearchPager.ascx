﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchPager.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.PageParts.Controls.SearchPager" %>


<asp:ListView ID="lvSearchPager" runat="server" ItemPlaceholderID="phPagerItem">
    <LayoutTemplate>
       
            <asp:LinkButton ID="lbPrevious" runat="server" CssClass="prev inactive" aria-label="Previous Page"></asp:LinkButton> 

            <asp:PlaceHolder ID="phPagerItem" runat="server" />

            <asp:LinkButton ID="lbNext" runat="server" CssClass="next" aria-label="Next Page"></asp:LinkButton>

       
    </LayoutTemplate>
    <ItemTemplate>
       
            <asp:LinkButton ID="lbPage" runat="server" />
       
    </ItemTemplate>
</asp:ListView>

                                      <%--  <a onclick="return false;" href="javascript:void(0)" class="prev inactive"></a> 
                                        <span class="current">1</span> <a href="javascript:void(0)">2</a> <a href="javascript:void(0)">3</a> <a href="javascript:void(0)">4</a> <a href="javascript:void(0)">5</a>
                                        <a href="javascript:void(0)" class="next"></a>--%>

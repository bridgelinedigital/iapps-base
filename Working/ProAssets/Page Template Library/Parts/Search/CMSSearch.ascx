﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CMSSearch.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.Page_Template_Library.Parts.Search.CMSSearch" %>


<div class="column med-7 lg-5">
    <div class="filters" id="divCMSFilters" runat="server">
        <h2 class="filters-heading">Refine Your Results</h2>
        <iappspro:refinments id="ctlRefinments" runat="server" israngerefinment="false"  />
    </div>
    <!--/.filters-->
</div>
<div class="column med-17 lg-19">
    <iappspro:selectedrefinments id="selectedRefinments" runat="server" />

    <div id="dvResult" runat="server" class="resultsTools">
        <div class="resultsTools-results"><span id="spDocCount" runat="server" class="resultsTools-count"></span><a class="resultsTools-mobileFilterDrawerToggle drawerToggle" data-for="filters-mobile" href="javascript:void(0)">Refine Results</a></div>
        <!--end .results-->
        <div class="resultsTools-options">
            <div class="pagination">
                <div class="pageNumbers">
                    <iappspro:searchpager id="searchPager1" runat="server" pagestoshow="10" />
                </div>
                <!--/.pageNumbers-->
            </div>
            <!--/.pagination-->
        </div>
        <!--/.displayOptions-->
    </div>
    <!--/.resultsPanel-->

    <asp:Repeater ID="rptResult" runat="server">
        <ItemTemplate>
            <div class="searchItem">
                <div class="searchItem-body">
                    <h2 class="searchItem-heading">
                        <asp:HyperLink ID="hlUrl" runat="server"></asp:HyperLink></h2>
                    <p class="searchItem-blurb">
                        <asp:Literal ID="ltlDesc" runat="server"></asp:Literal>
                    </p>
                </div>
                <!--/.searchItem-body-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div id="dvBottomPagination" runat="server" class="pagination">
        <div class="pageNumbers">
            <iappspro:searchpager id="searchPager" runat="server" pagestoshow="10" />
        </div>
        <!--/.pageNumbers-->
    </div>
    <!--/.pagination-->
</div>

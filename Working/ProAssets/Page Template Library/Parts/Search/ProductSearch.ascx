﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSearch.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.Page_Template_Library.Parts.Search.ProductSearch" %>


    <div class="column med-7 lg-5">
        <div class="filters">
            <h2 class="filters-heading">Refine Your Results</h2>
           <iappspro:refinments id="ctlProductRefinments" runat="server" israngerefinment="false" />
             <iappspro:refinments id="ctlProductRangeRefinments" runat="server" israngerefinment="true" />
        </div>
    </div>
    <div class="column med-17 lg-19">
        <iappspro:selectedrefinments id="productSelectedRefinments" runat="server" />

        <div id="dvResult" runat="server" class="resultsTools">
            <div class="resultsTools-results"><span id="spDocCount" runat="server" class="resultsTools-count"></span><a class="resultsTools-mobileFilterDrawerToggle drawerToggle" data-for="filters-mobile" href="javascript:void(0)">Refine Results</a></div>
            <!--end .results-->
            <div class="resultsTools-options">
                <iappspro:SearchSorter id="wseppSorter" runat="server" />

            </div>
            <!--/.displayOptions-->
        </div>
        <!--/.resultsPanel-->
        <div class="row row--tight productGrid">
            <asp:Repeater ID="rptResult" runat="server">
                <ItemTemplate>
                    <div class="column sm-12 lg-6">
                        <div class="productTile">
                            <div itemscope itemtype="http://schema.org/Product" class="productTile-wrapper">
                                <div id="dvSale" runat="server" class="productTile-message" visible="false">Sale</div>
                                <div class="productTile-image">
                                    <img itemprop="image" runat="server" id="imgProduct" src="<%# ProductImageNotFoundMediumPath %>" />
                                </div>
                                <!-- /.productTile-image -->
                                <div class="productTile-info">
                                    <h3 class="productTile-name">
                                        <a id="hlUrlForTitle" runat="server">
                                            <span itemprop="name">
                                                <asp:Literal ID="ltlTitle" runat="server"></asp:Literal>
                                            </span>
                                        </a>
                                    </h3>
<%--                                    <div itemprop="description" class="productTile-description">
                                        <p>
                                            <asp:Literal ID="ltlDesc" runat="server"></asp:Literal>
                                        </p>
                                    </div>--%>
                                    <!-- /.productTile-description -->
                                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="productTile-priceInfo  productTile-priceInfo--hasAlt">
                                        <span id="spSalesPrice" runat="server" class="productTile-price" visible="false"></span>
                                        <!-- /.productTile-price -->
                                        <span class="productTile-altPrice">
                                            <span itemprop="priceCurrency" content="<%= Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencyISOCode", "USD") %>"><%= Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencySymbol", "$") %></span>
                                            <span itemprop="price">
                                                <asp:Literal ID="ltlCurrentPrice" runat="server"></asp:Literal>
                                            </span>
                                        </span>
                                        <!--/.productTile-altPrice-->
                                    </div>
                                    <!-- /.productTile-priceInfo -->
                                </div>
                                <!-- /.productTile-info -->
                            </div>
                            <!-- /.productTile-wrapper -->
                        </div>
                        <!-- /.productTile -->
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblNoRecord" runat="server" Visible="false"></asp:Label>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="dvBottomPagination" runat="server" class="pagination">
            <div class="pageNumbers">
                <iappspro:searchpager id="productSearchPager" runat="server" PageSize="16" />
            </div>
            <!--/.pageNumbers-->
        </div>
        <!--/.pagination-->
    </div>

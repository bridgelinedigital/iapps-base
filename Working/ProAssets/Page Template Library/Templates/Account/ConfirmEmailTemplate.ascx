﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmEmailTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.ConfirmEmailTemplate" %>

<%@ Import Namespace="Bridgeline.FW.Commerce.Base.SiteSettings" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />
    <main>
        <div class="wrapper">
            <div class="contained">
                <div class="row">
                    <div class="column med-20 centered-med lg-16">
                        <div class="island">
                            <h1>Confirm Email</h1>
                            <asp:PlaceHolder ID="phSuccess" runat="server" Visible="false">
                                <div class="alert alert--success icon-check h-pushBottom">
                                    Your account has been confirmed successfully.
                                </div>
                                <a href="<%= SiteSettings.Instance.CustomersLoginPageURL %>" class="btn">Login</a>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="phError" runat="server" Visible="false">
                                <div class="alert alert--red icon-attention h-pushBottom">
                                    An error has occurred validating your account. Please contact an administrator for assistance.
                                </div>
                            </asp:PlaceHolder>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

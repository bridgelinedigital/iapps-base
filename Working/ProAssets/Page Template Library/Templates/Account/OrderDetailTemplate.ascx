﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderDetailTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.OrderDetailTemplate" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />
    <main>
        <div class="section">
            <div class="contained">
                <p><asp:HyperLink ID="hlBack" runat="server" CssClass="backLink">Back to Orders</asp:HyperLink></p>
                <h1>Order #<%=CurrentOrder.OrderNumber %></h1>
                <p><asp:HyperLink ID="hlReturnOrder" runat="server" CssClass="btn icon-refresh" Visible="false">Return </asp:HyperLink>
                <asp:HyperLink ID="hlClaimOrder" runat="server" CssClass="btn icon-refresh" Visible="false">Claim</asp:HyperLink></p>
                <div class="row">
                    <div class="column lg-18">
                        <div class="cartItemContainer">
                            <!-- Order Items -->
                            <asp:Repeater runat="server" ID="rptOrderItems" OnItemDataBound="rptOrderItems_ItemDataBound">
                                <ItemTemplate>
                                    <div class="cartItem">
                                        <figure class="cartItem-image"><img runat="server" id="imgProduct" title="product" alt="product" /></figure>
                                        <div class="cartItem-firstSection">
                                            <h2 class="cartItem-name"><a href="<%#((CartItemModel)Container.DataItem).SKU.Product.URL%>"><%#((CartItemModel)Container.DataItem).SKU.Product.Title%></a></h2>
                                            <asp:Repeater runat="server" ID="rptItemAttributes">
                                                <HeaderTemplate><ul class="cartItem-infoList"></HeaderTemplate>
                                                <ItemTemplate>
                                                    <li><%#DataBinder.Eval(Container.DataItem, "Name") %>: <strong><%#DataBinder.Eval(Container.DataItem, "Value") %></strong></li>
                                                </ItemTemplate>
                                                <FooterTemplate></ul></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <!--/.cartItem-firstSection-->
                                        <div class="cartItem-secondSection">
                                            <ul class="cartItem-infoList">
                                                <li>Qty: <strong><%#((CartItemModel)Container.DataItem).Quantity%></strong></li>
                                            </ul>
                                        </div>
                                        <!--/.cartItem-secondSection-->
                                        <div class="cartItem-cap"><span class="cartItem-price"><%#(((CartItemModel)Container.DataItem).Price * ((CartItemModel)Container.DataItem).Quantity).ToString("c")%><span><%#((CartItemModel)Container.DataItem).Price.ToString("c")%><%#((CartItemModel)Container.DataItem).SKU.Measure %></span></span></div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="column lg-6">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Subtotal:</th>
                                    <td><%= CurrentOrder.Total.ToString("c") %></td>
                                </tr>
                                <tr>
                                    <th>Tax:</th>
                                    <td><%= CurrentOrder.TotalTax.ToString("c") %></td>
                                </tr>
                                <tr>
                                    <th>Handling:</th>
                                    <td><%=this.TotalHandling.ToString("c") %></td>
                                </tr>
                                <tr>
                                    <th>Shipping:</th>
                                    <td><%=(CurrentOrder.TotalShippingCharge - this.TotalHandling).ToString("c") %></td>
                                </tr>
                                <%if (CurrentOrder.TotalDiscount > 0) { %>
                                <tr>
                                    <th>Discount / Coupons:</th>
                                    <td>- <%= CurrentOrder.TotalDiscount.ToString("c")%></td>
                                </tr>
                                <%} %>
                                <tr>
                                    <th>Total:</th>
                                    <td><span class="h-textLg"><%=  CurrentOrder.GrandTotal.ToString("c")  %></span></td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- Shipping Information -->
                        <asp:PlaceHolder runat="server" ID="phShipping">
                            <h2 class="h-textLg h-pushSmBottom">Shipping Info</h2>
                            <asp:Repeater runat="server" ID="rptOrderShippings" OnItemDataBound="rptOrderShippings_ItemDataBound">
                                <ItemTemplate>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Shipping</th>
                                                <td><%#((OrderShippingModel)Container.DataItem).ShippingOption.ShipperName%> - <%#((OrderShippingModel)Container.DataItem).ShippingOption.ShippingOptionName%></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Address</th>
                                                <td>
                                                    <strong><%# ((OrderShippingModel)Container.DataItem).OrderShippingAddress.Type.ToString() %></strong><br />
                                                    <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.FirstName%> <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.LastName%><br />
                                                    <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.AddressLine1%><br />
                                                    <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.City%>  <%# ((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.State.StateCode%>, <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.Zip%><br />
                                                    <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.Country.CountryCode%><br />
                                                    <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.Phone%>
                                                </td>
                                            </tr>

                                            <asp:Repeater runat="server" ID="rptTrackingNumbers">
                                                <HeaderTemplate>
                                                    <tr>
                                                        <th scope="row">Tracking #</th>
                                                        <td>
                                                </HeaderTemplate>

                                                <ItemTemplate>
                                                    <%#Eval("TrackingNumber")%><br />
                                                </ItemTemplate>

                                                <FooterTemplate>
                                                        </td>
                                                    </tr>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </asp:PlaceHolder>

                        <!-- Payment Information -->
                        <asp:PlaceHolder runat="server" ID="phPayment">
                            <h2 class="h-textLg h-pushSmBottom">Payment Info</h2>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th scope="row">Name on Card</th>
                                        <td><%=CreditCardInfo.NameOnCard %></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Card Type</th>
                                        <td><%=CreditCardInfo.GetCardType() %></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Card Number</th>
                                        <td>****<%=CreditCardInfo.Last4Digits %></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Expiration Date</th>
                                        <td><%=CreditCardInfo.ExpirationMonth %>/<%=CreditCardInfo.ExpirationYear %></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Billing Address</th>
                                        <td>
                                            <%=CreditCardInfo.BillingAddress.AddressLine1%><br>
                                            <%=CreditCardInfo.BillingAddress.City%>, <%=CreditCardInfo.BillingAddress.State.StateCode%> <%=CreditCardInfo.BillingAddress.Zip%><br>
                                            <%=CreditCardInfo.BillingAddress.Country.CountryCode%> <br />
                                            <%=CreditCardInfo.BillingAddress.Phone%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:PlaceHolder>
                    </div>
                </div>
                <p><asp:HyperLink ID="hlBackFooter" runat="server" CssClass="backLink">Back to Orders</asp:HyperLink></p>
            </div>
        </div>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WishlistTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.WishlistTemplate" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />
    <main>
        <div class="section">
            <div class="contained">
                <asp:MultiView runat="server" ID="mvWishlists" ActiveViewIndex="0">
                    <asp:View runat="server" ID="vwWishlistList">
                        <iAppsPro:ProfileNav ID="wseppProfileNav" runat="server" />

                        <div class="navOptions navOptions--sm navOptions--medHorizontal"> <span class="icon-cog"></span> <asp:LinkButton runat="server" ID="lbtnNewWishlist" CssClass="icon-plus" OnClick="lbtnNewWishlist_Click">Add new wishlist</asp:LinkButton> </div>

                        <iAppsPro:WishlistList ID="wseppWishlistList" runat="server" />
                    </asp:View>
                    <asp:View runat="server" ID="vwWishlistForm">
                        <p><a href="<%=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.AddNewWishListPageURL %>" class="backLink">Back to Wishlists</a></p>
                        <iAppsPro:WishlistDetail ID="wseppWishlistDetail" runat="server" />
                        <p><a href="<%=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.AddNewWishListPageURL %>" class="backLink">Back to Wishlists</a></p>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />

    <iAppsPro:AddedToCartPopup runat="server" ID="wseppAddedToCartPopup" ImageSizeToDisplay="Small" />
</div>
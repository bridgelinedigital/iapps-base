﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.ProfileTemplate" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" ShowBreadcrumb="false" />
    <main>
        <div class="section">
            <div class="contained">
                <div runat="server" id="divRegSuccess" class="alert alert--success icon-check h-pushBottom" visible="false">Thank you, your registration has been completed.</div>
                <iAppsPro:ProfileNav ID="wseppProfileNav" runat="server" />
                <div class="row">
                    <div class="column med-20 centered-med lg-16">
                        <div class="customerForm">
                            <h1 class="customerForm-heading">Profile Details</h1>
                            <div runat="server" id="divGeneralError" class="alert alert--red icon-attention" visible="false"><asp:literal runat="server" ID="litError"></asp:literal></div>
                            <iAppsPro:CustomerProfileForm ID="wseppCustomerProfile" runat="server" />
                            <div class="formFooter">
                                <asp:LinkButton runat="server" ID="btnSave" CssClass="btn icon-check">Save</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                <!--/.column--> 
                </div>
                <!--/.row--> 
            </div>
        <!--/.contained-->
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

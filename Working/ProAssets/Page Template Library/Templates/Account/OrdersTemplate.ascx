﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrdersTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.OrdersTemplate" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />

    <main>
        <div class="section">
            <div class="contained">
                <iAppsPro:ProfileNav ID="wseppProfileNav" runat="server" />

                <iAppsPro:CustomerOrderListing ID="wseppOrderListing" runat="server" />
            </div>
        </div>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>
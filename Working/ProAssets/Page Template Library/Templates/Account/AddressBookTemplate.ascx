﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressBookTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.AddressBookTemplate" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />
    <main>
        <div class="section">
            <div class="contained">
                <asp:MultiView runat="server" ID="mvAddressBook" ActiveViewIndex="0">
                    <asp:View runat="server" ID="vwAddressList">
                        <iAppsPro:ProfileNav ID="wseppProfileNav" runat="server" />

                        <div class="navOptions navOptions--sm navOptions--medHorizontal"> <span class="icon-cog"></span> <asp:LinkButton runat="server" ID="lbtnNewAddress" CssClass="icon-plus" OnClick="lbtnNewAddress_Click">Add new address</asp:LinkButton> </div>

                        <iAppsPro:CustomerAddressList ID="wseppAddressList" runat="server" />
                    </asp:View>
                    <asp:View runat="server" ID="vwAddressDetail">
                        <div class="row">
                            <div class="column med-20 centered-med lg-16">
                                <p><asp:LinkButton runat="server" ID="btnBackToAddressBookTop" class="backLink" OnClick="btnBackToAddressBook_Click" CausesValidation="false">Back to Address Book</asp:LinkButton></p>

                                <div class="customerForm">
                                    <h1 class="customerForm-heading">Add/Edit Address</h1>

                                    <div class="row row--XTight">
                                        <iAppsPro:CustomerAddressForm ID="wseppAddressForm" runat="server" />
                                    </div>

                                    <div class="formFooter">
                                        <asp:LinkButton runat="server" ID="btnCancelAddress" CausesValidation="false" Text="Cancel" CssClass="btn icon-close"></asp:LinkButton> <asp:LinkButton runat="server" ID="btnSaveAddress" Text="Save" CssClass="btn btn--primary icon-check"></asp:LinkButton>
                                    </div>
                                </div>

                                <p><asp:LinkButton runat="server" ID="btnBackToAddressBookBottom" class="backLink" OnClick="btnBackToAddressBook_Click" CausesValidation="false">Back to Address Book</asp:LinkButton></p>
                            </div>                            
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>
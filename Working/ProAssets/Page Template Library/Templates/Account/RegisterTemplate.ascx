﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisterTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.RegisterTemplate" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />
    <main>
        <div class="wrapper">
            <div class="contained">
                <div class="row">
                    <div class="column med-20 centered-med lg-16">
                        <div class="island">
                            <h1>Register</h1>
                            <div runat="server" id="divGeneralError" class="alert alert--red icon-attention h-pushBottom" visible="false"><asp:literal runat="server" ID="litError"></asp:literal></div>
                            <div runat="server" id="divSuccessMessage" class="alert alert--success icon-check h-pushBottom" visible="false"><asp:literal runat="server" ID="litSuccessMessage"></asp:literal></div>
                            <iAppsPro:CustomerRegistrationForm ID="wseppRegisterForm" runat="server" />

                            <div class="formFooter">
                                <asp:LinkButton runat="server" ID="lbtnRegister" CssClass="btn icon-check">Submit</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                <!--/.column--> 
                </div>
                <!--/.row--> 
            </div>
        <!--/.contained-->
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentMethodsTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.PaymentMethodsTemplate" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />
    <main>
        <div class="section">
            <div class="contained">
                <asp:MultiView runat="server" ID="mvPaymentMethod" ActiveViewIndex="0">
                    <asp:View runat="server" ID="vwPaymentMethodList">
                        <iAppsPro:ProfileNav ID="wseppProfileNav" runat="server" />

                        <div class="navOptions navOptions--sm navOptions--medHorizontal"> <span class="icon-cog"></span> <asp:LinkButton runat="server" ID="lbtnNewPaymentMethod" CssClass="icon-plus" OnClick="lbtnNewAddress_Click">Add new payment method</asp:LinkButton> </div>

                        <iAppsPro:CustomerPaymentMethodList ID="wseppPaymentMethodList" runat="server" />
                    </asp:View>
                    <asp:View runat="server" ID="vwPaymentMethodDetail">
                        <div class="row">
                            <div class="column med-20 centered-med lg-16">
                                <p><asp:LinkButton runat="server" ID="btnBackLinkTop" class="backLink" OnClick="btnBackLink_Click" CausesValidation="false">Back to Payment Methods</asp:LinkButton></p>
                                <div class="customerForm">
                                    <h1 class="customerForm-heading">Add/Edit Payment Method</h1>
                                    <asp:PlaceHolder ID="phErrorMessage" runat="server" Visible="false">
                                        <p class="alert alert--danger icon-attention">
                                            <asp:Literal ID="ltErrorMessage" runat="server"></asp:Literal>
                                        </p>
                                    </asp:PlaceHolder>
                                    <iAppsPro:CheckoutPaymentMethodForm ID="wseppPaymentMethodForm" runat="server" />
                                    <div class="formFooter">
                                        <asp:LinkButton runat="server" ID="btnCancelPaymentMethod" CausesValidation="false" Text="Cancel" CssClass="btn icon-close"></asp:LinkButton> <asp:LinkButton runat="server" ID="lbtnNext" Text="Save" CssClass="btn icon-check"></asp:LinkButton>
                                    </div>
                                </div>
                                <p><asp:LinkButton runat="server" ID="btnBackLinkBottom" class="backLink" OnClick="btnBackLink_Click" CausesValidation="false">Back to Payment Methods</asp:LinkButton></p>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>
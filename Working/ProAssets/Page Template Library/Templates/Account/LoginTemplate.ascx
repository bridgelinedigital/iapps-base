﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.LoginTemplate" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />

    <main>
        <div class="section">
            <div class="contained">
                <div class="row">
                    <div class="column med-14">
                        <iAppsPro:StandardLogin ID="wseppStandardLogin" runat="server" ValidationGroup="StandardLogin" />
                    </div>
                    <div class="column med-10">
                        <iAppsPro:ExpressLogin ID="wseppExpressLogin" runat="server" ValidationGroup="ExpressLogin" />
                    </div>
                </div>
            </div>
        </div>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

<iAppsPro:ForgotPassword ID="wseppForgotPassword" runat="server" />


﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReturnOrderTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Account.ReturnOrderTemplate" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" ShowBreadcrumb="false" />
    <main>
        <div class="section">
            <div class="contained">

                <p><asp:HyperLink ID="hlBack" runat="server" CssClass="backLink" Text="Back to Orders" /></p>
                <h1> <asp:Literal runat="server" ID="ltlHeading" Text="Return Items" ></asp:Literal> | Order #<asp:Literal ID="ltlOrderNumber" runat="server" /></h1>

                <asp:MultiView ID="mvReturnOrder" runat="server">
                    <asp:View ID="vwOrderReturnItems" runat="server">
                        <ul class="status-bar">
                            <li class="current"><asp:LinkButton runat="server" OnClick="lbOrderReturnItems_Click" Text="Choose item to Return" ID="lbvwReturn1" /></li>
                            <li><asp:LinkButton runat="server" OnClick="lbReviewReturn_Click" Text="Review Return Information" ID="lbvwReturn2" /></li>
                            <li>
                                <% if(this.IsSystemUser) { %>
                                    <asp:LinkButton ID="lnkEditZoneReturnAuthorization" runat="server" OnClick="lnkEditZoneReturnAuthorization_Click" Text="Generate Return Authorization"  />
                                <%} else { %>
                                <a href=""><asp:Literal ID="ltlvwReturn3" runat="server" Text="Generate Return Authorization"></asp:Literal> </a>
                                <%} %>
                            </li>
                        </ul>
                        <div class="row">
                            <div class="column h-soft h-hardTop">
                                <FWZoneControls:PageZoneContainer ID="PageZoneContainer3" runat="server"></FWZoneControls:PageZoneContainer>
                            </div>
                            <div class="column">
                                <div class="cartItemContainer">
                                    <asp:Repeater ID="rptrReturnItems" runat="server" OnItemDataBound="rptrReturnItems_ItemDataBound">
                                        <ItemTemplate>
                                            <div class="cartItem cartItem—return">
                                                <asp:HiddenField ID="hfOrderItemId" runat="server" Value='<% Eval("OrderItemId") %>' />
                                                <asp:HiddenField ID="hfSkuId" runat="server" />
                                                <div class="cartItem-selection">
                                                    <asp:CheckBox ID="cbReturnItem" runat="server"></asp:CheckBox>
                                                    <%--<input id="cbReturnItem" runat="server"  type="checkbox" name="cartItems" />--%>
                                                    <label for='<%#Container.FindControl("cbReturnItem").ClientID%>'><span id="spCbText" runat="server">Return</span></label>
                                                    <asp:HiddenField runat="server" ID="hfValidatorId" Value='<%#Container.FindControl("rfvReturnReason").ClientID %>' />
                                                </div>
                                                <figure class="cartItem-image"><img id="imgProduct" runat="server" /></figure>
                                                <div class="cartItem-firstSection">
                                                    <h3 class="cartItem-name"><asp:HyperLink ID="hlProduct" runat="server"></asp:HyperLink></h3>
                                                    <ul class="cartItem-infoList">
                                                    <li>Shipped: <strong><asp:Literal runat="server" ID="ltlShipDate"></asp:Literal></strong></li>
                                                    <li>Quantity: <strong><asp:Literal ID="ltlShippingQuantity" runat="server" /></strong></li>
                                                    </ul>
                                                </div>
                                                <!--/.cartItem-firstSection-->
                                                <div class="cartItem-secondSection">
                                                    <ul class="cartItem-infoList">
                                                        <li><strong>Qty to Return</strong></li>
                                                        <li>
                                                            <div class="productTools-counter h-flushRight">
                                                                <div class="productTools-counter-control productTools-counter-control--subtract"></div>
                                                                <input id="txtQuantity" runat="server" type="text" class="productTools-counter-display" value="1">
                                                                <div class="productTools-counter-control productTools-counter-control--add"></div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <ul class="cartItem-infoList">
                                                        <li><strong><asp:Literal ID="ltlReason" runat="server" Text="Reason for Return"></asp:Literal></strong></li>
                                                        <li>
                                                            <div class="row row--XTight">
                                                                <div class="column">
                                                                    <asp:DropDownList ID="ddlReturnReason" runat="server" AppendDataBoundItems="true" CssClass="productTools-optionSelect">
                                                                        <asp:ListItem Selected="True" Text=" - Make A Selection - " Value="0"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlClaimReason" runat="server" Visible="false" AppendDataBoundItems="true" CssClass="productTools-optionSelect">
                                                                        <asp:ListItem Selected="True" Text=" - Make A Selection - " Value="0"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rfvReturnReason" runat="server" ControlToValidate="ddlReturnReason" Display="Dynamic" InitialValue="0"
                                                                        ErrorMessage="Reason for return is required" CssClass="form-error" ValidationGroup="ChooseItems"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <div class="row">
                                        <div class="column">
                                            <asp:CustomValidator runat="server" ClientIDMode="Static" ID="cvalOneSelected" ValidationGroup="ChooseItems" CssClass="alert alert--danger icon-attention" ErrorMessage="You must select at least one item for return" ClientValidationFunction="IsOneItemSelected"></asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div class="column med-12 h-soft h-pushTop">
                                        <label for="textfield1"><strong>Additional Comments</strong></label>
                                        <textarea id="txtAdditionalComments" runat="server" name="" rows="7" placeholder="comments..."></textarea>
                                        <div class="formFooter h-flushBottom">
                                            <asp:LinkButton runat="server" OnClick="lbContinue_Click" ValidationGroup="ChooseItems" ID="lbContinueChooseItem" ClientIDMode="Static" CssClass="btn icon-check">Continue</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            <!--/.cartItemContainer-->
                            </div>

                            <!--/.column-->
                        </div>        
                        <script>
                            $(document).ready(function () {
                                $('.cartItem input[type="checkbox"]').each(
                                    function () {
                                        var enableValidator = $(this).is(':checked');
                                        var hiddenValidator = $(this).parent().find('input[type=hidden]');
                                        if (hiddenValidator.length > 0) {
                                            ValidatorEnable($('#' + hiddenValidator[0].value)[0], enableValidator);
                                        }
                                    });

                                $('.cartItem input[type="checkbox"]').change(function () {
                                    var enableValidator = $(this).is(':checked');
                                    var hiddenValidator = $(this).parent().find('input[type=hidden]');
                                    if (hiddenValidator.length > 0) {
                                        ValidatorEnable($('#' + hiddenValidator[0].value)[0], enableValidator);
                                    }
                                    var validatorChooseOne = $("#cvalOneSelected");
                                    if (validatorChooseOne != null && validatorChooseOne.length > 0) {
                                        ValidatorValidate(validatorChooseOne[0]);
                                    }
                                });
                            });
                            function IsOneItemSelected(source, arguments) {
                                var oneSelected = false;
                                $('.cartItem input[type="checkbox"]').each(
                                    function () {
                                        if ($(this).is(':checked'))
                                            oneSelected = true;
                                    });
                                arguments.IsValid = oneSelected;
                            }
                        </script>
                    </asp:View>
                    <asp:View ID="vwReviewReturn" runat="server">
                        <ul class="status-bar">
                            <li class="done"><asp:LinkButton runat="server" OnClick="lbOrderReturnItems_Click" Text="Choose Items To Return" ID="lbReviewReturn1" /></li>
                            <li class="current"><asp:LinkButton runat="server" OnClick="lbReviewReturn_Click" Text="Review Return Information"  ID="lbReviewReturn2"/></li>
                            <li>
                                <% if(this.IsSystemUser) { %>
                                    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="lnkEditZoneReturnAuthorization_Click" Text="Generate Return Authorization" />
                                <%} else { %>
                                    <a href=""><asp:Literal runat="server" ID="lbReviewReturn3" Text="Generate Return Authorization"></asp:Literal> </a>
                                <%} %>
                            </li>
                        </ul>
                        <div class="row">
                             <div class="column h-soft h-hardTop">
                                <FWZoneControls:PageZoneContainer ID="PageZoneContainer2" runat="server"></FWZoneControls:PageZoneContainer>
                            </div>
                            <div class="column">
                            <div class="cartItemContainer">
                                <asp:Repeater ID="rptrReviewItems" runat="server" OnItemDataBound="rptrReviewItems_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="cartItem cartItem—return cartItem—return--review">
                                            <figure class="cartItem-image"><img id="img1" runat="server" /></figure>
                                            <div class="cartItem-firstSection">
                                                <h3 class="cartItem-name"><asp:HyperLink ID="hlProduct" runat="server" /></h3>
                                                <ul class="cartItem-infoList">
                                                    <li>Shipped: <strong><asp:Literal runat="server" ID="ltlShipDate"></asp:Literal></strong></li>
                                                    <li>Quantity: <strong><asp:Literal ID="ltlShippingQuantity" runat="server" /></strong></li>
                                                </ul>
                                            </div>
                                            <div class="cartItem-secondSection">
                                                <ul class="cartItem-infoList">
                                                    <li><strong>Qty to Return:</strong></li>
                                                    <li><asp:Literal ID="ltlReturnQuantity" runat="server" /></li>
                                                </ul>
                                                <ul class="cartItem-infoList">
                                                    <li><strong>Reason for Return:</strong></li>
                                                    <li><asp:Literal ID="ltlReturnReason" runat="server" /></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <div class="column med-12 h-soft h-pushTop">
                                    <label for="textfield1"><strong>Additional Comments Around Return</strong></label>
                                    <textarea id="txtAdditionalCommentsReview" runat="server" name="" rows="7" disabled="disabled"></textarea>
                                    <div class="formFooter h-flushBottom">
                                        <asp:LinkButton runat="server" OnClick="lbContinue_Click" CssClass="btn icon-check">Continue</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <!--/.cartItemContainer-->
                            </div>

                            <!--/.column-->
                        </div>
                    </asp:View>
                    <asp:View ID="vwReturnAuthorization" runat="server">
                        <ul class="status-bar">
                             <% if(this.IsSystemUser) { %>
                            <li class="done"><asp:LinkButton runat="server" OnClick="lbOrderReturnItems_Click" Text="Choose Items To Return" ID="lbReturnAuth1" /></li>
                            <li class="done"><asp:LinkButton runat="server" OnClick="lbReviewReturn_Click" Text="Review Return Information" ID="lbReturnAuth2" /></li>
                            <li class="current">                               
                                <asp:LinkButton ID="LinkButton3" runat="server" OnClick="lnkEditZoneReturnAuthorization_Click" Text="Generate Return Authorization" />                                
                            </li>
                            <%} else { %>
                            <li class="done"><a href=""><asp:Literal ID="ltlReturnAuth1" runat="server" Text="Items To Return"> </asp:Literal> </a></li>
                            <li class="done"><a href=""><asp:Literal ID="ltlReturnAuth2" runat="server" Text="Review Return Information"> </asp:Literal> </a></li>
                            <li class="current"><a href=""><asp:Literal ID="ltlReturnAuth3" runat="server" Text="Generate Return Authorization"> </asp:Literal> </a></li>
                            <%} %>
                        </ul>
                        <div class="row">
                            <div class="column">
                                <h2 class="h-h3 h-flushBottom"><asp:Literal ID="ltlReturnAuthorizationInfo" runat="server" Text="Return Authorization Information"> </asp:Literal> </h2>
                                <hr>
                                <div class="islet">
                                    <dl>
                                    <dt><strong> <asp:Literal ID="ltlRAMReferenceNumber" runat="server" Text="RMA Reference Number"></asp:Literal> </strong></dt>
                                    <dd><asp:literal runat="server" id="ltlRMANumber">xxxxx</asp:literal></dd>
                                    <dt><strong><asp:Literal ID="ltlRMAStatus" runat="server" Text="RMA Status"></asp:Literal> </strong></dt>
                                    <dd><asp:Literal ID="ltlStatusText" runat="server" Text="Awaiting Items for Return" ></asp:Literal> </dd>
                                    </dl>
                                </div>
                                <asp:PlaceHolder ID="plInstruction" runat="server">
                                <h2 class="h-h3 h-flushBottom">Instructions for Return</h2>
                                <hr>
                                <div class="islet">
                                    <FWZoneControls:PageZoneContainer ID="PageZoneContainer1" runat="server"></FWZoneControls:PageZoneContainer>
                                </div>
                                <div class="islet">
                                    <div class="row">
                                        <div class="column med-6">
                                            <ul class="list--bare h-softXSmTop">
                                            <li>Returns Department</li>
                                            <li><%if(ReturnWarehouse != null) { %><%=ReturnWarehouse.Address.AddressLine1 %><%} %></li>
                                            <li><%if(ReturnWarehouse != null) { %><%=String.Format("{0}, {1}  {2}", ReturnWarehouse.Address.City, ReturnWarehouse.Address.State.StateCode, ReturnWarehouse.Address.Zip) %><%} %></li>
                                            </ul>
                                        </div>
                                        <div class="column med-18">
                                            <a href="javascript:window.print()" class="btn icon-printer h-floatLeft h-clearBoth h-flushTop h-pushSmBottom">Print Return Receipt</a>
                                            <asp:HyperLink ID="hlShippingLabel" runat="server" CssClass="btn icon-printer h-floatLeft h-clearBoth h-pushSmBottom">Print Shipping Label</asp:HyperLink>                        
                                        </div>
                                    </div>
                                </div>
                                    </asp:PlaceHolder>
                                <h2 class="h-h3 h-flushBottom">Items being refunded</h2>
                                <hr>
                                <div class="islet">
                                    <div class="cartItemContainer">
                                        <asp:Repeater ID="rptrItemsAuthorization" runat="server" OnItemDataBound="rptrReviewItems_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="cartItem cartItem—return cartItem—return--review">
                                                    <figure class="cartItem-image"><img id="img2" runat="server" /></figure>
                                                    <div class="cartItem-firstSection">
                                                        <h3 class="cartItem-name"><asp:HyperLink ID="hlProduct" runat="server" /></h3>
                                                        <ul class="cartItem-infoList">
                                                            <li>Shipped: <strong><asp:Literal runat="server" ID="ltlShipDate"></asp:Literal></strong></li>
                                                            <li>Quantity: <strong><asp:Literal ID="ltlShippingQuantity" runat="server" /></strong></li>
                                                        </ul>
                                                    </div>
                                                    <div class="cartItem-secondSection">
                                                        <ul class="cartItem-infoList">
                                                            <li><strong>Qty to Return:</strong></li>
                                                            <li><asp:Literal ID="ltlReturnQuantity" runat="server" /></li>
                                                        </ul>
                                                        <ul class="cartItem-infoList">
                                                            <li><strong>Reason for Return:</strong></li>
                                                            <li><asp:Literal ID="ltlReturnReason" runat="server" /></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <!--/.cartItemContainer-->
                                </div>
                                <!--/.islet-->
                            </div>
                            <!--/.column-->
                        </div>
                    </asp:View>
                    <asp:View ID="vwOrderNotEligible" runat="server">
                        <p>Order not eligible for return</p>
                    </asp:View>
                </asp:MultiView>

                </div>
        <!--/.contained-->
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>


<script type="text/javascript">
    
    $(document).ready(function() {

      $('.cartItem-selection label').click(function(){
        if (!$(this).siblings('input').is(":checked")) {
          $(this).closest('.cartItem—return').addClass('row-selected');
        }
        else {
          $(this).closest('.cartItem—return').removeClass('row-selected');
        }
      });

      $('.cartItem-selection input').each(function() {
        if ( $(this).is(":checked") ) {
          $(this).closest('.cartItem—return').addClass('row-selected');
        }
      });
    });
  </script>
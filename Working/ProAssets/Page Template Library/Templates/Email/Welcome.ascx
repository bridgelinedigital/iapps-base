﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Welcome.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Email.Welcome" %>

<iAppsPro:EmailStyles id="ctlStyles" runat="server" />

<center class="center">
    <FWControls:FWTextContainer ID="fwTxtPreHeader" runat="server" />

    <div class="wrappingDiv">
        <!--[if (gte mso 9)|(IE)]>
        <table cellspacing="0" cellpadding="0" border="0" width="680" align="center">
            <tr>
                <td>
        <![endif]-->
        <iAppsPro:EmailHeader id="ctlHeader" runat="server" />
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" class="wrappingTable">
        <tr>
            <td>
                <FWControls:FWXMLContainer runat="server" id="fwxmlHero"></FWControls:FWXMLContainer>
                <FWControls:FWXMLContainer runat="server" id="fwxmlOnColText"></FWControls:FWXMLContainer>

                <FWControls:FWTextContainer runat="server" id="fwtxtSeparator1"></FWControls:FWTextContainer>
                <FWControls:FWXMLContainer runat="server" id="FWXMLContainer1"></FWControls:FWXMLContainer>
            </td>
        </tr>
        </table>
        <iAppsPro:EmailFooter id="ctlFooter" runat="server" />
        <!--[if (gte mso 9)|(IE)]>
                </td>
            </tr>
        </table>
        <![endif]-->
    </div>

</center>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Newsletter.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Email.Newsletter" %>

<iAppsPro:EmailStyles id="ctlStyles" runat="server" />

<center class="center">
    <FWControls:FWTextContainer ID="fwTxtPreHeader" runat="server" />

    <div class="wrappingDiv">
        <!--[if (gte mso 9)|(IE)]>
        <table cellspacing="0" cellpadding="0" border="0" width="680" align="center">
            <tr>
                <td>
        <![endif]-->
        <iAppsPro:EmailHeader id="ctlHeader" runat="server" />
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" class="wrappingTable">
        <tr>
            <td>
                <FWControls:FWXMLContainer runat="server" id="fwxmlOneColText"></FWControls:FWXMLContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtSeparator1"></FWControls:FWTextContainer>

                <FWControls:FWXMLContainer runat="server" id="fwxmlImgLeft"></FWControls:FWXMLContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtSeparator2"></FWControls:FWTextContainer>

                <FWControls:FWXMLContainer runat="server" id="fwxmlImgRighty"></FWControls:FWXMLContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtSeparator3"></FWControls:FWTextContainer>

                <FWControls:FWXMLContainer runat="server" id="fwxmlImgLeft2"></FWControls:FWXMLContainer>
            </td>
        </tr>
        </table>
        <iAppsPro:EmailFooter id="ctlFooter" runat="server" />
        <!--[if (gte mso 9)|(IE)]>
                </td>
            </tr>
        </table>
        <![endif]-->
    </div>

</center>

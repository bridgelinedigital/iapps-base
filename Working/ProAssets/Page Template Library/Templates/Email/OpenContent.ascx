﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OpenContent.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Email.OpenContent" %>

<iAppsPro:EmailStyles id="ctlStyles" runat="server" />

<center class="center">
    <FWControls:FWTextContainer ID="fwTxtPreHeader" runat="server" />

    <div class="wrappingDiv">
        <!--[if (gte mso 9)|(IE)]>
        <table cellspacing="0" cellpadding="0" border="0" width="680" align="center">
            <tr>
                <td>
        <![endif]-->
        <iAppsPro:EmailHeader id="ctlHeader" runat="server" />
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" class="wrappingTable">
        <tr>
            <td>
                <FWControls:FWXMLContainer runat="server" id="fwxmlContentDef1"></FWControls:FWXMLContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtContentText1"></FWControls:FWTextContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtSeperator1"></FWControls:FWTextContainer>

                <FWControls:FWXMLContainer runat="server" id="fwxmlContentDef2"></FWControls:FWXMLContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtContentText2"></FWControls:FWTextContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtSeperator2"></FWControls:FWTextContainer>

                <FWControls:FWXMLContainer runat="server" id="fwxmlContentDef3"></FWControls:FWXMLContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtContentText3"></FWControls:FWTextContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtSeperator3"></FWControls:FWTextContainer>

                <FWControls:FWXMLContainer runat="server" id="fwxmlContentDef4"></FWControls:FWXMLContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtContentText4"></FWControls:FWTextContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtSeperator4"></FWControls:FWTextContainer>

                <FWControls:FWXMLContainer runat="server" id="fwxmlContentDef5"></FWControls:FWXMLContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtContentText5"></FWControls:FWTextContainer>
                <FWControls:FWTextContainer runat="server" id="fwtxtSeperator5"></FWControls:FWTextContainer>
            </td>
        </tr>
        </table>
        <iAppsPro:EmailFooter id="ctlFooter" runat="server" />
        <!--[if (gte mso 9)|(IE)]>
                </td>
            </tr>
        </table>
        <![endif]-->
    </div>

</center>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextOnly.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Email.TextOnly" %>


<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
    <tr>
        <td>
            <FWControls:FWTextContainer runat="server" id="fwtxtContent"></FWControls:FWTextContainer>
        </td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
    <tr>
        <td>
            <br />[SiteProperty:Title]
            <br /><span>[SiteProperty:AddressLine1], [SiteProperty:City], [SiteProperty:StateCode], [SiteProperty:ZipCode]</span>
            <br /><span>[SiteProperty:Phone]</span>
            <br />
            <br />
            <a href="[Unsubscribe]">unsubscribe</a>
        </td>
    </tr>
</table>

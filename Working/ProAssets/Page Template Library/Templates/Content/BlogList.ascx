﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BlogList.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Content.BlogList" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />

    <main>
        <div class="section">
            <div class="contained">    
                <div class="row">
					<div class="column med-7 lg-5">
                        <div class="filters">
							<FWControls:FWBlogCategoryContainer ID="fwBlogCategories" runat="server" FwContainerId="42c408cf-b944-4d7f-93d3-69db58893238" />
							<iAppsPro:BlogArchiveList runat="server" id="wseppArchivesList" />
                        </div>
					</div>

                    <div class="column med-17 lg-19">
						<CLControls:PageTitle runat="server" ID="cltitle" ControlTag="h1" />
						<p id="P1" runat="server" visible="false"><span class="tag is-closeable">Applied Filter</span> <span class="tag is-closeable">Applied Filter</span></p>

                        <FWControls:FWBlogContainer runat="server" ID="fwBlogListing" PageSize="5" PageButtonCount="5" PrevLinkText="" NextLinkText="" FwContainerId="bd63118b-d7ca-4cf2-a048-86ad1288e4c8" />
                    </div>
                </div>
            </div>
            <!--/.contained--> 
        </div>
          <!--/.wrapper--> 
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>
<script>
    $(document).ready(function(){
	    truncateList(5);
    });
</script>

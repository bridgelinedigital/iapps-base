﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BlogPost.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Content.BlogPost" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />

    <main>
        <div class="section">
            <div class="contained">
                <div class="row">
					<div class="column med-7 lg-5">
                        <div class="filters">
							<FWControls:FWBlogCategoryContainer ID="fwBlogCategories" runat="server" />
							<iAppsPro:BlogArchiveList runat="server" id="wseppArchivesList" />
                        </div>
					</div>
                	<div class="column med-17 lg-19">
						<asp:PlaceHolder ID="phBlogListTop" runat="server">
                            <p>
                                <a class="backLink" href="/<%= BlogListingUrl %>">Back to Blog List</a>
						    </p>
                        </asp:PlaceHolder>
						<p class="h-hideMedUp">
                            <a class="drawerToggle icon-menu" data-for="filters-mobile">Topics/Previous Posts</a>
						</p>
                        <FWControls:FWBlogPostContainer runat="server" ID="fwblogPost" FwContainerId="fa6ccad9-979d-426d-8658-8743ab1cd4ed"></FWControls:FWBlogPostContainer>
                        <FWControls:FWCommentsContainer id="comments" runat="server" EnableContextMenu="true"
                            objecttype="Post">
                            <FWCaptchaStyle FontName="Verdana" ImageWidth="200" ImageHeight="34" MinFontSize="17" MaxFontSize="20" XValue="50" YValue="0" />
                        </FWControls:FWCommentsContainer>
                        <asp:PlaceHolder ID="phBlogListBottom" runat="server">
                            <p>
                                <a class="backLink" href="/<%= BlogListingUrl %>">Back to Blog List</a>
						    </p>
                        </asp:PlaceHolder>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>
<script>
    $(document).ready(function(){
	    truncateList(5);
    });
</script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Homepage.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Content.Homepage" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />

    <main>
        <h1 class="h-visuallyHidden"><%= CurrentSite.Title %></h1>
        <FWZoneControls:PageZoneContainer ID="fwpzcMainContent" runat="server"></FWZoneControls:PageZoneContainer>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

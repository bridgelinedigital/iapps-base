﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Interior.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Content.Interior" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />

    <main>
        <iAppsPro:BreadcrumbNav runat="server" ID="ctlBreadcrumbNav" />
        <iAppsPro:PageTitle runat="server" ID="ctlPageTitle" />
        <FWZoneControls:PageZoneContainer ID="PageZoneContainer1" runat="server"></FWZoneControls:PageZoneContainer>       
        
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

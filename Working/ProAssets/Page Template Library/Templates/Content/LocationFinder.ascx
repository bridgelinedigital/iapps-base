﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationFinder.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Content.LocationFinder" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />

    <main>
        <iAppsPro:BreadcrumbNav runat="server" ID="ctlBreadcrumbNav" />

        <div class="section h-hard section--contrastLight locationFinder">
            <div class="locationFinder-content">
                <h3>Find Locations Within</h3>
                <div class="locationFinder-proximity">
                    <div class="fakeSelectWrap locationFinder-fakeSelectWrap">
                        <asp:Label ID="lblRadius" AssociatedControlID="ddlRadius" CssClass="fakeSelectMask" runat="server">5 miles</asp:Label>
                        <asp:DropDownList ID="ddlRadius" ClientIDMode="Static" CssClass="fakeSelect" runat="server" >
                            <asp:ListItem Value="5" Text="5 mi"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10 mi"></asp:ListItem>
                            <asp:ListItem Value="15" Text="15 mi"></asp:ListItem>
                            <asp:ListItem Value="20" Text="20 mi"></asp:ListItem>
                            <asp:ListItem Value="25" Text="25 mi"></asp:ListItem>
                            <asp:ListItem Value="50" Text="50 mi"></asp:ListItem>
                            <asp:ListItem Value="100" Text="100 mi"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <p>of</p>
                    <asp:TextBox ID="txtAddress" ClientIDMode="Static" CssClass="locationFinder-input" placeholder="Zip or Address" runat="server" />
                </div>

                <p class="separator">OR</p>
                <h3 class="locationFinder-heading">Search Along A Route</h3>
                <asp:TextBox ID="txtAddressStart" CssClass="locationFinder-input" ClientIDMode="Static" MaxLength="100" placeholder="Starting address" runat="server"  />
                <asp:TextBox ID="txtAddressEnd" CssClass="locationFinder-input" ClientIDMode="Static" MaxLength="100" placeholder="Ending Address" runat="server"  />
                <p>
                    <asp:HyperLink ID="btnSearch" CssClass="btn" runat="server" ClientIDMode="Static" Text="Find Locations" NavigateUrl="javascript:;" data-loading-text="Searching..." />
                </p>
                <span id="errors" style="color: red; font-weight: bold"></span>
            </div>

            <div class="locationFinder-map" id="locMap">
                <div id="map-canvas" data-map-lat="39.5" data-map-lng="-98.4" data-map-zoom="4" style="height: 463px;"></div>
            </div>
        </div>

        <div class="section locationResults">
            <div class="contained">
                <h3 class="locationResults-heading" style="display:none">Locations</h3>
                <div class="locations-list"></div>
            </div>
        </div>

    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

<script type="text/javascript">
    $(document).keydown(function (event) {
        var keyCode = (event.keyCode ? event.keyCode : event.which);
        if (keyCode == 13) {
            $('#btnSearch').trigger('click');
            return false;
        }
    });
    $(document).ready(function()
    {
        if ($('#txtAddress').val() != '')
            $('#btnSearch').trigger('click');
    });
</script>

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/script%20library/front-end/RouteBoxer.js"></script>
<script type="text/javascript" src="/script%20library/front-end/locations-list.js"></script>
<script type="text/javascript" src="/script%20library/front-end/locations-map.js"></script>
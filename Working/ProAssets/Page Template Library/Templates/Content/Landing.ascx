﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Landing.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Content.Landing" %>


<div class="pageWrap">
    <iAppsPro:HeaderMainStripped id="Header" runat="server" />

    <main>
        <FWZoneControls:PageZoneContainer ID="PageZoneContainer1" runat="server"></FWZoneControls:PageZoneContainer>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.Page_Template_Library.Templates.Content.SearchTemplate " %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<link href="https://jquery-ui-bootstrap.github.io/jquery-ui-bootstrap/css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
<script>   
    function SendProductClickToGA(contentType, itemId) {
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            "event": "select_content",
            "content_type": contentType,
            "item_id": itemId
        });
    }
</script>
<div class="pageWrap">

    <iappspro:headermain id="Header" runat="server" />

    <main>
        <iappspro:breadcrumbnav runat="server" id="ctlBreadcrumbNav" />

        <div class="section h-softLgEnds">
            <div class="contained">
                <div class="row">
                    <div class="column lg-16">
                        <h1 class="searchBox-heading">Search Results for: <span>
                            <asp:Literal ID="ltlSearchText" runat="server"></asp:Literal> </span>
                        </h1>
                        <div class="searchBox searchBox--hiVis">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="searchBox-textField" ClientIDMode="Static" aria-label="Search"> </asp:TextBox>
                            <a class="searchBox-submit" id="searchText-submit"></a>
                        </div>
                        <!--end .searchBox-->
                    </div>
                </div>
                <div class="row suggest" >
                    Did you mean: <a id="hlSuggest"></a>
                </div>
                <div id="dvTabs" runat="server" class="row">
                    <div class="column">
                        <nav class="navHorizontal navHorizontal--hiVis">
                            <ul class="searchTabs">
                                <li class="selected licommerce"><a href="javascript:void(0)">Products (<asp:Literal ID="ltlProductCount" runat="server"></asp:Literal>
                                    )</a> </li>
                                <li class="licms"><a href="javascript:">Content (<asp:Literal ID="ltlCMSCount" runat="server"></asp:Literal>)</a> </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row divProduct" id="divProduct" runat="server">
                    <iappspro:productsearch id="productSearch" runat="server"></iappspro:productsearch>
                </div>
                <div class="row divCms" id="divCMS" runat="server">
                    <iappspro:cmssearch id="cmsSearch" runat="server"></iappspro:cmssearch>
                </div>
            </div>
        </div>

    </main>

    <iappspro:footermain id="Footer" runat="server" />
    <asp:HiddenField ID="hdnSelectedTab" runat="server"  ClientIDMode="Static"/>
</div>

<script>

    var productEnum = {
        CMS: 'CMS',
        Commerce: 'Commerce',
        CMSAndCommerce: 'CMSAndCommerce'
    };
   
    $(document).ready(function () {

         $('#txtSearch').keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                submitSiteSearch($(this).val());
            }
        });

         $('#searchText-submit').click(function () {
            submitSiteSearch($('#txtSearch').val());
        });


        truncateList(5);
    });

    $(document).ready(function () {


        if (searchControl == productEnum.Commerce)
            SetTabSelected(productEnum.Commerce);
        else if (searchControl == productEnum.CMS)
            SetTabSelected(productEnum.CMS);
        else if (searchControl == productEnum.CMSAndCommerce) {
            var tabselectedCalled = false;
            $('.searchTabs').on("click", "li", function () {
                if (!$(this).hasClass('selected')) {
                    if ($(this)[0].innerText.toLowerCase().indexOf('content') != -1) {
                        tabselectedCalled = true;
                        SetTabSelected(productEnum.CMS);
                    }
                    else {
                        SetTabSelected(productEnum.Commerce);
                        tabselectedCalled = true;
                    }
                }
            });
            if (!tabselectedCalled && !selectedTab)
                SetTabSelected(productEnum.Commerce);
            else if (!tabselectedCalled)
                SetTabSelected(selectedTab);
        }
    

        function SetTabSelected(selectedTab)
        {
            $(".divCms").show();
            $(".divProduct").show();
            
            if (selectedTab == productEnum.CMS)
                CMS();
            else
                Commerce();

            BindSuggestion(selectedTab);
        }
    });

    function CMS()
    {
       
        if (!$(".licms").hasClass("selected"))
            $(".licms").addClass('selected');

        if ($(".licommerce").hasClass("selected"))
            $(".licommerce").removeClass('selected');

        $(".divProduct").hide();
        $("#hdnSelectedTab").val(productEnum.CMS);

        if (autoCompleteEnabled) {
            getAutoComplete('FrontEnd');
        }
    }

    function Commerce() {
        if (!$(".licommerce").hasClass("selected"))
            $(".licommerce").addClass('selected');

        if ($(".licms").hasClass("selected"))
            $(".licms").removeClass('selected');

        $(".divCms").hide();
        $("#hdnSelectedTab").val(productEnum.Commerce);

        if (autoCompleteEnabled) {
            getAutoComplete('Product');
        }
    }

    function BindSuggestion(selectedTab) {
        var bind = false;
        var didYouMean = " ";
        var suggestionLink = '<%=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.GetProductSearchPageUrl() %>?q=';

        if (suggestionEnabled) {
            if (selectedTab == productEnum.CMS) {
                if (cmsSuggestion != '') {
                    bind = true;
                    suggestionLink += escape(cmsSuggestion);
                    didYouMean += cmsSuggestion;
                }
            }
            else if (productSuggestion != '') {
                bind = true;
                suggestionLink += escape(productSuggestion);
                didYouMean += productSuggestion;
            }
        }

        if (bind) {
            $('.suggest').show();
            $('#hlSuggest').text(didYouMean);
            $('#hlSuggest').attr("href", suggestionLink);
        }
        else
            $('.suggest').hide();
    }

    function getAutoComplete(indexType) {
       $("#txtSearch").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/api/search/GetAutoComplete",
                    dataType: "json",
                    data: {
                        text: request.term,
                        IndexType: indexType
                    },
                    success: function (data) {
                        if(data != null)
                            response(data.options);
                    },
                    error: function (request, status, error) {
                        console.log(error);
                    }
                });
            },
            minLength: 2
        });
    }

</script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="T03-Generic-SideNav.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Content.T03_Generic_SideNav" %>


<%@ Register Src="~/Page Template Library/Parts/Controls/Header.ascx" TagName="Header" TagPrefix="uc" %>
<%@ Register Src="~/Page Template Library/Parts/Controls/Footer.ascx" TagName="Footer" TagPrefix="uc" %>



<div class="pageWrap">
    <uc:Header id="Header" runat="server" />

    <main>
        <div class="wrapper">
            <div class="contained">
              <div class="row">
                <div class="column med-8 lg-6"> 
                  <CLControls:CLHierarchicalNav ID="navSide" runat="server" XsltFileName="SideNav.xslt" SiteMapProvider="CommerceSite" Depth="2" />
                </div>
                <!--/.column-->
                <div class="column med-16 lg-18">
                  <FWZoneControls:PageZoneContainer ID="PageZoneContainer1" runat="server"></FWZoneControls:PageZoneContainer>
                </div>
                <!--/.column--> 
              </div>
              <!--/.row--> 
      
            </div>
            <!--/.contained--> 
          </div>



                                
    </main>

    <uc:Footer id="Footer" runat="server" />
</div>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InteriorDynamic.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Content.InteriorDynamic" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />

    <main>
        <iAppsPro:BreadcrumbNav runat="server" ID="ctlBreadcrumbNav" />
        <iAppsPro:PageTitle runat="server" ID="ctlPageTitle" />
           <FWControls:FWTextContainer ID="fwTxtDynamic" runat="server" />        
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

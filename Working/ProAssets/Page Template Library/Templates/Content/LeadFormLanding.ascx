﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeadFormLanding.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Content.LeadFormLanding" %>

<div class="pageWrap">
    <iAppsPro:HeaderMainStripped id="Header" runat="server" />

    <main>
        <FWZoneControls:PageZoneContainer ID="PageZoneContainer4" runat="server"></FWZoneControls:PageZoneContainer>
        <div class="section">
            <div class="contained">
                <div class="row">
                    <div class="column med-12">
                        <FWZoneControls:PageZoneContainer ID="PageZoneContainer2" runat="server"></FWZoneControls:PageZoneContainer>
                    </div>
                    <div class="column med-12">
                        <FWZoneControls:PageZoneContainer ID="PageZoneContainer3" runat="server"></FWZoneControls:PageZoneContainer>
                    </div>
                </div>
            </div>
        </div>
        <FWZoneControls:PageZoneContainer ID="PageZoneContainer1" runat="server"></FWZoneControls:PageZoneContainer>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
  
</div>

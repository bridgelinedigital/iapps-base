﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductListingTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Catalog.ProductListingTemplate" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<%@ Import Namespace="SiteSettings=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />
    <main>
        <FWZoneControls:PageZoneContainer ID="PageZoneContainer1" runat="server"></FWZoneControls:PageZoneContainer>                

        <div class="section">
            <div class="contained">
                <div class="row">
                    <div class="column med-7 lg-5">
                        <div class="filters">
                            <asp:Repeater runat="server" ID="rptFacets" OnItemDataBound="rptFacets_ItemDataBound">
                                <HeaderTemplate>
                                    <h2 class="filters-heading">Refine Your Results</h2>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <iAppsPro:FacetGroup ID="wseppFacetGroup" runat="server" OnFacetAdded="wseppFacetGroup_FacetAdded" CategoryFacet="<%# Container.DataItem %>" />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="column med-17 lg-19">
                        <h1><%=NavCategoryTitle %></h1>
                        <p>
                            <asp:Repeater runat="server" ID="rptSelectedFacets" OnItemCommand="rptSelectedFacets_ItemCommand">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbSelectedFacet" runat="server" CommandArgument='<%# Eval("ValueId") %>' Text='<%# Eval("Value") %>' CommandName="RemoveFacet" CssClass="tag is-closeable"></asp:LinkButton>
                                </ItemTemplate>
                                <SeparatorTemplate> </SeparatorTemplate>
                            </asp:Repeater>
                        </p>
                        <div class="resultsTools  h-pushBottom">
                            <div class="resultsTools-results">
                                <span class="resultsTools-count"><%=TotalProducts %> result<%=TotalProducts > 1 ? "s" : "" %></span>
                                <a class="resultsTools-mobileFilterDrawerToggle drawerToggle" data-for="filters-mobile" href="javascript:void(0)">Refine Results</a>
                            </div>
                            <div class="resultsTools-options">
                                <iAppsPro:Sorter ID="wseppSorter" runat="server" />
                            </div>
                        </div>
                        <div class="row row--tight productGrid">
                            <asp:Repeater runat="server" ID="rptProducts" OnItemDataBound="rptProducts_ItemDataBound">
                                <ItemTemplate>
                                    <div class="column sm-12 lg-6">
                                        <div class="productTile">
                                            <div itemscope itemtype="http://schema.org/Product" class="productTile-wrapper">
                                                <%--if product on sale display message--%>
                                                <asp:PlaceHolder id="phProductFlag" runat="server" Visible="false">
                                                    <div class="productTile-message"><asp:Literal ID="ltlProductFlag" runat="server" /></div>
                                                </asp:PlaceHolder>
                                                <div class="productTile-image">
                                                    <img itemprop="image" runat="server" id="imgProduct" src="<%# ProductImageNotFoundMediumPath %>" alt="<%#((ProductModel)Container.DataItem).Title %>"/>
                                                </div>
                                                <div class="productTile-info">
                                                    <h2 class="productTile-name">
                                                        <% if (GTMEnabled) {%>
                                                        <a onclick="onProductClick('<%#((ProductModel)Container.DataItem).ProductCode %>','<%#((ProductModel)Container.DataItem).URL %>')">
                                                        <%}else{%>
                                                            <a href="<%#((ProductModel)Container.DataItem).URL %>">
                                                        <%}%>
                                                            <span itemprop="name"><%#((ProductModel)Container.DataItem).Title %></span></a></h2>
                                                    <div itemprop="description" class="productTile-description"><%#((ProductModel)Container.DataItem).Description %></div>
                                                    <div class="productTile-description"><%#((ProductModel)Container.DataItem).ShortDescription %></div>
                                                    <asp:PlaceHolder id="phPrice" runat="server">
                                                        <%--If on sale, add css class  'productTile-priceInfo--hasAlt' to div--%>
                                                        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="productTile-priceInfo <%#((ProductModel)Container.DataItem).PriceInfo.IsSale ? "productTile-priceInfo--hasAlt" : "" %>">
                                                            <span class="productTile-price">
                                                                <span itemprop="priceCurrency" content="<%= SiteSettings.Instance.CurrencyISOCode %>"><%= SiteSettings.Instance.CurrencySymbol %></span><span itemprop="price" content="<%# ((ProductModel)Container.DataItem).PriceInfo.ListPrice.ToString("F") %>"><%# ((ProductModel)Container.DataItem).PriceInfo.ListPrice.ToString("F") %></span>
                                                            </span> 
                                                            <%--Only dipslay when on sale--%>
                                                            <span class="productTile-altPrice">
                                                                <span><%= SiteSettings.Instance.CurrencySymbol %></span><span><%# ((ProductModel)Container.DataItem).PriceInfo.CurrentPrice.ToString("F") %></span>
                                                            </span>
                                                        </div>
                                                    </asp:PlaceHolder>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <iAppsPro:Pager ID="wseppPager" runat="server" PagesToShow="5" />
                    </div>
                </div>
            </div>
        </div>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>
<script>
    $(document).ready(function(){
	    truncateList(5);
    });
</script>

﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ProductDetailTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Catalog.ProductDetailTemplate" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<%@ Import Namespace="SiteSettings=Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />

    <main>
        <FWZoneControls:PageZoneContainer ID="fwpzcTop" runat="server"></FWZoneControls:PageZoneContainer>
        <div class="section">
            <div class="contained">
                <div itemscope itemtype="http://schema.org/Product">
                    <div class="row">
                        <!-- Product Image Gallery Here -->
                        <div class="column lg-12">
                            <%if (ProductGalleryLarge.Count == 0) { %>
                            <figure>
                                <img src="<%= ProductImageNotFoundLargePath %>" alt="<%= ProductInfo.Title %>" />
                            </figure>
                            <%} else if (ProductGalleryLarge.Count == 1) { %>
                            <figure>
                                <img itemprop="image" src="<%=ProductGalleryLarge[0].URL %>" alt="<%= ProductInfo.Title %>" />
                            </figure>
                            <% } else { %>
                            <asp:Repeater runat="server" ID="rptImagesMain">
                                <HeaderTemplate>
                                    <ul class="productGallery">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li data-thumb="<%#ProductGalleryThumbs[Container.ItemIndex].URL %>" title="<%#ProductGalleryThumbs[Container.ItemIndex].Title %>" alt="<%#ProductGalleryThumbs[Container.ItemIndex].AltText %>">
                                        <a href="<%#Eval("URL") %>" class="openPopup" popupType="image">
                                            <img itemprop="image" src='<%#ProductGalleryMedium[Container.ItemIndex].URL %>' title="<%#ProductGalleryMedium[Container.ItemIndex].Title %>" 
                                                alt="<%#ProductGalleryMedium[Container.ItemIndex].AltText %>" />
                                        </a>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                            <script>
                                $('.productGallery').slick({
                                    dots: true,
                                    infinite: true,
                                    speed: 500,
                                    fade: true,
                                    slide: 'li',
                                    cssEase: 'linear',
                                    slidesToShow: 1,
                                    //autoplay: true,
                                    //autoplaySpeed: 5000,
                                    arrows: false,
                                    //create the nav images
                                    customPaging: function (slick, index) {
                                        return '<button type="button" style="background:url(' + $(slick.$slides[index]).data('thumb') + ') no-repeat center center;"></button>';
                                    }
                                });
                            </script>
                            <% } %>
                        </div>
                        <div class="column lg-12">
                            <h1 class="h-pushXSmBottom" itemprop="name"><%=ProductInfo.Title %></h1>
                            <div class="productTools">
                                <div class="sideToSideSm h-pushSmBottom">
                                    <asp:PlaceHolder ID="phPrice" runat="server">
                                        <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="productTools-priceInfo <%=SelectedPriceInfo.IsSale ? "productTools-priceInfo--hasAlt" : "" %>">                                        
                                            <span class="productTools-price">
                                                <span itemprop="priceCurrency" content="<%= SiteSettings.Instance.CurrencyISOCode %>"><%= SiteSettings.Instance.CurrencySymbol %></span><span itemprop="price" content="<%= SelectedPriceInfo.ListPrice.ToString("F") %>"><%= SelectedPriceInfo.ListPrice.ToString("F") %></span>
                                            </span>
                                            <asp:PlaceHolder id="phSalePrice" runat="server">
                                                <span class="productTools-altPrice">
                                                    <%= Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.CurrencySymbol %><%= SelectedPriceInfo.CurrentPrice.ToString("F") %>
                                                </span>
                                            </asp:PlaceHolder> 
                                        </p>
                                    </asp:PlaceHolder>

                                    <asp:Repeater runat="server" id="rptRatings">
                                        <HeaderTemplate>
                                            <span class="rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                <meta itemprop="worstRating" content="1" />
                                                <span class="h-visuallyHidden">
                                                    <span itemprop="ratingValue"><%=AverageRating.Average.ToString() %></span> rating
                                                </span>
                                                <meta itemprop="bestRating" content="5" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <i class="<%#Container.DataItem %>"r"></i>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                                <a href="#reviews"><span itemprop="reviewCount"><%=AverageRating.TotalReviews.ToString() %></span> reviews</a>
                                            </span>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                
                                </div>
                                <ul class="infoList productTools-infoList">
                                    <li>SKU: <%= SelectedSku != null ? SelectedSku.SKU : "" %></li>
                                </ul>

                                <iAppsPro:SkuSelector runat="server" ID="wseSkuSelector" />

                                <div class="sideToSideMed h-pushSmBottom">
                                    <asp:Placeholder runat="server" ID="phAddToCart">
                                        <div class="productTools-actions">
                                            <div class="productTools-counter">
                                                <div class="productTools-counter-control productTools-counter-control--subtract"></div>
                                                <input runat="server" id="txtQty" type="text" class="productTools-counter-display" value="1" aria-label="Quantity">
                                                <div class="productTools-counter-control productTools-counter-control--add"></div>
                                            </div>
                                            <asp:LinkButton runat="server" ID="lbtnAddToCart" data-mfp-src="#cart-popup" class="btn productTools-buttonCart">Add to Cart</asp:LinkButton>
                                        </div>
                                    </asp:Placeholder>
                                    <p runat="server" id="phInvalidSku" visible="false" class="alert alert--yellow icon-attention">This product is unavailable in the combination of options you've selected.</p>
                                    <p runat="server" id="phStockAlert" class="productTools-stockAlert">Currently out of stock</p>
                                    <p runat="server" id="phAddtoWishlist"><a data-mfp-src="#wishlist-popup" class="icon-heart openPopup">Add to Wishlist</a></p>
                                </div>

                                <asp:Placeholder ID="phRequestQuote" runat="server" Visible="false">
                                    <asp:HyperLink ID="hlRequestQuote" runat="server" CssClass="btn btn--primary">Request a Quote</asp:HyperLink>
                                </asp:Placeholder>

                                <ul runat="server" id="ulTabs" class="tabs tabs--accordion productTools-tabs">
                                    <li><a href="javascript:void(0)">Description</a>
                                        <div itemprop="description">
                                            <%=ProductInfo.Description %>
                                        </div>
                                    </li>
                                    <li runat="server" id="liSpecs"><a href="javascript:void(0)">Details</a>
                                        <div>
                                            <asp:Repeater runat="server" ID="rptSpecifications">
                                                <HeaderTemplate>
                                                    <table class="table table-responsive">
                                                        <tbody>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <th><%#Eval("Name") %></th>
                                                        <td><%#Eval("Value") %></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                        </tbody>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </li>
                                    <!-- Dynamic attribute driven tabs go here -->
                                    <asp:Repeater runat="server" ID="rptDynamicTabs">
                                        <ItemTemplate>
                                            <li>
                                                <a class="icon-down-open" href="javascript:void(0)"><%#((AttributeModel)Container.DataItem).Name %></a>
                                                <div>
                                                    <%#((AttributeModel)Container.DataItem).Value %>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <FWControls:FWCommentsContainer ID="productcomments" runat="server" EnableContextMenu="true"
                        ObjectType="Product" AllowComments="true" ShowUserInfo="true" RequiresApproval="true"
                        AllowRating="true" CustomXsltPath="~/xslt/wseProductReview.xslt" ListExistingComments="true"
                        ClientSideContainerIdForRatingContainers="td_rating">
                        <FWCaptchaStyle FontName="Verdana" ImageWidth="200" ImageHeight="34" MinFontSize="17"
                            MaxFontSize="20" XValue="50" YValue="0" />
                        <RatingsContainer runat="server" ID="ratings" AllowMultipleVotes="true" ObjectType="Product"
                            RatingMessage="Rated ##RATINGVALUE## by ##TOTALVOTES## users." AllowAutoSave="false"
                            AllowAutoBind="false">
                            <FWControls:RatingLevel Rating="1" DefaultImage="/images/star.png"
                                FullImage="/images/fullstar.png" HalfImage="/images/halfstar.png"
                                HoverImage="/images/starhover.png" Text="" UserMessage="" AltText="1 Star"/>
                            <FWControls:RatingLevel Rating="2" DefaultImage="/images/star.png"
                                FullImage="/images/fullstar.png" HalfImage="/images/halfstar.png"
                                HoverImage="/images/starhover.png" Text="" UserMessage="" AltText="2 Stars"/>
                            <FWControls:RatingLevel Rating="3" DefaultImage="/images/star.png"
                                FullImage="/images/fullstar.png" HalfImage="/images/halfstar.png"
                                HoverImage="/images/starhover.png" Text="" UserMessage="" AltText="3 Stars"/>
                            <FWControls:RatingLevel Rating="4" DefaultImage="/images/star.png"
                                FullImage="/images/fullstar.png" HalfImage="/images/halfstar.png"
                                HoverImage="/images/starhover.png" Text="" UserMessage="" AltText="4 Stars"/>
                            <FWControls:RatingLevel Rating="5" DefaultImage="/images/star.png"
                                FullImage="/images/fullstar.png" HalfImage="/images/halfstar.png"
                                HoverImage="/images/starhover.png" Text="" UserMessage="" AltText="5 Stars"/>
                        </RatingsContainer>
                    </FWControls:FWCommentsContainer>
                </div>

                <!-- Related Products Listing -->
                <iAppsPro:RelatedProductsList runat="server" ID="ctlRelatedProducts" />
            </div>
        </div>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />

    <iAppsPro:AddedToCartPopup runat="server" ID="wseppAddedToCartPopup" ImageSizeToDisplay="Small" />

    <%if (AllowWishlist) { %>
    <div id="wishlist-popup" class="popup mfp-hide">
        <div class="navOptions">
            <span><%=ProductInfo.Title %>:</span>
            <asp:Repeater runat="server" ID="rptAddWishlist">
                <ItemTemplate>
                    <asp:LinkButton CssClass="icon-plus" runat="server" ID="lbtnAddToExistingWishlist" CausesValidation="false" OnClick="lbtnAddToExistingWishlist_Click"
                        CommandArgument="<%#((Bridgeline.FW.Commerce.Orders.WishList)Container.DataItem).Id%>">Add to <%#((Bridgeline.FW.Commerce.Orders.WishList)Container.DataItem).Title%></asp:LinkButton>
                </ItemTemplate>
            </asp:Repeater>
            <asp:LinkButton CssClass="icon-plus" runat="server" ID="lbtnAddToNew" CausesValidation="false" CommandArgument="<%#Guid.Empty.ToString() %>">Add to New Wishlist</asp:LinkButton>
        </div>
    </div>
    <%} %>
</div>

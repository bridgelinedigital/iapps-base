﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSearch.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Catalog.ProductSearch" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<%@ Import Namespace="Bridgeline.FW.Commerce.Base.SiteSettings" %>


<%--<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.css" rel="stylesheet"/>--%>
<link href="https://jquery-ui-bootstrap.github.io/jquery-ui-bootstrap/css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
    $(function () {
        $("#txtSearch").autocomplete({
            source: autoComplete
        });
    });

    function SendProductClickToGA(contentType,itemId) {
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            "event": "select_content",
            "content_type": contentType,
            "item_id": itemId
            });
    }
</script>

<div class="row" style="margin-top: 50px;">
    <div class="col-md-3"></div>
    <div class="col-md-4">
        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control input-sm" ClientIDMode="Static"> </asp:TextBox>
    </div>
    <div class="col-md-2">
        <asp:Button ID="btnSearch" runat="server" CssClass="form-control btn btn-success btn-sm" Text="Search" />
    </div>
    <div class="col-md-1">Sort:  </div>
    <div class="col-md-2">
        <iappspro:sorter id="wseppSorter" runat="server" />
    </div>
</div>

<div class="row">
    <div class="col-md-3 ">
        <iappspro:searchrefinments id="searchRefinments" IsRangeRefinment="false" runat="server" /> <br />
        <iappspro:searchrefinments id="searchRangeRefinments" IsRangeRefinment="true" runat="server" /> 
    </div>
    <div class="col-md-9">

        <asp:HyperLink ID="hlSuggest" runat="server"></asp:HyperLink>
        <div class="row">
            <div class="column sm-12 lg-6">
                <strong>
                    <asp:Literal ID="ltlheader" runat="server"></asp:Literal>
                </strong>
                <asp:Literal ID="ltlTook" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row row--tight productGrid">
            <asp:Repeater runat="server" ID="rptResult">
                <ItemTemplate>
                    <div class="column sm-12 lg-6">
                        <div class="productTile">
                            <div itemscope itemtype="http://schema.org/Product" class="productTile-wrapper">
                                <!-- if product on sale display message -->
                                <asp:PlaceHolder ID="phProductFlag" runat="server" Visible="false">
                                    <div class="productTile-message">
                                        <asp:Literal ID="ltlProductFlag" runat="server" />
                                    </div>
                                </asp:PlaceHolder>
                                <div class="productTile-image">
                                    <a id="hlUrl" runat="server">
                                        <img itemprop="image" runat="server" id="imgProduct" src="<%# ProductImageNotFoundMediumPath %>" />
                                    </a>
                                </div>

                                <div class="productTile-info">
                                    <h3 class="productTile-name">
                                        <a id="hlUrlForTitle" runat="server">
                                            <span itemprop="name">
                                                <asp:Literal ID="ltlTitle" runat="server"></asp:Literal>
                                            </span>
                                        </a></h3>

                                    <p itemprop="description" class="productTile-description">
                                        <asp:Literal ID="ltlDesc" runat="server"></asp:Literal>
                                    </p>


                                    <div class="productTile-description">
                                        <asp:Literal ID="ltlShortDesc" runat="server"></asp:Literal>
                                    </div>

                                    <%--<asp:Literal ID="ltlListPrice" runat="server"></asp:Literal>--%>

                                    <asp:PlaceHolder ID="phPrice" runat="server">
                                        <div itemprop="offers" id="dvOffer" runat="server" itemscope itemtype="http://schema.org/Offer">
                                            <span class="productTile-price">
                                                <span itemprop="priceCurrency" content="<%= Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencyISOCode", "USD") %>"><%= Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencySymbol", "$") %></span>
                                                <asp:Literal ID="ltlListPrice" runat="server"></asp:Literal>
                                            </span>
                                            <!-- Only dipslay when on sale -->

                                            <span itemprop="price" class="productTile-altPrice">
                                                <span itemprop="priceCurrency" content="<%= Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencyISOCode", "USD") %>"><%= Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencySymbol", "$") %></span>
                                                <asp:Literal ID="ltlCurrentPrice" runat="server"></asp:Literal>
                                            </span>
                                        </div>
                                    </asp:PlaceHolder>
                                </div>
                            </div>
                        </div>
                    </div>

                </ItemTemplate>

                <FooterTemplate>
                    <asp:Label ID="lblNoRecord" runat="server" Visible="false"></asp:Label>
                </FooterTemplate>
            </asp:Repeater>
        </div>

        <div class="row" style="margin-top: 20px;">
            <iappspro:searchpager id="searchPager" runat="server" pagestoshow="10" />
        </div>

    </div>
</div>

<div class="row" style="margin-top: 20px;">
      <iappspro:SearchExplain id="searchExplain" runat="server" />
</div>
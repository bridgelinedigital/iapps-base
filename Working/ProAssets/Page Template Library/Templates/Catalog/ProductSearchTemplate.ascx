﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSearchTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Catalog.ProductSearchTemplate" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<%@ Import Namespace="Bridgeline.FW.Commerce.Base.SiteSettings" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" />
    <main>
        <div class="section h-softLgEnds">
            <div class="contained">
                <div class="row">
                    <div class="column med-7 lg-5">
                        <!-- Begin facet filters -->
                        <div class="filters">
                            <asp:Repeater runat="server" ID="rptFacets" OnItemDataBound="rptFacets_ItemDataBound">
                                <HeaderTemplate>
                                    <h2 class="filters-heading">Refine Your Results</h2>
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <iAppsPro:FacetGroup ID="wseppFacetGroup" runat="server" OnFacetAdded="wseppFacetGroup_FacetAdded" CategoryFacet="<%# Container.DataItem %>" />
                                </ItemTemplate>

                                <FooterTemplate></FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <!-- Begin facet filters -->
                    </div>

                    <div class="column med-17 lg-19">
                        <div class="searchBox searchBox--hiVis">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="searchBox-textField" placeholder="Previously entered search term" />
                            <a class="searchBox-submit"></a>
                        </div>
                        <nav class="navHorizontal" runat="server" id="navSearchType" visible="false">
                          <ul>
                            <li class="selected"><a href="">Products (107)</a> </li>
                            <li><a href="">Content (56)</a> </li>
                          </ul>
                        </nav>
                        <p>
                            <asp:Repeater runat="server" ID="rptSelectedFacets" OnItemCommand="rptSelectedFacets_ItemCommand" OnItemDataBound="rptSelectedFacets_ItemDataBound">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbSelectedFacet" runat="server" CommandArgument='<%# Eval("ValueId") %>' Text='<%# Eval("Value") %>' CommandName="RemoveFacet" CssClass="tag is-closeable"></asp:LinkButton>
                                </ItemTemplate>
                                <SeparatorTemplate> </SeparatorTemplate>
                            </asp:Repeater>
                        </p>
                        <div class="resultsTools h-pushBottom">
                            <div class="resultsTools-results">
                                <span class="resultsTools-results"><%=TotalProducts %> result<%=TotalProducts > 1 ? "s" : "" %></span><a class="resultsTools-mobileFilterDrawerToggle drawerToggle" data-for="filters-mobile" href="javascript:void(0)">Refine Results</a>
                            </div>
                            <div class="resultsTools-options">
                                <iAppsPro:Sorter ID="wseppSorter" runat="server" />
                            </div>
                        </div>
                        <div class="row row--tight productGrid">
                            <asp:Repeater runat="server" ID="rptProducts" OnItemDataBound="rptProducts_ItemDataBound">
                                <ItemTemplate>
                                    <div class="column sm-12 lg-6">
                                        <div class="productTile">
                                            <div itemscope itemtype="http://schema.org/Product" class="productTile-wrapper">
                                                <!-- if product on sale display message -->
                                                <asp:PlaceHolder id="phProductFlag" runat="server" Visible="false">
                                                    <div class="productTile-message"><asp:Literal ID="ltlProductFlag" runat="server" /></div>
                                                </asp:PlaceHolder>
                                                <div class="productTile-image"><a href="<%#((ProductModel)Container.DataItem).URL %>"><img itemprop="image" runat="server" id="imgProduct" src="<%# ProductImageNotFoundMediumPath %>" alt="<%#((ProductModel)Container.DataItem).Title %>"/></a></div>
                                                <div class="productTile-info">
                                                    <h3 class="productTile-name"><a href="<%#((ProductModel)Container.DataItem).URL %>"><span itemprop="name"><%#((ProductModel)Container.DataItem).Title %></span></a></h3>
                                                    <p itemprop="description" class="productTile-description"><%#((ProductModel)Container.DataItem).Description %></p>
                                                    <div class="productTile-description"><%#((ProductModel)Container.DataItem).ShortDescription %></div>
                                                    <asp:PlaceHolder id="phPrice" runat="server">
                                                        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="productTile-priceInfo <%#((ProductModel)Container.DataItem).PriceInfo.IsSale ? "productTile-priceInfo--hasAlt" : "" %>">
                                                            <span class="productTile-price"><span itemprop="priceCurrency" content="<%=  Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencyISOCode", "USD") %>"><%= Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencySymbol", "$") %></span><%#((ProductModel)Container.DataItem).PriceInfo.ListPrice.ToString("F") %></span> 
                                                            <!-- Only dipslay when on sale -->
                                                            <span itemprop="price" class="productTile-altPrice"><span itemprop="priceCurrency" content="<%=  Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencyISOCode", "USD") %>"><%= Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("CurrencySymbol", "$") %></span><%#((ProductModel)Container.DataItem).PriceInfo.CurrentPrice.ToString("F") %></span> 
                                                        </div>
                                                    </asp:PlaceHolder>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <iAppsPro:Pager ID="wseppPager" runat="server" OnPageChanged="wseppPager_PageChanged" PagesToShow="5" />
                    </div>
                </div>
            </div>
        </div>
    </main>

    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function () {
        $('#<%=txtSearch.ClientID%>').keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                submitSiteSearch($(this).val());
            }
        });

        $('.searchBox-submit').click(function () {
            submitSiteSearch($(this).prev('#<%=txtSearch.ClientID%>').val());
        });
    });
    //]]>

    $(document).ready(function(){
	    truncateList(5);
    });
</script>
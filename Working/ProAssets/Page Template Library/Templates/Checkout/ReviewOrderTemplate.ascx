﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ReviewOrderTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Checkout.ReviewOrderTemplate" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<%@ Import Namespace="Bridgeline.FW.Commerce.Base.SiteSettings" %>

<div class="pageWrap">
    <!-- Checkout Header -->

<asp:PlaceHolder runat="server" ID="cphStripe" Visible="false">
    <script src="https://js.stripe.com/v3/"></script>
    <script> //Stripe
        $().ready(function () {

            $("[id$='lbtnSubmit']").click(async (e) => {
                e.preventDefault();
                var result;
                // Confirm the PaymentIntent using the details collected by the ConfirmationToken
                if (stripeConfirmationToken.startsWith("pm_")) {
                    result = await stripe.confirmPayment({
                        clientSecret: stripeClientSecret,
                        confirmParams: {
                            payment_method: stripeConfirmationToken,
                            return_url: '<%= Context.Items[Bridgeline.FW.UI.UIConstants.PUBLIC_SITE_URL].ToString()+Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.GetReviewOrderPageUrl() %>',
                        },
                        redirect: 'if_required'
                    });
                }
                else {
                    result = await stripe.confirmPayment({
                        clientSecret: stripeClientSecret,
                        confirmParams: {
                            confirmation_token: stripeConfirmationToken,
                            return_url: '<%= Context.Items[Bridgeline.FW.UI.UIConstants.PUBLIC_SITE_URL].ToString()+Bridgeline.FW.Commerce.Base.SiteSettings.SiteSettings.Instance.GetReviewOrderPageUrl() %>',
                        },
                        redirect: 'if_required'
                    });
                }

                if (result.error) {
                    $("[id$='divErrorMsg']").show();
                    $("[id$='divErrorMsg']").html('Payment Failure - ' + result.error.message);
                    $("[id$='lbtnSubmit']").attr('disabled');
                    console.log(JSON.stringify(result.error));
                    return false;
                }
                console.log(JSON.stringify(result));
                $("[id$='hfStripe']").val(JSON.stringify(result));
                theForm.__EVENTTARGET.value = 'ctl01$lbtnSubmit';
                theForm.submit();
                WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl01$lbtnSubmit",
                    "",
                    true,
                    "",
                    "",
                    false,
                    true));
                    return true;
              
            });
        });
    </script>
</asp:PlaceHolder>
    
<asp:PlaceHolder runat="server" ID="cphPayerAuth" Visible="false">
       <script> //Cordinal Commerce Payer Authentication
           $().ready(function () {
            Cardinal.configure({
                logging: {
                    debug:'verbose'
                }
               });
                Cardinal.setup("init", {
                    jwt: $("[id$='hfJWTContainer']").val()
               });

               Cardinal.on('payments.setupComplete', function (setupCompleteData) {
                $("[id$='hfSessionId']").val(setupCompleteData.sessionId);
               });

          

               var cardChanged = false;
               $("[id$='lbtnSubmit']").click(StartPayerAuth);
        });
   


            Cardinal.on("payments.validated", function (data, jwt) {
                var actionCode = data.ErrorDescription.toUpperCase();
                if ('ActionCode' in data)
                    actionCode = data.ActionCode;
                switch (actionCode) {
                    case "SUCCESS":
                        // Handle successful transaction, send JWT to backend to verify
                        $('#hfJWTContainer').val(jwt);
                        $('#hfCardinalObject').val(JSON.stringify(data));
                         WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl01$lbtnSubmit",
                            "",
                            true,
                            "",
                            "",
                            false,
                            true));
                        return true;

                    case "NOACTION":
                        // Handle no actionable outcome
                        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl01$lbtnSubmit",
                            "",
                            true,
                            "",
                            "",
                            false,
                            true));
                        return true;

                    case "FAILURE":
                        //$('#divErrorMsg').css('display', 'inline-block');
                        $("[id$='divErrorMsg']").show();
                        $("[id$='divErrorMsg']").html('Payment Failure - Authentication Failed - Your card issuer cannot authenticate this card. Please select another card or form of payment to complete your purchase.');
                        break;

                    case "ERROR":
                        //$('#divErrorMsg').css('display', 'inline-block')
                        $("[id$='divErrorMsg']").show();
                        $("[id$='divErrorMsg']").html('Payment Error - Authentication Failed - Your card issuer cannot authenticate this card. Please select another card or form of payment to complete your purchase.');
                        break;

                    default:
                       // $('#divErrorMsg').css('display', 'inline-block')
                        $("[id$='divErrorMsg']").show();
                        $("[id$='divErrorMsg']").html('Error - Authentication Failed - Your card issuer cannot authenticate this card. Please select another card or form of payment to complete your purchase.');
                        break;
                }
            });

           function StartPayerAuth(e)
           {
               e.preventDefault();
               var obj = JSON.parse( $("[id$='hfCardinalObject']").val());
               Cardinal.continue('cca', obj[0], obj[1]);
           }
           
    </script>  
</asp:PlaceHolder>
<asp:HiddenField ID="hfCardinalObject" runat="server" />
<asp:HiddenField ID="hfJWTContainer"  runat="server" />
<asp:HiddenField ID="hfSessionId" runat="server" />
<asp:HiddenField ID="hfStripe" runat="server" />
    <iAppsPro:HeaderMainStripped id="Header" runat="server" />
    <main>
        <div class="section">
            <div class="contained">
                <ul class="status-bar">
                    <li class="done"><a href="<%=ShippingUrl %>">Shipping</a></li>
                    <li class="done"><a href="<%=PaymentUrl %>">Payment</a></li>
                    <li class="current">Review</li>
                </ul>
                <div class="row">
                    <div class="column lg-18">
                        <div class="sideToSideMed h-pushBottom">
                            <h1 class="h-textLg">Review Cart</h1>
                            <div class="navOptions navOptions--sm navOptions--medHorizontal"><span class="icon-cog"></span><a class="icon-pencil" href="<%=SiteSettings.Instance.CartURL %>">Edit Cart</a></div>
                        </div>
                        <div class="cartItemContainer">
                            <asp:Repeater runat="server" ID="rptOrderItems" OnItemDataBound="rptOrderItems_ItemDataBound">
                                <ItemTemplate>
                                    <div class="cartItem">
                                        <figure class="cartItem-image"><img runat="server" id="imgProduct" title="product" alt="product" /></figure>
                                        <div class="cartItem-firstSection">
                                            <h2 class="cartItem-name"><a href="<%#((CartItemModel)Container.DataItem).SKU.Product.URL %>"><%# ((CartItemModel)Container.DataItem).SKU.Product.Title %> - <%#((CartItemModel)Container.DataItem).SKU.SKU %></a></h2>
                                            <asp:Repeater runat="server" ID="rptItemAttributes">
                                                <HeaderTemplate><ul class="cartItem-infoList"></HeaderTemplate>
                                                <ItemTemplate>
                                                    <li><%#DataBinder.Eval(Container.DataItem, "Name") %>: <strong><%#DataBinder.Eval(Container.DataItem, "Value") %></strong></li>
                                                </ItemTemplate>
                                                <FooterTemplate></ul></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <!--/.cartItem-firstSection-->
                                        <div class="cartItem-secondSection">
                                            <ul class="cartItem-infoList">
                                                <li>Qty: <strong><%#((CartItemModel)Container.DataItem).Quantity %></strong></li>
                                            </ul>
                                        </div>
                                        <!--/.cartItem-secondSection-->
                                        <div class="cartItem-cap"><span class="cartItem-price"><%#(((CartItemModel)Container.DataItem).Price * ((CartItemModel)Container.DataItem).Quantity).ToString("c") %><span><%#((CartItemModel)Container.DataItem).Price.ToString("c") %>ea</span></span></div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="sideToSideMed h-pushBottom">
                            <FWControls:FWTextContainer runat="server" ID="fwtxtPlaceOrderInstructions">
                                <FwText>
                                    <p class="h-textSm"> <strong>Your order is not complete until you click Place Order.</strong><br/> By placing your order you agree to our <a href="">terms &amp; conditions</a> </p>
                                </FwText>
                            </FWControls:FWTextContainer>
                            <div class="navOptions navOptions--sm navOptions--medHorizontal"><span class="icon-cog"></span><a class="icon-pencil" href="<%=SiteSettings.Instance.CartURL %>">Edit Cart</a></div>
                        </div>
                    </div>
                    
                    <div class="column lg-6">
                        <table class="table">
                            <tr>
                                <th>Subtotal:</th>
                                <td><%=CurrentOrder!= null ? CurrentOrder.Total.ToString("c") : "" %></td>
                            </tr>
                            <tr>
                                <th>Handling:</th>
                                <td><%= CurrentOrder != null ? this.TotalHandling.ToString("c") : "" %></td>
                            </tr>
                            <tr>
                                <th>Shipping:</th>
                                <td><%= CurrentOrder != null ? (CurrentOrder.TotalShippingCharge - this.TotalHandling).ToString("c") : "" %></td>
                            </tr>
                            <tr>
                                <th>Tax:</th>
                                <td><%= CurrentOrder != null ? CurrentOrder.TotalTax.ToString("c") : "" %></td>
                            </tr>
                            <tr>
                                <th>Discounts/Coupons:</th>
                                <td><%= CurrentOrder != null ? CurrentOrder.TotalDiscount.ToString("c") : "" %></td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td><span class="h-textLg"><%= CurrentOrder != null ? CurrentOrder.GrandTotal.ToString("c") : "" %></span></td>
                            </tr>
                        </table>
                    </div>

                    <div class="column lg-18">
                        <div class="formFooter">
                            <p><asp:LinkButton runat="server" ID="lbtnSubmit" CssClass="btn btn--primary icon-check">Place Order</asp:LinkButton></p>
                            <div class="form-error" runat="server" id="divErrorMsg" style="display: none;" ></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderConfirmationTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Checkout.OrderConfirmationTemplate" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>

<div class="pageWrap">
    <!-- Checkout Header -->
    <iAppsPro:HeaderMain id="Header" runat="server" ShowBreadcrumb="false" />
    <main>
        <div class="section">
            <div class="contained">
                <asp:MultiView runat="server" ID="mvConfirmation" ActiveViewIndex="0">
                    <asp:View runat="server" ID="vwOrderConfirmation">
                        <div class="sideToSideMed h-pushBottom">
                            <h1>Thank you for your purchase <span class="h-h3">(Order #<%=CurrentOrder != null ? CurrentOrder.OrderNumber : "" %>)</span></h1>
                            <div class="navOptions navOptions--sm navOptions--medHorizontal"><span class="icon-cog"></span><a class="icon-printer" href="javascript:window.print();">Print Receipt</a></div>
                        </div>
                        <div class="row">
                            <div class="column lg-18">
                                <div class="cartItemContainer">
                                    <asp:Repeater runat="server" ID="rptOrderItems" OnItemDataBound="rptOrderItems_ItemDataBound">
                                        <ItemTemplate>
                                            <div class="cartItem">
                                                <figure class="cartItem-image"><img runat="server" id="imgProduct" src="http://fpoimg.com/150x150?text=Product Image"></figure>
                                                <div class="cartItem-firstSection">
                                                    <h2 class="cartItem-name"><a href="<%#((CartItemModel)Container.DataItem).SKU.Product.URL %>"><%# ((CartItemModel)Container.DataItem).SKU.Product.Title %> - <%#((CartItemModel)Container.DataItem).SKU.SKU %></a></h2>
                                                    <asp:Repeater runat="server" ID="rptItemAttributes">
                                                        <HeaderTemplate><ul class="cartItem-infoList"></HeaderTemplate>
                                                        <ItemTemplate>
                                                            <li><%#DataBinder.Eval(Container.DataItem, "Name") %>: <strong><%#DataBinder.Eval(Container.DataItem, "Value") %></strong></li>
                                                        </ItemTemplate>
                                                        <FooterTemplate></ul></FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <!--/.cartItem-firstSection-->
                                                <div class="cartItem-secondSection">
                                                    <ul class="cartItem-infoList">
                                                        <li>Qty: <strong><%#((CartItemModel)Container.DataItem).Quantity %></strong></li>
                                                    </ul>
                                                </div>
                                                <!--/.cartItem-secondSection-->
                                                <div class="cartItem-cap"><span class="cartItem-price"><%#(((CartItemModel)Container.DataItem).Price * ((CartItemModel)Container.DataItem).Quantity).ToString("c") %><span><%#((CartItemModel)Container.DataItem).Price.ToString("c") %>ea</span></span></div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                            <div class="column lg-6">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Subtotal:</th>
                                            <td><%=CurrentOrder != null ? CurrentOrder.Total.ToString("c") : "" %></td>
                                        </tr>
                                        <tr>
                                            <th>Handling:</th>
                                            <td><%=CurrentOrder != null ? this.TotalHandling.ToString("c") : "" %></td>
                                        </tr>
                                        <tr>
                                            <th>Shipping:</th>
                                            <td><%=CurrentOrder != null ? (CurrentOrder.TotalShippingCharge - this.TotalHandling).ToString("c") : "" %></td>
                                        </tr>
                                        <tr>
                                            <th>Tax:</th>
                                            <td><%=CurrentOrder != null ? CurrentOrder.TotalTax.ToString("c") : "" %></td>
                                        </tr>
                                        <tr>
                                            <th>Discounts/Coupons:</th>
                                            <td><%=CurrentOrder != null ? CurrentOrder.TotalDiscount.ToString("c") : "" %></td>
                                        </tr>
                                        <tr>
                                            <th>Total:</th>
                                            <td><span class="h-textLg"><%=CurrentOrder != null ? CurrentOrder.GrandTotal.ToString("c") : "" %></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <h2 class="h-textLg h-pushSmBottom">Shipping</h2>
                                <asp:Repeater runat="server" ID="rptOrderShippings" OnItemDataBound="rptOrderShippings_ItemDataBound">
                                    <ItemTemplate>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Shipping</th>
                                                    <td><%#((OrderShippingModel)Container.DataItem).ShippingOption.ShipperName%> - <%#((OrderShippingModel)Container.DataItem).ShippingOption.ShippingOptionName%></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Address</th>
                                                    <td>
                                                        <strong><%# ((OrderShippingModel)Container.DataItem).OrderShippingAddress.Type.ToString() %></strong><br />
                                                        <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.FirstName%> <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.LastName%><br />
                                                        <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.AddressLine1%><br />
                                                        <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.City%>  <%# ((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.State.StateCode%>, <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.Zip%><br />
                                                        <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.Country.CountryCode%><br />
                                                        <%#((OrderShippingModel)Container.DataItem).OrderShippingAddress.Address.Phone%>
                                                    </td>
                                                </tr>

                                                <asp:Repeater runat="server" ID="rptTrackingNumbers">
                                                    <HeaderTemplate>
                                                        <tr>
                                                            <th scope="row">Tracking #</th>
                                                            <td>
                                                    </HeaderTemplate>

                                                    <ItemTemplate>
                                                        <%#Eval("TrackingNumber")%><br />
                                                    </ItemTemplate>

                                                    <FooterTemplate>
                                                            </td>
                                                        </tr>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:PlaceHolder ID="phPayment" runat="server">
                                    <h2 class="h-textLg h-pushSmBottom">Payment Info</h2>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Name on Card</th>
                                                <td><%=CreditCardInfo.NameOnCard %></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Card Type</th>
                                                <td><%=CreditCardInfo.GetCardType() %></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Card Number</th>
                                                <td>****<%=CreditCardInfo.Last4Digits %></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Expiration Date</th>
                                                <td><%=CreditCardInfo.ExpirationMonth %>/<%=CreditCardInfo.ExpirationYear %></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Billing Address</th>
                                                <td>
                                                    <%=CreditCardInfo.BillingAddress.AddressLine1%><br>
                                                    <%=CreditCardInfo.BillingAddress.City%>, <%=CreditCardInfo.BillingAddress.State.StateCode%> <%=CreditCardInfo.BillingAddress.Zip%><br>
                                                    <%=CreditCardInfo.BillingAddress.Country.CountryCode%> <br />
                                                    <%=CreditCardInfo.BillingAddress.Phone%>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </asp:PlaceHolder>
                            </div>
                        </div>
                        <div class="sideToSideMed h-pushBottom">
                            <FWControls:FWTextContainer runat="server" ID="fwtxtMessage">
                                <FwText>
                                    <p>Content area for customer support information, returns information, links to other support pages on the site.</p>
                                </FwText>
                            </FWControls:FWTextContainer>
                            <div class="navOptions navOptions--sm navOptions--medHorizontal"><span class="icon-cog"></span><a class="icon-printer printLink" href="javascript:window.print();">Print Receipt</a></div>
                        </div>
                    </asp:View>
                    <asp:View runat="server" ID="vwInvalidOrder">
                        <div class="h-stacksToSidesMed h-pushBottom">
                            <h1>Invalid Order</h1>
                            <p>There is currently no order information available</p>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Checkout.ShoppingCartTemplate" %>

<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Extensions" %>
<%@ Import Namespace="Bridgeline.FW.Commerce.Base.SiteSettings" %>

<div class="pageWrap">
    <iAppsPro:HeaderMain id="Header" runat="server" ShowBreadcrumb="false" />

    <main>
        <div class="section">
            <div class="contained">
                <h1>Shopping Cart (<%=CurrentCart.CartItems.Count() %>)</h1>
                <div class="row">
                    <div class="column lg-18">
                        <p><a class="icon-check btn" style='<%= CurrentCart.CartItems.Count()>0? "display: inline" : "display: none" %>' href="<%=CheckoutUrl %>">Checkout</a></p>
                        <div class="cartItemContainer">
                            <asp:Repeater runat="server" ID="rptCartItems" OnItemDataBound="rptCartItems_ItemDataBound" OnItemCommand="rptCartItems_ItemCommand">
                                <ItemTemplate>
                                    <div class="cartItem">
                                        <figure class="cartItem-image"><img runat="server" id="imgProduct" title="product" alt="product" /></figure>
                                        <div class="cartItem-firstSection">
                                            <h2 class="cartItem-name">
                                                <a href="<%#((CartItemModel)Container.DataItem).SKU.Product.URL %>"><%# ((CartItemModel)Container.DataItem).SKU.Product.Title %> - <%#((CartItemModel)Container.DataItem).SKU.SKU %></a>
                                            </h2>
                                            <asp:Repeater runat="server" ID="rptItemAttributes">
                                                <HeaderTemplate><ul class="cartItem-infoList"></HeaderTemplate>
                                                <ItemTemplate>
                                                    <li><%#DataBinder.Eval(Container.DataItem, "Name") %>: <strong><%#DataBinder.Eval(Container.DataItem, "Value") %></strong></li>
                                                </ItemTemplate>
                                                <FooterTemplate></ul></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="cartItem-secondSection">
                                            <div class="cartItem-actions">
                                                <div class="cartItem-actions-counter">
                                                    <div class="cartItem-actions-counter-control cartItem-actions-counter-control--subtract"></div>
                                                    <input runat="server" id="txtQty" type="text" class="cartItem-actions-counter-display" value="<%#((CartItemModel)Container.DataItem).Quantity.ToString() %>" data-increment="<%#((CartItemModel)Container.DataItem).SKU.OrderIncrement.ToString() %>" aria-label="Quantity"/>
                                                    <div class="cartItem-actions-counter-control cartItem-actions-counter-control--add"></div>
                                                </div>
                                                <asp:RequiredFieldValidator runat="server" ID="rvalQty" ControlToValidate="txtQty" Display="Dynamic" ErrorMessage="Quantity is required" CssClass="form-error"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvQuantity" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtQty" ClientValidationFunction="validateQuantity"
                                                    ErrorMessage='<%# "Quantity must be ordered in increments of "+((CartItemModel)Container.DataItem).SKU.OrderIncrement.ToString()%>' CssClass="form-error">
                                                </asp:CustomValidator>

                                                <ul class="cartInfo-actions-links">
                                                  <li>
                                                    <asp:LinkButton runat="server" ID="lbtnUpdate" CssClass="cartInfo-actions-link icon-refresh" CommandName="UpdateItem" CommandArgument='<%#((CartItemModel)Container.DataItem).CartItemId %>' CausesValidation="true" >update</asp:LinkButton>
                                                  </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="cartItem-cap">
						                    <span class="cartItem-price"><%#(((CartItemModel)Container.DataItem).Price * ((CartItemModel)Container.DataItem).Quantity).ToString("c") %>
							                    <span><%#((CartItemModel)Container.DataItem).Price.ToString("c") %>ea</span>
						                    </span>
                                            <asp:LinkButton runat="server" ID="lbtnRemove" CssClass="cartItem-removeLink" CommandName="RemoveItem" CommandArgument='<%#((CartItemModel)Container.DataItem).CartItemId %>'>remove</asp:LinkButton>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="column lg-6">
                        <table class="table">
                            <tr>
                                <th>Subtotal:</th>
                                <td><span class="h-textLg"><%= rptCartItems.Items.Count>0? CurrentCart.Total.ToString("c"):(0).ToString("c") %></span></td>
                            </tr>
                        </table>
                    </div>

                    <div class="column lg-18">
                        <p><a class="icon-check btn" style='<%= CurrentCart.CartItems.Count()>0? "display: inline" : "display: none" %>' href="<%=CheckoutUrl %>">Checkout</a></p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />
</div>

<script>
    function validateQuantity(oSrc, args) {
        var increment = ($("#" + oSrc.controltovalidate).attr("data-increment"));   
        args.IsValid = (args.Value % increment == 0);
    }
</script> 
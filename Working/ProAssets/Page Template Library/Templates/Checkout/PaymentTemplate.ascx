﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PaymentTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Checkout.PaymentTemplate" %>
<%@ Register Src="~/Page Template Library/Parts/Checkout/PaymentMethodSelector.ascx" TagName="PaymentMethodSelector" TagPrefix="WSE" %>
<%@ Register Src="~/Page Template Library/Parts/Checkout/PaymentMethodForm.ascx" TagName="PaymentMethodForm" TagPrefix="WSE" %>
<%@ Import Namespace="Bridgeline.iAPPS.Pro.API.Model" %>
<div class="pageWrap">
    <!-- Checkout Header -->
    <iAppsPro:HeaderMainStripped id="Header" runat="server" />
    <asp:PlaceHolder runat="server" ID="cphPayerAuth" Visible="false">
    <script src="/Script%20Library/front-end/flex-sdk-web.js"></script>
       <script> //Cordinal Commerce Payer Authentication
           $().ready(function () {
            Cardinal.configure({
                logging: {
                    debug:'verbose'
                }
               });
                Cardinal.setup("init", {
                    jwt: $("[id$='hfJWTContainer']").val()
               });

               Cardinal.on('payments.setupComplete', function (setupCompleteData) {
                $("[id$='hfSessionId']").val(setupCompleteData.sessionId);
               });

          

               var cardChanged = false;
               $("[id$='lbtnNext']").click(StartPayerAuth);
        });
        function StartPayerAuth(e) {
            e.preventDefault();
            var cardNumber = $("[id$='hfBIN']").val();
            if (cardNumber!='')
                Cardinal.trigger("bin.process", cardNumber);
             WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl01$lbtnNext",
                            "",
                            true,
                            "",
                            "",
                            false,
                            true));
                        return true;
        }
         </script>  
    <asp:HiddenField ID="hfJWTContainer"  runat="server" />
    <asp:HiddenField ID="hfSessionId" runat="server" />
     <asp:HiddenField ID="hfBIN" runat="server" />
</asp:PlaceHolder>
    <main>
        <div class="section">
            <div class="contained">
                <ul class="status-bar">
                    <li class="done"><a href="<%=ShippingUrl %>">Shipping</a></li>
                    <li class="current">Payment</li>
                    <li>Review</li>
                </ul>

                <div class="row">
                    <div class="column lg-18">
                        <asp:Label id="lblPaymentError" runat="server" CssClass="form-error"></asp:Label>
                        <div class="row">
                            <div class="column med-13">
                                <div class="swapBox">
                                    <asp:CheckBox runat="server" ID="chkHasCoupon" />
                                    <asp:Label runat="server" ID="lblHasCoupon" AssociatedControlID="chkHasCoupon">I have a Coupon Code</asp:Label>
                                    <div class="swap inlineLabel">
                                        <asp:Label runat="server" ID="lblCouponCode" AssociatedControlID="txtCouponCode">Coupon Code</asp:Label>
                                        <asp:TextBox runat="server" ID="txtCouponCode" CssClass="swap-input" />
                                        <asp:LinkButton runat="server" ID="lbtnAddCoupon" CausesValidation="false">Apply</asp:LinkButton>
                                    </div>
                                </div>
                                <span runat="server" id="spanCouponError" class="form-error" visible="false">Invalid Coupon</span>
                                <asp:Repeater runat="server" ID="rptAppliedCoupons" OnItemCommand="rptAppliedCoupons_ItemCommand" OnItemDataBound="rptAppliedCoupons_ItemDataBound">
                                    <HeaderTemplate><p></HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnAppliedCoupon" runat="server" CommandArgument='<%# Eval("Id") %>' CommandName="RemoveCoupon" CausesValidation="false"><%#((CouponModel)Container.DataItem).Title %></asp:LinkButton>
                                    </ItemTemplate>
                                    <FooterTemplate></p></FooterTemplate>
                                </asp:Repeater>
                            </div>

                            <div runat="server" id="phGiftCard" class="column med-13" visible="false">
                                <div class="swapBox">
                                    <input type="checkbox" id="hasGiftCard">
                                    <label for="hasGiftCard">I have a Gift Card</label>
                                    <div class="swap inlineLabel">
                                        <label for="giftCard1">Gift Card #</label>
                                        <input type="text" class="swap-input" id="giftCard1">
                                        <a href="">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <asp:PlaceHolder runat="server" ID="phGuestPaymentTypeSelector" Visible="false">
                            <fieldset>
                                <legend class="legend--labelStyle">Select a Payment Method</legend>
                                <div class="formRadioButton">
                                    <span>
                                        <asp:RadioButton runat="server" ID="rbtnGuestTypeCredit" GroupName="GuestPaymentType" Checked="true" AutoPostBack="true" OnCheckedChanged="rbtnGuestTypeCredit_CheckedChanged" />
                                        <asp:Label runat="server" ID="lblGuestTypeCredit" AssociatedControlID="rbtnGuestTypeCredit" Text="Credit Card" />
                                    </span>
                                    <span>
                                        <asp:RadioButton runat="server" ID="rbtnGuestTypePaypal" GroupName="GuestPaymentType" AutoPostBack="true" OnCheckedChanged="rbtnGuestTypeCredit_CheckedChanged" />
                                        <asp:Label runat="server" ID="lblGuestTypePaypal" AssociatedControlID="rbtnGuestTypePaypal" Text="PayPal" />
                                    </span>
                                </div>
                            </fieldset>
                        </asp:PlaceHolder>

                        <asp:MultiView runat="server" id="mvPayment" ActiveViewIndex="0">
                            <asp:View runat="server" id="vwCurrentPayment">
                                <h1 class="h-textLg">Payment Method</h1>
                                <asp:Placeholder runat="server" ID="phRemainingPayment" Visible="false">
                                    <p class="alert alert--success icon-wallet">
                                      Please select a payment method for remaining order total: <strong>$XXX.XX</strong>
                                    </p>
                                </asp:Placeholder>
                                <div class="infoAction">
                                    <div class="infoAction-item">
                                        <div class="infoAction-title"><asp:Literal runat="server" ID="litCardNickname">My Bank Card</asp:Literal></div>
                                        <div class="infoAction-info"><asp:Literal runat="server" ID="litCardType"></asp:Literal>Credit Card Payment <asp:Literal runat="server" ID="litLast4Digits"></asp:Literal></div>
                                        <asp:LinkButton runat="server" ID="lbtnChangePaymentMethod" CssClass="infoAction-action infoAction-action--change" OnClick="lbtnChangePaymentMethod_Click" CausesValidation="false"> Change </asp:LinkButton>
                                    </div>
                                </div>
                                <div class="inlineLabel">
                                    <asp:Label runat="server" ID="lblCvv" AssociatedControlID="txtCvv">Security Code</asp:Label>
                                    <asp:TextBox runat="server" ID="txtCvv" placeholder="Security Code" />
                                    <asp:RequiredFieldValidator runat="server" ID="rfvalCvv" ControlToValidate="txtCvv" CssClass="form-error" Display="Dynamic" ErrorMessage="Security Code is required."></asp:RequiredFieldValidator>
                                </div>
                            </asp:View>

                            <asp:View runat="server" id="vwSelectPayment">
                                <div class="sideToSideMed h-pushBottom">
                                    <h1 class="h-textLg">Select Payment Method</h1>
                                    <nav class="navOptions navOptions--sm navOptions--medHorizontal">
                                        <span class="icon-cog"></span>
                                        <asp:LinkButton runat="server" id="lbtnCancelSelect" CssClass="icon-close" OnClick="lbtnCancelSelect_Click" >Cancel</asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbtnNewPaymentMethod" CssClass="icon-plus" OnClick="lbtnNewPaymentMethod_Click">New Credit Card</asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbtnPaypal" CssClass="icon-paypal" OnClick="lbtnPaypal_Click" Visible="false">PayPal</asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbtnOnAccount" OnClick="lbtnOnAccount_Click" Visible="false">On Account</asp:LinkButton>
                                    </nav>
                                </div><!--/.h-stacksToSidesMed-->

                                <iAppsPro:CheckoutPaymentMethodSelector ID="wseppPaymentSelector" runat="server" />
                            </asp:View>

                            <asp:View runat="server" ID="vwPaypal">
                                <p class="alert alert--yellow icon-attention">You have selected PayPal as your method of payment.  Clicking continue will direct you to the PayPal website where you can enter your payment details, once completed you will return here to complete your purchase.</p>
                            </asp:View>

                            <asp:View runat="server" ID="vwOnAccount">

                            </asp:View>

                            <asp:View runat="server" id="vwNewPaymentMethod">
                                <div class="sideToSideMed h-pushBottom">
                                    <h1 class="h-textLg">Enter New Credit Card</h1>
                                    <div runat="server" id="divCancelNewCard" class="navOptions navOptions--sm navOptions--medHorizontal"><span class="icon-cog"></span>
                                        <asp:LinkButton runat="server" id="lbtnCancelNewCard" CssClass="icon-close" OnClick="lbtnCancelNewCard_Click" CausesValidation="false" >Cancel New Credit Card</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row row--XTight">
                                    <iAppsPro:CheckoutPaymentMethodForm ID="wseppNewPaymentMethod" runat="server" IsCustomer="True" />
                                </div>

                            </asp:View>

                            <asp:View runat="server" id="vwGuestCheckout">
                                <h2 class="h-textLg">Payment Method</h2>
                                <div class="matrix matrix--XTight">
                                    <iAppsPro:CheckoutPaymentMethodForm ID="wseppGuestPaymentForm" runat="server" IsCustomer="False" />
                                </div>
                            </asp:View>
                        </asp:MultiView>

                    </div>

                    <div class="column lg-6">
                        <table class="table">
                            <tr>
                                <th>Subtotal:</th>
                                <td><%=CurrentOrder.Total.ToString("c") %></td>
                            </tr>
                            <tr>
                                <th>Handling:</th>
                                <td><%=this.TotalHandling.ToString("c") %></td>
                            </tr>
                            <tr>
                                <th>Shipping:</th>
                                <td><%=(CurrentOrder.TotalShippingCharge - this.TotalHandling).ToString("c") %></td>
                            </tr>
                            <tr>
                                <th>Tax:</th>
                                <td><%=CurrentOrder.TotalTax.ToString("c") %></td>
                            </tr>
                            <tr>
                                <th>Discounts/Coupons:</th>
                                <td><%=CurrentOrder.TotalDiscount.ToString("c") %></td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td><span class="h-textLg"><%=CurrentOrder.GrandTotal.ToString("c") %></span></td>
                            </tr>
                        </table>
                    </div>

                    <div class="column lg-18">
                        <div class="formFooter"><asp:LinkButton runat="server" id="lbtnNext" CssClass="btn icon-check">Continue to review </asp:LinkButton></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />

</div>
<script>
    $(document).ready(function () {
        $('.swapBox input[type=checkbox]').change(
            function ()
            {
                if (!$(this).is(':checked'))
                {
                    $('#<%=spanCouponError.ClientID%>').hide();
                    $('#<%=txtCouponCode.ClientID%>').val('');
                }
            });
    });
</script>
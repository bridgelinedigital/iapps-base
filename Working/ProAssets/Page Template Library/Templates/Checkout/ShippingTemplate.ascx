﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ShippingTemplate.ascx.cs" Inherits="Bridgeline.iAPPS.Pro.UI.Templates.Checkout.ShippingTemplate" %>

<div class="pageWrap">
    <iAppsPro:HeaderMainStripped id="Header" runat="server" />

    <main>
        <div class="section">
            <div class="contained">
                <ul class="status-bar">
                    <li class="current">Shipping</li>
                    <li>Payment</li>
                    <li>Review</li>
                </ul>

                <div class="row">
                    <div class="column lg-18">
                        <asp:MultiView runat="server" id="mvShipping" ActiveViewIndex="0">
                            <asp:View runat="server" id="vwCurrentAddress">
                                <h1 class="h-textLg">Shipping Address</h1>
                                <div class="infoAction">
                                    <div class="infoAction-item">
                                        <div class="infoAction-title"><asp:Literal runat="server" ID="litAddressNickname">Home</asp:Literal></div>
                                        <div class="infoAction-info"><asp:Literal runat="server" id="litAddressName">John Smith</asp:Literal></div>
                                        <div class="infoAction-info"><asp:Literal runat="server" ID="litAddressPhone">555.555.5555</asp:Literal></div>
                                        <div class="infoAction-info">
                                            <asp:Literal runat="server" ID="litAddressStreet">1234 main Street</asp:Literal><br />
                                            <asp:Literal runat="server" ID="litAddressCityStZip">Anytown, 90210 USA</asp:Literal>
                                        </div>
                                        <asp:LinkButton runat="server" ID="lbtnChangeAddress" CssClass="infoAction-action infoAction-action--change" OnClick="lbtnChangeAddress_Click"> Change </asp:LinkButton>
                                    </div>
                                </div>
                                <h2 class="h-textLg">Shipping Method</h2>
                                <asp:Label runat="server" ID="lblShippingOptions" AssociatedControlID="ddlShippingOptions" CssClass="h-visuallyHidden">Shipping Method</asp:Label>
                                <asp:DropDownList runat="server" id="ddlShippingOptions"></asp:DropDownList>
                            </asp:View>

                            <asp:View runat="server" id="vwSelectAddress">
                                <div class="sideToSideMed h-pushBottom">
                                    <h1 class="h-textLg">Select Shipping Address</h1>
                                    <div class="navOptions navOptions--sm navOptions--smHorizontal"><span class="icon-cog"></span>
                                        <asp:LinkButton runat="server" id="lbtnCancelSelect" CausesValidation="false" CssClass="icon-close" OnClick="lbtnCancelSelect_Click">Cancel</asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="lbtnNewAddress" CssClass="icon-plus" OnClick="lbtnNewAddress_Click">New Address</asp:LinkButton>
                                    </div>
                                </div><!--/.h-stacksToSidesMed-->

                                <iAppsPro:ShippingAddressSelector ID="wseppAddressSelector" runat="server" />
                            </asp:View>

                            <asp:View runat="server" id="vwNewAddress">
                                <div class="sideToSideMed h-pushBottom">
                                    <h1 class="h-textLg">Enter New Shipping Address</h1>
                                    <div runat="server" id="divCancelNewAddress" class="navOptions navOptions--sm navOptions--medHorizontal"><span class="icon-cog"></span>
                                        <asp:LinkButton runat="server" id="lbtnCancelNewAddress" CausesValidation="false" CssClass="icon-close" OnClick="lbtnCancelNewAddress_Click">Cancel New Address</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row row--XTight">
                                    <iAppsPro:ShippingAddressForm ID="wseppNewAddressForm" runat="server" IsCustomer="true" />
                                </div>

                            </asp:View>

                            <asp:View runat="server" id="vwGuestCheckout">
                                <h2 class="h-textLg">Shipping Address</h2>
                                <div class="matrix matrix--XTight">
                                    <iAppsPro:ShippingAddressForm ID="wseppGuestAddressForm" runat="server" />
                                </div>
                            </asp:View>
                        </asp:MultiView>
                    </div>

                    <div class="column lg-6">
                        <table class="table table--bordered  table--striped  table--data">
                            <tr>
                                <th>Subtotal:</th>
                                <td><%=CurrentOrder.Total.ToString("c") %></td>
                            </tr>
                            <tr>
                                <th>Handling:</th>
                                <td><%=this.TotalHandling.ToString("c") %></td>
                            </tr>
                        </table>
                    </div>

                    <div class="column lg-18">
                        <div class="formFooter"><asp:LinkButton runat="server" id="lbtnNext" CssClass="btn icon-check">Continue to payment</asp:LinkButton></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <iAppsPro:FooterMain id="Footer" runat="server" />

</div>

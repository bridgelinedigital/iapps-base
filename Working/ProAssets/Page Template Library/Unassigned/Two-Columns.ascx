﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Two-Columns.ascx.cs" Inherits="Bridgeline.iAPPS.Marketier.DefaultTemplates.Generic.TwoColumns" %>

<style type="text/css">
    body, #container { padding: 0; color: #41474e; background: #f3f4f5; }
    #container { width: 100%; }
    #container td.container { padding: 0 20px; text-align: center; }
    #template { width: 600px; font-family: Arial, sans-serif; line-height: 1; text-align: left; }
    #template h1 { margin: 0 0 15px 0; font-size: 26px; font-weight: normal; color: #41474e; line-height: 32px; }
    #template h2 { margin: 0 0 15px 0; font-size: 20px; font-weight: normal; color: #41474e; line-height: 25px; }
    #template h3 { margin: 0 0 15px 0; font-size: 18px; font-weight: normal; color: #41474e; line-height: 22px; }
    #template h4 { margin: 0 0 10px 0; font-size: 16px; font-weight: normal; color: #41474e; line-height: 20px; }
    #template p { margin: 0; }
    #template a { color: #00789f; text-decoration: none; }
    #template img { margin: 0; border: 0; }
    #template #preheader { font-family: Arial, sans-serif; color: #41474e; font-size: 10px; line-height: 12px; }
    #template #preheader td.teaser { width: 65%; }
    #template #preheader td.view-in-browser { width: 35%; }
    #template #header { background: #ffffff; font-family: Arial, sans-serif; line-height: 1; }
    #template #header img { display: block; width: 100%; }
    #template #header img.iapps-placeholder-icon { width: auto; }
    #template #promo { background: #e7e9eb; font-family: Arial, sans-serif; font-size: 16px; color: #41474e; line-height: 24px; }
    #template #promo td.left { width: 60%; }
    #template #promo td.right { width: 40%; }
    #template #promo td.right img { display: block; }
    #template #content, #template #content #primary-content { background: #ffffff; font-family: Arial, sans-serif; font-size: 14px; color: #41474e; line-height: 21px; }
    #template #content { width: 100%; }
    #template #content #primary-content { width: 380px; }
    #template #content #sidebar { width: 160px; font-family: Arial, sans-serif; font-size: 12px; color: #41474e; line-height: 18px; }
    #template #content td.columns-2 { width: 50%; }
    #template #content td.columns-3 { width: 33.3333%; }
    #template #content p { margin-bottom: 20px; }
    #template #content p:last-child { margin-bottom: 0; }
    #template #footer { font-family: Arial, sans-serif; font-size: 10px; color: #b4b9c0; line-height: 15px; text-align: center; }
    #template #footer td { padding: 10px 20px; }
    #template #footer p { margin-bottom: 5px; }
    #template #footer p:last-child { margin: 0; }
    #template #footer a { color: #b4b9c0; }
</style>
<style type="text/css" media="screen and (max-device-width: 480px)">
    body[yahoo] #container td.container { padding: 0 10px !important; }
    body[yahoo] #template { width: 100% !important; }
    body[yahoo] #template #preheader td { display: block !important; width: 100% !important; }
    body[yahoo] #template #preheader td.teaser { padding-bottom: 10px; }
    body[yahoo] #template #promo td { display: block !important; width: 100% !important; }
    body[yahoo] #template #promo td.left { padding-bottom: 20px; }
    body[yahoo] #template #promo td.right img { width: 100% !important; }
    body[yahoo] #template #content #primary-content { margin-bottom: 20px; width: 100% !important; }
    body[yahoo] #template #content #sidebar { width: 100% !important; }
</style>
<table id="container" border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td class="container" valign="top">
                <center>
                    <table id="template" border="0" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <td valign="top">
                                    <table id="preheader" width="100%" border="0" cellpadding="0" cellspacing="20">
                                        <tbody>
                                            <tr>
                                                <td class="teaser" valign="top">
                                                    <FWControls:FWTextContainer ID="fwTxtTeaserLeft" runat="server" ViewCssClass="preheader" />
                                                </td>
                                                <td class="view-in-browser" valign="top">
                                                    <FWControls:FWTextContainer ID="fwTxtTeaserRight" runat="server" ViewCssClass="preheader" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table id="header" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td valign="top">
                                                    <FWControls:FWImageContainer ID="fwImgHeader" runat="server" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:PlaceHolder ID="phPromo" runat="server">
                                <tr>
                                    <td valign="top">
                                        <table id="promo" width="100%" border="0" cellpadding="0" cellspacing="20">
                                            <tbody>
                                                <tr>
                                                    <td class="left" valign="top">
                                                        <FWControls:FWTextContainer ID="fwTxtPromoLeft" runat="server" ViewCssClass="promo" />
                                                    </td>
                                                    <td class="right" valign="top">
                                                        <FWControls:FWImageContainer ID="fwImgPromoRight" runat="server" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                            <tr>
                                <td valign="top">
                                    <table id="content" width="100%" border="0" cellpadding="0" cellspacing="20">
                                        <tbody>
                                            <tr>
                                                <td class="columns-2" valign="top">
                                                    <FWControls:FWTextContainer ID="fwTxtContent1" runat="server" ViewCssClass="content" />
                                                </td>   
                                                <td class="columns-2" valign="top">
                                                    <FWControls:FWTextContainer ID="fwTxtContent2" runat="server" ViewCssClass="content" />
                                                </td>                                                     
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td valign="top">
                                    <table id="footer" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td valign="top">
                                                    <center>
                                                        <FWControls:FWTextContainer ID="fwTxtFooter" runat="server" ViewCssClass="footer" />
                                                    </center>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </center>
            </td>
        </tr>
    </tbody>
</table>

﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:template match="/">

    <!--<textarea rows="10" cols="75">
      <xsl:copy-of select="*"/>
    </textarea>-->

    <xsl:for-each select="/ArrayOfMenuItem/MenuItem">
      <div class="column med-6">
        <span class="footerMain-heading">
          <xsl:value-of select="Title"/>
        </span>
        <nav class="footerMain-topNav">
          <xsl:call-template name="BuildChildItems">
            <xsl:with-param name="Children" select="Children" />
          </xsl:call-template>
        </nav>
      </div>
    </xsl:for-each>

    <!--
    <xsl:for-each select="//Nodes/Node">

        <div class="column med-6">
          <h4 class="footerMain-heading">
            <xsl:value-of select="@Title"/>
          </h4>
          <xsl:if test="count(Node) &gt; 0">
            <nav class="footerMain-topNav">
              <ul>
                <xsl:for-each select="Node">
                  <li>
                    <a href="{@Url}">
                      <xsl:value-of select="@Title"/>
                    </a>
                  </li>
                </xsl:for-each>
              </ul>
            </nav>
          </xsl:if>
        </div>
      </xsl:for-each>
      -->
  </xsl:template>

  <xsl:template name="BuildChildItems">
    <xsl:param name="Children" />

    <xsl:if test="count($Children/MenuItem) &gt; 0">
      <ul>
        <xsl:for-each select="$Children/MenuItem">
          <li>
            <a href="{Url}">
              <xsl:value-of select="Title"/>
            </a>
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>

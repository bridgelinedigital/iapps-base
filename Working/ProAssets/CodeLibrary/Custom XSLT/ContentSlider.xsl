﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:param name="Page" />
  <xsl:param name="PageSize" />
  <xsl:param name="RecordCount" />
  <xsl:param name="Title" />
  <xsl:param name="Url" />
  <xsl:param name="Pager" />


  <xsl:template match="/">

    <div>
      <xsl:attribute name="class">
        <xsl:text>section contentSlider</xsl:text>
        <xsl:if test="//DocumentElement/Records[1]/contentDefinitionNode[@objectId='backgroundContrast']/@select != ''">
          <xsl:value-of select="concat(' section--contrast', //DocumentElement/Records[1]/contentDefinitionNode[@objectId='backgroundContrast']/@select)"/>
        </xsl:if>
      </xsl:attribute>
      <div class="contained">
        <div class="contentSlider-content">
          <xsl:for-each select="//DocumentElement/Records">
            <div class="contentSlider-item">
              <p><xsl:value-of select="content"/></p>
              <xsl:if test="byLine != ''">
                <p class="contentSlider-byline"><xsl:value-of select="byLine"/></p>
              </xsl:if>
            </div>
          </xsl:for-each>
        </div>
      </div>
    </div>
    <script>
      $('.contentSlider-content').slick({
      arrows: false,
      dots: true,
      fade: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 4000,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      });
    </script>
  </xsl:template>
</xsl:stylesheet>










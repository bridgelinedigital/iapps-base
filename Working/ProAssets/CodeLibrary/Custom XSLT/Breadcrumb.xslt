﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  <xsl:output method="html" indent="yes" omit-xml-declaration="yes" />

  <xsl:template match="/">
    <xsl:if test="count(//Records) > 1 ">
      <ul class="navSecondary-breadcrumb">
        <xsl:for-each select="//Records">
          <li>
            <xsl:if test="position() = last()">
              <xsl:attribute name="class">active</xsl:attribute>
            </xsl:if>
            <a href="{Url}">
              <xsl:attribute name="href">
                <xsl:choose>
                  <xsl:when test="Url != ''">
                    <xsl:value-of select="Url"/>
                  </xsl:when>
                  <xsl:otherwise>javascript:void(0)</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <xsl:value-of select="Title"/>
            </a>
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
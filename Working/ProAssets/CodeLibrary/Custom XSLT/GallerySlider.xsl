﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="/">

    <div class="section gallery gallery--slider">
      <div class="contained">
        <div class="gallery-images">
          <xsl:for-each select="//DocumentElement/Records">
            <figure class="gallery-figure">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="URL"/>
                </xsl:attribute>
                <xsl:attribute name="title">
                  <xsl:value-of select="Title" />
                </xsl:attribute>
                <img width="300">
                  <xsl:attribute name="src">
                    <xsl:value-of select="URL" />
                  </xsl:attribute>
                  <xsl:attribute name="alt">
                    <xsl:value-of select="Title" />
                  </xsl:attribute>
                </img>
              </a>
              <xsl:if test="Description != ''">
                <figcaption>
                  <xsl:value-of select="Description" />
                </figcaption>
              </xsl:if>
            </figure>
          </xsl:for-each>
        </div>
      </div>
    </div>

    <script>
      $('.gallery-images').magnificPopup({
        delegate: 'figure:not(.slick-cloned) a',
        type: 'image',
        gallery: {
          enabled: true
        }
      });
    </script>
    <script>
      $('.gallery--slider .gallery-images').slick({
        slidesToShow: 6,
        slidesToScroll: 6,
        dots: false,
        slide: 'figure',
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4
          }
          }, {
          breakpoint: 641,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
          }, {
            breakpoint: 500,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
      });
    </script>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  

</xsl:stylesheet>
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:param name="Page" />
  <xsl:param name="PageSize" />
  <xsl:param name="RecordCount" />
  <xsl:param name="Pager" />

  <xsl:template match="/">
    <div class="section listContent">
      <div class="contained">
        <xsl:for-each select="//DocumentElement/Records">
          <div class="listContentItem">
            <xsl:if test="thumbnailImage != ''">
              <figure class="listContentItem-figure">
                <img src="{thumbnailImage}" alt="thumbnailImageAltText" />
              </figure>
            </xsl:if>
            <div class="listContentItem-body">
              <h3 class="listContentItem-heading">
                <a href="{PageUrl}">
                  <xsl:value-of select="PageTitle"/>
                </a>
              </h3>
              <xsl:if test="shortDescription != ''">
                <p>
                  <xsl:value-of select="shortDescription"/>
                </p>
              </xsl:if>
            </div>
          </div>
        </xsl:for-each>
        <xsl:if test="$Pager != ''">
          <div class="pagination">
            <div class="pageNumbers">
              <xsl:value-of select="$Pager"/>
            </div>
          </div>
        </xsl:if>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

      <xsl:template match="/">
    <div class="section socialFeed socialFeed--facebook">
      <div class="socialFeed-inner">
        <xsl:for-each select="//DocumentElement/Records">
          <div class="socialFeed-item">
            <figure class="socialFeed-figure">
              <a href="">
                <img>
                  <xsl:attribute name="src">
                    <xsl:call-template name="Replace">
                      <xsl:with-param name="text" select="mainImage" />
                    </xsl:call-template>
                  </xsl:attribute>
                </img>
              </a>
            </figure>
            <div class="socialFeed-content">
              <h5 class="socialFeed-user">
                <xsl:value-of select="username" />
              </h5>
              <ul class="infoList socialFeed-infoList">
                <li>
                  <xsl:value-of select="dateOfPost" />
                </li>
              </ul>
              <p>
                <xsl:value-of select="content"/>
              </p>
            </div>
          </div>
        </xsl:for-each>
      </div>
    </div>

    <script>
      $('.socialFeed--facebook .socialFeed-inner').slick({
      arrows: false,
      dots: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 4000,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 2,
      responsive: [{
      breakpoint: 641,
      settings: {
      slidesToShow: 1,
      slidesToScroll: 1
      }
      }]
      });
    </script>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  

</xsl:stylesheet>
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:param name="Page" />
  <xsl:param name="PageSize" />
  <xsl:param name="RecordCount" />
  <xsl:param name="Title" />
  <xsl:param name="Url" />
  <xsl:param name="Pager" />

  <xsl:template match="/">
    <div class="section">
      <div class="contained">
        <ul class="accordion">
          <xsl:for-each select="//DocumentElement/Records">
            <xsl:variable name="iconClass">
              <xsl:if test="icon != ''">
                <xsl:value-of select="icon"/>
              </xsl:if>
            </xsl:variable>
            <li>
              <span class="{$iconClass}"><xsl:value-of select="title" /></span>
              <div class="accordion-content">
                <xsl:value-of select="content"/>
              </div>
            </li>
          </xsl:for-each>
        </ul>
      </div>
    </div>
    <script>
      initAccordion();
    </script>
  </xsl:template>
</xsl:stylesheet>

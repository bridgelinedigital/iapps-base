﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:param name="Page" />
  <xsl:param name="PageSize" />
  <xsl:param name="RecordCount" />
  <xsl:param name="Pager" />

  <xsl:template match="/">
    <div class="section listContent">
      <div class="contained">
        <xsl:for-each select="//DocumentElement/Records">
          <div class="listContentItem">
            <xsl:if test="primaryImage != ''">
              <figure class="listContentItem-figure">
                <img src="{primaryImage}" alt="" />
              </figure>
            </xsl:if>
            <div class="listContentItem-body">
              <ul class="infoList blogItem-infoList">
                <xsl:if test="primaryDate">
                  <li>
                    <xsl:call-template name="_formatDate">
                      <xsl:with-param name="date" select="primaryDate" />
                    </xsl:call-template>
                  </li>
                </xsl:if>
                <xsl:if test="startDate">
                  <li>
                    <xsl:call-template name="_formatDate">
                      <xsl:with-param name="date" select="startDate" />
                    </xsl:call-template>
                    <xsl:if test="endDate">
                      -
                      <xsl:call-template name="_formatDate">
                        <xsl:with-param name="date" select="endDate" />
                      </xsl:call-template>
                    </xsl:if>
                  </li>
                </xsl:if>
                <xsl:if test="authorName">
                  <li>
                    <xsl:value-of select="authorName"/>
                  </li>
                </xsl:if>
                <xsl:if test="locationName">
                  <li>
                    <xsl:value-of select="locationName"/>
                  </li>
                </xsl:if>
              </ul>
              <h3 class="listContentItem-heading">
                <a href="{PageUrl}">
                  <xsl:choose>
                    <xsl:when test="articleTitle != ''">
                      <xsl:value-of select="articleTitle"/>
                    </xsl:when>
                    <xsl:when test="eventTitle != ''">
                      <xsl:value-of select="eventTitle"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="PageTitle"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </a>
              </h3>
              <xsl:if test="shortDescription != ''">
                <p>
                  <xsl:value-of select="shortDescription"/>
                  <a href="{PageUrl}" class="trailingLink">more information</a>
                </p>
              </xsl:if>
            </div>
          </div>
        </xsl:for-each>
        <xsl:if test="$Pager != ''">
          <div class="pagination">
            <div class="pageNumbers">
              <xsl:value-of select="$Pager"/>
            </div>
          </div>
        </xsl:if>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="_formatDate">
    <!-- new date format to set just the month : current format: 2010-08-25T10:15:00+05:30 -->
    <xsl:param name="date" />
    <!-- Month -->
    <xsl:variable name="month" select="substring($date, 6, 2)"/>
    <xsl:choose>
      <xsl:when test="$month='01'">Jan</xsl:when>
      <xsl:when test="$month='02'">Feb</xsl:when>
      <xsl:when test="$month='03'">Mar</xsl:when>
      <xsl:when test="$month='04'">Apr</xsl:when>
      <xsl:when test="$month='05'">May</xsl:when>
      <xsl:when test="$month='06'">June</xsl:when>
      <xsl:when test="$month='07'">July</xsl:when>
      <xsl:when test="$month='08'">Aug</xsl:when>
      <xsl:when test="$month='09'">Sept</xsl:when>
      <xsl:when test="$month='10'">Oct</xsl:when>
      <xsl:when test="$month='11'">Nov</xsl:when>
      <xsl:when test="$month='12'">Dec</xsl:when>
      <xsl:otherwise> </xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <!-- new date format to set just the day : current format: 2010-08-25T10:15:00+05:30 -->
    <!-- Day -->
    <xsl:value-of select="substring(normalize-space($date), 9, 2)" />
    <xsl:text>, </xsl:text>
    <!--year-->
    <xsl:value-of select="substring(normalize-space($date), 1, 4)" />
  </xsl:template>

</xsl:stylesheet>
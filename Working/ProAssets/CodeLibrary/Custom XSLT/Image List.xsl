﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <div class="section listImages">
      <div class="contained">
        <div class="row">
          <xsl:for-each select="//DocumentElement/Records">
            <div class="column sm-12 lg-6">
              <figure class="listImages-figure">
                <xsl:if test="thumbnailImage">
                  <a href="{PageUrl}">
                    <img src="{thumbnailImage}" alt="{thumbnailImageAltText}" />
                  </a>
                </xsl:if>
                <figcaption class="listImages-figcaption">
                  <h3 class="listImages-subHeading">
                    <a href="{PageUrl}">
                      <xsl:value-of select="PageTitle" />
                    </a>
                  </h3>
                  <xsl:if test="shortDescription">
                    <p>
                      <xsl:value-of select="shortDescription"/>
                    </p>
                  </xsl:if>
                </figcaption>
              </figure>
            </div>
          </xsl:for-each>
        </div>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
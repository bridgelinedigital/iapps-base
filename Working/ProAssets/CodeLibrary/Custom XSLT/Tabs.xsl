﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <div class="section">
      <div class="contained">
        <ul class="tabs tabs--accordion">
          <xsl:for-each select="//DocumentElement/Records">
            <li>
              <a href="javascript:void(0)"><xsl:value-of select="tabName"/></a>
              <div><xsl:value-of select="content"/></div>
            </li>
          </xsl:for-each>
        </ul>
      </div>
    </div>
    <script>
      initTabs();
    </script>
  </xsl:template>
</xsl:stylesheet>

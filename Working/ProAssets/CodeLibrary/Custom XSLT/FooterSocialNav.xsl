﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <nav class="footerMain-topNav footerMain-topNav--socialNav">
      <ul>
        <xsl:for-each select="/ArrayOfMenuItem/MenuItem">
          <li>
            <a href="{Url}" class="footerMainSocialLink footerMainSocialLink--{CssClass}" aria-label="{CssClass}">&#160;</a>
          </li>
        </xsl:for-each>
      </ul>
    </nav>
  </xsl:template>

</xsl:stylesheet>

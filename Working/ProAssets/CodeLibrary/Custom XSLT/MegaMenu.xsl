﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <nav id="navMain" runat="server" class="navMain">
      <ul>
        <xsl:for-each select="/ArrayOfMenuItem/MenuItem">
          <li>
            <xsl:attribute name="class">
              <xsl:if test="count(Children/MenuItem) &gt; 0">hasChildren </xsl:if>
              <xsl:value-of select="CssClass"/>
            </xsl:attribute>
            <a href="{Url}"><xsl:value-of select="Title"/></a>

            <xsl:call-template name="BuildChildItems">
              <xsl:with-param name="Children" select="Children" />
            </xsl:call-template>
          </li>
        </xsl:for-each>
      </ul>
    </nav>
  </xsl:template>

  <xsl:template name="BuildChildItems">
    <xsl:param name="Children" />

    <xsl:if test="count($Children/MenuItem) &gt; 0">
      <ul>
        <xsl:for-each select="$Children/MenuItem">
          <li class="{CssClass}">
            <a href="{Url}"><xsl:value-of select="Title"/></a>
            <xsl:call-template name="BuildChildItems">
              <xsl:with-param name="Children" select="Children" />
            </xsl:call-template>
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>

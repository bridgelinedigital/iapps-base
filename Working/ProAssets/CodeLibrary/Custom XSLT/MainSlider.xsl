﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts">
    <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    
    <div class="slider sliderMain">
      <xsl:for-each select="//DocumentElement/Records">

        <xsl:variable name="classTextPosition">
          <xsl:if test="textPosition = 'center'">sliderMain-slide--centerCenter</xsl:if>
          <xsl:if test="textPosition = 'upper left'">sliderMain-slide--leftTop</xsl:if>
          <xsl:if test="textPosition = 'lower right'">sliderMain-slide--rightBottom</xsl:if>
        </xsl:variable>

        <xsl:variable name="classTextTheme">
          <xsl:if test="textTheme != ''">
            <xsl:value-of select="concat(' sliderMain-slide--text', textTheme)"/>
          </xsl:if>
        </xsl:variable>



        <xsl:variable name="imgLg">
          <xsl:call-template name="Replace">
            <xsl:with-param name="text" select="mainImage"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="imgSm">
          <xsl:call-template name="Replace">
            <xsl:with-param name="text" select="mobileImage"/>
          </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="targetUrl">
          <xsl:choose>
            <xsl:when test="targetPage != ''">
              <xsl:value-of select="targetPage"/>
            </xsl:when>
            <xsl:when test="targetFile != ''">
              <xsl:value-of select="targetFile"/>
            </xsl:when>
            <xsl:when test="targetExternal != ''">
              <xsl:value-of select="targetExternal"/>
            </xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        
        <div class="sliderMain-slide {$classTextPosition} {$classTextTheme}">
          <xsl:if test="$targetUrl != ''">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$targetUrl"/>
            </xsl:attribute>
            <xsl:call-template name="RotatorPanel">
              <xsl:with-param name="title" select="title" />
              <xsl:with-param name="description" select="description" />
              <xsl:with-param name="imgLg" select="$imgLg" />
              <xsl:with-param name="imgSm" select="$imgSm" />
            </xsl:call-template>
          </a>
          </xsl:if>
          <xsl:if test="$targetUrl = ''">
            <xsl:call-template name="RotatorPanel">
              <xsl:with-param name="title" select="title" />
              <xsl:with-param name="description" select="description" />
              <xsl:with-param name="imgLg" select="$imgLg" />
              <xsl:with-param name="imgSm" select="$imgSm" />
            </xsl:call-template>
          </xsl:if>
        </div>
      </xsl:for-each>
    </div>
    <script>
      $('.slider').slick({
      arrows: false,
      dots: true,
      fade: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 4000,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1
      });
    </script>
  </xsl:template>

  <xsl:template name="RotatorPanel">
    <xsl:param name="title" />
    <xsl:param name="description" />
    <xsl:param name="imgLg" />
    <xsl:param name="imgSm" />

    <div class="sliderMainImage" data-lg="{$imgLg}" data-sm="{$imgSm}">
      &#160;
    </div>
    <span class="sliderMainContent">
      <span class="sliderMainContent-inner">
        <span class="sliderMainContent-copy">
          <xsl:if test="$title != ''">
            <h2 class="sliderMainContent-heading">
              <xsl:value-of select="$title" />
            </h2>
          </xsl:if>
          <xsl:if test="$description != ''">
            <p>
              <xsl:value-of select="$description" />
            </p>
          </xsl:if>
        </span>
      </span>
    </span>
  </xsl:template>
  
  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
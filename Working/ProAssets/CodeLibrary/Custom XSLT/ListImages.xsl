﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:param name="Page" />
  <xsl:param name="PageSize" />
  <xsl:param name="RecordCount" />
  <xsl:param name="Title" />
  <xsl:param name="Url" />
  <xsl:param name="Pager" />

  <xsl:template match="/">

    <div class="section listImages">
      <div class="contained">
        <div class="row">

          <xsl:for-each select="//DocumentElement/Records">
            <xsl:call-template name="FeatureBlock">
              <xsl:with-param name="title" select="title" />
              <xsl:with-param name="description" select="description" />
              
              <xsl:with-param name="image" select="imageSmall" />
              <xsl:with-param name="targetPage" select="targetPage" />
              <xsl:with-param name="targetFile" select="targetFile" />
              <xsl:with-param name="targetExternal" select="targetExternal" />
              
              <xsl:with-param name="calloutTargetPage" select="calloutTargetPage" />
              <xsl:with-param name="calloutTargetFile" select="calloutTargetFile" />
              <xsl:with-param name="calloutTargetExternal" select="calloutTargetExternal" />
              
              <xsl:with-param name="calloutButtonText" select="calloutButtonText" />
            </xsl:call-template>
          </xsl:for-each>

        </div>
      </div>
    </div>
    
  </xsl:template>

  <xsl:template name="FeatureBlock">
    <xsl:param name="title" />
    <xsl:param name="description" />
    <xsl:param name="image" />
    <xsl:param name="targetPage" />
    <xsl:param name="targetFile" />
    <xsl:param name="targetExternal" />
    <xsl:param name="calloutTargetPage" />
    <xsl:param name="calloutTargetFile" />
    <xsl:param name="calloutTargetExternal" />
    <xsl:param name="calloutButtonText" />

    <xsl:variable name="targetUrl">
      <xsl:choose>
        <xsl:when test="$targetPage != ''">
          <xsl:value-of select="$targetPage"/>
        </xsl:when>
        <xsl:when test="$targetFile != ''">
          <xsl:value-of select="$targetFile"/>
        </xsl:when>
        <xsl:when test="$targetExternal != ''">
          <xsl:value-of select="$targetExternal"/>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="calloutTargetUrl">
      <xsl:choose>
        <xsl:when test="$calloutTargetPage != ''">
          <xsl:value-of select="$calloutTargetPage"/>
        </xsl:when>
        <xsl:when test="$calloutTargetFile != ''">
          <xsl:value-of select="$calloutTargetFile"/>
        </xsl:when>
        <xsl:when test="$calloutTargetExternal != ''">
          <xsl:value-of select="$calloutTargetExternal"/>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <div class="column sm-12 lg-6">
      <figure class="listImages-figure">
        <a>
          <xsl:attribute name="href">
            <xsl:value-of select="$targetUrl"/>
          </xsl:attribute>
          <img>
            <xsl:attribute name="src">
              <xsl:call-template name="Replace">
                <xsl:with-param name="text" select="$image" />
              </xsl:call-template>
            </xsl:attribute>
            <xsl:attribute name="alt">
              <xsl:value-of select="$title" />
            </xsl:attribute>
          </img>
        </a>
        <figcaption class="listImages-figcaption">
          <xsl:if test="$title != ''">
            <h3 class="listImages-subHeading">
              <xsl:value-of select="$title" />
            </h3>
          </xsl:if>
          <xsl:if test="$description != '' or $targetUrl != ''">
            <p>
              <xsl:if test="$description != ''">
                <xsl:value-of select="$description" />&#160;
              </xsl:if>
              <xsl:if test="$targetUrl != ''">
                <a class="trailingLink">
                  <xsl:attribute name="href">
                    <xsl:value-of select="$targetUrl"/>
                  </xsl:attribute>
                  Learn More
                </a>
              </xsl:if>
            </p>
          </xsl:if>
          <xsl:if test="$calloutTargetUrl != '' and $calloutButtonText != ''">
            <p>
              <a class="btn">
                <xsl:attribute name="href">
                  <xsl:value-of select="$calloutTargetUrl"/>
                </xsl:attribute>
                <xsl:value-of select="$calloutButtonText" />
              </a>
            </p>
          </xsl:if>
        </figcaption>
      </figure>
    </div>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
;(function(window, document, $, undefined) {

	var map, locations, markers, bounds, infoWindow, directionsService, directionsRenderer, metroCountry, metroRadius;

    var init = function() {
	    var mapCanvas = $('#map-canvas');
	    if (mapCanvas.length > 0 && mapCanvas.data('map-init') != 'true') {
	        var lat = mapCanvas.data('map-lat');
	        var lng = mapCanvas.data('map-lng');
	        var location = new google.maps.LatLng(lat, lng);
	        var zoom = mapCanvas.data('map-zoom') ? mapCanvas.data('map-zoom') : 4;

	        map = new google.maps.Map(mapCanvas.get(0), {
	            center: location,
	            mapTypeId: google.maps.MapTypeId.ROADMAP,
	            zoom: zoom
	        });

	        infoWindow = new google.maps.InfoWindow({ maxWidth: 200 });

	        mapCanvas.data('map-init', 'true');

	        locations = [];
	        if (typeof mapCanvasData !== 'undefined' && $.isArray(mapCanvasData)) {
	            locations = mapCanvasData
	        }
	        markers = [];
	        addMarkers();
	        metroCountry = "United States";
	        metroRadius = 2000;
	    }

	    $('#pnlStoreLocator .form-control-location, #pnlStoreLocator .form-control-route').on('keypress', function (e) {
	        if (e.which == 13) {
	            validateLocator();
	        }
	    });

	    $('#pnlStoreLocator .form-control-route').on('keypress', function (e) {
	        $('#txtAddress').val('');
	    });

	    $('#btnSearch').on('click', function (e) {
	        validateLocator();
	    });
	}

	var addMarkers = function() {
		var location, position, i, il;
		if (locations.length > 0) {
			bounds = new google.maps.LatLngBounds();
			for (i = 0, il = locations.length; i < il; i++) {
				location = locations[i];
				position = new google.maps.LatLng(location.lat, location.lng);
				addMarker(position, location.icon, location.title, location.content);
				bounds.extend(position);			
			}
		}
	};

	var addMarker = function(position, icon, title, content) {
		var marker = new google.maps.Marker({
			map: map,
			position: position,
			draggable: false,
			animation: google.maps.Animation.DROP,
			icon: icon
		});

  		if (title || content) {
  			attachInfoWindow(marker, title, content);
  		}

  		markers.push(marker);
	};

	var clearMarkers = function() {
		setMarkersMap(null);
	};

	var showMarkers = function() {
		setMarkersMap(map);
	};

	var setMarkersMap = function(map) {
		for (var i = 0, il = markers.length; i < il; i++) {
			markers[i].setMap(map);
		}
	};
    
	var clearMap = function () {
	    clearMarkers();

	    if (typeof directionsRenderer !== 'undefined') {
	        directionsRenderer.setMap(null);
	    }
	};

	var processMap = function (results) {
	    convertResults(results);
	    addMarkers();
	    addLocations();
	    refresh();
	};

	var addLocations = function() {
	    var location, i, il;
	    if (locations.length > 0) {

	        $('.results-bar').show();
	        $('.locations-count').html(locations.length);
	        $('.location-zip').html($('#txtAddress').val());

	        var list = $('<ol class="locationResults-list">');
	        for (i = 0, il = locations.length; i < il; i++) {
	            location = locations[i];

	            var item = $('<li itemscope itemtype="http://schema.org/LocalBusiness" class="locationResults-item">');
	            item.append($('<h4 itemprop="name" class="locationResults-name">' + location.title + '</h4>'));
	            item.append(location.content);
	            item.append($('</li>'));

	            list.append(item);
	        }
	        list.append('</ol>');
	        $('.locations-list').empty().append(list);
	    }
	    $(document).trigger('viewport:resize:after');
	};

	var refresh = function() {
		if (bounds) {
			if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
				var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
				var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01);
				bounds.extend(extendPoint1);
				bounds.extend(extendPoint2);
			}
			map.fitBounds(bounds);
		}
	};	

	var attachInfoWindow = function(marker, title, content) {
		var html = "<div class=\"infowindow\">";
		if (title) {
			html += '<h2>' + title + '</h2>';
		}
		if (content) {
			html += content;
		}
		html += "</div>";

		google.maps.event.addListener(marker, "click", function(e) {
			infoWindow.close();
			infoWindow.setContent(html);
			infoWindow.open(map, marker);
		});
	};

	var mapLocations = function (address, radius) {
	    clearMap();

	    if (!$.isNumeric(radius)) {
	        radius = "5";
	    }

	    mapLocationsService(address, radius);
	};

	var mapLocationsService = function (address, radius) {

	    var geocoder = new google.maps.Geocoder();
	    geocoder.geocode({ 'address': address }, function (results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	            lat = results[0].geometry.location.lat();
	            lng = results[0].geometry.location.lng();

	            var SearchByLocationRequest = { ParentSiteIds: StoreLocator.ParentSiteIds, MetroAreaName: StoreLocator.MetroArea, SearchLocation: address, Latitude: lat, Longitude: lng, Radius: radius };

	            $.getJSON("/iappspro-api/LocationFinder/SearchByLocation", SearchByLocationRequest)
                    .done(function (data) {
                        if (data.Success || data.success) {
                            if (data.results.length > 0) {
                                $('.locationResults-heading').show();
                                processMap(data.results);
                            } else {
                                
                                var radiusValue = $('#ddlRadius option:selected').next().val();

                                if (radiusValue) {
                                    $('#ddlRadius').val(radiusValue);
                                    mapLocationsService(address, radiusValue);
                                } else {
                                    if (data.results.length > 0) {
                                        processMap(data.results);
                                    } else {
                                        $('.locationResults-heading').hide();
                                        $('.locations-list').empty();
                                        $("#errors").html(StoreLocator.NoResultsText);
                                    }
                                }
                            }
                        }
                    })
                    .fail(function (jqXHR, textStatus, err) {
                        //$('#product').text('Error: ' + err);
                    });

	        } else {// end of the gecoding if
	            $('.locationResults-heading').hide();
	            $('.locations-list').empty();
	            $("#errors").html(StoreLocator.BadAddressText);
	        }
	    });
	};
	
	var mapDirections = function(from, to) {
	    clearMap();

	    if (typeof directionsService === 'undefined') {
			directionsService = new google.maps.DirectionsService();
		}

	    if (typeof directionsRenderer === 'undefined') {
	        directionsRenderer = new google.maps.DirectionsRenderer({ map: map });
	    } else {
	        directionsRenderer.setMap(map);
	    }
        
		var directionsRequest = {
			origin: from,
			destination: to,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
		};

		directionsService.route(directionsRequest, function(response, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
				mapDirectionsService(response, StoreLocator.DefaultRouteRadius);
			} else {
		        $('.locationResults-heading').hide();
		        $('.locations-list').empty();
		        $("#errors").html("We couldn't find the location you searched. Please verify and try again.");
			}
		});
	};

	var mapDirectionsService = function (response, radius) {
	    var boxer = new RouteBoxer();
	    var boxes = boxer.box(response.routes[0].overview_path, radius * 1.609344);

	    var SearchByRouteRequest = { ParentSiteIds: StoreLocator.ParentSiteIds, MetroAreaName: StoreLocator.MetroArea, RouteBoxes: boxes.toString() };

	    $.getJSON("/iappspro-api/LocationFinder/SearchByRoute", SearchByRouteRequest)
            .done(function (data) {
                if (data.Success || data.success) {
                    if (data.results.length > 0) {
                        directionsRenderer.setDirections(response);
                        processMap(data.results);
                    } else {
                        radius += 10;
                        if (radius < 60) {
                            mapDirectionsService(response, radius);
                        } else {
                            if (data.results.length > 0) {
                                directionsRenderer.setDirections(response);
                                processMap(data.results);
                            } else {
                                $('.locationResults-heading').hide();
                                $('.locations-list').empty();
                                $("#errors").html(StoreLocator.NoResultsText);
                            }
                        }
                    }
                }
            })
            .fail(function (jqXHR, textStatus, err) {
                //$('#product').text('Error: ' + err);
            });
	};

	var convertResults = function(results) {

	    var result, location, i, il;

		locations = [];

		for (i = 0, il = results.length; i < il; i++) {
		    result = results[i];
			
			location = {
				lat: result.lat,
				lng: result.long,
				icon: '//chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + (i+1) + '|db2525|000000',
				title: result.title
			};

			location.content = '<p>' + 
                result.fullAddress.split('|').join('<br />') +
                '</p>' +
				'<p>' +
                '<a class="btn" href="http://maps.google.com/?saddr=Current+Location&daddr=' + result.lat + ',' + result.long + '" target="_blank">Directions</a> ' +
                '<a class="btn" href="' + result.url + '" target="_blank">Website</a>' +
                '</p>';

			locations.push(location);
		}

	};

	var validateLocator = function() {

	    var address = $('#txtAddress');
	    var addressValue = address.val().trim();

	    var radius = $('#ddlRadius');
	    var radiusValue = radius.val();
	    if (!radiusValue) {
	        radiusValue = StoreLocator.DefaultSearchRadius;
	        radius.val(radiusValue);
	    }

	    var addressStart = $('#txtAddressStart');
	    var addressStartValue = addressStart.val().trim();

	    var addressEnd = $('#txtAddressEnd');
	    var addressEndValue = addressEnd.val().trim();

	    address.closest('.formRow').removeClass('has-error');
	    addressStart.closest('.formRow').removeClass('has-error');
	    addressEnd.closest('.formRow').removeClass('has-error');


	    if (addressValue == '' && addressStartValue == '' && addressEndValue == '')
	    {
	        $("#errors").html("Please enter an address");
	    }
	    else if (addressValue == '' &&  ((addressStartValue == '' && addressEndValue != '') || (addressStartValue != '' && addressEndValue == ''))) {
	        $("#errors").html("Please enter both addresses to search by route");
	    }
	    else {
	        if (addressValue != '') {
	            addressStart.val('');
	            addressEnd.val('');
	            $("#errors").html('');
	            mapLocations(addressValue, radiusValue);
	        } else if (addressStartValue != '' && addressEndValue != '') {
    	        $("#errors").html('');
	            mapDirections(addressStartValue, addressEndValue);
	        }
	    }
	};

	$(document).ready(function () {
	    init();

	    if ($('#isMetro').length > 0) {
	        mapLocations(metroCountry, metroRadius);
	    } else {
	        var paramLocation = getParameterByName('location');
	        var paramRadius = getParameterByName('radius');
	        if (paramLocation) {
	            $('#txtAddress').val(paramLocation);
	            $('#ddlRadius').val(paramRadius);
	            var paramRadiusValue = $('#ddlRadius').val();
	            if (!paramRadiusValue) {
	                paramRadiusValue = StoreLocator.DefaultSearchRadius;
	                $('#ddlRadius').val(paramRadiusValue);
	            }
	            mapLocations(paramLocation, paramRadiusValue);
	        }
	    }
	});

	$(document).on('viewport:sm viewport:md viewport:lg', function(e) {
		refresh();
	});

	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
		var results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

})(window, document, jQuery);
;(function(window, document, $, undefined) {

	var locationsListOffset = 4;
	
	var locationsListReset = function() {
		if ($.fn.fitrows) {
			$('.locations-list').each(function(index) {
				$(this).find('li').fitrows({
					itemOffset: locationsListOffset
				});
			});
		}
	};

	$(document).on('viewport:mobile', function(e) {
		locationsListOffset = 1;
	});

	$(document).on('viewport:xs', function(e) {
		locationsListOffset = 2;
	});

	$(document).on('viewport:sm', function(e) {
		locationsListOffset = 3;
	});

	$(document).on('viewport:md viewport:lg', function(e) {
		locationsListOffset = 4;
	});

	$(document).on('viewport:resize:after', function(e) {
		locationsListReset();
	});

	$(document).ready(function(e) {
		$('.locations-search .state-dropdown').on('change', function(e) {
			var select = $(this);
			if (select.val()) {
				$('.locations').hide();
				$('.locations-' + select.val()).show();
			} else {
				$('.locations').show();
			}
		});
	});

})(window, document, jQuery);	
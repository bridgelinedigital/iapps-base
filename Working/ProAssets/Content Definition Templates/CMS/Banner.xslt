﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="targetUrl">
    <xsl:choose>
      <xsl:when test="//contentDefinitionNode[@objectId='targetPage']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetPage']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='targetFile']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetFile']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='targetExternal']/@select != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetExternal']/@select"/>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:template match="/">
    <div>
      <xsl:attribute name="class">
        <xsl:text>section banner</xsl:text>
        <xsl:if test="//contentDefinitionNode[@objectId='backgroundContrast']/@select != ''"><xsl:value-of select="concat(' section--contrast', //contentDefinitionNode[@objectId='backgroundContrast']/@select)"/></xsl:if>
        <xsl:if test="//contentDefinitionNode[@objectId='textPosition']/@select != ''"><xsl:value-of select="concat(' banner--', //contentDefinitionNode[@objectId='textPosition']/@select)"/></xsl:if>
        <xsl:if test="//contentDefinitionNode[@objectId='textTheme']/@select != ''"><xsl:value-of select="concat(' banner--text', //contentDefinitionNode[@objectId='textTheme']/@select)"/></xsl:if>
        <xsl:if test="//contentDefinitionNode[@objectId='backgroundFixed']/@select = 'True'"> banner--fixedBackground</xsl:if>
		<xsl:if test="//contentDefinitionNode[@objectId='backgroundFixed']/@select = 'true'"> banner--fixedBackground</xsl:if>
      </xsl:attribute>
      <xsl:attribute name="style">
        <xsl:if test="//contentDefinitionNode[@objectId='backgroundImage']/@Value != ''">
          <xsl:text>background-image:url(</xsl:text>
          <xsl:call-template name="Replace">
            <xsl:with-param name="text" select="//contentDefinitionNode[@objectId='backgroundImage']/@Value" />
          </xsl:call-template>
          <xsl:text>);</xsl:text>
        </xsl:if>
      </xsl:attribute>
      <div class="banner-contained contained">
        <div class="bannerCopy">
          <h2 class="bannerCopy-heading"><xsl:value-of select="//contentDefinitionNode[@objectId='title']/@select"/></h2>
          <p><xsl:value-of select="//contentDefinitionNode[@objectId='description']/@select"/></p>
          <xsl:if test="//contentDefinitionNode[@objectId='calloutButtonText']/@select != '' and $targetUrl != ''">
            <p>
              <a href="{$targetUrl}">
                <xsl:attribute name="class">
                  <xsl:text>btn</xsl:text>
                  <xsl:if test="//contentDefinitionNode[@objectId='calloutButtonCss']/@select != ''">
                    <xsl:value-of select="concat(' ', //contentDefinitionNode[@objectId='calloutButtonCss']/@select)" />
                  </xsl:if>
                </xsl:attribute>

                <xsl:value-of select="//contentDefinitionNode[@objectId='calloutButtonText']/@select"/>
              </a>
            </p>
          </xsl:if>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="title" select="//contentDefinitionNode[@objectId='title']/@select" />
  <xsl:variable name="primaryImage" select="//contentDefinitionNode[@objectId='primaryImage']/@Value" />
  <xsl:variable name="primaryImageAltText" select="//contentDefinitionNode[@objectId='primaryImageAltText']/@select" />
  <xsl:variable name="content" select="//contentDefinitionNode[@objectId='content']/@select" />

  <xsl:template match="/">
    <div class="section">
      <div class="contained">
        <h2>
          <xsl:value-of select="$title" />
        </h2>
        <div class="newsDetail">
          <div class="newsDetail-bodyContent">
            <xsl:if test="$primaryImage">
              <figure class="newsDetail-image">
                <img>
                  <xsl:attribute name="src">
                    <xsl:call-template name="Replace">
                      <xsl:with-param name="text" select="$primaryImage" />
                    </xsl:call-template>
                  </xsl:attribute>
                  <xsl:attribute name="alt">
                    <xsl:value-of select="$primaryImageAltText" />
                  </xsl:attribute>
                </img>
              </figure>
            </xsl:if>
            <xsl:value-of select="$content" />
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
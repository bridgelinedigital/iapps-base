﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="eventTitle" select="//contentDefinitionNode[@objectId='eventTitle']/@select" />
  <xsl:variable name="startDate" select="//contentDefinitionNode[@objectId='startDate']/@select" />
  <xsl:variable name="endDate" select="//contentDefinitionNode[@objectId='endDate']/@select" />
  <xsl:variable name="locationName" select="//contentDefinitionNode[@objectId='locationName']/@select" />
  <xsl:variable name="address1" select="//contentDefinitionNode[@objectId='address1']/@select" />
  <xsl:variable name="address2" select="//contentDefinitionNode[@objectId='address2']/@select" />
  <xsl:variable name="city" select="//contentDefinitionNode[@objectId='city']/@select" />
  <xsl:variable name="state" select="//contentDefinitionNode[@objectId='state']/@select" />
  <xsl:variable name="zip" select="//contentDefinitionNode[@objectId='zip']/@select" />
  <xsl:variable name="mapLink" select="//contentDefinitionNode[@objectId='mapLink']/@select" />
  <xsl:variable name="directionsLink" select="//contentDefinitionNode[@objectId='directionsLink']/@select" />
  <xsl:variable name="contactName" select="//contentDefinitionNode[@objectId='contactName']/@select" />
  <xsl:variable name="contactPhone" select="//contentDefinitionNode[@objectId='contactPhone']/@select" />
  <xsl:variable name="button1Text" select="//contentDefinitionNode[@objectId='button1Text']/@select" />
  <xsl:variable name="button1PageLink" select="//contentDefinitionNode[@objectId='button1PageLink']/@Value" />
  <xsl:variable name="button2Text" select="//contentDefinitionNode[@objectId='button2Text']/@select" />
  <xsl:variable name="button2PageLink" select="//contentDefinitionNode[@objectId='button2PageLink']/@Value" />
  <xsl:variable name="button3Text" select="//contentDefinitionNode[@objectId='button3Text']/@select" />
  <xsl:variable name="button3PageLink" select="//contentDefinitionNode[@objectId='button3PageLink']/@Value" />
  <xsl:variable name="primaryImage" select="//contentDefinitionNode[@objectId='primaryImage']/@Value" />
  <xsl:variable name="eventContent" select="//contentDefinitionNode[@objectId='eventContent']/@select" />

  <xsl:template match="/">
    <div class="contained">
      <h1>
        <xsl:value-of select="$eventTitle" />
      </h1>
      <div class="eventDetail">
        <xsl:if test="$primaryImage != ''">
          <figure class="eventDetail-image">
            <img>
              <xsl:attribute name="src">
                <xsl:call-template name="Replace">
                  <xsl:with-param name="text" select="$primaryImage" />
                </xsl:call-template>
              </xsl:attribute>
              <xsl:attribute name="alt">
                <xsl:value-of select="$eventTitle" />
              </xsl:attribute>
            </img>
          </figure>
        </xsl:if>
        <div class="eventDetail-body">
          <table>
            <tbody>
              <tr>
                <th class="icon-calendar">Date</th>
                <td>
                  <xsl:call-template name="FormatDate">
                    <xsl:with-param name="Date" select="$startDate" />
                  </xsl:call-template>
                  <xsl:if test="$endDate != ''">
                    <xsl:text> - </xsl:text>
                    <xsl:call-template name="FormatDate">
                      <xsl:with-param name="Date" select="$endDate" />
                    </xsl:call-template>
                  </xsl:if>
                </td>
              </tr>
              <tr>
                <th class="icon-clock">Time</th>
                <td>
                  <xsl:call-template name="FormatTime">
                    <xsl:with-param name="Date" select="$startDate" />
                  </xsl:call-template>
                  <xsl:if test="$endDate != ''">
                    <xsl:text> - </xsl:text>
                    <xsl:call-template name="FormatDate">
                      <xsl:with-param name="Date" select="$endDate" />
                    </xsl:call-template>
                  </xsl:if>
                </td>
              </tr>
              <xsl:if test="$locationName != '' or $address1 != '' or $address2 != '' or $city != '' or $state !='' or $zip != '' or $mapLink != '' or $directionsLink != ''">
                <tr>
                  <th class="icon-location">Location</th>
                  <td class="eventDetail-locationDetail">
                    <xsl:if test="$locationName != ''">
                      <span class="eventDetail-locationDetail-title">
                        <xsl:value-of select="$locationName"/>
                      </span>
                    </xsl:if>
                    <xsl:if test="$address1 != '' or $address2 != '' or $city != '' or $state !='' or $zip != ''">
                      <address itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                        <xsl:if test="$address1 != ''">
                          <span itemprop="streetAddress">
                            <xsl:value-of select="$address1"/>
                            <xsl:if test="$address2 != ''">
                              <xsl:text>, </xsl:text>
                              <xsl:value-of select="$address2"/>
                            </xsl:if>
                            <xsl:if test="$city != '' or $state != '' or $zip != ''">
                              <xsl:text>, </xsl:text>
                            </xsl:if>
                          </span>
                        </xsl:if>
                        <xsl:if test="$city != ''">
                          <xsl:value-of select="$city"/>
                          <xsl:if test="$state != '' or $zip != ''">
                            <xsl:text>, </xsl:text>
                          </xsl:if>
                        </xsl:if>
                        <xsl:if test="$state != ''">
                          <xsl:value-of select="$state"/>
                          <xsl:if test="$zip != ''">
                            <xsl:text>, </xsl:text>
                          </xsl:if>
                        </xsl:if>
                        <xsl:if test="$zip != ''">
                          <xsl:value-of select="$zip"/>
                        </xsl:if>
                      </address>
                    </xsl:if>
                    <xsl:if test="$mapLink != '' or $directionsLink != ''">
                      <ul>
                        <xsl:if test="$mapLink != ''">
                          <li>
                            <a>
                              <xsl:attribute name="href">
                                <xsl:call-template name="Replace">
                                  <xsl:with-param name="text" select="$mapLink" />
                                </xsl:call-template>
                              </xsl:attribute>
                              <xsl:attribute name="target">
                                <xsl:text>_blank</xsl:text>
                              </xsl:attribute>
                              <xsl:text>Map</xsl:text>
                            </a>
                          </li>
                        </xsl:if>
                        <xsl:if test="$directionsLink != ''">
                          <li>
                            <a>
                              <xsl:attribute name="href">
                                <xsl:call-template name="Replace">
                                  <xsl:with-param name="text" select="$directionsLink" />
                                </xsl:call-template>
                              </xsl:attribute>
                              <xsl:attribute name="target">
                                <xsl:text>_blank</xsl:text>
                              </xsl:attribute>
                              <xsl:text>Directions</xsl:text>
                            </a>
                          </li>
                        </xsl:if>
                      </ul>
                    </xsl:if>
                  </td>
                </tr>
              </xsl:if>
              <xsl:if test="$contactName != '' or $contactPhone != ''">
                <th class="icon-profile">Contact</th>
                <td>
                  <xsl:if test="$contactName != ''">
                    <xsl:value-of select="$contactName"/>
                  </xsl:if>
                  <xsl:if test="$contactPhone != ''">
                    <xsl:if test="$contactName != ''">
                      <br/>
                    </xsl:if>
                    <a href="tel:">
                      <xsl:value-of select="$contactPhone"/>
                    </a>
                  </xsl:if>
                </td>
              </xsl:if>
            </tbody>
          </table>
          <xsl:if test="$button1Text != '' or $button2Text != '' or $button3Text != ''">
            <ul class="eventDetail-CTAs">
              <xsl:if test="$button1Text != '' and $button1PageLink != ''">
                <li>
                  <a>
                    <xsl:attribute name="href">
                      <xsl:call-template name="Replace">
                        <xsl:with-param name="text" select="$button1PageLink" />
                      </xsl:call-template>
                    </xsl:attribute>
                    <xsl:value-of select="$button1Text"/>
                  </a>
                </li>
              </xsl:if>
              <xsl:if test="$button2Text != '' and $button2PageLink != ''">
                <li>
                  <a>
                    <xsl:attribute name="href">
                      <xsl:call-template name="Replace">
                        <xsl:with-param name="text" select="$button2PageLink" />
                      </xsl:call-template>
                    </xsl:attribute>
                    <xsl:value-of select="$button2Text"/>
                  </a>
                </li>
              </xsl:if>
              <xsl:if test="$button3Text != '' and $button3PageLink != ''">
                <li>
                  <a>
                    <xsl:attribute name="href">
                      <xsl:call-template name="Replace">
                        <xsl:with-param name="text" select="$button3PageLink" />
                      </xsl:call-template>
                    </xsl:attribute>
                    <xsl:value-of select="$button3Text"/>
                  </a>
                </li>
              </xsl:if>
            </ul>
          </xsl:if>
        </div>
        <div class="eventDetail-bodyContent">
          <xsl:value-of select="$eventContent" />
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="FormatTime">
    <xsl:param name="Date" />
    <xsl:choose>
      <xsl:when test="((number(substring($Date,12,2))-12)>0)">
        <xsl:value-of select="number(substring($Date,12,2))-12"/>: <xsl:value-of select="substring($Date,15,2)"/> PM
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="substring($Date,12,2) = '00'" >
            12: <xsl:value-of select="substring($Date,15,2)"/> AM
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="substring($Date,12,2)"/>: <xsl:value-of select="substring($Date,15,2)"/> AM
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date" />
    <xsl:variable name="yyyy" select="substring($Date,1,4)" />
    <xsl:variable name="dd" select="substring($Date,9,2)" />
    <xsl:variable name="mm" select="substring($Date,6,2)" />

    <xsl:variable name="Y">
      <xsl:choose>
        <xsl:when test="$mm &lt; 3">
          <xsl:value-of select="$yyyy - 1"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$yyyy + 0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="y" select="$Y mod 100"/>
    <xsl:variable name="c" select="floor($Y div 100)"/>
    <xsl:variable name="d" select="$dd+0"/>
    <xsl:variable name="m">
      <xsl:choose>
        <xsl:when test="$mm &lt; 3">
          <xsl:value-of select="$mm + 12"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$mm + 0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="w" select="($d + floor(($m + 1) * 2.6) + $y + floor($y div 4) + floor($c div 4) - $c * 2 - 1) mod 7"/>

    <xsl:variable name="www">
      <xsl:choose>
        <xsl:when test="$w = 0">Sun</xsl:when>
        <xsl:when test="$w = 1">Mon</xsl:when>
        <xsl:when test="$w = 2">Tue</xsl:when>
        <xsl:when test="$w = 3">Wed</xsl:when>
        <xsl:when test="$w = 4">Thu</xsl:when>
        <xsl:when test="$w = 5">Fri</xsl:when>
        <xsl:when test="$w = 6">Sat</xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="mmm">
      <xsl:choose>
        <xsl:when test="$mm =  1">Jan</xsl:when>
        <xsl:when test="$mm =  2">Feb</xsl:when>
        <xsl:when test="$mm =  3">Mar</xsl:when>
        <xsl:when test="$mm =  4">Apr</xsl:when>
        <xsl:when test="$mm =  5">May</xsl:when>
        <xsl:when test="$mm =  6">Jun</xsl:when>
        <xsl:when test="$mm =  7">Jul</xsl:when>
        <xsl:when test="$mm =  8">Aug</xsl:when>
        <xsl:when test="$mm =  9">Sep</xsl:when>
        <xsl:when test="$mm = 10">Oct</xsl:when>
        <xsl:when test="$mm = 11">Nov</xsl:when>
        <xsl:when test="$mm = 12">Dec</xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:value-of select="concat($www, ', ', $mmm, ' ', $d)"/>

  </xsl:template>

</xsl:stylesheet>
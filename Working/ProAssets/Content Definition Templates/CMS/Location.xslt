﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="backgroundContrast" select="//contentDefinitionNode[@objectId='backgroundContrast']/@select" />
  <xsl:variable name="locationName" select="//contentDefinitionNode[@objectId='locationName']/@select" />
  <xsl:variable name="address1" select="//contentDefinitionNode[@objectId='address1']/@select" />
  <xsl:variable name="address2" select="//contentDefinitionNode[@objectId='address2']/@select" />
  <xsl:variable name="city" select="//contentDefinitionNode[@objectId='city']/@select" />
  <xsl:variable name="state" select="//contentDefinitionNode[@objectId='state']/@select" />
  <xsl:variable name="zipcode" select="//contentDefinitionNode[@objectId='zipcode']/@select" />
  <xsl:variable name="phone1" select="//contentDefinitionNode[@objectId='phone1']/@select" />
  <xsl:variable name="phone2" select="//contentDefinitionNode[@objectId='phone2']/@select" />
  <xsl:variable name="directionLink" select="//contentDefinitionNode[@objectId='directionLink']/@select" />
  <xsl:variable name="embeddedMapLink" select="//contentDefinitionNode[@objectId='mapURL']/@select" />
  <xsl:variable name="mapPosition" select="//contentDefinitionNode[@objectId='mapPosition']/@select" />
  <xsl:variable name="websiteUrl" select="//contentDefinitionNode[@objectId='websiteUrl']/@select" />

  <xsl:template match="/">
    <div>
      <xsl:attribute name="class">
        <xsl:text>section h-hard location</xsl:text>
        <xsl:if test="$backgroundContrast != ''">
          <xsl:value-of select="concat(' section--contrast', $backgroundContrast)"/>
        </xsl:if>
        <xsl:if test="$mapPosition != ''">
          <xsl:value-of select="concat(' location--map', $mapPosition)"/>
        </xsl:if>
      </xsl:attribute>

      <div class="location-map" id="locMap">
        &lt;iframe src="<xsl:value-of select="$embeddedMapLink" />" width="600" height="450" frameborder="0" style="border:0" allowfullscreen&gt;&lt;/iframe&gt;
      </div>
      <div itemscope="itemscope" class="location-content">
        <xsl:attribute name="itemtype">http://schema.org/LocalBusiness</xsl:attribute>
        <h3 itemprop="name" class="location-heading"><xsl:value-of select="$locationName"/></h3>
        <p>
          <span itemprop="address" itemscope="itemscope">
            <xsl:attribute name="itemtype">
              http://schema.org/PostalAddress
            </xsl:attribute>
            <span itemprop="streetAddress">
              <xsl:value-of select="$address1" />
              <xsl:if test="$address2 != ''">
                <br/>
                <xsl:value-of select="$address2" />
              </xsl:if>
              <br/>
              <span itemprop="addressLocality"><xsl:value-of select="$city" />,</span>
              <span itemprop="addressRegion"><xsl:value-of select="$state" /></span>
              <span itemprop="postalCode"><xsl:value-of select="$zipcode" /></span>
            </span>
            <xsl:if test="$phone1 != ''">
              <br/>
              <span class="location-phone" itemprop="telephone"><xsl:value-of select="$phone1" /></span>
            </xsl:if>
            <xsl:if test="$phone2 != ''">
              <br/>
              <span class="location-fax" itemprop="faxNumber"><xsl:value-of select="$phone2" /></span>
            </xsl:if>
          </span>
        </p>
        <xsl:if test="$directionLink != '' or $websiteUrl != ''">
          <p>
            <xsl:if test="$directionLink != ''">
              <a class="btn" href="{$directionLink}">Directions</a>
            </xsl:if>
            <xsl:if test="$directionLink != '' and $websiteUrl != ''">
              &#160;
            </xsl:if>
            <xsl:if test="$websiteUrl != ''">
              <a class="btn" href="{$websiteUrl}">Website</a>
            </xsl:if>
          </p>
        </xsl:if>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>

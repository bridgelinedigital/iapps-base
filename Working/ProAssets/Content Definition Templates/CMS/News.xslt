﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="date" select="//contentDefinitionNode[@objectId='primaryDate']/@select" />
  <xsl:variable name="authorName" select="//contentDefinitionNode[@objectId='authorName']/@select" />
  <xsl:variable name="articleTitle" select="//contentDefinitionNode[@objectId='articleTitle']/@select" />
  <xsl:variable name="articleContent" select="//contentDefinitionNode[@objectId='articleContent']/@select" />
  <xsl:variable name="primaryImage" select="//contentDefinitionNode[@objectId='primaryImage']/@Value" />

  <xsl:template match="/">
    <div class="section">
      <div class="contained">
        <ul class="newsDetail-infoList">
          <li>
            <xsl:call-template name="_formatDate">
              <xsl:with-param name="date" select="$date" />
            </xsl:call-template>
          </li>
          <li>
            <xsl:value-of select="$authorName" />
          </li>
        </ul>
        <h1>
          <xsl:value-of select="$articleTitle" />
        </h1>
        <div class="newsDetail">
          <div class="newsDetail-bodyContent">
            <xsl:if test="$primaryImage != ''">
              <figure class="newsDetail-image">
                <img>
                  <xsl:attribute name="src">
                    <xsl:call-template name="Replace">
                      <xsl:with-param name="text" select="$primaryImage" />
                    </xsl:call-template>
                  </xsl:attribute>
                  <xsl:attribute name="alt">
                    <xsl:value-of select="$articleTitle" />
                  </xsl:attribute>
                </img>
              </figure>
            </xsl:if>
            <xsl:value-of select="$articleContent" />
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="_formatDate">
    <!-- new date format to set just the month : current format: 2010-08-25T10:15:00+05:30 -->
    <xsl:param name="date" />
    <!-- Month -->
    <xsl:variable name="month" select="substring($date, 6, 2)"/>
    <xsl:choose>
      <xsl:when test="$month='01'">Jan</xsl:when>
      <xsl:when test="$month='02'">Feb</xsl:when>
      <xsl:when test="$month='03'">Mar</xsl:when>
      <xsl:when test="$month='04'">Apr</xsl:when>
      <xsl:when test="$month='05'">May</xsl:when>
      <xsl:when test="$month='06'">June</xsl:when>
      <xsl:when test="$month='07'">July</xsl:when>
      <xsl:when test="$month='08'">Aug</xsl:when>
      <xsl:when test="$month='09'">Sept</xsl:when>
      <xsl:when test="$month='10'">Oct</xsl:when>
      <xsl:when test="$month='11'">Nov</xsl:when>
      <xsl:when test="$month='12'">Dec</xsl:when>
      <xsl:otherwise> </xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <!-- new date format to set just the day : current format: 2010-08-25T10:15:00+05:30 -->
    <!-- Day -->
    <xsl:value-of select="substring(normalize-space($date), 9, 2)" />
    <xsl:text>, </xsl:text>
    <!--year-->
    <xsl:value-of select="substring(normalize-space($date), 1, 4)" />
  </xsl:template>
</xsl:stylesheet>
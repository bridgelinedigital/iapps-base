﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <div class="section">
      <div class="contained">
        <ul class="tabs tabs--accordion">
          <li>
            <a href="javascript:void(0)">
              <xsl:value-of select="//contentDefinitionNode[@objectId='tabName']/@select"/>
            </a>
            <div>
              <xsl:value-of select="//contentDefinitionNode[@objectId='content']/@select"/>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
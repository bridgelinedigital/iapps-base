﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <div class="section">
      <div class="contained">
        <p class="alert alert--warning icon-attention">The main slider panel must be used in a list.</p>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>










﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <div>
      <xsl:attribute name="class">
        <xsl:text>section h-hard socialShare</xsl:text>
        <xsl:if test="//contentDefinitionNode[@objectId='backgroundContrast']/@select != ''">
          <xsl:value-of select="concat(' section--contrast', //contentDefinitionNode[@objectId='backgroundContrast']/@select)"/>
        </xsl:if>
      </xsl:attribute>
      <div class="socialShare-inner">
        <h4 class="socialShare-heading">
          <xsl:value-of select="//contentDefinitionNode[@objectId='title']/@select" />
        </h4>
        <div class="socialShare-icons">
			<xsl:variable name="twitter">
				<xsl:value-of select="//contentDefinitionNode[@objectId='twitter']/@select" />
			</xsl:variable>
          <xsl:if test="$twitter = 'true' or $twitter = 'True'  ">
            <span class="st-custom-button st_twitter_large" data-network="twitter">
              <span class="stButton">
                <span class="stLarge">
                  <xsl:text> </xsl:text>
                </span>
              </span>
            </span>
          </xsl:if>
		  <xsl:variable name="facebook">
				<xsl:value-of select="//contentDefinitionNode[@objectId='facebook']/@select" />
			</xsl:variable>
           <xsl:if test="$facebook = 'true' or $facebook = 'True'  ">
            <span class="st-custom-button st_facebook_large" data-network="facebook">
              <span class="stButton">
                <span class="stLarge">
                  <xsl:text> </xsl:text>
                </span>
              </span>
            </span>
          </xsl:if>
		  <xsl:variable name="email">
				<xsl:value-of select="//contentDefinitionNode[@objectId='email']/@select" />
			</xsl:variable>
          <xsl:if test="$email = 'true' or $email = 'True'  ">
            <span class="st-custom-button st_email_large" data-network="email">
              <span class="stButton">
                <span class="stLarge">
                  <xsl:text> </xsl:text>
                </span>
              </span>
            </span>
          </xsl:if>
		  <xsl:variable name="print">
				<xsl:value-of select="//contentDefinitionNode[@objectId='print']/@select" />
			</xsl:variable>
        <xsl:if test="$print = 'true' or $print = 'True'  ">
            <span class="st-custom-button st_print_large" data-network="print">
              <span class="stButton">
                <span class="stLarge">
                  <xsl:text> </xsl:text>
                </span>
              </span>
            </span>
          </xsl:if>
		  <xsl:variable name="shareThis">
				<xsl:value-of select="//contentDefinitionNode[@objectId='shareThis']/@select" />
			</xsl:variable>
           <xsl:if test="$shareThis = 'true' or $shareThis = 'True'  ">
            <span class="st-custom-button st_sharethis_large" data-network="sharethis">
              <span class="stButton">
                <span class="stLarge">
                  <xsl:text> </xsl:text>
                </span>
              </span>
            </span>
          </xsl:if>
        </div>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
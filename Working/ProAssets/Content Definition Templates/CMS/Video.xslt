﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="targetUrl">
    <xsl:choose>
      <xsl:when test="//contentDefinitionNode[@objectId='targetPage']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetPage']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='targetFile']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetFile']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='targetExternal']/@select != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetExternal']/@select"/>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="calloutTargetUrl">
    <xsl:choose>
      <xsl:when test="//contentDefinitionNode[@objectId='calloutTargetPage']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='calloutTargetPage']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='calloutTargetFile']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='calloutTargetFile']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='calloutTargetExternal']/@select != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='calloutTargetExternal']/@select"/>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:template match="/">
    <div>
      <xsl:attribute name="class">
        <xsl:text>section video h-hard</xsl:text>
        <xsl:if test="//contentDefinitionNode[@objectId='backgroundContrast']/@select != ''">
          <xsl:value-of select="concat(' section--contrast', //contentDefinitionNode[@objectId='backgroundContrast']/@select)"/>
        </xsl:if>
        <xsl:if test="//contentDefinitionNode[@objectId='videoPosition']/@select = 'right'">
          <xsl:text> video--imgRight</xsl:text>
        </xsl:if>
      </xsl:attribute>
      <div class="video-embed">
        <xsl:value-of select="//contentDefinitionNode[@objectId='videoEmbedCode']/@select" />
      </div>
      <div class="video-content">
        <xsl:if test="//contentDefinitionNode[@objectId='title']/@select != ''">
          <h3 class="video-heading">
            <xsl:value-of select="//contentDefinitionNode[@objectId='title']/@select" />
          </h3>
        </xsl:if>
        <xsl:if test="//contentDefinitionNode[@objectId='description']/@select != '' or $targetUrl != ''">
          <p>
            <xsl:if test="//contentDefinitionNode[@objectId='description']/@select != ''">
              <xsl:value-of select="//contentDefinitionNode[@objectId='description']/@select" />&#160;
            </xsl:if>
            <xsl:if test="$targetUrl != ''">
              <a class="trailingLink">
                <xsl:attribute name="href">
                  <xsl:value-of select="$targetUrl"/>
                </xsl:attribute>
                Learn More
              </a>
            </xsl:if>
          </p>
        </xsl:if>
        <xsl:if test="$calloutTargetUrl != ''">
          <p>
            <a class="btn">
              <xsl:attribute name="href">
                <xsl:value-of select="$calloutTargetUrl"/>
              </xsl:attribute>
              <xsl:value-of select="//contentDefinitionNode[@objectId='calloutButtonText']/@select" />
            </a>
          </p>
        </xsl:if>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
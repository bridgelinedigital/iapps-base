﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="iconClass">
    <xsl:if test="//contentDefinitionNode[@objectId='icon']/@select != ''">
      <xsl:value-of select="//contentDefinitionNode[@objectId='icon']/@select"/>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="Description" select="//contentDefinitionNode[@objectId='description']/@select" />

  <xsl:template match="/">
    <div class="section">
      <div class="contained">
        <ul class="accordion">
          <li>
            <span class="{$iconClass}"><xsl:value-of select="//contentDefinitionNode[@objectId='title']/@select" /></span>
            <div class="accordion-content">
              <xsl:value-of select="//contentDefinitionNode[@objectId='content']/@select"/>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
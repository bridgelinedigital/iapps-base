﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <div>
      <xsl:attribute name="class">
        <xsl:text>section socialFeed</xsl:text>
        <xsl:if test="//contentDefinitionNode[@objectId='socialNetwork']/@select != ''">
          <xsl:value-of select="concat(' socialFeed--', //contentDefinitionNode[@objectId='socialNetwork']/@select)"/>
        </xsl:if>
      </xsl:attribute>
      <div class="socialFeed-inner">
        <div class="socialFeed-item">
          <figure class="socialFeed--figure">
            <a href="">
              <xsl:call-template name="Replace">
                <xsl:with-param name="text" select="//contentDefinitionNode[@objectId='mainImage']/@Value" />
              </xsl:call-template>
            </a>
          </figure>
          <div class="socialFeed-content">
            <h5 class="socialFeed-user"><xsl:value-of select="//contentDefinitionNode[@objectId='username']/@select" /></h5>
            <ul class="infoList socialFeed-infoList">
              <li><xsl:value-of select="//contentDefinitionNode[@objectId='dateOfPost']/@select" /></li>
            </ul>
            <p><xsl:value-of select="//contentDefinitionNode[@objectId='content']/@select"/></p>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
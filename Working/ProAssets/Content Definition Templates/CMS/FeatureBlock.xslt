﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="targetUrl">
    <xsl:choose>
      <xsl:when test="//contentDefinitionNode[@objectId='targetPage']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetPage']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='targetFile']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetFile']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='targetExternal']/@select != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetExternal']/@select"/>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="ctaUrl">
    <xsl:choose>
      <xsl:when test="//contentDefinitionNode[@objectId='targetPage']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetPage']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='targetFile']/@Value != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetFile']/@Value"/>
      </xsl:when>
      <xsl:when test="//contentDefinitionNode[@objectId='targetExternal']/@select != ''">
        <xsl:value-of select="//contentDefinitionNode[@objectId='targetExternal']/@select"/>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="alt">
    <xsl:value-of select="//contentDefinitionNode[@objectId='alt']/@select"/>
  </xsl:variable>

  <xsl:variable name="arialabel">
    <xsl:value-of select="//contentDefinitionNode[@objectId='arialabel']/@select"/>
  </xsl:variable>

  <xsl:template match="/">

    <div class="section featureBlock">
      <div class="contained">
        <div class="row">
          <div class="column">
            <figure class="featureBlock-figure">
                <img>
                  <xsl:attribute name="alt">
                    <xsl:value-of select="//contentDefinitionNode[@objectId='title']/@select"/>
                  </xsl:attribute>
                  <xsl:attribute name="alt">
                    <xsl:choose>
                      <xsl:when test="$alt != ''">
                        <xsl:value-of select="$alt" />
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="//contentDefinitionNode[@objectId='title']/@select" />
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                  <xsl:attribute name="src">
                    <xsl:value-of select="//contentDefinitionNode[@objectId='imageSmall']/@Value"/>
                  </xsl:attribute>
                </img>
              <figcaption class="featureBlock-figcaption">
                <h3 class="featureBlock-subHeading"><xsl:value-of select="//contentDefinitionNode[@objectId='title']/@select"/></h3>
                <p>
                  <xsl:value-of select="//contentDefinitionNode[@objectId='description']/@select"/>
                  <xsl:if test="$targetUrl != ''">
                    <a href="{$targetUrl}" class="trailingLink" aria-label="{$arialabel}">Learn more</a>
                  </xsl:if>
                </p>
                <xsl:if test="//contentDefinitionNode[@objectId='calloutButtonText']/@select != '' and $ctaUrl != ''">
                  <p>
                    <a href="{$ctaUrl}">
                      <xsl:attribute name="class">
                        <xsl:text>btn</xsl:text>
                        <xsl:if test="//contentDefinitionNode[@objectId='calloutButtonCss']/@select != ''">
                          <xsl:value-of select="concat(' ', //contentDefinitionNode[@objectId='calloutButtonCss']/@select)" />
                        </xsl:if>
                      </xsl:attribute>
                      <xsl:value-of select="//contentDefinitionNode[@objectId='calloutButtonText']/@select"/>
                    </a>
                  </p>
                </xsl:if>
              </figcaption>
            </figure>
          </div>
          <!--/.column-->
        </div>
        <!--/.row-->
      </div>
      <!--/.contained-->
    </div>

  </xsl:template>
  
</xsl:stylesheet>

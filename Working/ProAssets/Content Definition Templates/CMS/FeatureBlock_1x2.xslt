﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <div>
      <xsl:attribute name="class">
        <xsl:text>section featureBlock featureBlock--two</xsl:text>
        <xsl:if test="//contentDefinitionNode[@objectId='backgroundContrast']/@select != ''">
          <xsl:value-of select="concat(' section--contrast', //contentDefinitionNode[@objectId='backgroundContrast']/@select)"/>
        </xsl:if>
      </xsl:attribute>
      <div class="contained">
        <xsl:if test="//contentDefinitionNode[@objectId='optionalHeading']/@select != ''">
          <h2 class="featureBlock-heading">
            <xsl:value-of select="//contentDefinitionNode[@objectId='optionalHeading']/@select" />
          </h2>
        </xsl:if>
        <div class="row">
          <div class="column med-12">
            <xsl:call-template name="FeatureBlock">
              <xsl:with-param name="title" select="//contentDefinitionNode[@objectId='title1']/@select" />
              <xsl:with-param name="description" select="//contentDefinitionNode[@objectId='description1']/@select" />
              <xsl:with-param name="image" select="//contentDefinitionNode[@objectId='image1']/@Value" />
              <xsl:with-param name="alt" select="//contentDefinitionNode[@objectId='alt1']/@select" />
              <xsl:with-param name="arialabel" select="//contentDefinitionNode[@objectId='arialabel1']/@select" />
              <xsl:with-param name="targetPage" select="//contentDefinitionNode[@objectId='targetPage1']/@Value" />
              <xsl:with-param name="targetFile" select="//contentDefinitionNode[@objectId='targetFile1']/@Value" />
              <xsl:with-param name="targetExternal" select="//contentDefinitionNode[@objectId='targetExternal1']/@select" />
              <xsl:with-param name="calloutTargetPage" select="//contentDefinitionNode[@objectId='calloutTargetPage1']/@Value" />
              <xsl:with-param name="calloutTargetFile" select="//contentDefinitionNode[@objectId='calloutTargetFile1']/@Value" />
              <xsl:with-param name="calloutTargetExternal" select="//contentDefinitionNode[@objectId='calloutTargetExternal1']/@select" />
              <xsl:with-param name="calloutButtonText" select="//contentDefinitionNode[@objectId='calloutButtonText1']/@select" />
              <xsl:with-param name="calloutButtonCss" select="//contentDefinitionNode[@objectId='calloutButtonCss1']/@select" />
            </xsl:call-template>
          </div>
          
          <div class="column med-12">
            <xsl:call-template name="FeatureBlock">
              <xsl:with-param name="title" select="//contentDefinitionNode[@objectId='title2']/@select" />
              <xsl:with-param name="description" select="//contentDefinitionNode[@objectId='description2']/@select" />
              <xsl:with-param name="image" select="//contentDefinitionNode[@objectId='image2']/@Value" />
              <xsl:with-param name="alt" select="//contentDefinitionNode[@objectId='alt2']/@select" />
              <xsl:with-param name="arialabel" select="//contentDefinitionNode[@objectId='arialabel2']/@select" />
              <xsl:with-param name="targetPage" select="//contentDefinitionNode[@objectId='targetPage2']/@Value" />
              <xsl:with-param name="targetFile" select="//contentDefinitionNode[@objectId='targetFile2']/@Value" />
              <xsl:with-param name="targetExternal" select="//contentDefinitionNode[@objectId='targetExternal2']/@select" />
              <xsl:with-param name="calloutTargetPage" select="//contentDefinitionNode[@objectId='calloutTargetPage2']/@Value" />
              <xsl:with-param name="calloutTargetFile" select="//contentDefinitionNode[@objectId='calloutTargetFile2']/@Value" />
              <xsl:with-param name="calloutTargetExternal" select="//contentDefinitionNode[@objectId='calloutTargetExternal2']/@select" />
              <xsl:with-param name="calloutButtonText" select="//contentDefinitionNode[@objectId='calloutButtonText2']/@select" />
              <xsl:with-param name="calloutButtonCss" select="//contentDefinitionNode[@objectId='calloutButtonCss2']/@select" />
            </xsl:call-template>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="FeatureBlock">
    <xsl:param name="title" />
    <xsl:param name="description" />
    <xsl:param name="image" />
    <xsl:param name="alt" />
    <xsl:param name="arialabel" />
    <xsl:param name="targetPage" />
    <xsl:param name="targetFile" />
    <xsl:param name="targetExternal" />
    <xsl:param name="calloutTargetPage" />
    <xsl:param name="calloutTargetFile" />
    <xsl:param name="calloutTargetExternal" />
    <xsl:param name="calloutButtonText" />
    <xsl:param name="calloutButtonCss" />

    <xsl:variable name="targetUrl">
      <xsl:choose>
        <xsl:when test="$targetPage != ''">
          <xsl:value-of select="$targetPage"/>
        </xsl:when>
        <xsl:when test="$targetFile != ''">
          <xsl:value-of select="$targetFile"/>
        </xsl:when>
        <xsl:when test="$targetExternal != ''">
          <xsl:value-of select="$targetExternal"/>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="calloutTargetUrl">
      <xsl:choose>
        <xsl:when test="$calloutTargetPage != ''">
          <xsl:value-of select="$calloutTargetPage"/>
        </xsl:when>
        <xsl:when test="$calloutTargetFile != ''">
          <xsl:value-of select="$calloutTargetFile"/>
        </xsl:when>
        <xsl:when test="$calloutTargetExternal != ''">
          <xsl:value-of select="$calloutTargetExternal"/>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <figure class="featureBlock-figure">
        <img>
          <xsl:attribute name="src">
            <xsl:call-template name="Replace">
              <xsl:with-param name="text" select="$image" />
            </xsl:call-template>
          </xsl:attribute>
          <xsl:attribute name="alt">
            <xsl:choose>
              <xsl:when test="$alt != ''">
                <xsl:value-of select="$alt" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$title" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
        </img>
      <figcaption class="featureBlock-figcaption">
        <xsl:if test="$title != ''">
          <h3 class="featureBlock-subHeading">
            <xsl:value-of select="$title" />
          </h3>
        </xsl:if>
        <xsl:if test="$description != '' or $targetUrl != ''">
          <p>
            <xsl:if test="$description != ''">
              <xsl:value-of select="$description" />&#160;
            </xsl:if>
            <xsl:if test="$targetUrl != ''">
              <a class="trailingLink">
                <xsl:attribute name="href">
                  <xsl:value-of select="$targetUrl"/>
                </xsl:attribute>
                <xsl:attribute name="aria-label">
                  <xsl:value-of select="$arialabel" />
                </xsl:attribute>
                Learn more
              </a>
            </xsl:if>
          </p>
        </xsl:if>
        <xsl:if test="$calloutTargetUrl != ''">
          <p>
            <a>
              <xsl:attribute name="class">
                <xsl:text>btn</xsl:text>
                <xsl:if test="$calloutButtonCss != ''">
                  <xsl:value-of select="concat(' ', $calloutButtonCss)" />
                </xsl:if>
              </xsl:attribute>
              <xsl:attribute name="href">
                <xsl:value-of select="$calloutTargetUrl"/>
              </xsl:attribute>
              <xsl:value-of select="$calloutButtonText" />
            </a>
          </p>
        </xsl:if>
      </figcaption>
    </figure>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="title">
    <xsl:value-of select="//contentDefinitionNode[@objectId='title']/@select"/>
  </xsl:variable>
  <xsl:variable name="content">
    <xsl:value-of select="//contentDefinitionNode[@objectId='content']/@select"/>
  </xsl:variable>

  <xsl:variable name="imageUrl">
    <xsl:call-template name="Replace">
      <xsl:with-param name="text" select="//contentDefinitionNode[@objectId='imageUrl']/@Value" />
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="altText">
    <xsl:value-of select="//contentDefinitionNode[@objectId='altText']/@select"/>
  </xsl:variable>
  <xsl:variable name="imageAlignmentDir">
    <xsl:choose>
      <xsl:when test="//contentDefinitionNode[@objectId='imageAlignment']/@select = 'Left'">ltr</xsl:when>
      <xsl:otherwise>rtl</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="imageAlignmentClassPrefix">
    <xsl:choose>
      <xsl:when test="//contentDefinitionNode[@objectId='imageAlignment']/@select = 'Left'">imgLeft</xsl:when>
      <xsl:otherwise>imgRight</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="buttonText">
    <xsl:value-of select="//contentDefinitionNode[@objectId='buttonText']/@select"/>
  </xsl:variable>
  <xsl:variable name="buttonTargetPage">
    <xsl:value-of select="//contentDefinitionNode[@objectId='buttonTargetPage']/@Value"/>
  </xsl:variable>
  <xsl:variable name="buttonTargetFile">
    <xsl:value-of select="//contentDefinitionNode[@objectId='buttonTargetFile']/@Value"/>
  </xsl:variable>
  <xsl:variable name="buttonTargetExternal">
    <xsl:value-of select="//contentDefinitionNode[@objectId='buttonTargetExternal']/@Value"/>
  </xsl:variable>

  <xsl:variable name="buttonTargetUrl">
    <xsl:choose>
      <xsl:when test="$buttonTargetPage != ''">
        <xsl:value-of select="$buttonTargetPage"/>
      </xsl:when>
      <xsl:when test="$buttonTargetFile != ''">
        <xsl:value-of select="$buttonTargetFile"/>
      </xsl:when>
      <xsl:when test="$buttonTargetExternal != ''">
        <xsl:value-of select="$buttonTargetExternal"/>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>


  <xsl:template match="/">
    <table cellspacing="0" cellpadding="0" border="0" width="100%" class="imgLeft">
      <tr>
        <!-- dir=ltr is where the magic happens. This can be changed to dir=rtl to swap the alignment on wide while maintaining stack order on narrow. -->
        <td dir="{$imageAlignmentDir}" bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" class="{$imageAlignmentClassPrefix}-inner">
          <xsl:comment>
            <![CDATA[[if mso]>
              <table border="0" cellspacing="0" cellpadding="0" align="center" width="660">
                <tr>
                  <td align="center" valign="top" width="660">
            <![endif]]]>
          </xsl:comment>
          <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" class="{$imageAlignmentClassPrefix}-body">
            <tr>
              <td align="center" valign="top" class="{$imageAlignmentClassPrefix}-bodyInner">
                <xsl:comment>
                  <![CDATA[[if mso]>
                    <table border="0" cellspacing="0" cellpadding="0" align="center" width="660">
                      <tr>
                        <td align="left" valign="top" width="220">
                  <![endif]]]>
                </xsl:comment>
                <div class="{$imageAlignmentClassPrefix}-imgDiv stack-column">
                  <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                      <td dir="{$imageAlignmentDir}" class="{$imageAlignmentClassPrefix}-imgCell">
                        <img src="{$imageUrl}" width="200" alt="" class="{$imageAlignmentClassPrefix}-img center-on-narrow" />
                      </td>
                    </tr>
                  </table>
                </div>
                <xsl:comment>
                  <![CDATA[[if mso]>
                      </td>
                      <td align="left" valign="top" width="440">
                    <![endif]]]>
                </xsl:comment>
                <div class="{$imageAlignmentClassPrefix}-textDiv stack-column">
                  <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                      <td dir="{$imageAlignmentDir}" class="{$imageAlignmentClassPrefix}-textCell center-on-narrow">
                        <strong class="{$imageAlignmentClassPrefix}-heading">
                          <xsl:value-of select="$title"/>
                        </strong>
                        <br />
                        <br />
                        <xsl:value-of select="$content" />
                        <xsl:if test="$buttonText != ''">
                          <br />
                          <br />
                          <!-- Button : Begin -->
                          <table cellspacing="0" cellpadding="0" border="0" class="buttonTable center-on-narrow">
                            <tr>
                              <td class="buttonTable-td">
                                <a href="{$buttonTargetUrl}" class="buttonTable-link">
                                  &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;<span class="buttonTable-linkInner">
                                    <xsl:value-of select="$buttonText"/>
                                  </span>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                                </a>
                              </td>
                            </tr>
                          </table>
                          <!-- Button : END -->
                        </xsl:if>
                      </td>
                    </tr>
                  </table>
                </div>
                <xsl:comment>
                  <![CDATA[[if mso]>
                      </td>
                    </tr>
                  </table>
                <![endif]]]>
                </xsl:comment>
              </td>
            </tr>
          </table>
          <xsl:comment>
            <![CDATA[[if mso]>
                  </td>
                </tr>
              </table>
            <![endif]]]>
          </xsl:comment>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
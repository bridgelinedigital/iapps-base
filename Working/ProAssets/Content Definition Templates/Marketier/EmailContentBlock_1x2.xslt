﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="content1">
    <xsl:value-of select="//contentDefinitionNode[@objectId='content1']/@select"/>
  </xsl:variable>
  <xsl:variable name="imageUrl1">
    <xsl:call-template name="Replace">
      <xsl:with-param name="text" select="//contentDefinitionNode[@objectId='imageUrl1']/@Value" />
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="altText1">
    <xsl:value-of select="//contentDefinitionNode[@objectId='altText1']/@select"/>
  </xsl:variable>

  <xsl:variable name="content2">
    <xsl:value-of select="//contentDefinitionNode[@objectId='content2']/@select"/>
  </xsl:variable>
  <xsl:variable name="imageUrl2">
    <xsl:call-template name="Replace">
      <xsl:with-param name="text" select="//contentDefinitionNode[@objectId='imageUrl2']/@Value" />
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="altText2">
    <xsl:value-of select="//contentDefinitionNode[@objectId='altText2']/@select"/>
  </xsl:variable>

  <xsl:template match="/">
    <table cellspacing="0" cellpadding="0" border="0" width="100%" class="twoCol">
      <tr>
        <td align="center" height="100%" valign="top" width="100%">
          <xsl:comment><![CDATA[[if mso]>
              <table border="0" cellspacing="0" cellpadding="0" align="center" width="660">
                <tr>
                  <td align="center" valign="top" width="660">
            <![endif]]]></xsl:comment>
          <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" class="twoCol-wrapper01">
            <tr>
              <td align="center" valign="top" class="twoCol-wrapper02">
                <xsl:comment><![CDATA[[if mso]>
                    <table border="0" cellspacing="0" cellpadding="0" align="center" width="660">
                      <tr>
                        <td align="left" valign="top" width="330">
                  <![endif]]]></xsl:comment>
                <div class="twoCol-wrapper03 stack-column">
                  <xsl:call-template name="BuildContentBlock">
                    <xsl:with-param name="content" select="//contentDefinitionNode[@objectId='content1']/@select" />
                    <xsl:with-param name="imageUrl" select="//contentDefinitionNode[@objectId='imageUrl1']/@Value" />
                    <xsl:with-param name="altText" select="//contentDefinitionNode[@objectId='altText1']/@select" />
                  </xsl:call-template>
                </div>
                <xsl:comment><![CDATA[[if mso]>
                    </td>
                    <td align="left" valign="top" width="330">
                  <![endif]]]></xsl:comment>
                <div class="twoCol-wrapper03 stack-column">
                  <xsl:call-template name="BuildContentBlock">
                    <xsl:with-param name="content" select="//contentDefinitionNode[@objectId='content2']/@select" />
                    <xsl:with-param name="imageUrl" select="//contentDefinitionNode[@objectId='imageUrl2']/@Value" />
                    <xsl:with-param name="altText" select="//contentDefinitionNode[@objectId='altText2']/@select" />
                  </xsl:call-template>
                </div>
                <xsl:comment><![CDATA[[if mso]>
                        </td>
                      </tr>
                    </table>
                  <![endif]]]></xsl:comment>
              </td>
            </tr>
          </table>
          <xsl:comment><![CDATA[[if mso]>
                  </td>
                </tr>
              </table>
            <![endif]]]></xsl:comment>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="BuildContentBlock">
    <xsl:param name="content" />
    <xsl:param name="imageUrl" />
    <xsl:param name="altText" />

    <xsl:variable name="imageUrlReplaced">
      <xsl:call-template name="Replace">
        <xsl:with-param name="text" select="$imageUrl" />
      </xsl:call-template>
    </xsl:variable>

    <div class="twoCol-wrapper03 stack-column">
      <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
          <td class="twoCol-wrapper04">
            <table class="twoCol-column" cellspacing="0" cellpadding="0" border="0" width="100%">
              <tr>
                <td>
                  <img src="{$imageUrlReplaced}" width="310" alt="{$altText}" class="twoCol-img center-on-narrow" />
                </td>
              </tr>
              <tr>
                <td class="twoCol-text stack-column-center">
                  <xsl:value-of select="$content"/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="content">
    <xsl:value-of select="//contentDefinitionNode[@objectId='content']/@select"/>
  </xsl:variable>

  <xsl:variable name="buttonText">
    <xsl:value-of select="//contentDefinitionNode[@objectId='buttonText']/@select"/>
  </xsl:variable>
  <xsl:variable name="buttonTargetPage">
    <xsl:value-of select="//contentDefinitionNode[@objectId='buttonTargetPage']/@Value"/>
  </xsl:variable>
  <xsl:variable name="buttonTargetFile">
    <xsl:value-of select="//contentDefinitionNode[@objectId='buttonTargetFile']/@Value"/>
  </xsl:variable>
  <xsl:variable name="buttonTargetExternal">
    <xsl:value-of select="//contentDefinitionNode[@objectId='buttonTargetExternal']/@Value"/>
  </xsl:variable>

  <xsl:variable name="buttonTargetUrl">
    <xsl:choose>
      <xsl:when test="$buttonTargetPage != ''">
        <xsl:value-of select="$buttonTargetPage"/>
      </xsl:when>
      <xsl:when test="$buttonTargetFile != ''">
        <xsl:value-of select="$buttonTargetFile"/>
      </xsl:when>
      <xsl:when test="$buttonTargetExternal != ''">
        <xsl:value-of select="$buttonTargetExternal"/>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>


  <xsl:template match="/">
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
      <tr>
        <td>
          <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
              <td class="oneColText">
                <xsl:value-of select="$content"/>
                <xsl:if test="$buttonText != ''">
                  <br />
                  <br />
                  <!-- Button : Begin -->
                  <table cellspacing="0" cellpadding="0" border="0" class="buttonTable center-on-narrow">
                    <tr>
                      <td class="buttonTable-td">
                        <a href="{$buttonTargetUrl}" class="buttonTable-link">
                          &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;<span class="buttonTable-linkInner">
                            <xsl:value-of select="$buttonText"/>
                          </span>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
                        </a>
                      </td>
                    </tr>
                  </table>
                  <!-- Button : END -->
                </xsl:if>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
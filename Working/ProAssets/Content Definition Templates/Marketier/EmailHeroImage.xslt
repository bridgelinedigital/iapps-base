﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:variable name="imageUrl">
    <xsl:call-template name="Replace">
      <xsl:with-param name="text" select="//contentDefinitionNode[@objectId='imageUrl']/@Value" />
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="title">
    <xsl:value-of select="//contentDefinitionNode[@objectId='title']/@select"/>
  </xsl:variable>
  <xsl:variable name="altText">
    <xsl:value-of select="//contentDefinitionNode[@objectId='altText']/@select"/>
  </xsl:variable>

  <xsl:template match="/">
    <table cellspacing="0" cellpadding="0" border="0" width="100%" class="hero">
      <tr>
        <td>
          <img class="hero-img" src="{$imageUrl}" width="680" height="300" title="{$title}" alt="{$altText}" border="0" align="center" />
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
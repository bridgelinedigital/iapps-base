@ECHO OFF

SET @INSTUTIL="C:\Windows\Microsoft.NET\Framework\v4.0.30319\installutil.exe"
SET @ROOT=%~dp0

ECHO Stopping Service
net stop Bridgeline.iAPPSService

ECHO Uninstalling Service
%@INSTUTIL% %@ROOT%\Bridgeline.iAPPS.WindowsService.exe /u

PAUSE
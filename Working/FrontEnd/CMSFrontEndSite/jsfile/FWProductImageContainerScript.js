﻿var Data;
function ProductImageContainerContextMenuClickHandler(menuItem)
{
        Data = menuItem.ParentMenu.ContextData;
        if(menuItem.ID == "ManageProductImages")
        {
            var adminpath = jCommerceAdminSiteUrl + '/StoreManager/Products/ProductImages.aspx?SelectedProductSkuIds=00000000-0000-0000-0000-000000000000&SelectedProductId='+
                                                                        Data.HiddenFwObjectId.value;
                                 
            jProductId= jCommerceProductId;               
            functionName = "RedirectToCommerceAdmin();";
            // the following function is defined in the js file rendered by the BaseContainer
            // Once the callback function comes back with the token for redirection
            // it constructs the querystring for token and then sets the destinationUrl = adminpath?querystring with token
            // and calls the function specified above.
            SendTokenRequestByURL(adminpath);
            Data.ContextMenu.hide();
        }
}

function RedirectToCommerceAdmin()
{
    if (destinationUrl!="")
    {
        var currentUrl = document.URL;
        if (currentUrl==null || currentUrl=='')
        {
            currentUrl = document.referrer;
        }
        if (currentUrl.indexOf('?') > 0)
        {
            currentUrl = currentUrl.substring(0,currentUrl.indexOf('?'));
        }      
        currentUrl = currentUrl.toLowerCase().replace(jPublicSiteUrl.toLowerCase(),"");
        destinationUrl = destinationUrl + "&BackToAdmin=1&LastFrontEndPage="+currentUrl;                                 
        window.location.href = destinationUrl;    
    }
}
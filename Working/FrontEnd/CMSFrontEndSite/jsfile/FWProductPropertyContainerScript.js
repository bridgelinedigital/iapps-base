﻿var NewContent = false;
var Data;

function ProductPropertyContainerContextMenuClickHandler(menuItem)
{
        Data = menuItem.ParentMenu.ContextData;
        var adminpath ="";
        var redirect = false;
        if( menuItem.ID == "EditText")
        {
             NewContent = false;
             ShowEditor(Data.ControlID);
             Data.ContextMenu.hide();
             setFwObjectId(Data, Data.HiddenFwObjectId.value);
        }
        else if(menuItem.ID == "ManageGeneralDetails")
        {
              adminpath = jCommerceAdminSiteUrl + '/StoreManager/Products/GeneralDetails.aspx?SelectedProductSkuIds=00000000-0000-0000-0000-000000000000&SelectedProductId='+
                                                                        Data.HiddenFwObjectId.value;  
               redirect = true;
        }
        else if(menuItem.ID == "ManagePriceSets")
        {
           adminpath = jCommerceAdminSiteUrl + '/StoreManager/Products/ProductPriceSets.aspx?SelectedProductSkuIds=00000000-0000-0000-0000-000000000000&SelectedProductId='+
                                                                        Data.HiddenFwObjectId.value;  
            redirect = true;
        }
        else if(menuItem.ID == "ManageInventory")
        {
            adminpath = jCommerceAdminSiteUrl + '/StoreManager/Products/InventoryAndRestocking.aspx?SelectedProductSkuIds=00000000-0000-0000-0000-000000000000&SelectedProductId=' +
                                                                        Data.HiddenFwObjectId.value;  
              redirect = true;
        }
        else if(menuItem.ID == "ManageProductImages")
        {
            adminpath = jCommerceAdminSiteUrl + '/StoreManager/Products/ProductImages.aspx?SelectedProductSkuIds=00000000-0000-0000-0000-000000000000&SelectedProductId='+
                                                                        Data.HiddenFwObjectId.value;
             redirect = true;
        }
        else if(menuItem.ID == "ManageNavigation")
        {
            adminpath = jCommerceAdminSiteUrl + '/StoreManager/Products/NavigationBuilder.aspx?SelectedProductId='+
                                                                        Data.HiddenFwObjectId.value;
             redirect = true;
        }
        else if(menuItem.ID == "ManageSEOTerms")
        {
            adminpath = jCommerceAdminSiteUrl + '/StoreManager/Products/SEOTerms.aspx?SelectedProductId='+
                                                                        Data.HiddenFwObjectId.value;
             redirect = true;
        }
        
        if(redirect)
        {                      
            jProductId= jCommerceProductId;               
            functionName = "RedirectToCommerceAdmin();";
            // the following function is defined in the js file rendered by the BaseContainer
            // Once the callback function comes back with the token for redirection
            // it constructs the querystring for token and then sets the destinationUrl = adminpath?querystring with token
            // and calls the function specified above.
            SendTokenRequestByURL(adminpath);
            Data.ContextMenu.hide();
        }
}

function RedirectToCommerceAdmin()
{
    if (destinationUrl!="")
    {
        var currentUrl = document.URL;
        if (currentUrl==null || currentUrl=='')
        {
            currentUrl = document.referrer;
        }
        if (currentUrl.indexOf('?') > 0)
        {
            currentUrl = currentUrl.substring(0,currentUrl.indexOf('?'));
        }      
        currentUrl = currentUrl.toLowerCase().replace(jPublicSiteUrl.toLowerCase(),"");
        destinationUrl = destinationUrl + "&BackToAdmin=1&LastFrontEndPage="+currentUrl;                                 
        window.location.href = destinationUrl;    
    }
}
/* Copyright Bridgeline Digital, Inc. An unpublished work created in 2009. All rights reserved. This software contains the confidential and trade secret information of Bridgeline Digital, Inc. ("Bridgeline").  Copying, distribution or disclosure without Bridgeline's express written permission is prohibited */
// Tree and Common -->

var overlayDisplay = null;
var prevElement = null;
var prevOverlay = null;
var treeClientId = null;
var menuClientId = null;
function onCollapse(sender, eventArgs) {
    eval(treeClientId).expandAll();
}
function setOverlay(destObj, srcObj) {
    var objLeft = srcObj.offsetLeft;
    var objTop = srcObj.offsetTop;
    var objParent = srcObj.parentNode;
    while (objParent != null && objParent.tagName.toUpperCase() != "BODY") {
        objLeft += objParent.offsetLeft;
        objTop += objParent.offsetTop;
        objParent = objParent.parentNode;
    }
    if (prevElement != null && prevElement.parentNode != null) {
        prevElement.parentNode.className = "percent";
        prevElement.className = "overlayArrow";
    }
    if (destObj.innerHTML != '') {
        destObj.style.left = (objLeft - 3) + "px";
        destObj.style.top = (objTop + 16) + "px";
        destObj.className = "overlayInfo";
        srcObj.parentNode.className = "percentHover";
        srcObj.className = "overlayArrowHover";
    }
}
function setRootPos(destObj, srcObj) {
    var objLeft = srcObj.offsetLeft;
    var objTop = srcObj.offsetTop;
    var objParent = srcObj.offsetParent;
    while (objParent != null && objParent.tagName.toUpperCase() != "BODY") {
        objLeft += objParent.offsetLeft;
        objTop += objParent.offsetTop;
        objParent = objParent.offsetParent;
    }
    destObj.style.left = objLeft + "px";
    destObj.style.top = objTop + "px";
}
function setTreePos(destObj, nodeY, nodeX) {
    destObj.style.top = nodeY + "px";
    destObj.style.left = (nodeX + 180) + "px";
    destObj.className = "percent";
}
function TreeOverlay() {
    //code library renders tree id as "leftNavTree"
    eval(treeclientid).expandAll();
    var rootNode = eval(treeclientid).get_nodes();
    for (var i = 0; i < rootNode.get_length(); i++) {
        GetNodes(rootNode.getNode(i));
    }
}
function GetNodes(currentNode) {
    var isNodeFound = false;
    var nodeUrl = currentNode.get_navigateUrl();
    var arrUrl = "";
    var parentElement = document.getElementById("treeOverlay");
    for (var count = 0; count < arrOverlay.length; count++) {
        //adding iAppsCM for localhost only
        var menuUrl = "/" + arrOverlay[count][1].substring(0, arrOverlay[count][1].lastIndexOf("/"));
        if (menuUrl == nodeUrl) {
            isNodeFound = true;
            break;
        }
    }
    if (isNodeFound) {
        var htmlObject = document.createElement("div");
        var insertHTML = "<div class=\"overlayArrow\" onclick=\"OverlayClick('" + arrOverlay[count][0] + "', this);\">";
        insertHTML += arrOverlay[count][3] + "%";
        insertHTML += "</div>";
        htmlObject.innerHTML = insertHTML;
        setTreePos(htmlObject, currentNode.getY(), currentNode.getX());
        parentElement.appendChild(htmlObject);
    }
    else {
        var htmlObject = document.createElement("div");
        var insertHTML = "<div class=\"overlayArrow\">";
        insertHTML += "0%";
        insertHTML += "</div>";
        htmlObject.innerHTML = insertHTML;
        setTreePos(htmlObject, currentNode.getY(), currentNode.getX());
        parentElement.appendChild(htmlObject);
    }
    var childNodes = currentNode.get_nodes();
    if (childNodes.get_length() > 0) {
        for (var j = 0; j < childNodes.get_length(); j++) {
            GetNodes(childNodes.getNode(j));
        }
    }
}
function OverlayClick(objectId, overlayObject) {
    var parentElement = document.getElementById("treeOverlay");
    if (prevElement == null)
        prevElement = overlayObject;
    if (overlayDisplay != null && overlayDisplay.innerHTML != '') {
        if (parentElement.innerHTML.indexOf(overlayDisplay.innerHTML) > 0) {
            parentElement.removeChild(overlayDisplay);
        }
    }
    overlayDisplay = document.createElement("div");
    var overlayInfoText = GetOverlayDetails(objectId);
    if (overlayInfoText != "") {
        if (prevOverlay == overlayInfoText) {
            prevOverlay = null;
            setOverlay(overlayDisplay, overlayObject);
            overlayDisplay = null;
        }
        else {
            prevOverlay = overlayInfoText;
            overlayDisplay.innerHTML = overlayInfoText;
            parentElement.appendChild(overlayDisplay);
            setOverlay(overlayDisplay, overlayObject);
            prevElement = overlayObject;
        }
    }
    else {
        setOverlay(overlayDisplay, overlayObject);
        overlayDisplay = null;
        prevOverlay = null;
    }
}
function GetOverlayDetails(objectId) {
    var htmlItems = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">";
    for (var i = 0; i < arrOverlay.length; i++) {
        if (arrOverlay[i][0] == objectId) {
            htmlItems += "<tr>";
            htmlItems += "<td style=\"padding-right:10px;text-align:right;\" valign=\"top\">" + arrOverlay[i][2] + "</td>";
            htmlItems += "<td style=\"padding-left:5px;\" valign=\"top\">" + __JSMessages["Clicks"] + "</td>";
            htmlItems += "</tr>";
            htmlItems += "<tr>";
            htmlItems += "<td style=\"padding-right:10px;text-align:right;\" valign=\"top\">" + arrOverlay[i][3] + "%</td>";
            htmlItems += "<td style=\"padding-left:5px;\" valign=\"top\">" + __JSMessages["ClickPercent"] + "</td>";
            htmlItems += "</tr>";
            if (arrOverlay[i][4] != "" && arrOverlay[i][5] != "") {
                var arrWatchName = arrOverlay[i][4].split(",");
                var arrWatchPercentage = arrOverlay[i][5].split(",");
                for (var j = 0; j < arrWatchName.length; j++) {
                    htmlItems += "<tr>";
                    htmlItems += "<td style=\"padding-right:10px;text-align:right;\" valign=\"top\">" + arrWatchPercentage[j] + "%</td>";
                    htmlItems += "<td style=\"padding-left:5px;\" valign=\"top\"> " + __JSMessages["Watch"] + arrWatchName[j] + "</td>";
                    htmlItems += "</tr>";
                }
            }
        }
    }
    htmlItems += "</table>";
    return (htmlItems);
}


// Menu  -->
function setMenuPos(destObj, srcObj, prev, width) {
    var objLeft = srcObj.offsetLeft;
    var objTop = srcObj.offsetTop;
    var objParent = srcObj.offsetParent;
    while (objParent != null && objParent.tagName.toUpperCase() != "BODY") {
        objLeft += objParent.offsetLeft;
        objTop += objParent.offsetTop;
        objParent = objParent.offsetParent;
    }
    destObj.style.left = (objLeft + (prev + width) - 10) + "px";
    destObj.style.top = (objTop + 8) + "px";
    destObj.className = "percent";
}
function calculateMenuCount(position) {
    var Count = 0;
    var Menu = eval(menuClientId);
    var rootElements = Menu.get_items();
    for (i = 0; i < position; i++) {
        Count += 1;
        ChildCount = GetChildMenuCount(rootElements.getItem(i));
        Count += ChildCount;
        ChildCount = 0;
    }
    return Count;
}
var ChildCount = 0;
function GetChildMenuCount(menuItem) {
    var childItems = menuItem.get_items();
    if (childItems.get_length() > 0) {
        for (var j = 0; j < childItems.get_length(); j++) {
            ChildCount++;
            if (childItems.getItem(j).get_items().get_length() == 0)
                continue;
            else {
                GetChildMenuCount(childItems.getItem(j));
            } // else end
        } // for end
    } //if end
    return ChildCount;
}

function MenuOverlay() {
    if (pageState == 'Overlay') {
        var topItems = eval(menuClientId).get_items();
        var arrMenuIds = new Array(topItems.get_length());
        var findMenuMatched = false;
        for (var j = 0; j < topItems.get_length(); j++) {
            var currentItemIndex = 0;
            arrMenuIds[j] = new Array(2);
            if (topItems.getItem(j).get_navigateUrl() != null && topItems.getItem(j).get_navigateUrl() != 'undefined') {
                arrMenuIds[j][0] = topItems.getItem(j).get_navigateUrl().replace("/", "");
            }
            currentItemIndex = calculateMenuCount(j);
            arrMenuIds[j][1] = menuClientId + "_" + currentItemIndex;
        }
        var parentElement = document.getElementById("overlay");
        setRootPos(document.getElementById("overlay"), document.getElementById(menuClientId));
        var previousWidth = 0;
        var prevObjectPosition = 0;
        for (i = 0; i < arrMenuIds.length; i++) {
            findMenuMatched = false;
            for (j = 0; j < arrOverlay.length; j++) {
                count = 0;
                pos = arrOverlay[j][1].indexOf("/");
                while (pos != -1) {
                    count++;
                    pos = arrOverlay[j][1].indexOf("/", pos + 1);
                }
                if (arrMenuIds[i][0] != "" && (arrOverlay[j][1] == arrMenuIds[i][0] || arrOverlay[j][6] == arrMenuIds[i][0]) && count == 1) {
                    prevObjectPosition = i;
                    var menuItemObject = document.getElementById(arrMenuIds[i][1]);
                    var htmlObject = document.createElement("div");
                    var insertHTML = "<div class=\"overlayArrow\" onclick=\"OverlayClick('" + arrOverlay[j][0] + "', this);\">";
                    insertHTML += arrOverlay[j][3] + "%";
                    insertHTML += "</div>";
                    htmlObject.innerHTML = insertHTML;
                    if (i > 0) {
                        var prevMenuItemObject = document.getElementById(arrMenuIds[prevObjectPosition][1]);
                        previousWidth += prevMenuItemObject.offsetWidth;
                    }
                    if (menuItemObject != null)
                        setMenuPos(htmlObject, document.getElementById(menuClientId), menuItemObject.offsetLeft, menuItemObject.offsetWidth);
                    parentElement.appendChild(htmlObject);
                    findMenuMatched = true;
                }
            }
            if (!findMenuMatched) {
                for (k = 0; k < urlMapArray.length; k++) {
                    if (arrMenuIds[i][0] != "" && urlMapArray[k][0] == arrMenuIds[i][0]) {
                        for (j = 0; j < arrOverlay.length; j++) {
                            if (arrOverlay[j][1] == urlMapArray[k][1]) {
                                prevObjectPosition = i;
                                var menuItemObject = document.getElementById(arrMenuIds[i][1]);
                                var htmlObject = document.createElement("div");
                                var insertHTML = "<div class=\"overlayArrow\" onclick=\"OverlayClick('" + arrOverlay[j][0] + "', this);\">";
                                insertHTML += arrOverlay[j][3] + "%";
                                insertHTML += "</div>";
                                htmlObject.innerHTML = insertHTML;
                                if (i > 0) {
                                    var prevMenuItemObject = document.getElementById(arrMenuIds[prevObjectPosition][1]);
                                    previousWidth += prevMenuItemObject.offsetWidth;
                                }
                                if (menuItemObject != null)
                                    setMenuPos(htmlObject, document.getElementById(menuClientId), menuItemObject.offsetLeft, menuItemObject.offsetWidth);
                                parentElement.appendChild(htmlObject);
                                findMenuMatched = true;
                            }
                        }
                    }
                }
            }
            if (!findMenuMatched && arrMenuIds[i][0] != "") {
                prevObjectPosition = i;
                var menuItemObject = document.getElementById(arrMenuIds[i][1]);
                var htmlObject = document.createElement("div");
                var insertHTML = "<div class=\"overlayArrow\">";
                insertHTML += "0%";
                insertHTML += "</div>";
                htmlObject.innerHTML = insertHTML;
                if (i > 0) {
                    var prevMenuItemObject = document.getElementById(arrMenuIds[prevObjectPosition][1]);
                    previousWidth += prevMenuItemObject.offsetWidth;
                }
                if (menuItemObject != null)
                    setMenuPos(htmlObject, document.getElementById(menuClientId), menuItemObject.offsetLeft, menuItemObject.offsetWidth);
                parentElement.appendChild(htmlObject);
            }
        }
    }
}


var emptyLinks = "";
var dummyLinks = "#";

// Anchor Tags 
function AnchorOverlay() {
    var parentElement = document.getElementById("overlay");
    var findAnchorMatched = false;
    var currentPageLinks = window.location + "";
    var isHttps_ForOverlay = -1;
    currentPageLinks = currentPageLinks.toLowerCase() + "#";
    if (pageState == 'Overlay') {
        var existingAnchors = document.getElementsByTagName("a");
        for (var count = 0; count < existingAnchors.length; count++) {
            if ($(existingAnchors[count]).is(':hidden') || $(existingAnchors[count]).hasClass('no-overlay')) {
                continue;
            }
            isHttps_ForOverlay = -1;
            var currentAnchor = existingAnchors[count].href;
            findAnchorMatched = false;
            currentAnchor = currentAnchor.toLowerCase();
            var javascriptLinks = currentAnchor.indexOf("javascript:") > -1;
            if (currentAnchor != emptyLinks) {
                if (currentAnchor.indexOf("https://") > -1) {
                    isHttps_ForOverlay = 1;
                }
                currentAnchor = isHttps_ForOverlay > -1 ? currentAnchor.replace("https://", "") : currentAnchor.replace("http://", "");
                currentAnchor = currentAnchor.replace("///", "/");
                currentAnchor = currentAnchor.replace("//", "/");
                //remove public site URL from cuurentAnchor
                var iAPPS_siteURL = jPublicSiteUrl.replace('https://', '');
                iAPPS_siteURL = jPublicSiteUrl.replace('http://', '');
                currentAnchor = currentAnchor.replace(iAPPS_siteURL, '');
                currentAnchor = decodeURIComponent(currentAnchor);
            }
            for (var j = 0; j < arrOverlay.length; j++) {
                if (!javascriptLinks && ifAnchorMatches(currentAnchor, arrOverlay[j][1], arrOverlay[j][6], isHttps_ForOverlay)) {
                    var htmlObject = document.createElement("div");
                    var insertHTML = "<div class=\"overlayArrow\" onclick=\"OverlayClick('" + arrOverlay[j][0] + "', this);\">";
                    insertHTML += arrOverlay[j][3] + "%";
                    insertHTML += "</div>";
                    htmlObject.innerHTML = insertHTML;
                    setAnchorOverlay(htmlObject, existingAnchors[count]);
                    parentElement.appendChild(htmlObject);
                    findAnchorMatched = true;
                }
            }
            if (!javascriptLinks && !findAnchorMatched && currentAnchor != emptyLinks && currentAnchor != dummyLinks && currentAnchor != currentPageLinks) {
                var htmlObject = document.createElement("div");
                var insertHTML = "<div class=\"overlayArrow\">";
                insertHTML += "0%";
                insertHTML += "</div>";
                htmlObject.innerHTML = insertHTML;
                setAnchorOverlay(htmlObject, existingAnchors[count]);
                parentElement.appendChild(htmlObject);
            }
        }
    }
}
function setAnchorOverlay(destObj, srcObj) {
    var objLeft = srcObj.offsetLeft;
    var objTop = srcObj.offsetTop;
    var objParent = srcObj.offsetParent;
    while (objParent != null && objParent.tagName.toUpperCase() != "BODY") {
        objLeft += objParent.offsetLeft;
        objTop += objParent.offsetTop;
        objParent = objParent.offsetParent;
    }
    destObj.style.top = objTop + "px";
    destObj.style.left = (srcObj.offsetWidth + objLeft) + "px";
    destObj.className = "percent";
}

function escapeHTMLEncode(str) {
    var div = document.createElement('div');
    var text = document.createTextNode(str);
    div.appendChild(text);
    return div.innerHTML;
}

function ifAnchorMatches(currentAnchor, overLayAnchor, alternateOverlayAnchor, isHttps_ForOverlay) {
    var currentPageLinks = window.location + "";
    var emptyPageLinks = currentPageLinks.toLowerCase();
    currentPageLinks = currentPageLinks.toLowerCase() + "#";
    var nullPageLinks = currentPageLinks.toLowerCase() + "null";
    // without any domain and http/https

    var noDomain_currentAnchor = currentAnchor.replace(document.domain, '');
    if (noDomain_currentAnchor.indexOf("/") == 0)
        noDomain_currentAnchor = noDomain_currentAnchor.substring(1, noDomain_currentAnchor.length);
    if (checkLinkExists(noDomain_currentAnchor, overLayAnchor, alternateOverlayAnchor, currentPageLinks, nullPageLinks, emptyPageLinks)) {
        return true;
    }



    // with only domain
    if (checkLinkExists(currentAnchor, overLayAnchor, alternateOverlayAnchor, currentPageLinks, nullPageLinks, emptyPageLinks)) {
        return true;
    }

    // full with http or https       and domain          
    currentAnchor = isHttps_ForOverlay > -1 ? "https://" + currentAnchor : "http://" + currentAnchor;

    if (checkLinkExists(currentAnchor, overLayAnchor, alternateOverlayAnchor, currentPageLinks, nullPageLinks, emptyPageLinks)) {
        return true;
    }

    return false;

}


function checkLinkExists(currentAnchor, overLayAnchor, alternateOverlayAnchor, currentPageLinks, nullPageLinks, emptyPageLinks) {
    if (((overLayAnchor != "" && currentAnchor == overLayAnchor.toLowerCase()) ||
        (alternateOverlayAnchor != "" && currentAnchor == alternateOverlayAnchor.toLowerCase())
            ) && currentAnchor != emptyLinks && currentAnchor != dummyLinks && currentAnchor != currentPageLinks
                && currentAnchor != nullPageLinks && currentAnchor != emptyPageLinks
         ) {
        return true;
    }

    currentAnchor = escapeHTMLEncode(currentAnchor);

    if (((overLayAnchor != "" && currentAnchor == overLayAnchor.toLowerCase()) ||
        (alternateOverlayAnchor != "" && currentAnchor == alternateOverlayAnchor.toLowerCase())
             ) && currentAnchor != emptyLinks && currentAnchor != dummyLinks && currentAnchor != currentPageLinks
                && currentAnchor != nullPageLinks && currentAnchor != emptyPageLinks
            ) {
        return true;
    }

    return false;
}
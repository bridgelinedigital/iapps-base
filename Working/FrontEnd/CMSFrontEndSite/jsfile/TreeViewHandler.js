﻿/* Copyright Bridgeline Digital, Inc. An unpublished work created in 2009. All rights reserved. This software contains the confidential and trade secret information of Bridgeline Digital, Inc. ("Bridgeline").  Copying, distribution or disclosure without Bridgeline's express written permission is prohibited */
var oldNodeText;
var myArgs;
var nodeText;
var treeOperation;
var nodeId;
var newNode;
var connectPage = false;
function TreeViewClickHandler(menuItem) {
    var contextData = menuItem.ParentMenu.ContextData;
    createPageFromTreeView = true;
    //alert(menuItem.ID);
    if (menuItem.ID == "AddSubMenu") {
        newNode = null;
        TreeView.beginUpdate();
        newNode = new ComponentArt.Web.UI.TreeViewNode();
        newNode.set_text(__JSMessages["NewNode"]);
        newNode.set_id('New Node');
        newNode.setProperty("CssClass", invisibleNodeCssClass);
        newNode.setProperty("HoverCssClass", invisibleNodeCssClass);
        newNode.setProperty("SelectedCssClass", invisibleSelectedNodeCssClass);
        contextData.Expand();
        contextData.get_nodes().add(newNode);
        TreeView.endUpdate();
        TreeContextMenuID.hide();
        TreeView.render();
        newNode.select();
        newNode.select();
        newNode.edit();
        nodeText = newNode.get_text()
        nodeId = contextData.get_id();
        proceed = false;
        treeOperation = "New";
        TreeContextMenuID.hide();
        return false;
        // eventArgs.set_cancel(true);
    }
    else if (menuItem.ID == "RenameMenu") {
        treeOperation = "ReName";
        contextData.edit();
        TreeContextMenuID.hide();
        proceed = false;
        return false;
        //eventArgs.set_cancel(true);
    }
    else if (menuItem.ID == "RemoveMenu") {
        var result = window.confirm(__JSMessages["AreYouSureToDeleteTheSelectedMenu"]);
        if (result == true) {
            nodeId = contextData.get_id();
            var result = DeleteMenuNode(nodeId);

            if (result == "true") {
                contextData.remove();
            }
            else if (result == "false") {
                alert(__JSMessages["ErrorDeleteMenuItemActionFailed"]);
            }
            else
                alert(result);
        }
        TreeContextMenuID.hide();
        return false;
    }
    else if (menuItem.ID == "EditProperties") {
        if (contextData.get_id().length == 36) {
            var adminpath = adminSiteUrl + "Administration/Site/EditMenuItemProperties.aspx?";
            var location = window.location;
            var queryString;
            var location1;
            if (location.href.search(/Token/) > 0) {
                location1 = location.href.substring(0, location.href.indexOf("Token"));
                queryString = "menuId=" + contextData.get_id() + "&BackToAdmin=1&returnUrl=editor&PageId=" + PageId;
            }
            else {
                queryString = "menuId=" + contextData.get_id() + "&BackToAdmin=1&returnUrl=editor&PageId=" + PageId;
            }
            queryString = adminpath + queryString;
            //Modified By AV
            //window.location.href = 
            //   debugger;
            SendTokenRequestByURLInSite(queryString);
            //End Modified
        }
        else {
            alert(__JSMessages["SelectedMenuNodeIsNotHavingProperId"]);
        }

    }
    else if (menuItem.ID == "CreateAndConnectNewPage") {
        //ValidatePageName();
        //TreeContextMenuID.hide();

        var menuItemName = menuItem.get_parentMenu().get_contextData().get_text();
        var pageNameTextbox = document.getElementById(CreateAndConnectNewPage_PageNameClientId);

        pageNameTextbox.value = menuItemName;

        connectPage = true;
    }
    else if (menuItem.ID == "CreateNewPage") {
        //ValidatePageName();

        var menuItemName = menuItem.get_parentMenu().get_contextData().get_text();
        var pageNameTextbox = document.getElementById(CreateNewPage_PageNameClientId);

        pageNameTextbox.value = menuItemName;

        connectPage = false;
    }
    else if (menuItem.ID == "ConnectExistingPage") {
        ConnectExistingPagePrivate();
        TreeContextMenuID.hide();
    }
}

function ConnectExistingPagePrivate() {
    SourcePopUpPageLibrary = "ConnectPage";
    var adminpath = adminSiteUrl + "InsertPageFromLibraryPopup.aspx";
    SendTokenRequestByURLWithMethodName(adminpath, "DisplayPageLibaryPopup();");
}

function DisplayPageLibaryPopup() {
    if (destinationUrl != "") {
        pagepopup = dhtmlmodal.open('PageLibrary', 'iframe', destinationUrl, __JSMessages["PageLibrary"], 'width=780px,height=513px,center=1,resize=0,scrolling=1');
        pagepopup.onclose = function () {
            var a = document.getElementById('PageLibrary');
            var ifr = a.getElementsByTagName("iframe");
            window.frames[ifr[0].name].location.replace("/blank.html");
            return true;
        }
    }
}

var selectedPage;
function selectedPageItem() {
    //debugger;
    //var selectedPageText = document.getElementById("txtPageName");
    //FullUrl = selectedPage.getMember("CompleteFriendlyURL").get_text();
    var pageId = selectedPage.getMember("Id").get_text();

    var returnValue = ConnectExistingPage(nodeId, pageId);
    if (returnValue == "True") {
        alert(__JSMessages["PageConnectedSuccessfullyToTheSelectedMenuItem"]);
    }
    else {
        alert(__JSMessages["PageCouldNotBeConnectedSuccessfully"]);
    }

}

function PutInEditMode(nodeId) {
    var node = TreeView.findNodeById(nodeId)
    node.edit();
}

var proceed = true;
var menuedit = true;
//Rename method of Tree view on client side
function NavigationTree_Rename(sender, eventArgs) {
    var result = null;
    var node = eventArgs.get_node();
    var parNode = eventArgs.get_node().get_parentNode();
    var newText = Trim(eventArgs.get_newText()); //eventArgs.get_newText();
    var oldText = eventArgs.get_node().get_text();
    var parentId = null;

    if (parNode == null)
        parentId = "00000000-0000-0000-0000-000000000000";
    else
        parentId = parNode.get_id()

    proceed = true;
    menuedit = true;
    // check for invalid chars in the node name
    if (!newText.match(/^[a-zA-Z0-9. _'\-<>.&$!|#:,()]+$/)) {
        if (newText != '' && IsNameAscii(newText) == 'false') {
            // unicode 
            proceed = true;
            menuedit = true;
        }
        else {
            alert(__JSMessages["MenuItemNameCanHaveOnlyAlphanumericCharacters"]);
            window.setTimeout('PutInEditMode(\'' + node.get_id() + '\')', 1);
            proceed = false;
            menuedit = false;
        }
    }


    // check if duplicate menu name within the same parent exist
    var currentNodeId = eventArgs.get_node().get_id();
    //var bExists = DoesMenuNameOrUrlExist(currentNodeId,newText,newText);
    var bExists;
    if (currentNodeId == "New Node")
        bExists = DoesMenuNameOrUrlExist("00000000-0000-0000-0000-000000000000", parentId, newText, newText);
    else
        bExists = DoesMenuNameOrUrlExist(currentNodeId, parentId, newText, newText);


    if (bExists == "1" || bExists == 1) {
        alert(__JSMessages["MenuItemWithTheSameFriendlyUrlAlreadyExists"])
        menuedit = false;
    }
    else if (bExists == "2" || bExists == 2) {
        alert(__JSMessages["MenuItemWithTheSameNameAlreadyExistsInThisLevel"])
        menuedit = false;
    }
    else if (bExists == "3" || bExists == 3) {
        alert(__JSMessages["MenuItemWithTheSameFriendlyUrlAndNameAlreadyExist"]);
        menuedit = false;
    }
    if (menuedit == false) {
        if (treeOperation != "New") {
            eventArgs.get_node().setProperty("Text", oldText);
            eventArgs.set_cancel(true);
        }
        else {
            newNode.remove();
        }
    }

    if (menuedit == true) {
        if (treeOperation != "New") {
            // server side callback method
            result = EditMenuNode(eventArgs.get_node().get_id(), eventArgs.get_newText());
            //alert(result);
            setTimeout(function () {
                if (result.toLowerCase() != "true" && treeOperation == 'ReName') {
                    alert(__JSMessages["CannotRenameTheCurrentMenu"]);
                }
                if (result.toLowerCase() == "true") {
                    eventArgs.get_node().set_toolTip(eventArgs.get_newText());
                }
            }, 1000);
        }
        else {
            // server side callback method
            var id = CreateMenuNode(newText, parentId);
            if (id.toString().length == 36) {
                TreeView.beginUpdate();
                eventArgs.get_node().set_id(id);
                eventArgs.get_node().set_toolTip(newText);
                TreeView.endUpdate();
                result = "True";
            }
            else {
                result = "False";
                if (treeOperation == "New") {
                    newNode.remove();
                }
                else {
                    eventArgs.get_node().setProperty("Text", oldText);
                }
            }
        }
        if (result != "True") {
            eventArgs.set_cancel(true);
        }
    }
}

var treeContextMenuX = 0;
var treeContextMenuY = 0;
var treeNode = null;
var evnt;
var TreeView;


function NavigationTreeViewOnContextMenu(sender, eventArgs) {

    TreeView = sender;
    var evt = eventArgs.get_event();
    evnt = evt;
    evt.cancelBubble = true;
    evt.returnValue = false;

    treeContextMenuX = evt.pageX ? evt.pageX : evt.x;
    treeContextMenuY = evt.pageY ? evt.pageY : evt.y;
    treeNode = eventArgs.get_node();
    nodeId = treeNode.get_id();
    //alert(nodeId);

    roleValue = GetNodeAccess(nodeId);
    //alert(roleValue);
    SetMenuItem(roleValue)

    if (roleValue != "0") {
        if (proceed == true) {
            TreeContextMenuID.set_contextData(treeNode);
            TreeContextMenuID.ShowContextMenuAtEvent(evt, treeNode);
        }
    }
}

function SetMenuItem(roleValue) {
    var menuItems = TreeContextMenuID.get_items();

    //If the role is Global A, A, P then show only connect Existing page and Create and Connect New Page
    if (roleValue == "6") {
        menuItems.getItem(5).set_visible(false);
        menuItems.getItem(6).set_visible(false);
        menuItems.getItem(2).set_visible(false);
        menuItems.getItem(1).set_visible(false);
        menuItems.getItem(7).set_visible(false);
    }
    else {
        menuItems.getItem(5).set_visible(true);
        menuItems.getItem(6).set_visible(true);
        menuItems.getItem(2).set_visible(true);
        menuItems.getItem(1).set_visible(true);
        menuItems.getItem(7).set_visible(true);
    }

    if (roleValue == "15") {
        menuItems.getItem(4).set_visible(false);
        menuItems.getItem(0).set_visible(false);
        menuItems.getItem(1).set_visible(false);
        menuItems.getItem(7).set_visible(false);
    }
    else {
        menuItems.getItem(4).set_visible(true);
        menuItems.getItem(0).set_visible(true);
        menuItems.getItem(1).set_visible(true);
        menuItems.getItem(7).set_visible(true);
    }
}


function ValidatePageName(pageName, templateId, connectPage) {
    //alert("ValidatePageName");
    var pageValid = false;
    var errorMsg = "";
    if (pageName == "") {
        errorMsg += __JSMessages["PageNameCouldNotBeEmpty"];
        pageValid = false;
    }
    else if (pageName != "") {
        if (pageName.match(/^[a-zA-Z0-9. _'\-<>.&$!|#:,()]+$/)) {
            pageValid = true;
        }
        else {
            if (IsNameAscii(pageName) == "true") {
                if (errorMsg != "") {
                    errorMsg += "\n" + __JSMessages["PageNameCanHaveOnlyAlphanumericCharacters"];
                }
                else {
                    errorMsg = __JSMessages["PageNameCanHaveOnlyAlphanumericCharacters"];
                }
                pageValid = false;
            }
            else
                pageValid = true;   // unicode page name

        }
    }

    if (templateId == "") {
        if (errorMsg != "") {
            errorMsg += "\n" + __JSMessages["PleaseSelectATemplate"];
        }
        else {
            errorMsg += __JSMessages["PleaseSelectATemplate"];
        }
        pageValid = false;
    }

    var error = DuplicatePagesWithFriendlyNameAndTitle("", pageName, "", nodeId);
    if (error == "1") {
        errorMsg += __JSMessages["ThePageWithSameUrlFriendlyNameAlreadyExists"];
        pageValid = false;
    }
    else if (error == "2") {
        errorMsg += __JSMessages["ThePageWithSameNameAlreadyExists"];
        pageValid = false;
    }
    else if (error == "3") {
        errorMsg += __JSMessages["ThePageWithSameNameUrlFriendlyNameAlreadyExists"];
        pageValid = false;
    }


    if (pageValid) {
        var returnValue = CreateNewPageCallBack(nodeId, pageName, templateId, connectPage);
        if (returnValue != "") {
            if (connectPage == true) {
                //alert("Page created and connected successfully.");
            }
            else {
                //alert("Page created successfully.");
            }
            window.location.href = returnValue;
        }
        else {
            alert(__JSMessages["ErrorPageCreateAndConnectActionFailed"]);
        }

        TreeContextMenuID.hide();
    }
    else {
        alert(errorMsg);
    }

    //no post back
    //return pageValid;

}
function CloseConnectPage() {
    TreeContextMenuID.hide();
    return false;
}



function Trim(s) {
    // Remove leading spaces and carriage returns
    while ((s.substring(0, 1) == ' ') || (s.substring(0, 1) == '\n') || (s.substring(0, 1) == '\r')) {
        s = s.substring(1, s.length);
    }
    // Remove trailing spaces and carriage returns
    while ((s.substring(s.length - 1, s.length) == ' ') || (s.substring(s.length - 1, s.length) == '\n') || (s.substring(s.length - 1, s.length) == '\r')) {
        s = s.substring(0, s.length - 1);
    }
    return s;
}




/* start commerce related */

function ProductNavTreeViewOnContextMenu(sender, eventArgs) {
    TreeView = sender;
    var evt = eventArgs.get_event();
    evnt = evt;
    evt.cancelBubble = true;
    evt.returnValue = false;
    treeContextMenuX = evt.pageX ? evt.pageX : evt.x;
    treeContextMenuY = evt.pageY ? evt.pageY : evt.y;
    treeNode = eventArgs.get_node();
    nodeId = treeNode.get_id();
    TreeContextMenuID.set_contextData(treeNode);
    TreeContextMenuID.ShowContextMenuAtEvent(evt, treeNode);

}

function ProductNavTreeViewClickHandler(menuItem) {
    var contextData = menuItem.ParentMenu.ContextData;
    if (menuItem.ID == "ManageProductNav") {
        if (contextData.get_id().length == 36) {
            var adminpath = jCommerceAdminSiteUrl + "/StoreManager/NavigationCategories/NavigationBuilder.aspx?";
            var location = window.location;
            var queryString;
            var location1;
            if (location.href.search(/Token/) > 0) {
                location1 = location.href.substring(0, location.href.indexOf("Token"));
                queryString = "menuId=" + contextData.get_id() + "&returnUrl=editor&PageId=" + PageId;
            }
            else {
                queryString = "menuId=" + contextData.get_id() + "&returnUrl=editor&PageId=" + PageId;
            }
            queryString = adminpath + queryString;
            jProductId = jCommerceProductId;
            SendTokenRequestByURLInSite(queryString);
            //End Modified
        }
        else {
            alert(__JSMessages["SelectedMenuNodeIsNotHavingProperId"]);
        }

    }
}




/* end commerce related */


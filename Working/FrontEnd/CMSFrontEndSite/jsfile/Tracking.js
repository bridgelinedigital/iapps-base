﻿
var iAPPSTracking = {

    setCookie: function (cname,cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires + " ;path=/";
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    Post:  function(data,callback) {
        var xmlhttp;

        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
                if (xmlhttp.status == 200) {
                    var result = JSON.parse(xmlhttp.responseText);
                    var visitorId = result.items[0].visitorId;
                    iAPPSTracking.setCookie("iappsvisitor", visitorId, 365);
                }
                else if(xmlhttp.status == 400) {
                }
                else {
                }
            }
        }
        xmlhttp.open("GET", window.postURL + '?URL=' + encodeURI(data.URL) + "&visitor=" + data.Visitor + "&contact=" + data.Contact + "&ObjectId=" + data.ObjectId + "&TypeId=" + data.ObjectType + "&LinkURL=" + data.LinkURL, true);
       // xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.send();
    },
    Collect: function () {
        var objectId='';
        if (typeof pageState !== 'undefined' && typeof PageId !== 'undefined') {
            if (pageState == 'View')
                objectId = PageId;
        }
        var data = {
            Time: new Date(),
            URL: window.location.href,
            Visitor: iAPPSTracking.getCookie("iappsvisitor"),
            Contact: iAPPSTracking.getCookie("iappscontact"),
            ObjectType: 404,
            ObjectId: objectId,
            LinkURL: ''
        }
        return data;
    },

    Initialize: function () {
        
        var data = iAPPSTracking.Collect();
        setTimeout(function () {
            iAPPSTracking.Post(data);
        }, 0);
        //add event bubling to links.
        if (document.addEventListener)
            document.addEventListener('click', iAPPSTracking.LinkOnClick, false);
        else
            document.attachEvent('onclick', iAPPSTracking.LinkOnClick);
        
    },

    LinkOnClick: function (e)
    {
        var e = e || window.e;

        if (e.target.tagName !== 'A')
            return;
        // collect and post data.
        var data = iAPPSTracking.Collect();
        data.ObjectType = 31;
        data.LinkURL = e.target.href;
        iAPPSTracking.Post(data);
        return;
    }
}

iAPPSTracking.Initialize();


var hasCMSLicense;
var hasAnalyticsLicense;
var hasCommerceLicense;
var hasMarketierLicense;
var thisPageLinkedMenus;
var thisMenuId;

function JSSavePage() {
    Bridgeline.iapps.containers.modified = false;
    document.getElementById("hfSaveAsDraft").value = "True";
    __doPostBack('__Page', 'SavePageAsDraft');
    //document.forms["form1"].submit();
}

var RefreshPageVariants;
function OpenSegmentationFrame(returnVal, refresh) {
    RefreshPageVariants = refresh;
    $(document).scrollTop(0);

    var navigateUrl = returnVal.NavigateUrl;
    if (navigateUrl.indexOf("?") != 0)
        navigateUrl = "?" + navigateUrl;

    var v_deviceurl = document.URL.substring(0, document.URL.indexOf('?'));
    if (v_deviceurl == '')
        v_deviceurl = document.URL;

    iAPPS_ShowEditDeviceFrame(v_deviceurl + navigateUrl);
    return false;
}

function SubmitToWorkFlowOrPublish(SubmitToWorkFlow) {
    OpeniAppsAdminPopup("SubmitToWorkFlow", "PageId=" + PageId + "&SubmitToWorkFlow=" + SubmitToWorkFlow, "ReloadBasePage");
}

function SubmitForTranslation(SubmitToWorkFlow, popupMode) //popup mode has the values of the TranslationPopupMode enum
{
    OpeniAppsAdminPopup("SubmitForTranslation", "PageId=" + PageId + "&SubmitToWorkFlow=" + SubmitToWorkFlow + "&ObjectTypeId=8&PopupMode=" + popupMode, "ReloadBasePage");
}

function ReloadBasePage() {
    if ((popupActionsJson.Action == "Publish") &&
            typeof popupActionsJson.CustomAttributes["PageRedirectUrl"] != "undefined") {
        window.location.href = popupActionsJson.CustomAttributes["PageRedirectUrl"];
    }
    else
        __doPostBack('', '');
}

function ReloadPage() {
    window.location.href = window.location.href;
}

function SetiAPPSContainerInfo(Data, contentId) {
    if (typeof iapps_containerInfo != "undefined") {
        var container = $.grep(iapps_containerInfo, function (e, index) {
            return e.Name.toLowerCase() == Data.Container.getAttribute("ContainerName").toLowerCase();
        });

        if (container && container.length == 1)
            container[0].ContentId = contentId;
    }
}

var iAPPS_SelectedDeviceWidth;
var iAPPS_SelectedDeviceHeight;
var iAPPS_SelectedDeviceItem;
var iAPPS_SelectedAudienceItem;
var iAPPS_SelectedNavigateUrl;
var iAPPS_SelectedDevice;
var iAPPS_SelectedDeviceMode;

function fn_iAPPSSaveDeviceContent() {
    var v_devicehtml = $('div#tablet');
    var v_savebutton = $(v_devicehtml).find('iframe').contents().find('#lbtnSaveDeviceData');
    $(v_savebutton).click();
}

function iAPPS_ShowEditDeviceFrame(navigateUrl) {
    $("html, body", parent.document).css("overflow", "hidden");

    $("#iapps-page-variant-popup").remove();

    var deviceFrame = $("<iframe />");

    deviceFrame.attr("src", navigateUrl + "&PageState=Edit").attr("scrolling", "yes");

    var deviceFrameDiv = $("<div id='iapps-page-variant-popup' />");
    deviceFrameDiv.append(deviceFrame);

    $("body").append(deviceFrameDiv);
}

function SaveDeviceWindow() {
    Bridgeline.iapps.containers.modified = false;

    $("#iapps-page-variant-popup", parent.document).hide();

    return true;
}

function CloseDeviceWindow() {
    if (SaveSegmentData())
        parent.ClosePageVariantPopup();

    return false;
}

function ClosePageVariantPopup() {
    RefreshPageVariants();

    $(document).scrollTop(0);
    $("#iApps_LoadingPanel").remove();
    $(".iAPPSPopupOverlay").css("z-index", "");

    $("#iapps-page-variant-popup").fadeOut(function () {
        $(this).remove();
        $("html, body").css("overflow", "auto");
    });

    if (parent.RefreshPageVariants != null)
        parent.RefreshPageVariants.call();

    return false;
}

function SaveSegmentData() {
    if (Bridgeline.iapps.containers.modified) {
        return confirm(__JSMessages["PleaseSaveTheSegmentOtherwiseYourChangesWillBeLost"]);
    }

    return true;
}

function JumpToAdminCheckLicense(selectedId) {
    return CheckLicense(selectedId, true);
}

function ShowContentSaveWarning() {
    var iappsTop = ($(window).height() - $('#iapps_warning_msg').outerHeight(true)) / 2 + $(window).scrollTop() + "px";
    var iappsLeft = ($(window).width() - $('#iapps_warning_msg').outerWidth(true)) / 2 + $(window).scrollLeft() + "px";
    $('#iapps_warning_msg').css({
        top: iappsTop,
        left: iappsLeft
    });
    $('#iapps_warning_msg').show();
    setTimeout(function () {
        $('#iapps_warning_msg').fadeOut('slow');
    }, 2000);
    //check if function js method is implemented by developers before calling it
    if (typeof iAPPS_onBeforeContentSave == 'function') {
        iAPPS_onBeforeContentSave();
    }
}

function OpenBasePageForAudienceSegment(url) {
    basePageAudienceSegment = dhtmlmodal.open('AudienceSegment', 'iframe', url, '', '');
}


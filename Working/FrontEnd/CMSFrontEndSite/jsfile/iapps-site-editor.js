var SiteEditor = {
    polyfills: {
        initialize: function () {
            if (!Array.prototype.find) {
                Array.prototype.find = function (predicate) {
                    if (this === null) {
                        throw new TypeError('Array.prototype.find called on null or undefined');
                    }
                    if (typeof predicate !== 'function') {
                        throw new TypeError('predicate must be a function');
                    }
                    var list = Object(this);
                    var length = list.length >>> 0;
                    var thisArg = arguments[1];
                    var value;

                    for (var i = 0; i < length; i++) {
                        value = list[i];
                        if (predicate.call(thisArg, value, i, list)) {
                            return value;
                        }
                    }
                    return undefined;
                };
            }

            if (!Array.prototype.findIndex) {
                Array.prototype.findIndex = function (predicate) {
                    if (this === null) {
                        throw new TypeError('Array.prototype.findIndex called on null or undefined');
                    }
                    if (typeof predicate !== 'function') {
                        throw new TypeError('predicate must be a function');
                    }
                    var list = Object(this);
                    var length = list.length >>> 0;
                    var thisArg = arguments[1];
                    var value;

                    for (var i = 0; i < length; i++) {
                        value = list[i];
                        if (predicate.call(thisArg, value, i, list)) {
                            return i;
                        }
                    }
                    return -1;
                };
            }

            if (!Array.prototype.includes) {
                Array.prototype.includes = function (searchElement /*, fromIndex*/) {
                    'use strict';
                    var O = Object(this);
                    var len = parseInt(O.length, 10) || 0;
                    if (len === 0) {
                        return false;
                    }
                    var n = parseInt(arguments[1], 10) || 0;
                    var k;
                    if (n >= 0) {
                        k = n;
                    } else {
                        k = len + n;
                        if (k < 0) { k = 0; }
                    }
                    var currentElement;
                    while (k < len) {
                        currentElement = O[k];
                        if (searchElement === currentElement ||
                            (searchElement !== searchElement && currentElement !== currentElement)) { // NaN !== NaN
                            return true;
                        }
                        k++;
                    }
                    return false;
                };
            }
        }
    },

    toolbar: {
        userOptionClicked: function (event) {
            var commandName = $(this).data("command-name");

            switch (commandName) {
                case "CheckAccessibility":
                    event.preventDefault();

                    Bridgeline.iapps.components.checkAccessibility();

                    break;
                case "PageNotes":
                    event.preventDefault();

                    Bridgeline.iapps.components.showPageNotes();

                    break;
                case "PageProperties":
                    event.preventDefault();

                    Bridgeline.iapps.components.openPageDetail(PageId);

                    break;
                case "PageHistory":
                    event.preventDefault();

                    Bridgeline.iapps.components.openPageHistory(PageId);

                    break;
                case "TranslationSubmit":
                    event.preventDefault();

                    Bridgeline.iapps.components.submitForTranslation(PageId);

                    break;
                case "DistributionSubmit":
                    event.preventDefault();

                    Bridgeline.iapps.components.submitForDistribution(PageId);

                    break;
                case "PageVariants":
                    event.preventDefault();

                    $(document).scrollTop(0);

                    OpeniAppsAdminPopup("ManagePageVariants", "ReloadFrame=true&FromSiteEditor=true&PageId=" + PageId, "ReloadPage");

                    break;
                case "PreviewLink":
                    event.preventDefault();

                    OpeniAppsAdminPopup("SendPageLink", "currentPageId=" + PageId + "&currentPageMapId=" + thisMenuId);

                    break;
                case "SharedPageInfo":
                    event.preventDefault();

                    $("#iapps-shared-page").show();

                    break;
                case "SaveDraft":
                case "Publish":
                case "WorkflowSubmit":
                    Bridgeline.iapps.containers.modified = false;

                    //var invalidContainers = $.grep(iapps_containerInfo, function (e, index) {
                    //    return e.IsRequired === true && e.ContentId === EmptyGuid;
                    //});

                    //if (invalidContainers.length > 0) {
                    //    event.preventDefault();

                    //    var message = "Please enter content in the following containers:\n";

                    //    $.each(invalidContainers, function () {
                    //        message += stringformat(" - {0}\n", this.DisplayTitle)
                    //    });

                    //    alert(message);
                    //}

                    break;
                case "WorkflowApprove":
                case "WorkflowReject":
                case "WorkflowRemove":
                    Bridgeline.iapps.containers.modified = false;
                    break;
                case "WorkflowSequence":
                    event.preventDefault();

                    Bridgeline.iapps.components.openWorkflowSequence(PageId, WorkflowId);

                    break;
            }
        },

        siteDropDown: {
            render: function () {
                var dropdown = $("li.iapps-site-selector");
                if (typeof siteSwitchJson !== "undefined" && siteSwitchJson.length > 1) {
                    var filter = $(".iapps-site-filter", dropdown);

                    $(".iapps-current-site").addClass("iapps-site-selector-toggle");

                    // Focus textbox on hover
                    var siteSelectorTimer = null;

                    dropdown.hover(function () {
                        if (siteSelectorTimer)
                            clearTimeout(siteSelectorTimer);

                        filter.focus();
                    }, function () {
                        siteSelectorTimer = setTimeout(function () {
                            filter.blur();
                            filter.val("");

                            SiteEditor.toolbar.siteDropDown.filter("");
                        }, 1000);
                    });

                    // Filter site list
                    filter.on("keyup", function (event) {
                        if (event.which === 13)
                            event.preventDefault();
                        else if (event.which === 27) {
                            $(this).val("");

                            SiteEditor.toolbar.siteDropDown.filter("");
                        }
                        else {
                            var keywords = $(this).val();

                            SiteEditor.toolbar.siteDropDown.filter(keywords);
                        }
                    });

                    // Redirect with token
                    $(document).on("click", "li.iapps-site-selector ul a", function (event) {
                        event.preventDefault();

                        var siteId = $(this).parent().data("site-id");
                        var url = $(this).attr("href");

                        RedirectWithToken(GetCurrentProductName(), url, siteId);
                    });

                    // Display menu
                    dropdown.show();
                }
                else {
                    dropdown.remove();
                }
            },

            filter: function (keywords) {
                if (keywords)
                    keywords = keywords.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");

                var dropdown = $("li.iapps-site-selector");
                var filterNotice = $(".iapps-site-filter-notice", dropdown)

                $("ul", dropdown).hide().empty();
                filterNotice.hide();

                var html = '';
                var filterRegex = new RegExp("(" + keywords + ")", "gi");
                var visibleSites = 0;

                for (var i = 0; i < siteSwitchJson.length && visibleSites < 100; i++) {
                    var site = siteSwitchJson[i];

                    if (filterRegex.test(site.title) || (site.groups !== null && site.groups.some(function (group) {
                        return filterRegex.test(group.title);
                    }))) {
                        html += '<li data-site-id="' + site.id + '" data-site-title="' + site.title + '">';
                        html += '<a href="' + site.url + '">' + (keywords ? site.title.replace(filterRegex, '<strong>$1</strong>') : site.title) + '</a>';
                        html += '</li>';

                        visibleSites++;
                    }
                }

                $("ul", dropdown).html(html);

                if (visibleSites > 0)
                    $("ul", dropdown).show();

                if (visibleSites >= 100)
                    filterNotice.show();
            },

            initialize: function (toolbar) {
                this.render();
                this.filter("");
            }
        },

        pagePreview: {
            displayPreview: function () {
                var preview = $("#iapps-page-preview");
                var iframe = $("iframe", preview);
                var selectedDevice = $(".iapps-page-preview-device option:selected");
                var orientation = $(".iapps-page-preview-orientation").val();
                var deviceWidth = selectedDevice.data("width");
                var deviceHeight = selectedDevice.data("height");

                if (orientation === "Portrait") {
                    iframe.css({
                        width: deviceWidth ? deviceWidth + "px" : "100%",
                        height: deviceHeight ? deviceHeight + "px" : "100%"
                    });
                }
                else if (orientation === "Landscape") {
                    iframe.css({
                        width: deviceHeight ? deviceHeight + "px" : "100%",
                        height: deviceWidth ? deviceWidth + "px" : "100%"
                    });
                }

                var scale = 1;

                if (selectedDevice.val() !== "Desktop" && (deviceHeight > preview.height() || deviceWidth > preview.width())) {
                    var heightScale = preview.height() / deviceHeight;

                    if (heightScale < scale)
                        scale = heightScale;

                    var widthScale = preview.width() / deviceWidth;

                    if (widthScale < scale)
                        scale = widthScale;
                }

                iframe.css("transform", "scale(" + scale + ")");
            },

            initialize: function () {
                if ($("#iapps-page-preview").length > 0 && parent === window.top) {
                    $("body").addClass("iapps-page-preview");

                    var device = $(".iapps-page-preview-device");
                    var orientation = $(".iapps-page-preview-orientation");

                    orientation.toggle(device.val() !== "Desktop");

                    device.on("change", function () {
                        orientation.toggle(device.val() !== "Desktop");

                        SiteEditor.toolbar.pagePreview.displayPreview();
                    });

                    orientation.on("change", function () {
                        SiteEditor.toolbar.pagePreview.displayPreview();
                    });

                    $(window).on("resize", function () {
                        SiteEditor.toolbar.pagePreview.displayPreview();
                    });

                    this.displayPreview();
                }
                else if ($("#iapps-page-preview", parent.document).length > 0) {
                    $(document).on("click", "a[href]:not([href^='#']):not([target='_blank'])", function (event) {
                        event.preventDefault();
                        event.stopPropagation();

                        if ($(".iapps-page-preview-device", parent.document).val() === "Desktop")
                            parent.location.href = $(this).attr("href");
                        else
                            alert("Navigation is disabled when previewing a mobile device layout");
                    });
                }
            }
        },

        initialize: function () {
            var toolbar = $("#iapps-site-editor-toolbar");

            $(".iapps-toolbar-toggle", toolbar).on("click", function () {
                $("#iapps-site-editor-toolbar").toggleClass("iapps-toolbar-expanded");
            });

            $("[data-command-name]", toolbar).on("click", SiteEditor.toolbar.userOptionClicked);

            this.siteDropDown.initialize(toolbar);
            this.pagePreview.initialize();
        }
    },

    modals: {
        initialize: function () {
            $("[data-modal-action]", "div.iapps-modal").on("click", function (event) {
                event.preventDefault();

                var button = $(this);
                var modal = button.closest("div.iapps-modal");
                var action = button.data("modal-action");

                switch (action) {
                    case "close":
                        modal.trigger("close");
                        modal.hide();

                        break;
                }
            });

            $("div.iapps-modal-header", "div.iapps-modal-draggable").on("mousedown.iapps-modal", function (event) {
                event.preventDefault();

                var header = $(this);
                var modal = header.closest("div.iapps-modal-draggable");
                var offsetY = event.pageY - $(this).parent().offset().top;
                var offsetX = event.pageX - $(this).parent().offset().left;

                $("body").on("mousemove.iapps-modal", function (event) {
                    modal.css({
                        top: event.clientY - offsetY,
                        left: event.clientX - offsetX
                    });
                }).on("mouseup", function () {
                    $("body").unbind("mousemove.iapps-modal");
                });
            });
        },
    },

    pageBuilder: {
        availableControls: null,
        pageConfiguration: null,

        save: function () {
            // Set the index of each control
            var zoneContainers = $(".iapps-drop-zone");

            zoneContainers.each(function () {
                var zoneContainer = $(this);
                var zoneData = SiteEditor.pageBuilder.pageConfiguration.find(function (zone) { return zone.ZoneId === zoneContainer.attr("data-control-id"); });

                zoneContainer.children(".iapps-content-container-placeholder").each(function () {
                    var control = $(this);

                    for (var i = 0; i < zoneData.ZoneItems.length; i++) {
                        var controlData = zoneData.ZoneItems[i];

                        if (controlData.ZoneItemId === control.attr("data-control-id")) {
                            controlData.ZoneItemIndex = control.index();

                            break;
                        }
                    }
                });
            });

            $("#hdnPageEditorJSON").val(JSON.stringify(SiteEditor.pageBuilder.pageConfiguration));

            return true;
        },

        initialize: function () {
            if (this.pageConfiguration && this.availableControls) {
                var toolbox = $("#iapps-page-builder-controls");
                var removeDropZone = $(".iapps-drop-zone-remove", toolbox);

                $(".iapps-content-container-placeholder", toolbox).draggable({
                    connectToSortable: ".iapps-drop-zone",
                    helper: "clone",
                    revert: false,                    
                    start: function (event, ui) {
                        var zoneContainers = $(".iapps-drop-zone");

                        zoneContainers.each(function () {
                            var zoneContainer = $(this);
                            var zoneId = zoneContainer.attr("data-control-id");
                            var zoneData = SiteEditor.pageBuilder.pageConfiguration.find(function (zone) { return zone.ZoneId === zoneId });

                            if (!zoneData) {
                                zoneData = {
                                    ZoneId: zoneId,
                                    ZoneItems: []
                                }

                                SiteEditor.pageBuilder.pageConfiguration.push(zoneData);
                            }

                            var controlType = ui.helper.attr("data-qualified-name");
                            var controlModel = SiteEditor.pageBuilder.availableControls[controlType];
                            var maxControls = zoneContainer.attr("data-max-controls") ? parseInt(zoneContainer.attr("data-max-controls")) : null;
                            var allowedControls = zoneContainer.attr("data-allowed-controls") ? zoneContainer.attr("data-allowed-controls").trim().split(/,\s*/) : null;

                            var validZone =
                                !(maxControls && zoneData.ZoneItems.length >= maxControls) &&
                                !(allowedControls && allowedControls.length > 0 && allowedControls.indexOf(controlType) === -1);

                            if (!validZone)
                                zoneContainer.addClass("iapps-invalid");
                        });
                    },
                    stop: function (event, ui) {
                        var zoneContainers = $(".iapps-drop-zone");

                        zoneContainers.removeClass("iapps-invalid");
                    }
                });

                $(".iapps-drop-zone").sortable({
                    helper: "clone",
                    appendTo: "#iapps-page-builder-controls",
                    items: ".iapps-content-container-placeholder:not(.iapps-drag-disable)",
                    forcePlaceHolderSize: true,
                    revert: false,
                    start: function (event, ui) {
                        var control = ui.item;

                        if (!control.attr("data-context") && !control.attr("data-content-id"))
                            removeDropZone.addClass("iapps-active");
                    },
                    stop: function (event, ui) {
                        var zoneContainer = $(this);
                        var control = ui.item;

                        if (zoneContainer.hasClass("iapps-invalid"))
                            ui.item.remove();
                        else if (control.attr("data-context") === "toolbox") {
                            var zoneData = SiteEditor.pageBuilder.pageConfiguration.find(function (zone) { return zone.ZoneId === zoneContainer.attr("data-control-id"); });
                            var controlType = control.attr("data-qualified-name");
                            var controlModel = SiteEditor.pageBuilder.availableControls[controlType];
                            var controlId = zoneData.ZoneId + "_" + FWCallback.GetRandomString();

                            // Clone toolbox data
                            var zoneItem = JSON.parse(JSON.stringify(controlModel));

                            zoneItem.ZoneItemId = controlId;
                            zoneItem.ZoneItemIndex = control.index();
                            zoneItem.SiteId = SiteEditor.pageBuilder.siteId;

                            zoneData.ZoneItems.push(zoneItem);

                            control.removeAttr("data-context");
                            control.attr("data-control-id", controlId);
                        }

                        removeDropZone.removeClass("iapps-active");
                    }
                });

                removeDropZone.droppable({
                    drop: function (event, ui) {
                        var control = $(ui.draggable);

                        if (control.parent().is(".iapps-drop-zone") && !control.attr("data-content-id")) {
                            var zoneContainer = control.parent();
                            var zoneData = SiteEditor.pageBuilder.pageConfiguration.find(function (zone) { return zone.ZoneId === zoneContainer.attr("data-control-id"); });
                            var controlIndex = zoneData.ZoneItems.findIndex(function (zoneItem) { return zoneItem.ZoneItemId === control.attr("data-control-id") });

                            if (controlIndex > -1)
                                zoneData.ZoneItems.splice(controlIndex, 1);

                            ui.draggable.remove();
                        }
                    }
                });

            }
        }
    },

    initialize: function () {
        this.polyfills.initialize();
        this.toolbar.initialize();
        this.modals.initialize();
        this.pageBuilder.initialize();
    }
};

$(document).ready(function () {
    setTimeout(function () {
        SiteEditor.initialize();
    }, 100);
});
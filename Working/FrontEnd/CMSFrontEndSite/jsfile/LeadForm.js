﻿var $ = jQuery.noConflict();

var iAppsLeadForm = {
    LoadLeadForm: function (leadForm) {

        var leadFormObj = eval(leadForm);
        var leadFormId = "#" + leadFormObj.Id;

        var formUrl = "http://" + leadFormObj.SiteUrl + "/" + leadFormObj.api + "/Form/GetLeadFormHTML?formId=" + leadFormObj.FormId; //TODO: Read the api also from the variable 

        iAppsLeadForm.ShowLoadingPanel();
        $.get(formUrl, function (data) {
            $(leadFormId).html(data);
        })
        .done(function () {
            iAppsLeadForm.HideLoadingPanel();
            if (typeof iAPPSTracking === 'undefined') {
                var trackingScriptPath = "http://" + leadFormObj.SiteUrl + "/jsfile/Tracking.js";
                window.postURL = "http://" + leadFormObj.SiteUrl + "/api/tracking/";
                $.getScript(trackingScriptPath);
            }
        })
        .fail(function (error) {
            alert(error.responseText);
        });
        $(leadFormId).validate({
            submitHandler: function (form) {
                iAppsLeadForm.ShowLoadingPanel();
                if (typeof iAPPSTracking === 'undefined') {
                    var trackingScriptPath = "http://" + leadFormObj.SiteUrl + "/jsfile/Tracking.js";
                    window.postURL = "http://" + leadFormObj.SiteUrl + "/api/tracking/";
                    $.getScript(trackingScriptPath, function () {
                        iAppsLeadForm.SubmitForm(form, leadFormObj);
                    });
                }
                else
                    iAppsLeadForm.SubmitForm(form, leadFormObj);
            }
        });
    },
    SubmitForm: function (form, leadFormObj) {
        if (typeof LeadFormCustomValidation === "function") {
            var submitproceed = LeadFormCustomValidation(form);

            if (!submitproceed) {
                console.log("Custom validation returned false");
                iAppsLeadForm.HideLoadingPanel();
                return;
            }
        }
        var formIdEscaped = leadFormObj.FormId.replace(/-/g, "_");
        var formProperties = eval("leadFormProperties_" + formIdEscaped);
        console.log(formProperties);

        var formelements = $(form).serialize();

        formelements = formelements + '&iappsvisitor=' + iAPPSTracking.getCookie("iappsvisitor");
        formelements = formelements + '&iappscontact=' + iAPPSTracking.getCookie("iappscontact");
        console.log(formelements);

        var postUrl = "http://" + leadFormObj.SiteUrl + "/api/Form/AddLeadFormContact";
        var leadFormPost = $.post(postUrl, formelements);

        leadFormPost.done(function (data) {
            iAppsLeadForm.HideLoadingPanel();
            if (data != '' && data.length == 36)
                iAPPSTracking.setCookie("iappscontact", data, 365);
            //Navigate to Thank you url
            if (formProperties.ThankYouUrl != '')
                window.location.href = formProperties.ThankYouUrl;
            else {
                var thankyoudiv = "#thankYouContent_" + leadFormObj.FormId;
                $(form).html($(thankyoudiv).html());
            }
        })
        .fail(function (error) {
            iAppsLeadForm.HideLoadingPanel();
            alert(error.responseText);
        });
    },
    ShowLoadingPanel: function () {
        $('#loading').show();

    },
    HideLoadingPanel: function () {
        $('#loading').hide();

    }
}








var isToolbarLoaded = false;
function ShowToolbar(oWnd) {
    var editorDisplayTop = document.getElementById("editableArea").offsetTop - 110;
    var toolbarTop = editorDisplayTop > 0 ? editorDisplayTop + "px" : "0px";
    var toolbarLeft;
    if ((screen.width - document.getElementById("editableArea").offsetLeft) < 718) {
        toolbarLeft = (document.getElementById("editableArea").offsetLeft - 718) + "px";
    }
    else {
        toolbarLeft = document.getElementById("editableArea").offsetLeft + "px";
    }
    oWnd.moveTo(toolbarLeft, toolbarTop);
}

function ToggleFloatingToolbar(editor) {
    var button = iAPPS_GetElementsByClassName("ToggleFloatingToolbar")[0];
    setTimeout(function () {
        button.click();
        var oWnd = editor.get_toolAdapter().get_window();
        ShowToolbar(oWnd);
    }, 10);
}

function HideEditorToolbar() {
    //get reference to the toolbar container
    var toolbarPopup = oureditor.get_toolAdapter().get_toolbarHolder().get_popupElement();
    //get reference to the toolbar close button
    var toolbarCloseButton = iAPPS_GetElementsByClassName("rwCloseButton", toolbarPopup)[0];
    toolbarCloseButton.click();
}


function iAPPS_GetElementsByClassName(classname, node) {
    if (!node) node = document.getElementsByTagName("body")[0];
    var a = [];
    var re = new RegExp('\\b' + classname + '\\b');
    var els = node.getElementsByTagName("*");
    for (var i = 0, j = els.length; i < j; i++)
        if (re.test(els[i].className)) a.push(els[i]);
    return a;
}

//simulate InternetExplorer's click() method for showing the toolbar
if (!document.all) {
    HTMLElement.prototype.click = function () {
        var evt = this.ownerDocument.createEvent('MouseEvents');
        evt.initMouseEvent('click', true, true, this.ownerDocument.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        this.dispatchEvent(evt);
    }
}

function ShowFloatingToolBar() {
    oureditor.set_html(textToEdit);
    ToggleFloatingToolbar(oureditor);
    setTimeout(function () {
        //add input buttons instead of tools only when the toolbar is loaded for the first time
        if (!isToolbarLoaded) {
            if (oureditor.getToolByName("SharePoint") != null) {
                var sharepointText = oureditor.getToolByName("SharePoint").get_element();
                if (sharepointText != null) {
                    var parentCustomTool = sharepointText.parentNode;
                    sharepointText.style.display = 'none';
                    if (parentCustomTool != null) {
                        parentCustomTool.style.width = 'auto';
                        parentCustomTool.innerHTML = '<span class="sharepoint-text">' + sharepointText.getAttribute('title') + '</span>';
                    }
                }
            }
            var saveAsButton = document.getElementById("btniAPPSSaveAs");
            var saveButton = document.getElementById("btniAPPSSave");
            var cancelButton = document.getElementById("btniAPPSCancel");
            if (saveAsButton != null && saveAsButton != undefined) {
                //get a reference to the custom tool and its parent element
                var customButton = oureditor.getToolByName("SaveAsContent").get_element();
                var parentCustomTool = customButton.parentNode;
                //insert the button before the custom button
                parentCustomTool.insertBefore(saveAsButton, customButton);
                //hide the custom button
                customButton.style.display = "none";
                saveAsButton.style.display = "inline";
            }
            if (saveButton != null && saveButton != undefined) {
                //get a reference to the custom tool and its parent element
                var customButton = oureditor.getToolByName("SaveContent").get_element();
                var parentCustomTool = customButton.parentNode;
                //insert the button before the custom button
                parentCustomTool.insertBefore(saveButton, customButton);
                //hide the custom button
                customButton.style.display = "none";
                saveButton.style.display = "inline";
            }
            if (cancelButton != null && cancelButton != undefined) {
                //get a reference to the custom tool and its parent element
                var customButton = oureditor.getToolByName("CancelContent").get_element();
                var parentCustomTool = customButton.parentNode;
                //insert the button before the custom button
                parentCustomTool.insertBefore(cancelButton, customButton);
                //hide the custom button
                customButton.style.display = "none";
                cancelButton.style.display = "inline";
            }
            var languagedropdown = oureditor.getToolByName("TranslateTool").get_element();
            languagedropdown.setAttribute("title", "<%$ Resx:JSMessages, PoweredByBing %>");
        }
    }, 500);
    CopyStylesToEditor(oureditor, Data.Container);
}
function copyParentStylesToEditor(editor) {
    var theIFrame = document.getElementById("RadEContentIframe" + editor.Id);
    var theMainEditorTable = document.getElementById("RadEWrapper" + editor.Id);

    var theParentEl = theMainEditorTable.parentNode.parentNode;
    var theContentArea = editor.get_contentArea();
    for (var attr in theParentEl.currentStyle) {
        theContentArea.style[attr] = theParentEl.currentStyle[attr];
    }
}

function CopyStylesToEditor(editor, container) {
    var theDocBody = editor.get_document().body; //Telerik: changed in new version
    var containerCSS = container.getAttribute("ViewCssClass");
    theDocBody.setAttribute("id", containerCSS);
    theDocBody.setAttribute("class", containerCSS);
}

function InvokeClick(controlId) {
    var Control = document.getElementById(controlId);
    if (browser == "ie") {
        Control.click();
    }
    else {
        var fireOnThis = Control;
        if (document.createEvent) {
            var evObj = document.createEvent('MouseEvents');
            evObj.initEvent('click', true, false);
            fireOnThis.dispatchEvent(evObj);
        }
        else if (document.createEventObject) {
            fireOnThis.fireEvent('onclick');
        }
    }
}

function querystringInNavigateUrl(navigateUrl, key) {
    var re = new RegExp('(?:\\?|&)' + key + '=(.*?)(?=&|$)', 'gi');
    var r = [], m;
    while ((m = re.exec(navigateUrl)) != null) r.push(m[1]);
    return r;
}

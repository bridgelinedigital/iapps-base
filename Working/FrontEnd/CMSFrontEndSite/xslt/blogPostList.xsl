<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:param name="Pager"></xsl:param>
	<xsl:param name="DisplayAuthorName"></xsl:param>
	<xsl:param name="DisplayPostDate"></xsl:param>
	<xsl:template match="/">

		<xsl:for-each select="//Records">
			<div class="blogItem">
				<h2>
					<a href="{./PostUrl}">
						<xsl:value-of select="./Title"/>
					</a>
				</h2>
				<xsl:if test="$DisplayAuthorName = 'true' or $DisplayPostDate = 'true'">
					<div class="postDate">
						<xsl:if test="$DisplayPostDate = 'true'">
							<xsl:call-template name="FormatDate">
								<xsl:with-param name="Date" select="./PostDate" />
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="$DisplayAuthorName = 'true'">
							<xsl:if test="$DisplayPostDate = 'true'"> | </xsl:if>
							<xsl:value-of select="./AuthorName"/>
						</xsl:if>
					</div>
				</xsl:if>
				<div class="content">
					<xsl:value-of select="./ShortDescription"/>
				</div>
				<xsl:if test="./AllowComments = 'true'">
					<a href="{./PostUrl}#comments">
						<xsl:value-of select="./NoOfComments"/> Comment(s) &gt;
					</a>
				</xsl:if>
			</div>
		</xsl:for-each>
		<div class="pagination">
			<xsl:value-of select="$Pager"/>
		</div>
	</xsl:template>

	<xsl:template name="FormatDate">
		<xsl:param name="Date" />
		<xsl:variable name="year">
			<xsl:value-of select="substring($Date,1,4)" />
		</xsl:variable>
		<xsl:variable name="month">
			<xsl:value-of select="substring($Date,6,2)" />
		</xsl:variable>
		<xsl:variable name="day">
			<xsl:value-of select="substring($Date,9,2)" />
		</xsl:variable>
		<xsl:value-of select="$day"/>
		<xsl:value-of select="' '"/>
		<xsl:choose>
			<xsl:when test="$month = '1' or $month = '01'">Jan</xsl:when>
			<xsl:when test="$month = '2' or $month = '02'">Feb</xsl:when>
			<xsl:when test="$month = '3' or $month = '03'">Mar</xsl:when>
			<xsl:when test="$month = '4' or $month = '04'">Apr</xsl:when>
			<xsl:when test="$month = '5' or $month = '05'">May</xsl:when>
			<xsl:when test="$month = '6' or $month = '06'">Jun</xsl:when>
			<xsl:when test="$month = '7' or $month = '07'">Jul</xsl:when>
			<xsl:when test="$month = '8' or $month = '08'">Aug</xsl:when>
			<xsl:when test="$month = '9' or $month = '09'">Sep</xsl:when>
			<xsl:when test="$month = '10'">Oct</xsl:when>
			<xsl:when test="$month = '11'">Nov</xsl:when>
			<xsl:when test="$month = '12'">Dec</xsl:when>
		</xsl:choose>
		<xsl:value-of select="' '"/>
		<xsl:value-of select="$year"/>
	</xsl:template>
</xsl:stylesheet>


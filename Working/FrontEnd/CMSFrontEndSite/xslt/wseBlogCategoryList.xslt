﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="Url"></xsl:param>
  <xsl:param name="SelectedId"></xsl:param>
  
  <xsl:template match="/">
    <xsl:if test="count(//Records) &gt; 0">
      <h3 class="filters-subHeading">Topics</h3>
      <ul class="filters-list truncateList">
        <xsl:for-each select="//Records">
          <li>
            <xsl:choose>
              <xsl:when test="./Id = $SelectedId">
                <xsl:value-of select="./Title"/>&#160;(<xsl:value-of select="./NoOfPosts"/>)
              </xsl:when>
              <xsl:otherwise>
                <a>
                  <xsl:attribute name="href">
                    <xsl:value-of select="./CompleteFriendlyUrl"/>
                  </xsl:attribute>
                  <xsl:value-of select="./Title"/>&#160;(<xsl:value-of select="./NoOfPosts"/>)
                </a>
              </xsl:otherwise>
            </xsl:choose>
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>

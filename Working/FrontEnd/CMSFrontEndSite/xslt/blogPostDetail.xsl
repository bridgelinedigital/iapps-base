<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:param name="DisplayAuthorName"></xsl:param>
	<xsl:param name="DisplayPostDate"></xsl:param>
	<xsl:template match="/">

		<xsl:for-each select="//Records">

			<xsl:variable name="h1tag">
				<xsl:value-of select="./H1Tag" />
			</xsl:variable>
			 
			<xsl:if test="$h1tag!=''">
				<h1>
					<xsl:value-of select="$h1tag"/>
				</h1> 
			</xsl:if>
			<xsl:if test="$h1tag=''">
				<h2>
					<xsl:value-of select="./Title"/>
				</h2>
			</xsl:if>
			<xsl:if test="$DisplayPostDate = 'true'">
				<div class="postDate">
					<xsl:call-template name="FormatDate">
						<xsl:with-param name="Date" select="./PostDate" />
					</xsl:call-template>
					<xsl:if test="$DisplayAuthorName = 'true'">
							| <xsl:value-of select="./AuthorName"/>
					</xsl:if>
				</div>
			</xsl:if>
			<div class="content">
				<xsl:value-of select="./Description"/>
			</div>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="FormatDate">
		<xsl:param name="Date" />
		<xsl:variable name="year">
			<xsl:value-of select="substring($Date,1,4)" />
		</xsl:variable>
		<xsl:variable name="month">
			<xsl:value-of select="substring($Date,6,2)" />
		</xsl:variable>
		<xsl:variable name="day">
			<xsl:value-of select="substring($Date,9,2)" />
		</xsl:variable>
		<xsl:value-of select="$day"/>
		<xsl:value-of select="' '"/>
		<xsl:choose>
			<xsl:when test="$month = '1' or $month = '01'">Jan</xsl:when>
			<xsl:when test="$month = '2' or $month = '02'">Feb</xsl:when>
			<xsl:when test="$month = '3' or $month = '03'">Mar</xsl:when>
			<xsl:when test="$month = '4' or $month = '04'">Apr</xsl:when>
			<xsl:when test="$month = '5' or $month = '05'">May</xsl:when>
			<xsl:when test="$month = '6' or $month = '06'">Jun</xsl:when>
			<xsl:when test="$month = '7' or $month = '07'">Jul</xsl:when>
			<xsl:when test="$month = '8' or $month = '08'">Aug</xsl:when>
			<xsl:when test="$month = '9' or $month = '09'">Sep</xsl:when>
			<xsl:when test="$month = '10'">Oct</xsl:when>
			<xsl:when test="$month = '11'">Nov</xsl:when>
			<xsl:when test="$month = '12'">Dec</xsl:when>
		</xsl:choose>
		<xsl:value-of select="' '"/>
		<xsl:value-of select="$year"/>
	</xsl:template>
</xsl:stylesheet>


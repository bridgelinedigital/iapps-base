<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform" extension-element-prefixes="str" xmlns:str="http://exslt.org/strings"  xmlns:FWControls="remove"
     xmlns:msxsl="urn:schemas-microsoft-com:xslt"
xmlns:js="urn:custom-javascript"
exclude-result-prefixes="msxsl js"
>
  <xsl:param name="CaptchaImageUrl"></xsl:param>
  <xsl:param name="AllowComments"></xsl:param>
  <xsl:param name="ShowUserInfo"></xsl:param>
  <xsl:param name="ApproveComments"></xsl:param>
  <xsl:param name="AllowRating"></xsl:param>
  <xsl:param name="AllowToReply"></xsl:param>
  <xsl:param name="ListExistingComments"></xsl:param>
  <xsl:param name="AllowPaging"></xsl:param>
  <xsl:param name="AllowSorting"></xsl:param>
  <xsl:param name="Pager"></xsl:param>
  <xsl:param name="SortOrder"></xsl:param>
  <xsl:template match="/">
    <xsl:if test="$ListExistingComments = 'true'">
      <div class="commentsContent">
        <a name="comments">
          <xsl:comment>Comments Bookmark</xsl:comment>
        </a>
        <xsl:if test="count(//Records) &gt; 0">
          <h3>Comments</h3>
          <xsl:if test="$AllowPaging = 'true' or $AllowSorting = 'true'">
            <div class="commentsNav">
              <xsl:if test="$AllowSorting = 'true'">
                <div class="commentsSort">
                  <select onchange="return sortCommentsPage(this.value);">
                    <xsl:choose>
                      <xsl:when test="$SortOrder = 'asc'">
                        <option value="DESC">Latest to Oldest</option>
                        <option value="ASC" selected="true">Oldest to Latest</option>
                      </xsl:when>
                      <xsl:otherwise>
                        <option value="DESC" selected="true">Latest to Oldest</option>
                        <option value="ASC">Oldest to Latest</option>
                      </xsl:otherwise>
                    </xsl:choose>
                  </select>
                </div>
              </xsl:if>
              <xsl:if test="$AllowPaging = 'true'">
                <div class="commentsPaging">
                  <xsl:value-of select="$Pager"/>
                </div>
              </xsl:if>
              <div class="clearFix">
                <xsl:comment>Clear float</xsl:comment>
              </div>
            </div>
          </xsl:if>

          <xsl:for-each select="//Records">
            <xsl:variable name="Style">
              <xsl:value-of select="concat('padding-left:',./Level * 20, 'px;')"/>
            </xsl:variable>
            <xsl:variable name="className">
              <xsl:if test="position() mod 2 = 1">
                <xsl:value-of select="'altRow'"/>
              </xsl:if>
            </xsl:variable>

            <xsl:variable name="ratingsImagesPath">
              <xsl:value-of  select="./RatingsImagePathInSequence"/>
            </xsl:variable>

            <div class="commentItem {$className}">
              <xsl:if test="./Level != '0'">
                <xsl:attribute name="style">
                  <xsl:value-of select="$Style"/>
                </xsl:attribute>
              </xsl:if>
              <div class="date">
                <xsl:call-template name="FormatDate">
                  <xsl:with-param name="Date" select="./CreatedDate" />
                </xsl:call-template>
                <xsl:if test="./PostedBy != ''">
                  |
                </xsl:if>
                <xsl:value-of select="./PostedBy"/>
                <xsl:if test="./RatingValue != ''">
                  |
                </xsl:if>
              </div>

              <div class="content">
                <xsl:value-of select="./Comments"/>
              </div>

              <div>
                <p>
                  Review Rating:
                  <xsl:value-of select="./RatingValue"/>
                </p>
                <xsl:call-template name="splitAndCreateRatingImages">
                  <xsl:with-param name="str" select="$ratingsImagesPath"/>
                </xsl:call-template>
              </div>

              <xsl:if test="$AllowComments = 'true'">
                <xsl:if test="$AllowToReply = 'true'">
                  <a href="#commentForm">
                    <xsl:attribute name="onclick">
                      <xsl:variable name="focusControlId">
                        <xsl:choose>
                          <xsl:when test="$ShowUserInfo = 'true'">
                            <xsl:value-of select="'txtName'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'txtComment'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>
                      replyToComment('txtParent','<xsl:value-of select="./Id"/>');window.location.href = '#commentForm';document.getElementById('<xsl:value-of select="$focusControlId"/>').focus();return false;
                    </xsl:attribute>
                    Reply
                  </a>
                </xsl:if>
              </xsl:if>
            </div>
          </xsl:for-each>
        </xsl:if>
      </div>
    </xsl:if>

    <xsl:if test="$AllowComments = 'true'">
      <div class="commentsForm">
        <a name="commentForm">
          <xsl:comment>Comments form Bookmark</xsl:comment>
        </a>
        <table class="formComment">
          <tr>
            <th colspan="2">Leave Comment</th>
          </tr>
          <xsl:if test="$ShowUserInfo = 'true'">
            <tr>
              <td class="col1">
                Name<span class="required">*</span>
              </td>
              <td class="col2">
                <input class="text" type="text" id="txtName" />
              </td>
            </tr>

            <tr>
              <td class="col1">
                Email<span class="required">*</span><br/>
                <span class="fieldInfo">(For verification purposes only)</span>
              </td>
              <td class="col2">
                <input class="text"  type="text" id="txtEmail" />
              </td>
            </tr>
          </xsl:if>
          <tr>
            <td colspan="2" id="td_rating" align="right">
            </td>
          </tr>
          <tr>
            <td class="col1">
              Comment<span class="required">*</span>
            </td>
            <td class="col2">
              <textarea id="txtComment" onfocus="return initCommentBox(this);">
                <xsl:text> </xsl:text>
              </textarea>
            </td>
          </tr>
          <xsl:if test="$ShowUserInfo = 'true'">
            <tr>
              <td class="col1">
                Enter the text shown in this image:<span class="required">*</span>
                <span class="fieldInfo">(Input is case sensitive)</span>
              </td>
              <td class="col2 captcha">
                <img>
                  <xsl:attribute name="src">
                    <xsl:value-of select="$CaptchaImageUrl"/>
                  </xsl:attribute>
                </img>
                <br />
                <input class="text1 text2" type="text" id="txtCaptcha" />
              </td>
            </tr>
          </xsl:if>
          <tr>
            <td colspan="2">
              <input type="hidden" id="txtParent" />
              <input class="btnComment"  type="submit" value="Add Comment" onclick="return AddComment('txtName','txtEmail','txtComment','txtCaptcha', 'txtParent');" />
            </td>
          </tr>
        </table>
        <xsl:if test="$ApproveComments = 'true'">
          <div class="commentsMessage">
            * - Only comments approved by post author will be displayed here.
          </div>
        </xsl:if>
      </div>
    </xsl:if>
  </xsl:template>


  <!-- recursive template for splitting links (url|url) -->
  <xsl:template name="splitAndCreateRatingImages">
    <xsl:param name="str"/>
    <xsl:choose>
      <xsl:when test="contains($str,'|')">
        <img>
          <xsl:attribute name="src">
            <xsl:value-of  select="substring-before($str,'|')"/>
          </xsl:attribute>
        </img>
        <xsl:call-template name="splitAndCreateRatingImages">
          <xsl:with-param name="str"
  select="substring-after($str,'|')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <img>
          <xsl:attribute name="src">
            <xsl:value-of  select="$str"/>
          </xsl:attribute>
        </img>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date" />
    <xsl:variable name="year">
      <xsl:value-of select="substring($Date,1,4)" />
    </xsl:variable>
    <xsl:variable name="month">
      <xsl:value-of select="substring($Date,6,2)" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring($Date,9,2)" />
    </xsl:variable>
    <xsl:value-of select="$day"/>
    <xsl:value-of select="' '"/>
    <xsl:choose>
      <xsl:when test="$month = '1' or $month = '01'">Jan</xsl:when>
      <xsl:when test="$month = '2' or $month = '02'">Feb</xsl:when>
      <xsl:when test="$month = '3' or $month = '03'">Mar</xsl:when>
      <xsl:when test="$month = '4' or $month = '04'">Apr</xsl:when>
      <xsl:when test="$month = '5' or $month = '05'">May</xsl:when>
      <xsl:when test="$month = '6' or $month = '06'">Jun</xsl:when>
      <xsl:when test="$month = '7' or $month = '07'">Jul</xsl:when>
      <xsl:when test="$month = '8' or $month = '08'">Aug</xsl:when>
      <xsl:when test="$month = '9' or $month = '09'">Sep</xsl:when>
      <xsl:when test="$month = '10'">Oct</xsl:when>
      <xsl:when test="$month = '11'">Nov</xsl:when>
      <xsl:when test="$month = '12'">Dec</xsl:when>
    </xsl:choose>
    <xsl:value-of select="' '"/>
    <xsl:value-of select="$year"/>
  </xsl:template>
</xsl:stylesheet>


<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="SubmitButton"></xsl:param>
  <xsl:param name="RenderResultLink"></xsl:param>
  <xsl:param name="ViewResultsLink"></xsl:param>
  <xsl:param name="RenderRequiredIndicator"></xsl:param>
  <xsl:template match="/">

    <div class="formBody">
      <xsl:for-each select="//FormElement">
        <div class="{@class}">
          <xsl:if test="@type != 'HiddenField'">
            <span class="labelItems">
              <xsl:value-of select="@label"/>
              <xsl:if test="$RenderRequiredIndicator = 'true'">
                <xsl:if test="./Properties/Attributes/Attribute[@name='Required']/@selectedValue = 'True'">
                  <label>*</label>
                </xsl:if>
              </xsl:if>
            </span>
          </xsl:if>
          <xsl:value-of select="./Properties/ControlTag"/>
        </div>
      </xsl:for-each>
      <div class="clearFix">&#160;</div>
    </div>
    <div class="formFooter">
      <div class="formSubmit">
        <xsl:value-of select="$SubmitButton"/>
        <xsl:if test="$RenderResultLink = 'true'">
          &#160;<xsl:value-of select="$ViewResultsLink"/>
        </xsl:if>
      </div>
    </div>
  </xsl:template>

</xsl:stylesheet>


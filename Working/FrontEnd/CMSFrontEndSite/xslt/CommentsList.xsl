<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" extension-element-prefixes="str" xmlns:str="http://exslt.org/strings"  >
  <xsl:param name="AllowComments"></xsl:param>
  <xsl:param name="AllowRating"></xsl:param>
  <xsl:param name="ShowUserInfo"></xsl:param>
  <xsl:param name="ApproveComments"></xsl:param>
  <xsl:param name="AllowPaging"></xsl:param>
  <xsl:param name="AllowSorting"></xsl:param>
  <xsl:param name="Pager"></xsl:param>
  <xsl:param name="SortOrder"></xsl:param>
  <xsl:param name="ReCaptchaSiteKey"></xsl:param>

  <xsl:template match="/">
      <a name="comments">
        <xsl:comment>Comments Bookmark</xsl:comment>
      </a>
      <xsl:if test="count(//Records) &gt; 0">
        <h3 class="h-underline h-softSmBottom">Comments</h3>
        <xsl:if test="$AllowPaging = 'true' or $AllowSorting = 'true'">
          <div class="commentsNav">
            <xsl:if test="$AllowSorting = 'true'">
              <div class="commentsSort">
                <select onchange="return sortCommentsPage(this.value);">
                  <xsl:choose>
                    <xsl:when test="$SortOrder = 'asc'">
                      <option value="DESC">Latest to Oldest</option>
                      <option value="ASC" selected="true">Oldest to Latest</option>
                    </xsl:when>
                    <xsl:otherwise>
                      <option value="DESC" selected="true">Latest to Oldest</option>
                      <option value="ASC">Oldest to Latest</option>
                    </xsl:otherwise>
                  </xsl:choose>
                </select>
              </div>
            </xsl:if>
            <xsl:if test="$AllowPaging = 'true'">
              <div class="commentsPaging">
                <xsl:value-of select="$Pager"/>
              </div>
            </xsl:if>
            <div class="clearFix">
              <xsl:comment>Clear float</xsl:comment>
            </div>
          </div>
        </xsl:if>
        <xsl:for-each select="//Records">

          <xsl:variable name="ratingsImagesPath">
            <xsl:value-of  select="./RatingsImagePathInSequence"/>
          </xsl:variable>

          <div>
            <xsl:choose>
              <xsl:when test="./Level != '0'">
                <xsl:attribute name="class">
                  <xsl:value-of select="'comment comment-reply'"/>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="class">
                  <xsl:value-of select="'comment'"/>
                </xsl:attribute>              
              </xsl:otherwise>
            
            </xsl:choose>
            
            <p class="comment-byline">
              <xsl:call-template name="FormatDate">
                <xsl:with-param name="Date" select="./CreatedDate" />
              </xsl:call-template>
              <xsl:if test="./PostedBy != ''">
                |
              </xsl:if>
              <xsl:value-of select="./PostedBy"/>
              <xsl:if test="./RatingValue != ''">
                |
              </xsl:if>
            </p>

            <xsl:if test="$AllowRating = 'true'">
              Review Rating:
              <div>
                <xsl:call-template name="splitAndCreateRatingImages">
                  <xsl:with-param name="str" select="$ratingsImagesPath"/>
                </xsl:call-template>
              </div>
            </xsl:if>

            <p>
              <xsl:value-of select="./Comments"/>
            </p>

            <xsl:if test="$AllowComments = 'true'">
              <p>
                <a class="comment-replyLink" href="#commentForm">
                <xsl:attribute name="onclick">
                  <xsl:variable name="focusControlId">
                    <xsl:choose>
                      <xsl:when test="$ShowUserInfo = 'true'">
                        <xsl:value-of select="'txtName'"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="'txtComment'"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:variable>
                  replyToComment('txtParent','<xsl:value-of select="./Id"/>');window.location.href = '#commentForm';document.getElementById('<xsl:value-of select="$focusControlId"/>').focus();return false;
                </xsl:attribute>
                Reply
              </a>
              </p>
            </xsl:if>
          </div>
        </xsl:for-each>
      </xsl:if>
    
    <xsl:if test="$AllowComments = 'true'">
      <hr />
      <div class="islet fill--greyLightest">
        <h3 class="h-underline h-softSmBottom">Leave a Comment</h3>
        <div class="row">
          <div class="column med-12">
            <label for="txtName">Name</label>
            <input type="text" id="txtName" />
          </div>
          <div class="formRow med-12">
            <label for="txtEmail">Email <em>(For verification purposes only)</em></label>
            <input class="formTextBox"  type="text" id="txtEmail" />
          </div>
          <div class="formRow">
            <label for="txtComment"> Comment</label>
            <textarea id="txtComment" class="formTextarea" onfocus="return initCommentBox(this);">
              <xsl:text> </xsl:text>
            </textarea>  
          </div>
          <div class="formRow">
            <label for="g-recaptcha-response">CAPTCHA</label>
            <div id="comments-recaptcha" class="g-recaptcha" data-sitekey="{$ReCaptchaSiteKey}">
              <xsl:text> </xsl:text>
            </div>
          </div>
        </div>
        <div class="formFooter">
          <div class="formSubmit">
            <input type="hidden" id="txtParent" />
            <input class="btn" type="submit" value="Submit" onclick="return AddComment('txtName','txtEmail','txtComment','txtCaptcha', 'txtParent');" />  
            <em class="h-textSm h-colorGrey">Only comments approved by post author will be displayed</em> 
          </div>
        </div>
        
      </div>
    </xsl:if>
  </xsl:template>


  <!-- recursive template for splitting links (url|url) -->
  <xsl:template name="splitAndCreateRatingImages">
    <xsl:param name="str"/>
    <xsl:choose>
      <xsl:when test="contains($str,'|')">
        <img>
          <xsl:attribute name="src">
            <xsl:value-of  select="substring-before($str,'|')"/>
          </xsl:attribute>
        </img>
        <xsl:call-template name="splitAndCreateRatingImages">
          <xsl:with-param name="str"
  select="substring-after($str,'|')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <img>
          <xsl:attribute name="src">
            <xsl:value-of  select="$str"/>
          </xsl:attribute>
        </img>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date" />
    <xsl:variable name="year">
      <xsl:value-of select="substring($Date,1,4)" />
    </xsl:variable>
    <xsl:variable name="month">
      <xsl:value-of select="substring($Date,6,2)" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring($Date,9,2)" />
    </xsl:variable>
    <xsl:value-of select="$day"/>
    <xsl:value-of select="' '"/>
    <xsl:choose>
      <xsl:when test="$month = '1' or $month = '01'">Jan</xsl:when>
      <xsl:when test="$month = '2' or $month = '02'">Feb</xsl:when>
      <xsl:when test="$month = '3' or $month = '03'">Mar</xsl:when>
      <xsl:when test="$month = '4' or $month = '04'">Apr</xsl:when>
      <xsl:when test="$month = '5' or $month = '05'">May</xsl:when>
      <xsl:when test="$month = '6' or $month = '06'">Jun</xsl:when>
      <xsl:when test="$month = '7' or $month = '07'">Jul</xsl:when>
      <xsl:when test="$month = '8' or $month = '08'">Aug</xsl:when>
      <xsl:when test="$month = '9' or $month = '09'">Sep</xsl:when>
      <xsl:when test="$month = '10'">Oct</xsl:when>
      <xsl:when test="$month = '11'">Nov</xsl:when>
      <xsl:when test="$month = '12'">Dec</xsl:when>
    </xsl:choose>
    <xsl:value-of select="' '"/>
    <xsl:value-of select="$year"/>
  </xsl:template>
</xsl:stylesheet>


﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">

  <xsl:output method="html" indent="yes"/>
  <xsl:param name="validationrules"></xsl:param>
  <xsl:param name="siteUrl"></xsl:param>
  <xsl:param name="submitButtonText"></xsl:param>
  <xsl:param name="leadFormProperties"></xsl:param>
  <xsl:param name="thankYouContent"></xsl:param>
  <xsl:template match="/">
    <style type="text/css">
      label.error {
      color: red;
      font-style: italic;
      }
    </style>
    <!--<form id="ContactForm" >-->
    <!--<div class="modal fade bs-example-modal-sm center-block" tabindex="-1" role="dialog" id="loading">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          
          <img src="{$siteUrl}/images/ajax-loader.gif" style="padding-left:220px"/>
        </div>
      </div>
    </div>-->
    <div id="loading" style="display: none;">
      <div id="gridBody">
        <div class="resultRow" style="text-align: center;">
          <img src="{$siteUrl}/iapps_images/ajax-loader.gif" alt="Please wait..." />
        </div>
      </div>
    </div>

    <div>
      <input type="hidden" name ="FormId" id="FormId" value="{LeadForm/FormId}"></input>

      <xsl:for-each select="//Fields/LeadFormField">


        <div >

          <!--For Text Box-->
          <xsl:if test="ControlType='TextBox'">
            <label class="control-label">
              <xsl:value-of select="Label"/>
            </label>
            <input name="{Id}"  type="{Validation/type}" id="{Id}" >
              <xsl:call-template name="requiredValidation" />
              <xsl:call-template name="minmaxLengthValidation" />
              <xsl:call-template name="customValidation" />
            </input>
          </xsl:if>


          <!--For Text Area-->
          <xsl:if test="ControlType='TextArea'">
            <label class="control-label">
              <xsl:value-of select="Label"/>
            </label>
            <textarea name="{Id}"  id="{Id}" >
              <xsl:call-template name="requiredValidation" />
              <xsl:call-template name="minmaxLengthValidation" />

            </textarea>
          </xsl:if>

          <!--For Drop Down-->
          <xsl:if test="ControlType='DropdownList'">
            <label class="control-label">
              <xsl:value-of select="Label"/>
            </label>

            <select id="{Id}" name="{Id}" >
              <xsl:call-template name="requiredValidation" />

              <xsl:for-each select="Values/FieldValue">
                <option value="{Key}">
                  <xsl:value-of select="Value"/>
                </option>
              </xsl:for-each>
            </select>
          </xsl:if>

          <!--For Check Box-->
          <xsl:if test="ControlType='CheckBox'">
            <label class="control-label">
              <xsl:value-of select="Label"/>
            </label>
            <input type="checkbox" name="{Id}" id="{Id}" ></input>
          </xsl:if>


          <!--For multi select-->
          <xsl:if test="ControlType='CheckBoxList'">
            <label class="control-label">
              <xsl:value-of select="Label"/>
            </label>
            <xsl:for-each select="Values/FieldValue">
              <input type="checkbox" name="{../../Id}"  id="{Key}" value="{Value}" >
                <xsl:if test="position() = 1">
                  <xsl:call-template name="requiredValidationForList" />
                </xsl:if>
                <xsl:value-of select="Value"/>
              </input>
            </xsl:for-each>
            <br/>
            <label for="{Label}" class="error" style="display: inline;"></label>
          </xsl:if>


          <!-- For Radio button list -->
          <xsl:if test="ControlType='RadioButtonList'">
            <label class="control-label">
              <xsl:value-of select="Label"/>
            </label>
            <xsl:for-each select="Values/FieldValue">
              <input type="radio" name="{../../Id}" id="{Key}" value="{Value}" >
                <xsl:if test="position() = 1">
                  <xsl:call-template name="requiredValidationForList" />
                </xsl:if>
                <xsl:value-of select="Value"/>
              </input>
            </xsl:for-each>
            <br/>
            <label for="{Label}" class="error" style="display: inline;"></label>
          </xsl:if>
          <!-- For Email Opt-in-->
          <xsl:if test="ControlType='EmailOptIn'">
            <xsl:element name="input">
              <xsl:attribute name="type">checkbox</xsl:attribute>
              <xsl:attribute name="name"><xsl:value-of select="Id"/></xsl:attribute>
              <xsl:attribute name="id"><xsl:value-of select="Id"/></xsl:attribute>
              <xsl:attribute name="value">true</xsl:attribute>
              <xsl:if test="DefaultValue='true'">
                <xsl:attribute name="checked">checked</xsl:attribute>
              </xsl:if>
            </xsl:element>
              <label class="control-label">
                <xsl:value-of select="Label"/>
              </label>
              <br/>
              <label for="{Label}" class="error" style="display: inline;"></label>
            </xsl:if>
        </div>
      </xsl:for-each>
      <div class="clearFix">&#160;</div>
    </div>

    <div class="formFooter">
      <div class="formSubmit">
        <input id="btnSubmit" type="Submit" value="{$submitButtonText}" ></input>
      </div>
    </div>
    <!--</form>-->

    <div id="thankYouContent_{LeadForm/FormId}" style="display:none;">
      <xsl:value-of select="$thankYouContent"/>
    </div>

    <script>
      <xsl:value-of select="$leadFormProperties"/>
    </script>
  </xsl:template>

  <!-- Required field validation with custom message-->
  <xsl:template name="requiredValidation" >
    <xsl:if test="Validation/required='true'">
      <xsl:attribute name="required">
        true
      </xsl:attribute>
      <xsl:attribute name="data-msg-required">
        <xsl:value-of select="Title"/> is required
      </xsl:attribute>
    </xsl:if>
  </xsl:template>

  <xsl:template name="requiredValidationForList" >
    <xsl:if test="../../Validation/required='true'">
      <xsl:attribute name="required">
        true
      </xsl:attribute>
      <xsl:attribute name="data-msg-required">
        <xsl:value-of select="../../Title"/> is required
      </xsl:attribute>
    </xsl:if>
  </xsl:template>

  <!-- Min and Max length validation -->
  <xsl:template name="minmaxLengthValidation" >
    <xsl:if test="Validation/type='number' and Validation/minlength > 0" >
      <xsl:attribute name="min">
        <xsl:value-of select="Validation/minlength"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="Validation/type='number' and Validation/maxlength > 0" >
      <xsl:attribute name="max">
        <xsl:value-of select="Validation/maxlength"/>
      </xsl:attribute>
    </xsl:if>
  </xsl:template>

  <!-- custom field validation with custom message-->
  <xsl:template name="customValidation" >
    <xsl:if test="Validation/customvalidationtype!=''">
      <xsl:attribute name="data-rule-{Validation/customvalidationtype}">
        true
      </xsl:attribute>
      <xsl:attribute name="data-msg-{Validation/customvalidationtype}">
        Please enter valid <xsl:value-of select="Title"/>
      </xsl:attribute>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>

﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" extension-element-prefixes="str" xmlns:str="http://exslt.org/strings"  xmlns:FWControls="remove"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:js="urn:custom-javascript" exclude-result-prefixes="msxsl js">
  <xsl:param name="AllowComments"></xsl:param>
  <xsl:param name="ShowUserInfo"></xsl:param>
  <xsl:param name="ApproveComments"></xsl:param>
  <xsl:param name="AllowRating"></xsl:param>
  <xsl:param name="AllowToReply"></xsl:param>
  <xsl:param name="ListExistingComments"></xsl:param>
  <xsl:param name="ReCaptchaSiteKey"></xsl:param>

  <xsl:template match="/">

    <!--
    <textarea rows="10" cols="75">
      <xsl:copy-of select="*"/>
    </textarea>
-->
    <div class="sideToSideSm h-pushSmBottom h-softSmBottom h-underline">
      <h2>Reviews</h2>
      <xsl:if test="$AllowComments = 'true'">
        <p>
          <a name="reviews" class="btn toggleLink" data-text-swap="Cancel">Write a review</a>
        </p>
      </xsl:if>
    </div>

    <xsl:if test="$AllowComments = 'true'">
      <div class="toggleLinkTarget is-hidden islet h-fillColorGreyLightest h-pushBottom">
        <div class="row">
          <div class="column med-12">
            <label for="txtName">Name</label>
            <input type="text" id="txtName" />
          </div>

          <div class="column med-12">
            <label for="txtEmail">Email</label>
            <input type="text" id="txtEmail" />
          </div>

          <div class="column">
            <label for="txtComment">Review</label>
            <textarea id="txtComment" placeholder="Write your review" onfocus="return initCommentBox(this);">
              <xsl:text> </xsl:text>
            </textarea>
          </div>
        </div>

        <div class="row">
          <div class="column">
            <table>
              <tr>
                <td colspan="2" id="td_rating" align="right">
                </td>
              </tr>
            </table>
            <text> </text>
          </div>
        </div>

        <div class="row">
          <div class="column">
            <label for="g-recaptcha-response">CAPTCHA</label>
            <div id="comments-recaptcha" class="g-recaptcha" data-sitekey="{$ReCaptchaSiteKey}">
              <xsl:text> </xsl:text>
            </div>
          </div>
        </div>

        <div class="formFooter">
          <input type="hidden" id="txtParent" />
          <input name="" type="submit" value="Submit" disableValidation="true" onclick="return AddComment('txtName','txtEmail','txtComment','txtCaptcha', 'txtParent');" />
        </div>
      </div>
    </xsl:if>

    <xsl:if test="$ListExistingComments = 'true'">

      <xsl:for-each select="//Records">
        <xsl:variable name="ratingsImagesPath">
          <xsl:value-of  select="./RatingsImagePathInSequence"/>
        </xsl:variable>
        <div class="productRating" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
          <div class="productRating-title">
            <span class="rating" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
              <meta itemprop="worstRating" content = "1" />
              <span class="h-visuallyHidden">
                <span itemprop="ratingValue">
                  <xsl:value-of select="./RatingValue"/>
                </span> rating
              </span>
              <meta itemprop="bestRating" content="5" />
              <xsl:call-template name="splitAndCreateRatingImages">
                <xsl:with-param name="str" select="$ratingsImagesPath"/>
              </xsl:call-template>
            </span>
          </div>
          <ul class="infoList productRating-infoList">
            <li>
              <meta itemprop="datePublished">
                <xsl:attribute name="content">
                  <xsl:value-of select="./CreatedDate"/>
                </xsl:attribute>
              </meta>
              <xsl:call-template name="FormatDate">
                <xsl:with-param name="Date" select="./CreatedDate" />
              </xsl:call-template>
            </li>
            <li itemprop="author">
              <xsl:value-of select="./PostedBy"/>
            </li>
          </ul>
          <div itemprop="description">
            <p>
              <xsl:value-of select="./Comments"/>
            </p>
          </div>
        </div>
      </xsl:for-each>
    </xsl:if>

  </xsl:template>


  <!-- recursive template for splitting links (url|url) -->
  <xsl:template name="splitAndCreateRatingImages">
    <xsl:param name="str"/>
    <xsl:choose>
      <xsl:when test="contains($str,'|')">
        <xsl:variable name="imgPath">
          <xsl:value-of  select="substring-before($str,'|')"/>
        </xsl:variable>
        <xsl:text>&lt;i class="</xsl:text>
        <xsl:choose>
          <xsl:when test="$imgPath = '/iapps_images/fullstar.png'">icon-star</xsl:when>
          <xsl:when test="$imgPath = '/iapps_images/halfstar.png'">icon-star-half-alt</xsl:when>
          <xsl:otherwise>icon-star-alt</xsl:otherwise>
        </xsl:choose>
        <xsl:text>"&gt;&lt;/i&gt;</xsl:text>
        <xsl:call-template name="splitAndCreateRatingImages">
          <xsl:with-param name="str" select="substring-after($str,'|')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>&lt;i class="</xsl:text>
        <xsl:choose>
          <xsl:when test="$str = '/iapps_images/fullstar.png'">icon-star</xsl:when>
          <xsl:when test="$str = '/iapps_images/halfstar.png'">icon-star-half-alt</xsl:when>
          <xsl:otherwise>icon-star-alt</xsl:otherwise>
        </xsl:choose>
        <xsl:text>"&gt;&lt;/i&gt;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date" />
    <xsl:variable name="year">
      <xsl:value-of select="substring($Date,1,4)" />
    </xsl:variable>
    <xsl:variable name="month">
      <xsl:value-of select="substring($Date,6,2)" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring($Date,9,2)" />
    </xsl:variable>
    <xsl:value-of select="$month"/>
    <xsl:value-of select="'/'"/>
    <xsl:value-of select="$day"/>
    <xsl:value-of select="'/'"/>
    <xsl:value-of select="$year"/>
  </xsl:template>
</xsl:stylesheet>

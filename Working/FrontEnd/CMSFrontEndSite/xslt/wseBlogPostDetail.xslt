﻿<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="DisplayAuthorName"></xsl:param>
  <xsl:param name="DisplayPostDate"></xsl:param>
  
  <xsl:template match="/">
    <xsl:for-each select="//Records">
      <xsl:variable name="postImage">
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="./ImageUrl" />
        </xsl:call-template>
      </xsl:variable>
      <ul class="infoList">
        <xsl:if test="$DisplayPostDate = 'true'">
          <li>
            <xsl:call-template name="FormatDate">
              <xsl:with-param name="Date" select="./PostDate" />
            </xsl:call-template>
          </li>
        </xsl:if>
        <xsl:if test="$DisplayAuthorName = 'true'">
          <li>
            <xsl:value-of select="./AuthorName"/>
          </li>
        </xsl:if>
        <xsl:if test="./AllowComments = 'true'">
          <li>
            <a href="{./PostUrl}#comments">
              Comments (<xsl:value-of select="./NoOfComments"/>)
            </a>
          </li>
        </xsl:if>
      </ul>
      <h1>
        <xsl:value-of select="./Title"/>
      </h1>
      <xsl:if test="$postImage != ''">
        <figure class="figure--right">
            <img src="{$postImage}" alt="" />
        </figure>
      </xsl:if>
      <h2 class="subhead">
        <xsl:value-of select="./ShortDescription"/>
      </h2>
      <xsl:value-of select="./Description"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date" />
    <xsl:variable name="year">
      <xsl:value-of select="substring($Date,1,4)" />
    </xsl:variable>
    <xsl:variable name="month">
      <xsl:value-of select="substring($Date,6,2)" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring($Date,9,2)" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$month = '1' or $month = '01'">Jan</xsl:when>
      <xsl:when test="$month = '2' or $month = '02'">Feb</xsl:when>
      <xsl:when test="$month = '3' or $month = '03'">Mar</xsl:when>
      <xsl:when test="$month = '4' or $month = '04'">Apr</xsl:when>
      <xsl:when test="$month = '5' or $month = '05'">May</xsl:when>
      <xsl:when test="$month = '6' or $month = '06'">Jun</xsl:when>
      <xsl:when test="$month = '7' or $month = '07'">Jul</xsl:when>
      <xsl:when test="$month = '8' or $month = '08'">Aug</xsl:when>
      <xsl:when test="$month = '9' or $month = '09'">Sep</xsl:when>
      <xsl:when test="$month = '10'">Oct</xsl:when>
      <xsl:when test="$month = '11'">Nov</xsl:when>
      <xsl:when test="$month = '12'">Dec</xsl:when>
    </xsl:choose>
    <xsl:value-of select="' '"/>
    <xsl:value-of select="$day"/>
    <xsl:value-of select="', '"/>
    <xsl:value-of select="$year"/>
  </xsl:template>

  <xsl:template name="Replace">
    <xsl:param name="text" />
    <xsl:param name="replace" select="' '"/>
    <xsl:param name="by" select="'%20'"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="Replace">
          <xsl:with-param name="text" select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>


﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" extension-element-prefixes="str" xmlns:str="http://exslt.org/strings"  xmlns:FWControls="remove"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:js="urn:custom-javascript" exclude-result-prefixes="msxsl js">
  <xsl:param name="CaptchaImageUrl"></xsl:param>
  <xsl:param name="AllowComments"></xsl:param>
  <xsl:param name="ShowUserInfo"></xsl:param>
  <xsl:param name="ApproveComments"></xsl:param>
  <xsl:param name="AllowRating"></xsl:param>
  <xsl:param name="AllowToReply"></xsl:param>
  <xsl:param name="ListExistingComments"></xsl:param>
  <xsl:template match="/">

    <!--
    <textarea rows="10" cols="75">
      <xsl:copy-of select="*"/>
    </textarea>
-->
    <xsl:if test="$AllowComments = 'true'">
      <div class="row">
        <div class="column">
          <p><a class="btn toggleLink" data-text-swap="Cancel">Write a review</a></p>
          <div class="toggleLinkTarget is-hidden island island--grey h-pushBottom">
            <div class="row">
              <div class="column med-12">
                <label for="txtName">Name</label>
                <input type="text" id="txtName" />
              </div>
              <div class="column med-12">
                <label for="txtEmail">Email</label>
                <input type="text" id="txtEmail" />
              </div>
            </div>
            <div class="row">
              <!--
              <div class="column">
                <label for="txtTitle">Review Title</label>
                <input id="txtTitle" type="text" />
              </div>
              -->
              <div class="column">
                <label for="txtComment">Review</label>
                <textarea id="txtComment" placeholder="Write your review" onfocus="return initCommentBox(this);">
                  <xsl:text> </xsl:text>
                </textarea>
              </div>
            </div>
            <xsl:if test="$AllowRating = 'true'">
              <div class="row">
                <div class="column">
                  <div id="td_rating"/>
                </div>
              </div>
            </xsl:if>
            <div class="row">
              <div class="column">
                <img>
                  <xsl:attribute name="src">
                    <xsl:value-of select="$CaptchaImageUrl"/>
                  </xsl:attribute>
                </img>
              </div>
            </div>
            <div class="row">
              <div class="column">
                <label for="txtCaptcha">Enter the text above</label>
                <input type="text" id="txtCaptcha" />
              </div>
            </div>
            <p class="h-pushBottom">
              <input type="hidden" id="txtParent" />
              <input name="" type="submit" value="Submit comment" disableValidation="true" onclick="return AddComment('txtName','txtEmail','txtComment','txtCaptcha', 'txtParent');" />
            </p>
          </div>
        </div>
      </div>
    </xsl:if>

    <xsl:if test="$ListExistingComments = 'true'">
      
        <xsl:for-each select="//Records">
          <xsl:variable name="ratingsImagesPath">
            <xsl:value-of  select="./RatingsImagePathInSequence"/>
          </xsl:variable>
          <div class="productRating">
            <div class="productRating-title">
              <!--<span class="productRating-rating">
                <i class="icon-star"></i>
                <i class="icon-star"></i>
                <i class="icon-star-half-alt"></i>
                <i class="icon-star-empty"></i>
                <i class="icon-star-empty"></i>
                <xsl:call-template name="splitAndCreateRatingImages">
                  <xsl:with-param name="str" select="$ratingsImagesPath"/>
                </xsl:call-template>
              </span>-->
            </div>
            <div class="productRating-date">
              <xsl:call-template name="FormatDate">
                <xsl:with-param name="Date" select="./CreatedDate" />
              </xsl:call-template>
            </div>
            <p>
              <xsl:value-of select="./Comments"/>
            </p>

          </div>
        </xsl:for-each>
    </xsl:if>

  </xsl:template>


  <!-- recursive template for splitting links (url|url) -->
  <xsl:template name="splitAndCreateRatingImages">
    <xsl:param name="str"/>
    <xsl:choose>
      <xsl:when test="contains($str,'|')">
        <img width="25px">
          <xsl:attribute name="src">
            <xsl:value-of  select="substring-before($str,'|')"/>
          </xsl:attribute>
        </img>
        <xsl:call-template name="splitAndCreateRatingImages">
          <xsl:with-param name="str"
  select="substring-after($str,'|')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <img width="25px">
          <xsl:attribute name="src">
            <xsl:value-of  select="$str"/>
          </xsl:attribute>
        </img>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="FormatDate">
    <xsl:param name="Date" />
    <xsl:variable name="year">
      <xsl:value-of select="substring($Date,1,4)" />
    </xsl:variable>
    <xsl:variable name="month">
      <xsl:value-of select="substring($Date,6,2)" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring($Date,9,2)" />
    </xsl:variable>
    <xsl:value-of select="$day"/>
    <xsl:value-of select="' '"/>
    <xsl:choose>
      <xsl:when test="$month = '1' or $month = '01'">Jan</xsl:when>
      <xsl:when test="$month = '2' or $month = '02'">Feb</xsl:when>
      <xsl:when test="$month = '3' or $month = '03'">Mar</xsl:when>
      <xsl:when test="$month = '4' or $month = '04'">Apr</xsl:when>
      <xsl:when test="$month = '5' or $month = '05'">May</xsl:when>
      <xsl:when test="$month = '6' or $month = '06'">Jun</xsl:when>
      <xsl:when test="$month = '7' or $month = '07'">Jul</xsl:when>
      <xsl:when test="$month = '8' or $month = '08'">Aug</xsl:when>
      <xsl:when test="$month = '9' or $month = '09'">Sep</xsl:when>
      <xsl:when test="$month = '10'">Oct</xsl:when>
      <xsl:when test="$month = '11'">Nov</xsl:when>
      <xsl:when test="$month = '12'">Dec</xsl:when>
    </xsl:choose>
    <xsl:value-of select="' '"/>
    <xsl:value-of select="$year"/>
  </xsl:template>
</xsl:stylesheet>


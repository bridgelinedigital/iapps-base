﻿<%@ Page Language="C#" Trace="false" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Bridgeline.Framework.Cache.Service" %>
<%@ Import Namespace="Bridgeline.Framework.Cache.Provider" %>
<%@ Import Namespace="Bridgeline.Framework.Provider.Marketier.AutomationFlow" %>
<%@ Import Namespace="Bridgeline.Framework.Model" %>


<script runat="server">
    FlowHelper fh = new FlowHelper();
    String flowCacheKey = "FlowDictionary";
      public String cacheKey  = "TestCacheKey";
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnFillFlowCache.Click += BtnFillFlowCache_Click;
        btnGetFlowFromCache.Click += BtnGetFlowCache_Click;
        btnLogMessage.Click += BtnLogMessage_Click;
        btnRedisCache.Click += BtnRedis_Click;
        btnDisplayCache.Click += BtnDisplayCache_Click;
        btnGetAdminCache.Click += btnGetAdminCache_Click;
        ltlMachineName.Text = Environment.MachineName;
    }
    private void BtnFillFlowCache_Click(object sender, EventArgs e)
    {
        fh.FillFlowCache();
    }
    private void BtnGetFlowCache_Click(object sender, EventArgs e)
    {
        BindFlow();
    }
    private void BindFlow()
    {
        Dictionary<Guid, FlowEntity> dicFlow = new Dictionary<Guid, FlowEntity>();
        var memoryCacheProvider = new MemoryCacheProvider();
        dicFlow = memoryCacheProvider.Get<Dictionary<Guid, FlowEntity>>(flowCacheKey);
        List<FlowEntity> ActiveFlows = new List<FlowEntity>();
        if (dicFlow != null)
        {
            ActiveFlows = dicFlow.Values.ToList();
        }
        rptFlowDetails.DataSource = ActiveFlows;
        rptFlowDetails.DataBind();
    }

    private void BtnLogMessage_Click(object sender, EventArgs e)
    {
        var rc = new RedisCacheProvider();
        rc.LogMessage("Hello cached");
    }
    private void BtnRedis_Click(object sender, EventArgs e)
    {
        var rc = new RedisCacheProvider();
        rc.Add("K1", "Value1");
        ltlRedisValue.Text = rc.Get<String>("K1");
        
    }

    private void BtnDisplayCache_Click(object sender, EventArgs e)
    {
        var ls = new LocalCacheService();
        string flowCacheFormat = "Flow{0}List";
        foreach (var eValue in Enum.GetNames(typeof(Enums.AutomationFlow.ControlAction)))
        {
            string cacheKey = string.Format(flowCacheFormat, eValue);
            var list = ls.Get<List<Guid>>(cacheKey);

            Response.Write("Key: " + cacheKey);
            if (list != null && list.Count > 0)
            {
                foreach (var item in list)
                {
                    Response.Write("Value: " + item);
                }
            }
        }
    }
    private void btnGetAdminCache_Click(object sender, EventArgs e)
    {
         var lc = new LocalCacheService();
         String s  = lc.Get<String>(cacheKey);

        Response.Write("Cache Value: " + s);
    }

</script>

<div>
    <form id="frm" runat="server">
        Machine Name :
        <asp:literal id="ltlMachineName" runat="server"></asp:literal>
        <br />
        <br />

        <asp:button id="btnFillFlowCache" runat="server" text="Fill Flow Cache" />

        <br />
        <br />
        <asp:button id="btnGetFlowFromCache" runat="server" text="Get Flow Cache" />
        <br />
        <br />
        Flows:
        <asp:repeater id="rptFlowDetails" runat="server">
            <ItemTemplate>
                <div>
                    <p>
                       Id :  <%# Eval("Id") %> ,   Title: <%# Eval("Title") %>
                    </p>
                </div>
            </ItemTemplate>
       </asp:repeater>
        <br />
        <br />
        <asp:button id="btnLogMessage" runat="server" text="Log Message" />
        <br />
        <br />
        <br />
        <br />
        <asp:button id="btnRedisCache" runat="server" text="TestRedis" />
        <br />
        From Redis :  <asp:literal id="ltlRedisValue" runat="server"></asp:literal>
        <br />
        <asp:button id="btnDisplayCache" runat="server" text="Event Cache" />
         <br />
        <br />
        <asp:button id="btnGetAdminCache" runat="server" text="Admin Cache" />
    </form>
</div>

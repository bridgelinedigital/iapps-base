﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System.IO" %>


<script runat="server">
    string cacheKey = "FileCache";
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

    }
    protected void Submit_GetCache(object sender, EventArgs e)
    {
        String value = String.Empty;
        if (HttpContext.Current != null && HttpContext.Current.Cache[cacheKey] != null) {
            value= HttpContext.Current.Cache[cacheKey].ToString();
        }

        Response.Write("Cache Value : " + value);
    }

    protected void Submit_UpdateCache(object sender, EventArgs e)
    {
        HttpContext.Current.Cache.Insert(cacheKey, txtCacheValue.Text,
                          new System.Web.Caching.CacheDependency(GetFilePath()));

         String value = String.Empty;
        if (HttpContext.Current != null && HttpContext.Current.Cache[cacheKey] != null) {
            value= HttpContext.Current.Cache[cacheKey].ToString();
        }

        Response.Write("Cache Value : " + value);
    }

    private string GetFilePath()
    {
        return "D:\\Logs\\TestCache.txt";
    }
</script>
<form runat="server" id="form1">
    <div>
        <asp:textbox id="txtCacheValue" runat="server"></asp:textbox>
        <asp:button id="btnGetCache" runat="server" text="Get CaheValue" onclick="Submit_GetCache" />
         <asp:button id="btnUpdateCache" runat="server" text="Update Cache" onclick="Submit_UpdateCache" />
    </div>
</form>

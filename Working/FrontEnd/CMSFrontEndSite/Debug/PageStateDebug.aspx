﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="System.Web.Configuration" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        SetPageModeInSession();
        if (Session["PageStateDebug"] != null)
            Response.Write(Session["PageStateDebug"].ToString());
        else
            Response.Write("Page State Debug is null");

    }

    private void SetPageModeInSession()
    {
        if (Context.Request.QueryString["PageStateDebug"] != null)
        {
            string qstrPageState = Context.Request.QueryString["PageStateDebug"].ToLower();

            switch (qstrPageState)
            {
                case "view":
                    Session["PageStateDebug"] = "View";
                    break;
                case "edit":
                    Session["PageStateDebug"] = "Edit";
                    break;
                case "preview":
                    Session["PageStateDebug"] = "Preview";
                    break;
                case "overlay":
                    Session["PageStateDebug"] = "Overlay";
                    break;
                case "emailview":
                case "emailView":
                    Session["PageStateDebug"] = "EmailView";
                    break;
                case "emailedit":
                    Session["PageStateDebug"] = "EmailEdit";
                    break;
                case "emailoverlay":
                    Session["PageStateDebug"] = "EmailOverlay";
                    break;
                case "templateedit":
                    Session["PageStateDebug"] = "TemplateEdit";
                    break;
                default:
                    Session["PageStateDebug"] = qstrPageState;
                    break;
            }
        }
        else
            Response.Write("QueryString is empty");

    }
</script>


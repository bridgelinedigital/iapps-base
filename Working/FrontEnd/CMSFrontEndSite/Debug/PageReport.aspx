﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var dicIdToPageMap = Bridgeline.FW.UI.PageMapEngine.PageMapService.GetUrlToPageMap();

        rptPages.DataSource = dicIdToPageMap.Values.Distinct()
            .Where(p => p.Status == Bridgeline.FW.UI.PageEngine.PageStatus.Published)
            .OrderBy(p => p.CompleteFriendlyUrl);
        rptPages.DataBind();

        /*foreach (var item in dicIdToPageMap.OrderBy(p => p.Value.CompleteFriendlyUrl))
        {
            Response.Write(string.Format("{0} : {1}<br />", item.Value.Id, item.Key));
        }

        Response.Write("<br />-----------------------------<br />");

        var urlRules = Bridgeline.FW.UI.UrlRewriter.UrlRewriteRulesService.GetAllRules();
        
        for (int i = 0; i < urlRules.Count; i++)
		{
            Response.Write(string.Format("{0} : {1}<br />", 
                HttpUtility.UrlDecode(urlRules[i].LookFor), urlRules[i].SendTo));
        }

        Response.Write("<br />-----------------------------<br />");

        Response.Write(string.Format("MachineName: {0}<br />", Environment.MachineName));*/

    }

</script>
<style>
    table {
        column-span: 0;
        border-collapse: collapse;
        width: 100%;
    }
    td {
        border:solid 1px #d2d2d2;
        padding: 2px 6px;
    }
</style>

<table>
    <tr>
        <td>SNo</td>
        <td>Title</td>
        <td>Url</td>
        <td>Id</td>
        <td>CreatedDate</td>
        <td>PublishDate</td>
    </tr>
    <asp:repeater id="rptPages" runat="server">
    <itemtemplate>
        <tr>
            <td style="width: 3%"><%#Container.ItemIndex + 1  %></td>
            <td style="width: 10%"><%#Eval("Title")  %></td>
            <td style="width: 30%"><%#Eval("CompleteFriendlyUrl")  %></td>
            <td style="width: 15%"><%#Eval("Id")  %></td>
            <td style="width: 9%"><%#Eval("CreatedDate")  %></td>
            <td style="width: 9%"><%#Eval("PublishDate")  %></td>
        </tr>
    </itemtemplate>
</asp:repeater>
</table>

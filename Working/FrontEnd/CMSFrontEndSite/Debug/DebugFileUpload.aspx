﻿
<%@ Page Language="C#" Trace="false" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="Bridgeline.FW.ContentBlock" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>
<%@ Import Namespace="Bridgeline.iAppsCMS.Web" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>

<script runat="server">
  
    protected void Page_Load(object sender, EventArgs e)
        {
        }


        protected void submitFile_Click(object sender, EventArgs e)
        {
            try
            {
                Guid fileId = new Guid(txtFileId.Text);

                AssetFile CurrentFile = new AssetFile(fileId, true);
                CurrentFile.ParentId = new Guid(txtParentId.Text);
                bool caContinue = true;
                string errorMessage = string.Empty;
                if (fileAsset.PostedFile.ContentLength > 0 || fileAsset.PostedFile.ContentLength > 0)
                {
                    caContinue = uploadFile(CurrentFile, out errorMessage);
                }


                ltlMessage.Text = "Success: " + caContinue.ToString() + " Message:"  + errorMessage;
            }
            catch(Exception ex)
            {
                ltlMessage.Text = "Error: " + ex.Message;
            }
        }

        private bool uploadFile(AssetFile CurrentFile, out string message)
        {
            FileUploadProcessor fileUpload = new FileUploadProcessor();
            message = string.Empty;
            bool uploaded = false;

            UploadFileType uploadFileType = UploadFileType.AssetFile;
            IFWFile ifwFile = (IFWFile)CurrentFile;
            message = fileUpload.ProcessFile(CurrentFile.ParentId, null, uploadFileType, ifwFile);
            if (message.ToLower().EndsWith("successfully."))
            {
                uploaded = true;
                if (string.IsNullOrEmpty(CurrentFile.FileName) || CurrentFile.Id != Guid.Empty)
                {
                    message = Bridgeline.iAPPS.Resources.GUIStrings.FileUpdatedSuccessfully;
                }
            }
            return uploaded;
        }

</script>

<div>
    <form id="form1" runat="server">
        <div>
           FileID:  <asp:TextBox ID="txtFileId" runat="server"></asp:TextBox>
            ParentId : <asp:TextBox ID="txtParentId" runat="server"></asp:TextBox>
             <asp:FileUpload ID="fileAsset" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                        CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" Width="280" />
            <asp:Button ID="submitFile" runat="server" Text="ProcessFile"  UseSubmitBehavior="true" OnClick="submitFile_Click"/>



        </div>
        <div>
            <asp:Literal ID="ltlMessage" runat="server"></asp:Literal>

        </div>
    </form>
</div>

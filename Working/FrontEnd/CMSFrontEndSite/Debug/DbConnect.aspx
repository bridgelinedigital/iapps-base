﻿<%@ Page Language="C#" %>

<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        try
        {
            String connectionString = ConfigurationManager.ConnectionStrings["DataAccessQuickStart"].ConnectionString;

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("Select * from SISite", conn))
                {
                    conn.Open();
                    var response = cmd.ExecuteScalar();

                    Response.Write("First SiteId: " + response);
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

        try
        {
            
            Response.Write("<br/> EnforcePCIRequirements: " + Bridgeline.FW.Global.FWSiteSettings.Instance.EnforcePCIRequirements);

            Response.Write("<br/> EnforcePCIRequirements: " + Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetBoolValue("EnforcePCIRequirements").ToString());
        }
        catch(Exception ex)
        {
            Response.Write(ex.Message);
        }

    }

</script>

﻿<%@ Page Language="C#"%>


<script runat="server">
    public String cacheKey  = "TestCacheKey";
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnSetCache.Click += btnSetCache_Click;
        btnGetCache.Click += btnGetCache_Click;
    }

    private void btnSetCache_Click(object sender, EventArgs e)
    {
        Bridgeline.FW.Cache.DistCacheService.Add(cacheKey, txtValue.Text);

    }
    private void btnGetCache_Click(object sender, EventArgs e)
    {
         Bridgeline.FW.Global.FWSiteSettings instance  = Bridgeline.FW.Cache.DistCacheService.Get<Bridgeline.FW.Global.FWSiteSettings>(txtValue.Text);

        if (instance != null)
            Response.Write(instance.SiteId);
        else
            Response.Write("false");
    }

</script>

<div>
    <form id="frm" runat="server">
    <asp:TextBox id="txtValue" runat="server"></asp:TextBox>
    <asp:button id="btnSetCache" runat="server" text="Set Cache" visible="false" />

    <br />
    <br />

         <asp:button id="btnGetCache" runat="server" text="Get Cache" />

        </form>
</div>

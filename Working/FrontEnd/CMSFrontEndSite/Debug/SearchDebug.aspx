﻿<%@ Page Language="C#" Trace="false" %>

<%@ Import Namespace="System.Data" %>

<%@ Import Namespace="Bridgeline.FW.ContentBlock" %>



<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnGetFile.Click += BtnGetFile_Click;

    }
    private void BtnGetFile_Click(object sender, EventArgs e)
    {
       Guid fileId = new Guid(txtFileId.Text);
      AssetFile file = AssetFileManager.GetAssetFile(fileId, false);

      if (file != null)
      {
        Response.Write("File is not null");
        }
    else
        {
        Response.Write("FIle is null");
    }

    }


</script>

<div>
    <form id="frm" runat="server">

    <asp:button id="btnGetFile" runat="server" text="GetFile" />

    <br />
    <br />
        <asp:TextBox id="txtFileId" runat="server"></asp:TextBox>
        <br />
    <br />
        <asp:Literal id="ltlFileInfo" runat="server"></asp:Literal>
        </form>
</div>

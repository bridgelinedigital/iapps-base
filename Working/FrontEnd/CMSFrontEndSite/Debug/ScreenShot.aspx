﻿<%@ Page Language="C#" Trace="false"  %>
<%@ Import Namespace="Bridgeline.Framework.Model" %>
<%@ Import Namespace="Bridgeline.Framework.Service" %>

<%@ Import Namespace="System.Data" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnsubmit.Click += btnsubmit_Click;
        //GetSites();

    }


    private void btnsubmit_Click(object sender, EventArgs e)
    {
        ltlResponse.Text = String.Empty;
        GetScreenShot();
    }
    private void GetScreenShot()
    {

        if (!String.IsNullOrEmpty(txtUrl.Text))
        {
            var screenshotRequest = new ScreenshotRequest()
            {
                FileName = "TestImageScreenshot",
                ImageFormat = Enums.Screenshot.ImageFormat.JPG,
                Url = txtUrl.Text,
                UseCache = true,
                RelativeFolderUrl="/VersionedImages/Workflow",
                IsFullPage = true


            };
            var sc = new ScreenshotService();
            var response = sc.Generate(screenshotRequest);

            String errorMessage = String.Empty;
            if(response.ErrorMessage != null && response.ErrorMessage.Count() > 0)
                errorMessage  = String.Join(",", response.ErrorMessage.ToArray());

            ltlResponse.Text = "Success: " + response.Success + " Message: " + response.Message + " Url: " + response.Url + " ErrorMessage: " + errorMessage;
        }


    }

</script>

<form runat="server">

 <div>
     <asp:TextBox id="txtUrl" runat="server"></asp:TextBox>
      <asp:button id="btnsubmit" runat="server" text="Get ScreenShot" />
     <br />
     <br /><br /><br />
    <asp:Literal ID="ltlResponse" runat="server"></asp:Literal>


     
    </div>
    </form>
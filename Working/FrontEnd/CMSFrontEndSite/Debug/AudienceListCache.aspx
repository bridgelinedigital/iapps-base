﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.Framework.Dto" %>

<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
     rptSessionList.ItemDataBound += new RepeaterItemEventHandler(rrptSessionList_ItemDataBound);
    }

    protected void Submit_GetCache(object sender, EventArgs e)
    {
            var localCacheService = new Bridgeline.Framework.Cache.Service.LocalCacheService();
           List<SessionListFilterDto> lstResult = localCacheService.Get<List<SessionListFilterDto>>("SessionListFilters");

            Response.Write("lstResult: " + lstResult);

            Response.Write("lstResult Count: " + lstResult.Count);
          rptSessionList.DataSource = lstResult;
        rptSessionList.DataBind();
   
    }

    public void rrptSessionList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            SessionListFilterDto p = e.Item.DataItem as SessionListFilterDto;

            Literal ltlRequestParamKey = e.Item.FindControl("ltlRequestParamKey") as Literal;
            Literal ltlRequestParamValue = e.Item.FindControl("ltlRequestParamValue") as Literal;

            ltlRequestParamKey.Text = p.RequestParamKey;
            ltlRequestParamValue.Text = p.RequestParamValue;
        }
    }

</script>
<form runat="server" id="form1">
    <div>
       
        <asp:button id="btnGetCache" runat="server" text="Get Cache" onclick="Submit_GetCache" />


         <asp:repeater id="rptSessionList" runat="server" >
            <itemtemplate>
                <br />
               <strong> Key : </strong><asp:Literal id="ltlRequestParamKey" runat="server"></asp:Literal>
                <strong> Value : </strong> <asp:Literal id="ltlRequestParamValue" runat="server"></asp:Literal>

            </itemtemplate>
    </asp:repeater>
    </div>
</form>

﻿<%@ Page Language="C#" %>

<%@ Register TagPrefix="Common"
    Namespace="Bridgeline.Framework.Common"
    Assembly="Bridgeline.Framework.Common, Version=7.0.0.0, Culture=neutral, PublicKeyToken=d94118ec66347585" %>
<%@ Register TagPrefix="Service"
    Namespace="Bridgeline.Framework.Service"
    Assembly="Bridgeline.Framework.Service, Version=7.0.0.0, Culture=neutral, PublicKeyToken=d94118ec66347585" %>

<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        Guid campaignId = new Guid(Request.QueryString["CampaignId"]);

        var service = new Bridgeline.Framework.Service.CampaignService();
        var request = new Bridgeline.Framework.Model.CampaignEntityRequest();
        request.Id = campaignId;

        var campaign =  service.GetItem(request);
        var campaignEmail = campaign.Emails.FirstOrDefault();

        Response.Write(campaignEmail.EmailHtml);
    }

</script>

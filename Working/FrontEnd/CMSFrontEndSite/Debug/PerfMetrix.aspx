﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<script runat="server">

    public enum PerfMonitorMetrics
        {
            QueryLongRunning = 1,
            QueryLongestTimeBlocking=2,
            QueryCPUIntensive=3,
            QueryIOIntensive=4,
            QueryMostOftenExecuted=5,
            QueryMissingStatistics=6,
            IndexMissing=7

        }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }

    private System.Data.DataTable GetData(int queryIndex)
    {
        string strDBName = null;
        //get the database name
        HtmlInputText dbNameControl = this.Page.FindControl("txtDBName") as HtmlInputText;
        if(dbNameControl != null)
        {
            strDBName = dbNameControl.Value;
        }

        var bo = new Bridgeline.FW.Global.FWBusinessObject(this.GetType());
        bo.ProcNameRoot = "Perf";
        bo.Params.Add("QueryTypeId", queryIndex);
        bo.Params.Add("DbName", strDBName);

        var dataTable = bo.ExecuteDataTable("GetPerformanceMetrics");

        rptCampaigns.DataSource = dataTable;
        rptCampaigns.DataBind();

        return dataTable;
    }

    protected void lnkMissingIndex_Click(object sender, EventArgs e)
    {
        GetData((int)PerfMonitorMetrics.IndexMissing);
    }

    protected void lnkMostCPUIntensiveQueries_Click(object sender, EventArgs e)
    {
        GetData((int)PerfMonitorMetrics.QueryCPUIntensive);
    }

    protected void lnkMostUseMostIO_Click(object sender, EventArgs e)
    {
        GetData((int)PerfMonitorMetrics.QueryIOIntensive);
    }

    protected void lnkLongRunningQuery_Click(object sender, EventArgs e)
    {
        GetData((int)PerfMonitorMetrics.QueryLongRunning);
    }


    protected void lnkLongestTimeBlocking_Click(object sender, EventArgs e)
    {
        GetData((int)PerfMonitorMetrics.QueryLongestTimeBlocking);
    }


    protected void lnkMostOftenExecutedQueries_Click(object sender, EventArgs e)
    {
        GetData((int)PerfMonitorMetrics.QueryMostOftenExecuted);
    }

    protected void lnkQueriesWithMissingStats_Click(object sender, EventArgs e)
    {
        GetData((int)PerfMonitorMetrics.QueryMissingStatistics);
    }


</script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
    h2 {
    }

    table {
        column-span: 0;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: solid 1px #999;
        padding: 2px 6px;
        text-align: left;
    }

    th {
        background-color: #ccc;
    }
</style>
<form runat="server">
    <div class="wrapper">
        <h2>
            <asp:literal id="ltHeading" runat="server" />
        </h2>

        <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1"></span>
  </div>
  <input runat="server" id="txtDBName" type="text" class="form-control" placeholder="Database Name" aria-label="Database Name" aria-describedby="basic-addon1">
</div>

        <div class="btn-group" role="group" aria-label="Basic example">
            <asp:linkbutton id="lnkLongRunningQry" class="btn btn-secondary" runat="server" text="Long Running" onclick="lnkLongRunningQuery_Click" />
             <asp:linkbutton id="lnkLongestTimeBlocking" class="btn btn-secondary" runat="server" text="Longest time blocking" onclick="lnkLongestTimeBlocking_Click" />
                <asp:linkbutton id="lnkMostCPUIntensiveQueries" class="btn btn-secondary" runat="server" text="Most CPU Usage" onclick="lnkMostCPUIntensiveQueries_Click" />
                <asp:linkbutton id="lnkMostUseMostIO" class="btn btn-secondary" runat="server" text="Most IO Usage" onclick="lnkMostUseMostIO_Click" />
                <asp:linkbutton id="lnkMostOftenExecutedQueries" class="btn btn-secondary" runat="server" text="Executed Most Often" onclick="lnkMostOftenExecutedQueries_Click" />
                <asp:linkbutton id="lnkQueriesWithMissingStats" class="btn btn-secondary" runat="server" text="Missing Statistics" onclick="lnkQueriesWithMissingStats_Click" />
</div>
      <hr class="mt-1 mb-1"/>
         <div class="btn-group" role="group" aria-label="Basic example">
            <asp:linkbutton id="lnkMissingIndex" class="btn btn-secondary" runat="server" text="Missing Index" onclick="lnkMissingIndex_Click" />
</div>
       
        <br />
          <asp:GridView class="table"  ID="rptCampaigns"  runat="server" autoGeneratedColumns="true">  
              <rowstyle backcolor="LightCyan"  
           forecolor="DarkBlue"

           />
</asp:GridView>
    </div>
</form>

﻿<%@ Page Language="C#" %>

<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var site = Bridgeline.FW.SiteBlock.SiteManager.GetCurrentSite();

       System.Net.ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                    
        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls |
            System.Net.SecurityProtocolType.Tls11 |
            System.Net.SecurityProtocolType.Tls12;

        var request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(site.PrimarySiteUrl);
        request.Method = "HEAD";
        request.AllowAutoRedirect = true;
        request.KeepAlive = false;
        request.Timeout = 20 * 1000; //20 seconds
        var response = request.GetResponse();

        Response.Write(response.ResponseUri.AbsoluteUri);
    }

    public static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

</script>

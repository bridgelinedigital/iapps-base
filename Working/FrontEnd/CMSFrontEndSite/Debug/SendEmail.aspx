﻿<%@ Page Language="C#"  Trace="true"%>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="Bridgeline.FW.Global.Utility" %>
<%@ Import Namespace="Bridgeline.FW.TaskBlock" %>
<%@ Import Namespace="Bridgeline.FW.Global.FWEmail" %>
<%@ Import Namespace="Bridgeline.FW.Global.ConfigHelper" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>

<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        Exception ex = new Exception("From debug page");

        //LoggingManager.LogFWException(ex);
    }

    protected void Submit_SendEmail(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Text = "Sending ...";
            SmtpClient mailClient = null;
            MailMessage mailMessage = new MailMessage("vsinha@bridgeline.com", txtEmail.Text);
            mailMessage.Subject = "Debug Test email";
            mailMessage.Body = "Debug Test email " + Environment.NewLine + "Machine Name : " + System.Environment.MachineName + Environment.NewLine + "Site Name: " + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName();

            string server = Util.GetAdditionalProperties(typeof(Emails), "SmtpServer");
            string enableSSL = Util.GetAdditionalProperties(typeof(Emails), "EnableSSL");
            bool isSSLEnabled = false;

            if (!String.IsNullOrEmpty(enableSSL))
                isSSLEnabled = Convert.ToBoolean(enableSSL);


            if (string.IsNullOrEmpty(server))
                mailClient = new SmtpClient();
            else
                mailClient = new SmtpClient(server);

            if (isSSLEnabled)
                mailClient.EnableSsl = isSSLEnabled;

            try
            {
                string userName = Util.GetAdditionalProperties(typeof(Emails), "UserName");
                string password = Util.GetAdditionalProperties(typeof(Emails), "Password");

                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                    mailClient.Credentials = new System.Net.NetworkCredential(userName, password);
            }
            catch { }

            mailClient.Send(mailMessage);

            lblStatus.Text = "Email has been send to " + txtEmail.Text;
            lblMachineName.Text = "Machine : " + System.Environment.MachineName;
            lblSiteName.Text = "Site: " + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName();

        }
        catch (Exception ex)
        {
            lblStatus.Text = "Failure sending email, Error :" + ex.Message + ex.StackTrace + ex.InnerException;
        }
    }

    protected void Submit_SendFormEmail(object sender, EventArgs e)
    {
        try
        {
            var responses = new Dictionary<string, List<string>>();
            responses.Add("FName", new List<string>() {
                "JJJ"
            });

            SendContactFormSubmissionEmail(txtEmail.Text, FWAppSettings.GetStringValue("FormsLibrary.SenderEmail", "admin@bridgelinesw.com"), "Test Title", responses, new Guid("CCF96E1D-84DB-46E3-B79E-C7392061219B"), "http://iapps.com");

        }
        catch(Exception ex)
        {
            lblStatus.Text = "Failure sending email, Error :" + ex.Message + ex.StackTrace;

        }
    }

    public void SendContactFormSubmissionEmail(string toEmail, string fromEmail, string formTitle, Dictionary<string,List<string>> fieldResponses, Guid contactId, string siteUrl)
    {
        StringBuilder body = new StringBuilder();

        foreach (System.Collections.Generic.KeyValuePair<string, List<string>> fieldResponse in fieldResponses)
        {
            body.AppendFormat("<tr><td><div>{0}</div>", fieldResponse.Key);

            foreach (string response in fieldResponse.Value)
            {
                body.AppendFormat("<div style=\"font-weight: bold;\">{0}</div>", response);
            }

            body.Append("</td></tr>");
        }

        Hashtable htSubstitutions = new Hashtable();
        htSubstitutions.Add("[FORMTITLE]", formTitle);
        htSubstitutions.Add("[DYNAMICBODY]", body.ToString());
        htSubstitutions.Add("[CONTACTURL]", String.Format("{0}/MarketierAdmin/Contacts/ManageContactDetails.aspx?Id={1}", siteUrl.TrimEnd('/'), contactId));

        Guid templateId = FWAppSettings.GetGuid("EmailTemplateForLeadFormSubmitted");

        Response.Write("<br/> send email SendContactFormSubmissionEmail");
        SendEmail(templateId, toEmail, fromEmail, htSubstitutions, null,String.Empty);

        Response.Write("<br/> at last of SendContactFormSubmissionEmail");
    }

    public void SendEmail(Guid emailTemplateKey, string toEmail, string fromEmail, Hashtable htSubstitutions, List<string> lstAttachments, string emailBody)
    {
        // Read this from config
        Bridgeline.FW.TaskBlock.EmailTemplate emailTemplate = new Bridgeline.FW.TaskBlock.EmailTemplate(emailTemplateKey, true);
        if (string.IsNullOrEmpty(emailBody))
        {
            emailBody = emailTemplate.Body;
            Response.Write("<br/> email body: " + emailBody);

        }
        Response.Write("<br/> start of SendEmail");
        SendEmail(emailTemplate.Subject, fromEmail, toEmail, null, null, emailBody, emailTemplate.Mimetype, htSubstitutions, lstAttachments, true);
        Response.Write("<br/> last of SendEmail");
    }

    public void SendEmail(string subject, string fromEmail, string toEmail, string ccEmail, string bccEmail, string body, Bridgeline.FW.TaskBlock.MimeTypeEnum mimeType,
          Hashtable htSubstitutions, List<string> lstAttachments, bool includeHeaderFooter)
    {
        FWMailHelper mailHelper = new FWMailHelper();
        Response.Write("<br/> start of mail helper SendEmail");
        mailHelper.SendEmail(subject, fromEmail, toEmail, ccEmail, bccEmail, body, mimeType, htSubstitutions, lstAttachments, includeHeaderFooter);

        Response.Write("<br/> end of mail helper SendEmail");
    }





  



  </script>
<form runat="server" id="form1">
    <div>
        Debug Email Sending: 
        <br />
        To: <asp:textbox id="txtEmail" runat="server"></asp:textbox>
        <asp:button id="submitbtn" runat="server" text="SendEmail" onclick="Submit_SendEmail" />
        <br />
        <asp:button id="submitFormEmail" runat="server" text="SendFormEmail" onclick="Submit_SendFormEmail" />

        <br /><br /><br /><br />
        <asp:Label id="lblStatus" runat="server"></asp:Label>
        <br />
        <asp:Label id="lblMachineName" runat="server"></asp:Label>
        <br />
        <asp:Label id="lblSiteName" runat="server"></asp:Label>
    </div>
</form>

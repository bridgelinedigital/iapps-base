﻿<%@ Page Language="C#" %>

<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

    }

    protected void Submit_Setting(object sender, EventArgs e)
    {
        var siteSettingValue = Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue(txtSiteSetting.Text, String.Empty);
        Response.Write("Value: " + siteSettingValue);
    }
</script>
<form runat="server" id="form1">
    <div>
        <asp:textbox id="txtSiteSetting" runat="server"></asp:textbox>
        <asp:button id="submitbtn" runat="server" text="Get SiteSettingValue" onclick="Submit_Setting" />
    </div>
</form>

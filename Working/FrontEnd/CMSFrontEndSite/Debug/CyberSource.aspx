﻿<%@ Page Language="C#" %>


<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string merchantId = "iappsuser";
        string keyId = "847a2fa4-3ed1-4a11-be45-84cbc1245358";
        var sharedSecret = new System.Security.SecureString();
        "IL26HjY0EuKPNkmIUS7T7QMqO4SlCU6R9DOvdrIpWYw=".ToList().ForEach(s => sharedSecret.AppendChar(s));

        var environment = FlexServerSDK.Authentication.Environment.TEST;

        var flexCredentials = new FlexServerSDK.Authentication.CyberSourceFlexCredentials(
            environment,
            merchantId,
            keyId,
            sharedSecret
        );

        var flexServiceConfiguration = new FlexServerSDK.FlexServiceConfigurationBuilder()
        .SetDebugLoggingEnabled(true)
        .Build();

        var flexService = FlexServerSDK.FlexServiceFactory.Create(flexCredentials, flexServiceConfiguration);
        var siteURL = "https://perkins.cloud.stage.iapps.com";

        var ex = flexService.CreateKey(FlexServerSDK.Model.EncryptionType.RsaOaep, siteURL);

        Response.Write(ex.Result);

    }

</script>
<div>Test Message 4</div>
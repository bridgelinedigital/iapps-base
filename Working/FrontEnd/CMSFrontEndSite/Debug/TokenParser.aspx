﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string token = Request.QueryString["iAppsApiToken"];
        Response.Write("<br />-----------------------------<br />");

        var fat = FormsAuthentication.Decrypt(token);
        if (fat == null)
            Response.Write(string.Format("FAT IS NULL"));
        else
        {
            if (fat.Expired)
            Response.Write(string.Format("FAT IS Expired"));
            
            string userData = fat.UserData;

            var userIdentity = new Bridgeline.FW.Global.Security.FWIdentity(fat, false);

            Response.Write(string.Format("FAT Expiration : {0}<br />", fat.Expiration));
            Response.Write(string.Format("User Data : {0}<br />", userData));
            Response.Write(string.Format("UserId : {0}<br />", userIdentity.Member.ProviderUserKey));
           
        }
        
        Response.Write("<br />-----------------------------<br />");

    }

</script>


﻿<%@ Page Language="C#" %>
<%@ Register Tagprefix="Utilities" 
   Namespace="System.Web.Http" 
   Assembly="System.Web.Http, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register Tagprefix="Contract" 
   Namespace="Bridgeline.Framework.Contract" 
   Assembly="Bridgeline.Framework.Contract, Version=5.3.0.0, Culture=neutral, PublicKeyToken=d94118ec66347585" %>
<%@ Import Namespace="System.Web.Http" %>
<%@ Register Tagprefix="Common" 
   Namespace="Bridgeline.Framework.Dto" 
   Assembly="Bridgeline.Framework.Dto, Version=5.3.0.0, Culture=neutral, PublicKeyToken=d94118ec66347585" %>

<%@ Register Tagprefix="Common" 
   Namespace="Bridgeline.Framework.Common" 
   Assembly="Bridgeline.Framework.Common, Version=5.3.0.0, Culture=neutral, PublicKeyToken=d94118ec66347585" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var impersonate = new Bridgeline.iAPPSAnalytics.Security.ImpersonationHelper();
        Response.Write(impersonate.impersonateValidUser("ysemegn", "blglobal", "kqwq@ym1Q1"));
        return;

        /*var ruleItems = new List<Bridgeline.Framework.Model.SiteRuleItem>();

        var ruleItem = new Bridgeline.Framework.Model.SiteRuleItem();
        ruleItem.Type = Bridgeline.Framework.Model.SiteRuleType.SiteAttributes;
        ruleItem.PropertyName = "8351189e-c776-4cfa-a7e6-5a399f91eaf2"; 
        ruleItem.Operator = Bridgeline.Framework.Model.RuleOperator.Equal;
        ruleItem.Values.Add("Participation Page 1");

        ruleItems.Add(ruleItem);

        var sites = Bridgeline.FW.SiteBlock.SiteManager.GetSitesByFilters(ruleItems);

        Response.Write(sites.Count);

        var sites = Bridgeline.FW.SiteBlock.SiteManager.GetCachedSites();

        var lstSites = Bridgeline.FW.SiteBlock.SiteManager.GetSites(sites.Select(s => s.Id));

        var strTest = string.Join(",", lstSites.Select(s => s.Addresses != null ? s.Addresses.Count : -1));

        Response.Write(strTest);*/
        
        var unityResolver = new Bridgeline.Framework.Common.Unity.UnityResolver();
        unityResolver.GetTypes("Bridgeline.Framework.Provider", "Bridgeline.Framework.Provider");
        //return;

        var service = new Bridgeline.Framework.Service.AttributeService();
        service.GetItem(Guid.NewGuid());

        var type = typeof(Bridgeline.Framework.Contract.IDistributionProcessor);

        var processor = unityResolver.Resolve<Bridgeline.Framework.Contract.IDistributionProcessor>();

        if (processor == null)
            Response.Write("Processor is null");
        else
            Response.Write(processor.GetType().Name);
    }

</script>

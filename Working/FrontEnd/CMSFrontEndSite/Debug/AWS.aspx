﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var storageService = new Bridgeline.FW.UI.Web.ExternalStorageService();

        Guid siteId = Bridgeline.FW.Global.GlobalManager.GetSiteContext();
        var stream = storageService.DownloadFile(Request.QueryString["Url"], siteId);
        if (stream == null)
            Response.Write("File not exists");
        else
            Response.Write("File exists");
    }

</script>


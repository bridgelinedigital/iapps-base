﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var browserCaps = Bridgeline.FW.Global.GlobalManager.FindCurrentDevice();

        string deviceOS = HttpContext.Current.Request.Browser.Platform;
        string mobileBrowser = HttpContext.Current.Request.Browser.Type;
        string deviceName = string.Empty;
        bool isMobile = false;
        bool isTablet = false;

        if (browserCaps.ContainsKey("is_wireless_device") && browserCaps.ContainsKey("is_tablet"))
        {
            if (browserCaps["is_wireless_device"] == "true" || browserCaps["is_tablet"] == "true")
            {
                deviceOS = browserCaps["device_os"] + " " + browserCaps["device_os_version"];
                mobileBrowser = browserCaps["mobile_browser"] + " " + browserCaps["mobile_browser_version"];
                deviceName = browserCaps["brand_name"] + " " + browserCaps["model_name"];
                isTablet = browserCaps["is_tablet"] == "true" ? true : false;
                isMobile = browserCaps["is_wireless_device"] == "true" && isTablet == false ? true : false;

            }
        }

        Response.Write("<br />-----------------------------<br />");
        Response.Write(string.Format("Device OS : {0}<br />", deviceOS));
        Response.Write(string.Format("Browser : {0}<br />", deviceOS));
        Response.Write(string.Format("Device Name : {0}<br />", deviceName));
        Response.Write(string.Format("Is Tablet : {0}<br />", isTablet));
        Response.Write(string.Format("Is Mobile : {0}<br />", isMobile));
        Response.Write(string.Format("UserAgent : {0}<br />", HttpContext.Current.Request.UserAgent));

        Response.Write("<br />-----------------------------<br />");

    }

</script>


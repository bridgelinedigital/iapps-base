﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<script runat="server">

    protected bool IsAutoResponder
    {
        get
        {
            return Request.QueryString["Type"] == "2";
        }
    }

    protected bool IsDailyReport
    {
        get
        {
            return Request.QueryString["ReportType"] == "Daily";
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        rptCampaigns.DataSource = GetData();
        rptCampaigns.DataBind();

        ltHeading.Text = IsAutoResponder ? "Autoresponders" : "Campaigns";

        if (IsDailyReport)
            thRunDate.InnerText = "Send Date";
        else if (IsAutoResponder)
            thRunDate.InnerText = "First Run Date";
    }

    private System.Data.DataTable GetData()
    {
        int type = IsAutoResponder ? 2 : 1;

        var bo = new Bridgeline.FW.Global.FWBusinessObject(this.GetType());
        bo.ProcNameRoot = "CampaignRunHistoryDto";
        bo.Params.Add("Type", type);

        DateTime startDate = DateTime.MinValue;
        if (DateTime.TryParse(Request.QueryString["StartDate"], out startDate))
            bo.Params.Add("StartDate", startDate);

        DateTime endDate = DateTime.MinValue;
        if (DateTime.TryParse(Request.QueryString["EndDate"], out endDate))
            bo.Params.Add("EndDate", endDate);
        
        var dataTable = bo.ExecuteDataTable(IsDailyReport ? "GetDailyReport" : "GetReport");

        foreach (System.Data.DataRow dr in dataTable.Rows)
        {
            if (dr.Table.Columns.Contains("ExternalCode") && dr["ExternalCode"] != null && dr["ExternalCode"] != string.Empty)
                dr["SiteName"] = string.Format("{0} ({1})", dr["SiteName"], dr["ExternalCode"]);
        }

        return dataTable;
    }

    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        var export = new Bridgeline.FW.Global.IO.Csv.Export(HttpContext.Current);

        string fileName = string.Format("{0}_Report", IsAutoResponder ? "Autoresponders" : "Campaigns");

        var dataTable = GetData();

        string[] headers = new string[] 
        { 
            "Site Name", 
            "Campaign Name", 
            "Run Date", 
            "Sends", 
            "Opens", 
            "Unsubscribes", 
            "Bounces", 
        };

        string[] columns = new string[] 
        { 
            "SiteName", 
            "CampaignName", 
            "RunDate", 
            "Sends", 
            "Opens", 
            "Unsubscribes", 
            "Bounces", 
        };

        export.ExportData(dataTable, columns, headers, Bridgeline.FW.Global.IO.Csv.Export.ExportFormat.Excel, fileName);

    }

</script>
<style>
    h2 {
    }

    table {
        column-span: 0;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: solid 1px #999;
        padding: 2px 6px;
        text-align: left;
    }

    th {
        background-color: #ccc;
    }
</style>
<form runat="server">
    <div class="wrapper">
        <h2>
            <asp:literal id="ltHeading" runat="server" />
        </h2>
        <div class="button-row">
            <asp:linkbutton id="lnkDownload" runat="server" text="Download" onclick="lnkDownload_Click" />
        </div>
        <table>
            <tr>
                <th>Site Name</th>
                <th>Campaign Name</th>
                <th runat="server" id="thRunDate">Run Date</th>
                <th>Sends</th>
                <th>Opens</th>
                <th>Unsubscribes</th>
                <th>Bounces</th>
            </tr>
            <asp:repeater id="rptCampaigns" runat="server">
            <itemtemplate>
                <tr>
                    <td style="width: 20%"><%#Eval("SiteName")  %></td>
                    <td style="width: 20%"><%#Eval("CampaignName")  %></td>
                    <td style="width: 10%"><%#Eval("RunDate")  %></td>
                    <td style="width: 10%"><%#Eval("Sends")  %></td>
                    <td style="width: 10%"><%#Eval("Opens")  %></td>
                    <td style="width: 10%"><%#Eval("Unsubscribes")  %></td>
                    <td style="width: 10%"><%#Eval("Bounces")  %></td>
                </tr>
            </itemtemplate>
        </asp:repeater>
        </table>
    </div>
</form>

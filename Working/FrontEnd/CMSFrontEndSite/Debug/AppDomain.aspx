﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);


        var lstAssembly = new List<AssemblyData>();
        foreach (var item in AppDomain.CurrentDomain.GetAssemblies().GroupBy(a => a.FullName)
                        .Select(g => g.First()).ToList().OrderBy(a => a.FullName))
        {
            var data = new AssemblyData();
            data.Name = item.GetName().Name;
            data.VersionData = item.GetName().Version;
            data.BuildDate = RetrieveLinkerTimestamp(item);
            data.FileVersion = GetFileVersion(item);

            lstAssembly.Add(data);
        }

        rptAssembly.DataSource = lstAssembly;
        rptAssembly.DataBind();

    }

    private string GetFileVersion(System.Reflection.Assembly assembly)
    {
        try
        {
            var fileVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);

            return fileVersion.FileVersion;
        }
        catch
        {
            return string.Empty;
        }
    }

    private DateTime RetrieveLinkerTimestamp(System.Reflection.Assembly assembly)
    {
        try
        {
            string filePath = assembly.Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];
            System.IO.Stream s = null;

            try
            {
                s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                s.Read(b, 0, 2048);
            }
            finally
            {
                if (s != null)
                {
                    s.Close();
                }
            }

            int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
            int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            dt = dt.AddSeconds(secondsSince1970);
            dt = dt.ToLocalTime();
            return dt;
        }
        catch
        {
            return DateTime.MinValue;
        }
    }

    public class AssemblyData
    {
        public string Name { get; set; }

        public DateTime BuildDate { get; set; }

        public Version VersionData { get; set; }

        public string FileVersion { get; set; }
    }

</script>
<style>
    table {
        column-span: 0;
        border-collapse: collapse;
        width: 100%;
    }

    td {
        border: solid 1px #d2d2d2;
        padding: 2px 6px;
    }
</style>

<table>
    <tr>
        <td>SNo</td>
        <td>FullName</td>
        <td>Build Date</td>
        <td>Version</td>
        <td>File Version</td>
    </tr>
    <asp:repeater id="rptAssembly" runat="server">
    <itemtemplate>
        <tr>
            <td style="width: 3%"><%#Container.ItemIndex + 1  %></td>
            <td style="width: 40%"><%#Eval("Name")  %></td>
            <td style="width: 15%"><%#Eval("BuildDate")  %></td>
            <td style="width: 15%"><%#Eval("VersionData")  %></td>
            <td style="width: 15%"><%#Eval("FileVersion")  %></td>
        </tr>
    </itemtemplate>
</asp:repeater>
</table>

﻿<%@ Page Language="C#" Trace="false" %>

<%@ Import Namespace="System.Data" %>

<%@ Import Namespace="Bridgeline.Framework.Service" %>
<%@ Import Namespace="Bridgeline.Framework.Model" %>


<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnLogging.Click += BtnLogging_Click;
         btnLogApi.Click += btnLogApi_Click;
        
    }
    private void BtnLogging_Click(object sender, EventArgs e)
    {
        var ls = new LogService();
        var le = new LogEventEntity()
        {
            Message = "New Search Message",
            LogCategory = Enums.LogCategory.Search,
            TraceLevel = Enums.TraceLevel.Debug,
            Loggers = new List<Enums.Logger>()
            {
				Enums.Logger.File
            }
        };
        ls.Log(le);
    }

     private void btnLogApi_Click(object sender, EventArgs e)
    {
        var ls = new LogService();
        var le = new LogEventEntity()
        {
            Message = "New API Message",
            LogCategory = Enums.LogCategory.API,
            TraceLevel = Enums.TraceLevel.Debug,
            Loggers = new List<Enums.Logger>()
            {
				Enums.Logger.File
            }
        };
        ls.Log(le);
    }

</script>

<div>
    <form id="frm" runat="server">

    <asp:button id="btnLogging" runat="server" text="LogSearch" />

    <br />
    <br />

   <asp:button id="btnLogApi" runat="server" text="LogAPI" />
        </form>
</div>

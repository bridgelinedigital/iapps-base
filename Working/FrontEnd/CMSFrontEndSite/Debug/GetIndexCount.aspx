﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.Framework.Common" %>
<%@ Import Namespace="Bridgeline.Framework.Service" %>
<%@ Import Namespace="Bridgeline.Framework.Model" %>
<%@ Import Namespace="Bridgeline.Framework.Search" %>
<%@ Import Namespace="Nest" %>


<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

    }

    protected void Submit_Setting(object sender, EventArgs e)
    {
        var objectTypes = Bridgeline.Framework.Common.Utility.Util.EnumUtil.GetValues<Enums.ObjectType>();

        var countResponse = new Bridgeline.Framework.Search.CountResponse();
        long count = 0;
        long pendingCount = 0;
        long errorCount = 0;
        String type = String.Empty;
        String indexName = String.Empty;
        int counter = 1;

        var indexListCreated = new List<IndexEntity>();
        indexListCreated = GetIndexItemList(Enums.Search.IndexStatus.Created);

        Response.Write("After Get Index item list");
        var indexListError = new List<IndexEntity>();
        indexListError = GetIndexItemList(Enums.Search.IndexStatus.Error);
        Response.Write("After Get Index item list Error");
        //foreach (var item in objectTypes)
        //{
        //    switch (item)
        //    {
        //        case Enums.ObjectType.Page:
        //            count = GetDocCount(item, searchSetting.FrontEndIndexName);
        //            indexName = searchSetting.FrontEndIndexName;
        //            type = "Page";
        //            break;

        //    }
        //}
    }
    //private long GetDocCount(Enums.ObjectType type, String index)
    //{
    //    var elasticSearch = new ElasticSearch();
    //    long count = 0;
    //    TypeName[] types = null;
    //    var objectTypes = new List<Enums.ObjectType>();
    //    objectTypes.Add(type);
    //    types = elasticSearch.GetTypes(objectTypes);

    //    var response = client.Count<IDocument>(s => s
    //    .Index(index)
    //    .Type(types)
    //    );

    //    if (response != null)
    //        count = response.Count;

    //    return count;

    //}
    private List<IndexEntity> GetIndexItemList(Enums.Search.IndexStatus processStatus)
    {
        var indexList = new List<IndexEntity>();
        var indexService = new IndexService();
        var request = new IndexEntityRequest()
        {
            Processed = processStatus,
            PageSize = int.MaxValue
        };
        var indexResponse = indexService.Provider.Get(request);

        if (indexResponse != null && indexResponse.Items.Count() > 0)
        {
            indexList = indexResponse.Items.ToList();
        }
        return indexList;
    }
</script>
<form runat="server" id="form1">
    <div>
       
        <asp:button id="submitbtn" runat="server" text="Get Index Details" onclick="Submit_Setting" />
    </div>
</form>

﻿<%@ Page Language="C#" Trace="false" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.Global.Enum" %>

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnIsPageValid.Click += BtnIsPageValid_Click;


    }

    private void BtnIsPageValid_Click(object sender, EventArgs e)
    {
        Guid pageId = Guid.Parse(txtPageId.Text);
        var p = Bridgeline.FW.UI.PageEngine.PageService.GetPage(pageId);

        var menu = Bridgeline.FW.UI.PageMapEngine.PageMapService.GetPageMapNode(p.SiteId, p.ParentPageMapNodeId);
        String message = String.Empty;
        if (menu != null)
        {
            if (menu.ExcludeFromSearch)
                message = "ExcludeFromSearch";
            if(menu.HiddenFromNavigation)
                message += " HiddenFromNavigation";
            if(menu.MenuStatus == MenuStatusEnum.InVisible)
                message += " InVisible";
            if(menu.MenuStatus == MenuStatusEnum.Deleted)
                message += " Deleted";


            ltlMessage.Text = message;
        }
    }
</script>

<div>
    <form id="frm" runat="server">
       <asp:TextBox id="txtPageId" runat="server"></asp:TextBox>

        <asp:button id="btnIsPageValid" runat="server" text="Page Valid to Reindex" />
        <br /><br /><br /><br />
        <asp:Literal id="ltlMessage" runat="server"></asp:Literal>
    </form>
</div>

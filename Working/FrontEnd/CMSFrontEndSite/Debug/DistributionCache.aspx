﻿<%@ Page Language="C#" Trace="false" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Bridgeline.Framework.Cache.Service" %>
<%@ Import Namespace="Bridgeline.Framework.Cache" %>
<%@ Import Namespace="Bridgeline.Framework.Cache.Provider" %>
<%@ Import Namespace="Bridgeline.Framework.Model" %>


<script runat="server">
    //public String cacheKey  = "TestCacheKey";
    public DistributedCacheService dcs = new DistributedCacheService();
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnDisplayCache.Click += BtnDisplayCache_Click;
        btnSaveCache.Click += BtnSaveCache_Click;

    }
    private void BtnSaveCache_Click(object sender, EventArgs e)
    {
        dcs.Add(txtCacheKey.Text, txtCacheValue.Text);
        Response.Write("Key added to cache: " + txtCacheKey.Text);
    }
    private void BtnDisplayCache_Click(object sender, EventArgs e)
    {
        var response = dcs.Get<String>(txtCacheKey.Text);

        Response.Write("Cache Value: " + response);
    }

</script>

<div>
    <form id="frm" runat="server">

        <br />
        Cache Key: <asp:Textbox id="txtCacheKey" runat="server"></asp:Textbox>
        Cache Value: <asp:Textbox id="txtCacheValue" runat="server"></asp:Textbox>
        <asp:button id="btnSaveCache" runat="server" text="Save Cache" />
        <br />
        <asp:button id="btnDisplayCache" runat="server" text="Display Cache" />


    </form>
</div>

﻿<%@ Page Language="C#" Trace="false"  %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>
<%@ Import Namespace="Bridgeline.FW.UI.PageMapEngine" %>
<%@ Import Namespace="Bridgeline.FW.UI.PageEngine" %>

<%@ Import Namespace="System.Data" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        GetClientIP();
      
    }

    private void GetClientIP()
    {
        HttpRequest request = HttpContext.Current.Request;
        ltlRequestHostAddress.Text = request.UserHostAddress;

        if (String.IsNullOrEmpty(request.ServerVariables["HTTP_CLIENT_IP"]))
            ltlHTTP_CLIENT_IP.Text = "HTTP_CLIENT_IP is empty";
        else
            ltlHTTP_CLIENT_IP.Text = request.ServerVariables["HTTP_CLIENT_IP"];

        if (String.IsNullOrEmpty(request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
            ltlHTTP_X_FORWARDED_FOR.Text = "HTTP_X_FORWARDED_FOR is empty";
        else
            ltlHTTP_X_FORWARDED_FOR.Text = request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (String.IsNullOrEmpty(request.ServerVariables["REMOTE_ADDR"]))
            ltlREMOTE_ADDR.Text = "REMOTE_ADDR is empty";
        else
            ltlREMOTE_ADDR.Text = request.ServerVariables["REMOTE_ADDR"];


    }
</script>



 <div>
    Request Host Address : <strong> <asp:Literal ID="ltlRequestHostAddress" runat="server"></asp:Literal> </strong>
        <br />
        <br />
    Header HTTP_CLIENT_IP  : <strong> <asp:Literal ID="ltlHTTP_CLIENT_IP" runat="server"></asp:Literal> </strong>
        <br />
        <br />

    Header HTTP_X_FORWARDED_FOR  : <strong> <asp:Literal ID="ltlHTTP_X_FORWARDED_FOR" runat="server"></asp:Literal> </strong>
        <br />
        <br />

    Header REMOTE_ADDR  : <strong> <asp:Literal ID="ltlREMOTE_ADDR" runat="server"></asp:Literal> </strong>
        <br />
        <br />


    </div>
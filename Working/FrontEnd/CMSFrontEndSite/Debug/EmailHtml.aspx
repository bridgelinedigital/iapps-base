﻿<%@ Page Language="C#" Trace="false" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Bridgeline.Framework.Model" %>
<%@ Import Namespace="Bridgeline.FW.UI.Security" %>
<%@ Import Namespace="Bridgeline.FW.Global.Security" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnEmailHtml.Click += BtnEmailHtml_Click;

        ltlMachineName.Text = Environment.MachineName;
    }

    private void BtnEmailHtml_Click(object sender, EventArgs e)
    {
        using (var context = new FWAdminPrincipal())
        {
            var emailHtml = GetPageHtmlData(GlobalManager.GetSiteContext(), new Guid(txtEmailPageId.Text), null, false, null, null, true);
            Response.Write(emailHtml);
        }
    }


     public  string GetPageHtmlData(Guid siteId, Guid? pageId, Guid? contactId, bool sendLiveCampaign, Guid? campaignId, Guid? campaignEmailId, bool? isEmail)
        {
            string htmlEmailData = string.Empty;
            string queryString = string.Empty;

            FWPrincipal userPrincipal = HttpContext.Current.User as FWPrincipal;
            FWIdentity userIdentity = userPrincipal.Identity as FWIdentity;
            Guid tokenId = GetCMSToken();
            string pageUrl = Bridgeline.FW.Marketier.Base.SiteSettings.Instance.HomeUrl + "/PreviewEmailHandlerPage.ashx" + "?Token=" + tokenId.ToString();
            if (pageId != null) queryString = "&PageId=" + pageId.ToString();
            queryString += "&UserName=" + userIdentity.Member.UserName + "&UserId=" + userIdentity.Member.ProviderUserKey.ToString();
            if (contactId != null && contactId != Guid.Empty) queryString += "&ContactId=" + contactId.ToString();
            if (campaignId != null) queryString += "&CampaignId=" + campaignId.ToString() + "&ShowLiveCampaign=" + sendLiveCampaign.ToString();
            if (campaignEmailId != null) queryString += "&CampaignEmailId=" + campaignEmailId.ToString();
            if(isEmail.HasValue && isEmail.Value)
            {
                queryString += "&IsEmail=true";
            }
            


            queryString += "&SiteId=" + siteId;
            pageUrl = pageUrl + queryString;

        Response.Write(pageUrl);

            Uri uri = new Uri(pageUrl);//  +'&UserName=iAppsUser&UserId=798613ee-7b85-4628-a6cc-e17ee80c09c5");
            HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(uri);

            webRequest.Method = WebRequestMethods.Http.Get;
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            StreamReader reader = new StreamReader(webResponse.GetResponseStream());
            htmlEmailData = reader.ReadToEnd();
            webResponse.Close();

            return htmlEmailData;
        }

      public  Guid GetCMSToken()
        {
            AuthenticateApplication authentication = new AuthenticateApplication();
            HttpContext context = HttpContext.Current;
            FWPrincipal userPrincipal = context.User as FWPrincipal;
            FWIdentity userIdentity = userPrincipal.Identity as FWIdentity;

            Guid newSiteId = GlobalManager.GetSiteContext();
            Guid productId = Guid.Empty;
            if (Bridgeline.FW.Marketier.Base.SiteSettings.Instance.CMSProductId != Guid.Empty)
            {
                productId = Bridgeline.FW.Marketier.Base.SiteSettings.Instance.CMSProductId;
            }
            else
            {
                productId = userPrincipal.ProductId;
            }
            string siteURL = GlobalManager.GetPublicSiteURLFromRequest();

            Guid authenticationToken = authentication.CreateLoginToken(siteURL, userIdentity.Member.UserName,
                                            new Guid(userIdentity.Member.ProviderUserKey.ToString()), productId, newSiteId);
            return authenticationToken;
        }
</script>

<div>
    <form id="frm" runat="server">
        Machine Name :
        <asp:literal id="ltlMachineName" runat="server"></asp:literal>
        <br />
        <br />

      <asp:TextBox id="txtEmailPageId" runat="server"></asp:TextBox>
       <br />
        <br />
        <asp:button id="btnEmailHtml" runat="server" text="Email Html" />
    </form>
</div>

﻿<%@ Page Language="C#" Trace="false"  %>
<%@ Import Namespace="Bridgeline.FW.SiteBlock" %>
<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>
<%@ Import Namespace="Bridgeline.FW.UI.PageMapEngine" %>
<%@ Import Namespace="Bridgeline.FW.UI.PageEngine" %>

<%@ Import Namespace="System.Data" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnsubmit.Click += btnsubmit_Click;
        //GetSites();
       
    }

    private void GetSites()
    {
        var sites = SiteManager.GetSites();
        string sitecollection = String.Empty;

        foreach (var site in sites)
        {
            sitecollection += "<br/>" + site.Title;
        }

        ltlSItes.Text = sitecollection;
    }
    private void btnsubmit_Click(object sender, EventArgs e)
    {
         GetSite();
    }
    private void GetSite()
    {
        if (!String.IsNullOrEmpty(txtSiteId.Text))
        {
            var site = SiteManager.GetSite(new Guid(txtSiteId.Text));
            if (site != null)
                ltlSItes.Text = "Site Found : Title: " + site.Title;
        }


    }

</script>

<form runat="server">

 <div>
     <asp:TextBox id="txtSiteId" runat="server"></asp:TextBox>
    <asp:Literal ID="ltlSItes" runat="server"></asp:Literal>


      <asp:button id="btnsubmit" runat="server" text="Get Site" />
    </div>
    </form>
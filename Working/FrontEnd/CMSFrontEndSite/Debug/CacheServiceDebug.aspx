﻿<%@ Page Language="C#" Trace="false"  %>
<%@ Register TagPrefix="Common"
    Namespace="Bridgeline.Framework.Cache"
    Assembly="Bridgeline.Framework.Cache, Version=7.0.0.0, Culture=neutral, PublicKeyToken=d94118ec66347585" %>

<%@ Import Namespace="Bridgeline.FW.SiteBlock" %>
<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>
<%@ Import Namespace="Bridgeline.FW.UI.PageMapEngine" %>
<%@ Import Namespace="Bridgeline.FW.UI.PageEngine" %>
<%@ Import Namespace="Bridgeline.FW.Cache" %>
<%@ Import Namespace="Bridgeline.Framework.Cache.Service" %>
<%@ Import Namespace="Bridgeline.FW.ContentBlock" %>
<%@ Import Namespace="Bridgeline.FW.Cache.Providers" %>
<%@ Import Namespace="System.Data" %>
<script runat="server">

    private string key = "TestKey";
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        btnGet.Click += btnGet_Click;
        btnSave.Click += btnSave_Click;
        btnGetContent.Click += btnGetContent_Click;
        btnClearContent.Click += btnClearContent_Click;
        btnCacheKeyAll.Click += btnCacheKeyAll_Click;
    }

    private void btnClearContent_Click(object sender, EventArgs e)
    {
        var m = new MemoryCacheProvider();
        m.Remove(txtCacheKey.Text);
    }
    private void btnCacheKeyAll_Click(object sender, EventArgs e)
    {
        var m = new MemoryCacheProvider();

        ltlAllCacheKey.Text = String.Join(",", m.GetAllCacheKey());
    }
    private void btnGetContent_Click(object sender, EventArgs e)
    {
        var contents = ContentManager.GetContents(ObjectType.Content);
        StringBuilder s = new StringBuilder();
        foreach (var item in contents)
        {
            s.Append("Id = " + item.Id);

        }

        ltlContents.Text = s.ToString();

    }
    private void btnGet_Click(object sender, EventArgs e)
    {
        String s = String.Empty;
        LocalCacheService lc = new LocalCacheService();
        s = lc.Get<String>(key);
        if (String.IsNullOrWhiteSpace(s))
            s = DistCacheService.Get<String>(key) + " from oldCache";

        Response.Write(s);
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
        DistCacheService.Add(key, txtBox1.Text);
    }



</script>

<form runat="server">

 <div>
    <%-- <asp:TextBox id="txtSiteId" runat="server"></asp:TextBox>
    <asp:Literal ID="ltlSItes" runat="server"></asp:Literal>--%>
     <asp:TextBox id="txtBox1" runat="server"></asp:TextBox>
      <asp:button id="btnSave" runat="server" text="Save in old Cache service" />
      <asp:button id="btnGet" runat="server" text="Get From new Local Cache service" />

     <br />

     <asp:button id="btnGetContent" runat="server" text="Get Contents" />
     <asp:Literal id="ltlContents" runat="server"></asp:Literal>
     <br />
      <asp:button id="btnCacheKeyAll" runat="server" text="Get All Cache Key" />
     <asp:Literal id="ltlAllCacheKey" runat="server"></asp:Literal>
    <br />
     <asp:TextBox id="txtCacheKey" runat="server"></asp:TextBox>
     <asp:button id="btnClearContent" runat="server" text="Clear Contents" />
    </div>
    </form>
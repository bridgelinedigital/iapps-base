﻿<%@ Page Language="C#" %>
<%@ Import Namespace="Bridgeline.FW.UserBlock" %>
<%@ Import Namespace="Bridgeline.FW.Global.ConfigHelper" %>
<%@ Import Namespace="Bridgeline.iAppsCMS.Administration.User" %>


<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var cmsUserGroups = UserManager.GetGroupsIncludingGlobal(FWAppSettings.GetGuid("CMSProductId"), (int)GroupTypeCodes.SystemGroup);
        var socialUserGroups = UserManager.GetGroupsIncludingGlobal(FWAppSettings.GetGuid("SocialProductId"), (int)GroupTypeCodes.SystemGroup);
        var commerceUserGroups = UserManager.GetGroupsIncludingGlobal(FWAppSettings.GetGuid("CommerceProductId"), (int)GroupTypeCodes.SystemGroup);
        var analyzerUserGroups = UserManager.GetGroupsIncludingGlobal(FWAppSettings.GetGuid("AnalyticsProductId"), (int)GroupTypeCodes.SystemGroup);

        Response.Write("cmsUserGroups: <br/>" + Environment.NewLine);

        foreach (var g in cmsUserGroups)
            Response.Write(g.Title + Environment.NewLine);

        Response.Write("<br/>socialUserGroups: <br/>");

        foreach(var g in socialUserGroups)
            Response.Write(g.Title + Environment.NewLine);

        Response.Write("<br/>commerceUserGroups: <br/>");

        foreach (var g in commerceUserGroups)
            Response.Write(g.Title + Environment.NewLine);

        Response.Write("<br/>analyzerUserGroups: <br/>");

        foreach (var g in analyzerUserGroups)
            Response.Write(g.Title + Environment.NewLine);


    }


</script>

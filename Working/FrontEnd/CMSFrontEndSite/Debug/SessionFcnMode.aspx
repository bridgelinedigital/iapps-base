﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="System.Web.Configuration" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        HttpRuntimeSection f =
            ConfigurationManager.GetSection("system.web/httpRuntime") as HttpRuntimeSection;
        //HttpRuntimeSection f = new HttpRuntimeSection();
        Response.Write("FcnMode: " + f.FcnMode.ToString());
        Response.Write("<br/> ExecutionTimeout: " + f.ExecutionTimeout.ToString());

    }

</script>


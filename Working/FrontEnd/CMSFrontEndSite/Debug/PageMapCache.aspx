﻿<%@ Page Language="C#" Trace="true"  %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.Global" %>
<%@ Import Namespace="Bridgeline.FW.UI.PageMapEngine" %>
<%@ Import Namespace="Bridgeline.FW.UI.PageEngine" %>

<%@ Import Namespace="System.Data" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        
      var pageMap = Bridgeline.FW.UI.PageMapEngine.PageMapService.GetPageMap();
      Response.Write(string.Format("{0} : <strong> {1} </strong><br />", "PageMap LastModifiedDate: ", pageMap.LastModifiedDate));

        
        Bridgeline.FW.Global.FWBusinessObject bo = new FWBusinessObject(typeof(Bridgeline.FW.UI.PageMapEngine.PageMap));
        bo["siteId"] = GlobalManager.GetSiteContext();
        DataRow dr = null;
        dr = bo.ExecuteDataRow("GetRoot");
        bo.Dispose();
        Response.Write(string.Format("{0} : <strong> {1} </strong><br />", "PageMapBase from DB LastModifiedDate: ", dr["LastPageMapModificationDate"].ToString()));
        
        var dicIdToPageMap = Bridgeline.FW.UI.PageMapEngine.PageMapService.GetUrlToPageMap();
        rptPages.DataSource = dicIdToPageMap.Values.OrderBy(p => p.CompleteFriendlyUrl);
        rptPages.DataBind();

        if (Request.QueryString["GetFromDB"] != null && Request.QueryString["GetFromDB"] == "true")
        {
            FWBusinessObject pdbo = new FWBusinessObject(typeof(PageDefinition));
            pdbo.Params["siteId"] = GlobalManager.GetSiteContext(); ;
            DataSet dsPageDefinitionNodes = pdbo.ExecuteDataSet("Get");
            DataTable dt = dsPageDefinitionNodes.Tables[0];
            rptPagesFromDB.DataSource = dt;
            rptPagesFromDB.DataBind();
            pdbo.Dispose();
        }
        
        System.Diagnostics.Trace.Flush(); 
        /*foreach (var item in dicIdToPageMap.OrderBy(p => p.Value.CompleteFriendlyUrl))
        {
            Response.Write(string.Format("{0} : {1}<br />", item.Value.Id, item.Key));
        }

        Response.Write("<br />-----------------------------<br />");

        var urlRules = Bridgeline.FW.UI.UrlRewriter.UrlRewriteRulesService.GetAllRules();
        
        for (int i = 0; i < urlRules.Count; i++)
		{
            Response.Write(string.Format("{0} : {1}<br />", 
                HttpUtility.UrlDecode(urlRules[i].LookFor), urlRules[i].SendTo));
        }

        Response.Write("<br />-----------------------------<br />");

        Response.Write(string.Format("MachineName: {0}<br />", Environment.MachineName));*/

    }

</script>
<style>
    table {
        column-span: 0;
        border-collapse: collapse;
        width: 100%;
    }
    td {
        border:solid 1px #d2d2d2;
        padding: 2px 6px;
    }
</style>

<table>
    <tr>
        <td>SNo</td>
        <td>Title</td>
        <td>Url</td>
        <td>Id</td>
        <td>CreatedDate</td>
        <td>PublishDate</td>
    </tr>
    <asp:repeater id="rptPages" runat="server">
    <itemtemplate>
        <tr>
            <td style="width: 3%"><%#Container.ItemIndex + 1  %></td>
            <td style="width: 12%"><%#Eval("Title")  %></td>
            <td style="width: 30%"><%#Eval("CompleteFriendlyUrl")  %></td>
            <td style="width: 10%"><%#Eval("Id")  %></td>
            <td style="width: 10%"><%#Eval("CreatedDate")  %></td>
            <td style="width: 10%"><%#Eval("PublishDate")  %></td>
        </tr>
    </itemtemplate>
</asp:repeater>
</table>
<br /> <br /> <br />
<strong> FROM DB </strong>
<table>
    <tr>
        <td>SNo</td>
        <td>Title</td>
       
    </tr>
    <asp:repeater id="rptPagesFromDB" runat="server">
    <itemtemplate>
        <tr>
            <td style="width: 3%"><%#Container.ItemIndex + 1  %></td>
            <td style="width: 20%"><%#Eval("Title")  %></td>
            
        </tr>
    </itemtemplate>
</asp:repeater>
</table>
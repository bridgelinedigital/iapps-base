﻿<%@ Page Language="C#" Trace="false"  %>

<%@ Register Tagprefix="Utilities" 
   Namespace="System.Net.Http" 
   Assembly="System.Net.Http, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string credentials = Base64Encode(Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("SSO.Bridgeline.ClientId") + ":" + Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("SSO.Bridgeline.ClientSecret"));

        System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls12;
        
        using (var client = new System.Net.Http.HttpClient())
        {
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", credentials);

            var url = Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("SSO.Bridgeline.EndPoint") + "/oauth2/token";

            var param = new Dictionary<string, string>();
            param.Add("grant_type", "authorization_code");
            param.Add("client_id", Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("SSO.Bridgeline.ClientId"));
            param.Add("code", Request.QueryString["code"]);
            param.Add("redirect_uri", Bridgeline.FW.Global.ConfigHelper.FWAppSettings.GetStringValue("SSO.Bridgeline.Callback"));

            var httpContent = new System.Net.Http.FormUrlEncodedContent(param);

            var response = client.PostAsync(url, httpContent).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            Response.Write(response.IsSuccessStatusCode);
            Response.Write(result);
        }

    }

    private string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }
</script>



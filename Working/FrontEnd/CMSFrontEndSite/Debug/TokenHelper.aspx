﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Register Tagprefix="Utilities" 
   Namespace="System.Web.Http" 
   Assembly="System.Web.Http, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register Tagprefix="Contract" 
   Namespace="Bridgeline.Framework.Contract" 
   Assembly="Bridgeline.Framework.Contract, Version=5.2.0.0, Culture=neutral, PublicKeyToken=d94118ec66347585" %>
<%@ Import Namespace="System.Web.Http" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        btnGetToken.Click += btnGetToken_Click;
        btnParseToken.Click += btnParseToken_Click;
    }

    void btnParseToken_Click(object sender, EventArgs e)
    {
        var service = new Bridgeline.Framework.Provider.TokenProvider();
        ltToken.Text = service.ParseMergeTokens(txtAttributeName.Text);
    }

    void btnGetToken_Click(object sender, EventArgs e)
    {
        var service = new Bridgeline.Framework.Service.AttributeService();

        var request = new Bridgeline.Framework.Model.AttributeEntityRequest();
        request.Category = (int)Bridgeline.Framework.Model.Enums.AttributeCategory.Contact;
        
        var response = service.GetItems(request);

        ltToken.Text = string.Empty;
        if (response != null && response.Items.Count() > 0)
        {
            var item = response.Items.Where(a => a.Title.ToLower() == txtAttributeName.Text.ToLower());
            if (item != null && item.Count() > 0)
                ltToken.Text = string.Format("Token for <strong>'{0}'</strong> is: [Field:{1}]", item.FirstOrDefault().Title, item.FirstOrDefault().Id);
        }

        if (string.IsNullOrEmpty(ltToken.Text))
            ltToken.Text = string.Format("Cannot find the attribute <strong>'{0}'</strong>", txtAttributeName.Text);
    }

</script>

<style>
    .container {
        border:solid 1px #d2d2d2;
        padding:25px;
        margin:25px;
        width: 500px;
    }
</style>

<FWControls:FWActionLessForm runat="server" ID="form1" method="post">
    <fieldset class="container">
        <legend>Token Helper</legend>
        <div>
            <asp:textbox id="txtAttributeName" runat="server" width="250"></asp:textbox>
            <asp:button id="btnGetToken" runat="server" Text="Get Token" />
            <asp:button id="btnParseToken" runat="server" Text="Parse Token" />
        </div>
        <div style="margin-top:20px;">
            <asp:literal id="ltToken" runat="server" />
        </div>
    </fieldset>
</FWControls:FWActionLessForm>

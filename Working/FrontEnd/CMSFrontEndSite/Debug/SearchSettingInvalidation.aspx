﻿<%@ Page Language="C#" %>

<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

    }

    protected void Submit_Setting(object sender, EventArgs e)
    {
        var sh = new Bridgeline.Framework.Search.SearchSetting();
        var searchSettingValue = sh.GetSearchSettingValue(txtSearchSetting.Text);
        Response.Write("Value: " + searchSettingValue);
    }
</script>
<form runat="server" id="form1">
    <div>
        <asp:textbox id="txtSearchSetting" runat="server"></asp:textbox>
        <asp:button id="submitbtn" runat="server" text="Get Search SettingValue" onclick="Submit_Setting" />
    </div>
</form>

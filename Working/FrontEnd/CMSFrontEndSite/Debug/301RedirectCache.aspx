﻿<%@ Page Language="C#" %>
<%@ Import Namespace="Bridgeline.FW.UI" %>
<%@ Import Namespace="Bridgeline.FW.UI.PageMapEngine" %>
<script runat="server">

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        rptRedirects.ItemDataBound += new RepeaterItemEventHandler(rptRedirects_ItemDataBound);
        GetRedirectCache();
    }
    private void GetRedirectCache()
    {
        Dictionary<string, PageRedirect> lstRedirects = PageRedirectService.GetCachedRedirects();
        rptRedirects.DataSource = lstRedirects.Values;
        rptRedirects.DataBind();
    }
    public void rptRedirects_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            PageRedirect p = e.Item.DataItem as PageRedirect;

            Literal ltlOldUrl = e.Item.FindControl("ltlOldUrl") as Literal;
            Literal ltlNewUrl = e.Item.FindControl("ltlNewUrl") as Literal;

            ltlOldUrl.Text = p.OldUrl;
            ltlNewUrl.Text = p.NewUrl;
        }
    }


</script>
<form runat="server" id="form1">
    <div>
        <asp:repeater id="rptRedirects" runat="server" >
            <itemtemplate>
                <br />
               <strong> Old Url: </strong><asp:Literal id="ltlOldUrl" runat="server"></asp:Literal>
                <strong> New Url: </strong> <asp:Literal id="ltlNewUrl" runat="server"></asp:Literal>

            </itemtemplate>
    </asp:repeater>
   </div>
</form>

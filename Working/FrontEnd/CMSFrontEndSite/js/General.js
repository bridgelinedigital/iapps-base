﻿/* Copyright Bridgeline Digital, Inc. An unpublished work created in 2009. All rights reserved. This software contains the confidential and trade secret information of Bridgeline Digital, Inc. ("Bridgeline").  Copying, distribution or disclosure without Bridgeline's express written permission is prohibited */
// JScript File
//Global variables for toolbar and inline editing
var editable = true;
var autohide = true;
var varPopMenu = false;
var objDoc;
var oureditor;
/** Browser Detection Code **/
var detect = navigator.userAgent.toLowerCase();
var OS, browser, version, total, thestring, ie6 = false;
if (navigator.appVersion.substr(22, 2) == "6.") { ie6 = true; }
if (checkIt('safari')) browser = "safari";
else if (checkIt('msie')) browser = "ie";
else if (!checkIt('compatible')) {
    browser = "mozilla";
}
function checkIt(string) {
    place = detect.indexOf(string) + 1;
    thestring = string;
    return place;
}
/** Browser Detection Code **/

var flashEvents = "";
function _uFlash() {
    var f = "-", n = navigator;
    if (n.plugins && n.plugins.length) {
        for (var ii = 0; ii < n.plugins.length; ii++) {
            if (n.plugins[ii].name.indexOf('Shockwave Flash') != -1) {
                f = n.plugins[ii].description.split('Shockwave Flash ')[1];
                break;
            }
        }
    }
    else if (window.ActiveXObject) {
        for (var ii = 10; ii >= 2; ii--) {
            try {
                var fl = eval("new ActiveXObject('ShockwaveFlash.ShockwaveFlash." + ii + "');");
                if (fl) {
                    f = ii + '.0';
                    break;
                }
            }
            catch (e) {
            }
        }
    }
    return f;
}
function _uVoid() { return; }
function _uUnloadEvent() { return; }
function _uEvent100100() { return; }

//function to hide and un-hide the toolbar
function alterToolBar() {
    if (autohide == true) {
        autohide = false;
        //document.getElementById('bar').style.top = (objDoc.scrollTop - 27) + 'px';
        if (document.getElementById("expandArrow") != null) {
            if (document.getElementById('bar') != null)
                document.getElementById('bar').style.top = '-27px';
            document.getElementById("expandArrow").src =  jPublicSiteRootUrl+"/iapps_images/toolbar-expand.png";
            document.getElementById("expandArrow").title = __JSMessages["ClickToExpandToolbar"];
            document.getElementById("expandArrow").alt = __JSMessages["ClickToExpandToolbar"];
            if (ie6) {
                cover(44, 13, document.getElementById("autohide"));
            }
        }
    }
    else {
        autohide = true;
        //document.getElementById('bar').style.top = objDoc.scrollTop + 'px';
        if (document.getElementById("expandArrow") != null) {
            if (document.getElementById('bar') != null)
                document.getElementById('bar').style.top = '0px';
            document.getElementById("expandArrow").src = jPublicSiteRootUrl+"/iapps_images/toolbar-collapse.png";
            document.getElementById("expandArrow").title = __JSMessages["ClickToCollapseToolbar"];
            document.getElementById("expandArrow").alt = __JSMessages["ClickToCollapseToolbar"];
            if (ie6) {
                if (document.getElementById('bar') != null)
                    cover(1001, 27, document.getElementById('bar'));
                if (document.getElementById('autohide') != null)
                    cover(44, 13, document.getElementById('autohide'));
            }
        }
    }
}

function cover(width, height, coverObject) {
    var ieMat = null; //for IE6 iframe shim
    // Cover <select> elements with <iframe> elements only in IE < 7
    if (navigator.appVersion.substr(22, 2) == "6.") {
        if (ieMat == null) {
            ieMat = document.createElement('iframe');
            if (document.location.protocol == "https:")
                ieMat.src = "//0";
            else if (window.opera != "undefined")
                ieMat.src = "";
            else
                ieMat.src = "javascript:false";
            ieMat.scrolling = "no";
            ieMat.frameBorder = "0";
            ieMat.style.position = "absolute";
            ieMat.style.top = coverObject.offsetTop + "px";
            ieMat.style.left = coverObject.offsetLeft + "px";
            ieMat.style.width = width + "px";
            ieMat.style.height = height + "px";
            ieMat.style.filter = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)";
            ieMat.style.zIndex = "-1";
            if (document.getElementById('bar') != null)
                document.getElementById('bar').insertBefore(ieMat, document.getElementById('toolbarContainer'));
        }
    }
}

//function to scroll the toolbar when the user scrolls through the page
function scrollAdjust() {
    if (document.getElementById('bar') != null && objDoc != null) {
        if (autohide) {
            document.getElementById('bar').style.top = (objDoc.scrollTop) + 'px';
        }
        else {
            document.getElementById('bar').style.top = (objDoc.scrollTop - 27) + 'px';
        }
    }
}

function HideInfo() {
    if (document.getElementById("dhtmltooltip") != null)
        document.getElementById("dhtmltooltip").style.visibility = "hidden";
}

function ExtraInformationPosition(e) {

    var ie5 = document.all && document.getElementById;
    var ns6 = document.getElementById && !document.all;
    var menuobj = document.getElementById("dhtmltooltip");
    //Find out how close the mouse is to the corner of the window
    var rightedge = ie5 ? (document.documentElement.clientWidth - e.clientX) : (window.innerWidth - e.clientX);
    var bottomedge = ie5 ? (document.documentElement.clientHeight - e.clientY) : (window.innerHeight - e.clientY);

    //if the horizontal distance isn't enough to accomodate the width of the context menu
    if (rightedge < menuobj.offsetWidth) {
        //move the horizontal position of the menu to the left by it's width
        xLeft = ie5 ? document.documentElement.scrollLeft + e.clientX - menuobj.offsetWidth : window.pageXOffset + e.clientX - menuobj.offsetWidth;
    }
    else {
        //position the horizontal position of the menu where the mouse was clicked
        xLeft = ie5 ? document.documentElement.scrollLeft + e.clientX : window.pageXOffset + e.clientX;
    }
    //if the vertical distance isn't enough to accomodate the height of the context menu
    if (bottomedge < menuobj.offsetHeight) {
        //move the vertical position of the menu up by it's height
        yTop = ie5 ? document.documentElement.scrollTop + e.clientY - menuobj.offsetHeight : window.pageYOffset + e.clientY - menuobj.offsetHeight;
    }
    else {
        if (browser == "safari") {
            //position the vertical position of the menu where the mouse was clicked
            yTop = ie5 ? document.documentElement.scrollTop + e.clientY : e.clientY;
        }
        else {
            //position the vertical position of the menu where the mouse was clicked
            yTop = ie5 ? document.documentElement.scrollTop + e.clientY : window.pageYOffset + e.clientY;
        }
    }

    menuobj.style.visibility = "visible";
    menuobj.style.top = yTop + 'px';
    menuobj.style.left = xLeft + 'px';
    return false;
}
function hideEmptyContainer() {
    if (!document.getElementById("autohide") || !document.getElementById("jsInclude")) {

        var y;
        var child = document.getElementById("outer");
        var contentDivString = document.getElementById("contentDivString");
        contentDivs = contentDivString.value.split(",");

        if (contentDivs.length >= 0) {
            for (var i = 0; i < (contentDivs.length); i++) {

                var ele = document.getElementById(contentDivs[i]);

                if (ele != "" || ele != null) {

                    for (var j = 0; j < ele.childNodes.length; j++) {

                        if (ele.childNodes[j].nodeName == "DIV") {
                            if (ele.childNodes[j].getAttribute("FwObjectId") == '00000000-0000-0000-0000-000000000000') {
                                ele.style.display = "none";
                            }
                        }
                    }
                }
            }
        }
    }
}

function hideEmptySideBarContainer() {
    if (!document.getElementById("autohide") || !document.getElementById("jsInclude")) {
        var y;
        var child = document.getElementById("outer");
        var contentDivString = document.getElementById("sidebarDivString");

        contentDivs = contentDivString.value.split(",");

        if (contentDivs.length >= 0) {
            for (var i = 0; i < (contentDivs.length); i++) {

                var ele = document.getElementById(contentDivs[i]);
                if (ele != "" || ele != null) {

                    for (var j = 0; j < ele.childNodes.length; j++) {
                        if (ele.childNodes[j].nodeName == "DIV") {
                            if (ele.childNodes[j].getAttribute("FwObjectId") == '00000000-0000-0000-0000-000000000000') {
                                ele.style.display = "none"
                            }
                        }
                    }
                }
            }
        }
    }
}
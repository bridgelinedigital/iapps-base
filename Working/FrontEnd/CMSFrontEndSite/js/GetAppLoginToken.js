﻿/* Copyright Bridgeline Digital, Inc. An unpublished work created in 2009. All rights reserved. This software contains the confidential and trade secret information of Bridgeline Digital, Inc. ("Bridgeline").  Copying, distribution or disclosure without Bridgeline's express written permission is prohibited */
function sendRequestInSite(url, callback, postData) {
    var req = createXMLHTTPObjectInSite();
    if (!req) return;
    var method = (postData) ? "POST" : "GET";
    req.open(method, url, true);
    req.setRequestHeader('User-Agent', 'XMLHTTP/1.0');
    if (postData)
        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function () {
        if (req.readyState != 4) return;
        if (req.status != 200 && req.status != 304) {
            return;
        }
        callback(req);
    }
    req.send(postData);
    return;
}

function pausecompInSite(sec) {
    var millis = sec * 1000;
    var date = new Date();
    var curDate = null;

    do { curDate = new Date(); }
    while (curDate - date < millis);
}

var XMLHttpFactoriesInSite = [
	function () { return new XMLHttpRequest() },
	function () { return new ActiveXObject("Msxml2.XMLHTTP") },
	function () { return new ActiveXObject("Msxml3.XMLHTTP") },
	function () { return new ActiveXObject("Microsoft.XMLHTTP") }
];

function createXMLHTTPObjectInSite() {
    var xmlhttp = false;
    for (var i = 0; i < XMLHttpFactoriesInSite.length; i++) {
        try {
            xmlhttp = XMLHttpFactoriesInSite[i]();
        }
        catch (e) {
            continue;
        }
        break;
    }
    return xmlhttp;
}

function handleResponseInSite(req) {
    var callResponse = req.responseText;
    var emptyGuid = '00000000-0000-0000-0000-000000000000';
    if (callResponse.length == emptyGuid.length) {
        if (callResponse != 'Invalid') {
            //check '?' we have to add here
            if (navigateURL!='' && navigateURL.indexOf('?') > -1) {
                destinationUrl = navigateURL + '&Token=' + callResponse;
            }
            else {
                destinationUrl = navigateURL + '?Token=' + callResponse;
            }
            //Modified by AV
            setTimeout(functionToCallAfterTokenCallback, 1000);
            // ENd Modified
        }
    }
}

var itemContext;
var navigateURL;
var destinationUrl = "";
function SendTokenRequest(sender, eventArgs) {
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    if (eventArgs != null) {
        itemContext = eventArgs.get_item().get_id();
        navigateURL = eventArgs.get_item().get_navigateUrl();
        if (itemContext == 'GoToHome' || itemContext == 'ViewPageHistory' || itemContext == 'ViewEditProperties') {
            urlToCall = urlToCall + "&removeSession=True";
            sendRequestInSite(urlToCall, handleResponseInSite, "GET");
            //Modified by AV
            functionToCallAfterTokenCallback = "RedirectInSite()";
            // End Modified
            eventArgs.set_cancel(true);
        }
    }
}

//Added By AV
var functionToCallAfterTokenCallback;
//End Added 
function SendTokenRequestByURLInSite(redirectURL) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    //Modified by AV
    functionToCallAfterTokenCallback = "RedirectInSite()";
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");
    //End Modified
}

function SendTokenRequestByURLWithMethodName(redirectURL, methodName) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    //Modified By AV
    functionToCallAfterTokenCallback = methodName;
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");

    //End Modified
}

function SendTokenRequestByURLWithMethodNameForCommerce(redirectURL, methodName) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jCommerceProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    //Modified By AV
    functionToCallAfterTokenCallback = methodName;
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");

    //End Modified
}

function SendTokenRequestByURLWithMethodNameForMarketier(redirectURL, methodName) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jMarketierProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    //Modified By AV
    functionToCallAfterTokenCallback = methodName;
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");

    //End Modified
}

function RedirectInSite() {
    if (destinationUrl != "") {
        window.location = destinationUrl;
    }
}

function SendTokenRequestByURLInSiteWithEditor(redirectURL) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    //Modified by AV
    functionToCallAfterTokenCallback = "ShowEditorWithToken(destinationUrl)";
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");
    //End Modified
}

function SendTokenRequestByURLInSiteWithEditorForInsertImage(redirectURL, methodName) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    //Modified by AV
    functionToCallAfterTokenCallback = methodName + "(destinationUrl)";
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");
    //End Modified
}

function SendTokenRequestByURLInSiteWithEditorForInsertProductImage(redirectURL) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jCommerceProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    functionToCallAfterTokenCallback = "ShowEditorWithTokenForInsertProductImage(destinationUrl)";
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");
}

function SendTokenRequestByURLInSiteWithEditorForInsertLinkToProduct(redirectURL) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jCommerceProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    functionToCallAfterTokenCallback = "ShowEditorWithTokenForInsertLinkToProduct(destinationUrl)";
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");
}

//added by Sambath
function SendTokenRequestByURLInSiteWithEditorForEditImage(redirectURL) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    functionToCallAfterTokenCallback = "ShowEditorWithTokenForEditImage(destinationUrl)";
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");
}

function SendTokenRequestByURLInSiteForMangeLink(redirectURL) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jMarketierProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    //Modified by AV
    functionToCallAfterTokenCallback = "ShowEditorWithTokenForManageLink(destinationUrl)";
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");
    //End Modified
}

function SendTokenRequestByURLInSiteWithEditorForInsertFlash(redirectURL) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    //Modified by AV
    functionToCallAfterTokenCallback = "ShowEditorWithTokenForInsertFlash(destinationUrl)";
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");
    //End Modified
}

function SendTokenRequestByURLInSiteWithEditorForSnippetManager(redirectURL) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jProductId + "&userName=" + juserName;
    var urlToCall = jPublicSiteUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    //Modified by AV
    functionToCallAfterTokenCallback = "ShowEditorWithTokenForSnippetManager(destinationUrl)";
    sendRequestInSite(urlToCall, handleResponseInSite, "GET");
    //End Modified
}
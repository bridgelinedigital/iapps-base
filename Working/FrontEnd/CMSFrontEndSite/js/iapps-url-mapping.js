﻿var iAppsPopupMappings = {
    "SelectFormLibraryPopup": "/selectlibrary-popup/38",
    "SelectContentLibraryPopup": "/Popups/SelectContentLibraryPopup.aspx",
    "SelectListPopup": "/Popups/SelectListPopup.aspx",
    "SelectManualListPopup": "/Popups/SelectManualListPopup.aspx",
    "SelectSharepointLibrary": "/Popups/SelectSharepointLibrary.aspx",
    "SelectAdminUserPopup": "/Popups/SelectAdminUserPopup.aspx",
    "ManagePageDetails": "/Popups/ManagePageDetails.aspx",
    "ManageContentDetails": "/Popups/ManageContentDetails.aspx",
    "ManageAssetFileDetails": "/Popups/ManageAssetFileDetails.aspx",
    "ManageAssetImageDetails": "/Popups/ManageAssetImageDetails.aspx",
    "ManageFormDetails": "/Popups/ManageFormDetails.aspx",
    "EditLeadFormPopup": "/Popups/EditLeadForm.aspx",
    "ManageContentDefinitionDetails": "/Popups/ManageContentDefinitionDetails.aspx",
    "ManageScriptDetails": "/Popups/ManageScriptDetails.aspx",
    "ManageStyleDetails": "/Popups/ManageStyleDetails.aspx",
    "ManageTemplateDetails": "/Popups/ManageTemplateDetails.aspx",
    "ManagePostDetails": "/Popups/ManagePostDetails.aspx",
    "ManageSiteDetails": "/Popups/ManageSiteDetails.aspx",
    "ManageAdminUserDetails": "/Popups/ManageAdminUserDetails.aspx",
    "ManagePageMapNodeDetails": "/Popups/ManagePageMapNodeDetails.aspx",
    "ManageSecurityLevelDetails": "/Popups/ManageSecurityLevelDetails.aspx",
    "ManageComments": "/Popups/ManageComments.aspx",
    "ManageDisplayOrder": "/Popups/ManageDisplayOrder.aspx",
    "ManagePageVariants": "/Popups/ManagePageVariants.aspx",
    "ManagePermissionsByTarget": "/Popups/ManagePermissionsByTarget.aspx",
    "ManageTrackingCodes": "/Popups/ManageTrackingCodes.aspx",
    "EditImage": "/Popups/ImageEditor.aspx",
    "FormDefinitionPopup": "/Popups/FormDefinitionPopup.aspx",
    "FormPreview": "/Popups/FormPreview.aspx",
    "ViewPagesUsingObject": "/Popups/ViewPagesUsingObject.aspx",
    "DeletePageWarning": "/Popups/DeletePageWarning.aspx",
    "ImageEditor": "/Popups/ImageEditor.aspx",
    "ImageQuickUpload": "/ImageQuickUpload.aspx",
    "PreviewImagePopup": "/Popups/ShowAssetImageInfo.aspx",
    "MoveCopyPage": "/Popups/MoveCopyPage.aspx",
    "MoveCopyContent": "/Popups/MoveCopyContent.aspx",
    "MoveAssetFile": "/Popups/MoveAssetFile.aspx",
    "SelectTargetDirectory": "/Popups/SelectTargetDirectory.aspx",
    "AssignIndexTermsPopup": "/Popups/AssignTagsPopup.aspx",
    "ShowHyperLinkManager": "/Popups/ShowHyperLinkManager.aspx",
    "ShowSnippetManager": "/Popups/ShowSnippetManager.aspx",
    "ShowFlashManager": "/Popups/ShowFlashManager.aspx",
    "FileHistoryPopup": "/Popups/FileHistoryPopup.aspx",
    "EditContentDataViewProperties": "/Popups/CodeLibrary/EditContentDataViewProperties.aspx",
    "EditMenuDataViewProperties": "/Popups/CodeLibrary/EditMenuDataViewProperties.aspx",
    "EditDataViewProperties": "/Popups/CodeLibrary/EditDataViewProperties.aspx",
    "EditListDataViewProperties": "/Popups/CodeLibrary/EditListDataViewProperties.aspx",
    "SelectListType": "/Popups/CodeLibrary/SelectListType.aspx",
    "IndexTermFilter": "/Popups/CodeLibrary/IndexTermFilter.aspx",
    "PopulateManualList": "/Popups/PopulateManualListPopup.aspx",
    "DeactivateCMSGroup": "/Popups/DeactivateUserPopup.aspx",
    "ChangePassword": "/General/ChangePassword.aspx",
    "SubmitToWorkFlow": "/Popups/SubmitIntoWorkflow.aspx",
    "ViewWorkflowExecution": "/Popups/ViewWorkflowExecution.aspx",
    "ViewWorkflow": "/Popups/ViewWorkflowPopup.aspx",
    "ViewWorkflowPages": "/Popups/ViewPagesInWorkflow.aspx",
    "AddBrightCovePlayerPopup": "/Popups/AddBrightCovePlayerPopup.aspx",
    "PageContentVariants": "/Popups/VariantContentListing.aspx",
    "BlogSettings": "/Popups/ManageBlogSettings.aspx",
    "ImportPages": "/Popups/ImportPages.aspx",
    "ImportContents": "/Popups/ImportContents.aspx",
    "ResetCachePopup": "/Popups/ResetCachePopup.aspx",
    "TranslationAssetDetails": "/Popups/TranslationAssetDetails.aspx",
    "ReimportFromMaster": "/Popups/ReimportFromMaster.aspx",
    "SubmitForTranslation": "/Popups/SubmitForTranslation.aspx",
    "SendPageLink": "/Popups/SendPageLink.aspx",
    "AssignWorkflowWarning": "/Popups/AssignWorkflowWarning.aspx",
    "ManageAssetStructureSettings": "/Popups/ManageAssetStructureSettings.aspx",
    "ViewGroupDefinitions": "/Popups/ViewGroupDefinitions.aspx",
    "InsertVideo": "/Popups/InsertVideo.aspx",
    "TranslatedProperties": "/Popups/TranslatedProperties.aspx",
    "EditSiteGrouping": '/Popups/ManageSiteGrouping.aspx',
    "ManageSiteHierarchy": "/Popups/ManageSiteHierarchy.aspx",
    "ManageBestBetQuery": "/Popups/ManageBestBetQuery.aspx",
    "SelectBlogLibraryPopup": "/Popups/SelectBlogLibraryPopup.aspx",
    "ApplyMutliCSSClasses": "/Popups/ApplyMutliCSSClasses.aspx",
    "CheckUsage": "/Popups/CheckUsage.aspx",
    "ContentDefinitionPopup": "/Libraries/Data/ContentDefinitionPopup.aspx",
    "ManageAttributeDetails": "/Popups/ManageAttributeDetails.aspx",
    "ManageAttributeCategoryDetails": "/Popups/ManageAttributeCategoryDetails.aspx",
    "SelectAttributePopup": "/Popups/SelectAttributePopup.aspx",
    "HtmlEditor": "/Popups/HtmlEditor.aspx",
    "FileUploader": "/Popups/FileUploader.aspx",
    "AssignAttributes": "/Popups/AssignAttributes.aspx",
    "ManageChannelDetails": "/Popups/ManageChannelDetails.aspx",
    "CreateForm": "/Popups/CreateForm.aspx",
    "SelectSitePopup": "/Popups/SelectSitePopup.aspx",
    "ViewDistributionItems": "/Popups/ViewDistributionItems.aspx",
    "ManageDistributionRun": "/Popups/ManageDistributionRun.aspx",
    "SubmitForDistribution": "/Popups/SubmitForDistribution.aspx",
    "ManageAddressDetails": "/Popups/ManageAddressDetails.aspx",
    "SubmitForSynchronization": "/Popups/SubmitForSynchronization.aspx",
    "AssignDistributionPopup": "/Popups/AssignDistributionPopup.aspx",
    "SelectMenuPopup": "/Popups/SelectMenuPopup.aspx",
    "FormEmbedCode": "/Popups/FormInstallationCode.aspx",
    "SelectSiteGroupPopup": "/Popups/SelectSiteGroupPopup.aspx"

};
var iAppsCommercePopupMappings = {
    "NavCategoryFilter": "/Popups/NavCategory/NavCategoryFilter.aspx",
    "SelectProductPopup": "/Popups/StoreManager/SelectProductSkuPopup.aspx",
    "SelectProductImage": "/Popups/StoreManager/SelectProductImagePopup.aspx",
    "ManageCouponRule": "/Popups/StoreSetup/Coupons/ManageCouponRule.aspx",
    "AddProductSkuPopup": "/Popups/StoreManager/AddProductSkuPopup.aspx",
    "PreviewCoupon": "/Popups/StoreSetup/Coupons/PreviewCoupon.aspx",
    "CodeImportWarning": "/Popups/StoreSetup/Coupons/CodeImportWarning.aspx",
    "SelectCustomerGroups": "/Popups/Administration/SelectCustomerGroups.aspx",
    "FilterProducts": "/Popups/StoreSetup/Coupons/ProductFilter.aspx",
    "ProductAttributeSearch": "/Popups/StoreManager/ProductAttributeSearch.aspx",
    "ProductTypeValue": "/Popups/StoreManager/ProductTypeValuePopup.aspx",
    "PreviewProducts": "/Popups/StoreManager/PreviewProducts.aspx",
    "MultiSelectCustomer": "/Popups/StoreManager/Customers/MultiSelectCustomer.aspx",
    "SearchCustomer": "/Popups/StoreManager/Customers/SearchCustomer.aspx",
    "SearchCoupons": "/Popups/StoreSetup/Coupons/SearchCoupons.aspx",
    "AddProductPopup": "/Popups/StoreManager/AddProductSkuPopup.aspx",
    "SelectShippingMethod": "/Popups/StoreManager/Orders/SelectShippingMethod.aspx",
    "PersonalizedSKUAttribute": "/Popups/StoreManager/Orders/PersonalizedSKUAttribute.aspx",
    "ApplyPaymentToOrder": "/Popups/StoreSetup/Payments/ApplyPaymentToOrder.aspx",
    "ShipOrder": "/Popups/StoreManager/Orders/ShipOrder.aspx",
    "ViewShipments": "/Popups/StoreManager/Orders/ViewShipments.aspx",
    "TrackOrder": "/Popups/StoreManager/Orders/TrackOrder.aspx",
    "ShippingLabel": "/Popups/StoreManager/Orders/ShippingLabel.aspx",
    "InitiateReturn": "/StoreManager/Orders/InitiateReturn.aspx",
    "RefundAmount": "/Popups/StoreManager/Orders/RefundAmount.aspx",
    "CaptureAmount": "/Popups/StoreManager/Orders/CaptureAmount.aspx",
    "ShowReasonPopup": "/Popups/StoreManager/Orders/StatusChangeReason.aspx",
    "ProductTypeMultiSelect": "/Popups/StoreManager/ProductTypeMultiSelect.aspx",
    "GlobalMultiAttributeValue": "/Popups/StoreManager/GlobalMultiAttributeValue.aspx",
    "HtmlEditor": "/Popups/HTMLEditor.aspx",
    "ShowManageSkuPopup": "/Popups/StoreManager/SkuPriceSets.aspx"
};

var iAppsMarketierPopupMappings = {
    "InsertManagedLink": "/selectlibrary-popup/31",
    "EmailRunHistory": "/Popups/EmailRunHistory.aspx",
    "SelectEmailRecipients": "/Popups/SelectEmailRecipients.aspx",
    "ScheduleEmailSend": "/Popups/ScheduleEmailSend.aspx",
    "ConfirmEmailSchedule": "/Popups/ConfirmEmailSchedule.aspx",
    "ChooseTemplate": "/Popups/ChooseTemplate.aspx",
    "ViewLandingPages": "/Popups/ViewLandingPages.aspx",
    "MoveContacts": "/Popups/MoveContacts.aspx",
    "EmailBestPractice": "/Popups/EmailBestPractice.aspx",
    "SendTestEmail": "/Popups/SendTestEmail.aspx",
    "TextEmail": "/Popups/TextEmail.aspx",
    "AssignIndexTerms": "/Popups/AssignIndexTerms.aspx",
    "ViewAttachedContactLists": "/Popups/ViewAttachedContactLists.aspx",
    "AutoresponderStatistics": "/Popups/AutoresponderStatistics.aspx",
    "AssignToDistributionList": "/Popups/AssignToDistributionList.aspx",
    "WorkflowRejectionNotes": "/Popups/WorkflowRejectionNotes.aspx",
    "LeavingMarketierWarning": "/Popups/LeavingMarketierWarning.aspx",
    "Unsubscribe": "/Popups/Unsubscribe.aspx",
    "SpamCheck": "/Popups/RunSpamCheck.aspx",
    "ExportEmails": "/Popups/ExportEmails.aspx",
    "CampaignSendDetails": "/Popups/CampaignSendDetails.aspx",
    "ConfirmCampaignSend": "/Popups/ConfirmCampaignSend.aspx",
    "ManageCampaignDistribution": "/Popups/ManageCampaignDistribution.aspx",
    "ManageContactListDetails": "/Popups/ManageContactListDetails.aspx",
    "ManageBlackoutDates": "/Popups/ManageBlackoutDates.aspx",
    "DownloadTrackingCode": "/Popups/TrackingInstallationCode.aspx",
    "AddEditExternalLandingPage": "/Popups/AddEditExternalLandingPage.aspx",
    "ManageTrackingCodes": "/Popups/ManageTrackingCodes.aspx"
};

var iAppsAnalyzerPopupMappings = {
    "PageViewDetails": "/Popups/PageViewDetails.aspx",
    "LinkBuilder": "/Popups/LinkBuilder.aspx",
    "CampaignLinks": "/Popups/CampaignLinks.aspx",
    "SelectWatch": "/Popups/SelectWatch.aspx",
    "SelectFileLibraryPopup": "/Popups/SelectFileLibraryPopup.aspx",
    "SelectImageLibraryPopup": "/Popups/SelectImageLibraryPopup.aspx",
    "SelectPageLibraryPopup": "/Popups/SelectPageLibraryPopup.aspx"
};

var iAppsSocialPopupMappings = {
    "ManageItems": "/Activity/CreateItem",
    "ShareMessage": "/Activity/ShareMessage",
    "SendItem": "/Activity/SendItem",
    "ViewEngagements": "/Activity/ViewEngagements",
    "Engage": "/Activity/Engage",
    "SocialAccounts": "/User/SocialAccounts",
    "DisplayErrorMessage": "/General/DisplayErrorMessage",
    "ReadMore": "/Activity/ReadMore",
    "SelectImageLibraryPopup": "/Popups/SelectImageLibraryPopup.aspx",
    "Retweet": "/Activity/Retweet",
    "Reply": "/Activity/Reply",
    "ViewUserDetails": "/User/ViewUserDetails",
    "ViewNotifications": "/SocialNotification/MessageNotifications",
    "ReplyTwitterConversation": "/Conversation/ReplyTwitterConversation",
    "DeleteConversationItem": "/Conversation/DeleteConversationItem"
};

var iAppsAdminMappings = {
    "ManageListDetails": "/Libraries/Data/ManageListDetails.aspx",
    "ManageContentLibrary": "/Libraries/Data/ManageContentLibrary.aspx",
    "ManagePageLibrary": "/page-landing",
    "ManageFileLibrary": "/Libraries/Data/ManageFileLibrary.aspx",
    "ManageImageLibrary": "/Libraries/Data/ManageImageLibrary.aspx",
    "ManageWebsiteUserDetails": "/Administration/User/ManageWebsiteUserDetails.aspx",
    "SubmitForTranslation": "/Popups/SubmitForTranslation.aspx",
    "ManageBlogs": "/Libraries/Data/ManageBlogs.aspx",
    "ManageAdminUserDetails": "/user-details",
    "ManageAdminUserPermissions": "/Administration/User/ViewEditPermissions.aspx",
    "ManageAdminGroupDetails": "/Administration/User/ManageAdminGroupDetails.aspx"
}

var iAppsCommerceMappings = {
    "ManageCustomerDetails": "/Administration/ManageCustomerDetails.aspx",
    "ManageCustomerGroupDetails": "/Administration/ManageCustomerGroupDetail.aspx",
    "PaymentMethods": "/StoreManager/Customers/Payments.aspx",
    "Shipping": "/StoreManager/Customers/Shipping.aspx",
    "Notes": "/StoreManager/Customers/CustomerNotes.aspx",
    "ViewOrders": "/StoreManager/Customers/CustomerOrderList.aspx",
    "NewOrders": "/StoreManager/Orders/NewOrders.aspx",
    "Logins": "/StoreManager/Customers/AccountLogins.aspx"
}

var iAppsSocialMappings = {
    "ManageAdminUserDetails": "/User/ManageUserDetails",
    "ManageAdminGroupDetails": "/Administration/User/SocialGroupDetails.aspx"
}

function GetAdminUrl() {
    var aUrl = "";
    if (typeof jCMSAdminSiteUrl != "undefined")
        aUrl = jCMSAdminSiteUrl;
    else if (typeof jAdminSiteUrl != "undefined")
        aUrl = jAdminSiteUrl;
    else if (typeof adminSiteUrl != "undefined")
        aUrl = adminSiteUrl;

    return aUrl;
}

// This method is called for opening popups
// callbackFunctionOnClose - This method will be called when popup is closed
// customAttributes - Key/Value pair that will be passed to popup as part of popup DTO
function OpeniAppsAdminPopup(actionName, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay) {
    var mUrl = GetAdminUrl() + iAppsPopupMappings[actionName];
    return OpeniAppsAdminPopup_("CMS", actionName, mUrl, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay);
}

function OpeniAppsCommercePopup(actionName, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay) {
    if (CheckLicense("Commerce", true)) {
        var mUrl = jCommerceAdminSiteUrl + iAppsCommercePopupMappings[actionName];
        return OpeniAppsAdminPopup_("Commerce", actionName, mUrl, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay);
    }
}

function OpeniAppsMarketierPopup(actionName, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay) {
    if (CheckLicense("Marketier", true)) {
        var mUrl = jmarketierAdminURL + iAppsMarketierPopupMappings[actionName];
        return OpeniAppsAdminPopup_("Marketier", actionName, mUrl, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay);
    }
}

function OpeniAppsAnalyzerPopup(actionName, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay) {
    if (CheckLicense("Analyzer", true)) {
        var mUrl = jAnalyticAdminSiteUrl + iAppsAnalyzerPopupMappings[actionName];
        return OpeniAppsAdminPopup_("Analyzer", actionName, mUrl, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay);
    }
}

function OpeniAppsSocialPopup(actionName, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay) {
    if (CheckLicense("Social", true)) {
        var mUrl = jSocialAdminURL + iAppsSocialPopupMappings[actionName];
        return OpeniAppsAdminPopup_("Social", actionName, mUrl, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay);
    }
}

function OpeniAppsAdminPopup_(productName, actionName, mUrl, querystringWithoutQuestionMark, callbackFunctionOnClose, customAttributes, delay) {
    if (typeof popupActionsJson != "undefined") {
        popupActionsJson.Key = actionName;
        popupActionsJson.Params = querystringWithoutQuestionMark;
        popupActionsJson.FolderId = EmptyGuid;
        popupActionsJson.SelectedItems = [];
        if (typeof customAttributes != "undefined" && customAttributes != null)
            popupActionsJson.CustomAttributes = customAttributes;

        popupActionsJson.Action = "Load";
    }

    if (querystringWithoutQuestionMark != null && querystringWithoutQuestionMark != "") {
        mUrl += "?" + querystringWithoutQuestionMark;
    }

    var productId = jProductId;
    if (typeof editorContext == "undefined" || editorContext != 'SiteEditor' || productName.toLowerCase() != "cms")
        productId = GetjProductId(productName);

    if (((typeof editorContext != "undefined" && editorContext == 'SiteEditor') || jProductId.toLowerCase() != productId.toLowerCase()) && mUrl.indexOf("Token=") == -1) {
        var token = FWCallback.GetAuthToken(juserName, jUserId, productId, jAppId, false);
        if (token != "") {
            if (mUrl.indexOf("?") > -1)
                mUrl += "&Token=" + token;
            else
                mUrl += "?Token=" + token;
        }
    }

    var currentProductName = GetCurrentProductName();
    if (typeof basePageType != "undefined" && basePageType == "emailbasepage")
        currentProductName = "marketier";

    if (mUrl.indexOf("?") > -1)
        mUrl += "&ParentProduct=" + currentProductName;
    else
        mUrl += "?ParentProduct=" + currentProductName;

    return OpeniAppsModalWindow(actionName, mUrl, callbackFunctionOnClose, delay)
}

function OpeniAppsModalWindow(actionName, mUrl, callbackFunctionOnClose, delay) {
    var openAfter = 200;
    if (typeof delay != "undefined" && !isNaN(delay))
        openAfter = parseInt(delay);

    var modalWindow = dhtmlmodal.open(actionName, 'iframe', mUrl, '', {
        OnClose: function () {
            if (callbackFunctionOnClose && callbackFunctionOnClose != "") {
                if (typeof popupActionsJson != "undefined" && popupActionsJson.Action != "Cancel")
                    eval(callbackFunctionOnClose + "();");
            }
        }, delay: openAfter
    });
    $("body").data("iAppsAdminPopupWindow", modalWindow);

    return modalWindow;
}

// This method should be called internally inside this file only
function CloseiAppsAdminPopup(popupJson) {
    if (popupJson)
        popupActionsJson = popupJson;

    if ($("body").data("iAppsAdminPopupWindow"))
        $("body").data("iAppsAdminPopupWindow").hide();
}

function CloseiAppsCommercePopup(popupJson) {
    CloseiAppsAdminPopup(popupJson);
}

// This method is called from popup to cancel the popup.
function CanceliAppsAdminPopup(reloadParent, message) {
    var pJson = null;
    if (typeof popupActionsJson != "undefined") {
        pJson = popupActionsJson;
        var action = "Cancel";
        if (typeof reloadParent != "undefined" && reloadParent == true)
            action = "Load";
        if (pJson != null)
            pJson.Action = action;
    }
    if (parent != null)
        parent.CloseiAppsAdminPopup(pJson);

    if (typeof message != "undefined" && message != "")
        alert(message);

    return false;
}

// Call this method from popup for selecting a node or grid items. This will assign the values to grid DTO and close popup
function SelectiAppsAdminPopup(message) {
    if (popupActionsJson.Action == "" || popupActionsJson.Action == "Load")
        popupActionsJson.Action = "Save";
    if (typeof gridActionsJson != "undefined") {
        popupActionsJson.FolderId = gridActionsJson.FolderId;
        popupActionsJson.SelectedItems = gridActionsJson.SelectedItems;
    }
    if (typeof treeActionsJson != "undefined") {
        popupActionsJson.SiteId = treeActionsJson.SiteId;
    }
    parent.CloseiAppsAdminPopup(popupActionsJson);

    if (typeof message != "undefined" && message != "")
        alert(message);
}

// Call this method from popup code behind. This will pass back the DTO from popup and close popup
function SaveiAppsAdminPopup(message) {
    if (typeof message != "undefined" && message != "")
        alert(message);

    popupActionsJson = JSON.parse($("#hdnPopupActionsDTO").val());
    if (popupActionsJson.Action == "" || popupActionsJson.Action == "Load")
        popupActionsJson.Action = "Save";
    parent.CloseiAppsAdminPopup(popupActionsJson);
}

function GetPopupActionParams() {
    var params = $.getQueryString(popupActionsJson.Params);

    var displayValue = "";
    if (typeof params["Id"] != "undefined" && params["Id"].length == 36)
        displayValue = stringformat("Id : {0}", params["Id"]);
    if (typeof params["NodeId"] != "undefined" && params["NodeId"].length == 36)
        displayValue += stringformat("NodeId : {0}", params["NodeId"]);

    return displayValue;
}

function JumpToLastVisitedFrontPage() {
    var pageUrl = jLastVisitedFrontEndPage;
    if (jLastVisitedFrontEndPage == null || jLastVisitedFrontEndPage == '')
        pageUrl = "Default.aspx";
    JumpToFrontPage(pageUrl);
}

function JumpToEmailFrontPage(pageUrl, querystringWithoutQuestionMark) {
    if (querystringWithoutQuestionMark != undefined && querystringWithoutQuestionMark != "")
        querystringWithoutQuestionMark += "&";
    else
        querystringWithoutQuestionMark = "";

    querystringWithoutQuestionMark += "iAppsEmailBuilder=true";
    return JumpToFrontPage(pageUrl, querystringWithoutQuestionMark);
}

function JumpToFrontPageBySite(siteId, pageUrl, querystringWithoutQuestionMark) {
    var siteUrl = FWCallback.GetPrimarySiteUrl(siteId);

    JumpToFrontPage(siteUrl + "/" + pageUrl, querystringWithoutQuestionMark);

    return false;
}

// To open a front page; should be called from admin page only
function JumpToFrontPage(pageUrl, querystringWithoutQuestionMark) {
    ShowiAppsLoadingPanel();
    if (typeof pageUrl == "undefined") pageUrl = "Default.aspx";
    if (pageUrl.indexOf("://") == -1)
        pageUrl = jPublicSiteUrl + "/" + pageUrl;

    window.location = GetFrontPageUrlWithToken(pageUrl, querystringWithoutQuestionMark);

    return false;
}

function GetFrontPageUrlWithToken(pageUrl, querystringWithoutQuestionMark) {
    var fullUrl = pageUrl;
    if (pageUrl.indexOf("Token=") == -1) {
        var token = FWCallback.GetContextUserAuthToken("CMS");
        if (token != "") {
            if (pageUrl.indexOf("?") > -1)
                pageUrl += "&Token=" + token;
            else
                pageUrl += "?Token=" + token;
        }

        return JumpToFrontPage_1(pageUrl, querystringWithoutQuestionMark);
    }
    else {
        return JumpToFrontPage_1(pageUrl, '');
    }
}

function JumpToFrontPage_1(fullUrl, querystringWithoutQuestionMark) {
    var qsCollection = $.getQueryString(querystringWithoutQuestionMark);
    if (querystringWithoutQuestionMark != null && querystringWithoutQuestionMark != "")
        fullUrl += "&" + querystringWithoutQuestionMark;

    if (typeof qsCollection["PageState"] == "undefined")
        fullUrl += "&PageState=Edit";

    if (jProductId == jMarketierProductId)
        fullUrl += "&LastVisitedMarketierAdminPageUrl=" + GetCurrentUrl();
    else if (jProductId == jCommerceProductId)
        fullUrl += "&LastVisitedeCommerceAdminPageUrl=" + GetCurrentUrl();
    else if (jProductId == jSocialProductId)
        fullUrl += "&LastVisitedSocialAdminPageUrl=" + GetCurrentUrl();
    else if (jProductId == jAnalyticsProductId)
        fullUrl += "&LastVisitedAnalyticsAdminPageUrl=" + GetCurrentUrl();
    else
        fullUrl += "&LastVisitedCMSAdminPageUrl=" + GetCurrentUrl();

    return fullUrl;
}

function JumpToAdminPage(productName, lastVisited) {
    if (!lastVisited)
        lastVisited = false;

    productName = productName.toLowerCase();

    var pageUrl = "";
    if (productName == "cms") {
        if (jLastVisitedCMSAdminPageUrl != "" && lastVisited)
            pageUrl = jLastVisitedCMSAdminPageUrl;
        else
            pageUrl = String.format("{0}/Default.aspx", jAdminSiteUrl);
    }
    else if (productName == "marketier") {
        if (jLastVisitedMarketierAdminPageUrl != "" && lastVisited)
            pageUrl = jLastVisitedMarketierAdminPageUrl;
        else
            pageUrl = String.format("{0}/Default.aspx", jmarketierAdminURL);
    }
    else if (jProductId == "commerce") {
        if (jLastVisitedeCommerceAdminPageUrl != "" && lastVisited)
            pageUrl = jLastVisitedeCommerceAdminPageUrl;
        else
            pageUrl = String.format("{0}/Default.aspx", jCommerceAdminSiteUrl);
    }
    else if (jProductId == "analyzer") {
        if (jLastVisitedAnalyticsAdminPageUrl != "" && lastVisited)
            pageUrl = jLastVisitedAnalyticsAdminPageUrl;
        else
            pageUrl = String.format("{0}/Default.aspx", jAnalyzerAdminSiteUrl);
    }
    else if (jProductId == "social") {
        if (jLastVisitedSocialAdminPageUrl != "" && lastVisited)
            pageUrl = jLastVisitedSocialAdminPageUrl;
        else
            pageUrl = String.format("{0}/Default.aspx", jLastVisitedSocialAdminPageUrl);
    }

    if (pageUrl != "") {
        var token = FWCallback.GetAuthToken(juserName, jUserId, GetjProductId(productName), jAppId, false);
        if (token != "") {
            pageUrl = String.format("{0}?Token={1}&LastFrontEndPage={2}", pageUrl, token, GetCurrentUrl());
        }

        window.location.href = pageUrl;
    }
}

function GetCurrentUrl() {
    var currentUrl = document.URL;
    if (currentUrl == null || currentUrl == '') {
        currentUrl = document.referrer;
    }
    if (currentUrl.indexOf('?') > 0) {
        currentUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
    }

    return currentUrl;
}

// To open a file or image 
function OpeniAppsFile(fileUrl) {
    if (fileUrl != "")
        return window.open(fileUrl, null, 'height=600, width=500,status= no,resizable= no, scrollbars=yes,toolbar=no,location=no,menubar=no,modal=yes,top=50,left=200');
}

function GetRedirectPageUrl(actionName) {
    var jUrl = "";
    if (jProductId == jCMSProductId)
        jUrl = iAppsAdminMappings[actionName];
    else if (jProductId == jSocialProductId)
        jUrl = iAppsSocialMappings[actionName];
    else if (jProductId == jMarketierProductId)
        jUrl = iAppsAdminMappings[actionName];
    else if (jProductId == jCommerceProductId)
        jUrl = iAppsCommerceMappings[actionName];

    return jUrl;
}

function RedirectToAdminPage(actionName, querystringWithoutQuestionMark) {
    var domainUrl = GetAdminUrl();
    if (editorContext != 'SiteEditor')
        domainUrl = GetCurrentDomainUrl();

    var mUrl = domainUrl + GetRedirectPageUrl(actionName);
    if (querystringWithoutQuestionMark != null && querystringWithoutQuestionMark != "") {
        mUrl += "?" + querystringWithoutQuestionMark;
    }

    if (editorContext == 'SiteEditor') {
        var token = FWCallback.GetAuthToken(juserName, jUserId, jProductId, jAppId, false);
        if (token != "") {
            if (mUrl.indexOf("?") > -1)
                mUrl += "&Token=" + token;
            else
                mUrl += "?Token=" + token;
        }
    }

    window.location = mUrl;
}

function OpenLicenseWarningPopup(productName, hasLicenese, hasPermission) {
    Bridgeline.iapps.licensing.showWarning(productName, hasLicenese, hasPermission);
}

function OpenVersionCheckPopup() {
    OpeniAppsModalWindow("LicenseWarning", stringformat("{0}/Popups/iAPPSVersionCheck.aspx", jCommonLoginUrl));
}
<%@ Page Language="C#" AutoEventWireup="true" Inherits="BasePage" CodeBehind="BasePage.aspx.cs" ValidateRequest="false" Theme="General" EnableEventValidation="false" %>

<%@ Register TagPrefix="iAPPS" TagName="SiteEditorToolbar" Src="~/UserControls/SiteEditorToolbar.ascx" %>

<!DOCTYPE html>
<html id="htmlTag" runat="server" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!--Copyright Bridgeline Digital, Inc. An unpublished work created in 2009. All rights reserved. This software contains the confidential and trade secret information of Bridgeline Digital, Inc. ("Bridgeline").  Copying, distribution or disclosure without Bridgeline's express written permission is prohibited-->
    <asp:PlaceHolder ID="phOutputCache" runat="server" />
    <title></title>
    <asp:PlaceHolder ID="phGTDataLayer" runat="server" />
    <asp:PlaceHolder ID="phEmbedHeadStart" runat="server" />
    <asp:PlaceHolder ID="phMetgaTag" runat="server" />
    <asp:PlaceHolder ID="phLinkTags" runat="server" />
    <asp:PlaceHolder ID="phScriptTags" runat="server" />
    <FWControls:FWScriptHolder ID="phIncludeScriptBlock" runat="server" />
    <asp:PlaceHolder ID="phEmbedHeadEnd" runat="server" />
</head>
<body id="bodyTag" runat="server">
    <asp:PlaceHolder ID="phEmbedBodyStart" runat="server" />
    <asp:PlaceHolder ID="phOverlayContainer" runat="server" Visible="false">
        <div id="overlay" class="overlayPlaceHolder">
        </div>
        <div id="treeOverlay" class="overlayPlaceHolder">
        </div>
    </asp:PlaceHolder>
    <FWControls:FWActionLessForm runat="server" ID="form1" method="post">
        <asp:ScriptManager ID="scriptManager" runat="server" ScriptMode="Release" EnableCdn="true" LoadScriptsBeforeUI="false">
            
            </asp:ScriptManager>
        <iAPPS:SiteEditorToolbar ID="siteEditorToolbar" runat="server" />
        <asp:HiddenField ID="hdnPageEditorJSON" runat="server" ClientIDMode="Static" />
        <asp:PlaceHolder ID="TemplateHolder" runat="server" EnableViewState="true" />
        <asp:PlaceHolder ID="phAutoSave" runat="server" />
        <asp:PlaceHolder ID="phSiteEditorLogout" runat="server">
            <asp:HiddenField ID="hfSaveAsDraft" runat="server" Value="False" />
            <asp:Button ID="btnLogoutFromSiteEditor" OnClick="btnLogoutFromSiteEditor_Click"
                runat="server" Style="display: none;" />
            <script type="text/javascript">
                function LogOutFromSiteEditor() {
                    var btnLogoutFromSiteEditor = document.getElementById('<%=btnLogoutFromSiteEditor.ClientID %>');
                    __doPostBack(btnLogoutFromSiteEditor.name, null);
                }
            </script>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phPagePreview" runat="server" Visible="false">
            <div id="iapps-page-preview">
                <iframe id="ifPagePreview" runat="server"></iframe>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phDebugInfo" runat="server" Visible="false">
            <div class="iapps-reset">
                <div id="iapps-debug-info" class="iapps-modal iapps-modal-draggable iapps-modal-opacity">
                    <div class="iapps-modal-header">
                        <h4>Debug Information</h4>
                    </div>
                    <div class="iapps-modal-content">
                        <asp:ListView ID="lvDebugInfo" runat="server" ItemPlaceholderID="phDebugInfo">
                            <LayoutTemplate>
                                <table class="iapps-no-border">
                                    <colgroup>
                                        <col style="width: 200px;" />
                                    </colgroup>
                                    <tbody>
                                        <asp:PlaceHolder ID="phDebugInfo" runat="server" />
                                    </tbody>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <th class="iapps-align-right">
                                        <%# Eval("Key") %>
                                    </th>
                                    <td>
                                        <%# Eval("Value") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <div class="iapps-modal-footer">
                        <button class="iapps-button" data-modal-action="close">Close</button>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <script type="text/javascript" src="<%=Bridgeline.FW.UI.FWUIUtility.GetVersionedFilePath("/jsfile/Basepage.js") %>"></script>
    </FWControls:FWActionLessForm>

    <asp:PlaceHolder ID="phDeferredScriptTags" runat="server"></asp:PlaceHolder>
    <FWControls:FWScriptHolder ID="phStartUpScriptBlock" runat="server" />
    <asp:PlaceHolder ID="phEmbedBodyEnd" runat="server" />
</body>
</html>

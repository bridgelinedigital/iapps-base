<%@ Control Language="C#" %>
<%@ Register Assembly="Bridgeline.FW.UI.ControlLibrary" Namespace="Bridgeline.FW.UI.ControlLibrary"
    TagPrefix="FWControls" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<!-- BEGIN Footer -->
<div class="footer">
    <p>
        Copyright  2007 Bridgeline Digital, Inc.</p>
    <FWControls:FWMenuContainer ID="Menu2" runat="server" FwContainerId="86ff31ae-f9fc-40c4-8584-41f7bfa8b902">
        <FwMenu CssClass="menuFooter" Orientation="Horizontal">
           
        </FwMenu>
    </FWControls:FWMenuContainer>
</div>
<!-- END Footer -->

<%@ Control Language="C#"  AutoEventWireup="true" Inherits="StaticMessage" Codebehind="StaticMessage.ascx.cs" %>
<%@ Register Assembly="Bridgeline.FW.UI.ControlLibrary" Namespace="Bridgeline.FW.UI.ControlLibrary"
    TagPrefix="FWControls" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register Src="Header.ascx" TagPrefix="Pr" TagName="Header" %>
<%@ Register Src="Footer.ascx" TagPrefix="Pr" TagName="Footer" %>


<div>
    <!--Add Header Here -->
    <div class="entireContentAreaIP">
        <div class="pageAreaIP">
            <Pr:Header ID="prheader" runat="server" />
            <div class="headerBottomLine">
                &nbsp;</div>
            <!--END Header -->
            <div class="clearFix">
            </div>
            <div class="menuIP">
                <!--BEGIN Main Menu -->
                <div class="mainMenu">
                    <div class="menuMain" id="ctl00_Mainmenu1_Menu1">
 <FWControls:FWMenuContainer ID="Menu1" runat="server" FwContainerId="b3767d86-2bd1-467b-85d3-e5296c3c13a9">
            <FwMenu ID="FwMenu1" runat="server" CssClass="AspNet-Menu" DataSourceID="topMenuDS"
                RenderDrillDownDepth="1" Orientation="Horizontal">
                <ItemLooks>
                    <ComponentArt:ItemLook LookId="TopItemLook" ImageHeight="32" />
                    <ComponentArt:ItemLook LookId="TopItemSelectedLook" CssClass="AspNet-Menu-Selected"
                        ImageHeight="32" />
                </ItemLooks>
                <CustomAttributeMappings>
                    <ComponentArt:CustomAttributeMapping From="rolloverOffImage" To="Look-ImageUrl" />
                    <ComponentArt:CustomAttributeMapping From="rolloverOnImage" To="Look-HoverImageUrl" />
                </CustomAttributeMappings>
            </FwMenu>
        </FWControls:FWMenuContainer>
                    </div>
                    <div class="clearFix">
                    </div>
                </div>
                <div class="clearFix">
                </div>
            </div>
            <!--BEGIN Main Content -->
            <div class="clearFix">
            </div>
            <div class="mainContentIP">
                <div class="topSpacer">
                    &nbsp;</div>
                <!--BEGIN Left ContentPlaceHolder-->
                <div class="leftContentPlaceHolderIP">
                </div>
                <!--END Left ContentPlaceHolder / BEGIN Mid ContentPlaceHolder -->
                <div class="midContentPlaceHolderIP" style="padding: 200px 0 0 0; text-align: center;
                    color: #666666; font-size: 1.1em;">
                    
                &nbsp; <div id="staticMessage" runat="server" /> 
                    <br />
                    &nbsp; Please click to return to the <asp:LinkButton ID="homepagelink" runat="server" Text="Site Homepage in the Editor" OnClick="homepagelink_Click" ></asp:LinkButton> 
                    &nbsp;
                                    
                </div>
                <!--END Mid ContentPlaceHolder / BEGIN Right ContentPlaceHolder-->
                <div class="rightContentPlaceHolderIP">
                    <div class="rightContentHeaderIP">
                        <div class="rightContentHeaderIPleftBG">
                            <div class="rightContentHeaderIPrightBG">
                            </div>
                        </div>
                    </div>
                    <div class="rightContentPlaceHolderIPContents" id="ctl00_ContentPlaceHolder3_RelatedItems1_rightContentPlaceHolderIPContents">
                        
                    </div>
                    <!--END Right ContentPlaceHolder-->
                </div>
                <!--END Main Content -->
                <div class="clearFix">
                </div>
            </div>
        </div>
        <div class="clearFix">
        </div>
        <Pr:Footer ID="prfooter" runat="server" />
    </div>
</div>

<asp:SiteMapDataSource ID="topMenuDS" runat="server" SiteMapProvider="TopMenu" ShowStartingNode="False" />

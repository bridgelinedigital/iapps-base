﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="CMSFrontEndSite.EmailBasePage" CodeBehind="EmailBasePage.aspx.cs" ValidateRequest="false" Theme="General" EnableEventValidation="false" %>

<%@ Register TagPrefix="iAPPS" TagName="EmailBuilderToolbar" Src="~/UserControls/EmailbuilderToolbar.ascx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>iAPPS Marketier: Email Builder</title>
    <asp:PlaceHolder ID="phLinkTags" runat="server" />
    <asp:PlaceHolder ID="phScriptTags" runat="server" />
    <FWControls:FWScriptHolder ID="phIncludeScriptBlock" runat="server" />
</head>
<body yahoo="fix" style="margin: 0; padding: 0;">
    <FWControls:FWActionLessForm runat="server" ID="form1" method="post">
        <asp:ScriptManager ID="scEmailBuilder" runat="server" EnablePageMethods="true" />
        <iAPPS:EmailBuilderToolbar ID="emailBuilderToolbar" runat="server" />
        <div id="iAppsTemplateHolder">
            <asp:PlaceHolder ID="TemplateHolder" runat="server" />
        </div>
    </FWControls:FWActionLessForm>
    <FWControls:FWScriptHolder ID="phStartUpScriptBlock" runat="server" />
</body>
</html>

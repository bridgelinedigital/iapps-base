﻿function tinymce_Loaded(editor, element, data) {
    editor.ui.registry.addButton('custom1', {
        tooltip: 'Custom Button',
        icon: 'paste-column-after',
        onAction: () => {
            editor.windowManager.open(dialogConfig);
        }
    });
}

const dialogConfig = {
    title: 'Custom Modal',
    body: {
        type: 'panel',
        items: [
            {
                type: 'input',
                name: 'name',
                label: 'enter your name'
            }
        ]
    },
    buttons: [
        {
            type: 'cancel',
            name: 'closeButton',
            text: 'Cancel'
        },
        {
            type: 'submit',
            name: 'submitButton',
            text: 'Add Content',
            buttonType: 'primary'
        }
    ],
    initialData: {
        name: 'your name'
    },
    onSubmit: (api) => {
        const data = api.getData();
        
        tinymce.activeEditor.execCommand('mceInsertContent', false, `<p>Custom content added by <strong>${data.name}</strong></p>`);
        api.close();
    }
};
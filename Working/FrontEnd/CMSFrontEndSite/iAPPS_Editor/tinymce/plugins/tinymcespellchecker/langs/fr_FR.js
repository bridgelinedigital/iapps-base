/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("fr_FR", {
  "The spelling service was not found: ({0})": "Le v\xe9rificateur d'orthographe est introuvable\xa0: ({0})",
  "Spellcheck": "Correcteur orthographique",
  "Spellcheck...": "Correcteur orthographique...",
  "Spellcheck language": "Langue de la v\xe9rification d'orthographe",
  "No misspellings found.": "Aucune erreur d\u2019orthographe trouv\xe9e.",
  "Ignore": "Ignorer",
  "Ignore all": "Ignorer tout",
  "Misspelled word": "Mot mal orthographi\xe9",
  "Suggestions": "Suggestions",
  "Accept": "Accepter",
  "Change": "Modifier",
  "Finding word suggestions": "Trouver des suggestions de mots",
  "Language": "Langue",
  "No suggestions found": "Aucune suggestion trouv\xe9e",
  "No suggestions": "Aucune suggestion"
});

/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("vi", {
  "The spelling service was not found: ({0})": "Kh\xf4ng t\xecm th\u1ea5y d\u1ecbch v\u1ee5 so\xe1t ch\xednh t\u1ea3: ({0})",
  "Spellcheck": "So\xe1t ch\xednh t\u1ea3",
  "Spellcheck...": "So\xe1t ch\xednh t\u1ea3...",
  "Spellcheck language": "Ng\xf4n ng\u1eef d\xf9ng \u0111\u1ec3 so\xe1t ch\xednh t\u1ea3",
  "No misspellings found.": "Kh\xf4ng t\xecm th\u1ea5y l\u1ed7i ch\xednh t\u1ea3.",
  "Ignore": "B\u1ecf qua",
  "Ignore all": "B\u1ecf qua t\u1ea5t c\u1ea3",
  "Misspelled word": "T\u1eeb b\u1ecb sai ch\xednh t\u1ea3",
  "Suggestions": "\u0110\u1ec1 xu\u1ea5t",
  "Accept": "Ch\xe2\u0301p nh\xe2\u0323n",
  "Change": "Thay \u0111\u1ed5i",
  "Finding word suggestions": "T\xecm \u0111\u1ec1 xu\u1ea5t t\u1eeb",
  "Language": "Ng\xf4n ng\u01b0\u0303",
  "No suggestions found": "Kh\xf4ng t\xecm th\u1ea5y \u0111\u1ec1 xu\u1ea5t n\xe0o",
  "No suggestions": "Kh\xf4ng c\xf3 \u0111\u1ec1 xu\u1ea5t n\xe0o"
});

/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("da", {
  "The spelling service was not found: ({0})": "Stavetjenesten blev ikke fundet: ({0})",
  "Spellcheck": "Stavekontrol",
  "Spellcheck...": "Stavekontrol ...",
  "Spellcheck language": "Sprog for stavekontrol",
  "No misspellings found.": "Ingen stavefejl fundet.",
  "Ignore": "Ignorer",
  "Ignore all": "Ignorer alt",
  "Misspelled word": "Ord stavet forkert",
  "Suggestions": "Forslag",
  "Accept": "Accept\xe9r",
  "Change": "Skift",
  "Finding word suggestions": "Find ordforslag",
  "Language": "Language",
  "No suggestions found": "Der blev ikke fundet nogen forslag",
  "No suggestions": "Ingen forslag"
});

/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("nb_NO", {
  "The spelling service was not found: ({0})": "Stavekontroll ble ikke funnet: ({0})",
  "Spellcheck": "Stavekontroll",
  "Spellcheck...": "Stavekontroll...",
  "Spellcheck language": "Stavekontrollspr\xe5k",
  "No misspellings found.": "Ingen feilstavinger funnet.",
  "Ignore": "Ignorer",
  "Ignore all": "Ignorer alle",
  "Misspelled word": "Feilstavet ord",
  "Suggestions": "Forslag",
  "Accept": "Godta",
  "Change": "Endre",
  "Finding word suggestions": "Finner ordforslag",
  "Language": "Spr\xe5k",
  "No suggestions found": "Finner ingen forslag",
  "No suggestions": "Ingen forslag"
});

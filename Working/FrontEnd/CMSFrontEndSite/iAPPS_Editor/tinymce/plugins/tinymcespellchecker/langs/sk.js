/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("sk", {
  "The spelling service was not found: ({0})": "Funkcia kontroly pravopisu sa nena\u0161la: ({0})",
  "Spellcheck": "Kontrola pravopisu",
  "Spellcheck...": "Kontrola pravopisu...",
  "Spellcheck language": "Kontrola pravopisu jazyka",
  "No misspellings found.": "Nena\u0161li sa \u017eiadne pravopisn\xe9 chyby.",
  "Ignore": "Ignorova\u0165",
  "Ignore all": "Ignorova\u0165 v\u0161etko",
  "Misspelled word": "Slovo s pravopisnou chybou",
  "Suggestions": "N\xe1vrhy",
  "Accept": "Prija\u0165",
  "Change": "Zmeni\u0165",
  "Finding word suggestions": "H\u013eadaj\xfa sa n\xe1vrhy slova",
  "Language": "Jazyk",
  "No suggestions found": "Nena\u0161li sa \u017eiadne n\xe1vrhy",
  "No suggestions": "\u017diadne n\xe1vrhy"
});

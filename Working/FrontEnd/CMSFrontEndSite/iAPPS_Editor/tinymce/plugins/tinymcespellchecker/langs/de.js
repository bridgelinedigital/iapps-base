/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("de", {
  "The spelling service was not found: ({0})": "Der Rechtschreibpr\xfcfungsdienst wurde nicht gefunden: ({0})",
  "Spellcheck": "Rechtschreibhilfe",
  "Spellcheck...": "Rechtschreibhilfe...",
  "Spellcheck language": "Sprache f\xfcr Rechtschreibpr\xfcfung",
  "No misspellings found.": "Keine Rechtschreibfehler gefunden.",
  "Ignore": "Ignorieren",
  "Ignore all": "Alles Ignorieren",
  "Misspelled word": "Falsch geschriebenes Wort",
  "Suggestions": "Vorschl\xe4ge",
  "Accept": "Annehmen",
  "Change": "\xc4ndern",
  "Finding word suggestions": "Wortvorschl\xe4ge finden",
  "Language": "Sprache",
  "No suggestions found": "Keine Vorschl\xe4ge gefunden",
  "No suggestions": "Keine Vorschl\xe4ge"
});

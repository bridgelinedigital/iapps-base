/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("ca", {
  "The spelling service was not found: ({0})": "Leina dortografia no es troba disponible: ({0})",
  "Spellcheck": "Corrector ortogr\xe0fic",
  "Spellcheck...": "Corrector ortogr\xe0fic...",
  "Spellcheck language": "Idioma del corrector ortogr\xe0fic",
  "No misspellings found.": "No s'han trobat errors ortogr\xe0fics.",
  "Ignore": "Ignorar",
  "Ignore all": "Ignorar tots",
  "Misspelled word": "Paraula escrita malament",
  "Suggestions": "Suggeriments",
  "Accept": "Accepta",
  "Change": "Canviar",
  "Finding word suggestions": "Trobar suggeriments de paraules",
  "Language": "Idioma",
  "No suggestions found": "No s'ha trobat cap suggeriment",
  "No suggestions": "No hi ha cap suggeriment"
});

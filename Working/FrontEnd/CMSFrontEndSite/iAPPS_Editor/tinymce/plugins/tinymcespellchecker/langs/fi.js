/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("fi", {
  "The spelling service was not found: ({0})": "Oikeinkirjoituksen tarkistuspalvelua ei l\xf6ytynyt: ({0})",
  "Spellcheck": "Tarkista oikeinkirjoitus",
  "Spellcheck...": "Tarkista oikeinkirjoitus...",
  "Spellcheck language": "Oikeinkirjoituksen tarkistuksen kieli",
  "No misspellings found.": "Oikeinkirjoitusvirheit\xe4 ei l\xf6ytynyt.",
  "Ignore": "Ohita",
  "Ignore all": "\xc4l\xe4 huomioi mit\xe4\xe4n",
  "Misspelled word": "V\xe4\xe4rin kirjoitettu sana",
  "Suggestions": "Ehdotukset",
  "Accept": "Hyv\xe4ksy",
  "Change": "Muuta",
  "Finding word suggestions": "Etsit\xe4\xe4n sanaehdotuksia",
  "Language": "Kieli",
  "No suggestions found": "Ehdotuksia ei l\xf6ytynyt",
  "No suggestions": "Ei ehdotuksia"
});

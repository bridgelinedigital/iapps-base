/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("ko_KR", {
  "The spelling service was not found: ({0})": "\ub9de\ucda4\ubc95 \uac80\uc0ac \uc11c\ube44\uc2a4\ub97c \ucc3e\uc744 \uc218 \uc5c6\uc2b5\ub2c8\ub2e4: ({0})",
  "Spellcheck": "\ub9de\ucda4\ubc95 \uac80\uc0ac",
  "Spellcheck...": "\ub9de\ucda4\ubc95 \uac80\uc0ac...",
  "Spellcheck language": "\uc5b8\uc5b4 \ub9de\ucda4\ubc95 \uac80\uc0ac",
  "No misspellings found.": "\uc624\ud0c8\uc790\uac00 \uc5c6\uc2b5\ub2c8\ub2e4.",
  "Ignore": "\ubb34\uc2dc",
  "Ignore all": "\uc804\uccb4 \ubb34\uc2dc",
  "Misspelled word": "\ucca0\uc790\uac00 \ud2c0\ub9b0 \ub2e8\uc5b4",
  "Suggestions": "\uc81c\uc548",
  "Accept": "\uc218\ub77d",
  "Change": "\ubcc0\uacbd",
  "Finding word suggestions": "\ub2e8\uc5b4 \ucc3e\uae30 \uc81c\uc548",
  "Language": "\uc5b8\uc5b4",
  "No suggestions found": "\ucd94\ucc9c \ud56d\ubaa9\uc774 \uc5c6\uc2b5\ub2c8\ub2e4",
  "No suggestions": "\ucd94\ucc9c \ud56d\ubaa9 \uc5c6\uc74c"
});

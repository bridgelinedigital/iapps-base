/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("ms", {
  "The spelling service was not found: ({0})": "Perkhidmatan ejaan tidak ditemukan: ({0})",
  "Spellcheck": "Semak ejaan",
  "Spellcheck...": "Semak ejaan...",
  "Spellcheck language": "Bahasa semakan ejaan",
  "No misspellings found.": "Tiada salah ejaan ditemukan.",
  "Ignore": "Abaikan",
  "Ignore all": "Abaikan semua",
  "Misspelled word": "Perkataan disalah eja",
  "Suggestions": "Cadangan",
  "Accept": "Terima",
  "Change": "Tukar",
  "Finding word suggestions": "Sedang mencari cadangan kata",
  "Language": "Bahasa",
  "No suggestions found": "Tiada cadangan ditemui",
  "No suggestions": "Tiada cadangan"
});

/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("ro", {
  "The spelling service was not found: ({0})": "Serviciul de corectare ortografic\u0103 nu a fost g\u0103sit: ({0})",
  "Spellcheck": "Verificare ortografic\u0103",
  "Spellcheck...": "Verificare ortografic\u0103...",
  "Spellcheck language": "Limb\u0103 de verificare ortografic\u0103",
  "No misspellings found.": "Nu s-au g\u0103sit cuvinte ortografiate incorect.",
  "Ignore": "Ignorare",
  "Ignore all": "Ignor\u0103 tot",
  "Misspelled word": "Cuv\xe2nt ortografiat incorect",
  "Suggestions": "Sugestii",
  "Accept": "Acceptare",
  "Change": "Modificare",
  "Finding word suggestions": "G\u0103sire sugestii de cuvinte",
  "Language": "Limb\u0103",
  "No suggestions found": "Nu a fost g\u0103sit\u0103 nicio sugestie",
  "No suggestions": "Nicio sugestie"
});

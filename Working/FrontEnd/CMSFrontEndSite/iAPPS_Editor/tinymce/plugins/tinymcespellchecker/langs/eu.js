/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("eu", {
  "The spelling service was not found: ({0})": "Ezin izan da zuzenketa-zerbitzua aurkitu: ({0})",
  "Spellcheck": "Zuzenketa ortografikoa",
  "Spellcheck...": "Zuzenketa ortografikoa...",
  "Spellcheck language": "Ortografia zuzentzeko hizkuntza",
  "No misspellings found.": "Ez da ortografia-hutsegiterik aurkitu.",
  "Ignore": "Ez ikusi",
  "Ignore all": "Ez ikusi guztia",
  "Misspelled word": "Gaizki idatzitako hitza",
  "Suggestions": "Iradokizunak",
  "Accept": "Onartu",
  "Change": "Aldatu",
  "Finding word suggestions": "Aurkitu hitzaren iradokizunak",
  "Language": "Hizkuntza",
  "No suggestions found": "Ez da iradokizunik aurkitu",
  "No suggestions": "Ez dago iradokizunik"
});

/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("pl", {
  "The spelling service was not found: ({0})": "Nie znaleziono us\u0142ugi sprawdzania pisowni: ({0})",
  "Spellcheck": "Sprawd\u017a pisowni\u0119",
  "Spellcheck...": "Sprawd\u017a pisowni\u0119...",
  "Spellcheck language": "J\u0119zyk sprawdzania pisowni",
  "No misspellings found.": "Nie znaleziono b\u0142\u0119d\xf3w pisowni.",
  "Ignore": "Ignoruj",
  "Ignore all": "Ignoruj wszystko",
  "Misspelled word": "B\u0142\u0119dnie zapisane s\u0142owo",
  "Suggestions": "Sugestie",
  "Accept": "Zaakceptuj",
  "Change": "Zmie\u0144",
  "Finding word suggestions": "Znajdowanie sugerowanych s\u0142\xf3w",
  "Language": "J\u0119zyk",
  "No suggestions found": "Nie znaleziono \u017cadnych sugestii",
  "No suggestions": "Brak sugestii"
});

/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("pt_BR", {
  "The spelling service was not found: ({0})": "O servi\xe7o de ortografia n\xe3o foi encontrado: ({0})",
  "Spellcheck": "Verificador ortogr\xe1fico",
  "Spellcheck...": "Verificador ortogr\xe1fico...",
  "Spellcheck language": "Idioma de verifica\xe7\xe3o ortogr\xe1fica",
  "No misspellings found.": "Nenhum erro ortogr\xe1fico encontrado.",
  "Ignore": "Ignorar",
  "Ignore all": "Ignorar tudo",
  "Misspelled word": "Palavra com erro",
  "Suggestions": "Sugest\xf5es",
  "Accept": "Aceitar",
  "Change": "Alterar",
  "Finding word suggestions": "Localizando sugest\xf5es de palavras",
  "Language": "Idioma",
  "No suggestions found": "Nenhuma sugest\xe3o encontrada",
  "No suggestions": "Sem sugest\xf5es"
});

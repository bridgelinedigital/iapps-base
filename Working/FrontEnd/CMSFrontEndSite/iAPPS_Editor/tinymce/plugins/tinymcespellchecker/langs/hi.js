/*!
 * TinyMCE Language Pack
 *
 * Copyright (c) 2023 Ephox Corporation DBA Tiny Technologies, Inc.
 * Licensed under the Tiny commercial license. See https://www.tiny.cloud/legal/
 */
tinymce.addI18n("hi", {
  "The spelling service was not found: ({0})": "\u0935\u0930\u094d\u0924\u0928\u0940 \u0938\u0947\u0935\u093e \u0928\u0939\u0940\u0902 \u092e\u093f\u0932\u0940: ({0})",
  "Spellcheck": "\u0935\u0930\u094d\u0924\u0928\u0940 \u0915\u0940 \u091c\u093e\u0902\u091a",
  "Spellcheck...": "\u0935\u0930\u094d\u0924\u0928\u0940 \u0915\u0940 \u091c\u093e\u0902\u091a...",
  "Spellcheck language": "\u0935\u0930\u094d\u0924\u0928\u0940 \u091c\u093e\u0902\u091a \u0915\u0940 \u092d\u093e\u0937\u093e",
  "No misspellings found.": "\u0915\u094b\u0908 \u0917\u0932\u0924 \u0935\u0930\u094d\u0924\u0928\u0940 \u0928\u0939\u0940\u0902 \u092e\u093f\u0932\u0940\u0964",
  "Ignore": "\u0905\u0928\u0926\u0947\u0916\u093e \u0915\u0930\u0947\u0902",
  "Ignore all": "\u0938\u092d\u0940 \u0915\u094b \u0905\u0928\u0926\u0947\u0916\u093e \u0915\u0930\u0947\u0902",
  "Misspelled word": "\u0917\u0932\u0924 \u0935\u0930\u094d\u0924\u0928\u0940 \u0935\u093e\u0932\u093e \u0936\u092c\u094d\u0926",
  "Suggestions": "\u0938\u0941\u091d\u093e\u0935",
  "Accept": "\u0938\u094d\u0935\u0940\u0915\u093e\u0930 \u0915\u0930\u0947\u0902",
  "Change": "\u092a\u0930\u093f\u0935\u0930\u094d\u0924\u093f\u0924 \u0915\u0930\u0947\u0902",
  "Finding word suggestions": "\u0936\u092c\u094d\u0926 \u0938\u0941\u091d\u093e\u0935\u094b\u0902 \u0915\u094b \u0922\u0942\u0902\u0922\u093e \u091c\u093e \u0930\u0939\u093e \u0939\u0948",
  "Language": "\u092d\u093e\u0937\u093e",
  "No suggestions found": "\u0915\u094b\u0908 \u0938\u0941\u091d\u093e\u0935 \u0928\u0939\u0940\u0902 \u092e\u093f\u0932\u0947",
  "No suggestions": "\u0915\u094b\u0908 \u0938\u0941\u091d\u093e\u0935 \u0928\u0939\u0940\u0902"
});

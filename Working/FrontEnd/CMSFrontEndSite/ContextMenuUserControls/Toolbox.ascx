﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Toolbox.ascx.cs" Inherits="CMSFrontEndSite.ContextMenuUserControls.Toolbox" %>
<div id="iapps-tool-box">
    <div class="iapps-float-box">
        <div class="iapps-float-box-header">
            <asp:Localize ID="lc1" runat="server" Text="<%$ Resx:GUIStrings, Toolbox %>" />
            <img src="/iapps_images/close.gif" class="iapps-float-box-close" alt="Close" />
        </div>
        <asp:Repeater ID="rptWidgets" runat="server">
            <HeaderTemplate>
                <div class="iapps-float-box-content">
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Panel ID="pnlToolBoxItem" CssClass="iapps-tool-box-item" runat="server" />
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>

<%@ Control Language="C#" AutoEventWireup="true" Inherits="TypeOfContent" CodeBehind="TypeOfContent.ascx.cs" ClientIDMode="Static" %>
<script type="text/javascript">
    function SelectedContentType() {
        var valid;
        var dwObjectId = document.getElementById('<%=dwContentTemplates.ClientID%>');
        var selectedValue, selectedText;
        selectedValue = dwObjectId.value;
        if (selectedValue == '') {
            alert(__JSMessages["PleaseSelectContentDef"]);
            Valid = false;
        }
        else {
            valid = true

            selectedText = dwObjectId[dwObjectId.selectedIndex].text;

            if (window.parent.self["SetContentType"]) {
                window.parent.SetContentType(selectedText, selectedValue);
                window.parent.enableSelect = true;
            }
            else {
                if (window.parent.frames != null && window.parent.frames.length > 0) {
                    var frameObject;
                    for (var i = 0; i < window.parent.frames.length; i++) {
                        frameObject = window.parent.frames[i];
                        if (frameObject != null && frameObject.location != null) {
                            if (frameObject.location.href != null) {
                                if (frameObject.self["SetContentType"]) {
                                    frameObject.SetContentType(selectedText, selectedValue);
                                    frameObject.enableSelect = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

        }
        Data.ContextMenu.hide();
        return valid;
    }

    function Close() {
        Data.ContextMenu.hide();
        return false;
    }
    function LoadXMLAsPerTemplate(thisTemplateContentDefinitionIds) {
        //alert('LoadXMLAsPerTemplate' + thisTemplateContentDefinitionIds);
        var dwObjectFilteredList = document.getElementById('<%=dwContentTemplates.ClientID%>');
        var dwObjectMainList = document.getElementById('<%=dwContentTemplatesHidden.ClientID%>');
        var btnObject = document.getElementById('btnShowAll');

        var myOption;
        var row;
        if (thisTemplateContentDefinitionIds != '') {
            //Clear all items in dwObjectFilteredList and add it from main list
            dwObjectFilteredList.innerHTML = "";
            var listArray = thisTemplateContentDefinitionIds.split(",");
            btnObject.style.visibility = "visible";
            for (row = 0; row < dwObjectMainList.length; row++) {
                if (listArray.contains(dwObjectMainList[row].value)) {
                    myOption = new Option(dwObjectMainList[row].text, dwObjectMainList[row].value);
                    dwObjectFilteredList.options[dwObjectFilteredList.length] = myOption;
                }
            }
        }
        else if (dwObjectFilteredList.length != dwObjectMainList.length) {
            //Filtering happened already. so we need to refill even if path is not mentioned for this.
            dwObjectFilteredList.innerHTML = "";
            btnObject.style.visibility = "hidden";

            for (row = 0; row < dwObjectMainList.length; row++) {
                myOption = new Option(dwObjectMainList[row].text, dwObjectMainList[row].value);
                dwObjectFilteredList.options[dwObjectFilteredList.length] = myOption;
            }
        }
    }

    Array.prototype.contains = function (element) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == element) {
                return true;
            }
        }
        return false;
    }

    function ShowAllTemplates() {
        LoadXMLAsPerTemplate('');
    }

</script>
<table border="0" cellpadding="0" cellspacing="0" width="300">
    <tr>
        <td align="left" valign="top" style="padding: 10px 0 0 10px;" width="75">
            <span class="formLabels formLabelsType"><asp:Localize runat="server" Text="<%$ Resx:GUIStrings, ContentDefinitionTemplate %>" /></span>
        </td>
        <td style="padding: 10px;">
            <asp:ListBox ID="dwContentTemplates" runat="server" CssClass="contentListBox" Width="185"></asp:ListBox>
            <asp:ListBox ID="dwContentTemplatesHidden" runat="server" style="visibility: hidden;"></asp:ListBox>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="right" style="padding: 10px;">
            <input type="button" id="cancel" onclick="Close(); " runat="server" value="<%$ Resx:GUIStrings, Close %>"
                class="iapps-button" title="Close" />
            <input id="btnShowAll" type="button" class="iapps-primary-button" value="<%$ Resx:GUIStrings, ShowAll %>" onclick="return ShowAllTemplates();"
                style="visibility: hidden;" runat="server" title="Select" />
            <input type="button" class="iapps-primary-button" value="<%$ Resx:GUIStrings, Select %>" onclick="return SelectedContentType();"
                runat="server" title="Select" />
        </td>
    </tr>
</table>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadFormAttachment.aspx.cs" 
    Inherits="UploadFormAttachment" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Upload File</title>
    <script type="text/javascript">
        function closeIframe() {
            parent.diwindow.hide();
        }

        function ValidateFormRequest() {
            var isValid = true;
            try {
                isValid = window.self !== window.top;
            } catch (e) {
                isValid = true;
            }

            if (!isValid) {
                document.getElementById("upload_section").innerHTML = "<%= Bridgeline.iAPPS.Resources.GUIStrings.InvalidInput %>";
                document.getElementById("upload_footer").innerHTML = "";
            }
        }
    </script>
    <style type="text/css">
        body
        {
            font-family: "Trebuchet MS";
            font-size: 67.5%;
            color: #666;
        }
        div.iapps-modal
        {
            background-color: #ffffff;
            min-width: 300px;
        }
        div.modal-header
        {
            background-color: #f9f9f9;
            border-bottom: solid 1px #fff;
            color: #6a6c6e;
            padding: 10px 25px;
        }
        div.modal-header h2
        {
            font-size: 20px;
            margin: 0;
            padding: 0;
        }
        div.modal-content
        {
            padding: 10px;
            padding-top: 30px;
            min-height: 80px;
            border-top: solid 1px #e6e6e6;
            border-bottom: solid 1px #e6e6e6;
            background-color: #fff;
            text-align: center;
        }
        div.modal-footer
        {
            padding: 10px 25px;
            text-align: right;
            background-color: #f9f9f9;
            border-top: solid 1px #fff;
        }
        input.button
        {
            margin-left: 10px;
            font-family: "Trebuchet MS";
            font-weight: bold;
            font-size: 1.1em;
            padding:2px 6px;
            color: #102a5c;
            background-color: #e8ebef;
            border-radius: 3px;
            border: solid 1px #97b8ef;
            outline: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="iapps-modal">
        <div class="modal-header">
            <h2>
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resx:GUIStrings, UploadFile %>" /></h2>
        </div>
        <div class="modal-content" id="upload_section">
            <asp:Label ID="ErrorLabel" runat="server" ForeColor="red" />
            <asp:FileUpload ID="fileUpload" ToolTip="<%$ Resx:GUIStrings, Browse %>" runat="server" CssClass="file" />
        </div>
        <div class="modal-footer" id="upload_footer">
            <asp:Button ID="btnUploadImage" runat="server" Text="<%$ Resx:GUIStrings, UploadFile %>"
                CssClass="iapps-button" OnClick="uploadImage_Click" />
            <asp:Button ID="btnCancel" OnClientClick="closeIframe();return false;" runat="server"
                Text="<%$ Resx:GUIStrings, Close %>" CssClass="iapps-button" />
        </div>
    </div>
    </form>
</body>
</html>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectUserControl.ascx.cs"
    Inherits="SelectUserControl" %>
<script type="text/javascript">
    function SelectedUserControl() {
        var valid;
        var dwObjectId = document.getElementById('<%=dwUserControls.ClientID%>');
        var selectedValue = dwObjectId.value;
        if (selectedValue == '') {
            alert("Please select a UserControl.");
            valid = false;
        }
        else {
            valid = true
            SetUserControl(selectedValue);
        }
        return valid;
    }

    function Close() {
        Data.ContextMenu.hide();
        return false;
    }
</script>
<div class="contextMenuContainer contentTemplatesType">
    <div class="contextMenuContent contextMenuType">
        <label class="labels" for="<%=dwUserControls.ClientID%>">
            <span class="formLabels formLabelsType">
                <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, UserControls %>" /></span>
            <asp:ListBox ID="dwUserControls" runat="server" CssClass="contentListBox"></asp:ListBox>
        </label>
        <span style="display: block; text-align: right; clear: both; padding: 10px;">
            <input type="button" id="cancel" onclick="Close();" runat="server" value="<%$ Resx:GUIStrings, Close %>"
                class="iapps-button" title="<%$ Resx:GUIStrings, Close %>" />
            <input type="button" class="iapps-primary-button" runat="server" value="<%$ Resx:GUIStrings, Select %>"
                onclick="return SelectedUserControl();" title="<%$ Resx:GUIStrings, Select %>" />
        </span>
    </div>
</div>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditLabelText.ascx.cs" Inherits="EditLabelText" %>
<%@ Import Namespace="Bridgeline.iAPPS.Resources" %>

<div class="contextMenuContainer contentTemplatesType">
    <div class="contextMenuContent contextMenuType">
        <label class="labels" for="txtLabel">
            <span class="formLabels formLabelsType" style="display: block; float: left;">
                <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, LabelText %>" /></span>
            <textarea id="txtLabel" rows="4" cols="10" style="clear: both; width: 210px; height: 60px;
                margin-left: 10px;"></textarea>
        </label>
        <span style="display: block; text-align: right; clear: both; padding: 0px 10px 10px 0px;">
            <input type="button" id="cancel" onclick="Close(); " runat="server" value="<%$ Resx:GUIStrings, Close %>"
                class="iapps-button" title="<%$ Resx:GUIStrings, Close %>" />
            <input type="button" class="iapps-primary-button" value="<%= GUIStrings.Save %>"
                onclick="return SaveText();" title="<%= GUIStrings.Save %>" />
        </span>
    </div>
</div>
<script type="text/javascript">

    function SaveText() {
        var dwObjectId = document.getElementById('txtLabel');
        var valid;
        var selectedValue = dwObjectId.value;
        if (selectedValue == '') {
            alert("<%= GUIStrings.PleaseEnterAText %>");
            Valid = false;
        }
        else {
            valid = true
            window.parent.SetLabelText(selectedValue);
        }
        return valid;
    }

    function Close() {
        Data.ContextMenu.hide();
        return false;
    }

    var dwObjectId = document.getElementById('txtLabel');
    dwObjectId.value = window.parent.GetLabelText();
</script>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TypeOfPageTitle.ascx.cs"
    Inherits="TypeOfPageTitle" %>
<script type="text/javascript">
    function SelectedTitleType() {
        var valid;
        var dwObjectId = document.getElementById('<%=dwTitleTypes.ClientID%>');
        var selectedValue = dwObjectId.value;
        if (selectedValue == "") {
            alert("<%= Bridgeline.iAPPS.Resources.GUIStrings.PleaseSelectATitleType %>");
            valid = false;
        }
        else {
            valid = true
            SetTitleType(selectedValue);
        }
        return valid;
    }

    function Close() {
        Data.ContextMenu.hide();
        return false;
    }
</script>
<div class="contextMenuContainer contentTemplatesType">
    <div class="contextMenuContent contextMenuType">
        <label class="labels" for="<%=dwTitleTypes.ClientID%>">
            <span class="formLabels formLabelsType">
                <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, TitleType %>" /></span>
            <asp:ListBox ID="dwTitleTypes" runat="server" CssClass="contentListBox"></asp:ListBox>
        </label>
        <span style="display: block; text-align: right; clear: both; padding: 10px;">
            <input type="button" id="cancel" onclick="Close(); " runat="server" value="<%$ Resx:GUIStrings, Close %>"
                class="iapps-button" title="<%$ Resx:GUIStrings, Close %>" />
            <input type="button" class="iapps-primary-button" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.Select %>"
                onclick="return SelectedTitleType();" title="<%= Bridgeline.iAPPS.Resources.GUIStrings.Select %>" />
        </span>
    </div>
</div>

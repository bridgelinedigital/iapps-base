<%@ Page Language="C#" AutoEventWireup="true" Codebehind="VideoPlayerPopup.aspx.cs" Inherits="VideoPlayerPopup" %>
<%@ Import Namespace="Bridgeline.iAPPS.Resources" %>

<link href="<% =resourcePath %>/css/base.css" rel="stylesheet" media="all" type="text/css" />
<script src="<% =resourcePath %>/scripts/swfobject.js" type="text/javascript" ></script>


<script type="text/javascript">
	//embed the video player swf + pass in the movieUrl and movieSkinUrl parameters
    //swfobject.embedSWF("/Video Gallery/player.swf", "player", 480, 370, "9.0.0", "/Video Gallery/expressInstall.swf", { moviesource: "<% =movieUrl %>", movieskin: "/Video Gallery/SkinOverPlaySeekMute.swf" }, null, null);
    swfobject.embedSWF("<% =resourcePath %>" +"/player.swf", "player", "<% =playerHeight %>", "<% =playerWidth %>", "9.0.0", "<% =resourcePath %>" +"/expressInstall.swf", { moviesource: "<% =movieUrl %>", movieskin: "<% =resourcePath %>" +"/SkinOverPlaySeekMute.swf" }, null, null);
</script>
<div class="videoPlayer">
    <div class="content">
        <a class="closePlayer" title="<%= GUIStrings.ClosePlayer %>" onclick="self.parent.tb_remove();"><asp:localize runat="server" text="<%$ Resx:GUIStrings, ClosePlayer %>" /></a>
        <div id="player"></div>
    </div>
</div>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseImageGallery.ascx.cs" Inherits="ChooseImageGallery" %>
<%@ Import Namespace="Bridgeline.iAPPS.Resources" %>
<script type="text/javascript">
    function SelectedGalleryType() {
        var valid;
        var dwObjectId = document.getElementById('<%=dwGalleryType.ClientID%>');
        var selectedValue = dwObjectId.value;
        if (selectedValue == '') {
            alert("<%= GUIStrings.PleaseSelectAGalleryType %>");
            Valid = false;
        }
        else {
            valid = true
            window.parent.ChooseGalleryType(selectedValue);
        }
        return valid;
    }

    function Close() {
        Data.ContextMenu.hide();
        return false;
    }
</script>
<div class="contextMenuContainer contentTemplatesType">
    <div class="contextMenuContent contextMenuType">
        <label class="labels" for="<%=dwGalleryType.ClientID%>">
            <span class="formLabels formLabelsType">
                <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, GalleryType %>" /></span>
            <asp:ListBox ID="dwGalleryType" runat="server" CssClass="contentListBox"></asp:ListBox>
        </label>
        <span style="display: block; text-align: right; clear: both; padding: 10px;">
            <input type="button" class="iapps-primary-button" value="Select" onclick="return SelectedGalleryType();"
                title="<%$ Resx:GUIStrings, Select %>" runat="server" />
            <input type="button" id="cancel" onclick="Close();" runat="server" value="<%$ Resx:GUIStrings, Close %>"
                class="iapps-button" title="<%$ Resx:GUIStrings, Close %>" />
        </span>
    </div>
</div>

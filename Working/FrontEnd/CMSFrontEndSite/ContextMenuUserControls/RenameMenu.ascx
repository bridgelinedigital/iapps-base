<%@ Control Language="C#" %>
<script type="text/javascript">
    var CLNav_RenameMenu_txt = "<%=txtMenuName.ClientID %>";
    function RenameMenu() {
        CLHierarchicalNav_RenameMenu(document.getElementById('<%= txtMenuName.ClientID %>').value); 
        document.getElementById('<%= txtMenuName.ClientID %>').value = '';
    }
</script>
<div class="ContextMenuW">
    <table class="ImageFileTable">
        <tr>
            <td class="ImageFileLabel">
               <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, MenuName %>" />
            </td>
            <td class="ImageFileControl">
                <asp:TextBox ID="txtMenuName" runat="server" CssClass="textBoxes" Width="196" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td class="ImageFileButton">
                <input type="button" class="iapps-button" value="<%$ Resx:GUIStrings, Cancel %>" onclick="CLHierarchicalNav_HideContextMenu();"
                    runat="server" />
                <input type="button" class="iapps-primary-button" value="<%$ Resx:GUIStrings, RenameMenu %>" onclick="return RenameMenu();"
                    runat="server" />
            </td>
        </tr>
    </table>
</div>

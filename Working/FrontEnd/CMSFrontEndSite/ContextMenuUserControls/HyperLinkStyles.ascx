<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HyperLinkStyles.ascx.cs"
    Inherits="HyperLinkStyles" %>
<script type="text/javascript">
    function SelectedStyle() {
        var valid;
        var dwObjectId = document.getElementById('<%=dwStyles.ClientID%>');
        var selectedValue = dwObjectId.value;
        if (selectedValue == '') {
            alert("Please select a style ");
            valid = false;
        }
        else {
            valid = true
            window.parent.SetLinkStyle(selectedValue);
            Data.ContextMenu.hide();
        }
        return valid;
    }

    function Close() {
        Data.ContextMenu.hide();
        return false;
    }
</script>
<div class="contextMenuContainer contentTemplatesType">
    <div class="contextMenuContent contextMenuType">
        <label class="labels" for="<%=dwStyles.ClientID%>">
            <span class="formLabels formLabelsType" style="display: block;">
                <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, SelectStyle %>" /></span>
            <asp:ListBox ID="dwStyles" runat="server" CssClass="contentListBox"></asp:ListBox>
        </label>
        <span style="display: block; text-align: right; clear: both; padding: 10px;">
            <input type="button" id="cancel" onclick="Close(); " runat="server" value="<%$ Resx:GUIStrings, Close %>"
                class="iapps-button" title="<%$ Resx:GUIStrings, Close %>" />
            <input type="button" class="iapps-primary-button" value="<%$ Resx:GUIStrings, Select  %>"
                onclick="return SelectedStyle();" title="<%$ Resx:GUIStrings, Select %>" runat="server"  />
        </span>
    </div>
</div>

<%@ Control Language="C#" %>
<script type="text/javascript">
    function CreateMenu() {
        CLHierarchicalNav_CreateSubMenu(document.getElementById('<%= txtSubMenuName.ClientID %>').value, document.getElementById('<%= ddlCreateAs.ClientID %>').value);
        document.getElementById('<%= txtSubMenuName.ClientID %>').value = '';
    }
</script>
<div class="ContextMenuW">
    <table class="ImageFileTable">
        <tr>
            <td class="ImageFileLabel">
                <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, SubMenuName %>" />
            </td>
            <td class="ImageFileControl">
                <asp:TextBox ID="txtSubMenuName" runat="server" CssClass="textBoxes" Width="180" />
            </td>
        </tr>
        <tr>
            <td class="ImageFileLabel">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resx:GUIStrings, CreateAs %>" />
            </td>
            <td class="ImageFileControl">
                <asp:DropDownList ID="ddlCreateAs" runat="server" Width="186">
                    <asp:ListItem Text="<%$ Resx:GUIStrings, SubMenu %>" Value="0" />
                    <asp:ListItem Text="<%$ Resx:GUIStrings, Sibling %>" Value="1" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td class="ImageFileButton">
                <input type="button" class="iapps-button" value="<%$ Resx:GUIStrings, Cancel %>" onclick="CLHierarchicalNav_HideContextMenu();"
                    runat="server" />
                <input type="button" class="iapps-primary-button" value="<%$ Resx:GUIStrings, AddMenu %>" onclick="return CreateMenu();" 
                    runat="server" />
            </td>
        </tr>
    </table>
</div>

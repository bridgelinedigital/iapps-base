<%@ Control Language="C#" AutoEventWireup="true" Inherits="ContextMenuUserControls_WorkflowReject" Codebehind="WorkflowReject.ascx.cs" %>
<script language="javascript" type="text/javascript">

function show()
{ 
//debugger;
    var txtReject=document.getElementById('<%=txtRejectText.ClientID %>');
    if(txtReject.style.visibility!="visible" && txtReject.style.visibility!="" )
    {
        //do  nothing
    }
    else
    {
        document.getElementById('<%=txtRejectText.ClientID %>').focus();
    }
    
}
function RemoveFocus()
{
    parent.focus();
//    var btnCancel=document.getElementById('<%=CancelBtn1.ClientID %>')
//    if(btnCancel.style.visibility!="visible" && btnCancel.style.visibility!="" )
//    {
//        //do  nothing
//    }
//    else
//    {
//        document.getElementById('<%=CancelBtn1.ClientID %>').focus();
//    }
    //document.getElementById('<%=CancelBtn1.ClientID %>').focus();
}
</script>
<table id="tblId" class="ImageFileTable" onmouseover="show()" onmouseout="RemoveFocus()">
    <tr>
        <td class="ImageFileLabel"><asp:Localize runat="server" Text="<%$ Resx:GUIStrings, Reason %>" /></td>
        <td class="ImageFileControl">
            <asp:TextBox CssClass="ImageFileTextArea" TextMode="MultiLine" ID="txtRejectText" runat="server" Height="81px" Width="250px"></asp:TextBox></td>
    </tr>
    <tr>
        <td>
        </td>
        <td class="ImageFileButton" align="center">
            <asp:Button CssClass="iapps-button" Text="<%$ Resx:GUIStrings, Reject %>" ID="SaveBtn1" 
                runat="server" OnClick="SaveBtn1_Click1" />          
            <asp:Button CssClass="iapps-button" Text="<%$ Resx:GUIStrings, Cancel %>" ID="CancelBtn1" runat="server" OnClick="CancelBtn1_Click"  />
        </td>
    </tr>
</table>
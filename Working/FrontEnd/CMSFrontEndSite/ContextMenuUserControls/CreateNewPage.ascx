<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_ContextMenu_CreateNewPage"
    CodeBehind="CreateNewPage.ascx.cs" %>

<script type="text/javascript">
    var CreateNewPage_PageNameClientId = '<%= pageName.ClientID %>';

    function CreatePage(pageNameControlId, templateControlId, connectToMenuClientId) {
        var pageName = document.getElementById(pageNameControlId).value;
        var templateId = "";
        if (eval(templateControlId).getSelectedItem())
            templateId = eval(templateControlId).getSelectedItem().get_value();

        var connectToMenu = false;
        if (connectToMenuClientId != "")
            connectToMenu = document.getElementById(connectToMenuClientId).checked;
        if (createPageFromTreeView)
            return ValidatePageName(pageName, templateId, connectToMenu);
        else
            return ValidateAndCreatePage(pageName, templateId, connectToMenu);
    }

    function CloseCreateMenu() {
        if (createPageFromTreeView)
            return CloseConnectPage();
        else
            return CloseConnectPageMenu();
    }

    function showTemplateThumbnail(virtualPath, imageUrl, title) {
        var returnHtml;

        if (imageUrl != null && imageUrl != "" && virtualPath != null) {
            virtualPath = virtualPath.replace("~", "") + "/";
            returnHtml = "<div class=\"templatePreview\"><img src=\"" + virtualPath + imageUrl + "\" alt=\"\" />" + title + "</div>";
        }
        else {
            returnHtml = "<div class=\"templatePreview noImage\">" + title + "</div>";
        }

        return returnHtml; 
    }
</script>

<div class="ContextMenuW">
    <table class="ImageFileTable">
        <tr>
            <td class="ImageFileLabel">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resx:GUIStrings, PageName %>" />
            </td>
            <td class="ImageFileControl">
                <asp:TextBox ID="pageName" runat="server" CssClass="textBoxes" Width="196"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="ImageFileLabel">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resx:GUIStrings, Template %>" />
            </td>
            <td class="ImageFileControl">
                <ComponentArt:ComboBox ID="cmbTemplate" runat="server" SkinID="Default" Width="175"
                    DropDownHeight="300" DropDownWidth="300" DataFields="VirtualPath,Image" ItemClientTemplateId="ctTemplatePreview">
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="ctTemplatePreview" runat="server">
                            ## showTemplateThumbnail(DataItem.VirtualPath, DataItem.Image, DataItem.Text) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:ComboBox>
            </td>
        </tr>
        <tr id="trConnectToMenu" runat="server">
            <td>
                &nbsp;
            </td>
            <td class="ImageFileControl ImageFileLabel">
                <table>
                    <tr>
                        <td><asp:CheckBox ID="chkConnectToMenu" runat="server" /></td>
                        <td class="ImageFileLabel" style="padding:0;vertical-align:middle;"><asp:Localize ID="Localize3" runat="server" Text="<%$ Resx:GUIStrings, ConnectToMenu %>" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td class="ImageFileButton">
                <asp:Button runat="server" ID="btnCancel" CssClass="iapps-button" Text="<%$ Resx:GUIStrings, Cancel %>" 
                    OnClientClick="return CloseCreateMenu();" />
                <asp:Button ID="btnAddPage" runat="server" CssClass="iapps-primary-button" Text="<%$ Resx:GUIStrings, AddPage %>" />
            </td>
        </tr>
    </table>
</div>

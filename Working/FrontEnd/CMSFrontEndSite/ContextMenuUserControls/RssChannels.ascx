<%@ Control Language="C#" AutoEventWireup="true" Inherits="ContextMenu_RssChannels" Codebehind="RssChannels.ascx.cs" %>

<script type="text/javascript" language="javascript">
    var addPage = true;
    function AddPage()
    {        
        var callbackArgs;
        var listBox = document.getElementById('<%=rssChannels.ClientID%>');            
        var listcount = listBox.options.length;
        var channelIds = '';            
        for (i = 0 ; i < listcount ; i++)
        {
            if (listBox.options[i].selected)
            {
                channelIds += listBox.options[i].value + ",";
            }
        }
        if (channelIds != '')
        {
            callbackArgs = channelIds;        
            AddPageCallBack.callback(callbackArgs); 
        } 
        else
        {
            alert(__JSMessages["PleaseSelectAChannel"]);
        }
        return false;
    }
    
    function ButtonCallBackComplete(sender, eventArgs)
    {
        if(addPage == true)
        {
            alert(__JSMessages["PageAddedToChannel"]);
        } 
        addPage = true;
    }
    
    function ButtonCallBackError(sender, eventArgs)
    {
        addPage = false;
        alert(__JSMessages["ErrorAddingPage"]);
    }
</script>

<table class="ImageFileTable">
    <tr>
        <td class="ImageFileLabel" nowrap="nowrap">
            <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, RSSChannel %>" /></td>
        <td class="ImageFileControl">
            <asp:ListBox ID="rssChannels" runat="server" SelectionMode="Multiple" Rows="3" Width="150"
                AutoPostBack="false"></asp:ListBox></td>
    </tr>
    <tr>
        <td>
        </td>
        <td class="ImageFileButton">
            <asp:Button ID="btnAddToRss" runat="server" CssClass="iapps-primary-button" Text="<%$ Resx:GUIStrings, AddPageToRSS %>"
                OnClientClick="return AddPage();" />
        </td>
    </tr>
</table>
<ComponentArt:CallBack ID="AddPageCallBack" runat="server" EnableViewState="true">
    <Content>
    </Content>
    <ClientEvents>
        <CallbackComplete EventHandler="ButtonCallBackComplete" />
        <CallbackError EventHandler="ButtonCallBackError" />
    </ClientEvents>
</ComponentArt:CallBack>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailBuilderToolbar.ascx.cs" Inherits="CMSFrontEndSite.UserControls.EmailBuilderToolbar" %>

<div class="iapps-reset">
    <div id="iapps-site-editor-toolbar" class="iapps-email-toolbar">
        <div class="iapps-container clearfix">
            <ul class="iapps-site-editor-dropdown iapps-product-suite">
                <li>
                    <span id="spanLogoContainer" runat="server" class="iapps-logo">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="~/iapps_images/unbound-toolbar-logo.png" AlternateText="Unbound" />
                    </span>
                    <asp:ListView ID="lvProductNavigation" runat="server" ItemPlaceholderID="phProductNavigation">
                        <LayoutTemplate>
                            <div class="iapps-dropdown-menu-container">
                                <div class="iapps-dropdown-menu">
                                    <ul>
                                        <asp:PlaceHolder ID="phProductNavigation" runat="server" />
                                    </ul>
                                </div>
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:LinkButton ID="lbTitle" runat="server" CausesValidation="false" />
                            </li>
                        </ItemTemplate>
                    </asp:ListView>
                </li>
            </ul>
            <asp:ListView ID="lvPageModes" runat="server" ItemPlaceholderID="phPageModes">
                <LayoutTemplate>
                    <ul class="iapps-page-modes">
                        <asp:PlaceHolder ID="phPageModes" runat="server" />
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li id="liPageMode" runat="server">
                        <asp:LinkButton ID="lbPageMode" runat="server" CausesValidation="false">
                            <span>
                                <asp:Literal ID="litTitle" runat="server" />
                            </span>
                        </asp:LinkButton>
                    </li>
                </ItemTemplate>
            </asp:ListView>
            <asp:PlaceHolder ID="phEmailBuilder" runat="server" Visible="true">
                <asp:ListView ID="lvEmailSteps" runat="server" ItemPlaceholderID="phEmailSteps">
                    <LayoutTemplate>
                        <ul class="iapps-email-builder-steps iapps-clear-fix">
                            <asp:PlaceHolder ID="phEmailSteps" runat="server" />
                        </ul>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:LinkButton ID="lbtnSteps" runat="server" CausesValidation="false" />
                        </li>
                    </ItemTemplate>
                </asp:ListView>
            </asp:PlaceHolder>
            <asp:Panel ID="pnlButtons" runat="server" CssClass="iapps-buttons">
                <asp:Button ID="btnSave" runat="server" CssClass="iapps-button iapps-button-primary iapps-email-toolbar-action" Text="Save" CommandName="Save" OnClick="btnSave_Click" CausesValidation="false" />
                <asp:Button ID="btnPublish" runat="server" CssClass="iapps-button iapps-button-primary iapps-email-toolbar-action" Text="Publish" CommandName="Publish" OnClick="btnSave_Click" CausesValidation="false" />
            </asp:Panel>
            <asp:Panel ID="pnlPreview" runat="server" CssClass="iapps-buttons" Visible="false">
                <asp:DropDownList ID="ddlPreviewSites" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPreviewSites_SelectedIndexChanged" />
            </asp:Panel>
            <asp:ListView ID="lvUserOptions" runat="server" ItemPlaceholderID="phUserOptions">
                <LayoutTemplate>
                    <ul class="iapps-site-editor-dropdown iapps-user-options">
                        <asp:PlaceHolder ID="phUserOptions" runat="server" />
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li id="liListItem" runat="server">
                        <span>
                            <asp:Literal ID="litTitle" runat="server" />
                        </span>
                        <asp:ListView ID="lvDropDown" runat="server" ItemPlaceholderID="phDropDown">
                            <LayoutTemplate>
                                <div class="iapps-dropdown-menu-container">
                                    <div class="iapps-dropdown-menu">
                                        <asp:PlaceHolder ID="phDropDown" runat="server" />
                                    </div>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <asp:ListView ID="lvMenus" runat="server" ItemPlaceholderID="phMenus">
                                    <LayoutTemplate>
                                        <ul>
                                            <asp:PlaceHolder ID="phMenus" runat="server" />
                                        </ul>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <asp:LinkButton ID="lbMenuItem" runat="server" CausesValidation="false" />
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ItemTemplate>
                        </asp:ListView>
                    </li>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</div>
<asp:HiddenField ID="hdnEmailBuilderData" runat="server" />

<asp:PlaceHolder ID="plMinifiedJS" runat="server">
    <iapps-site-editor></iapps-site-editor>
    <script type="text/javascript" src="/angular/script/iapps-site-editor.min.js" defer="defer"></script>
</asp:PlaceHolder>

<asp:PlaceHolder ID="plDebugJS" runat="server" Visible="false">
    <script type="text/javascript" src="/angular/config/system.config.dev.js"></script>
    <script>
        System.import('app/siteeditor/iapps-site-editor.component')
              .then(null, console.error.bind(console));
    </script>
    <iapps-site-editor></iapps-site-editor>

</asp:PlaceHolder>

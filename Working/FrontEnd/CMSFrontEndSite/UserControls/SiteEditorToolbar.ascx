﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteEditorToolbar.ascx.cs" Inherits="CMSFrontEndSite.UserControls.SiteEditorToolbar" %>

<div class="iapps-reset">
    <div id="iapps-site-editor-toolbar" class="iapps-toolbar-expanded">
        <div class="iapps-container clearfix">
            <asp:PlaceHolder ID="phSiteEditor" runat="server">
                <ul class="iapps-site-editor-dropdown iapps-product-suite">
                    <li>
                        <span id="spanLogoContainer" runat="server" class="iapps-logo">
                            <asp:Image ID="imgLogo" runat="server" ImageUrl="~/iapps_images/unbound-toolbar-logo.png" AlternateText="Unbound" />
                        </span>
                        <asp:ListView ID="lvProductNavigation" runat="server" ItemPlaceholderID="phProductNavigation">
                            <LayoutTemplate>
                                <div class="iapps-dropdown-menu-container">
                                    <div class="iapps-dropdown-menu">
                                        <ul>
                                            <asp:PlaceHolder ID="phProductNavigation" runat="server" />
                                        </ul>
                                    </div>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li>
                                    <asp:LinkButton ID="lbTitle" runat="server" CausesValidation="false" />
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
                    </li>
                </ul>
                <asp:ListView ID="lvPageModes" runat="server" ItemPlaceholderID="phPageModes">
                    <LayoutTemplate>
                        <ul class="iapps-page-modes">
                            <asp:PlaceHolder ID="phPageModes" runat="server" />
                        </ul>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li id="liPageMode" runat="server">
                            <asp:LinkButton ID="lbPageMode" runat="server" CausesValidation="false">
                                <span>
                                    <asp:Literal ID="litTitle" runat="server" />
                                </span>
                            </asp:LinkButton>
                        </li>
                    </ItemTemplate>
                </asp:ListView>
                <asp:PlaceHolder ID="phPageStatus" runat="server">
                    <span class="iapps-page-status">
                        <asp:Literal ID="litPageStatus" runat="server" />
                    </span>
                </asp:PlaceHolder>
                <asp:ListView ID="lvUserOptions" runat="server" ItemPlaceholderID="phUserOptions">
                    <LayoutTemplate>
                        <ul class="iapps-site-editor-dropdown iapps-user-options">
                            <asp:PlaceHolder ID="phUserOptions" runat="server" />
                            <asp:PlaceHolder ID="phSiteSelector" runat="server" Visible="false">
                                <li class="iapps-site-selector" style="display: none;">
                                    <span>
                                        <asp:PlaceHolder ID="phEnvironment" runat="server">
                                            <span id="spnEnvironment" runat="server" class="iapps-current-environment">
                                                <asp:Literal ID="litEnvironment" runat="server" />
                                            </span>
                                        </asp:PlaceHolder>
                                        <asp:Literal ID="litSite" runat="server" />
                                    </span>
                                    <div class="iapps-dropdown-menu-container">
                                        <div class="iapps-dropdown-menu">
                                            <input type="text" class="iapps-site-filter" placeholder="Type to filter" />
                                            <ul></ul>
                                            <p class="iapps-site-filter-notice">Filter to see more sites</p>
                                        </div>
                                    </div>
                                </li>
                            </asp:PlaceHolder>
                        </ul>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li id="liListItem" runat="server">
                            <span>
                                <asp:Literal ID="litTitle" runat="server" />
                            </span>
                            <asp:ListView ID="lvDropDown" runat="server" ItemPlaceholderID="phDropDown">
                                <LayoutTemplate>
                                    <div class="iapps-dropdown-menu-container">
                                        <div class="iapps-dropdown-menu">
                                            <asp:PlaceHolder ID="phDropDown" runat="server" />
                                        </div>
                                    </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <asp:ListView ID="lvMenus" runat="server" ItemPlaceholderID="phMenus">
                                        <LayoutTemplate>
                                            <ul>
                                                <asp:PlaceHolder ID="phMenus" runat="server" />
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <asp:LinkButton ID="lbMenuItem" runat="server" CausesValidation="false" />
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </ItemTemplate>
                            </asp:ListView>
                        </li>
                    </ItemTemplate>
                </asp:ListView>
                <asp:PlaceHolder ID="phPagePreview" runat="server">
                    <div class="iapps-page-preview-options">
                        <asp:DropDownList ID="ddlPagePreviewSegment" runat="server" AutoPostBack="true" />
                        <asp:DropDownList ID="ddlPagePreviewDevice" runat="server" CssClass="iapps-page-preview-device">
                            <asp:ListItem>Desktop</asp:ListItem>
                            <asp:ListItem data-width="728" data-height="1024">Tablet</asp:ListItem>
                            <asp:ListItem data-width="414" data-height="736">Smartphone (large)</asp:ListItem>
                            <asp:ListItem data-width="320" data-height="568">Smartphone (small)</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlPagePreviewOrientation" runat="server" CssClass="iapps-page-preview-orientation">
                            <asp:ListItem>Portrait</asp:ListItem>
                            <asp:ListItem>Landscape</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phPageBuilder" runat="server">
                    <div class="iapps-buttons">
                        <asp:Button ID="btnPageBuilderCancel" runat="server" CausesValidation="false" CssClass="iapps-button" Text="Cancel" />
                        <asp:Button ID="btnPageBuilderSave" runat="server" CausesValidation="false" CssClass="iapps-button iapps-button-primary" OnClientClick="return SiteEditor.pageBuilder.save();" Text="Save" />
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phPageVariant" runat="server">
                    <div class="iapps-buttons">
                        <asp:Button ID="btnPageVariantCancel" runat="server" CausesValidation="false" CssClass="iapps-button" OnClientClick="return CloseDeviceWindow();" Text="Cancel" />
                        <asp:Button ID="btnPageVariantSave" runat="server" CausesValidation="false" CssClass="iapps-button iapps-button-primary" OnClientClick="return SaveDeviceWindow();" Text="Save" />
                    </div>
                </asp:PlaceHolder>
                <asp:ListView ID="lvSharedPage" runat="server" ItemPlaceholderID="phMenus">
                    <LayoutTemplate>
                        <div id="iapps-shared-page" class="iapps-modal iapps-modal-draggable iapps-modal-opacity" style="display: none;">
                            <div class="iapps-modal-header">
                                <h4>Shared Page</h4>
                            </div>
                            <div class="iapps-modal-content">
                                <ul class="iapps-vertical-list">
                                    <asp:PlaceHolder ID="phMenus" runat="server" />
                                </ul>
                            </div>
                            <div class="iapps-modal-footer">
                                <button class="iapps-button" data-modal-action="close">Close</button>
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="litMenu" runat="server" />
                        </li>
                    </ItemTemplate>
                </asp:ListView>
                <asp:ListView ID="lvPageBuilder" runat="server" ItemPlaceholderID="phControls">
                    <LayoutTemplate>
                        <div id="iapps-page-builder-controls" class="iapps-modal iapps-modal-draggable">
                            <div class="iapps-modal-header">
                                <h4>Controls</h4>
                            </div>
                            <div class="iapps-modal-content">
                                <asp:PlaceHolder ID="phControls" runat="server" />
                                <div class="iapps-drop-zone-remove">
                                    <p>Remove control</p>
                                </div>
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <asp:Panel ID="pnlControl" CssClass="iapps-content-container-placeholder" runat="server" data-context="toolbox">
                            <span class="iapps-control-type">
                                <asp:Literal ID="litControlType" runat="server" />
                            </span>
                        </asp:Panel>
                    </ItemTemplate>
                </asp:ListView>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phVersionPreview" runat="server">
                <div class="iapps-page-version-info">
                    <span class="iapps-column-left">
                        <asp:Literal ID="ltVersionNumber" runat="server" />
                    </span>
                    <span class="iapps-column-right">
                       <asp:Literal ID="ltVersionInfo" runat="server" /> 
                    </span>
                </div>
            </asp:PlaceHolder>
        </div>
        <span class="iapps-toolbar-toggle" title="Collapse/Expand Toolbar"></span>
    </div>
</div>

<asp:PlaceHolder ID="phMinifiedJS" runat="server" Visible="false">
    <iapps-site-editor></iapps-site-editor>
    <script type="text/javascript" src="/angular/script/iapps-site-editor.min.js" defer="defer"></script>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phDebugJS" runat="server" Visible="false">
    <script type="text/javascript" src="/angular/config/system.config.dev.js"></script>
    <script>
        System.import('app/siteeditor/iapps-site-editor.component')
            .then(null, console.error.bind(console));
    </script>
    <iapps-site-editor></iapps-site-editor>
</asp:PlaceHolder>

﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Text" %>

<script runat="server">

    private const String RandomPassword = "xaq5pHut";
    private const int FileDuration = 10; //in minutes
    private const String SiteOk = "SITE_IS_OK";
    private const String SiteOffline = "SITE_OFFLINE";

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        bool siteUp = true;
        if (BringOffline)
        {
            siteUp = false;
            CreateOffLineFile();
        }
        else if (BringOnline)
        {
            siteUp = true;
            DeleteOffLineFile();
        }
        else
        {
            FileInfo fileInfo = new FileInfo(FilePath);
            if (fileInfo.Exists)
            {
                if (DateTime.Now >= fileInfo.CreationTime.AddMinutes(FileDuration))
                {
                    siteUp = true;
                    DeleteOffLineFile();
                }
                else
                {
                    siteUp = false;
                }
            }
            else
            {
                siteUp = true;
            }
        }

        SetSiteStatus(siteUp);
    }

   private void DeleteOffLineFile()
    {
        try
        {
            FileInfo fileInfo = new FileInfo(FilePath);
            if (fileInfo.Exists)
            {
                File.Delete(FilePath);
            }
        }
        catch { } //If there is issue in file delete ignore it and make the site up.
    }

    private void CreateOffLineFile()
    {
        using (System.IO.StreamWriter file = new System.IO.StreamWriter(FilePath, false))
        {
            file.WriteLine("Offline");
        }
    }
    private void SetSiteStatus(bool siteUp)
    {
        if (siteUp)
            ltlSiteStatus.Text = SiteOk;
        else
            ltlSiteStatus.Text = SiteOffline;
    }
   
    private bool BringOffline
    {
        get
        {
            bool bringOffline = false;
            if(Request.QueryString["bringoffline"]!= null)
            {
                if (Request.QueryString["bringoffline"].Equals(RandomPassword))
                    bringOffline = true;
            }
            return bringOffline;
        }
    }

    private bool BringOnline
    {
        get
        {
            bool bringOnline = false;
            if (Request.QueryString["bringonline"] != null)
            {
                if (Request.QueryString["bringonline"].Equals(RandomPassword))
                    bringOnline = true;
            }
            return bringOnline;
        }
    }

    private String FilePath
    {
        get
        {
            return Server.MapPath("/") + "offline.txt";
        }
    }


    
</script>

    <form id="form1" runat="server">
    <div>
        <asp:Literal ID="ltlSiteStatus" runat="server"></asp:Literal>
    </div>
    </form>
